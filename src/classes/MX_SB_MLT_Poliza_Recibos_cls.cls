/**
* -------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_Poliza_Recibos_cls
* Autor: Angel Ignacio Nava
* Proyecto: Siniestros - BBVA
* Descripción : Clase controladora del componente MX_SB_MLT_Poliza_Recibos
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                       Descripción
* --------------------------------------------------------------------------------
* 1.0           02/01/2020     Angel Ignacio Nava           Creación
* --------------------------------------------------------------------------------
*/
public without sharing class MX_SB_MLT_Poliza_Recibos_cls { //NOSONAR
    /*
    * @description mthod paara consultaClipert
    * @param String numpoliza id de contrato (conocido como poliza)
    * @return List<Map<String,String>> lista de mapas para llenado de tablas en front
    */ 
    @auraEnabled
    public static List<Map<String,String>> consultaClipert(String numPoliza) {
        List<Map<String,String>> recibosList = new List<Map<String,String>> ();
        String sUrl ='';
        sUrl = 'https://150.250.242.234:36030/mfsa_mult_web_service_01/services/api/policy/V01/data/123456/certificates/1?origin={origin}&ucc={ucc}';
        String sMetAction ='';
        sMetAction = '/facturasClipert';
        String sToken ='';
        sToken = 'a1313';        
        String jsonBody ='';
          jsonBody = '{"test":"clipert"}';
          String endpoint ='';
          endpoint = sUrl + '' + sMetAction;
          final HttpRequest request = new HttpRequest();
          request.setEndpoint(endpoint);
          request.setMethod('GET');
          request.setHeader('Accept', 'application/json, text/plain, */*');
          request.setHeader('Content-Type', 'application/json;charset=UTF-8');
          request.setTimeout(120000);
          request.setBody(jsonBody);
          request.setHeader('acces-token', sToken);
        String sJsonRes = ' {"quotation": { "id": 616892, "number": null, ';
        sJsonRes+= '"expirationDate": "2019-16-08"}, "policy": { "id": null, "number": null, "validity": { "period": {"startDate": "2018-12-28", "endDate": "2019-01-28" }}}, "payment": { "certificate": "33244","receipt": "3424234","status":"COB","startDate":"2019-12-01","endDate":"2019-12","amount":"300.00","operation":"3424234","type":"FAC","description":"FACTURACION"}}';
        recibosList = parseResponse(sJsonRes,numPoliza);
        return recibosList;
    } 

    /*
    * @description mthod parseResponse para parsear el resultado de consulta
    * @param String sJsonRes json de respuesta String numpoliza id de contrato
    * @return List<Map<String,String>> lista de mapas para llenado de tablas en front
    */ 
    private static  List<Map<String,String>> parseResponse(String sJsonRes,String numPoliza) {
        String newsJsonRes = sJsonRes;
        newsJsonRes=numpoliza;
        final List<Map<String,String>> respuesta = new List<Map<String,String>>();
        final Map<String,String> respMap = new Map<String,String>();
        Map<String,String> datosClienteStr = new Map<String,String>();
        datosClienteStr =consultaCliente(numPoliza);
        respMap.put('nombre',datosClienteStr.get('nombre'));
        respMap.put('rfc',datosClienteStr.get('rfc'));
        respMap.put('numpoliza',newsJsonRes);
        respMap.put('certificate','31231');
        respMap.put('receipt','5454545');
        respMap.put('status','activo');
        respMap.put('startDate','2019-01-01');
        respMap.put('endDate','2019-01-01');
        respMap.put('operation','1231231');
        respMap.put('type','valid');
        respMap.put('amount','2000');
        respMap.put('description','operacion exito');
        respuesta.add(respMap);
        return respuesta;    
    } 

    /*
    * @description mthod consultaCliente realiza consulta de informacion del cliente
    * @param String id de contract
    * @return Map<String,String> mapade informacion del cliente
    */ 
     private static Map<String,String> consultaCliente(String numPoliza) {
         final Map<String,String> response = new  Map<String,String>();
         final contract contrato = [select id,accountid,MX_SB_SAC_NumeroPoliza__c from contract where id=:numPoliza limit 1];
         final account cuenta = [select firstname,lastname,id,RFC__c from account where id=:contrato.AccountId];
            response.put('nombre',cuenta.firstname+' '+cuenta.LastName);
        	response.put('rfc',cuenta.RFC__c);
         return response;
     } 
}