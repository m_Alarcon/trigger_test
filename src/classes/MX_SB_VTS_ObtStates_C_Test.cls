/**
* @description       : Clase de prueba para selector: MX_RTL_CustomMeta_Selector
* @author            : Diego Olvera
* @group             : BBVA
* @last modified on  : 02-25-2021
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   11-20-2020   Diego Olvera   Initial Version
* 1.1   02-18-2021   Diego Olvera 	Se agrega funcion de recuperacion de estados
* 1.2   25-02-2021   Diego Olvera   Se agrega funcion para cubrir controller de clase MX_SB_VTS_GetSetDatosDCDPFrm_Ctrl
**/
@isTest
public without sharing class MX_SB_VTS_ObtStates_C_Test {
    
    /**@description Nombre usuario*/
    private final static String ASESORCTRL = 'AsesorTest';
    /**@description Nombre del estado*/
    private final static String ESTADOCTRL = 'Campeche';
     /** Variable de Apoyo: aNoEmpty */
    Static String aNoEmpty = 'Lista No Vacia';
    
    @TestSetup
    static void makeData() {
        final User userRecCtrl = MX_WB_TestData_cls.crearUsuario(ASESORCTRL, 'System Administrator');
        insert userRecCtrl;
    }
    @isTest
    static void obtStatesValueSer() {
        final User userObtCtrl = [Select Id from User where LastName =: ASESORCTRL];
        System.runAs(userObtCtrl) {
            Test.startTest();
            try {
                MX_SB_VTS_ObtStates_Ctrl.getDirValueCtrl(ESTADOCTRL);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Error');
                System.assertEquals('Error', extError.getMessage(),'no recuperó los estados ctrl');
            }
            Test.stopTest();
        }
    }
    @isTest
    static void obtDirCtrl() {
        final User userObtCtrl = [Select Id from User where LastName =: ASESORCTRL];
        System.runAs(userObtCtrl) {
            Test.startTest();
            try {
                MX_SB_VTS_ObtStates_Ctrl.obtEstadosCtrl();
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal');
                System.assertEquals('Fatal', extError.getMessage(),'no recuperó valores del ctrl');
            }
            Test.stopTest();
        }
    }
     @isTest static void saveAddReCtrlTst() {
        final User tstUsr = MX_WB_TestData_cls.crearUsuario('TestUsr', System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(tstUsr) {
            Test.startTest();
            final Object objFillTest = MX_SB_VTS_GetSetDatosDCDPFrm_Test.obtDataTestDCDP();
            final Map<String, Object> mFillTest = (Map<String, Object>) objFillTest;
            final Map<String, Object> mValTest = MX_SB_VTS_GetSetDatosDCDPFrm_Ctrl.setDataFormDCDP(mFillTest);
            MX_SB_VTS_GetSetDatosDCDPFrm_Ctrl.saveAddReCtrl(mFillTest);
            Test.stopTest();
            System.assert(!mValTest.isEmpty(), aNoEmpty);
        }
    }
}