/*
----------------------------------------------------------
* Nombre: MX_SB_PS_MethodPago_Service_test
* Daniel Perez Lopez
* Proyecto: Salesforce-Presuscritos
* Descripción : clase test para clase MX_SB_PS_MethodPago_Service_cls

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   Descripción
* --------------------------------------------------------------------------------
* 1.0           08/07/2020     Daniel Perez		        Creación
* 1.1           23/02/2021     Juan Benitez      Se añade busqueda generica List SObject
* --------------------------------------------------------------------------------
*/
@SuppressWarnings('sf:UseSingleton')
@IsTest
public with sharing class MX_SB_PS_MetodoPago_Service_test {
    
     /*
    * @description Method setup
    * @param Opportunidad opp
    */
    @TestSetup
    static void dataservclstest() {
         
        MX_SB_PS_MetodoPago_test.creadatatest();
        Final MX_SB_VTS_Generica__c genrica= new MX_SB_VTS_Generica__c(MX_SB_VTS_Description__c='EXITO', 
                                             MX_SB_VTS_HEADER__c='EXITOSO', MX_SB_VTS_HREF__c='OK', MX_SB_VTS_SRC__c='OK', 
                                             MX_SB_VTS_Type__c='OPM', Name='mensaje');
        insert genrica;
    }
    
     /*
    * @description test a method actualizaoportunidad
    * @param Opportunidad opp
    */
    @isTest static void methactopp() {
        Final Opportunity oppmeth = [Select id,Name from Opportunity  limit 1];
        oppmeth.Name='nuevoname';
    Test.startTest();
        MX_SB_PS_MetodoPago_Service_cls.actualizaoportunidad(oppmeth);
        System.assertEquals('nuevoname',oppmeth.Name,'Se ha actualizado oportunidad');
    Test.stopTest();
    }

     /*
    * @description test a method getopportunity
    * @param String oppId,Fields
    */
    @isTest static void getOpportunity() {
        Final Opportunity getopp = [Select Id,Name from Opportunity limit 1];
        Test.startTest();
        getopp.name= 'optest';
        update getopp;
            Final Opportunity opname = MX_SB_PS_MetodoPago_Service_cls.getOpportunity(getopp.Id,' Id,Name ');
            System.assertEquals('optest',opname.Name,'Se ha encontrado oportnidad');
        Test.stopTest();
    }
    /* 
    *@ description test fndgeenricaOp
    * Busueda de mensajes de asesor
    */
    @isTest static void testfindTGenericaOP() {
        Test.startTest();
            Final list<MX_SB_VTS_Generica__c> strList =MX_SB_PS_MetodoPago_Service_cls.findTGenericaOP('OK');
            System.assertEquals(strList[0].MX_SB_VTS_HEADER__c,'EXITOSO','Se ha encontrado mensaje');
        Test.stopTest();
    }

    /*
    *
    */
    @isTest static void testfindGenericObject() {
        test.startTest();
        MX_SB_PS_MetodoPago_Service_cls.findGenericObject('3003','ivr');
        MX_SB_PS_MetodoPago_Service_cls.findGenericObject(null,'quot');
        Final List<Sobject> sob3= MX_SB_PS_MetodoPago_Service_cls.findGenericObject('ok','outToOP');
        System.assert(!sob3.isEmpty(),'Se ha encontrado Mensaje Asesor');
        test.stopTest();
    }

}