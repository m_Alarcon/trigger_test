/**
 * @File Name          : MX_SB_VTS_SendTrackingTelCupon_Helper.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 3/6/2020 22:47:36
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    28/5/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_SendTrackingTelCupon_Helper {

    /**variable ERROR creada*/
    public final static Integer ISERROR = 0;
    /**variable methodo Envio httpResponse*/
    public final static String METHOD = 'EntRegistroGestion';
    /**Constructor */
    private MX_SB_VTS_SendTrackingTelCupon_Helper() {}

    /**
    * @description Recupera bandeja de envios
    * @author Eduardo Hernandez Cuamatzi | 3/6/2020 
    * @return Map<String, MX_SB_VTS_Lead_tray__c> mapa de bandejas
    **/
    public static Map<String, MX_SB_VTS_Lead_tray__c> fillMapTrays() {
        final Map<String, MX_SB_VTS_Lead_tray__c> mapLeadsTry = new Map<String, MX_SB_VTS_Lead_tray__c>();
        for (MX_SB_VTS_Lead_tray__c lstTrays : MX_SB_VTS_Lead_Trays_Selector.findTraysHotLeads()) {
            mapLeadsTry.put(lstTrays.MX_SB_VTS_ProveedorCTI__r.MX_SB_VTS_Identificador_Proveedor__c, lstTrays);
        }
        return mapLeadsTry;
    }

    /**
    * @description Envio de Oportunidades por servicio
    * @author Eduardo Hernandez Cuamatzi | 3/6/2020 
    * @param String proveedor Proveedor activo
    * @param Map<Id MX_SB_VTS_FamliaProveedores__c> mapProveedors Mapa de proveedores
    * @param Map<String MX_SB_VTS_Lead_tray__c> mapLeadsTry mapa de bandejas
    * @param List<Opportunity> validOpps Oportunidades a enviar
    * @return Map<String, Object> Mapa de respuesta del servicio web
    **/
    public static Map<String,Object> sendOppsSmart(String proveedor, Map<Id,MX_SB_VTS_FamliaProveedores__c> mapProveedors, Map<String, MX_SB_VTS_Lead_tray__c> mapLeadsTry, List<Opportunity> validOpps) {
        final String serviceId = MX_SB_VTS_LeadMultiCTI_Util.evaluteSmartValue();
        final Map<Id,String> schedults = new Map<Id,String>();
        final String timeUser = MX_SB_VTS_SendTrackingTelemarketing_cls.findLocalTime(System.Label.MX_SB_VTS_FormatDateSmart);
        final List<String> varListName = new List<String>();
        final List<SObject> lstOppsSOb = (List<SObject>)validOpps;
        for(SObject objectRec : validOpps) {
            schedults.put(objectRec.Id, MX_SB_VTS_SendTrackingTelemarketing_cls.validaCuponesreq(timeUser, '', varListName));
        }
        return MX_SB_VTS_SendLead_helper_cls.EntRegistroGestion_invoke(METHOD, lstOppsSOb, '', 
            serviceId, mapLeadsTry.get(proveedor).MX_SB_VTS_ID_Bandeja__c, schedults, '');
    }

    /**
    * @description Procesa respuesta del servicio
    * @author Eduardo Hernandez Cuamatzi | 3/6/2020 
    * @param Map<String Object> responseSmart Mapa de respuesta del servicio
    * @param Map<String MX_SB_VTS_Lead_tray__c> mapLeadsTry Mapa de bandejas
    * @param List<Opportunity> validOpps Lista de Oportunidades enviadas
    * @param String proveedor Proveedor al que se enviaron los registros
    * @return List<SObject> Lista de Objectos procesados
    **/
    public static List<SObject> processResponseOpps(Map<String,Object> responseSmart, Map<String, MX_SB_VTS_Lead_tray__c> mapLeadsTry, List<Opportunity> validOpps, String proveedor) {
        final List<SObject> lstOpps = new List<SObject>();
        final Integer codeRespSmart = (Integer)responseSmart.get('CARGA');
        Boolean sendOk = true;
        if (codeRespSmart < ISERROR) {
            sendOk = false;
        }
        final Set<String> setOppsId = new Set<String>();
        for(Opportunity oppId : validOpps) {
            setOppsId.add(oppId.Id);
        }
        for(Opportunity oppsCupon : MX_RTL_Opportunity_Selector.selectSObTrayById(setOppsId)) {
            oppsCupon.EnviarCTI__c = sendOk;
            oppsCupon.MX_SB_VTS_TrayAttention__c = mapLeadsTry.get(proveedor).MX_SB_VTS_ID_Bandeja__c;
            oppsCupon.xmlRespuesta__c = String.valueOf(responseSmart.get('CARGA'));
            lstOpps.add(oppsCupon);
        }
        return lstOpps;
    }
}