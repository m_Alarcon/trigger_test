/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase test de Account - selector

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     25/05/2020      Roberto Isaac Soto Granados           Creación Clase
*   1.1     09/06/2020      Daniel Perez Lopez                    Modificacion
*   1.2     09/07/2020      Juan Carlos Benitez                   Modificacion
*   1.3     07/10/2020      Gabriel García                        Se agregan methods test
*****************************************************************************************/
@isTest
private class MX_RTL_Account_Selector_test {
    /*Usuario de pruebas*/
    private static User testUserAcc = new User();
    /*Caso de pruebas*/
    private static Case testCaseAcc = new Case();
    /*Aclaración de pruebas*/
    private static BPyP_Aclaraciones__c testAclara = new BPyP_Aclaraciones__c();
    /*cuenta de pruebas*/
    private static Account testAcc = new Account();
    /*Visita de pruebas*/
    private static dwp_kitv__Visit__c testVisit = new dwp_kitv__Visit__c();
    /*String para folios*/
    private static String testCodeAcc = '1234';
    /* VAR CONDITION */
    Final static String CONDITION='Id';

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        final String nameProfile = [SELECT Name from profile where name in ('Administrador del sistema','System Administrator') limit 1].Name;
        final User objUsrTst = MX_WB_TestData_cls.crearUsuario('PruebaAdminTst', nameProfile);
        insert objUsrTst;
        System.runAs(objUsrTst) {
        final String  tiporegistro = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
		final Account ctatest = new Account(recordtypeid=tiporegistro,firstname='JOSE',lastname='MARTINEZ',apellidomaterno__C='SANCHEZ',RFC__c='JOMS920912',PersonEmail='test@gmail.com');
        insert ctatest;
        }
        testUserAcc = UtilitysDataTest_tst.crearUsuario('testUser', 'BPyP Estandar', 'BPYP BANQUERO BANCA PERISUR');
        testUserAcc.Title = 'Privado';
        insert testUserAcc;

        System.runAs(testUserAcc) {
            testAcc.LastName = 'testAcc';
            testAcc.FirstName = 'testAcc';
            testAcc.OwnerId = testUserAcc.Id;
            testAcc.No_de_cliente__c = testCodeAcc;
            insert testAcc;
            testCaseAcc.OwnerId = testUserAcc.Id;
            testCaseAcc.Status = 'Nuevo';
            testCaseAcc.MX_SB_SAC_Folio__c = testCodeAcc;
            testCaseAcc.Description = 'Description';
            testCaseAcc.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_EU_Case_Apoyo_General'].Id;
            insert testCaseAcc;
            testAclara.BPyP_Producto__c = 'TDC';
            testAclara.BPyP_Tarjeta__c = testCodeAcc;
            testAclara.BPyP_Folio__c = testCodeAcc;
            testAclara.BPyP_NumeroCliente__c = testCodeAcc;
            testAclara.BPyP_Caso__c = testCaseAcc.Id;
            insert testAclara;
            testVisit.dwp_kitv__visit_start_date__c = Date.today()+4;
            testVisit.dwp_kitv__visit_duration_number__c = '15';
            testVisit.dwp_kitv__visit_status_type__c = '04';
            insert testVisit;
        }
    }

    /*Ejecuta la acción para cubrir clase*/
    static testMethod void cuentasPorNoDeClienteTest() {
        List<Account> aclaraciones = new List<Account>();
        testUserAcc = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        testCaseAcc = [SELECT Id, MX_SB_SAC_Folio__c FROM Case WHERE MX_SB_SAC_Folio__c =: testCodeAcc LIMIT 1];
        System.runAs(testUserAcc) {
            Test.startTest();
                aclaraciones = MX_RTL_Account_Selector.cuentasPorNoDeCliente(new Set<String>{testCaseAcc.MX_SB_SAC_Folio__c});
            Test.stopTest();
        }
        System.assert(!aclaraciones.isEmpty(), 'Cuentas encontrados');
    }

    /*Ejecuta la acción para cubrir method*/
    static testMethod void cuentabynombretest() {
        final Map<String,String> nombrecompleto = new Map<String,String>();
        nombrecompleto.put('nombreCta','JOSE');
        nombrecompleto.put('aPaternoCta','MARTINEZ');
        nombrecompleto.put('aMaternoCta','SANCHEZ');
        Test.startTest();
            final List<Account> cuentaspoliza = MX_RTL_Account_Selector.getAccountByNombre(nombrecompleto);
        Test.stopTest();
        System.assert(!cuentaspoliza.isEmpty(), 'Cuentas encontrados');
    }
    @isTest
    static void testgetAccountReusable() {
        Final Id ide = [SELECT Id from Account where LastName = 'testAcc' and FirstName = 'testAcc' limit 1].Id;
        Final set<Id> ids = new Set<Id>{ide};
		Final Map<String,String> mapa = new Map<String, String>();
        	mapa.put('fields', ' Id, Segmento__c,Name, MX_RTL_CURP__pc  ');
        test.startTest();
        	MX_RTL_Account_Selector.getAccountReusable(ids, CONDITION, mapa);
                system.assert(true,'Se ha encontrado datos de la cuenta');
        test.stopTest();
    }
    @isTest
    static void testupsrtAccount() {
        Final Account pAcc =[SELECT Id from Account where LastName = 'testAcc' and FirstName = 'testAcc' limit 1];
        pAcc.MX_RTL_CURP__pc='BEHJ920306HDFNRN00';
        Final Account pAcc2= new Account();
        pAcc2.FirstName='Luis';
        pAcc2.LastName='Herrera';
        pAcc2.RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
        test.startTest();
        	MX_RTL_Account_Selector.upsrtAccount(pAcc2);
            system.assert(true,'Se han actualizado los datos de la cuenta');
        test.stopTest();
    }

    /*
    * @description method que obtiene agregateResult
    * @param  void
    */
    @isTest
    static void fetchListAggAccByDataBaseTest() {
        final List<Account> listAccSel = [SELECT Id FROM Account LIMIT 1];
        List<AggregateResult> listVAggVisitT = new List<AggregateResult>();
        listVAggVisitT = MX_RTL_Account_Selector.fetchListAggAccByDataBase(' count(Id) cuenta', ' WHERE Id =: filtro0', new List<String>{listAccSel[0].Id,'','','',''}, new List<String>());
        listVAggVisitT.clear();
        listVAggVisitT = MX_RTL_Account_Selector.fetchListAggAccByDataBase(' count(Id) cuenta', ' WHERE Id =: filtro0', new List<String>{listAccSel[0].Id,'','','',''});
        System.assertEquals(listVAggVisitT[0].get('cuenta'), 1, 'Error SOQL');
    }

    /*
    * @description method que obtiene Lista de Cuentas
    * @param  void
    */
    @isTest
    static void fetchListAccByDataBaseTest() {
        final List<Account> listAccSelT = [SELECT Id FROM Account LIMIT 1];
        final List<Account> listAccT = MX_RTL_Account_Selector.fetchListAccByDataBase(' Id ', ' WHERE Id =: filtro0', new List<String>{listAccSelT[0].Id,'','','',''}, System.today(), System.today());
        System.assertEquals(listAccSelT[0].Id, listAccT[0].Id, 'SOQL Fail');
    }

     /*
    * @description method que obtiene Lista de Cuentas
    * @param  void
    */
    @isTest
    static void fetchListAccByIdsTest() {
        final List<Account> listAccSelT = [SELECT Id FROM Account LIMIT 1];
        List<Account> listAccT = new List<Account>();
        listAccT = MX_RTL_Account_Selector.fetchListAccByRecType(' Id ', '', new List<String>{'','','','',''}, new List<String>());
        listAccT.clear();
        listAccT = MX_RTL_Account_Selector.fetchListAccByIds(' Id ', ' WHERE Id IN: setIds', new List<String>{'','','','',''}, new Set<Id>{listAccSelT[0].Id});
        System.assertEquals(listAccSelT[0].Id, listAccT[0].Id, 'SOQL Fail');
    }
}