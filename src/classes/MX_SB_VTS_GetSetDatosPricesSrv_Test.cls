/**
 * @description       : Clase de cobertura que da soporte a MX_SB_VTS_GetSetDatosPricesSrv_Ctrl
 * @author            : Alexandro Corzo
 * @group             : 
 * @last modified on  : 26-02-2021
 * @last modified by  : Alexandro Corzo
 * Modifications Log 
 * Ver   Date         Author              Modification
 * 1.0   26-02-2021   Alexandro Corzo     Initial Version
 * 1.1   09-03-2021   Eduardo Hernández   Se corrige clase test
**/
@isTest
public class MX_SB_VTS_GetSetDatosPricesSrv_Test {
    /** Variable de Apoyo: sLeadSource */
    Static String sLeadSource = 'Call me back';
    /** Variable de Apoyo: sProducto */
    Static String sProducto = 'Hogar seguro dinámico';
    /** Variable de Apoyo: sVenta */
    Static String sVenta = 'Venta';
    /** Variable de Apoyo: sNoEmpty */
    Static String sNoEmpty = 'Lista No Vacia';
    /** Variable de Apoyo: sUserName */
    Static String sUserName = 'UserOwnerTest01';
    /** Variables de Apoyo: sPersonAcct */
    Static String sPersonAcct = 'PersonAccount'; 
    /** Variable de Apoyo: sURL */
    Static String sUrl = 'http://www.example.com';
    /** Variable de Apoyo: sContratos */
    Static String sContratos = 'Contratos';
    /**Tipo de domicilio */
    public final static String DOMTYPE = 'Domicilio asegurado';
    /** Variable de Apoyo: STROPPOCOMD */
    public final static String STROPPOCOMD = 'CmbCompleta';
    /** Variable de Apoyo: STROPPOREND */
    public final static String STROPPOREND = 'CmbRentado';
    /** Variable de Apoyo: STROPPOOCOM */
    public final static String STROPPOOCOM = 'OutBoundCompleta';
    /** Variable de Apoyo: STRQCMBCOM */
    public final static String STRQCMBCOM = 'Call me back-Completa';
    /** Variable de Apoyo: STRQCMBREN */
    public final static String STRQCMBREN = 'Call me back-Rentado';
    /** Variable de Apoyo: STRQOUTCOM */
    public final static String STRQOUTCOM = 'Outbound-Completa';
    /** Variable de Apoyo: CreatePrices */
    public final static String CREATEPRICES = 'createPrices';
    /**@description llave de mapa*/
    private final static string TOKEN = '12345678';
    /**@description llave de ERROR*/
    private final static string TSEC = 'tsec';

    /**
     * @description: función de preparación Clase de Prueba 
     * @author: 	 Alexandro Corzo
     */   
    @TestSetup
    static void makeData() {      
        final String strOppoComId = obtOppoId(STROPPOCOMD);
        obtQuoteId(strOppoComId, STRQCMBCOM, '1234567890');
        final String strOppoRenId = obtOppoId(STROPPOREND);
        obtQuoteId(strOppoRenId, STRQCMBREN, '0987654321');
        final String strOppoOCId = obtOppoId(STROPPOOCOM);
        obtQuoteId(strOppoOCId, STRQOUTCOM, '1029384756');
        insert new iaso__GBL_Rest_Services_Url__c(Name = CREATEPRICES, iaso__Url__c = sUrl, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
    }

    /**
     * @description: Instrucción de Clase de Prueba Call me back - Completa
     * @author: 	 Alexandro Corzo
     */
    @isTest static void tstDataPricCtrl() {
        final User oUsr = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(oUsr) {
            final String objMCallOutStr = MX_SB_VTS_ScheduldOppsTele_tst.quotetationStr(CREATEPRICES);
            final Map<String, String> headersMock = new Map<String, String>();
            headersMock.put(TSEC, TOKEN);
            final MX_WB_Mock objMCallOut = new MX_WB_Mock(200, 'Complete', objMCallOutStr, headersMock);
            iaso.GBL_Mock.setMock(objMCallOut);
            Test.startTest();
                final String strOppoName = strOppoComD;                                      
                final List<Opportunity> lstOppo = MX_RTL_Opportunity_Selector.resultQueryOppo('Id', 'Name =\'' + strOppoName + '\'', true);
                final String strOppoId = lstOppo[0].Id;                   
                final List<Object> lstData = MX_SB_VTS_GetSetDatosPricesSrv_Ctrl.setDataPricesCtrl(strOppoId, '1000000');
            Test.stopTest();
            System.assert(!lstData.isEmpty(), sNoEmpty);
        }
    }

	/**
     * @description: Instrucción de Clase de Prueba Call me back - Rentado
     * @author: 	 Alexandro Corzo
     */
    @isTest static void tstDataPricRen() {
        final User oUsr = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(oUsr) {
            final String objMCallOutStr = MX_SB_VTS_ScheduldOppsTele_tst.quotetationStr(CREATEPRICES);
            final Map<String, String> headersMock = new Map<String, String>();
            headersMock.put(TSEC, TOKEN);
            final MX_WB_Mock objMCallOut = new MX_WB_Mock(200, 'Complete', objMCallOutStr, headersMock);
            iaso.GBL_Mock.setMock(objMCallOut);
            Test.startTest();
                final String strOppoName = strOppoRenD;                                      
                final List<Opportunity> lstOppo = MX_RTL_Opportunity_Selector.resultQueryOppo('Id', 'Name =\'' + strOppoName + '\'', true);
                final String strOppoId = lstOppo[0].Id;                   
                final List<Object> lstData = MX_SB_VTS_GetSetDatosPricesSrv_Ctrl.setDataPricesCtrl(strOppoId, '1000000');
            Test.stopTest();
            System.assert(!lstData.isEmpty(), sNoEmpty);
        }
    }          
	
	/**
     * @description: Instrucción de Clase de Prueba Outbound - Completa
     * @author: 	 Alexandro Corzo
     */
    @isTest static void tstDataOutCom() {
        final User oUsr = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(oUsr) {
            final String objMCallOutStr = MX_SB_VTS_ScheduldOppsTele_tst.quotetationStr(CREATEPRICES);
            final Map<String, String> headersMock = new Map<String, String>();
            headersMock.put(TSEC, TOKEN);
            final MX_WB_Mock objMCallOut = new MX_WB_Mock(200, 'Complete', objMCallOutStr, headersMock);
            iaso.GBL_Mock.setMock(objMCallOut);
            Test.startTest();
                final String strOppoName = strOppoOCom;                                      
                final List<Opportunity> lstOppo = MX_RTL_Opportunity_Selector.resultQueryOppo('Id', 'Name =\'' + strOppoName + '\'', true);
                final String strOppoId = lstOppo[0].Id;                   
                final List<Object> lstData = MX_SB_VTS_GetSetDatosPricesSrv_Ctrl.setDataPricesCtrl(strOppoId, '1000000');
            Test.stopTest();
            System.assert(!lstData.isEmpty(), sNoEmpty);
        }
    }

    /**
     * @description: Genera una Opportunity para clase de prueba
     * @author: 	 Alexandro Corzo
     */
    private static String obtOppoId(String strNameOppo) {
        final User oUsrOppo = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        insert oUsrOppo;
        final Account oAcc = MX_WB_TestData_cls.crearCuenta(sContratos, sPersonAcct);
        oAcc.PersonEmail = 'test@test.com';
        oAcc.PersonMobilePhone = '5555555555';
        insert oAcc;
        final Opportunity oppObj = MX_WB_TestData_cls.crearOportunidad(strNameOppo, oAcc.Id, oUsrOppo.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
        oppObj.Reason__c = sVenta;
        oppObj.Producto__c = System.Label.MX_SB_VTS_Hogar;
        oppObj.StageName = System.Label.MX_SB_VTS_COTIZACION_LBL;
        oppObj.TelefonoCliente__c = oAcc.PersonMobilePhone;
        oppObj.LeadSource = System.Label.MX_SB_VTS_OrigenCallMeBack;
        insert oppObj;
        final MX_RTL_MultiAddress__c addressT = new MX_RTL_MultiAddress__c();
        addressT.MX_RTL_MasterAccount__c = oAcc.Id;
        addressT.MX_RTL_AddressStreet__c = 'test';
        addressT.MX_RTL_AddressType__c = DOMTYPE;
        addressT.Name = 'Direccion Propiedad';
        addressT.MX_RTL_Opportunity__c = oppObj.Id;
        addressT.MX_RTL_PostalCode__c = '14260';
        addressT.MX_RTL_Tipo_Propiedad__c = 'Rentado';
        addressT.MX_SB_VTS_Metros_Cuadrados__c  = '100';
        insert addressT;
        return oppObj.Id;
    }

    /**
     * @description: Genera una Quote para clase de prueba
     * @author: 	 Alexandro Corzo
     */                                                   
    private static String obtQuoteId(String strOpporId, String strQuoteName, String strFolioCot) {
        final Quote objQuo = MX_WB_TestData_cls.crearQuote(strOpporId, strQuoteName, strFolioCot);
        objQuo.MX_SB_VTS_ASO_FolioCot__c = strFolioCot;
        objQuo.QuoteToName = strQuoteName;
        insert objQuo;
        final Opportunity objOpp = new Opportunity();
        objOpp.Id = strOpporId;
        objOpp.SyncedQuoteId = objQuo.Id;
        update objOpp;
        return objQuo.Id;
    }
}