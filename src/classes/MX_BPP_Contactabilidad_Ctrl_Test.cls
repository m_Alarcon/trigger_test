/**
* @File Name          : MX_BPP_Contactabilidad_Ctrl_Test.cls
* @Description        : Test class for MX_BPP_Contactabilidad_Ctrl
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 07/10/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      07/10/2020            Gabriel Garcia Rojas          Initial Version
**/
@isTest
private  class MX_BPP_Contactabilidad_Ctrl_Test {
	    /*Usuario de pruebas*/
    private static User testUserStartCtrl = new User();
    /** namerole variable for records */
    final static String NAME_ROLESCR = '%BPYP BANQUERO BANCA PERISUR%';
    /** nameprofile variable for records */
    final static String NAME_PROFILECR = 'BPyP Estandar';
    /** name variable for records */
    final static String NAMESTCR = 'testUser';
    /** name variable for records */
    final static String NAMECR_SUCURSAL = '6343 PEDREGAL';
    /** namedivision variable for records */
    final static String NAMECR_DIVISION = 'METROPOLITANA';
    /** name variable for oficina */
    final static String PRIVADOSCR ='PRIVADO';

    /*Variable test 15*/
    static final String NUMCT15='15';
    /** error message */
    final static String MESSAGECTRL = 'Fail method';

     /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testUserStartCtrl = UtilitysDataTest_tst.crearUsuario(NAMESTCR, NAME_PROFILECR, NAME_ROLESCR);
        testUserStartCtrl.Title = PRIVADOSCR;
        testUserStartCtrl.Divisi_n__c = NAMECR_DIVISION;
        testUserStartCtrl.BPyP_ls_NombreSucursal__c = NAMECR_SUCURSAL;
        testUserStartCtrl.VP_ls_Banca__c = 'Red BPyP';
        insert testUserStartCtrl;

        final DateTime fechaTestCtrl = System.today();

        final Id rtAccCtrl = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta BPyP').getRecordTypeId();

        final Account cuentaCtrl = new Account(FirstName = 'CuentaTestCtrl', LastName = 'LastTestCtrl', No_de_cliente__c = '87654321', RecordTypeId = rtAccCtrl, OwnerId = testUserStartCtrl.Id );
        insert cuentaCtrl;

        System.runAs(testUserStartCtrl) {
            final List<dwp_kitv__Visit__c> listVisitaCt = new List<dwp_kitv__Visit__c>();
            final dwp_kitv__Visit__c visit1Ctrl = new dwp_kitv__Visit__c(Name = 'VisitaTest', dwp_kitv__visit_status_type__c = '05', dwp_kitv__account_id__c = cuentaCtrl.Id,
                                                              dwp_kitv__visit_duration_number__c = NUMCT15, dwp_kitv__visit_start_date__c = fechaTestCtrl);
            final dwp_kitv__Visit__c visit2Ctrl = new dwp_kitv__Visit__c(Name = 'VisitaTest2', dwp_kitv__visit_status_type__c = '01', dwp_kitv__account_id__c = cuentaCtrl.Id,
                                                               dwp_kitv__visit_duration_number__c = NUMCT15, dwp_kitv__visit_start_date__c = fechaTestCtrl);

            listVisitaCt.add(visit1Ctrl);
            listVisitaCt.add(visit2Ctrl);

            insert listVisitaCt;
        }
    }

    /**
     * @description constructor test
     * @author Gabriel Garcia | 07/10/2020
     * @return void
     **/
    @isTest
    private static void testConstructor() {
        final MX_BPP_Contactabilidad_Ctrl instanceTestSCtrl = new MX_BPP_Contactabilidad_Ctrl();
        System.assertNotEquals(instanceTestSCtrl, null, MESSAGECTRL);
    }

    /**
     * @description test userInfoCtrl
     * @author Gabriel Garcia | 07/10/2020
     * @return void
     **/
    @isTest
    private static void userInfoCtrlTest() {
        final User usuarioDOCC = [SELECT Id FROM User WHERE Name=: NAMESTCR];
        System.runAs(usuarioDOCC) {
            final MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper wrapperCtrlCont = MX_BPP_Contactabilidad_Ctrl.fetchInfoByUser();
            System.assertNotEquals(wrapperCtrlCont, null, MESSAGECTRL);
        }
    }

    /**
     * @description test fetchUserInfo
     * @author Gabriel Garcia | 07/10/2020
     * @return void
     **/
    @isTest
    private static void fetchUserInfoTest() {
        final List<String> listinfoUserCC = MX_BPP_Contactabilidad_Ctrl.fetchUserInfo();
        System.assertNotEquals(listinfoUserCC.size(), 0 , MESSAGECTRL);
    }

    /**
     * @description test fetchData
     * @author Gabriel Garcia | 07/010/2020
     * @return void
     **/
    @isTest
    private static void fetchDataTest() {
        Test.startTest();
        final MX_BPP_Contactabilidad_Service.WRP_ChartStacked wrpCharTest = MX_BPP_Contactabilidad_Ctrl.fetchData('Division', PRIVADOSCR, new List<String>{''}, null, null);
        System.assertNotEquals(wrpCharTest, null, MESSAGECTRL);

        Test.stopTest();
    }

    /**
     * @description test contactabilityAccount
     * @author Gabriel Garcia | 07/010/2020
     * @return void
     **/
    @isTest
    private static void contactabilityAccountTest() {
        Test.startTest();
        final List<Account> listAccTSer = MX_BPP_Contactabilidad_Ctrl.contactabilityAccount(new List<String>{'', 'Contact', PRIVADOSCR, '10', 'Division'} , null, null);
        System.assertNotEquals(listAccTSer, null, MESSAGECTRL);

        Test.stopTest();
    }

    /**
     * @description test fetchUserByOfficeServ
     * @author Gabriel Garcia | 07/010/2020
     * @return void
     **/
    @isTest
    private static void fetchUserByOfficeServTest() {
        Test.startTest();
        final List<User> listUserTSer = MX_BPP_Contactabilidad_Ctrl.fetchUserByOfficeServ(NAMECR_SUCURSAL, PRIVADOSCR);
        System.assertNotEquals(listUserTSer, null, MESSAGECTRL);

        Test.stopTest();
    }

    /**
     * @description test fetchPgs
     * @author Gabriel Garcia | 07/10/2020
     * @return void
     **/
    @isTest
    private static void fetchPgsCCTest() {
        final Integer totalPagCC = MX_BPP_Contactabilidad_Ctrl.fetchPgs(20);
        System.assertEquals(totalPagCC, 2 , MESSAGECTRL);
    }

    /**
     * @description test fetchPgs
     * @author Gabriel Garcia | 10/11/2020
     * @return void
     **/
    @isTest
    static void gtRISelectTest() {
        final User usua = [SELECT Id FROM User WHERE Name =: NAMESTCR];
        test.startTest();
        System.runAs(usua) {
            final EU001_RI__c varRi = MX_BPP_UserAndRIDataFactory.creaRI(usua.Id);
            insert varRi;
            final String userTitle = MX_BPP_Contactabilidad_Ctrl.gtRISelect(varRi.Id);
            System.assertEquals(PRIVADOSCR, userTitle, MESSAGECTRL);
        }
        test.stopTest();
    }
}