/**
 * @description       : 
 * @author            : Daniel Perez Lopez
 * @group             : 
 * @last modified on  : 11-24-2020
 * @last modified by  : Daniel Perez Lopez
 * Modifications Log 
 * Ver   Date         Author               Modification
 * 1.0   11-24-2020   Daniel Perez Lopez   Initial Version
**/
@isTest
public class MX_SB_PS_wrpColoniasResp_Test {
    @isTest
    static void method1() {
		test.startTest();
        	final MX_SB_PS_wrpColoniasResp clase = new MX_SB_PS_wrpColoniasResp();
        	System.assertEquals('CDMX',clase.iCatalogItem.suburb[0].city.name,'evaluado con exito ');
        test.stopTest();
    }
}