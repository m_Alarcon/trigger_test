/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RTL_ContentVersion_Selector_Test
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2021-01-14
* @Description 	Test Class for MX_RTL_ContentVersion_Selector
* @Changes            Date		|		Author		|		Description
* 			      2021-03-29		Héctor Saldaña	  New methods added: getrecordByLinkEntityIdTest,
* 			                                          getContentVersionByDocsTest, getContentVersionByDocsTest
* 			                                          getLibrariesTest, getDocumentsTest
*  
*/
@isTest
public class MX_RTL_ContentVersion_Selector_Test {
    
    /*Usuario BPP de pruebas*/
    private static User testBPPUser = new User();
    
    @testSetup
    static void setup() {
        testBPPUser = UtilitysDataTest_tst.crearUsuario('testBPPUser', 'BPyP Estandar', 'BPYP BANQUERO BANCA PERISUR');
        testBPPUser.Title = 'Privado';
        insert testBPPUser;

        final User usuarioAdmin = UtilitysDataTest_tst.crearUsuario('PruebaAdm', Label.MX_PERFIL_SystemAdministrator, 'BBVA ADMINISTRADOR');
        insert usuarioAdmin;
    }
    
    /**
    * @Description 	Test Method for constructor
    * @Return 		NA
    **/
	@isTest
    static void constructorTest() {
        final MX_RTL_ContentVersion_Selector contentVSelector = new MX_RTL_ContentVersion_Selector();
        System.assertNotEquals(null, contentVSelector, 'Error on constructor');
    }
    
    /*
    * @description Test method for MX_RTL_ContentVersion_Selector.getContentVersionByIds()
    * @param  void
    */
    @isTest
    static void getContentVersionByIdsTest() {
        final Set<Id> idsContentVersion = new Set<Id>();
        testBPPUser = [SELECT Id, Title FROM User WHERE LastName = 'testBPPUser'];
        final dwp_kitv__Visit__c testRecord = new dwp_kitv__Visit__c();
        System.runAs(testBPPUser) {
            testRecord.dwp_kitv__visit_start_date__c = Date.today()+4;
            testRecord.dwp_kitv__visit_duration_number__c = '15';
            testRecord.dwp_kitv__visit_status_type__c = '04';
            insert testRecord;
        }
        
        final ContentVersion contentVersion = new ContentVersion(Title = 'PenguinsBIE', PathOnClient = 'PenguinsBIE.pdf', VersionData = Blob.valueOf('Test ContentBIE'), FirstPublishLocationId=testRecord.Id);
        insert contentVersion;
        idsContentVersion.add(contentVersion.Id);
        
        final List<ContentVersion> results = MX_RTL_ContentVersion_Selector.getContentVersionByIds('Id, ContentDocumentId', idsContentVersion);
        System.assertNotEquals(null, results, 'Error on getContentVersionByIds');
    }

    /**
    * @description Test method which covers getContentDocLinkByLinkEntityId() method from Selector Class
    * @params NA
    * @return void
    **/
    @IsTest
    static void getrecordByLinkEntityIdTest() {
        final User admUsr = [SELECT Id FROM User WHERE LastName = 'PruebaAdm' LIMIT 1];
        Test.StartTest();

        System.runAs(admUsr) {
            final Account acc = new Account();
            acc.FirstName = 'user BPyP';
            acc.LastName = 'Test Acc';
            acc.No_de_cliente__c = 'D2050394';
            acc.RecordTypeId = RecordTypeMemory_cls.getRecType('Account', 'MX_BPP_PersonAcc_Client');
            acc.PersonEmail = 'testusr.minuta@test.com';
            insert acc;

            final dwp_kitv__Visit__c visitTest = new dwp_kitv__Visit__c();
            visitTest.Name = 'Visita Prueba';
            visitTest.dwp_kitv__visit_start_date__c = Date.today()+4;
            visitTest.dwp_kitv__account_id__c = acc.Id;
            visitTest.dwp_kitv__visit_duration_number__c = '15';
            visitTest.dwp_kitv__visit_status_type__c = '01';
            insert visitTest;

            final ContentVersion contentversion1 = new ContentVersion(Title = 'Test Files', PathOnClient = 'Test_File.pdf', VersionData = Blob.valueOf('Unit Test ContentVersion'), ContentLocation = 'S');
            insert contentversion1;

            final ContentVersion contentVersion2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id =: contentversion1.Id LIMIT 1];
            final ContentDocumentLink cdl = new ContentDocumentLink(ContentDocumentId = contentVersion2.ContentDocumentId, LinkedEntityId = visitTest.Id, ShareType = 'V');
            insert cdl;

            final List<ContentDocumentLink> cdlLst = MX_RTL_ContentVersion_Selector.getContentDocLinkByLinkEntityId(visitTest.id);
            System.assertEquals(cdlLst[0].ContentDocumentId, contentVersion2.ContentDocumentId, 'No se encontraron registros relacionados');
        }

        Test.StopTest();
    }

    /**
    * @description Test method which covers getContentVersionByContentDocument() method from Selector Class
    * @params NA
    * @return void
    **/
    @IsTest
    static void getContentVersionByDocsTest() {
        final User admUsr = [SELECT Id FROM User WHERE LastName = 'PruebaAdm' LIMIT 1];
        Test.StartTest();

        System.runAs(admUsr) {

            final ContentVersion contentversion1 = new ContentVersion(Title = 'Test PDF', PathOnClient = 'PDF_Test.pdf', VersionData = Blob.valueOf('Get Content Version By Document Id test'), ContentLocation = 'S');
            insert contentversion1;

            final ContentVersion contentVersion2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id =: contentversion1.Id LIMIT 1];

            final List<ContentVersion> cvLst = MX_RTL_ContentVersion_Selector.getContentVersionByContentDocument(new Set<Id>{contentVersion2.ContentDocumentId});
            System.assertEquals('Test PDF',cvLst[0].Title, 'Registro incorrecto');
        }

        Test.StopTest();
    }

    /**
    * @description Test method which covers getLibrariesByContentDocument() method from Selector Class
    * @params NA
    * @return void
    **/
    @IsTest
    static void getLibrariesTest() {
        final User admUsr = [SELECT Id FROM User WHERE LastName = 'PruebaAdm' LIMIT 1];
        Test.StartTest();

        System.runAs(admUsr) {
            final ContentWorkspace lib = new ContentWorkspace();
            lib.Name = 'Test Library';
            insert lib;
            final ContentWorkspace lib2 = [SELECT Id, Name from ContentWorkspace where Id =: lib.id LIMIT 1];
            final ContentVersion cv1tst = new ContentVersion(Title = 'Libraries Test', PathOnClient = 'Libraries_Test.pdf', VersionData = Blob.valueOf('Unit Test for get Libraries by Content Document'), ContentLocation = 'S');
            insert cv1tst;

            final ContentVersion cv2tst = [SELECT Id, Title, ContentDocumentId, FirstPublishLocationId FROM ContentVersion where Id =: cv1tst.Id LIMIT 1];
            final ContentDocumentLink cdl = new ContentDocumentLink(ContentDocumentId = cv2tst.ContentDocumentId, ShareType = 'I', Visibility = 'AllUsers', LinkedEntityId = lib2.Id);
            insert cdl;

            final List<ContentWorkspace> librariesLst = MX_RTL_ContentVersion_Selector.getLibrariesByContentDocument(new Set<Id>{cv2tst.ContentDocumentId});
            System.assertEquals('Test Library', librariesLst[0].Name, 'Libreria Incorrecta');
        }

        Test.StopTest();
    }

    /**
    * @description Test method which covers getContentDocumentById() method from Selector Class
    * @params NA
    * @return void
    **/
    @IsTest
    static void getDocumentsTest() {
        final User admUsr = [SELECT Id FROM User WHERE LastName = 'PruebaAdm' LIMIT 1];
        Test.StartTest();

        System.runAs(admUsr) {
            final ContentVersion content1 = new ContentVersion(Title = 'Documents Test', PathOnClient = 'Documents_Test.pdf', VersionData = Blob.valueOf('Unit Test for get Content Documents by Id'), ContentLocation = 'S');
            insert content1;

            final ContentVersion content2 = [SELECT Id, Title, ContentDocumentId, FirstPublishLocationId FROM ContentVersion where Id =: content1.Id LIMIT 1];

            final List<ContentDocument> docList = MX_RTL_ContentVersion_Selector.getContentDocumentById(new Set<Id>{content2.ContentDocumentId});
            system.assertEquals('Documents Test',docList[0].Title, 'Titulo del Documento Incorrecto');
        }

        Test.StopTest();
    }

}