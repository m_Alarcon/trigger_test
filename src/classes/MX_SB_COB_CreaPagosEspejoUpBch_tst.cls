/**
*
*Clase test MX_SB_COB_CreaPagosEspejoUpBch_tst
*/
@isTest
public class MX_SB_COB_CreaPagosEspejoUpBch_tst {
    @Testsetup
    static void setClase(){
        final Bloque__c bloque = new Bloque__c();
        insert bloque;

    }
	@isTest
    static void insertPagosEspejo() {
        final bloque__c bloque = [select id,name from bloque__c limit 1 ];
        final CargaArchivo__c archivo = new CargaArchivo__c(MX_SB_COB_Bloque__c=bloque.name,MX_SB_COB_ContadorConError__c=0,MX_SB_COB_ContadorSinError__c=0,MX_SB_COB_resultado__c='');
        insert archivo;


        final pagosespejo__c pago = new pagosespejo__c(MX_SB_COB_BloqueLupa__c =bloque.id,MX_SB_COB_factura__c='1233');
        final List<PagosEspejo__c> listaPagos = new List<PagosEspejo__c>();
        listaPagos.add(pago);
        Test.startTest();
        final MX_SB_COB_CreaPagosEspejoUpBch_cls espejoBatch1 = new  MX_SB_COB_CreaPagosEspejoUpBch_cls(listaPagos);
        final Id IDSecondJob = Database.executeBatch(espejoBatch1);//guardado de pagosespejo
       
        system.assertNotEquals(null, IDSecondJob, 'prueba resultado');
        Test.stopTest();  
    }
    @isTest
    static void updatePagosEspejo(){
        final bloque__c bloque = [select id,name from bloque__c limit 1];
                final CargaArchivo__c archivo = new CargaArchivo__c(MX_SB_COB_Bloque__c=bloque.name,MX_SB_COB_ContadorConError__c=0,MX_SB_COB_ContadorSinError__c=0,MX_SB_COB_resultado__c='');
        insert archivo;
        
        
        final pagosespejo__c pago = new pagosespejo__c(MX_SB_COB_BloqueLupa__c =bloque.id,MX_SB_COB_factura__c='1233');
        insert pago;
        
        final pagosespejo__c pagoUp = new pagosespejo__c(MX_SB_COB_BloqueLupa__c =bloque.id,MX_SB_COB_factura__c='1233');
        
        final List<PagosEspejo__c> listaPagos = new List<PagosEspejo__c>();
        listaPagos.add(pagoUp);
        Test.startTest();
            final MX_SB_COB_CreaPagosEspejoUpBch_cls espejoBatch1 = new  MX_SB_COB_CreaPagosEspejoUpBch_cls(listaPagos);
            final Id IDSecondJob = Database.executeBatch(espejoBatch1);//guardado de pagosespejo
         system.assertNotEquals(null, IDSecondJob, 'prueba resultado');
       Test.stopTest();
}
}