/**
* @File Name          : MX_RTL_Campaign_Selector_Test.cls
* @Description        : Test class for MX_RTL_Campaign_Selector
* @Author             : Gerardo Mendoza Aguilar
* @Group              :
* @Last Modified By   : Gerardo Mendoza Aguilar
* @Last Modified On   : 11-20-2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      18/11/2020           Gerardo Mendoza Aguilar          Initial Version
**/
@isTest
public Without Sharing class MX_RTL_Campaign_Selector_Test {
    /*Property FIEDLS*/
    final static String QUERYFIELDS = 'Id, EndDate';
   
    /**
    * @description
    * @TestSetup
    * Methodo para crear registros que serán usados en clase test
    * @author Gerardo Mendoza Aguilar | 11-18-2020 
    **/
    @TestSetup static void dataSelectorTest() {
        final Campaign campTest = new Campaign(Name = 'Campaign Test', CampaignMemberRecordTypeId=Schema.SObjectType.CampaignMember.getRecordTypeInfosByDeveloperName().get('MX_BPP_CampaignMember').getRecordTypeId());
        insert campTest;
    }

    /**
    * @description Methodo test usado para encontrar la informacion de la campania
    * 18/11/2020
    * @author Gerardo Mendoza Aguilar | 11-18-2020 
    **/
    @isTest
    public static void selectCampaignTest() {
        Test.startTest();
        Final Id ids = [SELECT Id FROM Campaign LIMIT 1].Id;
        Final String queryFilters = 'WHERE Id = '+'\''+ ids +'\'';
        Final List<Campaign> camp = MX_RTL_Campaign_Selector.selectCampaign(QUERYFIELDS, queryFilters);
        System.assert(!camp.isEmpty(), 'Campanya encontrada');
        Test.stopTest();
    }
}