/**
* Indra
* @author           Daniel perez lopez
* Project:          Presuscritos
* Description:      Clase test para methods de selector
*
* Changes (Version)
* ------------------------------------------------------------------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    --------------------------------------
* @version  1.0     2020-09-04      Daniel perez lopez.     Creación de la Clase
*/

@isTest
public class MX_RTL_PricebookEntry_Selector_tst {   
     /**
    * @Method Data Setup
    * @Description Method para preparar datos de prueba 
    * @return void
    **/
	@testSetup
    public static void  data() { 
    	MX_SB_PS_Container1_Ctrl_Test.data();
    }
    /**
    * @Method Data Setup
    * @Description Method para buscar quotelineitems 
    * @return void
    **/
    @isTest
    static void pricebookentries() {
        test.startTest();
            final Product2 prod = [Select id,Name from Product2 where Name='Respaldo Seguro Para Hospitalización' limit 1];
        	final String[] ids = new String[]{};
            ids.add(prod.Id);
            final PricebookEntry[] lista = MX_RTL_PricebookEntry_Selector.pricebookEntries(' id,PriceBook2Id' ,ids);
        	System.assert(lista.size()>0,'Exito en consulta');
        test.stopTest();
    }
	
    /**
    * @Method Data quoteliItems
    * @Description Method para obtener quotelineitems 
    * @return void
    **/
    @isTest
    static void quoteliItems() {
        test.startTest();
            final Quote qtstli = [Select id,Name from quote where name='qtest' limit 1];
            final Set<Id> ids = new Set<Id>();
            ids.add(qtstli.Id);
            QuoteLineItem[] lista2 = MX_RTL_QuoteLineItem_Selector.quoteliItems(' Id, QuoteId ',ids);
        	lista2[0].MX_SB_VTS_Plan__c='unplan';
        	lista2 = MX_RTL_QuoteLineItem_Selector.upsertQuoteLI(lista2);
        	System.assert(lista2.size()>0,'Exito en consulta');
        test.stopTest();
    }
}