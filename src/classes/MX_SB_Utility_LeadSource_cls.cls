/**-------------------------------------------------------------------------
* Nombre: MX_SB_Utility_LeadSource_cls
* @author Arsenio   Perez   Lopez
* Proyecto: MW SB - BBVA
* Descripción : Clase Utilidad Origen LeadSource
* --------------------------------------------------------------------------
*                         Fecha           Autor                   Descripción
* -------------------------------------------------------------------
* @version 1.0           05/11/2019      Arsenio Perez            Establece el valor del LeadSource.
* @version 1.1           13/08/2020      Francisco Javier         Fix - Se hace correccion de null en lastname
* @version 1.2		 19/08/2020	 Francisco Javier	  Fix - Se corrige issue en el update de cuentas
* --------------------------------------------------------------------------*/ 
public with sharing class MX_SB_Utility_LeadSource_cls  { //NOSONAR
    Private MX_SB_Utility_LeadSource_cls() {        
    }
    /**
        Method Descrip: Obtiene el origen de la oportunidad
     */
     public static Opportunity obtenerLeadSource(Opportunity oppAc) {
         final Opportunity oppAct = oppAc;
        if(IsBanq(oppAct)) {
            ChangeSource(oppAct);
        }
        return oppAct;
     }
     /**
     * @description 
     * @author ChangeMeIn@UserSettingsUnder.SFDoc | 7/11/2019 
     * @param opt 
     * @return Boolean 
     **/
     private static Boolean isBanq(Opportunity opt) {
         Boolean ownerbank =false;
         if (String.isNotBlank(opt.AccountId)) {
             final List<Account> tempa = [Select id, OwnerID from Account where id=:opt.AccountId];
             if (tempa.isEmpty()==false) {
                final List<Banquero__c> banx = [SELECT Id, Name, MX_SB_BCT_Banquero__c, MX_SB_BCT_Cliente__c,MX_SB_BCT_Suspendido__c FROM Banquero__c where MX_SB_BCT_Banquero__c =:tempa.get(0).ownerId and MX_SB_BCT_Suspendido__c=false and MX_SB_BCT_Cliente__c=:opt.AccountId];
                ownerbank = (banx.isEmpty()==false) ? true : false;
             }
         }
         return ownerbank;
     }
      /**
      * @description 
      * @author ChangeMeIn@UserSettingsUnder.SFDoc | 7/11/2019 
      * @param opt 
      * @return Opportunity 
      **/
      private static Opportunity changeSource(Opportunity opt) {
          final Opportunity oppAct = opt;
          String leadsourc = oppAct.LeadSource;
          if(String.isNotBlank(oppAct.id) && String.isBlank(oppAct.LeadSource)) {
              leadsourc = [Select id, LeadSource from Opportunity where id=:oppAct.Id].LeadSource;
          }
          String leadreturn = '';
              switch on leadsourc {
                  when 'Call me back' {
                      leadreturn = System.Label.MX_SB_BCT_OrigenCallMeBack;
                  }
                  when 'Tracking Web' {
                    leadreturn = System.Label.MX_SB_BCT_TRACKINGWEB_LBL;
                  }
                  when else {
                      leadreturn= leadsourc;
                  }
              }
              oppAct.LeadSource = leadreturn;
              return oppAct;
          }
}