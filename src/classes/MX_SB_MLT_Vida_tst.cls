@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_Vida_tst
* Autor Marco Antonio Cruz Barboza
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_Vida_Controller

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0           23/04/2020        Marco Cruz                        Creación
* -----------------------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_Vida_tst {
    /*
	* var String Valor Subramo Ahorro
	*/
    public static final String SUBRAMO ='Ahorro';
    /*
	* var String Valor boton cancelar
	*/    
    public static final String CANCELAR = 'Cancelar';
    /*
	* var String Valor boton Volver
	*/    
    public static final String VOLVER = 'Volver';
    /*
	* var String Valor boton Crear
	*/    
    public static final String CREAR = 'Crear';
    
    @TestSetup
    static void createData() {
        Final String profileVida = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User userVida = MX_WB_TestData_cls.crearUsuario('PruebaAdminTst', profileVida);  
        insert userVida;
        System.runAs(userVida) {
            final String  recordTId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Proveedores_MA).getRecordTypeId();
            final Account accountVidatst = new Account(RecordTypeId = recordTId, Name='Usuario Vida Test',Tipo_Persona__c='Física');
            insert accountVidatst;
            
            final Contract contractVida = new Contract(AccountId = accountVidatst.Id, MX_SB_SAC_NumeroPoliza__c='policyTest');
            insert contractVida;
            
            Final Siniestro__c vidaTest = new Siniestro__c();
            vidaTest.MX_SB_SAC_Contrato__c = contractVida.Id;
            vidaTest.MX_SB_MLT_NombreConductor__c = 'JUAN ALBERTO';
            vidaTest.MX_SB_MLT_APaternoConductor__c = 'TELLEZ';
            vidaTest.MX_SB_MLT_AMaternoConductor__c = 'ALCANTARA';
            vidaTest.MX_SB_MLT_Fecha_Hora_Siniestro__c =date.valueof('2020-03-27T22:04:00.000+0000');
            vidaTest.MX_SB_MLT_Telefono__c = '5534253647';
            Final datetime citaAgenda = datetime.now();
            vidaTest.MX_SB_MLT_CitaVidaAsegurado__c = citaAgenda.addDays(2);
            vidaTest.MX_SB_MLT_AtencionVida__c = 'Agendar Cita';
            vidaTest.MX_SB_MLT_PreguntaVida__c = false;
            vidaTest.MX_SB_MLT_SubRamo__c = SUBRAMO;
            vidaTest.TipoSiniestro__c = 'Siniestros';
            vidaTest.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_creacion;
            vidaTest.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoVida).getRecordTypeId();
            insert vidaTest;
        }
    }
    /*
    * @description method que prueba MX_SB_MLT_SiniestroVida_Controller camposSin
    * @param  void
    * @return String 
    */
    @isTest
    static void camposSiniestro () {
        Final String siniIdTest = [SELECT Id FROM Siniestro__c where MX_SB_MLT_SubRamo__c = 'Ahorro'].Id;
        test.startTest();
        MX_SB_MLT_Vida_Controller.searchSinVida(siniIdTest);
        test.stopTest();
        system.assert(true, 'Successs');
    }
    /*
    * @description method que prueba MX_SB_MLT_SiniestroVida_Controller changeStep
    * @param  void
    * @return String 
    */
    @isTest
    static void changeStepTest() {
        Final String siniId =[Select Id,MX_SB_MLT_JourneySin__c FROM Siniestro__c where MX_SB_MLT_Subramo__c='Ahorro' limit 1].Id;
        test.startTest();
        	MX_SB_MLT_Vida_Controller.changeStep(CANCELAR, '', siniId);
            MX_SB_MLT_Vida_Controller.changeStep(CREAR, '', siniId);
            MX_SB_MLT_Vida_Controller.changeStep(VOLVER, '', siniId);
        test.stopTest();
        System.assert(true, 'Cambio de Etapa con exito!');
    }
    /*
    * @description method que prueba MX_SB_MLT_SiniestroVida_Controller insertFallecido
    * @param  void
    * @return String 
    */
    @isTest
    static void insertFallTest() {
        final Siniestro__c siniFallecido = [SELECT Id, MX_SB_MLT_NameDefunct__c, MX_SB_MLT_NamePDefunct__c, MX_SB_MLT_NameMDefunct__c, MX_SB_MLT_DateDifunct__c,
                                     MX_SB_MLT_Subtipo__c, MX_SB_MLT_Relationship__c, MX_SB_MLT_CorreoElectronico__c 
                                     FROM Siniestro__c where MX_SB_MLT_SubRamo__c = 'Ahorro' limit 1];
        siniFallecido.MX_SB_MLT_NameDefunct__c = 'ARMANDO';
        siniFallecido.MX_SB_MLT_NamePDefunct__c = 'TELLEZ';
        siniFallecido.MX_SB_MLT_NameMDefunct__c = 'ALCANTARA';
        siniFallecido.MX_SB_MLT_DateDifunct__c = datetime.now();
        siniFallecido.MX_SB_MLT_Subtipo__c = 'Infarto';
        siniFallecido.MX_SB_MLT_Relationship__c = 'Otro';
        siniFallecido.MX_SB_MLT_CorreoElectronico__c = 'juanalberto.tellez@test.com';
        test.startTest();
        	MX_SB_MLT_Vida_Controller.insertFallecido(siniFallecido);
        test.stopTest();
        system.assert(true, 'Exito al insertar el registro');
    }
    /*
    * @description method que prueba MX_SB_MLT_SiniestroVida_Controller sinAction
    * @param  void
    * @return String 
    */
    @isTest
    static void siniActionTest() {
        Final String recordTId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoVida).getRecordTypeId();
        final String idRegister = [SELECT Id FROM Siniestro__c where RecordTypeId =: recordTId 
                                 AND MX_SB_MLT_SubRamo__c = 'Ahorro' limit 1].Id;
        test.startTest();
            MX_SB_MLT_Vida_Controller.sinAction(CANCELAR, idRegister);
            MX_SB_MLT_Vida_Controller.sinAction(VOLVER, idRegister);
        test.stopTest();
        System.assert(true, 'Acción con exito!');
    }
    /*
    * @description method que prueba MX_SB_MLT_SiniestroVida_Controller sendService
    * @param  void
    */
    @isTest
    static void sendServicesTest() {
        Final String recTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_RamoVida').getRecordTypeId();
        Final String siniestroTest = [SELECT Id, RecordTypeId, MX_SB_MLT_NombreConductor__c FROM Siniestro__c
                                      WHERE RecordTypeId =: recTypeId
                                      AND MX_SB_MLT_NombreConductor__c = 'JUAN ALBERTO'].Id;
        test.startTest();
        	MX_SB_MLT_Vida_Controller.sendService(siniestroTest);
        test.stopTest();
        System.assert(true,'Se ha creado el servicio con exito');
        
    }
    /*
    * @description method que prueba MX_SB_MLT_SiniestroVida_Controller addressValid
    * @param  void
    */
    @isTest
    static void addressValidTest() {
        Final String rtId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_RamoVida').getRecordTypeId();
        Final Siniestro__c sinTest = [SELECT Id, MX_SB_MLT_Address__c, MX_SB_MLT_DireccionDestino__c FROM Siniestro__c
                                      WHERE RecordTypeId =: rtId
                                      AND MX_SB_MLT_NombreConductor__c = 'JUAN ALBERTO'];
        sinTest.MX_SB_MLT_Address__c = 'Henequén 58, INFONAVIT Iztacalco, Iztacalco, 08900 Ciudad de México, CDMX, México';
        sinTest.MX_SB_MLT_DireccionDestino__c = 'Nísperos 98, Ojo de Agua, 55770 Ojo de Agua, Méx., México';
        update sinTest;
        Final String sinId = sinTest.id;
        test.startTest();
        	MX_SB_MLT_Vida_Controller.addressValid(sinId);
        test.stopTest();
        System.assert(true,'Se han validado las direcciones');
    }
    
}