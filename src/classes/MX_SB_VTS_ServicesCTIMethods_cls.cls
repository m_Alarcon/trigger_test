/**
 * @File Name          : MX_SB_VTS_ServicesCTIMethods_cls.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 21/4/2020 23:13:01
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    17/4/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
public without sharing class MX_SB_VTS_ServicesCTIMethods_cls {
    /**Variable de status peticiones smart */
    final static List<String> LEADSTATUS = new List<String>{'200','201','203','204','205','206','207','208','226'};
        /**Constructor */
    private MX_SB_VTS_ServicesCTIMethods_cls() {}//NOSONAR

    /**
    * @description Genera mapa de familia proveedores por producto
    * @author Eduardo Hernandez Cuamatzi | 21/4/2020 
    * @param familiaProds lista de familias de productos
    * @return Map<Id, List<MX_SB_VTS_FamliaProveedores__c>> 
    **/
    public static Map<Id, List<MX_SB_VTS_FamliaProveedores__c>> mapvalsFamPro(Set<Id> familiaProds) {
        final Map<Id, List<MX_SB_VTS_FamliaProveedores__c>> valsFamPro = new Map<Id, List<MX_SB_VTS_FamliaProveedores__c>>();
        for(MX_SB_VTS_FamliaProveedores__c famProvee : [Select Id, Name, MX_SB_VTS_ProveedorCTI__c, MX_SB_VTS_Familia_de_productos__c, 
                                                        MX_SB_VTS_Familia_de_productos__r.Name, MX_SB_VTS_ProveedorCTI__r.MX_SB_VTS_Identificador_Proveedor__c from MX_SB_VTS_FamliaProveedores__c 
                                                        where MX_SB_VTS_Familia_de_productos__c IN : familiaProds AND MX_SB_VTS_ProveedorCTI__r.MX_SB_VTS_IsReadyCTI__c = true]) {
            if(valsFamPro.containsKey(famProvee.MX_SB_VTS_Familia_de_productos__c)) {
                valsFamPro.get(famProvee.MX_SB_VTS_Familia_de_productos__c).add(famProvee);
            } else {
                final List<MX_SB_VTS_FamliaProveedores__c> lstFam = new List<MX_SB_VTS_FamliaProveedores__c>{famProvee};
                valsFamPro.put(famProvee.MX_SB_VTS_Familia_de_productos__c, lstFam);
            }
        }
        return valsFamPro;
    }

    /**
    * @description llena wrapper para envios al call center
    * @author Eduardo Hernandez Cuamatzi | 21/4/2020 
    * @param newEntry lista de registros
    * @param leadProducts lista de productos
    * @param valsFamPro lista de familia proveedores
    * @param valsFam lista de familia de productos
    * @return List<MX_SB_VTS_LeadMultiCTI_Util.WrapperEnvioCTI> 
    **/
    public static List<MX_SB_VTS_LeadMultiCTI_Util.WrapperEnvioCTI> fillItemRequest(Map<Id, Lead> newEntry, Map<Id, String> leadProducts, Map<Id, List<MX_SB_VTS_FamliaProveedores__c>> valsFamPro, Map<String, Product2> valsFam) {
        final List<MX_SB_VTS_LeadMultiCTI_Util.WrapperEnvioCTI> lstEnvioCTI = new List<MX_SB_VTS_LeadMultiCTI_Util.WrapperEnvioCTI>();
        for(Id leadId : leadProducts.keySet()) {
            final MX_SB_VTS_LeadMultiCTI_Util.WrapperEnvioCTI itemRequest = new MX_SB_VTS_LeadMultiCTI_Util.WrapperEnvioCTI();
            final Lead leadRecord = newEntry.get(leadId);
            itemRequest.leadId = leadRecord;
            itemRequest.product = leadProducts.get(leadId);
            itemRequest.sNombre = MX_SB_VTS_LeadMultiCTI_Util.fillFullName(leadRecord);
            if(leadRecord.MobilePhone.startsWith(Label.wsLadaCDMX)) {
                itemRequest.sTelefono = Label.wsLadaNacCel + leadRecord.MobilePhone;
            } else {
                itemRequest.sTelefono = Label.wsLadaProCel + leadRecord.MobilePhone;
            }
            itemRequest.typeSend = 'Lead';
            itemRequest.itipoVal = 0;
            itemRequest.proveedor = MX_SB_VTS_LeadMultiCTI_Util.getRandomProvider(valsFamPro, valsFam, leadProducts, leadId);
            lstEnvioCTI.add(itemRequest);
        }
        return lstEnvioCTI;
    }

    /**
    * @description procesar envios a smart center
    * @author Eduardo Hernandez Cuamatzi | 21/4/2020 
    * @param lstLeadIds lista de Ids enviados
    * @param httpRest respuesta del servicio smart center
    * @param mapTrays mapa de bandejas de atención
    * @return void 
    **/
    public static void processSmart(List<Id> lstLeadIds, HttpResponse httpRest, Map<Id,String> mapTrays) {
        final Integer statusCode = httpRest.getStatusCode();
        Integer codeResp = -1;
        final List<Lead> lstLead = new List<Lead>();
        if(LEADSTATUS.contains(String.valueOf(statusCode))) {
            Map<String,Object> retBody = new Map<String,Object>();
            retBody = (Map<String,Object>) JSON.deserializeUntyped(httpRest.getBody());
            codeResp = (Integer)retBody.get('CARGA');
        }
        for (Id leadId : lstLeadIds) {
            Boolean sendOk = true;
            if (codeResp < 0) {
                sendOk = false;
            }
            final Lead leadupdate = new Lead(Id = leadId, EnviarCTI__c = sendOk, xmlRespuesta__c = String.valueOf(codeResp), MX_SB_VTS_TrayAttention__c = mapTrays.get(leadId));
            lstLead.add(leadupdate);
        }
        update lstLead;
    }

    /**
     * sendSmart petición post al servicio de Smart
     * @param  Id función ejecutar, mapa de valores, lista Id leads
     */
    public static void sendSmartCall(String methodId, Map<String,String> mapsent, List<Id> lstLeadIds, Map<Id,String> mapTrays) {
        final HttpResponse HttpRest = MX_SB_VTS_SendLead_helper_cls.invoke(methodId, mapsent);
        MX_SB_VTS_ServicesCTIMethods_cls.processSmart(lstLeadIds, HttpRest, mapTrays);
    }
}