/**
*
* test para  clase de MX_SB_COB_CreaPagosEspejoSch_cls
*/
@isTest
public class MX_SB_COB_CreaPagosEspejoSch_tst {
    @Testsetup
    static void setObjetos() {
        final Bloque__c bloque = new Bloque__c();
        insert bloque;
        final CargaArchivo__c archivo = new CargaArchivo__c(MX_SB_COB_Bloque__c=bloque.name,MX_SB_COB_ContadorConError__c=0,MX_SB_COB_ContadorSinError__c=0,MX_SB_COB_resultado__c='');
        insert archivo;
        final pagosespejo__c pago = new pagosespejo__c(MX_SB_COB_BloqueLupa__c =bloque.id,MX_SB_COB_factura__c='1233',MX_SB_COB_InsertCreaPago__c=false);
        insert pago;
    }
    
	@isTest
    static void scheduleTest() {
        final List<PagosEspejo__c> pagos = [select id,MX_SB_COB_BloqueLupa__c,MX_SB_COB_factura__c,MX_SB_COB_InsertCreaPago__c from pagosespejo__c limit 20];
        Test.StartTest();
        final MX_SB_COB_CreaPagosEspejoSch_cls sh1 = new MX_SB_COB_CreaPagosEspejoSch_cls();
        sh1.lstPagoEspejo = pagos;
        String sch = '0 0 23 * * ?';
        final String jobId = system.schedule('Test Territory Check', sch, sh1);
        System.assertNotEquals(null, jobId, 'no se ejecuto el chron job');
        Test.stopTest();
    }
}