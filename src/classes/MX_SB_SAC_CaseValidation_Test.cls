/**
 * @File Name          : MX_SB_SAC_CaseValidation_Test.cls
 * @Description        : Provides coverage to class MX_SB_SAC_CaseValidation
 * @Author             : Jaime Terrats
 * @Group              : BBVA
 * @Last Modified By   : Jaime Terrats
 * @Last Modified On   : 07-20-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    10/31/2019       Jaime Terrats           Initial Version
 * 1.1    12/13/2019       Jaime Terrats           Update test class
 * 1.2    03/02/2020       Michelle Valderrama     Update test class for new LBBVA Record Type
**/
@isTest
public without sharing class MX_SB_SAC_CaseValidation_Test {
    /** name variable for admin user and cases */
    final static String NAME = 'testSacValidate';
    /** variable for case description */
    final static String ASESOR_SAC = 'asesor sac';
    /** variable for case description */
    final static String LOREM_IPSUM = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum';
    /** value Servicios for picklist reason */
    final static String SERVICIOS = 'Servicios';
    /** value Servicios for picklist reason */
    final static String DETALLE = 'Duplicado de póliza';
    /** error message variable */
    final static String ASSERT_MSG = 'Error esperado.';
    /** tipification value */
    final static String CANCELACION = 'Cancelación';
    /** tipification value */
    final static String RETENCION = 'Retención';
    /** product type value */
    final static String AUTO = 'Auto';
    /** value after case update */
    final static String SUB_DETAIL = 'Venta de unidad';
    /**
    * @description makeData generates mock data
    * @author Jaime Terrats | 10/31/2019
    * @return void
    **/
    @TestSetup
    public static void makeData() {
        final User usr = MX_WB_TestData_cls.crearUsuario(NAME, System.Label.MX_SB_VTS_ProfileAdmin);
        System.runAs(usr) {
            final User sacUsr = MX_WB_TestData_cls.crearUsuario(ASESOR_SAC, System.Label.MX_SB_SAC_AsesorSACProfile);
            insert sacUsr;
            final List<PermissionSet> sacPermSets = [Select Id from PermissionSet where Name in ('MX_SB_SAC_AsesorSAC', 'MX_SB_SAC_accesoCheckListConjuntoPermiso', 'Marketing Cloud Connector', 'MX_SAC_FichaCasos')];
            final List<PermissionSetAssignment> assignPermSets = new List<PermissionSetAssignment>();
            for(PermissionSet prmSet : sacPermSets) {
                final PermissionSetAssignment permSetAssmnt = new PermissionSetAssignment();
                permSetAssmnt.PermissionSetId = prmSet.Id;
                permSetAssmnt.AssigneeId = sacUsr.Id;
                assignPermSets.add(permSetAssmnt);
            }
            insert assignPermSets;
            final List<Case> casesToInsert =  new List<Case>();
            final String[] recordTypes = new String[] {System.Label.MX_SB_SAC_Generico, System.Label.MX_SB_SAC_RT, System.Label.MX_SB_SAC_RTAccidentesPersonales,
                                                        System.Label.MX_SB_SAC_RTDanos, System.Label.MX_SB_SAC_RTSalud, System.Label.MX_SB_SAC_RTVida, System.Label.MX_SB_SAC_RTLBBVAAuto, System.Label.MX_SB_SAC_RTLBBVAVida};
            final List<String> rTypes = new List<String>();
            for(Integer i = 0; i < recordTypes.size(); i++) {
                final String rtId = Schema.SObjectType.case.getRecordTypeInfosByDeveloperName().get(recordTypes[i]).getRecordTypeId();
                rTypes.add(rtId);
            }
            for(Integer i = 0; i < 7; i++) {
                final Case nCase = new Case();
                nCase.Subject = NAME + i;
                nCase.RecordTypeId = rTypes[i];
                nCase.MX_SB_SAC_Nombre__c = NAME;
                nCase.MX_SB_SAC_ApellidoPaternoCliente__c = NAME;
                nCase.MX_SB_SAC_ApellidoMaternoCliente__c = NAME;
                nCase.MX_SB_SAC_EsCliente__c = true;
                nCase.Origin = 'Teléfono';
                nCase.MX_SB_SAC_SubOrigenCaso__c = 'Inbound';
                nCase.MX_SB_SAC_TipoContacto__c = 'Asegurado';
                casesToInsert.add(nCase);
            }

            insert casesToInsert;
        }
    }
    /**
    * @description provides coverage of function tetValidateChange()
    * @author Jaime Terrats | 10/31/2019
    * @return void
    **/
    @isTest
    public static void testValidateChange() {
        Test.startTest();
        final Case uCase = [Select Id, Reason, MX_SB_SAC_FinalizaFlujo__c, MX_SB_SAC_Detalle__c, MX_SB_SAC_FlujoCotizacion__c, Description, OwnerId from Case where Subject =: Name+1];
        final User sacUser = [Select Id from User where LastName =: ASESOR_SAC];
        uCase.OwnerId = sacUser.Id;
        uCase.MX_SB_SAC_FinalizaFlujo__c = true;
        uCase.Description = LOREM_IPSUM;
        uCase.Reason = 'Consultas';
        uCase.MX_SB_SAC_FlujoCotizacion__c = false;
        uCase.MX_SB_SAC_Detalle__c = '';
        update uCase;

        System.runAs(sacUser) {
            try {
                uCase.Reason = SERVICIOS;
                update uCase;
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(System.Label.MX_SB_SAC_ErrorEdicion), ASSERT_MSG);
            }
        }
        Test.stopTest();
    }
    /**
    * @description provides coverage for reason field
    * @author Jaime Terrats | 10/31/2019
    * @return void
    **/
    @isTest
    public static void testValidateChange2() {
        Test.startTest();
        final Case uCase = [Select Id, Description, MX_SB_SAC_FinalizaFlujo__c, MX_SB_SAC_Detalle__c, Reason, OwnerId from Case where Subject =: Name+2];
        final User sacUser = [Select Id from User where LastName =: ASESOR_SAC];
        uCase.OwnerId = sacUser.Id;
        uCase.MX_SB_SAC_FinalizaFlujo__c = true;
        uCase.Reason = 'Consultas';
        uCase.MX_SB_SAC_Detalle__c = 'Información cotizador';
        update uCase;
        System.runAs(sacUser) {
            try {
                uCase.Description = LOREM_IPSUM;
                update uCase;
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(System.Label.MX_SB_SAC_PreventComments), ASSERT_MSG);
            }
        }
        Test.stopTest();
    }

    /**
    * @description will validate if user shouldnt add case description
    * @author Jaime Terrats | 07-10-2020
    **/
    @isTest
    public static void testValidateChange3() {
        Test.startTest();
        final Case uCase = [Select Id, Reason, Description, OwnerId, MX_SB_SAC_FinalizaFlujo__c, MX_SB_SAC_Ramo__c, MX_SB_SAC_Aplicar_Mala_Venta__c, MX_SB_SAC_Detalle__c from Case where Subject =: Name+3];
        final User sacUser = [Select Id from User where LastName =: ASESOR_SAC];
        uCase.Reason = CANCELACION;
        uCase.MX_SB_SAC_Ramo__c = System.Label.MX_SB_VTS_Vida;
        uCase.MX_SB_SAC_FinalizaFlujo__c = true;
        uCase.OwnerId = sacUser.Id;
        update uCase;
        System.runAs(sacUser) {
            try {
                uCase.Description = LOREM_IPSUM;
                update uCase;
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(System.Label.MX_SB_SAC_PreventComments), ASSERT_MSG);
            }
        }
        Test.stopTest();
    }

    /**
    * @description
    * @author Jaime Terrats | 07-10-2020
    **/
    @isTest
    public static void testValidateChange4() {
        Test.startTest();
        final Case uCase = [Select Id, Reason, Description, OwnerId, MX_SB_SAC_FinalizaFlujo__c, MX_SB_SAC_Ramo__c, MX_SB_SAC_Aplicar_Mala_Venta__c, MX_SB_SAC_Detalle__c,
                            MX_SB_SAC_EsMalaVenta__c from Case where Subject =: Name+4];
        final User sacUser = [Select Id from User where LastName =: ASESOR_SAC];
        uCase.Reason = CANCELACION;
        uCase.MX_SB_SAC_Ramo__c = AUTO;
        uCase.MX_SB_SAC_FinalizaFlujo__c = true;
        uCase.MX_SB_SAC_Aplicar_Mala_Venta__c = 'Si';
        uCase.MX_SB_SAC_EsMalaVenta__c = true;
        uCase.MX_SB_SAC_TerminaMalasVentas__c = true;
        uCase.OwnerId = sacUser.Id;
        update uCase;
        System.runAs(sacUser) {
            try {
                uCase.MX_SB_SAC_Detalle__c = 'Malas Ventas';
                uCase.Description = '';
                update uCase;
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(System.Label.MX_SB_SAC_RequestComments), ASSERT_MSG);
            }
        }
        Test.stopTest();
    }

    /**
    * @description
    * @author Jaime Terrats | 07-10-2020
    **/
    @isTest
    public static void testValidateChange5() {
        Test.startTest();
        final Case uCase = [Select Id, Reason, Description, OwnerId, MX_SB_SAC_FinalizaFlujo__c, MX_SB_SAC_Ramo__c, MX_SB_1500_is1500__c, MX_SB_SAC_Detalle__c from Case where Subject =: Name+5];
        final User sacUser = [Select Id from User where LastName =: ASESOR_SAC];
        uCase.Reason = RETENCION;
        uCase.MX_SB_SAC_Ramo__c = AUTO;
        uCase.MX_SB_SAC_FinalizaFlujo__c = false;
        uCase.MX_SB_1500_is1500__c = true;
        uCase.MX_SB_SAC_Detalle__c = 'Malas Ventas';
        uCase.OwnerId = sacUser.Id;
        update uCase;
        System.runAs(sacUser) {
            try {
                uCase.Description = LOREM_IPSUM;
                update uCase;
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(System.Label.MX_SB_SAC_PreventCmnts), ASSERT_MSG);
            }
        }
        Test.stopTest();
    }

    /**
    * @description
    * @author Jaime Terrats | 07-10-2020
    **/
    @isTest
    public static void testValidateChange6() {
        Test.startTest();
        final Case uCase = [Select Id, Reason, Description, OwnerId, MX_SB_SAC_OcultarArgumentos__c, MX_SB_SAC_Finalizar_Retencion__c,
                            MX_SB_SAC_FinalizaFlujo__c, MX_SB_SAC_Ramo__c, MX_SB_SAC_Aplicar_Mala_Venta__c from Case where Subject =: Name+6];
        final User sacUser = [Select Id from User where LastName =: ASESOR_SAC];
        uCase.Reason = CANCELACION;
        uCase.MX_SB_SAC_Ramo__c = AUTO;
        uCase.MX_SB_SAC_FinalizaFlujo__c = true;
        uCase.MX_SB_SAC_Aplicar_Mala_Venta__c = 'No';
        uCase.MX_SB_SAC_Finalizar_Retencion__c = true;
        uCase.MX_SB_SAC_OcultarArgumentos__c = true;
        uCase.OwnerId = sacUser.Id;
        update uCase;
        System.runAs(sacUser) {
            try {
                uCase.Description = '';
                update uCase;
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(System.Label.MX_SB_SAC_RequestComments), ASSERT_MSG);
            }
        }
        Test.stopTest();
    }

    /**
    * @description
    * @author Jaime Terrats | 07-10-2020
    **/
    @isTest
    public static void testValidateChange7() {
        Test.startTest();
        final Case uCase = [Select Id, Reason, Description, OwnerId, MX_SB_SAC_FinalizaFlujo__c, MX_SB_SAC_Ramo__c,
                            MX_SB_SAC_TerminaMalasVentas__c, MX_SB_SAC_Detalle__c, MX_SB_SAC_OcultarArgumentos__c,
                            MX_SB_SAC_Informacion_Adicional_Vida__c from Case where Subject =: Name+0];
        final User sacUser = [Select Id from User where LastName =: ASESOR_SAC];
        uCase.Reason = RETENCION;
        uCase.MX_SB_SAC_Ramo__c = System.Label.MX_SB_VTS_Vida;
        uCase.MX_SB_SAC_FinalizaFlujo__c = true;
        uCase.MX_SB_SAC_TerminaMalasVentas__c = true;
        uCase.MX_SB_SAC_OcultarArgumentos__c = true;
        uCase.OwnerId = sacUser.Id;
        update uCase;
        System.runAs(sacUser) {
            try {
                uCase.MX_SB_SAC_Informacion_Adicional_Vida__c = 'Mala venta con siniestro';
                uCase.Description = LOREM_IPSUM;
                update uCase;
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(System.Label.MX_SB_SAC_OnlyNA), ASSERT_MSG);
            }
        }
        Test.stopTest();
    }

    /**
    * @description
    * @author Jaime Terrats | 07-10-2020
    **/
    @isTest
    public static void testValidateChange8() {
        Test.startTest();
        final Case uCase = [Select Id, Reason, Description, OwnerId from Case where Subject =: Name+1];
        final User sacUser = [Select Id from User where LastName =: ASESOR_SAC];
        uCase.Reason = CANCELACION;
        uCase.OwnerId = sacUser.Id;
        update uCase;
        System.runAs(sacUser) {
            try {
                uCase.Description = LOREM_IPSUM;
                update uCase;
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(System.Label.MX_SB_SAC_PreventCmnts), ASSERT_MSG);
            }
        }
        Test.stopTest();
    }

    /**
    * @description
    * @author Jaime Terrats | 07-10-2020
    **/
    @isTest
    public static void testValidateChange9() {
        Test.startTest();
        final Case uCase = [Select Id, Reason, OwnerId, MX_SB_SAC_Ramo__c, MX_SB_SAC_Detalle__c, MX_SB_SAC_Aplicar_Mala_Venta__c,
                            RecordTypeId, MX_SB_SAC_FinalizaFlujo__c, MX_SB_SAC_InformacionAdicional__c from Case where Subject =: Name+2];
        final User sacUser = [Select Id from User where LastName =: ASESOR_SAC];
        final Id rtId  = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_SAC_RT).getRecordTypeId();
        uCase.Reason = CANCELACION;
        uCase.MX_SB_SAC_Ramo__c = AUTO;
        uCase.OwnerId = sacUser.Id;
        uCase.MX_SB_SAC_FinalizaFlujo__c = true;
        uCase.RecordTypeId = rtId;
        uCase.MX_SB_SAC_Aplicar_Mala_Venta__c = 'No';
        update uCase;
        System.runAs(sacUser) {
            uCase.MX_SB_SAC_Detalle__c = 'Venta';
            uCase.MX_SB_SAC_InformacionAdicional__c = 'Devolución a otra cuenta';
            update uCase;
            final Case tCase = [Select MX_SB_SAC_Subdetalle__c from Case where Subject =: Name+2];
            System.assert(SUB_DETAIL.contains(tCase.MX_SB_SAC_Subdetalle__c), ASSERT_MSG);
        }
        Test.stopTest();
    }
}