/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 02-03-2021
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   02-03-2021   Eduardo Hernández Cuamatzi   Initial Version
**/
@SuppressWarnings('sf:UseSingleton, sf:DMLWithoutSharingEnabled')
public class MX_RTL_IasoServicesInvoke_Selector {


    /**
    * @description Invoca servicios aso
    * @author Eduardo Hernández Cuamatzi | 02-03-2021 
    * @param servicesName DeveloperName del registro de servicio Iaso
    * @param paramsSrv Parametros de Mapa<String,Object> con las propiedades y valores
    * @return HttpResponse Respuesta del servicio
    **/
    public static HttpResponse callServices(String servicesName, Map<String, Object> paramsSrv) {
        HttpResponse responseSrv = new HttpResponse();
        responseSrv = iaso.GBL_Integration_GenericService.invoke(servicesName, paramsSrv);
        return responseSrv;
    }


    /**
    * @description Invoca servicios aso
    * @author Eduardo Hernández Cuamatzi | 02-03-2021 
    * @param servicesName DeveloperName del registro de servicio Iaso
    * @param paramsSrv Parametros de Mapa<String,Object> con las propiedades y valores
    * @param bodyRequest Request que reemplaza parametros en el servicio
    * @return HttpResponse Respuesta del servicio
    **/
    public static HttpResponse callServices(String servicesName, Map<String, Object> paramsSrv, HttpRequest bodyRequest) {
        HttpResponse responseSrv = new HttpResponse();
        responseSrv = iaso.GBL_Integration_GenericService.invoke(servicesName, paramsSrv, bodyRequest);
        return responseSrv;
    }

    /**
    * @description Invoca servicios aso
    * @author Eduardo Hernández Cuamatzi | 02-03-2021 
    * @param servicesName DeveloperName del registro de servicio Iaso
    * @param paramsSrv Parametros de Json objetos con las propiedades y valores
    * @return HttpResponse Respuesta del servicio
    **/
    public static HttpResponse callServices(String servicesName, String paramsSrv) {
        HttpResponse responseSrv = new HttpResponse();
        responseSrv = iaso.GBL_Integration_GenericService.invoke(servicesName, paramsSrv);
        return responseSrv;
    }
}