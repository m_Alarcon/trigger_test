/**---------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_PurecloudController_cls
* Autor: Marco Antonio Cruz Barboza
* Proyecto: Siniestros - BBVA
* Descripción : Clase controladora de Visualforce y Componente Aura
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                       Desripción
* --------------------------------------------------------------------------------
* 1.0           20/11/2019     Marco Antonio Cruz Barboza   Creación
* 1.1		15/01/2020	   Daniel Perez Lopez			Migración a Task
* 1.2           05/05/2020     Juan Carlos Benitez			Tipificacion de llamada
* 1.3		07/05/2020	Marco Antonio Cruz Barboza	Se agrega procedimiento de cambio de status
* 1.4		26/05/2020	Marco Antonio Cruz Barboza	Se anexa parametro a URL de VisualForce
* --------------------------------------------------------------------------------
**/
public with sharing class MX_SB_MLT_PurecloudController_cls {
    /*
    * String tipo de llamada no relacionada
    */
    public static final String NORELATED ='Llamada no relacionada';
    /*
    * String tipo de llamada relacionada
    */
    public static final String RELATED ='Llamada relacionada';
    /*Teléfono del Caso*/
    public String casePhone {get;set;}
    /*Identificador del Caso*/
    public String caseId {get;set;}
    /*Label de la llamada*/
    public String caseLabel {get;set;}
    /**
	* Get de valor de teléfono de la página
	*/
    
    @TestVisible public MX_SB_MLT_PurecloudController_cls (ApexPages.StandardController stdController) {
        stdController.view();
        casePhone=ApexPages.currentPage().getParameters().get('Phone');
        caseLabel=ApexPages.currentPage().getParameters().get('Origen');
    }
    /**
	* Creación de Caso con Phone
	*/
    public static Id getIdCase(Id idRecType, String strCasePhone) {
        final Case newCase = new Case(RecordTypeId  = idRecType, Status = 'Nuevo', Origin = 'Purecloud', SuppliedPhone = strCasePhone );       
        insert newCase;
        return newCase.Id;	
    } 
    
    /**
	* Creación de Task con Phone
	*/
    public static Id getIdTask(Id idRecType, String strCasePhone, String strTaskLabel) {
        final Task inserTask = new Task(Telefono__c=strCasePhone,RecordTypeId=idRecType,Subject='Llamada PureCloud',MX_SB_VTS_LookActivity__c=strTaskLabel);
        insert inserTask;
        return inserTask.Id;	
    }
    /**
    * Actualización de status
    */
    @AuraEnabled
    public static void changeStatus(String recordId) {
        Final String recordTPId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(label.MX_SB_MLT_TomaReporte).getRecordTypeId();
        Final Case changeSini = [SELECT Id, Status FROM Case WHERE Id =:recordId AND RecordTypeId =:recordTPId ];
        changeSini.Status = 'Cerrado';
        UPDATE changeSini;
    }
    /**
	* Obtiene estado y resultado de la llamada
	*/
    @AuraEnabled
    public static Task getInfo (String recId) {
        return [SELECT Id, Status, Resultado_llamada__c from Task where id =:recId];
    }
        /**
	* Tipificacion de la llamada(relacionada o no relacionada)
	*/
    @AuraEnabled
    public static void updTask(Id recId, String tipo, string subtipo) {
        final Task tarea =[SELECT Id, Status, Resultado_llamada__c from Task where id =:recId];
        if(tipo==NORELATED) {
        tarea.Status=tipo;
        tarea.Resultado_llamada__c=subtipo;
        }
        if(tipo==RELATED) {
            tarea.Status=tipo;
        	tarea.Resultado_llamada__c=subtipo;
        }
        update tarea;
    } 
    
    /**
	* Crea el caso con el teléfono enviado como parámetro
	*/
    @TestVisible public pageReference createCase () {
        Id idTask = [SELECT Id FROM RecordType WHERE SobjectType = 'Task' AND DeveloperName = 'MX_SB_MLT_PureCloud' Limit 1].Id;
        idTask = getIdTask(idTask,  casePhone.escapeHtml4(), caseLabel);
        final PageReference retURL = new PageReference('/'+idTask);
        retURL.setRedirect(true);
        return retURL;
    } 
    
    /**
	* Retorna SuppliedPhone del Caso creado
	*/
    @AuraEnabled
    public static string getValue(String recId) {
        final Id sobjectId =recId;
        String getStr='';
        String value;
        String varpure ='';
        varpure ='Purecloud';
        if(sobjectId.getSobjectType()==Schema.Task.SObjectType && getStr=='' && varpure=='Purecloud') {
            value = [SELECT Telefono__c FROM Task WHERE Id =:recId LIMIT 1].Telefono__c;
        } else {
            getStr = [SELECT Origin FROM Case WHERE Id =:recId LIMIT 1].Origin;
            If(getStr == varpure) { 
                value = [SELECT SuppliedPhone FROM Case WHERE Id =:recId LIMIT 1].SuppliedPhone;
            } else {
                value = [SELECT MX_SB_MLT_Phone__c FROM Case WHERE Id =:recId LIMIT 1].MX_SB_MLT_Phone__c;
            } 
        } 
        return value;
    }  
    
}