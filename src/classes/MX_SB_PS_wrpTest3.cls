/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpTest3
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
@isTest
public class MX_SB_PS_wrpTest3 {
     /*final string message properties*/
	Final static string LBUILDC = 'Build constructor';

	 /*Methodo MX_SB_PS_wrpCostumerDocumentResp */
	public  static testMethod void  init7() {
		final MX_SB_PS_wrpCostumerDocumentResp wrpDocRes  = new MX_SB_PS_wrpCostumerDocumentResp();
		System.assertEquals(wrpDocRes, wrpDocRes,LBUILDC);
	}
	/*Methodo MX_SB_PS_wrpCotizadorVida */
	public  static testMethod void  init8() {
		final MX_SB_PS_wrpCotizadorVida cotiVida = new MX_SB_PS_wrpCotizadorVida();
		System.assertEquals(cotiVida, cotiVida,LBUILDC);
	 }
	 /*Methodo MX_SB_PS_wrpCotizadorVidaResp */
	public  static testMethod void  init9() {
		final MX_SB_PS_wrpCotizadorVidaResp cotiVidaResp =  new MX_SB_PS_wrpCotizadorVidaResp();
		System.assertEquals(cotiVidaResp, cotiVidaResp,LBUILDC);
	 }
	 /*Methodo MX_SB_PS_wrpDatosParticulares */
	public  static testMethod void  init10() {
		final MX_SB_PS_wrpDatosParticulares wDatPar  = new MX_SB_PS_wrpDatosParticulares();
		System.assertEquals(wDatPar, wDatPar,LBUILDC);
	 }
	 /*Methodo MX_SB_PS_wrpDatosParticularesResp */
	public  static testMethod void  init11() {
		final MX_SB_PS_wrpDatosParticularesResp wDatParRes = new MX_SB_PS_wrpDatosParticularesResp();
		System.assertEquals(wDatParRes, wDatParRes,LBUILDC);
	}
	 /*Methodo MX_SB_PS_wrpEstadosResp */
	public  static testMethod void  init12() {
		final MX_SB_PS_wrpEstadosResp wrpEstadosResp  = new MX_SB_PS_wrpEstadosResp();
		System.assertEquals(wrpEstadosResp, wrpEstadosResp,LBUILDC);
	}

}