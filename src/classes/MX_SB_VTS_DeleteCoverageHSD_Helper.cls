/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 01-04-2021
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   01-04-2021   Eduardo Hernández Cuamatzi   Initial Version
 * 1.1   02-25-2021   Eduardo Hernández Cuamatzi   Se agrega guardado de mensualidades
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_DeleteCoverageHSD_Helper {

    /**Campos base de coberturas */
    final static String LSTFIELDSCOBER = 'MX_SB_VTS_TradeValue__c, MX_SB_VTS_GoodTypeCode__c, MX_SB_VTS_RelatedQuote__c, MX_SB_VTS_CoveragePercentage__c, MX_SB_VTS_CoverageCode__c, MX_SB_VTS_ContractCriterial__c,'+
    'MX_SB_VTS_CodeTrade__c, MX_SB_VTS_CategoryCode__c, MX_SB_MLT_SumaAsegurada__c, MX_SB_MLT_IdeCobertura__c, MX_SB_MLT_Estatus__c, MX_SB_MLT_Descripcion__c, MX_SB_MLT_Cobertura__c, Id';

    /**Variable de Id para mapas*/
    final static String STRID = 'id';

    /**Recupera las coberturas por codigo ligadas a un registro de cotización */
    public static List<Cobertura__c> findCoverCodes(Id quoteId, List<String> coverages) {
        final Set<Id> quotesId = new Set<Id>{quoteId};
        return MX_RTL_Cobertura_Selector.findCoverByCode(LSTFIELDSCOBER, '', quotesId, coverages);
    }

    /**
    * @description Recupera datos de producto por nombre
    * @author Eduardo Hernández Cuamatzi | 12-30-2020 
    * @param productName Nombre del producto a buscar
    * @return Map<String, String> Mapa de coberturas recuperadas
    **/
    public static Map<String, String> findProdCodByName(String productName) {
        final Map<String, String> mapProductCods = new Map<String, String>();
        final Set<String> lstProductNames= new Set<String>{productName};
        final Set<String> lstProcess= new Set<String>{'VTS'};
        for(MX_SB_SAC_Catalogo_Clipert_Producto__c clipProdut : MX_RTL_Catalogo_Clipert_Product_Selector.catalogoClipName(lstProductNames, lstProcess, true)) {
            mapProductCods.put(clipProdut.MX_SB_SAC_Producto__r.Name.toUpperCase(), clipProdut.MX_SB_VTS_ProductCode__c);
        }
        return mapProductCods;
    }

    /**
    * @description LLena elemento de producto
    * @author Eduardo Hernández Cuamatzi | 12-30-2020 
    * @param productCode código de producto a agregar
    * @param masterTrade Cobertura principal
    * @return Map<String, Object> Estructura de elemento Product
    **/
    public static Map<String, Object> fillProductElement(String productCode, String masterTrade) {
        final Map<String, Object> dataProduct = new Map<String, Object>();
        dataProduct.put(STRID, productCode);
        final Map<String, Object> planProduct = new Map<String, Object>();
        planProduct.put(STRID, System.Label.MX_SB_VTS_HSD_PlanCode);
        planProduct.put('reviewCode', System.Label.MX_SB_VTS_HSD_RevCode);
        dataProduct.put('plan', planProduct);
        final Map<String, Object> tradeObject = new Map<String, Object>();
        tradeObject.put(STRID, masterTrade);
        dataProduct.put('trade', tradeObject);
        return dataProduct;
    }

    /**
    * @description Llena datos de pagos
    * @author Eduardo Hernández Cuamatzi | 12-30-2020 
    * @param mapFrecuen Lista de pagos de la respuesta
    * @return Map<String, String> 
    **/
    public static Map<String, String> fillDatPayments(List<Object> mapFrecuen) {
        final Map<String, String> dataPayments = new Map<String, String>();
        for(Object premiumPay: mapFrecuen) {
            final Map<String,Object> premiumPayMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(premiumPay));
            final String payment = (String)premiumPayMap.get(STRID);
            final List<Object> lstMapPremiums = (List<Object>)JSON.deserializeUntyped(JSON.serialize(premiumPayMap.get('premiums')));
            final String totalPremiums = fillPremiums(lstMapPremiums, 'YEARLY_TOTAL');
            final String totalFirstPay = fillPremiums(lstMapPremiums, 'FIRST_PAYMENT');
            final String paymentFrac = payment+'_Frac';
            dataPayments.put(payment, totalPremiums);
            dataPayments.put(paymentFrac, totalFirstPay);
        }
        return dataPayments;
    }

        /**
    * @description Recupera valores de pagos
    * @author Eduardo Hernández Cuamatzi | 12-30-2020 
    * @param lstMapPremiums Lista de montos
    * @return String Monto de mensualidad
    **/
    public static String fillPremiums(List<Object> lstMapPremiums, String typePayment) {
        String totalPremium = '0.00';
        for (Object premiumVal : lstMapPremiums) {
            final Map<String,Object> premiumValMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(premiumVal));
            final String dataPay = (String)premiumValMap.get(STRID);
            if(dataPay.equalsIgnoreCase(typePayment)) {
                final List<Object> totalAmounts = (List<Object>)JSON.deserializeUntyped(JSON.serialize(premiumValMap.get('amounts')));
                final Map<String,Object> totalAmountsMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(totalAmounts[0]));
                totalPremium = String.valueOf(totalAmountsMap.get('amount'));
            }
        }
        return totalPremium;
    }

    /**
    * @description Recupera el monto de un elemento de premiums
    * @author Eduardo Hernández Cuamatzi | 01-05-2021 
    * @param premiumPay datos de pagos
    * @param codePremium codigo de premium a recuperar
    * @return String Valor de prima
    **/
    public static String amountTotal (Object premiumPay, String codePremium) {
        final Map<String,Object> premiumPayMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(premiumPay));
        final List<Object> lstMapPremiums = (List<Object>)JSON.deserializeUntyped(JSON.serialize(premiumPayMap.get('premiums')));
        String totalAmoutStr = '0.00';
        for (Object premiumVal : lstMapPremiums) {
            final Map<String,Object> premiumValMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(premiumVal));
            final String dataPay = (String)premiumValMap.get(STRID);
            if(dataPay == codePremium) {
                final List<Object> totalAmounts = (List<Object>)JSON.deserializeUntyped(JSON.serialize(premiumValMap.get('amounts')));
                final Map<String,Object> totalAmountsMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(totalAmounts[0]));
                totalAmoutStr = String.valueOf(totalAmountsMap.get('amount'));
            }
        }
        return totalAmoutStr;
    }

    /**
    * @description Crea un mapa de datos particulares en base a la lista de coberturas
    * @author Eduardo Hernández Cuamatzi | 01-08-2021 
    * @param lstCovers lista de datos particulares por codigo de contracting code
    * @return Map<String, Object> Mapa de datos particulares
    **/
    public static Map<String, Object> fillMapCritical(List<Cobertura__c> lstCovers) {
        final Map<String,Object> mapCritial = new Map<String,Object>();
        for (Cobertura__c coverageRec : lstCovers) {
            mapCritial.put(coverageRec.MX_SB_MLT_Cobertura__c, coverageRec);
        }
        return mapCritial;
    }
}