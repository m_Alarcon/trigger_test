@isTest
/**
 * @File Name          : MX_RTL_Contract_Service_Test.cls
 * @Description        : Clase que pruebas los methodos de MX_RTL_Contract_Service
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 06/06/2020, 08:13:34 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/26/2020   Juan Carlos Benitez     Initial Version
 * 1.1    6/11/2020   Marco Antonio Cruz       Add methods for testing
**/
public class MX_RTL_Contract_Service_Test {
/**Var Nombre **/
    final static String NAMETEST ='Edwin';
/**variable para Apellido de Contrato **/
	final static String LNAMETEST ='Alcantara';
/**Var SAC_NO_POLIZA **/
    final static String SAC_POLIZA='44428231';
	/**Variable para RFC de Contrato**/
    final static String TESTRFC='CUBM364859PF';
	/**Variable para RFC de Contrato**/
    final static String TESTCERT='LORD3728';
	/*
	*Variable String para DML
	*/
    Final static String TESTFIELDS = 'Id,MX_SB_MLT_Certificado__c,MX_SB_SAC_FechaFinContrato__c,MX_SB_SAC_EstatusPoliza__c,MX_WB_Producto__c,createddate,accountid,MX_WB_celularAsegurado__c,MX_SB_SAC_EmailAsegurado__c,' +
        'MX_SB_SAC_RFCAsegurado__c,MX_SB_SAC_NumeroPoliza__c,MX_SB_SB_Placas__c,MX_SB_SAC_NombreClienteAseguradoText__c, ' +
        'MX_WB_apellidoMaternoAsegurado__c,MX_WB_apellidoPaternoAsegurado__c,MX_SB_SAC_Segmento__c, ' +
        'Description,StartDate,EndDate,MX_SB_SAC_TipoMoneda__c ';
    /*
	*Variable String para DML
	*/
    Final static String TESTFIELDSPD = 'Id, MX_SB_MLT_Certificado__c,MX_SB_SAC_FechaFinContrato__c,MX_SB_SAC_EstatusPoliza__c,MX_WB_Producto__r.Name, ' +
        'createddate,accountid,MX_WB_celularAsegurado__c,MX_SB_SAC_EmailAsegurado__c, ' +
        'MX_SB_SAC_RFCAsegurado__c,MX_SB_SAC_NumeroPoliza__c,MX_SB_SB_Placas__c,MX_SB_SAC_NombreClienteAseguradoText__c, ' +
        'MX_WB_apellidoMaternoAsegurado__c,MX_WB_apellidoPaternoAsegurado__c,MX_SB_SAC_Segmento__c, ' +
        'Description,StartDate,EndDate,MX_SB_SAC_TipoMoneda__c';
    
  @TestSetup
   static void testDt() {
        Final String profName = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User usrTest = MX_WB_TestData_cls.crearUsuario(NAMETEST, profName);  
	    System.runAs(usrTest) {
            final String accountRTT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_Proveedores_MA').getRecordTypeId();
			final List<Account> accountsIns = new List<Account>();
            for(Integer i=0; i<10; i++) {
                final Account nAccountTst= new Account();
                nAccountTst.RecordTypeId=accountRTT;
                nAccountTst.Name=NAMETEST+'_'+i;
                nAccountTst.Tipo_Persona__c='Física';
                accountsIns.add(nAccountTst);
            }
            Database.insert(accountsIns);
            final List<Contract> contract2Insert = new List<Contract>();
            for(Integer iter = 0; iter < 10; iter++) {
                final Contract testContract = new Contract();
                testContract.AccountId=accountsIns[0].Id;
                testContract.MX_SB_SAC_NumeroPoliza__c =SAC_POLIZA+iter;
                testContract.MX_SB_SAC_NombreClienteAseguradoText__c = NAMETEST+'_'+ iter;
                testContract.MX_WB_apellidoPaternoAsegurado__c = LNAMETEST + '_' + iter;
                testContract.MX_SB_SAC_RFCAsegurado__c = TESTRFC + iter;
                testContract.MX_SB_MLT_Certificado__c = TESTCERT + iter;
                contract2Insert.add(testContract);
            }
            Database.insert(contract2Insert);
        }
   }
	@isTest
    static void tstGetCntrIdSACnPoliza() {
        Final List<Contract> policyTest =[SELECT Id, MX_SB_SAC_NumeroPoliza__c FROM Contract WHERE MX_SB_SAC_NumeroPoliza__c  LIKE: '%' + SAC_POLIZA+ '%' ];
        test.startTest();
        	MX_RTL_Contract_Service.getContractIdBySACnPoliza(policyTest[0].MX_SB_SAC_NumeroPoliza__c);
        	system.assertEquals(policyTest.size(), 10, 'Se han encontrado polizas');
        test.stopTest();
    }
    
   	@isTest
    static void testContractIdBySACnPoliza() {
        Final List<Contract> policyTest2 =[SELECT Id, MX_SB_SAC_NumeroPoliza__c FROM Contract WHERE MX_SB_SAC_NumeroPoliza__c  LIKE: '%' + SAC_POLIZA+ '%' ];
        test.startTest();
        	MX_RTL_Contract_Service.getContractIdBySACnPoliza(policyTest2[0].MX_SB_SAC_NumeroPoliza__c, TESTFIELDS);
        	system.assertEquals(policyTest2.size(), 10, 'Se han encontrado polizas');
        test.stopTest();
    }

    @isTest
    static void testContractIdByRFC() {
        Final List<Contract> testPol =[SELECT Id, MX_SB_SAC_RFCAsegurado__c
                                          FROM Contract WHERE MX_SB_SAC_NumeroPoliza__c  LIKE: '%' + SAC_POLIZA+ '%' ];
        test.startTest();
        	MX_RTL_Contract_Service.getContractIdByRFC(testPol[0].MX_SB_SAC_RFCAsegurado__c, TESTFIELDS);
        	system.assertEquals(testPol.size(), 10, 'Pólizas encontradas');
        test.stopTest();
    }
    
    @isTest
    static void testContractIdByNombre() {
        Final List<Contract> policyTst =[SELECT Id, MX_SB_SAC_NombreClienteAseguradoText__c, MX_WB_apellidoPaternoAsegurado__c
                                          FROM Contract WHERE MX_SB_SAC_NumeroPoliza__c  LIKE: '%' + SAC_POLIZA+ '%' ];
        test.startTest();
        	MX_RTL_Contract_Service.getContractIdByNombre(policyTst[0].MX_SB_SAC_NombreClienteAseguradoText__c, policyTst[0].MX_WB_apellidoPaternoAsegurado__c, TESTFIELDS);
        	system.assertEquals(policyTst.size(), 10, 'Se han encontrado');
        test.stopTest();
    }
    
    @isTest
    static void testContractIdByAccount() {
        Final List<Contract> tstPolicy =[SELECT Id, accountId
                                          FROM Contract WHERE MX_SB_SAC_NumeroPoliza__c  LIKE: '%' + SAC_POLIZA+ '%' ];
        test.startTest();
        	MX_RTL_Contract_Service.getContractIdByAccount(tstPolicy[0].accountId, TESTFIELDS);
        	system.assertEquals(tstPolicy.size(), 10, 'Exito en la busqueda');
        test.stopTest();
    }
    
    @isTest
    static void testContractIdByPoliza() {
        Final List<Contract> srchTestPol =[SELECT Id, MX_SB_SAC_NumeroPoliza__c, MX_SB_MLT_Certificado__c 
                                          FROM Contract WHERE MX_SB_SAC_NumeroPoliza__c  LIKE: '%' + SAC_POLIZA+ '%' ];
        test.startTest();
        	MX_RTL_Contract_Service.getContractIdByPoliza(srchTestPol[0].MX_SB_SAC_NumeroPoliza__c,srchTestPol[0].MX_SB_MLT_Certificado__c, TESTFIELDSPD);
        	system.assertEquals(srchTestPol.size(), 10, 'Polizas localizadas');
        test.stopTest();
    }

    @isTest
    static void testUpsertSinCont() {
        Final Account testUpsert = [SELECT Id, Name FROM Account WHERE Name LIKE: '%' + NAMETEST + '%' LIMIT 1];
		Final Contract testContract = new Contract();
        	testContract.AccountId = testUpsert.Id;
        test.startTest();
        	MX_RTL_Contract_Service.upsertSinCont(testContract);
        	system.assert(true,'Actualización Correcta');
        test.stopTest();
    }
}