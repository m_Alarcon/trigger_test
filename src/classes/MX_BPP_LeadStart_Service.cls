/**
* @File Name          : MX_BPP_LeadStart_Service.cls
* @Description        :
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 23/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      23/06/2020        Gabriel García Rojas              Initial Version
**/
@SuppressWarnings('sf:unusedVariable')
public class MX_BPP_LeadStart_Service {

    /*Name Profile DO*/
    final static String NAMEPDO = 'BPyP Director Oficina';
    /*Name Profile DD*/
    final static String NAMEPDD = 'BPyP Director Divisional';
    /*Name Profile STAFF*/
    final static String NAMEPSTAFF = 'BPyP STAFF';
    /*Name Profile Soporte*/
    final static String NAMEPSP = 'Soporte BPyP';

    /*Variable methods fetchDataClass*/
    final static String QUERYFIELDSLEAD = ' Id, Status, LeadSource, OwnerId ';
    /*Variable methods fetchDataClass*/
    final static String QUERYFIELDSU = ' Id, Name, BPyP_ls_NombreSucursal__c, Divisi_n__c ';
    /*Variable methods fetchDataClass*/
    final static String BANCA = 'Red BPyP';
    /*Variable methods fetchDataClass*/
    final static String NAMEPERFIL = 'BPyP Estandar';
    /*Variable methods userInfo*/
    final static String NAMEPERFILU = '%BPyP Estandar%';
    /*Variable methods fetchDataClass*/
    final static String CONDICION = ' WHERE VP_ls_Banca__c =:filtro2 AND  Divisi_n__c != null AND BPyP_ls_NombreSucursal__c !=null AND Profile.Name =: filtro3 ';
    /*Variable method infoLeadByClause */
    final static String QUERYFIELDS = ' Id, Name, Status ';
    /*Variable method userInfo*/
    final static String FIELDS = ' Id, Name, DivisionFormula__c, BPyP_ls_NombreSucursal__c,  Profile.Name ';
    /*Variable method userInfo*/
    final static String CONDITIONUSER = ' Id IN: setId ';
    /*Variable method userInfo*/
    final static String QCLAUSEDIRO = ' WHERE BPyP_ls_NombreSucursal__c =: filtro1 AND VP_ls_Banca__c =: filtro2 AND IsActive = true AND Profile.Name Like: filtro3 ';
    /*Variable method userInfo*/
    final static String QCLAUSEDIRD = ' WHERE DivisionFormula__c =: filtro0 AND VP_ls_Banca__c =: filtro2 AND IsActive = true AND Profile.Name Like: filtro3 ';
    /*Variable method userInfo*/
    final static String QCLAUSESTAFF = ' WHERE VP_ls_Banca__c =: filtro2 AND IsActive = true AND Profile.Name Like: filtro3 ';

    /**Constructor */
    @TestVisible
    private MX_BPP_LeadStart_Service() { }

    /**
    * @description Get info trow profile
    * @author Gabriel Garcia Rojas
    * @return List<String>
    **/
    @SuppressWarnings('sf:DUDataflowAnomalyAnalysis')
    public static List<String> fetchusdata() {
		List<String> userDataList = new List<String>();
		try {
			userDataList = MX_BPP_CompUtils_cls.getUsrData();
		} catch(Exception e) {
            throw new AuraHandledException(System.Label.MX_BPP_PyME_Error_Generico + ' ' + e);
        }
		return userDataList;
    }

    /**
    * @description fetch info data
    * @author Gabriel Garcia Rojas
    * @return MX_BPP_LeadStart_Helper.WRP_ChartStacked
    **/
    public static MX_BPP_LeadStart_Helper.WRP_ChartStacked fetchDataClass(List<String> paramsLead, List<String> paramsUser, List<String> paramsDate, String expression) {
        final List<Lead> listLeads = MX_BPP_LeadStart_Helper.infoLeads(paramsLead, paramsUser, paramsDate, QUERYFIELDSLEAD, '');
        final List<String> listParams = new List<String>{'', '', BANCA, NAMEPERFIL};
        final Map<String,User> usrN = new Map<String,User>(MX_RTL_User_Selector.fetchListUserByDataBase(QUERYFIELDSU, CONDICION, listParams));
        final Set<String> lsLabels = new Set<String>();
        final Set<String> lsTyOpp = new Set<String>();
        final Map<String, Integer> tempData = MX_BPP_LeadStart_Helper.resultLeadAssignToUse(listLeads, usrN, expression, lsLabels, lsTyOpp);
        final List<String> tempColor = MX_BPP_LeadStart_Helper.tempColorGet(lsLabels);

        return new MX_BPP_LeadStart_Helper.WRP_ChartStacked(new List<String>(lsTyOpp), new List<String>(lsLabels), tempColor, tempData);
    }

    /**
    * @description Get CampaingMember list
    * @author Gabriel Garcia Rojas
    * @return List<CampaignMember>
    **/
    public static List<CampaignMember> infoLeadByClause(List<String> paramsLead, List<String> paramsUser, List<String> paramsDate, String limite) {
        String limitAndOffset = '';
        if(String.isNotBlank(limite)) {
            limitAndOffset = 'limit 10 offset ' + limite;
        }
        final List<Lead> listLeads = MX_BPP_LeadStart_Helper.infoLeads(paramsLead, paramsUser, paramsDate, QUERYFIELDS, limitAndOffset);
        return MX_BPP_LeadStart_Helper.fetchCampaingMember(listLeads);
    }

    /**
    * @description return Total of record by Division, Oficina or User
    * @author Gabriel Garcia Rojas
    * @return Map<String, Integer>
    **/
    @SuppressWarnings('sf:DUDataflowAnomalyAnalysis')
    public static Integer fetchPgs(Integer numRecords) {
    	Integer numpgs = 0;
		try {
			if(numRecords > 0) {
				numpgs = math.mod((numRecords),10)==0?((numRecords)/10):((numRecords)/10)+1; //NOSONAR
			}
		} catch(QueryException e) {
            throw new AuraHandledException(System.Label.MX_BPP_PyME_Error_Generico + ' ' + e);
        }
    	return numpgs;
    }

    /**
    * @description Get Info by user
    * @author Gabriel Garcia Rojas
    * @return InfoTabCampaignStartWrapper
    **/
	public static InfoTabCampaignStartWrapper userInfo() {
        final String userId = System.UserInfo.getUserId();
        final Set<Id> setId = new Set<Id>{userId};
        final User usuarioActual = MX_RTL_User_Selector.resultQueryUser(FIELDS, CONDITIONUSER, true, setId).get(0);

        final String nameDivision = usuarioActual.DivisionFormula__c;
        final String nameOffice = usuarioActual.BPyP_ls_NombreSucursal__c;

        List<User> listUser = new List<User>();
        final InfoTabCampaignStartWrapper infoList = new InfoTabCampaignStartWrapper();
        final List<String> listParams = new List<String>{nameDivision, nameOffice, banca, NAMEPERFILU};

        if(usuarioActual.Profile.Name == NAMEPDO && listUser.isEmpty() && !listParams.isEmpty()) {
            listUser = MX_RTL_User_Selector.fetchListUserByDataBase(fields, qClauseDirO, listParams);
            MX_BPP_LeadStart_Helper.infoByBanquero(infoList, usuarioActual, listUser);
        } else if(usuarioActual.Profile.Name == NAMEPDD) {
            listUser = MX_RTL_User_Selector.fetchListUserByDataBase(fields, qClauseDirD, listParams);
        	MX_BPP_LeadStart_Helper.infoByDirectorDivisional(infoList, usuarioActual, listUser);
        } else if(usuarioActual.Profile.Name == NAMEPSTAFF || usuarioActual.Profile.Name == NAMEPSP || usuarioActual.Profile.Name == Label.MX_BPP_Profile_SystemAdministrator ) {
            listUser = MX_RTL_User_Selector.fetchListUserByDataBase(fields, qClauseStaff, listParams);
        	MX_BPP_LeadStart_Helper.infoByStaff(infoList, usuarioActual, listUser);
        }

		return infoList;
    }

    /**
    * @description Class InfoTabCampaignStartWrapper
    * @author Gabriel Garcia Rojas
    **/
    public class InfoTabCampaignStartWrapper {

        /**map UserName */
        @auraEnabled
        public Map<String,List<String>> maplistUserName {get;set;}
        /**map Sucursales */
        @auraEnabled
        public Map<String,Set<String>> mapSucursales {get;set;}
        /**set divisiones */
        @auraEnabled
        public Set<String> setDivisiones {get;set;}

        /**Constructor Wrapper */
        public InfoTabCampaignStartWrapper() {
            maplistUserName = new Map<String,List<String>>();
            mapSucursales = new Map<String,Set<String>>();
            setDivisiones = new Set<String>();
        }
    }
}