/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_FichaProducto_Ctrl_Test
* @Author   	Héctor Israel Saldaña Pérez | hectorisrael.saldana.contractor@bbva.com
* @Date     	Created: 2021-03-29
* @Description 	Test Class for MX_BPP_FichaProducto_Ctrl code coverage
* @Changes
*
*/
@IsTest
public class MX_BPP_FichaProducto_Ctrl_Test {

	@TestSetup
	private static void setup() {
		final User usuarioAdmin = UtilitysDataTest_tst.crearUsuario('PruebaAdm', Label.MX_PERFIL_SystemAdministrator, 'BBVA ADMINISTRADOR');
		insert usuarioAdmin;

		System.runAs(usuarioAdmin) {
			final Account accTst = new Account();
			accTst.FirstName = 'user BPyP';
			accTst.LastName = 'Test Acc';
			accTst.No_de_cliente__c = 'D2050394';
			accTst.RecordTypeId = RecordTypeMemory_cls.getRecType('Account', 'MX_BPP_PersonAcc_Client');
			accTst.PersonEmail = 'testusr.minuta@test.com';
			insert accTst;

			final dwp_kitv__Visit__c visitTst = new dwp_kitv__Visit__c();
			visitTst.Name = 'Visita Prueba';
			visitTst.dwp_kitv__visit_start_date__c = Date.today()+4;
			visitTst.dwp_kitv__account_id__c = accTst.Id;
			visitTst.dwp_kitv__visit_duration_number__c = '15';
			visitTst.dwp_kitv__visit_status_type__c = '01';
			insert visitTst;

			final ContentVersion contentVersion = new ContentVersion();
			contentVersion.Title = 'PenguinsBIE';
			contentVersion.PathOnClient = 'PenguinBIE.pdf';
			contentVersion.VersionData = Blob.valueOf('Test ContentVersion');

			contentVersion.ContentLocation = 'S';
			insert contentVersion;

			final ContentVersion contentVersion2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion.Id LIMIT 1];
			final ContentDocumentLink contentDocLink = new ContentDocumentLink();
			contentDocLink.ContentDocumentId = contentVersion2.ContentDocumentId;
			contentDocLink.LinkedEntityId = visitTst.Id;
			contentDocLink.ShareType = 'V';
			insert contentDocLink;

		}

	}

	/**
    * @description Test method which covers getAssetLibraries() method from Controller class
    * @params NA
    * @return void
    **/
	@IsTest
	static void getCntDetailsTest() {
		final dwp_kitv__Visit__c tstVisit = [SELECT Id, Name FROM dwp_kitv__Visit__c WHERE Name = 'Visita Prueba' LIMIT 1];
		final String result = MX_BPP_FichaProducto_Ctrl.getContentDetails(tstVisit.Id);
		final List<ContentVersion> cvResult = (List<ContentVersion>)JSON.deserialize(result, List<ContentVersion>.class);
		System.assertEquals('PenguinsBIE', cvResult[0].Title, 'Titulo del archivo incorrecto');
	}

	/**
    * @description Test method which covers getContentDetails() method from Controller class
    * @params NA
    * @return void
    **/
	@IsTest
	static void getLibrariesTest() {
		final User admUsr = [SELECT Id FROM User WHERE LastName = 'PruebaAdm' LIMIT 1];
		Test.StartTest();

		System.runAs(admUsr) {
			final ContentWorkspace workspace1 = new ContentWorkspace();
			workspace1.Name = 'Test Library';
			insert workspace1;
			final ContentWorkspace workspace2 = [SELECT Id, Name from ContentWorkspace where Id =: workspace1.id LIMIT 1];
			final ContentVersion cv1tst = new ContentVersion();
			cv1tst.Title = 'PenguinsBIE';
			cv1tst.PathOnClient = 'PenguinBIE.pdf';
			cv1tst.VersionData = Blob.valueOf('Test ContentVersion');
			cv1tst.ContentLocation = 'S';
			insert cv1tst;
			final ContentVersion cv2tst = [SELECT Id, Title, ContentDocumentId, FirstPublishLocationId FROM ContentVersion where Id =: cv1tst.Id LIMIT 1];
			final ContentDocumentLink cdl = new ContentDocumentLink();
			cdl.ContentDocumentId = cv2tst.ContentDocumentId;
			cdl.ShareType = 'I';
			cdl.Visibility = 'AllUsers';
			cdl.LinkedEntityId = workspace2.Id;
			insert cdl;

			final List<Id> docIds = new List<Id>{cv2tst.ContentDocumentId};
			final String result = MX_BPP_FichaProducto_Ctrl.getAssetLibraries(docIds);
			final List<ContentWorkspace> resultLst = (List<ContentWorkspace>) JSON.deserialize(result, List<ContentWorkspace>.class);
			final ContentDocument docTst = [SELECT Id, ParentId FROM ContentDocument WHERE Id =: cv2tst.ContentDocumentId LIMIT 1];
			System.assertEquals(docTst.ParentId, resultLst[0].Id, 'No se encuentra archivo en folder');
		}

		Test.stopTest();
	}

	/**
    * @description Test method which covers getContentDocuments() method from Controller class
    * @params NA
    * @return void
    **/
	@IsTest
	static void getDocumentsTest() {
		final ContentVersion testContentV1= new ContentVersion();
		testContentV1.Title = 'PenguinsBIE2';
		testContentV1.PathOnClient = 'PenguinBIE2.pdf';
		testContentV1.VersionData = Blob.valueOf('Test ContentVersion2');
		testContentV1.ContentLocation = 'S';
		insert testContentV1;

		final ContentVersion resultCV = [SELECT Id, Title, ContentDocumentId FROM ContentVersion where Id =: testContentV1.Id LIMIT 1];
		final List<Id> listIds = new List<Id>{resultCV.ContentDocumentId};
		final String strResult = MX_BPP_FichaProducto_Ctrl.getContentDocuments(listIds);
		final List<ContentDocument> lstDocs = (List<ContentDocument>) JSON.deserialize(strResult, List<ContentDocument>.class);
		System.assertEquals(lstDocs[0].Id, resultCV.ContentDocumentId, 'No se encuentra documento por Id');
	}
}