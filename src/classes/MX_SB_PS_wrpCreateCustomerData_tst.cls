/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_PS_wrpCotizadorVidaResp_Test
* Autor Daniel Perez Lopez
* Proyecto: Salesforce Presuscritos
* Descripción : Prueba los Methods de la clase MX_SB_PS_wrpCotizadorVidaResp

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           10/12/2019      Daniel Lopez                         Creación
* 1.1           04/01/2021    Juan Carlos Benitez                 Se cambia assert
* --------------------------------------------------------------------------------
*/
@IsTest
public with sharing class MX_SB_PS_wrpCreateCustomerData_tst {
    @IsTest
    static void obtieneWraper() {
        test.startTest();
            final MX_SB_PS_wrpCreateCustomerData clase= new MX_SB_PS_wrpCreateCustomerData();            
            System.assert(clase.header.branchOffice!=null, 'exito en validadcion');
        test.stopTest();
    }
}