/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 03-19-2021
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   01-04-2021   Eduardo Hernández Cuamatzi   Initial Version
 * 1.1   03-03-2021   Diego Olvera                 Se añade función que recupera lista dependiendo de zona
 * 1.1.1 19-03-2021   Diego Olvera                 Se ajusta query para recuperar valores por categoria
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_RTL_Cobertura_Selector {

    /**
    * @description Recupera coberturas por quote
    * @author Eduardo Hernández Cuamatzi | 01-04-2021 
    * @param queryFields campos a recuperar
    * @param queryConditions condicion de query
    * @param quotesId Id de Quote
    * @return List<Cobertura__c> Lista de Coberturas
    **/
    public static List<Cobertura__c> findCoverByQuote(String queryFields, String queryConditions, Set<Id> quotesId) {
        final String query = 'SELECT '+queryFields+' FROM Cobertura__c WHERE MX_SB_VTS_RelatedQuote__c IN: quotesId' +queryConditions;
        return (List<Cobertura__c>)DataBase.query(String.escapeSingleQuotes(query).unescapeJava());
    }

    /**
    * @description Recupera coberturas por código de cobertura
    * @author Eduardo Hernández Cuamatzi | 01-04-2021 
    * @param queryFields Campos a recuperar 
    * @param queryConditions Filtro de query
    * @param quotesId Id de Quote
    * @param coverageCodes Codigos de coberturas a buscar
    * @return List<Cobertura__c> Lista de coberturas
    **/
    public static List<Cobertura__c> findCoverByCode(String queryFields, String queryConditions, Set<Id> quotesId, List<String> coverageCodes) {
        final String query = 'SELECT '+queryFields+' FROM Cobertura__c WHERE MX_SB_VTS_RelatedQuote__c IN: quotesId' +queryConditions + ' AND (MX_SB_VTS_CoverageCode__c IN: coverageCodes OR MX_SB_MLT_Cobertura__c IN: coverageCodes)';
        return (List<Cobertura__c>)DataBase.query(String.escapeSingleQuotes(query));
    }

     /**
    * @description Función que recupera lista de coberturas dependiendo de zona
    * @author Diego Olvera | 03-03-2021 
    * @param qryField, valTrad, idQuoVal, coverVals
    * @return List<Cobertura__c> Lista de coberturas
    **/  
     public static List<Cobertura__c> findCoverByTrade(String qryField, String valTrad, Set<Id> idQuoVal, List<String> coverVals) {
        final String tQuery = 'SELECT ' +qryField +' FROM Cobertura__c WHERE MX_SB_VTS_RelatedQuote__c IN: idQuoVal AND MX_SB_VTS_CodeTrade__c=: valTrad AND MX_SB_VTS_CoverageCode__c IN: coverVals AND MX_SB_VTS_CategoryCode__c IN: coverVals';
        return (List<Cobertura__c>)DataBase.query(String.escapeSingleQuotes(tQuery));
    }

    /**
    * @description Actualiza registros de coberturas
    * @author Eduardo Hernández Cuamatzi | 01-04-2021 
    * @param lstCoverages Lista de coberturas ah actualizar
    **/
    public static void updateCoverages(List<Cobertura__c> lstCoverages) {
        update lstCoverages;
    }
    
    /**
    * @description Inserta o Actualiza registros de coberturas
    * @author Alexandro Corzo | 04-01-2021 
    * @param lstCoverages Lista de coberturas ah insertar o actualizar
    **/
    public static void upsertCoverages(List<Cobertura__c> lstCoverages) {
        upsert lstCoverages;
    }
}