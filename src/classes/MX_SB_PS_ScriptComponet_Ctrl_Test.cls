/**
 * @description       :  Prueba los methodos de MX_SB_PS_ScriptComponet_Ctrl
 * @author            : Alan Ricardo Hernandez
 * @group             : 
 * @last modified on  : 19/02/2021
 * @last modified by  : Alan Ricardo Hernandez
 * Modifications Log 
 * Ver   Date         Author                   Modification
 * 1.0   17/02/2021   Alan Ricardo Hernandez   Initial Version
**/
@isTest
public class MX_SB_PS_ScriptComponet_Ctrl_Test {

    /** user data */
    public static final String TEST_USER = 'TestUser';

    @isTest
    static void getMetadataTest() {
        Test.startTest();
            Final List<MX_SB_PS_ScriptComponent__mdt> mdtScript= MX_SB_PS_ScriptComponet_Ctrl.obtenerDatos();
            System.assert(mdtScript[0].Activo__c, true);
        Test.stopTest();
    }
    
    @isTest
    static void getUserDetailsTest() {
        Final String nameProfile = [SELECT Name from profile where name in ('Administrador del sistema','System Administrator') limit 1].Name;
        Final User newUser = MX_WB_TestData_cls.crearUsuario(TEST_USER, nameProfile);
        Insert newUser;
        System.runAs(newUser) {
            Final User usuario = MX_SB_PS_ScriptComponet_Ctrl.getUserDetails(UserInfo.getUserId());
            System.assertEquals(usuario.Id, newUser.Id,'Usuario actual');
        }
    }
}