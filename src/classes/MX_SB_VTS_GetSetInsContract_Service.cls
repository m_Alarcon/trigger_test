/**
 * @File Name          : MX_SB_VTS_GetSetInsContract_Service.cls
 * @Description        : Clase Encargada de Proporcionar la Consulta de Polizas
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 08-13-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0       03/08/2020      Alexandro Corzo        Initial Version
 * 1.1		 07/09/2020		 Alexandro Corzo		Se agrega la función: obtQueryValPolizContractOppo
 * 1.1.1     09/03/2021		 Eduardo Hernández      Se agrega mejora para titulo de poliza
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_GetSetInsContract_Service {
    /** Variable de Apoyo: OKCODE */
    private static final Integer OKCODE = 200;
    /** Variable de Apoyo:  NOCONTENT */
    private static final Integer NOCONTENT = 204;
    /** Variable de Apoyo:  SRVNOTAVAL */
    private static final Integer SRVNOTAVAL = 503;
    /** Variable de Apoyo: sFldPolNumber */
    Static String sFldPolNumber = 'policyNumber';
    /** Variable de Apoyo: isExistFld */
    Static String isExistFld = 'isExist';
    /** Variable de Apoyo:  sFldStartDatCont */
    Static String sFldStartDatCont = 'startDateContract';
    /** Variable de Apoyo:  sFldEndDatCont */
    Static String sFldEndDatCont = 'endDateContract';
    /** Variable de Apoyo:  sFldIsContVal */
    Static String sFldIsContVal = 'isContractValid';
    /** Variable de Apoyo: sFldMessage */
    Static String sFldMessage = 'message';
    /** Variable de Apoyo: sFldParamVal */
    Static String sFldParamVal = 'isParamsValid';
    /** Variablr de Apoyo: sFldFDateN */
    Static String sFldFDateN = 'ddmmyyyy';
    /** Variable de Apoyo: sFldFDateI */
    Static String sFldFDateI = 'yyyymmdd';
    /** Variables de Apoyo: sFldIsValPol */
    Static String sFldIsValPol = 'isValidPoliz';
    /** Variables de Apoyo: sFldValPolMsg */
    Static String sFldValPolMsg = 'isValidPolizMsg';
    
    /**
     * @description: Realiza la consulta de la poliza conforme al numero de poliza proporcionado
     * @author: Eduardo Hernández Cuamatzi
     * @return: 	 Map<String,List<Object>> oReturnValues
     */
    public static Map<String,Object> obtQueryInsuranceContract(String sPolicyNumber) {
        Map<String, Object> oReturnValues = null;
        try {
            final Map<String, Object> sQuerySrv = new Map<String, Object>{'userId' => '1020', 'policyNumber' => sPolicyNumber};
            final HttpResponse oResponseSrv = MX_RTL_IasoServicesInvoke_Selector.callServices('getInsuranceContracts', sQuerySrv, obtRequestSrv());
            if(oResponseSrv.getStatusCode() == OKCODE) {
                final Map<String, Object> oReturnValuesSrv = (Map<String, Object>) Json.deserializeUntyped(oResponseSrv.getBody());
                final List<Object> oValInsCont = (List<Object>) oReturnValuesSrv.get('data');
                if(!oValInsCont.isEmpty()) {
                    final List<Object> oDataSuccess = new List<Object>();
                    for(Object oRow : oValInsCont) {
                        final String sPolicyNumberWs = ((Map<String, Object>) oRow).get(sFldPolNumber).toString();
                        if (sPolicyNumber.equalsIgnoreCase(sPolicyNumberWs)) {
                            oReturnValues = new Map<String, Object>();
                            oReturnValues.put(isExistFld, true);
                            oReturnValues.put(sFldStartDatCont, ((Map<String, Object>) ((Map<String, Object>) oRow).get('validityPeriod')).get('startDate').toString());
                            oReturnValues.put(sFldEndDatCont, ((Map<String, Object>) ((Map<String, Object>) oRow).get('validityPeriod')).get('endDate').toString());
                            oReturnValues.put(sFldPolNumber, ((Map<String, Object>) oRow).get(sFldPolNumber));
                            final Datetime dtFechaIni = System.today();
                            final String sEndDateContract = ((Map<String, Object>) ((Map<String, Object>) oRow).get('validityPeriod')).get('endDate').toString();
                            final String[] sEndDateContArray = sEndDateContract.split('-');
                            final Datetime dtFechaFin = Datetime.newInstance(Integer.valueOf(sEndDateContArray[0]), Integer.valueOf(sEndDateContArray[1]), Integer.valueOf(sEndDateContArray[2]));
                            oReturnValues.put(sFldIsContVal, MX_SB_VTS_GetSetInsContract_Helper.obtDiffDiasFechas(dtFechaIni, dtFechaFin));
                            oReturnValues.put(sFldMessage, 'La poliza existe en los registros!');
                            oReturnValues.put(sFldParamVal, true); 
                        } else {
                            oReturnValues = MX_SB_VTS_GetSetInsContract_Helper.fillDataMapObj(sPolicyNumber, NOCONTENT);
                        }
                    }
                    oReturnValues.put('response', oDataSuccess);
                    oReturnValues.put('record', oValInsCont);
                }    
            } else {
                oReturnValues = MX_SB_VTS_GetSetInsContract_Helper.fillDataMapObj(sPolicyNumber, oResponseSrv.getStatusCode());
            }
        } catch(Exception e) {
            oReturnValues = MX_SB_VTS_GetSetInsContract_Helper.fillDataMapObj(sPolicyNumber, SRVNOTAVAL);
        }
        return oReturnValues;
    }

    /**
     * @description: Realiza la validación de si la poliza esta asociada a un Contrato + Opportunidad
     * @author: Alexandro Corzo
     * @return: Map<String, Object> oReturnValues
     */
    public static Map<String, Object> obtQueryValPolizContractOppo(Map<String, Object> oParams, Id oppId) {
        Map<String, Object> oReturnValues = null;
       	final String sNumeroPoliza = oParams.get('policyNumber').toString();
        final Integer iExisteContrato = database.countQuery('SELECT COUNT() FROM Contract WHERE MX_SB_SAC_NumeroPoliza__c = \'' + sNumeroPoliza + '\'');
        if(iExisteContrato == 0) {
            String sProducto = null;
            sProducto = 'Seguro Estudia';
            final Opportunity objOpportunity = [SELECT AccountId, Account.Name FROM Opportunity WHERE Id =: oppId];
            final Product2 objProducto = [SELECT Id, Name FROM Product2 WHERE Name =: sProducto AND MX_SB_SAC_Proceso__c = 'VTS'];
            final Contract objContract = new Contract();
            if(Boolean.valueOf(oParams.get(isExistFld))) {                
				objContract.StartDate = Date.valueOf(oParams.get(sFldStartDatCont).toString());
                objContract.MX_SB_VTS_FechaInicio__c = MX_SB_VTS_GetSetInsContract_Helper.obtFormatDate(new List<String>{oParams.get(sFldStartDatCont).toString(), sFldFDateI, sFldFDateN, '-', '/'});
            	objContract.MX_SB_VTS_FechaFin__c = MX_SB_VTS_GetSetInsContract_Helper.obtFormatDate(new List<String>{oParams.get(sFldEndDatCont).toString(), sFldFDateI, sFldFDateN, '-', '/'});
                objContract.Status = 'Draft';
                objContract.MX_SB_SAC_EstatusPoliza__c = 'Activated';
            	oParams.put(sFldIsValPol, true);
            	oParams.put(sFldValPolMsg, 'La poliza existe en los registros!');
            } else {    
                objContract.StartDate = System.today();
                objContract.MX_SB_VTS_FechaInicio__c = MX_SB_VTS_GetSetInsContract_Helper.obtFormatDate(new List<String>{String.valueOf(System.today()), sFldFDateI, sFldFDateN, '-', '/'});
            	objContract.MX_SB_VTS_FechaFin__c = MX_SB_VTS_GetSetInsContract_Helper.obtFormatDate(new List<String>{String.valueOf(System.today()), sFldFDateI, sFldFDateN, '-', '/'});
                oParams.put(sFldIsValPol, false);
                if(Boolean.valueOf(oParams.get('isAvailableSrv'))) {
                	objContract.MX_SB_SAC_EstatusPoliza__c = 'Invalid';
                	oParams.put(sFldValPolMsg, 'La poliza no existe en los registros!');
                } else {
                	objContract.MX_SB_SAC_EstatusPoliza__c = 'Unavailable Service';
                	oParams.put(sFldValPolMsg, 'El servicio no se encuentra disponible, intente nuevamente!');
                }
            }
            objContract.AccountId = objOpportunity.AccountId;
            objContract.MX_WB_Oportunidad__c = oppId;
            objContract.MX_WB_Producto__c = objProducto.Id;
            objContract.MX_SB_SAC_NumeroPoliza__c = sNumeroPoliza;
            insert objContract; 
        } else {
            Boolean isPertenece = false;
            final List<Contract> lstContract = [SELECT Id, MX_WB_noPoliza__c, MX_SB_SAC_NumeroPoliza__c, MX_WB_Producto__r.Name, CreatedDate FROM Contract WHERE MX_SB_SAC_NumeroPoliza__c =: sNumeroPoliza];
            for(Contract oRow: lstContract) {
                if(!String.valueOf(oRow.Id).equals(String.valueOf(oppId))) {
               		oParams.put(sFldIsValPol, false);
                    oParams.put(sFldValPolMsg, 'Error: póliza registrada anteriormente');
                    isPertenece = true;
                    break;
                }
            }
            if(!isPertenece) {
                oParams.put(sFldIsValPol, false);
                oParams.put(sFldValPolMsg, 'Error: póliza registrada anteriormente a otra oportunidad!');
            }
        }
       	oReturnValues = oParams;
        return oReturnValues;
    }
    
    /**
     * @description: Recupera el registro del contrato registrado y valida si la poliza existe y es valida
     * @author: Alexandro Corzo
     * @return: Map<String, Object> objReturnValues
     */
    public static Map<String, Object> obtDataPolizContract(String sNumPoliz) {
        final Map<String, Object> objReturnValues = new Map<String, Object>();
        final Datetime dtFechaIni = System.today();
        Datetime dtFechaFin = null;
        String sEndDateContract = null;
        String[] sEndDatContArray = null;
        final List<Contract> lstContract = MX_RTL_Contract_Selector.getContractBySACnPoliza(sNumPoliz, 'MX_SB_SAC_NumeroPoliza__c, Status, MX_SB_VTS_FechaInicio__c, MX_SB_VTS_FechaFin__c, MX_SB_SAC_EstatusPoliza__c ', 'MX_SB_SAC_NumeroPoliza__c');
        for(Contract oRowContract : lstContract) {
            sEndDateContract = MX_SB_VTS_GetSetInsContract_Helper.obtFormatDate(new List<String>{oRowContract.MX_SB_VTS_FechaFin__c, sFldFDateN, sFldFDateI, oRowContract.MX_SB_SAC_EstatusPoliza__c == 'Incluida' ? '-' : '/', '-'});
            sEndDatContArray = sEndDateContract.split('-');
            dtFechaFin = Datetime.newInstance(Integer.valueOf(sEndDatContArray[0]), Integer.valueOf(sEndDatContArray[1]), Integer.valueOf(sEndDatContArray[2]));
            objReturnValues.put(sFldIsContVal, MX_SB_VTS_GetSetInsContract_Helper.obtDiffDiasFechas(dtFechaIni, dtFechaFin));
            if(String.isEmpty(oRowContract.MX_SB_SAC_EstatusPoliza__c) || oRowContract.MX_SB_SAC_EstatusPoliza__c.equalsIgnoreCase('Invalid')) {
                objReturnValues.put(isExistFld, false);
                objReturnValues.put(sFldStartDatCont, '');
            	objReturnValues.put(sFldEndDatCont, '');
                objReturnValues.put('isServiceAvailable', true);
            } else if(oRowContract.MX_SB_SAC_EstatusPoliza__c.equalsIgnoreCase('Unavailable Service')) {
                objReturnValues.put(isExistFld, false);
                objReturnValues.put(sFldStartDatCont, '');
            	objReturnValues.put(sFldEndDatCont, '');
                objReturnValues.put('isServiceAvailable', false);
            } else if(oRowContract.MX_SB_SAC_EstatusPoliza__c.equalsIgnoreCase('Incluida')) {
                objReturnValues.put(isExistFld, true);
                objReturnValues.put(sFldStartDatCont, oRowContract.MX_SB_VTS_FechaInicio__c);
            	objReturnValues.put(sFldEndDatCont, oRowContract.MX_SB_VTS_FechaFin__c);
            } else {
            	objReturnValues.put(isExistFld, true);
            	objReturnValues.put(sFldStartDatCont, MX_SB_VTS_GetSetInsContract_Helper.obtFormatDate(new List<String>{oRowContract.MX_SB_VTS_FechaInicio__c, sFldFDateN, sFldFDateI, '/', '-'}));
            	objReturnValues.put(sFldEndDatCont, MX_SB_VTS_GetSetInsContract_Helper.obtFormatDate(new List<String>{oRowContract.MX_SB_VTS_FechaFin__c, sFldFDateN, sFldFDateI, '/', '-'}));    
            }
        }
        return objReturnValues;
    }

    /**
     * @description : Realiza el armado del objeto Request para el consumo del servicio ASO
     * @author      : Alexandro Corzo
     * @return      : HttpRequest oRequest
     */
    public static HttpRequest obtRequestSrv() {
        final Map<String,iaso__GBL_Rest_Services_Url__c> allCodesASO = iaso__GBL_Rest_Services_Url__c.getAll();
        final iaso__GBL_Rest_Services_Url__c objASOSrv = allCodesASO.get('getInsuranceContracts');
        final String strEndoPoint = objASOSrv.iaso__Url__c;
        final HttpRequest oRequest = new HttpRequest();
        oRequest.setTimeout(120000);
        oRequest.setMethod('GET');
        oRequest.setHeader('Content-Type', 'application/json');
        oRequest.setHeader('Accept', '*/*');
        oRequest.setHeader('Host', 'https://test-sf.bbva.mx');
        oRequest.setEndpoint(strEndoPoint);
        return oRequest;
    }
}