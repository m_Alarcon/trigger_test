/**
 * @File Name          : MX_BPP_ConvertLeads_Service.cls
 * @Description        : Clase para convertir Leads
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 02/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      02/06/2020           Jair Ignacio Gonzalez G.   Initial Version
**/
public without sharing class MX_BPP_ConvertLeads_Service {
    /**
     * @Method getLeadToConverd
     * @Description Singletons
    **/
    @TestVisible private MX_BPP_ConvertLeads_Service() {
    }
    /**
     * @Method convertToLead
     * @param List<Database.LeadConvert> lsLeadsConvert
     * @Description Convierte un Lead
     * @return List<Database.LeadConvertResult>
    **/
    public static Map<String,String> convertToLead(List<Id> idsLeads) {
        final Savepoint saveState = Database.setSavepoint(); //NOSONAR
        try {
            final Id bppypLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('MX_BPP_Leads').getRecordTypeId();
            final List<CampaignMember>lsCampMembers = MX_BPP_ConvertLeads_Helper.getLeadToConverd(idsLeads);
            final Map<Id, Opportunity> mapLeadOpp = MX_BPP_ConvertLeads_Helper.createOpportunity(lsCampMembers, bppypLead);
            final Map<String,String> convertResult = MX_BPP_ConvertLeads_Helper.convertLeads(lsCampMembers, bppypLead, mapLeadOpp);
            MX_BPP_ConvertLeads_Helper.updateSubProducto(lsCampMembers, convertResult);
            return convertResult;
        } catch (DMLException err ) {
            Database.rollback(saveState);
            throw new AuraHandledException(Label.MX_WB_lbl_MsgConvertException + err);
        }
    }
}