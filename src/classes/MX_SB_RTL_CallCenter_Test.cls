/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 10-08-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   10-07-2020   Eduardo Hernández Cuamatzi   Initial Version
**/
@isTest
private class MX_SB_RTL_CallCenter_Test {
    @TestSetup
    static void makeData() {
        MX_SB_PC_TransferComunication_Ctrl_Test.initCallCenter();
    }

    @isTest
    static void findUrlCallSelect() {
        Test.startTest();
            final User userCallsel = [Select Id, CallCenterId from User where Name = 'Asesor callcenter' limit 1];
            final String callCenUserSel = MX_SB_PC_TransferComunication_Service.findUrlCallCenter(userCallsel.Id);
        System.assertEquals('https://apps.mypurecloud.com/crm/index.html?crm=salesforce&color=darkblue', callCenUserSel, 'Callcenter purecloud');
        Test.stopTest();        
    }
}