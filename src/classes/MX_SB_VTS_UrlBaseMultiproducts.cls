/**
 * @File Name          : MX_SB_VTS_UrlBaseMultiproducts.cls
 * @Description        :
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              :
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 29/5/2020 10:28:04
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/6/2019 18:58:56   Eduardo Hernández Cuamatzi     Initial Version
 * 1.1    19/7/2019           Jaime Terrats             Se agrega construccion de URLS
 * 1.1.1  02/12/2019          Eduardo Hernández Cuamatzi     Fix parametro valor Hogar
 * 1.1.2  20/12/2019          Tania Vázquez Hernández      Fix parametros sismo, fechanacimiento y espacios
**/
public without sharing class MX_SB_VTS_UrlBaseMultiproducts {

    /**
     * urlBaseHogar description
     * @param  quoteData     Datos de cotización
     * @param  accountData   Datos de cuenta
     * @param  inmutableVals valores fijos para url
     * @param  quoliData     Datos partida de cotización
     * @return               url con parámetros
     */
    public static String urlBaseHogar(Quote quoteData, Account accountData, Map<String, String> inmutableVals, QuoteLineItem quoliData) {
        String fianlUrl = '';
        fianlUrl = '&folioCot='+ MX_SB_VTS_rtwCotFillData.validEmptyStr(quoteData.MX_SB_VTS_Folio_Cotizacion__c);
        fianlUrl += '&nombreCot=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(accountData.FirstName);
        fianlUrl += '&apCot=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(accountData.LastName);
        fianlUrl += '&amCot=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(accountData.Apellido_materno__pc);
        fianlUrl += '&curp=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(accountData.RFC__c);
        fianlUrl += '&telCot=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoteData.MX_SB_VTS_Movil_txt__c);
        fianlUrl += '&totalAse=' + (MX_SB_VTS_rtwCotFillData.validEmptyStr(String.valueOf(quoliData.MX_SB_VTS_ValorHogar__c)) == '' ? '0' : String.valueOf(Integer.valueOf(quoliData.MX_SB_VTS_ValorHogar__c)));
        fianlUrl += '&plan=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoliData.MX_SB_VTS_Plan__c);
        fianlUrl += '&tipoPago=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoliData.MX_SB_VTS_Frecuencia_de_Pago__c);
        fianlUrl += '&tipoEd=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoliData.MX_SB_VTS_Casa_o_Departamento__c);
        fianlUrl += '&totalP=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoliData.MX_SB_VTS_No_Pisos_Inmueble__c);
        fianlUrl += '&pHabitado=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoliData.MX_SB_VTS_Piso_Habitado__c);
        fianlUrl += '&cSismo=' + (MX_SB_VTS_rtwCotFillData.validEmptyStr(String.valueOf(quoliData.MX_SB_VTS_Proteccion_Sismo__c)) == ''  ? 'sin' : String.valueOf(quoliData.MX_SB_VTS_Proteccion_Sismo__c).toLowerCase());
        fianlUrl += '&mantosA=' + true;
        fianlUrl += '&murosC=' + false;
        fianlUrl += '&concretoA=' + false;
        fianlUrl += '&generoCot=' + validateGender(accountData.MX_Gender__pc);
        fianlUrl += '&email=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoteData.MX_SB_VTS_Email_txt__c);
        fianlUrl += '&postalCot=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoteData.ShippingPostalCode);
        fianlUrl += '&dirCot=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoteData.MX_SB_VTS_Colonia__c);
        fianlUrl += '&noExt=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoteData.MX_SB_VTS_Numero_Exterior__c);
        fianlUrl += '&noInt=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoteData.MX_SB_VTS_Numero_Interior__c);
		if(fianlUrl.containsWhitespace()==true) {
               fianlUrl = fianlUrl.replace(' ', '%20');
        }
        return fianlUrl;
    }

    /**
     * urlBaseHogar description
     * @param  quoteData     Datos de cotización
     * @param  accountData   Datos de cuenta
     * @param  inmutableVals valores fijos para url
     * @param  quoliData     Datos partida de cotización
     * @return               url con parámetros
     */
    public static String urlBaseVida(Quote quoteData, Account accountData, Map<String, String> inmutableVals, QuoteLineItem quoliData, List<MX_SB_VTS_Beneficiario__c> beneficiarios) {
        String fianlUrl = '';
        Integer i = 0;
        fianlUrl = '&folioCot='+ MX_SB_VTS_rtwCotFillData.validEmptyStr( String.isNotBlank(quoteData.MX_SB_VTS_Folio_Cotizacion__c) ? quoteData.MX_SB_VTS_Folio_Cotizacion__c : String.valueOf(quoteData.Id));
        fianlUrl += '&nombreCot=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(accountData.FirstName);
        fianlUrl += '&apCot=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(accountData.LastName);
        fianlUrl += '&amCot=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(accountData.Apellido_materno__pc);
        fianlUrl += '&curp=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(accountData.RFC__c);
        fianlUrl += '&telCot=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoteData.MX_SB_VTS_Movil_txt__c);
        fianlUrl += '&totalAse=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(String.valueOf(Integer.valueOf(quoliData.MX_SB_VTS_Total_Gastos_Funerarios__c)));
        fianlUrl += '&plan=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoliData.MX_SB_VTS_Plan__c);
        fianlUrl += '&Cancerigenas=' + quoliData.MX_SB_VTS_Cancer_Tumores_Leucemia_Lupus__c;
        fianlUrl += '&cardioVascular=' + quoliData.MX_SB_VTS_Aneurisma_Trombosis_Embolia__c;
        fianlUrl += '&respiratorias=' + quoliData.MX_SB_VTS_EmficemaBronquitisTuberculosis__c;
        fianlUrl += '&hepaticas=' + quoliData.MX_SB_VTS_Insuficiencias_Cirrosis_Hepati__c;
        fianlUrl += '&generoCot=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(validateGender(accountData.MX_Gender__pc));
        fianlUrl += '&tipoPago=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoliData.MX_SB_VTS_Frecuencia_de_Pago__c);
        fianlUrl += '&email=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoteData.MX_SB_VTS_Email_txt__c);
        fianlUrl += '&postalCot=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoteData.ShippingPostalCode);
        fianlUrl += '&dirCot=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoteData.ShippingStreet);
        fianlUrl += '&noExt=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoteData.MX_SB_VTS_Numero_Exterior__c);
        fianlUrl += '&noInt=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(quoteData.MX_SB_VTS_Numero_Interior__c);
        if(beneficiarios.size() > 0) {
            for(MX_SB_VTS_Beneficiario__c beneficiario: beneficiarios) {
                fianlUrl += '&numBene=' + i + 1;
                fianlUrl += '&nomBen' + i + '=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(beneficiario.Name);
                fianlUrl += '&apBen' + i +'='+ MX_SB_VTS_rtwCotFillData.validEmptyStr(beneficiario.MX_SB_VTS_APaterno_Beneficiario__c);
                fianlUrl += '&amBen' + i  + '=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(beneficiario.MX_SB_VTS_AMaterno_Beneficiario__c);
                fianlUrl += '&parent' + i  + '=' + MX_SB_VTS_rtwCotFillData.validEmptyStr(beneficiario.MX_SB_VTS_Parentesco__c);
                fianlUrl += '&porcen' + i  + '=' + beneficiario.MX_SB_VTS_Porcentaje__c;
                i++;
            }
        }
		 if(fianlUrl.containsWhitespace()==true) {
               fianlUrl = fianlUrl.replace(' ', '%20');
        }
        return fianlUrl;
    }

    private static String validateGender(String gender) {
        String fixedValue = '';
        switch on gender {
            when 'Masculino' {
                fixedValue = 'M';
            }
            when 'Femenino' {
                fixedValue = 'F';
            }
		when 'M' {
                fixedValue = 'M';
            }
            when 'F' {
                fixedValue = 'F';
            }
        }

        return fixedValue;
    }
}