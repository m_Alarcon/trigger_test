/**
* -------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_Poliza_Recibos_cls
* Autor: Angel Ignacio Nava
* Proyecto: Siniestros - BBVA
* Descripción : Clase controladora del componente MX_SB_MLT_Poliza_Recibos
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                       Descripción
* --------------------------------------------------------------------------------
* 1.0           02/01/2020     Angel Ignacio Nava           Creación
* --------------------------------------------------------------------------------
*/
 public without sharing class MX_SB_MLT_Poliza_Generales_cls { //NOSONAR
    /*
	*@description Method for query to policy
	*@Params String siniestroId
	*@return Map<Srtring, String> with data of account linked to policy
	*/
	@auraEnabled
    public static Map<String,String> consultaPoliza(String siniestroId) {
        final Map<String,String> mapaSalida = new Map<String,String>();
        final siniestro__c sin = [select id,MX_SB_SAC_Contrato__c from siniestro__c where id = :siniestroId limit 1];
        if(sin.MX_SB_SAC_Contrato__c!=null) {
            final contract contrato = [select id,accountid,MX_SB_SAC_NumeroPoliza__c from contract where id=:sin.MX_SB_SAC_Contrato__c limit 1];
            final account acc = [select firstname,lastname,Apellido_materno__pc,id,RFC__c from account where id=:contrato.AccountId];
            mapaSalida.put('nombre',acc.firstname);
        	mapaSalida.put('apaterno',acc.LastName);
        	mapaSalida.put('amaterno',acc.Apellido_materno__pc);
        	mapaSalida.put('rfc',acc.RFC__c);
        	mapaSalida.put('numpoliza',contrato.MX_SB_SAC_NumeroPoliza__c);
            mapaSalida.put('idContrato',contrato.id);
        } 
        return mapaSalida;
    }    
}