/*
  @File Name          : MX_SB_VTS_Carrusel_Service.cls
  @Description        : Clase Service: Componente Carrusel
  @Author             : jesusalexandro.corzo.contractor@bbva.com
  @Group              : 
  @Last Modified By   : jesusalexandro.corzo.contractor@bbva.com
  @Last Modified On   : 07/8/2020 16:56:00
  @Modification Log   : 
  Ver               Date                Author      		                    	Modification
  1.0           	07/8/2020       	jesusalexandro.corzo.contractor@bbva.com    Initial Version
*/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_Carrusel_Service {
    /** Atributo de Apoyo: S_CONT */
    Static final String S_CONT = 'Contacto';
    /** Atributo de Apoyo: S_INFB */
    Static final String S_INFB = 'Información Básica';
    /** Atributo de Apoyo: S_PERP */
    Static final String S_PERP = 'Personaliza tu plan';
    /** Atributo de Apoyo: S_DATP */
    Static final String S_DATP = 'Datos personales';
    /** Atributo de Apoyo: S_FORP */
    Static final String S_FORP = 'Forma de pago';
    /** Atributo de Apoyo: S_REVP */
    Static final String S_REVP = 'Revisa tu plan';

    /**
     * @description: Recupera los datos para el componente de Carrusel si es Lead u Opportunidad
     * @author: 	 Alexandro Corzo
     * @return: 	 Map<String,List<Object>>
     */
	public static Map<String, List<Object>> getDataComponentServ(String sModule, String sId) {
        Map<String, List<Object>> oReturn = null;
        String isLead = null;
        String isOpportunity = null;
        isLead = 'Lead';
        isOpportunity = 'Opportunity';
        oReturn = new Map<String, List<Object>>();
        if (sModule == isLead) {
            final List<Object> lstOpts = new List<Object>();
            final List<Lead> lstLeads = MX_RTL_Lead_Selector.resultQueryLead('id, Status', 'Id = \'' + sId + '\'' , true);
            lstOpts.add('isLead');
            oReturn.put('Options', lstOpts);
            oReturn.put('Records', lstLeads);
        }
        if (sModule == isOpportunity) {
            final Opportunity objOpportunity = MX_RTL_Opportunity_Selector.getOpportunity(sId, 'Id, MX_SB_VTA_SubEtapa__c, StageName, Producto__c');
            final List<Opportunity> lstOpportunitys = new List<Opportunity>();
            final List<Object> lstOpts = new List<Object>();
            lstOpportunitys.add(objOpportunity);
            lstOpts.add('isOpportunity');
            oReturn.put('Options', lstOpts);
           	oReturn.put('Records', lstOpportunitys); 
        }
        return oReturn;
    }
    
    /**
     * @description: Actualiza los datos conforme a la SubEtapa seleccionada en el componente
     * @author: 	 Alexandro Corzo
     * @return: 	 Bolean
     */
    public static Boolean setDataComponentServ(Map<String, Object> objParams) {
        String isOppo = null;
        isOppo = 'Opportunity';
        Boolean isSuccess = false;
        final String sModule = objParams.get('sModule').toString();
        if(sModule == isOppo) {
            final String sId = objParams.get('sId').toString();
            final String sSubEtapa = objParams.get('sSubEtapa').toString();
            final String sProducto = objParams.get('sProducto').toString();
            final List<Opportunity> lstOpportunitys = new List<Opportunity>();
            final Opportunity objOpportunity = new Opportunity(
                MX_SB_VTA_SubEtapa__c = sSubEtapa,
                Producto__c = sProducto,
                Id = sId
            );
            lstOpportunitys.add(objOpportunity);
            MX_RTL_Opportunity_Selector.updateResult(lstOpportunitys, false);
            isSuccess = true;
        }
        return isSuccess;
    }
    
    /**
     * @description: Recupera los valores del campo de SubEtapa para el componente
     * @author: 	 Alexandro Corzo
     * @return: 	 Map<String,List<Object>>
     */
    public static Map<String, List<Object>> obtDataSubEtapaOppoServ() {
        // Variables - Locales
        List<Object> lstOptions = null;
        List<Schema.PicklistEntry> lstPle = null;
        Schema.DescribeFieldResult schFieldResult = null;
        Map<String,List<Object>> oReturn = null;
        lstOptions = new List<Object>();
        oReturn = new Map<String, List<Object>>();
        schFieldResult = Opportunity.MX_SB_VTA_SubEtapa__c.getDescribe();
        lstPle = schFieldResult.getPicklistValues();
        for(Schema.PicklistEntry oPlist : lstPle) {
            final Map<String, String> oData = new Map<String, String>();
            if(S_CONT.equalsIgnoreCase(oPlist.getLabel()) ||
               S_INFB.equalsIgnoreCase(oPlist.getLabel()) ||
               S_PERP.equalsIgnoreCase(oPlist.getLabel()) ||
               S_DATP.equalsIgnoreCase(oPlist.getLabel()) ||
               S_FORP.equalsIgnoreCase(oPlist.getLabel()) ||
               S_REVP.equalsIgnoreCase(oPlist.getLabel())) {
            	oData.put('label', oPlist.getLabel());
            	oData.put('value', oPlist.getValue());
            	lstOptions.add(oData);
            }
        }
        oReturn.put('optSubEtapasOppo', lstOptions);
        return oReturn;
    }
}