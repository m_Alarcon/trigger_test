/**
 * @File Name          : MX_SB_VTS_CallCTIs_utility.cls
 * @Description        : Clase de utileria test para envios a Call center
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 08-09-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    20/4/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
@isTest
public class MX_SB_VTS_CallCTIs_utility { //NOSONAR

    /**@description etapa Cotizacion*/
    private final static string COTIZACION = 'Cotización';
    /**@description Nombre permiso hogar*/
    private final static string PERMISOHOGAR = 'MX_SB_VTS_Hogar';
    /**@description Nombre permiso vida*/
    private final static string PERMISOVIDA = 'MX_SB_VTS_Vida';
    /**@description Nombre permiso contratos h*/
    private final static string PERMISOHOGARC = 'MX_SB_VTS_Permisos_Contrato_Hogar';
    /**@description Nombre permiso contratos v*/
    private final static string PERMISOVIDAC = 'MX_SB_VTS_Permisos_Contrato_Vida';
    /**@description Nombre permiso contratos v*/
    private final static string CLASESVTS = 'MX_SB_VTS_Ventas';
    /**@description Nombre permiso contratos v*/
    private final static string PERMISOOBJECIONES = 'MX_SB_VTS_Objeciones';
    /**@description Nombre permiso contratos v*/
    private final static string VISTASTRACKVCIP = 'MX_SB_VTS_VistasTrackingVCIP';
    /**Final list process */
    final static Set<String> PROCESSCAT = new Set<String>{'VTS'};

    /**Response service */
    final static String BODY = '<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://201.148.35.186/ws/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:ns2="http://xml.apache.org/xml-soap" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><SOAP-ENV:Body><ns1:getCallResponse><return><item><key xsi:type="xsd:string">status</key><value xsi:type="xsd:string">ERROR</value></item><item><key xsi:type="xsd:string">calls</key><value SOAP-ENC:arrayType="ns2:Map[2]" xsi:type="SOAP-ENC:Array"><item xsi:type="ns2:Map"><item><key xsi:type="xsd:string">Fecha_Llamada</key><value xsi:type="xsd:string">2019-02-07 16:37:41</value></item><item><key xsi:type="xsd:string">Tel_Marcado</key><value xsi:type="xsd:string">0458119135196</value></item><item><key xsi:type="xsd:string">Disposicion</key><value xsi:nil="true"/></item><item><key xsi:type="xsd:string">idUser</key><value xsi:type="xsd:string">VDAD</value></item><item><key xsi:type="xsd:string">leadId</key><value xsi:type="xsd:string">00QR000000G7gW3MAJ</value></item><item><key xsi:type="xsd:string">Tel_1</key><value xsi:type="xsd:string">0458119135196</value></item><item><key xsi:type="xsd:string">Tel_2</key><value xsi:type="xsd:string"></value></item><item><key xsi:type="xsd:string">Tel_3</key><value xsi:type="xsd:string"></value></item></item><item xsi:type="ns2:Map"><item><key xsi:type="xsd:string">Fecha_Llamada</key><value xsi:type="xsd:string">2019-02-07 16:37:17</value></item><item><key xsi:type="xsd:string">Tel_Marcado</key><value xsi:type="xsd:string">0458119135196</value></item><item><key xsi:type="xsd:string">Disposicion</key><value xsi:type="xsd:string">EQUIVOCADO INCORRECTO</value></item><item><key xsi:type="xsd:string">idUser</key><value xsi:type="xsd:string">PECC891119</value></item><item><key xsi:type="xsd:string">leadId</key><value xsi:type="xsd:string">00QR000000G7gW3MAJ</value></item><item><key xsi:type="xsd:string">Tel_1</key><value xsi:type="xsd:string">0458119135196</value></item><item><key xsi:type="xsd:string">Tel_2</key><value xsi:type="xsd:string"></value></item><item><key xsi:type="xsd:string">Tel_3</key><value xsi:type="xsd:string"></value></item></item></value></item><item><key xsi:type="xsd:string">recordings</key><value SOAP-ENC:arrayType="ns2:Map[1]" xsi:type="SOAP-ENC:Array"><item xsi:type="ns2:Map"><item><key xsi:type="xsd:string">Fecha_Grabacion</key><value xsi:type="xsd:string">2019-02-07 16:37:20</value></item><item><key xsi:type="xsd:string">Grabacion</key><value xsi:type="xsd:string">ASD01013_20190207-163718_0458119135196_PECC891119</value></item><item><key xsi:type="xsd:string">Duracion</key><value xsi:type="xsd:string">3</value></item><item><key xsi:type="xsd:string">idUser</key><value xsi:type="xsd:string">PECC891119</value></item><item><key xsi:type="xsd:string">leadId</key><value xsi:type="xsd:string">00QR000000G7gW3MAJ</value></item></item></value></item></return></ns1:getCallResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>';
    /**Request service */
    final static String TXTPOST = 'POST', URLLOCAL = 'local.MXSBVTSCache',URLEXA='http://www.example.com';
    
    /**
    * @description Crea una entrada en la custom setting para el servicio de marcado smartcenter
    * @author Eduardo Hernandez Cuamatzi | 20/4/2020 
    * @return MX_SB_VTS_Generica__c registro para insertar
    **/
    public static MX_SB_VTS_Generica__c genricaSmart() {
        final MX_SB_VTS_Generica__c setting = MX_WB_TestData_cls.GeneraGenerica('ValorSmart', 'CP6');
        setting.MX_SB_VTS_TimeValue__c = '104';
        return setting;
    }

    /**
    * @description  crea una nueva familia de productos
    * @author Eduardo Hernandez Cuamatzi | 20/4/2020 
    * @param productFamName Nombre de la familia
    * @return MX_WB_FamiliaProducto__c registro para insertar
    **/
    public static MX_WB_FamiliaProducto__c newFamiliy(String productFamName) {
        return MX_WB_TestData_cls.createProductsFamily (productFamName);
    }

    /**
    * @description Nuevo producto venta
    * @author Eduardo Hernandez Cuamatzi | 20/4/2020 
    * @param productName Nombre de la familia
    * @param objFamilyPro2 Familia de productos
    * @return Product2  registro para insertar
    **/
    public static Product2 newProduct(String productName, MX_WB_FamiliaProducto__c objFamilyPro2) {
        final Product2 proHogar = MX_WB_TestData_cls.productNew (productName);
        proHogar.IsActive = true;
        proHogar.MX_WB_FamiliaProductos__c = objFamilyPro2.Id;
        proHogar.MX_SB_SAC_Proceso__c = 'VTS';
        return proHogar;
    }

    /**
    * @description Crea un nuevo proveedor
    * @author Eduardo Hernandez Cuamatzi | 20/4/2020 
    * @param name Nombre del proveedor
    * @param identificador Nombre del proveedor cti
    * @return MX_SB_VTS_ProveedoresCTI__c  registro para insertar
    **/
    public static MX_SB_VTS_ProveedoresCTI__c newProvee(String name, String identificador) {
        final MX_SB_VTS_ProveedoresCTI__c smartProv = new MX_SB_VTS_ProveedoresCTI__c();
        smartProv.Name = name;
        smartProv.MX_SB_VTS_Identificador_Proveedor__c = identificador;
        smartProv.MX_SB_VTS_TieneSegmento__c = true;
        smartProv.MX_SB_VTS_IsReadyCTI__c = true;
        return smartProv;
    }

    /**
    * @description Nueva familia de proveedores
    * @author Eduardo Hernandez Cuamatzi | 20/4/2020 
    * @param objFamilyPro2 Familia de productos
    * @param nameFamProve Nombre de la familia de productos proveedor
    * @param smartProv Registro Proveedor
    * @return MX_SB_VTS_FamliaProveedores__c  registro para insertar
    **/
    public static MX_SB_VTS_FamliaProveedores__c newFamProveedor(MX_WB_FamiliaProducto__c objFamilyPro2, String nameFamProve, MX_SB_VTS_ProveedoresCTI__c smartProv) {
        final MX_SB_VTS_FamliaProveedores__c famProSmart = new MX_SB_VTS_FamliaProveedores__c();
        famProSmart.MX_SB_VTS_Familia_de_productos__c = objFamilyPro2.Id;
        famProSmart.Name = nameFamProve;
        famProSmart.MX_SB_VTS_ProveedorCTI__c = smartProv.Id;
        return famProSmart;
    }

    /**
    * @description Crea bandeja de Hotleads
    * @author Eduardo Hernandez Cuamatzi | 20/4/2020 
    * @param valuesTray valores para la bandeja
    * @param proHogar Producto para la bandeja
    * @param smartProv Proveedor de la bandeja
    * @return MX_SB_VTS_Lead_tray__c registro para insertar
    **/
    public static MX_SB_VTS_Lead_tray__c newTrayHotlead(String[] valuesTray, Product2 proHogar, MX_SB_VTS_ProveedoresCTI__c smartProv) {
        final MX_SB_VTS_Lead_tray__c hotLeadsSmart = new MX_SB_VTS_Lead_tray__c();
        hotLeadsSmart.Name = valuesTray[0];
        hotLeadsSmart.MX_SB_VTS_Description__c = valuesTray[1];
        hotLeadsSmart.MX_SB_VTS_ID_Bandeja__c = valuesTray[2];
        hotLeadsSmart.MX_SB_VTS_Producto__c = proHogar.Id;
        hotLeadsSmart.MX_SB_VTS_ProveedorCTI__c = smartProv.Id;
        hotLeadsSmart.MX_SB_VTS_Tipo_bandeja__c = valuesTray[3];
        return hotLeadsSmart;
    }

    /**
    * @description inserta servicios Iaso para Smartcenter
    * @author Eduardo Hernandez Cuamatzi | 20/4/2020 
    * @return void 
    **/
    public static void insertIasosVoid() {
        final String[] values = new String[]{'bbvaMexSmartCenters_Crear_Carga', 'http://www.example.com', URLLOCAL};
        MX_SB_VTS_CallCTIs_utility.insertIasos(values);
        final String[] values2 = new String[]{'Authentication', 'https://validation/ok', URLLOCAL};
        MX_SB_VTS_CallCTIs_utility.insertIasos(values2);
        final String[] values3 = new String[]{'bbvaMexSmartCenter', 'https://bbvacifrado.smartcenterservicios.com/ws_salesforce_desarrollo/api/login/authenticate', URLLOCAL};
        MX_SB_VTS_CallCTIs_utility.insertIasos(values3);
    }

    /**
    * @description Inserta metadata para servicios iaso
    * @author Eduardo Hernandez Cuamatzi | 20/4/2020 
    * @param valuesIaso valores de la metadata
    * @return void inserta registro Iaso
    **/
    public static void insertIasos(String[] valuesIaso) {
        insert new iaso__GBL_Rest_Services_Url__c(Name = valuesIaso[0], iaso__Url__c = valuesIaso[1], iaso__Cache_Partition__c = valuesIaso[2]);
    }

    /**
    * @description Crea un nuevo registro de proceso programado
    * @author Eduardo Hernandez Cuamatzi | 20/4/2020 
    * @param timeSch hora de ejecución para el proceso
    * @param turno turno de ejecución
    * @param processName Nombre del proceso programado
    * @param isActive indica si el registro estara activo
    * @return ProcNotiOppNoAten__c registro para insertar
    **/
    public static ProcNotiOppNoAten__c newProcNoct(String timeSch, String turno, String processName, Boolean isActive) {
        return MX_WB_TestData_cls.createProcNotiOppNoAten(processName, timeSch, turno, isActive);
    }

    /**
    * @description 
    * @author Eduardo Hernandez Cuamatzi | 4/6/2020 
    * @param List<String> values 
    * @param String product 
    * @param String recordTypeLab 
    * @param User asesorUser 
    * @param Account accToAssign 
    * @return void 
    **/
    public static void insertBasicsVTS(List<String> values, String recordTypeLab, User asesorUser, Account accToAssign) {
        final MX_WB_FamiliaProducto__c objFamilyPro2 = MX_SB_VTS_CallCTIs_utility.newFamiliy(values[0]);
        insert objFamilyPro2;
        
        final Product2 proHogar = MX_SB_VTS_CallCTIs_utility.newProduct(values[0], objFamilyPro2);
        insert proHogar;
        
        final MX_SB_VTS_ProveedoresCTI__c smartProv = MX_SB_VTS_CallCTIs_utility.newProvee(values[1], values[1]);
        insert  smartProv;

        final MX_SB_VTS_FamliaProveedores__c famProSmart = MX_SB_VTS_CallCTIs_utility.newFamProveedor(objFamilyPro2, values[1], smartProv);
        insert famProSmart;
        final String[] valuesTray = new String[]{'HotLeads', 'BandejHot', values[6], 'HotLeads'};
        final MX_SB_VTS_Lead_tray__c hotLeadsSmart =  MX_SB_VTS_CallCTIs_utility.newTrayHotlead(valuesTray, proHogar, smartProv);
        insert hotLeadsSmart;

        final Opportunity oppHsd = MX_WB_TestData_cls.crearOportunidad(values[2], accToAssign.Id, asesorUser.Id, recordTypeLab);
        oppHsd.Producto__c = values[0];
        oppHsd.Reason__c = values[3];
        oppHsd.Reason__c = values[3];
        oppHsd.StageName = COTIZACION;
        insert oppHsd;
        final Pricebook2 prB2 = MX_WB_TestData_cls.createStandardPriceBook2();
        final Quote quoteTemp = MX_WB_TestData_cls.crearQuote(oppHsd.Id, values[4], values[5]);
        quoteTemp.Pricebook2Id= prB2.Id;
        quoteTemp.OwnerId = asesorUser.Id;
        insert quoteTemp;
        final PricebookEntry pbEntry = MX_WB_TestData_cls.priceBookEntryNew(proHogar.Id);
        insert pbEntry;
        final QuoteLineItem qltem = new QuoteLineItem(
            QuoteId=quoteTemp.Id,
            PricebookEntryId=pbEntry.Id,
            Quantity=1,
            UnitPrice=1,
            Product2Id=proHogar.Id);
        insert qltem;
        final MX_SB_VTS_Generica__c genricaSm =  genricaSmart();
        insert genricaSm;
    }

    /**
    * @description Inserta permission sets
    * @author Eduardo Hernandez Cuamatzi | 26/5/2020 
    * @param String permissionName 
    * @param Id userId id de usuario
    * @return void 
    **/
    public static void insertPermissionSet(String permissionName, Id userId) {
        final PermissionSet permiosions = [SELECT Id FROM PermissionSet WHERE Name =: permissionName];
        insert new PermissionSetAssignment(AssigneeId = userId, PermissionSetId = permiosions.Id);
    }

    /**
    * @description Inserta permission sets
    * @author Eduardo Hernandez Cuamatzi | 26/5/2020 
    * @param String permissionName 
    * @param Id userId id de usuario
    * @return void 
    **/
    public static void insertPermissionVTS(User asesorUser) {
        insertPermissionSet(PERMISOHOGAR, asesorUser.Id);
        insertPermissionSet(PERMISOVIDA, asesorUser.Id);
        insertPermissionSet(CLASESVTS, asesorUser.Id);
        insertPermissionSet(PERMISOOBJECIONES, asesorUser.Id);
        insertPermissionSet(PERMISOHOGARC, asesorUser.Id);
        insertPermissionSet(PERMISOVIDAC, asesorUser.Id);
        insertPermissionSet(VISTASTRACKVCIP, asesorUser.Id);
    }

    /**
    * @description Inserta servicios IASO
    * @author Eduardo Hernandez Cuamatzi | 4/6/2020 
    * @return void 
    **/
    public static void insertIasoVTSH() {
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'bbvaMexSmartCenters_Crear_Carga', iaso__Url__c = URLEXA, iaso__Cache_Partition__c = URLLOCAL);
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'Authentication', iaso__Url__c = 'https://validation/ok', iaso__Cache_Partition__c = URLLOCAL);
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'bbvaMexSmartCenter', iaso__Url__c = 'https://bbvacifrado.smartcenterservicios.com/ws_salesforce_desarrollo/api/login/authenticate', iaso__Cache_Partition__c = URLLOCAL);
    }

    /**
    * @description Recupera lista de oportunidades trabajadas
    * @author Eduardo Hernandez Cuamatzi | 4/6/2020 
    * @return List<Opportunity> 
    **/
    public static List<Opportunity> lstOppsTest() {
        return [Select Id, Name, MX_WB_Cupon__c, MX_WB_EnvioCTICupon__c, EnviarCTI__c, Producto__c, LeadSource, CreatedDate, Account.PersonEmail, 
        Account.PersonMobilePhone, TelefonoCliente__c,Account.Apellido_materno__pc,Account.FirstName, Account.LastName from Opportunity where Name not In ('')];
    }

    /**
    * @description Estructura generica de datos para Hogar
    * @author Eduardo Hernandez Cuamatzi | 4/6/2020 
    * @return void 
    **/
    public static void initHogarGeneric() {
        final User integration = MX_WB_TestData_cls.crearUsuario('Integration', System.Label.MX_SB_VTS_ProfileIntegration);
        insert integration;

        final User admin = MX_WB_TestData_cls.crearUsuario('Integration', System.Label.MX_SB_VTS_ProfileAdmin);
        insert admin;

        final User asesorUser = MX_WB_TestData_cls.crearUsuario('AsesorTest', 'Telemarketing VTS');
        insert asesorUser;
        
        final Account accToAssign = MX_WB_TestData_cls.crearCuenta('Donnie', 'PersonAccount');
        accToAssign.PersonEmail = 'donnie.test@test.com';
        accToAssign.OwnerId = asesorUser.Id;
        insert accToAssign;
        final List<String> lstValues = new List<String>{
            System.Label.MX_SB_VTS_Hogar,
            'Smart Center',
            'OppTelemarketing',
            'Venta',
            '999999 1 OppTest Close Opp',
            '999990',
            '72'
        };
        System.runAs(asesorUser) {
            MX_SB_VTS_CallCTIs_utility.insertBasicsVTS(lstValues,  System.Label.MX_SB_VTS_Telemarketing_LBL, asesorUser, accToAssign);
        }
    }

    /**
    * @description Inicializa registros para leadmultiservices
    * @author Eduardo Hernandez Cuamatzi | 08-07-2020 
    **/
    public static void initLeadMulServ() {
        final MX_WB_FamiliaProducto__c objFamilyPro2 = MX_SB_VTS_CallCTIs_utility.newFamiliy(System.Label.MX_SB_VTS_SeguroEstudia_LBL);
        insert objFamilyPro2;
        final Product2 proHogar = MX_SB_VTS_CallCTIs_utility.newProduct(System.Label.MX_SB_VTS_SeguroEstudia_LBL, objFamilyPro2);
        insert proHogar;
        preparateProduClp(System.Label.MX_SB_VTS_SeguroEstudia_LBL, '008');

    }

    /**
    * @description Inicializa registros para leadmultiservices
    * @author Eduardo Hernandez Cuamatzi | 08-07-2020 
    **/
    public static void initLeadMulServHSD() {
        final MX_WB_FamiliaProducto__c objFamilyHSD = MX_SB_VTS_CallCTIs_utility.newFamiliy(System.Label.MX_SB_VTS_PRODUCTO_HogarDinamico_LBL);
        insert objFamilyHSD;
        final Product2 proHSD = MX_SB_VTS_CallCTIs_utility.newProduct(System.Label.MX_SB_VTS_PRO_HogarDinamico_LBL, objFamilyHSD);
        insert proHSD;
        preparateProduClp(System.Label.MX_SB_VTS_PRO_HogarDinamico_LBL, '3002');
    }

    /**
    * @description inserta productos en cat clip
    * @author Eduardo Hernandez Cuamatzi | 08-07-2020 
    * @param String prodName nombre del producto para catalogo
    * @param String prodCod codigo producto clippert
    **/
    public static void preparateProduClp(String prodName, String prodCod) {
        final Set<String> prodCods = new Set<String>{prodCod};
        insertSacProd(prodCods, prodName, prodCod);
    }


    /**
    * @description Insertar productos clippert
    * @author Eduardo Hernandez Cuamatzi | 08-07-2020 
    * @param Set<String> catprod nombre de producto
    * @param String prodName nombre producto unico
    * @param String prodCode codigo de clippert
    **/
    public static void insertSacProd(Set<String> catprod, String prodName, String prodCode) {
        final Set<String> prodNames = new Set<String>{prodName};
        final List<Product2> prdoLst = MX_RTL_Product2_Selector.findProsByNameProces(prodNames, PROCESSCAT, true);
        final MX_SB_SAC_Catalogo_Clipert_Producto__c prodClip = new MX_SB_SAC_Catalogo_Clipert_Producto__c();
        prodClip.Name = prodName;
        prodClip.MX_SB_VTS_ProductCode__c = prodCode;
        prodClip.MX_SB_SAC_Producto__c = prdoLst[0].Id;
        prodClip.MX_SB_SAC_Activo__c = true;
        final List<MX_SB_SAC_Catalogo_Clipert_Producto__c> lstProdClip = new List<MX_SB_SAC_Catalogo_Clipert_Producto__c>{prodClip};        
        MX_RTL_Catalogo_Clipert_Product_Selector.newProductClipp(lstProdClip, true);
    }    
    
     /**
    * @description Insertar estructura para quotes
    * @author Eduardo Hernandez Cuamatzi | 03-09-2020 
    **/
    public static void initDataMulti(Map<String,String> lstValues) {
        final Account accHSD = MX_WB_TestData_cls.crearCuenta(lstValues.get('lastNameUser'), 'PersonAccount');
        accHSD.PersonEmail = lstValues.get('email') ;
        accHSD.OwnerId = lstValues.get('asesorId');
        insert accHSD;
        final Opportunity oppDinamic = MX_WB_TestData_cls.crearOportunidad(lstValues.get('oppName'), accHSD.Id, lstValues.get('asesorId'), lstValues.get('oppDevRecId'));
        oppDinamic.Producto__c = lstValues.get('productName');
        oppDinamic.Reason__c = 'Venta';
        oppDinamic.StageName = lstValues.get('stageName');
        insert oppDinamic;
        final Quote quoteTemp = MX_WB_TestData_cls.crearQuote(oppDinamic.Id, lstValues.get('quoteName'), lstValues.get('folioQuote'));
        quoteTemp.Pricebook2Id= lstValues.get('prB2Id');
        quoteTemp.OwnerId = lstValues.get('asesorId');
        insert quoteTemp;
        final List<Product2> prdoLst = MX_RTL_Product2_Selector.findProsByNameProces(new Set<String>{lstValues.get('productName')}, PROCESSCAT, true);
        final PricebookEntry pbEntry = MX_WB_TestData_cls.priceBookEntryNew(prdoLst[0].Id);
        insert pbEntry;
        final QuoteLineItem qltem = new QuoteLineItem(
            QuoteId=quoteTemp.Id,
            PricebookEntryId=pbEntry.Id,
            Quantity=1,
            UnitPrice=1,
            Product2Id=prdoLst[0].Id);
        insert qltem;
    }
}