/**
 * @File Name          : MX_SB_VTS_Lead_Trays_Selector.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernandez Cuamatzi
 * @Last Modified On   : 3/6/2020 19:41:55
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    28/5/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_SB_VTS_Lead_Trays_Selector {
    
    /**
    * @description Recupera bandeja de hotleads
    * @author Eduardo Hernandez Cuamatzi | 3/6/2020 
    * @return List<MX_SB_VTS_Lead_tray__c> Lista de bandejas hotlead
    **/
    public static List<MX_SB_VTS_Lead_tray__c> findTraysHotLeads() {
        return [Select Id,MX_SB_VTS_ProveedorCTI__c,MX_SB_VTS_ProveedorCTI__r.MX_SB_VTS_Identificador_Proveedor__c, MX_SB_VTS_ID_Bandeja__c, MX_SB_VTS_Tipo_bandeja__c from 
        MX_SB_VTS_Lead_tray__c where MX_SB_VTS_Tipo_bandeja__c =: System.Label.MX_SB_VTS_HotLeads WITH SECURITY_ENFORCED];
    }
}