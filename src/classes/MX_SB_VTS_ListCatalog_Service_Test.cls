@isTest
/**
* @FileName          : MX_SB_VTS_ListCatalog_Service_Test
* @description       : Test class from MX_SB_VTS_ListCatalog_Service
* @Author            : Marco Antonio Cruz Barboza
* @last modified on  : 08-05-2020
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   08-05-2020   Marco Antonio Cruz Barboza           Initial Version
* 1.1   13-01-2021   Marco Antonio Cruz Barbosa           Ajustes a la clase para consumo ASO
**/
public class MX_SB_VTS_ListCatalog_Service_Test {
    /** User name testing */
    final static String TESTINGUSR = 'UserTest';
    /** Account Name testing*/
    final static String TESTACCOUNT = 'Test Account';
    /** Opportunity Name Testintg */
    final static String TESTOPPORT = 'Test Opportunity';
    /**URL Mock Test*/
    Final static String TESTURL = 'http://www.example.com';
    /**URL Mock Test*/
    Final static String POSTALCD = '07469';
    /**URL Mock Test*/
    Final static String SERVICEPAR = 'CO';
    /**Tsec Mock*/
    Final static Map<String,String> TSECMOCK = new Map<String,String> {'tsec'=>'1234456789'};
        
    /* 
    @Method: setupTestM
    @Description: create test data set
    */
    @TestSetup
    static void setupTestM() {
        final User usuTest = MX_WB_TestData_cls.crearUsuario(TESTINGUSR, System.Label.MX_SB_VTS_ProfileAdmin);
        System.runAs(usuTest) {
            final Account cuenTst = MX_WB_TestData_cls.crearCuenta(TESTACCOUNT, System.Label.MX_SB_VTS_PersonRecord);
            cuenTst.PersonEmail = 'pruebaVts@mxvts.com';
            insert cuenTst;
            final Opportunity oporTst = MX_WB_TestData_cls.crearOportunidad(TESTOPPORT, cuenTst.Id, usuTest.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            oporTst.LeadSource = 'Call me back';
            oporTst.Producto__c = 'Hogar';
            oporTst.Reason__c = 'Venta';
            oporTst.StageName = 'Cotizacion';
            insert oporTst;
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'getListInsuraceCustomerCatalog', iaso__Url__c = TESTURL, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
            
        }
    }
    
    /*
    @Method: getZipCodeTest
    @Description: return Map with a List of objects from a service
	@param String ZipCode
    */
    @isTest
    static void getZipCodeTest() {
		Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, 'Complete', '{"iCatalogItem": {"suburb": [{"neighborhood": {"id": "0196","name": "UNIDAD HABITACIONAL"},"county": {"id": "005","name": "GUSTAVO A. MADERO"},"city": {"id": "01","name": "CIUDAD DE MÉXICO"},"state": {"id": "09","name": "CIUDAD DE MEXICO"}}]}}', TSECMOCK); 
        iaso.GBL_Mock.setMock(mockCallout);
        test.startTest();
            Final Map<String,List<Object>> testResponse = MX_SB_VTS_ListCatalog_Service.srchZipCode(SERVICEPAR, POSTALCD);
            System.assertNotEquals(testResponse, null,'El mapa esta retornando');
        test.stopTest();
    }
}