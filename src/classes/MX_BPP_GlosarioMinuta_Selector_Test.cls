/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_GlosarioMinuta_Selector_Test
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2021-02-19
* @Description 	Test Class for MX_BPP_GlosarioMinuta_Selector
* @Changes
* 2021-03-03	Edmundo Zacarias		New method added: getAllRecordsTest
*/
@isTest
public class MX_BPP_GlosarioMinuta_Selector_Test {
    
	/**
    * @Description 	Test Method that verifies constructor method is created correct
    * @Return 		NA
    **/
	@isTest
    static void constructorTest() {
        final MX_BPP_GlosarioMinuta_Selector tipoMSelector = new MX_BPP_GlosarioMinuta_Selector();
        System.assertNotEquals(null, tipoMSelector, 'Error on constructor');
    }
    
    /**
    * @Description 	Test for which covers method getRecordsByDevName() from MX_BPP_GlosarioMinuta_Selector
    * @Return 		NA
    **/
	@isTest
    static void getRecordsByDevNameTest() {
        final Set<String> setDevName = new Set<String>();
        setDevName.add('NombreCliente');
    	final List<MX_BPP_GlosarioMinuta__mdt> listRecords = MX_BPP_GlosarioMinuta_Selector.getRecordsByDevName('Id, DeveloperName', setDevName);
        System.assertNotEquals(null, listRecords, 'Error on getRecordsByDevName');
    }
    
    /**
    * @Description 	Test which covers method getAllRecords() from MX_BPP_GlosarioMinuta_Selector
    * @Return 		NA
    **/
	@isTest
    static void getAllRecordsTest() {
    	final List<MX_BPP_GlosarioMinuta__mdt> listRecords = MX_BPP_GlosarioMinuta_Selector.getAllRecords('Id, DeveloperName');
        System.assertNotEquals(null, listRecords, 'Error on getAllRecords');
    }
}