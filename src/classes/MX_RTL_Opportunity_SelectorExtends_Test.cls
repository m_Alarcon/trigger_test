/**
* @File Name          : public class MX_RTL_Opportunity_Selector.cls
* @Author             : Gerardo Mendoza Aguilar
* @Group              :
* @Last Modified By   : Arsenio.perez.lopez.contractor@bbva.com
* @Last Modified On   : 02-11-2021
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      28/10/2020           Gerardo Mendoza Aguilar          Initial Version
**/
@isTest
public class MX_RTL_Opportunity_SelectorExtends_Test {

    /**
   * @Method Data Setup
   * @Description Method para preparar datos de prueba
   * @return void
   **/
   @testSetup
   public static void  data() {
		MX_SB_PS_OpportunityTrigger_test.makeData();
   }
   /**
   * @description
   * @author Gerardo Mendoza
   * @return void
   **/ 
   @isTest 
   public static void upsrtOppListTest() {
       Test.startTest();
       Final Id IdRecordType = MX_RTL_CampaignMember_Selector.getIdRecordTypeParam('MX_SB_COB_Cotizador_RS_Hospital');
       Final Id acctId = [Select Id from Account Limit 1].Id;
       Final List<Opportunity> oppList = new List<Opportunity>();
       Final Opportunity opp = new Opportunity( CloseDate =  Date.today() , Name = 'Opp Test', StageName = System.Label.MX_SB_PS_Etapa1, AccountId = acctId, LeadSource = 'Outbound', Plan__c = 'Individual', Producto__c = 'Respaldo Seguro Para Hospitalización', RecordTypeId  = IdRecordType);
       oppList.add(opp);
       MX_RTL_Opportunity_Selector.upsrtOppList(oppList);
       system.assert(!oppList.isEmpty(), 'Oportunidad creada');
       Test.stopTest();
   }
       /**
    * @description 
    * @author Arsenio.perez.lopez.contractor@bbva.com | 11-18-2020 
    **/
    @isTest
    public static void clonOppTest() {
        Test.startTest();
        final Id listOppSel = [SELECT Id FROM Opportunity LIMIT 1].Id;
        final Set<Id> adtos = new Set<Id>();
        adtos.add(listOppSel);
        final List<Opportunity> clonOpp =MX_RTL_Opportunity_Selector.clonOpp(adtos,'Name',false,false,false,false);
        System.assertNotEquals(listOppSel, clonOpp[0].Id);
        Test.stopTest();
    }
    /**
    * @description 
    * @author Arsenio.perez.lopez.contractor@bbva.com | 11-18-2020 
    **/
    @isTest
    static void testQryOppoNCond() {
    	Test.startTest();
        final List<Opportunity> lstOpp = MX_RTL_Opportunity_Selector.resultQueryOppo('Id, Name', 'Name in: iynList', new Set<String>{'Opp1'} );
        Test.stopTest();
        System.assert(!lstOpp.isEmpty(),'Se obtuvieron los datos');
    }
}