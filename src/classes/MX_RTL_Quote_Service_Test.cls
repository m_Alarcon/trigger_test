@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_RTL_Quote_Service_Test
* Autor: Juan Carlos Benitez Herrera
* Proyecto: SB Presuscritos - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_RTL_Quote_Service

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         09/07/2020    Juan Carlos Benitez Herrera              Creación
* 1.1         19/08/2020    Omar Gonzalez Rubio              		 Actualizacion se agregan nuevas funciones
* -----------------------------------------------------------------------------------------------
*/
public class MX_RTL_Quote_Service_Test {
	/** Nombre de usuario Test */
    final static String NAME = 'Denisse';
    /** Apellido Paterno */
    Final static STRING LNAME= 'MB';
    /** variable que almacena un folio de cotizacion */
    final static String FOLIOC = '556432';
    /** variable que almacena Id cotiza */
    final static String IDCOTIZA = '009556432';
    /* Variable que almacena el producto */
    final static String PROD2 = 'Auto Seguro Dinámico';
    
    @TestSetup
    static void createData() {
        MX_SB_PS_OpportunityTrigger_test.makeData();
            final Account accTst = [Select Id from Account limit 1];
            accTst.FirstName=NAME;
            accTst.Tipo_Persona__c='Física';
            accTst.LastName=LNAME;
            update accTst;
            Final Opportunity oppTst = [Select Id from Opportunity where AccountId=:accTst.Id limit 1];
            oppTst.Name= NAME;
            oppTst.StageName=System.Label.MX_SB_PS_Etapa1;
            update oppTst;
            Final Quote quotData= [Select Id from Quote where OpportunityId=:oppTst.Id limit 1 ];
            quotData.Name=NAME;
            quotData.MX_SB_VTS_Folio_Cotizacion__c=FOLIOC;
            quotData.MX_ASD_PCI_ResponseCode__c='""';
            quotData.MX_ASD_PCI_IdCotiza__c = IDCOTIZA;
            update quotData;
        }
    @isTest
    static void testgetUpsquot() {
        Final Opportunity oppId =[SELECT Id from Opportunity where Name =:'Denisse'];
        Final Quote quoteObj= [SELECT Id, OpportunityId from Quote where OpportunityId=:oppId.Id Limit 1];
        test.startTest();
        	MX_RTL_Quote_Service.getUpsquot(quoteObj);
                system.assert(true,'Se ha actualizado una cotizacion existente');
        test.stopTest();
    }
    @isTest
    static void quoteDataService() {
        Test.startTest();
        final List<Quote> cots = MX_RTL_Quote_Service.getQuoteData(FOLIOC,PROD2);
        Test.stopTest();
        System.assertEquals(cots.size(), 1, 'SUCCESS');
    }
    @isTest
    static void testupsrtQuote() {
        Final Quote quotIvr =[SELECT Id, Name from Quote where MX_SB_VTS_Folio_Cotizacion__c=:FOLIOC limit 1];
        test.startTest();
        	MX_RTL_Quote_Service.upserQuote(quotIvr);
		system.assert(true,'Se ha actualizado una cotizacion existente');
        test.stopTest();
    }
    @isTest
    static void quoteResponseIvr() {
        Test.startTest();
        final List<Quote> cotRes = MX_RTL_Quote_Service.getResponseIvr(FOLIOC,IDCOTIZA);
        Test.stopTest();
        System.assertEquals(cotRes.size(), 1, 'SUCCESS');
    }
    @isTest
    static void quoteRetrieve() {
        final Quote quoteR = [SELECT Id, OpportunityId FROM Quote WHERE MX_SB_VTS_Folio_Cotizacion__c =: FOLIOC];
        Test.startTest();
        final Quote quoteRet = MX_RTL_Quote_Service.getRetriveQuote(quoteR.OpportunityId,FOLIOC);
        Test.stopTest();
        System.assertEquals(quoteRet.Id, quoteR.Id, 'SUCCESS');
    }
    @isTest
    static void getResIvr() {
        Test.startTest();
        MX_RTL_Quote_Service.getResIvr(FOLIOC,IDCOTIZA);
        system.assert(true,'Registro actualizado');
        Test.stopTest();
    }
    @isTest
    static void getResIvrTwo() {
        final Quote quoteR = [SELECT Id, OpportunityId,MX_ASD_PCI_ResponseCode__c FROM Quote WHERE MX_SB_VTS_Folio_Cotizacion__c =: FOLIOC];
        quoteR.MX_ASD_PCI_ResponseCode__c = '3003';
        quoteR.MX_ASD_PCI_MsjAsesor__c = 'Error';
        update quoteR;
        Test.startTest();
        MX_RTL_Quote_Service.getResIvr(FOLIOC,IDCOTIZA);
        system.assert(true,'Registro actualizado');
        Test.stopTest();
    }
}