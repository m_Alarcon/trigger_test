/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPyP_Opportunity_Service
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-05-21
* @Description 	Service Layer for Opportunity
* @Changes
*  
*/
public with sharing class MX_BPyP_Opportunity_Service {
    
    /** String RecordTypeDeveloperName Opportunity */
    static final String RTOPP = 'MX_BPP_RedBpyp';
    
    /*Contructor clase MX_BPyP_Opportunity_Service */
    private MX_BPyP_Opportunity_Service() {}
    
    /**
    * @Description 	Check if status update is valid, otherwise it throws error
    * @Param		oppItem <Trigger.new>, oldMap <Trigger.oldMap>
    * @Return 		NA
    **/
    public static void checkStatusUpdate (List<Opportunity> newOppList, Map<Id, Opportunity> oldOppMap) {
        final Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(RTOPP).getRecordTypeId();
        Boolean isValid;
        for (Opportunity newOpp : newOppList) {
            final Opportunity oldOpp = oldOppMap.get(newOpp.Id);
            if (newOpp.RecordTypeId.equals(recordTypeId) && newOpp.StageName <> oldOpp.StageName) {
                isValid = MX_BPyP_Opportunity_Service_Helper.validPrevNextStatus(newOpp, newOpp.StageName, oldOpp.StageName);
                if(isValid == false) {
                    newOpp.addError(System.label.MX_BPyP_StageUpd_NoValid);
                }
            }
        }
    }

}