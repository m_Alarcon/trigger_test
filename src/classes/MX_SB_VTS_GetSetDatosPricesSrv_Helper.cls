/**
 * @File Name          : MX_SB_VTS_GetSetDatosPricesSrv_Helper.cls
 * @Description        : Clases de Apoyo Servicio - Prices - Cotizador Hogar
 * @Author             : Alexandro Corzo
 * @Group              : 
 * @Last Modified By   : Alexandro Corzo
 * @Last Modified On   : 23-02-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0       23/02/2021      Alexandro Corzo        Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public class MX_SB_VTS_GetSetDatosPricesSrv_Helper {
    /**Variable local para string  INSURED_AMOUNT*/
    private final static String INSUREDAMOUNG = 'INSURED_AMOUNT';

    /**Variable local para string  YEARLY_PREMIUM*/
    private final static String YEARLYPREMIUM = 'YEARLY_PREMIUM';
    
    /**
     * @description : Actualiza los registros de coberturas relacionadas
     * 				: en la Quote
     * @author      : Alexandro Corzo
     * @return      : Boolean isSuccess
     */
    public static Boolean fillUpdtDCov(String strRspJSON, String strIdQuote) {
        boolean isSuccess = false;
        try {
        	final String strConvJSON = strRspJSON.replaceAll('id', 'iddata').replaceAll('currency','currencydata');
        	final MX_SB_VTS_wrpCalculateQuotePrices objWrapperRsp = (MX_SB_VTS_wrpCalculateQuotePrices) JSON.deserialize(strConvJSON, MX_SB_VTS_wrpCalculateQuotePrices.class);
        	final List<Cobertura__c> lstCovQuo = fillQuoteCovs(strIdQuote);
        	final Set<Cobertura__c> lstCovsUpd = fillObjCovers(objWrapperRsp, lstCovQuo);
            final List<Cobertura__c> lstCovers = new List<Cobertura__c>();
            lstCovers.addAll(lstCovsUpd);
        	MX_RTL_Cobertura_Selector.upsertCoverages(lstCovers);
            isSuccess = true;
        } catch(Exception e) {
            isSuccess = false;
        }
        return isSuccess;
    }
    
    /**
     * @description : Otiene las coberturas actualmente registradas
     * 				: en la Quote
     * @author      : Alexandro Corzo
     * @return      : List<Cobertura__c> lstCoberturas
     */
    public static List<Cobertura__c> fillQuoteCovs(String strIdQuote) {
        final Set<Id> objSetId = new Set<Id>{strIdQuote};
        return MX_RTL_Cobertura_Selector.findCoverByQuote('Id, MX_SB_VTS_RelatedQuote__c, MX_SB_VTS_CodeTrade__c, MX_SB_VTS_TradeValue__c, MX_SB_VTS_CategoryCode__c, MX_SB_VTS_GoodTypeCode__c, MX_SB_VTS_CoverageCode__c, MX_SB_VTS_PayList__c,  MX_SB_VTS_CoversAmount__c, MX_SB_MLT_SumaAsegurada__c, MX_SB_MLT_Estatus__c, MX_SB_VTS_RelatedQuote__r.Name',' AND RecordType.DeveloperName = \'MX_SB_VTS_CoberturasASO\'', objSetId);
    }
    
    /**
     * @description : Obtiene la lista de coberturas y los montos que seran
     * 				: actualizados en Coberturas__c
     * @author      : Alexandro Corzo
     * @return      : List<Cobertura__c> lstCovsUpd
     */
    public static Set<Cobertura__c> fillObjCovers(MX_SB_VTS_wrpCalculateQuotePrices objWrapperRsp, List<Cobertura__c> lstCovQuo) {
        final Set<Cobertura__c> lstCovsUpd = new Set<Cobertura__c>();
        for(MX_SB_VTS_wrpCalculateQuotePrices.trades oRowTrades : objWrapperRsp.data.trades) {
            if(oRowTrades.iddata != null) {
            	final String sTradeID = oRowTrades.iddata;
                for(MX_SB_VTS_wrpCalculateQuotePrices.categories oRowCat : oRowTrades.categories) {
                    final String sCateID = oRowCat.iddata;
                    for(MX_SB_VTS_wrpCalculateQuotePrices.goodTypes oRowGT : oRowCat.goodTypes) {
						final String sGoodTypeID = oRowGT.iddata;                        
                        for(MX_SB_VTS_wrpCalculateQuotePrices.coverages oRowCov : oRowGT.coverages) {
                            final String sCoversID = oRowCov.iddata;
                            final Map<String, String> mDCovDet = new Map<String, String>();
                            final Map<String, String> mDatAmount = fillPreAmo(oRowCov);
                            mDCovDet.put('TradeID', sTradeID);
                            mDCovDet.put('CategID', sCateID);
                            mDCovDet.put('GoodTypeID', sGoodTypeID);
                            mDCovDet.put('CoversID', sCoversID);
                            mDCovDet.put('INS_AMOUNT', mDatAmount.get(INSUREDAMOUNG));
                            mDCovDet.put('YEA_PREMIUM', mDatAmount.get(YEARLYPREMIUM));
                            lstCovsUpd.add(obtCovNew(mDCovDet,lstCovQuo));
                        }
                    }
                }
            }
        }
        return lstCovsUpd;
    }
    
    /**
     * @description : Realiza la busqueda de la cobertura en listado y actualiza los valores
     * 				: en un nuevo objeto Cobertura__c para su actualización
     * @author      : Alexandro Corzo
     * @return      : Cobertura__c objCovUpd
     */
    public static Cobertura__c obtCovNew(Map<String,String> mDCovDet, List<Cobertura__c> lstCovQuo) {
        final Cobertura__c objCovUpd = new Cobertura__c();
        for(Cobertura__c objCov : lstCovQuo) {
            if(objCov.MX_SB_VTS_TradeValue__c.equals(mDCovDet.get('TradeID')) && 
               objCov.MX_SB_VTS_CategoryCode__c.equals(mDCovDet.get('CategID')) &&
               objCov.MX_SB_VTS_GoodTypeCode__c.equals(mDCovDet.get('GoodTypeID')) && 
               objCov.MX_SB_VTS_CoverageCode__c.equals(mDCovDet.get('CoversID'))) {
                    objCovUpd.Id = objCov.Id;
             		objCovUpd.MX_SB_VTS_CoversAmount__c = mDCovDet.get('YEA_PREMIUM');
                    objCovUpd.MX_SB_MLT_SumaAsegurada__c = Decimal.valueOf(mDCovDet.get('INS_AMOUNT'));
                    break;
               }
        }
        return objCovUpd;
    }
    
    /**
     * @description : Obtiene los montos contenidos en el JSON que van
     * 				: a ser actualizados en las coberturas asociadas a la Quote
     * @author      : Alexandro Corzo
     * @return      : Map<String, String> mDataPrices
     */
    public static Map<String, String> fillPreAmo(MX_SB_VTS_wrpCalculateQuotePrices.coverages oRowCov) {
        final Map<String, String> mDataPrices = new Map<String, String>();
        for(MX_SB_VTS_wrpCalculateQuotePrices.premiums oRowPrem : oRowCov.premiums) {
            if(INSUREDAMOUNG.equals(oRowPrem.iddata)) {
           		mDataPrices.put(INSUREDAMOUNG, obtAmountsDat(oRowPrem,INSUREDAMOUNG));
            }
            if(YEARLYPREMIUM.equals(oRowPrem.iddata)) {
                mDataPrices.put(YEARLYPREMIUM, obtAmountsDat(oRowPrem,YEARLYPREMIUM));
            }
        }
        return mDataPrices;
    }
    
    /**
     * @description : Realiza la busqueda del monto dentro del JSON conforme
     * 				: a las etiquetas: INSURED_AMOUNT, YEARLY_PREMIUM 
     * @author      : Alexandro Corzo
     * @return      : String strAmount
     */
    public static String obtAmountsDat(MX_SB_VTS_wrpCalculateQuotePrices.premiums oRowPrem, String strType) {
        String strAmount = '0';
        for(MX_SB_VTS_wrpCalculateQuotePrices.amounts oRowAmonts : oRowPrem.amounts) {
            switch on strType {
                when 'INSURED_AMOUNT','YEARLY_PREMIUM' {
                    strAmount = String.valueOf(oRowAmonts.amount);
                    break;
                }
            }
        }
        return strAmount;
    }
}