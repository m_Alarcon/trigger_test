/**
 * @File Name          : MX_SB_MLT_Timer_Ctrl.cls
 * @Description        :
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 5/26/2020, 06:00:03 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/25/2020   Juan Carlos Benitez          Initial Version
**/
public with sharing class MX_SB_MLT_Timer_Ctrl {
    /* UsesSingleton
    */
        @SuppressWarnings('sf:UseSingleton')
        private MX_SB_MLT_Timer_Ctrl() {}

    /** list of records to be retrieved */
        final static List<Task> TASK_DATA = new List<Task>();
    /** list of records to be retrieved */
        final static List<Siniestro__History> SINHIST_DATA = new List<Siniestro__History>();

    /**
    * @description
    * @author Juan Carlos Benitez | 5/25/2020
    * @param recId
    * @return List<Task>
    **/
    @AuraEnabled
		public static list<Task> fetchSinCrtDate(Id recId) {
            if(String.isNotBlank(recId)) {
                final Set<Id> ids = new Set<Id>{recId};
             	TASK_DATA.addAll(MX_RTL_Task_Service.getTaskData(ids));
            	}
            return TASK_DATA;
    }
       /**
    * @description
    * @author Juan Carlos Benitez | 5/25/2020
    * @param recId
    * @return List<Task>
    **/
    @AuraEnabled
    public static List<Siniestro__History> fetchSinLastMDate(string recId) {
         try {
            if(String.isNotBlank(recId)) {
                final Set<Id> ids = new Set<Id>{recId};
             	SINHIST_DATA.addAll(MX_RTL_Siniestro_History_Service.getSinHData(ids));
            	}
             } catch(QueryException qEx) {
                 throw new AuraHandledException(System.Label.MX_WB_LG_ErrorBack + qEx);
             }
            return SINHIST_DATA;
    }
}