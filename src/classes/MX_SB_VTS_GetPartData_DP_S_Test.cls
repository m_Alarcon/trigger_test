/**
* @description       : 
* @author            : Diego Olvera
* @group             : 
* @last modified on  : 10-21-2020
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   10-21-2020   Diego Olvera   Initial Version
* 1.1   12-01-2020   Diego Olvera   Ajustes a clase consumo ASO
**/
@isTest
public class MX_SB_VTS_GetPartData_DP_S_Test {
    /*User name test*/
    private final static String USERTEST = 'ASESORDATAPART';
    /*Account name test*/
    private final static String ACCOTEST = 'CUENTADATAPART';
    /*URL test*/
    private final static String URLTEST = 'http://www.ulrsampleDP2.com';
    /*Product code*/
    private static final String PRODUCTCODES = '3015';
    /*Alliance code*/
    private static final String ALLIANCECODES = 'FHSRT014';
    
    /* 
@Method: setupTest
@Description: create test data set
*/
    @TestSetup
    static void testDatosPart() {
        final User tstDpUserTest = MX_WB_TestData_cls.crearUsuario(USERTEST, 'System Administrator');
        insert tstDpUserTest;
        
        System.runAs(tstDpUserTest) {
            
            final Account testDpAcc = MX_WB_TestData_cls.crearCuenta(ACCOTEST, System.Label.MX_SB_VTS_PersonRecord);
            testDpAcc.PersonEmail = 'pruebaVts@mxvts.com';
            insert testDpAcc;
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'getParticularData', iaso__Url__c = URLTEST, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'getGTServicesSF', iaso__Url__c = URLTEST, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
        }        
    }
    
    /* 
@Method: getStatusOTPTest
@Description: test method to get the status code from web service
*/
    @isTest
    static void  getDatosPartTest() {
        final User sampleUsr = [SELECT Id, Name FROM User WHERE LastName =: USERTEST];
        System.runAs(sampleUsr) {
            final Map<String, String> headersMockTest = new Map<String, String>();
            headersMockTest.put('tsec', '845697844');
            final MX_WB_Mock mockCallout = new MX_WB_Mock(200, 'Complete', '{}', headersMockTest); 
            iaso.GBL_Mock.setMock(mockCallout);
            test.startTest();
            MX_SB_VTS_GetPartData_DP_Service.dptListCarInsCatSrvDp(PRODUCTCODES, ALLIANCECODES);  
            test.stopTest();
            System.assertNotEquals('No entro', null,'No recuperó nada datos');
        }
    }
}