/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 09-23-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   09-16-2020   Eduardo Hernández Cuamatzi   Initial Version
**/
@SuppressWarnings('sf:AvoidGlobalModifier, sf:UseSingleton')
global with sharing class MX_SB_RTL_PureCloudExtends_Ctrl implements purecloud.CTIExtension.SaveLog {

    /**
    * @description Override función de guardado de log
    * @author Eduardo Hernandez Cuamatzi | 10-02-2020 
    * @param String data Data de log de pure cloud
    * @return String Id de registro trabajado
    **/
    public static String onSaveLog(String data) {
        final Map<String, Object> saveLogData = (Map<String, Object>) JSON.deserializeUntyped(data);
        final String interactionType = saveLogData.get('eventName').toString();
        String recordUpdate = '';
        switch on interactionType {
            when 'interactionChanged' {
                WB_CrearLog_cls.fnCrearLog(data, 'PureCloudIntChang', false);
                recordUpdate = MX_SB_RTL_PureCloudExtends_Service.retriveLogId(saveLogData);
            }
        }
        
        return recordUpdate;
    }
}