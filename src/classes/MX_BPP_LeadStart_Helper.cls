/**
* @File Name          : MX_BPP_LeadStart_Helper.cls
* @Description        :
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 23/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      23/06/2020        Gabriel García Rojas              Initial Version
**/
public class MX_BPP_LeadStart_Helper {

     /**Constructor */
    @TestVisible
    private MX_BPP_LeadStart_Helper() { }

    /*car method temColorGet*/
    final static Integer NUMBCOL = 25;

    /**
    * @description Get Color for chart
    * @author Gabriel Garcia Rojas
    * @return List<String>
    **/
    public static List<String> tempColorGet(Set<String> setStatus) {
        final List<String> tempColor = new List<String>();
        final List<Integer> red = BPyP_OppRep_Utilities.genRedBBVA();
		final List<Integer> green = BPyP_OppRep_Utilities.genGreenBBVA();
		final List<Integer> blue = BPyP_OppRep_Utilities.genBlueBBVA();
        String colortemp = '';
        for(Integer idx = 0; idx < setStatus.size(); idx++) {
            colortemp = 'rgb('+red[Math.mod(idx,NUMBCOL)]+', '+green[Math.mod(idx,NUMBCOL)]+', '+blue[Math.mod(idx,NUMBCOL)]+')';
            tempColor.add(colortemp);
        }
        tempColor.add('rgb');
        return tempColor;
    }

    /**
    * @description Get CampaingMember list by list of Leads
    * @author Gabriel Garcia Rojas
    * @return List<CampaignMember>
    **/
    public static List<CampaignMember> fetchCampaingMember(List<Lead> listLeads) {
        final List<Id> listIdLeads = new List<Id>();
        for(Lead candidato: listLeads) {
        	listIdLeads.add(candidato.Id);
        }
        return MX_RTL_CampaignMember_Selector.getListCampignMembersByLeadIds(listIdLeads);
    }

    /**
    * @description Get info Lead
    * @author Gabriel Garcia Rojas
    * @return List<Lead>
    **/
	public static List<Lead> infoLeads(List<String> paramsLead, List<String> paramsUser, List<String> paramsDate, String queryFields, String limite) {
        final List<String> listClauseLead = new List<String>{' AND Status= ', ' AND LeadSource= '};
        final List<String> listClausesUser = new List<String>{' AND BPyP_ls_NombreSucursal__c= ', ' AND Divisi_n__c= ', 'AND Name= '};
        String clauseDate = '';

        if(String.isNotBlank(paramsDate[0]) && String.isNotBlank(paramsDate[1])) {
            clauseDate = ' WHERE MX_LeadEndDate__c >= ' + paramsDate[0] + ' AND MX_LeadEndDate__c <= ' + paramsDate[1];
        } else if (String.isNotBlank(paramsDate[0]) && String.isBlank(paramsDate[1])) {
        	clauseDate = ' WHERE MX_LeadEndDate__c >= ' + paramsDate[0];
        } else if (String.isBlank(paramsDate[0]) && String.isNotBlank(paramsDate[1])) {
        	clauseDate = ' WHERE MX_LeadEndDate__c <=' + paramsDate[1];
        }

        final String clauseLead = clausefilter(paramsLead, listClauseLead);
        final String clauseUser = clausefilter(paramsUser, listClausesUser);
        final String whereQuery = ' OwnerId IN '+
            				'(Select id from User where VP_ls_Banca__c=\'Red BPyP\' AND (NOT (BPyP_ls_NombreSucursal__c =\'\' )) ' +
            				' AND (NOT (Divisi_n__c = \'\' )) AND IsActive = true AND Profile.Name = \'BPyP Estandar\' '+ clauseUser  +' ) '+
            				clauseLead +
            				' AND Id IN (SELECT LeadId FROM CampaignMember ' + clauseDate +') ' + limite;

        return MX_RTL_Lead_Selector.resultQueryLead(queryFields, whereQuery, true);
    }

    /**
    * @description fetchclause for query
    * @author Gabriel Garcia Rojas
    * @return String
    **/
    public static String clausefilter(List<String> parameters, List<String> listClauses) {
        String filter = '';
        for(Integer i = 0; i < parameters.size(); i++) {
            if(String.isNotBlank(parameters[i])) {
                filter += ' ' + listClauses[i] + ' \'' + parameters[i] + '\'';
            }
        }
        return filter;
    }

    /**
    * @description return Total of record by Division, Oficina or User
    * @author Gabriel Garcia Rojas
    * @return Map<String, Integer>
    **/
    public static Map<String, Integer> resultLeadAssignToUse(List<Lead> lstLead, Map<String, User> usrN, String expression, Set<String> lsLabels, Set<String> lsTyOpp) {
        final Map<String, Integer> nwAgg = new Map<String, Integer>();
        for(Lead vrLd : lstLead) {
            if(usrN.containsKey(vrLd.OwnerId)) {
                switch on expression {
                    when 'division' {
                        if(nwAgg.get(vrLd.status + usrN.get(vrLd.OwnerId).Divisi_n__c) ==NULL ) {
                            nwAgg.put(vrLd.status+usrN.get(vrLd.OwnerId).Divisi_n__c,1);
                            lsLabels.add(vrLd.status);
                            lsTyOpp.add(usrN.get(vrLd.OwnerId).Divisi_n__c);
                        } else {
                            Integer aux= nwAgg.get(vrLd.status + usrN.get(vrLd.OwnerId).Divisi_n__c);
                            aux++;
                            nwAgg.put(vrLd.status+usrN.get(vrLd.OwnerId).Divisi_n__c,aux);
                        }
                    }
                    when 'oficina' {
                        if(nwAgg.get(vrLd.status + usrN.get(vrLd.OwnerId).BPyP_ls_NombreSucursal__c) ==NULL ) {
                            nwAgg.put(vrLd.status+usrN.get(vrLd.OwnerId).BPyP_ls_NombreSucursal__c,1);
                            lsLabels.add(vrLd.status);
                            lsTyOpp.add(usrN.get(vrLd.OwnerId).BPyP_ls_NombreSucursal__c);
                        } else {
                            Integer aux= nwAgg.get(vrLd.status + usrN.get(vrLd.OwnerId).BPyP_ls_NombreSucursal__c);
                            aux++;
                            nwAgg.put(vrLd.status+usrN.get(vrLd.OwnerId).BPyP_ls_NombreSucursal__c,aux);
                        }
                    }
                    when else {
                        if(nwAgg.get(vrLd.status + usrN.get(vrLd.OwnerId).Name) ==NULL ) {
                            nwAgg.put(vrLd.status+usrN.get(vrLd.OwnerId).Name,1);
                            lsLabels.add(vrLd.status);
                            lsTyOpp.add(usrN.get(vrLd.OwnerId).Name);
                        } else {
                            Integer aux= nwAgg.get(vrLd.status + usrN.get(vrLd.OwnerId).Name);
                            aux++;
                            nwAgg.put(vrLd.status+usrN.get(vrLd.OwnerId).Name,aux);
                        }
                    }
                }
            }
        }
        return nwAgg;
    }

    /**
    * @description Order info Bank
    * @author Gabriel Garcia Rojas
    * @return void
    **/
    public static void infoByBanquero(MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper infoList, User usuarioActual, List<User> listUser) {
        infoList.setDivisiones.add(usuarioActual.DivisionFormula__c);
        infoList.mapSucursales.put(usuarioActual.DivisionFormula__c, new Set<String>{usuarioActual.BPyP_ls_NombreSucursal__c});
        for(User userBanquero : listUser) {
            if(infoList.maplistUserName.containsKey(UserBanquero.DivisionFormula__c + userBanquero.BPyP_ls_NombreSucursal__c)) {
                infoList.maplistUserName.get(UserBanquero.DivisionFormula__c + userBanquero.BPyP_ls_NombreSucursal__c).add(userBanquero.Name);
            } else {
                infoList.maplistUserName.put(UserBanquero.DivisionFormula__c + userBanquero.BPyP_ls_NombreSucursal__c, new List<String>{userBanquero.Name});
            }
        }
    }

    /**
    * @description Order info DO
    * @author Gabriel Garcia Rojas
    * @return void
    **/
    public static void infoByDirectorDivisional(MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper infoList, User usuarioActual, List<User> listUser) {
        infoList.setDivisiones.add(usuarioActual.DivisionFormula__c);
        for(User userBanquero : listUser) {
            if(infoList.maplistUserName.containsKey(userBanquero.DivisionFormula__c + userBanquero.BPyP_ls_NombreSucursal__c)) {
                infoList.maplistUserName.get(userBanquero.DivisionFormula__c + userBanquero.BPyP_ls_NombreSucursal__c).add(userBanquero.Name);
            } else {
                infoList.maplistUserName.put(userBanquero.DivisionFormula__c + userBanquero.BPyP_ls_NombreSucursal__c, new List<String>{userBanquero.Name});
            }

            if(infoList.mapSucursales.containsKey(userBanquero.DivisionFormula__c)) {
                infoList.mapSucursales.get(userBanquero.DivisionFormula__c).add(userBanquero.BPyP_ls_NombreSucursal__c);
            } else {
                infoList.mapSucursales.put(userBanquero.DivisionFormula__c, new Set<String>{userBanquero.BPyP_ls_NombreSucursal__c});
            }
        }
    }

    /**
    * @description Order info DD
    * @author Gabriel Garcia Rojas
    * @return void
    **/
    public static void infoByStaff(MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper infoList, User usuarioActual, List<User> listUser) {
        for(User userBanquero : listUser) {
            infoList.setDivisiones.add(userBanquero.DivisionFormula__c);

            if(infoList.mapSucursales.containsKey(userBanquero.DivisionFormula__c)) {
                infoList.mapSucursales.get(userBanquero.DivisionFormula__c).add(userBanquero.BPyP_ls_NombreSucursal__c);
            } else {
                infoList.mapSucursales.put(userBanquero.DivisionFormula__c, new Set<String>{userBanquero.BPyP_ls_NombreSucursal__c});
            }

            if(infoList.maplistUserName.containsKey(userBanquero.DivisionFormula__c + userBanquero.BPyP_ls_NombreSucursal__c)) {
                infoList.maplistUserName.get(userBanquero.DivisionFormula__c + userBanquero.BPyP_ls_NombreSucursal__c).add(userBanquero.Name);
            } else {
                infoList.maplistUserName.put(userBanquero.DivisionFormula__c + userBanquero.BPyP_ls_NombreSucursal__c, new List<String>{userBanquero.Name});
            }
        }
    }

    /**
    * @description Class WRP_ChartStacked
    * @author Gabriel Garcia Rojas
    **/
    public class WRP_ChartStacked {
        /**lista lsLabels */
        @AuraEnabled public list<String> lsLabels {get; set;}
        /**lista lsTyOpp */
        @AuraEnabled public list<String> lsTyOpp {get; set;}
        /**lista lsColor */
        @AuraEnabled public list<String> lsColor {get; set;}
        /** Mapa lsData*/
        @AuraEnabled public Map<String, Integer> lsData {get; set;}
        /** Mapa lsTool*/
        @AuraEnabled public Map<String, String> lsTool {get; set;}

        /** Constructor Wrapper */
        public WRP_ChartStacked(list<String> label,list<String> tyOpp,list<String> color,Map<String, Integer> data) {
            lsLabels=label;
            lsTyOpp=tyOpp;
            lsColor=color;
            lsData=data;
        }
    }

}