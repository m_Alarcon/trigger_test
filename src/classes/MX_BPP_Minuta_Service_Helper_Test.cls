/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Minuta_Service_Helper_Test
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-10-06
* @Description 	Test Class for BPyP Minuta Service Layer Helper
* @Changes      Date		|		Author		|		Description
 * 			2021-02-22			Héctor Saldaña	  Add new methods tagsSeleccionTest()
 * 		                                                          tagsTextTest()
*                                                                 tagsSwitchTest()
*/
@isTest
public class MX_BPP_Minuta_Service_Helper_Test {
    
    /** BPYP Account RecordType */
    static final String STR_RT_BPYPACC = 'MX_BPP_PersonAcc_Client';
    
    /** Email test for Account and Contact **/
    static final String STR_EMAIL = 'test.minuta@test.com';

    @testSetup
    static void setup() {      
        final User usuarioAdmin = UtilitysDataTest_tst.crearUsuario('PruebaAdm', Label.MX_PERFIL_SystemAdministrator, 'BBVA ADMINISTRADOR');
        usuarioAdmin.Email = 'testUser@testDomain.com';
        insert usuarioAdmin;

        System.runAs(usuarioAdmin) {

            final Account newAccount = new Account();
            newAccount.FirstName = 'userBPyP';
            newAccount.LastName = 'Test Acc';
            newAccount.No_de_cliente__c = 'D2050394';
            newAccount.RecordTypeId = RecordTypeMemory_cls.getRecType('Account', STR_RT_BPYPACC);
            newAccount.PersonEmail = STR_EMAIL;
            insert newAccount;
			
            final dwp_kitv__Visit__c testVisit = createVisit(newAccount.Id, 'Visita Prueba');           
            insert testVisit;

        }
    }
    
    /**
    * @Description 	Test Method for MX_BPP_Minuta_Service_Helper.setVisitedClientIndicator()
    * @Return 		NA
    **/
	@isTest
    static void checkIfClientIsIncludedTest() {
        final User userAdmin = [SELECT Id FROM User WHERE Name = 'PruebaAdm'];
        final Set<String> setReceivers = new Set<String>();
        Boolean result;
        System.runAs(userAdmin) {
            Test.startTest();
            final dwp_kitv__Visit__c visita = [SELECT Id, dwp_kitv__account_id__c  FROM dwp_kitv__Visit__c WHERE Name = 'Visita Prueba' LIMIT 1];
            final Account cliente = [SELECT Id, PersonEmail FROM Account WHERE Id =:visita.dwp_kitv__account_id__c];
            setReceivers.add(cliente.PersonEmail);
            setReceivers.add('test@notvalidmail.com');
            result = MX_BPP_Minuta_Service_Helper.checkIfClientIsIncluded(setReceivers, cliente.Id);
            Test.stopTest();
            
            System.assert(result, 'Email no incluido en lista');
        }
    }
    
    /**
    * @Description 	Test Method for MX_BPP_Minuta_Service_Helper.setVisitedClientIndicator()
    * @Return 		NA
    **/
	@isTest
    static void setVisitedClientIndicatorTest() {
        final User userAdmin = [SELECT Id FROM User WHERE Name = 'PruebaAdm'];
        System.runAs(userAdmin) {
            Test.startTest();
            final dwp_kitv__Visit__c visita = [SELECT Id  from dwp_kitv__Visit__c WHERE Name = 'Visita Prueba' LIMIT 1];
            MX_BPP_Minuta_Service_Helper.checkVisitedClientIndicator(visita);
            Test.stopTest();
            
            final dwp_kitv__Visit__c updatedVisit = [SELECT Id, MX_BPP_Minuta_al_Cliente__c FROM dwp_kitv__Visit__c WHERE Id =:visita.Id];
            System.assert(updatedVisit.MX_BPP_Minuta_al_Cliente__c, 'Problema en marcaje');
        }
    }
    
    /**
    * @Description 	Test Method for constructor
    * @Return 		NA
    **/
	@isTest
    static void constructorTest() {
        final MX_BPP_Minuta_Service_Helper helperConst = new MX_BPP_Minuta_Service_Helper('Test', null);
        final MX_BPP_Minuta_Service_Helper helperConstNoArg = new MX_BPP_Minuta_Service_Helper();
        System.assert(helperConst <> null && helperConstNoArg <> null, 'Error on constructor');
    }

    /**
    * @Description 	Test Method that covers parsearTagsSeleccion() from MX_BPP_Minuta_Service_Helper class
    * @Return 		NA
    **/
    @IsTest
    static void tagsSeleccionTest() {
        final List<MX_BPP_MinutaWrapper.componentePicklist> result = MX_BPP_Minuta_Service_Helper.parsearTagsSeleccion('{Seleccion1}Test{Opcion1}Test1{Opcion1}{Opcion2}test2{Opcion2}{Seleccion1}');
        System.assertEquals(result[0].label, 'Test', 'Verificar información');

    }

    /**
    * @Description 	Test Method that covers parsearTagsTexto() MX_BPP_Minuta_Service_Helper class
    * @Return 		NA
    **/
    @IsTest
    static void tagsTextTest() {
        final List<MX_BPP_MinutaWrapper.componenteText> result = MX_BPP_Minuta_Service_Helper.parsearTagsTexto('{OpcionalTexto1}Producto{OpcionalTexto1}');
        System.assertEquals(result[0].label, 'Producto', 'Verificar información');

    }

    /**
    * @Description 	Test Method for that covers parseoTagsSwitch() from MX_BPP_Minuta_Service_Helper
    * @Return 		NA
    **/
    @IsTest
    static void tagsSwitchTest() {
        final List<MX_BPP_MinutaWrapper.componenteSwitch> result = MX_BPP_Minuta_Service_Helper.parseoTagsSwitch('{Switch1}Banquero{Switch1}');
        System.assertEquals(result[0].label, 'Banquero', 'Verificar información');

    }
    
    /**
    * @Description 	helper method to create a Visit
    * @Param		AccountID-accId Account Id
    * @Param		String-Name Visit Name
    * @Return 		dwp_kitv__Visit__c
    **/
    private static dwp_kitv__Visit__c createVisit(Id accId, String name) {
        final dwp_kitv__Visit__c testVisit = new dwp_kitv__Visit__c();
        testVisit.Name = name;
        testVisit.dwp_kitv__visit_start_date__c = Date.today()+4;
        testVisit.dwp_kitv__account_id__c = accId;
        testVisit.dwp_kitv__visit_duration_number__c = '15';
        testVisit.dwp_kitv__visit_status_type__c = '01';
        
        return testVisit;
    }

}