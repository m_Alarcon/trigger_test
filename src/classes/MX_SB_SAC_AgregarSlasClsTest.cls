/**
* Nombre: MX_SB_SAC_AgregarSlasClsTest
* @author   Karen Sanchez (KB)
* Proyecto: MX_SB_SAC - BBVA Bancomer
* Descripción : Clase test para asignar los Slas a una asignación desde un caso
*
*            No.    Fecha            Autor             Description
*
* @version  1.0   2019-04-24      Karen Belem (KB)      Creación
* @version  1.1   2020-002-25     Miguel Hernández      Actualizado con el ramo
* @version  1.2   2020-03-19      Gerardo Mendoza       Se actualizan variables a final
*/
@isTest
public class MX_SB_SAC_AgregarSlasClsTest {
    @TestSetup
    static void insertarRegistros() {
        final Account testAccount = MX_WB_TestData_cls.crearCuenta('Test','PersonAccount');
        insert testAccount;
        final Opportunity testOpportunity = MX_WB_TestData_cls.createOppClosed('Opportunity Test',testAccount.Id,'ASD');
        insert testOpportunity;
        final List <SlaProcess> entitlementPId = [SELECT Id, Name FROM SlaProcess WHERE SObjectType = 'Case'
                                AND IsActive =: TRUE
                                AND IsVersionDefault =: TRUE limit 1];
        final Entitlement testEntitlement = MX_WB_TestData_cls.crearAsignacion(entitlementPId[0].Name , testAccount.Id);
        testEntitlement.SlaProcessId=entitlementPId[0].Id;
        insert testEntitlement;
        final Case testCase = MX_WB_TestData_cls.createCase(testOpportunity.Id, 'Nuevo');
        testCase.Reason = 'Cancelación';
        testCase.MX_SB_SAC_Nombre__c = 'Test Test';
        testCase.MX_SB_SAC_SubOrigenCaso__c = 'Facebook';
        testCase.EntitlementId = testEntitlement.Id;
        insert testCase;
    }
    /**
    * @description 
    * @author Gerardo Mendoza Aguilar | 03-19-2021 
    **/
    @isTest
    static void insertarSLAs() {
        final Case objCaso = [SELECT Status,EntitlementId, Entitlement.SlaProcessId, Reason FROM Case LIMIT 1];
        MX_SB_SAC_AgregarSlasCls.mAgregaSLA(new List<String> {objCaso.Id});
        final Entitlement objEntitlement = [SELECT Id,Name FROM Entitlement limit 1];
        objEntitlement.Id = objCaso.EntitlementId;
        final Database.SaveResult result = Database.update(objEntitlement);
        System.assert(result.isSuccess(),'');
    }
    /**
    * @description 
    * @author Gerardo Mendoza Aguilar | 03-19-2021 
    **/
    @isTest
    static void insertarSLAsIncidencia() {
        Test.startTest();
        final Case objCaso1 = [SELECT Status, Description, EntitlementId, Entitlement.SlaProcessId, Reason, MX_SB_SAC_AplicaDevolucion__c,
                            MX_SB_SAC_FinalizaFlujo__c, MX_SB_SAC_ActivarReglaComentario__c, MX_SB_SAC_Motivo_Rechazo__c,
                            MX_SB_SAC_Detalle__c, MX_SB_SAC_Subdetalle__c, MX_SB_SAC_InformacionAdicional__c, MX_SB_SAC_Monto__c, MX_SB_SAC_Color__c, MX_SB_SAC_Finalizar_Retencion__c
                            FROM Case LIMIT 1];

        objCaso1.MX_SB_SAC_Detalle__c = 'Meses sin intereses';
        objCaso1.MX_SB_SAC_Subdetalle__c = 'En renovación';
        objCaso1.MX_SB_SAC_InformacionAdicional__c = 'Sin documentos y no implica devolución';
        objCaso1.MX_SB_SAC_Color__c = 'Rojo';
        objCaso1.MX_SB_SAC_Ramo__c='Auto';
        update objCaso1;        
        MX_SB_SAC_AgregarSlasCls.mAgregaSLA(new List<String> {objCaso1.Id});
        final List<Entitlement> objEntitlement1 = [SELECT Id,Name FROM Entitlement limit 1];
        System.assert(!objEntitlement1.isEmpty(),'');
        Test.stopTest();
    }
    /**
    * @description 
    * @author Gerardo Mendoza Aguilar | 03-19-2021 
    **/
    @isTest
    static void insertarSLAsCancelacion() {
        Test.startTest();
        final Case objCaso = [SELECT Status, Description, EntitlementId, Entitlement.SlaProcessId, Reason, MX_SB_SAC_AplicaDevolucion__c,
                            MX_SB_SAC_FinalizaFlujo__c, MX_SB_SAC_ActivarReglaComentario__c, MX_SB_SAC_Motivo_Rechazo__c,
                            MX_SB_SAC_Detalle__c, MX_SB_SAC_Subdetalle__c, MX_SB_SAC_InformacionAdicional__c, MX_SB_SAC_Monto__c, MX_SB_SAC_Color__c, MX_SB_SAC_Finalizar_Retencion__c
                            FROM Case LIMIT 1];

        objCaso.MX_SB_SAC_Detalle__c = 'Meses sin intereses';
        objCaso.MX_SB_SAC_Subdetalle__c = 'En renovación';
        objCaso.MX_SB_SAC_InformacionAdicional__c = 'Con documentos y no implica devolución';
        objCaso.MX_SB_SAC_Color__c = 'Rojo';
        objCaso.MX_SB_SAC_Ramo__c='Auto';
        update objCaso;
        MX_SB_SAC_AgregarSlasCls.mAgregaSLA(new List<String> {objCaso.Id});
        final List<Entitlement> objEntitlement = [SELECT Id,Name FROM Entitlement limit 1];
        System.assert(!objEntitlement.isEmpty(),'');
        Test.stopTest();
    }
}