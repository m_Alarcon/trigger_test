/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 11-10-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   11-10-2020   Eduardo Hernández Cuamatzi   Initial Version
**/
@istest
public class MX_SB_VTS_SendDocumentsRuc_s_Test {
    /**Variable test continua */
    final static String FINALTEST = 'test';
    /**Respuesta generar Doc */
    final static String BODYRESPONSE = '{"document": {"data": "test","size": 66545,"id": "GMDSEHO001"},"href": "http://150.100.104.43:36026"}'; 
    /**URL Mock Test*/
    private final static String URLTOTESTASO = 'http://www.example.com';
    /**Particion IASO*/
    private final static String IASOPARTASO = 'local.MXSBVTSCache';
    /**Producto hsd mayusculas */
    private final static String PRODUCTHSD = 'HOGAR SEGURO DINÁMICO';
    /**Proceso poliza */
    private final static String PROCESSPOLICY = 'policy';
    /**Id producto Hsd */
    //private final static String PRODUCTIDHSD = '123455';

    @testSetup static void setup() {
        final MX_SB_VTS_Generica__c setting = MX_WB_TestData_cls.GeneraGenerica('IASO01', 'IASO01');
        setting.MX_SB_VTS_Type__c = 'IASO01';
        setting.MX_SB_VTS_HEADER__c = 'http://www.example.com';
        insert setting;
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'sendDocumentsByMail', iaso__Url__c = URLTOTESTASO, iaso__Cache_Partition__c = IASOPARTASO);
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'getCustomerDocument', iaso__Url__c = URLTOTESTASO, iaso__Cache_Partition__c = IASOPARTASO);
    }

    @isTest
    static void fillBodyCustomDocument() {
        final Opportunity oppCustDoc = new Opportunity(Producto__c = PRODUCTHSD);
        final Map<String, String> mapDocuments = MX_SB_VTS_SendDocumentsRuc_Helper.fillDataDocs(oppCustDoc, PROCESSPOLICY);
        final HttpRequest fillBody = MX_SB_VTS_SendDocumentsRuc_service.fillBodyCustomDocument('1234', mapDocuments, PROCESSPOLICY, PRODUCTHSD);
        System.assertEquals(fillBody.getEndPoint(), 'http://www.example.comgetCustomerDocument', 'body Construido');
    }

    @isTest
    static void processResponse() {
        final Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(BODYRESPONSE);
        final MX_SB_VTS_SendDocumentsRuc_service.WrapDocumentRes wrapDocumentRes = MX_SB_VTS_SendDocumentsRuc_service.processResponse(responseMap);
        System.assertEquals(66545, wrapDocumentRes.dataDoc.sizeDoc, 'Respuesta procesada correctamente');
    }

    @isTest
    static void fillDocumentEmail() {
        final List<String> lstHrls = new List<String> {URLTOTESTASO, URLTOTESTASO, URLTOTESTASO};
        final Quote quoteData = new Quote();
        quoteData.MX_SB_VTS_ASO_FolioCot__c = '562512';
        quoteData.MX_SB_VTS_Nombre_Contrante__c = FINALTEST;
        quoteData.MX_SB_VTS_Apellido_Paterno_Contratante__c = FINALTEST;
        quoteData.MX_SB_VTS_Apellido_Materno_Contratante__c = FINALTEST;
        quoteData.MX_SB_VTS_RFC__c = '12345678';
        quoteData.MX_SB_VTS_Email_txt__c = FINALTEST+'@'+FINALTEST+'.com';
        quoteData.MX_SB_VTS_Numero_de_Poliza__c = '9981245';
        final List<String> lstProcSend = new List<String>{'HOSETE', 'Hogar Seguro Dinámico'};
        final Map<String, Object> fillDocs = MX_SB_VTS_SendDocumentsRuc_service.fillDocumentEmail(quoteData, lstHrls, lstProcSend);
        System.assertEquals('12345678', (String)fillDocs.get('pass'), 'Body sendDocument correcto');
    }
}