/**
* @File Name          : public class MX_RTL_Campaign_Selector.cls
* @Author             : Gerardo Mendoza Aguilar
* @Group              :
* @Last Modified By   : Gerardo Mendoza Aguilar
* @Last Modified On   : 12-08-2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      11/11/2020           Gerardo Mendoza Aguilar          Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public Without Sharing class MX_RTL_Campaign_Selector {

    /**
    * @description Query Campaign
    * @author Gerardo Mendoza | 11/11/2020
    * @param String queryFields, String queryFilters
    * @return List<Campaign>
    **/  
    public static List<Campaign> selectCampaign(String queryFields, String queryFilters) {
        return Database.query('SELECT ' + String.escapeSingleQuotes(queryFields) + ' FROM Campaign ' + (String.escapeSingleQuotes(queryFilters)).unescapeJava() + ' ORDER BY CreatedDate DESC LIMIT 1');
    }
}