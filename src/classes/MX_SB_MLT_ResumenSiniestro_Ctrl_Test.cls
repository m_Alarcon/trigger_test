@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_ResumenSiniestro_Ctrl_Test
* Autor Juan Carlos Benitez Herrera
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_ResumenSiniestro_Ctrl

* --------------------------------------------------------------------------------
* Versión       Fecha                  Autor                        Descripción
* --------------------------------------------------------------------------------
* 1.0           02/06/2020      Juan Carlos Benitez Herrera          Creación
* --------------------------------------------------------------------------------
*/
public class MX_SB_MLT_ResumenSiniestro_Ctrl_Test {
	        /*
	* var String Valor Subramo Ahorro
	*/
    public static final String SUBRAMO ='Salud';
	/** Nombre de usuario Test */
    final static String NAME = 'Nami '; 
    /** current user id */
    final static String USR_ID = UserInfo.getUserId();
    /**
     * @description
     * @author Juan Carlos Benitez | 5/26/2020
     * @return void
     **/
    @TestSetup
    static void createData() {
        Final String profileVida = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User userVida = MX_WB_TestData_cls.crearUsuario(NAME, profileVida);  
        insert userVida;
        System.runAs(userVida) {
            final String rcordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Proveedores_MA).getRecordTypeId();
            final Account accntVidaTest = new Account(RecordTypeId = rcordTypeId, Name=NAME, Tipo_Persona__c='Física');
            insert accntVidaTest;
            final Contract cntractVida = new Contract(AccountId = accntVidaTest.Id, MX_SB_SAC_NumeroPoliza__c='policyTest');
            insert cntractVida;
            Final List<Siniestro__c> sinInsert = new List<Siniestro__c>();
            for(Integer j = 0; j < 10; j++) {
                Final Siniestro__c nSin = new Siniestro__c();
                nSin.MX_SB_SAC_Contrato__c = cntractVida.Id;
                nSin.MX_SB_MLT_NombreConductor__c = NAME + '_' + j;
                nSin.MX_SB_MLT_APaternoConductor__c = 'LastName for '+NAME;
                nSin.MX_SB_MLT_AMaternoConductor__c = 'LastName2 for '+NAME;
                nSin.MX_SB_MLT_Fecha_Hora_Siniestro__c =date.valueof('2020-03-27T22:04:00.000+0000');
                nSin.MX_SB_MLT_Telefono__c = '5534253647';
                nSin.MX_SB_MLT_AtencionVida__c = 'Agendar Cita';
                nSin.MX_SB_MLT_PreguntaVida__c = false;
                nSin.TipoSiniestro__c = 'Siniestros';
                nSin.MX_SB_MLT_SubRamo__c = SUBRAMO;
                nSin.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_creacion;
                nSin.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoVida).getRecordTypeId();
                sinInsert.add(nSin);
            }
            Database.insert(sinInsert);
        }
    }
    @isTest
    static void tstsearchSinVida() {
        final String sinId =[SELECT Id from Siniestro__c WHERE MX_SB_MLT_NombreConductor__c =: NAME + '_3' ].Id;
        test.startTest();
			MX_SB_MLT_ResumenSiniestro_Ctrl.searchSinVida(sinId);
        system.assert(True,'Se ha encontrado Exitosamente');
        test.stopTest();
    }
    @isTest
        static void testConstrct() {
        Final MX_SB_MLT_ResumenSiniestro_Ctrl controller = new MX_SB_MLT_ResumenSiniestro_Ctrl();
        system.assert(true,controller);
	}
}