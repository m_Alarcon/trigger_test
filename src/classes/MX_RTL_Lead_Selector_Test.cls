/**
* Name: MX_RTL_Lead_Selector_Test
* @author Ángel Lavana Rosas
* Description : New Class to test class MX_RTL_Lead_Selector, create records with Name _CPN
*				and records without _CPN
* Ver                  Date            Author                   Description
* @version 1.0         Jun/05/2020     Ángel Lavana Rosas       Initial Version
* @version 1.1         Jun/05/2020     Jair Ignacio Gonzalez    Add testLeadsConvert
**/
@isTest
public class MX_RTL_Lead_Selector_Test {
    /* @Description: Create test data set
	*/
    @isTest
    public static void leadSelectorCPN() {
        final List<Id> leadsId = new List<Id>{};
            final List<lead> leads = new List<Lead>{};

                for(integer contador = 0; contador<=5; contador++) {
                   final Lead candidato = new Lead(FirstName = 'Campaña' + contador,
                                              LastName = 'prueba' + contador,
                                              MX_SB_VTS_CampaFaceName__c = 'SEGUROS WIBE BBVA_CPNWHOOP0' + contador ,
                                              MX_SB_VTS_CodCampaFace__c = '',
                                              MX_SB_VTS_DetailCampaFace__c = 'ANUNCIO POR_FACEBOOK ' + contador);
                    leads.add(candidato);
                }
        for(integer contador = 0; contador<=5; contador++) {
            final Lead candidato = new Lead(FirstName = 'Campaña' + contador,
                                      LastName = 'prueba' + contador,
                                      MX_SB_VTS_CampaFaceName__c = 'SEGUROS WIBE_BBVA' + contador ,
                                      MX_SB_VTS_CodCampaFace__c = '',
                                      MX_SB_VTS_DetailCampaFace__c = 'ANUNCIO POR_FACEBOOK ' + contador);
            leads.add(candidato);
        }
        insert leads;
        for(Lead get : leads) {
            leadsId.add(get.Id);
        }
        final List<Lead> valor = MX_RTL_Lead_Selector.leadSelectorCupon(leadsId);
        System.AssertNotEquals(leads,valor);
    }

    /**
     * *Descripción: Clase de prueba para leadsConvert
    **/
    @isTest
    static void testLeadsConvert() {
        final Account oAcc = new Account(Name='Test Account',No_de_cliente__c = '8316252');
        insert oAcc;
        final Opportunity oOpp = new Opportunity(Name='Test', StageName='Abierta', CloseDate=Date.today().addDays(15),
            RecordTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('MX_BPP_RedBpyp').getRecordTypeId()
            );
        insert oOpp;
        final Lead oLead = new Lead(LastName='Lead Test', RecordTypeId=Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('MX_BPP_Leads').getRecordTypeId(), MX_ParticipantLoad_id__c=oAcc.No_de_cliente__c);
        insert oLead;
        final List<Database.LeadConvert> bulkLeadconvert = new List<Database.LeadConvert>();
        final Database.LeadConvert Leadconvert = new Database.LeadConvert();
                Leadconvert.setLeadId(oLead.Id);
                Leadconvert.setConvertedStatus('Convertido');
                Leadconvert.setOpportunityId(oOpp.Id);
                Leadconvert.setAccountId(oAcc.Id);
                bulkLeadconvert.add(Leadconvert);
        Test.startTest();
        final List<Database.LeadConvertResult> lsResult = MX_RTL_Lead_Selector.leadsConvert(bulkLeadconvert);
        Test.stopTest();
        System.assertEquals(lsResult.size(), 1,'Lead not converted');
    }

    @isTest
    private static void insertLead() {
        final Lead callmeAuto = new Lead();
        callmeAuto.Producto_Interes__c = 'Vida';
        callmeAuto.LeadSource = System.Label.MX_SB_VTS_OrigenCallMeBack;
        callmeAuto.FirstName = 'Usuario prueba';
        callmeAuto.LastName = System.Label.MX_SB_VTS_OrigenCallMeBack;
        callmeAuto.TelefonoCelular__c = '5524815910';
        final List<Lead> lstLeads = new List<Lead>{callmeAuto};
        Test.startTest();
            final List<Database.SaveResult> lstResults = MX_RTL_Lead_Selector.insertLeads(lstLeads, true);
        Test.stopTest();
        System.assert(lstResults[0].isSuccess(),'Lead not converted');
    }

    /**
     * @description
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void resultQueryLeadTest() {
        final List<Lead> listLeadW = MX_RTL_Lead_Selector.resultQueryLead(' Id ', '' , false);
        System.assertEquals(listLeadW.size(), 0, 'Fail Test');
    }

    /**
     * @description
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void resultQueryLeadFTest() {
        final List<Lead> listLeadC = MX_RTL_Lead_Selector.resultQueryLead(' Id ', ' FirstName = \'Campaña\'' , true);
        System.assertEquals(listLeadC.size(), 0, 'Fail Test');
    }
    
    /**
    *@Description   Test Method for getLeadsByAccountAndRtype
    *@author 		Edmundo Zacarias
    *@Date 			2020-08-06
    **/
    @isTest
    private static void testGetLeadsByAccountAndRtype() {
        final Set<Id> accIds = new Set<Id>();
        final Account testAcc = new Account(LastName = 'testAcc', FirstName = 'testAcc', No_de_cliente__c = '1234');
        insert testAcc;
        accIds.add(testAcc.Id);
        
        Test.startTest();
        final List<Lead> listLeadC = MX_RTL_Lead_Selector.getActiveLeadsByAccountAndRtype('Id', accIds, 'MX_BPP_Leads');
        Test.stopTest();
        
        System.assertEquals(0, listLeadC.size(), 'Failed query test');
    }
}