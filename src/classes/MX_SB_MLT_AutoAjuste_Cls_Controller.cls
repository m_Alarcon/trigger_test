/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_AutoAjuste_Cls_Controller
* Autor Marco Antonio Cruz Barboza
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Almacena  los Methods para componente MX_SB_MLT_AutoModal
*
* --------------------------------------------------------------------------------
* Versión       Fecha                  Autor                        Descripción
* --------------------------------------------------------------------------------
* 1.0           12/02/2020            Marco Cruz                     Creación
* 1.2           05/05/2020    Juan Carlos Benitez Herrera      Se modifica metodh autoajuste
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_AutoAjuste_Cls_Controller {
    /*
    * @VAR to avoid literal strings
    */
    public static Final String ACEPTAR = 'aceptar';
    /*
    * @VAR to avoid literal strings
    */
    public static Final String RECHAZAR = 'rechazar';
      /**
	* @description
	* Method Constructor
	**/
    private MX_SB_MLT_AutoAjuste_Cls_Controller() {

    }

     /*
    * @description Actualiza el registro si el usuario acepta autoajuste
    * @param String recordId
    * @return void
    */
    @auraEnabled
    public static void autoajuste(String recordId, String rechacept) {
		final Siniestro__c updateSiniestro = [SELECT Id, 
                                              MX_SB_MLT_JourneySin__c,
                                              MX_SB_MLT_RazonCierre__c,
                                              MX_SB_MLT_AjustadorAuto__c,
                                              MX_SB_MLT_AjustadorMoto__c,
                                              MX_SB_MLT_AutoAjusteAccepted__c
                                              FROM Siniestro__c
                                              WHERE Id=:recordId
                                              LIMIT 1];
        if(rechacept==ACEPTAR) {
			updateSiniestro.MX_SB_MLT_AjustadorAuto__c = false;
            updateSiniestro.MX_SB_MLT_AjustadorMoto__c = false;
            updateSiniestro.MX_SB_MLT_JourneySin__c = 'Cerrado';
            updateSiniestro.MX_SB_MLT_AutoAjusteAccepted__c = 'true';
            updateSiniestro.MX_SB_MLT_RazonCierre__c = 'AutoAjuste';
        }
        if(rechacept==RECHAZAR) { 
            updateSiniestro.MX_SB_MLT_AutoAjusteAccepted__c = 'false';
        } else {
            updateSiniestro.MX_SB_MLT_AutoAjusteAccepted__c = '';
        }
                update updateSiniestro;
	}
}