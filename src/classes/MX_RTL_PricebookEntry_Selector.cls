/**
* @author           Daniel perez lopez
* Project:          Presuscritos
* Description:      Clase service para llamar a selectores PricebookEntry
*
* Changes (Version)
* ------------------------------------------------------------------------------------------------
*           No.     Date            Author          1        Description
*           -----   ----------      --------------------    --------------------------------------
* @version  1.0     2020-09-05      Daniel perez lopez.     Creación de la Clase
*/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_RTL_PricebookEntry_Selector {
      /**
    * @description method para consultar los pricebookentry
    * @author Daniel Perez Lopez | 04/09/2020
    * @param String Fields
    * @param String[] ids
    * @return PricebookEntry[]
    **/
    public static PricebookEntry[] pricebookEntries(String fields ,String[] ids) {
        final String query = 'SELECT '+fields+' FROM PricebookEntry WHERE Product2Id IN:ids';
        return DataBase.query(String.escapeSingleQuotes(query));
    }
}