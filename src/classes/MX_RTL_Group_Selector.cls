/**
* Name: MX_RTL_Group_Selector
* @author Jose angel Guerrero
* Description : Nuevo selector de clase para la lista de grupos
* Ver                  Date            Author                   Description
* @version 1.0         Jul/13/2020     Jose angel Guerrero      Initial Version
**/
/**
* @description: Nuevo selector de clase para grupos
*/
@SuppressWarnings('sf:UseSingleton, sf:DMLWithoutSharingEnabled')
public class MX_RTL_Group_Selector {
  /**
* @description: funcion que trae lista de campos y prefijo
*/
    public static List<Group> getQueuesByPrefijo(String prefijo) {
        return [SELECT Id, DeveloperName, name from Group where Type='Queue' AND DeveloperName LIKE :prefijo LIMIT 100];
    }
}