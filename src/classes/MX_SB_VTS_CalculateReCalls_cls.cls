/**
 * @File Name          : MX_SB_VTS_CalculateReCalls_cls.cls
 * @Description        : 
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 3/12/2019 16:19:54
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.ACTIONZERO    19/11/2019   Eduardo Hernández Cuamatzi     Initial Version
**/
public virtual class MX_SB_VTS_CalculateReCalls_cls extends MX_SB_VTS_LeadMultiCTI_Util { //NOSONAR
    /**Fecha hora de llamada */
    public static Datetime dateTimeCALLS {get;set;}
    /**Fecha de llamada */
    public static Date dateCALLS {get;set;}
    /**Dia de la semana */
    public static String dateWeekend {get;set;}
    /**Motivo nivel 6 */
    public static MX_WB_MotivosNoContacto__c motivoFlow {get;set;}
    /**Motivo nextAction */
    public final static String NEXTACTION = 'nextAction';
    /**Motivo nextAction */
    public final static Integer ACTIONFOUR = 4;
    /**Motivo nextAction ACTIONZERO*/
    public final static Integer ACTIONZERO = 0;
    /**Motivo nextAction ACTIONTIME24*/
    public final static Integer ACTIONTIME24 = 24;
	/**Motivos Map*/
	public final static Map<String, MX_WB_MotivosNoContacto__c> MAPMOTIVOS = fillMotivos();
    /**
    * @description calcula reglas de remarcado
    * @author Eduardo Hernández Cuamatzi | 26/11/2019 
    * @param recordVals valores a recalcular
    * @param traySend bandeja para enviar registro
    * @param proveedor proveedor de maracado
    * @param level7 tipificacion nivel 7    
    * @return WrapperRecallCTI wrapper con valores de remarcado
    **/
    public static WrapperRecallCTI calculateRules(Map<String, String> recordVals, MX_SB_VTS_Lead_tray__c traySend, MX_SB_VTS_ProveedoresCTI__c proveedor, String level7, Datetime tiempoPrueba) {
        final WrapperRecallCTI newItem = new WrapperRecallCTI();
        final Datetime timeNow = tiempoPrueba;//System.now();
        dateCALLS = tiempoPrueba.addHours(-6).date();
        dateWeekend = timeNow.format('EEEE');
        dateTimeCALLS = tiempoPrueba;
        if(MAPMOTIVOS.containsKey(recordVals.get('level6'))) {
            newItem.idRecord = recordVals.get('Id');
            motivoFlow = MAPMOTIVOS.get(recordVals.get('level6'));
            newItem.login = '';
            newItem.serviceId = Integer.valueOf(MX_SB_VTS_SendLead_helper_cls.returnEmptyphone(traySend.MX_SB_VTS_ServicioID__c));
            newItem.sTelefono1 = recordVals.get('phone1');
            newItem.sTelefono2 = recordVals.get('phone2');
            newItem.sTelefono3 = recordVals.get('phone3');
            newItem.loadId = traySend.MX_SB_VTS_ID_Bandeja__c;
            final Boolean evaluateAgenda = MX_SB_VTS_CalculateReCallsUtil_cls.calculateReAgenda(level7, recordVals.get('Id'), timeNow);
            Map<String,String> newMapRe = new Map<String,String>();
            newMapRe = evaluateLevel7(level7, evaluateAgenda, recordVals,proveedor, timeNow);
            newItem.scheduleDate = newMapRe.get('schedulDate');
            newItem.nextAction = newMapRe.get(NEXTACTION);
            newItem.displayName = recordVals.get('displayName');
        }
        return newItem; 
    }
	 /**
    * @description evalua nivel 7
    * @author Eduardo Hernández Cuamatzi | 26/11/2019 
    * @param motivoRemarcado motivo de tipifiacion
    * @param timeNow hora de marcado
    * @param nextAction accion a calcular
    * @param proveedor 
    * @return Map<String, String> 
    **/
    public static Map<String, String> evaluateLevel7(String level7, Boolean evaluateAgenda, Map<String, String> recordVals,MX_SB_VTS_ProveedoresCTI__c proveedor ,Datetime timeNow) {
        Map<String,String> newMapRecall = new Map<String,String>();
        System.debug('recordVals.get(NEXTACTION):::: '+recordVals.get(NEXTACTION));
            if(level7.equalsIgnoreCase('N/A') || (level7.equalsIgnoreCase('Reagenda') && evaluateAgenda == false && recordVals.get(NEXTACTION).equalsIgnoreCase('1') == false)) {
                final MX_WB_MotivosNoContacto__c motivoRemarcado = MAPMOTIVOS.get(recordVals.get('level6'));
                newMapRecall = evaluteforDates(motivoRemarcado, timeNow, recordVals.get(NEXTACTION), proveedor);
            } else if(level7.equalsIgnoreCase('Reagenda') == true && evaluateAgenda && (recordVals.get(NEXTACTION).equalsIgnoreCase('1') || recordVals.get(NEXTACTION).equalsIgnoreCase('2'))) {
                newMapRecall.put('schedulDate', '');
                newMapRecall.put('nextAction', '1'); //NOSONAR
            }
        return newMapRecall;
    }
    /**
    * @description evalua fechas de remarcado
    * @author Eduardo Hernández Cuamatzi | 26/11/2019 
    * @param motivoRemarcado motivo de tipifiacion
    * @param timeNow hora de marcado
    * @param nextAction accion a calcular
    * @param proveedor 
    * @return Map<String, String> 
    **/
    public static Map<String, String> evaluteforDates(MX_WB_MotivosNoContacto__c motivoRemarcado, Datetime timeNow, String nextAction, MX_SB_VTS_ProveedoresCTI__c proveedor) {
        final Map<String, String> recordsCalculate = new Map<String, String>();
        List<String> reCalVals = new List<String>{'','0'};
        final Integer nextTime = MX_SB_VTS_CalculateReCallsUtil_cls.evaluteforNextA(nextAction,motivoRemarcado);
        if(nextTime > ACTIONZERO) {
            final Datetime addTime = timeNow.addHours(nextTime);
            if(addTime.hour() > ACTIONZERO) {
                reCalVals = newScheduldDate(addTime, proveedor, nextAction, motivoRemarcado, nextTime, false);
            } else {
                reCalVals = evaluateForExtraCase(addTime, proveedor,nextAction, motivoRemarcado);
            }
        }
        recordsCalculate.put('schedulDate',reCalVals[ACTIONZERO]);
        recordsCalculate.put('nextAction',reCalVals[1]); //NOSONAR
        
        return recordsCalculate;
    }

    /**
    * @description evalua esceneario extraordinario de ACTIONZERO horas
    * @author Eduardo Hernández Cuamatzi | 26/11/2019 
    * @param addTime tiempo calculado
    * @param proveedor proveedor a remarcar
    * @param nextAction acción siguiente
    * @param motivoRemarcado motivo tipificacion nivel 6
    * @return List<String> lista de valores calculados
    **/
    public static List<String> evaluateForExtraCase(Datetime addTime, MX_SB_VTS_ProveedoresCTI__c proveedor, String nextAction, MX_WB_MotivosNoContacto__c motivoRemarcado) {
        List<String> extraList = new List<String>(); //NOSONAR
        final Integer nextInt = Integer.valueOf(nextAction);
        final Integer nextCalldifFou = nextInt+1;
        if(nextCalldifFou < ACTIONFOUR) {
            extraList = evaluateExtraCaseNo4(nextCalldifFou, motivoRemarcado);
        } else {
            extraList = evaluateExtraCase4(nextCalldifFou, motivoRemarcado);
        }
        return extraList;
    }

    /**
    * @description evalua remarcado menor a 4 para extra caso
    * @author Eduardo Hernández Cuamatzi | 26/11/2019 
    * @param nextCalldifFou 
    * @param motivoRemarcado 
    * @return List<String> 
    **/
    public static List<String> evaluateExtraCaseNo4(Integer nextCalldifFou, MX_WB_MotivosNoContacto__c motivoRemarcado) {
        List<String> extraList = new List<String>();
        final Integer getNextTime = MX_SB_VTS_CalculateReCallsUtil_cls.evaluteforNextA(String.valueOf(nextCalldifFou), motivoRemarcado);
        if(getNextTime > ACTIONZERO && getNextTime < ACTIONTIME24) {
            final Date callNewDa = dateCALLS.addDays(1);
            final Datetime newcall = Datetime.newInstance(callNewDa.year(), callNewDa.month(), callNewDa.day(), 9, ACTIONZERO, ACTIONZERO);
            extraList.add(newcall.format(System.Label.MX_SB_VTS_FormatDateSmart));
            extraList.add(String.valueOf(nextCalldifFou));
        } else if(getNextTime > ACTIONZERO && getNextTime>ACTIONTIME24) {
            final Date callNewDados = dateTimeCALLS.addHours(getNextTime).Date();
            final Datetime nCalldifFou = Datetime.newInstance(callNewDados.year(), callNewDados.month(), callNewDados.day(), 9, ACTIONZERO, ACTIONZERO);
            extraList.add(nCalldifFou.format(System.Label.MX_SB_VTS_FormatDateSmart));
            String netNewcalla = String.valueOf(nextCalldifFou);
            if(nextCalldifFou+1 == ACTIONFOUR) {
                netNewcalla = String.valueOf(motivoRemarcado.MX_SB_VTS_AccionCuatro__c);
            }
            extraList.add(netNewcalla);
        } else if(getNextTime == ACTIONZERO) {
            extraList.add('');
            extraList.add('');
        }
        return extraList;
    }

    /**
    * @description evalua caso extra accion 4
    * @author Eduardo Hernández Cuamatzi | 26/11/2019 
    * @param nextCalldifFou hora de llamada diferida
    * @param motivoRemarcado motivo remarcado nivel 6
    * @return List<String> lista de valores calculados
    **/
    public static List<String> evaluateExtraCase4(Integer nextCalldifFou, MX_WB_MotivosNoContacto__c motivoRemarcado) {
        final List<String> extraList4 = new List<String>();
        if(Integer.valueOf(motivoRemarcado.MX_SB_VTS_AccionCuatro__c) > ACTIONZERO) {
            if(MX_SB_VTS_CalculateReCallsUtil_cls.evaluteforNextA(String.valueOf(Integer.valueOf(motivoRemarcado.MX_SB_VTS_AccionCuatro__c)), motivoRemarcado) < ACTIONTIME24) {
                final Date callNewDa = dateCALLS.addDays(1);
                final Datetime newcall = Datetime.newInstance(callNewDa.year(), callNewDa.month(), callNewDa.day(), 9, ACTIONZERO, ACTIONZERO);
                extraList4.add(newcall.format(System.Label.MX_SB_VTS_FormatDateSmart));
                extraList4.add(String.valueOf(Integer.valueOf(motivoRemarcado.MX_SB_VTS_AccionCuatro__c)));
            } else {
                final Integer getNextTime = MX_SB_VTS_CalculateReCallsUtil_cls.evaluteforNextA(String.valueOf(Integer.valueOf(motivoRemarcado.MX_SB_VTS_AccionCuatro__c)), motivoRemarcado);
                final Date callNewDados = dateTimeCALLS.addHours(getNextTime).Date();
                final Datetime nextCallFou = Datetime.newInstance(callNewDados.year(), callNewDados.month(), callNewDados.day(), 9, ACTIONZERO, ACTIONZERO);
                extraList4.add(nextCallFou.format(System.Label.MX_SB_VTS_FormatDateSmart));
                extraList4.add(String.valueOf(String.valueOf(Integer.valueOf(motivoRemarcado.MX_SB_VTS_AccionCuatro__c)+1)));
            }
        } else {
            extraList4.add('');
            extraList4.add('');
        }
        return extraList4;
    }

    /**
    * @description calcula siguiente hora de remarcado
    * @author Eduardo Hernández Cuamatzi | 26/11/2019 
    * @param addTime fecha con horas de accion calculada
    * @param proveedor proveedor de marcado
    * @param nextAction accion calculada
    * @param motivoRemarcado motivo de tipificación 6
    * @param totalHours horas calculadas
    * @param isG4 pertenece a bucle acción 4
    * @return List<String> lista de valores calculados
    **/
    public static List<String> newScheduldDate (Datetime addTime, MX_SB_VTS_ProveedoresCTI__c proveedor, String nextAction, MX_WB_MotivosNoContacto__c motivoRemarcado, Integer totalHours, Boolean isG4) {
        List<String> valuesCalc = new List<String>();
        final Datetime timeCalls = String.isEmpty(String.valueOf(dateTimeCALLS)) ? Datetime.now() : dateTimeCALLS;
        final Time limitCall =MX_SB_VTS_CalculateReCallsUtil_cls.finalDateTimeCall(proveedor, timeCalls);
        final Time newTimeCall = Time.newInstance(addTime.hour(), addTime.minute(), 00, 00);
        final Map<String,Object> mapIsNextCalc = new Map<String,Object>{'limitCall' => limitCall,'newTimeCall' => newTimeCall, 'dateCALLS' => dateCALLS,'addTime' => addTime.date(), 'nextAction' => Integer.valueOf(nextAction), 'isG4' => isG4};
        System.debug('mapIsNextCalc:::: '+mapIsNextCalc);
        final Integer nextAct = MX_SB_VTS_CalculateReCallsUtil2_cls.isNextCalc(mapIsNextCalc, motivoFlow);
        if(nextAct < 4 && isG4 == false) {
            valuesCalc = evaluateNoBuAct(newTimeCall, limitCall, timeCalls, addTime, nextAction, nextAct, totalHours);
        } else {
            final String newCall = addTime.format(System.Label.MX_SB_VTS_FormatDateSmart);
            valuesCalc = evaluateBuAct(nextAct, addTime, proveedor, isG4, newCall);
        }
        System.debug('valuesCalc::::: '+valuesCalc);
        return valuesCalc;
    }

    /**
    * @description evaluar accion en bucle
    * @author Eduardo Hernández Cuamatzi | 26/11/2019 
    * @param nextAct accion siguiente a evaluar
    * @param addTime hora calculada
    * @param proveedor proveedor marcado
    * @param isG4 pertenece a bucle
    * @param newCall nueva hora de marcado
    * @return List<String> valors de marcado
    **/
    public static List<String> evaluateBuAct(Integer nextAct, Datetime addTime, MX_SB_VTS_ProveedoresCTI__c proveedor, Boolean isG4, String newCall) {
        List<String> evaluateBuActLst = new List<String>();
        if(nextAct == 4 && String.isNotBlank(String.valueOf(motivoFlow.MX_SB_VTS_AccionCuatro__c)) && motivoFlow.MX_SB_VTS_AccionCuatro__c >ACTIONZERO && isG4 == false) {
            evaluateBuActLst = MX_SB_VTS_CalculateReCallsUtil_cls.evaluateG4(addTime, proveedor, String.valueOf(Integer.valueOf(motivoFlow.MX_SB_VTS_AccionCuatro__c)), motivoFlow);
        } else {
            if(MX_SB_VTS_CalculateReCallsUtil_cls.evaluteforNextA(String.valueOf(nextAct), motivoFlow) > ACTIONZERO) {
                evaluateBuActLst.add(newCall);
            } else {
                evaluateBuActLst.add('');
            }
            evaluateBuActLst.add(String.valueOf(nextAct));   
        }
        return evaluateBuActLst;
    }

    /**
    * @description evaluar para cciones de no bucle
    * @author Eduardo Hernández Cuamatzi | 26/11/2019 
    * @param newTimeCall hora calculdada
    * @param limitCall hora limite de marcado
    * @param timeCalls hora de marcado
    * @param addTime fecha calculada
    * @param nextAction accion calculada
    * @param nextAct accion siguinete
    * @param totalHours total horas calculadas
    * @return List<String> lista de valores calculados
    **/
    public static List<String> evaluateNoBuAct(Time newTimeCall, Time limitCall, Datetime timeCalls, Datetime addTime, String nextAction, Integer nextAct, Integer totalHours) {
        List<String> valuesCalcNoB = new List<String>();
        Integer addHoursCall = ACTIONZERO;
        String nextActionR = nextAction;
        if(newTimeCall > limitCall || timeCalls.date().daysBetween(addTime.date()) > ACTIONZERO && Integer.valueOf(nextAction) > 1) {
            nextActionR = String.valueOf(nextAct);
        }
        switch on Integer.valueOf(nextActionR) {
            when 1 {
                addHoursCall = Integer.valueOf(motivoFlow.MX_SB_VTS_AccionUno__c);
            }
            when 2 {
                addHoursCall = Integer.valueOf(motivoFlow.MX_SB_VTS_AccionDos__c);
            }
            when 3 {
                addHoursCall = Integer.valueOf(motivoFlow.MX_SB_VTS_AccionTres__c);
            }
        }
        valuesCalcNoB = reEvalActEndDay(nextActionR, addHoursCall, nextAct, totalHours, limitCall, timeCalls, newTimeCall);
        return valuesCalcNoB;
    }

    /**
    * @description evalua acciones para fin de hora de marcado
    * @author Eduardo Hernández Cuamatzi | 26/11/2019 
    * @param nextAction accion siguiente evaluada
    * @param addHoursCall horas añadidas
    * @param nextAct siguiente accion actual
    * @param totalHours horas totales agregadas
    * @param limitCall hora limite de marcado
    * @param timeCalls hora calculada
    * @param newTimeCall fecha calculada
    * @return List<String> lista de valores calculados
    **/
    public static List<String> reEvalActEndDay(String nextAction, Integer addHoursCall,Integer nextAct, Integer totalHours, Time limitCall, Datetime timeCalls, Time newTimeCall) {
        final List<String> reEvalActEndLst = new List<String>();
        if(Integer.valueOf(nextAction) == 1 && MX_SB_VTS_CalculateReCallsUtil_cls.evaluteforNextA(nextAction, motivoFlow) > ACTIONTIME24) {
            reEvalActEndLst.add(calculateNextC(dateTimeCALLS, totalHours, limitCall));
        } else {
            reEvalActEndLst.add(calculateNextC(dateTimeCALLS, addHoursCall, limitCall));
        }
        if(Integer.valueOf(nextAction) == 3 && (timeCalls.date().daysBetween(timeCalls.addHours(addHoursCall).date()) > ACTIONZERO || newTimeCall> limitCall)) {
            reEvalActEndLst.add(String.valueOf(Integer.valueOf(motivoFlow.MX_SB_VTS_AccionCuatro__c)));
        } else {
            reEvalActEndLst.add(String.valueOf(nextAct));
        }
        return reEvalActEndLst;
    }

}