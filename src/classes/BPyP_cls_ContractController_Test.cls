/*
----------------------------------------------------------------------------------------------------------------------------
|   Autor:                  Gabriel García                                                                      			|
|   Proyecto:               Migración Retail BPyP                                                                           |
|   Descripción:            Clase test de control de la clase BPyP_cls_ContractController.                                  |
|                                                                                                                           |
|   --------------------------------------------------------------------------                                              |
|   No.     Fecha               Autor                   Descripción                                                         |
|   ------  ----------  ----------------------      --------------------------                                              |
|   1.0     22-11-2019  Gabriel García                  Creación a partir de la clase test BibliotecaController_test        |
-----------------------------------------------------------------------------------------------------------------------------
*/
@isTest
public with sharing class BPyP_cls_ContractController_Test {

    /**
    *Test method
	**/
    @isTest static void testMethodContractTest() {
        final RecordType rtAccCF = [Select Id, DeveloperName from RecordType where SObjectType = 'Account' and developerName = 'BPyP_tre_noCliente'];
        final List<Profile> prof = [SELECT Id FROM Profile WHERE Name =: Label.MX_PERFIL_SystemAdministrator LIMIT 1];

        final User usr = new User();
        usr.Username ='Nombretest@gmail.com';
        usr.LastName= 'Test';
        usr.Email= 'Nombretest@gmail.com';
        usr.Alias= 'tst1';
        usr.TimeZoneSidKey= 'America/Mexico_City';
        usr.LocaleSidKey= 'en_US';
        usr.EmailEncodingKey= 'ISO-8859-1';
        usr.ProfileId= prof.get(0).Id;
        usr.LanguageLocaleKey ='es';
        usr.VP_ls_Banca__c = 'Red BPyP';
        insert usr;

        final Account acc = new Account();
        acc.Name ='Justino Flores';
        acc.BPyP_fh_FechaNacimiento__c = System.today();
        acc.BPyP_ls_Ocupacion__c = 'ABARROTES';
        acc.BPyP_ls_Clave__c = 'ABT';
        acc.RecordTypeId = rtAccCF.Id;
        acc.OwnerId = usr.Id;
        insert acc;

        final Contract contrato = new Contract();
        contrato.AccountId =acc.Id;
        insert contrato;

        final ApexPages.StandardController stdCont = new ApexPages.StandardController(contrato);
        final BPyP_cls_ContractController contInstCtrl = new BPyP_cls_ContractController(stdCont);
        Test.startTest();
        	System.assert(contInstCtrl != null, 'Constructor created');
        Test.stopTest();
    }
}