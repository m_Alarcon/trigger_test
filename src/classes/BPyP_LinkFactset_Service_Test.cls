/**
* ------------------------------------------------------------------------------------------------
* @Name     	BPyP_LinkFactset_Service_Test
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-10-19
* @Description 	Test Class for BPyP LinkFactset Service Layer
* @Changes
*  
*/
@isTest
public class BPyP_LinkFactset_Service_Test {
    
    /*String nombre de permissionSet FactSet */
    static final String STR_FSETPS_NAME = 'MX_BPP_Factset';
    
    @testSetup
    static void setup() {
        final User usuarioAdm = UtilitysDataTest_tst.crearUsuario('PruebaAdmin', Label.MX_PERFIL_SystemAdministrator, 'BBVA ADMINISTRADOR');
        insert usuarioAdm;
    }
    
    /**
    * @Description 	Test Method for MX_BPP_Minuta_Service.BPyP_LinkFactset()
    * @Return 		NA
    **/
    @isTest
    static void testConstructor() {
        final BPyP_LinkFactset_Service instance = new BPyP_LinkFactset_Service();
        System.assertNotEquals(instance, null, 'Problema en constructor');
    }
    
    /**
    * @Description 	Test Method for MX_BPP_Minuta_Service.validateFactsetPermissionSet()
    * @Return 		NA
    **/
	@isTest
    static void validateFactsetPermissionSetTest() {
        final User userAdmin = [SELECT Id FROM User WHERE Name = 'PruebaAdmin' LIMIT 1];
        final PermissionSet pSetFactset = [SELECT Id FROM PermissionSet WHERE Name =:STR_FSETPS_NAME LIMIT 1];
        final PermissionSetAssignment pSetAssignment = new PermissionSetAssignment(PermissionSetId = pSetFactset.Id, AssigneeId = userAdmin.Id);
        insert pSetAssignment;
        
        Test.startTest();
        final Boolean access = BPyP_LinkFactset_Service.validateFactsetPermissionSet(userAdmin.Id);
        Test.stopTest();
        
        System.assert(access, 'PermissionSet no asignado');
    }

}