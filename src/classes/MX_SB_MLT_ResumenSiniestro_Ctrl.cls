/**
 * @File Name          : MX_SB_MLT_ResumenSiniestro_Ctrl.cls
 * @Description        :
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 5/27/2020, 02:07:22 AM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/27/2020    Juan Carlos Benitez         Initial Version
**/
public class MX_SB_MLT_ResumenSiniestro_Ctrl {
    /** Lista de registros de Siniestro */
	Final static List<Siniestro__c> SIN_DATA = new List<Siniestro__c>();
    /** Constructor */
    @TestVisible
    private MX_SB_MLT_ResumenSiniestro_Ctrl () { }
    /*
    * @description METHODO busqueda siniestro vida
    * @param String siniId
    */
    @AuraEnabled
    public static List<Siniestro__c> searchSinVida (String siniId) {
         try {
            if(String.isNotBlank(siniId)) {
                final Set<Id> SinIds = new Set<Id>{siniId};
                SIN_DATA.addAll(MX_RTL_Siniestro_Service.getSiniestroData(SinIds));
            }
        } catch(QueryException sinEx) {
            throw new AuraHandledException(System.Label.MX_WB_LG_ErrorBack + sinEx);
        }
        return SIN_DATA;
    }
}