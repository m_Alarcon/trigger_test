/**
 * @File Name          : MX_RTL_personLifeEvent_Service.cls
 * @Description        :
 * @Author             : Daniel Ramirez
 * @Group              :
 * @Last Modified By   : Daniel Ramirez
 * @Last Modified On   : 22/10/2020, 11:43:34 AM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    22/10/2020      Daniel Ramirez          Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_RTL_personLifeEvent_Service {
    /** Campos evento de vida */
    Final static String PERSEVFILDS = ' id, Name ';
    /**
     * 
    * @description return life cicle
    * @author Daniel Ramirez | 22/10/2020
    * @param Ids
    * @return List<Contact>
    **/
    public static List <PersonLifeEvent> getLifeEventbyContactId (Id idContact) {
        return MX_RTL_personLifeEvent_Selector.getPersonLifeEvent(idContact, PERSEVFILDS);
    }
}