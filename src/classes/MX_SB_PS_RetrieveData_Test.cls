/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_PS_RetrieveData_Test
* Autor Daniel Perez Lopez
* Proyecto: Salesforce Presuscritos
* Descripción : Prueba los Methods de la clase MX_SB_PS_RetrieveData_Test

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           10/12/2019      Daniel Lopez                         Creación
* --------------------------------------------------------------------------------
*/
@IsTest
@SuppressWarnings('sf:UnusedLocalVariable , sf:DUDataflowAnomalyAnalysis')
public with sharing class MX_SB_PS_RetrieveData_Test {
   /*
*Variable TUSER para creacion de usuario de pruebas nombre de usuario
*/
    public static final String TUSER = 'TestUser';
    /*
*Variable TACC para creacion de usuario de pruebas nombre de cuenta
*/
    public static final String TACC = 'TestAccount';
        /*
*Variable la particion iaso 
*/
public static final String PARTITION = 'iaso.ServicesPartition';
/*
* variable para url statica
*/
public static final String URLSERVICE='https://150.250.220.36:18500/lifeInsurances';

    /*
*Constructor de datos para pruebas
*/
    @TestSetup
    public static void setupTest() {
        MX_SB_PS_OpportunityTrigger_test.makeData();
        final Quotelineitem quoli= [Select id from QuoteLineItem limit 1];
        quoli.MX_SB_VTS_Version__c ='001,002';
        update quoli;
        Test.setMock(HttpCalloutMock.class, new MX_SB_PS_IntegrationServiceMock());
        iaso.GBL_Mock.setMock(new MX_SB_PS_IntegrationServiceMock());
        final String nameProfile = [SELECT Name from profile where name in ('Administrador del sistema','System Administrator') limit 1].Name;
        final User testingUsr = MX_WB_TestData_cls.crearUsuario(TUSER, nameProfile);
        System.runAs(testingUsr) {
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'GetCorporateCatalog',iaso__Cache_Partition__c = PARTITION,iaso__Url__c='https://150.250.220.36:18500/corporateCatalogs');
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'GetLifeInsurance_PS',iaso__Cache_Partition__c = PARTITION,iaso__Url__c=URLSERVICE);
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'createCustomerData_PS',iaso__Cache_Partition__c = PARTITION,iaso__Url__c=URLSERVICE);
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'GetListDirectionByCP',iaso__Cache_Partition__c = PARTITION,iaso__Url__c=URLSERVICE);
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'GTServicesSF',iaso__Cache_Partition__c = PARTITION,iaso__Url__c=URLSERVICE);
        }
    }
    
    @IsTest
    static void obtienePlanes() {
        Test.startTest();
        MX_SB_PS_wrpDatosParticularesResp resp = new MX_SB_PS_wrpDatosParticularesResp();
        try {
        	resp = (MX_SB_PS_wrpDatosParticularesResp) JSON.deserialize(MX_SB_PS_RetrieveData.obtieneplanes('VVSH0001'),MX_SB_PS_wrpDatosParticularesResp.class);
        } catch(Exception e) {
			System.assert(e.getMessage().contains('exception'),'Excepcion Exitosa');            
        }
        Test.stopTest();
    }
    
    @IsTest
    static void getLapsos() {
        final Opportunity oppObj =[Select id from opportunity limit 1];
        final Quote quoteOld= [Select id,MX_SB_VTS_Folio_Cotizacion__c from Quote limit 1];
        Test.startTest();
        MX_SB_PS_wrpCotizadorVidaResp resp = new MX_SB_PS_wrpCotizadorVidaResp();
        final Map<String, String> mapa = new Map<String, String>();
        mapa.put('VHSEDAD', '23');
        mapa.put('VHSSUMA', '5000');
        mapa.put('VHSPLAN', 'Conyugal');
        mapa.put('GFLPARENT', '002');
        final Map<String, String> mapa1 = new Map<String, String>();
        mapa1.put('planselected', 'Conyugal');
        mapa1.put('formapago', 'MENSUAL');
        mapa1.put('intval', '3000');
        mapa1.put('oppid',oppObj.Id);
        mapa1.put('idcotizaold',quoteOld.MX_SB_VTS_Folio_Cotizacion__c);
        try {
        resp = (MX_SB_PS_wrpCotizadorVidaResp) JSON.deserialize(MX_SB_PS_RetrieveData.getLapsos(mapa1, mapa),MX_SB_PS_wrpCotizadorVidaResp.class);
        } catch(Exception e) {
              system.assert(e.getmessage().contains('Script'),'Error esperado');
        }
        
        Test.stopTest();            
    }
    @isTest
    static void testproductplan() {
        final String oppId = [Select id from opportunity limit 1].id;
        Final String folioCot=[Select id, MX_WB_Folio_Cotizacion__c from QuotelineItem where Quote.OpportunityId=:oppId limit 1].MX_WB_Folio_Cotizacion__c;
        test.startTest();
        	MX_SB_PS_RetrieveData.productplan();
            final string quili2= MX_SB_PS_RetrieveData.obtieneQLI(oppId);
            system.assertEquals(quili2,folioCot,'Se ha encotnrado quotelineitem');
        test.stopTest();
    }
	@isTest
    static void testgetIdPart() {
        test.startTest();
        	final string retval=MX_SB_PS_RetrieveData.getIdPart('Individual');
            system.assertEquals(retval,'003','Se ha encotnrado quotelineitem');
        test.stopTest();
    }
        @IsTest
    static void getPartdata() {
        Test.startTest();
        try {
        	final MX_SB_PS_wrpDatosParticularesResp respuesta = MX_SB_PS_RetrieveData.obtienePrtData('234');
        } catch(Exception e) {
            system.assert(e.getMessage().contains('exception'),'Excepcion esperada');
        }
        Test.stopTest();            
    }
}