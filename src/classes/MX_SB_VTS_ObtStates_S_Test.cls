/**
* @description       : Clase de prueba para service: MX_SB_VTS_ObtStates_Service
* @author            : Diego Olvera
* @group             : BBVA
* @last modified on  : 02-19-2021
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   11-20-2020   Diego Olvera   Initial Version
**/
@isTest
public without sharing class MX_SB_VTS_ObtStates_S_Test {
    /**@description Nombre usuario*/
    private final static String ASESORSERVICE = 'AsesorTest';
    /**@description Nombre del estado*/
    private final static String ESTADOSERVICE = 'Chiapas';
    
    @TestSetup
    static void makeData() {
        final User userRecServ = MX_WB_TestData_cls.crearUsuario(ASESORSERVICE, 'System Administrator');
        insert userRecServ;
    }
    @isTest
    static void obtStatesValueSer() {
        final User userObtSer = [Select Id from User where LastName =: ASESORSERVICE];
        System.runAs(userObtSer) {
            Test.startTest();
            try {
                MX_SB_VTS_ObtStates_Service.getDirValue(ESTADOSERVICE);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Error');
                System.assertEquals('Error', extError.getMessage(),'no recuperó los estados serv');
            }
            Test.stopTest();
        }
    }
}