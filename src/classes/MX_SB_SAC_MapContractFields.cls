/**
 * @File Name          : MX_SB_SAC_MapContractFields.cls
 * @Description        : 
 * @Author             : Jaime Terrats
 * @Group              : 
 * @Last Modified By   : Jaime Terrats
 * @Last Modified On   : 12/21/2019, 5:24:54 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/21/2019   Jaime Terrats     Initial Version
**/
public without sharing class MX_SB_SAC_MapContractFields {
    /** constructor */
    private MX_SB_SAC_MapContractFields() {} // NOSONAR

    /**
    * @description 
    * @author Jaime Terrats | 12/21/2019 
    * @param newContracts 
    * @return void 
    **/
    public static void mapFields(List<Contract> newContracts) {
        for(Contract nContract : newContracts) {
                nContract.MX_SB_SAC_NombreClienteAseguradoText__c = formatData(nContract.MX_WB_nombreAsegurado__c);
                nContract.MX_SB_SAC_NombreCuentaText__c = formatData(nContract.MX_SB_SAC_NombreCuenta__c);
                nContract.MX_SB_SAC_NumeroPoliza__c = formatData(nContract.MX_WB_noPoliza__c);
                nContract.MX_SB_SAC_NumeroSerie__c = formatData(nContract.MX_WB_numeroSerie__c);
                nContract.MX_SB_SB_Placas__c = formatData(nContract.MX_WB_placas__c);
                nContract.MX_SB_SAC_EmailAsegurado__c = formatData(nContract.MX_WB_emailAsegurado__c);
                nContract.MX_SB_SAC_RFCContratanteText__c = formatData(nContract.MX_SB_SAC_RFCContratante__c);
        }
    }

    /**
    * @description 
    * @author Jaime Terrats | 12/21/2019 
    * @param value 
    * @return String 
    **/
    private static String formatData(String value) {
        return String.isNotBlank(value) ? value.toUpperCase() : '';
    }
}