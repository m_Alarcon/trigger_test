/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 03-19-2021
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   01-04-2021   Eduardo Hernández Cuamatzi   Initial Version
 * 1.1   02-23-2021   Eduardo Hernández Cuamatzi   Se eliminan valores nuevos para coberturas eliminadas
 * 1.2   02-03-2021   Diego Olvera                 Ajuste a funcion para update de registros
 * 1.2.1 19-03-2021   Diego Olvera                 Ajuste a funcion para recuperar valor de categoria desde front
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_DeleteCoverageHSD_ctrl {

    /**String data Opps */
    private final static String DATAOPP = 'dataOpp';
    /**String para is Ok en results */
    private final static String ISOK = 'isOk';
    /**String para data quote */
    private final static String DATAQUOTESTR = 'dataQuote';
    /**String para data quote */
    private final static String DATACOTSTR = 'dataCot'  ;
    /** String para guardar valor de tipo de Trade zona Hidro */
    static final String VALRIHI = 'RIHI';
    /** String para guardar valor de tipo de Trade zona Sismica */
    static final String VALTERR = 'TERR';

    /**
    * @description Remueve coberturas de de una cotización
    * @author Eduardo Hernández Cuamatzi | 12-29-2020 
    * @param oppId Id de la Oportunidad trabajada
    * @param coveragesCodes Códigos de coberturas a eliminar
    * @param masterTrade Código cobertura principal
    * @return Map<String, Object> Respuesta de la petición del servicio
    **/
    @AuraEnabled
    public static Map<String, Object> removeCoverages(String oppId, List<String> coveragesCodes, Map<String, Object> rDataCob) {
        final Map<String, Object> response = new Map<String, Object>();
        try {
            final String masterTrade = rDataCob.get('ramo').toString();
            final Map<String, Object> cotizData = fillCotizData(oppId);
            List<Cobertura__c> mapCoverages = new List<Cobertura__c>();
            if(masterTrade == VALRIHI || masterTrade == VALTERR) {
                final String mCategorie = rDataCob.get('categorie').toString();
                coveragesCodes.add(mCategorie);
                mapCoverages = MX_SB_VTS_AddCoberturas_Service.findByTrade(((List<Opportunity>)cotizData.get(DATAOPP))[0].SyncedQuoteId, masterTrade, coveragesCodes);
            } else {
                mapCoverages = MX_SB_VTS_DeleteCoverageHSD_Helper.findCoverCodes(((List<Opportunity>)cotizData.get(DATAOPP))[0].SyncedQuoteId, coveragesCodes);
            }
            final Quote dataQuote = ((Map<Id, Quote>)cotizData.get(DATAQUOTESTR)).get(((List<Opportunity>)cotizData.get(DATAOPP))[0].SyncedQuoteId);
            final Map<String, Object> bodyDeleteCov = MX_SB_VTS_DeleteCoverageHSD_Services.fillRemoveCoverages(mapCoverages, ((List<Opportunity>)cotizData.get(DATAOPP))[0], masterTrade);
            final Map<String, Object> responServices = MX_SB_VTS_DeleteCoverageHSD_Services.processCoverages(JSON.serialize(bodyDeleteCov), dataQuote.MX_SB_VTS_ASO_FolioCot__c);
            if((Boolean)responServices.get('responseOk')) {
                final Map<String,Object> responseServ = (Map<String, Object>)Json.deserializeUntyped((String)responServices.get('getData'));
                response.put('dataPayments', MX_SB_VTS_DeleteCoverageHSD_Services.processResponse(JSON.serialize(responseServ.get('data')), dataQuote.Id));
                final List<String> paramsCov = new List<String>();
                MX_SB_VTS_DeleteCoverageHSD_Services.updateCoverage(mapCoverages, false, paramsCov);
            }
            response.put(ISOK, true);
            response.put(DATACOTSTR, fillCotizData(oppId));
        } catch (QueryException ex) {
            response.put(ISOK, false);
            response.put('messageError', ex.getMessage());
        }
        return response;
    }

    /**
    * @description Llena datos de una cotización
    * @author Eduardo Hernández Cuamatzi | 12-29-2020 
    * @param oppId Id de registro trabajado
    * @return Map<String, Object> Datos recuperados
    **/
    @AuraEnabled
    public static Map<String, Object> fillCotizData(String oppId) {
        final Map<String, Object> mapData = new Map<String, Object>();
        final Set<String> setOpps = new Set<String>{oppId};
        final List<Opportunity> lstOpps = MX_SB_VTS_CotizInitData_Service.findOppsById(setOpps);
        final Map<Id, Quote> mapQuotes = MX_SB_VTS_CotizInitData_Service.findQuotesByOppMap(lstOpps);
        final Map<Id, QuoteLineItem> lstQuoteLi = MX_SB_VTS_CotizInitData_Service.findQuolisByQuotMap(mapQuotes);
        mapData.put(DATAOPP, lstOpps);
        mapData.put(DATAQUOTESTR, mapQuotes);
        mapData.put('dataQuoli', lstQuoteLi);
        return mapData;
    }
    
    /**
    * @description Remueve coberturas de de una cotización
    * @author Eduardo Hernández Cuamatzi | 12-29-2020 
    * @param oppId Id de la Oportunidad trabajada
    * @param coveragesCodes Códigos de coberturas a eliminar
    * @param masterTrade Código cobertura principal
    * @return Map<String, Object> Respuesta de la petición del servicio
    **/
    @AuraEnabled
    public static Map<String, Object> editPetsHSD(String oppId, List<String> lstPartData, List<String> valuesCriterial, Boolean coverActive) {
        final Map<String, Object> response = new Map<String, Object>();
        try {
            final Map<String, Object> cotizData = fillCotizData(oppId);
            final Map<Id, List<Cobertura__c>> mapCoverages = MX_SB_VTS_DeleteCoverageHSD_Services.findContractingCriteria(((Map<Id, Quote>)cotizData.get(DATAQUOTESTR)), 'MX_SB_VTS_ContractingCriteria');
            final Quote dataQuote = ((Map<Id, Quote>)cotizData.get(DATAQUOTESTR)).get(((List<Opportunity>)cotizData.get(DATAOPP))[0].SyncedQuoteId);
            final Map<String, Object> bodyDeleteCov = MX_SB_VTS_DeleteCoverageHSD_Services.fillEditPets(mapCoverages.get(dataQuote.Id) , ((List<Opportunity>)cotizData.get(DATAOPP))[0], valuesCriterial, lstPartData);
            final Map<String, Object> responServices = MX_SB_VTS_DeleteCoverageHSD_Services.processCoverages(JSON.serialize(bodyDeleteCov), dataQuote.MX_SB_VTS_ASO_FolioCot__c);
            response.put(ISOK, true);
            if((Boolean)responServices.get('responseOk')) {
                final Map<String,Object> responseServ = (Map<String, Object>)Json.deserializeUntyped((String)responServices.get('getData'));
                response.put('dataPayments', MX_SB_VTS_DeleteCoverageHSD_Services.processResponse(JSON.serialize(responseServ.get('data')), dataQuote.Id));
                final List<Cobertura__c> mapCoveragesCodes = MX_SB_VTS_DeleteCoverageHSD_Helper.findCoverCodes(((List<Opportunity>)cotizData.get(DATAOPP))[0].SyncedQuoteId, lstPartData);
                if (lstPartData.contains('CODIGO_CUPON')) {
                    response.put('discounts', MX_SB_VTS_DeleteCoverageHSD_Services.processCupon(JSON.serialize(responseServ.get('data')), mapCoveragesCodes, valuesCriterial));
                } else {
                    MX_SB_VTS_DeleteCoverageHSD_Services.updateCoverage(mapCoveragesCodes, coverActive, valuesCriterial);
                }
            }
            response.put(DATACOTSTR, fillCotizData(oppId));
        } catch (QueryException ex) {
            response.put(ISOK, false);
            response.put('messageError', ex.getMessage());
        }
        return response;
    }

    /**
    * @description Recupera todas coberturas ligadas a una cotizacion
    * @author Eduardo Hernández Cuamatzi | 01-08-2021 
    * @param oppId 
    * @param quoteNameId 
    * @return Map<String, Object> 
    **/
    @AuraEnabled
    public static Map<String, Object> fillAllDataCovers(String oppId, String quoteNameId) {
        final Map<String, Object> response = new Map<String, Object>();
        final Set<String> setOpps = new Set<String>{oppId};
        final List<Opportunity> lstOpps = MX_SB_VTS_CotizInitData_Service.findOppsById(setOpps);
        final Map<Id, Quote> mapQuotes = MX_SB_VTS_CotizInitData_Service.findQuotesByOppMap(lstOpps);
        response.put(DATACOTSTR, fillCotizData(oppId));
        final String quoteName = lstOpps[0].LeadSource+'-'+quoteNameId;
        final Map<Id, Quote> mapQuotesSync = new Map<Id, Quote>();
        for (Id quoteIdKey : mapQuotes.keySet()) {
            if(mapQuotes.get(quoteIdKey).QuoteToName.equalsIgnoreCase(quoteName)) {
                mapQuotesSync.put(quoteIdKey, mapQuotes.get(quoteIdKey));
            }
        }
        final Map<Id, List<Cobertura__c>> mapCrits = MX_SB_VTS_DeleteCoverageHSD_Services.findContractingCriteria(mapQuotesSync, 'MX_SB_VTS_ContractingCriteria');
        final Map<Id, List<Cobertura__c>> mapCoverages = MX_SB_VTS_DeleteCoverageHSD_Services.findContractingCriteria(mapQuotesSync, 'MX_SB_VTS_CoberturasASO');
        response.put('criterials', MX_SB_VTS_DeleteCoverageHSD_Helper.fillMapCritical(evaluateList(mapCrits)));
        response.put('coverages', MX_SB_VTS_FillCoveragesTrades_helper.fillCoverages(evaluateList(mapCoverages)));
        return response;
    }


    /**
    * @description Evalua Mapa de coberturas
    * @author Eduardo Hernández Cuamatzi | 01-28-2021 
    * @param mapCoverages Mapa de coberturas por Quote
    * @return List<Cobertura__c> Lista de coberturas totales
    **/
    public static List<Cobertura__c> evaluateList(Map<Id, List<Cobertura__c>> mapCoverages) {
        List<Cobertura__c> lstCovers = new List<Cobertura__c>();
        for(List<Cobertura__c> lstCober : mapCoverages.values()) {
            lstCovers = lstCober;
        }
        return lstCovers;
    }

    /**
    * @description Sincroniza una quote con su Oportunidad mediante el nombre de la cotización
    * @author Eduardo Hernández Cuamatzi | 02-04-2021 
    * @param oppId Id de la Oportunidad 
    * @param quoteNameId Nombre de la cotización a buscar
    * @return Map<String, Object> Mapa de resultados
    **/
    @AuraEnabled
    public static Map<String, Object> syncQuoteByType(String oppId, String quoteNameId) {
        final Map<String, Object> response = new Map<String, Object>();
        final Set<String> setOpps = new Set<String>{oppId};
        final List<Opportunity> lstOpps = MX_SB_VTS_CotizInitData_Service.findOppsById(setOpps);
        response.put(DATACOTSTR, fillCotizData(oppId));
        final String quoteName = lstOpps[0].LeadSource+'-'+quoteNameId;
        final List<Quote> mapQuotes = MX_SB_VTS_CotizInitData_Service.findQuotesByOpp(lstOpps);
        for(Opportunity lsrCurrOpp : lstOpps) {
            for(Quote lstUpdQuo : mapQuotes) {
                if(lstUpdQuo.QuoteToName.equalsIgnoreCase(quoteName)) {
                    lsrCurrOpp.SyncedQuoteId = lstUpdQuo.Id;   
                }
            }
        }
        MX_RTL_Opportunity_Selector.updateResult(lstOpps, false);
        response.put(ISOK, true);
        return response;
    }
}