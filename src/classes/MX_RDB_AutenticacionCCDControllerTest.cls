/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RDB_AutenticacionCCDControllerTest
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2019-10-28
* @Group  		MX_RDB_AutenticacionCCD
* @Description 	Test class for MX_RDB_AutenticacionCCDController
* @Changes
*  
*/
@isTest
public class MX_RDB_AutenticacionCCDControllerTest {
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test method for the method MX_RDB_AutenticacionCCDController.getClienteData
    * @return N/A
    **/
	@isTest
    static void getClienteDataTst() {
        // Create test records
        final Id rtOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('MX_SB_BCT_Banquero_Opp').getRecordTypeId();

        final Account acc = new Account();
        acc.FirstName = 'Test Name';
        acc.LastName = 'Test Last';
        acc.PersonEmail = 'test@testbbva.com';
        acc.PersonMobilePhone = '5551040002';
        acc.PersonHomePhone = '5551040002';
        insert acc;

        final Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.StageName = 'Closed Won';
        opp.CloseDate = Date.today().addDays(1);
        opp.Producto__c = 'Auto Seguro Dinamico';
        opp.Reason__c = 'Venta';
        opp.MX_SB_VTS_Aplica_Cierre__c = true;
        opp.Estatus__c = 'Emitida';
        opp.LeadSource = 'Tracking Web';
        opp.FolioCotizacion__c = 'Test123';
        opp.Plan__c = 'Basico';
        opp.AccountId = acc.Id;
        opp.RecordTypeId = rtOpp;
        insert opp;
        final Account resultNoClient;
            
        Test.startTest();
        Exception noClientExc;
        try {
        	resultNoClient = MX_RDB_AutenticacionCCDController.getClienteData(null);
        } catch (Exception e) {
			noClientExc = e;
        }
        final Account resultClient = MX_RDB_AutenticacionCCDController.getClienteData(opp.Id);
        Test.stopTest();
        
        System.assertEquals(null, resultNoClient, 'Conflicto con null value');
        System.assertEquals(acc.Id, resultClient.Id, 'Conflicto al obtener la infromación del Cliente');
        System.assert(noClientExc <> null, 'Catch no en test');
    }

	/**
    * --------------------------------------------------------------------------------------
    * @Description Test method for the method MX_RDB_AutenticacionCCDController.getGTicket
    * @return N/A
    **/    
    @isTest
    static void getGTicketTst() {
        Test.startTest();
        final MX_RDB_grantingTicket__mdt gtTest = MX_RDB_AutenticacionCCDController.rGTicket();
        Test.stopTest();
        
        System.assertEquals('grantingTicket', gtTest.Label, 'Problema al obtener el valor del CustomMetadataType');
    }
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test method for the method MX_RDB_AutenticacionCCDController.getRNumber
    * @return N/A
    **/
    @isTest
    static void getRNumberTst() {
        Test.startTest();
        final MX_RDB_shortReferenceNumber__mdt srnTest = MX_RDB_AutenticacionCCDController.rRNumber();
        Test.stopTest();
        
        System.assertEquals('shortReferenceNumber', srnTest.Label, 'Problema al obtener el valor del CustomMetadataType');
    }
}