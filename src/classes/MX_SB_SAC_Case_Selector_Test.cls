/**
 * @description       : Class test MX_SB_SAC_Case_Selector.cls
 * @author            : Gerardo Mendoza Aguilar
 * @group             : 
 * @last modified on  : 02-16-2021
 * @last modified by  : Gerardo Mendoza Aguilar
 * Modifications Log 
 * Ver   Date         Author                    Modification
 * 1.0   02-16-2021   Gerardo Mendoza Aguilar   Initial Version
**/
@isTest
public class MX_SB_SAC_Case_Selector_Test {
 /**
    * @description 
    * @author Gerardo Mendoza Aguilar | 02-16-2021 
    **/
    @TestSetup
    static void makeData() { 
        Final String rType2 = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MX_SB_SAC_RT').getRecordTypeId();
        Final Case caseData2 =  new Case(
            RecordTypeId = rType2
            , MX_SB_SAC_Nombre__c = 'Clientes Test'
            , Origin = 'Teléfono'
            , MX_SB_SAC_TipoContacto__c = 'Asegurado'
            , Reason = 'Cancelación'
            , MX_SB_SAC_Aplicar_Mala_Venta__c = 'No'
            , MX_SB_SAC_Estado__c = 'Ciudad de México'
        );
        insert caseData2;
    }
    /**
    * @description 
    * @author Gerardo Mendoza Aguilar | 02-16-2021 
    **/
    @isTest
    public static void updateCase() {
        Test.startTest();
        Final List<Case> caseList2 = [Select Id, MX_SB_1500_isAuthenticated__c  from Case limit 1];
        caseList2[0].MX_SB_1500_isAuthenticated__c = true;
        MX_SB_SAC_Case_Selector.updateCase(caseList2);
        System.assert(!caseList2.isEmpty(), 'Caso actualizado');
        Test.stopTest();
    }
}