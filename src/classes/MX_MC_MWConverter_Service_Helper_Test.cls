/**
 * @File Name          : MX_MC_MWConverter_Service_Helper_Test.cls
 * @Description        : Clase para Test de MX_MC_MWConverter_Service_Helper
 * @author             : Eduardo Barrera Martínez
 * @Group              : BPyP
 * @Last Modified By   : Eduardo Barrera Martínez
 * @Last Modified On   : 28/07/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      28/07/2020           Eduardo Barrera Martínez.       Initial Version
**/
@isTest
private class MX_MC_MWConverter_Service_Helper_Test {

    /**
     * @Description Clase de prueba para MX_MC_MWConverter_Service_Helper
     * @author eduardo.barrera3@bbva.com | 28/07/2020
    **/
    @isTest
    static void createMessageThread() {
        final MX_MC_MWConverter_Service_Helper.MessageThread serviceHelper = new  MX_MC_MWConverter_Service_Helper.MessageThread();
        serviceHelper.message = 'Test';
        serviceHelper.isDraft = false;
        serviceHelper.attachments = null;
        serviceHelper.toJson();
        System.assert(serviceHelper != null, 'Objeto instanciado incorrectamente');
    }
}