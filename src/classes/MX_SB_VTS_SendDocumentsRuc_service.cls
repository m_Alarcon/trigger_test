/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 11-06-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   11-06-2020   Eduardo Hernández Cuamatzi   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_SB_VTS_SendDocumentsRuc_service {
    /**Codigos aceptables de respuesta */
    final static List<Integer> HTTPOK = new List<Integer>{201,200,202,203,204};

    /**
    * @description Recupera documento de poliza
    * @author Eduardo Hernández Cuamatzi | 11-10-2020 
    * @param folioPolizAso folio de poliza emitida
    * @return Map<String, Object> Mapa de resultados de consulta documentos
    **/
    public static Map<String, Object> sGetCustomerDocument(String folioPolizAso, Opportunity oppdata, String docType) {
        final Map<String, Object> respGetDocs = new Map<String, Object>();
        final Map<String, String> dataValuesDoc = MX_SB_VTS_SendDocumentsRuc_Helper.fillDataDocs(oppdata, docType);
        final HttpRequest requs = fillBodyCustomDocument(folioPolizAso, dataValuesDoc, docType, oppdata.Producto__c);
        try {
            final Map<String, Object> emptyMap = new Map<String, Object>();
            final HttpResponse response = MX_RTL_IasoServicesInvoke_Selector.callServices('getCustomerDocument', emptyMap, requs);
            final Integer statusCodeInt = response.getStatusCode();
            Boolean isASOoK= false;
            if(HTTPOK.contains(statusCodeInt)) {
                final Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
                final WrapDocumentRes docResponse = processResponse(responseMap);
                respGetDocs.put('documentSrc', docResponse.hrefDoc);
                isASOoK= true;
            }
            respGetDocs.put('docOk', isASOoK);
        } catch (CalloutException callEx) {
            respGetDocs.put('docOk', false);
            respGetDocs.put('callEx', callEx.getCause());
            respGetDocs.put('callExMsj', callEx.getMessage());
        }
        return respGetDocs;
    }

        /**
    * @description Construye request para peticion por paquete iaso
    * @author Eduardo Hernández Cuamatzi | 11-10-2020 
    * @param bodySendDoc Datos del body a enviar
    * @return HttpRequest Request personalizado
    **/
    public static HttpRequest fillGetCustomDocument(Map<String, Object> bodySendDoc) {
        final String mapStr = JSON.serialize(bodySendDoc);
        final HttpRequest requestDoc = new HttpRequest();
        final Set<String> lstTypesDoc = new Set<String>{'IASO01'};
        final List<MX_SB_VTS_Generica__c> dataGenDoc = MX_SB_VTS_Generica_Selector.findByTypeVal('Name, MX_SB_VTS_HEADER__c', lstTypesDoc);
        final Map<String,iaso__GBL_Rest_Services_Url__c> allCodesDoc = iaso__GBL_Rest_Services_Url__c.getAll();
        final iaso__GBL_Rest_Services_Url__c restGetDoc = allCodesDoc.get('getCustomerDocument');
        final String endPoint = restGetDoc.iaso__Url__c+'getCustomerDocument';
        requestDoc.setTimeout(120000);
        requestDoc.setMethod('POST');
        requestDoc.setHeader('Content-Type', 'application/json');
        requestDoc.setHeader('Accept', '*/*');
        requestDoc.setHeader('Host', dataGenDoc[0].MX_SB_VTS_HEADER__c);
        requestDoc.setEndpoint(endPoint);
        requestDoc.setBody(mapStr);
        return requestDoc;
    }

    /**
    * @description Llena elemento de types para Documentos
    * @author Eduardo Hernández Cuamatzi | 11-10-2020 
    * @param folioCotAso Folio poliza
    * @return String Cuerpo de elementName para recuperar servicio
    **/
    public static HttpRequest fillBodyCustomDocument(String folioCotAso, Map<String, String> mapDocuments, String proceso, String product) {
        final Map<String, Object> mapBody = new Map<String, Object>();
        mapBody.put('productId', mapDocuments.get('productId'));
        mapBody.put('version', mapDocuments.get('version'));
        mapBody.put('documentType', 'PDF');
        List<Object> listContent = new List<Object>();
        final Map<String, Object> mapContents = new Map<String, Object>();
        mapContents.put('elementName', mapDocuments.get('elementName'));
        final List<Map<String, Object>> listData = new List<Map<String, Object>>();
        listData.add(MX_SB_VTS_SendDocumentsRuc_Helper.fillElement(mapDocuments.get('element1'), mapDocuments.get('nameElement'), ''));
        final String oppProduct = product.toUpperCase();
        if(proceso.equalsIgnoreCase('policy')) {
            switch on oppProduct {
                when  'VIDA SEGURA DINÁMICO' {
                    listData.add(MX_SB_VTS_SendDocumentsRuc_Helper.fillElement('quoteId', folioCotAso, 'name'));
                }
                when 'HOGAR SEGURO DINÁMICO' {
                    listData.add(MX_SB_VTS_SendDocumentsRuc_Helper.fillElement('quoteId', folioCotAso, ''));
                }
            }
        }
        mapContents.put('listData', listData);
        listContent.add(mapContents);
        switch on oppProduct {
            when  'VIDA SEGURA DINÁMICO'{
                listContent = MX_SB_VTS_SendDocumentsRuc_Helper.processExtraElements(proceso, product, listContent);
            }
        }
        mapBody.put('listContent', listContent);
        return fillGetCustomDocument(mapBody);
    }

    /**
    * @description Procesa respuesta de generar documentos
    * @author Eduardo Hernández Cuamatzi | 11-10-2020 
    * @param responseMap Datos de respuesta de getCustomerDocuments
    * @return WrapDocumentRes Objeto que almacena datos de respuesta
    **/
    public static WrapDocumentRes processResponse(Map<String, Object> responseMap) {
        final WrapDocumentRes docResponse = new WrapDocumentRes();
        docResponse.hrefDoc = (String)responseMap.get('href');
        final Map<String, Object> mapData = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(responseMap.get('document')));
        final WrapDocument wrapDocs = new WrapDocument();
        wrapDocs.dataDoc = (String)mapData.get('data');
        wrapDocs.sizeDoc = (Integer)mapData.get('size');
        wrapDocs.idDoc = (String)mapData.get('id');
        docResponse.dataDoc = wrapDocs;
        return docResponse;
    }

    
    /**
    * @description Envio de correos por Gestor de eventos
    * @author Eduardo Hernández Cuamatzi | 11-10-2020 
    * @param quoteData Datos de cotizacion
    * @param wrapDoc datos de caratulas
    * @param lstProcSend Valores de proceso y producto a enviar
    * @return Boolean Envio exitoso o fallido
    **/
    public static Boolean sendDocumentByEmail(Quote quoteData, List<String> wrapDoc, List<String> lstProcSend) {
        final Map<String, Object> bodySendDoc = fillDocumentEmail(quoteData, wrapDoc, lstProcSend);
        Boolean envioOk = false;
        try {
            final HttpRequest requs = consultService(bodySendDoc);
            final Map<String, Object> bodyMapsReq = new Map<String, Object>();
            final HttpResponse response = MX_RTL_IasoServicesInvoke_Selector.callServices('sendDocumentsByMail', bodyMapsReq, requs);
            final Integer statusCodeInt = response.getStatusCode();
            if(HTTPOK.contains(statusCodeInt)) {
                envioOk = true;
            }
        } catch (CalloutException callEx) {
            envioOk = false;
        }
        return envioOk;
    }

    /**
    * @description Construye mapa de peticion
    * @author Eduardo Hernández Cuamatzi | 11-10-2020 
    * @param quoteData Datos de cotizacion
    * @param documentClients Datos de Documentos RUC
    * @param lstProcSend Datos para producto y asunto
    * @return Map<String, Object> 
    **/
    public static Map<String, Object> fillDocumentEmail(Quote quoteData, List<String> documentClients, List<String> lstProcSend) {
        final Map<String, Object> fillDocMap = new Map<String, Object>();
        String fullName = MX_SB_VTS_SendLead_helper_cls.returnEmpty(quoteData.MX_SB_VTS_Nombre_Contrante__c)+
            ' ' + MX_SB_VTS_SendLead_helper_cls.returnEmpty(quoteData.MX_SB_VTS_Apellido_Paterno_Contratante__c) + 
            ' ' + MX_SB_VTS_SendLead_helper_cls.returnEmpty(quoteData.MX_SB_VTS_Apellido_Materno_Contratante__c);
        fullName += '|'+quoteData.MX_SB_VTS_Numero_de_Poliza__c;
        final Map<String, String> customerName = new Map<String, String>();
        customerName.put('name', fullName);
        fillDocMap.put('customerName', customerName);
        fillDocMap.put('pass', quoteData.MX_SB_VTS_RFC__c);
        fillDocMap.put('productId', lstProcSend[0]);
        fillDocMap.put('subject', lstProcSend[1]);
        final List<String> lstHref = new List<String>();
        lstHref.addAll(documentClients);
        fillDocMap.put('listHref', lstHref);
        final List<String> lstailReceiver = new List<String>{quoteData.MX_SB_VTS_Email_txt__c};
        fillDocMap.put('mailReceiver', lstailReceiver);
        return fillDocMap;
    }

    /**
    * @description Construye request para peticion por paquete iaso
    * @author Eduardo Hernández Cuamatzi | 11-10-2020 
    * @param bodySendDoc Datos del body a enviar
    * @return HttpRequest Request personalizado
    **/
    public static HttpRequest consultService(Map<String, Object> bodySendDoc) {
        final String mapStr = JSON.serialize(bodySendDoc);
        final HttpRequest request = new HttpRequest();
        final Set<String> lstTypes = new Set<String>{'IASO01'};
        final List<MX_SB_VTS_Generica__c> dataGen = MX_SB_VTS_Generica_Selector.findByTypeVal('Name, MX_SB_VTS_HEADER__c', lstTypes);
        final Map<String,iaso__GBL_Rest_Services_Url__c> allCodes = iaso__GBL_Rest_Services_Url__c.getAll();
        final iaso__GBL_Rest_Services_Url__c restSendByEmail = allCodes.get('sendDocumentsByMail');
        final String endPoint = restSendByEmail.iaso__Url__c+'sendDocumentsByMail';
        request.setTimeout(120000);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Accept', '*/*');
        request.setHeader('Host', dataGen[0].MX_SB_VTS_HEADER__c);
        request.setEndpoint(endPoint);
        request.setBody(mapStr);
        return request;
    }

    /**Clase wrapper para datos de documentos*/
    public class WrapDocumentRes {
        /**Objeto de documetos */
        public WrapDocument dataDoc {get;set;}
        /**Link de repositorio */
        public String hrefDoc {get;set;}        
    }

    /**Datos documento RUC */
    public class WrapDocument {
        /**Documento en base64*/
        public String dataDoc {get;set;}
        /**Tamaño del documento */
        public Integer sizeDoc {get;set;}
        /**Id documento generado */
        public String idDoc {get;set;}
    }
}