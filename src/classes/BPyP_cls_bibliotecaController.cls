/**
*Desarrollado por:       Indra
*Autor:                  Abraham Tinajero
*Proyecto:               BPyP
*Descripción:            Visualiza Los links de los documentos delos productos
*
*+Cambios (Versiones)
*+-------------------------------------------------------------------------------
*+No.         Fecha           Autor                           Descripción
*------   ----------------   --------------------            ---------------
*1.0     21-10-2015         Abraham Tinajero                  Creación
*1.1.	 22-11-2019			Gabriel García					  Corrección de codesmells
**/
public with sharing class BPyP_cls_bibliotecaController {
    /** opntion variable */
    @TestVisible
    private transient List<SelectOption> options ;
    String oSelectOpt;
    /**
     * Variable Item get; set;
	*/
    public transient List<SelectOption> items {
        get {
            if(options == null) {
                System.debug('Ingreso a if');
                options = new List<SelectOption>();
                for(AggregateResult opp: [SELECT MX_RTL_Producto__c FROM OPPORTUNITY WHERE RECORDTYPE.NAME='BPyP' AND (NOT(MX_RTL_Producto__c = '')) group by MX_RTL_Producto__c ]) {
                    oSelectOpt = String.valueOf(opp.get('MX_RTL_Producto__c'));
                    System.debug('oSelectOpt 1'+oSelectOpt);
                    options.add(new SelectOption(oSelectOpt,oSelectOpt));
                }
            }
            System.debug('options '+options);
            return options;
        }
        set {
            this.items = items;
        }
    }

    /**
     * Variable countries get; set;
	*/
    public transient String[] countries {
        get {
        	return countries;
        }
        set {
            this.countries = countries;
        }
    }
}