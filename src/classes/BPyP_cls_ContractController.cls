/**
*Desarrollado por:       Indra
*Autor:                  Abraham Tinajero
*Proyecto:               BPyP
*Descripción:            Visualiza el detalle del objeto de contrato
*
*+Cambios (Versiones)
*+-------------------------------------------------------------------------------
*+No.         Fecha           Autor                           Descripción
*------   ----------------   --------------------            ---------------
*1.0     21-10-2015         Abraham Tinajero                  Creación
**/

public with sharing class BPyP_cls_ContractController {
    /**Contract getter setter */
    public Contract contractBPyP {get; set;}
    /**Account getter setter */
    public Account clientBPyP {get; set;}

    /**
    *Contructor method
	**/
    public  BPyP_cls_ContractController(ApexPages.StandardController strController) {

        final Id contractId = (Id) strController.getRecord().Id;
        this.contractBPyP = [Select Id, ContractNumber,
        					AccountId,
                            BPyP_Un_Contrato__c,
                            BPyP_tx_Servicio_inversion__c,
                            BPyP_tx_Perfil_calf__c,
                            BPyP_tx_Lim__c,
                            BPyP_tx_DCP__c,
                            BPyP_pr_DCPRE__c,
                            BPyP_tx_DLP__c,
                            BPyP_pr_DLPRE__c,
                            BPyP_tx_RV__c,
                            BPyP_pr_RVRE__c,
                            BPyP_tx_PP__c,
                            BPyP_pr_PPRE__c,
                            BPyP_tx_NE__c,
                            BPyP_pr_NERE__c,
                            BPyP_tx_FCKD__c,
                            BPyP_pr_FCKDRE__c,
                            BPyP_tx_EC__c,
                            BPyP_tx_RA1__c,
                            BPyP_pr_RA1RE__c,
                            BPyP_tx_RA2__c,
                            BPyP_pr_RA2RE__c,
                            BPyP_tx_RB__c,
                            BPyP_pr_RBRE__c,
                            BPyP_tx_RC__c,
                            BPyP_pr_RCRE__c,
                            BPyP_tx_Razonabilidad__c,
                            BPyP_Estado_contrato__c from Contract where Id =: contractId];

        this.clientBPyP = [Select Id, Name,BPyP_tx_Clasif_cli__c, BPyP_Un_Numero_Promotor__c from Account where Id =: contractBPyP.AccountId];

        }
}