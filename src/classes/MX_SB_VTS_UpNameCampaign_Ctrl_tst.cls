/**
* Name: MX_SB_VTS_UpNameCampaign_Ctrl_tst
* @author Ángel Lavana Rosas
* Description : New Class to test class  MX_SB_VTS_UpNameCampaign_Ctrl, create records with Name _CPN
*				and records without _CPN
* Ver                  Date            Author                   Description
* @version 1.0         Jun/02/2020     Ángel Lavana Rosas       Initial Version
**/
@isTest
public class MX_SB_VTS_UpNameCampaign_Ctrl_tst {
    /* @Description: Create test data set
	*/
    @isTest
    public static void nameCampaignCtrlCPN() {
        final List<Id> leadsId1 = new List<Id>{}; 
            final List<lead> leads1 = new List<Lead>{};    
                
                for(integer contador1 = 0; contador1<=5; contador1++) {    
                    final Lead candidato1 = new Lead(FirstName = 'Campaña' + contador1, 
                                              LastName = 'prueba' + contador1,  
                                              MX_SB_VTS_CampaFaceName__c = 'SEGUROS WIBE BBVA CPNWHOOP0' + contador1 ,
                                              MX_SB_VTS_CodCampaFace__c = '',
                                              MX_SB_VTS_DetailCampaFace__c = 'ANUNCIO POR FACEBOOK ' + contador1);
                    leads1.add(candidato1);
                }
                for(integer contador1 = 0; contador1<=5; contador1++) {    
                    final Lead candidato1 = new Lead(FirstName = 'Campaña' + contador1, 
                                              LastName = 'prueba' + contador1,  
                                              MX_SB_VTS_CampaFaceName__c = 'SEGUROS WIBE BBVA' + contador1 ,
                                              MX_SB_VTS_CodCampaFace__c = '',
                                              MX_SB_VTS_DetailCampaFace__c = 'ANUNCIO POR FACEBOOK ' + contador1);
                    leads1.add(candidato1); 
                }
        insert leads1;   
                for(Lead get1 : leads1) {
                    leadsId1.add(get1.Id);
                }
        final List<Lead> valor1 = MX_SB_VTS_UpNameCampaign_Ctrl.NameCampaignCtrl(leadsId1);
        System.AssertNotEquals(leads1,valor1);
    }
}