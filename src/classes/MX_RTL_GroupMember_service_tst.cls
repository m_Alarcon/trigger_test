/**
* Name: MX_SB_PC_AsignacionCola_tst
* @author Jose angel Guerrero
* Description : Clase Test para Controlador de componente Lightning de asignación de Colas
*
* Ver              Date            Author                   Description
* @version 1.0     Jul/13/2020     Jose angel Guerrero      Initial Version
*
**/
@isTest
public class MX_RTL_GroupMember_service_tst {
     /**
* @description: funcion constructor
*/
    @isTest
    public static void serviceBuscarLasColas() {
        final List <String> listaDeId = new list <String> {'miembro'} ;
        final list < GroupMember> miebro = MX_RTL_GroupMember_service.serviceBuscarLasColas(listaDeId,'prefijo');
        final list  < GroupMember> lista = new list < GroupMember> ();
        test.startTest();
        System.assertEquals(miebro, lista, 'aprobado');
        test.stopTest();
    }
    /**
* @description: funcion constructor
*/
    @isTest
    public static void serviceGroupMember() {
        final list < GroupMember> miebro = MX_RTL_GroupMember_service.serviceGroupMember('listaDeId', 'prefijo');
        final list  < GroupMember> lista = new list < GroupMember> ();
        test.startTest();
        System.assertEquals(miebro, lista, 'exito');
        test.stopTest();
    }
     /**
* @description: funcion constructor
*/
    @isTest
    public static void serviceBuscarCola() {
        final list < GroupMember> miebro = MX_RTL_GroupMember_service.serviceBuscarCola('colaBuscar');
        final list  < GroupMember> lista = new list < GroupMember> ();
        test.startTest();
        System.assertEquals(miebro, lista, 'aprobado');
        test.stopTest();
    }
     /**
* @description: funcion constructor
*/
    @isTest
    public static void serviceGuardarAsig() {
        final list <String> usersId = new list<String>();
        for(User users : [Select Id from User LIMIT 100]) {
            usersId.add(users.Id);
        }
        final list < GroupMember> miebro = MX_RTL_GroupMember_service.serviceGuardarAsig(usersId,'origen','asignada');
        final list  < GroupMember> lista = new list < GroupMember> ();
        test.startTest();
        System.assertEquals(miebro, lista, 'SUCESS');
        test.stopTest();
    }
    /**
* @description: funcion constructor
*/
    @isTest
    public static void serviceUpsertAsignaciones() {
        final List <GroupMember> listaDelete = new  List <GroupMember>();
        final List <GroupMember> listaAssert = new  List <GroupMember>();
        MX_RTL_GroupMember_service.upsertAsignaciones( listaDelete);
        test.startTest();
        System.assertEquals(listaDelete, listaAssert, 'guardado');
        test.stopTest();
    }
    /**
* @description: funcion constructor
*/
    @isTest
    public static void serviceDeleteAsignaciones() {
        final List <GroupMember> listaUpsert = new  List <GroupMember>();
        final List <GroupMember> listaAssert = new  List <GroupMember>();
        MX_RTL_GroupMember_service.deleteAsignaciones( listaUpsert);
        test.startTest();
        System.assertEquals(listaUpsert, listaAssert, 'eliminado');
        test.stopTest();
    }
}