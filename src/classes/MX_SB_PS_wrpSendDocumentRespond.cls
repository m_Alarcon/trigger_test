/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpSendDocumentRespond
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
 @SuppressWarnings('sf:ShortVariable')
public class  MX_SB_PS_wrpSendDocumentRespond {
  /*Public property for wrapper*/
	public document document {get;set;}
	  /*Public constructor for wrapper*/
    public MX_SB_PS_wrpSendDocumentRespond() {
        document = new document();
    }
	  /*Public subclass for wrapper*/
	public class document {
	  /*Public property for wrapper*/
         public string id {get;set;} 
   /*public constructor subclass*/
		public document() {
		 this.id = '';
		
		}				
    }
}