/**
 * @File Name          : MX_SB_VTS_Neighborhood_Service
 * @Description        :
 * @Author             : Marco Antonio Cruz Barboza
 * @Group              :
 * @Last Modified By   : Marco Antonio Cruz Barboza
 * @Last Modified On   : 26/12/2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    26/12/2020    Marco Antonio Cruz Barboza         Initial Version
**/
@SuppressWarnings('sf:AvoidGlobalModifier, sf:UseSingleton')
Global class MX_SB_VTS_Neighborhood_Service {
    	
    /**Final code 200 response Ok */
    private static final Integer STATUSOK = 200;

    /*
    @Method: getNeighborhood
    @Description: Return a List<Object> with a insured amount list from the web service neighborhoodSearch
    @Param: Map<String, Object> dataNeighborhood
    @Return: List<Object>
    */
    public static List<Object> getNeighborhood(Map<String,Object> dataNeighborhood) {
        Final List <Object> insValues = new List<Object>();
        Final Map<String,Object> serviceParams = new Map<String,Object>{
            	'LAT' => String.valueOf(dataNeighborhood.get('latitude')),
                'LONG' => String.valueOf(dataNeighborhood.get('longitude')),
                'IsNewDev' => String.valueOf(dataNeighborhood.get('isNewDev')),
                'TYPE' => String.valueOf(dataNeighborhood.get('propertyType'))
                    };
        Final HttpRequest getRequest = createRequest();
        try {
            final HttpResponse response = MX_RTL_IasoServicesInvoke_Selector.callServices('neighborhoodSearch',serviceParams ,getRequest);
            if(response.getStatusCode() == STATUSOK) {
                final List<MX_SB_VTS_wrpNeighborhood_Utils> wrapper = (List<MX_SB_VTS_wrpNeighborhood_Utils>)JSON.deserialize(response.getBody().replace('"departmentList":', '"houseList":'),List<MX_SB_VTS_wrpNeighborhood_Utils>.class);
                for(integer i=0; i<wrapper.size(); i++) {
                    insValues.add(wrapper[i].houseList[i].pricem2); 
                }
            } else {
                insValues.add('error');
            }
        } catch (System.CalloutException exp) {
            insValues.add('proxy');
        }
        return insValues;
    }
    
    /*
    @Method: createRequest
    @Description: create a HttpRequest for the webservice neighborhoodSearch
    @Param: 
    @Return: HttpRequest
    */
    public static HttpRequest createRequest() {
        Final Map<String,iaso__GBL_Rest_Services_Url__c> rstServices = iaso__GBL_Rest_Services_Url__c.getAll();
        Final iaso__GBL_Rest_Services_Url__c getUrlLP = rstServices.get('neighborhoodSearch');
        Final String endpointService = getUrlLP.iaso__Url__c;
        Final HttpRequest requestPC = new HttpRequest();
        requestPC.setTimeout(120000);
        requestPC.setMethod('GET');
        requestPC.setHeader('Content-Type', 'application/json');
        requestPC.setHeader('Accept', '*/*');
        requestPC.setHeader('Host', 'https://test-sf.bbva.mx/QRVS_A02');
        requestPC.setEndpoint(endpointService);
        return requestPC;
    }
    
}