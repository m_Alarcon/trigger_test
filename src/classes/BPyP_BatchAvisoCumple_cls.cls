/*
    Proyecto : ENGMX
    Versión     Fecha           Autor                   Descripción
    ____________________________________________________________________________________________
    1.0         28-Ago-2018     Cristian Espinosa       Creación del batch que envia un correo
                                                        semanal al ejecutivo BPyP, avisandole
                                                        sobre los proximos cumpleaños de sus
                                                        clientes.
                                                        *Clase de pruebas BPyP_BatchAvisoCumple_tst
                                                        *Clase schedule Schedule_BPyP_BatchAvisoCumple_cls
    1.1         19-Sep-2018     Cristian Espinosa       Se hace uso del campo MX_BPyP_Proximo_Cumpleanios__c
    1.2         10-Oct-2018     Ricardo Almanza A       Se limita Batch a Clientes
    1.3         02-Ene-2020     Jair Ignacio Gonzalez   Se cambio el campo MX_BPyP_Proximo_Cumpleanios__c por
                                                        PersonContact.MX_NextBirthday__c por la reingenieria de person account
*/
public with sharing class BPyP_BatchAvisoCumple_cls implements Database.Batchable<SObject>, Database.Stateful {

    /* Map del cliente y el correo del Owner */
    final public Map<String,List<Account>> mapAccByEmail = new Map<String,List<Account>>();

    /*
        @Descripción : Se obtienen los contactos que cumplen años en la semana.
    */
    public Database.QueryLocator start(Database.BatchableContext batchCntx) {
        final Datetime todayDt = DateTime.newInstance(System.today(), Time.newInstance(0,0,0,0));
        final String todayDate = todayDt.format('yyyy-MM-dd');
        final Datetime nextWeekDt = DateTime.newInstance(System.today().addDays(7), Time.newInstance(0,0,0,0));
        final String nextWeekDate = nextWeekDt.format('yyyy-MM-dd');
        final String actWeekCond = '(PersonContact.MX_NextBirthday__c >= '+todayDate+' AND PersonContact.MX_NextBirthday__c < '+nextWeekDate+') ORDER BY PersonContact.MX_NextBirthday__c ASC';
        final String condition = 'WHERE (Recordtype.DeveloperName = \'MX_BPP_PersonAcc_Client\' ) AND PersonContact.Birthdate != null AND '+actWeekCond;
        final String query ='SELECT Id, Name, PersonContact.MX_NextBirthday__c, OwnerId, Owner.Name, Owner.Email  FROM Account ' + condition;
        return Database.getQueryLocator(query);
    }

    /*
        @Descripción : Se llena el mapa mapAccByEmail, teniendo como llave el email del ejecutivo BPyP y como contenido
                      los clientes que le pertenecen y cumplen años esa semana.
    */
    public void execute(Database.BatchableContext batchCntx, List<Account> scope) {
        final List<String> lsEmails = new List<String>();

        for(Account acc : scope) {
            if(!lsEmails.contains(acc.Owner.Email)) {
                lsEmails.add(acc.Owner.Email);
            }
        }
        lsEmails.sort();

        for(String emailTmp : lsEmails) {
            List<Account> lsAccounts = new List<Account>(); //NOSONAR
            for(Account acc : scope) {
                if(acc.Owner.Email.equals(emailTmp)) {
                    lsAccounts.add(acc);
                }
            }
            if(!lsAccounts.isEmpty()) {
                lsAccounts.addAll(mapAccByEmail.containsKey(emailTmp)?mapAccByEmail.get(emailTmp):new List<Account>());
                mapAccByEmail.put(emailTmp,lsAccounts);
            }
        }
    }

    /*
        @Descripción : Se envian los email, tomando como destinatario los correos electronicos del mapa mapAccByEmail.
    */
    public void finish(Database.BatchableContext batchCntx) {
        final List<Messaging.SingleEmailMessage> lsSingleMails = new List<Messaging.SingleEmailMessage>();
        final List<OrgWideEmailAddress> addresses = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'notificaciones.dwp.reail.mx@bbva.com']; //NOSONAR
        if(!mapAccByEmail.isEmpty()) {
            for(String emailKey : mapAccByEmail.keyset()) {
                List<Account> lsAccounts = mapAccByEmail.get(emailKey); //NOSONAR
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); //NOSONAR
                mail.setSubject('Recordatorio de cumpleaños semanal');
                mail.setToAddresses(new List<Id>{lsAccounts[0].OwnerId});
                mail.setHTMLBody(getBody(lsAccounts));
                if (!addresses.isEmpty()) {
                    mail.setOrgWideEmailAddressId(addresses[0].Id);
                }
                lsSingleMails.add(mail);
            }
        }

        try {
            if(!lsSingleMails.isEmpty()) {
                Messaging.sendEmail(lsSingleMails);
            }
        } catch(Exception e) {
            System.debug('Exception in class BPyP_BatchAvisoCumple_cls, finish\nCaused by :'+e.getMessage());
        }

    }

    /*
        @Descripción : Método que retorna una cadena de texto, que sera el body del email a enviar a cada ejecutivo BPyP.
    */
    public String getBody(List<Account> accs) {
        String strBody = '<p><b>Buen día '+accs[0].Owner.Name+',</b></p>';
        strBody += '<p>En esta semana, los cumpleaños de tus clientes son :</p><table>';
        for(Account acc : accs) {
            strBody += '<tr><td><b>'+acc.Name+'</b></td><td>  </td>';
            strBody += '<td>'+acc.PersonContact.MX_NextBirthday__c.format()+'</td></tr>';
        }
        strBody += '</table><br/><b>¡Recuerda felicitarlos!</b>';
        strBody += '<br/><br/>Atentamente :<br/>';
        strBody += '<p><b>DWP Banca Patrimonial y Privada</b></p>';
        return strBody;
    }

}