/**
* ---------------------------------------------------------------------------------
* @Autor: Selena Yamili Rodriguez Vega , Ricardo Alberto Santos
* @Proyecto: Workflow GFD
* @Descripción : Class for asing current Q to new Iniciatives
				 this class sustitute Flow MX_RTE_Actualizar_a_PI_Actual
* ---------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ---------------------------------------------------------------------------------
* 1.0           01/04/2020     Selena Rodriguez               Creación de la clase
* ---------------------------------------------------------------------------------
*/

public with sharing class  MX_RTE_Asignar_Q {



    /*Variable for create Date today for auxiliar in if*/
    static Date myDate = Date.today();
    /*Variable for store new ID´s Q */
    static ID newQ;
    /*List for store PI´s or Q´s created in the current and last year */
    static List<MX_RTE_PI__c> datesQ=[Select id,MX_RTE_Fecha_Inicio__c,MX_RTE_Fecha_Fin__c from MX_RTE_PI__c where CreatedDate IN (THIS_YEAR,LAST_YEAR) ];
    /*List store Iniciatives to upsert*/
    static List<MX_RTE_Iniciativa__c> nwIniciatives =new List<MX_RTE_Iniciativa__c>();

    /*Class constructor*/
    private MX_RTE_Asignar_Q() {

    }

    /**
  * @description: Method for get the current Q and assign to the new Iniciative
  * @author Selena Rodriguez
  */
    @InvocableMethod
    public static void getIniciative () {

	for(MX_RTE_PI__c iter: datesQ) {
		if((iter.MX_RTE_Fecha_Inicio__c == null || iter.MX_RTE_Fecha_Inicio__c <= myDate) && (iter.MX_RTE_Fecha_fin__c == null || iter.MX_RTE_Fecha_Fin__c >= myDate)) {
            newQ =iter.Id;
        	}
      	}

     for (MX_RTE_Iniciativa__c tdIniciatives : [SELECT Id, MX_RTE_PI_Actual__c from MX_RTE_Iniciativa__c WHERE CreatedDate =TODAY and MX_RTE_PI_Actual__c = null ]) {
            tdIniciatives.MX_RTE_PI_Actual__c=newQ;
            nwIniciatives.add(tdIniciatives);
        	}
			upsert nwIniciatives;
    }

}