/**-------------------------------------------------------------------------
* Nombre: 		MX_SB_VTS_GetSetAlianzas_RP_Ctrl_tst
* @author 		Alexandro Corzo
* Proyecto: 	Servicio - Obtiene Alianza RP (Controller)
* Descripción : Clase de prueba para la clase encargada de recuperar los datos
*				de la alianza RP
* --------------------------------------------------------------------------
*                         Fecha           Autor                   Desripción
* --------------------------------------------------------------------------
* @version 1.0            04/11/2020      Alexandro Corzo         Creación de la Clase
* @version 1.1            12/01/2021      Alexandro Corzo         Ajustes a la clase consumo ASO
* --------------------------------------------------------------------------*/
@isTest
public class MX_SB_VTS_GetSetAlianzas_RP_Ctrl_tst {
    /** Variable de Apoyo: sUserName */
    Static String sUserName = 'UserOwnerTest01';
    /** Variable de Apoyo: sURL */
    Static String sUrl = 'http://www.example.com';
    /** Variable de Apoyo: sNoEmpty */
    Static String sNoEmpty = 'Lista No Vacia';
    
	/**
     * @description : Función de configuración para la ejecución
     * 				: de la clase de prueba. 
     * @author: 	 Alexandro Corzo
     */  
    @TestSetup
    static void makeData() {
    	insert new iaso__GBL_Rest_Services_Url__c(Name = 'getListInsuranceInformationCatalogs', iaso__Url__c = sUrl, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'getGTServicesSF', iaso__Url__c = sUrl, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
    }
    
    /**
     * @description : Función de prueba para obtener los datos de la alianza RP
     * @author      : Alexandro Corzo
     */
    @isTest static void tstObtLstInsInfCatCtrl() {
    	final User objUserTst = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(objUserTst) {
            final Map<String, String> mHeadersMock = new Map<String, String>();
            mHeadersMock.put('tsec', '1234567890');
            final MX_WB_Mock objMockCallOut = new MX_WB_Mock(200, 'Complete', '{"iCatalogItem":{"revisionPlanList":[{"catalogItemBase":{"id":"S"},"planReview":"001","planCode":"008","productCode":"3002","allianceCode":"DHSDT003"},{"catalogItemBase":{"id":"N"},"planReview":"002","planCode":"001","productCode":"3005","allianceCode":"DTST0001"}]}}', mHeadersMock);
            iaso.GBL_Mock.setMock(objMockCallOut);
            Test.startTest();
            	final Map<String, Object> mTestRst = MX_SB_VTS_GetSetAlianzas_RP_Ctrl.obtListInsInfoCatCtrl();
            Test.stopTest();
            System.assert(!mTestRst.isEmpty(), sNoEmpty);
        }
    }
    
    /**
     * @description : Función de prueba para simular una respuesta diferente a 200
     * @author      : Alexandro Corzo
     */
    @isTest static void tstAnotherRspSrv() {
        final User objUsrAR = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(objUsrAR) {
            final Map<String, String> mHeadersMockAR = new Map<String, String>();
            mHeadersMockAR.put('tsec', '0987654321');
            final MX_WB_MOck objMockCallOutAR = new MX_WB_MOCK(503, 'Complete', '', mHeadersMockAR);
            iaso.GBL_Mock.setMock(objMockCallOutAR);
            Test.startTest();
            	final Map<String, Object> mTestRstAR = MX_SB_VTS_GetSetAlianzas_RP_Ctrl.obtListInsInfoCatCtrl();
            Test.stopTest();
            System.assert(!mTestRstAR.isEmpty(), sNoEmpty);
        }
    }
}