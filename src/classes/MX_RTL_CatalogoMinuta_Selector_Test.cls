/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RTL_CatalogoMinuta_Selector_Test
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2021-01-19
* @Description 	Test Class for MX_RTL_CatalogoMinuta_Selector
* @Changes
*  
*/
@isTest
public class MX_RTL_CatalogoMinuta_Selector_Test {
    
    @testSetup
    static void setup() {
        MX_BPP_CatalogoMinutas__c recordTest;
        recordTest = new MX_BPP_CatalogoMinutas__c();
        recordTest.Name = 'BienvenidaDO';
        recordTest.MX_Acuerdos__c = 1;
        recordTest.MX_Asunto__c = 'Test Email';
        recordTest.MX_Contenido__c = 'Test Contenido';
        recordTest.MX_Despedida__c = 'Saludos';
        recordTest.MX_Firma__c = 'Atte. Test';
        recordTest.MX_Saludo__c = 'Buen día';
        recordTest.MX_TipoVisita__c = 'BienvenidaDO';
        recordTest.MX_CuerpoCorreo__c = 'Cuerpo del correo';
        recordTest.MX_ImageHeader__c = 'Imagen_header_1';
        insert recordTest;
    }
    
    /**
    * @Description 	Test Method for constructor
    * @Return 		NA
    **/
	@isTest
    static void constructorTest() {
        final MX_RTL_CatalogoMinuta_Selector catalogoMSelector = new MX_RTL_CatalogoMinuta_Selector();
        System.assertNotEquals(null, catalogoMSelector, 'Error on constructor');
    }
    
    /**
    * @Description 	Test Method for MX_RTL_CatalogoMinuta_Selector.getRecordsByTipoVisita()
    * @Return 		NA
    **/
	@isTest
    static void getRecordsByTipoVisitaTest() {
        final List<String> listString = new List<String>();
        listString.add('BienvenidaDO');
    	final List<MX_BPP_CatalogoMinutas__c> listRecords = MX_RTL_CatalogoMinuta_Selector.getRecordsByTipoVisita(listString);
        System.assertNotEquals(null, listRecords, 'Error on getRecordsByTipoVisita');
    }
    
    /**
    * @Description 	Test Method for MX_RTL_CatalogoMinuta_Selector.getRecordsById()
    * @Return 		NA
    **/
	@isTest
    static void getRecordsByIdTest() {
        final MX_BPP_CatalogoMinutas__c catalogoTest = [SELECT Id, MX_TipoVisita__c FROM MX_BPP_CatalogoMinutas__c WHERE MX_TipoVisita__c = 'BienvenidaDO' LIMIT 1];
        final List<String> listString = new List<String>();
        listString.add(catalogoTest.Id);
    	final List<MX_BPP_CatalogoMinutas__c> listRecords = MX_RTL_CatalogoMinuta_Selector.getRecordsById('Id, MX_TipoVisita__c', listString);
        System.assertNotEquals(null, listRecords, 'Error on getRecordsById');
    }

}