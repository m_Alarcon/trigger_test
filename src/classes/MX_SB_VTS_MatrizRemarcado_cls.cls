/**
 * @File Name          : MX_SB_VTS_MatrizRemarcado_cls.cls
 * @Description        : 
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 25/2/2020 15:39:05
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    28/10/2019   Eduardo Hernández Cuamatzi     Initial Version
**/
public without sharing class MX_SB_VTS_MatrizRemarcado_cls extends MX_SB_VTS_CalculateReCalls_cls { //NOSONAR
    /** Bandejas de marcado*/
    private final static Map<Id, MX_SB_VTS_Lead_tray__c> MAPTRAY = fillTrays();
    /** Bandejas de marcado*/
    private final static Map<String,MX_SB_VTS_Lead_tray__c> TRAYSEND = MX_SB_VTS_CalculateReCallsUtil_cls.findTrySend(MAPTRAY);
    /** PROVEEDORES */
    private final static Map<Id, MX_SB_VTS_ProveedoresCTI__c> PROVEEDORES = MX_SB_VTS_CalculateReCallsUtil_cls.fillProveedores();
    /** Mapa de llamadas */
    private final static Map<String,String> IDWRAPRECALL = new map<String,String>();
    /** Mapa de listas llamadas smart*/
    private final static List<WrapperRecallCTI> LSTWRAPSEND = new List<WrapperRecallCTI>();

    /**
    @Funcion de remarcado para Leads
    */
    @InvocableMethod
    public static void remarcado(List<Lead> recordsLead) {
        for(Lead leadRecord : recordsLead) {
            if(String.isNotBlank(leadRecord.MX_SB_VTS_Tipificacion_LV7__c) && String.isNotBlank(leadRecord.MX_SB_VTS_TrayAttention__c)) {
                final MX_SB_VTS_Lead_tray__c traySending = TRAYSEND.get(leadRecord.MX_SB_VTS_TrayAttention__c);
                final MX_SB_VTS_ProveedoresCTI__c proveedor = PROVEEDORES.get(traySending.MX_SB_VTS_ProveedorCTI__c);
                final Map<String, String> recordVals = new Map<String, String>();
                recordVals.put('Id', leadRecord.Id);
                recordVals.put('nextAction', String.valueOf(Integer.valueOf(leadRecord.MX_SB_VTS_NextActionCall__c)));
                recordVals.put('level6', leadRecord.MX_SB_VTS_RecallValue__c);
                recordVals.put('phone1', evalutePhone(leadRecord.MobilePhone,leadRecord.MX_WB_ph_Telefono1__c));
                recordVals.put('phone2', evalutePhone(leadRecord.MobilePhone,leadRecord.MX_WB_ph_Telefono2__c));
                recordVals.put('phone3', evalutePhone(leadRecord.MobilePhone,leadRecord.MX_WB_ph_Telefono3__c));
                recordVals.put('displayName', MX_SB_VTS_SendLead_helper_cls.fillFullName((SObject)leadRecord,' Remarc'));
                final WrapperRecallCTI wrappItem = calculateRules(recordVals, traySending, proveedor, leadRecord.MX_SB_VTS_Tipificacion_LV7__c, System.now());
                IDWRAPRECALL.put(wrappItem.idRecord, wrappItem.nextAction);
                LSTWRAPSEND.add(wrappItem);
            }
        }
        final List<WrapperRecallCTI> lstWrapSendFinal = new List<WrapperRecallCTI>();
		final List <sObject> updateEl = new List <sObject> ();
        for(WrapperRecallCTI wrappTest : LSTWRAPSEND) {
            if(String.isNotBlank(wrappTest.idRecord)) {
                if(String.isNotBlank(wrappTest.scheduleDate) == true && String.isNotBlank(wrappTest.idRecord) == true) {
                    lstWrapSendFinal.add(wrappTest);                
                } else {
                    final Lead leadUp =new Lead(id=wrappTest.idRecord, xmlEnvio__c = 'Fecha de remarcado:"", Accion siguiente:' +wrappTest.nextAction);
                    updateEl.add(leadUp);
                }
            }
        }
        updateRecsLead(updateEl, lstWrapSendFinal);
    }

    /**
	@Method updateRecs actualiza registros
	*/
	public static void updateRecsLead(List<Lead> updateEl, List<WrapperRecallCTI> lstWrapSendFinal) {
		if(updateEl.isEmpty() == false) {
			update updateEl;
		}
        if(lstWrapSendFinal.isEmpty() == false) {
            MX_SB_VTS_CalculateReCallsUtil_cls.sendSmart(lstWrapSendFinal,IDWRAPRECALL, false);
        }
	}
}