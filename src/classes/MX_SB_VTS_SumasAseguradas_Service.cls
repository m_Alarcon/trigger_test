/**
* @description       : Clase service que recupera y guarda registros de sumas aseguradas
* @author            : Marco Antonio Cruz Barboza
* @group             : BBVA
* @last modified on  : 01-27-2021
* @last modified by  : Marco Antonio Cruz Barboza
* Modifications Log 
* Ver   Date         Author                       Modification
* 1.0   01-27-2021   Marco Antonio Cruz Barboza   Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public without sharing class MX_SB_VTS_SumasAseguradas_Service {
    
    /**
    * @description Función que actualiza el campo de PrimerPropietario__c con e picklist de las sumas aseguradas
    * @author Marco Cruz | 01-27-2021 
    * @param srtOppId id Oppportunity, insuredAmo picklist values from JS to string.
    * @return void
    **/ 
    @AuraEnabled
    public static void updatePick(String srtOppId, String insuredAmo, String fieldPick) {
        final Opportunity opportAmo = MX_RTL_Opportunity_Selector.getOpportunity(srtOppId, fieldPick);
        final List<Opportunity> recordLst = new List<Opportunity>{opportAmo};
        for(Opportunity oppoIter : recordLst) {
			oppoIter.PrimerPropietario__c = insuredAmo;
        }
        MX_RTL_Opportunity_Selector.updateResult(recordLst, false);
    }
    
    /**
    * @description Función que recupera el campo de Monto_de_la_oportunidad__c con la suma asegurada
    * @author Marco Cruz | 01-27-2021 
    * @param srtOppId id Oppportunity
    * @return String insAmount which is a insured amount field
    **/ 
    @AuraEnabled
    public static String getPick(String srtOppId, String fieldPick) {
        return MX_RTL_Opportunity_Selector.getOpportunity(srtOppId, fieldPick).PrimerPropietario__c;
    }
    
    /**
    * @description Función que recuper el campo de PrimerPropietario__c con e picklist de las sumas aseguradas
    * @author Marco Cruz | 01-27-2021 
    * @param srtOppId, etapaForm Id de la oportunidad y etapa actual del lwc recuperada desde el front JS
    * @return void
    **/ 
    @AuraEnabled
    public static String getInsured(String oppId, String fieldAmo) {
        final Opportunity opportAmo = MX_RTL_Opportunity_Selector.getOpportunity(oppId, fieldAmo);
		return String.valueOf(opportAmo.Monto_de_la_oportunidad__c);
    }
    
}