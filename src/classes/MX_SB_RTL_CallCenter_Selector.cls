/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 09-10-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   09-09-2020   Eduardo Hernández Cuamatzi   Initial Version
**/
@SuppressWarnings('sf:UseSingleton, sf:DMLWithoutSharingEnabled')
public class MX_SB_RTL_CallCenter_Selector {

    /**
    * @description Recupera lista de callcenters
    * @author Eduardo Hernández Cuamatzi | 09-09-2020 
    * @param fields campos para la query
    * @param condition condiciones de la consulta
    * @return List<CallCenter> Lista de resultados
    **/
    public static List<CallCenter> findCallCenter(Set<string> lstIds) {
        return [Select Id, Name,AdapterUrl from CallCenter where Id =: lstIds];
    }
}