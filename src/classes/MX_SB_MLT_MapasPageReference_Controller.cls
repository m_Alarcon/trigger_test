/**
* -------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_MapasPageReference_Controller
* Autor: Daniel Lopez Perez
* Proyecto: Siniestros - BBVA
* Descripción : Clase que almacena los metodos usados en mapas de toma de Reporte.
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   	Descripción
* --------------------------------------------------------------------------------
* 1.0         11/10/2019     Juan Carlos Benitez Herrera	Creación
* 1.1         20/11/2019     Daniel Lopez Perez				Se refactoriza la clase
* 1.2         18/12/2019     Juan Carlos Benitez Herrera	Se resolvieron issues de sonar
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_MapasPageReference_Controller {
		/** Variable sin */
    public Siniestro__c sin {get; set;}
 		/** Variable sin2 */
    public Siniestro__c sin2 {get; set;}
		/** Variable sinRobo */
    public Siniestro__c sinRobo {get; set;}
		/** Variable sinRobo2 */
    public Siniestro__c sinRobo2 {get; set;}
		/** Variable siniestroId */
    public String siniestroId {get;set;}
		/** Variable nuevo */
    public Boolean nuevo {get;set;}
    /** Variable sinDest */
    public Siniestro__c sinDest {get; set;}
    /*
    * @description constructor que inicializa variables para el front
    * @param ninguno
    * @return Object Siniestro__c sin,sin2,sinRobo,sinRobo2,sinDest String SiniestroId
    */
    public MX_SB_MLT_MapasPageReference_Controller() {
        siniestroId = ApexPages.currentPage().getParameters().get('Id');
        nuevo=false;
        
		sin = [SELECT Id, Name, MX_SB_MLT_Origen__c, MX_SB_MLT_Address__c,MX_SB_MLT_URLLocation__c,MX_SB_MLT_NumeroCalle__c,recordType.DeveloperName,
                MX_SB_MLT_Calle__c,MX_SB_MLT_Colonia__c,MX_SB_MLT_Estado__c,MX_SB_MLT_CodigoPostal__c,MX_SB_SAC_Contrato__c,recordTypeId,CreatedDate
                FROM Siniestro__C WHERE id =: ApexPages.currentPage().getParameters().get('id') limit 1];
        
		sin2 = [SELECT Id, Name, MX_SB_MLT_Origen__c, MX_SB_MLT_Address__c,MX_SB_MLT_URLLocation__c,MX_SB_MLT_NumeroCalle__c,
                MX_SB_MLT_Calle__c,MX_SB_MLT_Colonia__c,MX_SB_MLT_Estado__c,MX_SB_MLT_CodigoPostal__c,MX_SB_SAC_Contrato__c
                FROM Siniestro__C WHERE id =: ApexPages.currentPage().getParameters().get('id') limit 1];
        
        sinRobo = [SELECT Id, Name, MX_SB_MLT_Origen__c, MX_SB_MLT_AddressSteal__c,MX_SB_MLT_URLLocationSteal__c,MX_SB_MLT_NumeroCalle_Robo__c,
                MX_SB_MLT_Calle_Robo__c,MX_SB_MLT_Colonia_Robo__c,MX_SB_SAC_Contrato__c,MX_SB_MLT_Estado_Robo__c,MX_SB_MLT_CP_Robo__c,MX_SB_MLT_Latitud__c,
                MX_SB_MLT_Longitud__c, recordTypeId
                FROM Siniestro__C WHERE id =: ApexPages.currentPage().getParameters().get('id') limit 1];
        sinDest = [SELECT Id, Name, MX_SB_MLT_Origen__c, MX_SB_MLT_DireccionDestino__c, MX_SB_MLT_CoordenadasDestino__c,
                  MX_SB_MLT_CalleDestino__c,recordTypeId, MX_SB_MLT_CodigoPostalDestino__c,recordType.DeveloperName,
                  MX_SB_MLT_ColoniaDestino__c, MX_SB_MLT_EstadoDestino__c, MX_SB_MLT_NumExtDestino__c
                  FROM Siniestro__c WHERE id =: ApexPages.currentPage().getParameters().get('id') limit 1];
        
        if(sin.MX_SB_MLT_URLLocation__c==null) {
            sin.MX_SB_MLT_URLLocation__c = '19.5038795,-99.1802701';
            sin.MX_SB_MLT_Address__c = 'Eje 5 Nte 990, Santa Barbara, 02230 Ciudad de México, CDMX, México';
        }
        
        if(sinRobo.MX_SB_MLT_URLLocationSteal__c==null) {
            sinRobo.MX_SB_MLT_URLLocationSteal__c = '19.5038795,-99.1802701';
            sinRobo.MX_SB_MLT_AddressSteal__c = 'Eje 5 Nte 990, Santa Barbara, 02230 Ciudad de México, CDMX, México';
        }
        if(sinDest.MX_SB_MLT_CoordenadasDestino__c==null) {
            sinDest.MX_SB_MLT_CoordenadasDestino__c = '19.5038795,-99.1802701';
            sinDest.MX_SB_MLT_DireccionDestino__c = 'Eje 5 Nte 990, Santa Barbara, 02230 Ciudad de México, CDMX, México';
        }
    }
    /*
    * @description Method que actualiza siniestro
    * @param ninguno
    * @return Object pagref
    */
    public PageReference updateSin() {
        update sin;
          final PageReference pagref= ApexPages.currentPage();
         	pagref.setRedirect(true);
         	return pagref;
    }
    
    /*
    * @description Method que actualiza siniestro
    * @param ninguno
    * @return Object pagref
    */
     public PageReference save() {
         	update sin;
          final  PageReference pagref= ApexPages.currentPage();
         	pagref.setRedirect(true);
         	return pagref;
        
    }

    /*
    * @description Method que actualiza siniestro
    * @param ninguno
    * @return Object pagref
    */
    public PageReference saveRobo() {
         	update sinRobo;
         	final PageReference pagref= ApexPages.currentPage();
         	pagref.setRedirect(true);
         	return pagref;
    }
    /*
    * @description Method que actualiza siniestro
    * @param ninguno
    * @return Object pagref
    */
    public PageReference saveDest() {
         	update sinDest;
         	final PageReference pagref= ApexPages.currentPage();
         	pagref.setRedirect(true);
         	return pagref;
    }
    
    /*
    * @description Method que obtiene  la etapa del siniestro
    * @param String siniId
    * @return MX_SB_MLT_JourneySin__c
    */
    @auraEnabled
    public static String getvalueJourney(String siniId) {
        return [Select MX_SB_MLT_JourneySin__c from Siniestro__C where id =:siniId].MX_SB_MLT_JourneySin__c;
    }
     /*
    * @description Method que obtiene  la direccion del siniestro
    * @param String siniId
    * @return MX_SB_MLT_DireccionDestino__c
    */
     @auraEnabled
    public static String getDataMAP(String siniId) {
        return [Select MX_SB_MLT_DireccionDestino__c from Siniestro__C where id =:siniId].MX_SB_MLT_DireccionDestino__c;
    }
}