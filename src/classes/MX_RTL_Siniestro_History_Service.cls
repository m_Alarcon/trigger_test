/**
 * @File Name          : MX_RTL_Siniestro_History_Service.cls
 * @Description        :
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 5/26/2020, 05:43:34 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/26/2020   Juan Carlos Benitez     Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public  class MX_RTL_Siniestro_History_Service { 
/** List of records to be returned */
    final static List<Siniestro__History> HISTORYSRCH = new List<Sobject>();
/** List of records to be returned */    
    final static List<SObject> LISTHISTORY = new List<SObject>();
/** VAR PARENTID ParentId */        
	Final static String PARENTID='ParentId';
/** VAR IDE id */    
	Final static String IDE='Id';
    
    /**
     * 
    * @description
    * @author Juan Carlos Benitez | 5/26/2020
    * @param Ids
    * @return List<Siniestro__History>
    **/
    public static List<Siniestro__History> getSinHData(Set<Id> ids) {
        Final Map<String,String> mapa = new Map<String,String>();
        mapa.put('fields', ' CreatedDate,Id,NewValue,OldValue,ParentId ');
        mapa.put('orderField', ' order by CreatedDate asc ');
        mapa.put('limite', ' limit 1 ');
        Final List <Siniestro__History> sinHistory = MX_RTL_Siniestro_History_Selector.getSinHDataByParentId(ids,PARENTID,LISTHISTORY,mapa);
        for(Siniestro__History obj :sinHistory) {
            	if(obj.NewValue =='Cerrado' && obj.OldValue=='Detalles del Siniestro o Asistencia' ) {
	                LISTHISTORY.add(obj);
    	         }
        	}
        return MX_RTL_Siniestro_History_Selector.getSinHDataByParentId(ids,IDE,LISTHISTORY,mapa);
    }
}