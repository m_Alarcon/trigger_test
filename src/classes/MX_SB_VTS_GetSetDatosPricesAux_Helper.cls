/**
 * @File Name          : MX_SB_VTS_GetSetDatosPricesAux_Helper.cls
 * @Description        : Clase Auxiliar Consumo de Servicio - Prices - Cotizador HSD
 * @Author             : Alexandro Corzo
 * @Group              : 
 * @Last Modified By   : Alexandro Corzo
 * @Last Modified On   : 25-02-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0       25/02/2021      Alexandro Corzo        Initial Version
 * 1.1		 01/02/2021		 Alexandro Corzo		Se corrigen Issues genera cotización
 * 1.1.1	 10/03/2021		 Eduardo Hernández		Fix sumas aseguradas
**/
@SuppressWarnings('sf:UseSingleton')
public class MX_SB_VTS_GetSetDatosPricesAux_Helper {
    /**
     * @description : Recupera el listado de unidades
     * @author      : Alexandro Corzo
     * @return      : void
     */
    public static Map<String, String> fillUnitsCov(Map<String, Object> mCoverDet, MX_SB_VTS_wrpPricesDataSrvRsp.coverages oRowCovers) {
        final Map<String, String> mRspData = new Map<String, String>();
        try {
            if(String.valueOf(mCoverDet.get('CoversID')).equals(oRowCovers.iddata)) {
                String strUnitsIna = '';
                String strUnitsYea = '';
                String strUnitsCov = '';
                for(MX_SB_VTS_wrpPricesDataSrvRsp.units oRowUnits : oRowCovers.units) {
                    if(oRowUnits.unitsElements.percentage == null) {
                        if ('INSURED_AMOUNT'.equalsIgnoreCase(oRowUnits.unitsElements.iddata)) {
                            strUnitsIna = '' + oRowUnits.unitsElements.amount;
                        }
                        if('YEARLY_PREMIUM'.equalsIgnoreCase(oRowUnits.unitsElements.iddata)) {
                            strUnitsYea = '' + oRowUnits.unitsElements.amount;
                        }
                    } else {
                        if(oRowUnits.unitsElements.unitType == null) {
                            strUnitsCov += oRowUnits.unitsElements.contractingCriteria.iddata + ',' +
                                oRowUnits.unitsElements.contractingCriteria.criterial + ',' +
                                oRowUnits.unitsElements.percentage + ',' + 
                                oRowUnits.unitsElements.amount + '|';
                        }
                    }
                }
                strUnitsCov = strUnitsCov.length() == 0 ? strUnitsCov : strUnitsCov.substring(0, strUnitsCov.length() - 1);
                mRspData.put('lstInsuAmount', strUnitsIna);
                mRspData.put('lstYearAmount', strUnitsYea);
                mRspData.put('lstPercAmount', strUnitsCov);
            }
        } catch(Exception e) {
            mRspData.put('lstInsuAmount', '0');
            mRspData.put('lstYearAmount', '0');
            mRspData.put('lstPercAmount', '');
        }
        return mRspData;
    }

    /**
     * @description : Realiza listado de coberturas del consumo de servicio ASO
     * @author      : Alexandro Corzo
     * @return      : List<Object> lstCovers
     */
    public static List<Object> fillObjCovers(Map<String, Object> mParamsWrp) {
        final List<Object> lstCovers = new List<Object>();
        final MX_SB_VTS_wrpPricesDataSrvRsp objWrapperRsp = (MX_SB_VTS_wrpPricesDataSrvRsp) mParamsWrp.get('objWrapperRsp');
        for(MX_SB_VTS_wrpPricesDataSrvRsp.trades oRowTrades : objWrapperRsp.data.trades) {
            if(oRowTrades.iddata != null) {
                final String strTradeID = oRowTrades.iddata;            
                for(MX_SB_VTS_wrpPricesDataSrvRsp.categories oRowCat : oRowTrades.categories) {
                    final String strCateID = oRowCat.iddata;
                    for(MX_SB_VTS_wrpPricesDataSrvRsp.goodTypes oRowGT : oRowCat.goodTypes) {
                        final String strGoodTypeID = oRowGT.iddata;
                        for(MX_SB_VTS_wrpPricesDataSrvRsp.coverages oRowCov : oRowGT.coverages) {
                            final String strCoversID = oRowCov.iddata;
                            final Map<String, String> mDataCoversDetail = new Map<String, String>();
                            mDataCoversDetail.put('QuoteID', objWrapperRsp.data.iddata);
                            mDataCoversDetail.put('TradeID', strTradeID);
                            mDataCoversDetail.put('CategID', strCateID);
                            mDataCoversDetail.put('GoodTypeID', strGoodTypeID);
                            mDataCoversDetail.put('CoversID', strCoversID);
                            mDataCoversDetail.put('RamoID', MX_SB_VTS_GetSetDatosProcesSrv_Helper.getCodeTradeById(strTradeID));
                            final Map<String, Object> mCoverUnits = fillUnitsCov(mDataCoversDetail, oRowCov);
                            final String strUnitsCov = mCoverUnits.get('lstPercAmount').toString();
                            final String strUnitsYea = mCoverUnits.get('lstYearAmount').toString();
                            final String strUnitsIna = mCoverUnits.get('lstInsuAmount').toString();
                            mDataCoversDetail.put('UnitsList', strUnitsCov);
                            mDataCoversDetail.put('UnitsAmount', strUnitsYea);
                            mDataCoversDetail.put('UnitsAmountIns', strUnitsIna);
                            lstCovers.add(mDataCoversDetail);
                        }
                    }
                }
            }
        }
        return lstCovers;
    }

    /**
     * @description : Realiza listado de coberturas de Desastre servicio ASO
     * @author      : Alexandro Corzo
     * @return      : List<Object> lstCovers
     */
    public static List<Object> fillDisasCover(Map<String, Object> mParamsWrp) {
        final List<Object> lstDisasCov = new List<Object>();
        final MX_SB_VTS_wrpPricesDataSrvRsp objWrapperRsp = (MX_SB_VTS_wrpPricesDataSrvRsp) mParamsWrp.get('objWrapperRsp');
        Map<String, String> mDataDisCovDet = null;
        for(MX_SB_VTS_wrpPricesDataSrvRsp.disasterCovered oRowDisCov : objWrapperRsp.data.disasterCovered) {
            mDataDisCovDet = new Map<String, String>();
            mDataDisCovDet.put(oRowDisCov.iddata, oRowDisCov.value);
            lstDisasCov.add(mDataDisCovDet);
        }
        return lstDisasCov;
    }
    
    /**
     * @description : Realiza listado de Datos Particulares consumo de servicio ASO
     * @author      : Alexandro Corzo
     * @return      : List<Object> lstCovers
     */
    public static List<Object> fillObjDatPart(Map<String, Object> mParamsWrp) {
        final List<Object> lstDatPart = new List<Object>();
        final MX_SB_VTS_wrpPricesDataSrv.base objWrapper = (MX_SB_VTS_wrpPricesDataSrv.base) mParamsWrp.get('objWrapper');
        Map<String, String> mDataPartDetail = null;
        for(MX_SB_VTS_wrpPricesDataSrv.contractingCriteria oRowContCrit : objWrapper.contractingCriteria) {
        	mDataPartDetail = new Map<String, String>();
            mDataPartDetail.put('Criterial', oRowContCrit.criterial);
            mDataPartDetail.put('IdData', oRowContCrit.iddata);
            mDataPartDetail.put('Value', oRowContCrit.value);
            lstDatPart.add(mDataPartDetail);
        }
        return lstDatPart;
    }
    
    /**
    * @description Recupera valor de porcentaje de suma asegurada
    * @author Eduardo Hernández Cuamatzi | 03-02-2021 
    * @param lstDatPart Lista de datos particulares ah editar
    * @return String Porcentaje correspondiente si corresponde
    **/
    public static String fillCorrectInsured(List<Object> lstDatPart,String strNameCotiz) {
        String porcentInsured = '0.5';
        switch on strNameCotiz {
            when 'Completa', 'Balanceada', 'Basica' { porcentInsured = '.5'; }
            when else { porcentInsured = '1'; }
        }
        for(Object oRowDataNew : lstDatPart) {
            final Map<String, String> mDataPartN = (Map<String, String>)oRowDataNew;
            if(mDataPartN.get('criterial').equals('2008PORCSAIC')) {
                porcentInsured = mDataPartN.get('value');
                break;
            }
        }
        return porcentInsured;
    }


    /**
    * @description recupera el codigo de area correspondiente para rentado o propio
    * @author Eduardo Hernández Cuamatzi | 03-02-2021 
    * @param objMulAddr Direción ah evaluar
    * @return String Valor para coveredArea
    **/
    public static String fillCorrectAreaStr(MX_RTL_MultiAddress__c objMulAddr) {
        String coveredArea = '0';
        if(objMulAddr.MX_RTL_Tipo_Propiedad__c.equalsIgnoreCase('Rentado')) {
            coveredArea = '1';
        }
        return coveredArea;
    }
}