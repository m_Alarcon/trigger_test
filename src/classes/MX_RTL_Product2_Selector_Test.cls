/**
 * @File Name          : MX_RTL_Product2_Selector_Test.cls
 * @Description        : 
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : 
 * @Last Modified By   : Arsenio.perez.lopez.contractor@bbva.com
 * @Last Modified On   : 11-20-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    8/6/2020   Eduardo Hernández Cuamatzi     Initial Version
 * 1.1    20/11/2020   Arsenio Perez Lopez          Test resultQueryProdtest
**/
@isTest
private class MX_RTL_Product2_Selector_Test {
    @TestSetup
    static void makeData() {
        final MX_WB_FamiliaProducto__c objFamilyPro2 = MX_SB_VTS_CallCTIs_utility.newFamiliy(System.Label.MX_SB_VTS_Hogar);
        insert objFamilyPro2;
        final Product2 proHogar = MX_SB_VTS_CallCTIs_utility.newProduct(System.Label.MX_SB_VTS_Hogar, objFamilyPro2);
        proHogar.MX_SB_SAC_Proceso__c = 'VTS';
        insert proHogar;
    }

    @isTest
    private static void findProsByNameProces() {
        final Set<String> namesProd = new Set<String>{System.Label.MX_SB_VTS_Hogar};
        final Set<String> process = new Set<String>{'VTS'};
        Test.startTest();
            final List<Product2> lstTray = MX_RTL_Product2_Selector.findProsByNameProces(namesProd, process, true);
            System.assertEquals(lstTray.size(), 1, 'Producto');
        Test.stopTest();
    }
	    /**
    * @description 
    * @author Arsenio.perez.lopez.contractor@bbva.com | 11-20-2020 
    **/
    @isTest
    private static void resultQueryProdtest() {
        Test.startTest();
        final List<Product2> lstTraytrue =  MX_RTL_Product2_Selector.resultQueryProd('Id,Name', 'Name=\''+System.Label.MX_SB_VTS_Hogar+'\'', true);
        final List<Product2> lstTrayfalse =  MX_RTL_Product2_Selector.resultQueryProd('Id,Name', '', false);
        System.assertEquals(lstTraytrue.size(), lstTrayfalse.size(), 'Producto');
        Test.stopTest();
    }
}