/**
* Name: MX_SB_PC_AsignacionCola_tst
* @author Jose angel Guerrero
* Description : Clase Test para Controlador de componente Lightning de asignación de Colas
*
* Ver              Date            Author                   Description
* @version 1.0     Jul/13/2020     Jose angel Guerrero      Initial Version
*
**/
@isTest
public class MX_RTL_Group_Selector_tst {
    /**
* @description: funcion para guardar Asignaciones
*/
    public Final Static String IDUSUARIO = 'id1';
    /**
* @description: funcion para guardar Asignaciones
*/
    public Final Static String IDCOLA= 'id2';
    /**
* @description: funcion para guardar Asignaciones
*/
    public Final Static boolean SEASIGNA = true;
    /**
* @description: funcion para guardar Asignaciones
*/
    public Final Static boolean SEASIGNA2 = false;
    /**
    * @description: funcion constructor
    */
    @isTest
    public static void getQueuesByPrefijo() {
        final list <Group> prefijo = MX_RTL_Group_Selector.getQueuesByPrefijo('');
        final list <Group> exito = new list <Group> ();
        test.startTest();
        System.assertEquals(exito, prefijo, 'SUCESS');
        test.stopTest();
    }
}