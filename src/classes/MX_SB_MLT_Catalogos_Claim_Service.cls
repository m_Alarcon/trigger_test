/*
* @Nombre: MX_SB_MLT_Catalogos_Claim_Service.cls
* @Autor: Eder Alberto Hernández Carbajal
* @Proyecto: Siniestros - BBVA
* @Descripción : Clase Service para acceder a la clase MX_SB_MLT_Catalogos_Claim_Service.cls
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   	Descripción
* --------------------------------------------------------------------------------
* 1.0         26/05/2020    Eder Alberto Hernández Carbajal		Creación
* --------------------------------------------------------------------------------
*/
global with sharing class MX_SB_MLT_Catalogos_Claim_Service { //NOSONAR
    
    /**Tipo de catalogo ICD */
    Final static String ICD = 'ICD';
    
    /**Tipo de catalogo Ocupacion */
    Final static String OCP = 'Ocupación';
    
    /**Lista de registros de catalogo*/
    Final static List<MX_SB_MLT_Catalogos_Claim__c> RETURN_CAT = new List<MX_SB_MLT_Catalogos_Claim__c>();
    
    /** constructor */
    @TestVisible
    private MX_SB_MLT_Catalogos_Claim_Service() { }
    /*
    * @description Busqueda en catalogos
    * @param String descripcion, String tipo
    */
    public static List<MX_SB_MLT_Catalogos_Claim__c> getCatalogByDesc(String descripcion,String tipo) {
        if (tipo == ICD) {
            RETURN_CAT.addAll(MX_SB_MLT_Catalogos_Claim_Selector.getCatalog(descripcion, ICD));
        }
        if (tipo == OCP) {
        	RETURN_CAT.addAll(MX_SB_MLT_Catalogos_Claim_Selector.getCatalog(descripcion, OCP));
        }
        return RETURN_CAT;
    }

}