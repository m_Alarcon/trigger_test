/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_CatalogoMinutas_Service_Test
* @Author   	Héctor Saldaña | hectorisrael.saldana.contractor@bbva.com
* @Date     	Created: 2020-03-08
* @Description 	Test Class for MX_BPP_CatalogoMinutas_Service
* @Changes
*
*/
@isTest
public class MX_BPP_CatalogoMinutas_Service_Test {
    
    /**
    * @Description 	Test Method for checkInnerTagsIns
    * @Return 		NA
    **/
	@isTest
    static void checkInnerTagsInsTest() {
        final MX_BPP_CatalogoMinutas__c catalogoRecord = new MX_BPP_CatalogoMinutas__c();
        catalogoRecord.MX_Acuerdos__c = 1;
        catalogoRecord.MX_Asunto__c = 'Prueba Catalogo';
        catalogoRecord.Name = 'TestName';
        catalogoRecord.MX_TipoVisita__c = 'TestTV';
        catalogoRecord.MX_Saludo__c = 'Estimado {NombreCliente}';
        catalogoRecord.MX_Contenido__c = 'Contenido Test {OpcionalTexto1}{FechaAcuerdos}';
        catalogoRecord.MX_Despedida__c = 'Saludos Cordiales';
        catalogoRecord.MX_Firma__c = 'Firma Test';
        catalogoRecord.MX_ImageHeader__c = 'Test_Image';
        catalogoRecord.MX_CuerpoCorreo__c = 'Contenido Correo para {NombreCliente}';
        catalogoRecord.MX_Opcionales__c = '{OpcionalTexto1}Test{OpcionalTexto1}';
        
        Test.startTest();
        final Database.SaveResult result = Database.insert(catalogoRecord);
        Test.stopTest();
        
        System.assert(result.isSuccess(), 'Error on insert');
    }
    
    /**
    * @Description 	Test Method for checkInnerTagsUpd with lists
    * @Return 		NA
    **/
	@isTest
    static void checkInnerTagsUpdTest() {
        final List<MX_BPP_CatalogoMinutas__c> listRecords = new List<MX_BPP_CatalogoMinutas__c>();
        final List<MX_BPP_CatalogoMinutas__c> listRecordsUpd  = new List<MX_BPP_CatalogoMinutas__c>();
        final MX_BPP_CatalogoMinutas__c catalogoRecord1 = new MX_BPP_CatalogoMinutas__c();
        catalogoRecord1.MX_Acuerdos__c = 1;
        catalogoRecord1.MX_Asunto__c = 'Prueba Catalogo';
        catalogoRecord1.Name = 'Test';
        catalogoRecord1.MX_TipoVisita__c = 'TestTipoV';
        catalogoRecord1.MX_Contenido__c = 'Test Cont';
        catalogoRecord1.MX_Firma__c = 'Test Firma';
        catalogoRecord1.MX_ImageHeader__c = 'Test_Image';
        catalogoRecord1.MX_CuerpoCorreo__c = 'Contenido Correo para {NombreCliente}';
        catalogoRecord1.MX_Opcionales__c = '';
        listRecords.add(catalogoRecord1);
        
        final MX_BPP_CatalogoMinutas__c catalogoRecord2 = new MX_BPP_CatalogoMinutas__c();
        catalogoRecord2.MX_Acuerdos__c = 1;
        catalogoRecord2.MX_Asunto__c = 'Prueba Catalogo';
        catalogoRecord2.Name = 'Test2';
        catalogoRecord2.MX_TipoVisita__c = 'Test2';
        catalogoRecord2.MX_Contenido__c = 'Test2 Cont';
        catalogoRecord2.MX_Firma__c = 'Test2 Firma';
        catalogoRecord2.MX_ImageHeader__c = 'Test_Image';
        catalogoRecord2.MX_CuerpoCorreo__c = 'Contenido Correo para {NombreCliente}';
        catalogoRecord2.MX_Opcionales__c = '';
        listRecords.add(catalogoRecord2);
        insert listRecords;
        
        Test.startTest();
        catalogoRecord1.MX_Opcionales__c = '{OpcionalTexto1}Test{OpcionalTexto1}';
        catalogoRecord1.MX_Contenido__c = 'Contenido {OpcionalTexto1}';
        catalogoRecord2.MX_Saludo__c = 'Estimado {NombreCliente}';
        catalogoRecord2.MX_Despedida__c = 'Saludos Cordiales';
        catalogoRecord2.MX_Firma__c = 'Firma de Prueba';
        listRecordsUpd.add(catalogoRecord1);
        listRecordsUpd.add(catalogoRecord2);
        final Database.SaveResult[] result = Database.update(listRecordsUpd);
        Test.stopTest();
        
        System.assert(result[0].isSuccess() && result[1].isSuccess(), 'Error on update');
    }
    
    /**
    * @Description 	Test Method for constructor
    * @Return 		NA
    **/
	@isTest
    static void constructorTest() {
        final MX_BPP_CatalogoMinutas_Service constInstance = new MX_BPP_CatalogoMinutas_Service();
        System.assert(constInstance <> null, 'Error on constructor');
    }

}