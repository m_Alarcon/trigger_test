/**
* ---------------------------------------------------------------------------------
* @Autor: Rodrigo Amador Martinez Pacheco
* @Proyecto: Auditoria
* @Descripción : Pruebas unitarias para MX_RTL_Allowed_Extensions_selector
* ---------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ---------------------------------------------------------------------------------
* 1.0           18/06/2020     Rodrigo Martinez               Creación de la clase
* ---------------------------------------------------------------------------------
*/
@istest
public class MX_RTL_Allowed_Extensions_selector_test {

    /**
    * 
    * @Descripción : Contiene una extension de archivo no permitida en SF MX
    * @author Rodrigo Martinez
    */
    private static final String EXT_NO_EXISTENTE = 'exe';
    /**
    * 
    * @Descripción : Contiene una extension de archivo permitida en SF MX
    * @author Rodrigo Martinez
    */
    private static final String EXT_EXISTENTE = 'pdf';
     /**
    * 
    * @Descripción : Valor minimo de comparacion para extensiones
    * @author Rodrigo Martinez
    */
    private static final Integer MINIMO_ACEPTADO = 0;
     /**
    * 
    * @Descripción : Prueba el escenario cuando se busca una extension no permitida
    * @author Rodrigo Martinez
    */
    @istest
    public  static void selectByExtensionNoExistenteTest() {
        final List<MX_RTL_Allowed_Extensions__mdt> selectByExtension = 
            MX_RTL_Allowed_Extensions_selector.selectByExtension(EXT_NO_EXISTENTE);
        System.assertEquals(selectByExtension.size(), MINIMO_ACEPTADO,'La extension no deberia estar registrada en el custommetadata');
    }
    /**
    * 
    * @Descripción : Prueba el escenario cuando se busca una extension permitida
    * @author Rodrigo Martinez
    */
    @istest
    public  static void selectByExtensionExistenteTest() {
        final List<MX_RTL_Allowed_Extensions__mdt> selectByExtension = 
            MX_RTL_Allowed_Extensions_selector.selectByExtension(EXT_EXISTENTE);
        System.assert(selectByExtension.size() > MINIMO_ACEPTADO ,'La extension no esta registrada en el custommetadata');
    }
}