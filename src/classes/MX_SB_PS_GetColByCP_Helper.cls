/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_PS_GetColByCP_Helper
* Autor Daniel Perez Lopez
* Proyecto: Salesforce Presuscritos
* Descripción : Clase para obtener token en consumos iaso

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           26/10/2020      Daniel Lopez                         Creación
* 1.1           04/11/2020      Daniel Lopez                        Modificación
* 1.2           11/03/2021      Juan Benitez               Se cambia valor hard de endpoint
* --------------------------------------------------------------------------------
*/
@SuppressWarnings('sf:AvoidGlobalModifier')
global class MX_SB_PS_GetColByCP_Helper implements iaso.GBL_Integration_Headers {
    /*
    * Method para obtener token
    */
    global HttpRequest modifyRequest(HttpRequest request) {
       final Http http = new Http();
       final HttpRequest validationRequest = new HttpRequest();
       validationRequest.setEndpoint(Label.MX_SB_PS_EndPoint);
       validationRequest.setMethod('POST');
       validationRequest.setHeader('Content-Type', 'application/json;charset=UTF-8');
       validationRequest.setHeader('Content-Type', 'application/json');
       validationRequest.setHeader('Accept-Encoding', 'gzip,deflate');
       validationRequest.setHeader('Host', 'bbvacifrado.smartcenterservicios.com');
       validationRequest.setHeader('Connection', 'Keep-Alive');
       validationRequest.setHeader('User-Agent', 'PostmanRuntime/7.11.0');
       validationRequest.setHeader('content-length', '56');
       validationRequest.setHeader('Cache-Control', 'no-cache');
       validationRequest.setHeader('Accept', '*/*');
       String validReqBody ='{"authentication": {"userID": "ZM10137","consumerID": "10000137","authenticationType": "04",';
       validReqBody +='"authenticationData": [{"idAuthenticationData": "password","authenticationData": ["ODBjYzllMTViZDZi"]}]},'+
  +'"backendUserRequest": {"userId": "","accessCode": "","dialogId": ""}}';
       validationRequest.setBody(validReqBody);
       final HttpResponse response = http.send(validationRequest);
       request.setHeader('tsec', response.getHeader('tsec'));
        return request;
    }
}