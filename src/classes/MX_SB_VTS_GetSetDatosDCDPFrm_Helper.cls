/**
 * @File Name          : MX_SB_VTS_GetSetDatosDCDPFrm_Helper.cls
 * @Description        : Clase Encargada de Proporcionar Soporte
 *                       a la clase: MX_SB_VTS_GetSetDatosDCDPFrm_Service
 * @Author             : Alexandro Corzo
 * @Group              : 
 * @Last Modified By   : Diego Olvera
 * @Last Modified On   : 03-01-2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0       17/11/2020      Alexandro Corzo        Initial Version
 * 1.1       08/02/2021      Alexandro Corzo        Se realizan ajustes a clase para Codesmells
 * 1.2       24/02/2021      Diego  Olvera          Se agrega validacion que impide creacion automatica de registros en direccion
 * 1.3       04/03/2021      Alexandro Corzo        Se realizan ajustes a la clase bug reportado
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_GetSetDatosDCDPFrm_Helper {
    /** Variable de Apoyo: FLDOPPID */
    final static String FLDOPPID = 'OppoId';
    /** Variable de Apoyo: FLDTELCEL */
    final static String FLDTELCEL = 'TelefonoCel';
    /** Variable de Apoyo: FLDDIRPROP */
    final static String FLDDIRPROP = 'DirProp';
    /** Variable de Apoyo: FLDDIRCONT */
    final static String FLDDIRCONT = 'DirCont';
    /** Variable de Apoyo: OBJNULLASIG */
    final static String OBJNULLASIG = null;
    /** Variable de Apoyo: FLDCITY */
    final static String FLDCITY = 'city';
    /** Variable de Apoyo: FLDCOUNTY */
    final static String FLDCOUNTY = 'county';
    /** Variable de Apoyo: FLDTYPE */
    final static String FLDTYPE = 'type';
    /** Variable de Apoyo: STRSPACEFLD */
    final static String STRSPACEFLD = ' ';
    /** Variable de Apoyo: STRNULL */
    final static String STRNULL = '';

	/**
     * @description : Realiza el registro de la Quote conforme a los
     *                datos recibidos en el formulario: Datos de Contratante
     * @author:     : Alexandro Corzo
     * @return:     : Boolean isSuccess
     */
    public static Boolean saveDataQuote(Map<Object, Object> mDataQuote) {
        Boolean isSuccess = false;
        try {
            final Opportunity objOpportunity = MX_RTL_Opportunity_Selector.getOpportunity(mDataQuote.get(FLDOPPID).toString(), 'Name');
            final Quote objQuote = new Quote();
            objQuote.Name = objOpportunity.Name;
            objQuote.Id = mDataQuote.get('QuoteId').toString();
            objQuote.OpportunityId = mDataQuote.get(FLDOPPID).toString();
            objQuote.MX_SB_VTS_Nombre_Contrante__c = mDataQuote.get('NombreCliente').toString();
            objQuote.MX_SB_VTS_Apellido_Paterno_Contratante__c = mDataQuote.get('ApellidoPaterno').toString();
            objQuote.MX_SB_VTS_Apellido_Paterno_Contratante__c = mDataQuote.get('ApellidoMaterno').toString();
            objQuote.MX_SB_VTS_Sexo_Conductor__c = Boolean.valueOf(mDataQuote.get('SexoMasculino')) ? 'H' : 'M';
            objQuote.MX_SB_VTS_FechaNac_Contratante__c = Date.valueOf(mDataQuote.get('FechaNac').toString());
            objQuote.MX_SB_VTS_LugarNac_Contratante__c = mDataQuote.get('LugarNac').toString();
            objQuote.MX_SB_VTS_RFC__c = mDataQuote.get('rfc').toString();
            objQuote.MX_SB_VTS_Homoclave_Contratante__c = mDataQuote.get('Homoclave').toString();
            objQuote.MX_SB_VTS_Movil_txt__c = mDataQuote.get(FLDTELCEL).toString();
            objQuote.Email = mDataQuote.get('CorreoElect').toString();
            objQuote.MX_SB_VTS_Curp_Contratante__c = mDataQuote.get('Curp').toString();
            MX_RTL_Quote_Selector.upsrtQuote(objQuote);
            isSuccess = true;
        } catch(Exception e) {
            isSuccess = false;
        }
        return isSuccess;
    }
    
    /**
     * @description : Realiza el registro o actualización de la Dirección
     *              : de la Propiedad
     * @author:     : Alexandro Corzo
     * @return:     : Boolean isSuccess
     */
    public static Boolean saveDataAddress(Map<Object, Object> mDataQuote, Map<Object, Object> mDataAddress) {
    	Boolean isSuccess = false;
        try {
	        final List<MX_RTL_MultiAddress__c> lstMultiQuery = MX_RTL_MultiAddress_Selector.rsttQryMulAdd('Id', 'Name = \'Direccion Propiedad\' AND MX_RTL_Opportunity__c = \'' + mDataQuote.get(FLDOPPID).toString() + '\'', true);
            final MX_RTL_MultiAddress__c objMultiAddr = lstMultiQuery[0];
            final MX_RTL_MultiAddress__c objMultiAddrDP = new MX_RTL_MultiAddress__c();
            objMultiAddrDP.Id = String.valueOf(objMultiAddr.Id);
            objMultiAddrDP.Name = 'Direccion Propiedad';
            objMultiAddrDP.MX_RTL_PostalCode__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRPROP)).get('DirPropiedad_CP').toString();
            objMultiAddrDP.MX_RTL_AddressState__c  = ((Map<Object, Object>) mDataAddress.get(FLDDIRPROP)).get('DirPropiedad_Estado').toString();
            objMultiAddrDP.MX_RTL_AddressCity__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRPROP)).get('DirPropiedad_Ciudad').toString();
            objMultiAddrDP.MX_RTL_AddressMunicipality__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRPROP)).get('DirPropiedad_Alcaldia').toString();
            objMultiAddrDP.MX_RTL_AddressStreet__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRPROP)).get('DirPropiedad_Calle').toString();
            objMultiAddrDP.MX_RTL_AddressExtNumber__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRPROP)).get('DirPropiedad_NumExt').toString();
            objMultiAddrDP.MX_RTL_AddressIntNumber__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRPROP)).get('DirPropiedad_NumInt').toString();
            objMultiAddrDP.MX_RTL_AddressMunicipalityCode__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRPROP)).get('DirPropiedad_CountryId').toString();
            objMultiAddrDP.MX_RTL_AddressStateCode__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRPROP)).get('DirPropiedad_StateId').toString();
            objMultiAddrDP.MX_RTL_AddressCityCode__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRPROP)).get('DirPropiedad_CityId').toString();
            objMultiAddrDP.MX_SB_VTS_Colonias__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRPROP)).get('DirPropiedad_ColoniaLbl').toString();
            objMultiAddrDP.MX_RTL_AddressCountryCode__c = '052';
            objMultiAddrDP.MX_RTL_AddressCountry__c = 'Mexico';
            if(Boolean.valueOf(mDataAddress.get('EnvFisPoliz'))) {
                if(mDataAddress.get('EnvTypeVal').toString().equals('dirpropiedad')) {
                    objMultiAddrDP.MX_RTL_SendPolice__c = true;
                } else {
                    objMultiAddrDP.MX_RTL_SendPolice__c = false;
                }	
            }
            MX_RTL_MultiAddress_Selector.upsertAddress(new List<MX_RTL_MultiAddress__c>{objMultiAddrDP});
            if(Boolean.valueOf(((Map<Object, Object>) mDataAddress.get(FLDDIRCONT)).get('DirContratante_CdP')) && Boolean.valueOf(((Map<Object, Object>) mDataAddress.get('DirCont')).get('sameDir'))) {
                saveDataAddrDC(mDataQuote, mDataAddress);
            }
            isSuccess = true;
        } catch(Exception e) {
            isSuccess = false;
        }
        return isSuccess;
    }

    /**
     * @description : Realiza el registro o actualización de la Dirección
     *              : del Contratante
     * @author:     : Alexandro Corzo
     * @return:     : Boolean isSuccess
     */
    public static Boolean saveDataAddrDC (Map<Object, Object> mDataQuote, Map<Object, Object> mDataAddress) {
        Boolean isSuccess = false;
        try {
            final MX_RTL_MultiAddress__c objMultiAddrDC = new MX_RTL_MultiAddress__c();
            final Integer intExistDC = database.countQuery('SELECT COUNT() FROM MX_RTL_MultiAddress__c WHERE Name = \'Direccion Contratante\' AND MX_RTL_Opportunity__c = \'' + mDataQuote.get(FLDOPPID).toString() + '\'');
            if(intExistDC == 0) {
                final Opportunity objOpportunity = MX_RTL_Opportunity_Selector.getOpportunity(mDataQuote.get(FLDOPPID).toString(), 'AccountId');
                objMultiAddrDC.MX_RTL_MasterAccount__c = objOpportunity.AccountId;
            } else {
                final List<MX_RTL_MultiAddress__c> lstMultiQueryC = MX_RTL_MultiAddress_Selector.rsttQryMulAdd('Id', 'Name = \'Direccion Contratante\' AND MX_RTL_Opportunity__c = \'' + mDataQuote.get(FLDOPPID).toString() + '\'',true);
                final MX_RTL_MultiAddress__c objMultiAddrDirC = lstMultiQueryC[0];
                objMultiAddrDC.Id = String.valueOf(objMultiAddrDirC.Id);
            }
            objMultiAddrDC.Name = 'Direccion Contratante';
            objMultiAddrDC.MX_RTL_Opportunity__c = mDataQuote.get(FLDOPPID).toString();
            objMultiAddrDC.MX_RTL_PostalCode__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRCONT)).get('DirContratante_CP').toString();
            objMultiAddrDC.MX_RTL_AddressState__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRCONT)).get('DirContratante_Estado').toString();
            objMultiAddrDC.MX_RTL_AddressCity__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRCONT)).get('DirContratante_Ciudad').toString();
            objMultiAddrDC.MX_RTL_AddressMunicipality__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRCONT)).get('DirContratante_Alcaldia').toString();
            objMultiAddrDC.MX_RTL_AddressStreet__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRCONT)).get('DirContratante_Calle').toString();
            objMultiAddrDC.MX_RTL_AddressExtNumber__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRCONT)).get('DirContratante_NumExt').toString();
            objMultiAddrDC.MX_RTL_AddressIntNumber__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRCONT)).get('DirContratante_NumInt').toString();
            objMultiAddrDC.MX_RTL_AddressMunicipalityCode__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRCONT)).get('DirContratante_CountryId').toString();
            objMultiAddrDC.MX_RTL_AddressStateCode__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRCONT)).get('DirContratante_StateId').toString();
            objMultiAddrDC.MX_RTL_AddressCityCode__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRCONT)).get('DirContratante_CityId').toString();
            objMultiAddrDC.MX_SB_VTS_Colonias__c = ((Map<Object, Object>) mDataAddress.get(FLDDIRCONT)).get('DirContratante_ColoniaLbl').toString();
            objMultiAddrDC.MX_RTL_AddressCountryCode__c = '052';
            objMultiAddrDC.MX_RTL_AddressCountry__c = 'Mexico';
            objMultiAddrDC.MX_RTL_AddressType__c = 'Domicilio cliente';
            if(Boolean.valueOf(mDataAddress.get('EnvFisPoliz'))) {
                if(mDataAddress.get('EnvTypeVal').toString().equals('dircontratante')) {
                    objMultiAddrDC.MX_RTL_SendPolice__c = true;
                } else {
                    objMultiAddrDC.MX_RTL_SendPolice__c = false;
                }	
            }
            MX_RTL_MultiAddress_Selector.upsertAddress(new List<MX_RTL_MultiAddress__c>{objMultiAddrDC});
            isSuccess = true;
        } catch(Exception e) {
            isSuccess = false;
        }
        return isSuccess;
    }
    
    /**
     * @description : Realiza el llenado del Wrapper en el apartado de Header
     *              : para el armado del body del servicio ASO CreateCustomerData
     * @author:     : Alexandro Corzo
     * @return:     : MX_SB_VTS_wrpCreateCustomerDataSrv.header objWrapperHeader
     */
    public static MX_SB_VTS_wrpCreateCustomerDataSrv.header fillDataHeader() {
        final MX_SB_VTS_wrpCreateCustomerDataSrv.header objWrapperHeader = new MX_SB_VTS_wrpCreateCustomerDataSrv.header();
        objWrapperHeader.aapType = '10000137';
        objWrapperHeader.dateRequest = '2020-10-22 17:27:17.040';
        objWrapperHeader.channel = '4';
        objWrapperHeader.subChannel = '26';
        objWrapperHeader.branchOffice = '';
        objWrapperHeader.managementUnit = 'DHSDT003';
        objWrapperHeader.user = 'jmeter';
        objWrapperHeader.idSession = '3232-3232';
        objWrapperHeader.idRequest = '1212-121212-12121-212';
        objWrapperHeader.dateConsumerInvocation = '2020-10-22 17:27:17.040';
        return objWrapperHeader;
    }
    
    /**
     * @description : Realiza el llenado del Wrapper en el apartado de Quote
     *              : para el armado del body del servicio ASO CreateCustomerData
     * @author:     : Alexandro Corzo
     * @return:     : MX_SB_VTS_wrpCreateCustomerDataSrv.quote objWrapperQuote
     */
    public static MX_SB_VTS_wrpCreateCustomerDataSrv.quote fillDataQuote(String strIdQuoteSrv) {
        final MX_SB_VTS_wrpCreateCustomerDataSrv.quote objWrapperQuote = new MX_SB_VTS_wrpCreateCustomerDataSrv.quote();
        objWrapperQuote.idQuote = strIdQuoteSrv;
        objWrapperQuote.compensationPlan = null;
        objWrapperQuote.credit = null;
        objWrapperQuote.dateQuote = null;
        objWrapperQuote.payment = null;
        objWrapperQuote.status = null;
        objWrapperQuote.totalPremium = null;
        objWrapperQuote.totalPremiumLocalCurrency = null;
        return objWrapperQuote;
    }
    
    /**
     * @description : Realiza el llenado del Wrapper en el apartado de ClientType
     *              : para el armado del body del servicio ASO CreateCustomerData
     * @author:     : Alexandro Corzo
     * @return:     : MX_SB_VTS_wrpCreateCustomerDataSrv.clientType objWrapperCType
     */
    public static MX_SB_VTS_wrpCreateCustomerDataSrv.clientType fillDataClientType() {
        final MX_SB_VTS_wrpCreateCustomerDataSrv.clientType objWrapperCType = new MX_SB_VTS_wrpCreateCustomerDataSrv.clientType();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.catalogItemBase objWrapperCIB = new MX_SB_VTS_wrpCreateCustomerDataSrv.catalogItemBase();
        objWrapperCIB.id = 'P';
        objWrapperCIB.name = 'PERSONA';
        objWrapperCType.catalogItemBase = objWrapperCIB;
        return objWrapperCType;
    }
    
    /**
     * @description : Realiza el llenado del Wrapper en el apartado de Holder
     *              : para el armado del body del servicio ASO CreateCustomerData
     * @author:     : Alexandro Corzo
     * @return:     : MX_SB_VTS_wrpCreateCustomerDataSrv.holder objWrapperHolder
     */
    public static MX_SB_VTS_wrpCreateCustomerDataSrv.holder fillDataHolder(Map<Object, Object> mDataObjDC, Map<Object, Object> mDataObjDP) {
        final MX_SB_VTS_wrpCreateCustomerDataSrv.holder objWrapperHolder = new MX_SB_VTS_wrpCreateCustomerDataSrv.holder();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.activity objWrapperAct = new MX_SB_VTS_wrpCreateCustomerDataSrv.activity();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.nacionality objWrapperNac = new MX_SB_VTS_wrpCreateCustomerDataSrv.nacionality();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.mainAddress objWrapperMainA = new MX_SB_VTS_wrpCreateCustomerDataSrv.mainAddress();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.correspondenceAddress objWrapperCAddr = new MX_SB_VTS_wrpCreateCustomerDataSrv.correspondenceAddress();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.legalAddress objWrapperLAddr = new MX_SB_VTS_wrpCreateCustomerDataSrv.legalAddress();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.deliveryInformation objWrapperDelInfo = new MX_SB_VTS_wrpCreateCustomerDataSrv.deliveryInformation();
        objWrapperAct.catalogItemBase = MX_SB_VTS_GetSetDatosDCDPAux_Helper.fillDataCIBase('701', 'COMERCIO');
        objWrapperHolder.id = OBJNULLASIG;
        objWrapperHolder.account = OBJNULLASIG;
        objWrapperHolder.email = valCorreoEle(mDataObjDC);
        objWrapperNac.catalogItemBase = MX_SB_VTS_GetSetDatosDCDPAux_Helper.fillDataCIBase('052', 'MEXICO');
        objWrapperHolder.rfc = mDataObjDC.get('rfc').toString();
        objWrapperHolder.moralPersonalityData = OBJNULLASIG;
        final String strMaDoor = ((Map<Object, Object>) mDataObjDP.get(FLDDIRCONT)).get('DirContratante_NumInt').toString();
        objWrapperMainA.door = strMaDoor.equalsIgnoreCase('') ? ' ' : strMaDoor;
        objWrapperMainA.outdoorNumber = ((Map<Object, Object>) mDataObjDP.get(FLDDIRCONT)).get('DirContratante_NumExt').toString();
        objWrapperMainA.streetName = ((Map<Object, Object>) mDataObjDP.get(FLDDIRCONT)).get('DirContratante_Calle').toString();
        objWrapperMainA.suburb = MX_SB_VTS_GetSetDatosDCDPAux_Helper.fillDataSUrb(new Map<String, Object>{FLDTYPE => FLDDIRCONT, FLDCITY => OBJNULLASIG, FLDCOUNTY => OBJNULLASIG, 'state' =>  OBJNULLASIG}, mDataObjDC, mDataObjDP);
        objWrapperMainA.zipCode = ((Map<Object, Object>) mDataObjDP.get(FLDDIRCONT)).get('DirContratante_CP').toString();
        final String strCorreDoor = ((Map<Object, Object>) mDataObjDP.get(FLDDIRPROP)).get('DirPropiedad_NumInt').toString();
        objWrapperCAddr.door = strCorreDoor.equalsIgnoreCase('') ? ' ' : strCorreDoor;
        objWrapperCAddr.outdoorNumber = ((Map<Object, Object>) mDataObjDP.get(FLDDIRPROP)).get('DirPropiedad_NumExt').toString();
        objWrapperCAddr.streetName = ((Map<Object, Object>) mDataObjDP.get(FLDDIRPROP)).get('DirPropiedad_Calle').toString();
        objWrapperCAddr.suburb = MX_SB_VTS_GetSetDatosDCDPAux_Helper.fillDataSUrb(new Map<String, Object>{FLDTYPE => FLDDIRPROP, FLDCITY => OBJNULLASIG, FLDCOUNTY => OBJNULLASIG, 'state' => OBJNULLASIG}, mDataObjDC, mDataObjDP);
        objWrapperCAddr.zipCode = ((Map<Object, Object>) mDataObjDP.get(FLDDIRPROP)).get('DirPropiedad_CP').toString();
		final String strLegDoor = ((Map<Object, Object>) mDataObjDP.get(FLDDIRCONT)).get('DirContratante_NumInt').toString();     
        objWrapperLAddr.door = strLegDoor.equalsIgnoreCase('') ? ' ' : strLegDoor;
        objWrapperLAddr.outdoorNumber = ((Map<Object, Object>) mDataObjDP.get(FLDDIRCONT)).get('DirContratante_NumExt').toString();
        objWrapperLAddr.streetName = ((Map<Object, Object>) mDataObjDP.get(FLDDIRCONT)).get('DirContratante_Calle').toString();
        objWrapperLAddr.suburb = MX_SB_VTS_GetSetDatosDCDPAux_Helper.fillDataSUrb(new Map<String, Object>{FLDTYPE => FLDDIRCONT, FLDCITY => OBJNULLASIG, FLDCOUNTY => OBJNULLASIG, 'state' => OBJNULLASIG}, mDataObjDC, mDataObjDP);
        objWrapperLAddr.zipCode = ((Map<Object, Object>) mDataObjDP.get(FLDDIRCONT)).get('DirContratante_CP').toString();
        objWrapperDelInfo.deliveryInstructions = ' ';
        objWrapperDelInfo.deliveryTimeEnd = '2016-10-13 13:34:31';
        objWrapperDelInfo.deliveryTimeStart = '2016-10-13 13:34:31';
        objWrapperDelInfo.referenceStreets = ' ';
        objWrapperHolder.activity = objWrapperAct;
        objWrapperHolder.nacionality = objWrapperNac;
        objWrapperHolder.physicalPersonalityData = MX_SB_VTS_GetSetDatosDCDPAux_Helper.fillDataPPL(mDataObjDC);
        objWrapperHolder.mainAddress = objWrapperMainA;
        objWrapperHolder.mainContactData = MX_SB_VTS_GetSetDatosDCDPAux_Helper.fillMCData(mDataObjDC, mDataObjDP);
        objWrapperHolder.correspondenceAddress = objWrapperCAddr;
        objWrapperHolder.correspondenceContactData = MX_SB_VTS_GetSetDatosDCDPAux_Helper.fillCContD(mDataObjDC, mDataObjDP);
        objWrapperHolder.legalAddress = objWrapperLAddr;
        objWrapperHolder.fiscalContactData = MX_SB_VTS_GetSetDatosDCDPAux_Helper.fillFCData(mDataObjDC, mDataObjDP);
        objWrapperHolder.deliveryInformation = objWrapperDelInfo;
        return objWrapperHolder;
    }

    /**
     * @description : Valida si el correo electronico contiene información
     * @author:     : Alexandro Corzo
     * @return:     : String strCorreoEle
     */
    public static String valCorreoEle(Map<Object, Object> mDataObjDC) {
        return mDataObjDC.get('CorreoElect') == STRNULL ? STRSPACEFLD : mDataObjDC.get('CorreoElect').toString();
    }
}