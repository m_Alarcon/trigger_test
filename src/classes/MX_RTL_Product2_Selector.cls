/**
 * @File Name          : MX_RTL_Product2_Selector.cls
 * @Description        :
 * @Author             : Jaime Terrats
 * @Group              :
 * @Last Modified By   : Arsenio.perez.lopez.contractor@bbva.com
 * @Last Modified On   : 16-11-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    28/5/2020   Eduardo Hernandez Cuamatzi     Initial Version
 * 1.1    07/08/2020  Arsenio Perez Lopez            Add Query Method
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_RTL_Product2_Selector {
    /** 
     * @description list of possible filters 
     **/
    final static String SELECT_STR = 'SELECT ';
    /**
    * @description Recupera productos por nombre, activo y proceso
    * @author Eduardo Hernandez Cuamatzi | 3/6/2020
    * @param Set<String> namesProd nombre de los productos
    * @param Set<String> process Procesos a buscar
    * @param Boolean active Si el producto esta activo
    * @return List<Product2> Lista de productos encontrados
    **/
    public static List<Product2> findProsByNameProces(Set<String> namesProd, Set<String> process, Boolean active) {
        return [Select Id, Name, IsActive, MX_SB_SAC_Proceso__c,MX_WB_FamiliaProductos__c, CreatedDate from Product2
        where Name IN: namesProd AND IsActive =: active AND MX_SB_SAC_Proceso__c =: process AND MX_WB_FamiliaProductos__c NOT IN ('')];
    }
	 /**
	* @description 
    * @author Arsenio.perez.lopez.contractor@bbva.com | 11-12-2020 
    * @param String queryfield 
    * @param String conditionField 
    * @param boolean whitCondition 
    * @return List<Product2> 
    **/
    public static List<Product2> resultQueryProd(String queryfield, String conditionField, boolean whitCondition) {
        String sQuery;
        switch on String.valueOf(whitCondition) {
            when 'false' {
                sQuery = SELECT_STR + queryfield +' FROM Product2';
            }
            when 'true' {
                sQuery = SELECT_STR + queryfield +' FROM Product2 WHERE '+conditionField;
            }
        }
        return Database.query(String.escapeSingleQuotes(sQuery).unescapeJava());
      }
}