/**-------------------------------------------------------------------------
* Nombre: 		MX_SB_PS_GTServiceSF_cls
* @author 		Daniel Perez
* Proyecto: 	Prueba de Franting Ticket
* Descripción : Clase de Prueba de Granting Ticket
* --------------------------------------------------------------------------
*                         Fecha           Autor                   Desripción
* --------------------------------------------------------------------------
* @version 1.0            03/06/2019      Daniel Perez         Creación de la Clase
* --------------------------------------------------------------------------*/
@SuppressWarnings('sf:AvoidGlobalModifier')
global class MX_SB_PS_GTServiceSF_cls implements iaso.GBL_Integration_Headers {
	/**
     * @description: 	Realiza el consumo del servicio mock: getGTServicesSF 
     * 				 	el cual se encarga de devolver el tsec
     * @author: 		Daniel Perez
     * @return: 		HttpRequest
     */
    global HttpRequest modifyRequest(HttpRequest request) {
    	request.setMethod('POST');
        request.setHeader('Host', 'https://test-sf.bbva.mx');
        
        return request;
    }
}