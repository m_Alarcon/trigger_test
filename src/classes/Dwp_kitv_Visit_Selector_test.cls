/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase test de dwp_kitv_Visit - selector

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     25/05/2020      Roberto Isaac Soto Granados           Creación Clase
*   1.1     07/09/2020      Marco Cruz                            Se agrega method test
*   1.2     07/10/2020      Gabriel García                        Se agregan methods test
*	1.3		13/01/2021		Edmundo Zacarias					  Se agrega method test
*****************************************************************************************/
@isTest
private class Dwp_kitv_Visit_Selector_test {
    /*Usuario de pruebas*/
    private static User testUser = new User();
    /*Caso de pruebas*/
    private static Case testCase = new Case();
    /*Aclaración de pruebas*/
    private static BPyP_Aclaraciones__c testAclara = new BPyP_Aclaraciones__c();
    /*cuenta de pruebas*/
    private static Account testAcc = new Account();
    /*Visita de pruebas*/
    private static dwp_kitv__Visit__c testVisit = new dwp_kitv__Visit__c();
    /*String para folios*/
    private static String testCode = '1234';

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testUser = UtilitysDataTest_tst.crearUsuario('testUser', 'BPyP Estandar', 'BPYP BANQUERO BANCA PERISUR');
        testUser.Title = 'Privado';
        insert testUser;

        System.runAs(testUser) {
            testAcc.LastName = 'testAcc';
            testAcc.FirstName = 'testAcc';
            testAcc.OwnerId = testUser.Id;
            testAcc.No_de_cliente__c = testCode;
            insert testAcc;
            testCase.OwnerId = testUser.Id;
            testCase.Status = 'Nuevo';
            testCase.MX_SB_SAC_Folio__c = testCode;
            testCase.Description = 'Description';
            testCase.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_EU_Case_Apoyo_General'].Id;
            insert testCase;
            testAclara.BPyP_Producto__c = 'TDC';
            testAclara.BPyP_Tarjeta__c = testCode;
            testAclara.BPyP_Folio__c = testCode;
            testAclara.BPyP_NumeroCliente__c = testCode;
            testAclara.BPyP_Caso__c = testCase.Id;
            insert testAclara;
            testVisit.dwp_kitv__visit_start_date__c = Date.today()+4;
            testVisit.dwp_kitv__visit_duration_number__c = '15';
            testVisit.dwp_kitv__visit_status_type__c = '04';
            insert testVisit;
        }
    }

    /*Ejecuta la acción para cubrir clase*/
    static testMethod void creaVisitaTest() {
        Id aclaraciones;
        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        System.runAs(testUser) {
            Test.startTest();
            	testVisit.dwp_kitv__visit_start_date__c = Date.today()+4;
            	testVisit.dwp_kitv__visit_duration_number__c = '15';
            	testVisit.dwp_kitv__visit_status_type__c = '04';
            	aclaraciones = Dwp_kitv_Visit_Selector.creaVisita(new List<dwp_kitv__Visit__c>{testVisit});
            Test.stopTest();
        }
        System.assert(!String.isBlank(aclaraciones), 'Visita creada');
    }

    /*
    * @description method que prueba Dwp_kitv_Visit_Selector insertVisit
    * @param  void
    */
    @isTest
    static void insertVisitTest() {
        Final Id visitRT = Schema.SObjectType.dwp_kitv__Visit__c.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Tipifi_Ventas_TLMKT).getRecordTypeId();
        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        System.runAs(testUser) {
            Test.startTest();
            	Final List<dwp_kitv__Visit__c> listTest = new List<dwp_kitv__Visit__c>();
            		Final datetime insertDate = datetime.now();
            		testVisit.RecordTypeId = visitRT;
                    testVisit.dwp_kitv__visit_duration_number__c = '15';
                    testVisit.dwp_kitv__visit_start_date__c = insertDate;
                    testVisit.Name = 'Llamada Realizada ' + insertDate;
            		listTest.add(testVisit);
                    Dwp_kitv_Visit_Selector.insertVisit(listTest);
            Test.stopTest();
        }
        System.assert(true, 'Visita creada');
    }

    /*
    * @description method que prueba Dwp_kitv_Visit_Selector updteVisit
    * @param  void
    */
    @isTest
    static void updteVisitTest() {
        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        System.runAs(testUser) {
            Test.startTest();
            testVisit.dwp_kitv__visit_start_date__c = Date.today()+4;
            testVisit.dwp_kitv__visit_duration_number__c = '15';
            testVisit.dwp_kitv__visit_status_type__c = '04';
            insert testVisit;
            testVisit.dwp_kitv__visit_start_date__c = Date.today() + 5;
            Dwp_kitv_Visit_Selector.updteVisit(new List<dwp_kitv__Visit__c>{testVisit});

            Test.stopTest();
        }
        final dwp_kitv__Visit__c visitResult = [SELECT Id, dwp_kitv__visit_start_date__c FROM dwp_kitv__Visit__c WHERE Id =:testVisit.Id];
        System.assertEquals(testVisit.dwp_kitv__visit_start_date__c, visitResult.dwp_kitv__visit_start_date__c, 'Conflicto en update');
    }

    /*
    * @description method que obtiene agregateResult
    * @param  void
    */
    @isTest
    static void fetchListAggVisitByDataBaseTest() {
        final List<dwp_kitv__Visit__c> listVisitasSel = [SELECT Id FROM dwp_kitv__Visit__c LIMIT 1];
        final List<AggregateResult> listVAggVisitT = Dwp_kitv_Visit_Selector.fetchListAggVisitByDataBase(' count(Id) visita', ' WHERE Id =: filtro0', new List<String>{listVisitasSel[0].Id,'','','','','','',''}, System.today(), System.today());
        System.assertEquals(listVAggVisitT[0].get('visita'), 1, 'Error SOQL');
    }

    /*
    * @description method que obtiene Lista de Visitas
    * @param  void
    */
    @isTest
    static void fetchListVisitByParamsTest() {
        final List<dwp_kitv__Visit__c> listVisitasSelT = [SELECT Id FROM dwp_kitv__Visit__c LIMIT 1];
        final List<dwp_kitv__Visit__c> listVisitT = Dwp_kitv_Visit_Selector.fetchListVisitByParams(' Id ', ' WHERE Id =: filtro0', new List<String>{listVisitasSelT[0].Id,'','','','','','',''}, System.today(), System.today());
        System.assertEquals(listVisitasSelT[0].Id, listVisitT[0].Id, 'SOQL Fail');
    }
    
    /*
    * @description Test method for Dwp_kitv_Visit_Selector.getVisitInfoByIds()
    * @param  void
    */
    @isTest
    static void getVisitInfoByIdsTest() {
        final Set<Id> visitIds = new Set<Id>();
        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        System.runAs(testUser) {
            testVisit.dwp_kitv__visit_duration_number__c = '15';
            testVisit.dwp_kitv__visit_start_date__c = Date.today()+4;
            testVisit.dwp_kitv__visit_status_type__c = '04';
            insert testVisit;
			visitIds.add(testVisit.Id);
        }
        Test.startTest();
        final List<dwp_kitv__Visit__c> resultSelector = Dwp_kitv_Visit_Selector.getVisitInfoByIds('Id, Name', visitIds);
        Test.stopTest();
        System.assertEquals(visitIds.size(), resultSelector.size(), 'SOQL Fail');
    }
}