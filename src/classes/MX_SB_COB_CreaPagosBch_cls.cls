/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_COB_CreaPagosBch_cls
* Autor Angel Nava
* Proyecto: Cobranza - BBVA Bancomer
* Descripción : Clase de proceso batch para creacion de pagos
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Desripción
* --------------------------------------------------------------------------------
* 1.0           01/05/2019     Angel Nava				          Creación
* --------------------------------------------------------------------------------
*/
global with sharing class MX_SB_COB_CreaPagosBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    /** String query */
    global string query { get;set; }
    /** String sResulProcArchivoOk */
    global String sResProArch { get;set; }
    /** String sIdArchivoTj */
    global String sIdArchivoTj { get;set; }
    /**Consulta */
    global MX_SB_COB_CreaPagosBch_cls (string query) {
        this.query = query;
    }
    /** */
    global Database.querylocator start(Database.BatchableContext batCnt) {
        sResProArch = '';
        sIdArchivoTj = '';
        return Database.getQueryLocator(this.query);
    }
    /** */
    global void execute(Database.BatchableContext batCnt, List<PagosEspejo__c> scope) {
		final List<PagosEspejo__c> lPagosEspejoUpd = new List<PagosEspejo__c>();
        for (PagosEspejo__c objCarArch : scope) {
			lPagosEspejoUpd.add(new PagosEspejo__c(id = objCarArch.id, MX_SB_COB_InsertCreaPago__c = true));
        }
        update lPagosEspejoUpd;

    }
    /** */
    global void finish(Database.BatchableContext batCnt) {

    }

}