@IsTest
/*
-------------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_MapLayoutNuevoSin_cls_TEST
* Autor Juan Carlos Benitez Herrera
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_MapLayoutNuevoSin_Controller

* -----------------------------------------------------------------------------------
* Versión       Fecha               Autor                               Descripción
* ------------------------------------------------------------------------------------
* 1.0           12/03/2020      Juan Carlos Benitez                         Creación
* 1.1           12/03/2020      Angel Nava                                  Migración de campos contract
* ------------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_MapLayoutNuevoSin_cls_TEST {
    @TestSetup
    static void creaDatos() {
            final Account personAc =new Account(name='Gruatest',recuperacion__c=false,Tipo_Persona__c='Física');
            insert personAc;
            final Contract contrato = new Contract(AccountId=personAc.id,MX_SB_SAC_NumeroPoliza__c='OT2040302203', name='JUAN CARLOS');
            insert contrato;
            final siniestro__c sinies= new Siniestro__c();
            sinies.MX_SB_MLT_APaternoConductor__c='BENITEZ';
        	sinies.MX_SB_SAC_Contrato__c=contrato.Id;
            sinies.MX_SB_MLT_AMaternoConductor__c='HERRERA';
            sinies.MX_SB_MLT_NombreConductor__c='JUAN CARLOS';
            sinies.RecordTypeId=Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_RamoAuto').getRecordTypeId();
            sinies.MX_SB_MLT_Origen__c = 'Phone';
            sinies.MX_SB_MLT_URLLocation__c = '19.5038795,-99.1802701';
            sinies.MX_SB_MLT_Address__c = 'Eje 5 Nte 990, Santa Barbara, 02230 Ciudad de México, CDMX, México';
            sinies.MX_SB_SAC_Contrato__c = contrato.Id;
        	sinies.MX_SB_MLT_NumeroCalle__c ='990';
            sinies.MX_SB_MLT_Calle__c ='Eje 5 Nte';
            sinies.MX_SB_MLT_Colonia__c='Santa Barbara';
            sinies.MX_SB_MLT_Estado__c='Ciudad de México';
            sinies.MX_SB_MLT_CodigoPostal__c='02230';
            insert sinies;
             Final String nameProfile = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User objUsrTst = MX_WB_TestData_cls.crearUsuario('PruebaAdminTst', nameProfile);  
        insert objUsrTst;
    }
        /*
    * @description method que prueba MX_SB_MLT_MapLayoutNuevoSin_Controller
    * @param  void
    * @return String 
    */
    @IsTest public static void method() {
        final Siniestro__c sinie= [SELECT id ,MX_SB_MLT_URLLocation__c FROM siniestro__c WHERE MX_SB_MLT_URLLocation__c = '19.5038795,-99.1802701' limit 1];
        ApexPages.currentPage().getParameters().put('19.5038795,-99.1802701',sinie.MX_SB_MLT_URLLocation__c);
        final PageReference pageRef = Page.CommunitiesSelfRegConfirm;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', sinie.id);
        final MX_SB_MLT_MapLayoutNuevoSin_Controller clas = new MX_SB_MLT_MapLayoutNuevoSin_Controller();
        final MX_SB_MLT_MapLayoutNuevoSin_Controller cla = new MX_SB_MLT_MapLayoutNuevoSin_Controller();
        final String urlLocation= Url.getSalesforceBaseUrl().toExternalForm();
        final String uRLo= urlLocation.replace('--c.visualforce.com', '.lightning.force.com');
        Test.startTest();
            clas.sin=sinie;
        	clas.updateSin();
        	cla.insin();
        	clas.urlLocation= urlLocation;
            clas.uRLo =uRLo;
        system.assertEquals(null,clas.updateSin(),'Exitoso');
        Test.stopTest();
    }
}