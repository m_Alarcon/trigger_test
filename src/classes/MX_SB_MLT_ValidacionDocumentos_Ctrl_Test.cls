@isTest
/**
 * @File Name          : MX_SB_MLT_ValidacionDocumentos_Ctrl_Test.cls
 * @Description        : Prueba los methodos de MX_SB_MLT_ValidacionDocumentos_Ctrl
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 01/06/2020, 09:40:22 AM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/26/2020   Juan Carlos Benitez         Initial Version
**/
public class MX_SB_MLT_ValidacionDocumentos_Ctrl_Test {
	        /*
	* var String Valor Subramo Ahorro
	*/
    public static final String SUBRAMO ='Fallecimiento';
    /*
	* var String Valor boton cancelar
	*/    
    public static final String CANCELAR = 'Cancelar';
    /*
	* var String Valor boton Volver
	*/    
    public static final String VOLVER = 'Volver';
    /*
	* var String Valor boton Crear
	*/    
    public static final String CREAR = 'Crear';
    
	/** Nombre de usuario Test */
    final static String NAME = 'Ichiji Vinsmoke';
    
    /** current user id */
    final static String USR_ID = UserInfo.getUserId();
    /**
     * @description
     * @author Juan Carlos Benitez | 5/26/2020
     * @return void
     **/
    @TestSetup
    static void createData() {
        Final String profileVidavalid = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User userVida = MX_WB_TestData_cls.crearUsuario(NAME, profileVidavalid);  
        insert userVida;
        System.runAs(userVida) {
            final String recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Proveedores_MA).getRecordTypeId();
            final Account accountVidaTest = new Account(RecordTypeId = recordTypeId, Name=NAME, Tipo_Persona__c='Física');
            insert accountVidaTest;
            final Contract contractVida = new Contract(AccountId = accountVidaTest.Id, MX_SB_SAC_NumeroPoliza__c='policyTest');
            insert contractVida;
            Final List<Siniestro__c> sinInsert = new List<Siniestro__c>();
            for(Integer i = 0; i < 10; i++) {
                Final Siniestro__c nSinvalida = new Siniestro__c();
                nSinvalida.MX_SB_SAC_Contrato__c = contractVida.Id;
                nSinvalida.MX_SB_MLT_NombreConductor__c = NAME + '_' + i;
                nSinvalida.MX_SB_MLT_APaternoConductor__c = 'LastName for '+NAME;
                nSinvalida.MX_SB_MLT_AMaternoConductor__c = 'LastName2 for '+NAME;
                nSinvalida.MX_SB_MLT_Fecha_Hora_Siniestro__c =date.valueof('2020-03-27T22:04:00.000+0000');
                nSinvalida.MX_SB_MLT_Telefono__c = '5540342107';
                Final datetime citaAgenda = datetime.now();
                nSinvalida.MX_SB_MLT_CitaVidaAsegurado__c = citaAgenda.addDays(2);
                nSinvalida.MX_SB_MLT_AtencionVida__c = 'Agendar Cita';
                nSinvalida.MX_SB_MLT_PreguntaVida__c = false;
                nSinvalida.MX_SB_MLT_SubRamo__c = SUBRAMO;
                nSinvalida.TipoSiniestro__c = 'Siniestros';
                nSinvalida.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_creacion;
                nSinvalida.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoVida).getRecordTypeId();
                sinInsert.add(nSinvalida);
            }
            Database.insert(sinInsert);
            Final List<MX_SB_VTS_Beneficiario__c> benefInsrt = new List<MX_SB_VTS_Beneficiario__c>();
            for(Integer i=0; i<10; i++) {
                Final MX_SB_VTS_Beneficiario__c nBen = new MX_SB_VTS_Beneficiario__c();
                nBen.MX_SB_MLT_ccId__c ='';
                nBen.MX_SB_MLT_Q_EnvioKitCorreo__c ='Si';
                nBen.MX_SB_MLT_Kit__c='K0'+i;
                benefInsrt.add(nBen);
            }
            Database.insert(benefInsrt);
        }
    }
    @isTest
    static void testsearchSinVida() {
        final String siniId =[SELECT Id from Siniestro__c WHERE MX_SB_MLT_NombreConductor__c =:  NAME + '_1' ].Id;
    test.startTest();
    	MX_SB_MLT_ValidacionDocumentos_Ctrl.searchSinVida(siniId);
        system.assert(true,'Siniestro Vida Localizado');
    test.stopTest();
    }
    
    @isTest
    static void testsearchKit() {
        final Siniestro__c sin =[SELECT Id from Siniestro__c WHERE MX_SB_MLT_NombreConductor__c =:  NAME + '_1' ];
        final Map<String,String> mapa= new Map<String, String>();
        	test.startTest();
       			mapa.put('product','Pyman');
                mapa.put('subramo','Fallecimiento');
                mapa.put('subtipo','Natural');
                mapa.put('regla1','menos de 2.5 mdp suma asegurada');
                mapa.put('regla2','18 a 55 años');
        		MX_SB_MLT_ValidacionDocumentos_Ctrl.searchkit(mapa);
        		MX_SB_MLT_ValidacionDocumentos_Ctrl.kit(mapa);
        		MX_SB_MLT_ValidacionDocumentos_Ctrl.updateSin(sin);
        		MX_SB_MLT_ValidacionDocumentos_Ctrl.siniestroAction(CREAR,sin.Id);
        		system.assert(true,'Se ha encontrado el Kit exitosamente');
        	test.stopTest();
    }
    @isTest
    static void testbenefactor () {
        final Siniestro__c sinies =[SELECT Id from Siniestro__c WHERE MX_SB_MLT_NombreConductor__c =:  NAME + '_1' ];
        Final String contrId =[Select Id, MX_SB_SAC_Contrato__c from Siniestro__c where MX_SB_MLT_NombreConductor__c =:  NAME + '_2' ].MX_SB_SAC_Contrato__c;
        Final List<MX_SB_VTS_Beneficiario__c> benefs = [SELECT Id,MX_SB_MLT_Q_EnvioKitCorreo__c,MX_SB_MLT_Kit__c,MX_SB_SAC_Email__c From MX_SB_VTS_Beneficiario__c where MX_SB_MLT_Q_EnvioKitCorreo__c ='Si'];
        test.startTest();
        List<MX_SB_VTS_Beneficiario__c> myList= new List<MX_SB_VTS_Beneficiario__c>();
        myList = [Select ID,MX_SB_MLT_Q_EnvioKitCorreo__c,MX_SB_MLT_Kit__c, MX_SB_SAC_Email__c from MX_SB_VTS_Beneficiario__c where Id=:benefs];
        	MX_SB_MLT_ValidacionDocumentos_Ctrl.benefactor(contrId);
        	MX_SB_MLT_ValidacionDocumentos_Ctrl.updtBenef(myList, sinies.Id);
        system.assert(true,'Se ha seleccionado el Kit exitosamente');
        test.stopTest();
    }
}