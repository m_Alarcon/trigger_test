/**
 * @description       : 
 * @author            : Eduardo Hernandez Cuamatzi
 * @group             : 
 * @last modified on  : 10-02-2020
 * @last modified by  : Eduardo Hernandez Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   10-02-2020   Eduardo Hernandez Cuamatzi   Initial Version
**/
@isTest
public class MX_SB_VTS_SendTrackingTelemarketing_Test {
    @TestSetup
    static void makeData() {
        MX_SB_VTS_ScheduldOppsTele_tst.initSendTeleOpps();
    }

    @isTest 
    private static void fillContractServ() {
        final String queyrOppServ = MX_SB_VTS_ScheduldOppsTele_tst.fillQuery();
        final List<Opportunity> lsOpps = DataBase.query(queyrOppServ);
        final Map<String, String> mapOpps = new Map<String, String> ();
        final List<String> responseSer = new List<String>();
        for(Opportunity opp: lsOpps) {
            mapOpps.put(opp.FolioCotizacion__c, opp.Id);
            responseSer.add('ASDQW412DDFASF83');
        }
        Test.startTest();
            final String getQuotetions = MX_SB_VTS_ScheduldOppsTele_tst.quotetationStr('GetQuotation');
        	Final Map<String,String> tsecTst = new Map<String,String> {'tsec'=>'2122113'};
            final MX_WB_Mock mockCallout = new MX_WB_Mock(200, 'Complete', getQuotetions, tsecTst);
            iaso.GBL_Mock.setMock(mockCallout);
            final Map<String,Map<String,Object>> responseObjects = MX_SB_VTS_SendTrackingTelemarketing_Ser.findQuoteStatus(responseSer, mapOpps);
            final Contract fillContractRec = MX_SB_VTS_SendTrackingTelemarketing_Ser.fillContract(responseObjects, lsOpps[0]);
            System.assertEquals(fillContractRec.MX_WB_Oportunidad__c, lsOpps[0].Id, 'Contrato correcto');
		Test.stopTest();
    }
}