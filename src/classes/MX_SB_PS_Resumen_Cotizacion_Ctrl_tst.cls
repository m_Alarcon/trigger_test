/**
* Indra
* @author           Daniel perez lopez
* Project:          Presuscritos
* Description:      Clase test para methods de selector
*
* Changes (Version)
* ------------------------------------------------------------------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    --------------------------------------
* @version  1.0     2020-09-04      Daniel perez lopez.     Creación de la Clase
* @version  1.1     2020-10-08      Gerardo Mendoza Aguilar. Agrego method getContractData
* @version  1.2     12-01-2020   Gerardo Mendoza Aguilar    Agrego method calculateAge
* @version  1.3     03-17-2021   Juan Carlos Benitez         Se corrigen asserts
*/
@isTest
public class MX_SB_PS_Resumen_Cotizacion_Ctrl_tst {
	/*
* variable para url statica
*/
public static final String URLSERVICE='https://150.250.220.36:18500/lifeInsurances'; 
	 /**
    * @Method Data Setup
    * @Description Method para preparar datos de prueba 
    * @return void
    **/
	@testSetup
    public static void  data() { 
    	MX_SB_PS_OpportunityTrigger_test.makeData();
        Test.setMock(HttpCalloutMock.class, new MX_SB_PS_IntegrationServiceMock());
        iaso.GBL_Mock.setMock(new MX_SB_PS_IntegrationServiceMock());
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'GetSendDocumentByEmail',iaso__Cache_Partition__c = 'iaso.ServicesPartition',iaso__Url__c=URLSERVICE);
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'GetInsuranes',iaso__Cache_Partition__c = 'iaso.ServicesPartition',iaso__Url__c=URLSERVICE);
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'GetCustomerDocument_PS',iaso__Cache_Partition__c = 'iaso.ServicesPartition',iaso__Url__c=URLSERVICE);
    }
    
    /**
    * @Method Data Setup
    * @Description Method para buscar cuentas 
    * @return void
    **/
    @isTest
    static void loadOppF() {
        test.startTest();
            final opportunity opt = [Select id,Name from Opportunity where Name='opp1' limit 1];
        	final Opportunity[] lista = new Opportunity[]{};
            lista.add(MX_SB_PS_Resumen_Cotizacion_Ctrl.loadOppF(opt.Id));
        	System.assert(lista.size()>0,'Exito en consulta');
        test.stopTest();
    }
    
     /**
    * @Method Data Setup
    * @Description Method para buscar contactos 
    * @return void
    **/
    @isTest
    static void loadAccountF() {
        test.startTest();
            final opportunity opt = [Select id,Name,AccountId from Opportunity where Name='opp1' limit 1];
        	final Account[] lista =MX_SB_PS_Resumen_Cotizacion_Ctrl.loadAccountF(opt.AccountId);
        	MX_SB_PS_Resumen_Cotizacion_Ctrl.loadContactAcc(opt.AccountId);
        	System.assert(lista.size()>0,'Exito en consulta');
        test.stopTest();
    }
    
    /**
    * @Method Data Setup
    * @Description Method para buscar quotelineitems 
    * @return void
    **/
    @isTest
    static void loadQuoteLineItem() {
        test.startTest();
            Final Quote qot= [Select id,Name from Quote where Name='qtest' limit 1];
        	final QuoteLineItem[] lista =MX_SB_PS_Resumen_Cotizacion_Ctrl.loadQuoteLineItem(qot.Id);
        	System.assert(lista.size()>0,'Exito en consulta');
        test.stopTest();
    }
    
    /**
    * @Method Data Setup
    * @Description Method para buscar contrato asociado a la oportunidad
    * @return void
    **/
    @isTest
    static void getContractData() {
        test.startTest();
        final opportunity opp1test = [Select id,Name from Opportunity where Name='opp1' limit 1];
        final account acctest1= [SELECT id from Account limit 1];
        final contract contracto= new contract(AccountId=acctest1.Id,MX_WB_Oportunidad__c =opp1test.Id);
		insert contracto;
		final Contract[] lista = MX_SB_PS_Resumen_Cotizacion_Ctrl.getContractData(opp1test.Id);
        System.assert(lista.size()>0,'Exito');
        test.stopTest();
    }
    @isTest
    static void testFolioQuote() {
        final Map<String, String> headersMock = new Map<String, String>();
        headersMock.put('tsec', '368897845');
        final MX_WB_Mock mockCalloutTest = new MX_WB_Mock(200, 'Complete', '{"document": {"data": "test","size": 66545,"id": "GMDSEHO001"},"href": "http://150.100.104.43:36026/segurosmedc/RUC_1604859637892_193.pdf"}', headersMock);
        iaso.GBL_Mock.setMock(mockCalloutTest);
        final List<QuoteLineItem> quoli1= [SELECT Id, QuoteId, MX_WB_Folio_Cotizacion__c from QuoteLineItem where MX_WB_Folio_Cotizacion__c='11,11,11'];
		Final List<Opportunity> oppObjList=[Select Id, SyncedQuoteId from Opportunity where SyncedQuoteId=:quoli1[0].QuoteId ];
        test.startTest();
        try {
        	MX_SB_PS_Resumen_Cotizacion_Ctrl.getDocuments(oppObjList[0].Id, 'GMDSEHO001');
        } catch(Exception e) {
                final Boolean expectedError = e.getMessage().contains('Script') ? true : false;
                System.assert(expectedError,'Script-Thrown exception');
        }
        test.stopTest();
    }
    /**
    * @description 
    * @author Arsenio.perez.lopez.contractor@bbva.com | 11-18-2020 
    **/
    @isTest
	static void testCloOpp() {
 		Test.startTest();
            final Opportunity opp = [Select Id, Name from Opportunity where Name ='Opp1'];
            try {           
                MX_SB_PS_Resumen_Cotizacion_Ctrl.getCloneOpp(opp.Id, System.Label.MX_SB_VTS_Hogar);
            } catch(Exception e) {
                System.assert(e.getMessage().contains('List'),'No se han encontrado informacion');
            }
        Test.stopTest();
    }
    /**
    * @description 
    * @author Gerardo Mendoza Aguilar | 12-01-2020 
    **/
    @isTest
    static void calculateAge() {
        Test.startTest();
        final Integer age = MX_SB_PS_Resumen_Cotizacion_Helper.calculateAge(date.valueOf('1989-01-01'));
        system.assertNotEquals(age, 0, 'Edad calculada');
        Test.stopTest();
    }
    @isTest
    static void testemiteQuoteNoCobro() {
    final Map<String, String> headersMock = new Map<String, String>();
        headersMock.put('tsec', '368897845');
	Final String findMock=[Select Id, iaso__Mock_LTA__c from iaso__GBL_integration_service__mdt where DeveloperName ='GetContratacionSinCobro_PS'].iaso__Mock_LTA__c;
        final MX_WB_Mock mockCalloutTest = new MX_WB_Mock(200, 'Complete', findMock, headersMock);        
        iaso.GBL_Mock.setMock(mockCalloutTest);
        Final List<QuotelineItem> quoli=[Select Id ,QuoteId,MX_WB_Folio_Cotizacion__c,Quote.OpportunityId from QuotelineItem where MX_WB_Folio_Cotizacion__c='11,11,11'];
        Final List<Quote> quot =[Select Id,OpportunityId from Quote where Id=:quoli[0].QuoteId];
        test.startTest();
	final List<QuoteLineItem> quoli2=[SELECT id ,createddate,UnitPrice,MX_SB_VTS_Tipo_Plan__c,MX_SB_PS_Dias3a5__c,MX_SB_PS_Dias6a35__c,MX_SB_PS_Dias36omas__c,MX_SB_VTS_Total_Gastos_Funerarios__c,MX_SB_VTS_FormaPAgo__c,MX_WB_noPoliza__c ,MX_WB_Folio_Cotizacion__c  FROM QuoteLineItem where Quote.OpportunityId=:quot[0].OpportunityId ];
        quoli2[0].MX_WB_Folio_Cotizacion__c='11,11,11';
        update quoli2[0];
        try {
             MX_SB_PS_Resumen_Cotizacion_Ctrl.emiteQuoteNoCobro(quot[0].OpportunityId);
        } catch(Exception e) {
             System.assert(e.getMessage().contains('TestMethod'),'Entra a Assertion Script');
        }
        test.stopTest();
        
    }
    @isTest
    static void testwrpDocuments() {
        test.startTest();
        	Final MX_SB_PS_Resumen_Cotizacion_Ctrl.wrpDocuments wrap1= new MX_SB_PS_Resumen_Cotizacion_Ctrl.wrpDocuments();
        	Final MX_SB_PS_Resumen_Cotizacion_Ctrl.wrpinfo wrap2= new MX_SB_PS_Resumen_Cotizacion_Ctrl.wrpinfo();
        	Final MX_SB_PS_Resumen_Cotizacion_Ctrl.Dtresp wrap3 = new MX_SB_PS_Resumen_Cotizacion_Ctrl.Dtresp();
        	Final MX_SB_PS_Resumen_Cotizacion_Ctrl.datas wrap4  = new MX_SB_PS_Resumen_Cotizacion_Ctrl.datas();
        	wrap4.policyNumber='020403';
        	wrap3.data=wrap4;
        	wrap2.data = '';
        	wrap2.href = '';
        	wrap1.document= wrap2;
        	wrap1.href='';
        	system.assertEquals(wrap4.policyNumber,'020403','Constructor');
        test.stopTest();
    }
}