/**
 * @File Name          : MX_SB_VTS_SendFacebookLeads_tst.cls
 * @Description        : 
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 08-13-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    20/4/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
@isTest
private class MX_SB_VTS_SendFacebookLeads_tst {
    /**Response service */
    final static String BODYASD = '<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://201.148.35.186/ws/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:ns2="http://xml.apache.org/xml-soap" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><SOAP-ENV:Body><ns1:getCallResponse><return><item><key xsi:type="xsd:string">status</key><value xsi:type="xsd:string">ERROR</value></item><item><key xsi:type="xsd:string">calls</key><value SOAP-ENC:arrayType="ns2:Map[2]" xsi:type="SOAP-ENC:Array"><item xsi:type="ns2:Map"><item><key xsi:type="xsd:string">Fecha_Llamada</key><value xsi:type="xsd:string">2019-02-07 16:37:41</value></item><item><key xsi:type="xsd:string">Tel_Marcado</key><value xsi:type="xsd:string">0458119135196</value></item><item><key xsi:type="xsd:string">Disposicion</key><value xsi:nil="true"/></item><item><key xsi:type="xsd:string">idUser</key><value xsi:type="xsd:string">VDAD</value></item><item><key xsi:type="xsd:string">leadId</key><value xsi:type="xsd:string">00QR000000G7gW3MAJ</value></item><item><key xsi:type="xsd:string">Tel_1</key><value xsi:type="xsd:string">0458119135196</value></item><item><key xsi:type="xsd:string">Tel_2</key><value xsi:type="xsd:string"></value></item><item><key xsi:type="xsd:string">Tel_3</key><value xsi:type="xsd:string"></value></item></item><item xsi:type="ns2:Map"><item><key xsi:type="xsd:string">Fecha_Llamada</key><value xsi:type="xsd:string">2019-02-07 16:37:17</value></item><item><key xsi:type="xsd:string">Tel_Marcado</key><value xsi:type="xsd:string">0458119135196</value></item><item><key xsi:type="xsd:string">Disposicion</key><value xsi:type="xsd:string">EQUIVOCADO INCORRECTO</value></item><item><key xsi:type="xsd:string">idUser</key><value xsi:type="xsd:string">PECC891119</value></item><item><key xsi:type="xsd:string">leadId</key><value xsi:type="xsd:string">00QR000000G7gW3MAJ</value></item><item><key xsi:type="xsd:string">Tel_1</key><value xsi:type="xsd:string">0458119135196</value></item><item><key xsi:type="xsd:string">Tel_2</key><value xsi:type="xsd:string"></value></item><item><key xsi:type="xsd:string">Tel_3</key><value xsi:type="xsd:string"></value></item></item></value></item><item><key xsi:type="xsd:string">recordings</key><value SOAP-ENC:arrayType="ns2:Map[1]" xsi:type="SOAP-ENC:Array"><item xsi:type="ns2:Map"><item><key xsi:type="xsd:string">Fecha_Grabacion</key><value xsi:type="xsd:string">2019-02-07 16:37:20</value></item><item><key xsi:type="xsd:string">Grabacion</key><value xsi:type="xsd:string">ASD01013_20190207-163718_0458119135196_PECC891119</value></item><item><key xsi:type="xsd:string">Duracion</key><value xsi:type="xsd:string">3</value></item><item><key xsi:type="xsd:string">idUser</key><value xsi:type="xsd:string">PECC891119</value></item><item><key xsi:type="xsd:string">leadId</key><value xsi:type="xsd:string">00QR000000G7gW3MAJ</value></item></item></value></item></return></ns1:getCallResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>';
    /**Texto Facebook */
    final static String FACEBOOK = 'Facebook';
    /**Final batch context */
    final static Database.BatchableContext BATCHCONTEXT;

    @TestSetup
    static void makeData() {

        final MX_SB_VTS_Generica__c setting = MX_SB_VTS_CallCTIs_utility.genricaSmart();
        insert setting;
        final MX_WB_FamiliaProducto__c objFamilyPro2 = MX_SB_VTS_CallCTIs_utility.newFamiliy('Hogar');
        insert objFamilyPro2;
        final Product2 proHogar = MX_SB_VTS_CallCTIs_utility.newProduct(System.Label.MX_SB_VTS_Hogar, objFamilyPro2);
        
        final MX_SB_VTS_ProveedoresCTI__c smartProv = MX_SB_VTS_CallCTIs_utility.newProvee('Smart Center', 'Smart Center');
        insert  smartProv;

        final MX_SB_VTS_FamliaProveedores__c famProSmart = MX_SB_VTS_CallCTIs_utility.newFamProveedor(objFamilyPro2, 'SmartHogar', smartProv);
        insert famProSmart;
        final String[] valuesTray = new String[]{'HotLeads', 'BandejHot', '72', 'HotLeads'};
        final MX_SB_VTS_Lead_tray__c hotLeadsSmart =  MX_SB_VTS_CallCTIs_utility.newTrayHotlead(valuesTray, proHogar, smartProv);
        insert hotLeadsSmart;

        MX_SB_VTS_CallCTIs_utility.insertIasosVoid();

        final ProcNotiOppNoAten__c objProNot = MX_SB_VTS_CallCTIs_utility.newProcNoct('0', 'Matutino', FACEBOOK, false);
        objProNot.MX_SB_VTS_ScheduledTime__c = '15';
        insert objProNot;
        
        final Lead callmeAuto = new Lead();
        callmeAuto.Producto_Interes__c = 'Auto Seguro Dinamico';
        callmeAuto.LeadSource = FACEBOOK;
        callmeAuto.FirstName = 'Prueba';
        callmeAuto.LastName = FACEBOOK;
        callmeAuto.TelefonoCelular__c = '5524815910';
        insert callmeAuto;

        final Lead callmeHS = new Lead();
        callmeHS.Producto_Interes__c = 'Hogar';
        callmeHS.LeadSource = FACEBOOK;
        callmeHS.FirstName = 'Prueba';
        callmeHS.LastName = FACEBOOK;
        callmeHS.TelefonoCelular__c = '5524812910';
        insert callmeHS;
    }

    @isTest
    private static void sendFacebookLeadsHS() {
        Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_Integration_MockGenerator());
        iaso.GBL_Mock.setMock(new MX_SB_VTS_Integration_MockGenerator());
        String sQueryHS = 'Select Id, LeadSource, MobilePhone, Producto_Interes__c, FirstName, LastName, Apellido_Materno__c, MX_SB_VTS_HoraCallMe__c, MX_SB_VTS_FechaHoraAgenda__c, EnviarCTI__c from Lead';
        sQueryHS += ' where LeadSource = \'Facebook\' AND EnviarCTI__c = false AND CreatedDate >= YESTERDAY AND Producto_Interes__c = \'Hogar\'';
        final MX_SB_VTS_SendFacebookLeads_cls schFace = new MX_SB_VTS_SendFacebookLeads_cls(sQueryHS);
        final List<Lead> scopeLeads = Database.query(sQueryHS);
        Test.startTest();
            schFace.execute(BATCHCONTEXT, scopeLeads);
        Test.stopTest();
        System.assertEquals(scopeLeads.size(), 1, 'Execute correcto');
    }

    @isTest
    private static void sendFacebookLeadsASD() {
        final MX_WB_Mock mock = new MX_WB_Mock(200,'OK', BODYASD,new Map<String,String>());
        Test.setMock(HttpCalloutMock.class, mock);
        String sQueryASD = 'Select Id, LeadSource, MobilePhone, Producto_Interes__c, FirstName, LastName, Apellido_Materno__c, MX_SB_VTS_HoraCallMe__c, MX_SB_VTS_FechaHoraAgenda__c, EnviarCTI__c from Lead';
        sQueryASD += ' where LeadSource = \'Facebook\' AND EnviarCTI__c = false AND CreatedDate >= YESTERDAY AND Producto_Interes__c != \'Hogar\'';
        final MX_SB_VTS_SendFacebookLeads_cls schFace = new MX_SB_VTS_SendFacebookLeads_cls(sQueryASD);
        final List<Lead> scopeLeads = Database.query(sQueryASD);
        Test.startTest();
            schFace.execute(BATCHCONTEXT, scopeLeads);
        Test.stopTest();
        System.assertEquals(scopeLeads.size(), 1, 'Execute correcto');
    }

    @isTest
    private static void activeProcessSch() {
        final ProcNotiOppNoAten__c processFace = [Select Id, Activo__c,Proceso__c from ProcNotiOppNoAten__c where Proceso__c =: FACEBOOK];
        final Lead leadTest = [Select Id, EnviarCTI__c FROM Lead where TelefonoCelular__c = '5524815910'];
        leadTest.EnviarCTI__c = true;
        update leadTest;
        Test.startTest();
            processFace.Activo__c = true;
            processFace.MX_SB_VTS_ScheduledTime__c = '43';
            processFace.Horario__c = 'A los';
            processFace.Minutos__c = '55';
            update processFace;
        Test.stopTest();
        System.assertEquals(processFace.Proceso__c,FACEBOOK,'Proceso programado');
    }
}