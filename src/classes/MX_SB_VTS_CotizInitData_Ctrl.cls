/**
 * @File Name          : MX_SB_VTS_CotizInitData_Ctrl.cls
 * @Description        : 
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : BBVA Seguros
 * @Last Modified By   : Diego Olvera
 * @Last Modified On   : 09-24-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    21/5/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_CotizInitData_Ctrl {
   
    /**@description llave de mapa error*/
    private final static string HASERROR = 'hasError';

    /**
    * @description 
    * @author Eduardo Hernandez Cuamatzi | 4/5/2020 
    * @param oppId Id de la Oportunidad cotizada
    * @return Map<String, Object> datos iniciales de carga
    **/
    @AuraEnabled
    public static Map<String, Object> initDataCotiz(String oppId) {
        final Map<String, Object> response = new Map<String, Object>();
        final Set<String> setOppsId = new Set<String>();
        setOppsId.add(oppId);
        final List<Opportunity> oppRecords = MX_SB_VTS_CotizInitData_Service.findOppsById(setOppsId);
        final Map<Id, Quote> lstQuotes = MX_SB_VTS_CotizInitData_Service.findQuotesByOppMap(oppRecords);
        final Map<Id, QuoteLineItem> lstQuoteLi = MX_SB_VTS_CotizInitData_Service.findQuolisByQuotMap(lstQuotes);
        response.put('oppRecord', oppRecords[0]);
        response.put('lstQuotes', lstQuotes);
        response.put('lstQuoteLi', lstQuoteLi);
        response.put(HASERROR, false);
        return response;
    }

    /**
    * @description Recupera datos de una cotización especifica
    * @author Eduardo Hernandez Cuamatzi | 4/5/2020 
    * @param quoteId Id de la cotización a consultar
    * @return Map<String, Object> datos de la cotización, partida de cotización, Coberturas
    **/
    @AuraEnabled
    public static Map<String, Object> findDataClient(String quoteId) {
        final Map<String, Object> response = new Map<String, Object>();
        final List<Quote> queteDat = MX_SB_VTS_CotizInitData_Service.findQuotesById(quoteId);
        if(queteDat.isEmpty() == false) {
            final Set<String> oppId = new Set<String>{String.valueOf(queteDat[0].OpportunityId)};
            final List<Opportunity> oppRecords = MX_SB_VTS_CotizInitData_Service.findOppsById(oppId);
            final Set<Id> setQuotesId = new Set<Id>{queteDat[0].Id};
            final List<QuoteLineItem> quoteLi = MX_SB_VTS_CotizInitData_Service.findQuolisByQuotMap(setQuotesId);
            response.put('oppRecord', oppRecords[0]);
            response.put('queteDat', queteDat[0]);
            response.put('lstQuoteLi', quoteLi[0]);
        } else {
            response.put(HASERROR, true);
        }
        return response;
    }

     /**
    * @description recupera datos de la cotización a enviar
    * @author Eduardo Hernandez Cuamatzi | 16/4/2020 
    * @return Map<String, Object> 
    **/
    @AuraEnabled
    public static Map<String, Object> findQuoteEmail(String quoteId) {
        final Map<String, Object> repsonse = new Map<String, Object>();
        final List<Quote> quoteCot = MX_SB_VTS_CotizInitData_Service.findQuotesById(quoteId);
        repsonse.put('dataQuote', quoteCot);
        return repsonse;
    }

    /**
    * @description Envio de cotización por webservice
    * @author Eduardo Hernandez Cuamatzi | 4/5/2020 
    * @param quoteId 
    * @param name 
    * @param email 
    * @return Map<String, Object> 
    **/
    @AuraEnabled
    public static Map<String, Object> sendingEmail(String quoteId, String name, String email) {
        final Map<String, Object> repsonse = new Map<String, Object>();
        repsonse.put('response', true);
        return repsonse;
    }

    /**
    * @description Valida número de poliza
    * @author Eduardo Hernández Cuamatzi | 08-12-2020 
    * @param polizNum Número de poliza a validar
    * @return Map<String, Object> Respuesta de validación
    **/
    @AuraEnabled
    public static Map<String, Object> validatePoliz(String polizNum, String oppId) {
        final Map<String, Object> mapServe = MX_SB_VTS_GetSetInsContract_Service.obtQueryInsuranceContract(polizNum);
        Map<String, Object> returnValues = null;
        if((Boolean)mapServe.get('isExist')) {
            returnValues = MX_SB_VTS_GetSetInsContract_Service.obtQueryValPolizContractOppo(mapServe, oppId);
        } else {
            returnValues = MX_SB_VTS_GetSetInsContract_Service.obtQueryValPolizContractOppo(mapServe, oppId);
        }
        return returnValues;
    }

    /**
    * @description Actualiza valor de Oportunidad desde el front con valor de Propio o rentado
    * dependiendo de lo que el usuario seleccione.
    * @author Diego Olvera | 17-09-2020 
    * @param polizNum recId, strValPropio, strValRentado
    * @return List<Opportunity> Respuesta de validación
    **/
    @AuraEnabled
    public static List<Opportunity> updCurrRec(String recId, String strValPropio, String strValRentado) {
        return MX_SB_VTS_CotizInitData_Service.updCurrRecService(recId, strValPropio, strValRentado);
    }
    /**
    * @description Actualiza valor de Oportunidad desde el front con valor de Propio o rentado
    * dependiendo de lo que el usuario seleccione.
    * @author Diego Olvera | 17-09-2020 
    * @param  reId
    * @return List<Opportunity> Respuesta de validación
    **/
    @AuraEnabled
    public static List<Opportunity> getOppVal(String reId) {
        final Set<String> setOppsVal = new Set<String>();
        setOppsVal.add(reId);
        return MX_SB_VTS_CotizInitData_Service.findOppsById(setOppsVal);
    }

    /**
    * @description Actualiza datos de frecuencia de pago
    * @author Eduardo Hernández Cuamatzi | 02-11-2021 
    * @param oppId Oportunidad trabajada
    * @param lstPayments Lista de pagos posibles
    * @return Map<String, Object> respuesta de la operación
    **/
    @AuraEnabled
    public static Map<String, Object> updateFrecuencie(String oppId, List<String> lstPayments) {
        final Map<String, Object> responseFrec = new Map<String, Object>();
        try {
            final Set<String> setOppsId = new Set<String>();
            setOppsId.add(oppId);
            final List<Opportunity> oppRecs = MX_SB_VTS_CotizInitData_Service.findOppsById(setOppsId);
            final Map<Id, Quote> mapQuotes = MX_SB_VTS_CotizInitData_Service.findQuotesByOppMap(oppRecs);
            final Map<Id, QuoteLineItem> lstQuoteLi = MX_SB_VTS_CotizInitData_Service.findQuolisByQuotMap(mapQuotes);
            final List<QuoteLineItem> lstQuoli = new List<QuoteLineItem>();
            for(Id quoliItem : lstQuoteLi.keySet()) {
                lstQuoteLi.get(quoliItem).MX_SB_VTS_Frecuencia_de_Pago__c = lstPayments[0];
                lstQuoteLi.get(quoliItem).MX_SB_VTS_Monto_Mensualidad__c = lstPayments[1];
                lstQuoteLi.get(quoliItem).UnitPrice = Decimal.valueOf(lstPayments[2]);
                lstQuoli.add(lstQuoteLi.get(quoliItem));
            }
            MX_RTL_QuoteLineItems_Selector.insertQuotesLine(lstQuoli);
            responseFrec.put('isOk', true);
        } catch (QueryException quEx) {
            responseFrec.put('isOk', false);
            responseFrec.put('msjError', quEx.getMessage());
        }
        return responseFrec;
    }
}