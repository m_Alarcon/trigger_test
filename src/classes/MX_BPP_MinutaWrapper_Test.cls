/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_MinutaWrapper_Test
* @Author   	Héctor Israel Saldaña Pérez | hectorisrael.saldana.contractor@bbva.com
* @Date     	Created: 2021-02-05
* @Description 	Test Class for Wrapper class MX_BPP_MinutaWrapper.cls
* @Changes
* 2021-22-02		Héctor Saldaña	  	New methods added: constructorWrapperComponents()
 * 2021-09-03   	Héctor Saldaña      Added coverage for tagEstaticos Inner Class within MinutaWrapper
*/
@isTest
private class MX_BPP_MinutaWrapper_Test {

	/**
	* @Description 	Test Method for Minuta Wrapper constructor
	* @Return 		NA
	**/
	@isTest
	static void testConsctructor() {
		Test.startTest();
		final PageReference testPage = Page.MX_BPP_Minuta_BienvenidaDO_VF;
		Test.setCurrentPage(testPage);
		ApexPages.currentPage().getParameters().put('Id', 'testId');
		ApexPages.currentPage().getParameters().put('myCS', '{testKey:testValue}' );
		ApexPages.currentPage().getParameters().put('documents', '[{testKeyDoc:testKeyValue}]');
		final MX_BPP_MinutaWrapper testWrapper = new MX_BPP_MinutaWrapper();
		System.assertEquals(testWrapper.configParamsPty.currentId, 'testId', 'Verificar que el Id coincida');
		test.stopTest();
	}

	/**
	* @Description 	Test Method for Miunuta Wrapper - Wrapper Components inner class
	* @Return 		NA
	**/
	@IsTest
	static void constructorWrapperComponents() {
		Test.startTest();
		final MX_BPP_MinutaWrapper.wrapperComponentes wrapperTest = new MX_BPP_MinutaWrapper.wrapperComponentes();
		final List<MX_BPP_MinutaWrapper.componenteText> lstTxtCmp = new List<MX_BPP_MinutaWrapper.componenteText>();
		final MX_BPP_MinutaWrapper.componenteText txtCmp = new MX_BPP_MinutaWrapper.componenteText();
		txtCmp.tagInicio = 'tag txt';
		txtCmp.label = 'label txt';
		txtCmp.name = 'name txt';
		txtCmp.cmpRequired = false;
		lstTxtCmp.add(txtCmp);

		final List<MX_BPP_MinutaWrapper.componenteSwitch> lstSwitchCmp = new List<MX_BPP_MinutaWrapper.componenteSwitch>();
		final MX_BPP_MinutaWrapper.componenteSwitch switchCmp = new MX_BPP_MinutaWrapper.componenteSwitch();
		switchCmp.name = 'test switch';
		switchCmp.label = 'switch label';
		switchCmp.tagInicio = 'tag switch';
		switchCmp.cmpRequired = true;
		lstSwitchCmp.add(switchCmp);

		final List<MX_BPP_MinutaWrapper.componentePicklistValues> lstValues = new List<MX_BPP_MinutaWrapper.componentePicklistValues>();
		final MX_BPP_MinutaWrapper.componentePicklistValues opc1 = new MX_BPP_MinutaWrapper.componentePicklistValues();
		opc1.label = 'opcion1';
		opc1.value = 'opcion1';
		lstValues.add(opc1);

		final List<MX_BPP_MinutaWrapper.componentePicklist> lstPicklist = new List<MX_BPP_MinutaWrapper.componentePicklist>();
		final MX_BPP_MinutaWrapper.componentePicklist pickList = new MX_BPP_MinutaWrapper.componentePicklist();
		pickList.options = lstValues;
		pickList.label = 'picklist label';
		pickList.tagInicio = 'picklist tag';
		pickList.name = 'picklist name';
		pickList.cmpRequired = true;
		lstPicklist.add(pickList);

		wrapperTest.cmpText = lstTxtCmp;
		wrapperTest.cmpPicklist = lstPicklist;
		wrapperTest.cmpSwitch = lstSwitchCmp;

		final MX_BPP_MinutaWrapper.tagsEstaticos cmpStatic = new MX_BPP_MinutaWrapper.tagsEstaticos();
		cmpStatic.tagInicio = '{NombreBanquero}';
		cmpStatic.campo = 'Name';
		cmpStatic.objeto = 'Owner';
		cmpStatic.contenido = 'Dagoberto';
		cmpStatic.relacionado = true;
		cmpStatic.isDO = false;
        System.assert(wrapperTest.cmpText[0].tagInicio == 'tag txt' && wrapperTest.cmpSwitch[0].label == 'switch label' && wrapperTest.cmpPicklist[0].options.size() == 1, 'Verificar información');
		Test.stopTest();
	}
}