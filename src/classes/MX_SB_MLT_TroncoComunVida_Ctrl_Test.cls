@isTest
/**
 * @File Name          : MX_SB_MLT_TroncoComunVida_Ctrl_Test.cls
 * @Description        : Clase Test que prueba los methods de MX_SB_MLT_TroncoComunVida_Ctrl
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 06/06/2020, 02:47:22 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/27/2020    Juan Carlos Benitez         Initial Version
**/
public class MX_SB_MLT_TroncoComunVida_Ctrl_Test {
    /** var recordType developerName **/
    Final static string DEVNAME='MX_SB_MLT_RamoAuto';
	/**var nombre conductor **/
    Final static string NAME= 'Rebeca';
	/** current user id */
    final static String USR_ID = UserInfo.getUserId();
    /** var subramo */
    final static String SUBRAMO ='Fallecimiento';
	/** var  */
    final static String TASKTYPE ='PureCloud';
    /**
     * @description
     * @author Juan Carlos Benitez | 5/26/2020
     * @return void
     **/
        @TestSetup
    static void createTstData() {
        Final String profileAM = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User userAVida = MX_WB_TestData_cls.crearUsuario(NAME, profileAM);  
        insert userAVida;
        System.runAs(userAVida) {
            final String rcordTypId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Proveedores_MA).getRecordTypeId();
            final Account accnVidaTst = new Account(RecordTypeId = rcordTypId, Name=NAME, Tipo_Persona__c='Física');
            insert accnVidaTst;
            final Contract cntractVida = new Contract(AccountId = accnVidaTst.Id, MX_SB_SAC_NumeroPoliza__c='policyTest');
            insert cntractVida;
            final Task taskPureCloud = new Task();
            	taskPureCloud.Telefono__c ='5540342107';
            	taskPureCloud.Subject='Llamada PureCloud';
            	taskPureCloud.RecordTypeId=Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_PureCloud').getRecordTypeId();
            insert taskPureCloud;
            Final List<Siniestro__c> sinInsert = new List<Siniestro__c>();
            for(Integer j = 0; j < 10; j++) {
                Final Siniestro__c nSin = new Siniestro__c();
                nSin.MX_SB_MLT_NombreConductor__c = NAME + '_' + j;
                nSin.MX_SB_MLT_APaternoConductor__c = 'LastName for '+NAME;
                nSin.MX_SB_MLT_AMaternoConductor__c = 'LastName2 for '+NAME;
                nSin.MX_SB_MLT_Fecha_Hora_Siniestro__c =date.valueof('2020-03-27T22:04:00.000+0000');
                nSin.MX_SB_SAC_Contrato__c = cntractVida.Id;
                nSin.MX_SB_MLT_Telefono__c = '5535849302';
                nSin.TipoSiniestro__c = 'Siniestros';
                nSin.MX_SB_MLT_AtencionVida__c = 'Agendar Cita';
                nSin.MX_SB_MLT_SubRamo__c = SUBRAMO;
                nSin.MX_SB_MLT_PreguntaVida__c = false;
                nSin.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_creacion;
                nSin.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoVida).getRecordTypeId();
                sinInsert.add(nSin);
            }
            Database.insert(sinInsert);
        }
    }
    @isTest
    	static void testRecordtype () {
        	test.startTest();
        		MX_SB_MLT_TroncoComunVida_Ctrl.getRecordType(DEVNAME);
            	system.assert(true,'Se ha encontrado el tipo de registro');
        	test.stopTest();
    	}
    @isTest
    	static void testupsertSini () {
            final Siniestro__c sinIn = [SELECT Id from Siniestro__c where MX_SB_MLT_NombreConductor__c =: NAME+'_4' ];
            sinIn.MX_SB_MLT_ComentariosSiniestro__c='No cuenta con copia de poliza';
        	test.startTest();
            	MX_SB_MLT_TroncoComunVida_Ctrl.upSertsini(sinIn);
            	system.assert(true,'Se ha actualizado el siniestro');
            test.stopTest();
    	}
        @isTest
    static void testTaskWhatId () {
        final Task taskPC =[SELECT Id from Task where recordType.DeveloperName='MX_SB_MLT_PureCloud'];
        test.startTest();
        	MX_SB_MLT_TroncoComunVida_Ctrl.whatIdSin(taskPC, TASKTYPE);
		system.assert(true,'Se ha relacionado correctamente el siniestro y la tarea');
        test.stopTest();
    }
    @isTest
    static void testcheckPolicy () {
        Final String policy = [SELECT MX_SB_SAC_NumeroPoliza__c FROM Contract where MX_SB_SAC_NumeroPoliza__c='policyTest'].MX_SB_SAC_NumeroPoliza__c;
		test.startTest();
        	MX_SB_MLT_TroncoComunVida_Ctrl.checkPolicy(policy);
        system.assert(true,'Se ha encontrado la poliza');
        test.stopTest();
    }
}