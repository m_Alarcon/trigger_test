@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_PS_InformacionCuenta_Ctrl_Test
* Autor: Juan Carlos Benitez Herrera
* Proyecto: SB Presuscritos - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_PS_InformacionCuenta_Ctrl

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         09/07/2020    Juan Carlos Benitez Herrera              Creación
* -----------------------------------------------------------------------------------------------
*/
public class MX_SB_PS_InformacionCuenta_Ctrl_Test {
    /** Nombre de usuario Test */
    final static String NAME = 'Mariana';
    /** Apellido Paterno */
    Final static STRING LNAME= 'Esteves';
    
    @TestSetup
    static void createData() {
        	MX_SB_PS_OpportunityTrigger_test.makeData();
            final Account accTstC = [Select id,PersonContactId from Account limit 1];
            accTstC.FirstName=NAME;
        	accTstC.LastName=LNAME;
        	accTstC.Tipo_Persona__c='Física';
            update accTstC;
            Final Opportunity oppTest = [Select Id from Opportunity where AccountId=:accTstC.Id limit 1];
        	oppTest.StageName=System.Label.MX_SB_PS_Etapa1;
        	oppTest.Name= NAME;
            update oppTest;
            Final PersonLifeEvent personlife = new PersonLifeEvent(Name=Name+LNAME, EventType='Job', EventDate=date.today(), PrimaryPersonId=accTstC.PersonContactId);
            insert personlife;
    }
/*
* @description method que prueba LoadAccountF
* @param  void
* @return void
*/
    @isTest
    static void testLoadAccountF() {
        Final ID ide= [SELECT Id from Account where Name=: NAME+' '+LNAME limit 1].Id;
        test.startTest();
        	MX_SB_PS_InformacionCuenta_Ctrl.LoadAccountF(ide);
            system.assert(true,'Cuenta encontrada existosamente');
        test.stopTest();
    }
    /*
* @description method que prueba LoadOppF
* @param  void
* @return void
*/
    @isTest
    static void testLoadOppF() {        
        Final ID idOpp =[SELECT Id from Opportunity limit 1].Id;
        test.startTest();
        	MX_SB_PS_InformacionCuenta_Ctrl.LoadOppF(idOpp);
            system.assert(true,'Oportunidad encontrada existosamente');
        test.stopTest();
    }
    
    /*
* @description method que prueba LoadOppF
* @param  void
* @return void
*/
    @isTest
    static void testloadPersLiEvent() {
        Final ID conId= [SELECT Id from Contact where Name=: NAME+' '+LNAME limit 1].Id;
        test.startTest();
        	MX_SB_PS_InformacionCuenta_Ctrl.loadPersLiEvent(conId);
            system.assert(true,'Personal life encontrado');
        test.stopTest();
    }    
}