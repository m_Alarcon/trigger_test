/**
 * @File Name          : MX_SB_1500_GetExecutiveData_Ctlr.cls
 * @Description        :
 * @Author             : Jaime Terrats
 * @Group              :
 * @Last Modified By   : Jaime Terrats
 * @Last Modified On   : 5/26/2020, 1:08:03 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/25/2020   Jaime Terrats     Initial Version
**/
public without sharing class MX_SB_1500_GetExecutiveData_Ctlr { // NOSONAR
    /** case fields for query */
    final static String CASE_FIELDS = 'MX_SB_1500_CR__c, MX_SB_1500_Email__c, MX_SB_1500_ExecutiveGivenName__c, MX_SB_1500_ExecutiveSureName__c, MX_SB_1500_M__c, MX_SB_1500_PhoneExt__c, MX_SB_1500_Phone__c';
    /** list of records to be retrieved */
    final static List<Case> CASE_DATA = new List<Case>();

    /**
    * @description
    * @author Jaime Terrats | 5/25/2020
    * @param recordId
    * @return List<Case>
    **/
    @AuraEnabled(cacheable=true)
    public static List<Case> getExecutiveData(Id recordId) {
        try {
            if(String.isNotBlank(recordId)) {
                final Set<Id> ids = new Set<Id>{recordId};
                CASE_DATA.addAll(MX_RTL_Case_Service.getCaseData(CASE_FIELDS, ids, 'false'));
            }
        } catch(QueryException qEx) {
            throw new AuraHandledException(System.Label.MX_WB_LG_ErrorBack + qEx);
        }
        return CASE_DATA;
    }
}