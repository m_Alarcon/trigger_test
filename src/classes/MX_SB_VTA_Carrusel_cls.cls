/*
  @File Name          : MX_SB_VTA_Carrusel_cls.cls
  @Description        : 
  @Author             : Arsenio.perez.lopez.contractor@BBVA.com
  @Group              : 
  @Last Modified By   : Arsenio.perez.lopez.contractor@BBVA.com
  @Last Modified On   : 22/1/2020 10:40:49
  @Modification Log   : 
  Ver               Date                Author      		                    Modification
  1.0           23/1/2020       Arsenio.perez.lopez.contractor@BBVA.com     Initial Version
  1.1           26/6/2020       jesusalexandro.corzo.contractor@bbva.com    Adecuaciones a la clase
  1.2           1/7/2020        jesusalexandro.corzo.contractor@bbva.com    Se realizan ajustes a la clase
*/
public without sharing class MX_SB_VTA_Carrusel_cls { //NOSONAR
    /*
    * @Method: getconsultallamada
    * @Description: creates task depending on the business flow
    * @Params: myTask: Task Object, segmented: Boolean, leadId
    */
    @AuraEnabled(cacheable=true)
    public static List<Scripts_Stage_Product__c> getconsultallamada(String subEtapa, String stage, Boolean staticUrl, String producto) {
      final List<Scripts_Stage_Product__c> listLocal = new List<Scripts_Stage_Product__c>();
          for(Scripts_Stage_Product__c temp: getValuesList(subEtapa,stage,producto)) {
            if(staticUrl) {
                String myUrl = PageReference.forResource('MX_SB_VTA_IMG_Carrusel').getUrl();
                if(myUrl.indexOf('?')>0) {
                  myUrl = myUrl.subString(0, myUrl.indexOf('?'));
                }
                temp.MX_SB_VTS_SRC__c=myUrl+'/'+temp.MX_SB_VTS_SRC__c;
              }
            listLocal.add(temp);
          }
      return listLocal;
    }
    
    /*
     * @Method: getValuesList
     * @Description: Recupera la URL y el Script conforme a la etapa, subetapa y
     *               producto que tiene almacedados la oportunidad.
     * @Params: String subEtapa, String stage, String producto
     */
    private static List<Scripts_Stage_Product__c> getValuesList(String subEtapa, String stage, String producto) {
      return [Select id,MX_SB_VTS_SRC__c,MX_SB_VTS_HEADER__c,MX_SB_VTS_HREF__c,MX_WB_Script__c from Scripts_Stage_Product__c where MX_WB_Etapa__c =: stage and MX_SB_VTS_Status__c =: subEtapa and MX_SB_VTS_Product__c =: producto and MX_SB_VTS_Type__c = 'CP7'];
    }
}