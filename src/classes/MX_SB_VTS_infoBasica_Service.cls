/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 02-10-2021
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   02-10-2021   Eduardo Hernández Cuamatzi   Initial Version
 * 1.1   09-03-2021   Eduardo Hernández Cuamatzi   Se agrega limpieza de quotes
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_infoBasica_Service {

    /**
    * @description actualiza registro Oportunidad
    * @author Eduardo Hernández Cuamatzi | 02-11-2021 
    * @param oppData Oportunidad ah actualizar
    **/
    public static void upsertOppData(Opportunity oppData) {
        MX_RTL_Opportunity_Selector.upsrtOpp(oppData);
    }

    /**
    * @description Actualiza una cotizacion con el cupon
    * @author Eduardo Hernández Cuamatzi | 12-10-2020 
    * @param lstQuotes Lista de Quotes ah actualizar
    * @param cupon Cupon a guardar
    * @return Boolean Resultado de operacion DML
    **/
    public static Boolean updateQuoteCupon(Map<Id, Quote> lstQuotes, String cupon) {
        Boolean updateCupon = true;
        final List<Quote> lstQuoteUpdate = new List<Quote>();
        for (Quote quoteItem : lstQuotes.values()) {
            if (String.isBlank(cupon) || String.isEmpty(cupon)) {
                quoteItem.MX_SB_VTS_Cupon__c = '';
            } else {
                quoteItem.MX_SB_VTS_Cupon__c = cupon;
            }
            lstQuoteUpdate.add(quoteItem);
        }
        final Database.UpsertResult[] quoteResults = MX_RTL_Quote_Selector.upsrtQuote(lstQuoteUpdate, false);
        for (Database.UpsertResult quoteResult : quoteResults) {
            if (quoteResult.isSuccess() == false) {
                updateCupon = false;
            }
        }
        return updateCupon;
    }

    /**
    * @description Resetea Cotizaciones 
    * @author Eduardo Hernández Cuamatzi | 03-03-2021 
    * @param strOppoId Id de la oportunidad de la que se busca limpiar las quotes
    **/
    public static Boolean resetQuotes(String strOppoId) {
        final List<Quote> lstQuotesDel = MX_RTL_Quote_Selector.resultQryQuote('Id, Name, QuoteToName', 'OpportunityId = \'' + strOppoId + '\'', true);
        MX_RTL_Quote_Selector.deletQuote(lstQuotesDel);
        final List<Quote> lstQuotes = MX_RTL_Quote_Selector.resultQryQuote('Id, Name, QuoteToName', 'OpportunityId = \'' + strOppoId + '\'', true);
        return lstQuotes.isEmpty();
    }
}