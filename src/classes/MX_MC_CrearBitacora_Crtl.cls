/**
 * @File Name          : MX_MC_CrearBitacora_Crtl.cls
 * @Description        : Controlador de LWC MX_MC_CrearBitacora
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 16/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      16/06/2020           Jair Ignacio Gonzalez G.   Initial Version
**/
public without sharing class MX_MC_CrearBitacora_Crtl {

    /**
     * @Method MX_MC_CrearBitacora_Crtl
     * @Description Singletons
    **/
    @TestVisible private MX_MC_CrearBitacora_Crtl() {
    }

    /**
     * @Method convertLeads
     * @param Id idLead
     * @Description Convierte un Lead
     * @return Map<String,String>
    **/
    @AuraEnabled(cacheable=true)
    public static Boolean hasBitacora(Id idConv) {
        return MX_MC_CrearBitacora_Service.hasBitacora(idConv);
    }

    /**
     * @Method convertLeads
     * @param Id idLead
     * @Description Convierte un Lead
     * @return Map<String,String>
    **/
    @AuraEnabled(cacheable=true)
    public static dwp_kitv__Visit__c importBitacora(Id idConv) {
        return MX_MC_CrearBitacora_Service.importBitacora(idConv);
    }
}