/**
 * @description       : Clase extends de MX_SB_RetrieveData
 * @author            : Daniel Perez Lopez
 * @group             : 
 * @last modified on  : 12-30-2020
 * @last modified by  : Daniel Perez Lopez
 * Modifications Log 
 * Ver   Date         Author               Modification
 * 1.0   11-30-2020   Daniel Perez Lopez   Initial Version
**/
@SuppressWarnings('sf:UseSingleton,sf:DUDataflowAnomalyAnalysis')
public class MX_SB_PS_RetrieveDataExt_Ctrl {
     /** Integer 17*/
     final static Integer E17 =17;
      /** Integer 26*/
    final static Integer E26 =26;
     /** Integer 70*/
     final static Integer E70 =70;
     /** String para conparacion*/
    final static String CONYUG = 'Cónyuge';

    /**
    * @description : Methodo helper obtener codigo
    *  apartir de tipo de dato a consultar
    * @author Daniel Perez Lopez | 12-29-2020 
    * @param valor 
    * @param tipo 
    * @return string 
    **/
    public static string getCodes(String valor, String tipo) {
        string code='';
        switch on Tipo {
            when 'edad' {
                Final Integer edad =integer.valueOf(valor);
                if(edad<=E17) {
                    code='001';
                } else {
                    if (edad<=E26) {
                        code='002';
                    } else if(edad<=E70) {
                        code = '003';
                    } else {
                        code ='004';
                    }
                }
            }
            when 'parentesco' {
                Final string parentU=valor.toUpperCase();
                switch on parentU {
                    when 'TITULAR' {
                        code='001';
                    }
                    when 'CÓNYUGE' {
                        code='003';
                    }
                    when 'HIJO' {
                        code='004';
                    }
                }
            }
        }
        return code;
    }

    /**
    * @description: methodo de ordenamiento de beneficiarios
    * @author Daniel Perez Lopez | 12-30-2020 
    * @return MX_SB_VTS_Beneficiario__c[] 
    **/
    public static MX_SB_VTS_Beneficiario__c[] ordenabeneficiario (String quoteid) {
        final Map<String,String> mpdata = new Map<String,String>();
            mpdata.put('qFields','Id,MX_SB_VTS_Parentesco__c,MX_SB_SAC_FechaNacimiento__c,MX_SB_PS_Edad__c,Name,MX_SB_VTS_AMaterno_Beneficiario__c, MX_SB_VTS_APaterno_Beneficiario__c, MX_SB_VTS_Porcentaje__c ');
            mpdata.put('firstCon',' MX_SB_VTS_Quote__c ');
            mpdata.put('firstConVal',quoteid);
            mpdata.put('strObject','MX_SB_VTS_Beneficiario__c');
            mpdata.put('whereClause',' order by id asc');
            MX_SB_VTS_Beneficiario__c[] listaben= (MX_SB_VTS_Beneficiario__c[]) MX_SB_RTL_SObject_Selector.getObjectWC(mpdata);
            Integer count4remove=0;
            Integer removeIndex=0;
            Boolean doremove =false;
            for(MX_SB_VTS_Beneficiario__c itb : listaben) {
                if(itb.MX_SB_VTS_Parentesco__c==CONYUG) {
                    removeIndex=count4remove;
                    doremove=true;
                }
                count4remove++;
            }
            if (doremove) {
                listaben=removed(listaben,removeIndex);
            }
            return listaben;
    }
    /**
    * @description : methodo helper para reducir
    *  complejidad en ordenamiento de beneficiarios
    * @author Juan Carlos Benitez | 12-30-2020 
    * @param MX_SB_VTS_Beneficiario__c[]
    * @param Integer
    * @return MX_SB_VTS_Beneficiario__c[] 
    **/
    public static MX_SB_VTS_Beneficiario__c[] removed (MX_SB_VTS_Beneficiario__c[] listaben,Integer removeIndex ) {
        MX_SB_VTS_Beneficiario__c removed=new MX_SB_VTS_Beneficiario__c();
            removed= (MX_SB_VTS_Beneficiario__c) listaben.remove(removeIndex);
                if(listaben.size()==0) {
                    listaben.add(removed);    
                } else {
                    listaben.add(0,removed);
                }
                return listaben;
    }

}