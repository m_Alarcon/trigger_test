@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_RTL_Siniestro_Service_Test
* Autor: Eder Alberto Hernández Carbajal
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_RLT_Siniestro_Service

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         02/06/2020   Eder Alberto Hernández Carbajal              Creación
* -----------------------------------------------------------------------------------------------
*/
public with sharing class MX_RTL_Siniestro_Service_Test {
   	
    /*
	* var String Valor boton cancelar
	*/    
    public static final String CANCELAR = 'Cancelar';
    /*
	* var String Valor boton Volver
	*/    
    public static final String VOLVER = 'Volver';
    /*
	* var String Valor boton Crear
	*/    
    public static final String CREAR = 'Crear';
    
    /** Nombre de usuario Test */
    final static String NAME = 'Kamado Tanjiro';
    
    /** Nombre de usuario Test */
    final static String RECORDTYPEVIDA = 'MX_SB_MLT_RamoVida';
    
    /** current user id */
    final static String USR_ID = UserInfo.getUserId();
    /**
     * @description
     * @author Eder Hernández | 02/06/2020
     * @return void
     **/
    @TestSetup
    static void createData() {
        Final String profileName = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User userTest = MX_WB_TestData_cls.crearUsuario(NAME, profileName);  
        insert userTest;
        System.runAs(userTest) {
            final String recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Proveedores_MA).getRecordTypeId();
            final Account accTest = new Account(RecordTypeId = recordTypeId, Name=NAME, Tipo_Persona__c='Física');
            insert accTest;
            
            final Contract contractTest = new Contract(AccountId = accTest.Id, MX_SB_SAC_NumeroPoliza__c='policyTest');
            insert contractTest;
            
            
            Final List<Siniestro__c> listSin = new List<Siniestro__c>();
            for(Integer i = 0; i < 10; i++) {
                Final Siniestro__c testSin = new Siniestro__c();
                testSin.MX_SB_SAC_Contrato__c = contractTest.Id;
                testSin.MX_SB_MLT_NombreConductor__c = NAME + '_' + i;
                testSin.MX_SB_MLT_APaternoConductor__c = 'LastName for '+NAME;
                testSin.MX_SB_MLT_AMaternoConductor__c = 'LastName2 for '+NAME;
                testSin.MX_SB_MLT_Fecha_Hora_Siniestro__c =date.valueof('2020-03-27T22:04:00.000+0000');
                testSin.MX_SB_MLT_Telefono__c = '5534253647';
                Final datetime citaAgenda = datetime.now();
                testSin.MX_SB_MLT_CitaVidaAsegurado__c = citaAgenda.addDays(2);
                testSin.MX_SB_MLT_AtencionVida__c = 'Agendar Cita';
                testSin.MX_SB_MLT_PreguntaVida__c = false;
                testSin.MX_SB_MLT_SubRamo__c = 'Ahorro';
                testSin.TipoSiniestro__c = 'Siniestros';
                testSin.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_creacion;
                testSin.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoVida).getRecordTypeId();
                listSin.add(testSin);
            }
            Database.insert(listSin);
        }
    }
    /*
    * @description method que prueba MX_RTL_Siniestro_Service constructor
    * @param  void
    * @return void
    */
    @isTest
   	static void testConstructor() {
        Final MX_RTL_Siniestro_Service ctrlService = new MX_RTL_Siniestro_Service();
        system.assert(true,ctrlService);
    }
    /*
    * @description method que prueba MX_RTL_Siniestro_Service getSiniestroData
    * @param  void
    * @return void
    */
    @isTest
    static void testGetSiniestroData() {
        Final List<Siniestro__c> lSin = [SELECT Id FROM Siniestro__c limit 10];
        Final Set<Id> sinIds = new Set<Id>();
        for(Siniestro__c sin : lSin) {
            sinIds.add(sin.Id);
        }
		Final List<Siniestro__c> testSin = MX_RTL_Siniestro_Service.getSiniestroData(sinIds);
        System.assertEquals(testSin.size(), 10, 'Failed!');
    }
	/*
    * @description method que prueba MX_RTL_Siniestro_Service actualizarSin Asistencias
    * @param  void
    * @return void
    */
    @isTest
    static void testActualizarSinAsistencia() {
        Final Siniestro__c sinTest = [SELECT Id, MX_SB_MLT_NombreConductor__c, TipoSiniestro__c FROM Siniestro__c where MX_SB_MLT_NombreConductor__c = :NAME + '_6'];
        sinTest.TipoSiniestro__c = 'Cristales';
        Database.update(sinTest);
        sinTest.MX_SB_MLT_NombreConductor__c = 'Actualizado';
		test.startTest();
        	MX_RTL_Siniestro_Service.actualizarSin(sinTest);
        test.stopTest();
        System.assert(true, 'Asistencia actualizada!');
    }
    /*
    * @description method que prueba MX_RTL_Siniestro_Service actualizarSin Siniestro
    * @param  void
    * @return void
    */
    @isTest
    static void testActualizarSinSiniestro() {
        Final Siniestro__c sinTest = [SELECT Id, MX_SB_MLT_NombreConductor__c, TipoSiniestro__c FROM Siniestro__c where MX_SB_MLT_NombreConductor__c = :NAME + '_6'];
        sinTest.MX_SB_MLT_NombreConductor__c = 'Actualizado';
		test.startTest();
        	MX_RTL_Siniestro_Service.actualizarSin(sinTest);
        test.stopTest();
        System.assert(true, 'Siniestro actualizado!');
    }
    /*
    * @description method que prueba MX_RTL_Siniestro_Service sinAction
    * @param  void
    * @return void
    */
    @isTest
    static void testSinAction() {
        Final Siniestro__c sinTest = [SELECT Id FROM Siniestro__c where MX_SB_MLT_NombreConductor__c = :NAME + '_4'];
        Final Set<Id> updateId = new Set<Id>{sinTest.Id};
		test.startTest();
        	MX_RTL_Siniestro_Service.sinAction(VOLVER,updateId);
        	MX_RTL_Siniestro_Service.sinAction(CANCELAR,updateId);
        test.stopTest();
        System.assert(true, 'Etapa cambiada!');
    }
    /*
    * @description method que prueba MX_RTL_Siniestro_Service getRecordTId
    * @param  void
    * @return void
    */
    @isTest
    static void testGetRecordTId() {
        MX_RTL_Siniestro_Service.getRecordTId(RECORDTYPEVIDA);
        System.assert(true, 'Busqueda exitosa!');
    }
    /*
    * @description method que prueba MX_RTL_Siniestro_Service upsertSini
    * @param  void
    * @return void
    */
    @isTest
    static void testUpsertSini() {
        Final Siniestro__c sinTest = [SELECT Id, MX_SB_MLT_APaternoConductor__c FROM Siniestro__c where MX_SB_MLT_NombreConductor__c = :NAME + '_8'];
        sinTest.MX_SB_MLT_APaternoConductor__c = 'Apellido actualizado';
		test.startTest();
        	MX_RTL_Siniestro_Service.upsertSini(sinTest);
        test.stopTest();
        System.assert(true, 'Actualización exitosa!');
    }
}