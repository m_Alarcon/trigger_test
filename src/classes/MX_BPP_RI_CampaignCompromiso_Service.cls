/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_RI_CampaignCompromiso_Service
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-06-16
* @Description 	Service Layer for MX_BPP_RI_CampaignCompromiso
* @Changes
*  
*/
public with sharing class MX_BPP_RI_CampaignCompromiso_Service {
    
    /*Contructor MX_BPP_RI_CampaignCompromiso_Service */
    @testVisible private MX_BPP_RI_CampaignCompromiso_Service() {}
    
    /**
    * @Description 	Lista de Leads convertidos del propietario de la RI
    * @Param		oppItem <Trigger.new>, oldMap <Trigger.oldMap>
    * @Return 		OpportunityWrapper ConvertedLead List
    **/
    public static List<EU001_cls_CompHandler.OpportunityWrapper> getConvertedWrapList (List<EU001_cls_CompHandler.OpportunityWrapper> listOppWInt) {
        final List<EU001_cls_CompHandler.OpportunityWrapper> listOppW = new List<EU001_cls_CompHandler.OpportunityWrapper>();
        final Map<Id, EU001_cls_CompHandler.OpportunityWrapper> mapIdLeads = new Map<Id, EU001_cls_CompHandler.OpportunityWrapper>();
        
        for(EU001_cls_CompHandler.OpportunityWrapper oppWInt : listOppWInt) {
            mapIdLeads.put(oppWInt.oppId, oppWInt);
        }
        
        final Map<String, String> mapOpps = MX_BPP_ConvertLeads_Service.convertToLead(new List<Id>(mapIdLeads.keySet()));
        for (String idLead : mapOpps.keySet()) {
            final EU001_cls_CompHandler.OpportunityWrapper oppWrap = mapIdLeads.get(idLead);
            oppWrap.oppId = mapOpps.get(idLead);
            oppWrap.seleccion = true;
            listOppW.add(oppWrap);
        }
        
        return listOppW;
    }

}