/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 10-30-2020
 * @last modified by  : Eduardo Hernandez Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   08-26-2020   Eduardo Hernández Cuamatzi   Initial Version
 * 1.1   02-22-2021   Eduardo Hernández Cuamatzi   Se agrega token antifraude a consumo de otp
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_PaymentModule_Ctrl {
    /**Campos Telefono */
    final static String FIELDS = 'Id, TelefonoCliente__c';
    /**Llave respuesta ok */
    final static String ISOK = 'isOk';
    /**
    * @description Recupera datos de pagos para cotización sincronizada
    * @author Eduardo Hernandez Cuamatzi | 10-02-2020 
    * @param String oppId Id de Oportunidad
    * @return Map<String, Object> Mapa de datos
    **/
    @AuraEnabled
    public static Map<String, Object> findDataQuote(String oppId) {
        final Map<String, Object> response = new Map<String, Object>();
        try {
            final List<paymentResum> listPayments = MX_SB_VTS_PaymentModule_Service.fillDataPayment(oppId);
            response.put(ISOK, true);
            response.put('dataPayment', listPayments);
            response.put('quoteData', MX_SB_VTS_PaymentModule_Service.findQuote(oppId));
            response.put('generica', MX_SB_VTS_PaymentModule_Service.findGenericaVals('CP21'));
        } catch (DmlException dmlEx) {
            response.put('isOk', false);
            response.put('DetailError', dmlEx.getMessage());
        }
        return response;
    }
    
    /**
    * @description Recupera estatus de OTP
    * @author Eduardo Hernandez Cuamatzi | 10-02-2020 
    * @param String recordId Ide de Oportunidad
    * @return Map<String, String> Mapa de resultados
    **/
    @AuraEnabled
    public static Map<String, String> getCodeStatus(String recordId, String tokenSF) {
        final String phone = MX_SB_VTS_GenerarOTP_Service.getPhone(recordId, FIELDS);
        final Quote quoteData = MX_SB_VTS_PaymentModule_Service.findQuote(recordId);
        final Map<String, String> getValues = new Map<String, String>();
        getValues.putAll(MX_SB_VTS_GenerarOTP_Service.getStatusOTP(phone, quoteData, tokenSF));
        return getValues;
    }

    /**
    * @description Actualiza Quote previo al envio a IVR
    * @author Eduardo Hernandez Cuamatzi | 10-02-2020 
    * @param String oppId Id del registro a actualizar
    **/
    @AuraEnabled
    public static void updateQuoteIVR(String oppId) {
        final Quote quoteData = MX_SB_VTS_PaymentModule_Service.findQuote(oppId);
        quoteData.MX_ASD_PCI_DescripcionCode__c = '';
        quoteData.MX_ASD_PCI_ResponseCode__c = '';
        quoteData.MX_ASD_PCI_MsjAsesor__c = '';
        MX_SB_VTS_PaymentModule_Service.updateQuote(quoteData);
    }

    /**
    * @description Recupera bandera para tipo de generación de token
    * @author Eduardo Hernandez Cuamatzi | 10-02-2020 
    * @return Map<String, Object> Mapa de valores
    **/
    @AuraEnabled
    public static Map<String, Object> findGenerica() {
        final Map<String, Object> response = new Map<String, Object>();
        final List<MX_SB_VTS_Generica__c> lstGenerica = MX_SB_VTS_PaymentModule_Service.findGenericaVals('CP21');
        response.put(ISOK, true);
        response.put('generica', Boolean.valueOf(lstGenerica[0].MX_SB_SAC_ProductFam__c));
        return response;
    }

    /**
    * @description Recupera bandera para tipo de generación de token
    * @author Eduardo Hernandez Cuamatzi | 10-02-2020 
    * @return Map<String, Object> Mapa de valores
    **/
    @AuraEnabled
    public static Map<String, Object> findDataCode(String ivrCode) {
        final Map<String, Object> response = new Map<String, Object>();
        try {
            final List<MX_RTL_IVRCodes__mdt> lstGenerica = MX_SB_VTS_PaymentModule_Service.findDataCodeVals(ivrCode);
            if(lstGenerica.isEmpty() == false) {
                response.put(ISOK, true);
                response.put('title', lstGenerica[0].MX_RTL_TitleMsg__c);
                response.put('msjDetail', lstGenerica[0].MX_RTL_MsgSF__c);
            } else {
                response.put(ISOK, false);
            }
        } catch (DmlException dmlEx) {
            response.put(ISOK, false);
            response.put('DetailError', dmlEx.getMessage());
        }
        return response;
    }

    /**
    * @description Formaliza status cotización
    * @author Eduardo Hernández Cuamatzi | 10-30-2020 
    * @param oppId Id de la Oportunidad
    * @return Map<String, Object> Mapa de resultado al consumir servicio
    **/
    @AuraEnabled
    public static Map<String, Object> updateStatuscOT(String oppId) {
        final Map<String, Object> response = new Map<String, Object>();
        final Quote quoteStatus = MX_SB_VTS_PaymentModule_Service.findQuote(oppId);
        final Set<String> lstStatusQu = new Set<String>{'Formalizada', 'Emitida'};
        Boolean quoteUpdated = false;
        if(lstStatusQu.contains(quoteStatus.Status)) {
            quoteUpdated = true;
        } else {
            response.put('updateQuote', MX_SB_VTS_PaymentModule_Helper.formQuote(quoteStatus));
        }
        response.put('statusQuote', quoteUpdated);
        response.put(ISOK, true);
        return response;
    }

    /**
    * @description Emite Poliza
    * @author Eduardo Hernández Cuamatzi | 10-30-2020 
    * @param oppId Id del registro de la oportunidad
    * @return Map<String, Object> Mapa de respuesta de emision de la poliza
    **/
    @AuraEnabled
    public static Map<String, Object> createdPolyce(String oppId) {
        final Map<String, Object> response = new Map<String, Object>();
        final Quote quoteStatus = MX_SB_VTS_PaymentModule_Service.findQuote(oppId);
        Boolean policyCreated = false;
        if(String.isBlank(quoteStatus.MX_SB_VTS_Numero_de_Poliza__c) || quoteStatus.Status != 'Emitida') {
            response.put('polyceStatus', MX_SB_VTS_PaymentModule_Helper.createdPolyce(quoteStatus));
            policyCreated = true;
        }
        response.put('policyCreated', policyCreated);
        response.put(ISOK, true);
        return response;
    }

    /**Meteodo aura para enviar documentos por correo */
    @AuraEnabled
    public static Map<String, Object> findCustomerDocument(String oppId, String docType) {
        return MX_SB_VTS_SendDocumentsRuc_ctrl.findCustomerDocument(oppId, docType);
    }

    /**Meteodo aura para enviar documentos por correo */
    @AuraEnabled
    public static Map<String, Object> sendCustomerDocument(String oppId, List<String> lstHrefsDocs) {
        return MX_SB_VTS_SendDocumentsRuc_ctrl.sendCustomerDocument(oppId, lstHrefsDocs);
    }

    /**Wrapper para datos de pago */
    public class paymentResum {
        /**valore llave de resumen pago */
        @AuraEnabled
        public string idValue {get;set;}
        /**valor title o label a mostrar */
        @AuraEnabled
        public string textVal {get;set;}
        /**valor a desplegar en el front */
        @AuraEnabled
        public string dataValue {get;set;}
    }
    /**
    * @description Función que actualiza etapa actual de la oportunidad
    * @author Diego Olvera | 27-11-2020 
    * @param  srtOppId, etapaForm Id de la oportunidad y etapa actual del lwc recuperada desde el front JS
    * @return void
    **/ 
    @AuraEnabled
    public static void updCurrentOppCtrl(String srtOppId, String etapaForm) {
        MX_SB_VTS_AddressInfo_Service.updCurrentOpp(srtOppId, etapaForm);
    }
}