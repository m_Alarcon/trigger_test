@isTest
/**
* @FileName          : MX_SB_VTS_Colonias_Test
* @description       : Test class from MX_SB_VTS_Colonias_Ctrl
* @Author            : Marco Antonio Cruz Barboza
* @last modified on  : 08-05-2020
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   08-05-2020   Marco Antonio Cruz Barboza          Initial Version
**/
public class MX_SB_VTS_Colonias_Test {

    /** User name testing */
    final static String TESTINGUSR = 'UserTest';
    /** Account Name testing*/
    final static String TESTACCOUNT = 'Test Account';
    /** Opportunity Name Testintg */
    final static String TESTOPPORT = 'Test Opportunity';
    /**URL Mock Test*/
    final static String URLTEST = 'http://www.example.com';
    /**ZipCode Test*/
    Final static String ZIPCODE = '07469';
    /**Body Response Mock*/
    Final static String RESPMOCK = '[{"comparables": 13,"houseList": [{"comparablesNumber": 13,"price": 4667515.5,"pricem2": 15789.149,"surface": 295.6154,"byRooms": [{"price": 837700.0,"pricem2": 10471.25,"room": 1,"surface": 80.0}],"comparables": [{"bathroom": 5,"description": "Excelente casa espaciosa de 4 niveles  con 12 recámaras , 5 baños, 3 estacionamientos, patio, comed","latitude": 19.3287,"linkportal": "http://www.doomos.com.mx/de/7997057_casa---coyoacan.html","longitude": -99.165,"m2": 445.0,"price": 6800000.0,"rooms": 12}]}]}]';
    /**Tsec Mock*/
    Final static Map<String,String> TSECTST = new Map<String,String> {'tsec'=>'1234456789'};

    
    /* 
    @Method: setupTest
    @Description: create test data set
    */
    @TestSetup
    static void setupTest() {
        final User usrTest = MX_WB_TestData_cls.crearUsuario(TESTINGUSR, System.Label.MX_SB_VTS_ProfileAdmin);
        System.runAs(usrTest) {
            final Account tstAcc = MX_WB_TestData_cls.crearCuenta(TESTACCOUNT, System.Label.MX_SB_VTS_PersonRecord);
            tstAcc.PersonEmail = 'pruebaVts@mxvts.com';
            insert tstAcc;
            final Opportunity tstOpport = MX_WB_TestData_cls.crearOportunidad(TESTOPPORT, tstAcc.Id, usrTest.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            tstOpport.LeadSource = 'Call me back';
            tstOpport.Producto__c = 'Hogar';
            tstOpport.Reason__c = 'Venta';
            tstOpport.StageName = 'Cotizacion';
            insert tstOpport;
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'getListInsuraceCustomerCatalog', iaso__Url__c = URLTEST, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'listPlaces', iaso__Url__c = URLTEST, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'neighborhoodSearch', iaso__Url__c = URLTEST, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
            
        }
    }

    /*
    @Method: getZipCodeTest
    @Description: return Map with a List of objects from a service
	@param String ZipCode
    */
    @isTest
    static void getZipCodeTest() {
		Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, 'Complete', '{"iCatalogItem": {"suburb": [{"neighborhood": {"id": "0196","name": "UNIDAD HABITACIONAL"},"county": {"id": "005","name": "GUSTAVO A. MADERO"},"city": {"id": "01","name": "CIUDAD DE MÉXICO"},"state": {"id": "09","name": "CIUDAD DE MEXICO"}}]}}', TSECTST); 
        iaso.GBL_Mock.setMock(mockCallout);
        test.startTest();
            Final Map<String,List<Object>> testResponse = MX_SB_VTS_Colonias_Ctrl.getZipCode(ZIPCODE);
            System.assertNotEquals(testResponse, null,'El mapa esta retornando');
        test.stopTest();
    }
    
   	/*
    @Method: getListPlaces
    @Description: return Map with a List of objects from a service
	@param String ZipCode
    */
    @isTest
    static void getListPlaces() {
        Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, 'Complete', '{"data": [{"id": "636|Pedregal de Santo Domingo","name": "Pedregal de Santo Domingo","geolocation": {"latitude": 19.328390326852794,"longitude": -99.16694122424343},"geographicPlaceTypes": [{"id": "MUNICIPALITY","name": "Municipality","value": "Coyoacán"},{"id": "ADMINISTRATIVE_AREA_4","name": "Administrative Area 4"}],"state": {"id": "DF","name": "Ciudad de México"},"country": {"id": "1","name": "México"}}]}', TSECTST);  
        iaso.GBL_Mock.setMock(mockCallout);
        test.startTest();
            Final Map<String,List<Object>> tstResp = MX_SB_VTS_Colonias_Ctrl.getListPlaces(ZIPCODE);
            System.assertNotEquals(tstResp, null,'El mapa esta retornando');
        test.stopTest();
    }
    
    /*
    @Method: getListPlaces
    @Description: return Map with a List of objects from a service
	@param String ZipCode
    */
    @isTest
    static void getNeighborhoodTest() {
		Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, 'Complete', RESPMOCK, TSECTST); 
        iaso.GBL_Mock.setMock(mockCallout);
        Final Map<String,Object> sendInfo = new Map<String,Object>();
        sendInfo.put('latitude','19.328390326852794');
        sendInfo.put('longitude','-99.16694122424343');
        sendInfo.put('propertyType','flat');
        sendInfo.put('isNewDev','true');
        test.startTest();
            Final List<Object> tstResp = MX_SB_VTS_Colonias_Ctrl.getNeighborhood(sendInfo);
            System.assertNotEquals(tstResp, null,'El mapa esta retornando');
        test.stopTest();
    }

}