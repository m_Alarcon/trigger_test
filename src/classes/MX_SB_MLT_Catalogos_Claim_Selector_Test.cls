@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_Catalogos_Claim_Selector_Test
* Autor: Eder Alberto Hernández Carbajal
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_Catalogos_Claim_Selector

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         02/06/2020   Eder Alberto Hernández Carbajal              Creación
* -----------------------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_Catalogos_Claim_Selector_Test {
    
    /**Tipo de catalogo Ocupacion */
    Final static String TIPO_OCP = 'Ocupación';
    
    /**Tipo de catalogo Ocupacion */
    Final static String TIPO_ICD = 'ICD';
    
    /**Descripción Test*/
    Final static String TEST_OCP = 'WINCHERO';
    
    /**Descripción Test*/
    Final static String TEST_ICD = 'CIRROSIS';

    /*
    * @description method que prueba MX_SB_MLT_Catalogos_Claim_Selector constructor
    * @param  void
    * @return void
    */
    @isTest
    private static void testConstructor() {
        Final MX_SB_MLT_Catalogos_Claim_Selector constSelect = new MX_SB_MLT_Catalogos_Claim_Selector();
        system.assert(true,constSelect);
    }
    /*
    * @description method que prueba MX_SB_MLT_Catalogos_Claim_Selector getCatalog ICD
    * @param  void
    * @return void
    */
    @isTest
    static void testGetCatalogICD() {
        Test.startTest();
        MX_SB_MLT_Catalogos_Claim_Selector.getCatalog(TEST_ICD, TIPO_ICD);
        Test.stopTest();
        System.assert(true, 'Se encontraron coincidencias.');
    }
    /*
    * @description method que prueba MX_SB_MLT_Catalogos_Claim_Selector getCatalog Ocupacion
    * @param  void
    * @return void
    */
    @isTest
    static void testGetCatalogOcp() {
        Test.startTest();
        MX_SB_MLT_Catalogos_Claim_Selector.getCatalog(TEST_OCP, TIPO_OCP);
        Test.stopTest();
        System.assert(true, 'Se encontraron coincidencias.');
    }
}