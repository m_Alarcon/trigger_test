/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Campaign_Service_Helper_Test
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-08-10
* @Description 	Test Class for MX_BPP_Campaign_Service_Helper
* @Changes
*  
*/
@IsTest
public class MX_BPP_Campaign_Service_Helper_Test {
    
    /** String RecordTypeDeveloperName Campaign */
    static final String RTCAMPAIGN = 'BPyP_Campaign';
    
    @TestSetup
    static void setup() {
        final Campaign newCampaign= new Campaign();
        newCampaign.Name = 'TestCampaignBPYP';
        newCampaign.IsActive = true;
        newCampaign.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get(RTCAMPAIGN).getRecordTypeId();
        insert newCampaign;
    }
    
    /**
    *@Description   Test Method for insertCampaignMemberStatus
    *@author 		Edmundo Zacarias
    *@Date 			2020-08-10
    **/
    @isTest
    private static void testCreateRelatedCMemberStatus() {
        final Set<Id> campaignsId = new Set<Id>();
        final List<String> strStatusList = new List<String>();
        final Campaign baseCampaign = [SELECT Id FROM Campaign WHERE Name = 'TestCampaignBPYP' AND RecordType.DeveloperName =:RTCAMPAIGN LIMIT 1];
        campaignsId.add(baseCampaign.Id);
        strStatusList.add('Test Status');
        
        Test.startTest();
        MX_BPP_Campaign_Service_Helper.createRelatedCMemberStatus(campaignsId, strStatusList);
        Test.stopTest();
        
        final List<CampaignMemberStatus> results = [SELECT Id FROM CampaignMemberStatus WHERE CampaignId = :baseCampaign.Id];
        System.assertNotEquals(null, results.size(), 'Problema en insert CampaignMemberStatus');
    }

}