/**
 * @description       : Clase controller que sirve de apoyo para la clase service MX_SB_VTS_InitQuo_Service
 * @author            : Diego Olvera
 * @group             : BBVA
 * @last modified on  : 02-17-2021
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   01-08-2021   Diego Olvera   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_InitQuo_Ctrl {
    /**
    * @description Función que se ejecuta a partir del front del cotizador para crear quote
    * @author Diego Olvera | 27-11-2020 
    * @param  idOp, nVal Id de la oportunidad recuperada desde el front JS
    * @return void
    **/
    @AuraEnabled
    public static void ctrDataQuoteCtrl(String idOp, String nVal) {
        MX_SB_VTS_InitQuo_Service.ctrDataQuote(idOp, nVal);
    }
    /**
    * @description Actualiza el campo de Quote en registro de Direcciones
    * @author Diego Olvera | 26-01-2020 
    * @param currId Id de registro trabajado (Opportunity)
    * @return Map<String, Object> Datos recuperados
    **/
    @AuraEnabled
    public static void updSyncAddCtrl(String currId) {
         MX_SB_VTS_InitQuo_Service.updSyncAdd(currId);
    } 
}