/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_Tipificaciones
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 25/06/2020                 Creación de la clase.
1.1					Gerardo Mendoza Aguilar        17/11/2020                 Se agrego Method upsrtTask
*/
@SuppressWarnings('sf:DUDataflowAnomalyAnalysis')
public with sharing  class MX_SB_PS_Tipificaciones {
	 /*Final Static String*/
    final static string ONE = '1';
	 /*Final Static String*/
	final static string STRTRUE = 'true';
	/*Constructor */
    private MX_SB_PS_Tipificaciones() {}
    /*Methodo recibe parametro Objeto y trae informacion de picklist dependientes de dicho objeto*/
    @AuraEnabled 
    public static Map<String, List<String>> getDependentMap(sObject objDetail, string contrfieldApiName,string depfieldApiName) {
        final String controllingField = contrfieldApiName.toLowerCase();
        final String dependentField = depfieldApiName.toLowerCase();    
        final Map<String,List<String>> objResults = new Map<String,List<String>>();
        final Schema.sObjectType objType = objDetail.getSObjectType();
        final Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
        final Schema.SObjectField theField = objFieldMap.get(dependentField);
        final Schema.SObjectField ctrlField = objFieldMap.get(controllingField);
        final List<Schema.PicklistEntry> contrEntries = ctrlField.getDescribe().getPicklistValues();
        final List<PicklistEntryWrapper> depEntries = wrapPicklistEntries(theField.getDescribe().getPicklistValues());
        final List<String> controllingValues = new List<String>();
        String labelfor ='';
        for (Schema.PicklistEntry ple : contrEntries) {
            labelfor = ple.getLabel() ;
            objResults.put(labelfor, new List<String>());
            controllingValues.add(labelfor);
        }
        String labelfor2 ='';
        labelfor2 = 'None';
        for (PicklistEntryWrapper plew : depEntries) {
            labelfor2 = plew.label;
            if(plew.Active==STRTRUE) {
                final String validForBits = base64ToBits(plew.validFor);
                for (Integer i = 0; i < validForBits.length(); i++) {
                    final String bit = validForBits.mid(i, 1);
                    if (bit == ONE) {
                        objResults.get(controllingValues.get(i)).add(labelfor2);
                    }
                }
            }
        }
        return objResults;
    }
    /*Methodo redondea los valores de picklist*/
    public static String decimalToBinary(Integer val) {
        String bits = '';
        Integer valEv = val;
        while (valEv > 0) {
            final Integer remainder = Math.mod(valEv, 2);
            valEv = Integer.valueOf(Math.floor(valEv / 2));
            bits = remainder + bits;
        }
        return bits;
    }
    /*Methodo redondea los valores de picklist*/
    public static String base64ToBits(String validFor) {
        final String  BASE64 = '' +
            'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
            'abcdefghijklmnopqrstuvwxyz' +
            '0123456789+/';
        String validForBits = '';
        String thisChar = '';
        for (Integer i = 0; i < validFor.length(); i++) {
            thisChar = validFor.mid(i, 1);
            final Integer val =  BASE64.indexOf(thisChar);
            final String bits = decimalToBinary(val).leftPad(6, '0');
            validForBits += bits;
        }
        return validForBits;
    }
    /*Methodo redondea los valores de picklist*/
    private static List<PicklistEntryWrapper> wrapPicklistEntries(List<Schema.PicklistEntry> pLES) {
        return (List<PicklistEntryWrapper>)
            JSON.deserialize(JSON.serialize(pLES), List<PicklistEntryWrapper>.class);
    }
    /*Clasemwrapper*/
    public class PicklistEntryWrapper {
        /*Public property for wrapper*/
        public String active {get;set;}
        /*Public property for wrapper*/
        public String defaultValue {get;set;}
        /*Public property for wrapper*/
        public String label {get;set;}
        /*Public property for wrapper*/
        public String value {get;set;}
        /*Public property for wrapper*/
        public String validFor {get;set;}
        /*Subclass constructor*/
        public PicklistEntryWrapper() {            
            active = '';
            defaultValue = '';
            label = '';
            value= '';
            validFor = '';
        }
        
    }
       /*Methodo que devuelve el valor stage de oportunidad*/
    @auraEnabled
    public static Integer fetchOpp(String strId,String varObject,String pckList) {
        final string[] setValues = pcklist.split(',');
		final string oid = strId;
        final string oField = setValues[0];
		final string startIndex = setValues[1];
        final string oObject = varObject;
        final Schema.SObjectType obj_describe = Schema.getGlobalDescribe().get(oObject) ;
        final Schema.DescribeSObjectResult obj_describe_re = obj_describe.getDescribe() ;
        final Map<String,Schema.SObjectField> fields = obj_describe_re.fields.getMap() ;
        final Schema.DescribeFieldResult fieldResult = fields.get(oField).getDescribe();   
        final Sobject opp =  MX_SB_PS_Utilities.fetchSobject(oId,oField,oObject);
        integer index;  
        final String[] stages = new String[0];
        for(PicklistEntry value: fieldResult.getPicklistValues()) {
            stages.add(value.getValue());
        }
        index = stages.indexOf(String.valueOf(opp.get(oField)));
        return index-stages.indexOf(startIndex);
        
    }
           /*Methodo que devuelve el valor stage de oportunidad*/
    @auraEnabled
    public static map<string,list<string>>  configType(String recordId) {
        final map<string,list<string>>  mpGener = new Map<String,list<String>>();
        string strIndex = '';
        list<String> strValues =  new list<String>();
        Final MX_SB_VTS_Generica__c[]  generica = [SELECT Id, MX_SB_VTS_Type__c, MX_SB_VTS_Src__c,MX_SB_VTS_Status__c, Name FROM MX_SB_VTS_Generica__c WHERE MX_SB_VTS_Type__c = 'FLDTY' ORDER BY MX_SB_VTS_Status__c ];    
        for(MX_SB_VTS_Generica__c g :generica) {          
            if(strIndex=='') {
                strIndex = g.MX_SB_VTS_Status__c;
            }
            strValues.add(g.MX_SB_VTS_Src__c);
            if(strIndex != g.MX_SB_VTS_Status__c) {
                strValues.remove(strValues.size()-1);
                mpGener.put(strIndex,strValues);
                strIndex = g.MX_SB_VTS_Status__c;
                strValues =  new list<String>();
                strValues.add(g.MX_SB_VTS_Src__c);
            }
        }
        mpGener.put(strIndex,strValues);
        return mpGener;
    }
	/*Methodo que devuelve el valor stage de oportunidad*/
    @auraEnabled
    public static void  saveType(String strId,String strType1) {
        final MX_SB_VTS_Generica__c[]  generica = [SELECT Id, MX_SB_VTS_HREF__c FROM MX_SB_VTS_Generica__c WHERE MX_SB_VTS_Type__c = 'FLDTY' AND MX_SB_VTS_Src__c =: strType1];
        final String fieldType = generica[0].MX_SB_VTS_HREF__c;
        final String[] tl1to5 = fieldType.split(',');
        final Opportunity opp = new Opportunity();
        opp.Id  = strId;
        opp.MX_SB_VTS_Tipificacion_LV1__c=tl1to5[0];
        opp.MX_SB_VTS_Tipificacion_LV2__c=tl1to5[1];
        opp.MX_SB_VTS_Tipificacion_LV3__c=tl1to5[2];
        opp.MX_SB_VTS_Tipificacion_LV4__c=tl1to5[3];
        opp.MX_SB_VTS_Tipificacion_LV5__c=tl1to5[4];
        opp.MX_SB_VTS_Tipificacion_LV6__c=strType1;
        Database.update(opp);
    }
	/*Methodo que devuelve el valor stage de oportunidad*/
    @AuraEnabled
    public static Opportunity getOppData (String oppId) {
        return MX_SB_PS_Utilities.fetchOpp(oppId);
    }
    
    /**
    * @description :methodo para obtener inf
    * de tarea relacionada a oportunidad
    * @author Gerardo Mendoza Aguilar | 11-13-2020 
    * @param oppId 
    * @return List<Task> 
    **/
    @AuraEnabled
    public static List<Task> getTaskData(String oppId) {
        List<Task> taskData = new List<Task>();
        if(String.isNotBlank(oppId)) {
            taskData = MX_RTL_Task_Service.selectTaskById(oppId);
        }
        return taskData;
    }

    /**
    * @description : methodo para actualizacion de tarea
    * @author Gerardo Mendoza Aguilar | 11-17-2020 
    * @param taskObj 
    * @param type 
    **/
    @AuraEnabled
    public static void upsrtTask(Task taskObj, String type) {
        MX_RTL_Task_Service.updateTask(taskObj, type);
    }
}