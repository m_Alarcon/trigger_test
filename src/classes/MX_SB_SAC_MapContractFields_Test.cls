/**
 * @File Name          : MX_SB_SAC_MapContractFields_Test.cls
 * @Description        : 
 * @Author             : Jaime Terrats
 * @Group              : 
 * @Last Modified By   : Jaime Terrats
 * @Last Modified On   : 12/21/2019, 5:39:00 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/21/2019   Jaime Terrats     Initial Version
**/
@isTest
private class MX_SB_SAC_MapContractFields_Test {
    /** admin name */
    final static String ADMIN_NAME = 'test contract';
    /** account email */
    final static String PERSON_EMAIL = 'test@contract.com';
    /** account name */
    final static String ACC_NAME = 'John';
    /** account last name */
    final static String LAST_NAME = 'Doe';
    /**
    * @description 
    * @author Jaime Terrats | 12/21/2019 
    * @return void 
    **/
    @TestSetup
    static void makeData() {
        final User admin = MX_WB_TestData_cls.crearUsuario(ADMIN_NAME, system.label.MX_SB_VTS_ProfileAdmin);
        Insert admin;

        System.runAs(admin) {
            MX_WB_TestData_cls.createStandardPriceBook2();

            final MX_WB_FamiliaProducto__c objFamilyPro2 = MX_WB_TestData_cls.createProductsFamily(System.Label.MX_SB_VTS_FamiliaASD);
            insert objFamilyPro2;

            final Product2 producto = MX_WB_TestData_cls.productNew(System.Label.MX_SB_VTS_AutoSeguroCorrecto);
            producto.isActive = true;
            producto.MX_WB_FamiliaProductos__c = objFamilyPro2.Id;
            insert producto;

            final PricebookEntry pbe = MX_WB_TestData_cls.priceBookEntryNew(producto.Id);
            insert pbe;

            final Account acc = MX_WB_TestData_cls.crearCuenta(LAST_NAME, System.Label.MX_SB_VTS_PersonRecord);
            acc.PersonEmail = PERSON_EMAIL;
            acc.FirstName = ACC_NAME;
            insert acc;

            final Contract contr = MX_WB_TestData_cls.vtsCreateContract(acc.Id, admin.Id, producto.Id);
            insert contr;
        }
    }

    /**
    * @description 
    * @author Jaime Terrats | 12/21/2019 
    * @return void 
    **/
    @isTest
    private static void testUpdate() {
        final List<Contract> contractsToUpdate = new List<Contract>();
        final Contract contr = [Select Id, MX_SB_SAC_NombreClienteAseguradoText__c, MX_WB_nombreAsegurado__c, 
                        MX_SB_SAC_NombreCuentaText__c, MX_SB_SAC_NombreCuenta__c, MX_SB_SAC_NumeroPoliza__c,
                        MX_WB_noPoliza__c, MX_SB_SAC_NumeroSerie__c, MX_WB_numeroSerie__c, MX_SB_SB_Placas__c,
                        MX_WB_placas__c, MX_SB_SAC_EmailAsegurado__c, MX_SB_SAC_RFCContratanteText__c,
                        MX_WB_emailAsegurado__c, MX_SB_SAC_RFCContratante__c 
                        from Contract where Account.PersonEmail =: PERSON_EMAIL];
        
        contr.MX_WB_nombreAsegurado__c = ACC_NAME;
        contr.MX_WB_noPoliza__c = 'POL123';
        contractsToUpdate.add(contr);
        Test.startTest();
        MX_SB_SAC_MapContractFields.mapFields(contractsToUpdate);
        System.AssertEquals(contr.MX_WB_noPoliza__c, contr.MX_SB_SAC_NumeroPoliza__c, 'Coinciden los datos?');
        Test.stopTest();
    }
}