/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase test de MX_BPP_RelatedLeads_Crtl

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     19/06/2020      Roberto Isaac Soto Granados           Creación Clase
*****************************************************************************************/
@isTest
private class MX_BPP_RelatedLeads_Crtl_test {
    /*Lead de pruebas*/
    private static Lead testLead = new Lead();
    /*Account de pruebas*/
    private static Account testAcc = new Account();
    /*Miembro de Campaña de pruebas*/
    private static CampaignMember testMember = new CampaignMember();
    /*Campaña de pruebas*/
    private static Campaign testCampaign = new Campaign();

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testAcc.FirstName = 'Cuenta';
        testAcc.LastName = 'Test';
        testAcc.No_de_Cliente__c = '12345678';
        testAcc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('MX_BPP_PersonAcc_Client').getRecordTypeId();
        insert testAcc;
        testLead.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('MX_BPP_Leads').getRecordTypeId();
        testLead.MX_ParticipantLoad_id__c = '12345678';
        testLead.FirstName = 'Lead';
        testLead.LastName = 'Test';
        testLead.isConverted = false;
        testLead.LeadSource = 'Preaprobados';
        testLead.MX_LeadEndDate__c = Date.today()+5;
        insert testLead;
        testCampaign.Name = 'Campaign';
        insert testCampaign;
        testMember.LeadId = testLead.Id;
        testMember.CampaignId = testCampaign.Id;
        testMember.MX_LeadEndDate__c = Date.today().addDays(13);
        insert testMember;
    }

    /*Ejecuta la acción para cubrir clase*/
    static testMethod void convertLeadsTest() {
        testLead = [SELECT Id FROM Lead LIMIT 1];
        final Map<String,String> testMap = MX_BPP_RelatedLeads_Crtl.convertLeads(testLead.Id);
        System.assert(!testMap.isEmpty(), 'Consulta correcta');
    }

    /*Ejecuta para cubrir clase*/
    static testMethod void relatedLeadsTest() {
        testLead = [SELECT Id FROM Lead LIMIT 1];
        final List<CampaignMember> testList = MX_BPP_RelatedLeads_Crtl.relatedLeads(testLead.Id);
        System.assert(testList.isEmpty(), 'Consulta vacía');
    }

    /*Ejecuta para cubrir clase*/
    static testMethod void buttonMenuTest() {
        final User usrAdmn = UtilitysDataTest_tst.crearUsuario('PruebaAdm', Label.MX_PERFIL_SystemAdministrator, 'BBVA ADMINISTRADOR');
        insert usrAdmn;
        final User tstUsr = [SELECT Id FROM User Where Name = 'PruebaAdm'];
        Test.startTest();
        final Boolean testBool = MX_BPP_RelatedLeads_Crtl.buttonMenu(tstUsr.Id);
        Test.stopTest();
        System.assert(testBool, 'El perfil no coincide');
    }

    /*Ejecuta para cubrir constructor*/
    @isTest
    private static void testConstructor() {
        final MX_BPP_RelatedLeads_Crtl instanceC = new MX_BPP_RelatedLeads_Crtl();
        System.assertNotEquals(instanceC, null, 'Test contructor');
    }
}