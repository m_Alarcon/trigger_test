/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_CatalogoMinutas_Service_Helper_Test
* @Author   	Héctor Saldaña | hectorisrael.saldana.contractor@bbva.com
* @Date     	Created: 2020-03-08
* @Description 	Test Class for MX_BPP_CatalogoMinutas_Service_Helper
* @Changes
*
*/
@isTest
public class MX_BPP_CatalogoMinutas_Service_HelperTst {
    
    @testSetup
	static void setup() {
        final List<MX_BPP_CatalogoMinutas__c> records = new List<MX_BPP_CatalogoMinutas__c>();
        for(Integer i=1; i<=12; i++) {
            final MX_BPP_CatalogoMinutas__c catalogoRecord = new MX_BPP_CatalogoMinutas__c();
            catalogoRecord.MX_Acuerdos__c = 0;
            catalogoRecord.MX_Asunto__c = 'Prueba Catalogo';
            catalogoRecord.Name = 'Test' + i;
            catalogoRecord.MX_TipoVisita__c = 'Test' + i;
            catalogoRecord.MX_Saludo__c = 'Estimado {NombreCliente}';
            catalogoRecord.MX_Contenido__c = 'Contenido';
            catalogoRecord.MX_Despedida__c = 'Saludos Cordiales';
            catalogoRecord.MX_Firma__c = 'Firma Test';
            catalogoRecord.MX_ImageHeader__c = 'Test_Image';
            catalogoRecord.MX_CuerpoCorreo__c = 'Contenido Correo para {NombreCliente}';
            records.add(catalogoRecord);
        }
        insert records;        
    }
    
    /**
    * @Description 	Test Method for validateOpTagSyntax (validates dynamic tags)
    * @Return 		NA
    **/
	@isTest
    static void validateOpTagSyntaxTest() {
        String error;
        final List<MX_BPP_CatalogoMinutas__c> lstRecords = [SELECT Id, MX_Opcionales__c, MX_HasText__c, MX_HasCheckbox__c, MX_HasPicklist__c, MX_Contenido__c FROM MX_BPP_CatalogoMinutas__c LIMIT 20];
        lstRecords[0].MX_Opcionales__c = '{OpcionalTexto1}Test{OpcionalTexto2}';
        lstRecords[1].MX_Opcionales__c = '{OpcionalText1}Test{OpcionalText1}';
        lstRecords[1].MX_Contenido__c = '{OpcionalText1}';
        lstRecords[2].MX_Opcionales__c = '{OpcionalTexto1}{OpcionalTexto1}';
        lstRecords[3].MX_Opcionales__c = '{OpcionalTexto}Test{OpcionalTexto}';
        lstRecords[4].MX_Opcionales__c = '{OpcionalTexto1}Test{OpcionalTexto1';
        lstRecords[5].MX_Opcionales__c = '{OpcionalText1}Test{OpcionalText1}';
        lstRecords[6].MX_Opcionales__c = '{OpcionalTexto1}{OpcionalTexto1}';
        lstRecords[7].MX_Opcionales__c = '{Seleccion1}Test{Seleccion1}';
        lstRecords[7].MX_Contenido__c = '{Seleccion1}';
        lstRecords[8].MX_Opcionales__c = '{Seleccion1}Test{Opcion1}Opc{Opcion2}{Seleccion1}';
        lstRecords[8].MX_Contenido__c = '{Seleccion1}';
        lstRecords[9].MX_Opcionales__c = '{Seleccion1}Test{Opcion1}{Opcion1}{Seleccion1}';
        lstRecords[10].MX_Opcionales__c = '{Seleccion1}Test{Opcion}Opc{Opcion}{Seleccion1}';
        lstRecords[11].MX_Opcionales__c = '{Seleccion1}Test{Test1}Caso1{Test1}{Seleccion1}';
        lstRecords[11].MX_Contenido__c = '{Seleccion1}';
        Test.startTest();
        MX_BPP_CatalogoMinutas_Service_Helper.validateOpTagSyntax(lstRecords);
        try {
            update lstRecords;
        } catch (Exception e) {
            error = e.getTypeName();
        }
        Test.stopTest();
        System.assert(String.isNotBlank(error), 'Error on validateOpTagSyntax');
    }
    
    /**
    * @Description 	Test Method for validateStTagSyntax validates static inner tags
    * @Return 		NA
    **/
	@isTest
    static void validateStTagSyntaxTest() {
        String error;
        final List<MX_BPP_CatalogoMinutas__c> lstRecords = [SELECT Id, MX_Opcionales__c, MX_Contenido__c, MX_Saludo__c, MX_Despedida__c, MX_Firma__c, MX_CuerpoCorreo__c FROM MX_BPP_CatalogoMinutas__c LIMIT 20];
        lstRecords[0].MX_Contenido__c = 'Contenido y {AcuerdosSinFecha}';
        Test.startTest();
        MX_BPP_CatalogoMinutas_Service_Helper.validateStTagSyntax(lstRecords);
        try {
            update lstRecords;
        } catch (Exception e) {
            error = e.getTypeName();
        }
        Test.stopTest();
        System.assert(String.isNotBlank(error), 'Error on validateStTagSyntax');
    }

}