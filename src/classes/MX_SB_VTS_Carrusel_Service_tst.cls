/*
  @File Name          : MX_SB_VTS_Carrusel_Service_tst.cls
  @Description        : Clase de Prueba: Componente Carrusel
  @Author             : jesusalexandro.corzo.contractor@bbva.com
  @Group              : 
  @Last Modified By   : jesusalexandro.corzo.contractor@bbva.com
  @Last Modified On   : 07/8/2020 17:06:00
  @Modification Log   : 
  Ver               Date                Author      		                    	Modification
  1.0           	07/8/2020       	jesusalexandro.corzo.contractor@bbva.com    Initial Version
*/
@isTest
public class MX_SB_VTS_Carrusel_Service_tst {
	/* Variables de Apoyo: txtModOppo  */
    static String txtModOppo = 'Opportunity';
    /* Variables de Apoyo: txtModLead  */
    static String txtModLead = 'Lead';
    /* Variables de Apoyo: txtMessage  */
    static String txtMessage = 'Se obtuvieron los datos con Exito!';
    /* Variables de Apoyo: txtMessageSave  */
    static String txtMessageSave = 'Se guardaron los datos con Exito!';
    /* Variables de Apoyo: txtSEtapa  */
    static String txtSEtapa = 'Contacto';
    /* Variables de Apoyo: txtSName  */
    static String txtSName = 'Contacto';
    /* Variables de Apoyo: txtNombreProducto  */
    static String txtNombreProducto = 'Seguro Estudia';
    
    /**
     * @description: Preparación de valores para clase de prueba.
     * @author: Alexandro Corzo
     */   
	@testSetup
    static void makeData() {
        User oUser = null;
    	oUser = MX_WB_TestData_cls.crearUsuario('TestUser', System.label.MX_SB_VTS_ProfileAdmin);
        Insert oUser;
        System.runAs(oUser) {            
            final Lead oLeadCarrusel = MX_WB_TestData_cls.createLead('Lead Service');
            oLeadCarrusel.FirstName = 'Usuario';
            oLeadCarrusel.LastName = 'Service';
            oLeadCarrusel.LeadSource = 'Call me back';
            insert oLeadCarrusel;
            final Account oAccCarrusel = MX_WB_TestData_cls.crearCuenta('Carrusel', 'PersonAccount');
            oAccCarrusel.PersonEmail = 'test_service@test.com';
            insert oAccCarrusel;
            final Opportunity oOppCarrusel = MX_WB_TestData_cls.crearOportunidad('Opportunity Service', oAccCarrusel.Id, oUser.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            oOppCarrusel.LeadSource = 'Call me back';
            oOppCarrusel.Producto__c = 'Seguro Estudia';
            insert oOppCarrusel;
        }
    }

    /**
     * @description: Realiza prueba de almacenamiento de datos: Opportunity
     * @author: Alexandro Corzo
     */ 
    @isTest
    static void testServiceDataComponet() {
        final Opportunity oDataTestOppo = [SELECT Id FROM Opportunity WHERE Name = 'Opportunity Service'];
        Map<String, Object> oTestComponent = null;
        Boolean isSuccess = false;
        oTestComponent = new Map<String, Object>();
        oTestComponent.put('sModule', txtModOppo);
        oTestComponent.put('sId', oDataTestOppo.Id);
        oTestComponent.put('sSubEtapa', txtSEtapa);
        oTestComponent.put('sStageName', txtSName);
        oTestComponent.put('sProducto', txtNombreProducto);
        isSuccess = MX_SB_VTS_Carrusel_Service.setDataComponentServ(oTestComponent);
        System.assert(isSuccess, txtMessageSave);
    }
    
    /**
     * @description: Realiza prueba de recuperacion de datos: Lead
     * @author: Alexandro Corzo
     */   
    @isTest
    static void testServiceComponentLead() {
        final Lead oDataTest = [SELECT Id FROM Lead WHERE LastName = 'Service'];
        final Map<String, List<Object>> oDataTestLead = MX_SB_VTS_Carrusel_Service.getDataComponentServ(txtModLead, oDataTest.Id);
		System.assert(!oDataTestLead.isEmpty(), txtMessage);
    }

    /**
     * @description: Obtienen los datos para el campo: SubEtapa
     * @author: Alexandro Corzo
     */ 
    @isTest
    static void testServiceSubEtapaOppo() {
        final Map<String, List<Object>> oDataTestSubEtapa = MX_SB_VTS_Carrusel_Service.obtDataSubEtapaOppoServ();
        System.assert(!oDataTestSubEtapa.isEmpty(), txtMessage);
    }
    
    /**
     * @description: Realiza prueba de recuperacion de datos: Opportunity
     * @author: Alexandro Corzo
     */ 
    @isTest 
    static void testServiceComponentOppo() {
        final Opportunity oDataTest = [SELECT Id FROM Opportunity WHERE Name = 'Opportunity Service'];
        final Map<String, List<Object>> oDataTestOppo = MX_SB_VTS_Carrusel_Service.getDataComponentServ(txtModOppo, oDataTest.Id);
		System.assert(!oDataTestOppo.isEmpty(), txtMessage);
    }
}