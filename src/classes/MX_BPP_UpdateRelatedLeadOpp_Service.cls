/**
 * @File Name          : MX_BPP_UpdateRelatedLeadOpp_Service.cls
 * @Description        : Actualiza las Opportunidades y Leads de CampaingMembers
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 24/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      24/06/2020           Jair Ignacio Gonzalez G.   Initial Version
**/
public without sharing class MX_BPP_UpdateRelatedLeadOpp_Service {

    /**
     * MX_BPP_UpdateRelatedLeadOpp_Service constructor
     */
    private MX_BPP_UpdateRelatedLeadOpp_Service() {
    }

    /**
    * @Method updateOppFromCampMbrs
    * @param Map<Id,CampaignMember> triggerNewMap, Map<Id,CampaignMember> triggerOldMap
    * @Description Actualiza Opportunidades abiertas de un CampaignMembers
    **/
    public static void updateOppFromCampMbrs(Map<Id,CampaignMember> triggerNewMap, Map<Id,CampaignMember> triggerOldMap) {
        final Id rtBPYP = Schema.SObjectType.CampaignMember.getRecordTypeInfosByDeveloperName().get('MX_BPP_CampaignMember').getRecordTypeId();
        final Set<Id> setValidsIds = validsCampaignMembers(triggerNewMap, triggerOldMap, rtBPYP);
        final List<CampaignMember> lsOppIds = MX_RTL_CampaignMember_Selector.getLeadOpportunityInfoByIds('Id, LeadId, Lead.IsConverted, MX_SuccesAmount__c, Lead.ConvertedOpportunityId', new List<Id>(setValidsIds));
        final List<Opportunity> lsOpp = prepareOpp(lsOppIds);
        if (!lsOpp.isEmpty()) {
            MX_RTL_Opportunity_Selector.updateResult(lsOpp, false);
        }
    }

    /**
    * @Method validsCampaignMembers
    * @param Map<Id,CampaignMember> triggerNewMap, Map<Id,CampaignMember> triggerOldMap, Id rtBPYP
    * @Description Obtiene los CampaignMembers validos para actualizar sus oportunidades
    * @return Set<Id>
    **/
    public static Set<Id> validsCampaignMembers(Map<Id,CampaignMember> triggerNewMap, Map<Id,CampaignMember> triggerOldMap, Id rtBPYP) {
        final Set<Id> returnIds = new Set<Id>();
        final Set<Id> allCampMbrs = triggerNewMap.keySet();
        for (Id oIdCampMbrs : allCampMbrs) {
            if (triggerNewMap.get(oIdCampMbrs).RecordTypeId == rtBPYP
                && triggerNewMap.get(oIdCampMbrs).LeadSource == System.Label.MX_BPyP_CampaignMember_LeadSource
                && triggerNewMap.get(oIdCampMbrs).Status == 'Con Exito'
                && triggerNewMap.get(oIdCampMbrs).Status != triggerOldMap.get(oIdCampMbrs).Status) {
                returnIds.add(oIdCampMbrs);
            }
        }
        return returnIds;
    }

    /**
    * @Method prepareOpp
    * @param List<CampaignMember> lsOppIds
    * @Description Actualiza los valores de la oportunidad
    * @return List<Opportunity> returnOpp
    **/
    public static List<Opportunity> prepareOpp(List<CampaignMember> lsOppIds) {
        final List<Opportunity> returnOpp = new List<Opportunity>();
        final Map<Id,Opportunity> mapOpp = selectOpps(lsOppIds); //NOSONAR
        for (CampaignMember oCampMbr : lsOppIds) {
            if (oCampMbr.Lead.ConvertedOpportunityId != null) {
                final Opportunity oOpp = mapOpp.get(oCampMbr.Lead.ConvertedOpportunityId);
                oOpp.Id = oCampMbr.Lead.ConvertedOpportunityId;
                oOpp.StageName = 'Cerrada ganada';
                if (oCampMbr.MX_SuccesAmount__c != null) {
                    oOpp.op_amountPivote_dv__c=oCampMbr.MX_SuccesAmount__c;
                    oOpp.Amount=oCampMbr.MX_SuccesAmount__c;
                }
                oOpp.Description = oOpp.Description + ' ' + System.Label.MX_BPyP_UpdOpp_FromBulk;
                returnOpp.add(oOpp);
            }
        }
        return returnOpp;
    }

    /**
    * @Method selectOpps
    * @param List<CampaignMember> lsOppIds
    * @Description retorna un map con las Oportunidades
    * @return Map<Id,Opportunity> mapOpp
    **/
    public static Map<Id,Opportunity> selectOpps(List<CampaignMember> lsOppIds) {
        final Set<Id> idsOpps = new Set<Id>();
        for (CampaignMember oCampMbr : lsOppIds) {
            if (oCampMbr.Lead.ConvertedOpportunityId != null) {
                idsOpps.add(oCampMbr.Lead.ConvertedOpportunityId);
            }
        }
        return new Map<Id,Opportunity>(MX_RTL_Opportunity_Selector.selectBppSObById(idsOpps));
    }

}