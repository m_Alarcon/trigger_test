/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_MinutaWrapper
* @Author   	Héctor Saldaña | hectorisrael.saldana.contractor@bbva.com
* @Date     	Created: 2021-02-05
* @Description 	BPyP Minuta Wrapper
* @Changes		Date		|		Author		|		Description
 * 			2021-19-02			Héctor Saldaña	  New Inner Classes created (wrapperComponentes, componenteText
 * 												  componentePicklist, componentePicklistValues, componenteSwitch,
 * 												  tagsEstaticos)
*
*/
public without sharing class MX_BPP_MinutaWrapper {

	/*Accesor property to instantiate a configParams Object*/
	public configParams configParamsPty {get;set;}
	/*Accesor property to instantiate a catalogoTemplate Object*/
	public catalogoTemplate catalogoPty {get;set;}
	/*Accesor property to instantiate a wrapperComponentes Object*/
	public wrapperComponentes contenedorCmps {get;set;}

	/*Constructor Method*/
	public MX_BPP_MinutaWrapper() {
		this.configParamsPty = new configParams();
		this.catalogoPty = new catalogoTemplate();
		this.contenedorCmps = new wrapperComponentes();
	}

	/**
    * @Description Inner Class Helper to initiate the template configuration for each of the templates
    * @author Héctor Saldaña
    * @date 2021-22-01
    **/
	public class configParams {
		/*Accessor property for the current Visit Id*/
		public String currentId {get; set;}
		/*Accessor property for custom settings configuration*/
		public String sMyCS {get; set;}
		/*Accessor property for documents attached to the template*/
		public String sDocuments {get; set;}
		/*Accessor property for the current Visit record once got the current Id property*/
		public dwp_kitv__Visit__c visitObj {get; set;}

		/**Constructor Method*/
		public configParams() {
			this.currentId = MX_BPP_Minuta_Utils.getParameter('Id');
			this.sMyCS = MX_BPP_Minuta_Utils.getParameter('myCs');
			this.sDocuments = MX_BPP_Minuta_Utils.getParameter('documents');
		}
	}

	/**
    * @Description Inner Class to initiate a Configuration Object with helper properties to load the Templates
    * @author Héctor Saldaña
    * @date 2021-02-02
    **/
	public class catalogoTemplate {
		/*Accesor property for 'Numero de Opcionales' in the related 'Catalogo'*/
		public Integer sNumeroOpcionales {get;set;}
		/*Accesor property to the current Visualforce Page Name*/
		public String sVfName {get;set;}
		/*Accessor property for the related Catalogo*/
		public String sIdCatalogo {get; set;}
		/*Acccesor property to indicate if Ficha Producto component needs to be dsiplayed or not*/
		public Boolean showFichaProducto {get;set;}

		/*Constructor method*/
		public catalogoTemplate() {
			this.sNumeroOpcionales = 0;
			this.showFichaProducto = false;
		}
	}

	/**
    * @Description Inner Class to initiate a components wrapper object within it
    * @author Héctor Saldaña
    * @date 2021-19-02
    **/
	public class wrapperComponentes {
        /* Accesor property for text component */
		public List<componenteText> cmpText {get;set;}
        /* Accesor property for picklist component */
		public List<componentePicklist> cmpPicklist {get;set;}
        /* Accesor property for switch component */
		public List<componenteSwitch> cmpSwitch {get;set;}
	}

	/**
    * @Description Inner Class to initiate a Text Component Object
    * @author Héctor Saldaña
    * @date 2021-19-02
    **/
	public class componenteText {
        /* Accesor name property for text component */
		public String name {get;set;}
        /* Accesor label property for text component */
		public String label {get;set;}
        /* Accesor tag property for text component */
		public String tagInicio {get; set;}
		/*Accesor property if componente is required to display*/
		public Boolean cmpRequired {get;set;}
	}

	/**
    * @Description Inner Class to initiate a Picklist Component Object
    * @author Héctor Saldaña
    * @date 2021-19-02
    **/
	public class componentePicklist {
        /* Accesor name property for picklist component */
		public String name {get;set;}
        /* Accesor label property for picklist component */
		public String label {get;set;}
        /* Accesor values property list for picklist component */
		public List<componentePicklistValues> options {get;set;}
        /* Accesor tag property for picklist component */
		public String tagInicio {get;set;}
		/*Accesor property if componente is required to display*/
		public Boolean cmpRequired {get;set;}
	}

	/**
    * @Description Inner Class to initiate Picklist values for a Picklist component Object
    * @author Héctor Saldaña
    * @date 2021-19-02
    **/
	public class componentePicklistValues {
        /* Accesor label property for picklist value */
		public String label {get;set;}
        /* Accesor value property for picklist value */
		public String value {get;set;}
	}

	/**
    * @Description Inner Class to initiate a Input component of type checkbox
    * @author Héctor Saldaña
    * @date 2021-19-02
    **/
	public class componenteSwitch {
        /* Accesor name property for switch component */
		public String name {get;set;}
        /* Accesor label property for switch component */
		public String label {get;set;}
        /* Accesor tag property for switch component */
		public String tagInicio {get;set;}
		/*Accesor property if componente is required to display*/
		public Boolean cmpRequired {get;set;}
	}

	/**
    * @Description Inner Class to initiate an Object to store Static tags
    * @author Héctor Saldaña
    * @date 2021-19-02
    **/
	public class tagsEstaticos {
        /* Accesor tag property for static tag */
		public String tagInicio {get;set;}
        /* Accesor object property for static tag */
		public String objeto {get; set;}
        /* Accesor field property for static tag */
		public String campo {get;set;}
        /* Accesor related property for static tag */
		public Boolean relacionado {get;set;}
        /* Accesor isDO property for static tag */
        public Boolean isDO {get;set;}
        /* Accesor content property for static tag */
        public String contenido {get;set;}
	}
    
}