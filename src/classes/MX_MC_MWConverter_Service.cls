/**
 * @File Name           : MX_MC_MWConverter_Service.cls
 * @Description         :
 * @Author              : eduardo.barrera3@bbva.com
 * @Group               :
 * @Last Modified By    : eduardo.barrera3@bbva.com
 * @Last Modified On    : 04/27/2020, 14:21:20 PM
 * @Modification Log    :
 * =============================================================================
 * Ver          Date                     Author                     Modification
 * =============================================================================
 * 1.0      04/07/2020, 12:48:35 PM  eduardo.barrera3@bbva.com      Initial Version
 * 1.2      04/27/2020, 14:21:20 PM  eduardo.barrera3@bbva.com      Method converstion for input and output service fields.
 **/
global without sharing class MX_MC_MWConverter_Service extends mycn.MC_VirtualServiceAdapter {//NOSONAR

    /**
    * @Description MSG_THR_ID property
    **/
    private static final String MSG_THR_ID = 'messageThreadId';

    /**
    * @Description IS_DRAFT property
    **/
    private static final String IS_DRAFT = 'isDraft';

    /**
    * @Description Returns a generic Object based on method call from Class in MC component
    * @author eduardo.barrera3@bbva.com | 04/27/2020
    * @param parameters
    * @return Map<String, Object>
    **/
    global override Map<String, Object> convertMap (Map<String, Object> parameters) {

        final Map<String, Object> inputMX = new Map<String, Object>();

        if (parameters.containsKey(MSG_THR_ID)) {

            final Case caseConversation = mycn.MC_ConversationsDatabase.getCaseByThreadId(String.valueOf(parameters.get(MSG_THR_ID)));
            Account clientAcc;
            String customerCode = '';
            String valueToSend  = '';
            if (caseConversation.AccountId != null) {
                clientAcc = mycn.MC_Account_Selector.getClientById(caseConversation.AccountId);
                customerCode = MX_MC_Utils.getCustomerCode(clientAcc);
            }

            inputMX.put('message',parameters.get('content'));
            inputMX.put(IS_DRAFT,String.valueOf(parameters.get(IS_DRAFT)));
            inputMX.put(MSG_THR_ID,parameters.get(MSG_THR_ID));

            if (customerCode == null || customerCode == '' ) {
                valueToSend = customerCode;
            } else {
                valueToSend =  MX_MC_valueCipher_Service.callValueCipher(customerCode,true);
            }
            inputMX.put('filter','customerId=' + valueToSend);

        } else {

            final User currentUser = mycn.MC_UserSelector.getUserById(UserInfo.getUserId());
            final String managerId = mycn.MC_UserUtilities.getManagerCode(currentUser);
            String valueToSend = '';

            if (managerId == null || managerId == '') {
                valueToSend = managerId;
            } else {
                valueToSend = MX_MC_valueCipher_Service.callValueCipher(managerId,true);
            }
            inputMX.put('filter', 'customerId=' + valueToSend);
            inputMX.put('receiver', MX_MC_valueCipher_Service.callValueCipher(String.valueOf(parameters.get('customerId')),true));
            inputMX.put('subject', parameters.get('subject'));
            inputMX.put(IS_DRAFT, String.valueOf(parameters.get(IS_DRAFT)));
            inputMX.put('message', parameters.get('content'));
            inputMX.put('messageIsDraft', String.valueOf(parameters.get(IS_DRAFT)));
        }

        return inputMX;
    }

    /**
    * @Description Returns a generic Object
    * @author eduardo.barrera3@bbva.com | 04/07/2020
    * @return Object
    **/
    global override Object convert(String body, String method, Map<String, Object> parameters) {

        Object mxObject = null;

        if ('postMessageThread'.equals(method)) {
            mxObject = this.mxToStandardCreatedMessage(body, parameters);
        } else if ('postThread'.equals(method)) {
            mxObject = this.mxToStandardCreatedConversation(body, parameters);
        }

        return mxObject;
    }

    /**
    * @Description Returns a MC_MessageThreadWrapper.Message list
    * @author eduardo.barrera3@bbva.com | 04/27/2020
    * @return Object
    **/
    public Object mxToStandardCreatedMessage(String body, Map<String, Object> parameters) {

        String customerCode;
        final mycn.MC_MessageThreadWrapper.Message standardMessage = new mycn.MC_MessageThreadWrapper.Message();
        standardMessage.id = MX_MC_Utils.getHeaderLocation(String.valueOf(parameters.get('location')));
        standardMessage.messageThreadId = parameters.get(MSG_THR_ID).toString();
        standardMessage.isDraft = Boolean.valueOf(parameters.get(IS_DRAFT).toString());

        final Case caseConversation = mycn.MC_ConversationsDatabase.getCaseByThreadId(standardMessage.messageThreadId);

        if (caseConversation.AccountId == null) {
           customerCode = '';
        } else {
        	customerCode = MX_MC_Utils.getCustomerCode(mycn.MC_Account_Selector.getClientById(caseConversation.AccountId));
        }

        final mycn.MC_MessageThreadWrapper.Receiver mxReceiver = new mycn.MC_MessageThreadWrapper.Receiver();
        mxReceiver.id = customerCode;
        standardMessage.receiver = mxReceiver;

        MX_MC_pushNotification_Service.callPushNotification(customerCode);
        return standardMessage;
    }

    /**
    * @Description Returns a MC_MessageThreadWrapper.Message list
    * @author eduardo.barrera3@bbva.com | 04/27/2020
    * @return Object
    **/
    public Object mxToStandardCreatedConversation(String body, Map<String, Object> parameters) {

        final mycn.MC_MessageThreadWrapper.MessageThread stdConversation = new mycn.MC_MessageThreadWrapper.MessageThread();
        stdConversation.id = MX_MC_Utils.getHeaderLocation(String.valueOf(parameters.get('location')));
        stdConversation.subject = parameters.get('subject').toString();
        final mycn.MC_MessageThreadWrapper.Customer customer = new  mycn.MC_MessageThreadWrapper.Customer();
        customer.id = parameters.get('customerId').toString();
        stdConversation.customer = customer;
        stdConversation.isDraft = Boolean.valueOf(parameters.get(IS_DRAFT).toString());

        MX_MC_pushNotification_Service.callPushNotification(customer.id);
        return stdConversation;
    }
}