@IsTest
/**
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_MapasPageReference_Test
* Autor Daniel Perez Lopez
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Procedimiento de la clase MX_SB_MLT_MapasPageReference_Controller

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           10/12/2019      Daniel Lopez                         Creación
* 1.1           12/03/2020      Angel Nava                           migración campos contract
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_MapasPageReference_Test {
    @TestSetup
    static void creaDatos() {
           
            final Account gruaAc =new Account(name='Gruatest',recuperacion__c=false,Tipo_Persona__c='Física');
            insert gruaAc;
            final Account ambulanciaAc =new Account(name='Ambulanciatest',recuperacion__c=false,Tipo_Persona__c='Física');
            insert ambulanciaAc;
            final Account acparamedico =new Account(name='Paramedicotest',recuperacion__c=false,Tipo_Persona__c='Física');
            insert acparamedico;
            final Contract contrato = new Contract(AccountId=gruaAc.id,MX_SB_SAC_NumeroPoliza__c='A4234', name='testcontract');
            insert contrato;

            final Siniestro__c sinie= new Siniestro__c();
            sinie.Folio__c='sinitest';
            sinie.MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro';
            sinie.MX_SB_SAC_Contrato__c = contrato.Id;
            insert sinie;

            final siniestro__c sinie2= new Siniestro__c();
            sinie2.Folio__c='sinitest2';
            sinie2.Poliza__c='folio2';
            sinie2.MX_SB_MLT_Certificado_Inciso__c='1';
            sinie2.MX_SB_MLT_Placas__c='ldny123';
            sinie2.MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro';
            sinie2.MX_SB_MLT_TipoRobo__c='Robo Total';
            sinie2.MX_SB_SAC_Contrato__c = contrato.Id;
            insert sinie2;
            Final String rTCase = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_TomaReporte').getRecordTypeId();
             Final String nameProfile = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User objUsrTst = MX_WB_TestData_cls.crearUsuario('PruebaAdminTst', nameProfile);  
        insert objUsrTst;
        System.runAs(objUsrTst) {
        	
             final Case objCaseTst = new Case(Nombre_Proveedor__c=gruaAc.name,MX_SB_MLT_Siniestro__c =sinie2.id,RecordTypeId = rTCase);
            insert objCaseTst;
        }
    }
    /*
    * @description procedimiento que prueba MX_SB_MLT_MapasPageReference_Controller
    * @param  void
    * @return String 
    */
    @IsTest public static void metodo1() {
        final Siniestro__c sinie= [SELECT id ,Folio__c FROM siniestro__c WHERE Folio__c='sinitest2' limit 1];
        
        final PageReference pageRef = Page.CommunitiesSelfRegConfirm;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', sinie.id);
        final MX_SB_MLT_MapasPageReference_Controller clse = new MX_SB_MLT_MapasPageReference_Controller();
        Test.startTest();
            clse.sin=sinie;
            clse.sin2=sinie;
            clse.sinRobo=sinie;
            clse.sinRobo2=sinie;
            clse.siniestroId='sinie';
            clse.nuevo=true;
            clse.updateSin();
            clse.save();
            clse.saveRobo();
        	clse.saveDest();
        	system.assertEquals('sinie',clse.siniestroId,'Exitoso');
        Test.stopTest();
    }
}