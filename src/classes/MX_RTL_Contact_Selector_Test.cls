@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_RTL_Contact_Selector_Test
* Autor: Juan Carlos Benitez Herrera
* Proyecto: SB Presuscritos - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_RTL_Contact_Selector

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         10/07/2020    Juan Carlos Benitez Herrera              Creación
* -----------------------------------------------------------------------------------------------
*/
public class MX_RTL_Contact_Selector_Test {
    /*VAR CONDITION AccountId*/
    final static string CONDITION ='AccountId';
    
        /** Nombre de usuario Test */
    final static String NAME = 'Jessica';
    /** Apellido Paterno */
    Final static STRING LNAME= 'Duarte';
    
    @TestSetup
    static void createData() {
        Final String profName = [SELECT Name from profile where name = 'Asesores Presuscritos' limit 1].Name;
        final User usuarioT = MX_WB_TestData_cls.crearUsuario(NAME, profName);
        insert usuarioT;
        System.runAs(usuarioT) {
            final String pAcT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            final Account cuentTst = new Account(FirstName=NAME, LastName=LNAME, Tipo_Persona__c='Física', RecordTypeId = pAcT);
            insert cuentTst;
        }
    }

	@isTest
    static void testgetContactReus() {
		Final String recId =[SELECT PersonContactId from Account where FirstName=:NAME].PersonContactId;
        Final set<Id> ids = new set<Id>{recId};
        final Map<String,String> mapa = new Map<String,String>();
       	mapa.put('fields', ' Id, Name, Birthdate , Email, MX_RTL_CURP__c, MailingStreet, MailingPostalCode,MailingCity,MailingState,MX_WB_ph_Telefono1__c,MX_WB_ph_Telefono2__c ');
        test.startTest();
        	MX_RTL_Contact_Selector.getContactReusable(ids, CONDITION, mapa);
            system.assert(true,'Se ha encontrado el contacto');
        test.stopTest();
    }
    @isTest
    static void testupsrtContact() {
        Final Id paccId =[SELECT PersonContactId from Account where FirstName=:NAME].PersonContactId;
        Final Contact cntc=[SELECT Id FROM Contact where Id=:paccId];
        cntc.Apellido_materno__c='Nava';
        test.startTest();
        	MX_RTL_Contact_Selector.upsrtContact(cntc);
            system.assert(true,'Se ha actualizado el contacto correctamente');
        test.stopTest();
    }
}