/**
* @File Name          : MX_RTL_Contact_Selector.cls
* @Description        :
* @Author             : Juan Carlos Benitez
* @Group              :
* @Last Modified By   : Juan Carlos Benitez
* @Last Modified On   : 6/26/2020, 09:20:03 PM
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    6/26/2020   Juan Carlos Benitez          Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_RTL_Contact_Selector {
    /** List of records to be returned */
    final static List<SObject> CONTATEARCH = new List<SObject>();
    /**
    * @description
    * @author Juan Carlos Benitez | 6/24/2020
    * @param Set<Id> ids
    * @param String Condition
    * @param Map<String, String> mapa
    * @return CONTATEARCH
    **/
    public static List<Contact> getContactReusable(Set<Id> ids, String condition, Map<String,String> mapa) {
        switch on condition {
            when 'AccountId' {
                CONTATEARCH.addAll(Database.query(String.escapeSingleQuotes('SELECT ' +mapa.get('fields')+ ' FROM Contact where AccountId=:ids')));   
            }
        }
		return CONTATEARCH;
    }
     /**
    * @description
    * @author Juan Carlos Benitez | 6/24/2020
    * @param Contact cntc
    **/
    public static void upsrtContact(Contact cntc) {
        upsert cntc;
    }
}