/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Minuta_BienvenidaDO_Ctrl_Test
* @Author   	Héctor Israel Saldaña Pérez | hectorisrael.saldana.contractor@bbva.com
* @Date     	Created: 2021-01-13
* @Description 	Test Class for BPyP Minuta BienvenidaDO Controller
* @Changes
* 2021-22-02	Edmundo Zacarias		New test method added testSendEmailError
 * 2021-08-03   Héctor Saldaña          New fields added in createCatalogoMinuta() as per required
*/
@isTest
public class MX_BPP_Minuta_BienvenidaDO_Ctrl_Test {

    @testSetup
    static void setup() {        
        User testUser = new User();
        testUser = UtilitysDataTest_tst.crearUsuario('UserTest', 'BPyP Estandar', 'BPYP BANQUERO BANCA PERISUR');
        testUser.Title = 'Privado';
        insert testUser;
        
        User testUserAdmin = new User();
        testUserAdmin = UtilitysDataTest_tst.crearUsuario('PruebaAdm', Label.MX_PERFIL_SystemAdministrator, 'BBVA ADMINISTRADOR');
        insert testUserAdmin;
        
        System.runAs(testUserAdmin) {
        	final Account testAcc = new Account();
            testAcc.LastName = 'testAcc';
            testAcc.FirstName = 'testAcc';
            testAcc.OwnerId = testUserAdmin.Id;
            testAcc.No_de_cliente__c = '1234DJ9';
            insert testAcc;
            
            final dwp_kitv__Visit__c testVisit = new dwp_kitv__Visit__c();
            testVisit.dwp_kitv__visit_start_date__c = Date.today()+4;
            testVisit.dwp_kitv__account_id__c = testAcc.Id;
            testVisit.dwp_kitv__visit_duration_number__c = '15';
            testVisit.dwp_kitv__visit_status_type__c = '01';
            insert testVisit;

            final dwp_kitv__Template_for_type_of_visit_cs__c myCS = new dwp_kitv__Template_for_type_of_visit_cs__c();
            myCS.Name = 'Test';
            myCS.dwp_kitv__Attach_file__c = true;
            myCS.dwp_kitv__Disable_send_field_in_BBVA_team__c = true;
            myCS.dwp_kitv__Disable_send_field_in_contacts__c = true;
            myCS.dwp_kitv__Email_Template_Name__c = 'Test Template';
            myCs.dwp_kitv__Minimum_Number_of_Agreement__c = 0;
            myCS.dwp_kitv__Minimum_members_BBVA_team__c = 0;
            myCS.dwp_kitv__Multiple_templates_type__c = false;
            myCS.dwp_kitv__Subject__c = 'Test Subject';
            myCS.dwp_kitv__validation_main_contact__c = false;
            myCS.dwp_kitv__Visualforce_Name__c = 'MX_BPP_Minuta_BienvenidaDO_VF';
            insert myCS;

            final MX_BPP_CatalogoMinutas__c catalogoTest = createCatalogoMinuta('BienvenidaDO', 'BienvenidaDO');
            insert catalogoTest;

            final ContentVersion contentVersion = new ContentVersion(Title = 'PenguinsBIE', PathOnClient = 'PenguinsBIE.pdf', VersionData = Blob.valueOf('Test ContentBIE'), FirstPublishLocationId=testVisit.Id);
            insert contentVersion;
        }
    }
    
    /**
    * @Description 	Test controller constrcutor is built correctly
    * @Param		NA
    * @Return 		Void
    **/
    @isTest
    static void testController() {
        final dwp_kitv__Visit__c testVisit = [SELECT Id, RecordType.Name FROM dwp_kitv__Visit__c WHERE CreatedBy.Name = 'PruebaAdm' LIMIT 1];
        final MX_BPP_CatalogoMinutas__c lclCatalogo = [SELECT Id, Name, MX_Acuerdos__c, MX_Saludo__c, MX_ImageHeader__c FROM MX_BPP_CatalogoMinutas__c WHERE Name= 'BienvenidaDO' LIMIT 1];
        test.startTest();
        final PageReference testPage = Page.MX_BPP_Minuta_BienvenidaDO_VF;
        Test.setCurrentPage(testPage);
        ApexPages.currentPage().getParameters().put('Id', testVisit.Id);
        ApexPages.currentPage().getParameters().put('sCatalogoId', lclCatalogo.Id);
        ApexPages.currentPage().getParameters().put('textArray', '[]');
        ApexPages.currentPage().getParameters().put('switchArray', '[]');
        ApexPages.currentPage().getParameters().put('seleccionArray', '[]');
        final MX_BPP_Minuta_BienvenidaDO_Ctrl testCtrl = new MX_BPP_Minuta_BienvenidaDO_Ctrl();
        System.assertEquals(testVisit.Id, testCtrl.visitObj.Id, 'Verficar info');
        test.stopTest();
    }
    
    /**
    * @Description 	Test email was succesfully sent
    * @Param		NA
    * @Return 		Void
    **/
    @isTest
    static void testSendEmail() {
        
        List<ContentVersion> lstDocuments = new List<ContentVersion>();
        final MX_BPP_CatalogoMinutas__c lclCatalogo = [SELECT Id, Name, MX_Acuerdos__c, MX_Saludo__c, MX_Contenido__c, MX_Despedida__c, MX_Firma__c, MX_ImageHeader__c FROM MX_BPP_CatalogoMinutas__c WHERE Name= 'BienvenidaDO' LIMIT 1];
        final dwp_kitv__Template_for_type_of_visit_cs__c myCS = [Select Id, Name, dwp_kitv__Attach_file__c, dwp_kitv__Disable_send_field_in_BBVA_team__c,
                                                          dwp_kitv__Disable_send_field_in_contacts__c,dwp_kitv__Email_Template_Name__c, dwp_kitv__Minimum_Number_of_Agreement__c,
                                                          dwp_kitv__Minimum_members_BBVA_team__c, dwp_kitv__Multiple_templates_type__c, dwp_kitv__Subject__c, dwp_kitv__validation_main_contact__c,
                                                          dwp_kitv__Visualforce_Name__c FROM dwp_kitv__Template_for_type_of_visit_cs__c WHERE Name = 'Test'];
       
        final dwp_kitv__Visit__c testVisit = [SELECT Id, RecordType.Name, dwp_kitv__visit_status_type__c FROM dwp_kitv__Visit__c WHERE CreatedBy.Name = 'PruebaAdm' LIMIT 1];
        lstDocuments = [SELECT Title, PathOnClient, VersionData, FirstPublishLocationId FROM ContentVersion WHERE FirstPublishLocationId =: testVisit.Id];
        final String result = JSON.serialize(myCS); 
        final String lstDocsStr = JSON.serialize(lstDocuments);
        Test.startTest();
        final PageReference testPage = Page.MX_BPP_Minuta_BienvenidaDO_VF;
        Test.setCurrentPage(testPage);
        ApexPages.currentPage().getParameters().put('Id', testVisit.Id);
        ApexPages.currentPage().getParameters().put('sCatalogoId', lclCatalogo.Id);
        ApexPages.currentPage().getParameters().put('myCS', result );
        ApexPages.currentPage().getParameters().put('documents', lstDocsStr);
        ApexPages.currentPage().getParameters().put('textArray', '[]');
        ApexPages.currentPage().getParameters().put('switchArray', '[]');
        ApexPages.currentPage().getParameters().put('seleccionArray', '[]');
        final MX_BPP_Minuta_BienvenidaDO_Ctrl testCtrl = new MX_BPP_Minuta_BienvenidaDO_Ctrl();
        testCtrl.sendMail();
        System.assert(testCtrl.correoEnviado, 'Envío de correo:');
        test.stopTest();
    }

    /*
     * @Description Helper method which creates records of type 'Catalogo Minutas'
     * @Param String nameCatalogo - Name of the 'Catalagoo'
     * @Param String tipoVisita - Sets the 'Tipo visita' field
     * @return MX_BPP_CatalogoMinutas__c - record of type 'Catalogo Minutas'
     * **/
    private static MX_BPP_CatalogoMinutas__c createCatalogoMinuta(String nameCatalogo, String tipovisita) {

        final MX_BPP_CatalogoMinutas__c catalogoRecord = new MX_BPP_CatalogoMinutas__c();
        catalogoRecord.MX_Acuerdos__c = 1;
        catalogoRecord.MX_Asunto__c = 'Prueba Catalogo';
        catalogoRecord.Name = nameCatalogo;
        catalogoRecord.MX_TipoVisita__c = tipovisita;
        catalogoRecord.MX_Saludo__c = 'Saludo Test';
        catalogoRecord.MX_Contenido__c = 'Contenido Test';
        catalogoRecord.MX_Despedida__c = 'Despedida Test';
        catalogoRecord.MX_Firma__c = 'Firma Test';
        catalogoRecord.MX_ImageHeader__c = 'Imagen_Header';
        catalogoRecord.MX_CuerpoCorreo__c = 'Cuerpo del correo.';

        return catalogoRecord;
    }
    
    /**
    * @Description 	Test email with error
    * @Param		NA
    * @Return 		Void
    **/
    @isTest
    static void testSendEmailError() {
        List<ContentVersion> lstDocuments = new List<ContentVersion>();
        final MX_BPP_CatalogoMinutas__c lstCatalogo = [SELECT Id, Name, MX_Acuerdos__c, MX_Saludo__c, MX_Contenido__c, MX_Despedida__c, MX_Firma__c, MX_ImageHeader__c FROM MX_BPP_CatalogoMinutas__c WHERE Name= 'BienvenidaDO' LIMIT 1];
        final dwp_kitv__Template_for_type_of_visit_cs__c myCS = [SELECT Id, Name, dwp_kitv__Visualforce_Name__c, dwp_kitv__Attach_file__c,
                                                          dwp_kitv__Disable_send_field_in_contacts__c, dwp_kitv__Minimum_Number_of_Agreement__c, dwp_kitv__Email_Template_Name__c,
                                                          dwp_kitv__Multiple_templates_type__c, dwp_kitv__Minimum_members_BBVA_team__c, dwp_kitv__Subject__c, dwp_kitv__validation_main_contact__c,
                                                          dwp_kitv__Disable_send_field_in_BBVA_team__c FROM dwp_kitv__Template_for_type_of_visit_cs__c WHERE Name = 'Test'];
       
        final dwp_kitv__Visit__c tstVisit = [SELECT Id, RecordType.Name, dwp_kitv__visit_status_type__c FROM dwp_kitv__Visit__c WHERE CreatedBy.Name = 'PruebaAdm' LIMIT 1];
        lstDocuments = [SELECT Title, PathOnClient, VersionData, FirstPublishLocationId FROM ContentVersion WHERE FirstPublishLocationId =: tstVisit.Id];
        Test.startTest();
        final PageReference testPage = Page.MX_BPP_Minuta_BienvenidaDO_VF;
        Test.setCurrentPage(testPage);
        ApexPages.currentPage().getParameters().put('Id', tstVisit.Id);
        ApexPages.currentPage().getParameters().put('sCatalogoId', lstCatalogo.Id);
        ApexPages.currentPage().getParameters().put('myCS', JSON.serialize(myCS));
        ApexPages.currentPage().getParameters().put('documents', JSON.serialize(lstDocuments));
        ApexPages.currentPage().getParameters().put('textArray', '[]');
        ApexPages.currentPage().getParameters().put('switchArray', '[]');
        ApexPages.currentPage().getParameters().put('seleccionArray', '[]');
        final MX_BPP_Minuta_BienvenidaDO_Ctrl testInstCtrl = new MX_BPP_Minuta_BienvenidaDO_Ctrl();
        testInstCtrl.vfError = true;
        testInstCtrl.sendMail();
        System.assert(!testInstCtrl.correoEnviado, 'Envío de correo:');
        test.stopTest();
    }
}