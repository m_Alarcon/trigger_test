/**
 * @File Name          : QuotesSelector.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 26/5/2020 15:44:09
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    25/5/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class QuotesSelector {
    /**
    * @description Recupera Quotes de Oportunidades
    * @author Eduardo Hernandez Cuamatzi | 25/5/2020 
    * @param List<Opportunity> lstOpps Lista de Oportunidades
    * @return List<Quote> Lista de Cotizaciones
    **/
    public static List<Quote> selectSObjectsByOpps(List<Opportunity> lstOpps) {
        return [Select Id, Name, Status, MX_SB_VTS_ASO_FolioCot__c, MX_SB_VTS_Email_txt__c, QuoteToPostalCode, QuoteToName,
        QuoteToAddress, QuoteToCity, QuoteToCountry, QuoteToState, MX_SB_VTS_Apellido_Paterno_Contratante__c, MX_SB_VTS_Apellido_Materno_Contratante__c,
        MX_SB_VTS_Numero_de_Poliza__c,MX_SB_VTS_Cupon__c, MX_SB_VTS_Familia_Productos__c,MX_SB_VTS_Familia_Productos__r.Name,MX_SB_VTS_RFC__c,OpportunityId,
        MX_SB_VTS_Colonia__c,TotalPrice from Quote where OpportunityId IN: lstOpps WITH SECURITY_ENFORCED];
    }

    /**
    * @description Recupera Quotes por Id de Quote
    * @author Eduardo Hernandez Cuamatzi | 25/5/2020 
    * @param String QuoteId Id de Quote
    * @return List<Quote> Lista de Cotizaciones
    **/
    public static List<Quote> selectSObjectsById(String idQuote) {
        return [Select Id, Name, Status, MX_SB_VTS_ASO_FolioCot__c, MX_SB_VTS_Email_txt__c, QuoteToPostalCode, QuoteToName,
        QuoteToAddress, QuoteToCity, QuoteToState, MX_SB_VTS_Apellido_Paterno_Contratante__c, MX_SB_VTS_Apellido_Materno_Contratante__c,
        MX_SB_VTS_Numero_de_Poliza__c,MX_SB_VTS_Cupon__c, MX_SB_VTS_Familia_Productos__c,MX_SB_VTS_Familia_Productos__r.Name,MX_SB_VTS_RFC__c, OpportunityId,
        MX_SB_VTS_Colonia__c,AccountId,QuoteToLatitude,QuoteToLongitude,MX_SB_VTS_Nombre_Contrante__c,MX_SB_VTS_Numero_Exterior__c,MX_SB_VTS_Numero_Interior__c,
        QuoteToCountry,TotalPrice,Phone from Quote where Id =: idQuote WITH SECURITY_ENFORCED];
    }
}