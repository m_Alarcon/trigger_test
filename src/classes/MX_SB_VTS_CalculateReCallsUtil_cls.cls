/**
 * @File Name          : MX_SB_VTS_CalculateReCallsUtil_cls.cls
 * @Description        : 
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 17/1/2020 14:25:07
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    26/11/2019   Eduardo Hernández Cuamatzi     Initial Version
 * 1.0.1  26/11/2019   Eduardo Hernández Cuamatzi     Fix request json
**/
public without sharing class MX_SB_VTS_CalculateReCallsUtil_cls extends MX_SB_VTS_LeadMultiCTI_Util { //NOSONAR

    /** variable de status http para servicios de smart*/
    final static List<String> LSTSTATUS = new List<String>{'200','201','203','204','205','206','207','208','226'};
    /** variable auxiliar de evaluación*/
    final static integer DIAHORAS = 24;
     /** variable que contiene el method ue ocupará el servicio*/
    final static String METHOD = 'EntRegistroGestion';
    /**
    * @description recupera angendas
    * @author Eduardo Hernández Cuamatzi | 26/11/2019 
    * @param level7 tipíficacion 7
    * @param recordId Id del registro
    * @param timeNow hora de marcado
    * @return Boolean si existe una tarea de agenda
    **/
    public static Boolean calculateReAgenda(String level7, String recordId, Datetime timeNow) {
        Boolean agendaP = false;
        if(level7.equalsIgnoreCase('Reagenda')) {
            final List<String> idsTask = new List<String>{recordId};
            final List<Task> activys = [Select Id,FechaHoraReagenda__c from Task where Subject = 'Reagenda' AND FechaHoraReagenda__c >: timeNow AND (WhatId IN: idsTask OR WhoId IN: idsTask)];   
            agendaP = activys.isEmpty() ? true : false;
        }
        return agendaP;
    }


    /**
    * @description  evaluar horas de la siguiente accion
    * @author Eduardo Hernández Cuamatzi | 26/11/2019 
    * @param actionNext  acción a recuperar
    * @param motivoRemarcado motivo de remarcado nivel 6
    * @return Integer total de horas a remarcar
    **/
    public static Integer evaluteforNextA(String actionNext, MX_WB_MotivosNoContacto__c motivoRemarcado) {
        Decimal nextTime = 0;
            switch on actionNext {
                when '1'{
                    nextTime = motivoRemarcado.MX_SB_VTS_AccionUno__c;
                }
                when '2' {
                    nextTime = motivoRemarcado.MX_SB_VTS_AccionDos__c;
                }
                when '3' {
                    nextTime = motivoRemarcado.MX_SB_VTS_AccionTres__c;
                }                
            }
        return Integer.valueOf(nextTime);
    }

    /**
    * @description  evalua escenario accion 4
    * @author Eduardo Hernández Cuamatzi | 26/11/2019 
    * @param addTimeG4 tiempo calculado con accion previa
    * @param proveedorG4 proveedor de marcado
    * @param nextActionG4 acción siguiente bucle
    * @param motivoRemarcadoG4 motivo remarcado nivel6
    * @return List<String> lista de valores de remarcado
    **/
    public static List<String> evaluateG4(Datetime addTimeG4, MX_SB_VTS_ProveedoresCTI__c proveedorG4, String nextActionG4, MX_WB_MotivosNoContacto__c motivoRemarcadoG4) {
        String nextActionG4R = '0';
        Integer hoursRecalls = 0;
        switch on nextActionG4 {
            when '1' {
                nextActionG4R = '2';
                hoursRecalls = Integer.valueOf(motivoRemarcadoG4.MX_SB_VTS_AccionUno__c);
            }
            when '2' {
                nextActionG4R = '3';
                hoursRecalls = Integer.valueOf(motivoRemarcadoG4.MX_SB_VTS_AccionDos__c);
            }            
        }
        Datetime calcNewG4Time = addTimeG4.addHours(hoursRecalls);
        if(hoursRecalls>DIAHORAS) {
            calcNewG4Time = Datetime.newInstance(calcNewG4Time.year(), calcNewG4Time.month(), calcNewG4Time.day(), 9, 0, 0);
        }
        return MX_SB_VTS_CalculateReCalls_cls.newScheduldDate(calcNewG4Time, proveedorG4, nextActionG4R, motivoRemarcadoG4, hoursRecalls, true);
    }

    /**
    * @description enviar remarcado a smartCenter
    * @author Eduardo Hernández Cuamatzi | 26/11/2019 
    * @param lstWrapSend lista de valores a enviar
    * @param idsWRecallCTI Ids de registros a enviar
    * @param objectoChoose indica si es Oportunidad o lead
    **/
    public static void sendSmart(List<WrapperRecallCTI> lstWrapSend,  Map<String,String> idsWRecallCTI, boolean objectoChoose) {
        final Map<String,String> mapTosendSchd = new Map<String,String>();
        final List<MX_SB_VTS_SendLead_helper_cls.RequestSendLead> lstRequest = new List<MX_SB_VTS_SendLead_helper_cls.RequestSendLead>();
        for(WrapperRecallCTI wrappItem: lstWrapSend) {
            if(String.isNotBlank(wrappItem.scheduleDate) == true) {
            	final MX_SB_VTS_SendLead_helper_cls.RequestSendLead temp_rest = new MX_SB_VTS_SendLead_helper_cls.RequestSendLead();
               	temp_rest.ID_LEAD = MX_SB_VTS_SendLead_helper_cls.returnEmpty(wrappItem.idRecord);
                temp_rest.LOGIN = MX_SB_VTS_SendLead_helper_cls.returnEmpty(wrappItem.login);
                temp_rest.SERVICEID=Integer.valueOf(MX_SB_VTS_SendLead_helper_cls.returnEmptyphone(String.valueOf(wrappItem.serviceId)));
                temp_rest.LOADID=Integer.valueOf(wrappItem.loadId);
                temp_rest.SCHEDULEDATE=MX_SB_VTS_SendLead_helper_cls.returnEmpty(wrappItem.scheduleDate);
                temp_rest.PHONE1=MX_SB_VTS_SendLead_helper_cls.returnEmpty(wrappItem.sTelefono1);
                temp_rest.PHONE2=MX_SB_VTS_SendLead_helper_cls.returnEmpty(wrappItem.sTelefono2);
                temp_rest.PHONE3=MX_SB_VTS_SendLead_helper_cls.returnEmpty(wrappItem.sTelefono3);
                temp_rest.NAME= MX_SB_VTS_SendLead_helper_cls.returnEmpty(wrappItem.displayName);
                lstRequest.add(temp_rest);
                mapTosendSchd.put(temp_rest.ID_LEAD,wrappItem.scheduleDate);
            }
        }
        final Map<String,String> mapsent = new map<String,String>();
        mapsent.put('ListLeads',JSON.serialize(lstRequest));
        sendRequest(METHOD,mapsent,idsWRecallCTI, objectoChoose, mapTosendSchd);
	}

    /**
    * @description envia registros al proveedor
    * @author Eduardo Hernández Cuamatzi | 26/11/2019 
    * @param method de petición iaso
    * @param mapTosend mapa de registros a enviar
    * @param idsWRecallCTI Ids de registros enviados
    * @param objectoChoose tipo de objeto enviado
    **/
    @future(callout=true)
    public static void sendRequest(String method,Map<String,String> mapTosend,Map<String,String> idsWRecallCTI, boolean objectoChoose, Map<String,String> schdDates) {
        final HttpResponse httpRest = MX_SB_VTS_SendLead_helper_cls.invoke(method, mapTosend);
        final Integer statusCode = HttpRest.getStatusCode();
        if(LSTSTATUS.contains(String.valueOf(statusCode))) {
            updateNextAction(idsWRecallCTI,objectoChoose,	'0', schdDates);
        } else {
            updateNextAction(idsWRecallCTI,objectoChoose,	String.valueOf(statusCode), schdDates);
        }
    }

    /**
    * @description actualiza registros despues de la petición
    * @author Eduardo Hernández Cuamatzi | 26/11/2019 
    * @param updateRecords mapa de registros a actualizar
    * @param objectoChoose tipo de registros
    * @param xmlRespuesta respuesta del servicio 
    **/
    public static void updateNextAction(Map<String,String> updateRecords, boolean objectoChoose,String xmlRespuesta, Map<String,String> schdDates) {
        final List <sObject> updateEl = new List <sObject> ();
        for(String record : updateRecords.keyset()) {
            if(objectoChoose == true) {
                final Opportunity opp =new opportunity(id=record,MX_SB_VTS_NextActionCall__c = decimal.valueof(updateRecords.get(record).trim()),xmlRespuesta__c=xmlRespuesta, xmlEnvio__c = schdDates.get(record));
                updateEl.add(opp);
            } else if(objectoChoose == false) {
                final Lead leadUp =new Lead(id=record,MX_SB_VTS_NextActionCall__c = decimal.valueof(updateRecords.get(record).trim()),xmlRespuesta__c=xmlRespuesta, xmlEnvio__c = schdDates.get(record));
                updateEl.add(leadUp);
            }
        }
        update updateEl;
    }

    /**
    * @description  genera HorLimite
    * @author Eduardo Hernández Cuamatzi | 26/11/2019 
    * @param proveedor proveedor de marcado
    * @param timeRecall hora de remarcado
    * @return Time hora limite
    **/
    public static Time finalDateTimeCall(MX_SB_VTS_ProveedoresCTI__c proveedor, DateTime timeRecall) {
        final String dayWeek = timeRecall.format('EEEE');
        final Integer franja = String.isNotBlank(String.valueOf(proveedor.MX_SB_VTS_FranjaHorario__c)) ?  Integer.valueOf
        (proveedor.MX_SB_VTS_FranjaHorario__c) : 1;
        Time limitCalls = Time.newInstance(21, 00, 00, 00).addMinutes(-franja);
        if(dayWeek.equalsIgnoreCase('Saturday')) {
            if(String.isNotBlank(String.valueOf(proveedor.MX_SB_VTS_FinalBusinessHoursS__c))) {
                limitCalls = Time.newInstance(Integer.valueOf(proveedor.MX_SB_VTS_FinalBusinessHoursS__c.hour()), proveedor.MX_SB_VTS_FinalBusinessHoursS__c.minute(), 00, 00).addMinutes(-franja);
            } else {
                limitCalls = Time.newInstance(15, 30, 00, 00).addMinutes(-franja);
            }
        } else if (dayWeek.equalsIgnoreCase('Sunday') == false && String.isNotBlank(String.valueOf(proveedoR.MX_SB_VTS_FinishBusinessHoursL_V__c))) {
                limitCalls = Time.newInstance(Integer.valueOf(proveedor.MX_SB_VTS_FinishBusinessHoursL_V__c.hour()), proveedor.MX_SB_VTS_FinishBusinessHoursL_V__c.minute(), 00, 00).addMinutes(-franja);
        }
        return limitCalls;
    }

    /**
    * @description recupera los proveedores
    * @author Eduardo Hernández Cuamatzi | 26/11/2019 
    * @return Map<Id, MX_SB_VTS_ProveedoresCTI__c>  mapa de proveedores activos
    **/
    public static Map<Id, MX_SB_VTS_ProveedoresCTI__c> fillProveedores() {
        return new Map<Id, MX_SB_VTS_ProveedoresCTI__c>([Select Id, Name, MX_SB_VTS_BusinessHoursL_V__c,MX_SB_VTS_FinishBusinessHoursL_V__c,
        MX_SB_VTS_BusinessHoursL_S__c,MX_SB_VTS_FinalBusinessHoursS__c,MX_SB_VTS_FranjaHorario__c from MX_SB_VTS_ProveedoresCTI__c 
        where MX_SB_VTS_Identificador_Proveedor__c NOT IN ('')]);
    }

    /**
    * @description recupera las bandejas por Id de bandeja
    * @author Eduardo Hernández Cuamatzi | 4/11/2019 
    * @param mapTrays  mapa de todas las bandejas
    * @return Map<String, MX_SB_VTS_Lead_tray__c> mapa de Id de bandejas del proveedor
    **/
    public static Map<String,MX_SB_VTS_Lead_tray__c> findTrySend(Map<Id, MX_SB_VTS_Lead_tray__c> mapTrays) {
        final Map<String,MX_SB_VTS_Lead_tray__c> mapsTray = new Map<String,MX_SB_VTS_Lead_tray__c>();
        for(MX_SB_VTS_Lead_tray__c tray : mapTrays.values()) {
            mapsTray.put(tray.MX_SB_VTS_ID_Bandeja__c , tray);
        }
        return mapsTray;
    }
}