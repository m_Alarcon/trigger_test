/**-------------------------------------------------------------------------
* Nombre: 		MX_SB_VTS_GetSetDPForm_Ctrl_tst
* @author 		Alexandro Corzo
* Proyecto: 	Formulario - Dirección de la Propiedad
* Descripción : Clase de prueba para la clase de recuperación de colonias conforme al
*				CP ingresado.
* --------------------------------------------------------------------------
*                         Fecha           Autor                   Desripción
* --------------------------------------------------------------------------
* @version 1.0            20/07/2020      Alexandro Corzo         Creación de la Clase
* --------------------------------------------------------------------------*/
@isTest
public class MX_SB_VTS_GetSetDPForm_Ctrl_tst {
    /** Variable de Apoyo: sUserName */
    Static String sUserName = 'UserOwnerTest01';
    /** Variable de Apoyo: sNoEmpty */
    Static String sNoEmpty = 'Lista No Vacia';
    /** Variable de Apoyo: sCPTest */
    Static String sCPTest = '14260';
    /** Variable de Apoyo: sURL */
    Static String sUrl = 'http://www.example.com';
    
    /**
     * @description: Instrucción de Clase de Prueba para realizar
     *               la configuracion previa 
     * @author: 	 Alexandro Corzo
     */   
    @TestSetup
    static void makeData() {
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'getListInsuraceCustomerCatalog', iaso__Url__c = sUrl, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
    }
    
    /**
     * @description: Instrucción de Clase de Prueba para validar
     *               la clase Ctrl
     * @author:		 Alexandro Corzo
     */ 
    @isTest static void validaCtrlNew() {
        final User objUserTstC = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(objUserTstC) {
        	final Map<String, String> mHeadersMockC = new Map<String, String>();
            mHeadersMockC.put('tsec', '1234567890');
            final MX_WB_Mock objMockCallOutC = new MX_WB_Mock(200, 'Complete', '{"iCatalogItem":{"suburb":[{"neighborhood":{"id":"2651","name":"02 U CTM CULHUACAN 8"},"county":{"id":"003","name":"COYOACÁN"},"city":{"id":"01","name":"CIUDAD DE MÉXICO"},"state":{"id":"09","name":"CIUDAD DE MEXICO"}}]}}', mHeadersMockC);
            iaso.GBL_Mock.setMock(objMockCallOutC);
            Test.startTest();
            	final Map<String, Object> mTestRstC = MX_SB_VTS_GetSetDPForm_Ctrl.dataInputCP(sCPTest);
            Test.stopTest();
            System.assert(!mTestRstC.isEmpty(), sNoEmpty);
        }
    }
}