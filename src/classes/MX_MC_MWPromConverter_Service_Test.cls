/**
 * @File Name          : MX_MC_MWPromConverter_Service_Test.cls
 * @Description        : Clase para Test de MX_MC_MWPromConverter_Service
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 24/08/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      27/08/2020           Jair Ignacio Gonzalez Gayosso       Initial Version
**/
@isTest
public with sharing class MX_MC_MWPromConverter_Service_Test {

    /**
    * @Description MSG_THR_ID property
    **/
    private static final String THREAD_ID = 'testPromote_123';

    /**
    * @Description IS_DRAFT property
    **/
    private static final String IS_DRAFT = 'isDraft';


    /**
     * @Description Clase de prueba para MX_MC_MWPromConverter_Service
     * @author Jair Ignacio Gonzalez Gayosso | 24/08/2020
    **/
    @TestSetup
    static void makeData() {

        final Case convTest = new Case();
        convTest.mycn__MC_ConversationId__c = THREAD_ID;
        convTest.RecordTypeId = mycn.MC_ConversationsDatabase.getCaseConversationRecordType();
        convTest.Status = 'New';
        convTest.Subject = 'MC Conversation example';
        insert convTest;
        final iaso__GBL_Rest_Services_Url__c restUrl = new iaso__GBL_Rest_Services_Url__c(Name = 'MC_pushNotification',
                                                                                        iaso__Url__c = 'www.testMTWConverter.bbva',
                                                                                        iaso__Cache_Partition__c = 'iaso.ServicesPartition');
        insert restUrl;
    }

    /**
     * @Description Clase de prueba para MX_MC_MWPromConverter_Service
     * @author Jair Ignacio Gonzalez Gayosso | 24/08/2020
    **/
    @isTest
    static void testInputPatchConv() {
        final Map<String, Object> mxInput = new Map<String, Object>();
        mxInput.put('threadId', '"testPromote_123"');
        final MX_MC_MWPromConverter_Service mxConverter = new MX_MC_MWPromConverter_Service();
        final Map<String, Object> mxOutput = mxConverter.convertMap(mxInput);
        System.assertEquals(THREAD_ID, mxOutput.get('messageThreadId'), 'Valor asignado incorrectamente');
    }

    /**
     * @Description Clase de prueba para MX_MC_MWPromConverter_Service
     * @author Jair Ignacio Gonzalez Gayosso | 24/08/2020
    **/
    @isTest
    static void testOutputPatchConv() {
        final MX_WB_Mock mockCallout = new MX_WB_Mock(200,'OK', '[{"Test": "MX_MC_pushNotification_Service"}]',new Map<String,String>());
        final Map<String,Object> inputMx = new Map<String, Object>();
        inputMx.put('customerId','Customer test');
        inputMx.put('subject','Test Conversation');
        inputMx.put('location','http://bbva.test.com/conv/15');
        inputMx.put(IS_DRAFT,false);
        final MX_MC_MWPromConverter_Service mxConverter = new MX_MC_MWPromConverter_Service();
        Test.startTest();
        iaso.GBL_Mock.setMock(mockCallout);
        final Object response = mxConverter.convert('resource created correctly', 'postThread', inputMx);
        Test.stopTest();
        System.assert(response != null, 'La conversion es incorrecta');
    }
}