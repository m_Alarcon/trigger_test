/**
 * @File Name          : MX_SB_VTS_SendFacebookLeads_cls.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 21/4/2020 21:59:49
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    16/4/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
public with sharing class MX_SB_VTS_SendFacebookLeads_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
     /**variable query*/
     public string finalQuery {get;set;}
 
     /**
      * MX_SB_VTS_SendFacebookLeads_cls constructor
      * @param  finalQuery  query a ejecutar
      */
     public MX_SB_VTS_SendFacebookLeads_cls(String finalQuery) {
        this.finalQuery = finalQuery;
     }

     /**
     * start Inicio de batch
     * @param  batchContext contexto batch
     * @return ejecución de base
     */
    public Database.querylocator start(Database.BatchableContext batchContext) {      
        this.finalQuery += ' where LeadSource = \'Facebook\' AND EnviarCTI__c = false AND CreatedDate >= YESTERDAY';
        return Database.getQueryLocator(this.finalQuery);
    }

     /**
     * execute Ejecución de batch
     * @param  batchContext contexto batch
     * @param  scopeLeads  Resultado de ejecución base
     */
    public void execute(Database.BatchableContext batchContext, List<Lead> scopeLeads) {
        final Map<Id, String> leadProductsFace = new Map<Id, String>();
        final Set<String> valProdsFace = new Set<String>();
        final Map<Id, Lead> newEntry = new Map<Id, Lead>();
        for(Lead leadVal : scopeLeads) {
            if(String.isNotBlank(leadVal.LeadSource) && leadVal.LeadSource.equalsIgnoreCase(System.Label.MX_SB_VTS_OrigenFacebook) && String.isNotBlank(leadVal.MobilePhone)) {
                final String prodCorrect = MX_SB_VTS_ValidCorrectNamesASD.validacionProducto(leadVal.Producto_Interes__c);
                leadProductsFace.put(leadVal.Id, prodCorrect);
                valProdsFace.add(prodCorrect);
                newEntry.put(leadVal.Id, leadVal);
            }
        }
        if(valProdsFace.isEmpty() == false) {
            MX_SB_VTS_LeadMultiCTI.validFamilysCTI(newEntry, leadProductsFace, valProdsFace,'callOut');
        }
    }

    /**
     * finish ejecución al finalizar
     * @param  batchContext contexto batch
     */
    public void finish(Database.BatchableContext batchContext) { //NOSONAR
    }
}