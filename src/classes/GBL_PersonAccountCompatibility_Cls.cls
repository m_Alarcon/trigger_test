/**
* @author bbva.com developers
* @date 2019
*
* @group global_hub_kit_visit
*
* @description Class for PersonAccount Compatibility controller
**/
public with sharing class GBL_PersonAccountCompatibility_Cls {
    @testVisible
    /* Constructor*/
    private GBL_PersonAccountCompatibility_Cls() {
    }
    /* Metodo isPersonAccountEnabled*/
    public static boolean isPersonAccountEnabled() {
        return Schema.sObjectType.Account.fields.getMap().containsKey('isPersonAccount');
    }
}