/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RDB_FichaClienteServicios
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2019-11-29
* @Group  		MX_RDB_FichaCliente
* @Description 	Class for WS call outs and their payloads.
* @Changes
* 
*/
public class MX_RDB_FichaClienteServicios {
    
    private MX_RDB_FichaClienteServicios () {}
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description call the WS for update
    * @param accRecord Account record to be used as information to be sent
    * @return N/A
    **/
    public static void updateElectronicInformation(Account accRecord) {
        final String serviceName = Test.isRunningTest() ? 'updateElectronicInformationMock' : 'updateElectronicInformation';
        final String jsonInput = MX_RDB_FichaClienteServicios.getUpdateElectronicInformationPayload(accRecord);
        
        iaso.GBL_Integration_GenericService.invoke(serviceName, jsonInput);
    }
    
    private static String getUpdateElectronicInformationPayload(Account cliente) {
        String finalJSON = '{';
        finalJSON += ' "customerId":"' + cliente.MX_SB_BCT_Id_Cliente_Banquero__c + '",';
        finalJSON += ' "email":"' + cliente.PersonEmail + '",';
        finalJSON += ' "telephoneNumber":"' + cliente.Phone + '"';
        finalJSON += '}';
        return finalJSON;
    }
}