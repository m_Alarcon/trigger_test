/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Account_Service_Test
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-08-06
* @Description 	Test Class for Account Service
* @Changes
*  
*/
@isTest
public class MX_BPP_Account_Service_Test {
	/** String BPyP Estandar */
	static final String PR_BPYPESTANDAR = 'BPyP Estandar';
    
    /** String RecordType.DeveloperName Account */
    static final String RT_ACC = 'BPyP_tre_Cliente';
    
    /** String Account Name */
    static final String ACCNAME = 'userBPyP';
    
    /** String User LastName */
    static final String USRLASTNAME = 'UsuarioBPyP';
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description TestSetup
    **/
    @testSetup
    static void setup() {
        final User userBanq = UtilitysDataTest_tst.crearUsuario(USRLASTNAME, PR_BPYPESTANDAR, Null);
        userBanq.Segmento_Ejecutivo__c = 'EMPRESARIAL';
        insert userBanq;
        final Account cliente = UtilitysDataTest_tst.crearCuenta(ACCNAME, RT_ACC);
        cliente.No_de_cliente__c = '0987';
        insert cliente;
        
        final Lead candidato = new Lead();
        candidato.FirstName = 'Lead';
        candidato.LastName = 'BPPTest';
        candidato.LeadSource = 'Preaprobados';
        candidato.MX_ParticipantLoad_id__c = '0987';
        candidato.MX_WB_RCuenta__c = cliente.Id;
        candidato.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('MX_BPP_Leads').getRecordTypeId();
        candidato.OwnerId = userBanq.Id;
        insert candidato;
        
        final Case caso = new Case();
        caso.Subject = candidato.Name + ' Lead';
        caso.Description = 'Test Case';
        caso.MX_SB_SAC_Folio__c = '1001013';
        caso.Status = 'Nuevo';
        caso.AccountId = cliente.Id;
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MX_EU_Case_Apoyo_General').getRecordTypeId();
        caso.OwnerId = userBanq.Id;
        insert caso;
    }
    
    /**
    *@Description   Test Method for ownerUpdate
    *@author 		Edmundo Zacarias
    *@Date 			2020-08-06
    **/
    @isTest
    private static void tesOwnerUpdate() {
        final User newUser = UtilitysDataTest_tst.crearUsuario(USRLASTNAME + 'Tst', PR_BPYPESTANDAR, Null);
        newUser.Segmento_Ejecutivo__c = 'EMPRESARIAL';
        insert newUser;
        final List<Account> listAccs = [SELECT Id FROM Account WHERE Name =: ACCNAME AND RecordType.DeveloperName =: RT_ACC];
        final Set<Id> accIds = new Set<Id>();
        for(Account cuenta : listAccs) {
            cuenta.OwnerId = newUser.Id;
            accIds.add(cuenta.Id);
        }
        Test.startTest();
        update listAccs;
        Test.stopTest();
        
        final Case updatedCase = [SELECT Id, OwnerId FROM Case WHERE MX_SB_SAC_Folio__c = '1001013' AND RecordType.DeveloperName = 'MX_EU_Case_Apoyo_General' LIMIT 1];
        System.assertEquals(newUser.Id, updatedCase.OwnerId, 'Owner no actualizado');
    }
}