/*
* @Nombre: MX_SB_MLT_SiniestroVida_Controller
* @Autor: Marco Antonio Cruz Barboza
* @Proyecto: Siniestros - BBVA
* @Descripción : Clase que almacena los methods usados en la Toma de Reporte de Siniestro Vida
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   	Descripción
* --------------------------------------------------------------------------------
* 1.0         23/04/2020     Marco Antonio Cruz Barboza		Creación
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_Vida_Controller {
    /*
	* var String Valor boton cancelar
	*/    
    public static final String CANCELAR = 'Cancelar';
    /*
	* var String Valor boton Volver
	*/    
    public static final String VOLVER = 'Volver';
    /*
	* var String Valor boton Crear
	*/    
    public static final String CREAR = 'Crear';
    /*
    * var String tipo siniestro
    */
    public static final String TIPO = 'Siniestros';
    /*
    * @description constructor privado para evitar singleton
    * @param  void
    */
    private MX_SB_MLT_Vida_Controller() {
    }
        /*
    * @description Cambio de etapa de flujo de vida
    * @param String buttonValue,String strComentarios, String siniId
    * @return String void
    */
    @AuraEnabled
    public static Siniestro__c searchSinVida(String siniId) {
		Return MX_SB_MLT_SiniestroVida_Controller.searchSinVida(siniId);
    }
    /*
    * @description Cambio de etapa de flujo de vida
    * @param String buttonValue,String strComentarios, String siniId
    * @return String void
    */
    @AuraEnabled
    public static void changeStep(String buttonValue,String strComentarios, String siniId ) {
        final Siniestro__c siniestro = [SELECT Id, MX_SB_MLT_JourneySin__c,MX_SB_MLT_ComentariosSiniestro__c FROM Siniestro__c where id =: siniId limit 1];
        siniestro.MX_SB_MLT_ComentariosSiniestro__c= strComentarios;
        if(buttonValue==VOLVER) {
            siniestro.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_validacion;
        }
        if(buttonValue==CREAR) {
            siniestro.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_detalles;
        }
        if(buttonValue==CANCELAR) {
            siniestro.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_cerrado;
        }
        update siniestro;
    }
    /*
    * @description Se inserta información del fallecido
    * @param Siniestro__c fallecido
    */
    @AuraEnabled
    public static void insertFallecido (Siniestro__c fallecido) {
        final Siniestro__c sin = [SELECT id, MX_SB_MLT_JourneySin__c, TipoSiniestro__c FROM Siniestro__c where id =: fallecido.Id limit 1];
        if(sin.TipoSiniestro__c == tipo) {
            UPDATE fallecido;
        } else {
            fallecido.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_detalles;
        	UPDATE fallecido;
        }
    }
    /*
    * @description Acciones flujo vida
    * @param String sinId
    */
    @AuraEnabled
    public static void sinAction (String sinAction, String sinId) {
        final Siniestro__c siniestro = [SELECT id, MX_SB_MLT_JourneySin__c FROM Siniestro__c where id =: sinId limit 1];
        if (sinAction == VOLVER ) {
            siniestro.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_validacion;
        }
        if(sinAction == CANCELAR) {
            siniestro.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_cerrado;
            siniestro.Estatus__c = 'Cancelado';
        }
        update siniestro;
    }
    /*
    * @description Crea prototipo de Workorder para reembolso de tipo vida
    * @param String Id
    */
    @auraEnabled
    public static void sendService (String serviceWOId) {
        Final WorkOrder serviceAssistant = new WorkOrder();
        serviceAssistant.MX_SB_MLT_Siniestro__c = serviceWOId;
        serviceAssistant.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Servicios).getRecordTypeId();
        serviceAssistant.MX_SB_MLT_TipoServicioAsignado__c = Label.MX_SB_MLT_Reembolso;
        upsert serviceAssistant;
    }
    /*
    * @description Validación de direccion
    * @param String Id
    */
    @AuraEnabled
    public static Boolean addressValid(String sinId) {
        final Siniestro__c sin = [SELECT id, MX_SB_MLT_Address__c, MX_SB_MLT_DireccionDestino__c FROM Siniestro__c where id =: sinId limit 1];
        return sin.MX_SB_MLT_DireccionDestino__c == null || sin.MX_SB_MLT_Address__c == null ? false : true;
    }
}