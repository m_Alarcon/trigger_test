/**
* @File Name          : MX_BPP_LeadStart_Ctrl_Test.cls
* @Description        : Test class for MX_BPP_LeadStart_Ctrl
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 24/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      24/06/2020            Gabriel Garcia Rojas          Initial Version
**/
@isTest
private class MX_BPP_LeadStart_Ctrl_Test {
	/** name user for record */
    final static String NAMECTRL = 'testUserCtrl';
    /** name role variable for records */
    final static String NAMEROCTRL = '%BPYP DIRECTOR BANCA PERISUR%';
    /** name profile variable for records */
    final static String NAMEPROCTRL = 'BPyP Director Oficina';
    /** error message assert*/
    final static String MESSAGE_TS = 'Fail method';

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        final User testUserCtrl = UtilitysDataTest_tst.crearUsuario(NAMECTRL, NAMEPROCTRL, NAMEROCTRL);
        insert testUserCtrl;

    }

    /**
     * @description constructor test Ctrl
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void testConstructorCtrl() {
        final MX_BPP_LeadStart_Ctrl startCtrlTest = new MX_BPP_LeadStart_Ctrl();
        System.assertNotEquals(startCtrlTest, null, MESSAGE_TS);
    }

     /**
     * @description test userInfoCtrl
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void userInfoCtrlTest() {
        final User usuarioDO = [SELECT Id FROM User WHERE Name=: NAMECTRL];
        System.runAs(usuarioDO) {
            final MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper wrapperCtrl = MX_BPP_LeadStart_Ctrl.userInfo();
            System.assertNotEquals(wrapperCtrl.setDivisiones.size(), 0, MESSAGE_TS);
        }
    }

    /**
     * @description test fetchusdataCtrl
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void fetchusdataCtrlTest() {
        final List<String> listinfoUserCtrl = MX_BPP_LeadStart_Ctrl.fetchusdata();
        System.assertNotEquals(listinfoUserCtrl.size(), 0 , MESSAGE_TS);
    }

    /**
     * @description test fetchDataClassCtrl
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void fetchDataClassCtrlTest() {
        final MX_BPP_LeadStart_Helper.WRP_ChartStacked chartStackeCtrl = MX_BPP_LeadStart_Ctrl.fetchDataClass(new List<String>{'','Abierto'}, new List<String>{'','SUR'}, new List<String>{'',''}, 'oficina');
        System.assertNotEquals(chartStackeCtrl.lsColor.size(), 0 , MESSAGE_TS);
    }

    /**
     * @description test infoLeadByClauseCtrl
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void infoLeadByClauseCtrlTest() {
        final List<CampaignMember> listCtrlMem = MX_BPP_LeadStart_Ctrl.infoLeadByClause(new List<String>{'','Abierto'}, new List<String>{'','SUR'}, new List<String>{'',''}, '5');
        System.assertEquals(listCtrlMem.size(), 0 , MESSAGE_TS);
    }

    /**
     * @description test fetchPgsCtrl
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void fetchPgsCtrlTest() {
        final Integer totalPagCtrl = MX_BPP_LeadStart_Ctrl.fetchPgs(10);
        System.assertEquals(totalPagCtrl, 1 , MESSAGE_TS);
    }
}