/**
* @File Name          : MX_BPP_Oportunidad_Service.cls
* @Description        :
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 14/10/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      14/10/2020        Gabriel García Rojas              Initial Version
**/
@SuppressWarnings()
public with sharing class MX_BPP_Oportunidad_Service {

    /*Variable global Banca*/
    static final String BANCA = 'Red BPyP';
    /*Variable global cadena vacia*/
    static final String STREMPTY = '';

    /*Variable global Cliente Moral*/
    static final String RECTRECTE='BPyP_tre_Cliente';
    /*Variable global Cliente Físico*/
    static final String RECPACTE='MX_BPP_PersonAcc_Client';
    /*Variable global Cuentas*/
    static final String CUENTASTR ='Cuentas';
    /*Variable global Cuentas*/
    static final String PROSPECTOSTR ='Prospectos';
    /*Variable global Todas*/
    static final String TODASSTR ='TODAS';
    /*Variable global 25*/
    static final Integer NUMBCOL = 25;


    /**Constructor */
    @TestVisible
    private MX_BPP_Oportunidad_Service() { }

     /**
    * @description retorna un objeto WRP_ChartStacked para la estructura de gráficas
    * @author Gabriel Garcia Rojas
    * @param String params [tyAcc, tyOpp, tipoConsulta, filtro0]
    * @param Datetime sDate
    * @param Datetime eDate
    * @return WRP_ChartStacked
    **/
    @SuppressWarnings('sf:DUDataflowAnomalyAnalysis')
    public static WRP_ChartStacked fetchServiceDataOpp(List<String> params, Datetime sDate, Datetime eDate) {

        final String tyAcc = params[0];
        final String tyOpp = params[1];
        final String tipoConsulta = params [2];
        final String filtro0 = params[3];

		final list<String> tempColor = new List<String>();
		final List<Integer> red = BPyP_OppRep_Utilities.genRedBBVA();
		final List<Integer> green = BPyP_OppRep_Utilities.genGreenBBVA();
		final List<Integer> blue = BPyP_OppRep_Utilities.genBlueBBVA();


		final Map<String,Integer> tempData = new Map<String,Integer>();
		final Map<String,String> tempAmm = new Map<String,String>();

        String tipoAcc = '';
        String filtroStageName = '';

        final Map<String, List<String>> mapFields = fetchMapType();

        if(tyAcc.equals(CUENTASTR) || tyAcc.equals(PROSPECTOSTR)) {
             tipoAcc = mapFields.get(tyAcc)[2];
        }

        if(!tyOpp.equals(TODASSTR)) {
            filtroStageName = ' AND StageName =: filtro5 ';
        }

        Datetime sdt, edt;
        sdt = BPyP_OppRep_Utilities.getDate(sDate, eDate, 'sdt');
		edt = BPyP_OppRep_Utilities.getDate(sDate, eDate, 'edt');
        edt = edt.addDays(1);

        final List<String> listParamsOpp = new List<String>{filtro0, BANCA, STREMPTY, mapFields.get(tyAcc)[0], mapFields.get(tyAcc)[1], tyOpp };
        final String filedsOppQuery = ' count(Id) Registros, SUM(Amount) Monto, StageName, ' + mapFields.get(tipoConsulta)[0];
        final String clouseOppQuery = ' WHERE Owner.VP_ls_Banca__c =: filtro1 AND (NOT (Owner.BPyP_ls_NombreSucursal__c =: filtro2))  AND (NOT (Owner.Divisi_n__c =: filtro2)) '
            + ' AND Owner.IsActive = true ' + tipoAcc + filtroStageName
            + ' AND CreatedDate >=: sdt AND CreatedDate <=: edt '
            + mapFields.get(tipoConsulta)[1] + ' GROUP BY StageName, ' + mapFields.get(tipoConsulta)[0] + ' ORDER BY StageName limit 50000 ';

        final List<AggregateResult> ltyopp = MX_RTL_Opportunity_Selector.fetchAROppByFilters(filedsOppQuery, clouseOppQuery, listParamsOpp, sdt, edt );

        final Set<String> setLabels = new Set<String>();
        final Set<String> setStage = new Set<String>();

        for(AggregateResult aggR : ltyopp) {
            setLabels.add( (String) aggR.get(mapFields.get(tipoConsulta)[2]));
            setStage.add( (String) aggR.get('StageName'));
            tempData.put( (String) aggR.get('StageName') + (String) aggR.get(mapFields.get(tipoConsulta)[2]) , (Integer)aggR.get('Registros') );
            tempAmm.put( (String) aggR.get('StageName') + (String) aggR.get(mapFields.get(tipoConsulta)[2]) , String.valueOf(aggR.get('Monto')) );
        }

        for(Integer idx = 0;  idx < setStage.size(); idx++) {
            String colorStr = 'rgb('+red[Math.mod(idx,NUMBCOL)]+', '+green[Math.mod(idx,NUMBCOL)]+', '+blue[Math.mod(idx,NUMBCOL)]+')';
            tempColor.add(colorStr);
        }
		tempColor.add('rgb');

        return new WRP_ChartStacked(new List<String>(setLabels), new List<String>(setStage), tempColor, tempData, tempAmm);
	}

     /**
    * @description retorna una lista oportunidaddes filtradas
    * @author Gabriel Garcia Rojas
    * @param String params [bkm, tyAcc, tyOpp, offset]
    * @param Datetime sDate
    * @param Datetime eDate
    * @return List<Opportunity>
    **/
    @SuppressWarnings('sf:DUDataflowAnomalyAnalysis, sf:UnusedLocalVariable')
	public static List<Opportunity> fetchOpp(List<String> params, Datetime sDate, Datetime eDate) {
        final String filtro0 = params[0];
        final String tyAcc = params[1];
        final String tyOpp = params[2];
        final String offs = params [3];
        String limitAndOffset = ' limit 50000 ';
        String tipoAcc = '';
        String filtroStageName = '';

        final Map<String, List<String>> mapFields = fetchMapType();

        if(tyAcc.equals(CUENTASTR) || tyAcc.equals(PROSPECTOSTR)) {
             tipoAcc = mapFields.get(tyAcc)[2];
        }

        if(offs != null && String.isNotBlank(offs)) {
            limitAndOffset = ' limit 10 offset ' + offs;
        }

        if(!tyOpp.equals(TODASSTR)) {
            filtroStageName = ' AND StageName =: filtro5 ';
        }

		Datetime sdt, edt;
		if(sDate == null && eDate == null) {
			sdt = BPyP_OppRep_Utilities.obtStAndEndDate(system.today().month(),'St');
			edt = BPyP_OppRep_Utilities.obtStAndEndDate(system.today().month(),'En');
		} else {
			sdt = sDate;
			edt = eDate;
		}
        edt = edt.addDays(1);

        final List<String> listParamsOpp = new List<String>{filtro0, BANCA, STREMPTY, mapFields.get(tyAcc)[0], mapFields.get(tyAcc)[1], tyOpp};
        final String filedsOppQuery = ' Id, StageName, Name, Account.Name, MX_RTL_Producto__c, (Select MX_RTL_SubProducto__c From OpportunityLineItems), Amount, CloseDate, CreatedDate, Owner.Name ';
        final String clouseOppQuery = ' WHERE Owner.VP_ls_Banca__c =: filtro1 AND (NOT (Owner.BPyP_ls_NombreSucursal__c =: filtro2))  AND (NOT (Owner.Divisi_n__c =: filtro2)) '
            + ' AND Owner.IsActive = true ' + tipoAcc + filtroStageName
            + ' AND CreatedDate >=: sdt AND CreatedDate <=: edt '
            + mapFields.get('BanqueroOnly')[1] + limitAndOffset;

        return MX_RTL_Opportunity_Selector.fetchListOppFilters(filedsOppQuery, clouseOppQuery, listParamsOpp, sdt, edt);
	}

    /**
    * @description retorna mapa con las condiciones de búsqueda por division, oficina o ejecutivo
    * @author Gabriel Garcia Rojas
    * @return Map<String, List<String>>
    **/
    public static Map<String, List<String>> fetchMapType() {
        final Map<String, List<String>> mapFields = new Map<String, List<String>>();
        mapFields.put('Division', new List<String>{'Owner.Divisi_n__c', '', 'Divisi_n__c' });
        mapFields.put('Oficina', new List<String>{'Owner.BPyP_ls_NombreSucursal__c', 'AND Owner.Divisi_n__c =: filtro0 AND Account.Owner.Divisi_n__c =: filtro0', 'BPyP_ls_NombreSucursal__c' });
        mapFields.put('Banquero', new List<String>{'Owner.Name', 'AND Owner.BPyP_ls_NombreSucursal__c =: filtro0 AND Account.Owner.BPyP_ls_NombreSucursal__c =: filtro0', 'Name' });
        mapFields.put('BanqueroOnly', new List<String>{'Owner.Name', 'AND Owner.Name =: filtro0 AND Account.Owner.Name =: filtro0', 'Name' });

        mapFields.put('Cuentas', new List<String>{'BPyP_tre_Cliente', 'MX_BPP_PersonAcc_Client', ' AND (Account.RecordType.DeveloperName =: filtro3 OR Account.RecordType.DeveloperName =: filtro4) ' });
        mapFields.put('Prospectos', new List<String>{'BPyP_tre_noCliente', 'MX_BPP_PersonAcc_NoClient', ' AND (Account.RecordType.DeveloperName =: filtro3 OR Account.RecordType.DeveloperName =: filtro4) ' });

        return mapFields;
    }


    /** Wrapper class for chart.js with labels, data and color*/
    public class WRP_ChartStacked {
		/**lista lsLabels */
        @AuraEnabled public list<String> lsLabels {get; set;}
		/** Mapa lsTool*/
        @AuraEnabled public Map<String, String> lsTool {get; set;}
		/**lista lsColor */
        @AuraEnabled public list<String> lsColor {get; set;}
		/** Mapa lsData*/
        @AuraEnabled public Map<String, Integer> lsData {get; set;}
        /**lista lsTyOpp */
        @AuraEnabled public list<String> lsTyOpp {get; set;}

		/** mthd */
        public WRP_ChartStacked(list<String> label,list<String> tyOpp,list<String> color,Map<String, Integer> data,Map<String, String> amm) {
            lsLabels=label;
            lsData=data;
            lsTool=amm;
            lsTyOpp=tyOpp;
            lsColor=color;
        }

    }
}