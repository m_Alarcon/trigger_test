/**
* Indra
* @author           Daniel perez lopez
* Project:          Presuscritos
* Description:      Clase test para methods de selector
*
* Changes (Version)
* ------------------------------------------------------------------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    --------------------------------------
* @version  1.0     2020-09-04      Daniel perez lopez.     Creación de la Clase
*           1.1     2020-12-12      Daniel Perez Lopez      Se cambia origen de testsetup
*/

@isTest
public class MX_RTL_QuoteLineItem_Selector_tst {   
     /**
    * @Method Data Setup
    * @Description Method para preparar datos de prueba 
    * @return void
    **/
	@testSetup
    public static void  data() { 
    	MX_SB_PS_OpportunityTrigger_test.makeData();
    }
    /**
    * @Method Data quoteliItems
    * @Description Method para buscar quoteliItems 
    * @return void
    **/
    @isTest
    static void querieslineitem() {
        test.startTest();
            final Quote qtst1 = [Select id,Name from quote where MX_SB_VTS_Folio_Cotizacion__c='12345' limit 1];
            final Set<Id> ids = new Set<Id>();
            ids.add(qtst1.Id);
            final QuoteLineItem[] lista = MX_RTL_QuoteLineItem_Selector.quoteliItems(' Id, QuoteId ',ids);
        	MX_RTL_QuoteLineItem_Selector.getQuoteLineitemBy('QuoteId',' id ,MX_SB_VTS_Plan__c ',qtst1.Id);
        	System.assert(lista.size()>0,'Exito en consulta');
        test.stopTest();
    }
	
    /**
    * @Method Data Setup
    * @Description Method para actualizar quotelineitems 
    * @return void
    **/
    @isTest
    static void upsertQuoteLI() {
        test.startTest();
            final Quote qtst2 = [Select id,Name from quote where name='qtest' limit 1];
            final Set<Id> ids = new Set<Id>();
            ids.add(qtst2.Id);
            QuoteLineItem[] lista2 = MX_RTL_QuoteLineItem_Selector.quoteliItems(' Id, QuoteId ',ids);
        	Final QuoteLineItem[] lista3 = MX_RTL_QuoteLineItem_Selector.quoteliItemsordr(' Id, QuoteId ',ids);
        	lista2[0].MX_SB_VTS_Plan__c='unplan';
            lista3[0].MX_SB_VTS_Plan__c='5000';
        	lista2 = MX_RTL_QuoteLineItem_Selector.upsertQuoteLI(lista2);
        	System.assert(lista2.size()>0,'Exito en consulta');
        test.stopTest();
    }
}