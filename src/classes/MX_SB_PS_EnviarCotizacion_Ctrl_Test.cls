@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_PS_EnviarCotizacion_Ctrl_Test
* Autor: Juan Carlos Benitez Herrera
* Proyecto: SB Presuscritos - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_PS_EnviarCotizacion_Ctrl

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         9/24/2020    Juan Carlos Benitez Herrera              Creación
* -----------------------------------------------------------------------------------------------
*/
public class MX_SB_PS_EnviarCotizacion_Ctrl_Test {
    /** Nombre de usuario Test */
    final static String NAME = 'Marco';
    /** Apellido Paterno */
    Final static STRING LNAME= 'Cruz';
    /** Apellido Paterno */
    Final static STRING MAILTO= 'marco@domain.com';
    @TestSetup
    static void createData() {
            MX_SB_PS_OpportunityTrigger_test.makeData();
            final Account cntaTst = [Select Id from Account limit 1];
            cntaTst.FirstName=NAME;
            cntaTst.LastName=LNAME;
            cntaTst.Tipo_Persona__c='Física';
            update cntaTst;
            Final Opportunity oppTest = [Select id from Opportunity where AccountId=:cntaTst.Id];
            oppTest.Name= NAME;
            oppTest.StageName=System.Label.MX_SB_PS_Etapa3;
            update oppTest;
            Final Quote quteTest =[Select Id from Quote where OpportunityId=:oppTest.Id limit 1];
            quteTest.Name=NAME;
            update quteTest;
        }
    /*
* @description method que prueba loadContactAcc
* @param  void
* @return void
*/
    @isTest
    static void tstLoadAccounts() {
        Final ID idAcc= [SELECT Id from Account where Name=: NAME+' '+LNAME limit 1].Id;
        test.startTest();
        MX_SB_PS_EnviarCotizacion_Ctrl.loadContactAcc(idAcc);
        system.assert(true, 'Cuenta cargada exitosamente');
        test.stopTest();
    }
    /*
* @description method que prueba LoadOppE
* @param  void
* @return void
*/
    @isTest
    static void testLoadOppE() {
        Final ID idAc= [SELECT Id from Account where Name=: NAME+' '+LNAME limit 1].Id;
        Final ID idOpp =[SELECT Id from Opportunity where AccountId =:idAc].Id;
        test.startTest();
        MX_SB_PS_EnviarCotizacion_Ctrl.loadOppE(idOpp);
        system.assert(true, 'Oportunidad cargada exitosamente');
        test.stopTest();
    }
    @isTest
    static void testsendCotService () {
        final Map<String,String> mapa = new Map<String,String>();
        mapa.put('sNombre', NAME);
        mapa.put('sMail', MAILTO);
        mapa.put('sFolio', '98394');
        test.startTest();
        	MX_SB_PS_EnviarCotizacion_Ctrl.sendCotService(mapa);
        	system.assert(true,'Se ha enviado la cotizacion');
        test.stopTest();
    }
    @isTest
    static void testgetQuoteLneItem() {
        Final Quote quot= [SELECT Id from Quote limit 1];
        test.startTest();
        try {
        	MX_SB_PS_EnviarCotizacion_Ctrl.getQuoteLneItem(quot.Id);
        } catch(Exception e) {
            final Boolean expectedError = e.getMessage().contains('List') ? true : false;
            System.assert(expectedError, 'No se han encontrado quotelineItems asociadas');
        }
        test.stopTest();
    }
}