/**
 * @File Name           : MC_MessageDeleteWrapperConverter_MX.cls
 * @Description         :
 * @Author              : eduardo.barrera3@bbva.com
 * @Group               :
 * @Last Modified By    : eduardo.barrera3@bbva.com
 * @Last Modified On    : 05/18/2020, 13:00:02 PM
 * @Modification Log    :
 * =============================================================================
 * Ver          Date                     Author                     Modification
 * =============================================================================
 * 1.0      05/18/2020, 13:00:02 PM  eduardo.barrera3@bbva.com      Initial Version
 **/
global without sharing class MX_MC_MDWConverter_Service extends mycn.MC_VirtualServiceAdapter {//NOSONAR

    /**
    * @Description Returns a generic Object based on method call from Class in MC component
    * @author eduardo.barrera3@bbva.com | 05/18/2020
    * @param parameters
    * @return Map<String, Object>
    **/
    global override Map<String, Object> convertMap(Map<String, Object> parameters) {
        return parameters;
    }
}