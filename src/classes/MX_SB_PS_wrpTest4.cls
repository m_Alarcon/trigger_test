/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpTest4
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 11/08/2020     Creación del wrapper.*/
@isTest
public class MX_SB_PS_wrpTest4 {
    /*final string message properties*/
   Final static string LBUILDC = 'Build constructor';
      /*Methodo MX_SB_PS_wrpPlanesProducto */
	public  static testMethod void  init20() {
		final MX_SB_PS_wrpPlanesProducto wpPlanesPro = new MX_SB_PS_wrpPlanesProducto();
		System.assertEquals(wpPlanesPro, wpPlanesPro,LBUILDC);
	}
	  /*Methodo MX_SB_PS_wrpPlanesProductoResp */
	public  static testMethod void  init21() {
		final MX_SB_PS_wrpPlanesProductoResp wPlaProRes = new MX_SB_PS_wrpPlanesProductoResp();
		System.assertEquals(wPlaProRes, wPlaProRes,LBUILDC);
	}
	  /*Methodo MX_SB_PS_wrpRevisionPlanesResp */
	public  static testMethod void  init22() { 
		final MX_SB_PS_wrpRevisionPlanesResp wrpRPR  = new MX_SB_PS_wrpRevisionPlanesResp();
		System.assertEquals(wrpRPR, wrpRPR,LBUILDC);
	}
	  /*Methodo MX_SB_PS_wrpRevisonPlanes */
	public  static testMethod void  init23() {
		final MX_SB_PS_wrpRevisonPlanes wRevPlan  = new MX_SB_PS_wrpRevisonPlanes();
		System.assertEquals(wRevPlan, wRevPlan,LBUILDC);
	} 
	  /*Methodo MX_SB_PS_wrpSendDocument */
	public  static testMethod void  init24() {
		final MX_SB_PS_wrpSendDocument wpSendDoc =  new  MX_SB_PS_wrpSendDocument();
		System.assertEquals(wpSendDoc, wpSendDoc,LBUILDC);
	} 
	  /*Methodo MX_SB_PS_wrpSendDocumentRespond */
	public  static testMethod void  init25() {
		final MX_SB_PS_wrpSendDocumentRespond srpSD  = new MX_SB_PS_wrpSendDocumentRespond();
		System.assertEquals(srpSD, srpSD,LBUILDC);
	}
	  /*Methodo MX_SB_PS_wrpValidateOTPResp */
	public  static testMethod void  init26() {
		final MX_SB_PS_wrpValidateOTPResp wrpOTP  = new MX_SB_PS_wrpValidateOTPResp();
		System.assertEquals(wrpOTP, wrpOTP,LBUILDC);
   }

}