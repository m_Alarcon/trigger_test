/**
 * @File Name           : MX_MC_MTWConverter_Service_Helper.cls
 * @Description         :
 * @Author              : eduardo.barrera3@bbva.com
 * @Group               :
 * @Last Modified By    : eduardo.barrera3@bbva.com
 * @Last Modified On    : 04/29/2020, 17:55:32 PM
 * @Modification Log    :
 * =============================================================================
 * Ver          Date                     Author                     Modification
 * =============================================================================
 * 1.0      02/24/2020, 14:52:12 PM   eduardo.barrera3@bbva.com      Initial Version
 * 1.1      03/09/2020, 16:37:55 PM   eduardo.barrera3@bbva.com      Field names redefinition
 * 1.2      03/18/2020, 18:02:09 PM   eduardo.barrera3@bbva.com      Class for listConversation service response
 * 1.3      04/29/2020, 17:55:32 PM   eduardo.barrera3@bbva.com      Class for create message/conversation respose.
 **/
public without sharing class MX_MC_MTWConverter_Service_Helper {

    /**
     * ----------------------------------------------------------------------
     * @Name    BaseWrapper
     * @Author  eduardo.barrera3@bbva.com
     * @Date    Created: 02/24/2020
     * @Group
     * @Description Descriptions
     * @Changes
     *      | 2020-24-02    eduardo.barrera3@bbva.com   Initial Version
     **/

    public virtual class BaseWrapper {

        /**
         * @description
         * @author eduardo.barrera3@bbva.com
         * @return String
         **/
        public String toJson() {
            return JSON.serialize(this, true);
        }
    }

    /**
     * ----------------------------------------------------------------------
     * @Name    MessageThreadPage
     * @Author  eduardo.barrera3@bbva.com
     * @Date    Created: 03/11/2020
     * @Group
     * @Description Descriptions
     * @Changes
     *      | 2020-11-03    eduardo.barrera3@bbva.com   Initial Version
     **/

    public class MessageThreadPage extends BaseWrapper {

        /**
         * @Description messageThreadConversation property
         **/
        public List<MessageThreadConversation> data {get; set;}

        /**
         * @Description pagination property
         **/
        public Pagination pagination {get; set;}

        /**
         * @Description Returns itself in a list
         * @author eduardo.barrera3@bbva.com | 02/24/2020
         * @return List<MC_MessageThreadWrapperAdapter_MX.MessageThreadPage>
         **/
        public List<MX_MC_MTWConverter_Service_Helper.MessageThreadPage> toList() {
            return new List<MX_MC_MTWConverter_Service_Helper.MessageThreadPage> {this};
        }
    }

    /**
     * ----------------------------------------------------------------------
     * @Name    MessageThread
     * @Author  eduardo.barrera3@bbva.com
     * @Date    Created: 02/24/2020
     * @Group
     * @Description Descriptions
     * @Changes
     *      | 2020-24-02    eduardo.barrera3@bbva.com   Initial Version
     **/

    public class MessageThread extends BaseWrapper {

        /**
         * @Description data property
         **/
        public List<Message> data {get; set;}

        /**
         * @Description pagination property
         **/
        public Pagination pagination {get; set;}

        /**
         * @Description Returns itself in a list
         * @author eduardo.barrera3@bbva.com | 02/24/2020
         * @return List<MC_MessageThreadWrapperAdapter_MX.MessageThread>
         **/
        public List<MX_MC_MTWConverter_Service_Helper.MessageThread> toList() {
            return new List<MX_MC_MTWConverter_Service_Helper.MessageThread> {this};
        }
    }

    /**
     * ----------------------------------------------------------------------
     * @Name    Message
     * @Author  eduardo.barrera3@bbva.com
     * @Date    Created: 02/24/2020
     * @Group
     * @Description Descriptions
     * @Changes
     *      | 2020-24-02    eduardo.barrera3@bbva.com   Initial Version
     *      | 2020-09-03    eduardo.barrera3@bbva.com   Field name redefinition
     **/

    public class Message extends BaseWrapper {

        /**
         * @Description id property
         **/
        public String id {get; set;} //NOSONAR

        /**
         * @Description text property
         **/
        public String message {get; set;} //NOSONAR

        /**
         * @Description wasRead property
         **/
        public Boolean wasRead {get; set;}

        /**
         * @Description senderType property
         **/
        public String senderType {get; set;}

        /**
         * @Description isDraft property
         **/
        public Boolean isDraft {get; set;}

        /**
         * @Description attachments property
         **/
        public List<Attachment> attachments {get; set;}
    }

    /**
     * ----------------------------------------------------------------------
     * @Name    MessageThreadConversation
     * @Author  eduardo.barrera3@bbva.com
     * @Date    Created: 2020-11-03
     * @Group
     * @Description Descriptions
     * @Changes
     *      | 2020-11-03    eduardo.barrera3@bbva.com   Initial version
     **/

    public class MessageThreadConversation extends BaseWrapper {

        /**
         * @Description id property
         **/
        public String id {get; set;} //NOSONAR

        /**
         * @Description subject property
         **/
        public String subject {get; set;}

        /**
         * @Description creationDate property
         **/
        public String creationDate {get; set;}

        /**
         * @Description lastUpdate property
         **/
        public String lastUpdate {get; set;}

        /**
         * @Description customer property
         **/
        public Customer customer {get; set;}

        /**
         * @Description businessAgent property
         **/
        public Lookup businessAgent {get; set;}

        /**
         * @Description alternativeBusinessAgent property
         **/
        public Lookup alternativeBusinessAgent {get; set;} //NOSONAR

        /**
         * @Description conversationType property
         **/
        public String conversationType {get; set;}

        /**
         * @Description status property
         **/
        public Status status {get; set;}

        /**
         * @Description unreadMessages property
         **/
        public String unreadMessages {get; set;}
    }

    /**
     * ----------------------------------------------------------------------
     * @Name    Attachment
     * @Author  eduardo.barrera3@bbva.com
     * @Date    Created: 02/24/2020
     * @Group
     * @Description Descriptions
     * @Changes
     *      | 2020-24-02    eduardo.barrera3@bbva.com   Initial Version
     **/

    public class Attachment extends BaseWrapper {

        /**
         * @Description name property
         **/
        public String name {get; set;}

        /**
         * @Description url property
         **/
        public String url {get; set;}
    }

    /**
     * ----------------------------------------------------------------------
     * @Name    Pagination
     * @Author  eduardo.barrera3@bbva.com
     * @Date    Created: 02/24/2020
     * @Group
     * @Description Descriptions
     * @Changes
     *      | 2020-24-02    eduardo.barrera3@bbva.com   Initial Version
     *      | 2020-09-03    eduardo.barrera3@bbva.com   Field name redefinition
     **/

    public class Pagination extends BaseWrapper {

        /**
         * @Description links property
         **/
        public Link links {get; set;}

        /**
         * @Description page property
         **/
        public Integer page {get; set;}

        /**
         * @Description pageSize property
         **/
        public Integer pageSize {get; set;}

        /**
         * @Description totalElements property
         **/
        public Integer totalElements {get; set;}

        /**
         * @Description totalPages property
         **/
        public Integer totalPages {get; set;}
    }

    /**
     * ----------------------------------------------------------------------
     * @Name    Link
     * @Author  eduardo.barrera3@bbva.com
     * @Date    Created: 02/24/2020
     * @Group
     * @Description Descriptions
     * @Changes
     *      | 2020-24-02    eduardo.barrera3@bbva.com   Initial Version
     **/

    public class Link extends BaseWrapper { //NOSONAR

        /**
         * @Description first property
         **/
        public String first {get; set;}

        /**
         * @Description next property
         **/
        public String next {get; set;}
    }

    /**
     * ----------------------------------------------------------------------
     * @Name    Person
     * @Author  eduardo.barrera3@bbva.com
     * @Date    Created: 02/24/2020
     * @Group
     * @Description Descriptions
     * @Changes
     *      | 2020-24-02    eduardo.barrera3@bbva.com   Initial Version
     **/

    public virtual class Person {
        /**
         * @Description id property
         **/
        public String id {get; set;} //NOSONAR
    }

    /**
     * ----------------------------------------------------------------------
     * @Name    Customer
     * @Author  eduardo.barrera3@bbva.com
     * @Date    Created: 02/24/2020
     * @Group
     * @Description Descriptions
     * @Changes
     *      | 2020-24-02    eduardo.barrera3@bbva.com   Initial Version
     **/

    public class Customer extends Person {}

    /**
     * ----------------------------------------------------------------------
     * @Name    Receiver
     * @Author  eduardo.barrera3@bbva.com
     * @Date    Created: 02/24/2020
     * @Group
     * @Description Descriptions
     * @Changes
     *      | 2020-24-02    eduardo.barrera3@bbva.com   Initial Version
     **/
    public class Receiver extends Person {}

    /**
     * ----------------------------------------------------------------------
     * @Name    Lookup
     * @Author  eduardo.barrera3@bbva.com
     * @Date    Created: 02/24/2020
     * @Group
     * @Description Descriptions
     * @Changes
     *      | 2020-24-02    eduardo.barrera3@bbva.com   Initial Version
     **/
    public class Lookup {
        /**
         * @Description id property
         **/
        public String id {get; set;} //NOSONAR
    }

     /**
     * ----------------------------------------------------------------------
     * @Name    Status
     * @Author  eduardo.barrera3@bbva.com
     * @Date    Created: 04/29/2020
     * @Group
     * @Description Descriptions
     * @Changes
     *      | 2020-29-04    eduardo.barrera3@bbva.com   Initial Version
     **/
    public class Status {
        /**
         * @Description id property
         **/
        public String id {get; set;} //NOSONAR

        /**
         * @Description description property
         **/
        public String description {get; set;}
    }
}