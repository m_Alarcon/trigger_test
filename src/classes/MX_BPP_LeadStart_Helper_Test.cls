/**
* @File Name          : MX_BPP_LeadStart_Helper_Test.cls
* @Description        : Test class for MX_BPP_LeadStart_Helper
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 24/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      24/06/2020            Gabriel Garcia Rojas          Initial Version
**/
@isTest
private class MX_BPP_LeadStart_Helper_Test {
	/*Usuario de pruebas*/
    private static User testUserStart = new User();

    /** namerole variable for records */
    final static String NAME_ROLE = '%BPYP BANQUERO BANCA PERISUR%';
    /** nameprofile variable for records */
    final static String NAME_PROFILE = 'BPyP Estandar';
    /** namesucursal variable for records */
    final static String NAME_SUCURSAL = '6343 PEDREGAL';
    /** namedivision variable for records */
    final static String NAME_DIVISION = 'METROPOLITANA';
    /** name variable for records */
    final static String NAMEST = 'testUser';
    /** error message */
    final static String MESSAGETEST = 'Fail method';

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testUserStart = UtilitysDataTest_tst.crearUsuario(NAMEST, NAME_PROFILE, NAME_ROLE);
        insert testUserStart;

        final List<Lead> listLead = new List<Lead>();
        final Lead candHelp = new Lead(LastName = NAMEST, Status = 'Abierto');
        final Lead candHelp2 = new Lead(LastName = NAMEST+'2', Status = 'Abierto');
        listLead.add(candHelp);
        listLead.add(candHelp2);
        insert listLead;

        final Campaign campHelp = new Campaign(Name = NAMEST);
        insert campHelp;

        final List<CampaignMember> listCampMem = new List<CampaignMember>();
        final CampaignMember campMemHelp = new CampaignMember(CampaignId = campHelp.Id, Status='Sent', LeadId = candHelp.Id);
        final CampaignMember campMemHelp2 = new CampaignMember(CampaignId = campHelp.Id, Status='Sent', LeadId = candHelp2.Id);
        listCampMem.add(campMemHelp);
        listCampMem.add(campMemHelp2);
        insert listCampMem;
    }

    /**
     * @description constructor test
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void testConstructor() {
        final MX_BPP_LeadStart_Helper instanceTest = new MX_BPP_LeadStart_Helper();
        System.assertNotEquals(instanceTest, null, MESSAGETEST);
    }

    /**
     * @description test tempColorGet
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void tempColorGetTest() {
        final List<String> listColors = MX_BPP_LeadStart_Helper.tempColorGet(new Set<String>{'Abierto', 'En Gestión'});
        System.assertNotEquals(listColors.size(), 0 , MESSAGETEST);
    }

    /**
     * @description test fetchCampaingMember
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void fetchCampaingMemberTest() {
        final List<CampaignMember> listCampM = MX_BPP_LeadStart_Helper.fetchCampaingMember([SELECT Id FROM Lead LIMIT 10]);
        System.assertNotEquals(listCampM.size(), 0 , MESSAGETEST);
    }

    /**
     * @description test infoLeads
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void infoLeadsTest() {
        final List<Lead> listCandH = MX_BPP_LeadStart_Helper.infoLeads(new List<String>{'',''}, new List<String>{'',''}, new List<String>{'',''}, ' Id ', '');
        System.assertEquals(listCandH.size(), 0 , MESSAGETEST);
    }

    /**
     * @description test resultLeadAssignToUse
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void resultLeadAssignToUseTest() {
        final Set<String> lsLabels = new Set<String>();
        final Set<String> lsTyOpp = new Set<String>();
        final Id userId = System.UserInfo.getUserId();
        final Map<String, User> usrN = new Map<String, User>();
        final List<User> listUsuario = [SELECT Id, Name, Divisi_n__c, BPyP_ls_NombreSucursal__c FROM User WHERE Id =: userId];
        for(User usuarioT : listUsuario) { usrN.put(usuarioT.Id, usuarioT); }
        final Map<String, Integer> mapCandH = MX_BPP_LeadStart_Helper.resultLeadAssignToUse([SELECT Id, Status, OwnerId FROM Lead LIMIT 10], usrN , 'division', lsLabels, lsTyOpp);
        MX_BPP_LeadStart_Helper.resultLeadAssignToUse([SELECT Id, Status, OwnerId FROM Lead LIMIT 10], usrN , 'oficina', lsLabels, lsTyOpp);
        MX_BPP_LeadStart_Helper.resultLeadAssignToUse([SELECT Id, Status, OwnerId FROM Lead LIMIT 10], usrN , 'banquero', lsLabels, lsTyOpp);
        System.assertNotEquals(mapCandH.size(), 0 , MESSAGETEST);
    }

    /**
     * @description test infoByBanquero
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void infoByBanqueroTest() {
    	final MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper infoList = new MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper();
        final List<User> listUser = [SELECT Id, Name, DivisionFormula__c, BPyP_ls_NombreSucursal__c FROM User LIMIT 10];
        MX_BPP_LeadStart_Helper.infoByBanquero(infoList, listUser[0], listUser);
        System.assertNotEquals(listUser.size(), 0 , MESSAGETEST);
    }

    /**
     * @description test infoByDirectorDivisional
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void infoByDirectorDivisionalTest() {
    	final MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper infoList = new MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper();
        final List<User> listUser = [SELECT Id, Name, DivisionFormula__c, BPyP_ls_NombreSucursal__c FROM User LIMIT 10];
        MX_BPP_LeadStart_Helper.infoByDirectorDivisional(infoList, listUser[0], listUser);
        System.assertNotEquals(listUser.size(), 0 , MESSAGETEST);
    }

    /**
     * @description test infoByStaff
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void infoByStaffTest() {
    	final MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper infoList = new MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper();
        final List<User> listUser = [SELECT Id, Name, DivisionFormula__c, BPyP_ls_NombreSucursal__c FROM User LIMIT 10];
        MX_BPP_LeadStart_Helper.infoByStaff(infoList, listUser[0], listUser);
        System.assertNotEquals(listUser.size(), 0 , MESSAGETEST);
    }

    /**
     * @description test constructorTestChar
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void constructorTestChar() {
        final MX_BPP_LeadStart_Helper.WRP_ChartStacked instanceClass = new MX_BPP_LeadStart_Helper.WRP_ChartStacked(new List<String>(), new List<String>(), new List<String>(), new Map<String, Integer>());
        System.assertEquals(instanceClass.lsColor.size(), 0 , MESSAGETEST);
    }
}