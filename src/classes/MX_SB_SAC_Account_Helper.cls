/**
 * @File Name          : MX_SB_SAC_Account_Helper.cls
 * @Description        :
 * @Author             : Jaime Terrats
 * @Group              :
 * @Last Modified By   : Jaime Terrats
 * @Last Modified On   : 6/30/2020, 5:16:53 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/30/2020   Jaime Terrats     Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_SB_SAC_Account_Helper {
    /**
    * @description
    * @author Jaime Terrats | 6/30/2020
    * @param fields
    * @param devName
    * @param sOType
    * @return List<RecordType>
    **/
    public static List<RecordType> getPARecordType(String fields, String devName, String sOType) {
        return MX_RTL_RecordType_Selector.getRecordTypeByDevName(fields, devName, sOType);
    }

    /**
    * @description
    * @author Jaime Terrats | 6/30/2020
    * @param List<Account>
    * @return Account
    **/
    public static List<Account> upsertAccount(List<Account> accsToUpsert) {
        return MX_RTL_Account_Selector.upsertAccount(accsToUpsert);
    }
}