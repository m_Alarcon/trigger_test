/**
 * @File Name          : MX_SB_VTS_GetSetAlianzas_PP_Ctrl.cls
 * @Description        : Realiza el consumo del servicio listCarInsuranceCatalogs
 * @Author             : Alexandro Corzo
 * @Group              :
 * @Last Modified By   : Alexandro Corzo
 * @Last Modified On   : 14/10/2020 10:30:00
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0       14/10/2020      Alexandro Corzo        Initial Version
 * 1.1       12/01/2021      Alexandro Corzo        Se realiza ajuste consumo serivcio ASO
**/
@SuppressWarnings('sf:UseSingleton')
public class MX_SB_VTS_GetSetAlianzas_PP_Ctrl {
    /**
     * @description: Realiza el consumo del servicio listCarInsuranceCatalogs
     * @author: 	 Alexandro Corzo
     * @return: 	 Map<String, Object> oReturnValues
     */
	@AuraEnabled(cacheable=true)
    public static Map<String, Object> obtListCarInsCatCtrl(String sManagmenUnit, String sProductCode) {
        return MX_SB_VTS_GetSetAlianzas_PP_Service.obtListCarInsCatSrv(sManagmenUnit, sProductCode);
    }
}