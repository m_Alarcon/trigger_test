/**---------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_PureCloud
* Autor: Daniel López Perez
* Proyecto: Siniestros - BBVA
* Descripción : Clase test controladora de Visualforce
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                       Desripción
* --------------------------------------------------------------------------------
* 1.0           20/11/2019     Daniel López Perez           Creación
* 1.1			25/11/2019	   Marco Antonio Cruz Barboza	Modificación de test
* 1.2			07/05/2020	Juan Carlos Benitez	Cobertura Tipificacion Llamada
* 1.3			07/05/2020	   Marco Antonio Cruz Barboza	Se agrega methodo test de cambio de status
* 1.4			26/05/2020	   Marco Antonio Cruz Barboza	Se anexa parametro a URL de VisualForce
* --------------------------------------------------------------------------------
**/
@IsTest
public class MX_SB_MLT_PurecloudController_tst {
        /**
        * Variable que sustituye la repetición del número telefonico
        */
        Final static String PHONETEST = '543412342';
        /**
        * Variable que sustituye la repetición del label de la llamada
        */
        Final static String LABELTEST = 'WIBE';
        /**
        * method for testing class
        */
        @IsTest public static void metodouno() {
        final String RecordTypeIdTask = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_PureCloud').getRecordTypeId();
        final Task tasktest = new Task(Telefono__c=PHONETEST,RecordTypeId=RecordTypeIdTask,Subject='Call', MX_SB_VTS_LookActivity__c=LABELTEST);
        insert tasktest;
         	final PageReference pageRef = Page.MX_SB_MLT_PureCloud;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Phone', PHONETEST);
            ApexPages.currentPage().getParameters().put('Origen', LABELTEST);
            final ApexPages.StandardController testPage = new ApexPages.StandardController(tasktest);
        test.startTest();
            
            Final MX_SB_MLT_PurecloudController_cls methodTest = new MX_SB_MLT_PurecloudController_cls(testPage);
            methodTest.casePhone=PHONETEST;
            methodTest.caseId='';
         	methodTest.createCase();
            
            System.assertEquals(PHONETEST, tasktest.Telefono__c,'No Coinciden');
        test.stopTest();
    }
    /**
	* methodo testing tipificacion de la llamda
	*/
    @isTest
    public static void tipfcionLl() {
        final String RdTpIdTsk = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_PureCloud').getRecordTypeId();
        final Task tasktest = new Task(Telefono__c=PHONETEST,RecordTypeId=RdTpIdTsk,Subject='Call',Status='Llamada no relacionada', Resultado_llamada__c='Atención a Clientes');
        insert tasktest;
        final Task tsk =[SELECT Id, Status, Resultado_llamada__c from Task LIMIT 1];
		test.startTest();
        MX_SB_MLT_PureCloudController_cls.updTask(tsk.id, tsk.Status,tsk.Resultado_llamada__c);
        MX_SB_MLT_PureCloudController_cls.getInfo(tsk.Id);
        System.assert(true,'Actualizado con exito!');
        test.stopTest();
    }
    /**
	* metod for testing class
	*/
    @IsTest public static void metododos() {
        final Case testCase = new Case();
        testCase.Description='asdcjnk';
        testCase.SuppliedPhone = PHONETEST;
        testCase.MX_SB_MLT_Phone__c = PHONETEST;
        testCase.Origin = 'Purecloud';
        testCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(label.MX_SB_MLT_TomaReporte).getRecordTypeId();
        final String RecordTypeIdTask = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_PureCloud').getRecordTypeId();
        insert testCase;
        
        final Task tasktest = new Task(Telefono__c=PHONETEST,RecordTypeId=RecordTypeIdTask,Subject='Call');
        insert tasktest;
        final Case testCase2 = new Case();
        testCase2.Description='asdcjnk';
        testCase2.SuppliedPhone = PHONETEST;
        testCase2.MX_SB_MLT_Phone__c = PHONETEST;
        testCase2.Origin = 'Avaya';
        testCase2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(label.MX_SB_MLT_TomaReporte).getRecordTypeId();
        insert testCase2;
        
        test.startTest();
            String strValue = MX_SB_MLT_PurecloudController_cls.getValue(testCase.id);
        	strValue = MX_SB_MLT_PurecloudController_cls.getValue(tasktest.id);
            MX_SB_MLT_PurecloudController_cls.getValue(testCase2.id);
        test.stopTest();
        System.assertEquals(PHONETEST, strValue,'No Coinciden');      
    }
        /**
	* metod for testing class
	*/
    @IsTest public static void changeStatusTest() {
        final Case testStatus = new Case();
        testStatus.Description='test status';
        testStatus.SuppliedPhone = PHONETEST;
        testStatus.MX_SB_MLT_Phone__c = PHONETEST;
        testStatus.Status = 'Nuevo';
        testStatus.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(label.MX_SB_MLT_TomaReporte).getRecordTypeId();
        insert testStatus;
        
        test.startTest();
			MX_SB_MLT_PurecloudController_cls.changeStatus(testStatus.id);
        test.stopTest();
        System.assert(true,'No se actualizo');  
    }
    
}