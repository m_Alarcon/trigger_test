@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_Siniestro_cls_TEST
* Autor Daniel Perez Lopez
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_Siniestro_cls

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           10/12/2019      Daniel Lopez                         Creación
* --------------------------------------------------------------------------------
**/
public with sharing  class MX_SB_MLT_BusquedaUsuario_tst {

     @TestSetup
    static void creaDatos() {
            
            final Account acgrua =new Account(name='Gruatest',recuperacion__c=false,Tipo_Persona__c='Física');
            insert acgrua;
            final Account acambulancia =new Account(name='Ambulanciatest',recuperacion__c=false,Tipo_Persona__c='Física');
            insert acambulancia;
            final Account acparamedico =new Account(name='Paramedicotest',recuperacion__c=false,Tipo_Persona__c='Física');
            insert acparamedico;
    }
    
    @isTest
    static void cargaIni() {
        Integer numRes;
        numRes = 0;
        test.startTest();
        MX_SB_MLT_BusquedaUsuario_cls.busquedaIni('Proveedor de Gasolina');
        MX_SB_MLT_BusquedaUsuario_cls.busquedaIni('Proveedor de Cambio de llanta');
        MX_SB_MLT_BusquedaUsuario_cls.busquedaIni('Proveedor de Paso de corriente');
        final List<Account> cuenta = MX_SB_MLT_BusquedaUsuario_cls.busquedaIni('Proveedor de Grúa');
        MX_SB_MLT_BusquedaUsuario_cls.busquedaIni('Proveedor Cristalera');
        
        system.assertNotEquals(numRes, cuenta.size(), 'cuenta valida');
        test.stopTest();
    }
    @isTest
    static void busquedaUsu() {
        Integer numUsu;
        numUsu = 0;
        List<User> usuarios = new List<User>();
        test.startTest();
        usuarios = MX_SB_MLT_BusquedaUsuario_cls.buscaUsu('Luffy');
        system.assertEquals(numUsu, usuarios.size(),'usuarios encontrados');
        test.stopTest();
        
    }
    @isTest
    static void busquedaProv() {
        Integer numPro;
        numPro=1;
         List<Account> proveedores = new List<Account>();
        test.startTest();
        proveedores = MX_SB_MLT_BusquedaUsuario_cls.buscaCuenta('Gruatest');
        system.assertEquals(numPro, proveedores.size(),'usuarios encontrados');
        test.stopTest();
    }
}