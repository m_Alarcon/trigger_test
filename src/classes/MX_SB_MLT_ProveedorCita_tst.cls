/**
 * -------------------------------------------------------------------------------
 * @File Name          : MX_SB_MLT_ProveedorCita_tst.cls
 * @Description        : Clase para la creación de Servicios de Critalera.
 * @Author             : Marco Cruz Barboza
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/24/2020   	Marco Cruz Barboza         	Creación 
**/
@IsTest
public with sharing class MX_SB_MLT_ProveedorCita_tst {
    /**
	* @description 
	* @var TIPOSERVICIO sustituye label Cristales 
	*/
	Final static String TIPOSERVICIO = 'Cristales';
    
    @TestSetup
    static void testDatos() {
        Final String labelProfile = label.MX_SB_MLT_Asesores_Multiasistencia;
        Final String testNameProfile = [SELECT Name FROM Profile WHERE Name =:labelProfile LIMIT 1].Name;
        Final User testUser = MX_WB_TestData_cls.crearUsuario('testingUser', testNameProfile);
        Insert testUser;
        
        System.runAs(testUser) {
            final Account testAccount = new Account(Name='luffy',Tipo_Persona__c='Física');
            insert testAccount;
            
            final Contract testContract = new Contract(accountid=testAccount.Id,MX_SB_SAC_NumeroPoliza__c='PolizaTest');
            insert testContract;

            final Siniestro__c siniProveedor= new Siniestro__c();
            siniProveedor.MX_SB_SAC_Contrato__c = testContract.Id;
            siniProveedor.TipoSiniestro__c = TIPOSERVICIO;
            siniProveedor.MX_SB_MLT_Lugar_de_Atencion_Cristalera__c = Label.MX_SB_MLT_Cristalera;
        	siniProveedor.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoAuto).getRecordTypeId();
           	siniProveedor.MX_SB_MLT_CristalDa_ado__c = 'Parabrisas';
            insert siniProveedor;
            
            final Siniestro__c siniAjustador= new Siniestro__c();
            siniAjustador.MX_SB_SAC_Contrato__c = testContract.Id;
            siniAjustador.TipoSiniestro__c = TIPOSERVICIO;
            siniAjustador.MX_SB_MLT_Lugar_de_Atencion_Cristalera__c = Label.MX_SB_MLT_Ajustador;
        	siniAjustador.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoAuto).getRecordTypeId();
            insert siniAjustador;
            
        } 
    }
    
    @isTest
    static void getRecordTypeTest() {
        Final String labelDevName = Label.MX_SB_MLT_RamoAuto;
        Final String expectedId = [SELECT Id FROM RecordType WHERE DeveloperName =: labelDevName].Id;
        Final Id testIdSiniestro = MX_SB_MLT_ProveedorCita_cls.getRecordType(labelDevName);
        System.assertEquals(expectedId, testIdSiniestro, 'Test esta funcionando');
    }
    
    @isTest
    static void getDatosSiniestroTest() {
        Final String assignService = Label.MX_SB_MLT_Cristalera;
        Final String siniTstId = [SELECT Id, MX_SB_MLT_Lugar_de_Atencion_Cristalera__c FROM Siniestro__c WHERE MX_SB_MLT_Lugar_de_Atencion_Cristalera__c =: assignService].Id;
        Final Siniestro__c testSiniestro = MX_SB_MLT_ProveedorCita_cls.getDatosSiniestro(siniTstId);
        System.assertEquals('Parabrisas', testSiniestro.MX_SB_MLT_CristalDa_ado__c, 'Son datos iguales, al test es un exito');
    }
    
    @isTest
    static void creaServicioTest() {
        Final String service = Label.MX_SB_MLT_Cristalera;
        Final String idTest = [SELECT Id, MX_SB_MLT_Lugar_de_Atencion_Cristalera__c FROM Siniestro__c WHERE MX_SB_MLT_Lugar_de_Atencion_Cristalera__c =: service].Id;
        Final MX_SB_MLT_ProveedorCita_cls.ServiceDateData testData = new MX_SB_MLT_ProveedorCita_cls.ServiceDateData ();
        testData.fecha = '2020-02-25T00:00:00.000+0000';
        testData.idSin = idTest;
        Final Map<String,Object> testMap = MX_SB_MLT_ProveedorCita_cls.creaServicio(JSON.serialize(testData));
        System.assert(testMap.containsKey('showMessage'),'El Mapa esta coincidiendo');
    }
    
    @isTest
    static void generaServicioProveedorTest() {
        Final String service = Label.MX_SB_MLT_Cristalera;
        Final String serviceAjustador = Label.MX_SB_MLT_Ajustador;
        Final String recordProveedor = [SELECT Id, MX_SB_MLT_Lugar_de_Atencion_Cristalera__c FROM Siniestro__c WHERE MX_SB_MLT_Lugar_de_Atencion_Cristalera__c =: service].Id;
        Final String recordAjustador = [SELECT Id, MX_SB_MLT_Lugar_de_Atencion_Cristalera__c FROM Siniestro__c WHERE MX_SB_MLT_Lugar_de_Atencion_Cristalera__c =: serviceAjustador].Id;
        Final String idProveedor = [SELECT Id, RecordTypeId FROM Account WHERE Name ='luffy'].Id;
        Final String idAjustador = [SELECT Id, Name FROM User WHERE Name = 'testingUser'].Id;

        Boolean boolAjus = false;
        Boolean boolProv = false;
            
        if(!String.isEmpty(recordProveedor) && !String.isEmpty(recordAjustador) && !String.isEmpty(idProveedor) && !String.isEmpty(idAjustador)) {
            boolAjus = MX_SB_MLT_ProveedorCita_cls.generaServicioProveedor(recordAjustador, idAjustador);
            boolProv = MX_SB_MLT_ProveedorCita_cls.generaServicioProveedor(recordProveedor, idProveedor);
        }   
        System.assertEquals(boolProv, boolAjus, 'Test class ejecución con exito');
    }
    
}