/****************************************************************************************************
Información general
------------------------
author: Omar Gonzalez
company: Ids
Project: PCI

Information about changes (versions)
-------------------------------------
Number    Dates           Author            Description
------    --------        ---------------   -----------------------------------------------------------------
1.0      07-08-2020      Omar Gonzalez      Clase test para selector MX_ASD_PCI_ServiceCotizadorIvr_Service 
****************************************************************************************************/
@isTest
public class MX_ASD_PCI_ServiceCotizadorIvrTst {
    /*
* Campo que recibe la cotización 
*/
    private static MX_ASD_PCI_IvrWrapper.MX_ASD_PCI_DatosCotizacion cobro;
    /*
* Campo que recibe la cotización 
*/
    private static Quote quoteCobro;
    /*
* Campo que almacena el nombre de la cuenta 
*/
    public static final String SNAME = 'Omar Gonzalez';
    /*
* Campo que almacena el email para la cuenta 
*/
    public static final String EMAIL = 'test@gmail.com';
    /*
* Campo que almacena el IdCotiza para la opp
*/
    public static final String IDCOTIZA = '12300912';
    /*
* Campo que almacena el FolioAntifraude para la opp
*/
    public static final String FOLANTF = '98120019';
    /*
* Campo que almacena el FoliodeCotizacion para la opp 
*/
    public static final String FOLCT = '556432';
    
   /**
* @test setup que crea los registros
**/
    @TestSetup
    public static void makeData() {
        final User testUs = MX_WB_TestData_cls.crearUsuario ( 'TesterLastName',  Label.MX_SB_VTS_ProfileIntegration );
        insert testUs;
        final Account accountRec = MX_WB_TestData_cls.crearCuenta( 'LastName', 'PersonAccount' );
        accountRec.OwnerId = testUs.Id;
        accountRec.PersonEmail = 'tst@wibe.com';
        accountRec.CodigoPostal__c = '09390';
        insert accountRec;
        final Account accounOpp = MX_WB_TestData_cls.crearCuenta ( 'TestAcc', 'PersonAccount' );
        accounOpp.OwnerId = testUs.Id;
        accounOpp.PersonEmail = 'tst2@wibe.com';
        accounOpp.CodigoPostal__c = '09698';
        insert accounOpp;
        final Opportunity oppRecTst = MX_WB_TestData_cls.crearOportunidad ( 'Tst', accountRec.Id, testUs.Id, 'ASD' );
        oppRecTst.FolioCotizacion__c = null;
        oppRecTst.Estatus__c = 'Creada';
        oppRecTst.correoElectronicoContratante__c = 'tst@wibe.com';
        oppRecTst.TelefonoCliente__c = '5569705217';
        oppRecTst.Producto__c = 'Seguro de Moto Bancomer';
        oppRecTst.Reason__c = 'Venta';
        oppRecTst.MX_SB_VTS_Aplica_Cierre__c = true;
        oppRecTst.StageName=System.Label.MX_SB_PS_Etapa1;
        insert oppRecTst;
        final Product2 prod2 = MX_WB_TestData_cls.productNew('Auto Seguro Dinámico');
        prod2.IsActive=true;
        Insert prod2;
        final PricebookEntry lstProd = MX_WB_TestData_cls.priceBookEntryNew(prod2.Id);
        Insert lstProd;
        final Pricebook2 priceA2 = MX_WB_TestData_cls.createStandardPriceBook2();
        final Quote cotizacion = new Quote(OpportunityId=oppRecTst.Id, Name=SNAME,Pricebook2Id= priceA2.Id,status='Emitida',MX_SB_VTS_Numero_de_Poliza__c='ASD1234567');
        cotizacion.MX_SB_VTS_Folio_Cotizacion__c=FOLCT;
        insert cotizacion;
    }
    @isTest
    static void tetUpsquot() {
        Final Opportunity opptId =[SELECT Id from Opportunity where StageName=:System.Label.MX_SB_PS_Etapa1];
        Final Quote quotOb= [SELECT Id, OpportunityId from Quote where OpportunityId=:opptId.Id];
        test.startTest();
        	MX_RTL_Quote_Service.getUpsquot(quotOb);
                system.assert(true,'Se ha actualizado una cotizacion existente');
        test.stopTest();
    }
    @isTest static void serviceEnlaceIvrOne() {
        Cobro = new MX_ASD_PCI_IvrWrapper.MX_ASD_PCI_DatosCotizacion();
        Cobro.folioAntiFraude=FOLANTF;
        Cobro.folioDeCotizacion=FOLCT;
        Cobro.idCotiza=IDCOTIZA;
        Cobro.producto = 'Auto Seguro Dinámico';
        quoteCobro = [select Id,Name,MX_SB_VTS_Folio_Cotizacion__c, (Select Product2.Name from QuoteLineItems where Product2.Name='Auto Seguro Dinámico')  from Quote where MX_SB_VTS_Folio_Cotizacion__c=:FOLCT];
        Test.startTest();
        final List<MX_ASD_PCI_IvrWrapper.MX_ASD_PCI_ResponseSFDC> response = MX_ASD_PCI_ServiceCotizadorIvr.serviceEnlaceIvr(Cobro);
        System.assertEquals(quoteCobro.Id, response[0].quoteId,'SUCCESS');
        Test.stopTest();
    }
    @isTest static void serviceEnlaceIvrTwo() {
        COBRO = new MX_ASD_PCI_IvrWrapper.MX_ASD_PCI_DatosCotizacion();
        COBRO.folioAntiFraude=FOLANTF;
        COBRO.folioDeCotizacion=FOLCT;
        quoteCobro = [select Id,Name,MX_SB_VTS_Folio_Cotizacion__c from Quote where MX_SB_VTS_Folio_Cotizacion__c=:FOLCT];
        Test.startTest();
        final List<MX_ASD_PCI_IvrWrapper.MX_ASD_PCI_ResponseSFDC> response = MX_ASD_PCI_ServiceCotizadorIvr.serviceEnlaceIvr(cobro);
        System.assertEquals('Error los datos son incorrectos',response[0].message,'Error los datos son incorrectos');
        Test.stopTest();
    }
    @isTest static void serviceEnlaceIvrThree() {
        quoteCobro = [select Id,Name,MX_SB_VTS_Folio_Cotizacion__c from Quote where MX_SB_VTS_Folio_Cotizacion__c=:FOLCT];
        Test.startTest();
        final List<MX_ASD_PCI_IvrWrapper.MX_ASD_PCI_ResponseSFDC> response = MX_ASD_PCI_ServiceCotizadorIvr.serviceEnlaceIvr(cobro);
        System.assertEquals(null, response[0].quoteId,'ERROR');
        Test.stopTest();
    }
}