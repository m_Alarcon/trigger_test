/**
 * @File Name          : MX_MC_MWConverter_Service_Test.cls
 * @Description        : Clase para Test de MX_MC_MWConverter_Service
 * @author             : Eduardo Barrera Martínez
 * @Group              : BPyP
 * @Last Modified By   : Eduardo Barrera Martínez
 * @Last Modified On   : 27/07/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      27/07/2020           Eduardo Barrera Martínez.       Initial Version
**/
@isTest
public with sharing class MX_MC_MWConverter_Service_Test {

    /**
    * @Description MSG_THR_ID property
    **/
    private static final String THREAD_ID = 'test_thradId_123';

    /**
    * @Description CONTENT property
    **/
    private static final String CONTENT = 'content';

    /**
    * @Description IS_DRAFT property
    **/
    private static final String IS_DRAFT = 'isDraft';


    /**
     * @Description Clase de prueba para MX_MC_MWConverter_Service
     * @author eduardo.barrera3@bbva.com | 27/07/2020
    **/
    @TestSetup
    static void makeData() {

        final Case convTest = new Case();
        convTest.mycn__MC_ConversationId__c = THREAD_ID;
        convTest.RecordTypeId = mycn.MC_ConversationsDatabase.getCaseConversationRecordType();
        convTest.Status = 'New';
        convTest.Subject = 'MC Conversation example';
        insert convTest;
        final List<iaso__GBL_Rest_Services_Url__c> restUrl = new List<iaso__GBL_Rest_Services_Url__c>();
        restUrl.add(new iaso__GBL_Rest_Services_Url__c(Name = 'MC_valueCipher',
                                                                                        iaso__Url__c = 'www.testMTWConverter.bbva',
                                                                                        iaso__Cache_Partition__c = 'iaso.ServicesPartition'));
        restUrl.add(new iaso__GBL_Rest_Services_Url__c(Name = 'MC_pushNotification',
                                                                                        iaso__Url__c = 'www.testMTWConverter.bbva',
                                                                                        iaso__Cache_Partition__c = 'iaso.ServicesPartition'));
        insert restUrl;
    }

    /**
     * @Description Clase de prueba para MX_MC_MWConverter_Service
     * @author eduardo.barrera3@bbva.com | 27/07/2020
    **/
    @isTest
    static void testInputCreateMsg() {
        final Map<String, Object> mxInput = new Map<String, Object>();
        mxInput.put('messageThreadId',THREAD_ID);
        mxInput.put(CONTENT,'Message created');
        mxInput.put(IS_DRAFT,'false');
        final MX_MC_MWConverter_Service mxConverter = new MX_MC_MWConverter_Service();
        final Map<String, Object> mxOutput = mxConverter.convertMap(mxInput);
        System.assertEquals(mxInput.get(CONTENT), mxOutput.get('message'), 'Valor asignado incorrectamente');
    }

    /**
     * @Description Clase de prueba para MX_MC_MWConverter_Service
     * @author eduardo.barrera3@bbva.com | 27/07/2020
    **/
    @isTest
    static void testInputCreateConv() {
        final MX_WB_Mock encryptMock = new MX_WB_Mock(200,'OK', '{"results":["ak3loja12ja"]}', new Map<String,String>());
        final Map<String, Object> mxInput = new Map<String, Object>();
        mxInput.put('customerId',THREAD_ID);
        mxInput.put('subject','Conversation created');
        mxInput.put(IS_DRAFT,false);
        mxInput.put(CONTENT,'Conversation test');
        final MX_MC_MWConverter_Service mxConverter = new MX_MC_MWConverter_Service();
        Test.startTest();
        iaso.GBL_Mock.setMock(encryptMock);
        final Map<String, Object> mxOutput = mxConverter.convertMap(mxInput);
        Test.stopTest();
        System.assertEquals(mxInput.get(CONTENT), mxOutput.get('message'), 'Valor asignado incorrectamente');
    }

    /**
     * @Description Clase de prueba para MX_MC_MWConverter_Service
     * @author eduardo.barrera3@bbva.com | 27/07/2020
    **/
    @isTest
    static void testOutputCreateMsg() {
        final MX_WB_Mock encryptMock = new MX_WB_Mock(200,'OK', '{"results":"PushMsg"}', new Map<String,String>());
        final Map<String,Object> inputMx = new Map<String, Object>();
        inputMx.put('location','http://bbva.test.com/conv/messages/12');
        inputMx.put('messageThreadId',THREAD_ID);
        inputMx.put(IS_DRAFT,false);
        final MX_MC_MWConverter_Service mxConverter = new MX_MC_MWConverter_Service();
        Test.startTest();
        iaso.GBL_Mock.setMock(encryptMock);
        final Object response = mxConverter.convert('resource created correctly', 'postMessageThread', inputMx);
        Test.stopTest();
        System.assert(response != null, 'La conversion es incorrecta');
    }

    /**
     * @Description Clase de prueba para MX_MC_MWConverter_Service
     * @author eduardo.barrera3@bbva.com | 27/07/2020
    **/
    @isTest
    static void testOutputCreateConv() {
        final MX_WB_Mock encryptMock = new MX_WB_Mock(200,'OK', '{"results":"PushConv"}', new Map<String,String>());
        final Map<String,Object> inputMx = new Map<String, Object>();
        inputMx.put('location','http://bbva.test.com/conv/15');
        inputMx.put('subject','Test Conversation');
        inputMx.put('customerId','Customer test');
        inputMx.put(IS_DRAFT,false);
        final MX_MC_MWConverter_Service mxConverter = new MX_MC_MWConverter_Service();
        Test.startTest();
        iaso.GBL_Mock.setMock(encryptMock);
        final Object response = mxConverter.convert('resource created correctly', 'postThread', inputMx);
        Test.stopTest();
        System.assert(response != null, 'La conversion es incorrecta');
    }
}