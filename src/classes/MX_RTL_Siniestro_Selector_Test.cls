@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_RTL_Siniestro_Selector_Test
* Autor: Eder Alberto Hernández Carbajal
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_RLT_Siniestro_Selector

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         26/05/2020   Eder Alberto Hernández Carbajal              Creación
* -----------------------------------------------------------------------------------------------
*/
public with sharing class MX_RTL_Siniestro_Selector_Test {
    
    /*
	* var String Valor Subramo Ahorro
	*/
    public static final String SUBRAMO ='Ahorro';
    
    /** Nombre de usuario Test */
    final static String RECORDTYPE = 'MX_SB_MLT_RamoVida';
    
	/** Nombre de usuario Test */
    final static String NAME = 'Test_bulk';
    
    /** current user id */
    final static String USR_ID = UserInfo.getUserId();
    /**
     * @description
     * @author Eder Hernández | 5/26/2020
     * @return void
     **/
    @TestSetup
    static void createData() {
        Final String profiselector = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User userVida = MX_WB_TestData_cls.crearUsuario(NAME, profiselector);  
        insert userVida;
        System.runAs(userVida) {
            final String recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Proveedores_MA).getRecordTypeId();
            final Account accountVidaTest = new Account(RecordTypeId = recordTypeId, Name=NAME, Tipo_Persona__c='Física');
            insert accountVidaTest;
            
            final Contract contractVida = new Contract(AccountId = accountVidaTest.Id, MX_SB_SAC_NumeroPoliza__c='policyTest');
            insert contractVida;
            
            
            Final List<Siniestro__c> sinInsert = new List<Siniestro__c>();
            for(Integer i = 0; i < 10; i++) {
                Final Siniestro__c nSinselector = new Siniestro__c();
                nSinselector.MX_SB_SAC_Contrato__c = contractVida.Id;
                nSinselector.MX_SB_MLT_NombreConductor__c = NAME + '_' + i;
                nSinselector.MX_SB_MLT_APaternoConductor__c = 'LastName for '+NAME;
                nSinselector.MX_SB_MLT_AMaternoConductor__c = 'LastName2 for '+NAME;
                nSinselector.MX_SB_MLT_Fecha_Hora_Siniestro__c =date.valueof('2020-03-27T22:04:00.000+0000');
                nSinselector.MX_SB_MLT_Telefono__c = '5534253647';
                Final datetime citaAgenda = datetime.now();
                nSinselector.MX_SB_MLT_CitaVidaAsegurado__c = citaAgenda.addDays(2);
                nSinselector.MX_SB_MLT_AtencionVida__c = 'Agendar Cita';
                nSinselector.MX_SB_MLT_PreguntaVida__c = false;
                nSinselector.MX_SB_MLT_SubRamo__c = SUBRAMO;
                nSinselector.TipoSiniestro__c = 'Siniestros';
                nSinselector.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_creacion;
                nSinselector.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoVida).getRecordTypeId();
                sinInsert.add(nSinselector);
            }
            Database.insert(sinInsert);
        }
    }
    /*
    * @description method que prueba MX_RTL_Siniestro_Selector constructor
    * @param  void
    * @return void
    */
    @isTest
    private static void testConstructor() {
        Final MX_RTL_Siniestro_Selector ctrlSelector = new MX_RTL_Siniestro_Selector();
        system.assert(true,ctrlSelector);
    }
    /*
    * @description method que prueba MX_RTL_Siniestro_Selector getSiniestroById
    * @param  void
    * @return void 
    */
    @isTest
    static void testGetSiniestroById() {
        Final List<Siniestro__c> listSin = [SELECT Id FROM Siniestro__c limit 10];
        Final Set<Id> sinIds = new Set<Id>();
        for(Siniestro__c sin : listSin) {
            sinIds.add(sin.Id);
        }
		Final List<Siniestro__c> testSin = MX_RTL_Siniestro_Selector.getSiniestroById(sinIds);
        System.assertEquals(testSin.size(), 10, 'Failed!');
    }
    /*
    * @description method que prueba MX_RTL_Siniestro_Selector updateSiniestro
    * @param  void
    * @return void 
    */
    @isTest
    static void testUpdateSiniestro() {
        Final Siniestro__c updateSin = [SELECT Id, MX_SB_MLT_NombreConductor__c FROM Siniestro__c where MX_SB_MLT_NombreConductor__c = :NAME + '_3'];
        updateSin.MX_SB_MLT_NombreConductor__c = 'Update_Test';
		test.startTest();
        	MX_RTL_Siniestro_Selector.updateSiniestro(updateSin);
        test.stopTest();
        System.assert(true, 'El registro se actualizó con éxito!');
    }
	/*
    * @description method que prueba MX_RTL_Siniestro_Service srchRecordTId
    * @param  void
    * @return void
    */
    @isTest
    static void testsrchRecordTId() {
        MX_RTL_Siniestro_Selector.srchRecordTId(RECORDTYPE);
        System.assert(true, 'Busqueda exitosa!');
    }
    /*
    * @description method que prueba MX_RTL_Siniestro_Selector upsertSiniestro
    * @param  void
    * @return void 
    */
    @isTest
    static void testUpsertSiniestro() {
        Final Siniestro__c upsertSin = [SELECT Id, MX_SB_MLT_Telefono__c FROM Siniestro__c where MX_SB_MLT_NombreConductor__c = :NAME + '_7'];
        upsertSin.MX_SB_MLT_Telefono__c = '5527674618';
		test.startTest();
        	MX_RTL_Siniestro_Selector.upsertSiniestro(upsertSin);
        test.stopTest();
        System.assert(true, 'El registro se actualizó con éxito!');
    }
}