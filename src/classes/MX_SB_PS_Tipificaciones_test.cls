/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_Tipificaciones_test
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
*1.0					Julio Medellín                 25/06/2020                 Creación de la clase.
*1.1                    Juan Carlos Benitez             15/12/2020               Se añade Methodo getTaskDataTest
*1.2                    Juan Carlos Benitez             15/12/2020              fetchOpp para obtener info de opp
*1.3                    Juan Carlos Benitez             15/12/2020              upsrtTaskTest prueba actualizacion de tarea
*/

@istest
public class MX_SB_PS_Tipificaciones_test {
/*String contrfieldApiName*/
final static string CONTRFAPIN = 'MX_SB_VTS_Tipificacion_LV1__c';
/*String depfieldApiName*/
final static string DEPFAPIN = 'MX_SB_VTS_Tipificacion_LV2__c';

    /*Constructor for test class*/
	private MX_SB_PS_Tipificaciones_test() { }
	/*Setup de datos de prueba*/
	@TestSetup
    static void makeData() {
	        final Account acc =  new Account(Name='AccountTest');
            Insert acc;
            final Opportunity oppToTest = new Opportunity(Name= 'Opp1',AccountId=acc.Id ,StageName=System.Label.MX_SB_PS_Etapa1,Producto__c='Hogar', CloseDate=System.today());
            insert oppToTest;
            final MX_SB_VTS_Generica__c[] graList =  new MX_SB_VTS_Generica__c[]{};
             for(Integer i =0;i<3;i++) {
             final MX_SB_VTS_Generica__c Gerica = new MX_SB_VTS_Generica__c( MX_SB_VTS_Src__c='Prefiere otros productos',MX_SB_VTS_Status__c='value'+i, Name='codevalue'+i, MX_SB_VTS_Type__c = 'FLDTY',MX_SB_VTS_HREF__c='Contacto,Contacto Efectivo,Interesado,Acepta cotización,No Venta');
             graList.add(Gerica);
             }
            insert graList;
            Final List<Task> tskInsert = new List<Task>();
            	for(Integer i = 0; i < 2; i++) {
	                Final Task ntsk = new Task();
    	            ntsk.Telefono__c = '5534253647';
                    ntsk.whatId =oppToTest.Id;
        	        ntsk.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_PureCloud').getRecordTypeId();
            	    tskInsert.add(ntsk);
            	}
                Database.insert(tskInsert);
        
    }
          /*Methodo de pruebas positiva1*/
        public testMethod static void  prueba1() {              
        final Opportunity  opp  = [SELECT ID FROM Opportunity LIMIT 1];
		test.startTest();
            try {
        	MX_SB_PS_Tipificaciones.configType(opp.Id);
        	MX_SB_PS_Tipificaciones.getOppData(opp.Id);
			MX_SB_PS_Tipificaciones.getDependentMap(opp,CONTRFAPIN,DEPFAPIN); 
        	MX_SB_PS_Tipificaciones.saveType(opp.Id,'Prefiere otros productos');
            } catch(Exception e) {
				final Boolean expectedE =  e.getMessage().contains('value') ? true : false;
    	        System.Assert(expectedE, 'Salida Exitosa');
            }
        test.stopTest();
    }    
        
        /**
        * @description 
        * @author Daniel Ramirez Islas | 11-18-2020 
        **/
    @IsTest
        static void getTaskDataTest() {
            
            Test.startTest();
            final Opportunity opp = [Select id FROM Opportunity Limit 1];
            final List<Task> selectTaskById = MX_SB_PS_Tipificaciones.getTaskData(opp.Id);
            System.assert(!selectTaskById.isEmpty(), 'Tarea encontrada');
            Test.stopTest();
            
        }
        /**
* @description 
* @author Juan Carlos Benitez | 12-01-2021 
**/
    @isTest
    static void fetchOpp() {
        final Opportunity  opp  = [SELECT ID FROM Opportunity LIMIT 1];
        test.startTest();
	        final Integer entero=MX_SB_PS_Tipificaciones.fetchOpp(opp.Id,'Opportunity','MX_SB_VTS_Tipificacion_LV1__c,MX_SB_VTS_Tipificacion_LV2__c');
        	system.assertEquals(entero,0,'Salida exitosa');
        test.stopTest();
    }
        
/**
* @description 
* @author Daniel Ramirez Islas | 11-18-2020 
**/
@IsTest
static void upsrtTaskTest() {
    
    Test.startTest();
    final Task tarea = [SELECT id, Telefono__c FROM Task Limit 1];
    tarea.Telefono__c = '5537334045';
    MX_SB_PS_Tipificaciones.upsrtTask(tarea,'Llamada');
    final Task tarea2 = [SELECT id, Telefono__c FROM Task WHERE id =: tarea.id];
    System.assertEquals(tarea.Telefono__c,tarea2.Telefono__c,'Tarea Actualizada');
    Test.stopTest();
    
}
}