/*
* @Nombre: MX_RTL_Siniestro_Selector.cls
* @Autor: Eder Alberto Hernández Carbajal
* @Proyecto: Siniestros - BBVA
* @Descripción : Clase de la capa de acceso a datos del objeto Siniestro
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   	Descripción
* --------------------------------------------------------------------------------
* 1.0         26/05/2020    Eder Alberto Hernández Carbajal		Creación
* --------------------------------------------------------------------------------
*/
public with sharing class MX_RTL_Siniestro_Selector {
    
    /** Lista de registros de Siniestro */
    final static List<sObject> RETURN_SIN = new List<sObject>();
    
	/** constructor */
    @TestVisible
    private MX_RTL_Siniestro_Selector() { }
    
    /*
    * @description Actualiza siniestro
    * @param Set<Id> SinIds
    */
    public static void updateSiniestro(Siniestro__c siniestro) {
        UPDATE siniestro;
    }
    /*
    * @description Busqueda de datos del siniestro por Id
    * @param Set<Id> SinIds
    */
    public static List<Siniestro__c> getSiniestroById(Set<Id> sinIds) {
        RETURN_SIN.addAll([SELECT Id, Name, MX_SB_MLT_Telefono__c, MX_SB_MLT_NombreConductor__c,MX_SB_MLT_APaternoConductor__c, MX_SB_MLT_AMaternoConductor__c,
                           MX_SB_MLT_NombreAsegurado__c, MX_SB_MLT_InsuredLastName2__c, MX_SB_MLT_InsuredLastName__c, MX_SB_MLT_CorreoElectronico__c,
                           CreatedDate, MX_SB_MLT_InsuranceCompany__c, MX_SB_MLT_SubRamo__c, TipoSiniestro__c, MX_SB_MLT_NameDefunct__c, MX_SB_MLT_NamePDefunct__c,
                           MX_SB_MLT_NameMDefunct__c, MX_SB_MLT_Relationship__c, MX_SB_MLT_Subtipo__c, MX_SB_MLT_DateDifunct__c, MX_SB_MLT_Reembolso__c, MX_SB_MLT_MotivoReembolso__c,
                           MX_SB_SAC_Contrato__r.MX_SB_SAC_NombreClienteAseguradoText__c, MX_SB_SAC_Contrato__r.MX_WB_apellidoPaternoAsegurado__c,
                           MX_SB_SAC_Contrato__r.MX_WB_apellidoMaternoAsegurado__c, MX_SB_SAC_Contrato__r.MX_SB_SAC_EmailAsegurado__c, MX_SB_MLT_FechaNacimiento__c,
                           MX_SB_MLT_Curp__c, MX_SB_MLT_FechaNacConductor__c,MX_SB_MLT_DireccionDestino__c, MX_SB_MLT_Address__c, Fechasiniestro__c, MX_SB_MLT_CodOcupacion__c,
                           MX_SB_MLT_Ocupacion__c,MX_SB_MLT_ICD__c,MX_SB_MLT_CodICD__c,MX_SB_MLT_ClaimNumber__c,MX_SB_MLT_CausaSiniestro__c,CreatedBy.Name,recordType.Name,
                           MX_SB_SAC_Contrato__r.MX_WB_Producto__r.Name,MX_SB_MLT_JourneySin__c,MX_SB_SAC_Contrato__c,MX_SB_SAC_Contrato__r.StartDate, MX_SB_SAC_Contrato__r.EndDate,
                           MX_SB_MLT_PublicId_ICD__c,MX_SB_MLT_KitVida__c,MX_SB_MLT_Q_EnvioCorreo__c,Folio__c,Linea_de_Negocio__c,MX_SB_MLT_RFC__c,MX_SB_MLT_RegistroEmpleado__c,
                           MX_SB_MLT_Certificado_Inciso__c,Poliza__c,MX_SB_MLT_Placas__c,MX_SB_MLT_TelefonodelAsegurado__c,RecordType.DeveloperName
                           FROM Siniestro__c WHERE id in :sinIds]);
        return RETURN_SIN;
    }
    /*
    * @description Busqueda de id del recordtype por developername
    * @param String devName
    */
    public static String srchRecordTId(String devName) {
        return [SELECT Id FROM RecordType  Where SobjectType = 'Siniestro__c' and DeveloperName =:devName].Id;
    }
    /*
	* @description Actualiza o inserta el siniestro
	* @param Siniestro__c sinIn
	*/
    public static Siniestro__c upsertSiniestro(Siniestro__c sinIn) {
        UPSERT sinIn;
        Return sinIn;
    }

    /*
	* @description consulta siniestros porpoliza y tipo de producto
	* @param fields poliza producto
	*/
    public static Siniestro__c[] getSiniestroByProducto(String fields,String poliza, String producto) {
        return Database.query(string.escapeSingleQuotes('SELECT ' + fields +' FROM Siniestro__c WHERE MX_SB_SAC_Contrato__r.MX_SB_SAC_NumeroPoliza__c=:poliza AND MX_SB_SAC_Contrato__r.MX_WB_Producto__r.Name=:producto'));
    }
}