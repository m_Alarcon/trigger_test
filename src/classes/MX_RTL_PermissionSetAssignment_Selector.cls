/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RTL_PermissionSetAssignment_Selector
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-10-19
* @Description 	PermissionSetAssignment Selector
* @Changes
*  
*/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_RTL_PermissionSetAssignment_Selector {
    
    /**
    * @Description 	Regresa el listado de permissionsets en base a userId y APIName
    * @Params       userId String, permissionSetName String 
    * @Return 		List<PermissionSetAssignment>
    */
    public static List<PermissionSetAssignment> selectByUserIdWithName(String userId, String permissionSetName) {
        return [SELECT Id, PermissionSetId, Assignee.Id FROM PermissionSetAssignment WHERE AssigneeId =:userId AND PermissionSet.Name =:permissionSetName];
    }

}