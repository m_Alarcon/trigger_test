/*
* @Nombre: MX_SB_MLT_Vida_Service.cls
* @Autor: Eder Alberto Hernández Carbajal
* @Proyecto: Siniestros - BBVA
* @Descripción : Clase Service para la clase MX_RTL_Siniestro_Selector.cls
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   	Descripción
* --------------------------------------------------------------------------------
* 1.0         26/05/2020    Eder Alberto Hernández Carbajal		Creación
* --------------------------------------------------------------------------------
*/
global with sharing class MX_RTL_Siniestro_Service { //NOSONAR
    
    /** var String tipo siniestro */
    public static final String TIPO = 'Siniestros';
    
    /** var String Valor boton cancelar */
    public static final String CANCELAR = 'Cancelar';
    
    /** var String Valor boton volver */
    public static final String VOLVER = 'Volver';
    
	/** constructor */
    @TestVisible
    private MX_RTL_Siniestro_Service() { }
    /*
    * @description Busqueda de datos del siniestro
    * @param Set<Id> SinIds
    */
    public static List<Siniestro__c> getSiniestroData(Set<Id> sinIds) {
        return MX_RTL_Siniestro_Selector.getSiniestroById(sinIds);
    }
    /*
    * @description Actualización de Siniestro
    * @param Siniestro__c sin
    */  
    public static void actualizarSin(Siniestro__c sin) {
        final Set<Id> updateId = new Set<Id>{sin.Id};
        final List<Siniestro__c> typeSin = getSiniestroData(updateId);
        if(typeSin[0].TipoSiniestro__c == TIPO) {
            MX_RTL_Siniestro_Selector.updateSiniestro(sin);
        } else {
            sin.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_detalles;
            MX_RTL_Siniestro_Selector.updateSiniestro(sin);
        }
    }
    /*
    * @description Actualización de Siniestro
    * @param Siniestro__c sin
    */
    public static void sinAction(String siniestroAction,Set<Id> sinId) {
        final List<Siniestro__c> siniestro = getSiniestroData(sinId);
        if (siniestroAction == VOLVER ) {
            siniestro[0].MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_validacion;
        }
        if(siniestroAction == CANCELAR) {
            siniestro[0].MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_cerrado;
            siniestro[0].Estatus__c = 'Cancelado';
        }
        MX_RTL_Siniestro_Selector.updateSiniestro(siniestro[0]);
    }
    /*
    * @description obtiene el recordId del recordType buscado en Siniestro
    * @param String devName
    */
    public static String getRecordTId(String devName) {
        return MX_RTL_Siniestro_Selector.srchRecordTId(devName);
    }
    /*
    * @description Inserta o Actualiza el siniestro
    * @param Siniestro__c sinObject
    */
    public static Siniestro__c upsertSini(Siniestro__c sinIn) {
       return MX_RTL_Siniestro_Selector.upsertSiniestro(sinIn); 
    }
    
    /*
    * @description consulta siniestros por producto
    * @param field poliza producto
    */
    public static Siniestro__c[] getSinByProduct(String fields,String poliza,String producto) {
        return MX_RTL_Siniestro_Selector.getSiniestroByProducto(fields,poliza,producto);
    }
}