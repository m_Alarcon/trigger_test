/**
 * @File Name          : MX_SB_VTS_GetSetPPForm_Service.cls
 * @Description        : Clase encargada de controlar las acciones para el fomulario: Personaliza tu Protección
 * @Author             : Alexandro Corzo
 * @Group              : 
 * @Last Modified By   : Alexandro Corzo
 * @Last Modified On   : 25/06/2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0       25/06/2020      Alexandro Corzo        Initial Version
**/
public without sharing class MX_SB_VTS_GetSetPPForm_Service {
    /**
     * Constructor
     */
    @SuppressWarnings('sf:UseSingleton')
    private MX_SB_VTS_GetSetPPForm_Service() {}

    /**
     * @description: Recupera las coberturas disponibles para las opciones: Completa, Balanceada y Básica
     * @author: Alexandro Corzo
     * @return: 
     */
    @AuraEnabled
    public static Map<String,List<Object>> dataCoberturas() {
        final HttpResponse oRequest = new HttpResponse();
        oRequest.setBody('{"Cobertura-Opt1":' + 
                            '[' + 
                                '{"key": "Opt1-ASMC-key", "code": "Opt1-ASMC", "codetooltip": "Opt1-ASMC-tooltip", "description": "Mejorando tu casa", "detailedDescription": "", "group": "Asistencias", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "asistencias": true, "montocobertura": "1000.00", "tooltip": "Cuentas con ayuda de especialistas por si en tu día a día necesitas: plomería, electricidad, cerrajería, albañilería, jardinería y más."},' +
                                '{"key": "Opt1-ASMD-key", "code": "Opt1-ASMD", "codetooltip": "Opt1-ASMD-tooltip", "description": "Médica", "detailedDescription": "", "group": "Asistencias", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "asistencias": true, "montocobertura": "1000.00", "tooltip": "Cuentas con asesoría médica telefónica, médico a tu domicilio y traslados en ambulancia (terrestre o aérea)."},' +
                                '{"key": "Opt1-ASMA-key", "code": "Opt1-ASMA", "codetooltip": "Opt1-ASMA-tooltip", "description": "Mascotas", "detailedDescription": "", "group": "Asistencias", "visibleIndicator": true, "exclusivoIndicator": false, "mandatoryIndicator": false, "aliasCriterion": "", "asistencias": true, "montocobertura": "1000.00", "tooltip": "Cuentas con asesoría médica telefónica, médico a tu domicilio y traslados en ambulancia (terrestre o aérea)."},' +
                                '{"key": "Opt1-COIN-key", "code": "Opt1-COIN", "codetooltip": "Opt1-COIN-tooltip", "description": "Incendio", "detailedDescription": "SA: (50% madre)", "group": "Contenidos", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "contenidos": true, "montocobertura": "1000.00", "tooltip": "Cubre daños ocasionados a las cosas que estén dentro de tu casa por: incendio o rayo."},' +
                                '{"key": "Opt1-COTE-key", "code": "Opt1-COTE", "codetooltip": "Opt1-COTE-tooltip", "description": "Terremoto", "detailedDescription": "SA: (50% madre)", "group": "Contenidos", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "contenidos": true, "montocobertura": "1000.00", "tooltip": "Cubre daños ocasionados a las cosas que estén dentro de tu casa por: teremoto o erupción volcánica."},' +
                                '{"key": "Opt1-CODN-key", "code": "Opt1-CODN", "codetooltip": "Opt1-CODN-tooltip", "description": "Desastres naturales por agua", "detailedDescription": "SA: (50% madre)", "group": "Contenidos", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "contenidos": true, "montocobertura": "1000.00", "tooltip": "Cubre daños ocasionados a las cosas que estén dentro de tu casa por: inundación por lluvia, granizo, huracán entre otros."},' +
                                '{"key": "Opt1-CORB-key", "code": "Opt1-CORB", "codetooltip": "Opt1-CORB-tooltip", "description": "Robo", "detailedDescription": "* Pertenencias (25% madre) * Articulos de valor (15% madre) * Dinero en efectivo ($ 15,000)", "group": "Contenidos", "visibleIndicator": true, "exclusivoIndicator": false, "mandatoryIndicator": false, "aliasCriterion": "", "contenidos": true, "montocobertura": "1000.00", "tooltip": "Protege objetos dentro de tu hogar como: Televisiones, LapTops, celulares, joyas, obras de arte, artículos deportivos, dinero en efectivo y más."},' +
                                '{"key": "Opt1-COEE-key", "code": "Opt1-COEE", "codetooltip": "Opt1-COEE-tooltip", "description": "EE y Linea Blanca", "detailedDescription": "SA: (15% madre)", "group": "Contenidos", "visibleIndicator": true, "exclusivoIndicator": false, "mandatoryIndicator": false, "aliasCriterion": "", "contenidos": true, "montocobertura": "1000.00", "tooltip": "Protege los daños de tu pantalla, tablet, equipo de audio, consola de videojuego, centro de lavado, refrigerador, cafetera, entre otros."},' +
                                '{"key": "Opt1-CNIN-key", "code": "Opt1-CNIN", "codetooltip": "Opt1-CNIN-tooltip", "description": "Incendio", "detailedDescription": "SA: (100% madre)", "group": "Construccion", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "construccion": true, "montocobertura": "1000.00", "tooltip": "Cubre daños ocasionados a las cosas que estén dentro de tu casa por: incendio o rayo."},' +
                                '{"key": "Opt1-CNTE-key", "code": "Opt1-CNTE", "codetooltip": "Opt1-CNTE-tooltip", "description": "Terremoto", "detailedDescription": "SA: (100% madre)", "group": "Construccion", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "construccion": true, "montocobertura": "1000.00", "tooltip": "Cubre daños ocasionados a las cosas que estén dentro de tu casa por: teremoto o erupción volcánica."},' +
                                '{"key": "Opt1-CNDN-key", "code": "Opt1-CNDN", "codetooltip": "Opt1-CNDN-tooltip", "description": "Desastres naturales por agua", "detailedDescription": "SA: (100% madre)", "group": "Construccion", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "construccion": true, "montocobertura": "1000.00", "tooltip": "Cubre daños ocasionados a las cosas que estén dentro de tu casa por: inundación por lluvia, granizo, huracán entre otros."},' +
                                '{"key": "Opt1-CNCI-key", "code": "Opt1-CNCI", "codetooltip": "Opt1-CNCI-tooltip", "description": "Civil / Daños a terceros", "detailedDescription": "SA: (100% madre)", "group": "Construccion", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "construccion": true, "montocobertura": "1000.00", "tooltip": "Cubrimos los daños ocasionados por ti, tu familia o hasta tus mascotas a bienes o personas dentro y fuera de tu hogar."},' +
                                '{"key": "Opt1-CNRC-key", "code": "Opt1-CNRC", "codetooltip": "Opt1-CNRC-tooltip", "description": "Rotura de cristales", "detailedDescription": "SA: (15% madre)", "group": "Construccion", "visibleIndicator": true, "exclusivoIndicator": false, "mandatoryIndicator": false, "aliasCriterion": "", "construccion": true, "montocobertura": "1000.00", "tooltip": "Cubre la rotura accidental de vidrios o cristales interiores y exteriores como: ventanas, canceles, espejos y más."}' +
                            '],' + 
                        '"Cobertura-Opt2":' +
                            '[' +
                                '{"code": "Opt2-ASMC", "codetooltip": "Opt2-ASMC-tooltip", "description": "Mejorando tu casa", "detailedDescription": "", "group": "Asistencias", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "asistencias": true, "montocobertura": "1000.00", "tooltip": "Cuentas con ayuda de especialistas por si en tu día a día necesitas: plomería, electricidad, cerrajería, albañilería, jardinería y más."},' +
                                '{"code": "Opt2-ASMD", "codetooltip": "Opt2-ASMD-tooltip", "description": "Médica", "detailedDescription": "", "group": "Asistencias", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "asistencias": true, "montocobertura": "1000.00", "tooltip": "Cuentas con asesoría médica telefónica, médico a tu domicilio y traslados en ambulancia (terrestre o aérea)."},' +
                                '{"code": "Opt2-ASMA", "codetooltip": "Opt2-ASMA-tooltip", "description": "Mascotas", "detailedDescription": "", "group": "Asistencias", "visibleIndicator": true, "exclusivoIndicator": false, "mandatoryIndicator": false, "aliasCriterion": "", "asistencias": true, "montocobertura": "1000.00", "tooltip": "Cuentas con asesoría médica telefónica, médico a tu domicilio y traslados en ambulancia (terrestre o aérea)."},' +
                                '{"code": "Opt2-COIN", "codetooltip": "Opt2-COIN-tooltip", "description": "Incendio", "detailedDescription": "SA: (50% madre)", "group": "Contenidos", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", " ": true, "montocobertura": "1000.00", "tooltip": "Cubre daños ocasionados a las cosas que estén dentro de tu casa por: incendio o rayo."},' +
                                '{"code": "Opt2-COTE", "codetooltip": "Opt2-COTE-tooltip", "description": "Terremoto", "detailedDescription": "SA: (50% madre)", "group": "Contenidos", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "contenidos": true, "montocobertura": "1000.00", "tooltip": "Cubre daños ocasionados a las cosas que estén dentro de tu casa por: teremoto o erupción volcánica."},' +
                                '{"code": "Opt2-CODN", "codetooltip": "Opt2-CODN-tooltip", "description": "Desastres naturales por agua", "detailedDescription": "SA: (50% madre)", "group": "Contenidos", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "contenidos": true, "montocobertura": "1000.00", "tooltip": "Cubre daños ocasionados a las cosas que estén dentro de tu casa por: inundación por lluvia, granizo, huracán entre otros."},' +
                                '{"code": "Opt2-CORB", "codetooltip": "Opt2-CORB-tooltip", "description": "Robo", "detailedDescription": "* Pertenencias (25% madre) * Articulos de valor (15% madre) * Dinero en efectivo ($ 15,000)", "group": "Contenidos", "visibleIndicator": true, "exclusivoIndicator": false, "mandatoryIndicator": false, "aliasCriterion": "", "contenidos": true, "montocobertura": "1000.00", "tooltip": "Protege objetos dentro de tu hogar como: Televisiones, LapTops, celulares, joyas, obras de arte, artículos deportivos, dinero en efectivo y más."},' +
                                '{"code": "Opt2-COEE", "codetooltip": "Opt2-COEE-tooltip", "description": "EE y Linea Blanca", "detailedDescription": "SA: (15% madre)", "group": "Contenidos", "visibleIndicator": true, "exclusivoIndicator": false, "mandatoryIndicator": false, "aliasCriterion": "", "contenidos": true, "montocobertura": "1000.00", "tooltip": "Protege los daños de tu pantalla, tablet, equipo de audio, consola de videojuego, centro de lavado, refrigerador, cafetera, entre otros."}' +
                            '],' + 
                        '"Cobertura-Opt3":' +
                            '[' + 
                                '{"code": "Opt3-ASMC", "codetooltip": "Opt3-ASMC-tooltip", "description": "Mejorando tu casa", "detailedDescription": "", "group": "Asistencias", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "asistencias": true, "montocobertura": "1000.00", "tooltip": "Cuentas con ayuda de especialistas por si en tu día a día necesitas: plomería, electricidad, cerrajería, albañilería, jardinería y más."},' +
                                '{"code": "Opt3-ASMD", "codetooltip": "Opt3-ASMD-tooltip", "description": "Médica", "detailedDescription": "", "group": "Asistencias", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "asistencias": true, "montocobertura": "1000.00", "tooltip": "Cuentas con asesoría médica telefónica, médico a tu domicilio y traslados en ambulancia (terrestre o aérea)."},' +
                                '{"code": "Opt3-ASMA", "codetooltip": "Opt3-ASMA-tooltip", "description": "Mascotas", "detailedDescription": "", "group": "Asistencias", "visibleIndicator": true, "exclusivoIndicator": false, "mandatoryIndicator": false, "aliasCriterion": "", "asistencias": true, "montocobertura": "1000.00", "tooltip": "Cuentas con asesoría médica telefónica, médico a tu domicilio y traslados en ambulancia (terrestre o aérea)."},' +
                                '{"code": "Opt3-COIN", "codetooltip": "Opt3-COIN-tooltip", "description": "Incendio", "detailedDescription": "SA: (100% madre)", "group": "Construccion", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "construccion": true, "montocobertura": "1000.00", "tooltip": "Cubre daños ocasionados a las cosas que estén dentro de tu casa por: incendio o rayo."},' +
                                '{"code": "Opt3-COTE", "codetooltip": "Opt3-COTE-tooltip", "description": "Terremoto", "detailedDescription": "SA: (100% madre)", "group": "Construccion", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "construccion": true, "montocobertura": "1000.00", "tooltip": "Cubre daños ocasionados a las cosas que estén dentro de tu casa por: teremoto o erupción volcánica."},' +
                                '{"code": "Opt3-CODN", "codetooltip": "Opt3-CODN-tooltip", "description": "Desastres naturales por agua", "detailedDescription": "SA: (100% madre)", "group": "Construccion", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "construccion": true, "montocobertura": "1000.00", "tooltip": "Cubre daños ocasionados a las cosas que estén dentro de tu casa por: inundación por lluvia, granizo, huracán entre otros."},' +
                                '{"code": "Opt3-CNCI", "codetooltip": "Opt3-CNCI-tooltip", "description": "Civil / Daños a terceros", "detailedDescription": "SA: (100% madre)", "group": "Construccion", "visibleIndicator": true, "exclusivoIndicator": true, "mandatoryIndicator": true, "aliasCriterion": "", "construccion": true, "montocobertura": "1000.00", "tooltip": "Cubrimos los daños ocasionados por ti, tu familia o hasta tus mascotas a bienes o personas dentro y fuera de tu hogar."},' +
                                '{"code": "Opt3-CNRC", "codetooltip": "Opt3-CNRC-tooltip", "description": "Rotura de cristales", "detailedDescription": "SA: (15% madre)", "group": "Construccion", "visibleIndicator": true, "exclusivoIndicator": false, "mandatoryIndicator": false, "aliasCriterion": "", "construccion": true, "montocobertura": "1000.00", "tooltip": "Cubre la rotura accidental de vidrios o cristales interiores y exteriores como: ventanas, canceles, espejos y más."}' +
                            ']' + 
                        '}');
        final  Map<String, Object> oResults = (Map<String, Object>) JSON.deserializeUntyped(oRequest.getBody());
        final Map<String, List<Object>> oReturnVals = new Map<String, List<Object>>();
        final List<Object> oCoberturaOpt1 = (List<Object>) oResults.get('Cobertura-Opt1');
        final List<Object> oCoberturaOpt2 = (List<Object>) oResults.get('Cobertura-Opt2');
        final List<Object> oCoberturaOpt3 = (List<Object>) oResults.get('Cobertura-Opt3');
        oReturnVals.put('Cobertura-Opt1', oCoberturaOpt1);
        oReturnVals.put('Cobertura-Opt2', oCoberturaOpt2);
        oReturnVals.put('Cobertura-Opt3', oCoberturaOpt3);
        return oReturnVals;
    }

    /**
     * @description: Recupera valores por set de Ids
     * @author Alexandro Corzo | 29/06/2020
     * @return Map<String, List<Object>> Mapa de valores recuperados de un JSON
     */
    public static Map<String, List<Object>> dataAmountVal() {
        final HttpResponse oRequest = new HttpResponse();
        oRequest.setBody('{"Propietario": ["2040000", "1870000", "1700000", "1545000", "1416000"], "Rentado": ["1914000", "1626000", "570000", "370000", "170000"], "Cmb": ["51400", "26000", "37000", "40800", "50090"]}');
        final Map<String, Object> oResults = (Map<String, Object>) JSON.deserializeUntyped(oRequest.getBody());
        final List<Object> oSumAsegu = (List<Object>) oResults.get('Propietario');
        final List<Object> oSumAsegu1 = (List<Object>) oResults.get('Rentado');
        final List<Object> oSumAsegu2 = (List<Object>) oResults.get('Cmb');
        final Map<String, List<Object>> oReturnVals = new Map<String, List<Object>>();
        oReturnVals.put('Propietario', oSumAsegu);
        oReturnVals.put('Rentado', oSumAsegu1);
        oReturnVals.put('Cmb', oSumAsegu2);
        return oReturnVals;
    }
}