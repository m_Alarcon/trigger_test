/*
  @File Name          : MX_SB_VTS_Carrusel_Ctrl_tst.cls
  @Description        : Clase de Prueba: Componente Carrusel
  @Author             : jesusalexandro.corzo.contractor@bbva.com
  @Group              : 
  @Last Modified By   : jesusalexandro.corzo.contractor@bbva.com
  @Last Modified On   : 06/8/2020 20:20:00
  @Modification Log   : 
  Ver               Date                Author      		                    	Modification
  1.0           	06/8/2020       	jesusalexandro.corzo.contractor@bbva.com    Initial Version
*/
@isTest
public class MX_SB_VTS_Carrusel_Ctrl_tst {
    /* Variables de Apoyo: txtModuleOppo  */
    static String txtModuleOppo = 'Opportunity';
    /* Variables de Apoyo: txtModuleLead  */
    static String txtModuleLead = 'Lead';
    /* Variables de Apoyo: txtMsg  */
    static String txtMsg = 'Se obtuvieron los datos con Exito!';
    /* Variables de Apoyo: txtMsgSave  */
    static String txtMsgSave = 'Se guardaron los datos con Exito!';
    /* Variables de Apoyo: txtSubEtapa  */
    static String txtSubEtapa = 'Contacto';
    /* Variables de Apoyo: txtStageName  */
    static String txtStageName = 'Contacto';
    /* Variables de Apoyo: txtProducto  */
    static String txtProducto = 'Seguro Estudia';
    
    /**
     * @description: Preparación de valores para clase de prueba.
     * @author: Alexandro Corzo
     */   
	@testSetup
    static void makeData() {
        User usr = null;
    	usr = MX_WB_TestData_cls.crearUsuario('TestUser', System.label.MX_SB_VTS_ProfileAdmin);        
        Insert usr;
        System.runAs(usr) {            
            final Lead leadCarrusel = MX_WB_TestData_cls.createLead('Lead Carrusel');
            leadCarrusel.FirstName = 'Usuario';
            leadCarrusel.LastName = 'De Prueba';
            leadCarrusel.LeadSource = 'Call me back';
            insert leadCarrusel;
            final Account accCarrusel = MX_WB_TestData_cls.crearCuenta('Carrusel', 'PersonAccount');
            accCarrusel.PersonEmail = 'test@test.com';
            insert accCarrusel;
            final Opportunity oppCarrusel = MX_WB_TestData_cls.crearOportunidad('Opportunity Carrusel', accCarrusel.Id, usr.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            oppCarrusel.LeadSource = 'Call me back';
            oppCarrusel.Producto__c = 'Seguro Estudia';
            insert oppCarrusel;
        }
    }
    
    /**
     * @description: Realiza prueba de recuperacion de datos: Lead
     * @author: Alexandro Corzo
     */   
    @isTest
    static void testDataComponentLead() {
        final Lead oDataLead = [SELECT Id FROM Lead WHERE LastName = 'De Prueba'];
        final Map<String, List<Object>> oDataCmpLead = MX_SB_VTS_Carrusel_Ctrl.getDataComponent(txtModuleLead, oDataLead.Id);
		System.assert(!oDataCmpLead.isEmpty(), txtMsg);
    }
    
    /**
     * @description: Realiza prueba de recuperacion de datos: Opportunity
     * @author: Alexandro Corzo
     */ 
    @isTest 
    static void testDataComponentOppo() {
        final Opportunity oDataOppo = [SELECT Id FROM Opportunity WHERE Name = 'Opportunity Carrusel'];
        final Map<String, List<Object>> oDataCmpOppo = MX_SB_VTS_Carrusel_Ctrl.getDataComponent(txtModuleOppo, oDataOppo.Id);
		System.assert(!oDataCmpOppo.isEmpty(), txtMsg);
    }
    
    /**
     * @description: Realiza prueba de almacenamiento de datos: Opportunity
     * @author: Alexandro Corzo
     */ 
    @isTest
    static void testSaveDataComponet() {
        final Opportunity oDataOppo = [SELECT Id FROM Opportunity WHERE Name = 'Opportunity Carrusel'];
        Map<String, Object> oDataComponent = null;
        Boolean isSuccess = false;
        oDataComponent = new Map<String, Object>();
        oDataComponent.put('sModule', txtModuleOppo);
        oDataComponent.put('sId', oDataOppo.Id);
        oDataComponent.put('sSubEtapa', txtSubEtapa);
        oDataComponent.put('sStageName', txtStageName);
        oDataComponent.put('sProducto', txtProducto);
        isSuccess = MX_SB_VTS_Carrusel_Ctrl.setDataComponent(oDataComponent);
        System.assert(isSuccess, txtMsgSave);
    }
    
    /**
     * @description: Obtienen los datos para el campo: SubEtapa
     * @author: Alexandro Corzo
     */ 
    @isTest
    static void testDataSubEtapaOppo() {
        final Map<String, List<Object>> oDataSubEtapa = MX_SB_VTS_Carrusel_Ctrl.obtDataSubEtapaOppo();
        System.assert(!oDataSubEtapa.isEmpty(), txtMsg);
    }
}