/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RDB_ActualizarClienteControllerTest
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2019-11-29
* @Group  		MX_RDB_ActualizarCliente
* @Description 	Test class for MX_RDB_ActualizarClienteController
* @Changes
*  
*/

@isTest
public class MX_RDB_ActualizarClienteControllerTest {
    /**
    * --------------------------------------------------------------------------------------
    * @Description Setup method for the Test Class
    * @return N/A
    **/
    @testSetup
    static void setup() {        
        //CustomSetting for WS
        MX_RDB_Utilities.createWSRecord('getCustomer', 'https://maxwebsealdesa.prev.bancomer.mx:6443/QRSV_A02/customers-/');
        MX_RDB_Utilities.createWSRecord('listElectronicInformation', 'https://maxwebsealdesa.prev.bancomer.mx:6443/QRSV_A02/customers-/');
        
        MX_RDB_Utilities.createAccountRec('TestACRDB', 'TestACRDBLast', 'D0111028');
    }
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test method for the method MX_RDB_ActualizarClienteController.getClientID
    * @return N/A
    **/
	@isTest
    static void getClientIDTest() {
        final Account clienteTest = [SELECT Id, FirstName, LastName, MX_SB_BCT_Id_Cliente_Banquero__c FROM Account WHERE FirstName = 'TestACRDB' AND LastName = 'TestACRDBLast' LIMIT 1];
        String resultNoClientID;
        Exception noClientIDExc;
        String resultClientID;
        
        Test.startTest();
        try {
            resultNoClientID = MX_RDB_ActualizarClienteController.getClientID(null);
        } catch (Exception e) {
            noClientIDExc = e;
        }
        resultClientID = MX_RDB_ActualizarClienteController.getClientID(clienteTest.Id);
        Test.stopTest();
        
        System.assertEquals(null, resultNoClientID, 'Conflicto con el null');
        System.assertEquals(clienteTest.MX_SB_BCT_Id_Cliente_Banquero__c, resultClientID, 'Conflicto al obtener clientID');
        System.assertNotEquals(null, noClientIDExc, 'Catch no en test');
    }
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test method for the method MX_RDB_ActualizarClienteController.updateClientInfo
    * @return N/A
    **/
	@isTest
    static void updateClientInfoTest() {
        final Account clienteTest = [SELECT Id, FirstName, LastName, MX_SB_BCT_Id_Cliente_Banquero__c FROM Account WHERE FirstName = 'TestACRDB' AND LastName = 'TestACRDBLast' LIMIT 1];
        String resultClientID;
        
        Test.startTest();
        resultClientID = MX_RDB_ActualizarClienteController.updateClientInfo(clienteTest.MX_SB_BCT_Id_Cliente_Banquero__c);
        Test.stopTest();
        
        System.assertEquals('OK', resultClientID, 'Conflicto en respuesta');
    }

}