/**
 * @description       : 
 * @author            : Arsenio.perez.lopez.contractor@bbva.com
 * @group             : 
 * @last modified on  : 2-11-2020
 * @last modified by  : Juan Carlos Benitez Herrera
 * Modifications Log 
 * Ver   Date         Author                                    Modification
 * 1.0   11-24-2020   Arsenio.perez.lopez.contractor@bbva.com   Initial Version
 * 1.1    2-11-2020   Juan Carlos Benitez Herrera             Se modifica product2
 * 1.2    2-24-2021   Juan Carlos Benitez Herrera       Se añade producto__c a insert y Query
**/
@isTest
public class MX_SB_PS_OpportunityTrigger_test {
    /** Nombre de usuario Test */
    final static String NAME = 'Magdalena';
    /**
    * @description 
    * @author Arsenio.perez.lopez.contractor@bbva.com | 11-24-2020 
    **/
    @TestSetup
    public static void makeData() {
        final Account acc =  MX_WB_TestData_cls.crearCuenta('AccountTestPS','PersonAccount');  
        acc.Genero__c='M';
        insert acc;
        final MX_WB_FamiliaProducto__c famil= MX_WB_TestData_cls.createProductsFamily('Vida');
        insert famil;
        final product2  productNew = MX_WB_TestData_cls.productNew('Respaldo Seguro Para Hospitalización');
        productNew.MX_SB_SAC_Proceso__c='SAC';
        productNew.Family=famil.id;
        productNew.MX_WB_FamiliaProductos__c=famil.id;
        insert productNew;
        final PricebookEntry priceBookEntryNew =MX_WB_TestData_cls.priceBookEntryNew(productNew.Id);
        insert priceBookEntryNew;
        final Opportunity opp = MX_WB_TestData_cls.crearOportunidad('Opp1',acc.Id,userInfo.getUserId(), 'MX_SB_COB_Cotizador_RS_Hospital');
        opp.RecordTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_PS_RSHospitalario).getRecordTypeId();
        opp.Producto__c='Respaldo Seguro Para Hospitalización';
        insert opp;
        final Pricebook2 prB2 = MX_WB_TestData_cls.createStandardPriceBook2();
        final Quote quoteTemp = MX_WB_TestData_cls.crearQuote(opp.Id, 'qtest', '12345');
        quoteTemp.Pricebook2Id= prB2.Id;
        quoteTemp.BillingStreet='callemamalona,mamalona,mam';
        quoteTemp.BillingPostalCode='73997';
        quoteTemp.MX_SB_VTS_Colonia__c='Salida,salida';
        quoteTemp.MX_SB_VTS_TotalMonth__c=464.98;
        quoteTemp.MX_SB_PS_PagosSubsecuentes__c=11;
        insert quoteTemp;
        final QuoteLineItem qltem = new QuoteLineItem(
            QuoteId=quoteTemp.Id,
            PricebookEntryId=priceBookEntryNew.Id,
            Quantity=1,
            MX_WB_placas__c='30204',
            UnitPrice=1,
            MX_WB_Folio_Cotizacion__c='11,11,11',
            Product2Id=productNew.Id);
        insert qltem;
        opp.SyncedQuoteId =quoteTemp.id;
        update opp;
        Final MX_SB_VTS_Beneficiario__c benef = new MX_SB_VTS_Beneficiario__c();
                benef.Name = NAME +'_1';
                benef.MX_SB_VTS_Quote__c=quoteTemp.Id;
                benef.MX_SB_MLT_NombreContacto__c =acc.PersonContactId;
                benef.MX_SB_SAC_Email__c = NAME+'_'+'@'+'bbva.com';
                benef.MX_SB_SAC_FechaNacimiento__c =date.valueOf('1996-12-12');
        		benef.MX_SB_VTS_Parentesco__c ='Cónyuge';
        insert benef;
    }
    /**
    * @description 
    * @author Daniel Alberto Ramirez Islas  | 12-14-2020 
    **/
    @isTest
    public static void testcreateQLT() {
        Test.startTest();
        final Set<String> process = new Set<String>();
        final Set<String> names = new Set<String>();
        names.add('Respaldo Seguro Para Hospitalización');
        process.add('SAC');
        final List<Opportunity> opplist = [select id,Name,RecordTypeId,producto__c from Opportunity limit 2];
        MX_SB_PS_OpportunityTrigger.createQLT(opplist);
        System.assert(!opplist.isEmpty(), 'Producto encontrado');
        test.stopTest();
    }
}