/*
*
* @aBBVA Seguros - SAC Team
* @description This class will update fields from contract object
*
*           No  |     Date     |     Author               |    Description
* @version  1.0    21/12/2019     BBVA Seguros - SAC Team     Created
* @version  1.0.1  22/12/2019     Jaime Terrats             Remove code smells
*
*/
global class MX_SB_SAC_UpdateContractFieldsBatch implements Database.Batchable<sObject>, Database.Stateful { // NOSONAR
    /**
    * @description 
    * @author Miguel Hernandez | 12/22/2019 
    * @param batchCon 
    * @return Database.QueryLocator 
    **/
    global Database.QueryLocator start(Database.BatchableContext batchCon) { // NOSONAR
        return Database.getQueryLocator('SELECT Id FROM Contract');
    }
    /**
    * @description 
    * @author Miguel Hernandez | 12/22/2019 
    * @param batchCon 
    * @param scope 
    * @return void 
    **/
    global void execute(Database.BatchableContext batchCon, List<Contract> scope) { // NOSONAR
        final List<Contract> contractsToUpdate = new List<Contract>();
        contractsToUpdate.addAll(scope);
        Database.update(contractsToUpdate);
    }    
    /**
    * @description 
    * @author Miguel Hernandez | 12/22/2019 
    * @param batchCon 
    * @return void 
    **/
    global void finish(Database.BatchableContext batchCon) { // NOSONAR
        final AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, 
            JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob
            WHERE Id = :batchCon.getJobId()];
        WB_CrearLog_cls.fnCrearLog(job.status, 'Actualización de contratos', false);
    }    
}