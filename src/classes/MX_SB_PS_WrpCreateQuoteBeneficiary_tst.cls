/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_PS_wrpCotizadorVidaResp_Test
* Autor Daniel Perez Lopez
* Proyecto: Salesforce Presuscritos
* Descripción : Prueba los Methods de la clase MX_SB_PS_wrpCotizadorVidaResp

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           10/12/2019      Daniel Lopez                         Creación
* --------------------------------------------------------------------------------
*/
@IsTest
public with sharing class MX_SB_PS_WrpCreateQuoteBeneficiary_tst {
    @IsTest
    static void obtieneWraper() {
        test.startTest();
            final MX_SB_PS_WrpCreateQuoteBeneficiary clase= new MX_SB_PS_WrpCreateQuoteBeneficiary();   
                final MX_SB_PS_WrpCreateQuoteBeneficiary.technicalInformation techinfo = new MX_SB_PS_WrpCreateQuoteBeneficiary.technicalInformation();
                    techinfo.aapType='';
                    techinfo.dateRequest='';
                    techinfo.technicalChannel='';
                    techinfo.technicalSubChannel='';
                    techinfo.branchOffice='';
                    techinfo.managementUnit='';
                    techinfo.user='';
                    techinfo.technicalIdSession='';
                    techinfo.idRequest='12345';
                    techinfo.dateConsumerInvocation='';
            clase.technicalInformation=techinfo;
                final MX_SB_PS_WrpCreateQuoteBeneficiary.insured[] lins = new MX_SB_PS_WrpCreateQuoteBeneficiary.insured[]{};
                    final MX_SB_PS_WrpCreateQuoteBeneficiary.insured ins = new MX_SB_PS_WrpCreateQuoteBeneficiary.insured();
                    ins.id='';
                    ins.entryDate='';
                        final MX_SB_PS_WrpCreateQuoteBeneficiary.beneficiary[] lben= new MX_SB_PS_WrpCreateQuoteBeneficiary.beneficiary[]{};
                            final MX_SB_PS_WrpCreateQuoteBeneficiary.beneficiary ben = new MX_SB_PS_WrpCreateQuoteBeneficiary.beneficiary();
                                final MX_SB_PS_WrpCreateQuoteBeneficiary.person pers =new MX_SB_PS_WrpCreateQuoteBeneficiary.person();
                                pers.lastName='';
                                pers.secondLastName='';
                                pers.firstName='';
                            ben.person=pers;
                                final MX_SB_PS_WrpCreateQuoteBeneficiary.relationship rela = new MX_SB_PS_WrpCreateQuoteBeneficiary.relationship();
                                rela.id='';
                            ben.relationship=rela;
                            ben.percentageParticipation='';
                                final MX_SB_PS_WrpCreateQuoteBeneficiary.relationship bene = new MX_SB_PS_WrpCreateQuoteBeneficiary.relationship();
                                bene.id='';
                            ben.beneficiaryType=bene;
                        lben.add(ben);
                    ins.beneficiaries=lben;
                lins.add(ins);
            clase.insuredList=lins;
            System.assertEquals(clase.technicalInformation.idRequest,'12345','exito en validadcion');
        test.stopTest();
    }
}