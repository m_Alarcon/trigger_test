/**
 * @File Name          : MX_MC_MDWConverter_Service_Test.cls
 * @Description        : Clase para Test de MX_MC_MDWConverter_Service
 * @author             : Eduardo Barrera Martínez
 * @Group              : BPyP
 * @Last Modified By   : Eduardo Barrera Martínez
 * @Last Modified On   : 27/07/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      27/07/2020           Eduardo Barrera Martínez.       Initial Version
**/
@isTest
public with sharing class MX_MC_MDWConverter_Service_Test {

    /**
     * @Description Clase de prueba para MX_MC_MDWConverter_Service
     * @author eduardo.barrera3@bbva.com | 27/07/2020
    **/
    @isTest
    static void getInputParams () {
        final MX_MC_MDWConverter_Service mxConverter = new MX_MC_MDWConverter_Service();
        final Map<String, Object> inputMx = new Map<String,Object>();
        inputMx.put('messageThreadId','test123');
        final Map<String, Object> outputMx = mxConverter.convertMap(inputMx);
        System.assertEquals('test123', outputMx.get('messageThreadId'), 'El resultado no es el esperado');
    }
}