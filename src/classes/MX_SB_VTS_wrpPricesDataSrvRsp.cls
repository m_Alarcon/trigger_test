/**
* @FileName          : MX_SB_VTS_wrpPricesDataSrvRsp
* @description       : Wrapper class for use de web service of Prices Response
* @Author            : Alexandro Corzo
* @last modified on  : 29-12-2020
* Modifications Log 
* Ver   Date         Author                Modification
* 1.0   29-12-2020   Alexandro Corzo       Initial Version
* 1.1   25-02-2020   Alexandro Corzo       Se agregan comentarios a la clase
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton, sf:ExcessivePublicCoun,sf:LongVariable,sf:LongVariable,sf:ExcessivePublicCount, sf:ShortVariable,sf:ShortClassName')
public class MX_SB_VTS_wrpPricesDataSrvRsp {
	/**
	 * Invocación de Wrapper
	 */
    public data data {get; set;}
    
    /**
     * @description : Clase del objeto data del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class data {
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Atributo de Clase: allianceCode */
        public String allianceCode {get; set;}
        /** Atributo de Clase: coveredArea */
        public String coveredArea {get; set;}
        /** Atributo de Clase: certifiedNumberRequested */
        public String certifiedNumberRequested {get; set;}
        /** Invocación de Clase: frequencies */
        public frequencies[] frequencies {get; set;}
        /** Atributo de Clase: region */
        public String region {get; set;}
        /** Invocación de Clase: trades */
        public trades[] trades {get; set;}
        /** Atributo de Clase: protectionLevel */
        public double protectionLevel {get; set;}
        /** Invocación de Clase: disasterCovered */
        public disasterCovered[] disasterCovered {get; set;}
    }
    
    /**
     * @description : Clase del objeto frequencies del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class frequencies {
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Atributo de Clase: description */
        public String description {get; set;}
        /** Invocación de Clase: premiums */
        public premiums[] premiums {get; set;}
        /** Atributo de Clase: numberSubsequentPayments */
        public Integer numberSubsequentPayments {get; set;}
    }
    
    /**
     * @description : Clase del objeto premiums del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class premiums {
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Invocación de Clase: amounts */
        public amounts[] amounts {get; set;}
    }
    
    /**
     * @description : Clase del objeto amounts del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class amounts {
        /** Atributo de Clase: amount */
        public String amount {get; set;}
        /** Atributo de Clase: currencydata */
        public String currencydata {get; set;}
        /** Atributo de Clase: isMajor */
        public boolean isMajor {get; set;}
    }
    
    /**
     * @description : Clase del objeto trades del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class trades {
        /** Invocación de Clase: categories */
        public categories[] categories {get; set;}
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Atributo de Clase: description */
        public String description {get; set;}
    }
    
    /**
     * @description : Clase del objeto trades del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class categories {
        /** Invocación de Clase: goodTypes */
        public goodTypes[] goodTypes {get; set;}
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Atributo de Clase: description */
        public String description {get; set;}
    }
    
    /**
     * @description : Clase del objeto goodTypes del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class goodTypes {
        /** Atributo de Clase: code */
        public String code {get; set;}
        /** Invocación de Clase: coverages */
        public coverages[] coverages {get; set;}
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Atributo de Clase: description */
        public String description {get; set;}
    }
    
    /**
     * @description : Clase del objeto coverages del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class coverages {
        /** Invocación de Clase: units */
        public units[] units {get; set;}
        /** Atributo de Clase: id_data */
        public String description {get; set;}
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Invocación de Clase: relation */
        public relation relation {get; set;}
        /** Atributo de Clase: isMandatory */
        public String isMandatory {get; set;}
    }
    
    /**
     * @description : Clase del objeto relation del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class relation {
        /** Atributo de Clase: parentdata */
        public String parentdata {get; set;}
        /** Atributo de Clase: relationType */
        public String relationType {get; set;}
    }
    
    /**
     * @description : Clase del objeto units del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class units {
        /** Atributo de Clase: unitsElements */
        public unitsElements unitsElements {get; set;}
    }
    
    /**
     * @description : Clase del objeto unitsElements del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class unitsElements {
        /** Atributo de Clase: unitType */
        public String unitType {get; set;}
        /** Atributo de Clase: relationType */
        public double relationType {get; set;}
        /** Atributo de Clase: contractingCriteria */
        public contractingCriteria contractingCriteria {get; set;}
        /** Atributo de Clase: amount */
        public double amount {get; set;}
        /** Atributo de Clase: currencydata */
        public String currencydata {get; set;}
        /** Atributo de Clase: isMajor */
        public boolean isMajor {get; set;}
        /** Atributo de Clase: percentage */
        public double percentage {get; set;}
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
    }
    
    /**
     * @description : Clase del objeto contractingCriteria del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class contractingCriteria {
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Atributo de Clase: criterial */
        public String criterial {get; set;}
    }
    
    /**
     * @description : Clase del objeto disasterCovered del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class disasterCovered {
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Atributo de Clase: value */
        public String value {get; set;}
    }
}