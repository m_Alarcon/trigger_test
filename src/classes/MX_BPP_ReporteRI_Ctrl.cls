/**
* @File Name          : MX_BPP_ReporteRI_Ctrl.cls
* @Description        : Controlador para la VF Page y el Componente del reporte de RI
* @Author             : hugoivan.carrillo.contractor@bbva.com
* @Created on         : 23/10/2019
* Ver       Date            Author                 Modification
* 1.0    10/23/2019   MX_BPP_ReporteRI_Ctrl     Initial Version
**/
public with sharing class MX_BPP_ReporteRI_Ctrl {
    /*Accesor EU001_RI__c riObj*/
    public EU001_RI__c riObj {get;set;}
    /*Accesor User userObj*/
    public User bankerObj {get;set;}
    /*Accesor User userObj*/
    public User dOFObj {get;set;}
    /*Accesor ExpUni_Tablero_Grupal__c tableroObj*/
    public List<ExpUni_Tablero_Grupal__c> tableroObj {get;set;}
    /*Accesor EU_001_Compromiso__c comprObj*/
    public List<EU_001_Compromiso__c> comprObj {get;set;}
    /*Accesor Case apoyObj*/
    public List<Case> apoyOBJ {get;set;}
    /*Accesor String todayS*/
    public String todayS {get;set;}

    /*
    * @Descripción Constructor de la clase
    * @return No retorna nada
    */
    public MX_BPP_ReporteRI_Ctrl() {
        try {
            //Asigna el valor del Id de la RI desde la URL de la VF Page
            final id riID = String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('Id').escapeHtml4());
            //Consulta para obtener los parametros necesarios de la RI
            this.riObj = [SELECT id, Name, CreatedById, EU001_fm_Propietario__c,
                            RecordType.name, RecordType.DeveloperName, OwnerId, EU001_tl_Comentario_DO__c
                          FROM EU001_RI__c WHERE Id =:riID];
            //Consulta para obtener los parametros del propietario de la RI y de su jefe
            this.bankerObj = [SELECT id, CompanyName, Name, EmployeeNumber, Director_de_oficina__c, Email
                              FROM User WHERE Id =:this.riObj.OwnerId];
            //Consulta para obtener los parametros del jefe del propietario de la RI
            this.dOFObj = [SELECT id, CompanyName, Name, Title, Email
                           FROM User WHERE Id =:this.bankerObj.Director_de_oficina__c];
            //Consulta para obtener los parametros de los tableros
            this.tableroObj = [SELECT EU_001_tx_Indicador__c,EU_Real__c,MX_BPP_Valor3__c,MX_BPP_Valor4__c,
                               MX_BPP_Valor5__c,EU_Logro_Proy_Comp__c, MX_BPP_FechaIndicador__c
                               FROM ExpUni_Tablero_Grupal__c
                               WHERE MX_BPP_Codigo_Tablero__c =:this.bankerObj.EmployeeNumber
                               AND EU_Indicador__c =:Label.MX_BPP_Tablero];
            //Consulta para obtener los parametros de los compromisos de la RI
            this.comprObj = [SELECT Name,EU_001_dv_Compromiso__c,EU_fm_Fecha_Cierre__c,EU_001_ls_Estatus__c,
                             EU_001_Rb_Oportunidad__r.Name,EU_001_Rb_Oportunidad__r.MX_RTL_Familia__c,
                             EU_001_Rb_Oportunidad__r.MX_RTL_Producto__c,EU_001_Fm_Cliente__c,
                             Cliente__r.Name,EU_001_tx_Compromiso__c
                             FROM EU_001_Compromiso__c
                             WHERE EU_001_Rb_Revisi_n_Individual_RI__c =:riID];
            //Consulta para obtener los parametros de los apoyos de la RI
            this.apoyObj = [SELECT Id,CaseNumber, Status,EU001_ls_Tipo_de_apoyo__c,
                            EU001_fh_Fecha_propuesta_de_cierre__c,Description
                            FROM Case WHERE OwnerId=:this.riObj.OwnerId];
            this.todayS = getFecha(date.today());
        } catch(Exception e) {
            System.debug('El PDF se está creando o el ID recivido no corresponde a ninguna RI ' + e);
        }

    }

    /**
    * @Description método que regresa la fecha completa.
    * @return Regresa una cadena que contiene una fecha.
    **/
    public string getFecha(datetime fechIn) {
        String fechFinal = '';
        if(fechIn != null) {
            final string[] fechRes = string.valueofGmt(fechIn).split(' ')[0].split('-');
            fechFinal = fechRes[2] + ' de ' + getMes(fechRes[1]) + ' del ' + fechRes[0];
        }
        return fechFinal;
    }

    /**
    * @Description método que regresa el mes.
    * @return Regresa una cadena que contiene el mes.
    **/
    public string getMes(String mesIn) {
        String mesF = '';
        final Map<String, String> monthValues = new Map<String, String>();
        monthValues.put('01', 'Enero');
        monthValues.put('02', 'Febrero');
        monthValues.put('03', 'Marzo');
        monthValues.put('04', 'Abril');
        monthValues.put('05', 'Mayo');
        monthValues.put('06', 'Junio');
        monthValues.put('07', 'Julio');
        monthValues.put('08', 'Agosto');
        monthValues.put('09', 'Septiembre');
        monthValues.put('10', 'Octubre');
        monthValues.put('11', 'Noviembre');
        monthValues.put('12', 'Diciembre');
        if(monthValues.containskey(mesIn)) {
            mesF = monthValues.get(mesIn);
        }
        return mesF;
    }

    /**
    * ------------------------------------------------------------------
    * @Name     sendMail
    * @Author   hugoivan.carrillo.contractor@bbva.com
    * @Date     Created: 23/10/2019
    * @Group
    * @Description Método llamado desde el componente para enviar el e-mail.
    * @Changes
    *     | 30/10/2019  hugoivan.carrillo.contractor@bbva.com Initial Version
    **/
    @AuraEnabled
    public static void sendMail(Id currentId) {
        final MX_BPP_ReporteRI_Ctrl sendPDF = new MX_BPP_ReporteRI_Ctrl();
        sendPDF.sendTemplateEmail(currentId);
    }

    /**
    * ------------------------------------------------------------------
    * @Name     sendTemplateEmail
    * @Author   hugoivan.carrillo.contractor@bbva.com
    * @Date     Created: 23/10/2019
    * @Group
    * @Description Método para generar y envíar el reporte como PDF.
    * @Changes
    *     | 23/10/2019  hugoivan.carrillo.contractor@bbva.com Initial Version
    **/
    public void sendTemplateEmail(ID currentId) {
        Blob report;
        final PageReference templateVF = page.MX_BPP_ReporteRI;
        templateVF.getParameters().put('id', currentId);
        templateVF.setRedirect(true);
        final EU001_RI__c newRI = [SELECT Id, Name, Owner.Email, Owner.Id FROM EU001_RI__c WHERE Id =: currentId];
        final User bnk = [SELECT Id, Director_de_oficina__c FROM User WHERE Id =: newRI.OwnerId];
        final User dOF = [SELECT Id, Name, Email FROM User WHERE Id =: bnk.Director_de_oficina__c];
        final List<String> emailList = new List<String>();
        emailList.add(newRI.Owner.Email);
        emailList.add(dOF.Email);
        try {
            report = templateVF.getContentAsPDF();
        } catch(VisualforceException e) {
            report = Blob.valueOf('Texto de prueba para pdf ' + e);
        }

        final Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        final Messaging.EmailFileAttachment efa1 = new Messaging.EmailFileAttachment();
        efa1.setFileName('ReporteRI.pdf');
        efa1.setBody(report);

        email.setSubject( 'Reporte de la ' + newRI.Name);
        email.setToAddresses(emailList);
        email.setPlainTextBody('Buen día,\nEste es el reporte de la Reunión Individual ' + newRI.Name);
        email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa1});
        final Messaging.SendEmailResult [] sendingEmail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        System.debug('Result: ' + sendingEmail);
    }

    /**
    * ------------------------------------------------------------------
    * @Name     returnURL
    * @Author   hugoivan.carrillo.contractor@bbva.com
    * @Date     Created: 05/12/2019
    * @Group
    * @Description Método llamado desde el componente para regresar la URL.
    * @Changes
    *     | 05/12/2019  hugoivan.carrillo.contractor@bbva.com Initial Version
    **/
    @AuraEnabled
    public static String returnURL() {
        final String sfURL = String.valueOf(System.Url.getSalesforceBaseUrl().gethost());
        return sfURL.removeEndIgnoreCase('.my.salesforce.com');
    }
}