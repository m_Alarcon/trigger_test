@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_PS_Asegurados_Table_Ctrl_Test
* Autor: Juan Carlos Benitez Herrera
* Proyecto: SB Presuscritos - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_PS_Asegurados_Table_Ctrl

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         09/07/2020    Juan Carlos Benitez Herrera              Creación
* -----------------------------------------------------------------------------------------------
*/
public class MX_SB_PS_Asegurados_Table_Ctrl_Test {
    /** Nombre de usuario Test */
    final static String NAME = 'Mariani';
    /** Apellido Paterno */
    Final static STRING LNAME= 'Martinez';
    @TestSetup
    static void loadData() {
			MX_SB_PS_OpportunityTrigger_test.makeData();
            final Account accTst = [Select Id from Account  limit 1];
            accTst.FirstName=NAME;
            accTst.LastName=LNAME;
            accTst.Tipo_Persona__c='Física';
            update accTst;
            Final Opportunity oppTest = [SELECT Id from Opportunity where AccountId=:accTst.Id limit 1];
            oppTest.Name= NAME;
            oppTest.StageName=System.Label.MX_SB_PS_Etapa1;
            update oppTest;
            Final Quote quotObj = [Select  Id from Quote where OpportunityId=:oppTest.Id limit 1];
            quotObj.Name=NAME;
            update quotObj;
            Final MX_SB_VTS_Beneficiario__c benefic= new MX_SB_VTS_Beneficiario__c(RecordTypeId=Schema.SObjectType.MX_SB_VTS_Beneficiario__c.getRecordTypeInfosByDeveloperName().get('MX_SB_PS_Asegurado').getRecordTypeId(),Name = NAME, MX_SB_SAC_Email__c = NAME+'@'+'bbva.com');
            insert benefic;
        }
    
    @isTest
    static void testLoadOppF() {
        Final String oppId =[SELECT Id from Opportunity where Name=:NAME Limit 1].Id;
        test.startTest();
        	MX_SB_PS_Asegurados_Table_Ctrl.loadOppF(oppId);
            system.assert(true,'Carga de opportunidad exitosa');
		test.stopTest();
    }
    @isTest 
    static void testsrchBenefs() {
        Final String quotId = [SELECT Id from Quote where Name=:NAME limit 1].Id;
        test.startTest();
        	MX_SB_PS_Asegurados_Table_Ctrl.srchBenefs(quotId);
            system.assert(true,'Se han encontrado beneficiarios');
		test.stopTest();
    }
    @isTest
    static void testdltBenef() {
    Final MX_SB_VTS_Beneficiario__c benefData1= [SELECT Id from MX_SB_VTS_Beneficiario__c WHERE Name=:NAME limit 1];
        test.startTest();
        	MX_SB_PS_Asegurados_Table_Ctrl.dltBenef(benefData1);
            system.assert(true,'Se han removido los beneficiarios');
		test.stopTest();
    }
}