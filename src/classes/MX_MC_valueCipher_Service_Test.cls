/**
 * @File Name          : MX_MC_valueCipher_Service_Test.cls
 * @Description        : Clase para Test de MX_MC_valueCipher_Service
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 03/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      03/06/2020           Jair Ignacio Gonzalez G.   Initial Version
**/
@isTest
private class MX_MC_valueCipher_Service_Test {
    /**
    * @Description CLTNUMBER property
    **/
    final static String CLTNUMBER = '158469';
    /**
     **Descripción: Clase makeData
    **/
    @TestSetup static void makeData() {
        final iaso__GBL_Rest_Services_Url__c restUrl = new iaso__GBL_Rest_Services_Url__c(Name = 'MC_valueCipher',
                                                                                        iaso__Url__c = 'www.test.bbva',
                                                                                        iaso__Cache_Partition__c = 'iaso.ServicesPartition');
        insert restUrl;
    }
    /**
     * *Descripción: Clase de prueba para MX_MC_valueCipher_Service
    **/
    @isTest static void testServiceMethods() {
        final MX_WB_Mock encryptMock = new MX_WB_Mock(200,'OK', '{"results":["0mVKCSmxooI"]}', new Map<String,String>());
        final MX_WB_Mock decryptMock = new MX_WB_Mock(200,'OK', '{"results":["158469"]}', new Map<String,String>());
        Test.startTest();
        iaso.GBL_Mock.setMock(encryptMock);
        final String cltEncrypt = MX_MC_valueCipher_Service.callValueCipher(CLTNUMBER, true);
        iaso.GBL_Mock.setMock(decryptMock);
        final String cltDecrypt = MX_MC_valueCipher_Service.callValueCipher(cltEncrypt, false);
        Test.stopTest();
        System.assertEquals(CLTNUMBER, cltDecrypt, 'Cant call MC_valueCipher');
    }
}