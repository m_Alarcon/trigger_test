/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase Selector para objeto Account

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     20/05/2020      Roberto Isaac Soto Granados           Creación Clase
*   1.1     07/10/2020      Gabriel Garcia                        Se agregan method de AggregateResult y clausula con filtros
                                                                  Sustitución de String por variable SELECT_STR
*****************************************************************************************/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton, sf:TooManyMethods')
public class MX_RTL_Account_Selector {

    /** list of possible filters */
    final static String SELECT_STR = 'SELECT ';
    /** list of possible filters */
    final static String FROMACC_STR = ' FROM Account ';
    /** list of possible filters */
    final static Set<String> ACCOUNT_FILTERS = new Set<String>();
   /** List of records to be returned */
    final static List<SObject> ACCSEARCH = new List<SObject>();
    /*Llamada a cuentas por número de cliente*/
    public static List<Account> cuentasPorNoDeCliente(Set<String> noDeClientes) {
        return [SELECT Id, LastName, OwnerId, No_de_cliente__c, Phone, PersonEmail FROM Account WHERE No_de_cliente__c IN: noDeClientes];
    }

    /*Consulta de cuentas por nombre completo*/
    public static List<Account> getAccountByNombre(Map<String,String> nombreCompleto) {
        return [SELECT id,createddate,name,firstname,middlename,lastname,ApellidoMaterno__c, RFC__c,PersonEmail ,PersonHomePhone FROM Account WHERE firstname =:nombreCompleto.get('nombreCta')
        AND lastname =:nombreCompleto.get('aPaternoCta') AND ApellidoMaterno__c =:nombreCompleto.get('aMaternoCta') order by createddate  desc limit 200];
    }

        /**
    * @description
    * @author Jaime Terrats | 6/8/2020
    * @param queryParams
    * @return List<Account>
    **/
    public static List<Account> getAccountsByCustomerData(Map<String, String> queryParams) {
        ACCOUNT_FILTERS.add(queryParams.get('searchParam'));
        return Database.query(String.escapeSingleQuotes(SELECT_STR + queryParams.get('fields') + ' from  Account where FirstName in: ACCOUNT_FILTERS'
                                                        + ' or LastName in: ACCOUNT_FILTERS or Apellido_Materno__pc in: ACCOUNT_FILTERS'
                                                        + ' or PersonEmail in: ACCOUNT_FILTERS or RFC__c in: ACCOUNT_FILTERS'));
    }

    /**
    * @description
    * @author Jaime Terrats | 6/30/2020
    * @param accsToUpsert
    * @return List<Account>
    **/
    public static List<Account> upsertAccount(List<Account> accsToUpsert) {
        Database.upsert(accsToUpsert);

        return accsToUpsert;
    }
    /**
    * @description
    * @author Juan Carlos Benitez | 6/24/2020
    * @param Set<Id> ids
    * @param String Condition
    * @param Map<String, String> mapa
    * @return ACCSEARCH
    **/
    public static List<Account> getAccountReusable(Set<Id> ids, String condition, Map<String,String> mapa) {
        switch on condition {
            when 'Id' {
                ACCSEARCH.addAll(Database.query(String.escapeSingleQuotes(SELECT_STR +mapa.get('fields')+ ' FROM Account where Id=:ids')));
            }
        }
		return ACCSEARCH;
    }
    /**
    * @description
    * @author Juan Carlos Benitez | 6/24/2020
    * @param Account pAcc
    * @return pAcc
    **/
    public static Account upsrtAccount(Account pAcc) {
        insert pAcc;
        return pAcc;
    }
    /**
    * @description
    * @author Juan Carlos Benitez | 6/24/2020
    * @param Account pAcc
    * @return pAcc
    **/
    public static Account updteAccount (Account pAcc) {
        update pAcc;
        return pAcc;
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param queryfields
    * @param queryFilters
    * @param listFil
    * @return List<AggregateResult>
    * @example Query SELECT Id, Name FROM Account WHERE Id =:filtro0 AND Name =: filtro1;
    **/
    @SuppressWarnings('sf:DUDataflowAnomalyAnalysis, sf:UnusedLocalVariable')
    public static List<AggregateResult> fetchListAggAccByDataBase(String queryfields, String queryFilters, List<String> listFilters) {
        List<String> lisToFilt = new List<String>(5);
        Integer posicion = 0;
        for(String filtro : listFilters) {
            lisToFilt[posicion] = filtro;
            posicion++;
        }

        final String filtro0 = lisToFilt[0];
        final String filtro1 = lisToFilt[1];
        final String filtro2 = lisToFilt[2];
        final String filtro3 = lisToFilt[3];
        final String filtro4 = lisToFilt[4];
        return Database.query(SELECT_STR + String.escapeSingleQuotes(queryfields) + FROMACC_STR + String.escapeSingleQuotes(queryFilters));
    }

    /**
    * @description
    * @author Jair Ignacio Gonzalez G
    * @param queryfields
    * @param queryFilters
    * @param listFil
    * @return List<AggregateResult>
    * @example Query SELECT Id, Name FROM Account WHERE RecordType.DeveloperName IN:listRecordTypes AND Name =: filtro0;
    **/
    @SuppressWarnings('sf:DUDataflowAnomalyAnalysis, sf:UnusedLocalVariable')
    public static List<AggregateResult> fetchListAggAccByDataBase(String queryfields, String queryFilters, List<String> listFilters, List<String> listRecordTypes) {
        List<String> listToFil = new List<String>(5);
        Integer posicion = 0;
        for(String filtro : listFilters) {
            listToFil[posicion] = filtro;
            posicion++;
        }

        final String filtro0 = listToFil[0];
        final String filtro1 = listToFil[1];
        final String filtro2 = listToFil[2];
        final String filtro3 = listToFil[3];
        final String filtro4 = listToFil[4];
        return Database.query(SELECT_STR + String.escapeSingleQuotes(queryfields) + FROMACC_STR + String.escapeSingleQuotes(queryFilters));
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param queryfields
    * @param queryFilters
    * @param listFiltro
    * @return List<Account>
    * @example Query SELECT Id, Name FROM Account WHERE Id =:filtro0 AND Name =: filtro1;
    **/
    @SuppressWarnings('sf:DUDataflowAnomalyAnalysis, sf:UnusedLocalVariable')
    public static List<Account>fetchListAccByDataBase(String queryfields, String queryFilters, List<String> listFilters, Datetime stDate, Datetime enDate) {
        List<String> listFil = new List<String>(5);
        Integer posicion = 0;
        for(String filtro : listFilters) {
            listFil[posicion] = filtro;
            posicion++;
        }

        final String filtro0 = listFil[0];
        final String filtro1 = listFil[1];
        final String filtro2 = listFil[2];
        final String filtro3 = listFil[3];
        final String filtro4 = listFil[4];
        return Database.query(SELECT_STR + String.escapeSingleQuotes(queryfields) + FROMACC_STR + String.escapeSingleQuotes(queryFilters));
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param queryfields
    * @param queryFilters
    * @param listFilters
    * @return List<Account>
    * @example Query SELECT Id, Name FROM Account WHERE Id =:filtro0 AND Name =: filtro1;
    **/
    @SuppressWarnings('sf:UnusedLocalVariable, sf:DUDataflowAnomalyAnalysis')
    public static List<Account>fetchListAccByIds(String queryfields, String queryFilters, List<String> listFilters, Set<Id> setIds) {
        List<String> listToFilters = new List<String>(5);
        Integer posicion = 0;
        for(String filtro : listFilters) {
            listToFilters[posicion] = filtro;
            posicion++;
        }

        final String filtro0 = listToFilters[0];
        final String filtro1 = listToFilters[1];
        final String filtro2 = listToFilters[2];
        final String filtro3 = listToFilters[3];
        final String filtro4 = listToFilters[4];
        return Database.query(SELECT_STR + String.escapeSingleQuotes(queryfields) + FROMACC_STR + String.escapeSingleQuotes(queryFilters));
    }

    /**
    * @description
    * @author Jair Ignacio Gonzalez Gayosso
    * @param queryfields
    * @param queryFilters
    * @param listFilters
    * @return List<Account>
    * @example Query SELECT Id, Name FROM Account WHERE RecordType.DeveloperName IN:listRecordTypes AND Name =: filtro0;
    **/
    @SuppressWarnings('sf:UnusedLocalVariable, sf:DUDataflowAnomalyAnalysis, sf:TooManyMethods')
    public static List<Account>fetchListAccByRecType(String queryfields, String queryFilters, List<String> listFilters, List<String> listRecordTypes) {
        List<String> listToFilters = new List<String>(5);
        Integer posicion = 0;
        for(String filtro : listFilters) {
            listToFilters[posicion] = filtro;
            posicion++;
        }

        final String filtro0 = listToFilters[0];
        final String filtro1 = listToFilters[1];
        final String filtro2 = listToFilters[2];
        final String filtro3 = listToFilters[3];
        final String filtro4 = listToFilters[4];
        return Database.query(SELECT_STR + String.escapeSingleQuotes(queryfields) + FROMACC_STR + String.escapeSingleQuotes(queryFilters));
    }
}