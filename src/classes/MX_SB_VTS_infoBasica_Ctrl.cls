/**
* @File Name          : MX_SB_VTS_CotizInitData_Ctrl.cls
* @Description        : 
* @Author             : Eduardo Hernández Cuamatzi
* @Group              : BBVA Seguros
* @Last Modified By   : Diego Olvera
* @Last Modified On   : 14-10-2020
* @Modification Log   : 
* Ver       Date            Author      		    Modification
* 1.0    21/5/2020   Eduardo Hernandez Cuamatzi     Initial Version
* 1.1    09/03/2020  Eduardo Hernandez Cuamatzi     Se agrega limpieza de quotes
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_infoBasica_Ctrl {
    /**IsOk variableStr */
    private final static string ISOK = 'isOk';
    /**
    * @description Actualiza cupon 
    * @author Eduardo Hernández Cuamatzi | 02-10-2021 
    * @param oppId Id de la oportunidad 
    * @param cuponCode Código cupon
    * @return Map<String, Object> Mapa de resultados
    **/
    @AuraEnabled
    public static Map<String, Object> updateCuponCot(String oppId, String cuponCode) {
        final Map<String, Object> responseUpdate = new Map<String, Object>();
        try {
            final Set<String> setOppsId = new Set<String>{oppId};
            final List<Opportunity> oppRecords = MX_SB_VTS_CotizInitData_Service.findOppsById(setOppsId);
            final Map<Id, Quote> lstQuotes = MX_SB_VTS_CotizInitData_Service.findQuotesByOppMap(oppRecords);
            String msjdetail = 'Ocurrio un error al guardar el cupón';
            if(lstQuotes.isEmpty() == false) {
                responseUpdate.put(ISOK, MX_SB_VTS_infoBasica_Service.updateQuoteCupon(lstQuotes, cuponCode));
                msjdetail = '';
            }
            responseUpdate.put('dataOpp', oppRecords[0]);
            responseUpdate.put('msjdetail', msjdetail);
        } catch (DmlException demEx) {
            responseUpdate.put(ISOK, false);
            responseUpdate.put('msjdetail', demEx.getMessage());
        }
        return responseUpdate;
    }

    /**
    * @description Actualiza etapa de una Oportunidad
    * @author Eduardo Hernández Cuamatzi | 02-10-2021 
    * @param oppId Id de la Oportunidad
    * @param stageName Etapa nueva
    **/
    @AuraEnabled
    public static void updateOpp(String oppId, String stageName) {
        final Set<String> setOppsId = new Set<String>{oppId};
        final List<Opportunity> oppRecords = MX_SB_VTS_CotizInitData_Service.findOppsById(setOppsId);
        oppRecords[0].StageName = stageName;
        MX_SB_VTS_infoBasica_Service.upsertOppData(oppRecords[0]);
    }

    /**
    * @description Actualiza las cotizaciones de una Oportunidad con el cupon introducido
    * @author Eduardo Hernández Cuamatzi | 12-10-2020 
    * @param oppId Oportunidad trabajada
    * @param cupon Cupon a guardar
    * @return Map<String, Object> Mapa de resultados
    **/
    @AuraEnabled
    public static Map<String, Object> updateCupon(String oppId, String cupon) {
        final Map<String, Object> response = new Map<String, Object>();
        try {
            final Set<String> setOppsId = new Set<String>{oppId};
            final List<Opportunity> oppRecords = MX_SB_VTS_CotizInitData_Service.findOppsById(setOppsId);
            final Map<Id, Quote> lstQuotes = MX_SB_VTS_CotizInitData_Service.findQuotesByOppMap(oppRecords);
            response.put(ISOK, MX_SB_VTS_infoBasica_Service.updateQuoteCupon(lstQuotes, cupon));
        } catch (DmlException demEx) {
            response.put(ISOK, false);
            response.put('msjError', demEx.getMessage());
        }
        return response;
    }

    /**
    * @description Resetea Cotizaciones 
    * @author Eduardo Hernández Cuamatzi | 03-03-2021 
    * @param strOppoId Id de la oportunidad de la que se busca limpiar las quotes
    **/
    @AuraEnabled
    public static Map<String, Object> resetQuotes(String strOppoId) {
        final Map<String, Object> mapReset = new Map<String, Object>();
        mapReset.put(ISOK, MX_SB_VTS_infoBasica_Service.resetQuotes(strOppoId));
        return mapReset;
    }
}