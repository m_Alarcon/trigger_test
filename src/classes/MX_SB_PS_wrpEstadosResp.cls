/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpEstadosResp
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
 @SuppressWarnings('sf:ShortClassName, sf:ShortVariable')
public  class  MX_SB_PS_wrpEstadosResp {
     /*Public property for wrapper*/
	public data[] data {get;set;}
	/*public constructor subclass*/
	public MX_SB_PS_wrpEstadosResp() {
		 this.data =  new data[] {};
		 this.data.add(new data());
	}
	/*Public subclass for wrapper*/
	public class data {
	    /*Public property for wrapper*/
		public country country {get;set;}
	    /*Public property for wrapper*/	
		public String name {get;set;}	
	    /*Public property for wrapper*/
		 public string id {get;set;} 
         /*Public property for wrapper*/		
		public geolocation geolocation {get;set;}
		/*public constructor subclass*/
		public data() {
		 this.country = new country();
		 this.name = '';
		 this.id = '';
		 this.geolocation = new geolocation();
		}
	}
	  /*Public subclass for wrapper*/
	public class country {
	  /*Public property for wrapper*/
		public String name {get;set;}
        /*Public property for wrapper*/		
		 public string id {get;set;} 
        /*public constructor subclass*/
		public country() {
		 this.name = '';
		 this.id = '';
		}		
	}
	  /*Public subclass for wrapper*/
	public class geolocation {
	  /*Public property for wrapper*/
		public String latitude {get;set;}
      /*Public property for wrapper*/		
		public String longitude {get;set;}
      /*public constructor subclass*/
		public geolocation() {
		 this.latitude = '';
		 this.longitude = '';
		}		
	}
	
	}