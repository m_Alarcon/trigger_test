/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase para obtener acceso a componente

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     14/10/2019      Roberto Isaac Soto Granados           Creación Clase
*****************************************************************************************/
public without sharing class BPyP_LinkFactset {
    /**
    * Contructor clase factset
    */
    private BPyP_LinkFactset() {}

    /**
    * Valida accesso al Botón factset
    */
    @AuraEnabled
    public static boolean checkAccess() {
        final String userId = System.UserInfo.getUserId();
        return BPyP_LinkFactset_Service.validateFactsetPermissionSet(userId);
    }

}