/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpGenerateOTP
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
@SuppressWarnings('sf:ShortVariable')
public class MX_SB_PS_wrpGenerateOTP {
    /*Public property for wrapper*/
	public quotation quotation {get;set;}
   /*Public property for wrapper*/
	public String sessionId {get;set;}
     /*Public property for wrapper*/	
    public contact contact {get;set;}
	  /*Public constructor for wrapper*/
    public MX_SB_PS_wrpGenerateOTP() {
        quotation = new quotation();
		sessionId = '';
        contact = new contact();
    } 
	  /*Public subclass for wrapper*/
	public class quotation {
	   /*Public property for wrapper*/
		 public string id {get;set;} 	
		/*public constructor subclass*/
		public quotation() {
		id =  '';
		}	
	}
	  /*Public subclass for wrapper*/
	public class contact {
	/*Public property for wrapper*/
		public String mobile {get;set;}
    /*public constructor subclass*/
		public contact() {
		this.mobile = '';
		}	 		
	}

}