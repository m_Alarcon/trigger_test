/**
* @File Name          : MX_SB_PS_Asegurados_Table_Ctrl.cls
* @Description        :
* @Author             : Juan Carlos Benitez
* @Group              :
* @Last Modified By   : Juan Carlos Benitez
* @Last Modified On   : 7/02/2020, 9:00:03 AM
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    6/24/2020   Juan Carlos Benitez          Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_SB_PS_Asegurados_Table_Ctrl {
    /**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param oppId
* @return List<Opportunity>
**/
    @AuraEnabled
    public static Opportunity loadOppF(String oppId) {
        return MX_SB_PS_Utilities.fetchOpp(oppId);
    }
    
/**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param String quotId
* @return Lit<MX_SB_VTS_Beneficiario__c>
**/
    @AuraEnabled
    public static list<MX_SB_VTS_Beneficiario__c> srchBenefs(String quotId) {
        return MX_RTL_Beneficiario_Service.getbenefbyValue(quotId);
    }
	/**
    * @description
    * @author Juan Carlos Benitez | 5/26/2020
    * @param  MX_SB_VTS_Beneficiario__c benef
    * @return void
    **/
    @AuraEnabled
    public static void dltBenef(MX_SB_VTS_Beneficiario__c benef) {
        MX_RTL_Beneficiario_Service.dltBenef(benef);
    }
    
}