/**
 * @File Name          : MX_SB_VTS_Campanas_lwc_cls
 * @Description        : 
 * @Author             : tania.vazquez.contractor@BBVA.com
 * @Group              : 
 * @Last Modified By   : tania.vazquez.contractor@BBVA.com
 * @Last Modified On   : 11/02/2020 15:00:42
 * @Modification Log   : 
 * Ver       Date            Author      		    		Modification
 * 1.0    10/2/2020   tania.vazquez.contractor@BBVA.com     Initial Version
**/
@IsTest
public class MX_SB_VTS_Campanas_lwc_tst {
	@TestSetup
    static void makeData() {
        final User usuarioAdmin = MX_WB_TestData_cls.crearUsuario('testUser',System.label.MX_SB_VTS_ProfileAdmin);
        insert usuarioAdmin;
        system.runAs(usuarioAdmin) {
            final Account temAccount = MX_WB_TestData_cls.crearCuenta('example','');
            insert temAccount;
            final Opportunity oppGenerada = MX_WB_TestData_cls.crearOportunidad( 'PruebaUno',  temAccount.Id,  usuarioAdmin.Id, 'MX_SB_VTS_Telemarketing');
            oppGenerada.stageName='Contacto';
            oppGenerada.LeadSource ='Facebook';
            oppGenerada.Producto__c ='Hogar';
            upsert OppGenerada;
            
            final Lead leadGenerada = MX_WB_TestData_cls.createLead('PruebaUnoLead');
            leadGenerada.LeadSource ='Facebook';
            leadGenerada.Producto_Interes__c ='Hogar';
            upsert LeadGenerada;

        }
    }
    @IsTest
    static void methodName() {
        Final User usuarionormal = MX_WB_TestData_cls.crearUsuario('testUser',System.label.MX_SB_VTS_Telemarketing_VTS);
        insert usuarionormal; 
        Final Opportunity opp = [SELECT id from Opportunity where name='PruebaUno'][0];
        Final Lead lea = [SELECT id from Lead where LastName='PruebaUnoLead'][0];
        Test.startTest();
        System.runAs(usuarionormal) {
            MX_SB_VTS_Campanas_lwc_cls.getcampaigndata(String.valueof(opp.Id));
            MX_SB_VTS_Campanas_lwc_cls.getcampaigndata(String.valueof(lea.Id));
        }
        System.assert(String.isNotBlank(opp.Id),true);
        Test.stopTest();
        
    }
}