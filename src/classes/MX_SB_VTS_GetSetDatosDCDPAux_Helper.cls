/**
 * @File Name          : MX_SB_VTS_GetSetDatosDCDPAux_Helper.cls
 * @Description        : Clase Encargada de Proporcionar Soporte
 *                       a la clase: MX_SB_VTS_GetSetDatosDCDPFrm_Helper
 * @Author             : Alexandro Corzo
 * @Group              : 
 * @Last Modified By   : Alexandro Corzo
 * @Last Modified On   : 17/02/2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0       17/02/2021      Alexandro Corzo        Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_GetSetDatosDCDPAux_Helper {
    /** Variable de Apoyo: FLDTYPE */
    final static String FLDTYPE = 'type';
    /** Variable de Apoyo: FLDTELCEL */
    final static String FLDTELCEL = 'TelefonoCel';
    /** Variable de Apoyo: OBJNULLASIG */
    final static String OBJNULLASIG = null;
    /** Variable de Apoyo: FLDCITY */
    final static String FLDCITY = 'city';
    /** Variable de Apoyo: FLDCOUNTY */
    final static String FLDCOUNTY = 'county';
    /** Variable de Apoyo: FLDDIRCONT */
    final static String FLDDIRCONT = 'DirCont';
    
    /**
     * @description : Realiza el llenado del Wrapper en el apartado de CIB
     *              : para el armado del body del servicio ASO CreateCustomerData
     * @author:     : Alexandro Corzo
     * @return:     : MX_SB_VTS_wrpCreateCustomerDataSrv.catalogItemBase objWrapperCIBase
     */
    public static MX_SB_VTS_wrpCreateCustomerDataSrv.catalogItemBase fillDataCIBase(String strId, String strName) {
        final MX_SB_VTS_wrpCreateCustomerDataSrv.catalogItemBase objWrapperCIBase = new MX_SB_VTS_wrpCreateCustomerDataSrv.catalogItemBase();
        objWrapperCIBase.id = strId;
        objWrapperCIBase.name = strName;
        return objWrapperCIBase;
    }

    /**
     * @description : Realiza el llenado del Wrapper en el apartado de PPL
     *              : para el armado del body del servicio ASO CreateCustomerData
     * @author:     : Alexandro Corzo
     * @return:     : MX_SB_VTS_wrpCreateCustomerDataSrv.physicalPersonalityData objWrapperPhyPD
     */
    public static MX_SB_VTS_wrpCreateCustomerDataSrv.physicalPersonalityData fillDataPPL(Map<Object, Object> mDataObjDC) {
        final MX_SB_VTS_wrpCreateCustomerDataSrv.physicalPersonalityData objWrapperPhyPD = new MX_SB_VTS_wrpCreateCustomerDataSrv.physicalPersonalityData();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.fiscalPersonality objWrapperFisP = new MX_SB_VTS_wrpCreateCustomerDataSrv.fiscalPersonality();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.sex objWrapperSex = new MX_SB_VTS_wrpCreateCustomerDataSrv.sex();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.civilStatus objWrapperCSta = new MX_SB_VTS_wrpCreateCustomerDataSrv.civilStatus();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.occupation objWrapperOccup = new MX_SB_VTS_wrpCreateCustomerDataSrv.occupation();
        objWrapperFisP.catalogItemBase = fillDataCIBase('N', 'FISICA');
        objWrapperPhyPD.fiscalPersonality = objWrapperFisP;
        objWrapperPhyPD.name = mDataObjDC.get('NombreCliente').toString();
        objWrapperPhyPD.lastName = mDataObjDC.get('ApellidoPaterno').toString();
        objWrapperPhyPD.mothersLastName = mDataObjDC.get('ApellidoMaterno').toString();
        final String strDateNac = mDataObjDC.get('FechaNac').toString();
        final String[] strFechaNac = strDateNac.split('\\/');
        final String strFechaNacN = strFechaNac[2] + '-' + strFechaNac[1] + '-' + strFechaNac[0];
        objWrapperPhyPD.birthDate = strFechaNacN;
        objWrapperPhyPD.curp = mDataObjDC.get('Curp').toString();
        final String strSexoId = Boolean.valueOf(mDataObjDC.get('SexoMasculino')) ? 'M' : 'F';
        final String strSexoName = Boolean.valueOf(mDataObjDC.get('SexoMasculino')) ? 'MASCULINO' : 'FEMENINO';
        objWrapperSex.catalogItemBase = fillDataCIBase(strSexoId, strSexoName);
        objWrapperPhyPD.sex = objWrapperSex;
        objWrapperCSta.catalogItemBase = fillDataCIBase('S', 'SOLTERO');
        objWrapperPhyPD.civilStatus = objWrapperCSta;
        objWrapperOccup.catalogItemBase = fillDataCIBase('701', '701');
        objWrapperPhyPD.occupation = objWrapperOccup;
        return objWrapperPhyPD;
    }

    /**
     * @description : Realiza el llenado del Wrapper en el apartado de SUrb
     *              : para el armado del body del servicio ASO CreateCustomerData
     * @author:     : Alexandro Corzo
     * @return:     : MX_SB_VTS_wrpCreateCustomerDataSrv.suburb objWrapperSuburb
     */
    public static MX_SB_VTS_wrpCreateCustomerDataSrv.suburb fillDataSUrb(Map<String, Object> oParams, Map<Object, Object> mDataObjDC, Map<Object, Object> mDataObjDP) {
        final MX_SB_VTS_wrpCreateCustomerDataSrv.suburb objWrapperSuburb = new MX_SB_VTS_wrpCreateCustomerDataSrv.suburb();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.neighborhood objWrapperNeigh = new MX_SB_VTS_wrpCreateCustomerDataSrv.neighborhood();
        String strColVal = null;
        String strColLbl = null;
        if(FLDDIRCONT.equals(oParams.get(FLDTYPE).toString())) {
            strColVal = ((Map<Object, Object>) mDataObjDP.get(oParams.get(FLDTYPE).toString())).get('DirContratante_ColoniaVal').toString();
            strColLbl = ((Map<Object, Object>) mDataObjDP.get(oParams.get(FLDTYPE).toString())).get('DirContratante_ColoniaLbl').toString();
        } else {
            strColVal = ((Map<Object, Object>) mDataObjDP.get(oParams.get(FLDTYPE).toString())).get('DirPropiedad_ColoniaVal').toString();
            strColLbl = ((Map<Object, Object>) mDataObjDP.get(oParams.get(FLDTYPE).toString())).get('DirPropiedad_ColoniaLbl').toString();
        }
        objWrapperSuburb.city = oParams.get(FLDCITY) == OBJNULLASIG ? OBJNULLASIG : oParams.get(FLDCITY).toString();
        objWrapperSuburb.county = oParams.get(FLDCOUNTY) == OBJNULLASIG ? OBJNULLASIG : oParams.get(FLDCOUNTY).toString();
        objWrapperNeigh.catalogItemBase = fillDataCIBase(strColVal, strColLbl);
        objWrapperSuburb.neighborhood = objWrapperNeigh;
        objWrapperSuburb.state = OBJNULLASIG;
        return objWrapperSuburb;
    }

    /**
     * @description : Realiza el llenado del Wrapper en el apartado de CPhone
     *              : para el armado del body del servicio ASO CreateCustomerData
     * @author:     : Alexandro Corzo
     * @return:     : MX_SB_VTS_wrpCreateCustomerDataSrv.cellphone objWrapperCPhone
     */
    public static MX_SB_VTS_wrpCreateCustomerDataSrv.cellphone fillCPhone(String strPhoneExt, Map<Object, Object> mDataObjDC, Map<Object, Object> mDataObjDP) {
        final MX_SB_VTS_wrpCreateCustomerDataSrv.cellphone objWrapperCPhone = new MX_SB_VTS_wrpCreateCustomerDataSrv.cellphone();
        objWrapperCPhone.phoneExtension = strPhoneExt;
        objWrapperCPhone.telephoneNumber = mDataObjDC.get(FLDTELCEL).toString();
        return objWrapperCPhone;
    }

    /**
     * @description : Realiza el llenado del Wrapper en el apartado de MCData
     *              : para el armado del body del servicio ASO CreateCustomerData
     * @author:     : Alexandro Corzo
     * @return:     : MX_SB_VTS_wrpCreateCustomerDataSrv.cellphone objWrapperCPhone
     */
    public static MX_SB_VTS_wrpCreateCustomerDataSrv.mainContactData fillMCData(Map<Object, Object> mDataObjDC, Map<Object, Object> mDataObjDP) {
        final MX_SB_VTS_wrpCreateCustomerDataSrv.mainContactData objWrapperMCData = new MX_SB_VTS_wrpCreateCustomerDataSrv.mainContactData();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.officePhone objWrapperOPhone = new MX_SB_VTS_wrpCreateCustomerDataSrv.officePhone();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.phone objWrapperPhone = new MX_SB_VTS_wrpCreateCustomerDataSrv.phone();
        objWrapperMCData.cellphone = fillCPhone('', mDataObjDC, mDataObjDP);
        objWrapperOPhone.phoneExtension = '';
        objWrapperOPhone.telephoneNumber = mDataObjDC.get(FLDTELCEL).toString();
        objWrapperMCData.officePhone = objWrapperOPhone;
        objWrapperPhone.phoneExtension = '';
        objWrapperPhone.telephoneNumber = mDataObjDC.get(FLDTELCEL).toString();
        objWrapperMCData.phone = objWrapperPhone;
        return objWrapperMCData;
    }

    /**
     * @description : Realiza el llenado del Wrapper en el apartado de CContD
     *              : para el armado del body del servicio ASO CreateCustomerData
     * @author:     : Alexandro Corzo
     * @return:     : MX_SB_VTS_wrpCreateCustomerDataSrv.cellphone objWrapperCPhone
     */
    public static MX_SB_VTS_wrpCreateCustomerDataSrv.correspondenceContactData fillCContD(Map<Object, Object> mDataObjDC, Map<Object, Object> mDataObjDP) {
        final MX_SB_VTS_wrpCreateCustomerDataSrv.correspondenceContactData objWrapperCContD = new MX_SB_VTS_wrpCreateCustomerDataSrv.correspondenceContactData();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.officePhone objWrapperOPhone = new MX_SB_VTS_wrpCreateCustomerDataSrv.officePhone();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.phone objWrapperPhone = new MX_SB_VTS_wrpCreateCustomerDataSrv.phone();
        objWrapperCContD.cellphone = fillCPhone('', mDataObjDC, mDataObjDP);
        objWrapperOPhone.phoneExtension = '';
        objWrapperOPhone.telephoneNumber = mDataObjDC.get(FLDTELCEL).toString();
        objWrapperCContD.officePhone = objWrapperOPhone;
        objWrapperPhone.phoneExtension = '';
        objWrapperPhone.telephoneNumber = mDataObjDC.get(FLDTELCEL).toString();
        objWrapperCContD.phone = objWrapperPhone;
        return objWrapperCContD;
    }

    /**
     * @description : Realiza el llenado del Wrapper en el apartado de FCData
     *              : para el armado del body del servicio ASO CreateCustomerData
     * @author:     : Alexandro Corzo
     * @return:     : MX_SB_VTS_wrpCreateCustomerDataSrv.cellphone objWrapperCPhone
     */
    public static MX_SB_VTS_wrpCreateCustomerDataSrv.fiscalContactData fillFCData(Map<Object, Object> mDataObjDC, Map<Object, Object> mDataObjDP) {
        final MX_SB_VTS_wrpCreateCustomerDataSrv.fiscalContactData objWrapperFCData = new MX_SB_VTS_wrpCreateCustomerDataSrv.fiscalContactData();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.officePhone objWrapperOPhone = new MX_SB_VTS_wrpCreateCustomerDataSrv.officePhone();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.phone objWrapperPhone = new MX_SB_VTS_wrpCreateCustomerDataSrv.phone();
        objWrapperFCData.cellphone = fillCPhone('', mDataObjDC, mDataObjDP);
        objWrapperOPhone.phoneExtension = '';
        objWrapperOPhone.telephoneNumber = mDataObjDC.get(FLDTELCEL).toString();
        objWrapperFCData.officePhone = objWrapperOPhone;
        objWrapperPhone.phoneExtension = '';
        objWrapperPhone.telephoneNumber = mDataObjDC.get(FLDTELCEL).toString();
        objWrapperFCData.phone = objWrapperPhone;
        return objWrapperFCData;
    }
}