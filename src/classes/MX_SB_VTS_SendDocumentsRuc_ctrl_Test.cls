/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 11-10-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   11-10-2020   Eduardo Hernández Cuamatzi   Initial Version
**/
@istest
public class MX_SB_VTS_SendDocumentsRuc_ctrl_Test {
    /**@description etapa Cotizacion*/
    private final static string STAGENAMEOPP = 'Cotización';
    /**Folio cotización*/
    private final static string FOLIOCOTASO = '9999999';
    /**URL Mock Test*/
    private final static String URLTOTESTASO = 'http://www.example.com';
    /**Particion IASO*/
    private final static String IASOPARTASO = 'local.MXSBVTSCache';
    
    @testSetup static void setup() {
        MX_SB_VTS_CallCTIs_utility.initHogarGeneric();
        final MX_SB_VTS_Generica__c setting = MX_WB_TestData_cls.GeneraGenerica('IASO01', 'IASO01');
        setting.MX_SB_VTS_Type__c = 'IASO01';
        setting.MX_SB_VTS_HEADER__c = URLTOTESTASO;
        insert setting;
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'GrantingTicketsDev', iaso__Url__c = URLTOTESTASO, iaso__Cache_Partition__c = IASOPARTASO);
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'getGTServicesSF', iaso__Url__c = URLTOTESTASO, iaso__Cache_Partition__c = IASOPARTASO);
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'sendDocumentsByMail', iaso__Url__c = URLTOTESTASO, iaso__Cache_Partition__c = IASOPARTASO);
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'getCustomerDocument', iaso__Url__c = URLTOTESTASO, iaso__Cache_Partition__c = IASOPARTASO);
    }

    /**Recupera Quote a sincronizar */
    public static Quote findQuote(Id oppId) {
        final Quote quoteData = [Select Id, Name from Quote where OpportunityId =: oppId];
        quoteData.MX_SB_VTS_ASO_FolioCot__c = FOLIOCOTASO;
        quoteData.MX_SB_VTS_Nombre_Contrante__c = 'Test';
        quoteData.MX_SB_VTS_Apellido_Paterno_Contratante__c = 'Test';
        quoteData.MX_SB_VTS_Apellido_Materno_Contratante__c = 'Test';
        quoteData.MX_SB_VTS_RFC__c = '12345678';
        quoteData.MX_SB_VTS_Email_txt__c = 'test@test.com';
        quoteData.MX_SB_VTS_Numero_de_Poliza__c = FOLIOCOTASO;
        return quoteData;
    }

    @isTest
    static void findCustomerDocument() {
        final Map<String, String> headersMock = new Map<String, String>();
        headersMock.put('tsec', '368897844');
        final MX_WB_Mock mockCalloutTest = new MX_WB_Mock(200, 'Complete', '{"document": {"data": "test","size": 66545,"id": "GMDSEHO001"},"href": "http://150.100.104.43:36026/segurosmedc/RUC_1604859637892_193.pdf"}', headersMock);
        iaso.GBL_Mock.setMock(mockCalloutTest);
        final Opportunity oppRec = [Select Id, Name, SyncedQuoteId from Opportunity where StageName =: STAGENAMEOPP];
        final Quote findingQuote = findQuote(oppRec.Id);
        update findingQuote;
        MX_SB_VTS_PaymentModule_Service_Test.syncQuoteData(oppRec, findingQuote.Id);
        Test.startTest();
            final Map<String, Object> mapResponse = MX_SB_VTS_SendDocumentsRuc_ctrl.findCustomerDocument(oppRec.Id, 'policy');
            System.assert((Boolean)mapResponse.get('isOk'), 'Poliza enviada');
        Test.stopTest();
    }

    @isTest
    static void findCustomerDocumentKit() {
        final Map<String, String> headersMockKit = new Map<String, String>();
        headersMockKit.put('tsec', '368897844');
        final MX_WB_Mock mockKitTest = new MX_WB_Mock(200, 'Complete', '{"document": {"data": "test","size": 66545,"id": "GMDSEHO001"},"href": "http://150.100.104.43:36026/segurosmedc/RUC_1604859637892_193.pdf"}', headersMockKit);
        iaso.GBL_Mock.setMock(mockKitTest);
        final Opportunity oppRec = [Select Id, Name, SyncedQuoteId from Opportunity where StageName =: STAGENAMEOPP];
        final Quote fingQuoteKit = findQuote(oppRec.Id);
        update fingQuoteKit;
        MX_SB_VTS_PaymentModule_Service_Test.syncQuoteData(oppRec, fingQuoteKit.Id);
        Test.startTest();
            final Map<String, Object> mapResKit = MX_SB_VTS_SendDocumentsRuc_ctrl.findCustomerDocument(oppRec.Id, 'kit');
            System.assert((Boolean)mapResKit.get('isOk'), 'Kit enviado');
        Test.stopTest();
    }

    @isTest
    static void sendCustomerDocument() {
        final Map<String, String> headersSend = new Map<String, String>();
        headersSend.put('tsec', '3647844');
        final MX_WB_Mock mockKitTest = new MX_WB_Mock(200, 'Complete', '{"href": "http://150.100.104.43:36026/segurosmedc/RUC_1604859637892_193.pdf"}', headersSend);
        iaso.GBL_Mock.setMock(mockKitTest);
        final Opportunity oppRec = [Select Id, Name, SyncedQuoteId from Opportunity where StageName =: STAGENAMEOPP];
        final Quote findingQuote = findQuote(oppRec.Id);
        update findingQuote;
        MX_SB_VTS_PaymentModule_Service_Test.syncQuoteData(oppRec, findingQuote.Id);
        final List<String> lstDocsUrl = new List<String>{URLTOTESTASO, URLTOTESTASO};
        Test.startTest();
            final Map<String, Object> mapResSend = MX_SB_VTS_SendDocumentsRuc_ctrl.sendCustomerDocument(oppRec.Id, lstDocsUrl);
            System.assert((Boolean)mapResSend.get('isOk'), 'Documentos enviados');
        Test.stopTest();
    }
}