/**
* @description       : 
* @author            : Daniel.perez.lopez.contractor@bbva.com
* @group             : 
* @last modified on  : 02-06-2020
* @last modified by  : Arsenio.perez.lopez.contractor@bbva.com
* Modifications Log 
* Ver   Date         Author                                    Modification
* 1.0   11-24-2020   Daniel.perez.lopez.contractor@bbva.com   Initial Version
* 1.1   02-06-2020   Arsenio.perez.lopez.contractor@bbva.com  Se modifica makeData
* 1.2   03-10-2020   juancarlos.benitez.herra.contractor@bbva.com  Se modifican parms de testcreateBeneficiaries
**/
@isTest
public class MX_SB_PS_Datos_AseguradoExt_Ctrl_test {
    @TestSetup
    static void makeData() {
        MX_SB_PS_Resumen_Cotizacion_Helper_Test.Data();
        final Contact cntc = [SELECT Id, email, MailingPostalCode,MX_WB_ph_Telefono1__c, FirstName, LastName, birthdate, Apellido_materno__c, MX_RTL_CURP__c from Contact limit 1];
        cntc.email='marco@domain.mx';
        update cntc;
        final list<Quotelineitem> updateQuoli = new List<Quotelineitem>();
        for(Quotelineitem quoli: [Select Id,MX_SB_VTS_Version__c from QuoteLineItem limit 100]) {
        	quoli.MX_SB_VTS_Version__c ='001,002,003';
            updateQuoli.add(quoli);
        }
        update updateQuoli;
    }
    
    /**
* @description 
* @author Daniel.perez.lopez.contractor@bbva.com | 11-24-2020 
**/
    @isTest
    static void getcreateCustomerData() {
        final Opportunity oprtdata = [SELECT Id  from Opportunity limit 1];
        Final Quote quotObj = [SELECT Id from Quote where OpportunityId=:oprtdata.Id limit 1];
        quotObj.BillingStreet='Asturias,62,B101';
        update quotObj;
        final Contact cntc= [SELECT Id, email, MailingPostalCode,MX_WB_ph_Telefono1__c, FirstName, LastName, birthdate, Apellido_materno__c, MX_RTL_CURP__c from Contact limit 1];
        cntc.MailingPostalCode='77887';
        cntc.birthdate= Date.today().addYears(-8);
        update cntc;
        final Map<String,String> mapdata= new Map<String,String>();
        mapdata.put('rfc','BEHJ930405');
        test.startTest();
        try {
            MX_SB_PS_Datos_AseguradoExt_Ctrl.createCustomerData(oprtdata.id,cntc,mapdata);
        } catch (Exception e) {
            final Boolean expectedE =  e.getMessage().contains(''+System.Label.MX_SB_PS_ErrorGenerico+'') ? false : true;
            System.Assert(expectedE, 'Salida Exitosa');
        }
        test.stopTest();
    }
    @istest
    static void testdirwraper() {
        final Contact cntc= [SELECT Id, email, MailingPostalCode,MX_WB_ph_Telefono1__c, FirstName, LastName, birthdate, Apellido_materno__c, MX_RTL_CURP__c from Contact limit 1];
        final Map<String,String> mapadata= new Map<String,String>();
        test.startTest();
            final MX_SB_PS_wrpCreateCustomerData.mainAddress drwrap =MX_SB_PS_Datos_AseguradoExt_Ctrl.dirwraper(cntc,mapadata);
            system.assertEquals(drwrap.door,null,'Ha respondido exitosamente');
        test.stopTest();
    }
    
    /**
* @description 
* @author Daniel.perez.lopez.contractor@bbva.com | 11-24-2020 
**/
    @isTest
    static void getcreateQuoteBeneficiary() {
        final String queat = [Select Id from Quote limit 1].Id;
        final Opportunity oprtdata = [SELECT Id, syncedquoteid, Account.PersonContact.FirstName,Account.PersonContact.LastName, Account.PersonContact.Name  from Opportunity limit 1];
        test.startTest();
        try {
            MX_SB_PS_Datos_AseguradoExt_Ctrl.createQuoteBeneficiary(queat, 'slia12', oprtdata);
        } catch (Exception e) {
            final Boolean expectedEn =  e.getMessage().contains(''+System.Label.MX_SB_PS_ErrorGenerico+'') ? false : true;
            System.Assert(expectedEn, 'Salida 2');
        }
        test.stopTest();
    }
    /**
* @description 
* @author Daniel.perez.lopez.contractor@bbva.com | 11-24-2020 
**/
    @isTest
    static void getficalperson() {
        final Contact cntc= [SELECT Id, email, MailingPostalCode,MX_WB_ph_Telefono1__c, FirstName, LastName, birthdate, Apellido_materno__c, MX_RTL_CURP__c from Contact limit 1];
        cntc.MailingPostalCode='77887';
        cntc.birthdate= Date.today().addYears(-8);
        update cntc;
        final MX_SB_VTS_Beneficiario__c benes = [Select Id, Name,MX_SB_VTS_APaterno_Beneficiario__c,MX_SB_PS_CURP__c,MX_SB_VTS_AMaterno_Beneficiario__c from MX_SB_VTS_Beneficiario__c limit 1];
        benes.MX_SB_PS_CURP__c='ROVI490617HSPDSS05';
        update benes;
        final Opportunity oprtdata = [SELECT Id, syncedquoteid, Account.PersonContact.FirstName,Account.PersonContact.LastName, Account.PersonContact.Name, Account.Genero__c  from Opportunity limit 1];
        test.startTest();
        final MX_SB_PS_wrpCreateCustomerData.physicalPersonalityData ret = MX_SB_PS_Datos_AseguradoExt_Ctrl.ficalperson(cntc,oprtdata,benes);
        MX_SB_PS_Datos_AseguradoExt_Ctrl.ficalperson(cntc,oprtdata);
        MX_SB_PS_Datos_AseguradoExt_Ctrl.ordeBeneficiarios(oprtdata.syncedquoteid);
        System.assert(ret!=null,'Sale');
        test.stopTest();
    }
@isTest
    static void testcreateBeneficiaries() {
        Final String oppId=[Select Id from Opportunity limit 1].Id;
        Final List<Map<String,String>>lmapa= new List<Map<String,String>>();
        test.startTest();
        try {
        	MX_SB_PS_Datos_Asegurado_Ctrl.createBeneficiaries(lmapa,oppId,'regular');
        } catch(Exception e) {
        	final Boolean expectedError = e.getMessage().contains('Script') ? false : true;
        	System.assert(expectedError, 'Se han creado beneficiarios');
            }
        test.stopTest();
    }
    @isTest
    static void testgetcolonias() {
        test.startTest();
        try {
            MX_SB_PS_Datos_Asegurado_Ctrl.getcolonias('55764');
        } catch(Exception e) {
            System.assert(e.getMessage().contains('No content'), 'message=' + e.getMessage());
        }
        test.stopTest();
    }
    @istest
    static void testcorespdir() {
        final Opportunity oprtdata = [SELECT Id  from Opportunity limit 1];
        Final Quote quotObj = [SELECT Id from Quote where OpportunityId=:oprtdata.Id limit 1];
        Final QuotelineItem quoli =[Select Id from QuoteLineItem where QuoteId=:quotObj.Id limit 1];
        quoli.MX_SB_VTS_Version__c='001,002';
        update quoli;
        quotObj.BillingStreet='Asturias';
        update quotObj;
        final Contact cntc= [SELECT Id, email, MailingPostalCode,MX_WB_ph_Telefono1__c, FirstName, LastName, birthdate, Apellido_materno__c, MX_RTL_CURP__c from Contact limit 1];
        cntc.MailingPostalCode='77887';
        cntc.birthdate= Date.today().addYears(-8);
        update cntc;
        final MX_SB_PS_wrpCreateCustomerData.mainAddress str = new MX_SB_PS_wrpCreateCustomerData.mainAddress();
        test.startTest();
        try {
            MX_SB_PS_Datos_AseguradoExt_Ctrl.correspdirwraper(oprtdata, str);
        } catch (Exception e) {
            final Boolean expectedE =  e.getMessage().contains(''+System.Label.MX_SB_PS_ErrorGenerico+'') ? false : true;
            System.Assert(expectedE, 'Salida Exitosa');
        }
        test.stopTest();
    }
}