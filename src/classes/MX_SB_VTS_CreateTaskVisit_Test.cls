@isTest
/**
 * @File Name          : MX_SB_VTS_CreateTaskVisit_Test.cls
 * @Description        : Test class for MX_SB_VTS_CreateTaskVisit methods
 * @Author             : Marco Antonio Cruz
 * @Group              :
 * @Last Modified By   : Marco Antonio Cruz
 * @Last Modified On   : 07/09/2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    07/09/2020      Marco Antonio Cruz       Initial Version
**/
private class MX_SB_VTS_CreateTaskVisit_Test {
	
    /** Nombre de usuario Test */
    final static String NAMEUSER = 'Usuario de Prueba';
    
    @TestSetup
    static void dataSetUp() {
        Final User user4Test = MX_WB_TestData_cls.crearUsuario(NAMEUSER, System.Label.MX_SB_VTS_ProfileAdmin);  
        insert user4Test;
        System.runAs(user4Test) {
            final Lead leadTest = MX_WB_TestData_cls.createLead('Lead Test');
            leadTest.FirstName = 'Testing';
            leadTest.TelefonoCelular__c = '5534251637';
            leadTest.Status = 'Apertura de Cuenta';
            leadTest.Producto_Interes__c = 'Seguro Estudia';
            leadTest.LeadSource = 'Call me back';
            leadTest.Resultadollamada__c = 'No Contacto';
            leadTest.MX_SB_VTS_Tipificacion_LV2__c = 'No Contacto';
            leadTest.MX_SB_VTS_Tipificacion_LV3__c = 'No Aplica';
            leadTest.MX_SB_VTS_Tipificacion_LV4__c = 'No Aplica';
            leadTest.MX_SB_VTS_Tipificacion_LV5__c = 'No Venta (No contacto)';
            leadTest.MX_SB_VTS_Tipificacion_LV6__c = 'Teléfono inexistente o tritono';
            leadTest.MX_SB_VTS_Motivo_de_mayor_avance__c = 'Teléfono inexistente o tritono';
            leadTest.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
            insert leadTest;
        }
    }
    
     /**
     * @description Method to test insertVisit method from Dwp_kitv_Visit_Service
     * Author: Marco Cruz
	 */
    @isTest
    static void createVisitTaskTest() {
        Final User userToTest = [SELECT Id From User WHERE Name =:NAMEUSER Limit 1];
        System.runAs(userToTest) {
            Test.startTest();
                Final Lead leadToTest = [SELECT Id, Name, FirstName, TelefonoCelular__c, Status, Producto_Interes__c, LeadSource, Resultadollamada__c,
                                        MX_SB_VTS_Tipificacion_LV2__c, MX_SB_VTS_Tipificacion_LV3__c, MX_SB_VTS_Tipificacion_LV4__c, MX_SB_VTS_Tipificacion_LV5__c,
                                        MX_SB_VTS_Tipificacion_LV6__c, MX_SB_VTS_Motivo_de_mayor_avance__c, MX_SB_VTS_CodCampaFace__c, MX_SB_VTS_Grado_de_Avance__c FROM Lead WHERE FirstName = 'Testing'];
                Final List<Lead> listToTest = new List<Lead> {leadToTest};
            	MX_SB_VTS_CreateTaskVisit_Ctrl.createVisitTask(listToTest);
            Test.stopTest();
        }
        System.assert(true,'Task y Visita creada');
    }
    
}