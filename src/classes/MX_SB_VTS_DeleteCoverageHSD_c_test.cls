/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 03-04-2021
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   01-28-2021   Eduardo Hernández Cuamatzi   Initial Version
 * 1.1   02-03-2021   Diego Olvera                 Se agrega mapa con valores para cubrir cobertura
**/
@isTest
private class MX_SB_VTS_DeleteCoverageHSD_c_test {

    /**GLASS_BREAKAGE coverages*/
    private final static String GLASSBREAKAGE = 'GLASS_BREAKAGE';
    /**URL Mock Test*/
    private final static String URLTOTESTASO = 'http://www.example.com';
    /**Particion IASO*/
    private final static String IASOPARTASO = 'local.MXSBVTSCache';
    /**@description etapa Cotizacion*/
    private final static string COTIZACION = 'Cotización';
    /**@description Nombre usuario*/
    private final static string COMPLETE = 'Complete';
    /**Servicio IASO*/
    private final static String CALCULATEPRICES = 'CalculateQuotePrice';
    /**INCE Trade*/
    private final static String INCE = 'INCE';
    /**@description llave de mapa*/
    private final static string TOKEN = '12345678';
    /**@description llave de ERROR*/
    private final static string TSEC = 'tsec';
    /**@description llave de ERROR*/
    private final static string PORCCRISTAL = '2008PORCCRISTAL';

    @TestSetup
    static void makeData() {
        MX_SB_VTS_CallCTIs_utility.initHogarGeneric();
        final Opportunity oppIdCrit = [Select Id, Name from Opportunity where StageName =: COTIZACION];
        oppIdCrit.Producto__c = System.Label.MX_SB_VTS_PRO_HogarDinamico_LBL;
        oppIdCrit.LeadSource = 'Facebook';
        final Map<Id, Quote> mapQuoteData = new Map<Id, Quote>([Select Id, Name from Quote where OpportunityId =: oppIdCrit.Id]);
        for(Quote quoteItem : mapQuoteData.values()) {
            quoteItem.MX_SB_VTS_ASO_FolioCot__c = '12345';
            oppIdCrit.SyncedQuoteId = quoteItem.Id;
            MX_RTL_Cobertura_Selector_Test.initCoverages(quoteItem.Id);
            quoteItem.QuoteToName = 'Facebook-Completa';
        }
        update mapQuoteData.values();
        update oppIdCrit;
        MX_SB_VTS_CallCTIs_utility.initLeadMulServHSD();
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'getGTServicesSF', iaso__Url__c = URLTOTESTASO, iaso__Cache_Partition__c = IASOPARTASO);
        insert new iaso__GBL_Rest_Services_Url__c(Name = CALCULATEPRICES, iaso__Url__c = URLTOTESTASO, iaso__Cache_Partition__c = IASOPARTASO);
        final MX_SB_VTS_Generica__c setting = MX_WB_TestData_cls.GeneraGenerica('IASO01', 'IASO01');
        setting.MX_SB_VTS_Type__c = 'IASO01';
        setting.MX_SB_VTS_HEADER__c = URLTOTESTASO;
        insert setting;
    }

    @isTest
    static void removeCoverages() {
        final Opportunity oppIdCrit = [Select Id, Name,Producto__c from Opportunity where StageName =: COTIZACION];
        final String calculateStr = MX_SB_VTS_ScheduldOppsTele_tst.quotetationStr(CALCULATEPRICES);
        Final Map<String, String> headersMock = new Map<String, String>();
        headersMock.put(TSEC, TOKEN);
        Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, COMPLETE, calculateStr, headersMock); 
        iaso.GBL_Mock.setMock(mockCallout);
        Test.startTest();
            final List<String> coveragesCodes = new List<String>{GLASSBREAKAGE};
            final Map<String, Object> dataMap = new Map <String, Object>();
            dataMap.put('ramo', 'INCE');
        	dataMap.put('isChecked', false); 
            final Map<String, Object> bodyRemoveCov = MX_SB_VTS_DeleteCoverageHSD_ctrl.removeCoverages(oppIdCrit.Id, coveragesCodes, dataMap);
            System.assert((Boolean)bodyRemoveCov.get('isOk'), 'Cobertura eliminada');
        Test.stopTest();
    }

    @isTest
    static void editPetsHSD() {
        final Opportunity oppIdCrit = [Select Id, Name,Producto__c from Opportunity where StageName =: COTIZACION];
        final String calculateStr = MX_SB_VTS_ScheduldOppsTele_tst.quotetationStr(CALCULATEPRICES);
        Final Map<String, String> headersMock = new Map<String, String>();
        headersMock.put(TSEC, TOKEN);
        Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, COMPLETE, calculateStr, headersMock); 
        iaso.GBL_Mock.setMock(mockCallout);
        Test.startTest();
            final List<String> valuesCriterial = new List<String>{INCE, '002', '240', 'BUILDINGS', 'BUILDING', 'YEARLY'};
            final List<String> critCode = new List<String>{'2008ASMASCOTAS'};
            final Map<String, Object> bodyRemoveCov = MX_SB_VTS_DeleteCoverageHSD_ctrl.editPetsHSD(oppIdCrit.Id, critCode, valuesCriterial, false);
            System.assert((Boolean)bodyRemoveCov.get('isOk'), 'Asistencia eliminada');
        Test.stopTest();
    }

    @isTest
    static void fillMapCritical() {
        final Opportunity oppIdCrit = [Select Id, Name,Producto__c from Opportunity where StageName =: COTIZACION];
        final Quote mapQuoteData = [Select Id, Name from Quote where OpportunityId =: oppIdCrit.Id];
        final List<String> critCode = new List<String>{PORCCRISTAL, 'HOME_BURGLARY', 'FIRE_LIGHTNING'};
        final List<Cobertura__c> mapCoverages = MX_SB_VTS_DeleteCoverageHSD_Helper.findCoverCodes(mapQuoteData.Id, critCode);
        Test.startTest();
            final Map<String, Object> bodyRemoveCov = MX_SB_VTS_DeleteCoverageHSD_Helper.fillMapCritical(mapCoverages);
            System.assert(bodyRemoveCov.containsKey(PORCCRISTAL), 'Asistencia eliminada');
        Test.stopTest();
    }

    @isTest
    static void fillAllDataCovers() {
        final Opportunity oppIdCrit = [Select Id, Name,Producto__c from Opportunity where StageName =: COTIZACION];
        Test.startTest();
            final Map<String, Object> fillAllData = MX_SB_VTS_DeleteCoverageHSD_ctrl.fillAllDataCovers((String)oppIdCrit.Id, 'Completa');
            final List<Opportunity> lstOpps = (List<Opportunity>)((Map<String, Object>)fillAllData.get('dataCot')).get('dataOpp');
            System.assertEquals(lstOpps[0].Id, oppIdCrit.Id, 'Datos recuperados');
        Test.stopTest();
    }

    @isTest
    static void syncQuoteByType() {
        final Opportunity oppIdCrit = [Select Id, Name,Producto__c from Opportunity where StageName =: COTIZACION];
        Test.startTest();
            final Map<String, Object> syncQuote = MX_SB_VTS_DeleteCoverageHSD_ctrl.syncQuoteByType((String)oppIdCrit.Id, 'Completa');
            System.assert((Boolean)syncQuote.get('isOk'), 'Quote Ok');
        Test.stopTest();
    }
}