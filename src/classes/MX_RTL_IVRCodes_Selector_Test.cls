/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 10-20-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   10-12-2020   Eduardo Hernández Cuamatzi   Initial Version
**/
@isTest
public class MX_RTL_IVRCodes_Selector_Test {
    @isTest 
    static void findByTypeVal() {
        Test.startTest();
            String lstFields = 'Id, MasterLabel, MX_RTL_IVRCode__c, MX_RTL_Group__c, MX_RTL_MsgIVR__c,';
            lstFields += 'MX_RTL_MsgSF__c, MX_RTL_TitleMsg__c';
            final Set<String> lstTypes = new Set<String>{'VTS'};
            final List<MX_RTL_IVRCodes__mdt> lstIvrCode = MX_RTL_IVRCodes_Selector.findByTypeVal(lstFields, lstTypes, '3003');
            System.assertEquals(lstIvrCode[0].MX_RTL_TitleMsg__c, 'Tarjeta sin fondos suficientes', 'Mensaje traducido');
        Test.stopTest();
    }
}