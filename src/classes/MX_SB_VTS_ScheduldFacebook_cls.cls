/**
 * @File Name          : MX_SB_VTS_ScheduldFacebook_cls.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 21/4/2020 21:48:10
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    16/4/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
public without sharing class MX_SB_VTS_ScheduldFacebook_cls {

    /**Constructor */
    private MX_SB_VTS_ScheduldFacebook_cls() {} //NOSONAR

    /**
     * sendOppsHogar envia Oportunidades a SmartCenter
     */
    public static void sendOppsFace(List<ProcNotiOppNoAten__c> lsNotif, Map<Id,ProcNotiOppNoAten__c> mapOldNotif) {
        for (ProcNotiOppNoAten__c procNoti : lsNotif) {
            if(String.isNotBlank(procNoti.Proceso__c) && procNoti.Proceso__c.equals(System.Label.MX_SB_VTS_FacebookScheduled) && procNoti.Activo__c) {
                createSchedulLeads(procNoti);
            }
        }
    }

    /**
     * createSchedulLeads crear query base de scheduld
     * @param  registro de trabajos programados
     */
    public static void createSchedulLeads(ProcNotiOppNoAten__c lsNotif) {
        final MX_SB_VTS_SendFacebookLeads_sch schedulFaceLeads = new MX_SB_VTS_SendFacebookLeads_sch();
        String sQuery = 'Select Id, LeadSource, MobilePhone, Producto_Interes__c, FirstName, LastName, Apellido_Materno__c, MX_SB_VTS_HoraCallMe__c, ';
        sQuery += 'MX_SB_VTS_FechaHoraAgenda__c, EnviarCTI__c , Folio_Cotizacion__c, OwnerId from Lead';
        final String sch = '0 ' + lsNotif.MX_SB_VTS_ScheduledTime__c  + ' 0-23 1-31 * ? *';
        schedulFaceLeads.sQuery = sQuery;
        System.schedule('Facebook Leads : '+lsNotif.MX_SB_VTS_ScheduledTime__c, sch, schedulFaceLeads);
    }
}