/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Minuta_BienvenidaDO_Ctrl
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2021-01-11
* @Description 	MX_BPP_Minuta_BienvenidaDO_VF Controller
* @Changes
* 2021-22-02		Héctor Saldaña	  	Call to Service class on constructor
*/
public without sharing class MX_BPP_Minuta_BienvenidaDO_Ctrl {
    /*Accesor dwp_kitv__Visit__c visitObj*/
    public dwp_kitv__Visit__c visitObj {get;set;}
    /*Accesor User userObj*/
    public User userObj {get;set;}
    /*Accesor boolean correoEnviado*/
    public boolean correoEnviado {get;set;}
    /*Accesor boolean enviar*/
    public boolean enviar {get;set;}
    /*Accesor List<ContentVersion> documents*/
    transient public List<ContentVersion> documents {get;set;}
    /*Accesor boolean vfError*/
    public boolean vfError {get;set;}
    /*Accesor dwp_kitv__Template_for_type_of_visit_cs__c myCS*/
    public dwp_kitv__Template_for_type_of_visit_cs__c myCS {get;set;}
    /*Accesor String imgHeader */
    public String imgHeader {get; set;}
    /*Catalogo Minuta Property to instantiate a new Catalogo Minuta Object*/
    public MX_BPP_CatalogoMinutas__c catalogoMinuta {get; set;}
    /*Page Reference accesor to the current Visualforce*/
    public PageReference currentPageRef {get;set;}
    /*Minuta wrapper porperty to access de member variables from this class*/
    public MX_BPP_MinutaWrapper minutaWrapper {get;set;}
    /*
    * @Descripción Constructor de la clase
    * @return No retorna nada
    */
    public MX_BPP_Minuta_BienvenidaDO_Ctrl() {
        this.minutaWrapper = new MX_BPP_MinutaWrapper();
        this.minutaWrapper.catalogoPty.sIdCatalogo = MX_BPP_Minuta_Utils.getParameter('sCatalogoId');
        vfError = false;
        this.catalogoMinuta = String.isBlank(this.minutaWrapper.catalogoPty.sIdCatalogo) ?  new MX_BPP_CatalogoMinutas__c() : MX_BPP_Minuta_Template_Service.getCatalogoMinuta(this.minutaWrapper.catalogoPty.sIdCatalogo);
        final List<String> resourcesNames = new List<String> {this.catalogoMinuta.MX_ImageHeader__c};
        final Map<String, String> imgResources = MX_BPP_Minuta_Template_Service.getImageResources(resourcesNames);
        this.imgHeader = Test.isRunningTest() ? '' : imgResources.get(this.catalogoMinuta.MX_ImageHeader__c);
        this.visitObj = MX_BPP_Minuta_Service.getRecordInfo(this.minutaWrapper.configParamsPty.currentId);
        this.currentPageRef = Page.MX_BPP_Minuta_BienvenidaDO_VF;

        if(String.isNotBlank(this.minutaWrapper.configParamsPty.sDocuments.unescapeHtml4())) {
            this.documents = (List<ContentVersion>)System.JSON.deserialize(this.minutaWrapper.configParamsPty.sDocuments.unescapeHtml4(), List<ContentVersion>.class);
        }
        if(String.isNotBlank(this.minutaWrapper.configParamsPty.sMyCS.unescapeHtml4())) {
            this.enviar = true;
            this.myCS =(dwp_kitv__Template_for_type_of_visit_cs__c)System.JSON.deserialize(this.minutaWrapper.configParamsPty.sMyCS.unescapeHtml4(), dwp_kitv__Template_for_type_of_visit_cs__c.class);
        }

        this.userObj = MX_BPP_Minuta_Template_Service.getUserDetails(this.visitObj.OwnerId);
        MX_BPP_Minuta_Template_Service.replaceInnerTags(this.catalogoMinuta, this.visitObj.Id);
    }

    /*
    * @Description Send Mail Method
    * @Params Void
    * @Return NA
    **/
    public void sendMail() {
        
        if(enviar == true) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'La minuta se esta enviando...'));
            final String statusOldVisit = visitObj.dwp_kitv__visit_status_type__c;

            try {
                final Messaging.SendEmailResult[] results = MX_BPP_Minuta_Template_Service.sendTemplateEmail(visitObj, myCS, documents, statusOldVisit, userObj, vfError, this.currentPageRef, this.catalogoMinuta);
                if (results[0].isSuccess() && !vfError) {
                    correoEnviado = true;
                } else {
                    correoEnviado = false;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error en el envío del email: ' + results[0].errors[0].message));
                }
            } catch (Exception ex) {
                correoEnviado = false;
                dwp_kitv.Minute_Generator_Ctrl.updateStatusVisit(visitObj, statusOldVisit);
                String errMessage = ex.getMessage();
                Integer occurence;
                if (errMessage.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') || vfError) {
                    occurence = errMessage.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
                    errMessage = errMessage.mid(occurence, errMessage.length()).mid(errMessage.indexOf(',') + 1, errMessage.length()).mid(0, errMessage.lastIndexOf(':'));
                } else {
                    errMessage = ex.getMessage();
                }
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error en el envío del email: ' + errMessage ));
            }
        }
    }

}