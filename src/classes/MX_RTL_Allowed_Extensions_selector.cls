/**
* ---------------------------------------------------------------------------------
* @Autor: Rodrigo Amador Martinez Pacheco
* @Proyecto: Auditoria
* @Descripción : Selector para consumo de CustomMetadata MX_RTL_Allowed_Extensions_selector
* ---------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ---------------------------------------------------------------------------------
* 1.0           18/06/2020     Rodrigo Martinez               Creación de la clase
* ---------------------------------------------------------------------------------
*/

@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_RTL_Allowed_Extensions_selector {
    
    /**
	* 
	* @Descripción : Retorna un elemento en la lista que coincida con la extension registrada
    * en el custom metadata, recive como argumento la extension de un archivo con . por ejemplo
    *  ".pdf"
	* @author Rodrigo Martinez
	*/
    public static List<MX_RTL_Allowed_Extensions__mdt> selectByExtension(String extension) {
        return [SELECT ID FROM MX_RTL_Allowed_Extensions__mdt where extension__c = :extension];
    }

}