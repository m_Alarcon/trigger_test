@isTest
/**
* @File Name          : MX_RTL_Contract_Selector_Test.cls
* @Description        : Clase que prueba los methods de MX_RTL_Contract_Selector
* @Author             : Juan Carlos Benitez
* @Group              :
* @Last Modified By   : Juan Carlos Benitez
* @Last Modified On   : 06/06/2020, 06:07:22 PM
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    5/27/2020    Juan Carlos Benitez         Initial Version
* 1.1    6/11/2020     Marco Antonio Cruz       Add methods for testing
**/
public class MX_RTL_Contract_Selector_Test {
    /** Campos consulta de contract */
    public static Final String CNTRCTFIELDS=' Id ';
    /** Clausula campo SAC_NumeroPoliza */
    public static Final String CLAUSENPOL ='MX_SB_SAC_NumeroPoliza__c';
    /**Var Nombre **/
    final static String NAME ='Mariana';
    /**Var SAC_NO_POLIZA **/
    final static String SAC_NO_POLIZA='3304030';
    /**Variable para RFC de Contrato**/
    final static String RFCTEST='CUBM364859PF';
	/**Variable para RFC de Contrato**/
    final static String CERTTEST='LORD3728';
    /**variable para Apellido de Contrato **/
	final static String TESTLNAME ='Alcantara';
	/*
	*Variable String para DML
	*/
    Final static String DMLTEST = 'Id,MX_SB_MLT_Certificado__c,MX_SB_SAC_FechaFinContrato__c,MX_SB_SAC_EstatusPoliza__c,MX_WB_Producto__c,createddate,accountid,MX_WB_celularAsegurado__c,MX_SB_SAC_EmailAsegurado__c,' +
        'MX_SB_SAC_RFCAsegurado__c,MX_SB_SAC_NumeroPoliza__c,MX_SB_SB_Placas__c,MX_SB_SAC_NombreClienteAseguradoText__c, ' +
        'MX_WB_apellidoMaternoAsegurado__c,MX_WB_apellidoPaternoAsegurado__c,MX_SB_SAC_Segmento__c, ' +
        'Description,StartDate,EndDate,MX_SB_SAC_TipoMoneda__c';
    /*
    *Variable String para DML
    */
    Final static String DMLTEST2 = 'Id, MX_SB_MLT_Certificado__c,MX_SB_SAC_FechaFinContrato__c,MX_SB_SAC_EstatusPoliza__c,MX_WB_Producto__r.Name, ' +
        'createddate,accountid,MX_WB_celularAsegurado__c,MX_SB_SAC_EmailAsegurado__c, ' +
        'MX_SB_SAC_RFCAsegurado__c,MX_SB_SAC_NumeroPoliza__c,MX_SB_SB_Placas__c,MX_SB_SAC_NombreClienteAseguradoText__c, ' +
        'MX_WB_apellidoMaternoAsegurado__c,MX_WB_apellidoPaternoAsegurado__c,MX_SB_SAC_Segmento__c, ' +
        'Description,StartDate,EndDate,MX_SB_SAC_TipoMoneda__c';
    
    
    @TestSetup
    static void testData() {
        Final String profileName = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User usr = MX_WB_TestData_cls.crearUsuario(NAME, profileName);  
        System.runAs(usr) {
            final String accRTPr = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Proveedores_MA).getRecordTypeId();
            final List<Account> accsToInsert = new List<Account>();
            for(Integer i=0; i<10; i++) {
                final Account nAccount= new Account();
                nAccount.RecordTypeId=accRTPr;
                nAccount.Name=NAME+'_'+i;
                nAccount.Tipo_Persona__c='Física';
                accsToInsert.add(nAccount);
            }
            Database.insert(accsToInsert);
            final List<Contract> contractToInsert = new List<Contract>();
            for(Integer iter = 0; iter < 10; iter++) {
                final Contract nContract = new Contract();
                nContract.AccountId=accsToInsert[0].Id;
                nContract.MX_SB_SAC_NumeroPoliza__c ='3304030'+iter;
                nContract.MX_SB_SAC_NombreClienteAseguradoText__c = NAME+'_'+ iter;
                nContract.MX_WB_apellidoPaternoAsegurado__c = TESTLNAME + '_' + iter;
                nContract.MX_SB_SAC_RFCAsegurado__c = RFCTEST + iter;
                nContract.MX_SB_MLT_Certificado__c = CERTTEST + iter;
                contractToInsert.add(nContract);
            }
            Database.insert(contractToInsert);
        }
    }
    @isTest
    static void tstgetCtrctBySACnPol () {
		Final List<Contract> policyTest =[SELECT Id, MX_SB_SAC_NumeroPoliza__c FROM Contract WHERE MX_SB_SAC_NumeroPoliza__c  LIKE: '%' + SAC_NO_POLIZA+ '%' ];
        test.startTest();
        	MX_RTL_Contract_Selector.getContractBySACnPoliza(policyTest[0].MX_SB_SAC_NumeroPoliza__c , CNTRCTFIELDS, CLAUSENPOL);
        	system.assert(true,'Consulta Exitosa');
        test.stopTest();
    }
    
    @isTest
    static void testContractIdByNombre () {
		Final List<Contract> testContractName =[SELECT Id, MX_SB_SAC_NombreClienteAseguradoText__c, MX_WB_apellidoPaternoAsegurado__c
                                          FROM Contract WHERE MX_SB_SAC_NumeroPoliza__c  LIKE: '%' + SAC_NO_POLIZA+ '%' ];
        test.startTest();
        	MX_RTL_Contract_Selector.getContractIdByNombre(testContractName[0].MX_SB_SAC_NombreClienteAseguradoText__c , testContractName[0].MX_WB_apellidoPaternoAsegurado__c, DMLTEST);
        	system.assert(true,'Consulta correcta');
        test.stopTest();
    }
    
    @isTest
    static void testContractIdByRFC () {
		Final List<Contract> testContractRFC =[SELECT Id, MX_SB_SAC_RFCAsegurado__c
                                          FROM Contract WHERE MX_SB_SAC_NumeroPoliza__c  LIKE: '%' + SAC_NO_POLIZA+ '%' ];
        test.startTest();
        	MX_RTL_Contract_Selector.getContractIdByRFC(testContractRFC[0].MX_SB_SAC_RFCAsegurado__c , DMLTEST);
        	system.assert(true,'exito en consultar');
        test.stopTest();
    }
    
    @isTest
    static void testContractIdByAccount () {
        Final List<Contract> testContractAcc =[SELECT Id, accountId
                                               FROM Contract WHERE MX_SB_SAC_NumeroPoliza__c  LIKE: '%' + SAC_NO_POLIZA+ '%' ];
        test.startTest();
        MX_RTL_Contract_Selector.getContractIdByAccount(testContractAcc[0].accountId, DMLTEST);
            system.assert(true,'la consulta es un exito');
        test.stopTest();
    }
    
    @isTest
    static void testContractIdByPoliza () {
        Final List<Contract> testContractPol =[SELECT Id, MX_SB_SAC_NumeroPoliza__c, MX_SB_MLT_Certificado__c
                                               FROM Contract WHERE MX_SB_SAC_NumeroPoliza__c  LIKE: '%' + SAC_NO_POLIZA+ '%' ];
        test.startTest();
            MX_RTL_Contract_Selector.getContractIdByPoliza(testContractPol[0].MX_SB_SAC_NumeroPoliza__c, testContractPol[0].MX_SB_MLT_Certificado__c , DMLTEST2);
            system.assert(true,'La consulta fue un exito');
        test.stopTest();
    }

    @isTest
    static void testUpsertSinCont() {
        Final Account upsertTest = [SELECT Id, Name FROM Account WHERE Name LIKE: '%' + NAME + '%' LIMIT 1];
		Final Contract contractTest = new Contract();
        	contractTest.AccountId = upsertTest.Id;
        test.startTest();
        	MX_RTL_Contract_Selector.upsertSinCont(contractTest);
        	system.assert(true,'Se ha realizado el upsert');
        test.stopTest();
    }

    @isTest
    static void testInsContracts() {
        final User objUsrTest = MX_WB_TestData_cls.crearUsuario('TestUser', System.label.MX_SB_VTS_ProfileAdmin);        
        insert objUsrTest;
        System.runAs(objUsrTest) {
            final Account objAccount = MX_WB_TestData_cls.crearCuenta('Contratos', 'PersonAccount');
            objAccount.PersonEmail = 'test@test.com';
            insert objAccount;
            final Opportunity objOpportunity = MX_WB_TestData_cls.crearOportunidad('Opportunity Contratos', objAccount.Id, objUsrTest.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            objOpportunity.LeadSource = 'Call me back';
            objOpportunity.Producto__c = 'Seguro Estudia';
            insert objOpportunity;
            final Product2 objProduct = MX_WB_TestData_cls.crearProducto('Test Product', 'Seguros');
            insert objProduct;
            final Contract objContract = MX_WB_TestData_cls.vtsCreateContract(objAccount.Id, objUsrTest.Id, objProduct.Id);
            objContract.MX_WB_Oportunidad__c = objOpportunity.Id;
            objContract.Status = 'Draft';
            final List<Contract> lstContract = new List<Contract>();
            lstContract.add(objContract);
            final List<Contract> lstResponse = MX_RTL_Contract_Selector.insertContracts(lstContract);
            System.assert(!lstResponse.isEmpty(), 'Se realizo el proceso con exito');
        }
    }
    
}