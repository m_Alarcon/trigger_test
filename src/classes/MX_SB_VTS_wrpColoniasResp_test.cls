@isTest
/**
* @FileName          : MX_SB_VTS_ListCatalog_Service_Test
* @description       : Test class from MX_SB_VTS_ListCatalog_Service
* @Author            : Marco Antonio Cruz Barboza
* @last modified on  : 08-05-2020
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   08-05-2020   Marco Antonio Cruz Barboza          Initial Version
**/
public class MX_SB_VTS_wrpColoniasResp_test {
    /** User name testing */
    final static String NAMESER = 'UserTest';
    /** Account Name testing*/
    final static String TSTACCT = 'Test Account';
    
    @TestSetup
    static void setupTest() {
        final User usrTest = MX_WB_TestData_cls.crearUsuario(NAMESER, System.Label.MX_SB_VTS_ProfileAdmin);
        System.runAs(usrTest) {
            final Account AccountTes = MX_WB_TestData_cls.crearCuenta(TSTACCT, System.Label.MX_SB_VTS_PersonRecord);
            AccountTes.PersonEmail = 'pruebaVts@mxvts.com';
            insert AccountTes;
        }
    }
    
    /*
    @Method: wrpColoniasRespTest
    @Description: Use a wrapper class to deserialize a response from a web service.
	@param
    */
    @isTest
    static void wrpColoniasRespTest() {
        test.startTest();
            Final MX_SB_VTS_wrpColoniasResp.citys wrapperCity = new MX_SB_VTS_wrpColoniasResp.citys();
            Final MX_SB_VTS_wrpColoniasResp.county wrapperCount = new MX_SB_VTS_wrpColoniasResp.county();
            Final MX_SB_VTS_wrpColoniasResp.state wrapperState = new MX_SB_VTS_wrpColoniasResp.state();
            Final MX_SB_VTS_wrpColoniasResp.neighborhood wrapperNei = new MX_SB_VTS_wrpColoniasResp.neighborhood();
            Final MX_SB_VTS_wrpColoniasResp.suburb wrapperSub = new MX_SB_VTS_wrpColoniasResp.suburb();
            Final MX_SB_VTS_wrpColoniasResp.suburb[] wrapperList = new MX_SB_VTS_wrpColoniasResp.suburb[]{};
            Final MX_SB_VTS_wrpColoniasResp.iCatalogItem wrapperGnl = new MX_SB_VTS_wrpColoniasResp.iCatalogItem();
            Final MX_SB_VTS_wrpColoniasResp getSetter = new MX_SB_VTS_wrpColoniasResp();
                wrapperCity.name = 'CIUDAD DE MÉXICO';
                wrapperCity.idCity = '01';
                wrapperCount.name = 'GUSTAVO A. MADERO';
                wrapperCount.idCounty = '005';
                wrapperState.name = 'CIUDAD DE MEXICO';
                wrapperState.idState = '09';
                wrapperNei.name = 'UNIDAD HABITACIONAL, LA PATERA VALLEJO';
                wrapperNei.idNeighbor = '0196';
                wrapperSub.city = wrapperCity;
                wrapperSub.county = wrapperCount;
                wrapperSub.neighborhood = wrapperNei;
                wrapperSub.state = wrapperState;
                wrapperList.add(wrapperSub);
        		wrapperGnl.suburb = wrapperList;
        		getSetter.iCatalogItem = wrapperGnl;
        	System.assertNotEquals(wrapperGnl, null,'El wrapper esta retornando');
        test.stopTest();
    }

}