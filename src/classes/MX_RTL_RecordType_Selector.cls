/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase Selector para objeto RecordType

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     20/05/2020      Roberto Isaac Soto Granados           Creación Clase
*   1.1     06/06/2020      Gabriel Garcia Rojas             Se crea método getMapRecordType
*****************************************************************************************/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_RTL_RecordType_Selector {
    /*Llamada a recordType para conseguir Id*/
    public static List<RecordType> recordTypeLlamadaBPyP() {
        return [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_BPP_NuevaLlamadaBPyP'];
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param sObjectName
    * @param developerName
    * @return Map<Id, RecordType>
    **/
    public static Map<Id, RecordType> getMapRecordType(String sObjectName, String developerName) {
        Map<Id,RecordType> results;

        if(String.isEmpty(developerName)) {
            results = new Map<Id,RecordType>([SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType =: sObjectName]);
        } else {
            results = new Map<Id,RecordType>([SELECT Id, DeveloperName FROM RecordType WHERE SObjectType =: sObjectName AND DeveloperName =: developerName]);
        }

        return results;
    }
    /**
    * @description
    * @author Jaime Terrats | 6/30/2020
    * @param fields
    * @param devName
    * @param sOType
    * @return List<RecordType>
    **/
    public static List<RecordType> getRecordTypeByDevName(String fields, String devName, String sOType) {
        return Database.query(String.escapeSingleQuotes('select ' + fields + ' from RecordType where DeveloperName =: devName and SObjectType =: sOType'));
    }
}