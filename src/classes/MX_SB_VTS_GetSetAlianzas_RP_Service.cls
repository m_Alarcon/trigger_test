/**
 * @File Name          : MX_SB_VTS_GetSetAlianzas_RP_Service.cls
 * @Description        : Realiza el consumo del servicio getListInsuranceInformationCatalogs
 * @Author             : Alexandro Corzo
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 13/10/2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0       13/10/2020      Alexandro Corzo        Initial Version
 * 1.1       12/01/2021      Alexandro Corzo        Se agrega request consumo ASO
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_GetSetAlianzas_RP_Service {
    /** Variable de Apoyo: OKCODE */
    private static final Integer OKCODE = 200;
    /** Variable de Apoyo: ALLIANCECODE */
    private static final String ALLIANCECODE = 'DHSDT003';
    
    /**
     * @description: Realiza el consumo del servicio getListInsuranceInformationCatalogs
     * 				 para obtener: Alizanza Plan Prod 
     * @author: 	 Alexandro Corzo
     * @return: 	 Map<String, Object> mReturnValues
     */
    public static Map<String, Object> obtListInsInfoCatSrv() {
        final Map<String, Object> mReturnValues = new Map<String, Object>();
        final Map<String, Object> sParamsSrv = new Map<String, Object>{'TYPE' => 'RP'};
        final HttpResponse objRespSrv = MX_RTL_IasoServicesInvoke_Selector.callServices('getListInsuranceInformationCatalogs', sParamsSrv, obtRequestSrv());
        if(objRespSrv.getStatusCode() ==  OKCODE) {
            final List<Object> lstDetails = new List<Object>();
            final MX_SB_VTS_wrpServiciosAlianzasRP objWrapper = (MX_SB_VTS_wrpServiciosAlianzasRP) JSON.deserialize(objRespSrv.getBody(), MX_SB_VTS_wrpServiciosAlianzasRP.class);
        	for(MX_SB_VTS_wrpServiciosAlianzasRP.revisionPlanList oItemRow : objWrapper.iCatalogItem.revisionPlanList) {
                if(ALLIANCECODE.equalsIgnoreCase(oItemRow.allianceCode)) {
                	final Map<String, Object> objData = new Map<String, Object>();
	            	objData.put('catalogItemBase_id', oItemRow.catalogItemBase.id);
    	        	objData.put('planReview', oItemRow.planReview);
	            	objData.put('planCode', oItemRow.planCode);
	            	objData.put('productCode', oItemRow.productCode);
	            	objData.put('allianceCode', oItemRow.allianceCode);
	            	lstDetails.add(objData);
                }
        	}
            final Map<String, Object> mStatusCode = new Map<String, Object>{'code' => String.valueOf(OKCODE), 'description' => 'Consumo de Servicio Exitoso!'};
            mReturnValues.put('oData', lstDetails);    
            mReturnValues.put('oResponse', mStatusCode);
        } else {
            mReturnValues.put('oResponse', MX_SB_VTS_Codes_Utils.statusCodes(objRespSrv.getStatusCode()));
        }
        return mReturnValues;
    }

    /**
     * @description : Realiza el armado del objeto Request para el consumo del servicio ASO
     * @author      : Alexandro Corzo
     * @return      : HttpRequest oRequest
     */
    public static HttpRequest obtRequestSrv() {
        final Map<String,iaso__GBL_Rest_Services_Url__c> allCodesASO = iaso__GBL_Rest_Services_Url__c.getAll();
        final iaso__GBL_Rest_Services_Url__c objASOSrv = allCodesASO.get('getListInsuranceInformationCatalogs');
        final String strEndoPoint = objASOSrv.iaso__Url__c;
        final HttpRequest oRequest = new HttpRequest();
        oRequest.setTimeout(120000);
        oRequest.setMethod('POST');
        oRequest.setHeader('Content-Type', 'application/json');
        oRequest.setHeader('Accept', '*/*');
        oRequest.setHeader('Host', 'https://test-sf.bbva.mx');
        oRequest.setEndpoint(strEndoPoint);
        oRequest.setBody(obtBodyService());
        return oRequest;
    }
    
    /**
     * @description : Realiza el armado del Body que sera ocupado en el Request del Servicio
     * @author      : Alexandro Corzo
     * @return      : String sBodyService
     */
    public static String obtBodyService() {
    	String sBodyService = '';
        sBodyService += '{';
		sBodyService += '"header": {';
        sBodyService += '"aapType": "10000137",';
        sBodyService += '"dateRequest": "2017-10-03 12:33:27.104",';
        sBodyService += '"channel": "4",';
        sBodyService += '"subChannel": "26",';
        sBodyService += '"branchOffice": "INCE",';
        sBodyService += '"managementUnit": "DHSDT003",';
        sBodyService += '"user": "MARCO",';
        sBodyService += '"idSession": "3232-3232",';
        sBodyService += '"idRequest": "1212-121212-12121-212",';
        sBodyService += '"dateConsumerInvocation": "2017-10-03 12:33:27.104"';
        sBodyService += '},';
        sBodyService += '"areaCode": "0001"';
        sBodyService += '}';
        return sBodyService;
    }
}