/**
 * @description       : 
 * @author            : Gerardo Mendoza Aguilar
 * @group             : 
 * @last modified on  : 03-05-2021
 * @last modified by  : Gerardo Mendoza Aguilar
 * Modifications Log 
 * Ver   Date         Author                    Modification
 * 1.0   03-03-2021   Gerardo Mendoza Aguilar   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_SAC_Entitlement_Selector {
    /**
    * @description 
    * @author Gerardo Mendoza Aguilar | 03-03-2021 
    * @param listEntitlement 
    **/
    public static List<Entitlement> insertEntitlement(List<Entitlement> listEntitlement) {
        if(!listEntitlement.isEmpty()) {
            insert listEntitlement;
        }
        return listEntitlement;
    }
}