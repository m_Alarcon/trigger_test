/**
 * @description       :
 * @author            : Miguel Hernandez
 * @group             :
 * @last modified on  : 02-17-2021
 * @last modified by  : Gerardo Mendoza Aguilar
 * Modifications Log
 * Ver   Date         Author          Modification
 * 1.0   08-09-2020   Miguel Hernandez   Initial Version
 * 1.1   02-17-2021   Gerardo Mendoza    Method updateCase
**/
@isTest
private without sharing class MX_SB_SAC_Case_Service_Test {
    /** assert condition string */
    final static string ASSERT_CONDITION = 'Exito';
    /** error value in assert */
    final static String ASSERT_MSG_ERR = 'Algo fallo en la prueba';
    /** error from vr */
    final static String ERR_MSG = 'Se requieren agregar comentarios para poder guardar';
    /** user name */
    final static String USR_NAME = 'asesor sac';
    /** name variable for admin user and cases */
    final static String NAME = 'testSacValidate';
    /** product type value */
    final static String VIDA = 'Vida';
    /** product type value */
    final static String AUTO = 'Auto';
    /** variable for case description */
    final static String ASESOR_SAC = 'asesor sac';
    /** tipification value */
    final static String RETENCION = 'Retención';
    /** error message variable */
    final static String ASSERT_MSG = 'Error esperado.';
    /** product type value */
    final static String STATUS = 'Aceptado';
    /** variable for case description */
    final static String LOREM_IPSUM = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum';
    
    /**
    * @description
    * @author Miguel Hernandez | 08-09-2020
    **/
    @isTest
    static void invokeTest1() {
        MX_SB_SAC_CaseValidation_Test.makeData();
        MX_SB_SAC_CaseValidation_Test.testValidateChange5();
        System.assert(String.isNotBlank(ASSERT_CONDITION), '');
    }

    /**
    * @description
    * @author Miguel Hernandez | 08-09-2020
    **/
    @isTest
    static void invokeTest2() {
        MX_SB_SAC_CaseValidation_Test.makeData();
        Test.startTest();
        final Case uCase = [Select Id, Reason, Description, OwnerId, MX_SB_SAC_FinalizaFlujo__c, MX_SB_SAC_Ramo__c, MX_SB_1500_is1500__c, MX_SB_SAC_Detalle__c from Case where Subject =: Name+5];
        final User sacUser = [Select Id from User where LastName =: ASESOR_SAC];
        uCase.Reason = RETENCION;
        uCase.MX_SB_SAC_Ramo__c = VIDA;
        uCase.MX_SB_SAC_FinalizaFlujo__c = false;
        uCase.MX_SB_1500_is1500__c = true;
        uCase.MX_SB_SAC_Detalle__c = 'Malas Ventas';
        uCase.OwnerId = sacUser.Id;
        update uCase;
        System.runAs(sacUser) {
            try {
                uCase.Description = LOREM_IPSUM;
                update uCase;
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(System.Label.MX_SB_SAC_PreventCmnts), ASSERT_MSG);
            }
        }
        Test.stopTest();
    }

    /**
    * @description
    * @author Alan Santacruz | 18-11-2020
    **/
    @isTest
    static void invokeTest3() {
        MX_SB_SAC_CaseValidation_Test.makeData();
        final Case tCase = [Select Status, MX_SB_1500_is1500__c, MX_SB_SAC_ActivarReglaComentario__c, MX_SB_SAC_EsCopia__c from Case limit 1];
        final User tUser = [Select Id from User where LastName =: USR_NAME];
        tCase.MX_SB_1500_is1500__c = true;
        tCase.MX_SB_SAC_EsCopia__c = true;
        tCase.MX_SB_SAC_ActivarReglaComentario__c = false;
        update tCase;
        System.runAs(tUser) {
            try {
				final Case oldCase = tCase;
                tCase.Status = STATUS;
                MX_SB_SAC_Case_DescriptionRules_Helper.duplicate1500(tCase, oldCase);
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(ERR_MSG), ASSERT_MSG_ERR);
            }
        }
    }
    /**
    * @description 
    * @author Gerardo Mendoza Aguilar | 02-17-2021 
    **/
    @isTest
    public static void updateCase() {
        Test.startTest();
        MX_SB_SAC_CaseValidation_Test.makeData();
        Final List<Case> caseList1 = [Select Id, MX_SB_1500_isAuthenticated__c  from Case limit 1];
        caseList1[0].MX_SB_1500_isAuthenticated__c = true;
        MX_SB_SAC_Case_Service.updateCase(caseList1);
        System.assert(!caseList1.isEmpty(), 'Caso actualizado');
        Test.stopTest();
    }
}