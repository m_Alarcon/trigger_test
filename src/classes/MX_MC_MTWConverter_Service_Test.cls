/**
 * @File Name          : MX_MC_MTWConverter_Service_Test.cls
 * @Description        : Clase para Test de MX_MC_MTWConverter_Service
 * @author             : Eduardo Barrera Martínez
 * @Group              : BPyP
 * @Last Modified By   : Eduardo Barrera Martínez
 * @Last Modified On   : 10/07/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      10/07/2020           Eduardo Barrera Martínez.       Initial Version
**/
@isTest
private class MX_MC_MTWConverter_Service_Test {

    /**
    * @Description O_CUSTOMER property
    **/
    private static final String O_CUSTOMER = 'test_idC_123';

    /**
    * @Description O_RECEIVER property
    **/
    private static final String O_RECEIVER = 'test_idR_123';

    /**
    * @Description MESSAGE_THREAD_ID property
    **/
    private static final String MESSAGE_THREAD_ID = 'test_thradId_123';

    /**
    * @Description RES_LST_MESS property
    **/
    private static final String RES_LST_MESS = '{\"data\":[{\"id\":\"M912312A\",\"message\":\"Hola Juan, me gustaría información acerca de seguros de vida\",\"wasRead\":true,\"senderType\":\"CL\",\"isDraft\":false,\"attachments\":[{\"name\":\"test_name\",\"url\":\"test_url\"}]},{\"id\":\"M9921231\",\"message\":\"Hola Carlos,¿te interesa alguno en particular?\",\"wasRead\":true,\"senderType\":\"GE\",\"isDraft\":true,\"attachments\":[]}],\"pagination\":{\"links\":{\"first\":\"\",\"next\":\"\"},\"page\":1,\"pageSize\":2,\"totalElements\":4,\"totalPages\":2}}';

    /**
    * @Description CLIENT_ID property
    **/
    private static final String CLIENT_ID = 'client_code_1';

    /**
    * @Description RES_LST_CONV property
    **/
    private static final String RES_LST_CONV = '{\"data\":[{\"id\":\"1\",\"subject\":\"Solicitud Cotización\",\"creationDate\":\"2019-10-10\",\"lastUpdate\":\"2019-10-01\",\"customer\":{\"id\":\"client_code_1\"},\"businessAgent\":{\"id\":\"user_123\"},\"alternativeBusinessAgent\":{\"id\":\"0000212\"},\"conversationType\":\"E\",\"status\":{\"id\":\"1\",\"description\":\"NORESU\"},\"unreadMessages\":\"1\"},{\"id\":\"2\",\"subject\":\"Aclaración\",\"creationDate\":\"2019-10-09\",\"lastUpdate\":\"2019-10-09\",\"customer\":{\"id\":\"client_code_1\"},\"businessAgent\":{\"id\":\"8883222\"},\"alternativeBusinessAgent\":{\"id\":\"0000212\"},\"conversationType\":\"E\",\"status\":{\"id\":\"1\",\"description\":\"NORESU\"},\"unreadMessages\":\"1\"}],\"pagination\":{\"links\":{\"first\":\"\",\"next\":\"\"},\"page\":1,\"pageSize\":2,\"totalElements\":4,\"totalPages\":2}}';

    /**
    * @Description ASSERT_NOT_EQUAL property
    **/
    private static final String ASSERT_NOT_EQUAL = 'El resultado no es el esparado';

    /**
     * @Description Clase de prueba para MX_MC_MTWConverter_Service
     * @author eduardo.barrera3@bbva.com | 10/07/2020
    **/
    @TestSetup
   static void makeData() {
        final Case convTest = new Case();
        convTest.mycn__MC_ConversationId__c = 'test_thradId_123';
        convTest.RecordTypeId = mycn.MC_ConversationsDatabase.getCaseConversationRecordType();
        convTest.Status = 'New';
        convTest.Subject = 'MC Message example';
        insert convTest;
        final iaso__GBL_Rest_Services_Url__c restUrl = new iaso__GBL_Rest_Services_Url__c(Name = 'MC_valueCipher',
                                                                                        iaso__Url__c = 'www.testMTWConverter.bbva',
                                                                                        iaso__Cache_Partition__c = 'iaso.ServicesPartition');
        insert restUrl;

        final User thisUser = [SELECT Id, Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
        final User usrMC = UtilitysDataTest_tst.crearUsuario('Usuario_MC', thisUser.Profile.Name, Null);
        usrMC.EmployeeNumber = '123_test';
        insert usrMC;
    }

    /**
     * @Description Clase de prueba para MX_MC_MTWConverter_Service
     * @author eduardo.barrera3@bbva.com | 10/07/2020
    **/
    @isTest
    static void testInputConvListMessages() {

        final Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('messageThreadId','test_id_123');
        final MX_MC_MTWConverter_Service mxConverter = new MX_MC_MTWConverter_Service();
        Map<String, Object> convResult = new Map<String, Object>();
        convResult = mxConverter.convertMap(inputMap);
        System.assertEquals(inputMap, convResult, ASSERT_NOT_EQUAL);
    }

    /**
     * @Description Clase de prueba para MX_MC_MTWConverter_Service
     * @author eduardo.barrera3@bbva.com | 10/07/2020
    **/
    @isTest
    static void testInputConvListConversations() {

        final MX_WB_Mock encryptMock = new MX_WB_Mock(200,'OK', '{"results":["aj1lk115ds31p"]}', new Map<String,String>());

        final Map<String, Object> inputMap = new Map<String, Object>();
        final String filter = 'customer.id==' + O_CUSTOMER + ';receiver.id==' + O_RECEIVER;
        inputMap.put('filter',filter);

        final MX_MC_MTWConverter_Service mxConverter = new MX_MC_MTWConverter_Service();
        String receiver = '';
        Test.startTest();
        iaso.GBL_Mock.setMock(encryptMock);
        final User testUser = [SELECT Id, Title FROM User WHERE LastName = 'Usuario_MC'];
        System.runAs(testUser) {
            final Map<String, Object> convResult = mxConverter.convertMap(inputMap);
            final String filterResult = convResult.get('filter').toString();
            receiver = filterResult.substringAfter('receiver.id=');
        }
        Test.stopTest();

        System.assertEquals('aj1lk115ds31p', receiver, ASSERT_NOT_EQUAL);
    }

    /**
     * @Description Clase de prueba para MX_MC_MTWConverter_Service
     * @author eduardo.barrera3@bbva.com | 10/07/2020
    **/
    @isTest
    static void testOutputListMessages() {

        final Map<String, Object>  inputMap = new Map<String,Object>();
        inputMap.put('messageThreadId', MESSAGE_THREAD_ID);
        final MX_MC_MTWConverter_Service mxConverter = new MX_MC_MTWConverter_Service();
        mycn.MC_MessageThreadWrapper.MessageThread resultConv = new mycn.MC_MessageThreadWrapper.MessageThread();
        resultConv = (mycn.MC_MessageThreadWrapper.MessageThread) mxConverter.convert(RES_LST_MESS,'getMessageThread',inputMap);
        System.assertEquals('G', resultConv.messages[0].sender.type.id,ASSERT_NOT_EQUAL);
    }

    /**
     * @Description Clase de prueba para MX_MC_MTWConverter_Service
     * @author eduardo.barrera3@bbva.com | 10/07/2020
    **/
    @isTest
    static void testOutputListConversations() {

        final MX_WB_Mock encryptMock = new MX_WB_Mock(200,'OK', '{"results":["client_code_1"]}', new Map<String,String>());

        final Map<String, Object>  inputMap = new Map<String,Object>();
        inputMap.put('customerId', CLIENT_ID);

        final MX_MC_MTWConverter_Service mxConverter = new MX_MC_MTWConverter_Service();
        mycn.MC_MessageThreadWrapper.MessageThreadPage resultConv = new mycn.MC_MessageThreadWrapper.MessageThreadPage();
        Test.startTest();
        iaso.GBL_Mock.setMock(encryptMock);
        resultConv = (mycn.MC_MessageThreadWrapper.MessageThreadPage) mxConverter.convert(RES_LST_CONV,'getListMessageThreads',inputMap);
        Test.stopTest();

        System.assertEquals('client_code_1', resultConv.messageThreads[0].customer.id,ASSERT_NOT_EQUAL);
    }
}