/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_COB_CreaPolizaBch_tst
* Autor Angel Nava
* Proyecto: Cobranza - BBVA Bancomer
* Descripción : Clase para pruebas de MX_SB_COB_CreaPolizaBch_cls
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Desripción
* --------------------------------------------------------------------------------
* 1.0           01/05/2019     Angel Nava				          Creación
* --------------------------------------------------------------------------------
*/
@isTest
public class MX_SB_COB_CreaPolizaBch_tst {

    @Testsetup
    static void setObjetos() {
        final Bloque__c bloque = new Bloque__c();
        insert bloque;
        final CargaArchivo__c archivo = new CargaArchivo__c(MX_SB_COB_Bloque__c=bloque.name,MX_SB_COB_ContadorConError__c=0,MX_SB_COB_ContadorSinError__c=0,MX_SB_COB_resultado__c='');
        insert archivo;
        final pagosespejo__c pago = new pagosespejo__c(MX_SB_COB_BloqueLupa__c =bloque.id,MX_SB_COB_factura__c='aa23',MX_SB_COB_InsertCreaPago__c=false);
        insert pago;

    }

     @isTest
    static void creaPolizas() {
        final String VarRtCotizaEmitida = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Contrato Seguros').getRecordTypeId(); 

        test.startTest();
        final PagosEspejo__c[] pago = [select id,MX_SB_COB_factura__c,MX_SB_COB_BloqueLupa__c from pagosespejo__c limit 1];
        final Map<String,Map<String,PagosEspejo__c>> mapPolFactPagoEspejo = new Map<String,Map<String,PagosEspejo__c>>();
        final Map<String,pagosEspejo__c> pagoMap = new Map<String,pagosEspejo__c>();
        pagoMap.put(pago[0].MX_SB_COB_factura__c,pago[0]);
        final Map<String,Contract> mapPolNvas = new Map<String,Contract>();

         mapPolNvas.put('aa23', new Contract(
                        						RecordTypeId = VarRtCotizaEmitida,
                            					StartDate=date.today(),AccountId=null,
                            					MX_WB_noPoliza__c='aa23',
                            					MX_WB_emailAsegurado__c='a@a.com',
                                                MX_WB_telefonoAsegurado__c= '31231231',
                            					MX_WB_nombreAsegurado__c='Luffy',
                            MX_WB_apellidoPaternoAsegurado__c='Monkey'
                        					)
                                       );//fin mapPolNvas
         mapPolFactPagoEspejo.put('aa23',pagoMap);

        final MX_SB_COB_CreaPolizaBch_cls batch = new MX_SB_COB_CreaPolizaBch_cls(mapPolNvas,mapPolFactPagoEspejo);//se agrga el mapa poliza,opp

        final Id batchInstanceId = Database.executeBatch(batch, 200);
       system.assertNotEquals(null, batchInstanceId);
        test.stopTest();
    }

}