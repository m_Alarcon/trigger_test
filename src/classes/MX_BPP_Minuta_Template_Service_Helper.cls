/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Template_Service_Helper
* @Author   	Héctor Saldaña | hectorisrael.saldana.contractor@bbva.com
* @Date     	Created: 2021-01-20
* @Description 	BPyP Minuta Tamplate Service Helper Layer - Helper class that processes complex logic for Minuta Template Service Class
* @Changes
* 2021-22-02	Héctor Saldaña	  	New methods added replaceDynamicTags, getMetadataTags, replaceProfileTags
* 2021-22-02	Edmundo Zacarias  	New method added procesarAcuerdos, formatValue
 * 2021-28-03	Edmundo Zacarias  	New Logic added for body email during email construction process
*/
public without sharing class MX_BPP_Minuta_Template_Service_Helper {
	/* Field attach file object name */
	static final String FIELDATTACHFILE = 'dwp_kitv__Attach_file__c';
	/* Field templConvertedL */
	String templConvertedL {get; set;}
	/* Field lstDocuments */
	List<ContentVersion> lstDocuments {get; set;}
	/* Field email */
	public Messaging.SingleEmailMessage emailL {get; set;}

	/**Constructor Method*/
	private MX_BPP_Minuta_Template_Service_Helper() {}

	/* Constructor Method */
	public MX_BPP_Minuta_Template_Service_Helper(String templateConv, List<ContentVersion> documents) {
		this.templConvertedL = templateConv;
		this.lstDocuments = documents;
	}

	/**
    * @Description 	Save template as document
    * @Param		dwp_kitv__Visit__c visitObj, List<ContentVersion> documents, dwp_kitv__Template_for_type_of_visit_cs__c myCS, User userObj
    * @Return 		NA
    **/
	public void saveTemplateAsDocument(dwp_kitv__Visit__c visitObj, List<ContentVersion> documents, dwp_kitv__Template_for_type_of_visit_cs__c myCS,
									   User userObj, PageReference currntPageRef, MX_BPP_CatalogoMinutas__c currentCatalogo) {

		String templateConverted = 'BODY NULL';
		Blob docBlob;
		currntPageRef.getParameters().put('id',visitObj.Id);
		currntPageRef.getParameters().put('sCatalogoId', MX_BPP_Minuta_Utils.getParameter('sCatalogoId'));
		currntPageRef.getParameters().put('textArray', MX_BPP_Minuta_Utils.getParameter('textArray').unescapeHtml4());
		currntPageRef.getParameters().put('seleccionArray', MX_BPP_Minuta_Utils.getParameter('seleccionArray').unescapeHtml4());
		currntPageRef.getParameters().put('switchArray', MX_BPP_Minuta_Utils.getParameter('switchArray').unescapeHtml4());
		currntPageRef.setRedirect(true);

		try {
			docBlob = currntPageRef.getContentAsPDF();
			templateConverted = currntPageRef.getContent().tostring();
		} catch(VisualforceException e) {
			docBlob = Blob.valueOf('Texto de prueba para pdf ' + e);
		}
		this.templConvertedL = templateConverted;
		if(!this.lstDocuments.isEmpty()) {
			MX_BPP_Minuta_Utils.insertContentDocLink(this.lstDocuments, visitObj.Id, true);
		}
		documents.add(MX_BPP_Minuta_Utils.insertMinuta(docBlob, visitObj));

		this.lstDocuments = documents;
	}

	/**
	* @Description 	sendEmail
	* @Param		dwp_kitv__Template_for_type_of_visit_cs__c myCS, dwp_kitv__Visit__c visitObj, User userObj
	* @Return 		Messaging.SendEmailResult[]
	**/
	public Messaging.SendEmailResult[] sendMail(dwp_kitv__Template_for_type_of_visit_cs__c myCS, dwp_kitv__Visit__c visitObj, User userObj, MX_BPP_CatalogoMinutas__c currentCatalogo) {

		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

		final String htmlBody = '<![CDATA[ <html lang="es">'+
				'<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />' + currentCatalogo.MX_CuerpoCorreo__c + '</html>';
		email.setHtmlBody(htmlBody);
		email.setSubject(currentCatalogo.MX_Asunto__c);

		final List<String> typeAddress= dwp_kitv.Minute_Generator_Ctrl.getTypeAddress();
		email = dwp_kitv.Minute_Generator_Ctrl.setAddressToEmail(email,typeAddress,visitObj);
		final String[] correosEjec = new String[]{String.valueOf(userObj.Email)};
		if(userObj.Manager != null) {
			correosEjec.add(String.valueOf(userObj.Manager.Email));
		}
		email.setBccAddresses( correosEjec);

		final boolean attachFile= dwp_kitv.Minute_Generator_Ctrl.getExistField(myCS, FIELDATTACHFILE);
		final List<Id> ltsIdDoc = dwp_kitv.Minute_Generator_Ctrl.getIdsDocuments(this.lstDocuments);
		email = dwp_kitv.Minute_Generator_Ctrl.setFiles(email, ltsIdDoc, attachFile);
		email.setSaveAsActivity(false);
		this.emailL = email;

		final Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {email};

		return Messaging.sendEmail(messages);
	}

	/**
    * @Description Replaces static values taken from the metadata records MX_BPP_CatalogoMinutas__c into the respective fields in Catalogo Minuta record
    * @Params List<MX_BPP_MinutaWrapper.tagsEstaticos> lsttagsEst, MX_BPP_CatalogoMinutas__c catalogo, dwp_kitv__Visit__c visit
    * @Return NA
    **/
	public static void replaceStaticValues(List<MX_BPP_MinutaWrapper.tagsEstaticos> lsttagsEst, MX_BPP_CatalogoMinutas__c catalogo, dwp_kitv__Visit__c visit) {
		catalogo.MX_Saludo__c = MX_BPP_Minuta_Utils.validateNullValues(catalogo.MX_Saludo__c);
        catalogo.MX_Contenido__c = MX_BPP_Minuta_Utils.validateNullValues(catalogo.MX_Contenido__c);
        catalogo.MX_Despedida__c = MX_BPP_Minuta_Utils.validateNullValues(catalogo.MX_Despedida__c);
        catalogo.MX_Firma__c = MX_BPP_Minuta_Utils.validateNullValues(catalogo.MX_Firma__c);

		for(MX_BPP_MinutaWrapper.tagsEstaticos currentTag : lsttagsEst) {
			String replaceValue = '';
			if(currentTag.relacionado) {
				replaceValue = visit.getSObject(currentTag.objeto).get(currentTag.campo) == null ? '' : (String)visit.getSObject(currentTag.objeto).get(currentTag.campo);
			} else {
				replaceValue = visit.get(currentTag.campo) == null ? '' : (String)visit.get(currentTag.campo);
			}
			catalogo.MX_Saludo__c = catalogo.MX_Saludo__c.replace(currentTag.tagInicio, replaceValue);
			catalogo.MX_Contenido__c = catalogo.MX_Contenido__c.replace(currentTag.tagInicio, replaceValue);
			catalogo.MX_Despedida__c = catalogo.MX_Despedida__c.replace(currentTag.tagInicio, replaceValue);
			catalogo.MX_Firma__c = catalogo.MX_Firma__c.replace(currentTag.tagInicio, replaceValue);
			catalogo.MX_CuerpoCorreo__c = catalogo.MX_CuerpoCorreo__c.replace(currentTag.tagInicio, replaceValue);
		}
	}

	/**
    * @Description Retrieves the visit based on the Id and query fields received
    * @Params String recordId, String queryFields
    * @Return NA
    **/
	public static dwp_kitv__Visit__c getVisitByID(String recordId, String queryFields) {
		final Set<Id> visitsId = new Set<Id>();
		visitsId.add(recordId);
		final List <dwp_kitv__Visit__c> listVisits = Dwp_kitv_Visit_Selector.getVisitInfoByIds(queryfields, visitsId);

		return listVisits[0];
	}
    
    /**
    * @Description Replaces received dynamic inputs from the LWC MX_BPP_minuta into MX_Contenido__c field
    * @Params MX_BPP_CatalogoMinutas__c currentCatalogo, List<Object> tagsLst
    * @Return NA
    **/
	public static void replaceDynamicTags(MX_BPP_CatalogoMinutas__c catalogo, List<Object> tagsLst) {
		if(tagsLst instanceof  List<MX_BPP_MinutaWrapper.componenteText>) {
			for(MX_BPP_MinutaWrapper.componenteText current : (List<MX_BPP_MinutaWrapper.componenteText>)tagsLst) {
				current.label = current.label.contains('*') || current.label.contains('\n') ? MX_BPP_Minuta_Utils.formatValue(current.label) : current.label;
				catalogo.MX_Contenido__c = catalogo.MX_Contenido__c.replace(current.tagInicio, current.label);
			}
		} else if (tagsLst instanceof List<MX_BPP_MinutaWrapper.componentePicklist>) {
			for(MX_BPP_MinutaWrapper.componentePicklist curren : (List<MX_BPP_MinutaWrapper.componentePicklist>)tagsLst) {
				catalogo.MX_Contenido__c = catalogo.MX_Contenido__c.replace(curren.tagInicio, curren.label);
			}
		} else if (tagsLst instanceof List<MX_BPP_MinutaWrapper.componenteSwitch>) {
			for(MX_BPP_MinutaWrapper.componenteSwitch curren : (List<MX_BPP_MinutaWrapper.componenteSwitch>)tagsLst) {
				catalogo.MX_Contenido__c = catalogo.MX_Contenido__c.replace(curren.tagInicio, curren.label);
			}
		}

	}

    /**
    * @Description Retrieves Metadata records (MX_BPP_GlosarioMinuta__mdt) based on the received Set
    * @Params Set<String> tagsencontrados, MX_BPP_CatalogoMinutas__c catalogo, String visitId
    * @Return NA
    **/
	public static void getMetadataTags(Set<String> tagsencontrados, MX_BPP_CatalogoMinutas__c catalogo, String visitId) {
		String glosarioFields;
        glosarioFields = 'Id, developername, MX_TargetField__c, MX_TargetObject__c ';
		final List<MX_BPP_GlosarioMinuta__mdt> glosario = MX_BPP_GlosarioMinuta_Selector.getRecordsByDevName(glosarioFields, tagsencontrados);
		String fields = '';
		final List<MX_BPP_MinutaWrapper.tagsEstaticos> lsttagsEst = new List<MX_BPP_MinutaWrapper.tagsEstaticos>(); //NOSONAR
		for(MX_BPP_GlosarioMinuta__mdt glosarioTmp : glosario) {
			final Boolean hasObject  = glosarioTmp.MX_TargetObject__c == null ? false : true;
			final Boolean hasField  = glosarioTmp.MX_TargetField__c == null ? false : true;
			String newField = '';
			final MX_BPP_MinutaWrapper.tagsEstaticos objetoTag = new MX_BPP_MinutaWrapper.tagsEstaticos();
			if(hasField) {
				objetoTag.tagInicio = '{' + glosarioTmp.developerName + '}';
				objetoTag.relacionado = false;
				objetoTag.objeto = '';
				objetoTag.campo = glosarioTmp.MX_TargetField__c;
				if(hasObject) {
					objetoTag.relacionado = true;
					objetoTag.objeto = glosarioTmp.MX_TargetObject__c;
					newField += glosarioTmp.MX_TargetObject__c + '.'+ glosarioTmp.MX_TargetField__c + ', ';
				} else {
					newField += glosarioTmp.MX_TargetField__c + ', ';
				}
				lsttagsEst.add(objetoTag);//NOSONAR
				fields += newField;
			}
		}
		fields = fields.removeEnd(', ');
		final dwp_kitv__Visit__c visit = MX_BPP_Minuta_Template_Service_Helper.getVisitByID(visitId, fields);
		if(visit != null) {
			MX_BPP_Minuta_Template_Service_Helper.replaceStaticValues(lsttagsEst, catalogo, visit);
		}
	}
    
    /**
    * @Description Process inner tags over Catalago record which are related over Activity records related list
    * @Params MX_BPP_CatalogoMinutas__c catalogoMinuta, String tag, Id visitId
    * @Return NA
    **/
    public static void procesarAcuerdos(MX_BPP_CatalogoMinutas__c catalogoMinuta, String tag, Id visitId) {
        String result = '';
        final Set<Id> idSetVisit = new Set<Id>();
        idSetVisit.add(visitId);
        final List<Task> lstAcuerdos = MX_RTL_Task_Selector.getTaskDataById(idSetVisit);
        result += '<ul>';
        if (lstAcuerdos.isEmpty()) {
            result += '<li>Ninguno</li>';
        } else {
            for(Task acuerdo: lstAcuerdos) {
                result += '<li>';
                if (tag.contains('Fecha')) {
                    result += MX_BPP_Minuta_Utils.fechaToString(acuerdo.ActivityDate) + ' - ';
                }
                result += acuerdo.Description + '</li>';
            }
        }
        result += '</ul>';
        catalogoMinuta.MX_Contenido__c = catalogoMinuta.MX_Contenido__c.replace(tag, result);
    }
    
    /**
    * @Description Replaces propper content based on the current profile name
    * @Params  MX_BPP_CatalogoMinutas__c catalogo, Map<String, String> maptags
    * @Return void
    **/
	public static void replaceProfileTags(MX_BPP_CatalogoMinutas__c catalogo, Map<String, String> maptags) {
		String glosarioFields;
        glosarioFields = 'Id, developername, MX_TargetField__c, MX_TargetObject__c, MX_Director_Oficina__c';
		final List<MX_BPP_GlosarioMinuta__mdt> mdtGlosario = MX_BPP_GlosarioMinuta_Selector.getRecordsByDevName(glosarioFields, maptags.keySet());
		String queryfields;
		final Set<String> idSet = new Set<String>{userinfo.getProfileId()};
        queryfields = 'Id, Name';
		final String profileName = MX_RTL_Profile_Selector.getProfileNameById(queryfields, idSet)[0].Name;
		for(MX_BPP_GlosarioMinuta__mdt glosarioTemp : mdtGlosario) {
			final String tagBracket = '{'+ glosarioTemp.developername + '}';
			if(profileName ==  'BPyP Director Oficina' && glosarioTemp.MX_Director_Oficina__c == false) {
				catalogo.MX_Contenido__c = catalogo.MX_Contenido__c.replace(maptags.get(glosarioTemp.developername), '');

			} else if(profileName == 'BPyP Estandar' && glosarioTemp.MX_Director_Oficina__c == true) {
				catalogo.MX_Contenido__c = catalogo.MX_Contenido__c.replace(maptags.get(glosarioTemp.developername), '');
			}

			catalogo.MX_Contenido__c = catalogo.MX_Contenido__c.replace(tagBracket, '') ;
		}
	}

}