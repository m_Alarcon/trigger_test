/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RTL_Profile_Selector_Test
* @Author   	Héctor Saldaña | hectorisrael.saldana.contractor@bbva.com
* @Date     	Created: 2021-22-02
* @Description 	Test Class for MX_RTL_Profile_Selector 
* @Changes
*
*/
@isTest
public with sharing class MX_RTL_Profile_Selector_Test {

    /**
    * @description method that test getProfileNameById
    * @param NA
    * @return void
    **/
    @IsTest
    static void getProfileNameByIdTest() {
        final Set<String> setIds = new Set<String>();
        final String userId = UserInfo.getProfileId();
        setIds.add(userId);
        final List<Profile> profile = MX_RTL_Profile_Selector.getProfileNameById('Id, Name', setIds);
        System.assertEquals(userId, profile[0].Id, 'Error on getProfileNameById');
    }
}