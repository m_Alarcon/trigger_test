/*
* @Nombre: MX_SB_MLT_Catalogos_Claim_Selector.cls
* @Autor: Eder Alberto Hernández Carbajal
* @Proyecto: Siniestros - BBVA
* @Descripción : Clase de la capa de acceso a datos que consulta al objeto MX_SB_MLT_Catalogos_Claim__c
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   	Descripción
* --------------------------------------------------------------------------------
* 1.0         04/06/2020    Eder Alberto Hernández Carbajal		Creación
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_Catalogos_Claim_Selector {
    
    /** constructor */
    @TestVisible
    private MX_SB_MLT_Catalogos_Claim_Selector() { }
    /*
    * @description Busqueda en catalogo claim
    * @param String Id, String tipo
    */
    @AuraEnabled
    public static List<MX_SB_MLT_Catalogos_Claim__c> getCatalog(String descripcion, String tipo) {
        Final String qry = '%'+descripcion+'%';
		return [SELECT MX_SB_MLT_CodeClaim__c, MX_SB_MLT_Descripcion__c, MX_SB_MLT_Code__c FROM MX_SB_MLT_Catalogos_Claim__c WHERE MX_SB_MLT_Descripcion__c LIKE :qry AND MX_SB_MLT_Tipo__c =: tipo ORDER BY MX_SB_MLT_Code__c ASC];
    }
}