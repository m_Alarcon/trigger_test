/**
 * @File Name          : MX_RTL_OpportunityLineItem_Selector.cls
 * @Description        : Clase Data Layer para OpportunityLineItem
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 04/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * @version 1.0      04/06/2020           Jair Ignacio Gonzalez G.   Initial Version
 **/
public with sharing class MX_RTL_OpportunityLineItem_Selector {
    /**
     * @Method getLeadToConverd
     * @Description Singletons
    **/
    @TestVisible private MX_RTL_OpportunityLineItem_Selector() {
    }
    /**
     * @Method getOppLineItembyOppId
     * @param List<Id> idsOpp
     * @Description Consuta de OpportunityLineItem por Ids de Opportunity
     * @return List<OpportunityLineItem>
    **/
    public static List<OpportunityLineItem> getOppLineItembyOppId(List<Id> idsOpp) {
        return [SELECT Id, MX_RTL_SubProducto__c, MX_RTL_DetalleSubproducto__c, OpportunityId FROM OpportunityLineItem WHERE OpportunityId =: idsOpp];
    }

    /**
     * @Method updateOpportunityLineItem
     * @param List<OpportunityLineItem> lsOppLineItem
     * @Description Actualiza una lista de OpportunityLineItem
     * @return List<OpportunityLineItem>
    **/
    public static List<OpportunityLineItem> updateOpportunityLineItem(List<OpportunityLineItem> lsOppLineItem) {
        update lsOppLineItem;
        return lsOppLineItem;
    }
}