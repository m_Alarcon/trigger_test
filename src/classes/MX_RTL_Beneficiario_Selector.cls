/**
* @File Name          : MX_RTL_Beneficiario_Selector.cls
* @Description        :
* @Author             : Juan Carlos Benitez
* @Group              :
* @Last Modified By   : Juan Carlos Benitez
* @Last Modified On   : 5/26/2020, 05:22:22 AM
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    5/26/2020   Juan Carlos Benitez         Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_RTL_Beneficiario_Selector {
    /** List of records to be returned */
    final static List<SObject> R_BENEF = new List<SObject>();
    /** Record to be returned */
    final static List<SObject> RET_BENEF = new List<SObject>();
    /** Literal String SELECT */
    final static String LSELECT = 'SELECT ';
    /**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param Set<Id> contIds
* @param String benFields
* @return R_BENEF
**/
    public static List<MX_SB_VTS_Beneficiario__c> getBenefDataByContract(Set<Id> contIds,String benFields) {
        R_BENEF.addAll(dataBase.query(string.escapeSingleQuotes(LSELECT +benFields + ' FROM MX_SB_VTS_Beneficiario__c where MX_SB_VTS_Contracts__c=:contIds')));
        return R_BENEF;
    }
    /**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param List<MX_SB_VTS_Beneficiario__c> datas
* @param String benFields
* @return R_BENEF
**/
    public static List<MX_SB_VTS_Beneficiario__c> getBenefDataById(List<MX_SB_VTS_Beneficiario__c> datas, String benFields) {
        R_BENEF.addAll(dataBase.query(string.escapeSingleQuotes(LSELECT +benFields + ' FROM MX_SB_VTS_Beneficiario__c where Id=:datas')));
        return R_BENEF;
    }
    /**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param List<MX_SB_VTS_Beneficiario__c> benefData
* @return void
**/
    public static void updateBeneficiario(List<MX_SB_VTS_Beneficiario__c> benefData) {
        UPDATE benefData;
    }
    /**
* @description
* @author Juan Carlos Benitez | 6/30/2020
* @param MX_SB_VTS_Beneficiario__c benefObj
* @return void
**/
    public static void insertBenef(MX_SB_VTS_Beneficiario__c benefObj) {
        upsert benefObj;
    }
    /**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param List<MX_SB_VTS_Beneficiario__c> datas
* @param String benFields
* @return R_BENEF
**/
    public static List<MX_SB_VTS_Beneficiario__c> getBenefByAccId(String condition, String benFields, String value ) {
        switch on condition {
            when 'QuoteId' {
                RET_BENEF.addAll(dataBase.query(string.escapeSingleQuotes(LSELECT +benFields + ' FROM MX_SB_VTS_Beneficiario__c where MX_SB_VTS_Quote__c=:value')));
            }
            when 'Id' {
                RET_BENEF.addAll(dataBase.query(string.escapeSingleQuotes(LSELECT +benFields + ' FROM MX_SB_VTS_Beneficiario__c where Id=:value')));
            }
        }
        return RET_BENEF;
    } 
        /**
* @description
* @author Juan Carlos Benitez | 6/25/2020
* @param String quotId
* @return countQuery
**/
    public static integer getCountBenef(String quotId) {
    	return Database.countQuery('SELECT count() FROM MX_SB_VTS_Beneficiario__c where MX_SB_VTS_Quote__c=:quotId');
    }
        /**
* @description
* @author Juan Carlos Benitez | 6/30/2020
* @param MX_SB_VTS_Beneficiario__c benefObj
* @return void
**/
    public static void dltBenef(MX_SB_VTS_Beneficiario__c benef) {
        Delete benef;
    }
}