/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 01-04-2021
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   01-04-2021   Eduardo Hernández Cuamatzi   Initial Version
 * 1.1   02-22-2021   Eduardo Hernández Cuamatzi   Se agrega validación para actualización de coberturas
 * 1.1   02-22-2021   Eduardo Hernández Cuamatzi   Se agrega guardado de mensualidades
 * 1.1.1 09-03-2021   Eduardo Hernández Cuamatzi   Se agrega validación de cupón
**/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_SB_VTS_DeleteCoverageHSD_Services {
    /**Final code 200 response Ok */
    private static final Integer STATUSOK = 200;
    /** Variable apoyo diferencia de cupones*/
    private static final Decimal DIFFCUPON = 0.5;

    /**Coberturas base */
    final static List<String> BASICCOVERAGES = new List<String>{'FIRE_LIGHTNING', 'DEBRIS_REMOVAL', 'EXTRAORDINARY_EXPENSES'};

    /**Campos base de coberturas */
    public final static String LSTFIELDSCOBER = 'MX_SB_VTS_TradeValue__c, MX_SB_VTS_GoodTypeCode__c, MX_SB_VTS_RelatedQuote__c, MX_SB_VTS_CoveragePercentage__c, MX_SB_VTS_CoverageCode__c, MX_SB_VTS_ContractCriterial__c,'+
    'MX_SB_VTS_CodeTrade__c, MX_SB_VTS_CategoryCode__c, MX_SB_MLT_SumaAsegurada__c, MX_SB_MLT_IdeCobertura__c, MX_SB_MLT_Estatus__c, MX_SB_MLT_Descripcion__c, MX_SB_MLT_Cobertura__c, Id, MX_SB_VTS_SelectedPay__c, MX_SB_VTS_PayList__c, MX_SB_VTS_CoversAmount__c, CurrencyIsoCode';

    /**Variable de Id para mapas*/
    final static String STRID = 'id';
    /**Variable frecuencies para mapas*/
    final static String FRECUENCIES = 'frequencies';

    /**
    * @description Recupera datos particulares
    * @author Eduardo Hernández Cuamatzi | 01-08-2021 
    * @param mapQuotes datos de Quotes
    * @return Map<Id, List<Cobertura__c>> Mapa de coberturas por cotización
    **/
    public static Map<Id, List<Cobertura__c>> findContractingCriteria(Map<Id, Quote> mapQuotes, String devNameCov) {
        final Map<Id, List<Cobertura__c>> dataContQuotes = new Map<Id, List<Cobertura__c>>();
        final String extraQuery = ' AND RecordType.DeveloperName = \''+devNameCov+'\'';
        for (Cobertura__c variable : MX_RTL_Cobertura_Selector.findCoverByQuote(LSTFIELDSCOBER, extraQuery, mapQuotes.keySet())) {
            if(dataContQuotes.containsKey(variable.MX_SB_VTS_RelatedQuote__c)) {
                dataContQuotes.get(variable.MX_SB_VTS_RelatedQuote__c).add(variable);
            } else {
                final List<Cobertura__c> lstCoverages = new List<Cobertura__c>{variable};
                dataContQuotes.put(variable.MX_SB_VTS_RelatedQuote__c, lstCoverages);
            }
        }
        return dataContQuotes;
    }

    /**
    * @description Crea estructura para la petición de eliminar una cobertura
    * @author Eduardo Hernández Cuamatzi | 12-30-2020 
    * @param mapCoveragesVals Lista de coberturas a quitar
    * @param oppData Datos de la Oportunidad
    * @param masterTrade Cobertura principal de la cotización
    * @return Map<String, Object> Mapa de estructura para petición
    **/
    public static Map<String, Object> fillRemoveCoverages(List<Cobertura__c> mapCoveragesVals, Opportunity oppData, String masterTrade) {
        final Map<String, Object> jsonCriterial = new Map<String, Object>();
        jsonCriterial.put('allianceCode', 'DHSDT003');
        jsonCriterial.put('certifiedNumberRequested', '1');
        final Map<String, String> findProd = MX_SB_VTS_DeleteCoverageHSD_Helper.findProdCodByName(oppData.Producto__c);
        jsonCriterial.put('product', MX_SB_VTS_DeleteCoverageHSD_Helper.fillProductElement(findProd.get(oppData.Producto__c.toUpperCase()), masterTrade));
        final Map<String, Object> frecuenciElement = new Map<String, Object>();
        frecuenciElement.put(STRID, 'YEARLY');
        final List<Object> lstFrecElement = new List<Object>{frecuenciElement};
        jsonCriterial.put(FRECUENCIES, lstFrecElement);
        final List<Object> lstContractCrit = new List<Object>();
        jsonCriterial.put('contractingCriteria', lstContractCrit);
        final Map<String, Object> fillCovers = MX_SB_VTS_FillCoveragesTrades_helper.fillCoverages(mapCoveragesVals);
        final List<Object> lstTrades = new List<Object>();
        for (String keyItem : fillCovers.keySet()) {
            lstTrades.add(MX_SB_VTS_editCoverageHSD_Services.fillCategories(fillCovers.get(keyItem)));
        }
        jsonCriterial.put('trades', lstTrades);
        return jsonCriterial;
    }

    /**
    * @description Petición al servicio CalculateQuotePrice
    * @author Eduardo Hernández Cuamatzi | 12-30-2020 
    * @param bodyRequest Json de petición
    * @param folioAso folio de cotización
    * @return Map<String, Object> respuesta de la petición
    **/
    public static Map<String, Object> processCoverages(String bodyRequest, String folioAso) {
        final Map<String, Object> returnVals = new Map<String, Object>();
        final Map<String, Object> paramsSrv = new Map<String, Object>{'priceid' => folioAso};
        final HttpResponse responseSrv = MX_RTL_IasoServicesInvoke_Selector.callServices('CalculateQuotePrice', paramsSrv, requestDeleteCoverage(bodyRequest, folioAso));
        if(responseSrv.getStatusCode() ==  STATUSOK) {
            returnVals.put('responseOk', true);
            returnVals.put('getData', responseSrv.getBody());
        } else {
            returnVals.put('responseOk', false);
        }
        return returnVals;
    }

    /**
    * @description Crea request de petición
    * @author Eduardo Hernández Cuamatzi | 12-30-2020 
    * @param mapStr Body request
    * @param priceId Folio cotización
    * @return HttpRequest request para quitar las coberturas
    **/
    public static HttpRequest requestDeleteCoverage(String mapStr, String priceId) {
        final HttpRequest request = new HttpRequest();
        final Set<String> lstTypes = new Set<String>{'IASO01'};
        final List<MX_SB_VTS_Generica__c> dataGen = MX_SB_VTS_Generica_Selector.findByTypeVal('Name, MX_SB_VTS_HEADER__c', lstTypes);
        final Map<String,iaso__GBL_Rest_Services_Url__c> allCodes = iaso__GBL_Rest_Services_Url__c.getAll();
        final iaso__GBL_Rest_Services_Url__c restSendByEmail = allCodes.get('CalculateQuotePrice');
        final String endPoint = restSendByEmail.iaso__Url__c + priceId + '/calculate-prices';
        request.setTimeout(120000);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Accept', '*/*');
        request.setHeader('Host', dataGen[0].MX_SB_VTS_HEADER__c);
        request.setEndpoint(endPoint);
        request.setBody(mapStr);
        return request;
    }

    /**
    * @description Procesa respuesta de agregar o quitar coberturas
    * @author Eduardo Hernández Cuamatzi | 12-30-2020 
    * @param jsonResponse json de respuesta
    * @param quoteId Id de cotización
    * @return Map<String, String> Datos de pagos para mostrar en front
    **/
    public static Map<String, String> processResponse(String jsonResponse, Id quoteId) {
        final Map<String, Object> mapCalPrices  = (Map<String, Object>)Json.deserializeUntyped(jsonResponse);
        Map<String, String> dataPayments = new Map<String, String>();
        if(mapCalPrices.containsKey(FRECUENCIES)) {
            final List<Object> mapFrecuen = (List<Object>)JSON.deserializeUntyped(JSON.serialize(mapCalPrices.get(FRECUENCIES)));
            dataPayments = MX_SB_VTS_DeleteCoverageHSD_Helper.fillDatPayments(mapFrecuen);
        }
        final String totalAnual = dataPayments.get('YEARLY_Frac');
        final String totalsemi = dataPayments.get('SEMESTER_Frac');
        final String totalMens = dataPayments.get('MONTHLY_Frac');
        final String totalTrin = dataPayments.get('QUARTELY_Frac');
        final String totalMonthlys = totalAnual + '|' + totalsemi + '|' + totalTrin + '|' +totalMens;
        final Quote quoteUpdate = new Quote(Id = quoteId, 
        MX_SB_VTS_TotalAnual__c = Decimal.valueOf(dataPayments.get('YEARLY')), 
        MX_SB_VTS_TotalMonth__c = Decimal.valueOf(dataPayments.get('MONTHLY')), 
        MX_SB_VTS_TotalQuarterly__c = Decimal.valueOf(dataPayments.get('QUARTELY')), 
        MX_SB_VTS_TotalSemiAnual__c = Decimal.valueOf(dataPayments.get('SEMESTER')),
        MX_SB_VTS_GCLID__c = totalMonthlys);
        MX_RTL_Quote_Selector.upsrtQuote(quoteUpdate);
        return dataPayments;
    }

    /**
    * @description Crea peticiones para editar datos de Mascotas
    * @author Eduardo Hernández Cuamatzi | 01-01-2021 
    * @param mapCriterVals Lista de datos particulares
    * @param oppData Datos de Oportunidad
    * @param valuesCriterial Valores nuevos para la cobertura
    * @param critCode Código de dato particular
    * @return Map<String, Object> Mapa de estructura para petición de calculate prices
    **/
    public static Map<String, Object> fillEditPets(List<Cobertura__c> mapCriterVals, Opportunity oppData, List<String> valuesCriterial, List<String> critCode) {
        final Map<String, Object> petsCriterial = new Map<String, Object>();
        petsCriterial.put('allianceCode', 'DHSDT003');
        petsCriterial.put('certifiedNumberRequested', '1');
        final Map<String, String> findProd = MX_SB_VTS_DeleteCoverageHSD_Helper.findProdCodByName(oppData.Producto__c);
        petsCriterial.put('product', MX_SB_VTS_DeleteCoverageHSD_Helper.fillProductElement(findProd.get(oppData.Producto__c.toUpperCase()), valuesCriterial[0]));
        final Map<String, Object> freElement = new Map<String, Object>();
        freElement.put(STRID, valuesCriterial[5]);
        final List<Object> lstFreElement = new List<Object>{freElement};
        petsCriterial.put(FRECUENCIES, lstFreElement);
        final List<Object> lstContractCrit = new List<Object>();
        for (Cobertura__c criteriaContract : mapCriterVals) { 
            final Map<String, Object> criterialElement = new Map<String, Object>();
            criterialElement.put('criterial', criteriaContract.MX_SB_MLT_Cobertura__c);
            if(critCode.contains(criteriaContract.MX_SB_MLT_Cobertura__c)) {
                criterialElement.put(STRID, valuesCriterial[1]);
                criterialElement.put('value', valuesCriterial[2]);
            } else {
                criterialElement.put(STRID, criteriaContract.MX_SB_MLT_Descripcion__c);
                criterialElement.put('value', criteriaContract.MX_SB_VTS_TradeValue__c);
            }
            lstContractCrit.add(criterialElement);
        }
        petsCriterial.put('contractingCriteria', lstContractCrit);
        final Map<String, Object> mapTradesMas = new Map<String, Object>();
        final Map<String, Object> mapCategoriesMas = new Map<String, Object>();
        final Map<String, Object> mapGoodTypesMas = new Map<String, Object>();
        mapTradesMas.put(STRID, 'FIRE');
        mapCategoriesMas.put(STRID, valuesCriterial[3]);
        mapGoodTypesMas.put(STRID, valuesCriterial[4]);
        final List<Object> lstCoverages = new List<Object>();
        for(String coverage : BASICCOVERAGES) {
            final Map<String, Object> mapCoveragesMas = new Map<String, Object>();
            mapCoveragesMas.put(STRID, coverage);
            lstCoverages.add(mapCoveragesMas);
        }
        mapGoodTypesMas.put('coverages', lstCoverages);
        final List<Object> lstGoodTypes = new List<Object>{mapGoodTypesMas};
        mapCategoriesMas.put('goodTypes', lstGoodTypes);
        final List<Object> lstCategories = new List<Object>{mapCategoriesMas};
        mapTradesMas.put('categories', lstCategories);
        final List<Object> lstTrades = new List<Object>{mapTradesMas};
        petsCriterial.put('trades', lstTrades);
        return petsCriterial;
    }

    /**
    * @description Actualizar una lista de coberturas 
    * @author Eduardo Hernández Cuamatzi | 01-04-2021 
    * @param lstCovers Lista de coberturas
    * @param isActive Indica si se activa la cobertura o desactiva
    **/
    public static void updateCoverage(List<Cobertura__c> lstCovers, Boolean isActive, List<String> valuesCriterial) {
        for (Cobertura__c coverageItem : lstCovers) {
            String activeCover = 'Inactivo';
            if(isActive) {
                activeCover = 'Activo';
            }
            if(valuesCriterial.isEmpty()) {
                coverageItem.MX_SB_MLT_Estatus__c = activeCover;
            } else {
                coverageItem.MX_SB_MLT_Estatus__c = activeCover;
                coverageItem.MX_SB_MLT_Descripcion__c = valuesCriterial[1];
                coverageItem.MX_SB_VTS_TradeValue__c = valuesCriterial[2];
            }
        }
        MX_RTL_Cobertura_Selector.updateCoverages(lstCovers);
    }

    /**
    * @description Valida si se aplico un descuento
    * @author Eduardo Hernández Cuamatzi | 01-05-2021 
    * @param jsonResponse respuesta del servicio
    * @param mapCoveragesCodes mapa de coberturas a actualizar
    * @param valuesCriterial datos particulares
    * @param coverActive 
    * @return Boolean 
    **/
    public static Boolean processCupon(String jsonResponse, List<Cobertura__c> mapCoveragesCodes, List<String> valuesCriterial) {
        final Map<String, Object> mapCalPrices  = (Map<String, Object>)Json.deserializeUntyped(jsonResponse);
        final List<String> tempValues = new List<String>();
        tempValues.addAll(valuesCriterial);
        Boolean isValidCup = false;
        if(mapCalPrices.containsKey(FRECUENCIES)) {
            final List<Object> mapFrecuen = (List<Object>)JSON.deserializeUntyped(JSON.serialize(mapCalPrices.get(FRECUENCIES)));
            final String totalAmount = MX_SB_VTS_DeleteCoverageHSD_Helper.amountTotal(mapFrecuen[0], 'YEARLY_TOTAL');
            final String totalCupon = MX_SB_VTS_DeleteCoverageHSD_Helper.amountTotal(mapFrecuen[0], 'WITHOUT_DISCOUNT');
            if(totalAmount.equalsIgnoreCase(totalCupon) == false) {
                final Decimal tempPrim = Decimal.valueOf(totalAmount);
                final Decimal tempCup = Decimal.valueOf(totalAmount);
                final Decimal tempDiff = tempCup -tempPrim;
                if(tempDiff.abs() > DIFFCUPON) {
                    isValidCup = true;
                }
            } else {
                tempValues[2] = 'NA';
            }
        }
        MX_SB_VTS_DeleteCoverageHSD_Services.updateCoverage(mapCoveragesCodes, true, tempValues);
        return isValidCup;
    }
}