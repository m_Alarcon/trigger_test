/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpValidateOTPResp
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
 @SuppressWarnings('sf:ShortClassName, sf:LongVariable, sf:ShortVariable,sf:AvoidFieldNameMatchingTypeName')
public class MX_SB_PS_authenticationWraper {
  /*Public property for wrapper*/
        public authentication authentication {get;set;}
	  /*Public property for wrapper*/	
        public backendUserRequest backendUserRequest {get;set;}
       /*Public Constructor for wrapper*/  
       public MX_SB_PS_authenticationWraper() {
            authentication = new authentication();
            backendUserRequest = new backendUserRequest();
        }
      /*Public subclass for wrapper*/
    public class authentication {
	  /*Public property for wrapper*/
        public string userID {get;set;} 
	  /*Public property for wrapper*/	
        public string consumerID {get;set;} 
	  /*Public property for wrapper*/	
        public string authenticationType {get;set;}
	  /*Public property for wrapper*/	
        public authenticationData[] authenticationData {get;set;}
	/*Public Constructor for wrapper*/  
       public authentication() {
        userID = '';
		    consumerID = '';
		    authenticationType = '';
		    authenticationData =  new authenticationData[] {};
		    authenticationData.add(new authenticationData());
        }
    }
    /*Public subclass for wrapper*/  
   public class authenticationData {
	    /*Public property for wrapper*/	
      public string idAuthenticationData {get;set;}
	   /*Public property for wrapper*/	
      public String[] authenticationData {get;set;}
	   /*Public constructor subclass*/	
      public authenticationData() {
           authenticationData = new string[] {};
           idAuthenticationData = '';		   
       }
    }
      /*Public subclass for wrapper*/
     public class backendUserRequest { 
	   /*Public property for wrapper*/
        public string userId {get;set;}
	  /*Public property for wrapper*/	
        public string accessCode {get;set;}
	  /*Public property for wrapper*/	
         public string dialogId {get;set;}
	  /*Public Constructor for wrapper*/  
       public backendUserRequest() {
		   userId = '';
		   accessCode = '';
		   dialogId = '';

        }	 
    }
    
    }