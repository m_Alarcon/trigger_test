/**
* ------------------------------------------------------------------------------
* @Nombre: MX_RTE_InvitadosController_Test
* @Autor: Sandra Ventura García
* @Proyecto: Workflow GFD
* @Descripción : Clase test de controller para componentes MX_RTE_InvitadosGFD (MX_WF_TaskforceController)
* ------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ------------------------------------------------------------------------------
* 1.0           04/01/2020     Sandra Ventura               Creación
* 1.1           11/06/2020     Selena rodriguez              Restore methods for delete systemdebug
* ------------------------------------------------------------------------------
*/
@isTest
public class MX_RTE_InvitadosController_Test {
  /** error message test exeptions */
    final static String ERROR_MSG = 'Script-thrown exception';
    /*Variable user GFD*/
    final static user USER_GFD=UtilitysDataTest_tst.CrearUsuario('Usuario GFD','MX_RTE','Administrador RTE');
    /*Variable msje catch DML*/
    static String mensaje;

  /**
  * @description: test lista contactos invitados GFD
  * @author Sandra Ventura
  */
    @isTest static void listinvitados() {

            MX_RTE_UtilityEventos.createIniciativa();
            test.startTest();
            final Contact[] invrecu= MX_WF_TaskforceController.getInvitados('MX_RTE_Usuarios', true);
            System.assertEquals(invrecu.size(),1,'No se encontraron Invitados');
            test.stopTest();


    }
  /**
  * @description: test lista contactos invitados GFD excepcion
  * @author Sandra Ventura
  */
   @isTest
    static String listinvEx() {
        test.startTest();
        try {
          MX_WF_TaskforceController.getInvitados('invalid-name', true);
        } catch (Exception e) {
            mensaje = e.getMessage();

        }
        test.stopTest();
        System.assertEquals(ERROR_MSG, mensaje,'Insert failed');
        return mensaje='Invitados obtenidos';
    }
  /**
  * @description: test lista contactos GFD roles
  * @author Sandra Ventura
  */
    @isTest static void roleslist() {
            MX_RTE_UtilityEventos.createIniciativa();
            test.startTest();
            final List<AggregateResult> invrecu= MX_WF_TaskforceController.getInvitadosRol('MX_RTE_Usuarios', true);
            System.assertEquals(invrecu.size(),1,'No se encontraron inivtados');
            test.stopTest();
    }
  /**
  * @description: test lista contactos GFD roles
  * @author Sandra Ventura
  */
   @isTest
    static String listrolesEx() {
        test.startTest();
        try {
          MX_WF_TaskforceController.getInvitadosRol('invalid-name', false);
        } catch (Exception e) {
            mensaje = e.getMessage();

        }
        test.stopTest();
        System.assertEquals(ERROR_MSG, mensaje,'Insert failed');
        return mensaje='Usuarios insertados';
    }
  /**
  * @description: test lista iniciativas GFD roles
  * @author Sandra Ventura
  */
    @isTest static void listiniciativa() {
            MX_RTE_UtilityEventos.createIniciativa();
            final date now = date.today();
            test.startTest();
            final MX_RTE_Iniciativa__c[] iniciativa= MX_WF_TaskforceController.getInvitadosEquipo(now, true);
            System.assertEquals(iniciativa.size(),1,'No se encontraron iniciativas');
            test.stopTest();
    }
  /**
  * @description: test lista iniciativas GFD roles Exeption
  * @author Sandra Ventura
  */
    @isTest static String listinicsEx() {
        test.startTest();
        try {
            final date invalid = date.newinstance(1960, 2, 17);
          MX_WF_TaskforceController.getInvitadosEquipo(invalid, true);
        } catch (Exception e) {
            mensaje = e.getMessage();
        }
        test.stopTest();
        System.assertEquals(ERROR_MSG, mensaje,'Lista de iniciativas error');
        return mensaje='Iniciativas creadas';
    }
  /**
  * @description: test lista final de contactos GFD
  * @author Sandra Ventura
  */
    @isTest static void listfinalinv() {
            final MX_RTE_Iniciativa__c[] iniciativa = MX_RTE_UtilityEventos.createIniciativa();
            final Id recTypeUser = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_RTE_Usuarios' AND sObjectType = 'Contact'].Id;
            final List<MX_RTE_Equipo__c> listinvi =[SELECT MX_RTE_Nombre__c FROM MX_RTE_Equipo__c WHERE MX_RTE_Iniciativa__c=: iniciativa[0].Id];
            final List<Contact> roles = [SELECT MX_RTE_Rol__c FROM Contact WHERE recordtypeid=:recTypeUser AND Id in (SELECT MX_RTE_Nombre__c FROM MX_RTE_Equipo__c WHERE MX_RTE_Iniciativa__c=: iniciativa[0].Id)];
            final String[] siniciativa = New String[] {};
                final String[] sinvitados = New String[] {};
                    final String[] sroles = New String[] {};
                        siniciativa.add(iniciativa[0].Id);
            sinvitados.add(listinvi[0].MX_RTE_Nombre__c);
            sroles.add(roles[0].MX_RTE_Rol__c);
            sinvitados.addAll(siniciativa);
            test.startTest();
            final Contact[] contactos = MX_WF_TaskforceController.getInvitadosSel(sroles,sinvitados,'MX_RTE_Usuarios', true, true);
            System.assertEquals(contactos.size(),1,'No se encontraron contactos');
            test.stopTest();
    }
  /**
  * @description: test lista final de contactos GFD - exepcion
  * @author Sandra Ventura
  */
    @isTest static String finalexepc() {
        final String[] sinvitados = New String[] {};
        final String[] sroles = New String[] {};
        test.startTest();
        try {
             MX_WF_TaskforceController.getInvitadosSel(sroles,sinvitados,'MX_RTE_Invalid', false, true);
        } catch (Exception e) {
            mensaje = e.getMessage();
        }
        test.stopTest();
        System.assertEquals(ERROR_MSG, mensaje,'Lista de iniciativas exep');
        return mensaje='Lista iniciativas';
    }
}