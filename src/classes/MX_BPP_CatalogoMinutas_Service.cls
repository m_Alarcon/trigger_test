/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_CatalogoMinutas_Service
* @Author   	Héctor Saldaña | hectorisrael.saldana.contractor@bbva.com
* @Date     	Created: 2020-03-08
* @Description 	MX_BPP_CatalogoMinutas Service
* @Changes
*
*/
public without sharing class MX_BPP_CatalogoMinutas_Service {
    
    /* Constructor Method */
    @TestVisible
    private MX_BPP_CatalogoMinutas_Service() {}
    
    /**
    * @Description 	Checks the syntax for the record inner tags when update
     * @Param       Map<Id, MX_BPP_CatalogoMinutas__c> triggerNewMap
     * @Param       Map<Id, MX_BPP_CatalogoMinutas__c> triggerOldMap
    * @Return 		void
    **/
    public static void checkInnerTagsUpd(Map<Id, MX_BPP_CatalogoMinutas__c> triggerNewMap, Map<Id, MX_BPP_CatalogoMinutas__c> triggerOldMap) {
        final List<MX_BPP_CatalogoMinutas__c> catOpcToProcess = new List<MX_BPP_CatalogoMinutas__c>();
        final List<MX_BPP_CatalogoMinutas__c> catStatToProcess = new List<MX_BPP_CatalogoMinutas__c>();
        for(Id idCatalogo : triggerNewMap.keySet()) {
            MX_BPP_CatalogoMinutas_Service_Helper.cleanTextFields(triggerNewMap.get(idCatalogo));
            if ((MX_BPP_Minuta_Utils.hasChanged(triggerOldMap.get(idCatalogo).MX_Opcionales__c, triggerNewMap.get(idCatalogo).MX_Opcionales__c) ||
                MX_BPP_Minuta_Utils.hasChanged(triggerOldMap.get(idCatalogo).MX_Contenido__c, triggerNewMap.get(idCatalogo).MX_Contenido__c)) &&
                String.isNotBlank(triggerNewMap.get(idCatalogo).MX_Opcionales__c) && String.isNotBlank(triggerNewMap.get(idCatalogo).MX_Contenido__c)) {
                    catOpcToProcess.add(triggerNewMap.get(idCatalogo));
            }
            if(MX_BPP_CatalogoMinutas_Service_Helper.checkBodyTemplate(triggerOldMap.get(idCatalogo), triggerNewMap.get(idCatalogo))) {
                   catStatToProcess.add(triggerNewMap.get(idCatalogo));
            }
        }
        MX_BPP_CatalogoMinutas_Service_Helper.validateOpTagSyntax(catOpcToProcess);
        MX_BPP_CatalogoMinutas_Service_Helper.validateStTagSyntax(catStatToProcess);
    }
    
    /*
    * @Description 	Checks the syntax for the record inner tags when insert
     * @Param       List<MX_BPP_CatalogoMinutas__c> triggerNew
    * @Return 		void
    */
    public static void checkInnerTagsIns(List<MX_BPP_CatalogoMinutas__c> triggerNew) {
        final List<MX_BPP_CatalogoMinutas__c> catOpcToProcess = new List<MX_BPP_CatalogoMinutas__c>();
        for(MX_BPP_CatalogoMinutas__c catalogo : triggerNew) {
            MX_BPP_CatalogoMinutas_Service_Helper.cleanTextFields(catalogo);
            if (String.isNotEmpty(catalogo.MX_Opcionales__c)) {
            	catOpcToProcess.add(catalogo);
            }
        }
        MX_BPP_CatalogoMinutas_Service_Helper.validateOpTagSyntax(catOpcToProcess);
        MX_BPP_CatalogoMinutas_Service_Helper.validateStTagSyntax(triggerNew);
    }
    
}