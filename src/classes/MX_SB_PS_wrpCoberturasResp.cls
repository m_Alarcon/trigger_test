/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpCoberturasResp
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020                 Creación de la wrapper.
*/
 @SuppressWarnings('sf:LongVariable, sf:ShortVariable')
public class MX_SB_PS_wrpCoberturasResp {
    
    /*Public property for wrapper*/
	public insuranceCategorys[] insuranceCategorys {get;set;}
	
	/*Public constructor for wrapper*/
	public MX_SB_PS_wrpCoberturasResp() {
		insuranceCategorys =  new insuranceCategorys[] {};
        insuranceCategorys.add(new insuranceCategorys());		
	}
   /*Public subclass for wrapper*/
	public class insuranceCategorys {
	      /*Public property for wrapper*/
		public String description {get;set;}
		  /*Public property for wrapper*/
		 public string id {get;set;} 
		  /*Public property for wrapper*/
		public coverages[] coverages {get;set;}
		/*Public constructor for wrapper*/
		public insuranceCategorys() {
			description = '';
			id = '';
			coverages =  new coverages[] {};
			coverages.add(new coverages()); 		
		}
	}
	 /*Public subclass for wrapper*/
	public class coverages {
	  /*Public property for wrapper*/
		public String description {get;set;}
	  /*Public property for wrapper*/
		public String internalCode {get;set;}
	  /*Public property for wrapper*/	
		public String isMandatory {get;set;}
	  /*Public constructor for wrapper*/
		public coverages() {
			description ='';
            internalCode = '';
            isMandatory = '';			
		}
		
		
	}

}