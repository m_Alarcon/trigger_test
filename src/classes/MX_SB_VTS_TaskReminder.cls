/*
* BBVA - Mexico
* @Author: Diego Olvera
* MX_SB_VTS_TaskReminder
* @Version 1.0
* @LastModifiedBy: Diego Olvera
* @ChangeLog
* 1.0 Created class - Diego Olvera
* 1.1 Methods added from MX_SB_VTS_APX_ReagendaLead
*/
public without sharing virtual class MX_SB_VTS_TaskReminder { //NOSONAR
    /*Literal String Contacto */
    final static string CONTCTSTR = 'Contacto';
    /*Literal String Contacto */
    final static string QUEUESERE = 'MX_SB_VTS_ReagendaSe';

    public MX_SB_VTS_TaskReminder() {} //NOSONAR
    /*
* @Method: fetchTasks
* @Description: Returns a list of tasks assigned to the current user
*/
    @AuraEnabled
    public static List<Task> fetchTasks() {
        return [Select Id, ReminderDateTime, Who.Name, ActivityDate, OwnerId, Subject, FechaHoraReagenda__c, Description FROM Task
                WHERE OwnerId =: UserInfo.getUserId() AND ReminderDateTime > :Datetime.now().addMinutes(-15)
                ORDER BY ReminderDateTime ASC LIMIT 10];
    }

    /**
* @description: Assign a task and evaluates if there's a provider segmented, if not sends the task to a queue
* @author Eduardo Hernández Cuamatzi | 17/1/2020
* @param leadId
* @param myTask
* @param segmented
* @return Task
**/
    public static Task leadReagenda(String leadId, Task myTask, Boolean segmented, User managerId) {
        final Task mytaskLead = myTask;
        getLeadTask(leadId, myTask);
        mytaskLead.WhoId = leadId;
        if(segmented) {
            final Id queueId = [Select Id from Group where Type = 'Queue' and DeveloperName = 'MX_SB_VTS_Reagenda'].Id;
            final Lead getValues = [Select Resultadollamada__c, MX_SB_VTS_Tipificacion_LV2__c, MX_SB_VTS_Tipificacion_LV3__c, MX_SB_VTS_Tipificacion_LV4__c
                                    From Lead Where Id=: leadId];
            final List<Lead> sendLeadToCTI = checkRecordSource(leadId, true);
            for(Lead updLead : sendLeadToCTI) {
                if(String.isBlank(getValues.Resultadollamada__c) || String.isBlank(getValues.MX_SB_VTS_Tipificacion_LV2__c)
                   || String.isBlank(getValues.MX_SB_VTS_Tipificacion_LV3__c) || String.isBlank(getValues.MX_SB_VTS_Tipificacion_LV4__c)) {
                       updLead.OwnerId = queueId;
                       updLead.Resultadollamada__c = CONTCTSTR;
                       updLead.MX_SB_VTS_Tipificacion_LV2__c = 'Contacto Efectivo';
                       updLead.MX_SB_VTS_Tipificacion_LV3__c = 'No Interesado';
                       updLead.MX_SB_VTS_Tipificacion_LV4__c = 'No acepta cotización';
                       updLead.MX_SB_VTS_Tipificacion_LV5__c = 'No Venta (No acepta cotización)';
                       updLead.MX_SB_VTS_Tipificacion_LV6__c = 'Cliente no tiene tiempo';
                       //updLead.MX_SB_VTS_Tipificacion_LV7__c = 'Reagenda';
                   } else {
                       updLead.OwnerId = queueId;
                       //updLead.MX_SB_VTS_Tipificacion_LV7__c = 'Reagenda';
                   }
            }
            envioAutomatico(sendLeadToCTI, mytaskLead);
            Database.update(sendLeadToCTI);
            for(Lead updLead : sendLeadToCTI) {
                updLead.MX_SB_VTS_Tipificacion_LV7__c = 'Reagenda';
                updLead.MX_SB_VTS_IsReagenda__c = false;
            }
            Database.update(sendLeadToCTI);
        } else {
            reagendaLeadSE(leadId, myTask);
        }
        return mytaskLead;
    }

    /**
* @description: Assigns a tasks when leadSource equals Ventas Telemarketing with Seguro Estudia Product
* @author Diego Olvera | 17/1/2020
* @param leadId
* @param myTask
* @param managerId
* @return Task
**/
    public static void reagendaLeadSE(String leadId, Task myTask) {
        final Task mytaskLead = myTask;
        final List<Lead> sendCmbToCTI = checkRecordSource(leadId, true);
        final List<Group> queueSe = MX_SB_VTS_UpNameCampaign_Service.findGroupByName(QUEUESERE);
        for(Lead updLeadCmb : sendCmbToCTI) {
            for(Group lstGpo : queueSe) {
                mytaskLead.OwnerId = lstGpo.Id;
                mytaskLead.MX_SB_VTS_TipificacionNivel6__c = updLeadCmb.MX_SB_VTS_Tipificacion_LV6__c;
                updLeadCmb.Resultadollamada__c = '';
                updLeadCmb.MX_SB_VTS_Tipificacion_LV2__c = '';
                updLeadCmb.MX_SB_VTS_Tipificacion_LV3__c = '';
                updLeadCmb.MX_SB_VTS_Tipificacion_LV4__c = '';
                updLeadCmb.MX_SB_VTS_Tipificacion_LV5__c = '';
                updLeadCmb.MX_SB_VTS_Tipificacion_LV6__c = '';
                updLeadCmb.Status = Label.MX_SB_VTS_CONTACTO_LBL;
        	}
        }
        Database.update(sendCmbToCTI);
    }

    /**
* @description: Evaluates an opportunity according the new business requirement and assigns a tasks when leadSource equals SAC or Ventas Telemarketing
* @author Eduardo Hernández Cuamatzi | 17/1/2020
* @param leadId
* @param myTask
* @param segmented
* @param managerId
* @return Task
**/
    public static Task oppReagenda(String leadId, Task myTask, Boolean segmented, User managerId) {
        final Task taskOpp = myTask;
        final Opportunity lstOpp3 = [SELECT Id, RecordType.Name, MX_SB_VTS_Tipificacion_LV4__c,
                                     MX_SB_VTS_Tipificacion_LV5__c, MX_SB_VTS_Tipificacion_LV6__c,
                                     LeadSource  FROM Opportunity WHERE Id=: leadId];
        getOpportunityTask(leadId, taskOpp);
        taskOpp.WhatId = leadId;
        final List<Opportunity> sendOppToCTI = checkRecordSource(leadId, false);
        for(Opportunity updOpp : sendOppToCTI) {
            if(String.isBlank(lstOpp3.MX_SB_VTS_Tipificacion_LV4__c) || String.isBlank(lstOpp3.MX_SB_VTS_Tipificacion_LV5__c)
               || String.isBlank(lstOpp3.MX_SB_VTS_Tipificacion_LV6__c)) {
                   //updOpp.OwnerId = managerId.ManagerId;
                   updOpp.MX_SB_VTS_Tipificacion_LV1__c = CONTCTSTR;
                   updOpp.MX_SB_VTS_Tipificacion_LV2__c = 'Contacto Efectivo';
                   updOpp.MX_SB_VTS_Tipificacion_LV3__c = 'Interesado';
                   updOpp.MX_SB_VTS_Tipificacion_LV4__c = 'Acepta cotización';
                   updOpp.MX_SB_VTS_Tipificacion_LV5__c = 'No Venta (acepta cotización)';
                   updOpp.MX_SB_VTS_Tipificacion_LV6__c = 'Cliente no tiene tiempo (acepta cotización)';
                   //updOpp.MX_SB_VTS_Tipificacion_LV7__c = 'Reagenda';
               }
        }
        if(segmented == true) {
            envioAutomatico(sendOppToCTI, taskOpp);
        }
        Database.update(sendOppToCTI);
        for(Opportunity updOpp : sendOppToCTI) {
            updOpp.OwnerId = managerId.ManagerId;
            updOpp.MX_SB_VTS_Tipificacion_LV7__c = 'Reagenda';
            updOpp.MX_SB_VTS_IsOppReagenda__c = false;

		if(lstOpp3.RecordType.Name.equalsIgnoreCase('Cotizador RS Hospital')) {
                taskOpp.OwnerId = userinfo.getUserId();
                updOpp.MX_SB_VTS_Tipificacion_LV1__c = CONTCTSTR;
                updOpp.MX_SB_VTS_Tipificacion_LV2__c = 'Contacto Efectivo';
				updOpp.MX_SB_VTS_Tipificacion_LV3__c = 'No Interesado';
                updOpp.MX_SB_VTS_Tipificacion_LV4__c = 'No acepta cotización';
                updOpp.MX_SB_VTS_Tipificacion_LV5__c = 'No Venta (No acepta cotización)';
                updOpp.MX_SB_VTS_Tipificacion_LV6__c = 'Cliente no tiene tiempo';
                updOpp.MX_SB_VTS_Tipificacion_LV7__c = 'Reagenda';
            }
        }
        Database.update(sendOppToCTI);
        return taskOpp;
    }

    /**
* @description: Evaluates an opportunity according the new business requirement and assigns a tasks when leadSource equals SAC or Ventas Telemarketing
* @author Eduardo Hernández Cuamatzi | 17/1/2020
* @param leadId
* @param myTask
* @param segmented
* @param managerId
* @return Task
**/
    public static Task oppVtaReagenda(String leadId, Task myTask, Boolean segmented, User managerId) {
        final Task taskOpp = myTask;
        final Opportunity lstOpp3 = [SELECT Id, RecordType.Name, MX_SB_VTS_Tipificacion_LV4__c,
                                     MX_SB_VTS_Tipificacion_LV5__c, MX_SB_VTS_Tipificacion_LV6__c,
                                     LeadSource  FROM Opportunity WHERE Id=: leadId];
        getOpportunityTask(leadId, taskOpp);
        taskOpp.WhatId = leadId;
        final List<Opportunity> sendOppToCTI = checkRecordSource(leadId, false);
        final List<Group> queueSeOpp = MX_SB_VTS_UpNameCampaign_Service.findGroupByName(QUEUESERE); //NOSONAR
        for(Opportunity updOpp : sendOppToCTI) {
            if(lstOpp3.RecordType.Name.equalsIgnoreCase('Venta Asistida Digital')) {
                for(Group lstGroup : queueSeOpp ) {
                taskOpp.OwnerId = lstGroup.Id;
                updOpp.MX_SB_VTS_Tipificacion_LV1__c = '';
                updOpp.MX_SB_VTS_Tipificacion_LV2__c = '';
                updOpp.MX_SB_VTS_Tipificacion_LV3__c = '';
                updOpp.MX_SB_VTS_Tipificacion_LV4__c = '';
                updOpp.MX_SB_VTS_Tipificacion_LV5__c = '';
                updOpp.MX_SB_VTS_Tipificacion_LV6__c = '';
                updOpp.StageName = Label.MX_SB_VTS_CONTACTO_LBL;
                updOpp.MX_SB_VTA_SubEtapa__c = CONTCTSTR;
                taskOpp.MX_SB_VTS_TipificacionNivel6__c = lstOpp3.MX_SB_VTS_Tipificacion_LV6__c;
                }
            } else {
                taskOpp.OwnerId = managerId.ManagerId;
            }
        }
        if(segmented == true) {
            envioAutomatico(sendOppToCTI, taskOpp);
        }
        Database.update(sendOppToCTI);
        return taskOpp;
    }
    /*
* @Method: getLeadTask
* @Description: assigns id to task if already exists one
* @Params: myTask: Task Object, recordId: lead id
*/
    private static void getLeadTask(Id recordId, Task myTask) {
        final Lead tId = [Select MX_SB_VTS_LookActivity__c from Lead where Id =: recordId];
        if(String.isNotBlank(tId.MX_SB_VTS_LookActivity__c)) {
            myTask.Id = tId.MX_SB_VTS_LookActivity__c;
        }
    }
    /*
* @Method: getOpportunityTask
* @Description: assigns id to task if already exists one
* @Params: myTask: Task Object, recordId: lead id
*/
    private static void getOpportunityTask(Id recordId, Task myTask) {
        final Opportunity oId = [Select MX_SB_VTS_LookActivity__c from Opportunity where Id =: recordId];
        if(String.isNotBlank(oId.MX_SB_VTS_LookActivity__c)) {
            myTask.Id = oId.MX_SB_VTS_LookActivity__c;
        }
    }

    /**
* @Method: envioAutomatic
* @Description: invoke service to perform callout to external service
**/
    @AuraEnabled
    public static void  envioAutomatico(List<sObject> lstCTI, Task myTask) {
        try {
            final MX_SB_VTS_Lead_tray__c getTray = [Select Id, MX_SB_VTS_ID_Bandeja__c from MX_SB_VTS_Lead_tray__c where MX_SB_VTS_Tipo_Bandeja__c =: System.Label.MX_SB_VTS_HotLeads];
            final String formatDate = myTask.FechaHoraReagenda__c.format(System.Label.MX_SB_VTS_FormatDateSmart);
            final Map<Id,String> tempdate = new Map<Id,String>();
            for(sObject obj: lstCTI) {
                tempdate.put(obj.Id, formatDate);
            }
            MX_SB_VTS_SendLead_helper_cls.EntRegistroGestion_invoke('EntRegistroGestion', lstCTI, '', MX_SB_VTS_LeadMultiCTI_Util.evaluteSmartValue(), getTray.MX_SB_VTS_ID_Bandeja__c, tempdate, ' Reagenda');
        } catch(System.QueryException qEx) {
            throw new AuraHandledException(System.Label.MX_WB_LG_ErrorBack + qEx);
        }
    }

    /**
* @method: checkLeadSource: validate lead origin for call me back, tracking web and outbound tlmk
* @return: list of leads to be used
**/
    private static List<SObject> checkRecordSource(String recordId, Boolean isLead) {
        final List<SObject> recordList = new List<SObject>();
        if(isLead) {
            final List<Lead> sendLeadToCTI = [Select Id, Name, Apellido_Materno__c, LastName, FirstName, LeadSource,Producto_Interes__c,
                                              MobilePhone, MX_WB_ph_Telefono1__c, MX_WB_ph_Telefono2__c, MX_WB_ph_Telefono3__c, OwnerId,
                                              Resultadollamada__c, MX_SB_VTS_Tipificacion_LV2__c, MX_SB_VTS_Tipificacion_LV3__c,
                                              MX_SB_VTS_Tipificacion_LV4__c, MX_SB_VTS_Tipificacion_LV5__c,
                                              MX_SB_VTS_Tipificacion_LV6__c, MX_SB_VTS_Tipificacion_LV7__c, MX_SB_VTS_IsReagenda__c
                                              from Lead where Id =: recordId];
            for(Lead checkLead : sendLeadToCTI) {
                switch on checkLead.LeadSource {
                    when 'Call me back', 'Tracking Web', 'Facebook' {
                        checkLead.MX_WB_ph_Telefono1__c = checkLead.MobilePhone;
                        checkLead.MX_WB_ph_Telefono2__c = checkLead.MobilePhone;
                        checkLead.MX_WB_ph_Telefono3__c = checkLead.MobilePhone;
                        recordList.add(checkLead);
                    }
                    when 'Outbound TLMK' {
                        recordList.add(checkLead);
                    }
                }
            }
        } else {
            final List<Opportunity> sendOppToCTI = [Select Id, TelefonoCliente__c, Producto__c,MX_SB_VTS_Tipificacion_LV7__c, OwnerId, Account.Name, Account.Apellido_materno__pc,
                                                    LeadSource, Account.FirstName, Account.LastName, MX_SB_VTS_IsOppReagenda__c from Opportunity where Id =: recordId];
            recordList.addAll(sendOppToCTI);
        }
        return recordList;
    }
}