/**
 * @File Name          : QuotesSelector_Test.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernandez Cuamatzi
 * @Last Modified On   : 26/5/2020 14:06:33
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    26/5/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
@isTest
private class QuotesSelector_Test {
    /**@description Nombre permiso vida*/
    private final static string PERMISOVIDA = 'MX_SB_VTS_Vida';
    /**@description Nombre permiso contratos v*/
    private final static string PERMISOVIDAC = 'MX_SB_VTS_Permisos_Contrato_Vida';
    /**@description Nombre permiso contratos v*/
    private final static string CLASESVTS = 'MX_SB_VTS_Ventas';
    /**@description Nombre permiso contratos v*/
    private final static string PERMISOOBJECIONES = 'MX_SB_VTS_Objeciones';
    /**MX_SB_VTS_VistasTrackingVCIP */
    private final static string VISTASTRACK = 'MX_SB_VTS_VistasTrackingVCIP';
    /**@description Nombre permiso contratos h*/
    private final static string PERMISOHOGARC = 'MX_SB_VTS_Permisos_Contrato_Hogar';
    /**@description etapa Cotizacion*/
    private final static string COTIZACION = 'Cotización';
    /**@description Nombre usuario*/
    private final static string ASESORNAME = 'AsesorTest';
    /**@description Nombre permiso hogar*/
    private final static string PERMISOHOGAR = 'MX_SB_VTS_Hogar';
    /**@description Nombre permiso hogar*/
    private final static string QUOTERECUPER = 'Quote recuperado';
    
    @TestSetup
    static void makeData() {
        final User asesorUserQS = MX_WB_TestData_cls.crearUsuario(ASESORNAME, 'Telemarketing VTS');
        Insert asesorUserQS;

        final MX_WB_FamiliaProducto__c objFamProQS = MX_SB_VTS_CallCTIs_utility.newFamiliy('Hogar');
        insert objFamProQS;
        
        final Product2 proHogar = MX_SB_VTS_CallCTIs_utility.newProduct(System.Label.MX_SB_VTS_Hogar, objFamProQS);
        insert proHogar;

        final Account accToAsQS = MX_WB_TestData_cls.crearCuenta('Donnie', 'PersonAccount');
        accToAsQS.PersonEmail = 'donnie.test@test.com';
        insert accToAsQS;

        final Opportunity oppHsdQS = MX_WB_TestData_cls.crearOportunidad('OppTelemarketing', accToAsQS.Id, asesorUserQS.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
        oppHsdQS.Producto__c = System.Label.MX_SB_VTS_Hogar;
        oppHsdQS.Reason__c = 'Venta';
        oppHsdQS.Reason__c = 'Venta';
        oppHsdQS.StageName = COTIZACION;
        insert oppHsdQS;
        final Pricebook2 prB2 = MX_WB_TestData_cls.createStandardPriceBook2();
        final Quote quoteTemp = MX_WB_TestData_cls.crearQuote(oppHsdQS.Id, '999999 1 OppTest Close Opp', '999990');
        quoteTemp.Pricebook2Id= prB2.Id;
        quoteTemp.OwnerId = asesorUserQS.Id;
        final List<Quote> lsqQuot = new List<Quote>{quoteTemp};
        MX_RTL_Quote_Selector.insertQuotes(lsqQuot);
    }

    @isTest
    static void quoteByOpps() {
        final User asesorUser = [Select Id from User where LastName =: ASESORNAME];
        insertPermissionSet(PERMISOHOGAR, asesorUser.Id);
        insertPermissionSet(PERMISOVIDA, asesorUser.Id);
        insertPermissionSet(CLASESVTS, asesorUser.Id);
        insertPermissionSet(PERMISOOBJECIONES, asesorUser.Id);
        insertPermissionSet(PERMISOHOGARC, asesorUser.Id);
        insertPermissionSet(PERMISOVIDAC, asesorUser.Id);
        insertPermissionSet(VISTASTRACK, asesorUser.Id);
        Test.startTest();
        System.runAs(asesorUser) {
            final List<Opportunity> lstOpps = new List<Opportunity>();
            final Opportunity quoteId = [Select Id,Name from Opportunity where Name = 'OppTelemarketing'];
            lstOpps.add(quoteId);
            final List<Quote> quoliDat = MX_RTL_Quote_Selector.selectSObjectsByOpps(lstOpps);
            System.assertEquals(quoliDat.size(),1, 'Quotes recuperados');
        }
        Test.stopTest();
    }

    @isTest
    static void quoteById() {
        final User asesorUser = [Select Id from User where LastName =: ASESORNAME];
        insertPermissionSet(PERMISOHOGAR, asesorUser.Id);
        insertPermissionSet(PERMISOVIDA, asesorUser.Id);
        insertPermissionSet(CLASESVTS, asesorUser.Id);
        insertPermissionSet(PERMISOOBJECIONES, asesorUser.Id);
        insertPermissionSet(PERMISOHOGARC, asesorUser.Id);
        insertPermissionSet(PERMISOVIDAC, asesorUser.Id);
        insertPermissionSet(VISTASTRACK, asesorUser.Id);
        Test.startTest();
        System.runAs(asesorUser) {
            final Id quoteId = [Select Id from Quote where MX_SB_VTS_Folio_Cotizacion__c = '999990'].Id;
            final List<Quote> quoliDat = MX_RTL_Quote_Selector.selectSObjectsById(quoteId);
            System.assertEquals(quoliDat.size(),1, QUOTERECUPER);
        }
        Test.stopTest();
    }

    /**
    * @description Inserta permission sets
    * @author Eduardo Hernandez Cuamatzi | 26/5/2020 
    * @param String permissionName 
    * @param Id userId id de usuario
    * @return void 
    **/
    static void insertPermissionSet(String permissionName, Id userId) {
        final PermissionSet permissions = [SELECT Id FROM PermissionSet WHERE Name =: permissionName];
        insert new PermissionSetAssignment(AssigneeId = userId, PermissionSetId = permissions.Id);
    }


    @isTest
    static void resultQryQuote() {
        final User asesorUser = [Select Id from User where LastName =: ASESORNAME];
        insertPermissionSet(PERMISOHOGAR, asesorUser.Id);
        insertPermissionSet(PERMISOVIDA, asesorUser.Id);
        insertPermissionSet(CLASESVTS, asesorUser.Id);
        insertPermissionSet(PERMISOOBJECIONES, asesorUser.Id);
        insertPermissionSet(PERMISOHOGARC, asesorUser.Id);
        insertPermissionSet(PERMISOVIDAC, asesorUser.Id);
        insertPermissionSet(VISTASTRACK, asesorUser.Id);
        Test.startTest();
        System.runAs(asesorUser) {
            final Quote quoteId = [Select Id,OpportunityId from Quote where MX_SB_VTS_Folio_Cotizacion__c = '999990'];
            final List<Quote> lstQuotes = MX_RTL_Quote_Selector.resultQryQuote('Id, Name, QuoteToName', 'OpportunityId = \'' + quoteId.OpportunityId + '\'', true);
            System.assertEquals(lstQuotes.size(),1, QUOTERECUPER);
        }
        Test.stopTest();
    }

    @isTest
    static void resultQryQuoteFalse() {
        final User asesorUser = [Select Id from User where LastName =: ASESORNAME];
        insertPermissionSet(PERMISOHOGAR, asesorUser.Id);
        insertPermissionSet(PERMISOVIDA, asesorUser.Id);
        insertPermissionSet(CLASESVTS, asesorUser.Id);
        insertPermissionSet(PERMISOOBJECIONES, asesorUser.Id);
        insertPermissionSet(PERMISOHOGARC, asesorUser.Id);
        insertPermissionSet(PERMISOVIDAC, asesorUser.Id);
        insertPermissionSet(VISTASTRACK, asesorUser.Id);
        Test.startTest();
        System.runAs(asesorUser) {
            final Quote quoteId = [Select Id,OpportunityId from Quote where MX_SB_VTS_Folio_Cotizacion__c = '999990'];
            final List<Quote> lstQuFalse = MX_RTL_Quote_Selector.resultQryQuote('Id, Name, QuoteToName', 'OpportunityId = \'' + quoteId.OpportunityId + '\'', false);
            System.assertEquals(lstQuFalse.size(),1, QUOTERECUPER);
        }
        Test.stopTest();
    }

    @isTest
    static void deleteQuote() {
        final User asesorUser = [Select Id from User where LastName =: ASESORNAME];
        insertPermissionSet(PERMISOHOGAR, asesorUser.Id);
        insertPermissionSet(PERMISOVIDA, asesorUser.Id);
        insertPermissionSet(CLASESVTS, asesorUser.Id);
        insertPermissionSet(PERMISOOBJECIONES, asesorUser.Id);
        insertPermissionSet(PERMISOHOGARC, asesorUser.Id);
        insertPermissionSet(PERMISOVIDAC, asesorUser.Id);
        insertPermissionSet(VISTASTRACK, asesorUser.Id);
        Test.startTest();
        System.runAs(asesorUser) {
            final List<Quote> quoteId = MX_RTL_Quote_Selector.quoteData('999990');
            MX_RTL_Quote_Selector.deletQuote(quoteId);
            final List<Quote> quoteAfter = [Select Id,OpportunityId from Quote where MX_SB_VTS_Folio_Cotizacion__c = '999990'];
            System.assertEquals(quoteAfter.size(),0, QUOTERECUPER);
        }
        Test.stopTest();
    }
}