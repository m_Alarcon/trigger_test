/*
*
* @author BBVA Seguros - SAC Team
* @description This class will update object  Account from Visual Flows.
*
*           No  |     Date     |     Author               |    Description
* @version  1.0    11/08/2019     BBVA Seguros - SAC Team     Created
*
*/
public without sharing class MX_SB_SAC_UpdateAccount_Flows {
    
    /** Constructor */
    private MX_SB_SAC_UpdateAccount_Flows() {} // NOSONAR

    /**
    * @description 
    * @param MyAccounts 
    * @return List<ID> 
    **/
    @InvocableMethod(label='Update Account' description='Update Account')
    public static List<ID> updateAccount(List<Account> myAccounts) {

        final List<ID> updIDs = new List<ID>();

        final Database.SaveResult[] results = Database.update(myAccounts);

        for (Database.SaveResult result : results) {
            if (result.isSuccess()) {
                updIDs.add(result.getId());
            }
        }

        return updIDs;

    }

}