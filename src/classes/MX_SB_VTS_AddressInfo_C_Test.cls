/**
* @description       : Clase test que cubre a clase MX_SB_VTS_AddressInfo_Ctrl
* @author            : Diego Olvera
* @group             : BBVA
* @last modified on  : 02-11-2021
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   11-01-2020   Diego Olvera   Initial Version
**/
@isTest
public class MX_SB_VTS_AddressInfo_C_Test {
    /**@description Nombre usuario*/
    private final static String ASESORCMBNAM = 'AsesorTes';
    /**@description Nombre de la cuenta*/
    private final static String ACCCMBNAM = 'TestCm';
    /**@description Nombre de Oportunidad*/
    private final static String OPPCMBNAM = 'TestOppCm';
    /**@description Etapa de la Oportunida*/
    private final static String ETAPACMBO = 'Contacto';
    /**@description Monto de la Oportunidad a actualiza*/
    private final static String MONTOVA = '15000';
    
    @TestSetup
    static void makeData() {
        final User userRecCm = MX_WB_TestData_cls.crearUsuario(ASESORCMBNAM, 'System Administrator');
        insert userRecCm;
        final Account accntCm = MX_WB_TestData_cls.createAccount(ACCCMBNAM, System.Label.MX_SB_VTS_PersonRecord);
        insert accntCm;
        final Opportunity oppCm = MX_WB_TestData_cls.crearOportunidad(OPPCMBNAM, accntCm.Id, userRecCm.Id, System.Label.MX_SB_VTA_VentaAsistida);
        insert oppCm;
        final MX_RTL_MultiAddress__c nwAd = new MX_RTL_MultiAddress__c();
        nwAd.MX_RTL_MasterAccount__c = oppCm.AccountId;
        nwAd.MX_RTL_Opportunity__c = oppCm.Id;
        nwAd.Name = 'Direccion';
        insert nwAd;
        final Quote quoteRe = new Quote(OpportunityId=oppCm.Id, Name='Cmb');
        quoteRe.MX_SB_VTS_Folio_Cotizacion__c = '556432';
        insert quoteRe;
    }

    @isTest
    static void updCop() {
        final User userUpdOp = [Select Id from User where LastName =: ASESORCMBNAM];
        System.runAs(userUpdOp) {
            final Opportunity oppLstUp = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBNAM];
            final String etapaOpps = ETAPACMBO;
            if(etapaOpps == ETAPACMBO) {
                oppLstUp.StageName = 'Contacto';
            } else {
                oppLstUp.StageName = 'Cobro y resumen de contratación';
            }
            update oppLstUp;
            Test.startTest();
            try {
                MX_SB_VTS_AddressInfo_Ctrl.updCurrentOppCtrl(oppLstUp.Id, etapaOpps);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Mistak');
                System.assertEquals('Erro', extError.getMessage(),'No Actualizó la opp');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void getCop() {
        final User userGetOp = [Select Id from User where LastName =: ASESORCMBNAM];
        System.runAs(userGetOp) {
            final Opportunity oppLstVa = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBNAM];
            Test.startTest();
            try {
                MX_SB_VTS_AddressInfo_Ctrl.gtOppValsCrtl(oppLstVa.Id);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Faile');
                System.assertEquals('Faile', extError.getMessage(),'No obtuvo datos de la oportunidad');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void updAddres() {
        final User userGetOp = [Select Id from User where LastName =: ASESORCMBNAM];
        System.runAs(userGetOp) {
            final MX_RTL_MultiAddress__c getAddressVa = [Select Id, MX_RTL_Opportunity__c, Name FROM MX_RTL_MultiAddress__c WHERE Name = 'Direccion'];
            final Map<String, Object> dataMapCtr = new Map <String, Object>();
            dataMapCtr.put('checkedId', true);
            dataMapCtr.put('idAddrPro', getAddressVa.Id);
            dataMapCtr.put('valPropio', 'Propio');
            dataMapCtr.put('codigoPostal', '52922');
            dataMapCtr.put('State', 'Mx');
            dataMapCtr.put('Municipio', 'Mateos');
            dataMapCtr.put('City', 'Mx');
            dataMapCtr.put('Reference', 'Departamento');
            dataMapCtr.put('valUso', 'No');
            dataMapCtr.put('mtsBuild', '5002');
            Test.startTest();
            try {
                MX_SB_VTS_AddressInfo_Ctrl.createAddressCtrl(getAddressVa.MX_RTL_Opportunity__c, dataMapCtr);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Falla');
                System.assertEquals('Falla', extError.getMessage(),'No obtuvo datos de la dirección');
            }
            Test.stopTest();
        }
        
    }

    @isTest
    static void crtAddres() {
        final User userGetO = [Select Id from User where LastName =: ASESORCMBNAM];
        System.runAs(userGetO) {
            final MX_RTL_MultiAddress__c getAddressV = [Select Id, MX_RTL_Opportunity__c, Name FROM MX_RTL_MultiAddress__c WHERE Name = 'Direccion'];
            final Map<String, Object> dataMapCt = new Map <String, Object>();
            dataMapCt.put('checkedId', false);
            dataMapCt.put('idAddrPro', getAddressV.Id);
            dataMapCt.put('valPropio', 'Propio');
            dataMapCt.put('codigoPostal', '52927');
            dataMapCt.put('State', 'Hidalgo');
            dataMapCt.put('Municipio', 'Ixmi');
            dataMapCt.put('City', 'Mexico');
            dataMapCt.put('Reference', 'Casa');
            dataMapCt.put('valUso', 'Si');
            dataMapCt.put('mtsBuild', '500');
            Test.startTest();
            try {
                MX_SB_VTS_AddressInfo_Ctrl.createAddressCtrl(getAddressV.MX_RTL_Opportunity__c, dataMapCt	);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Horror');
                System.assertEquals('Fail', extError.getMessage(),'No obtuvo datos de la dirección');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void getDir() {
        final User userGe = [Select Id from User where LastName =: ASESORCMBNAM];
        System.runAs(userGe) {
            final MX_RTL_MultiAddress__c getAddres = [Select Id, MX_RTL_Opportunity__c, Name FROM MX_RTL_MultiAddress__c WHERE Name = 'Direccion'];
            Test.startTest();
            try {
                MX_SB_VTS_AddressInfo_Ctrl.getValAddressCtrl(getAddres.Id, 'Domicilio cliente');
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal error');
                System.assertEquals('Fatal error', extError.getMessage(),'No obtuvo datos del domicilio del cliente');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void updQu() {
        final User userGe = [Select Id from User where LastName =: ASESORCMBNAM];
        System.runAs(userGe) {
            final Opportunity oppLstUp = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBNAM];
            final Map<String, Object> dataMa = new Map <String, Object>();
            dataMa.put('NombreCliente', 'Kakashi');
            dataMa.put('ApellidoPaterno', 'Uchiha');
            dataMa.put('ApellidoMaterno', 'Kamui');
            dataMa.put('SexoHm', 'H');
            dataMa.put('FechaNac', '12/06/1987');
            dataMa.put('LugarNac', 'Monterrey');
            dataMa.put('rfc', 'MEMF3405');
            dataMa.put('Homoclave', '2S1');
            dataMa.put('TelefonoCel', '5510634745');
            dataMa.put('CorreoElect', 'sample1@sample.com');
            dataMa.put('Curp', 'MEMQ8305DFMLRG03');
            Test.startTest();
            try {
                MX_SB_VTS_AddressInfo_Ctrl.updQuoLineCtrl(oppLstUp.Id, dataMa);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal failed');
                System.assertEquals('Fatal failed', extError.getMessage(),'No obtuvo datos');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void getCobe() {
        final User userG = [Select Id from User where LastName =: ASESORCMBNAM];
        System.runAs(userG) {
             final Opportunity oppLst = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBNAM];
            Test.startTest();
            try {
                MX_SB_VTS_AddressInfo_Ctrl.fillCoberValsCtrl(oppLst.Id);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal1');
                System.assertEquals('Mistakes1', extError.getMessage(),'No obtuvo datos de la Cobertura');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void updSyncQ() {
        final User userGet = [Select Id from User where LastName =: ASESORCMBNAM];
        System.runAs(userGet) {
            final Opportunity oppL = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBNAM];
            Test.startTest();
            try {
                MX_SB_VTS_AddressInfo_Ctrl.updSyncQuoCtrl(oppL.Id, 'Outbound-Basica');
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal3');
                System.assertEquals('Fatal mistake3', extError.getMessage(),'No obtuvo datos de la Quote');
            }
            Test.stopTest();
        }
    }
    
     @isTest
    static void udpateAmoTest() {
        final User userU = [Select Id from User where LastName =: ASESORCMBNAM];
        System.runAs(userU) {
            final Opportunity oppLstUpdMa = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBNAM];
            final String montoA = MONTOVA;
            update oppLstUpdMa;
            Test.startTest();
            try {
                MX_SB_VTS_AddressInfo_Ctrl.updateSumas(oppLstUpdMa.Id, montoA);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Mistake failed');
                System.assertEquals('Error fatal', extError.getMessage(),'No Actualizó la opp con el valor');
            }
            Test.stopTest();
        }
    }
}