/**
 * @File Name          : MX_SB_VTS_GetAmountService.cls
 * @Description        : Mock generation to create a fake reponse and get values in a JSON
 * @Author             : Diego Olvera
 * @Group              :
 * @Last Modified By   : Diego Olvera
 * @Last Modified On   : 26/5/2020 16:58:29
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    28/4/2020   Diego Olvera     Initial Version
**/
public without sharing class MX_SB_VTS_GetAmountService {
      /* constructor
*/
    @SuppressWarnings('sf:UseSingleton')
    private MX_SB_VTS_GetAmountService() {}
    /**
    * @description Recupera valores por set de Ids
    * @author Diego Olvera Hernandez | 25/5/2020
    * @return Map<String, List<Object>> Mapa valores recuperados de un JSON
    **/
    @AuraEnabled(cacheable=true)
    public static Map<String,List<Object>> amountValues() {
        return MX_SB_VTS_GetAmountServiceCtrl.amountVal();
    }
}