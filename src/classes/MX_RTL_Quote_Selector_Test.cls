/**
 * @description       : Prueba los Methods de la clase MX_RTL_Quote_Selector
 * @author            : Daniel Perez Lopez
 * @group             : 
 * @last modified on  : 12-10-2020
 * @last modified by  : Daniel Alberto Ramirez Islas 
 * Modifications Log 
 * Ver   	Date         Author              		 Modification
 * 1.0         09/07/2020    Juan Carlos Benitez Herrera         Creación
 * 1.1         19/08/2020    Omar Gonzalez Rubio                 Actualizacion se agregan nuevas funciones
 * 1.2         11-25-2020    Daniel Perez Lopez   		         Se añade inserción de objetos a createDataB y methodos de prueba restantes
 * 1.3          2-12-2021   Juan Carlos Benitez Herrera           Se cambia product2 de test
 * 1.4          2-24-2021   Juan Carlos Benitez Herrera           Se añade producto__c en makedata
**/

@isTest
public class MX_RTL_Quote_Selector_Test {
    /** Nombre de usuario Test */
    final static String NAME = 'danylo';
    /** Apellido Paterno */
    Final static STRING LNAME= 'Hernandez';
    /** variable que almacena un folio de cotizacion */
    final static String FOLCOT = '556432';
    /**Campos Quote */
    final static String FIELDSQUOTE = 'Id,Name';
    /** variable que almacena un Id cotiza */
    final static String IDCOTQ = '0090556432';
    /* Variable que almacena el producto */
    final static String PROD2S = 'Auto Seguro Dinámico';
    /* Variable para Success */
    final static String SUCCS = 'SUCCESS'; 
    /** Campo que recibe la cotización */
    private static Quote ivrQuoteTst;
    
    @TestSetup
    static void createDataB() {
        final Account accT =  MX_WB_TestData_cls.crearCuenta('AccountTestPS','PersonAccount');  
        accT.Genero__c='M';
        insert accT;
        final MX_WB_FamiliaProducto__c famili= MX_WB_TestData_cls.createProductsFamily('Vida');
        insert famili;
        final product2  productNew2 = MX_WB_TestData_cls.productNew('Respaldo Seguro Para Hospitalización');
        productNew2.MX_SB_SAC_Proceso__c='SAC';
        productNew2.Family=famili.id;
        productNew2.MX_WB_FamiliaProductos__c=famili.id;
        insert productNew2;
        final PricebookEntry priceBkjEntr2 =MX_WB_TestData_cls.priceBookEntryNew(productNew2.Id);
        insert priceBkjEntr2;
        final Opportunity oppT = MX_WB_TestData_cls.crearOportunidad('Opp1',accT.Id,userInfo.getUserId(), 'MX_SB_COB_Cotizador_RS_Hospital');
        oppT.RecordTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_PS_RSHospitalario).getRecordTypeId();
        oppT.Producto__c='Respaldo Seguro Para Hospitalización';
        insert oppT;
        final Opportunity opp2T = MX_WB_TestData_cls.crearOportunidad('Opp2',accT.Id,userInfo.getUserId(), 'MX_SB_COB_Cotizador_RS_Hospital');
        opp2T.RecordTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_PS_RSHospitalario).getRecordTypeId();
        opp2T.Producto__c='Respaldo Seguro Para Hospitalización';
        insert opp2T;
        MX_WB_TestData_cls.createStandardPriceBook2();
        final Quote quoteTemp = new quote(opportunityid=oppT.Id, name=NAME, MX_SB_VTS_Folio_Cotizacion__c=FOLCOT,MX_ASD_PCI_IdCotiza__c='12345');        
        insert quoteTemp;
    }


    /**
    * @description Methodo test para obtener datos de la cotizacion
    * @author Daniel Alberto Ramirez Islas  | 12-02-2020 
    **/
    @isTest
    static void testgetQuote() {
        Final Quote quoteObj =[SELECT Id,OpportunityId, Name from Quote where Name = 'danylo' limit 1 ];
        test.startTest();
        	final list<quote> laquot = MX_RTL_Quote_Selector.getQuote(quoteObj);
                system.assert(!laquot.isEmpty(),'Se ha encontrado una cotizacion existente');
        test.stopTest();
    }

    /**
    * @description : Methodo test para actualiza datos de la cotizacion
    * @author Daniel Alberto Ramirez Islas  | 12-02-2020 
    **/
    @isTest
    static void testupsrtQuote() {
        Final Quote quoteObj2 =[SELECT Id, Name from Quote limit 1];
        test.startTest();
        	final quote quot2 =MX_RTL_Quote_Selector.upsrtQuote(quoteObj2);
		system.assertEquals(quot2.id,quoteObj2.id,'Se ha actualizado una cotizacion existente');
        test.stopTest();
    }
    /**
    * @description Methodo test para obtener una lista de cotizaciones apartir del folio cotizacion y producto
    * @author Daniel Alberto Ramirez Islas  | 12-02-2020 
    **/
     @isTest
    static void quoteDataSelector() {
        Test.startTest();
            final List<Quote> cots = MX_RTL_Quote_Selector.quoteData(FOLCOT,PROD2S);
            System.assertEquals(cots.size(), 1, SUCCS);
        Test.stopTest();
    }
    
    /**
    * @description Methodo test para obtener datos de cotizacion apartir de idcotiza 
    * @author Daniel Alberto Ramirez Islas  | 12-02-2020 
    **/
         @isTest
    static void quoteDataSelector2() {
        Test.startTest();
            final List<Quote> cots = MX_RTL_Quote_Selector.quoteData(FOLCOT);
            System.assertEquals(cots.size(), 1, SUCCS);
        Test.stopTest();
    }
        
    /**
    * @description Methodo test para otener cotizacion apartir del Id de la cotizacion
    * @author Daniel Alberto Ramirez Islas  | 12-02-2020 
    **/
    @isTest
    static void findQuoteById() {
        Test.startTest();
            final List<Quote> lstQuotes = MX_RTL_Quote_Selector.quoteData(FOLCOT,PROD2S);
            final Quote quoteId = MX_RTL_Quote_Selector.findQuoteById(lstQuotes[0].Id, FIELDSQUOTE);
            System.assertEquals(lstQuotes[0].Id, quoteId.Id, SUCCS);
        Test.stopTest();
    }
    
    /**
    * @description Methodo test para obtener informacion de cotizacion del IVR
    * @author Daniel Alberto Ramirez Islas  | 12-02-2020 
    **/
    @isTest
    static void quoteIvrRes() {
        Test.startTest();
        final List<Quote> cotResI = MX_RTL_Quote_Selector.quoteResponseIvr(FOLCOT,IDCOTQ);
        System.assertEquals(cotResI.size(), 0, SUCCS);
        Test.stopTest();
    }
    /**
    * @description Methodo test de MX_RTL_Quote_Selector.quoteRetrieve
    * @author Daniel Alberto Ramirez Islas  | 12-02-2020 
    **/
    @isTest
    static void quoteRetrieveTest() {
        Test.startTest();
        Final Quote param = [Select MX_SB_VTS_Folio_Cotizacion__c, OpportunityId From Quote Limit 1];
        final Quote cotResI1 = MX_RTL_Quote_Selector.quoteRetrieve(param.OpportunityId,param.MX_SB_VTS_Folio_Cotizacion__c);
        System.assert(cotResI1.id!=null, SUCCS);
        Test.stopTest();
    }
    @isTest
    static void quoteRetrieve() {
        Test.startTest();
        ivrQuoteTst = [SELECT Id, OpportunityId from Quote LIMIT 1];
        final Quote quoteRetri = MX_RTL_Quote_Selector.quoteRetrieve(ivrQuoteTst.OpportunityId,FOLCOT);
        Test.stopTest();
        System.assertNotEquals(quoteRetri.Id, ivrQuoteTst.Id, 'EXITO');
    }
    @isTest
    static void getMsjError() {
        Test.startTest();
        final List<MX_RTL_IVRCodes__mdt> codeMsj = MX_RTL_Quote_Selector.getMsjError('1003');
        Test.stopTest();
        System.assertEquals(codeMsj[0].MX_RTL_MsgSF__c, 'Por favor, intenta de nuevo', 'EXITO');
    }
}