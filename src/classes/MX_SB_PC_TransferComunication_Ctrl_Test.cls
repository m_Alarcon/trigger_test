/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 10-08-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   09-24-2020   Eduardo Hernández Cuamatzi   Initial Version
 * 1.0   08-10-2020   Omar Gonzalez                Editor
**/
@isTest
public class MX_SB_PC_TransferComunication_Ctrl_Test {
    /** Nombre de usuario Test */
    final static String NAMETS = 'Adriana';
    /** Apellido Paterno */
    Final static STRING LASNAME= 'Perez';
    /** variable que almacena un folio de cotizacion */
    final static String FOLIOQ = '556432';
    /** variable que almacena Id cotiza */
    final static String IDCOTIZAQ = '009556432';
    @TestSetup
    static void makeData() {
        initCallCenter();
        MX_SB_PS_OpportunityTrigger_test.makeData();
        Final Account accTest  = [Select Id from Account limit 1];
        Final Opportunity oppQuoTst = [Select id from Opportunity where AccountId=:accTest.Id limit 1 ];
        oppQuoTst.Name= NAMETS;
        oppQuoTst.StageName='Planes y Formas de pago';
        update oppQuoTst;
        Final Quote quotTstData= [Select Id from Quote where OpportunityId=:oppQuoTst.Id limit 1];
        quotTstData.Name=NAMETS;
        quotTstData.MX_SB_VTS_Folio_Cotizacion__c=FOLIOQ;
        quotTstData.MX_ASD_PCI_ResponseCode__c='""';
        quotTstData.MX_ASD_PCI_IdCotiza__c = IDCOTIZAQ;
        update quotTstData;
    }

    /**Inicia valores para callcenter */
    public static void initCallCenter() {
        final CallCenter callCentRec = [Select Id, Name from CallCenter where Name = 'PureCloud for Salesforce Lightning'];
        final User userTest = MX_WB_TestData_cls.crearUsuario('Asesor callcenter', System.Label.MX_WB_lbl_PerfilAsesoresTlmkt);
        userTest.CallCenterId = callCentRec.Id;
        insert userTest;
    }

    @isTest
    static void findCallcenter() {
        Test.startTest();
        final User userCall = [Select Id, CallCenterId from User where Name = 'Asesor callcenter' limit 1];
            System.runAs(userCall) {
                final String urlCallCenter = MX_SB_PC_TransferComunication_Ctrl.callCenterUrl();
                System.assertEquals('https://apps.mypurecloud.com/crm/index.html?crm=salesforce&color=darkblue', urlCallCenter, 'Call center recuperado');
            }
        Test.stopTest();        
    }
    @isTest
    static void quoteRetrieve() {
        final Quote quoteTstRet = [SELECT Id, OpportunityId FROM Quote WHERE MX_SB_VTS_Folio_Cotizacion__c =: FOLIOQ];
        Test.startTest();
        final Quote quoteRetMet = MX_SB_PC_TransferComunication_Ctrl.getQuoteIvr(quoteTstRet.OpportunityId,FOLIOQ);
        Test.stopTest();
        System.assertEquals(quoteRetMet.Id, quoteTstRet.Id, 'SUCCESS');
    }
    @isTest
    static void getResponseIvr() {
        Test.startTest();
        MX_SB_PC_TransferComunication_Ctrl.getResIvr(FOLIOQ,IDCOTIZAQ);
        system.assert(true,'Se ha actualizado una cotizacion existente');
        Test.stopTest();
    }
    @isTest
    static void activeLabel() {
        Test.startTest();
        final Boolean active = MX_SB_PC_TransferComunication_Ctrl.activeLabel();
        system.assert(true,active);
        Test.stopTest();
    }
    @isTest
    static void userInfPermission() {
        Test.startTest();
        final Boolean permissionIvr = MX_SB_PC_TransferComunication_Ctrl.userInfPermission();
        system.assert(true,permissionIvr);
        Test.stopTest();
    }
}