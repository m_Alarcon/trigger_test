/**
 * @File Name          : MX_SB_SAC_IdentificarCliente.cls
 * @Description        :
 * @Author             : Jaime Terrats
 * @Group              :
 * @Last Modified By   : Jaime Terrats
 * @Last Modified On   : 07-16-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    1/7/2020   Jaime Terrats     Initial Version
**/
public without sharing class MX_SB_SAC_IdentificarCliente {
    /** string to concatenate */
    final static String DOB = ' R = Fecha de Nacimiento: ';
    /** string to validate custom field mx_sb_sac_aplicar_mala_venta__c */
    final static String MV_NO = 'No';
    /** string to validate custom field mx_sb_Sac_aplicar_mala_venta__c */
    final static String MV_YES = 'Si';
    /** string to validate if 1500 Vida */
    final static String VID15 = 'Vida';
    /** constructor */
    private MX_SB_SAC_IdentificarCliente() {}

    /**
    * @description
    * @author Jaime Terrats | 1/7/2020
    * @param recId
    * @return sObject
    **/
    @AuraEnabled(cacheable=true)
    public static Map<String, SObject> initialData(String recId, String userId) {
        try {
            final String caseFields =  'Id, CaseNumber, Origin, Reason, Description, CreatedDate, MX_SB_SAC_Nombre__c, MX_SB_SAC_ApellidoPaternoCliente__c,'
                    + 'MX_SB_SAC_ApellidoMaternoCliente__c, MX_SB_1500_ExecutiveGivenName__c, MX_SB_1500_ExecutiveSureName__c, MX_SB_SAC_Aplicar_Mala_Venta__c,'
                    + 'MX_SB_1500_is1500__c, ProductId, MX_SB_SAC_MetodoVta__c,MX_SB_SAC_EsCliente__c, MX_SB_SAC_Ramo__c';
            final Set<Id> recordId = new Set<Id>{recId};
            final List<Case> getCase = MX_RTL_Case_Service.getCaseData(caseFields, recordId, 'false');
            getCase[0].MX_SB_SAC_Aplicar_Mala_Venta__c = String.isNotBlank(getCase[0].MX_SB_SAC_Aplicar_Mala_Venta__c) ? getCase[0].MX_SB_SAC_Aplicar_Mala_Venta__c : 'No';
            final User getUser = [select Id, Name, Department from User where Id =: userId];
            final Map<String, SObject> returnedValues = new Map<String, SObject>();
            returnedValues.put('case', getCase[0]);
            returnedValues.put('user', getUser);
            return returnedValues;
        } catch(QueryException qEx) {
            throw new AuraHandledException(System.Label.MX_WB_LG_ErrorBack + qEx);
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 1/10/2020
    * @return List<SObject>
    **/
    @AuraEnabled(cacheable=true)
    public static List<SObject> retrieveRecords(String keyword, String option) {
        try {
            final List<Contract> getContracts = new List<Contract>();
            final String likeStatement = '%'+keyword+'%';
            if(String.isNotBlank(likeStatement) && String.isNotBlank(keyword)) {
                switch on option {
                    when 'contratante' {
                        getContracts.addAll([Select Id, AccountId, Account.PersonEmail, MX_SB_SAC_NumeroPoliza__c, MX_SB_SAC_NombreCuentaText__c,
                                MX_SB_SAC_RFCContratanteText__c, MX_SB_SAC_RFCAsegurado__c,
                                MX_SB_SAC_NombreClienteAseguradoText__c, MX_WB_Producto__r.Name,
                                MX_WB_Producto__r.MX_WB_FamiliaProductos__r.Name, MX_SB_SB_Placas__c
                        from Contract where MX_SB_SAC_NombreCuentaText__c like: likeStatement or
                        MX_SB_SAC_RFCContratanteText__c like: likeStatement or MX_SB_SB_Placas__c like: likeStatement
                        or MX_SB_SAC_NumeroPoliza__c =: keyword or MX_SB_SAC_NumeroSerie__c =: keyword]);

                    }
                    when 'asegurado' {
                        getContracts.addAll([Select Id, AccountId, Account.PersonEmail, MX_SB_SAC_NumeroPoliza__c, MX_SB_SAC_NombreCuentaText__c,
                                MX_SB_SAC_RFCContratanteText__c, MX_SB_SAC_RFCAsegurado__c,
                                MX_SB_SAC_NombreClienteAseguradoText__c, MX_WB_Producto__r.Name,
                                MX_SB_SB_Placas__c, MX_WB_Producto__r.MX_WB_FamiliaProductos__r.Name
                        from Contract where MX_SB_SAC_NombreClienteAseguradoText__c like: likeStatement
                        or MX_SB_SAC_RFCAsegurado__c like: likeStatement or MX_SB_SB_Placas__c like: likeStatement
                        or MX_SB_SAC_NumeroPoliza__c =: keyword or MX_SB_SAC_NumeroSerie__c =: keyword]);
                    }
                }
            }
            return getContracts;
        } catch(QueryException qEx) {
            throw new AuraHandledException(System.Label.MX_WB_LG_ErrorBack + qEx);
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 1/14/2020
    * @param contractId
    * @param prod
    * @return List<String>
    **/
    @AuraEnabled(cacheable=true)
    public static List<String> retrieveQuestionnaire(String contractId, String prod, Case caseData) {
        try {
            final List<String> questions = new List<String>();
            final Contract contractData = [Select Id, MX_WB_Producto__r.MX_WB_FamiliaProductos__r.Name, MX_WB_subMarca__c,
                    MX_WB_Marca__c, MX_SB_SAC_TipoPago__c, MX_SB_SAC_FechaNacimiento__c,
                    MX_SB_SAC_RFCContratanteText__c, MX_SB_SAC_RFCAsegurado__c, BillingAddress,
                    MX_SB_SAC_Fecha_de_Nacimiento_Asegurado__c, MX_SB_SAC_NombreClienteAseguradoText__c,
                    MX_SB_SAC_NoCuenta__c, BillingStreet, BillingState, BillingPostalCode, BillingCity,
                    MX_SB_SAC_BeneficiarioPreferente__c
            from Contract where Id =: contractId and MX_WB_Producto__r.Name =: prod];

            String type = 'SAC-DQ';
            String famProd = contractData.MX_WB_Producto__r.MX_WB_FamiliaProductos__r.Name;
            if(MV_YES.equals(caseData.MX_SB_SAC_Aplicar_Mala_Venta__c)) {
                type = 'MV-DQ';
                famProd = 'Generico';
            } else if(caseData.MX_SB_1500_is1500__c == true && contractData.MX_WB_Producto__r.MX_WB_FamiliaProductos__r.Name == VID15) {
                type = 'SAC-1500';
                famProd = '1500 Vida';
            }

            final List<MX_SB_VTS_Generica__c> generica = [Select Id,  MX_SB_SAC_Question__c, MX_SB_SAC_Question2__c,
                    MX_SB_SAC_Question3__c, MX_SB_SAC_Question4__c,
                    MX_SB_SAC_RestoreQ__c from MX_SB_VTS_Generica__c
            where MX_SB_VTS_Type__c =: type
            and MX_SB_SAC_ProductFam__c =: famProd];
            if(generica.isEmpty() == false && MV_NO.equals(caseData.MX_SB_SAC_Aplicar_Mala_Venta__c)) {
                switch on contractData.MX_WB_Producto__r.MX_WB_FamiliaProductos__r.Name {
                    when 'ASD'  {
                        questions.addAll(validateASDQuestions(generica, contractData));
                    } when 'Vida' {
                        questions.addAll(validateVidaQuestions(generica, contractData, caseData));
                    }
                }
            } else if(!generica.isEmpty()) {
                questions.addAll(validateMVQuestions(generica, contractData));  
            }
            return questions;
        } catch(QueryException qEx) {
            throw new AuraHandledException(System.Label.MX_WB_LG_ErrorBack + qEx);
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 1/15/2020
    * @param question
    * @return List<String>
    **/
    private static List<String> validateASDQuestions(List<MX_SB_VTS_Generica__c> questionnaire, Contract contractData) {
        final List<String> questions = new List<String>();
        for(MX_SB_VTS_Generica__c gen : questionnaire) {
            if(String.isNotBlank(gen.MX_SB_SAC_Question__c)) {
                questions.add(gen.MX_SB_SAC_Question__c + ' R = Marca: ' + contractData.MX_WB_Marca__c
                        + ' Submarca: ' + contractData.MX_WB_subMarca__c);
            }
            if(String.isNotBlank(gen.MX_SB_SAC_Question2__c)) {
                questions.add(gen.MX_SB_SAC_Question2__c + ' R = Tipo de Tarjeta: '
                        + contractData.MX_SB_SAC_TipoPago__c);
            }
            if(String.isNotBlank(gen.MX_SB_SAC_Question3__c)) {
                if(String.isNotBlank(String.valueOf(contractData.MX_SB_SAC_FechaNacimiento__c))) {
                    final Time mockTime = Time.newInstance(0,0,0,0);
                    final DateTime birthDay = DateTime.newInstance(contractData.MX_SB_SAC_FechaNacimiento__c, mockTime);
                    final String formattedDate = birthDay.format('dd/MMM/yyyy');
                    questions.add(gen.MX_SB_SAC_Question3__c + ' R = ' + contractData.MX_SB_SAC_RFCContratanteText__c  + DOB + formattedDate);
                } else {
                    questions.add(gen.MX_SB_SAC_Question3__c + ' R = ' + contractData.MX_SB_SAC_RFCContratanteText__c);
                }
            }
        }
        return questions;
    }

    /**
    * @description
    * @author Jaime Terrats | 1/15/2020
    * @param questionnaire
    * @param contractData
    * @return List<String>
    **/
    private static List<String> validateVidaQuestions(List<MX_SB_VTS_Generica__c> questionnaire, Contract contractData, Case caseData) {
        final List<String> questions = new List<String>();
        for(MX_SB_VTS_Generica__c gen : questionnaire) {
            if(String.isNotBlank(gen.MX_SB_SAC_Question__c) && caseData.MX_SB_1500_is1500__c==false) {
                questions.add(gen.MX_SB_SAC_Question__c + ' R = Nombre del Asegurado: ' + contractData.MX_SB_SAC_NombreClienteAseguradoText__c);
            }
            if(String.isNotBlank(String.valueOf(contractData.MX_SB_SAC_FechaNacimiento__c))) {
                final Time mockTime = Time.newInstance(0,0,0,0);
                final DateTime birthDay = DateTime.newInstance(contractData.MX_SB_SAC_FechaNacimiento__c, mockTime);
                final String formattedDate = birthDay.format('dd/MMM/yyyy');
                questions.add(gen.MX_SB_SAC_Question2__c + DOB + formattedDate
                        + ' ' + contractData.MX_SB_SAC_RFCAsegurado__c);
            }
            if(String.isNotBlank(gen.MX_SB_SAC_Question3__c)) {
                questions.add(gen.MX_SB_SAC_Question3__c + ' R = + Nombre del Beneficiario: ' + contractData.MX_SB_SAC_BeneficiarioPreferente__c);
            }
            if(String.isNotBlank(gen.MX_SB_SAC_Question4__c)) {
                questions.add(gen.MX_SB_SAC_Question4__c + ' R = Domicilio: ' + contractData.BillingStreet + ' ' + contractData.BillingCity + ' ' + contractData.BillingPostalCode);
            }
            if(String.isNotBlank(gen.MX_SB_SAC_RestoreQ__c)) {
                questions.add(gen.MX_SB_SAC_RestoreQ__c + ' R = Ultimos 4 digitos: ' + contractData.MX_SB_SAC_NoCuenta__c);
            }
            if(caseData.MX_SB_1500_is1500__c==true) {
                questions.add(gen.MX_SB_SAC_Question__c + ' R = Tipo de Pago: ' + contractData.MX_SB_SAC_TipoPago__c);
            }
        }
        return questions;
    }
    
    /**
    * @description
    * @author Jaime Terrats | 1/24/2020
    * @param record
    * @return MX_SB_SAC_CatalogoTipificacion
    **/
    @AuraEnabled(cacheable=true)
    public static List<MX_SB_SAC_CatalogoTipificacion__c> retrieveTipification(String recordId, String prodId, String type) {
        final List<MX_SB_SAC_CatalogoTipificacion__c> questionnaire = new List<MX_SB_SAC_CatalogoTipificacion__c>();
        if(String.isNotBlank(recordId) && String.isNotBlank(prodid) && String.isNotBlank(type)) {
            try {
                final Case caseData = [Select Reason, MX_SB_SAC_Detalle__c, MX_SB_1500_is1500__c from Case where Id =: recordId];

                final String validateSubDetail = String.isNotBlank(caseData.MX_SB_SAC_Detalle__c) ? caseData.MX_SB_SAC_Detalle__c : 'Generico';
                questionnaire.addAll([Select Id, MX_SB_SAC_ProductosParaVenta__c, MX_SB_SAC_SubRamo__c,
                        MX_SB_SAC_TipoDeRegistro__c from MX_SB_SAC_CatalogoTipificacion__c where
                MX_SB_SAC_Motivo__c =: caseData.Reason and MX_SB_SAC_Producto__c =: prodId
                and MX_SB_SAC_Detalle__c =: validateSubDetail and MX_SB_SAC_TipoDeRegistro__c =: type]);
            } catch(QueryException qEx) {
                throw new AuraHandledException(System.Label.MX_WB_LG_ErrorBack + qEx);
            }
        }
        return questionnaire;
    }

    /**
    * @description
    * @author Jaime Terrats | 1/29/2020
    * @return List<SObject>
    **/
    @AuraEnabled
    public static List<SObject> makeCallout(String keyword) {
        try {
            final list<Contract> returnContracts = new List<Contract>();
            if(String.isNotBlank(keyword)) {
                final String formattedString = '{"numeroPoliza":"' + keyword + '"}';
                final Map<String, Object> callout = MX_SB_SAC_ConsultaContrato.consultaPoliza(formattedString, 'Número Póliza');
                final Map<String, Object> dmlCallout = MX_SB_SAC_ConsultaContrato.creaModificaPoliza(keyword, callout);
                if(dmlCallout.isEmpty() == false && callout.isEmpty() == false) {
                    final Contract rtnContract = (Contract)dmlCallout.get('arrAcctCont'); //Search based on ID returned for method.
                    if (rtnContract != null) {
                        returnContracts.add([Select Id, AccountId, Account.PersonEmail, MX_SB_SAC_NumeroPoliza__c, MX_SB_SAC_NombreCuentaText__c, MX_SB_SAC_RFCContratanteText__c,
                            MX_SB_SAC_RFCAsegurado__c, MX_SB_SB_Placas__c, MX_SB_SAC_NombreClienteAseguradoText__c, MX_WB_Producto__r.Name from Contract
                            where Id =: rtnContract.Id]);
                    }
                }
            }
            return returnContracts;
        } catch(QueryException qEx) {
            throw new AuraHandledException(System.Label.MX_WB_LG_ErrorBack + qEx);
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 4/8/2020
    * @param generica
    * @param contractData
    * @return List<String>
    **/
    private static List<String> validateMVQuestions(List<MX_SB_VTS_Generica__c> generica, Contract contractData) {
        final List<String> questions = new List<String>();
        for(MX_SB_VTS_Generica__c gen : generica) {
            if(String.isNotBlank(gen.MX_SB_SAC_Question__c)) {
                questions.add(gen.MX_SB_SAC_Question__c + contractData.MX_SB_SAC_NombreClienteAseguradoText__c);
            }
            if(String.isNotBlank(gen.MX_SB_SAC_Question2__c)) {
                if(String.isNotBlank(String.valueOf(contractData.MX_SB_SAC_FechaNacimiento__c))) {
                    final Time mockTime = Time.newInstance(0,0,0,0);
                    final DateTime birthDay = DateTime.newInstance(contractData.MX_SB_SAC_FechaNacimiento__c, mockTime);
                    final String formattedDate = birthDay.format('dd/MMM/yyyy');
                    questions.add(gen.MX_SB_SAC_Question2__c + contractData.MX_SB_SAC_RFCAsegurado__c + DOB + formattedDate);
                } else {
                    questions.add(gen.MX_SB_SAC_Question2__c + contractData.MX_SB_SAC_RFCAsegurado__c);
                }
            }
            if(String.isNotBlank(gen.MX_SB_SAC_Question3__c)) {
                questions.add(gen.MX_SB_SAC_Question3__c + contractData.MX_SB_SAC_TipoPago__c);
            }
        }
        return questions;
    }

    /**
    * @description
    * @author Jaime Terrats | 6/8/2020
    * @param searchParam
    * @return Map<String, List<SObject>>
    **/
    @AuraEnabled
    public static Map<String, List<SObject>> searchContractOrAccount(String searchParam) {
        try {
            final Map<String, List<SObject>> getRecords = new Map<String, List<SObject>>();
            final List<List<SObject>>  soslSearch = [find: searchParam in all fields returning Account(FirstName, LastName, Apellido_Materno__pc, RFC__c, PersonEmail),
            Contract(Id, AccountId, Account.PersonEmail, MX_SB_SAC_NumeroPoliza__c, MX_SB_SAC_NombreCuentaText__c, MX_SB_SAC_RFCContratanteText__c,
                    MX_SB_SAC_RFCAsegurado__c,MX_SB_SAC_NombreClienteAseguradoText__c, MX_WB_Producto__r.Name,
                    MX_WB_Producto__r.MX_WB_FamiliaProductos__r.Name, MX_SB_SB_Placas__c, MX_SB_SAC_EmailAsegurado__c) limit 500];
            if(soslSearch.get(0).isEmpty() && soslSearch.get(0).isEmpty()) {
                getRecords.put('contract', makeCallout(searchParam));
            } else {
                getRecords.put('account', soslSearch.get(0));
                getRecords.put('contract', soslSearch.get(1));
            }
            return getRecords;
        } catch(QueryException aEx) {
            throw new AuraHandledException(System.Label.MX_WB_LG_ErrorBack + aEx);
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 6/9/2020
    * @param searchParam
    * @return string
    **/
    @AuraEnabled
    public static List<Product2> searchProductData(String searchParam) {
        final Set<String> prodNames = new Set<String>{searchParam};
        final Set<String> process = new Set<String>{'SAC','VTS'};
        return MX_RTL_Product2_Selector.findProsByNameProces(prodNames, process, true);
    }
}