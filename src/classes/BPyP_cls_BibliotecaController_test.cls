/*
----------------------------------------------------------------------------------------------------------------------------
|   Autor:                  Abraham Tinajero                                                                      			|
|   Proyecto:               Red BPyP                                                                                        |
|   Descripción:            Clase test de control de la clase biblioteca Controler.                                         |
|                                                                                                                           |
|   --------------------------------------------------------------------------                                              |
|   No.     Fecha               Autor                   Descripción                                                         |
|   ------  ----------  ----------------------      --------------------------                                              |
|   1.0     17-07-2016  Abraham Tinajero                  Creación                                                          |
|   1.1     01-07-2019  Monserrat Hernández  		Se cambian Id de un profile por consulta								|
-----------------------------------------------------------------------------------------------------------------------------
*/
@isTest
public with sharing class BPyP_cls_BibliotecaController_test {

    @testsetup
    static void testsetup() {
        final Id personAccountRTId = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByDeveloperName().get('MX_BPP_PersonAcc_NoClient').getRecordTypeId();
        final Account acc = new Account();
        acc.FirstName = 'Justino';
        acc.LastName = 'Flores';
        final Double ramdomNoCliente = Math.random()*10000000;
        acc.No_de_cliente__c = String.valueOf(ramdomNoCliente.round());
        acc.PersonBirthdate = System.today();
        acc.MX_Occupation__pc = 'ABARROTES';
        acc.MX_OccupationCode__pc = 'ABT';
        acc.RecordTypeId = personAccountRTId;
        insert acc;

        UtilitysDataTest_tst.crearProdForm('Colocación', 'Collares');

        final Opportunity opp = UtilitysDataTest_tst.crearOportunidad('Test 001', acc.Id, UserInfo.getUserId(), 'Colocación', 'Collares', null, 'MX_BPP_RedBpyp');
        insert opp;

        final List<Profile> prof = [SELECT Id FROM Profile WHERE Name =: Label.MX_PERFIL_SystemAdministrator LIMIT 1];

        final User usr = new User();
        usr.Username ='Nombretest@gmail.com';
        usr.LastName= 'Test';
        usr.Email= 'Nombretest@gmail.com';
        usr.Alias= 'tst1';
        usr.TimeZoneSidKey= 'America/Mexico_City';
        usr.LocaleSidKey= 'en_US';
        usr.EmailEncodingKey= 'ISO-8859-1';
        usr.ProfileId= prof.get(0).Id;
        usr.LanguageLocaleKey ='es';
        usr.VP_ls_Banca__c = 'Red BPyP';
        insert usr;

        final User usr2 = new User();
        usr2.Username ='Nombretest2@gmail.com';
        usr2.LastName= 'Tes2t';
        usr2.Email= 'Nombrete2st@gmail.com';
        usr2.Alias= 'tst2';
        usr2.TimeZoneSidKey= 'America/Mexico_City';
        usr2.LocaleSidKey= 'en_US';
        usr2.EmailEncodingKey= 'ISO-8859-1';
        usr2.ProfileId= prof.get(0).Id;
        usr2.LanguageLocaleKey ='es';
        usr2.VP_ls_Banca__c = 'Red BPyP';
        usr2.Username ='Nombretest22@gmail.com';
        usr2.CR__c = '0832' ;
        usr2.Divisi_n__c = 'NORESTE';
        insert usr2;
    }

    @isTest static void testmethodBiblioteca() {
        final BPyP_cls_BibliotecaController biblioteca = new BPyP_cls_BibliotecaController();
        final RecordType rtAccCF = [Select Id, DeveloperName from RecordType where SObjectType = 'Account' and developerName = 'BPyP_tre_noCliente'];
        final List<User> listUser = [SELECT Id, Name FROM User WHERE LastName IN ('Test','Tes2t')];

        final List<SelectOption> opciones = biblioteca.items;
        biblioteca.items = opciones;
        final String[] cadena = biblioteca.countries;
        biblioteca.countries = cadena;

        final Account acc = new Account();
        acc.Name ='Justino Flores';
        acc.BPyP_fh_FechaNacimiento__c = System.today();
        acc.BPyP_ls_Ocupacion__c = 'ABARROTES';
        acc.BPyP_ls_Clave__c = 'ABT';
        acc.RecordTypeId = rtAccCF.Id;
        acc.OwnerId = listUser[0].Id;
        insert acc;

        final Contract contrato = new Contract();
        contrato.AccountId =acc.Id;
        insert contrato;

        final ApexPages.StandardController stdCont = new ApexPages.StandardController(contrato);
        final BPyP_cls_ContractController contInstCtrl = new BPyP_cls_ContractController(stdCont);

        acc.BPyP_tx_TipoID__c = 'INE';
        update acc;

        acc.OwnerId = listUser[0].Id;
        update acc;
        //insert plan;
        Test.startTest();
        	System.assert(contInstCtrl != null, 'Constructor created');
        Test.StopTest();
    }
}