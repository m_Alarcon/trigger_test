/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpGenerarBenificiario
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
@SuppressWarnings('sf:LongVariable, sf:ShortVariable,sf:VariableDeclarationHidesAnother')
public class MX_SB_PS_wrpGenerarBenificiario {
  
	/*Public property for wrapper*/
	public technicalInformation technicalInformation {get;set;}
	/*Public property for wrapper*/
	 public string id {get;set;} 	
	/*Public property for wrapper*/
	public insuredList[] insuredList {get;set;}
	
	/*Public constructor for wrapper*/
    public MX_SB_PS_wrpGenerarBenificiario() {
       technicalInformation = new technicalInformation();
        insuredList = new insuredList[] {};
        insuredList.add(new insuredList()); 		
    }
	
	/*Public subclass for wrapper*/
	public class technicalInformation {
	  /*Public property for wrapper*/
		public String aapType {get;set;}	
	  /*Public property for wrapper*/	
		public String dateRequest {get;set;}	
	  /*Public property for wrapper*/	
		public String technicalChannel {get;set;}
	  /*Public property for wrapper*/	
		public String technicalSubChannel {get;set;}	
	  /*Public property for wrapper*/	
		public String branchOffice {get;set;}
	  /*Public property for wrapper*/	
		public String managementUnit {get;set;}
	  /*Public property for wrapper*/	
		public String user {get;set;}
	  /*Public property for wrapper*/	
		public String technicalIdSession {get;set;}	
	  /*Public property for wrapper*/	
		public String idRequest {get;set;}
	  /*Public property for wrapper*/	
		public String dateConsumerInvocation {get;set;}
      /*public constructor subclass*/
		public technicalInformation() {
		 this.aapType = '';	
		 this.dateRequest = '';
		 this.technicalChannel = '';
		 this.technicalSubChannel = '';
		 this.branchOffice = '';
		 this.managementUnit = '';
		 this.user = '';
		 this.technicalIdSession = '';
		 this.idRequest = '';
		 this.dateConsumerInvocation = '';
		}		
	}
	  /*Public subclass for wrapper*/
	public class insuredList {
	  /*Public property for wrapper*/
		 public string id {get;set;} 	
	  /*Public property for wrapper*/	
		public String entryDate {get;set;}
	  /*Public property for wrapper*/	
		public beneficiaries[] beneficiaries {get;set;}
	  /*Public subConstructor for wrapper*/	
        public insuredList() {
			  this.id = '';
			  this.entryDate = '';
              this.beneficiaries = new  beneficiaries[] {};  
			  this.beneficiaries.add(new beneficiaries());
        }
    }
	  /*Public subclass for wrapper*/
	public class beneficiaries {
	  /*Public property for wrapper*/
		public person person {get;set;}
	  /*Public property for wrapper*/	
		public relationship relationship {get;set;}
	  /*Public property for wrapper*/	
		public String percentageParticipation {get;set;}	
	  /*Public property for wrapper*/	
		public beneficiaryType beneficiaryType {get;set;}
		 /*public constructor subclass*/
		public beneficiaries() {
		  this.person =  new person();
		  this.relationship =  new relationship();	
		  this.percentageParticipation = '';
		  this.beneficiaryType = new beneficiaryType();
		}			
	}
	  /*Public subclass for wrapper*/
	public class person {
	  /*Public property for wrapper*/
		public String lastName {get;set;}
	  /*Public property for wrapper*/	
		public String secondLastName {get;set;}
	  /*Public property for wrapper*/	
		public String firstName {get;set;}
       /*public constructor subclass*/
		public person() {
		 this.lastName = '';
		 this.secondLastName = '';
		 this.firstName = '';
		}					
	}
	  /*Public subclass for wrapper*/
	public class relationship {
	  /*Public property for wrapper*/
		 public string id {get;set;} 
       /*public constructor subclass*/
		public relationship() {
		 this.id = '';
		}					
	}
	  /*Public subclass for wrapper*/
	public class beneficiaryType {
	  /*Public property for wrapper*/
		 public string id {get;set;} 
         /*public constructor subclass*/
		public beneficiaryType() {
		 this.id = '';
		}					
	}

}