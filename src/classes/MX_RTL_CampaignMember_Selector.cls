/**
* @File Name          : MX_RTL_CampaignMember_Selector.cls
* @Description        :
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 01/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      01/06/2020            Gabriel Garcia Rojas          Initial Version
* 1.1      21/10/2020            Gerardo Mendoza Aguilar       Se agrego methodo getIdRecordTypeParam, updCampMemberList
**/
public without sharing class MX_RTL_CampaignMember_Selector {

    /**Constructor */
    @testVisible
    private MX_RTL_CampaignMember_Selector() { }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param queryfields
    * @param queryfilters
    * @return List<CampaignMember>
    **/
    public static List<CampaignMember> getListCampaignMemberByDatabase(String queryfields, String queryFilters) {
        return Database.query('SELECT ' + String.escapeSingleQuotes(queryfields) + ' FROM CampaignMember ' + (String.escapeSingleQuotes(queryFilters)).unescapeJava() + ' ORDER BY CreatedDate DESC LIMIT 1000');
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param queryfields
    * @param queryfilters
    * @return List<CampaignMember>
    **/
    public static List<CampaignMember> getListCampaignMemberByDatabase(String queryfields, String queryFilters, List<User> listUsers) {
        return Database.query('SELECT ' + String.escapeSingleQuotes(queryfields) + ' FROM CampaignMember ' + (String.escapeSingleQuotes(queryFilters)).unescapeJava() + ' ORDER BY CreatedDate DESC LIMIT 1000');
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param idsLeads
    * @return List<CampaignMember>
    **/
    public static List<CampaignMember> getListCampignMembersByLeadIds(List<Id> idsLeads) {
        return [SELECT Id, LeadId, Lead.isConverted, Lead.Status, Lead.MX_WB_RCuenta__c, Lead.MX_WB_RCuenta__r.Name, Lead.RecordTypeId, CampaignId,
                            Campaign.MX_WB_Producto__c, Campaign.MX_WB_Producto__r.Name, Lead.Name, Lead.MX_LeadAmount__c, Lead.MX_LeadEndDate__c, Lead.Description, Lead.MX_SubProductoCampaing__c, Lead.MX_DetalleSubProductoCampaing__c,
                            Lead.LeadSource, Lead.OwnerId, Lead.Owner.Name, Campaign.MX_WB_FamiliaProductos__c, Campaign.MX_WB_FamiliaProductos__r.Name, MX_LeadEndDate__c, MX_LeadStartDate__c, MX_SuccesAmount__c, Status FROM CampaignMember WHERE LeadId =: idsLeads];
    }

    /**
    * @description
    * @author Jair Ignacio Gonzalez Gayosso
    * @param String queryfields, List<Id> idsCampaigMembers
    * @return List<CampaignMember>
    **/
    public static List<CampaignMember> getLeadOpportunityInfoByIds(String queryfields, List<Id> idsCampaigMembers) {
        return Database.query('SELECT ' + String.escapeSingleQuotes(queryfields) + ' FROM CampaignMember WHERE Id in :idsCampaigMembers ORDER BY CreatedDate DESC LIMIT 1000');
    }
/**
* @description
* @author Gerardo Mendoza Aguilar
* @param String DeveloperName
* @return Id
**/  
    public static Id getIdRecordTypeParam(String developerName) {
        return [SELECT id from RecordType where DeveloperName= :developerName].Id;
    }
    
/**
* @description
* @author Gerardo Mendoza Aguilar
* @param List<CampaignMember> upsertObj
* @return
**/
    public static void updCampMemberList(List<CampaignMember> upsertObj) {
        if(!upsertObj.isEmpty()) {
            update upsertObj;
        }
    }
}