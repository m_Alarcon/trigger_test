/**
* ---------------------------------------------------------------------------------
* @Autor: Rodrigo Amador Martinez Pacheco
* @Proyecto: Auditoria
* @Descripción : Selector para consumo de CustomMetadata MX_RTL_Maximun_File_Size__mdt
* ---------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ---------------------------------------------------------------------------------
* 1.0           18/06/2020     Rodrigo Martinez               Creación de la clase
* ---------------------------------------------------------------------------------
*/

@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_RTL_Maximun_File_Size_selector {

/**
* 
* @Descripción : Retorna el tamaño máximo de un archivo que será cargado
* @author Rodrigo Martinez
*/
    public static Decimal selectMaximumFileSizeAllowed() {
        final List<MX_RTL_Maximun_File_Size__mdt> maximum= [SELECT Maximum_File_Size__c from MX_RTL_Maximun_File_Size__mdt LIMIT 1];
        return maximum.get(0).Maximum_File_Size__c;
    }
}