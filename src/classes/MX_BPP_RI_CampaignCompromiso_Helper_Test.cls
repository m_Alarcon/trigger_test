/**
 * @File Name          : MX_BPP_RI_CampaignCompromiso_Helper_Test.cls
 * @Description        : Clase test para la clase Helper de MX_BPP_RI_CampaignCompromiso_Helper
 * @author             : Hugo Ivan Carrillo Béjar
 * @Group              : BPyP
 * @Last Modified By   : Hugo Ivan Carrillo Béjar
 * @Last Modified On   : 19/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      19/06/2020           Hugo Ivan Carrillo Béjar   Initial Version
**/
@isTest
public class MX_BPP_RI_CampaignCompromiso_Helper_Test {

    /*Product string variable*/
    private static String product = 'Collares';
	/*Family string variable*/
    private static String family = 'Colocación';
    /*variable usuarioAdmin */
	private static User usuarioAdmin;
    /*variable usuarioAdmin */
	private static String pruebaAdm = 'PruebaAdm';
    /*variable usuarioAdmin */
	private static String roleBBVAAdmin = 'BBVA ADMINISTRADOR';
    /** name variables for records */
    final static String NAME = 'Campaing Member Test';

    @TestSetup
    private static void testSetUpData() {

        final MX_WB_FamiliaProducto__c familia = new MX_WB_FamiliaProducto__c(Name = 'Colocación');
        insert familia;

        final Product2 producto = new Product2(Name = 'Collares');
        insert producto;

    }

    @isTest
    private static void testConstructor() {
        final MX_BPP_RI_CampaignCompromiso_Helper instanceC = new MX_BPP_RI_CampaignCompromiso_Helper();
        System.assertNotEquals(instanceC, null, 'No Existe coincidencia');
    }

	@isTest
    static void testGetLeadToCompList() {
        usuarioAdmin = UtilitysDataTest_tst.crearUsuario(pruebaAdm, Label.MX_PERFIL_SystemAdministrator, roleBBVAAdmin);
        insert usuarioAdmin;
        final MX_WB_FamiliaProducto__c famID = [SELECT Id FROM MX_WB_FamiliaProducto__c WHERE Name =: family];
        final Product2 prodID = [SELECT Id FROM Product2 WHERE Name =: product];

        System.runAs(usuarioAdmin) {

            final Lead candidato = new Lead(LastName = NAME,
                                           IsConverted = false,
                                           MX_LeadEndDate__c = Date.today());
        	insert candidato;

        	final Campaign campanya = new Campaign(Name = NAME,
                                                  MX_WB_FamiliaProductos__c = famID.id,
                                                  MX_WB_Producto__c = prodID.id);
        	insert campanya;

        	final CampaignMember campMem = new CampaignMember(CampaignId = campanya.Id, Status='Sent', LeadId = candidato.Id, MX_LeadEndDate__c = Date.today());
        	insert campMem;
            test.StartTest();
            MX_BPP_AccAndOppDataFactory.generaCompromisos();
            final EU001_RI__c testRI = [SELECT Id FROM EU001_RI__c WHERE OwnerId =:UserInfo.getUserId() Limit 1];
            final String fInicio = String.valueOf(EU001_cls_CompHandler.getPrimerDiaTrim(Date.today()));
            final String fFin = String.valueOf(EU001_cls_CompHandler.getUltimoDiaTrim(Date.today()));
            final String[] leadParams = new String[] {testRI.Id, usuarioAdmin.Id, fInicio, fFin, family, product};
            MX_BPP_RI_CampaignCompromiso_Helper.getLeadToCompList(leadParams);
            System.assert(true, 'Assert ');
            Test.stopTest();
        }
    }
}