@isTest
/**
* @FileName          : MX_SB_VTS_wrpInsuredAmount_Test
* @description       : Test class from MX_SB_VTS_wrpInsuredAmount_Utils
* @Author            : Marco Antonio Cruz Barboza
* @last modified on  : 12-01-2021
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   12-01-2021   Marco Antonio Cruz Barboza          Initial Version
**/
public class MX_SB_VTS_wrpInsuredAmount_Test {

    /** Username for an apex test */
    final static String NOMBREUSR = 'UserTest';
    /** Account Name for an apex test*/
    final static String CNTATST = 'Test Account';
    
    @TestSetup
    static void setupTest() {
        final User tstUsuario = MX_WB_TestData_cls.crearUsuario(NOMBREUSR, System.Label.MX_SB_VTS_ProfileAdmin);
        System.runAs(tstUsuario) {
            final Account tstCuenta = MX_WB_TestData_cls.crearCuenta(CNTATST, System.Label.MX_SB_VTS_PersonRecord);
            tstCuenta.PersonEmail = 'pruebaVts@mxvts.com';
            insert tstCuenta;
        }
    }
    
    /*
    @Method: wrpInsuredAmountTest
    @Description: Use a wrapper class to deserialize a response from a web service.
	@param
    */
    @isTest
    static void wrpInsuredAmountTest() {
        test.startTest();
        	Final MX_SB_VTS_wrpInsuredAmount_Utils.suggestedAmount suggestAmo = new MX_SB_VTS_wrpInsuredAmount_Utils.suggestedAmount();
        	Final MX_SB_VTS_wrpInsuredAmount_Utils.suggestedCoverages suggestCov = new MX_SB_VTS_wrpInsuredAmount_Utils.suggestedCoverages();
        	Final MX_SB_VTS_wrpInsuredAmount_Utils.suggestedAmount[] suggestLst = new MX_SB_VTS_wrpInsuredAmount_Utils.suggestedAmount[]{};
            Final MX_SB_VTS_wrpInsuredAmount_Utils.suggestedCoverages[] coverageLst = new MX_SB_VTS_wrpInsuredAmount_Utils.suggestedCoverages[]{};
            Final MX_SB_VTS_wrpInsuredAmount_Utils gnlAmount = new MX_SB_VTS_wrpInsuredAmount_Utils();
        		suggestAmo.amount = '23000';
        		suggestAmo.currencies = 'MX';
        		suggestCov.id = '02';
        		suggestCov.suggestedAmount = suggestAmo;
        		suggestLst.add(suggestAmo);
        		coverageLst.add(suggestCov);
        		gnlAmount.suggestedAmount = suggestAmo;
        		gnlAmount.suggestedCoverages = coverageLst;
        	System.assertNotEquals(gnlAmount.suggestedCoverages, null,'El wrapper esta retornando');
        test.stopTest();
    }
    
}