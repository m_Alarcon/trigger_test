/**
* Name: MX_RTL_Group_Selector
* @author Jose angel Guerrero
* Description : Nuevo selector de clase para la lista de grupos
* Ver                  Date            Author                   Description
* @version 1.0         Jul/13/2020     Jose angel Guerrero      Initial Version
**/
/**
* @description: funcion que trae lista de id
*/
@SuppressWarnings('sf:UseSingleton, sf:DMLWithoutSharingEnabled')
public class MX_RTL_GroupMember_Selector {
    /**
* @description: funcion que trae lista de id
*/
    public static List< GroupMember > getMembersByIds(List <String> listaDeId,String prefijo) {
        return [SELECT UserOrGroupId, GroupId from GroupMember where UserOrGroupId
                IN :listaDeId AND GroupId IN(SELECT Id FROM Group WHERE Type='Queue'
                                             AND DeveloperName LIKE :prefijo ) LIMIT 500];
    }
    /**
* @description: funcion que trae id del usuario
*/
    public static List< GroupMember > getMemberByGroupAndID(String idUsuario,String idCola) {
        return [SELECT  UserOrGroupId,GroupId from GroupMember where UserOrGroupId = :idUsuario AND
                GroupId = :idCola LIMIT 1];
    }
    /**
* @description: funcion que trae id del usuario
*/
    public static List< GroupMember > getMemberByGroupId(String colaBuscar) {
        return [SELECT UserOrGroupId,GroupId,Id from GroupMember where GroupId = :colaBuscar LIMIT 300];
    }
    /**
* @description: funcion que trae id del usuario
*/
    public static List< GroupMember > getMemberByNueAsig(List <String> userIds, String origen, String destino) {
        return [Select ID from GroupMember where GroupId = :origen AND UserOrGroupId IN :userIds];
    }
}