/**
* @File Name          : MX_BPP_LeadCampaignTable_Helper_Test.cls
* @Description        : Test class for MX_BPP_LeadCampaignTable_Helper
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 01/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      01/06/2020            Gabriel Garcia Rojas          Initial Version
**/
@isTest
private class MX_BPP_LeadCampaignTable_Helper_Test {

    /*Usuario de pruebas*/
    private static User testUser = new User();

    /** namerole variable for records */
    final static String NAMEROLE = '%BPYP BANQUERO BANCA PERISUR%';
    /** nameprofile variable for records */
    final static String NAMEPROFILE = 'BPyP Estandar';
    /** namesucursal variable for records */
    final static String NAMESUCURSAL = '6343 PEDREGAL';
    /** namedivision variable for records */
    final static String NAMEDIVISION = 'METROPOLITANA';
    /** name variable for records */
    final static String NAME = 'testUser';
    /** error message */
    final static String MESSAGEFAIL = 'Fail method';

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testUser = UtilitysDataTest_tst.crearUsuario(NAME, NAMEPROFILE, NAMEROLE);
        insert testUser;
    }

	/**
     * @description constructor test
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void testConstructor() {
        final MX_BPP_LeadCampaignTable_Helper instanceC = new MX_BPP_LeadCampaignTable_Helper();
        System.assertNotEquals(instanceC, null, MESSAGEFAIL);
    }

    /**
     * @description
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void getListUderByUserAndRoleSuccessTest() {
        final Set<Id> staffRole = MX_RTL_UserRole_Selector.getUserRoleLikeName('%BPYP DIRECTOR OFICINA %').keySet();
        final Set<Id> bankerRole = MX_RTL_UserRole_Selector.getUserRoleLikeName('%BPYP BANQUERO %').keySet();
		final List<Id> listIds = new List<Id>(staffRole);

        Test.startTest();
        final List<User> listUser = MX_BPP_LeadCampaignTable_Helper.getListUderByUserAndRole(staffRole, bankerRole, listIds[0], new List<User>{testUser});
        Test.stopTest();

        System.assertNotEquals(listUser.size(), 0 , MESSAGEFAIL);
    }

    /**
     * @description
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void getListUderByUserAndRoleFailTest() {
        final Set<Id> staffRole2 = MX_RTL_UserRole_Selector.getUserRoleLikeName('%BPYP DIRECTOR OFICINA %').keySet();
        final Set<Id> bankerRole2 = MX_RTL_UserRole_Selector.getUserRoleLikeName('%BPYP BANQUERO %').keySet();
        final List<Id> listIdsBan2 = new List<Id>(bankerRole2);

        Test.startTest();
        final List<User> listUser2 = MX_BPP_LeadCampaignTable_Helper.getListUderByUserAndRole(staffRole2, bankerRole2, listIdsBan2[0], new List<User>{testUser});
        Test.stopTest();

        System.assertNotEquals(listUser2.size(), 0 , MESSAGEFAIL);
    }

    /**
     * @description
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void setFilterToCampaignMemberTest() {
        final List<String> paramList = new List<String>{'CuentaTest', 'Activo'};
        final List<String> filterList = new List<String>{'Lead.MX_WB_RCuenta__r.Name LIKE \'%CuentaTest%\'' , 'Lead.Status = \'Activo\''};

        Test.startTest();
        final String filtros = MX_BPP_LeadCampaignTable_Helper.setFilterToCampaignMember(paramList, filterList);
        Test.stopTest();
        System.assertEquals(filtros, ' WHERE Lead.MX_WB_RCuenta__r.Name LIKE \'%CuentaTest%\' AND Lead.Status = \'Activo\'' , MESSAGEFAIL);
    }

    /**
     * @description Test to method getOppPickListProductValue
     * @author Gabriel Garcia | 10/06/2020
     * @return void
     **/
    @isTest
    private static void dependentValuesGetTest() {
        Test.startTest();
        final Map<Object,List<String>> mapdependValue = MX_BPP_LeadCampaignTable_Service.getOppPickListProductValue('Opportunity.MX_RTL_Producto__c');
        Test.stopTest();
        System.assertNotEquals(mapdependValue.size(), 0, MESSAGEFAIL);
    }
}