/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RDB_AutenticacionCCDController.js
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2019-10-28
* @Group  		MX_RDB_AutenticacionCCD
* @Description 	Controller from MX_RDB_AutenticacionCCD Lightning Component
* @Changes
*  
*/
public with sharing class MX_RDB_AutenticacionCCDController {
    
    private MX_RDB_AutenticacionCCDController() {}
    /**
    * --------------------------------------------------------------------------------------
    * @Description Returns account record related to the Opportunity
    * @param oppID the record to be search
    * @return The related Account record with the fields Id, MX_SB_BCT_Id_Cliente_Banquero__c 
    **/
	@AuraEnabled
    public static Account getClienteData(String oppID) {
        try {
            return [SELECT Id, MX_SB_BCT_Id_Cliente_Banquero__c	FROM Account 
                    WHERE Id IN (SELECT AccountId FROM Opportunity WHERE Id =: oppID) LIMIT 1];
        } catch(System.QueryException e) {
            throw new AuraHandledException(System.Label.MX_SB_BCT_Error_ListException + e);
        }
    }
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description Returns the required values for the grantingTicket WebService
    * @return The MX_RDB_grantingTicket__mdt record with all the fields
    **/
    @AuraEnabled
    public static MX_RDB_grantingTicket__mdt rGTicket() {
        return [SELECT ID, Label, DeveloperName, MX_RDB_AccessCode__c, MX_RDB_AuthenticationData__c, MX_RDB_AuthenticationType__c, MX_RDB_ConsumerID__c, MX_RDB_DialogId__c, MX_RDB_IdAuthenticationData__c, MX_RDB_URL__c, MX_RDB_UserId__c 
                FROM MX_RDB_grantingTicket__mdt 
                WHERE DeveloperName = 'grantingTicket' LIMIT 1];
    }
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description Returns the required values for the getShortReferenceNumber WebService
    * @return The MX_RDB_shortReferenceNumber__mdt record with all the fields
    **/
    @AuraEnabled
    public static MX_RDB_shortReferenceNumber__mdt rRNumber() {
        return [SELECT ID, Label, MX_RDB_URL__c, DeveloperName, MX_RDB_service__c, MX_RDB_segment__c, MX_RDB_customerType__c, MX_RDB_reference__c, MX_RDB_referenceNumber__c
                FROM MX_RDB_shortReferenceNumber__mdt 
                WHERE DeveloperName = 'shortReferenceNumber' LIMIT 1];
    }
}