/**
* @File Name          : MX_BPP_Oportunidad_Ctrl_Test.cls
* @Description        : Test class for MX_BPP_Oportunidad_Ctrl
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 21/10/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      21/10/2020            Gabriel Garcia Rojas          Initial Version
**/
@isTest
private class MX_BPP_Oportunidad_Ctrl_Test {
	/*Usuario de pruebas*/
    private static User testUserStartOCtr = new User();
    /** namerole variable for records */
    final static String NAME_ROLECTRLO = '%BPYP BANQUERO BANCA PERISUR%';
    /** nameprofile variable for records */
    final static String NAME_PROFILESOCTR = 'BPyP Estandar';
    /** name variable for records */
    final static String NAMESTCTRLO = 'testUserOpp';
    /** name variable for records */
    final static String NAMECOT_OFFCTRLO = '6343 PEDREGAL';
    /** namedivision variable for records */
    final static String NAMECOT_DIVCTRLO = 'METROPOLITANA';
    /** name variable for oficina */
    final static String PRIVCTRLO ='PRIVADO';
    /** error message */
    final static String MESSFAILTEST = 'Fail method';
    /** String Cuentas */
    final static String CUENTASOCTRL = 'Cuentas';
    /** String Todas */
    final static String TODASCTRLO = 'TODAS';



    /*Setup para clase de prueba*/
    @testSetup
    static void setupOppService() {

        testUserStartOCtr = UtilitysDataTest_tst.crearUsuario(NAMESTCTRLO, NAME_PROFILESOCTR, NAME_ROLECTRLO);
        testUserStartOCtr.Title = PRIVCTRLO;
        testUserStartOCtr.Divisi_n__c = NAMECOT_DIVCTRLO;
        testUserStartOCtr.BPyP_ls_NombreSucursal__c = NAMECOT_OFFCTRLO;
        testUserStartOCtr.VP_ls_Banca__c = 'Red BPyP';
        insert testUserStartOCtr;

        final Id rtAccCtrl = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta BPyP').getRecordTypeId();
        final Id rtOppCtrl = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('BPyP').getRecordTypeId();

        final Account cuentaOCtrl = new Account(FirstName = 'CuentaTest', LastName = 'LastTest', No_de_cliente__c = '12345678', RecordTypeId = rtAccCtrl, OwnerId = testUserStartOCtr.Id );
        insert cuentaOCtrl;

        final Date fechaOppCtrl = System.today();

        System.runAs(testUserStartOCtr) {
            final Opportunity oppTestCtrl = new Opportunity();
            oppTestCtrl.Name = 'OppTestCtrl';
            oppTestCtrl.MX_RTL_Familia__c = 'Captación';
            oppTestCtrl.MX_RTL_Producto__c = 'Cuenta en Dólares';
            oppTestCtrl.op_amountPivote_dv__c = 100000;
            oppTestCtrl.AccountId = cuentaOCtrl.Id;
            oppTestCtrl.RecordTypeId = rtOppCtrl;
            oppTestCtrl.StageName = 'Abierta';
        	oppTestCtrl.CloseDate = fechaOppCtrl.addDays(30);

            insert oppTestCtrl;
        }
    }

    /**
    * @description constructor test
    * @author Gabriel Garcia | 21/10/2020
    * @return void
    **/
    @isTest
    private static void testConstructorCtrlOpp() {
        final MX_BPP_Oportunidad_Ctrl instTestOppSer = new MX_BPP_Oportunidad_Ctrl();
        System.assertNotEquals(instTestOppSer, null, MESSFAILTEST);
    }

    /**
     * @description test fetchDataOpp
     * @author Gabriel Garcia | 21/010/2020
     * @return void
     **/
    @isTest
    private static void fetchDataOppTest() {
        Test.startTest();
        final MX_BPP_Oportunidad_Service.WRP_ChartStacked wrpCharTestCtrl = MX_BPP_Oportunidad_Ctrl.fetchDataOpp(new List<String>{CUENTASOCTRL, TODASCTRLO, 'Division', ''}, null, null);
        System.assertNotEquals(wrpCharTestCtrl, null, MESSFAILTEST);

        Test.stopTest();
    }

    /**
     * @description test fetchOpp
     * @author Gabriel Garcia | 21/010/2020
     * @return void
     **/
    @isTest
    private static void fetchOppTest() {
        Test.startTest();
        final List<Opportunity> listOppCtrlTest = MX_BPP_Oportunidad_Ctrl.fetchOpp(new List<String>{NAMESTCTRLO, CUENTASOCTRL, TODASCTRLO, '10'}, null, null);
        System.assertNotEquals(listOppCtrlTest, null, MESSFAILTEST);

        Test.stopTest();
    }

    /**
     * @description test fetchPgs
     * @author Gabriel Garcia | 21/010/2020
     * @return void
     **/
    @isTest
    private static void fetchPgs() {
        Test.startTest();
        final Integer totalpage = MX_BPP_Oportunidad_Ctrl.fetchPgs(20);
        System.assertNotEquals(totalpage, 0, MESSFAILTEST);

        Test.stopTest();
    }

    /**
     * @description test fetchInfoByUser
     * @author Gabriel Garcia | 21/10/2020
     * @return void
     **/
    @isTest
    private static void fetchInfoByUserTest() {
        final User usuarioCtrlO = [SELECT Id FROM User WHERE Name=: NAMESTCTRLO];
        System.runAs(usuarioCtrlO) {
            final MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper wrapperCtrlOpp = MX_BPP_Oportunidad_Ctrl.fetchInfoByUser();
            System.assertNotEquals(wrapperCtrlOpp, null, MESSFAILTEST);
        }
    }

    /**
     * @description test fetchUserInfo
     * @author Gabriel Garcia | 21/10/2020
     * @return void
     **/
    @isTest
    private static void fetchUserInfoTestC() {
        final List<String> listinfoUserCC = MX_BPP_Oportunidad_Ctrl.fetchUserInfo();
        System.assertNotEquals(listinfoUserCC.size(), 0 , MESSFAILTEST);
    }

}