/**
 * @File Name          : MX_SB_VTS_GetSetDatosDCDPFrm_Service.cls
 * @Description        : Formulario - Datos del Contratante / Propiedad
 * @Author             : Alexandro Corzo
 * @Group              : 
 * @Last Modified By   : Diego Olvera
 * @Last Modified On   : 03-01-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0       17/11/2020      Alexandro Corzo        Initial Version
 * 1.1       08/02/2021      Alexandro Corzo        Se realizan ajustes a clase para Codesmells
 * 1.2       23/02/2021      Diego Olvera           Se agrega funcion para guardado/actualizacion
 * 1.3       04/03/2021      Alexandro Corzo        Se realizan ajustes a la clase bug reportado
**/
@SuppressWarnings('sf:UseSingleton')
public class MX_SB_VTS_GetSetDatosDCDPFrm_Service {
    /** Variable de Apoyo: OKCODE */
    private static final Integer OKCODE = 200;
    /** Variable de Apoyo: CODE204 */
    private static final Integer CODE204 = 204;
    /** Variable de Apoyo: SRVNOTAVAL */
    private static final Integer SRVNOTAVAL = 503;
    /** Variable de Apoyo: FLDENVFPOL */
    private static final String FLDENVFPOL = 'EnvFisPoliz';
    
    /**
     * @description : Almacena los Datos de los Formulario: Datos del Contratante
     * 				: en SF y de Dirección Pertenencia. Para posteriormente realizar
     * 				: el envio a Clippert.
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    public static Map<String, Object> setDataFormDCDPSrv(Map<String, Object> mDataObjDCDP) {
        final Map<Object, Object> mDataObjDC = (Map<Object,Object>) mDataObjDCDP.get('objDataFormDC');
        final Map<Object, Object> mDataObjDP = (Map<Object,Object>) mDataObjDCDP.get('objDataFormDP');
        final String strIdQuoteSrv = obtQuoteIdOppo(mDataObjDCDP.get('strOppoId').toString());
        final Map<String, Object> oResponseValues = new Map<String, Object>();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.iCatalogItem objWrapper = new MX_SB_VTS_wrpCreateCustomerDataSrv.iCatalogItem();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.shippingChannel objWrapperShipC = new MX_SB_VTS_wrpCreateCustomerDataSrv.shippingChannel();
        String strShipChannel = null;
        final MX_SB_VTS_wrpCreateCustomerDataSrv.header objWrapperHeader = MX_SB_VTS_GetSetDatosDCDPFrm_Helper.fillDataHeader();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.quote objWrapperQuote = MX_SB_VTS_GetSetDatosDCDPFrm_Helper.fillDataQuote(strIdQuoteSrv);
        final MX_SB_VTS_wrpCreateCustomerDataSrv.clientType objWrapperCType = MX_SB_VTS_GetSetDatosDCDPFrm_Helper.fillDataClientType();
        final MX_SB_VTS_wrpCreateCustomerDataSrv.holder objWrapperHolder = MX_SB_VTS_GetSetDatosDCDPFrm_Helper.fillDataHolder(mDataObjDC, mDataObjDP);
        final Boolean bEnvPolizDC = mDataObjDC.get(FLDENVFPOL) == null ? false : Boolean.valueOf(mDataObjDC.get(FLDENVFPOL));
        final Boolean bEnvPolizDP = mDataObjDP.get(FLDENVFPOL) == null ? false : Boolean.valueOf(mDataObjDP.get(FLDENVFPOL));
        if(bEnvPolizDC || bEnvPolizDP) {
        	strShipChannel = 'TRAD';
        } else {
            strShipChannel = 'EMAIL';
        }
        objWrapper.contractingHolderIndicator = true;
        objWrapper.contractor = null;
        objWrapper.authorizationDate = null;
        objWrapper.authorizationTime = null;
        objWrapper.certificateNumber = '1';
        objWrapperShipC.id = strShipChannel;
        objWrapper.shippingChannel = objWrapperShipC;
        objWrapper.updateDataIndicator = true;
        objWrapper.isForeign = 'false';
        objWrapper.header = objWrapperHeader;
        objWrapper.quote = objWrapperQuote;
        objWrapper.clientType = objWrapperCType;
        objWrapper.holder = objWrapperHolder;
        final String sWrapperJSON = String.valueOf(JSON.serialize(objWrapper));
        final Map<String, Object> oResultSrv = callInvokeService(sWrapperJSON, 'createCustomerData');
        oResponseValues.put('oResponse', oResultSrv);
        oResponseValues.put('oDataJSON', sWrapperJSON);
        if(MX_SB_VTS_GetSetDatosDCDPFrm_Helper.saveDataAddress(mDataObjDC, mDataObjDP)) {
            oResponseValues.put('oSaveDataAddr', true);
        } else {
            oResponseValues.put('oSaveDataAddr', false);
        }
        return oResponseValues;
    }
    
    /**
     * @description : Recuoera los Datos del Formulario: Dirección Pertenencia / Contratante
     * 				: en SF. Para posteriormente realizar el envio a Clippert.
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    public static Map<String, Object> setDataCompSrv(Map<String, Object> mDataObjDCDP) {
        final Map<Object, Object> mDataObjDP = (Map<Object,Object>) mDataObjDCDP.get('objDataFormDP');
        final Map<String, Object> oResponseValues = new Map<String, Object>();
        final Boolean bEnvPolizDP = mDataObjDP.get(FLDENVFPOL) == null ? false : Boolean.valueOf(mDataObjDP.get(FLDENVFPOL));
        if(bEnvPolizDP) {
            final String strOppId = mDataObjDCDP.get('strOppoId').toString();
            final String strIdQuoteSrv = obtQuoteIdOppo(strOppId);
            final String sWrapperJSONPF = obtAddrPolizFis(strOppId, strIdQuoteSrv);
            final String sWraJSONPFRep = sWrapperJSONPF.replaceAll('id_data', 'id');
            final Map<String, Object> oResultSrvPF = callInvokeService(sWraJSONPFRep, 'modifyHomePolicyTemp');
            oResponseValues.put('oResponse', oResultSrvPF);
            oResponseValues.put('oDataJSON', sWrapperJSONPF);
        } else {
            final Map<String, Object> mStatusCode = new Map<String, Object>{'code' => '200', 'description' => 'Consumo de Servicio Exitoso!'};
            oResponseValues.put('oResponse', mStatusCode);
            oResponseValues.put('oDataJSON', '');
        }
        return oResponseValues;
    }
    
    /**
     * @description : Realiza el consumo del servicio: createCustomerData enviando los datos
     * 				  a Clippert
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    public static Map<String, Object> callInvokeService(String sJSONSBody, String strService) {
        Map<String, Object> oReturnValues = null;
        try {
            final HttpResponse oResponseSrv = MX_RTL_IasoServicesInvoke_Selector.callServices(strService, new Map<String, Object>(), obtRequestSrv(sJSONSBody, strService));
            if(oResponseSrv.getStatusCode() == OKCODE || oResponseSrv.getStatusCode() == CODE204) {
                final Map<String, Object> mStatusCode = new Map<String, Object>{'code' => String.valueOf(oResponseSrv.getStatusCode()), 'description' => 'Consumo de Servicio Exitoso!'};
                oReturnValues = mStatusCode;
            } else {
                final Map<String, Object> mStatusCode = MX_SB_VTS_Codes_Utils.statusCodes(oResponseSrv.getStatusCode());
                oReturnValues = mStatusCode;
            }
        } catch(Exception e) {
        	final Map<String, Object> mStatusCode = new Map<String, Object>{'code' => String.valueOf(SRVNOTAVAL), 'description' => 'El servicio no se encuentra disponible, intente nuevamente!!'};
            oReturnValues = mStatusCode;
        }
        return oReturnValues;
    }
    
    /**
     * @description : Realiza el armado del objeto Request para el consumo del servicio ASO
     * @author      : Alexandro Corzo
     * @return      : HttpRequest oRequest
     */
    public static HttpRequest obtRequestSrv(String sDatosPartCli, String strService) {
        final Map<String,iaso__GBL_Rest_Services_Url__c> allCodASODPC = iaso__GBL_Rest_Services_Url__c.getAll();
        final iaso__GBL_Rest_Services_Url__c objASOCPDPC = allCodASODPC.get(strService);
        final String strEndPntDPC = objASOCPDPC.iaso__Url__c;
        final HttpRequest oRqstDPC = new HttpRequest();
        oRqstDPC.setTimeout(120000);
        oRqstDPC.setMethod('POST');
        oRqstDPC.setHeader('Content-Type', 'application/json');
        oRqstDPC.setHeader('Accept', '*/*');
        oRqstDPC.setHeader('Host', 'https://test-sf.bbva.mx');
        oRqstDPC.setEndpoint(strEndPntDPC);
        oRqstDPC.setBody(sDatosPartCli);
        return oRqstDPC;
    }
    
    /**
     * @description : Recupera el QuoteId del servicio registrado en Quote
     * @author      : Alexandro Corzo
     * @return      : String strFolioCot
     */
    public static String obtQuoteIdOppo(String strOppoId) {
        Quote objQuote = null;
    	final Opportunity objOpportunity = MX_RTL_Opportunity_Selector.getOpportunity(strOppoId, 'SyncedQuoteId');
        final String strQuoteId = objOpportunity.SyncedQuoteId;
        objQuote = MX_RTL_Quote_Selector.findQuoteById(strQuoteId, 'MX_SB_VTS_ASO_FolioCot__c');
        return objQuote.MX_SB_VTS_ASO_FolioCot__c;
    }
    
    /**
     * @description : Obtenemos JSON para envio de poliza en fisico
     * @author      : Alexandro Corzo
     * @return      : String objWrapperJSON
     */
    public static String obtAddrPolizFis(String strOppoId, String strQuoteId) {
        final List<MX_RTL_MultiAddress__c> lstMultiAddr = MX_RTL_MultiAddress_Selector.rsttQryMulAdd('Id, MX_RTL_AddressMunicipalityCode__c, MX_RTL_AddressStateCode__c, MX_RTL_AddressCityCode__c, MX_RTL_AddressCountryCode__c, MX_RTL_SendPolice__c, MX_RTL_PostalCode__c, MX_RTL_AddressStreet__c, MX_RTL_AddressExtNumber__c, MX_RTL_AddressIntNumber__c, Name', 'MX_RTL_Opportunity__c = \'' + strOppoId + '\'', true);
        final Map<String, String> mDataPolFis = new Map<String, String>();
        for(MX_RTL_MultiAddress__c oRowMulAddr : lstMultiAddr) {
            if(oRowMulAddr.MX_RTL_SendPolice__c) {
                mDataPolFis.put('quote_id', strQuoteId);
				mDataPolFis.put('county_id', String.valueOf(oRowMulAddr.MX_RTL_AddressCountryCode__c));
				mDataPolFis.put('state_id', String.valueOf(oRowMulAddr.MX_RTL_AddressStateCode__c));
				mDataPolFis.put('city_id', String.valueOf(oRowMulAddr.MX_RTL_AddressCityCode__c));
				mDataPolFis.put('township_id', String.valueOf(oRowMulAddr.MX_RTL_AddressMunicipalityCode__c));
				mDataPolFis.put('neighborhood_id', String.valueOf(oRowMulAddr.MX_RTL_AddressMunicipalityCode__c));  
                mDataPolFis.put('postal_code', String.valueOf(oRowMulAddr.MX_RTL_PostalCode__c));
                mDataPolFis.put('streetName', String.valueOf(oRowMulAddr.MX_RTL_AddressStreet__c));
                mDataPolFis.put('streetNumber', String.valueOf(oRowMulAddr.MX_RTL_AddressExtNumber__c));
                mDataPolFis.put('door', String.valueOf(oRowMulAddr.MX_RTL_AddressIntNumber__c));
                mDataPolFis.put('condominium', '');
            }
        }
        final MX_SB_VTS_wrpModHomePolTempDataSrv.base objWrapper = MX_SB_VTS_GetSetDatosPolizFisico_Helper.fillDataGen(mDataPolFis);
        return String.valueOf(JSON.serialize(objWrapper).replaceAll('iddata','id'));
    }

     /**
    * @description Función que hace guardado de registro de direccion
    * @author Diego Olvera | 17-02-2021 
    * @param Map<String, Object> dataAddSvr
    * @return void
    **/
    public static void saveAddRec(Map<String, Object> dataAddSvr) {
        final Map<Object, Object> dataObjDc = (Map<Object,Object>) dataAddSvr.get('objDataFormDC');
        final Map<Object, Object> dataObjDp = (Map<Object,Object>) dataAddSvr.get('objDataFormDP');
        MX_SB_VTS_GetSetDatosDCDPFrm_Helper.saveDataAddress(dataObjDc, dataObjDp);
    }
}