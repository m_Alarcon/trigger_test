/**
 * @File Name          : MX_SB_VTS_GetSetPPForm_Ctrl.cls
 * @Description        : Formulario - Personaliza tu Protección
 * @Author             : Alexandro Corzo
 * @Group              :
 * @Last Modified By   : Alexandro Corzo
 * @Last Modified On   : 29/6/2020 11:05:00
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0       29/6/2020       Alexandro Corzo        Initial Version
**/
public without sharing class MX_SB_VTS_GetSetPPForm_Ctrl {
    /*
     * Constructor
     */
    @SuppressWarnings('sf:UseSingleton')
    private MX_SB_VTS_GetSetPPForm_Ctrl() {}

    /**
     * @description: Recupera las coberturas disponibles para las opciones: Completa, Balanceada y Básica
     * @author: Alexandro Corzo
     * @return: 
     */
    @AuraEnabled(cacheable=true)
    public static Map<String, List<Object>> dataCoberturas() {
        return MX_SB_VTS_GetSetPPForm_Service.dataCoberturas();
        
    }

    /**
     * @description: Recupera valores por set de Ids
     * @author Alexandro Corzo | 29/06/2020
     * @return Map<String, List<Object>> Mapa de valores recuperados de un JSON
     */
    @AuraEnabled(cacheable=true)
    public static Map<String, List<Object>> dataAmountValues() {
        return MX_SB_VTS_GetSetPPForm_Service.dataAmountVal();
    }
}