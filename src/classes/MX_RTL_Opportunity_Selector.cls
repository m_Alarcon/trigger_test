/**
* Indra
* @author           Julio Medellín Oliva
* Project:          Presuscritos
* Description:      Clase de queries para opportunity.
*
* Changes (Version)
* ------------------------------------------------------------------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    --------------------------------------
* @version  1.0     2020-05-25      Julio Medellín Oliva.         Creación de la Clase
* @version  1.1     2020-06-03     Jair Ignacio Gonzalez G.       Add insertOpportunity
* @version  1.2     2020-10-21      Gabriel Garcia R.             Add fetchAROppByFilters and fetchListOppFilters
* @version  1.3     2020-10-21      Gerardo Mendoza Aguilar       Se agrega methodo upsrtOppList
* @version  1.4     2020-10-21      Arsenio Perez Lopez       Se agrega method clonOpp
* @version  1.4.1   2020-10-21      Eduardo Hernández             Se agregan campos a la query de selectSObjectsById
*/
@supresswarnings('sf:AvoidFinalLocalVariable, sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public with sharing class MX_RTL_Opportunity_Selector { //NOSONAR

    /** list of possible filters */
    final static String SELECT_STR = 'SELECT ';

  /**
    * @Method getOpportunity
    * @param String oppid
    * @param Sring fields
    * @Description method que retorna un objeto Opportunity
    * @return Objeto User
    **/
    public static Opportunity getOpportunity(string oppid,String fields) {
        final String query = SELECT_STR + Fields+' FROM Opportunity WHERE Id =:oppid';
        return DataBase.query(String.escapeSingleQuotes(query));
    }

      /**
    * @description Recupera Información de Oportunidades por set de Ids
    * @author Eduardo Hernandez Cuamatzi | 25/5/2020
    * @param Set<String> setOppsId Set de Ids en formato String
    * @return List<Opportunity> Lista de Oportunidades encontradas
    **/
    public static List<Opportunity> selectSObjectsById(Set<String> setOppsId) {
      return [Select Id, Name, MX_WB_comentariosCierre__c, Producto__c, MX_SB_VTS_IsOppReagenda__c, MX_WB_EnvioCTICupon__c, CreatedDate, LastModifiedDate, LeadSource, SyncedQuoteId,
      Monto_de_la_oportunidad__c, PrimerPropietario__c, OwnerId, Owner.Name,Account.PersonEmail FROM Opportunity where Id IN: setOppsId];
  }

  /**
  * @description Recupera Opps bandejas
  * @author Eduardo Hernandez Cuamatzi | 28/5/2020
  * @param Set<String> setOppsId Set de Ids en formato String
  * @return List<Opportunity> Lista de Oportunidades encontradas
  **/
  public static List<Opportunity> selectSObTrayById(Set<String> setOppsId) {
      return [Select Id, EnviarCTI__c, xmlRespuesta__c, MX_SB_VTS_TrayAttention__c FROM Opportunity where Id IN: setOppsId];
  }

  /**
  * @description Recupera Opps BPyP
  * @author Jair Ignacio Gonzalez Gayosso | 05/08/2020
  * @param Set<Id> setOppsId Set de Ids de Opportunidades
  * @return List<Opportunity> Lista de Oportunidades encontradas
  **/
  public static List<Opportunity> selectBppSObById(Set<Id> setOppsId) {
    return [Select Id, StageName, op_amountPivote_dv__c, Amount, Description FROM Opportunity where Id IN: setOppsId];
  }

  /**
  * @description Ejecuta update de registros para Oportunidades
  * @author Eduardo Hernandez Cuamatzi | 3/6/2020
  * @param List<Opportunity> lstOpps  Lista de Oportunidades a procesar
  * @param Boolean allOrNone Indica si se lanza una operacion de exception
  * @return List<Database.SaveResult> Lista de resultados de la operación DML
  **/
  public static List<Database.SaveResult> updateResult(List<Opportunity> lstOpps, Boolean allOrNone) {
      return Database.update(lstOpps, allOrNone);
  }

  /**
    * @Method insertOpportunity
    * @param List<Opportunity> lsOpportunity
    * @Description Inserta una lista de Oportunidades
    * @return List<Opportunity>
    **/
    public static List<Opportunity> insertOpportunity(List<Opportunity> lsOpportunity) {
      insert lsOpportunity;
      return lsOpportunity;
    }
      /**
    * @Method upsrtOpp
    * @param Opportunity oppObj
    * @return Opportunity
    **/
    public static void upsrtOpp(Opportunity oppObj) {
        upsert oppObj;
    }

    /**
    * @description Recupera Oportuniadades por Folio de cotización
    * @author Eduardo Hernández Cuamatzi | 08-25-2020
    * @param folioAso Folio de cotización
    * @return List<Opportunity> Lista de Oportunidades
    **/
    public static List<Opportunity> findOppsByTw(String folioAso) {
      return [Select Id from Opportunity where FolioCotizacion__c =: folioAso Order By LastModifiedDate DESC];
    }

    /**
    * @description Devuelve valores de Oportunidad dependiendo de campos y condiciones
    * @author Alexandro Corzo | 24/09/2020
    * @param String sId
    * @return List<Opportunity> Lista de Resultados
    **/
    public static List<Opportunity> resultQueryOppo(String queryfield, String conditionField, boolean whitCondition) {
      String sQuery;
      switch on String.valueOf(whitCondition) {
          when 'false' {
              sQuery = SELECT_STR + queryfield +' FROM Opportunity';
          }
          when 'true' {
              sQuery = SELECT_STR + queryfield +' FROM Opportunity WHERE '+conditionField;
          }
      }
      return Database.query(String.escapeSingleQuotes(sQuery).unescapeJava());
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param queryfields
    * @param queryFilters
    * @param listFilters
    * @return List<AggregateResult>
    * @example Query SELECT Id, Name FROM Opportunity WHERE Id =:filtro0 AND Name =: filtro1;
    **/
    @SuppressWarnings('sf:DUDataflowAnomalyAnalysis, sf:UnusedLocalVariable')
    public static List<AggregateResult> fetchAROppByFilters(String queryfields, String queryFilters, List<String> listFilters, DateTime sdt, DateTime edt) {
        List<String> listToFilters = new List<String>(6);
        Integer posicion = 0;
        for(String filtro : listFilters) {
            listToFilters[posicion] = filtro;
            posicion++;
        }

        final String filtro0 = listToFilters[0];
        final String filtro1 = listToFilters[1];
        final String filtro2 = listToFilters[2];
        final String filtro3 = listToFilters[3];
        final String filtro4 = listToFilters[4];
        final String filtro5 = listToFilters[5];
        return Database.query(SELECT_STR + String.escapeSingleQuotes(queryfields) + ' FROM Opportunity ' + String.escapeSingleQuotes(queryFilters));
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param queryfields
    * @param queryFilters
    * @param listFilters
    * @return List<Opportunity>
    * @example Query SELECT Id, Name FROM Opportunity WHERE Id =:filtro0 AND StageName =: filtro1;
    **/
    @SuppressWarnings('sf:DUDataflowAnomalyAnalysis, sf:UnusedLocalVariable')
    public static List<Opportunity>fetchListOppFilters(String queryfields, String queryFilters, List<String> listFiltOpp, DateTime sdt, DateTime edt) {
        List<String> listOppF = new List<String>(6);
        Integer posicion = 0;
        for(String filtro : listFiltOpp) {
            listOppF[posicion] = filtro;
            posicion++;
        }

        final String filtro0 = listOppF[0];
        final String filtro1 = listOppF[1];
        final String filtro2 = listOppF[2];
        final String filtro3 = listOppF[3];
        final String filtro4 = listOppF[4];
        final String filtro5 = listOppF[5];
        return Database.query(SELECT_STR + String.escapeSingleQuotes(queryfields) + ' FROM Opportunity ' + String.escapeSingleQuotes(queryFilters));
    }
   /**
    * @description
    * @author Gerardo Mendoza
    * @param upsertObj
    **/   
    public static void upsrtOppList(List<Opportunity> upsertObj) {
      if(!upsertObj.isEmpty()) {
          insert upsertObj;
      }
  }
        /**
  * @description 
  * @author Arsenio.perez.lopez.contractor@bbva.com | 11-11-2020 
  * @param Set<Id> ids 
  * @param String fieldtoCopy 
  * @param Boolean preserveId 
  * @param Boolean isDeepClone 
  * @param Boolean presReTime 
  * @param Boolean preseAutonum 
  * @return List<Opportunity> 
  **/
  public static List<Opportunity> clonOpp(Set<Id>  ids, String fieldtoCopy, Boolean preserveId, Boolean isDeepClone, Boolean presReTime, Boolean preseAutonum) {
      final String sQuery = SELECT_STR + fieldtoCopy +' FROM Opportunity WHERE Id In:ids';
      final List<Opportunity> opsstoClone = Database.query(String.escapeSingleQuotes(sQuery).unescapeJava());
      final List<Opportunity> opptocreate = new List<Opportunity>();
      if(!opsstoClone.isEmpty()) {
        for(Opportunity opp: opsstoClone) {
            opptocreate.add(opp.clone(preserveId,isDeepClone,presReTime,preseAutonum));
        }
      }
      return opptocreate;
    }
    /**
    * @description 
    * @author Arsenio.perez.lopez.contractor@bbva.com | 02-09-2021 
    * @param String queryfield 
    * @param String conditionField 
    * @param Set<String> inList 
    * @return List<Opportunity> 
    **/
    public static List<Opportunity> resultQueryOppo(String queryfield, String conditionField, Set<String> iynList) {
      final String sQuery = SELECT_STR + queryfield +' FROM Opportunity WHERE '+conditionField;
      return Database.query(String.escapeSingleQuotes(sQuery).unescapeJava());
    }
}