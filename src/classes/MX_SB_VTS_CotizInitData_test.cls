/**
 * @File Name          : MX_SB_VTS_CotizInitData_test.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Diego Olvera
 * @Last Modified On   : 09-23-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/5/2020   Eduardo Hernandez Cuamatzi     Initial Version
 * 1.1    09/03/2021 Eduardo Hernandez Cuamatzi     Se corrigen clases test
**/
@isTest
private class MX_SB_VTS_CotizInitData_test {

    /**@description llave de mapa*/
    private final static string HASERROR = 'hasError';
    /**@description llave de ERROR*/
    private final static string ERRORTEST = 'errorTest';
    /**@description etapa Cotizacion*/
    private final static string COTIZACION = 'Cotización';
    /**@description Nombre usuario*/
    private final static string ASESORNAME = 'AsesorTest';
    
    @TestSetup
    static void makeData() {
        MX_SB_VTS_CallCTIs_utility.initHogarGeneric();
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'getInsuranceContracts', iaso__Url__c = 'http://www.example.com', iaso__Cache_Partition__c = 'local.MXSBVTSCache');
    }

    @isTest
    static void initDataCotiz() {
        final User asesorUser = [Select Id from User where LastName =: ASESORNAME];
        MX_SB_VTS_CallCTIs_utility.insertPermissionVTS(asesorUser);
        final Opportunity oppId = [Select Id, Name from Opportunity where StageName =: COTIZACION];
        Test.startTest();
        System.runAs(asesorUser) {
            final Map<String, Object> dataCotiz = MX_SB_VTS_CotizInitData_Ctrl.initDataCotiz(oppId.Id);
            final Opportunity oppDat = (Opportunity)dataCotiz.get('oppRecord');
            System.assertEquals(oppId.Id, oppDat.Id, 'Datos recuperados');
        }
        Test.stopTest();
    }

    @isTest
    static void findDataClient() {
        final User asesorUser = [Select Id from User where LastName =: ASESORNAME];
        MX_SB_VTS_CallCTIs_utility.insertPermissionVTS(asesorUser);
        Test.startTest();
            System.runAs(asesorUser) {
                final Opportunity oppId = [Select Id, Name from Opportunity where StageName =: COTIZACION];
                final Quote quoteId = [Select Id, Name from Quote where OpportunityId =: oppId.Id];
                final Map<String, Object> dataCotiz = MX_SB_VTS_CotizInitData_Ctrl.findDataClient(String.valueOf(quoteId.Id));
                final Quote datQuote = (Quote)dataCotiz.get('queteDat');
                if(String.isEmpty(datQuote.Id)) {
                    datQuote = quoteId;
                }
                System.assertEquals(quoteId.Id, datQuote.Id, 'Datos obtenidos');
            }
        Test.stopTest();
    }

    @isTest
    static void findQuoteEmail() {
        final User asesorUser = [Select Id from User where LastName =: ASESORNAME];
        MX_SB_VTS_CallCTIs_utility.insertPermissionVTS(asesorUser);
        final Opportunity oppId = [Select Id, Name from Opportunity where StageName =: COTIZACION];
        final Quote quoteId = [Select Id, Name from Quote where OpportunityId =: oppId.Id];
        Test.startTest();
            System.runAs(asesorUser) {
                final Map<String, Object> dataCot = MX_SB_VTS_CotizInitData_Ctrl.findQuoteEmail(quoteId.Id);
                final List<Quote> datQuote = (List<Quote>)dataCot.get('dataQuote');
                System.assertEquals(quoteId.Id, datQuote[0].Id, 'Datos cotización');
            }
        Test.stopTest();
    }

    @isTest
    static void sendingEmail() {
        final User asesorUser = [Select Id from User where LastName =: ASESORNAME];
        MX_SB_VTS_CallCTIs_utility.insertPermissionVTS(asesorUser);
        final Opportunity oppId = [Select Id, Name from Opportunity where StageName =: COTIZACION];
        final Quote quoteId = [Select Id, Name from Quote where OpportunityId =: oppId.Id];
        Test.startTest();
            System.runAs(asesorUser) {
                final Map<String, Object> dataCot = MX_SB_VTS_CotizInitData_Ctrl.sendingEmail(quoteId.Id, 'test', 'test@gmail.com');
                final Boolean response = (Boolean)dataCot.get('response');
                System.assert(response, 'Correo enviado');
            }
        Test.stopTest();
    }

    @isTest
    static void findDataClientEx() {
        final User userTest = [Select Id from User where LastName =: ASESORNAME];
        Test.startTest();
            System.runAs(userTest) {
                final Map<String, Object> badResponse = MX_SB_VTS_CotizInitData_Ctrl.findDataClient(ERRORTEST);
                final Boolean responseError = (Boolean)badResponse.get(HASERROR);
                System.assert(responseError, 'Exception');
            }
        Test.stopTest();
    }

    @isTest
    static void initDataCotizEx() {
        final User userTest = [Select Id from User where LastName =: ASESORNAME];
        Test.startTest();
            System.runAs(userTest) {
                final Map<String, Object> badResponse = MX_SB_VTS_CotizInitData_Ctrl.findDataClient(ERRORTEST);
                final Boolean responseError = (Boolean)badResponse.get(HASERROR);
                System.assert(responseError, 'Exception envio');
            }
        Test.stopTest();
    }

    @isTest
    static void validPolizNum() {
        final Map<String, String> mHeaderMockInv = new Map<String, String>();
        mHeaderMockInv.put('tsec','0987654321');
        final MX_WB_Mock objMockCallOutInv = new MX_WB_Mock(200, 'Complete', '{"data": [{"id": "Sx0NaPO-BCQAjFNdbvFavA","policyNumber": "1029384756","productType": "LIFE_HEALTH","product": {"id": "4027","name": "VIDA INDIVIDUAL","plan": {"id": "001","planType": {"id": "001","name": "Seguro Estudia"}}},"insuredAmount": {"amount": 0,"currency": "MXN"},"status": {"id": "PENDING_ACTIVATION","description": "Pending Activation","reason": {"id": "INC","description": "INCLUIDA"}},"insuranceCompany": {"id": "02","name": "EMPRESA"},"paymentConfiguration": {"frequency": {"id": "MONTHLY","name": "MENSUAL"},"paymentType": "DIRECT_DEBIT"},"validityPeriod": {"startDate": "2018-11-28","endDate": "2019-11-28"},"currentInstallment": {"period": {}},"renewalPolicy": {"counter": 0},"certificateNumber": "1","subscriptionType": "INDIVIDUAL"}]}', mHeaderMockInv);
        iaso.GBL_Mock.setMock(objMockCallOutInv);
        Test.startTest();
            final Map<String, Object> validTest = MX_SB_VTS_GetSetInsContract_Service.obtQueryInsuranceContract('1B8722002Q');
        Test.stopTest();
        System.assert(!validTest.isEmpty(), 'Lista No Vacia');
    }
     @isTest
    static void updOppValue() {
		final User userTest = [Select Id from User where LastName =: ASESORNAME];
        final Account accnt = MX_WB_TestData_cls.createAccount('Account Test', System.Label.MX_SB_VTS_PersonRecord);
        insert accnt;
        final Opportunity opp = MX_WB_TestData_cls.crearOportunidad('Agenda Test', accnt.Id, userTest.Id, System.Label.MX_SB_VTA_VentaAsistida);
        opp.MX_WB_comentariosCierre__c = 'Propio';
        insert opp;
        System.runAs(userTest) {
            final Opportunity OppRecId = [SELECT Id, MX_WB_comentariosCierre__c  FROM Opportunity  where Name = 'Agenda Test'];
            Test.startTest();
            try {
                MX_SB_VTS_CotizInitData_Ctrl.updCurrRec(OppRecId.Id, OppRecId.MX_WB_comentariosCierre__c, OppRecId.MX_WB_comentariosCierre__c);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal');
                System.assertEquals('Fatal', extError.getMessage(),'no entro');
            }
            Test.stopTest();
        }
    }
    
    @isTest
    static void getOppValue() {
		final User userTest = [Select Id from User where LastName =: ASESORNAME];
        System.runAs(userTest) {
            final Opportunity OppRecId = [SELECT Id  FROM Opportunity  where StageName =: COTIZACION];
            Test.startTest();
            	try {
            	MX_SB_VTS_CotizInitData_Ctrl.getOppVal(OppRecId.Id);
            	} catch (System.AuraHandledException extError) {
                extError.setMessage('Error');
                System.assertEquals('Error', extError.getMessage(),'no entra');
            }
            Test.stopTest();
        }
    }
}