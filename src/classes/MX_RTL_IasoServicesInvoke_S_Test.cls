/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 02-03-2021
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   02-03-2021   Eduardo Hernández Cuamatzi   Initial Version
**/
@isTest
public class MX_RTL_IasoServicesInvoke_S_Test {

    /**URL Mock Test*/
    private final static String URLTOTESTASO = 'http://www.example.com';
    /**Particion IASO*/
    private final static String IASOPARTASO = 'local.MXSBVTSCache';
    /**Servicio IASO*/
    private final static String CALCULATEPRICES = 'CalculateQuotePrice';
    /**@description llave de mapa*/
    private final static string TOKEN = '12345678';
    /**@description llave de ERROR*/
    private final static string TSEC = 'tsec';
    /**@description Nombre usuario*/
    private final static String ASESORCMBNAM = 'AsesorTes';
    /**@description Nombre usuario*/
    private final static String SERVICIOEXIT = 'Servicio exitoso';
    /**@description Nombre usuario*/
    private final static string COMPLETE = 'Complete';
    
    @TestSetup
    static void makeData() {
        final User tstDpUserTest = MX_WB_TestData_cls.crearUsuario(ASESORCMBNAM, 'System Administrator');
        insert tstDpUserTest;
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'getGTServicesSF', iaso__Url__c = URLTOTESTASO, iaso__Cache_Partition__c = IASOPARTASO);
        insert new iaso__GBL_Rest_Services_Url__c(Name = CALCULATEPRICES, iaso__Url__c = URLTOTESTASO, iaso__Cache_Partition__c = IASOPARTASO);
        final MX_SB_VTS_Generica__c setting = MX_WB_TestData_cls.GeneraGenerica('IASO01', 'IASO01');
        setting.MX_SB_VTS_Type__c = 'IASO01';
        setting.MX_SB_VTS_HEADER__c = URLTOTESTASO;
        insert setting;
    }

    @isTest
    static void callServicesOne() {
        final User userU = [Select Id from User where LastName =: ASESORCMBNAM];
        System.runAs(userU) {
            initQuoteMock(CALCULATEPRICES);
            Test.startTest();
                final HttpResponse response = MX_RTL_IasoServicesInvoke_Selector.callServices(CALCULATEPRICES, new Map<String, Object>{});
                System.assertEquals(200, Integer.valueOf(response.getStatusCode()), SERVICIOEXIT);
            Test.stopTest();
        }
    }

    @isTest
    static void callServicesSecond() {
        final User userU = [Select Id from User where LastName =: ASESORCMBNAM];
        System.runAs(userU) {
            initQuoteMock(CALCULATEPRICES);
            Test.startTest();
                final HttpRequest bodyRequest = new HttpRequest();
                final HttpResponse response = MX_RTL_IasoServicesInvoke_Selector.callServices(CALCULATEPRICES, new Map<String, Object>{}, bodyRequest);
                System.assertEquals(200, Integer.valueOf(response.getStatusCode()), SERVICIOEXIT);
            Test.stopTest();
        }
    }

    @isTest
    static void callServicesThird() {
        final User userU = [Select Id from User where LastName =: ASESORCMBNAM];
        System.runAs(userU) {
            initQuoteMock(CALCULATEPRICES);
            Test.startTest();
                final HttpResponse response = MX_RTL_IasoServicesInvoke_Selector.callServices(CALCULATEPRICES,'{}');
                System.assertEquals(200, Integer.valueOf(response.getStatusCode()), SERVICIOEXIT);
            Test.stopTest();
        }
    }

    /**
    * @description Genera mock para la petición iaso
    * @author Eduardo Hernández Cuamatzi | 02-03-2021 
    * @param serviceName Nombre de la custom metaadata de donde se recupera el mock
    **/
    public static void initQuoteMock(String serviceName) {
        final String calculateStr = MX_SB_VTS_ScheduldOppsTele_tst.quotetationStr(serviceName);
        final Map<String, String> headersMock = new Map<String, String>();
        headersMock.put(TSEC, TOKEN);
        final MX_WB_Mock mockCallout = new MX_WB_Mock(200, COMPLETE, calculateStr, headersMock); 
        iaso.GBL_Mock.setMock(mockCallout);
    }
}