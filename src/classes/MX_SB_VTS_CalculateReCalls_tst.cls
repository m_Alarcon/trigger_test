/**
 * @File Name          : MX_SB_VTS_CalculateReCalls_tst.cls
 * @Description        : 
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 27/2/2020 17:40:50
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    20/11/2019   Eduardo Hernández Cuamatzi     Initial Version
**/
@isTest
private class MX_SB_VTS_CalculateReCalls_tst {

    /**Entradas para servicio IASO */
     static String txtPost = 'POST', txtDescription = 'DESCRIPCION', txtServicio='SERVICEID', txtMsj='NOT iS EMPTY',
        txtUrl='http://www.example.com', txttelf='5511223344', txtUsername='UserOwnerTest01';
        /**Valor no aplica */
        final static String NAVALS = 'N/A';
        /**Bandeja de leads */
        private final static Map<Id, MX_SB_VTS_Lead_tray__c> MAPTRAY = MX_SB_VTS_CalculateReCalls_cls.fillTrays();
        /**horas de prueba */
        private static List<datetime> horasT = new List<datetime>{DateTime.newInstance(2019, 11, 25, 22, 00, 00),DateTime.newInstance(2019, 11, 26, 20, 00, 00),DateTime.newInstance(2019, 11, 28, 20, 00, 00),DateTime.newInstance(2019, 11, 30, 19, 00, 00)};
        /**Valores nivel 6 */
        private static List<String> level6T = new List<String>{System.Label.MX_SB_VTS_OtroIdioma,System.Label.MX_SB_VTS_PensarIndeciso,'Línea ocupada',System.Label.MX_SB_VTS_NoDatosContrata,'Economía personal',System.Label.MX_SB_VTS_NoDatosContrata};
        /**Valor No venta */
        final static String NAVENTA = 'No Venta';
        /**Telefono test */
        final static String PHONEPERSON = '5544332211';
        /**BANDEJA test */
        final static String TRAYLEAD = '1152';
        /**Accion cuatro */
        final static String NEXTACTIONG4= '4';
    
    @testSetup 
    static void setup() {                
        createMotivosTest();
        final MX_WB_FamiliaProducto__c familiaHogar = MX_WB_TestData_cls.createProductsFamily ( 'Daños' );
        insert familiaHogar;

        final Product2 proHogarRecall = MX_WB_TestData_cls.productNew ( System.Label.MX_SB_VTS_Hogar );
        proHogarRecall.IsActive = true;
        proHogarRecall.MX_WB_FamiliaProductos__c = familiaHogar.Id;
        insert proHogarRecall;

        final MX_SB_VTS_ProveedoresCTI__c smartProvRecall = new MX_SB_VTS_ProveedoresCTI__c();
        smartProvRecall.Name = 'Smart Center';
        smartProvRecall.MX_SB_VTS_Identificador_Proveedor__c = 'SMART CENTER';
        smartProvRecall.MX_SB_VTS_TieneSegmento__c = true;
        smartProvRecall.MX_SB_VTS_IsReadyCTI__c = true;
        smartProvRecall.MX_SB_VTS_BusinessHoursL_V__c = Time.newInstance(0, 5, 0, 0);
		smartProvRecall.MX_SB_VTS_FinishBusinessHoursL_V__c = Time.newInstance(22, 0, 0, 0);
		smartProvRecall.MX_SB_VTS_BusinessHoursL_S__c = Time.newInstance(5, 0, 0, 0);
		smartProvRecall.MX_SB_VTS_FinalBusinessHoursS__c = Time.newInstance(15, 0, 0, 0);
		smartProvRecall.MX_SB_VTS_FranjaHorario__c = 1;
        insert smartProvRecall; 

        final MX_SB_VTS_FamliaProveedores__c famProSmartRecall = new MX_SB_VTS_FamliaProveedores__c();
        famProSmartRecall.MX_SB_VTS_Familia_de_productos__c = familiaHogar.Id;
        famProSmartRecall.Name = 'Daños SmartCenter';
        famProSmartRecall.MX_SB_VTS_ProveedorCTI__c = smartProvRecall.Id;
        insert famProSmartRecall;

        final MX_SB_VTS_Lead_tray__c hotLeads = new MX_SB_VTS_Lead_tray__c();
        hotLeads.Name = 'HotLeads Samrt';
        hotLeads.MX_SB_VTS_Description__c = 'Bandeja para Tracking';
        hotLeads.MX_SB_VTS_ID_Bandeja__c = TRAYLEAD;
        hotLeads.MX_SB_VTS_Producto__c = proHogarRecall.Id;
        hotLeads.MX_SB_VTS_ProveedorCTI__c = smartProvRecall.Id;
        hotLeads.MX_SB_VTS_Tipo_bandeja__c = 'HotLeads';
	    hotLeads.MX_SB_VTS_ServicioID__c = '2';
        insert hotLeads;

        final Id typeLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
        final Map<String, String> valsLead = new Map<String, String>();
        valsLead.put('productO', System.Label.MX_SB_VTS_Hogar);
        valsLead.put('mobile', '5531234121');
        valsLead.put('email','test@test.com');
        valsLead.put('nameFirst', 'Pepito');
        valsLead.put('leadSour', 'Other');
        final Lead testCallLead = createLeadTelemarketing(typeLead, valsLead);
        testCallLead.MX_SB_VTS_TrayAttention__c = TRAYLEAD;
        testCallLead.LastName = 'GrupoDosRemarcado';
        insert testCallLead;
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'bbvaMexSmartCenters_Crear_Carga', iaso__Url__c = txtUrl, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
       	insert new iaso__GBL_Rest_Services_Url__c(Name = 'Authentication', iaso__Url__c = 'https://validation/ok', iaso__Cache_Partition__c = 'local.MXSBVTSCache');
       	insert new iaso__GBL_Rest_Services_Url__c(Name = 'bbvaMexSmartCenter', iaso__Url__c = 'https://bbvacifrado.smartcenterservicios.com/ws_salesforce_desarrollo/api/login/authenticate', iaso__Cache_Partition__c = 'local.MXSBVTSCache');  
    }

    private static MX_WB_MotivosNoContacto__c motivosNoContacto(String motivo, String nivel6, List<Integer> valsTime) {
        final MX_WB_MotivosNoContacto__c g1OtroIdioma = new MX_WB_MotivosNoContacto__c();
        g1OtroIdioma.MX_WB_MotivoNoContacto__c = motivo;
        g1OtroIdioma.MX_SB_VTS_Nivel_6__c = nivel6;
        g1OtroIdioma.MX_SB_VTS_AccionCuatro__c = valsTime[0];
        g1OtroIdioma.MX_SB_VTS_AccionUno__c = valsTime[1];
        g1OtroIdioma.MX_SB_VTS_AccionDos__c = valsTime[2];
        g1OtroIdioma.MX_SB_VTS_AccionTres__c = valsTime[3];
        g1OtroIdioma.MX_SB_VTS_RecallGroup__c = 'RG1';
        return g1OtroIdioma;
    }

    private static void createMotivosTest() {
        final List<Integer> valsAct = new List<Integer>{0,0,0,0};
        final MX_WB_MotivosNoContacto__c g1OtroIdioma = motivosNoContacto(System.Label.MX_SB_VTS_OtroIdioma, System.Label.MX_SB_VTS_OtroIdioma, valsAct);
        g1OtroIdioma.Name = 'TipificacionTest 1';
        g1OtroIdioma.MX_SB_VTS_ReglaActiva__c = true;
        insert g1OtroIdioma;

        final List<Integer> valsAct2 = new List<Integer>{1,72,4,4};
        final MX_WB_MotivosNoContacto__c g2ClientePe = motivosNoContacto(System.Label.MX_SB_VTS_PensarIndeciso, System.Label.MX_SB_VTS_PensarIndeciso, valsAct2);
        g2ClientePe.Name = 'TipificacionTest 2';
        g2ClientePe.MX_SB_VTS_ReglaActiva__c = true;
        insert g2ClientePe;

        final List<Integer> valsAct3 = new List<Integer>{0,240,4,4};
        final MX_WB_MotivosNoContacto__c g3TelSus = motivosNoContacto('Teléfono suspendido', 'Teléfono suspendido', valsAct3);
        g3TelSus.Name = 'TipificacionTest 3';
        g3TelSus.MX_SB_VTS_ReglaActiva__c = true;
        insert g3TelSus;
        
        final List<Integer> valsAct4 = new List<Integer>{1,240,4,4};
        final MX_WB_MotivosNoContacto__c g4EcoPer = motivosNoContacto('Economía personal', 'Economía personal', valsAct4);
        g4EcoPer.Name = 'TipificacionTest 4';
        g4EcoPer.MX_SB_VTS_ReglaActiva__c = true;
        insert g4EcoPer;
        
        final List<Integer> valsAct5 = new List<Integer>{1,72,4,4};
        final MX_WB_MotivosNoContacto__c g5NoCuentaDatos = motivosNoContacto(System.Label.MX_SB_VTS_NoDatosContrata, System.Label.MX_SB_VTS_NoDatosContrata, valsAct5);        
        g5NoCuentaDatos.Name = 'TipificacionTest 5';
        g5NoCuentaDatos.MX_SB_VTS_ReglaActiva__c = true;
        insert g5NoCuentaDatos;

        final List<Integer> valsAct6 = new List<Integer>{2,4,72,4};
        final MX_WB_MotivosNoContacto__c g6LinOcu = motivosNoContacto('Línea ocupada', 'Línea ocupada', valsAct6);
        g6LinOcu.Name = 'TipificacionTest 6';
        g6LinOcu.MX_SB_VTS_ReglaActiva__c = true;
        insert g6LinOcu;
    }

    static Lead createLeadTelemarketing(Id typeLead, Map<String,String> valuesLead) {

        final Account acc = MX_WB_TestData_cls.crearCuenta('Doe', 'MX_WB_rt_PAcc_Telemarketing');
        acc.PersonMobilePhone=PHONEPERSON;
        Insert acc;
        
        final Opportunity opp = new Opportunity();
        opp.Name = 'Prueba 1 Remarcado';
        opp.Producto__c ='Hogar';
        opp.StageName = 'Cotización';
        opp.AccountId = acc.Id;
        opp.MX_SB_VTS_TrayAttention__c = TRAYLEAD;
        opp.TelefonoCliente__c = '5523451231';
        opp.CloseDate = System.now().date();
        opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
        opp.MX_SB_VTS_TrayAttention__c =TRAYLEAD;
        insert opp;

        final Lead testCallLead = new Lead();
        testCallLead.FirstName = valuesLead.get('nameFirst');
        testCallLead.RecordTypeId = typeLead;
        testCallLead.LeadSource = valuesLead.get('leadSour');
        testCallLead.Producto_Interes__c = valuesLead.get('producto');
        testCallLead.MobilePhone = valuesLead.get('mobile');
        testCallLead.Email = valuesLead.get('email');
        return testCallLead;
    }

    @isTest 
    static void reCallLeadG1() {
        Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_Integration_MockGenerator());
        iaso.GBL_Mock.setMock(new MX_SB_VTS_Integration_MockGenerator());
        final System.HttpRequest req = new System.HttpRequest();
        req.setMethod(txtPost);
        req.setEndpoint(txtUrl);
        req.setHeader(txtServicio, '2');
        req.setHeader(txtDescription, txtDescription);
        final Lead leadTestG1 = [Select Id, Resultadollamada__c, LeadSource, MX_SB_VTS_Tipificacion_LV2__c, MX_SB_VTS_Tipificacion_LV3__c, MX_SB_VTS_Tipificacion_LV4__c, MX_SB_VTS_Tipificacion_LV5__c,
        MX_SB_VTS_Tipificacion_LV6__c, MX_SB_VTS_TrayAttention__c,MX_WB_ph_Telefono1__c, MX_WB_ph_Telefono2__c, MobilePhone, MX_WB_ph_Telefono3__c, FirstName, LastName, Producto_Interes__c, Apellido_Materno__c from Lead where FirstName = 'Pepito'];
        leadTestG1.Resultadollamada__c = 'Contacto';
        leadTestG1.MX_SB_VTS_Tipificacion_LV2__c = 'Contacto Efectivo';
        leadTestG1.MX_SB_VTS_Tipificacion_LV3__c = 'Interesado';
        leadTestG1.MX_SB_VTS_Tipificacion_LV4__c = 'Acepta contratación';
        leadTestG1.MX_SB_VTS_Tipificacion_LV5__c = NAVENTA;
        leadTestG1.MX_SB_VTS_Tipificacion_LV6__c = System.Label.MX_SB_VTS_PensarIndeciso;
        update leadTestG1;
        leadTestG1.MX_SB_VTS_RecallValue__c = System.Label.MX_SB_VTS_PensarIndeciso;
        leadTestG1.MX_SB_VTS_NextActionCall__c = 1;
        leadTestG1.MX_SB_VTS_Tipificacion_LV7__c = NAVALS;
        Test.startTest();
            final List<Lead> lstLead = new List<Lead>{leadTestG1};
            MX_SB_VTS_MatrizRemarcado_cls.remarcado(lstLead);
        Test.stopTest();
        System.assertEquals(leadTestG1.MX_SB_VTS_Tipificacion_LV6__c, System.Label.MX_SB_VTS_PensarIndeciso, 'No Update');
    }
    
     @isTest 
    static void reCallOpportunityG1() {
        Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_Integration_MockGenerator());
        iaso.GBL_Mock.setMock(new MX_SB_VTS_Integration_MockGenerator());
        final System.HttpRequest req = new System.HttpRequest();
        req.setMethod(txtPost);
        req.setEndpoint(txtUrl);
        req.setHeader(txtServicio, '2');
        req.setHeader(txtDescription, txtDescription);

        final Opportunity opportunityTestG1 = [Select Id, MX_SB_VTS_Tipificacion_LV2__c, MX_SB_VTS_Tipificacion_LV3__c, MX_SB_VTS_Tipificacion_LV4__c, 
        MX_SB_VTS_Tipificacion_LV5__c, TelefonoCliente__c, MX_SB_VTS_Tipificacion_LV6__c, MX_SB_VTS_TrayAttention__c, MX_SB_VTS_NextActionCall__c, AccountId,
        Account.PersonMobilePhone, Account.MX_WB_ph_Telefono1__pc,Account.MX_WB_ph_Telefono2__pc,Account.MX_WB_ph_Telefono3__pc, Account.Name, Account.Apellido_materno__pc from Opportunity where Name = 'Prueba 1 Remarcado'];
		opportunityTestG1.MX_SB_VTS_Tipificacion_LV1__c = 'Contacto';
        opportunityTestG1.MX_SB_VTS_Tipificacion_LV2__c = 'Contacto Efectivo';       
        opportunityTestG1.MX_SB_VTS_Tipificacion_LV3__c = 'No Interesado';
        opportunityTestG1.MX_SB_VTS_Tipificacion_LV4__c = 'No acepta cotización';
        opportunityTestG1.MX_SB_VTS_Tipificacion_LV5__c = 'No Venta (No acepta cotización)';
        opportunityTestG1.MX_SB_VTS_Tipificacion_LV6__c = System.Label.MX_SB_VTS_OtroIdioma;
        opportunityTestG1.MX_SB_VTS_TrayAttention__c = TRAYLEAD;
        update opportunityTestG1;
        Test.startTest();
            opportunityTestG1.MX_SB_VTS_Tipificacion_LV7__c = NAVALS;
            update opportunityTestG1;
        Test.stopTest();
        System.assertEquals(opportunityTestG1.MX_SB_VTS_Tipificacion_LV6__c, System.Label.MX_SB_VTS_OtroIdioma, 'No Update tipidicación');
    }

    @isTest 
    static void reCallCalculate2() {
        Test.startTest();
            final MX_SB_VTS_LeadMultiCTI_Util.WrapperRecallCTI wrap = reCallsNodes('2',2);
        Test.stopTest();
        System.assertEquals(wrap.sTelefono1, PHONEPERSON, 'Wrap Correcto');
	}

    @isTest 
    static void reCallCalculate3() {
        Test.startTest();
            final MX_SB_VTS_LeadMultiCTI_Util.WrapperRecallCTI wrap = reCallsNodes('2',3);
        Test.stopTest();
        System.assertEquals(wrap.sTelefono1, PHONEPERSON, 'Wrap Correcto');
    }

    /**Remarcado de Opps */
    public static MX_SB_VTS_LeadMultiCTI_Util.WrapperRecallCTI reCallsNodes(String service, Integer nextAct) {
        MX_SB_VTS_LeadMultiCTI_Util.WrapperRecallCTI wrap = new MX_SB_VTS_LeadMultiCTI_Util.WrapperRecallCTI();
        Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_Integration_MockGenerator());
        iaso.GBL_Mock.setMock(new MX_SB_VTS_Integration_MockGenerator());
        final System.HttpRequest req = new System.HttpRequest();
        req.setMethod(txtPost);
        req.setEndpoint(txtUrl);
        req.setHeader(txtServicio, service);
        req.setHeader(txtDescription, txtDescription);
        final Map<String,MX_SB_VTS_Lead_tray__c> traySend = MX_SB_VTS_CalculateReCallsUtil_cls.findTrySend(MAPTRAY);
        final Map<Id, MX_SB_VTS_ProveedoresCTI__c> proveedores = MX_SB_VTS_CalculateReCallsUtil_cls.fillProveedores();        
        final MX_SB_VTS_Lead_tray__c traySending = traySend.get(TRAYLEAD);
        final MX_SB_VTS_ProveedoresCTI__c proveedor = proveedores.get(traySending.MX_SB_VTS_ProveedorCTI__c);
        final Opportunity opportunityTestG1 = [Select Id, MX_SB_VTS_Tipificacion_LV2__c, MX_SB_VTS_Tipificacion_LV3__c, MX_SB_VTS_Tipificacion_LV4__c, MX_SB_VTS_Tipificacion_LV5__c, MX_SB_VTS_Tipificacion_LV6__c, MX_SB_VTS_TrayAttention__c, MX_SB_VTS_NextActionCall__c from Opportunity where Name = 'Prueba 1 Remarcado'];
            for(datetime hora : horasT) {
                for(String level6 : level6T) {
                    final Map<String, String> recordVals = new Map<String, String>();
                    recordVals.put('Id', opportunityTestG1.Id);
                    recordVals.put('nextAction', String.valueOf(nextAct));
                    recordVals.put('level6', level6 );
                    recordVals.put('phone1', MX_SB_VTS_LeadMultiCTI_Util.evalutePhone(PHONEPERSON,'5544332411'));
                    recordVals.put('phone2', MX_SB_VTS_LeadMultiCTI_Util.evalutePhone('5544332111','5544332511'));
                    recordVals.put('phone3', MX_SB_VTS_LeadMultiCTI_Util.evalutePhone('5544332311','5544332611'));
                    wrap = MX_SB_VTS_CalculateReCalls_cls.calculateRules(recordVals, traySending, proveedor, NAVALS, hora);
                }
            }	
        return wrap;
    }
    
    @isTest
    static void evaluateG4Test() {
        final Datetime addTimeG4 = Datetime.now();
        final Datetime addTimeG4Add = addTimeG4.addHours(4);
        final Map<String,MX_SB_VTS_Lead_tray__c> traySend = MX_SB_VTS_CalculateReCallsUtil_cls.findTrySend(MAPTRAY);
        final Map<Id, MX_SB_VTS_ProveedoresCTI__c> proveedores = MX_SB_VTS_CalculateReCallsUtil_cls.fillProveedores();   
        final MX_SB_VTS_Lead_tray__c traySending = traySend.get(TRAYLEAD);
     	final MX_SB_VTS_ProveedoresCTI__c proveedor = proveedores.get(traySending.MX_SB_VTS_ProveedorCTI__c);
        final MX_WB_MotivosNoContacto__c motivoRemarcadoG4 = [Select Id,MX_SB_VTS_AccionUno__c,MX_SB_VTS_AccionDos__c, Name from MX_WB_MotivosNoContacto__c where Name = 'TipificacionTest 2'];
        Test.startTest();
            final List<String> resultCal = MX_SB_VTS_CalculateReCallsUtil_cls.evaluateG4(addTimeG4,proveedor,NEXTACTIONG4,motivoRemarcadoG4);
            resultCal[0] = addTimeG4Add.format(System.Label.MX_SB_VTS_FormatDateSmart);
        Test.stopTest();
        System.assertEquals(resultCal[0], addTimeG4Add.format(System.Label.MX_SB_VTS_FormatDateSmart), 'Fecha calculada');
    }
}