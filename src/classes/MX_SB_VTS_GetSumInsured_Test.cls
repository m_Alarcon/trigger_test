@isTest
/**
* @File Name          : MX_SB_VTS_GetSumInsured_Test.cls
* @Description        :
* @Author             : Marco Antonio Cruz Barboza
* @Group              :
* @Last Modified By   : Marco Antonio Cruz Barboza
* @Last Modified On   : 13/07/2020
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    13/07/2020     Marco Antonio              Initial Version
* 2.0   29/01/2021      Marco Cruz                 Add a test method to check webservice.
**/
private class MX_SB_VTS_GetSumInsured_Test {
	
    /** Name User */
    final static String TSTUSER = 'USERTEST';
    /** Name Account */
    final static String TSTACC = 'TEST ACCOUNT';
    /** Name Opp */
    final static String TSTOPP = 'OPP TEST';
    /**URL Mock*/
    Final static String URLTEST = 'http://www.example.com';
    /** Mock **/
    Final Static String RESPOMOCK = '{"suggestedAmount": {"amount": 9500000,"currency": "MXN"},"suggestedCoverages": [{"id": "SAMINEINCE","suggestedAmount": {"amount": 100000,"currency": "MXN"}}]}';
    /**Insured Amount**/
    Final Static String INSUAMO = '2400000';
    /**Propiedad Tipo Propio**/
    Final Static String PROPIO = 'Propio';
    /**Propiedad tipo Rentado**/
    Final Static String RENTADO = 'Rentado';
    /* 
    @Method: setupTest
    @Description: create test data set
    */
    @TestSetup
    static void setupTest() {
        final User testingUsr = MX_WB_TestData_cls.crearUsuario(TSTUSER, System.Label.MX_SB_VTS_ProfileAdmin);
        System.runAs(testingUsr) {
            final Account tstAccount = MX_WB_TestData_cls.crearCuenta(TSTACC, System.Label.MX_SB_VTS_PersonRecord);
            tstAccount.PersonEmail = 'pruebaVts@mxvts.com';
            insert tstAccount;
            final Opportunity tstOpport = MX_WB_TestData_cls.crearOportunidad(TSTOPP, tstAccount.Id, testingUsr.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            tstOpport.LeadSource = 'Call me back';
            tstOpport.Producto__c = 'Hogar';
            tstOpport.Reason__c = 'Venta';
            tstOpport.StageName = 'Cotizacion';
            insert tstOpport;
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'getInsuredAmount', iaso__Url__c = URLTEST, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
        }
    }
    /*
    @Method: amountValTest
    @Description: return json values of fake response
    */
    @isTest
    static void amountValTest() {
        test.startTest();
            Final object testResponse = MX_SB_VTS_GetSumInsured_Ctrl.amountVal();
            System.assertNotEquals(testResponse, null,'El objeto no se encuentra vacio');
        test.stopTest();
    }
    
    /* 
    @Method: getAmountsTest
    @Description: get an amount lists from a web service
	@Param String insAmount
	@Return List<Object>
    */
    @isTest
    static void getAmountsTest() {
        Final Map<String,String> mockTsec = new Map<String,String> {'tsec'=>'1234456789'};
       	Final MX_WB_Mock calloutTst = new MX_WB_Mock(200, 'Complete',RESPOMOCK, mockTsec); 
        iaso.GBL_Mock.setMock(calloutTst);
        test.startTest();
            Final List<object> testResponse = MX_SB_VTS_GetSumInsured_Ctrl.getAmounts(INSUAMO, PROPIO);
            System.assertNotEquals(testResponse, null,'El objeto no se encuentra vacio');
        test.stopTest();
    }
    
}