/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_COB_CreaPolizaSch_cls
* Autor Angel Nava
* Proyecto: Cobranza - BBVA Bancomer
* Descripción : Clase para ejecutar el batch de creapoliza cobranza
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Desripción
* --------------------------------------------------------------------------------
* 1.0           01/05/2019     Angel Nava				          Creación
* --------------------------------------------------------------------------------
*/
global with sharing class MX_SB_COB_CreaPolizaSch_cls implements Schedulable {
    /**mapa pagos espejo */
    global Map<String,Map<String,PagosEspejo__c>> mapPolFactPagoEspejo { get;set; }
    /**mapa polizas nuevas */
    global Map<String,Contract> mapPolNvas { get;set; }
    /**ctx */
    global void execute(SchedulableContext ctx) {
        Integer maximo = 100;
        final MX_SB_COB_CreaPolizaBch_cls polizaBch = new MX_SB_COB_CreaPolizaBch_cls(mapPolNvas,mapPolFactPagoEspejo);
        final Id batchId = database.executeBatch(polizaBch,maximo);
        if(batchId==null) {
            maximo=90;
        }
        system.abortJob(ctx.getTriggerId());
    }
}