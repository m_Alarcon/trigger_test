/*
*
* @author Jaime Terrats
* @description This class will be used to upsert data coming from Hogar tracking web
*
*           No  |     Date     |     Author      |    Description
* @version  1.0    04/10/2019     Jaime Terrats     Create data structure to get incoming data from web tracking
* @version  1.1    05/10/2019     Jaime Terrats     Remove code smells
* @version  1.2    05/22/2019     Jaime Terrats     Fix Quoli update and remove code smells
* @version  1.3    05/23/2019     Jaime Terrats     Fix Quoli Update
* @version  1.4    05/24/2019     Jaime Terrats     Fix null values for checkbox fields
* @version  1.5    06/06/2019     Jaime Terrats     Fix null value cupon
* @version  1.6    06/19/2019     Jaime Terrats     Fix opp close won
* 1.1.3 	   24/02/2020   Eduardo Hernández Cuamatzi     Fix registros duplicados
* 1.1.4        25/03/202    Francisco Javier        Fix cotizador seguros
* 1.1.5  	27/03/2020   	Eduardo Hernández Cuamatzi     Owner por proveedor
*/
@RestResource(urlMapping='/Hogar/*')
global without sharing class MX_SB_VTS_Hogar_Service {
    /*
    * Define opp status
    */
    private static final String EMITIDA='Emitida';
    /**Propietario de la Opp y de la cotización */
    private static String ownerIdH;
    /*Implants*/
    final static List<User> LSTIMPLANT = [Select Id, Name, MX_SB_VTS_ProveedorCTI__c from User where MX_SB_VTS_ProveedorCTI__c NOT IN ('') AND MX_SB_VTS_ImplantConnected__c = TRUE AND IsActive = TRUE];
    /**Constructor */
    private MX_SB_VTS_Hogar_Service() {} //NOSONAR

    /**
     * cotizarSeguroHogar Cotiza seguro de hogar
     * @param	datosCotizacion Datos e la cotización
     * @return	resSFDC	Valor de salesforce
     */
    @HttpPost
    global static List<MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC> cotizarSeguroHogar(MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion datosCotizacion) {
        final List<MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC> resSFDC = new List<MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC>();
        MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC res = new MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC();
        res.error = '';
        res.message = '';
        res.id = '';
        final String emailClient = datosCotizacion.datosCliente.Email.toLowerCase();
        final String phone = datosCotizacion.datosCliente.telefonoCasa.toLowerCase();
        final String movil = datosCotizacion.datosCliente.celular.toLowerCase();
        ownerIdH = MX_SB_VTS_utilityQuote.findOwnerProvider('Smart Center', LSTIMPLANT);
        final Boolean isInBlackList = MX_SB_VTS_utilityQuote.validateEmails(emailClient, phone, movil);
        if(isInBlackList == false) {
            // get Account, Opportunity and Quoute Line item
            Account findAcco = MX_SB_VTS_utilityQuote.findUniqueAccount(emailClient);
            if(String.isNotBlank(findAcco.Id)) {
                findAcco = upsertAccount(datosCotizacion, true);
                upsertOpportunity(findAcco, datosCotizacion);
                res.message += ' Cuenta localizada: ' + findAcco.Id;
                resSFDC.add(res);
            } else {
                findAcco = upsertAccount(datosCotizacion, false);
                upsertOpportunity(findAcco, datosCotizacion);
                res.message = 'Generando nuevo folio para el cliente';
                res.id += 'id de la cuenta: ' + findAcco.Id;
                resSFDC.add(res);
            }
        } else {
            res.message = 'Correo o número telefonico en lista negra';
            res.id += 'El registro no se creo por pertenecer a lista negra';
            resSFDC.add(res);
        }
        return resSFDC;
    }

    /**
     * upsertAccount Upsert a la cuenta
     * @param	datosCotizacion Datos e la cotización
     * @return	accsToUpsert	Regresa una lista
     */
    private static Account upsertAccount(MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion datosCotizacion, Boolean exist) {
        final String emailClient = datosCotizacion.datosCliente.Email.toLowerCase();
        Account findAccount = MX_SB_VTS_utilityQuote.findUniqueAccount(emailClient);
        if(exist == false) {
            final List<String> listVals = new List<String>{datosCotizacion.datosCliente.nombre, datosCotizacion.datosCliente.email, datosCotizacion.datosCliente.apPaterno, datosCotizacion.datosCliente.apMaterno};
            findAccount = MX_SB_VTS_utilityQuote.fillAccount(findAccount,listVals);
        }
        findAccount = accountInit(findAccount, datosCotizacion);
        switch on datosCotizacion.datosIniciales.origen.toLowerCase() {
            when  'inbound'{
                findAccount.AccountSource = 'Inbound';
            }
            when 'outbound' {
                findAccount.AccountSource = 'Outbound';
            }
        }
        try {
            Database.upsert(findAccount);
        } catch(DmlException dmlEx) {
            throw new DmlException('Err ' + dmlEx);
        }
        
        return MX_SB_VTS_utilityQuote.findUniqueAccount(findAccount.Id);
    }

    /**
     * accountInit Init de la cuenta
     * @param	retorig Datos de la cuenta
     * @param	datosCotizacion	Datos de la cotización
     * @return	ret	Regresa datos de la cuenta
     */
    private static Account accountInit(Account retorig, MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion datosCotizacion) {
        final Account retHogar = retorig;
        retHogar.PersonBirthdate = date.parse(datosCotizacion.datosCliente.fechaNacimiento);
        retHogar.PersonHomePhone = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.telefonoCasa);
        retHogar.PersonMobilePhone = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.celular);
        retHogar.MX_Gender__pc = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.sexo);
        retHogar.RFC__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.rfc);
        retHogar.BillingStreet = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomicilio.calleCliente);
        retHogar.BillingCity = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomicilio.ciudadCliente);
        retHogar.BillingPostalCode = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomicilio.cpCliente);
        retHogar.BillingState = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomicilio.estadoCliente);
        retHogar.Apellido_materno__pc = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.apMaterno);
	retHogar.LastName = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.apPaterno);
        retHogar.BillingCountry = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomicilio.paisCliente);
        retHogar.Numero_Exterior__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomicilio.numExtCliente);
        retHogar.Numero_Interior__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomicilio.numIntCliente);
        retHogar.Colonia__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomicilio.coloniaCliente);
        return retHogar;
    }

    /**
     * upsertOpportunity upsert a la oportunidad
     * @param	acc Id de la cuenta
     * @param	datosCotizacion	Datos de la cotización
     */
    private static void upsertOpportunity(Account acc, MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion datosCotizacion) {
        final List<Opportunity> lstOpps = MX_SB_VTS_utilityQuote.findOppQuote(acc.Id, datosCotizacion.datosIniciales.producto, datosCotizacion.datosIniciales.origen.toLowerCase());
        Opportunity oppsToUpsert = new Opportunity();
        final Product2 stcProductId = [select Id,MX_WB_FamiliaProductos__c from Product2 where Name =: datosCotizacion.datosIniciales.producto  And MX_SB_SAC_Proceso__c = 'VTS' AND IsActive = true];
        final Quote presupuestoHogar = MX_SB_VTS_utilityQuote.findUniqueQuote(datosCotizacion.datosIniciales.folioCotizacion, datosCotizacion.datosIniciales.folioTracking);
        if(String.isEmpty(presupuestoHogar.Id) && lstOpps.isEmpty()) {
            final Account findAccount = MX_SB_VTS_utilityQuote.findUniqueAccount(acc.Id);
            final Opportunity cotizacion = new Opportunity();
            cotizacion.Name = findAccount.Oportunidad_Totales__c + 1  + ' ' + acc.FirstName + ' ' + acc.LastName;
            cotizacion.CloseDate = System.today();
            cotizacion.AccountId = acc.Id;
            cotizacion.FolioCotizacion__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosIniciales.folioCotizacion);
            cotizacion.Reason__c = 'Venta';
            cotizacion.Producto__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosIniciales.producto);
	    cotizacion.TelefonoCliente__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.celular);
            cotizacion.Pricebook2Id = MX_SB_VTS_utilityQuote.findPriceBook(stcProductId.Id);
            if(EMITIDA.equalsIgnoreCase(datosCotizacion.datosIniciales.statusCotizacion)) {
                cotizacion.StageName = 'Closed Won';
                cotizacion.MX_SB_VTS_Aplica_Cierre__c = true;
            } else {
                cotizacion.StageName = 'Cotización';
            }
            cotizacion.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
            switch on datosCotizacion.datosIniciales.origen.toLowerCase() {
                when 'inbound' {
                    cotizacion.LeadSource = 'Tracking web';
                    cotizacion.OwnerId = ownerIdH;
                }
                when 'outbound' {
                    cotizacion.LeadSource = 'Outbound TLMK';
                }
            }
            oppsToUpsert = cotizacion;
        } else {
            oppsToUpsert = MX_SB_VTS_utilityQuote.finalOppToWork(presupuestoHogar, lstOpps);
            if(EMITIDA.contains(datosCotizacion.datosIniciales.statusCotizacion) &&
            String.isNotBlank(datosCotizacion.datosIniciales.numeroPoliza)) {
                oppsToUpsert.MX_SB_VTS_Aplica_Cierre__c = true;
            }
            upsertQuote(oppsToUpsert, datosCotizacion, stcProductId);
        }

        if(String.isBlank(oppsToUpsert.Id)) {
            upsertOpporExt(datosCotizacion, oppsToUpsert, stcProductId);
        }
    }

    /**
    * @description Actualiza opportunidad y genera Quote
    * @author Eduardo Hernandez Cuamatzi | 2/6/2020 
    * @param MX_SB_VTS_DatosCotizacion datosCotizacion 
    * @param Opportunity oppsToUpsert Oportunidad nueva
    * @param Product2 stcProductId producto de interes de la Oportunidad
    * @return void 
    **/
    private static void upsertOpporExt(MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion datosCotizacion, Opportunity oppsToUpsert, Product2 stcProductId) {
        try {
            final Quote presHogarTemp = MX_SB_VTS_utilityQuote.findUniqueQuote(datosCotizacion.datosIniciales.folioCotizacion, datosCotizacion.datosIniciales.folioTracking);
            if(String.isNotBlank(presHogarTemp.OpportunityId)) {
                oppsToUpsert.Id = presHogarTemp.OpportunityId;
                oppsToUpsert.LeadSource = presHogarTemp.Opportunity.LeadSource;
            }
            final String cuponHogar = String.isNotBlank(datosCotizacion.datosPrecio.cupon) ? datosCotizacion.datosPrecio.cupon : '';
            oppsToUpsert.MX_WB_Cupon__c = cuponHogar;
            oppsToUpsert.MX_WB_EnvioCTICupon__c = rtwCotizacion.validarCupon(cuponHogar);
            Database.Upsert(oppsToUpsert);
            upsertQuote(oppsToUpsert, datosCotizacion, stcProductId);
        } catch(DmlException dmlEx) {
            throw new DmlException('DML:'+dmlEx);
        }
    }

    /**
     * upsertQuote upsert al Quote
     * @param	opp Id de la oportunidad
     * @param	datosCotizacion	Datos de la cotización
     */
    private static void upsertQuote(Opportunity opp, MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion datosCotizacion, Product2 stcProductId) {
        Quote presupuestoHogar = MX_SB_VTS_utilityQuote.findUniqueQuote(datosCotizacion.datosIniciales.folioCotizacion, datosCotizacion.datosIniciales.folioTracking);
        if(String.isNotBlank(presupuestoHogar.Id)) {
            presupuestoHogar.Name = datosCotizacion.datosIniciales.folioCotizacion + ' ' + opp.Name;
            presupuestoHogar.ShippingStreet = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomAsegurado.calleCliente);
            presupuestoHogar.MX_SB_VTS_Colonia__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomAsegurado.coloniaCliente);
            presupuestoHogar.MX_SB_VTS_Numero_Exterior__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomAsegurado.numExtCliente);
            presupuestoHogar.MX_SB_VTS_Numero_Interior__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomAsegurado.numIntCliente);
            presupuestoHogar.ShippingPostalCode = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomAsegurado.cpCliente);
            presupuestoHogar.ShippingState = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomAsegurado.estadoCliente);
            presupuestoHogar.ShippingCity = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomAsegurado.ciudadCliente);
            presupuestoHogar.ShippingCountry = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomAsegurado.paisCliente);
            presupuestoHogar.MX_SB_VTS_Numero_de_Poliza__c =  MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosIniciales.numeroPoliza);
            presupuestoHogar.MX_SB_VTS_Movil_txt__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.celular);
            presupuestoHogar.MX_SB_VTS_Email_txt__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.email);
            presupuestoHogar = MX_SB_VTS_Vida_Service.quoteValidation(presupuestoHogar, datosCotizacion.datosIniciales.statusCotizacion, datosCotizacion.datosIniciales.folioCotizacion, opp);
        } else {
            final Id famProductId = stcProductId.MX_WB_FamiliaProductos__c;
            presupuestoHogar.MX_SB_VTS_ASO_FolioCot__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosIniciales.folioCotizacion);
            presupuestoHogar.MX_SB_VTS_Folio_Cotizacion__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosIniciales.folioCotizacion);
            presupuestoHogar.Name = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosIniciales.folioCotizacion) + ' ' + opp.Name;
            presupuestoHogar.OpportunityId = opp.Id;
            presupuestoHogar.ShippingStreet = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomAsegurado.calleCliente);
            presupuestoHogar.MX_SB_VTS_Colonia__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomAsegurado.coloniaCliente);
            presupuestoHogar.MX_SB_VTS_Numero_Exterior__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomAsegurado.numExtCliente);
            presupuestoHogar.MX_SB_VTS_Numero_Interior__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomAsegurado.numIntCliente);
            presupuestoHogar.ShippingPostalCode = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomAsegurado.cpCliente);
            presupuestoHogar.ShippingState = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomAsegurado.estadoCliente);
            presupuestoHogar.ShippingCity = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomAsegurado.ciudadCliente);
            presupuestoHogar.ShippingCountry = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomAsegurado.paisCliente);
            presupuestoHogar.MX_SB_VTS_Familia_Productos__c = famProductId;
            presupuestoHogar.MX_SB_VTS_Movil_txt__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.celular);
            presupuestoHogar.MX_SB_VTS_Email_txt__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.email);
            presupuestoHogar.MX_SB_VTS_Cupon__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosPrecio.cupon);
            presupuestoHogar.Pricebook2Id = MX_SB_VTS_utilityQuote.findPriceBook(stcProductId.Id);
            presupuestoHogar = MX_SB_VTS_Vida_Service.quoteValidation(presupuestoHogar, datosCotizacion.datosIniciales.statusCotizacion, datosCotizacion.datosIniciales.folioCotizacion, opp);
        }
        if(String.isBlank(presupuestoHogar.Id)) {
            presupuestoHogar.OwnerId = [Select Id, OwnerId from Opportunity where Id =:opp.Id].OwnerId;
        }
        try {
            Database.upsert(presupuestoHogar);
            upsertQuoteLineItem(presupuestoHogar, datosCotizacion, stcProductId.Id);
        } catch(DmlException dmlEx) {
            throw new DmlException('Error! '+dmlEx);
        }
    }

    /**
     * upsertQuoteLineItem Upsert a QuoteLineItem
     * @param	presupuesto Id presupuesto
     * @param	datosCotizacion	Datos de la cotización
     */
    private static void upsertQuoteLineItem(Quote presupuesto, MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion datosCotizacion, String stcProductId) {
        // Upsert Quote Line Item data
        final List<String> dataCot = new List<String>();
        dataCot.add(stcProductId);
        dataCot.add(datosCotizacion.datosPrecio.precioTotal);
        QuoteLineItem lstQuoli = new QuoteLineItem();
        for(QuoteLineItem quolit : [Select Id, PriceBookEntryId, UnitPrice, Quantity, MX_SB_VTS_Rotura_Cristales__c,
            MX_SB_VTS_Proteccion_Sismo__c, MX_WB_Descuento_con_cupones__c,
            MX_SB_VTS_Danos_Contenidos__c, MX_SB_VTS_Equipo_Electronico__c,
            MX_SB_VTS_Por_Robo__c, MX_SB_VTS_RespPrivada_Familiar__c,
            MX_SB_VTS_Precio_Anual__c, MX_SB_VTS_Frecuencia_de_Pago__c,
            MX_SB_VTS_Monto_Mensualidad__c, MX_SB_VTS_No_Pisos_Inmueble__c,
            MX_SB_VTS_Cercania_Mantos_Aquiferos__c, MX_SB_VTS_Piso_Habitado__c,
            MX_SB_VTS_Techo_Tabique_Ladrillo_Block__c, MX_SB_VTS_Muros_Tabique_Ladrillo_Block__c
            from QuoteLineItem where QuoteId =: presupuesto.Id AND MX_WB_Folio_Cotizacion__c =: presupuesto.MX_SB_VTS_ASO_FolioCot__c]) {
                lstQuoli = quolit;
        }
        if(String.isNotBlank(lstQuoli.Id)) {
            lstQuoli=getQuoteInit(lstQuoli, datosCotizacion);
        } else {
            QuoteLineItem quoli = new QuoteLineItem();
            quoli=getQuoteInit(quoli, datosCotizacion);
            quoli.QuoteId = presupuesto.Id;
            quoli.Quantity = 1;
            quoli.PriceBookEntryId = MX_SB_VTS_utilityQuote.getPricebookEntry(presupuesto, quoli, dataCot);
            quoli.MX_WB_Folio_Cotizacion__c = presupuesto.MX_SB_VTS_ASO_FolioCot__c;
            lstQuoli = quoli;
        }
        try {
            Database.upsert(lstQuoli);
        } catch(DmlException dmlEx) {
            throw new DmlException('Error: '+dmlEx);
        }
    }

    /**
     * getQuoteInit Obtine el QuoteInit
     * @param	datosCotizacion	Datos de la cotización
     * @return	quoli	Datos de cotización
     */
    private static QuoteLineItem getQuoteInit(QuoteLineItem quoli, MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion datosCotizacion) {
        quoli.MX_SB_VTS_Rotura_Cristales__c = MX_SB_VTS_rtwCotFillData.validDecimals(MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.tipoDeSeguro.roturaDeCristales));
        quoli.MX_SB_VTS_Proteccion_Sismo__c = String.isBlank(MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.tipoDeSeguro.conOSinSismo)) ? 'SIN' : datosCotizacion.tipoDeSeguro.conOSinSismo;
        quoli.MX_SB_VTS_Danos_Inmueble__c = MX_SB_VTS_rtwCotFillData.validDecimals(MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.tipoDeSeguro.danosInmueble));
        quoli.MX_SB_VTS_Danos_Contenidos__c = MX_SB_VTS_rtwCotFillData.validDecimals(MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.tipoDeSeguro.danosAContenidos));
        quoli.MX_SB_VTS_Equipo_Electronico__c = MX_SB_VTS_rtwCotFillData.validDecimals(MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.tipoDeSeguro.danosEquipoElectrico));
        quoli.MX_SB_VTS_Por_Robo__c = MX_SB_VTS_rtwCotFillData.validDecimals(MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.tipoDeSeguro.porRobo));
        quoli.MX_SB_VTS_RespPrivada_Familiar__c = MX_SB_VTS_rtwCotFillData.validDecimals(MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.tipoDeSeguro.responsabilidadPrivadaFamiliar));
        quoli.MX_SB_VTS_Precio_Anual__c = MX_SB_VTS_rtwCotFillData.validDecimals(MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.tipoDeSeguro.precioAnual));
        quoli.MX_SB_VTS_Frecuencia_de_Pago__c = MX_SB_VTS_Vida_Service.getPaymentFreq(datosCotizacion.tipoDeseguro.frequenciaPago, datosCotizacion.tipoDeseguro.cantidadDePagos);
        quoli.MX_SB_VTS_ValorHogar__c = Decimal.valueOf(MX_SB_VTS_rtwCotFillData.validDecimals(MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosIniciales.valorHogar)));
        quoli.MX_SB_VTS_Monto_Mensualidad__c = MX_SB_VTS_rtwCotFillData.validDecimals(MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosPrecio.precioParcialidades));
        quoli.MX_SB_VTS_Casa_o_Departamento__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosAdicionales.casaODepartamento);
        quoli.MX_SB_VTS_No_Pisos_Inmueble__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosAdicionales.noPisosInmueble);
        quoli.MX_SB_VTS_Piso_Habitado__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosAdicionales.pisoHabitado);
        quoli.UnitPrice = Decimal.valueOf(MX_SB_VTS_rtwCotFillData.validDecimals(MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosPrecio.precioTotal)));
        quoli.MX_WB_Descuento_con_cupones__c = String.isNotBlank(datosCotizacion.datosPrecio.cupon) ? 'Si': 'No';
        quoli.MX_SB_VTS_Cercania_Mantos_Aquiferos__c = MX_SB_VTS_utilityQuote.checkBooleanValues(datosCotizacion.datosAdicionales.cercaniaMantosAquiferos);
        quoli.MX_SB_VTS_Techo_Tabique_Ladrillo_Block__c = MX_SB_VTS_utilityQuote.checkBooleanValues(datosCotizacion.datosAdicionales.techoTabiqueLadrilloBlock);
        quoli.MX_SB_VTS_Muros_Tabique_Ladrillo_Block__c = MX_SB_VTS_utilityQuote.checkBooleanValues(datosCotizacion.datosAdicionales.murosTabiqueLadrilloBlock);
        return quoli;
    }
}