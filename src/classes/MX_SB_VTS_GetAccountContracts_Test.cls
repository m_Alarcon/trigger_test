/**
* BBVA - Mexico
* Jaime Terrats
* @Description Test class for aura component MX_SB_VTS_CMP_GetAccountContracts
* @Version 1.0
**/
@isTest
public class MX_SB_VTS_GetAccountContracts_Test {
    /** Variable de Apoyo: User email */
    final static String EMAILUSER = 'tGetAccCon@test.com';
    /** Variable de Apoyo: Account name */
    final static String ACCNAME = 'tAcc';
    /** Variable de Apoyo: Opportunity name */
    final static String OPPNAME = 'tOpp';
    /** Variable de Apoyo: Opportunity name invalid */
    final static String OPPNAMEINV = 'tOppInv';
    /** Variable de Apoyo: Opportunity name Unavailable Service */
    final static String OPPNAMEUS = 'tOppUs';
    /** Variable de Apoyo: Opportunity name Police Not Vigent */
    final static String OPPNAMENVIG = 'tOppNv';
    /** Variable de Apoyo: Opportunity name */
    final static String EMAILACC = 'tAccCon@test.com';
    /** Variable de Apoyo:  sFldStatusAct */
    final static String SFLDSTATUSACT = 'Activated';
    /** Variable de Apoyo: sFldStatsDraft */
    final static String SFLDSTATUSDRAFT = 'Draft';
    /** Variable de Apoyo: sFldFechaIni */
    final static String SFLDFECHAINI = '30/10/2020';
    /** Variable de Apoyo: sFldStardDate */
    final static String SFLDSTARTDATE = '2020-08-01';
    /** Variables de Apoyo: sFldReason */
    final static String SFLDREASON = 'Venta';
    /** VAriable de Apoyo: SFLDTSTOK */
    final static String SFLDTSTOK = 'Se obtuvieron los registros!';

    /** TestSetup: Mock Data */
    @TestSetup
    static void makeData() {
        tstMDataOppo();
        tstMDataOppoInv();
        tstMDataOppoUnS();
    }

    /** Coverage: happy path */
    @isTest
    static void testFetchActiveContractsByAccount() {
        List<Object> oContracts = null;
        final Id oppId = [Select Id from Opportunity where Name =: OPPNAME].Id;
        Test.startTest();
        oContracts = MX_SB_VTS_GetAccountContracts.fetchActiveContractsByAccount(oppId);
        Test.stopTest();
        System.assert(!oContracts.isEmpty(), SFLDTSTOK);
    }

    /** Coverage: Invalid Contract */
    @isTest
    static void testAccCintInv() {
        List <Object> oContract = null;
        final Id oppIdInv = [SELECT Id FROM Opportunity WHERE Name =: OPPNAMEINV].Id;
        Test.startTest();
        oContract = MX_SB_VTS_GetAccountContracts.fetchActiveContractsByAccount(oppIdInv);
        Test.stopTest();
        System.assert(!oContract.isEmpty(), SFLDTSTOK);
    }
    
    /** Coverage: Unavailable Service */
    @isTest
    static void testAccUnaServ() {
        List <Object> oContractUs = null;
        final Id oppIdUs = [SELECT Id FROM Opportunity WHERE Name =: OPPNAMEUS].Id;
        Test.startTest();
        oContractUs = MX_SB_VTS_GetAccountContracts.fetchActiveContractsByAccount(oppIdUs);
        Test.stopTest();
        System.assert(!oContractUs.isEmpty(), SFLDTSTOK);
    }
    
    /** Coverage: No Vigente */
    @isTest
    static void testAccNoVig() {
        List <Object> oContractNv = null;
        final Id oppIdNv = [SELECT Id FROM Opportunity WHERE Name =: OPPNAMEUS].Id;
        Test.startTest();
        oContractNv = MX_SB_VTS_GetAccountContracts.fetchActiveContractsByAccount(oppIdNv);
        Test.stopTest();
        System.assert(!oContractNv.isEmpty(), SFLDTSTOK);
    }

    /** Coverage: Creación de registros de prueba - Exitoso */
    public static void tstMDataOppo() {
        final User tUser = MX_WB_TestData_cls.crearUsuario('tGetAccCon', System.Label.MX_SB_VTS_ProfileAdmin);
        tUser.email = EMAILUSER;
        insert tUser;
        System.runAs(tUser) {
            final Account acc = MX_WB_TestData_cls.createAccount(ACCNAME, System.Label.MX_SB_VTS_PersonRecord);
            acc.PersonEmail = EMAILACC;
            acc.PersonMobilePhone = '5555555555';
            acc.AccountSource = 'Inbound';
            insert acc;
            final Product2 producto = MX_WB_TestData_cls.productNew(System.Label.MX_SB_VTS_Hogar);
            producto.isActive = true;
            insert producto;
            final Opportunity opp = MX_WB_TestData_cls.crearOportunidad(OPPNAME, acc.Id, tUser.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            opp.Reason__c = SFLDREASON;
            opp.Producto__c = System.Label.MX_SB_VTS_Hogar;
            opp.StageName = System.Label.MX_SB_VTS_COTIZACION_LBL;
            opp.TelefonoCliente__c = acc.PersonMobilePhone;
            opp.LeadSource = System.Label.MX_SB_VTS_OrigenCallMeBack;
            insert opp;
            final Contract con = MX_WB_TestData_cls.vtsCreateContract(acc.Id, tUser.Id, producto.Id);
            con.MX_WB_Oportunidad__c = opp.Id;
            con.MX_WB_noPoliza__c = '1234567890';
            con.MX_SB_SAC_NumeroPoliza__c = '1234567890';
            con.StartDate = Date.valueOf(SFLDSTARTDATE);
            con.MX_SB_VTS_FechaInicio__c = SFLDFECHAINI;
            con.MX_SB_VTS_FechaFin__c = '30/10/2024';
            con.Status = SFLDSTATUSDRAFT;
            con.MX_SB_SAC_EstatusPoliza__c = SFLDSTATUSACT;
            insert con;
            con.Status = SFLDSTATUSACT;
            update con;
        }
    }

    /** Coverage: Creación de registros de prueba - Invalidos */
    public static void tstMDataOppoInv() {
        final User tUserInv = MX_WB_TestData_cls.crearUsuario('tGetAccConInv', System.Label.MX_SB_VTS_ProfileAdmin);
        tUserInv.email = EMAILUSER;
        insert tUserInv;
        System.runAs(tUserInv) {
            final Account accInv = MX_WB_TestData_cls.createAccount(ACCNAME, System.Label.MX_SB_VTS_PersonRecord);
            accInv.PersonEmail = EMAILACC;
            accInv.PersonMobilePhone = '5555555555';
            accInv.AccountSource = 'Inbound';
            insert accInv;
            final Product2 productoInv = MX_WB_TestData_cls.productNew(System.Label.MX_SB_VTS_Hogar);
            productoInv.isActive = true;
            insert productoInv;
            final Opportunity oppInv = MX_WB_TestData_cls.crearOportunidad(OPPNAMEINV, accInv.Id, tUserInv.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            oppInv.Reason__c = SFLDREASON;
            oppInv.Producto__c = System.Label.MX_SB_VTS_Hogar;
            oppInv.StageName = System.Label.MX_SB_VTS_COTIZACION_LBL;
            oppInv.TelefonoCliente__c = accInv.PersonMobilePhone;
            oppInv.LeadSource = System.Label.MX_SB_VTS_OrigenCallMeBack;
            insert oppInv;
            final Contract conInv = MX_WB_TestData_cls.vtsCreateContract(accInv.Id, tUserInv.Id, productoInv.Id);
            conInv.MX_WB_Oportunidad__c = oppInv.Id;
            conInv.MX_WB_noPoliza__c = 'A123456789';
            conInv.MX_SB_SAC_NumeroPoliza__c = 'A123456789';
            conInv.StartDate = Date.valueOf(SFLDSTARTDATE);
            conInv.MX_SB_VTS_FechaInicio__c = SFLDFECHAINI;
            conInv.MX_SB_VTS_FechaFin__c = '30/10/2024';
            conInv.Status = SFLDSTATUSDRAFT;
            conInv.MX_SB_SAC_EstatusPoliza__c = 'Invalid';
            insert conInv;
            final Opportunity oppInvNv = MX_WB_TestData_cls.crearOportunidad(OPPNAMENVIG, accInv.Id, tUserInv.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            oppInvNv.Reason__c = SFLDREASON;
            oppInvNv.Producto__c = System.Label.MX_SB_VTS_Hogar;
            oppInvNv.StageName = System.Label.MX_SB_VTS_COTIZACION_LBL;
            oppInvNv.TelefonoCliente__c = accInv.PersonMobilePhone;
            oppInvNv.LeadSource = System.Label.MX_SB_VTS_OrigenCallMeBack;
            insert oppInvNv;
            final Contract conInvNv = MX_WB_TestData_cls.vtsCreateContract(accInv.Id, tUserInv.Id, productoInv.Id);
            conInvNv.MX_WB_Oportunidad__c = oppInvNv.Id;
            conInvNv.MX_WB_noPoliza__c = 'C123456789';
            conInvNv.MX_SB_SAC_NumeroPoliza__c = 'C123456789';
            conInvNv.StartDate = Date.valueOf(SFLDSTARTDATE);
            conInvNv.MX_SB_VTS_FechaInicio__c = SFLDFECHAINI;
            conInvNv.MX_SB_VTS_FechaFin__c = '30/10/2020';
            conInvNv.Status = SFLDSTATUSDRAFT;
            conInvNv.MX_SB_SAC_EstatusPoliza__c = SFLDSTATUSACT;
            insert conInvNv;
            conInvNv.Status = SFLDSTATUSACT;
            update conInvNv;
        }
    }

    /** Coverage: Creación de registros de prueba - Unavailable Service */
    public static void tstMDataOppoUnS() {
        final User tUserUnS = MX_WB_TestData_cls.crearUsuario('tGetAccConInv', System.Label.MX_SB_VTS_ProfileAdmin);
        tUserUnS.email = EMAILUSER;
        insert tUserUnS;
        System.runAs(tUserUnS) {
            final Account accUnS = MX_WB_TestData_cls.createAccount(ACCNAME, System.Label.MX_SB_VTS_PersonRecord);
            accUnS.PersonEmail = EMAILACC;
            accUnS.PersonMobilePhone = '5555555555';
            accUnS.AccountSource = 'Inbound';
            insert accUnS;
            final Product2 productoUns = MX_WB_TestData_cls.productNew(System.Label.MX_SB_VTS_Hogar);
            productoUns.isActive = true;
            insert productoUns;
            final Opportunity oppUnServ = MX_WB_TestData_cls.crearOportunidad(OPPNAMEUS, accUnS.Id, tUserUnS.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            oppUnServ.Reason__c = SFLDREASON;
            oppUnServ.Producto__c = System.Label.MX_SB_VTS_Hogar;
            oppUnServ.StageName = System.Label.MX_SB_VTS_COTIZACION_LBL;
            oppUnServ.TelefonoCliente__c = accUnS.PersonMobilePhone;
            oppUnServ.LeadSource = System.Label.MX_SB_VTS_OrigenCallMeBack;
            insert oppUnServ;
            final Contract conUnServ = MX_WB_TestData_cls.vtsCreateContract(accUnS.Id, tUserUnS.Id, productoUns.Id);
            conUnServ.MX_WB_Oportunidad__c = oppUnServ.Id;
            conUnServ.MX_WB_noPoliza__c = 'B123456789';
            conUnServ.MX_SB_SAC_NumeroPoliza__c = 'B123456789';
            conUnServ.StartDate = Date.valueOf(SFLDSTARTDATE);
            conUnServ.MX_SB_VTS_FechaInicio__c = SFLDFECHAINI;
            conUnServ.MX_SB_VTS_FechaFin__c = '30/10/2024';
            conUnServ.Status = SFLDSTATUSDRAFT;
            conUnServ.MX_SB_SAC_EstatusPoliza__c = 'Unavailable Service';
            insert conUnServ;
        }
    }
}