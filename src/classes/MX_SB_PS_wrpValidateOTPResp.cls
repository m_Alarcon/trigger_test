/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpValidateOTPResp
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
@SuppressWarnings('sf:ShortClassName, sf:ShortVariable')
public class MX_SB_PS_wrpValidateOTPResp {
/*Public property for wrapper*/
public data data {get;set;}
   /*public constructor subclass*/
	public MX_SB_PS_wrpValidateOTPResp() {
	 this.data = new data();
	}		
/*Public subclass for wrapper*/
public class data {
  /*Public property for wrapper*/
	public quotation quotation {get;set;}
	   /*public constructor subclass*/
	public data() {
	 this.quotation = new quotation();
	}		
}
  /*Public subclass for wrapper*/
public class quotation {
/*Public property for wrapper*/
public String id {get;set;}
/*public constructor subclass*/
	public quotation() {
	 this.id = '';
	}				
}

}