/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 11-26-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   11-26-2020   Eduardo Hernández Cuamatzi   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_SB_VTS_SendDocumentsRuc_Helper {

    /** String elementName */
    private final static String ELEMENTNAME = 'elementName';

    /**
    * @description Prepara los valores que se enviaran en las peticiones de documentos
    * @author Eduardo Hernández Cuamatzi | 11-26-2020 
    * @param oppData datos de Oportunidad para recuperar el producto
    * @param docType Tipo de documento a recuperar
    * @return Map<String, String> Mapa de valores requeridos para el body del request
    **/
    public static Map<String, String> fillDataDocs(Opportunity oppData, String docType) {
        final String oppProduct = oppData.Producto__c.toUpperCase();
        Map<String, String> dataDocs = new Map<String, String>();
        switch on oppProduct {
            when  'HOGAR SEGURO DINÁMICO' {
                dataDocs = fillDocsHSD(docType);
            }
            when  'VIDA SEGURA DINÁMICO' {
                dataDocs = fillDocsVSD(docType);
            }
        }
        return dataDocs;
    }

    /**
    * @description Dataos para documentos poliza, kit y condiciones HSD
    * @author Eduardo Hernández Cuamatzi | 11-26-2020 
    * @param docType Tipo de documento ah enviar
    * @return Map<String, String> Mapa de valores para petición
    **/
    public static Map<String, String> fillDocsHSD(String docType) {
        String version = '1.1';
        string eleName = 'IDRUC';
        string element1 = 'IDRUC';
        string nameElement = 'HOGAR';
        switch on docType {
            when  'policy' {
                version = '';
                eleName = 'proceso';
                element1 = 'idProceso';
            }
            when  'kit' {
                nameElement = 'GMDKWHO001';
            }
            when  'conditions' {
                nameElement = 'GMDCGHO001';
            }
        }
        return new Map<String, String>{
         'productId' => '',
         'version' => version,
         ELEMENTNAME => eleName,
         'element1' => element1,
         'nameElement' => nameElement
        };
    }

    /**
    * @description Valores para recuperar poliza, kit y condiciones
    * @author Eduardo Hernández Cuamatzi | 11-26-2020 
    * @param docType Tipo de documento que se requiere solicitar
    * @return Map<String, String> Datos de valores para petición
    **/
    public static Map<String, String> fillDocsVSD(String docType) {
        String productIdV = '';
        String versionV = '1';
        string elementNameV = 'canal';
        string element1V = 'nombreCanal';
        string nameElementV = 'BCOM';
        switch on docType {
            when  'policy' {
                versionV = '1.1';
                elementNameV = 'proceso';
                element1V = 'idProceso';
                nameElementV = 'VIDSE';
            }
            when  'kit' {
                productIdV = 'WKSEVIDI';
                elementNameV = 'canal';
            }
            when  'conditions' {
                productIdV = 'CSVDBCOM';
                elementNameV = 'canal';
            }
        }
        return new Map<String, String>{
         'productId' => productIdV,
         'version' => versionV,
         ELEMENTNAME => elementNameV,
         'element1' => element1V,
         'nameElement' => nameElementV
        };
    }

    /**
    * @description Genera elementos extra para el producto si es que aplica
    * @author Eduardo Hernández Cuamatzi | 11-26-2020 
    * @return Map<String, Object> Mapa de elemento array
    **/
    @SuppressWarnings('sf:DUDataflowAnomalyAnalysis')
    public static List<Object> processExtraElements(String proceso, String producto, List<Object> lstParent) {
        final List<Object> listContent = lstParent;
        List<Object> mapContents = new List<Object>();
        switch on proceso {
            when  'kit' {
                mapContents = processExtKit(producto);
                if(mapContents.isEmpty() == false) {
                    listContent.addAll(mapContents);
                }
            }
            when 'conditions' {
                mapContents = processExtConditions(producto);
                if(mapContents.isEmpty() == false) {
                    listContent.addAll(mapContents);
                }
            }
        }
        return listContent;
    }

    /**
    * @description Agrega elementos extra para kit
    * @author Eduardo Hernández Cuamatzi | 11-26-2020 
    * @param product producto a evaluar
    * @return List<Object> Lista de valores para petición de kit
    **/
    public static List<Object> processExtKit(String product) {
        final List<Object> mapContentsKit = new List<Object>();
        final String oppProduct = product.toUpperCase();
        switch on oppProduct {
            when  'VIDA SEGURA DINÁMICO' {
                mapContentsKit.add(fillFormatVSD());
                mapContentsKit.add(fillDataVitality());
            }
        }
        return mapContentsKit;
    }

    /**
    * @description Llena valores para el elemento Formato de VSD
    * @author Eduardo Hernández Cuamatzi | 11-26-2020 
    * @return Map<String, Object> Datos del elemento
    **/
    public static Map<String, Object> fillFormatVSD() {
        final Map<String, Object> mapContFormat = new Map<String, Object>();
        final List<Map<String, Object>> listData = new List<Map<String, Object>>();
        mapContFormat.put(ELEMENTNAME, 'formato');
        listData.add(fillElement('subproducto', '', ''));
        listData.add(fillElement('proceso', 'SEVIDI', ''));
        mapContFormat.put('listData', listData);
        return mapContFormat;
    }

    /**
    * @description LLena valores para peticion de documento kit
    * @author Eduardo Hernández Cuamatzi | 11-26-2020 
    * @return Map<String, Object> Mapa de valores de documentos en kit
    **/
    public static Map<String, Object> fillDataVitality() {
        final Map<String, Object> mapContFormat = new Map<String, Object>();
        final List<Map<String, Object>> listData = new List<Map<String, Object>>();
        mapContFormat.put(ELEMENTNAME, 'dataVitality');
        listData.add(fillElement('vitality', '', ''));
        listData.add(fillElement('gastos_funerarios', '', ''));
        listData.add(fillElement('apoyo_diario_hospitalizacion', '', ''));
        listData.add(fillElement('primer_diagnostico_cancer', '', ''));
        listData.add(fillElement('invalidez_total', '', ''));
        mapContFormat.put('listData', listData);
        return mapContFormat;
    }

    /**
    * @description Llena valores de elementos para listData
    * @author Eduardo Hernández Cuamatzi | 11-26-2020 
    * @param keyId valor en campo Id
    * @param nameValue Valor en campo name
    * @return Map<String, Object> 
    **/
    public static Map<String, Object> fillElement(String keyId, String nameValue, String specialName) {
        final Map<String, Object> mapData = new Map<String, Object>();
        mapData.put('id', keyId);
        if(String.isNotBlank(specialName)) {
            mapData.put(specialName, nameValue);
        } else {
            mapData.put('name', nameValue);
        }
        return mapData;
    }

    /**
    * @description Llena elementos para el peticion de condiciones
    * @author Eduardo Hernández Cuamatzi | 11-26-2020 
    * @param product Producto a evaluar
    * @return List<Object> Lista de valores ah agregar
    **/
    public static List<Object> processExtConditions(String product) {
        final List<Object> mapContentsCon = new List<Object>();
        final String oppProduct = product.toUpperCase();
        switch on oppProduct {
            when  'VIDA SEGURA DINÁMICO' {
                mapContentsCon.add(fillFormatVSD());
            }
        }
        return mapContentsCon;
    }
}