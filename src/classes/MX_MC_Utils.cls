/**
 * @File Name           : MX_MC_Utils.cls
 * @Description         :
 * @Author              : eduardo.barrera3@bbva.com
 * @Group               :
 * @Last Modified By    : eduardo.barrera3@bbva.com
 * @Last Modified On    : 17/03/2021, 15:39:09 PM
 * @Modification Log    :
 * =============================================================================
 * Ver          Date                     Author                     Modification
 * =============================================================================
 * 1.0      08/05/2020, 13:45:41 PM   eduardo.barrera3@bbva.com      Initial Version
 * 1.1      17/03/2021, 15:39:09 PM   eduardo.barrera3@bbva.com      Add logic for case update when conversation is answered
 **/
public without sharing class MX_MC_Utils {
    /**
     * @Method MX_MC_Utils
     * @Description Singletons
    **/
    @TestVisible private MX_MC_Utils() {
    }

   /**
    * @Description Returns a Boolean value based on draft messages
    * @author eduardo.barrera3@bbva.com | 06/27/2020
    * @param mxMessageList
    * @return Boolean
    **/

    public static Boolean hasDraftMessages (List<MX_MC_MTWConverter_Service_Helper.Message> mxMessageList) {

        Boolean hasDraftMessage = false;
        for (MX_MC_MTWConverter_Service_Helper.Message mxMessage : mxMessageList) {
            if (hasDraftMessage) {
                break;
            }
            if (mxMessage.isDraft) {
                hasDraftMessage = true;
            }
        }

        return hasDraftMessage;
    }

    /**
    * @Description Returns if a conversation is Draft
    * @author eduardo.barrera3@bbva.com | 07/23/2020
    * @param mxMessageList
    * @return Boolean
    **/
    public static Boolean isConvDraft (List<MX_MC_MTWConverter_Service_Helper.Message> mxMessageList) {

        Boolean allDraftMessage = true;
        for (MX_MC_MTWConverter_Service_Helper.Message mxMessage : mxMessageList) {
            if (!mxMessage.isDraft) {
                allDraftMessage = false;
                break;
            }
        }
        return allDraftMessage;
    }

    /**
    * @Description Returns customer code from provided Account
    * @author eduardo.barrera3@bbva.com | 06/27/2020
    * @param acc
    * @return String
    **/
    public static String getCustomerCode (Account acc) {
        return (String) acc.get(mycn.MC_Settings.readSetting('CustomerCode'));
    }

    /**
    * @Description Returns created ID resource from header location
    * @author eduardo.barrera3@bbva.com | 08/05/2020
    * @param headerLoc
    * @return String
    **/
    public static String getHeaderLocation (String headerLoc) {
        return headerLoc.substring(headerLoc.lastIndexOf('/') + 1);
    }

    /**
    * @Description Case field update in MC case records
    * @author eduardo.barrera3@bbva.com | 17/03/2021
    * @param lstNewCase, mapOldCase
    * @return void
    **/
    public static void checkData (List<Case> lstNewCase, Map<Id,Case> mapOldCase) {
        final List<Case> filteredCases = filterCasesByRecType(lstNewCase);
        if(filteredCases.isEmpty() == false) {
            for (SObject sObj :  filteredCases) {
                final Case newCase = (Case)sObj;
                final Case oldCase = (Case)mapOldCase.get(newCase.id);
                if (isChanged(newCase, oldCase, 'Status') && newCase.Status == 'Closed') {
                    newCase.mycn__MC_ActiveEntitlement__c = false;
                }
            }
        }
    }

    /**
    * @Description Filter cases by MC RecordType
    * @author eduardo.barrera3@bbva.com | 17/03/2021
    * @param lstNewCase
    * @return List<Case>
    **/
    public static List<Case> filterCasesByRecType (List<Case> lstNewCase) {
        final List<Case> casesToCheck = new List<Case>();
        final String caseRTName = mycn.MC_Settings.readSetting('MC_RecordType');
        final String recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(caseRTName).getRecordTypeId();
        final List<String> rtIds = new List<String>{recTypeId};
        for (Case newCase : lstNewCase) {
            if(rtIds.contains(newCase.RecordTypeId)) {
                    casesToCheck.add(newCase);
            }
        }
        return casesToCheck;
    }

    /**
    * @Description Check if case field has changed
    * @author eduardo.barrera3@bbva.com | 17/03/2021
    * @param newObject, oldObject, fieldName
    * @return boolean
    **/
    public static boolean isChanged (Case newObject, Case oldObject, String fieldName) {
        boolean hasChanged = false;
        try {
            final Object firstValue = newObject.get(fieldName);
            final Object secondValue = oldObject.get(fieldName);
            if (firstValue == null && secondValue != null || firstValue != null && secondValue == null) {
                hasChanged = true;
            } else if (firstValue == null && secondValue == null) {
                hasChanged = false;
            } else {
                hasChanged = firstValue != secondValue;
            }
        } catch (Exception e) {
            hasChanged = false;
        }
        return hasChanged;
    }
}