/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Template_Service
* @Author   	Héctor Saldaña | hectorisrael.saldana.contractor@bbva.com
* @Date     	Created: 2021-01-20
* @Description 	BPyP Minuta Tamplate Service Layer - Provides data to those resources (Mostly Visualforce pages)
* that need data from the actual Visit (dwp_kitv__Visit__c) to fill in the email template.
* @Changes
* 2021-22-02		Héctor Saldaña	  	New methods added: replaceInnerTags, checkVariableTags, checkStaticTags, checkProfileTags
 * 2021-05-03       Héctor Saldaña      Removed field rom queryFields from getCatalogoMinuta method
 * 2021-03-08       Edmundo Zacarías    Added new logic for regarding new flow during email construction
*/
public without sharing class MX_BPP_Minuta_Template_Service {

	/**Constructor method*/
    @TestVisible
	private MX_BPP_Minuta_Template_Service() {}

	/**
	* @Description Method that retrieves reesources from server to be displayed on the pages
	* @param resourcesNames List<String> - Resources names needed
    * @return Map<String,String> A key-value map of the resources
    **/
	public static Map<String, String> getImageResources(List<String> resourcesNames) {
		return MX_BPP_Minuta_Utils.generateURLs(resourcesNames);
	}

	/**
	* @Description Method that retrieves User details based on the Id received
	* @param ownerId Id from the visit's owner
    * @return User The user details
    **/
	public static User getUserDetails (String ownerId) {
		String conditionField, queryFields;
		conditionField = ' Id IN: setId ';
		queryFields = 'Id, CompanyName, Name, Oficina__c, Title, Email, MobilePhone, Manager.Name, Manager.Title, Manager.Email, Manager.MobilePhone, Manager.CompanyName';
		final Set<Id> usersIds = new Set<Id>();
		usersIds.add(ownerId);
		final List<User> listUsers = MX_RTL_User_Selector.resultQueryUser(queryFields, conditionField, true , usersIds);

		return listUsers[0];
	}

	/**
	* @Description Method that retrieves User details based on the Id received
	* @param ownerId Id from the visit's owner
    * @return User The user details
    **/
	public static MX_BPP_CatalogoMinutas__c getCatalogoMinuta (String catalogoId) {
		List<MX_BPP_CatalogoMinutas__c> resultCatalogo = new List<MX_BPP_CatalogoMinutas__c>();
		String queryFields = '';
		final List<String> lstIdCatalogo = new List<String>();
		lstIdCatalogo.add(catalogoId);
		queryFields = 'Id, Name, MX_Acuerdos__c, MX_Saludo__c, MX_Contenido__c, MX_CuerpoCorreo__c, MX_Despedida__c, MX_Firma__c, MX_ImageHeader__c, MX_Asunto__c';
		resultCatalogo = MX_RTL_CatalogoMinuta_Selector.getRecordsById( queryFields, lstIdCatalogo);

		return resultCatalogo[0] == null ? null : resultCatalogo[0];
	}

	/**
    * @Description Method that generates the elements that are sent in the mail as well as
    * send the mail to the recipients
    * @param visitObj Object with the general details of the visit
    * @param myCS Custom Settings that will give the configuration for the shipment
    * @return MinutePagerWrapper A class with the necessary elements to work on the view
    **/
	public static Messaging.SendEmailResult[] sendTemplateEmail(dwp_kitv__Visit__c visitObj, dwp_kitv__Template_for_type_of_visit_cs__c myCS,List<ContentVersion> documents, String statusOldVisit,
																User userObj, Boolean vfError, PageReference currntPageRef, MX_BPP_CatalogoMinutas__c currentCatalogo) {

		dwp_kitv.Minute_Generator_Ctrl.updateStatusVisit(visitObj,'05');
		final dwp_kitv__Visit__c visitObjNew=dwp_kitv.Minute_Generator_Ctrl.getVisitObj(visitObj.Id);
		final MX_BPP_Minuta_Template_Service_Helper helper = new MX_BPP_Minuta_Template_Service_Helper('BODY NULL', documents);
		helper.saveTemplateAsDocument(visitObjNew, documents, myCS, userObj, currntPageRef, currentCatalogo);

		final Messaging.SendEmailResult[] results = helper.sendMail(myCS, visitObjNew, userObj, currentCatalogo);
		if (results[0].isSuccess() && !vfError) {
			dwp_kitv.Minute_Generator_Ctrl.createActivity(visitObjNew.Id, myCS);
			MX_BPP_Minuta_Service.checkClientEmail(visitObjNew, helper.emailL);
		} else {
			dwp_kitv.Minute_Generator_Ctrl.updateStatusVisit(visitObjNew,statusOldVisit);
		}
		return results;
	}

    /**
    * @Description Calls every method to work with inner tags
    * @param 
    * @Return NA
    **/
    public static void replaceInnerTags(MX_BPP_CatalogoMinutas__c catalogo, Id visitId) {
        checkVariableTags(catalogo);
        checkStaticTags(catalogo, visitId);
        checkProfileTags(catalogo);
    }
    
    /**
    * @Description Process inner tags over Catalago record
    * @Params NA
    * @Return NA
    **/
    public static void checkVariableTags(MX_BPP_CatalogoMinutas__c catalogoMinuta) {
        final String textArray = MX_BPP_Minuta_Utils.getParameter('textArray').unescapeHtml4();
        final List<MX_BPP_MinutaWrapper.componenteText> lsttext = (List<MX_BPP_MinutaWrapper.componenteText>)System.JSON.deserialize(textArray, List<MX_BPP_MinutaWrapper.componenteText>.class);
        if(!lsttext.isEmpty()) {
            MX_BPP_Minuta_Template_Service_Helper.replaceDynamicTags(catalogoMinuta, lsttext);
        }

        final String seleccionArray = MX_BPP_Minuta_Utils.getParameter('seleccionArray').unescapeHtml4();
        final List<MX_BPP_MinutaWrapper.componentePicklist> pickListLst = (List<MX_BPP_MinutaWrapper.componentePicklist>)System.JSON.deserialize(seleccionArray, List<MX_BPP_MinutaWrapper.componentePicklist>.class);
        if(!pickListLst.isEmpty()) {
            MX_BPP_Minuta_Template_Service_Helper.replaceDynamicTags(catalogoMinuta, pickListLst);
        }

        final String switchArray = MX_BPP_Minuta_Utils.getParameter('switchArray').unescapeHtml4();
        final List<MX_BPP_MinutaWrapper.componenteSwitch> switchLst = (List<MX_BPP_MinutaWrapper.componenteSwitch>)System.JSON.deserialize(switchArray, List<MX_BPP_MinutaWrapper.componenteSwitch>.class);
        if(!switchLst.isEmpty()) {
            MX_BPP_Minuta_Template_Service_Helper.replaceDynamicTags(catalogoMinuta, switchLst);
        }

    }

    /**
    * @description Process inner static tags (the ones registered as Custom Metadata Types records) over Catalago record
    * @Param MX_BPP_CatalogoMinutas__c catalogoMinuta, Id visitId
    * @return void
    **/
    public static void checkStaticTags(MX_BPP_CatalogoMinutas__c catalogoMinuta, Id visitId) {
        final Pattern tagPattern = Pattern.compile('(\\{+([a-zA-Z]+)\\}+)');
        final Matcher tagMatcher = tagPattern.matcher(catalogoMinuta.MX_Saludo__c + ' ' + catalogoMinuta.MX_Contenido__c + ' ' + catalogoMinuta.MX_Despedida__c + ' ' + catalogoMinuta.MX_Firma__c + ' ' + catalogoMinuta.MX_CuerpoCorreo__c);
        final Set<String> tagsEncontrados = new Set<String>();
        while(tagMatcher.find()) {
            if(tagMatcher.group(2).contains('Acuerdos')) {
                MX_BPP_Minuta_Template_Service_Helper.procesarAcuerdos(catalogoMinuta, tagMatcher.group(1), visitId);
            } else {
                tagsEncontrados.add(tagMatcher.group(2));
            }
        }
        if(!tagsEncontrados.isEmpty()) {
            MX_BPP_Minuta_Template_Service_Helper.getMetadataTags(tagsEncontrados, catalogoMinuta, visitId);
        }
    }
    
    /**
    * @Description Process tags which are displayed based on the user profile name over Catalago record
    * @Param MX_BPP_CatalogoMinutas__c catalogoMinuta
    * @return void
    **/
    public static void checkProfileTags(MX_BPP_CatalogoMinutas__c catalogoMinuta) {
        final Pattern patronTag = Pattern.compile('((\\{+([a-zA-Z]+)\\}+)([^{}][a-zA-ZÀ-ÿ\\u00f1\\u00d1\\s:.<>/,]+)(\\{+([a-zA-Z]+)\\}+))');
        final Matcher matches = patronTag.matcher(catalogoMinuta.MX_Contenido__c);
        final Map<String,String> maptags = new Map<String, String>();
        while(matches.find()) {
            maptags.put(matches.group(3), matches.group(1));
        }

        if(!maptags.keySet().isEmpty()) {
            MX_BPP_Minuta_Template_Service_Helper.replaceProfileTags(catalogoMinuta, maptags);
        }
    }
}