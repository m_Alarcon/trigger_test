@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_PS_Plan_Familiar_h5h_Ctrl_Test
* Autor: Juan Carlos Benitez Herrera
* Proyecto: SB Presuscritos - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_PS_Plan_Familiar_h5h_Ctrl

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         09/07/2020    Juan Carlos Benitez Herrera              Creación
* 1.1         10/02/2021    Juan Carlos Benitez Herrera    Se añade methodo testUpdbenf
* -----------------------------------------------------------------------------------------------
*/
public class MX_SB_PS_Plan_Familiar_h5h_Ctrl_Test {
    /** Nombre de usuario Test */
    final static String NAME = 'Magdalena';
    /** Apellido Paterno */
    Final static STRING LNAME= 'Hernandez';
    
    @TestSetup
    static void createData() {
	    MX_SB_PS_OpportunityTrigger_test.makeData();
            final Account accTst = [Select Id from Account limit 1];
            accTst.FirstName =NAME;
	        accTst.LastName=LNAME;
            accTst.Tipo_Persona__c='Física';
            update accTst;
            Final Opportunity oppTest =[Select id from Opportunity where AccountId=:accTst.Id];
            oppTest.Name= NAME;
            oppTest.StageName=System.Label.MX_SB_PS_Etapa1;
            update oppTest;
            Final Quote quotTest=[Select Id from Quote where OpportunityId=:oppTest.Id limit 1];
            quotTest.Name=NAME;
            update quotTest;
            final List<MX_SB_VTS_Beneficiario__c> listBenefs = new List<MX_SB_VTS_Beneficiario__c>();
            for(Integer j = 10; j<10; j++) {
                Final MX_SB_VTS_Beneficiario__c benef = new MX_SB_VTS_Beneficiario__c();
                benef.RecordType.DeveloperName='MX_SB_MLT_Beneficiario';
                benef.Name = NAME +'_'+j;
                benef.MX_SB_VTS_Quote__c=quotTest.Id;
                benef.MX_SB_MLT_NombreContacto__c =accTst.PersonContactId;
                benef.MX_SB_SAC_Email__c = NAME+'_'+j+'@'+'bbva.com';
                listBenefs.add(benef);
            }
            Database.insert(listBenefs);
        }
    
    @isTest
    static void testgetintN() {
        Final Id quotId =[SELECT Id from Quote where Name=:NAME limit 1].Id;
        test.startTest();
       		MX_SB_PS_Plan_Familiar_h5h_Ctrl.getintN(quotId);
            system.assert(true,'Se han encontrado n cantidad de beneficiarios');
        test.stopTest();
    }
    @isTest
    static void testinsrtBenef() {
        Final Id ideC= [SELECT PersonContactId from Account where FirstName=:NAME limit 1].PersonContactId;
        Final Id ideQ= [SELECT Id from Quote where Name=:NAME limit 1].id;
        Final MX_SB_VTS_Beneficiario__c benefObh= new MX_SB_VTS_Beneficiario__c();
        benefObh.Name= NAME;
        benefObh.MX_SB_VTS_AMaterno_Beneficiario__c= LNAME;
        benefObh.MX_SB_VTS_Quote__c = ideQ;
		benefObh.MX_SB_MLT_NombreContacto__c=ideC;
        test.startTest();
        	MX_SB_PS_Plan_Familiar_h5h_Ctrl.insrtBenef(benefObh);
            system.assert(true,'Se ha encontrado creado beneficiario correctamente');
        test.stopTest();
    }
    @isTest
    static void testupsrtAcc() {
		Final Account pAcc =new Account();
        pAcc.FirstName=NAME;
        pAcc.LastName=LNAME;
        pAcc.Tipo_Persona__c='Física';
        pAcc.RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
        pAcc.Apellido_materno__pc=' Hernandez de Benitez';
        pAcc.MX_RTL_CURP__pc='BEHJ930405MDFNRN00';
        test.startTest();
        	MX_SB_PS_Plan_Familiar_h5h_Ctrl.upsrtAcc(pAcc);
            system.assert(true,'Se han actualizado la cuenta exitosamente');
        test.stopTest();
    }
    @isTest
    static void tstLoadOppF() {
        Final String oppId =[SELECT Id from Opportunity where Name=:NAME].Id;
		test.startTest();
        	MX_SB_PS_Plan_Familiar_h5h_Ctrl.LoadOppF(oppId);
            system.assert(true,'Se ha encontrado la oportunidad deseada');
        test.stopTest();
    }
       @isTest
    static void testsrchBenefs() {
        test.startTest();
	        Final Id ideC= [SELECT PersonContactId from Account where FirstName=:NAME limit 1].PersonContactId;
    	    Final Id ideQ= [SELECT Id from Quote where Name=:NAME limit 1].id;
        	Final MX_SB_VTS_Beneficiario__c benefObh= new MX_SB_VTS_Beneficiario__c(Name=NAME,MX_SB_VTS_AMaterno_Beneficiario__c= LNAME,MX_SB_VTS_Quote__c = ideQ,MX_SB_MLT_NombreContacto__c=ideC);
        	insert benefObh;
	        MX_SB_PS_Plan_Familiar_h5h_Ctrl.getbenefbyId(benefObh.Id);
            system.assert(true,'Se ha encontrado el beneficiario');
        test.stopTest();
    }
    @isTest
    static void testUpdbenf() {
        Final List<MX_SB_VTS_Beneficiario__c> benefDat =[SELECT Id from MX_SB_VTS_Beneficiario__c limit 1];
        benefDat[0].MX_SB_SAC_Email__c ='Luffy@gmail.com';
        test.startTest();
        	MX_SB_PS_Plan_Familiar_h5h_Ctrl.updateBenefData(benefDat);
        system.assertEquals(benefDat[0].MX_SB_SAC_Email__c,'Luffy@gmail.com','Se ha actualizado correctamente');
        test.stopTest();
    }
    
}