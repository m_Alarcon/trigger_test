/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase test de aclaraciones - trigger, ctrl y selector

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     20/05/2020      Roberto Isaac Soto Granados           Creación Clase
*****************************************************************************************/
@isTest
private class Aclaracion_test {
    /*Usuario de pruebas*/
    private static User testUserAcla = new User();
    /*Caso de pruebas*/
    private static Case testCaseAcl = new Case();
    /*Aclaración de pruebas*/
    private static BPyP_Aclaraciones__c testAclaraci = new BPyP_Aclaraciones__c();
    /*cuenta de pruebas*/
    private static Account testAccount = new Account();
    /*String para folios*/
    private static String testCodeA = '1234';

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testUserAcla = UtilitysDataTest_tst.crearUsuario('testUser', 'BPyP Estandar', 'BPYP BANQUERO BANCA PERISUR');
        testUserAcla.Title = 'Privado';
        insert testUserAcla;

        System.runAs(testUserAcla) {
            testAccount.LastName = 'testAcc';
            testAccount.FirstName = 'testAcc';
            testAccount.OwnerId = testUserAcla.Id;
            testAccount.No_de_cliente__c = testCodeA;
            insert testAccount;
            testCaseAcl.OwnerId = testUserAcla.Id;
            testCaseAcl.Status = 'Nuevo';
            testCaseAcl.MX_SB_SAC_Folio__c = testCodeA;
            testCaseAcl.Description = 'Description';
            testCaseAcl.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_EU_Case_Apoyo_General'].Id;
            insert testCaseAcl;
        }
    }

    /*Ejecuta la acción para cubrir Methods de la clase*/
    static testMethod void getCasesTest() {
        testUserAcla = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        testCaseAcl = [SELECT Id, MX_SB_SAC_Folio__c FROM Case WHERE MX_SB_SAC_Folio__c = '1234' LIMIT 1];
        System.runAs(testUserAcla) {
            Test.startTest();
                testAclaraci.BPyP_Producto__c = 'TDC';
            	testAclaraci.BPyP_Folio__c = testCodeA;
            	testAclaraci.BPyP_Caso__c = testCaseAcl.Id;
                testAclaraci.BPyP_NumeroCliente__c = testCodeA;
                testAclaraci.BPyP_Tarjeta__c = '9876';
            	insert testAclaraci;
            Test.stopTest();
        }
        System.assert(!String.isBlank(testAclaraci.Id), 'Aclaración creada correctamente');
    }

}