/**
 * @File Name          : MX_SB_MLT_ResumenSiniestro_Ctrl.cls
 * @Description        :
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 5/27/2020, 02:07:22 AM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/27/2020    Juan Carlos Benitez         Initial Version
**/

@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_SB_MLT_TroncoComunVida_Ctrl {
   	/*
    * @description Obtiene el tipo de registro
    * @param devname
    * @return Id RecType
    */
    @auraEnabled
	public static Id getRecordType(String devname) {
        return MX_RTL_Siniestro_Service.getRecordTId(devname);
    }
	/*
    * @description actualiza/inserta el sin
    * @param  sinIn
    * @return object Siniestro__c
	*/
	@AuraEnabled
    public static Siniestro__c upSertsini(Siniestro__c sinIn) {
		return MX_RTL_Siniestro_Service.upsertSini(sinIn);
    }
    /*
    * @description Relaciona la Tarea con el siniestro/ Crea una cita
    * @param  taskObject
    * @param  taskObject
	*/
    @AuraEnabled
    public static void whatIdSin(Task taskPC, String taskType) {
	   MX_RTL_Task_Service.updateTask(taskPC,taskType);
    }
	/*
    * @description Realiza la busqueda de Póliza en contratos
    * @param  policy
    * @return String ID del Contrato donde se localizo dicha póliza
	*/
	@AuraEnabled
    public static List<Contract> checkPolicy(String policy) {
        return MX_RTL_Contract_Service.getContractIdBySACnPoliza(policy);
    }
}