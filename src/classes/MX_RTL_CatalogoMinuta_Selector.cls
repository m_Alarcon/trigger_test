/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RTL_CatalogoMinuta_Selector
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2021-01-19
* @Description 	Selector for MX_BPP_CatalogoMinutas__c Object
* @Changes
*  
*/
public without sharing class MX_RTL_CatalogoMinuta_Selector {
    
    /** Constructor */
    @TestVisible
    private MX_RTL_CatalogoMinuta_Selector() {}
    
    /**
    * @description
    * @author Edmundo Zacarias Gómez
    * @param String queryfields, List<String> listTVisita
    * @return List<MX_BPP_CatalogoMinutas__c>
    **/
    public static List<MX_BPP_CatalogoMinutas__c> getRecordsByTipoVisita(List<String> listTVisita) {
        String query;
        query ='SELECT Id, Name, MX_Acuerdos__c, MX_Opcionales__c, MX_Adjuntos__c, MX_HasPicklist__c, MX_HasText__c, MX_HasCheckbox__c, MX_Saludo__c FROM MX_BPP_CatalogoMinutas__c WHERE MX_TipoVisita__c IN: listTVisita';
        return Database.query(String.escapeSingleQuotes(query));
    }

    /**
    * @description
    * @author Héctor Saldaña
    * @param String queryfields, List<String> listTVisita
    * @return List<MX_BPP_CatalogoMinutas__c>
    **/
    public static List<MX_BPP_CatalogoMinutas__c> getRecordsById(String queryfields, List<String> listTVisita) {
        final String query ='SELECT '+ queryfields +' FROM MX_BPP_CatalogoMinutas__c WHERE Id IN: listTVisita';
        return Database.query(String.escapeSingleQuotes(query));
    }

}