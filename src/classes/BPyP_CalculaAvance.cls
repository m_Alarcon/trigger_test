/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase que calcula el avance de la contactabilidad

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     12/02/2020      Roberto Isaac Soto Granados           Creación Clase
*   1.1     15/05/2020      Roberto Isaac Soto Granados     without sharing para acceder a usuarios
*   1.2     24/05/2020      Roberto Isaac Soto Granados     Validación en query para evitar recursividad
*****************************************************************************************/
public without sharing class BPyP_CalculaAvance {
    /*Contructor clase BPyP_CalculaAvance*/
    @TestVisible
    private BPyP_CalculaAvance() {}

    /*Método que realiza el cálculo del avance*/
    @InvocableMethod
    public static void updateAdvance(List<String> userId) {
        final List<AggregateResult> accsByUser = [SELECT Id, COUNT(Name) FROM Account WHERE OwnerId IN: userId AND
                                             Id IN (SELECT dwp_kitv__account_id__c FROM dwp_kitv__Visit__c
                                                   WHERE dwp_kitv__visit_status_type__c = '05' AND OwnerId IN: userId
                                                   AND CreatedDate = THIS_MONTH)
                                             AND (RecordType.DeveloperName = 'MX_BPP_PersonAcc_NoClient'
                                             OR (BPyP_convertido__c = true AND BPyP_fechaConversion__c = THIS_MONTH))
                                             GROUP BY Id];

        final List<User> userAdvance = [SELECT Id, BPyP_AvanceMetaProspectos__c FROM User WHERE Id IN: userId];
        userAdvance[0].BPyP_AvanceMetaProspectos__c = accsByUser.size();

        update userAdvance;
        updateOppWithVisit(userId);
    }

    /*Método que actualiza oportunidades*/
    public static void updateOppWithVisit(List<String> userId) {
        final List<Id> oppsIds = new List<Id>();
        final List<Opportunity> updateOpps = new List<Opportunity>();
        for(dwp_kitv__Visit_Topic__c topicsWithOpp : [SELECT dwp_kitv__opportunity_id__c FROM dwp_kitv__Visit_Topic__c
                                            		  WHERE (NOT(dwp_kitv__opportunity_id__c = NULL)) AND dwp_kitv__visit_id__c IN
                                                      (SELECT Id FROM dwp_kitv__Visit__c WHERE OwnerId IN: userId AND
                                                       dwp_kitv__visit_status_type__c = '05' AND CreatedDate = THIS_MONTH)]) {
             oppsIds.add(topicsWithOpp.dwp_kitv__opportunity_id__c);
        }

        for(Opportunity opp : [SELECT Id, BPyP_OppConVisita__c FROM Opportunity WHERE Id IN: oppsIds AND
                               StageName NOT IN ('Cerrada ganada', 'Descartada / Rechazada') AND
                               RecordType.DeveloperName LIKE '%BPyP%' AND BPyP_OppConVisita__c = false]) {
            opp.BPyP_OppConVisita__c = true;
            updateOpps.add(opp);
        }

        if(!updateOpps.isEmpty()) {
            update updateOpps;
        }
    }

}