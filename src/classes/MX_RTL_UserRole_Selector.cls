/**
* @File Name          : MX_RTL_CampaignMember_Selector.cls
* @Description        :
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 01/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      01/06/2020            Gabriel Garcia Rojas          Initial Version
**/
public without sharing class MX_RTL_UserRole_Selector {

    /**Constructor */
    @testVisible
    private MX_RTL_UserRole_Selector() { }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param stringName
    * @return Map<Id,UserRole>
    **/
    public static Map<Id,UserRole> getUserRoleLikeName(String stringName) {
        return new Map<Id, UserRole>([SELECT Id FROM UserRole WHERE Name LIKE: stringName]);
    }
}