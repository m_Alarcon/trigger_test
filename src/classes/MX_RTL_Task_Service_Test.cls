@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_RTL_Task_Service_Test
* Autor: Juan Carlos Benitez Herrera
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_RTL_Task_Service

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         26/05/2020   Juan Carlos Benitez Herrera               Creación
* 1.1         07/09/2020   Marco Cruz             		 Add methods to cover the percent
* 1.1         10/09/2020   Marco Cruz             		 Add method to cover the percent to upsert a list
* -----------------------------------------------------------------------------------------------
*/
public class MX_RTL_Task_Service_Test {
    /*
	* var String Valor Subramo Ahorro
	*/
    public static final String SUBRAMO ='Fallecimiento';

	/** Nombre de usuario Test */
    final static String NAME = 'Panchito Lopez';
    
    /** current user id */
    final static String USR_ID = UserInfo.getUserId();
    
    /*Usuario de pruebas*/
    private static User userTest = new User();
    
    /** boolean for assert if a record is upserted*/
    private static Boolean isUpsert = true;

    /**
     * @description Test SetUp
     * Author: Juan Carlos Benitez Herrera
	 */
    @TestSetup
    static void createData() {
        MX_SB_PS_OpportunityTrigger_test.makeData();
        Final String profVida = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User userVida = MX_WB_TestData_cls.crearUsuario(NAME, profVida);  
        insert userVida;
        System.runAs(userVida) {
            final String recdTpeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Proveedores_MA).getRecordTypeId();
            final Account accVidaTst = new Account(RecordTypeId = recdTpeId, Name=NAME, Tipo_Persona__c='Física');
            insert accVidaTst;

            final Contract contractVida = new Contract(AccountId = accVidaTst.Id, MX_SB_SAC_NumeroPoliza__c='policyTest');
            insert contractVida;
            Final List<Siniestro__c> sinInsert = new List<Siniestro__c>();
            for(Integer i = 0; i < 10; i++) {
                Final Siniestro__c nSin = new Siniestro__c();
                nSin.MX_SB_SAC_Contrato__c = contractVida.Id;
                nSin.MX_SB_MLT_NombreConductor__c = NAME + '_' + i;
                nSin.MX_SB_MLT_APaternoConductor__c = 'LastName for '+NAME;
                nSin.MX_SB_MLT_AMaternoConductor__c = 'LastName2 for '+NAME;
                nSin.MX_SB_MLT_Fecha_Hora_Siniestro__c =date.valueof('2020-03-27T22:04:00.000+0000');
                nSin.MX_SB_MLT_Telefono__c = '5534253647';
                nSin.MX_SB_MLT_SubRamo__c = SUBRAMO;
                nSin.TipoSiniestro__c = 'Siniestros';
                nSin.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_creacion;
                nSin.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoVida).getRecordTypeId();
                sinInsert.add(nSin);
            }
            Database.insert(sinInsert);
            
        	Final List<Task> tskInsert = new List<Task>();
            	for(Integer i = 0; i < 10; i++) {
	                Final Task ntsk = new Task();
    	            ntsk.Telefono__c = '5534253647';
                    ntsk.whatId =sinInsert[i].Id;
        	        ntsk.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_PureCloud').getRecordTypeId();
            	    tskInsert.add(ntsk);
            	}
                Database.insert(tskInsert);
                final Opportunity opp = MX_WB_TestData_cls.crearOportunidad('Opp1',accVidaTst.Id,userInfo.getUserId(), 'MX_SB_COB_Cotizador_RS_Hospital');
                opp.producto__c='Respaldo Seguro Para Hospitalización';
                insert opp;  
                Final Task ntsk2 = new Task(Telefono__c = '5534253647',whatId =opp.Id, RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_PureCloud').getRecordTypeId());
                insert ntsk2;
            }
    }

	/*
    * @description method que prueba MX_RTL_Task_Service getTaskData
    * @param  void
    * @return String 
    */
    @isTest
    static void testgetTaskData () {
		final List<Siniestro__c>  ids =[SELECT Id from Siniestro__c where  MX_SB_MLT_NombreConductor__c =:  NAME + '_1'];
        Final List<Task> listTsk = [SELECT Id, WhatId,CreatedDate
                           FROM Task WHERE WhatId in :ids];
        Final Set<Id> sinIds = new Set<Id>();
        for(Task tsk : listTsk) {
            sinIds.add(tsk.Id);
        }
        test.startTest();
			MX_RTL_Task_Service.getTaskData(sinIds);
        test.stopTest();
        system.assert(true, 'Exitoso');        
    }
    
    /*
    * @description method que prueba MX_RTL_Task_Service updateTask PureCloud
    * @param  void
    */
    static testMethod void updateTaskTest () {        
        userTest = [SELECT Id, Name FROM User WHERE Name =: NAME];
        system.runAs(userTest) {
            Test.startTest();
            Final Task getTaskTest = [SELECT Id, Telefono__c FROM Task WHERE recordType.DeveloperName = 'MX_SB_MLT_PureCloud' LIMIT 1];
                MX_RTL_Task_Service.updateTask(getTaskTest,'PureCloud');
            Test.stopTest();
            System.assert(true,'Tarea Actuaizada');
        }
        	
    }
    
    /*
    * @description method que prueba MX_RTL_Task_Service updateTask Cita
    * @param  void
    */
    static testMethod void updateTaskCitaTest () {        
        userTest = [SELECT Id, Name FROM User WHERE Name =: NAME];
        system.runAs(userTest) {
            Test.startTest();
            Final datetime getDateValue = datetime.now();
            Final Task testTask = new Task();
            testTask.Telefono__c = '1234567891';
        	testTask.ReminderDateTime = getDateValue.addMonths(1);
                MX_RTL_Task_Service.updateTask(testTask,'Cita');
            Test.stopTest();
            System.assert(true,'Tarea Actuaizada para Cita');
        }
        	
    }
    
    
   	/*
    * @description method que prueba MX_RTL_Task_Service insertTask
    * @param  void
    */
    @isTest
    static void insertTaskTest () {        
        userTest = [SELECT Id, Name FROM User WHERE Name =: NAME];
        system.runAs(userTest) {
            Test.startTest();
                Final datetime getDateTime = datetime.now();
                Final task createTask = new Task();
                createTask.Subject = 'Llamada Realizada ' + getDateTime;
            	createTask.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('MX_SB_VTA_Ventas_Asistida').getRecordTypeId();
                Final List<Task> listTaskTest = new List<Task> {createTask};
                MX_RTL_Task_Service.insertTask(listTaskTest);
            Test.stopTest();
            System.assert(true,'Tarea creada');
        }
        	
    }

    /*
    * @description method que prueba MX_RTL_Task_Service upsertTask
    * @param  void
    */
    @isTest
    static void upsertTaskTest () {        
        userTest = [SELECT Id, Name FROM User WHERE Name =: NAME];
        system.runAs(userTest) {
            Test.startTest();
                Final datetime getDateT = datetime.now();
                Final task createTsk = new Task();
                createTsk.Subject = 'Llamada Realizada ' + getDateT;
            	createTsk.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('MX_SB_VTA_Ventas_Asistida').getRecordTypeId();
                Final List<Task> lstTskTest = new List<Task> {createTsk};
                MX_RTL_Task_Service.upsertTask(lstTskTest);
                Final String checkVal = lstTskTest[0].Id;
                if (String.isEmpty(checkVal)) {
                    isUpsert = false;
                }
            Test.stopTest();
            System.assert(isUpsert,'Task upserted');
        }	
    }

    /**
    * @description : Methodo para obtener
    * informacion de la tarea  relacionada
    * a la opp
    * @author Daniel Ramirez Islas | 11-18-2020 
    **/
    @isTest
    static void selectTaskByIdTest () {
        Test.startTest();
            Final Opportunity opp = [Select id FROM Opportunity  Limit 1];
        	final Task ntsk3 = [Select id FROM Task Limit 1];
        	ntsk3.whatId = opp.Id;
        	update ntsk3;
            Final List<Task> selectTaskById = MX_RTL_Task_Service.selectTaskById(opp.Id);
            System.assert(!selectTaskById.isEmpty(), 'Tarea encontrada');
        Test.stopTest();
    }

}