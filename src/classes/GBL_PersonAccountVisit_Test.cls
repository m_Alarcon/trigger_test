/**
* @author bbva.com developers
* @date 2019
*
* @group global_hub_kit_visit
*
* @description Test Class for GBL_PersonAccountVisit_Cls
**/
@isTest
private with sharing class GBL_PersonAccountVisit_Test {
    /**variable usuarioAdmin */
	private static User usuarioAdmin;

    @isTest
     static void tstmethod () {
        final gcal__GBL_Google_Calendar_Sync_Environment__c calEnviro = new gcal__GBL_Google_Calendar_Sync_Environment__c(Name = 'DEV');
        insert calEnviro;

        usuarioAdmin = UtilitysDataTest_tst.crearUsuario ('PruebaAdm',Label.MX_PERFIL_SystemAdministrator,'BBVA ADMINISTRADOR');
        insert usuarioAdmin;

        System.runAs(usuarioAdmin) {
            System.debug('PersonAccount ' + GBL_PersonAccountCompatibility_Cls.isPersonAccountEnabled());
            if(GBL_PersonAccountCompatibility_Cls.isPersonAccountEnabled()) {
                final PersonAccountVisitKitPermissions__c csPermission = new PersonAccountVisitKitPermissions__c();
                csPermission.EnablePersonAccount__c = true;
                csPermission.SetupOwnerId = Userinfo.getProfileId();
                insert csPermission;

                final String recordTypeId  = [SELECT Id FROM RecordType WHERE DeveloperName = 'PersonAccount'].Id;
                final dwp_kitv__Visit__c visit = new dwp_kitv__Visit__c();//NOSONAR
                System.debug('++++ recordTypeId ' + recordTypeId);
                System.debug('++++ recordTypeId ' + [SELECT Id FROM RecordType WHERE DeveloperName = 'PersonAccount']);
                final Account cuenta = new Account(
                    RecordTypeId = recordTypeId,
                    FirstName='testName',
                    LastName='testLastName',
                    PersonMailingStreet='test address',
                    PersonMailingPostalCode='12345',
                    PersonMailingCity='SFO',
                    PersonEmail='test@test.com',
                    PersonHomePhone='5512345678',
                    PersonMobilePhone='5512345678'
                );
                System.debug('cuenta ***** ' + cuenta);
                insert cuenta;

                final Account cuenta2 = new Account(
                    Name='testName2'
                );
                insert cuenta2;

                Datetime fecha = DateTime.now();
                fecha = fecha.addDays(1);

                final List<dwp_kitv__Visit__c> newVisits = new List<dwp_kitv__Visit__c>();
                List<dwp_kitv__Visit__c> newVisitsQuery = new List<dwp_kitv__Visit__c>();
                final dwp_kitv__Visit__c visita = new dwp_kitv__Visit__c(
                    dwp_kitv__account_id__c = cuenta.Id,
                    dwp_kitv__visit_duration_number__c = '15',
                    dwp_kitv__visit_start_date__c = fecha
                );

                final String cuentaId = cuenta.Id;
                final String query = 'Select Id, PersonContactId from Account where Id =: cuentaId and isPersonAccount = true';//NOSONAR
                final Map<Id,Account> personAccounts = new Map<Id,Account>((List<Account>)Database.query(query));
                System.debug('personAccounts ' + personAccounts);
                final Id idContactoPA = (Id)personAccounts.get(cuenta.Id).get('PersonContactId');

                newVisits.add(visita);
                insert newVisits;

                newVisitsQuery = [SELECT Id, dwp_kitv__account_id__c, MX_TieneCuentaPersonal__c FROM dwp_kitv__Visit__c WHERE Id IN: newVisits];

                GBL_PersonAccountVisit_Cls.relateContactVisit(newVisitsQuery);

                final String contactsQuery = 'SELECT Id FROM dwp_kitv__Visit_Contact__c WHERE dwp_kitv__contact_id__c =: idContactoPA';//NOSONAR
                final List<dwp_kitv__Visit_Contact__c> listCVisitas = Database.query(contactsQuery);
                System.assert(!listCVisitas.isEmpty(), 'Account is a Person Account');
            }
        }
    }
}