/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpPlanesProductoResp
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
 @SuppressWarnings('sf:ShortVariable')
public class MX_SB_PS_wrpPlanesProductoResp {
  /*Public property for wrapper*/
	public iCatalogItem iCatalogItem {get;set;}
	  /*Public subclass for wrapper*/
	   /*public constructor subclass*/
		public MX_SB_PS_wrpPlanesProductoResp() {
		 this.iCatalogItem = new iCatalogItem();
		}
	/*public constructor subclass*/
	public class iCatalogItem {
	  /*Public property for wrapper*/
		public productPlan[] productPlan {get;set;}
		 /*public constructor subclass*/
		public iCatalogItem() {
		 this.productPlan =  new productPlan[] {};
		 this.productPlan.add(new productPlan());
		}	
	}
	  /*Public subclass for wrapper*/
	public class productPlan {
	  /*Public property for wrapper*/
		public catalogItemBase catalogItemBase {get;set;}
	  /*Public property for wrapper*/	
		public String planReview {get;set;}
	  /*Public property for wrapper*/	
		public String bouquetCode {get;set;}	
	/*public constructor subclass*/
		public productPlan() {
		 this.catalogItemBase = new catalogItemBase();
		 this.planReview = '';
		 this.bouquetCode = '';
		}	
	}
	  /*Public subclass for wrapper*/
	public class catalogItemBase {
	  /*Public property for wrapper*/
		public String name {get;set;}
	  /*Public property for wrapper*/	
		 public string id {get;set;} 
       /*public constructor subclass*/
		public catalogItemBase() {
		 this.name = '';
		 this.id = '';
		}			
	}

}