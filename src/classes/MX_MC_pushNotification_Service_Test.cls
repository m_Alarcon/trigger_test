/**
 * @File Name          : MX_MC_pushNotification_Service_Test.cls
 * @Description        : Clase para Test de MX_MC_pushNotification_Service
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 03/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      03/06/2020           Jair Ignacio Gonzalez G.   Initial Version
**/
@isTest
private class MX_MC_pushNotification_Service_Test {
    /**
     **Descripción: Clase makeData
    **/
    @TestSetup static void makeData() {
        final iaso__GBL_Rest_Services_Url__c restUrl = new iaso__GBL_Rest_Services_Url__c(Name = 'MC_pushNotification',
                                                                                        iaso__Cache_Partition__c = 'iaso.ServicesPartition');
        insert restUrl;
    }
    /**
     * *Descripción: Clase de prueba para MX_MC_pushNotification_Service
    **/
    @isTest static void testServiceMethods() {
        final MX_WB_Mock mockCallout = new MX_WB_Mock(500,'ERROR', '[{"Test": "MX_MC_pushNotification_Service"}]',new Map<String,String>());
        String jsonResp;
        Test.startTest();
        iaso.GBL_Mock.setMock(mockCallout);
        try {
            MX_MC_pushNotification_Service.callPushNotification('123456');
        } catch (CalloutException err) {
            jsonResp = err.getMessage();
        }
        Test.stopTest();
        System.assertEquals('[{"Test": "MX_MC_pushNotification_Service"}]', jsonResp, 'Cant call MC_pushNotification');
    }
}