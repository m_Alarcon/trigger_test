/**
 * @File Name          : MX_SB_VTS_LeadTriggerHandler.cls
 * @Description        :
 * @Author             : Diego Olvera
 * @Group              :
 * @Last Modified By   : Diego Olvera
 * @Last Modified On   : 08-03-2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      9/8/2019 10:54:24           Eduardo Hernández           Initial Version
 * 1.1      28/10/2019                  Francisco Javier            Se agrega funcion para validar el role del usuario.
 * 1.1.1    20/04/2020                  Eduardo Hernández           Se agrega funcionalidad para envio programado de leads.
 * 1.2		12/05/2020					Alexandro Corzo				Se agrega función para actualizar el valor del campo "Calificación de Solicitud"
 * 																	dependiendo del horario en que se registro el Lead. El cual puede ser Hot Lead o Cold Lead.
 * 1.3      17/08/2020                  Alexandro Corzo             Se mueve el llamado de la instrucción: updateCalSolicitud a beforeInsert.
**/
public without sharing class MX_SB_VTS_LeadTriggerHandler extends TriggerHandler { //NOSONAR
    /*variable de almacenamiento trriger New*/
    final private List<Lead> triggerNewMap;
    final private Map<Id, Lead> oldMaps;
    /*Constructor*/
    public MX_SB_VTS_LeadTriggerHandler() {
        super();
        this.triggerNewMap = Trigger.new;
        this.oldMaps = (Map<Id, Lead>)Trigger.oldMap;
    }

    /**
     * beforeInsert función para lógica de beforeInsert
     */
	protected override void beforeInsert() {
        MX_SB_VTS_LeadTrigger.validateRole(triggerNewMap);
        MX_SB_VTS_LeadTrigger.evaluateLeads(triggerNewMap);
        MX_BPP_LeadCampaign_Service.assignOwnerByNumberAccount(triggerNewMap);
        MX_SB_VTS_LeadTrigger.mayorAvanceTip(triggerNewMap);
    }

    /**
     * afterInsert función para lógica de beforeInsert
     */
    protected override void afterInsert() {
        MX_SB_VTS_LeadMultiCTI.validLeadCTI(triggerNewMap, 'callFuture');
        MX_SB_VTS_LeadTrigger.updateCalSolicitud(triggerNewMap, oldMaps);
    }
    /*
    * beforeUpdate función para lógica de beforeUpdate
    */
    protected override void beforeUpdate() {
        MX_SB_VTS_LeadTrigger.validateRole(triggerNewMap);
        MX_SB_VTS_LeadTrigger.substractContactTouch(triggerNewMap, oldMaps);
        MX_SB_VTS_LeadTrigger.addsContactCounter(triggerNewMap, oldMaps);
        MX_SB_VTS_LeadTrigger.substractEffectiveContact(triggerNewMap, oldMaps);
        MX_SB_VTS_LeadTrigger.substractNoEffectiveContact(triggerNewMap, oldMaps);
        MX_SB_VTS_LeadTrigger.mayorAvanceTip(triggerNewMap);
    }

    /**
     * AfterUpdate función para lógica de afterupdate
     */
	protected override void afterUpdate() {
        MX_SB_VTS_LeadTrigger.updateVisit(triggerNewMap);
    }
}