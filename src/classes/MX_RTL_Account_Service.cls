/**
 * @File Name          : MX_RTL_Account_Service.cls
 * @Description        :
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 2/08/2021, 10:43:34 AM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/26/2020   Juan Carlos Benitez     Initial Version
 * 1.1    1/08/2021   Juan Carlos Benitez     Se añaden campos a query
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public  class MX_RTL_Account_Service { 
	/** VAR IDE id */    
	Final static String IDE='Id';
    /*Integer ONE */
    Final static Integer ONE=1;
    /*List of accounts to be returned */
    Final static List<Account> ACC = new List<Account>();
    
	/**
     * 
    * @description
    * @author Juan Carlos Benitez | 5/26/2020
    * @param Ids
    * @return List<Account>
    **/
    public static List<Account> getSinHData(Set<Id> ids) {
        Final Map<String,String> mapa = new Map<String,String>();
        mapa.put('fields', ' Id, Segmento__c,RFC__c,MX_SB_SAC_Homoclave__c,ApellidoMaterno__c, MX_RTL_CURP__pc, PersonContactId, Colonia__c, Phone,PersonHomePhone,PersonOtherPhone ,Correo_Electronico__c, BillingCity, BillingCountry, BillingPostalCode, BillingState, BillingStreet,FinServ__PrimaryContact__c  ');
        return MX_RTL_Account_Selector.getAccountReusable(ids, IDE, mapa);
    }
    	/**
     * 
    * @description
    * @author Juan Carlos Benitez | 5/26/2020
    * @param Account pAcc
    * @return List<Account> pAcc
    **/

    public static List<Account> upsrtAcc(Account pAcc) {
        Final list<Account> acct = [SELECT Id, MX_RTL_CURP__pc from Account where MX_RTL_CURP__pc =:pAcc.MX_RTL_CURP__pc ];
        if(acct.size()<ONE) {
            pAcc.RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
			ACC.add(MX_RTL_Account_Selector.upsrtAccount(pAcc));
        } else {
            pAcc.Id=acct[0].Id;
			ACC.add(MX_RTL_Account_Selector.updteAccount(pAcc));
        }
        return ACC;
    }
}