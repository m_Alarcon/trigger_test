/**
 * @description       : Clase de prueba para clase de MX_SB_VTS_UpdSeRecord_Ctrl
 * @author            : Diego Olvera
 * @group             : 
 * @last modified on  : 10-02-2020
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   10-02-2020   Diego Olvera   Initial Version
**/
@isTest
public class MX_SB_VTS_UpdSeRecord_C_Test {
    @TestSetup
    static void makeData() {
        final User userRecord = MX_WB_TestData_cls.crearUsuario('UserTest', 'System Administrator');
        insert userRecord;
        final Lead leadRecord = MX_WB_TestData_cls.vtsTestLead('LeadTest', System.Label.MX_SB_VTS_Telemarketing_LBL);
        leadRecord.Producto_Interes__c = 'Seguro Estudia';
        insert leadRecord;
    }
    @isTest
    static void updCurrRecTst() {
        final User usrLeadRec = [Select Id from User where LastName = 'UserTest'];
        System.runAs(usrLeadRec) {
            final Lead lstCurrentLead = [Select Id from Lead Where LastName = 'LeadTest'];
            Test.startTest();
            try {
                MX_SB_VTS_UpdSeRecord_Ctrl.updCurrRec(lstCurrentLead.Id);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal');
                System.assertEquals('Fatal', extError.getMessage(),'no hay candidato');
            }
            Test.stopTest();
        }
    }
}