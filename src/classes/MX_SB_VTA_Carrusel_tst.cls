/**
 * @File Name          : MX_SB_VTA_Carrusel_tst.cls
 * @Description        : Carrusel   
 * @Author             : Arsenio.perez.lopez.contractor@BBVA.com
 * @Group              : BBVA
 * @Last Modified By   : Arsenio.perez.lopez.contractor@BBVA.com
 * @Last Modified On   : 28/1/2020 17:27:42
 * @Modification Log   : LastModif
 * Ver       Date            Author      		                    Modification
 * 1.0    10/2/2020   Arsenio.perez.lopez.contractor@BBVA.com       Initial Version
 * 1.0    04/6/2020   jesusalexandro.corzo@bbva.com                 Ajustes a la clase
**/
@IsTest
public with sharing class MX_SB_VTA_Carrusel_tst {
    /**Etapa de la oportunidad */
    static string informacionbas='Informacion básica';
    /**Nombre del producto */
    static string producHogar='hogar';
    /**Identificador de la Oportunidad */
    static Id oppoId = null;

    @TestSetup
    static void makeData() {
        final User usuarioAdmin = MX_WB_TestData_cls.crearUsuario('testStar',System.label.MX_SB_VTS_ProfileAdmin);
        insert usuarioAdmin;
        system.runAs(usuarioAdmin) {
            final Scripts_Stage_Product__c script = new Scripts_Stage_Product__c(
            Name='Apertura', 
            MX_SB_VTS_SRC__c='',
            MX_SB_VTS_HEADER__c='',
            MX_SB_VTS_HREF__c='',
            MX_WB_Script__c='',
            MX_WB_Etapa__c = 'Cotización',
            MX_SB_VTS_Status__c =informacionbas,
            MX_SB_VTS_Product__c =producHogar,
            MX_SB_VTS_Type__c = 'CP7');
            insert script;
            final Account temAccount = MX_WB_TestData_cls.crearCuenta('example','');
            insert temAccount;
            final Opportunity OppGenerada = MX_WB_TestData_cls.crearOportunidad( 'Prueba1',  temAccount.Id,  usuarioAdmin.Id, 'MX_SB_VTA_VentaAsistida');
            OppGenerada.MX_SB_VTA_SubEtapa__c =informacionbas;
            OppGenerada.stageName='Cotización';
            insert OppGenerada;
            oppoId = OppGenerada.id;
        }
    }

    @IsTest
    static void dataexist() {
        final User usuarionormal = MX_WB_TestData_cls.crearUsuario('testStar1','Telemarketing VTS');
        insert usuarionormal;
        final PermissionSet pes = [SELECT Id FROM PermissionSet WHERE Name = 'MX_SB_VTS_Vta_Asistida'];
        insert new PermissionSetAssignment(AssigneeId = usuarionormal.id, PermissionSetId = pes.Id);
        final Opportunity opp = [SELECT id, StageName from Opportunity where name='Prueba1'][0];
        Test.startTest();
        System.runAs(usuarionormal) {
            final List<Scripts_Stage_Product__c> temp =
            MX_SB_VTA_Carrusel_cls.getconsultallamada(informacionbas, opp.StageName,true,producHogar);
            System.assertNotEquals(temp, null,'El objeto no esta vacio');
        }
        Test.stopTest(); 
    }
}