/**
* @File Name          : MX_SB_VTS_GetAmountService_Test.cls
* @Description        :
* @Author             : Diego Olvera
* @Group              :
* @Last Modified By   : Diego Olvera
* @Last Modified On   : 26/5/2020 16:25:59
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    7/5/2020   Diego Olvera     Initial Version
**/
@isTest
private class MX_SB_VTS_GetAmountService_Test {
    /** Name User */
    final static String USERNAME = 'test';
    /** Name Account */
    final static String ACCNAME = 'account test';
    /** Name Opp */
    final static String OPPNAME = 'opp test';
      /* @Method: makeData
* @Description: create test data set
*/
    @TestSetup
    public static void makeData() {
        final User testUser = MX_WB_TestData_cls.crearUsuario(USERNAME, System.Label.MX_SB_VTS_ProfileAdmin);
        Insert testUser;
        final Account accnt = MX_WB_TestData_cls.crearCuenta(ACCNAME, System.Label.MX_SB_VTS_PersonRecord);
        accnt.PersonEmail = 'pruebaVts@mxvts.com';
        insert accnt;
        final Opportunity opp = MX_WB_TestData_cls.crearOportunidad(OPPNAME, accnt.Id, testUser.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
        opp.LeadSource = 'Call me back';
        opp.Producto__c = 'Hogar';
        opp.Reason__c = 'Venta';
        opp.StageName = 'Cotizacion';
        insert opp;
    }
      /* @Method: testGetAmountOpp
* @Description: return json values of fake response
*/
    @isTest
    private static void testGetAmountOpp() {
        final User  cUser=[SELECT ID,Name FROM User  WHERE NAME =: USERNAME LIMIT 1];
        System.runAs(cUser) {
            final object test = MX_SB_VTS_GetAmountService.amountValues();
            System.assertNotEquals(test, null,'El objeto no esta vacio');
        }
    }

}