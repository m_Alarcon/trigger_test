/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_DetalleCoberturas
* Autor Daniel Perez Lopez
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : clase controladora para componente de coberturas

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           14/02/2020      Daniel Lopez                         Creación
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_DetalleCoberturas { //NOSONAR
   	/*
   	 * @Variable ocupada para definir campos de tabla
	*/
    public static String[] campoCobert {
        get {
            return new String [] {'status','cobertura','descripcion','ideCobertura','moneda','sumaAsegurada','Act'};
        } set;
    }

    /*
    * @getTableCoberturas Obtiene los datos para la tabla Coberturas
    * @param String ideuno
    * @return Cobertura__c[]
    */
    @AuraEnabled
    public static Cobertura__c[] getTableCoberturas (String ideuno, String test) {
        final List<Map<String,String>> siniarray = new List<Map<String,String>>();
        final Cobertura__c[] listUpsert = new Cobertura__c[]{};
            final Map<String,String> datos1 = new Map<String,String>();
            datos1.put(campoCobert[0],campoCobert[6]);
            datos1.put(campoCobert[1],'DAMA');
            datos1.put(campoCobert[2],'Daños Materiales');
            datos1.put(campoCobert[3],'1701624');
            datos1.put(campoCobert[4],'PES');
            datos1.put(campoCobert[5],'3000');
        siniarray.add(datos1);
        for (Map<String,String> item : siniarray ) {
            listUpsert.add(upsertCobert(item));
        }
        upsert listUpsert;
        return listUpsert;
    }

    /*
    * @getTableDeducibles Obtiene los datos para la tabla deducibles
    * @param String ideuno
    * @return List<Map<String,String>>
    */
    @AuraEnabled
    public static List<Map<String,String>> getTableDeducibles (String ideuno) {
        	final List<Map<String,String>> siniarray2 = new List<Map<String,String>>();
            final Map<String,String> datos1 = new Map<String,String>();
            datos1.put('col1','Daños Materiales');
            datos1.put('col2','5');
        siniarray2.add(datos1);
        return siniarray2;
    }
    /*
    * @getTableDeducibles Actualiza datos de coberturas
    * @param mapa de datos de cobertura
    * @return Cobertura__c
    */
    @AuraEnabled
    public static Cobertura__c upsertCobert (Map<String,String> cobertura) {
        Cobertura__c[] upsertCobertu = new Cobertura__c[]{};
        final Cobertura__c cobertins =  new Cobertura__c();
        upsertCobertu = [SELECT id,NombreCobertura__c
                            FROM Cobertura__c
                            WHERE MX_SB_MLT_Cobertura__c=:cobertura.get('cobertura')
                            AND MX_SB_MLT_IdeCobertura__c=:cobertura.get('ideCobertura')];
        if (upsertCobertu.size()==0) {
            cobertins.MX_SB_MLT_Cobertura__c=cobertura.get('cobertura');
            cobertins.MX_SB_MLT_Descripcion__c=cobertura.get(campoCobert[2]);
            cobertins.MX_SB_MLT_Estatus__c= cobertura.get('status')==campoCobert[6]?'Activo':'Inactivo';
            cobertins.MX_SB_MLT_SumaAsegurada__c=Decimal.valueOf(cobertura.get('sumaAsegurada'));
        } else {
            cobertins.Id=upsertCobertu[0].Id;
            cobertins.MX_SB_MLT_Descripcion__c=cobertura.get(campoCobert[2]);
            cobertins.MX_SB_MLT_Estatus__c= cobertura.get('status')==campoCobert[6] ?'Activo':'Inactivo';
            cobertins.MX_SB_MLT_SumaAsegurada__c=Decimal.valueOf(cobertura.get('sumaAsegurada'));
        }
        return cobertins;
    }

}