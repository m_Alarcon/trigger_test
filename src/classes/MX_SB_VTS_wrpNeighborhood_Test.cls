@isTest
/**
* @FileName          : MX_SB_VTS_wrpNeighborhood_Test
* @description       : Test class from MX_SB_VTS_wrpNeighborhood_Utils
* @Author            : Marco Antonio Cruz Barboza
* @last modified on  : 12-01-2021
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   12-01-2021   Marco Antonio Cruz Barboza          Initial Version
**/
public class MX_SB_VTS_wrpNeighborhood_Test {

   	/** User name for an apex class test */
    final static String NMTEST = 'UserTest';
    /** Account Name for an apex class test*/
    final static String ACCOTEST = 'Test Account';
    
    @TestSetup
    static void setupTest() {
        final User tstUser = MX_WB_TestData_cls.crearUsuario(NMTEST, System.Label.MX_SB_VTS_ProfileAdmin);
        System.runAs(tstUser) {
            final Account testAccnt = MX_WB_TestData_cls.crearCuenta(ACCOTEST, System.Label.MX_SB_VTS_PersonRecord);
            testAccnt.PersonEmail = 'pruebaVts@mxvts.com';
            insert testAccnt;
        }
    }
    
    
    /*
    @Method: wrpColoniasRespTest
    @Description: Use a wrapper class to deserialize a response from a web service.
	@param
    */
    @isTest
    static void wrpNeighborhoodTest() {
        test.startTest();
			Final MX_SB_VTS_wrpNeighborhood_Utils.comparables comparables = new MX_SB_VTS_wrpNeighborhood_Utils.comparables();
        	Final MX_SB_VTS_wrpNeighborhood_Utils.comparables[] wrapperComp = new MX_SB_VTS_wrpNeighborhood_Utils.comparables[]{};	
        	Final MX_SB_VTS_wrpNeighborhood_Utils.byRooms byRooms = new MX_SB_VTS_wrpNeighborhood_Utils.byRooms();
        	Final MX_SB_VTS_wrpNeighborhood_Utils.byRooms[] wrapperRoom = new MX_SB_VTS_wrpNeighborhood_Utils.byRooms[]{};
        	Final MX_SB_VTS_wrpNeighborhood_Utils.houseList houseList = new MX_SB_VTS_wrpNeighborhood_Utils.houseList();
        	Final MX_SB_VTS_wrpNeighborhood_Utils.houseList[] wrapperHou = new MX_SB_VTS_wrpNeighborhood_Utils.houseList[]{};
            Final MX_SB_VTS_wrpNeighborhood_Utils stringComp = new MX_SB_VTS_wrpNeighborhood_Utils();
            comparables.bathroom = '2';
        	comparables.description = 'I am a test wrapper';
        	comparables.latitude = '23141342';
        	comparables.longitude = '3243242342';
        	comparables.linkportal = 'www.test.com';
        	comparables.m2 = '223';
        	comparables.price = '400000';
        	comparables.rooms = '5';
        	byRooms.price = '322333';
        	byRooms.pricem2 = '234';
        	byRooms.room = '2';
        	byRooms.surface = 'test';
        	houseList.comparablesNumber = '3';
        	houseList.price = '323442';
        	houseList.pricem2 = '234';
        	houseList.surface = 'testing';
        	wrapperComp.add(comparables);
        	wrapperRoom.add(byRooms);
        	houseList.comparables = wrapperComp;
        	houseList.byRooms = wrapperRoom;
        	wrapperHou.add(houseList);
        	stringComp.comparables = 'Test';
        	stringComp.houseList = wrapperHou;
        	System.assertNotEquals(stringComp.houseList, null,'El wrapper esta retornando');
        test.stopTest();
    }
    
}