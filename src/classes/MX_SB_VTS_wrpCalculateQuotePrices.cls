/**
* @FileName          : MX_SB_VTS_wrpCalculateQuotePrices
* @description       : Wrapper encargado de almacenar la información obtenida
*					 : del servicio calculate-prices
* @Author            : Alexandro Corzo
* @last modified on  : 03-03-2021
* Modifications Log 
* Ver   Date         Author                Modification
* 1.0   03-03-2021   Alexandro Corzo       Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton, sf:ExcessivePublicCoun,sf:LongVariable,sf:LongVariable,sf:ExcessivePublicCount, sf:ShortVariable,sf:ShortClassName')
public class MX_SB_VTS_wrpCalculateQuotePrices {
	/**
	 * Invocación de Wrapper
	 */
    public data data {get; set;}
        
    /**
     * @description : Clase del objeto data del Wrapper
     * @author: 	  Alexandro Corzo
     */
	public class data {
   		/** Invocación de Clase: trades */
        public trades[] trades {get; set;}
        /** Invocación de Clase: disasterCovered */
        public disasterCovered[] disasterCovered {get; set;}
        /** Invocación de Clase: frequencies */
        public frequencies[] frequencies {get; set;}
        /** Atributo de Clase: protectionLevel */
        public double protectionLevel {get; set;}
	}
    
    /**
     * @description : Clase del objeto trades del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class trades {
        /** Atributo de Clase: description */
        public String description {get; set;}
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Invocación de Clase: categories */
        public categories[] categories {get; set;}
    }
    
    /**
     * @description : Clase del objeto goodTypes del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class goodTypes {
        /** Atributo de Clase: description */
        public String description {get; set;}
        /** Atributo de Clase: code */
        public String code {get; set;}
        /** Invocación de Clase: coverages */
        public coverages[] coverages {get; set;}
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
    }

    /**
     * @description : Clase del objeto trades del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class categories {
        /** Atributo de Clase: description */
        public String description {get; set;}
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Invocación de Clase: goodTypes */
        public goodTypes[] goodTypes {get; set;}
    }
    
    /**
     * @description : Clase del objeto coverages del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class coverages {
        /** Atributo de Clase: description */
        public String description {get; set;}
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Invocación de Clase: amounts */
        public premiums[] premiums {get; set;}
    }
    
    /**
     * @description : Clase del objeto premiums del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class premiums {
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Invocación de Clase: amounts */
        public amounts[] amounts {get; set;}
    }
	
	/**
     * @description : Clase del objeto amounts del Wrapper
     * @author: 	  Alexandro Corzo
     */    
    public class amounts {
        /** Atributo de Clase: amount */
        public double amount {get; set;}
        /** Atributo de Clase: currency */
        public String currencydata {get; set;}
        /** Atributo de Clase: isMajor */
        public Boolean isMajor {get; set;}
    }
    
    /**
     * @description : Clase del objeto disasterCovered del Wrapper
     * @author: 	  Alexandro Corzo
     */ 
    public class disasterCovered {
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Atributo de Clase: value */
        public String value {get; set;}
    }
    
    /**Frecuencias de pagos */
    public class frequencies {
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Atributo de Clase: description */
        public String description {get; set;}
        /** Invocación de Clase: premiums */
        public premiums[] premiums {get; set;}
        /** Atributo de Clase: numberSubsequentPayments */
        public integer numberSubsequentPayments {get; set;}
    }
    
}