/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpGuardarDatosCliente
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
 @SuppressWarnings('sf:LongVariable, sf:ExcessivePublicCount, sf:ShortVariable,sf:ShortClassName')
public  class MX_SB_PS_wrpGuardarDatosCliente {
     /*Public property for wrapper*/
	public header header {get;set;}
	  /*Public property for wrapper*/
	public quote quote {get;set;}
	  /*Public property for wrapper*/
	public holder holder {get;set;}
	  /*Public property for wrapper*/
	public clientType clientType {get;set;}
	  /*Public property for wrapper*/
	public string contractingHolderIndicator {get;set;}
      /*Public constructor for wrapper*/
    public MX_SB_PS_wrpGuardarDatosCliente() {
        header = new header();
        quote = new quote();
        holder = new holder();
        clientType = new clientType();
		contractingHolderIndicator = '';
    }
	  /*Public subclass for wrapper*/
	public  class header {
	/*Public property for wrapper*/	
		public String idRequest {get;set;}
	  /*Public property for wrapper*/	
		public String channel {get;set;}
	  /*Public property for wrapper*/	
		public String subChannel {get;set;}
	  /*Public property for wrapper*/	
		public String branchOffice {get;set;}
	  /*Public property for wrapper*/	
		public String managementUnit {get;set;}
	  /*Public property for wrapper*/	
		public String user {get;set;}
	  /*Public property for wrapper*/	
		public String idSession {get;set;}
	  /*Public property for wrapper*/	
		public String dateConsumerInvocation {get;set;}
	  /*Public property for wrapper*/
		public String aapType {get;set;}	
	  /*Public property for wrapper*/	
		public String dateRequest {get;set;}
      
	  /*public constructor subclass*/
		public header() {
		 this.user = '';
		 this.aapType='';	
		 this.SubChannel = '';
		 this.branchOffice = '';
		 this.managementUnit = '';
		 this.IdSession = '';
		 this.idRequest = '';
		 this.dateConsumerInvocation = '';
		  this.dateRequest = '';
		 this.Channel = '';
		}		 		
	}
	  /*Public subclass for wrapper*/
	public class quote {
	  /*Public property for wrapper*/
		public String idQuote {get;set;}
       /*public constructor subclass*/
		public quote() {
		 this.idQuote = '';
		}			
	}
	  /*Public subclass for wrapper*/
	public class holder {
	  /*Public property for wrapper*/
		public String email {get;set;}
      /*Public property for wrapper*/		
		public String rfc {get;set;}	
	   /*Public property for wrapper*/	
		public activity activity {get;set;}
	  /*Public property for wrapper*/	
		public mainAddress mainAddress {get;set;}
	  /*Public property for wrapper*/	
		public correspondenceAddress correspondenceAddress {get;set;}
	  /*Public property for wrapper*/	
		public legalAddress legalAddress {get;set;}
	  /*Public property for wrapper*/	
		public nacionality nacionality {get;set;}
	  /*Public property for wrapper*/	
		public mainContactData mainContactData {get;set;}
	  /*Public property for wrapper*/	
		public correspondenceContactData correspondenceContactData {get;set;}
	  /*Public property for wrapper*/	
		public fiscalContactData fiscalContactData {get;set;}
	  /*Public property for wrapper*/	
		public physicalPersonalityData physicalPersonalityData {get;set;}
	  /*Public property for wrapper*/	
		public deliveryInformation deliveryInformation {get;set;}
		 /*public constructor subclass*/
		public holder() {
		 this.email = '';
		 this.rfc = '';
		 this.activity = new activity();
		 this.mainAddress =  new mainAddress();
		 this.correspondenceAddress =  new correspondenceAddress();
		 this.legalAddress =  new legalAddress();
		 this.nacionality =  new nacionality();
		 this.mainContactData =  new mainContactData();
		 this.correspondenceContactData = new correspondenceContactData();
		 this.fiscalContactData = new fiscalContactData();
		 this.physicalPersonalityData =  new physicalPersonalityData();
		 this.deliveryInformation = new deliveryInformation();
		}	
	}
	  /*Public subclass for wrapper*/
	public class activity {
	  /*Public property for wrapper*/
		public catalogItemBase catalogItemBase {get;set;}
		 /*public constructor subclass*/
		public activity() {
		 this.catalogItemBase = new catalogItemBase();
		}	
	}
	  /*Public subclass for wrapper*/
	public class catalogItemBase {
	  /*Public property for wrapper*/
		 public string id {get;set;} 
	  /*Public property for wrapper*/	
		public String name {get;set;}
		 /*public constructor subclass*/
		public catalogItemBase() {
		 this.id = '';
		 this.name = '';
		}	
	}
	  /*Public subclass for wrapper*/
	public class mainAddress {
	  /*Public property for wrapper*/
		public String zipCode {get;set;}	
	  /*Public property for wrapper*/	
		public String streetName {get;set;}
	  /*Public property for wrapper*/	
		public String outdoorNumber {get;set;}
	  /*Public property for wrapper*/	
		public String door {get;set;}
	  /*Public property for wrapper*/	
		public suburb suburb {get;set;}
		 /*public constructor subclass*/
		public mainAddress() {
		 this.zipCode = '';
		 this.streetName = '';
		 this.outdoorNumber = '';
		 this.door = '';
		 this.suburb =  new suburb();
		}	
	}
	  /*Public subclass for wrapper*/
	public class suburb {
	  /*Public property for wrapper*/
		public neighborhood neighborhood {get;set;}
		 /*public constructor subclass*/
		public suburb() {
		 this.neighborhood = new neighborhood();
		}	
	}
	  /*Public subclass for wrapper*/
	public class neighborhood {
	  /*Public property for wrapper*/
		public catalogItemBase catalogItemBase {get;set;}
		 /*public constructor subclass*/
		public neighborhood() {
		 this.catalogItemBase =  new catalogItemBase();
		}	
	}
	  /*Public subclass for wrapper*/
	public class correspondenceAddress {
	  /*Public property for wrapper*/
		public String zipCode {get;set;}	
	  /*Public property for wrapper*/	
		public String streetName {get;set;}
	  /*Public property for wrapper*/	
		public String outdoorNumber {get;set;}
	  /*Public property for wrapper*/	
		public String door {get;set;}
	  /*Public property for wrapper*/	
		public suburb suburb {get;set;}
		 /*public constructor subclass*/
		public correspondenceAddress() {
		 this.zipCode = '';
		 this.streetName = '';
		 this.outdoorNumber = '';
		 this.door = '';
		 this.suburb =  new suburb();
		}	
	}
	  /*Public subclass for wrapper*/
	public class legalAddress {
	  /*Public property for wrapper*/
		public String zipCode {get;set;}
	  /*Public property for wrapper*/	
		public String streetName {get;set;}
	  /*Public property for wrapper*/	
		public String outdoorNumber {get;set;}
	  /*Public property for wrapper*/	
		public String door {get;set;}
	  /*Public property for wrapper*/	
		public suburb suburb {get;set;}
		 /*public constructor subclass*/
		public legalAddress() {
		 this.zipCode = '';
		 this.streetName = '';
		 this.outdoorNumber = '';
		 this.door = '';
		 this.suburb =  new suburb();
		}	
	}
	/*Public subclass for wrapper*/
	public class nacionality {
	/*Public property for wrapper*/
		public catalogItemBase catalogItemBase {get;set;}
		 /*public constructor subclass*/
		public nacionality() {
		 this.catalogItemBase = new catalogItemBase();
		}	
	}
	/*Public subclass for wrapper*/
	public class mainContactData {
	  /*Public property for wrapper*/
		public cellphone cellphone {get;set;}
	  /*Public property for wrapper*/	
		public phone phone {get;set;}
	  /*Public property for wrapper*/	
		public officePhone officePhone {get;set;}
		 /*public constructor subclass*/
		public mainContactData() {
		 this.cellphone = new cellphone();
		 this.phone = new phone();
		 this.officePhone = new officePhone();
		}	
	}
	  /*Public subclass for wrapper*/
	public class cellphone {
	  /*Public property for wrapper*/
		public String phoneExtension {get;set;}	
	  /*Public property for wrapper*/	
		public String telephoneNumber {get;set;}
		 /*public constructor subclass*/
		public cellphone() {
		 this.phoneExtension = '';
		 this.telephoneNumber = ''; 
		}	
	}
	  /*Public subclass for wrapper*/
	public class phone {
	  /*Public property for wrapper*/
		public String phoneExtension {get;set;}	
	  /*Public property for wrapper*/	
		public String telephoneNumber {get;set;}	
		 /*public constructor subclass*/
		public phone() {
		 this.phoneExtension = '';
		 this.telephoneNumber = ''; 
		}	
	}
	  /*Public subclass for wrapper*/
	public class officePhone {
	  /*Public property for wrapper*/
		public String phoneExtension {get;set;}
	  /*Public property for wrapper*/	
		public String telephoneNumber {get;set;}
		 /*public constructor subclass*/
		public officePhone() {
		 this.phoneExtension = '';
		 this.telephoneNumber = ''; 
		}	
	}
	  /*Public subclass for wrapper*/
	public class correspondenceContactData {
	  /*Public property for wrapper*/
		public cellphone cellphone {get;set;}
	  /*Public property for wrapper*/	
		public phone phone {get;set;}
	  /*Public property for wrapper*/	
		public officePhone officePhone {get;set;}
		 /*public constructor subclass*/
		public correspondenceContactData() {
		 this.cellphone = new cellphone();
		 this.phone = new phone();
		 this.officePhone = new officePhone();
		}	
	}
	  /*Public subclass for wrapper*/
	public class fiscalContactData {
	  /*Public property for wrapper*/
		public cellphone cellphone {get;set;}
	  /*Public property for wrapper*/	
		public phone phone {get;set;}
	  /*Public property for wrapper*/	
		public officePhone officePhone {get;set;}
		/*public constructor subclass*/
		public fiscalContactData() {
		 this.cellphone = new cellphone();
		 this.phone = new phone();
		 this.officePhone = new officePhone();
		}	
	}
	  /*Public subclass for wrapper*/
	public class physicalPersonalityData {
	  /*Public property for wrapper*/
		public String name {get;set;}
      /*Public property for wrapper*/		
		public String lastName {get;set;}
	  /*Public property for wrapper*/	
		public String birthDate {get;set;}
	  /*Public property for wrapper*/	
		public sex sex {get;set;}
	  /*Public property for wrapper*/	
		public String mothersLastName {get;set;}
	  /*Public property for wrapper*/	
		public String curp {get;set;}
	  /*Public property for wrapper*/	
		public civilStatus civilStatus {get;set;}
	  /*Public property for wrapper*/	
		public occupation occupation {get;set;}
	  /*Public property for wrapper*/	
		public fiscalPersonality fiscalPersonality {get;set;}
		/*public constructor subclass*/
		public physicalPersonalityData() {
		 this.name = '';
		 this.lastName = '';
		 this.birthDate =  '';
		 this.sex =  new sex();
		 this.mothersLastName =  '';
		 this.curp = '';
		 this.civilStatus = new civilStatus();
		 this.occupation =  new occupation();
		 this.fiscalPersonality = new fiscalPersonality();
		}	
	}
	  /*Public subclass for wrapper*/
	public class sex {
	  /*Public property for wrapper*/
		public catalogItemBase catalogItemBase {get;set;}
		/*public constructor subclass*/
		public sex() {
		this.catalogItemBase =  new catalogItemBase();
		}	
	}
	  /*Public subclass for wrapper*/
	public class civilStatus {
	  /*Public property for wrapper*/
		public catalogItemBase catalogItemBase {get;set;}
		/*public constructor subclass*/
		public civilStatus() {
		this.catalogItemBase =  new catalogItemBase();
		}	
	}
	  /*Public subclass for wrapper*/
	public class occupation {
	  /*Public property for wrapper*/
		public catalogItemBase catalogItemBase {get;set;}
		/*public constructor subclass*/
		public occupation() {
		this.catalogItemBase =  new catalogItemBase();
		}	
	}
	  /*Public subclass for wrapper*/
	public class fiscalPersonality {
	  /*Public property for wrapper*/
		public catalogItemBase catalogItemBase {get;set;}
		/*public constructor subclass*/
		public fiscalPersonality() {
		this.catalogItemBase =  new catalogItemBase();
		}	
	}
	  /*Public subclass for wrapper*/
	public class deliveryInformation {
	  /*Public property for wrapper*/
		public String referenceStreets {get;set;}
	  /*Public property for wrapper*/	
		public String deliveryInstructions {get;set;}
	  /*Public property for wrapper*/	
		public String deliveryTimeStart {get;set;}
	  /*Public property for wrapper*/	
		public String deliveryTimeEnd {get;set;}
		/*public constructor subclass*/
		public deliveryInformation() {
		 this.referenceStreets ='';
		 this.deliveryInstructions  = '';
		 this.deliveryTimeEnd = '';
		 this.deliveryTimeStart = '';
		}	
	}
	 /*Public subclass for wrapper*/
	public class clientType {
	  /*Public property for wrapper*/
		public catalogItemBase catalogItemBase {get;set;}
		/*public constructor subclass*/
		public clientType() {
		this.catalogItemBase =  new catalogItemBase();
		}	
	}

}