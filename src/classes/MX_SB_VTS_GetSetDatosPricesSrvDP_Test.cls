/**
 * @description       : Clase de cobertura que da soporte a MX_SB_VTS_GetSetDatosPricesSrv_Ctrl
 * @author            : Alexandro Corzo
 * @group             : 
 * @last modified on  : 02-03-2021
 * @last modified by  : Alexandro Corzo
 * Modifications Log 
 * Ver   Date         Author              Modification
 * 1.0   02-03-2021   Alexandro Corzo     Initial Version
 * 1.1   09-03-2021   Eduardo Hernández   Se corrige clase test
**/
@isTest
public class MX_SB_VTS_GetSetDatosPricesSrvDP_Test {
    /** Variable de Apoyo: sVenta */
    Static String sVenta = 'Venta';
	/** Variable de Apoyo: sLeadSource */
    Static String sLeadSource = 'Call me back';
    /** Variable de Apoyo: sProducto */
    Static String sProducto = 'Hogar seguro dinámico';
    /** Variable de Apoyo: sNoEmpty */
    Static String sNoEmpty = 'Lista No Vacia';
    /** Variable de Apoyo: sUserName */
    Static String sUserName = 'UserOwnerTest01';
    /** Variables de Apoyo: sPersonAcct */
    Static String sPersonAcct = 'PersonAccount'; 
    /** Variable de Apoyo: sURL */
    Static String sUrl = 'http://www.example.com';
    /** Variable de Apoyo: sContratos */
    Static String sContratos = 'Contratos';
    /**Tipo de domicilio */
    public final static String DOMTYPE = 'Domicilio asegurado';
    /** Variable de Apoyo: STROPPOOREN */
    public final static String STROPPOOREN = 'OutBoundRentado';
    /** Variable de Apoyo: STRQOUTREN */
    public final static String STRQOUTREN = 'Outbound-Rentado';
    /** Variable de Apoyo: STRREGCOTREN */
    public final static String STRREGCOTREN = 'Rentado';
    /** Variable de Apoyo: CreatePrices */
    public final static String CREATEPRICES = 'createPrices';
    /**@description llave de mapa*/
    private final static string TOKEN = '12345678';
    /**@description llave de ERROR*/
    private final static string TSEC = 'tsec';
    
    /**
     * @description: función de preparación Clase de Prueba 
     * @author: 	 Alexandro Corzo
     */   
    @TestSetup
    static void makeData() {      
        final String strOppoORId = obtOppoId(STROPPOOREN);
        obtQuoteId(strOppoORId, STRQOUTREN, '1029384756');
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'createPrices', iaso__Url__c = sUrl, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
    }
    
    /**
     * @description: Instrucción de Clase de Prueba Outbound - Rentado
     * @author: 	 Alexandro Corzo
     */
    @isTest static void tstDataOutRen() {
        final User oUsr = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(oUsr) {
            final String objMCallOutStr = MX_SB_VTS_ScheduldOppsTele_tst.quotetationStr(CREATEPRICES);
            final Map<String, String> headersMock = new Map<String, String>();
            headersMock.put(TSEC, TOKEN);
            final MX_WB_Mock objMCallOut = new MX_WB_Mock(200, 'Complete', objMCallOutStr, headersMock);
            iaso.GBL_Mock.setMock(objMCallOut);
            Test.startTest();
                final String strOppoName = STROPPOOREN;                                      
                final List<Opportunity> lstOppo = MX_RTL_Opportunity_Selector.resultQueryOppo('Id', 'Name =\'' + strOppoName + '\'', true);
                final String strOppoId = lstOppo[0].Id;                   
                final List<Object> lstData = MX_SB_VTS_GetSetDatosPricesSrv_Ctrl.setDataPricesCtrl(strOppoId, '1000000');
            Test.stopTest();
            System.assert(!lstData.isEmpty(), sNoEmpty);
        }
    }
      
	/**
     * @description: Instrucción de Clase de Prueba Regenera Cotización
     * @author: 	 Alexandro Corzo
     */
   	@isTest static void tstReDatCot() {
   		Test.startTest();
        	final String strOppoRen = STROPPOOREN;                                      
            final List<Opportunity> lstOppoDat = MX_RTL_Opportunity_Selector.resultQueryOppo('Id', 'Name =\'' + strOppoRen + '\'', true);
        	final String strOppId = lstOppoDat[0].Id;
        	MX_SB_VTS_GetSetDatosPricesSrv_Ctrl.reDataCotizCtrl(strOppId, STRREGCOTREN);
        Test.stopTest();
        System.assert(!String.isEmpty(strOppId),'Se obtuvo identificador de Oportunidad');
    }
    
	/**
     * @description: Instrucción de Clase de Prueba Modifica Dato Part.
     * @author: 	 Alexandro Corzo
     */
	@isTest static void tstDatPricLst() {
        final User oUsrTst = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(oUsrTst) {
        	final String objMCallOutStr = MX_SB_VTS_ScheduldOppsTele_tst.quotetationStr(CREATEPRICES);
            final Map<String, String> headersMock = new Map<String, String>();
            headersMock.put(TSEC, TOKEN);
            final MX_WB_Mock objMCallOut = new MX_WB_Mock(200, 'Complete', objMCallOutStr, headersMock);
            iaso.GBL_Mock.setMock(objMCallOut);
			Test.startTest();
				final List<Object> lstDatPart = new List<Object>();
                lstDatPart.add(new Map<String, String>{'criterial' => '2008SA', 'id_data' => '001', 'value' => '500000'});
				final String strOppoTst = STROPPOOREN;                                      
                final List<Opportunity> lstOppoTst = MX_RTL_Opportunity_Selector.resultQueryOppo('Id', 'Name =\'' + strOppoTst + '\'', true);
                final String strOppoId = lstOppoTst[0].Id;
				final List<Object> lstResult = MX_SB_VTS_GetSetDatosPricesSrv_Ctrl.setDataPricCtrlLst(strOppoId, '1000000', lstDatPart);
			Test.stopTest();
			System.assert(!lstResult.isEmpty(), sNoEmpty);
        }
	}
    
     /**
     * @description: Instrucción de Clase de Prueba: Covertura Wrapper
     * @author: 	 Alexandro Corzo
     */
	@isTest static void initDataWrp() {
    	final MX_SB_VTS_wrpPricesDataSrvRsp.data objWrapper = new MX_SB_VTS_wrpPricesDataSrvRsp.data();
        final MX_SB_VTS_wrpPricesDataSrvRsp.frequencies objWrpFreq = new MX_SB_VTS_wrpPricesDataSrvRsp.frequencies();
        final MX_SB_VTS_wrpPricesDataSrvRsp.amounts objWrpAmouts = new MX_SB_VTS_wrpPricesDataSrvRsp.amounts();
        final MX_SB_VTS_wrpPricesDataSrvRsp.unitsElements objWrpUnEle = new MX_SB_VTS_wrpPricesDataSrvRsp.unitsElements();
        final MX_SB_VTS_wrpPricesDataSrvRsp.goodTypes objWrpGT = new MX_SB_VTS_wrpPricesDataSrvRsp.goodTypes();
        objWrapper.allianceCode = 'DHSDT003';
        objWrapper.coveredArea = '1';
        objWrapper.certifiedNumberRequested = '1';
        objWrapper.region = 'VCIP';
        objWrapper.protectionLevel = 5.0;
        objWrpFreq.iddata ='YEARLY';
        objWrpFreq.description = 'ANUAL';
        objWrpFreq.numberSubsequentPayments = 0;
        objWrpAmouts.amount = '28853';
        objWrpAmouts.currencydata = 'MXN';
        objWrpAmouts.isMajor = true;
        objWrpUnEle.relationType = 466751.5;
        objWrpUnEle.currencydata = 'MXN';
        objWrpUnEle.isMajor = true;
        objWrpGT.code = '16664657';
        objWrpGT.description = 'CRISTALES';
        System.assert(!String.isEmpty(objWrapper.allianceCode), 'El wrapper esta lleno');
	}
    
    /**
     * @description: Genera una Opportunity para clase de prueba
     * @author: 	 Alexandro Corzo
     */
    private static String obtOppoId(String strNameOppo) {
        final User oUsrTst = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        insert oUsrTst;
        final Account oAccTst = MX_WB_TestData_cls.crearCuenta(sContratos, sPersonAcct);
        oAccTst.PersonEmail = 'test@test.com';
        oAccTst.PersonMobilePhone = '5555555555';
        insert oAccTst;
        final Opportunity oppObjTst = MX_WB_TestData_cls.crearOportunidad(strNameOppo, oAccTst.Id, oUsrTst.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
        oppObjTst.Reason__c = sVenta;
        oppObjTst.Producto__c = System.Label.MX_SB_VTS_Hogar;
        oppObjTst.StageName = System.Label.MX_SB_VTS_COTIZACION_LBL;
        oppObjTst.TelefonoCliente__c = oAccTst.PersonMobilePhone;
        oppObjTst.LeadSource = System.Label.MX_SB_VTS_OrigenCallMeBack;
        insert oppObjTst;
        final MX_RTL_MultiAddress__c addrTst = new MX_RTL_MultiAddress__c();
        addrTst.MX_RTL_MasterAccount__c = oAccTst.Id;
        addrTst.MX_RTL_AddressStreet__c = 'test';
        addrTst.MX_RTL_AddressType__c = DOMTYPE;
        addrTst.Name = 'Direccion Propiedad';
        addrTst.MX_RTL_Opportunity__c = oppObjTst.Id;
        addrTst.MX_RTL_PostalCode__c = '14260';
        addrTst.MX_RTL_Tipo_Propiedad__c = 'Rentado';
        addrTst.MX_SB_VTS_Metros_Cuadrados__c  = '100';
        insert addrTst;
        return oppObjTst.Id;
    }

    /**
     * @description: Genera una Quote para clase de prueba
     * @author: 	 Alexandro Corzo
     */                                                   
    private static String obtQuoteId(String strOpporId, String strQuoteName, String strFolioCot) {
        final Quote objQuoTst = MX_WB_TestData_cls.crearQuote(strOpporId, strQuoteName, strFolioCot);
        objQuoTst.MX_SB_VTS_ASO_FolioCot__c = strFolioCot;
        objQuoTst.QuoteToName = strQuoteName;
        insert objQuoTst;
        final Opportunity objOppTst = new Opportunity();
        objOppTst.Id = strOpporId;
        objOppTst.SyncedQuoteId = objQuoTst.Id;
        update objOppTst;
        return objQuoTst.Id;
    }
}