/**
* @FileName          : MX_SB_VTS_GetQuotation_Service
* @description       : Service class for use de web service of generarOTP
* @Author            : Marco Antonio Cruz Barboza  
* @last modified on  : 18-09-2020
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   02-09-2020   Marco Antonio Cruz Barboza          Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:AvoidGlobalModifier, sf:UseSingleton')
global class MX_SB_VTS_GetQuotation_Service {

    /**
    * @description : Call an web service apex method to get de Status from OTP Service
    * @Param String Phone
    * @Return Map<String, String> with status and code from the result of webservice use.
    **/
    public static Map<String,String> getQuotation(Map <String, String> idCotiza) {
        Final Map<String,String> getValues = new Map<String,String>();
        Final String EncryptedId = JSON.serialize(new Map<String,Object> {
            'EncryptedId' => IdCotiza.get('0')
        });
        final HttpResponse getQuotation = MX_RTL_IasoServicesInvoke_Selector.callServices('GetQuotation', EncryptedId);
        getValues.put('Body', String.valueOf(getQuotation.getBody()));
        return getValues;
    }
    
}