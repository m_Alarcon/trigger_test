/**
 * @File Name          : MX_SB_VTS_CotizInitData_Service.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Diego Olvera
 * @Last Modified On   : 09-24-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    25/5/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
public without sharing class MX_SB_VTS_CotizInitData_Service {
    /**Constructor */
    @SuppressWarnings('sf:UseSingleton')
    private MX_SB_VTS_CotizInitData_Service() {}

    /**
    * @description Recupera Oportunidades por set de Ids
    * @author Eduardo Hernandez Cuamatzi | 25/5/2020 
    * @param Set<String> setOppId Set de Ids enviados desde el front
    * @return List<Opportunity> Lista de Oportunidades resultantes
    **/
    public static List<Opportunity> findOppsById(Set<String> setOppId) {
        return MX_RTL_Opportunity_Selector.selectSObjectsById(setOppId);
    }

    /**
    * @description Recuperar Quotes para lista de Oportunidades
    * @author Eduardo Hernandez Cuamatzi | 25/5/2020 
    * @param List<Opportunity> lstOpps Lista de Oportunidades a evaluar
    * @return List<Quote> Lista de Quotes resultantes
    **/
    public static List<Quote> findQuotesByOpp(List<Opportunity> lstOpps) {
        return MX_RTL_Quote_Selector.selectSObjectsByOpps(lstOpps);
    }

    /**
    * @description Recuperar Quotes para lista de Oportunidades por Id
    * @author Eduardo Hernandez Cuamatzi | 25/5/2020 
    * @param String quoteIdStrlstOpps Lista de Oportunidades a evaluar
    * @return List<Quote> Lista de Quotes resultantes
    **/
    public static List<Quote> findQuotesById(String quoteIdStr) {
        return MX_RTL_Quote_Selector.selectSObjectsById(quoteIdStr);
    }

    /**
    * @description Recuperar lista de Quotes relacionada a lista de Opps
    * @author Eduardo Hernandez Cuamatzi | 25/5/2020 
    * @param List<Opportunity> lstOpps Lista de Oportunidades a evaluar
    * @return Map<Id, Quote> Mapa de Quotes recuperadas
    **/
    public static Map<Id, Quote> findQuotesByOppMap(List<Opportunity> lstOpps) {
        return new Map<Id, Quote>(findQuotesByOpp(lstOpps));
    }

    /**
    * @description Recupera QuotesLineItems para un set de Oportunidades
    * @author Eduardo Hernandez Cuamatzi | 25/5/2020 
    * @param Set<Id> setOpps lista de Ids de Oportunidades
    * @return List<QuoteLineItem>  lista de QuoteLineItems
    **/
    public static List<QuoteLineItem> findQuolisByQuotMap(Set<Id> setOpps) {
        return QuoteLineItemsSelector.selSObjectsByQuotes(setOpps);
    }

    /**
    * @description Recupera un mapa de Id  QuoteLineItems a partir de un mapa de Quotes
    * @author Eduardo Hernandez Cuamatzi | 25/5/2020 
    * @param Map<Id Quote> mapQuote 
    * @return Map<Id, QuoteLineItem> 
    **/
    public static Map<Id, QuoteLineItem> findQuolisByQuotMap(Map<Id, Quote> mapQuote) {
        return new  Map<Id, QuoteLineItem>(findQuolisByQuotMap(mapQuote.keySet()));
    }
      /**
    * @description Actualiza valor de Oportunidad desde el front con valor de Propio o rentado
    * dependiendo de lo que el usuario seleccione.
    * @author Diego Olvera | 17-09-2020 
    * @param polizNum recId, strValPropio, strValRentado
    * @return List<Opportunity> Respuesta de validación
    **/
    public static List<Opportunity> updCurrRecService(String recId, String strValPropio, String strValRentado) {
        final Set<String> setOppsId = new Set<String>();
        setOppsId.add(recId);
        final List<Opportunity> oppRecords = MX_RTL_Opportunity_Selector.selectSObjectsById(setOppsId);
        for(Opportunity getLstOpp : oppRecords) {
            if(String.isNotBlank(strValPropio)) {
                getLstOpp.MX_WB_comentariosCierre__c = strValPropio;
            } else {
                getLstOpp.MX_WB_comentariosCierre__c = strValRentado;
            }
        }
        MX_RTL_Opportunity_Selector.updateResult(oppRecords, false);
        return oppRecords;
    }

    /**
    * @description Recupera un quote por Nombre o por Id
    * @author Eduardo Hernández Cuamatzi | 01-08-2021 
    * @param quoteName Nombre o Id de quote
    * @return Map<Id, Quote> Mapa de QuoteId
    **/
    public static Map<Id, Quote> findQuotesByNameId(String quoteName) {
        return new Map<Id, Quote>(MX_RTL_Quote_Selector.selectSObjectsById(quoteName));
    }
}