/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase test de BPyP_LinkFactset

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     14/10/2019      Roberto Isaac Soto Granados           Creación Clase
*****************************************************************************************/
@isTest
private class BPyP_LinkFactset_Test {
    /**
    * Usuario de pruebas
    */
    private static User testUser = new User();
    /**
    * Variable de acceso denegado
    */
    final static Boolean ACCESSDENIED = false;

    /**
    * Setup para clase de prueba
    */
    @testSetup
    static void setup() {
        testUser = UtilitysDataTest_tst.crearUsuario('testUser', 'BPyP Estandar', 'BPYP BANQUERO BANCA PERISUR');
        testUser.Title = 'Privado';
        insert testUser;
    }

    /**
    * Valida el acceso permitido
    */
    static testMethod void testUserConnectionTrue() {
        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
            System.runAs(testUser) {
            Test.startTest();
            final Boolean boolTest = BPyP_LinkFactset.checkAccess();
            Test.stopTest();
            System.assertEquals(ACCESSDENIED, boolTest, 'Acceso no permitido');
        }
    }

    /**
    * Valida el acceso no permitido
    */
    static testMethod void testUserConnectionFalse() {
        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        testUser.Title = '';
        update testUser;
        System.runAs(testUser) {
            Test.startTest();
            final Boolean boolTest = BPyP_LinkFactset.checkAccess();
            Test.stopTest();
            System.assertEquals(ACCESSDENIED, boolTest, 'Acceso no permitido');
        }
    }
}