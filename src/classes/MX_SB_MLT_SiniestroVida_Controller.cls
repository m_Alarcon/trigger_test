/*
* @Nombre: MX_SB_MLT_SiniestroVida_Controller
* @Autor: Marco Antonio Cruz Barboza
* @Proyecto: Siniestros - BBVA
* @Descripción : Clase que almacena los methods usados en la Toma de Reporte de Siniestro Vida
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   	Descripción
* --------------------------------------------------------------------------------
* 1.0         03/04/2020     Marco Antonio Cruz Barboza		Creación
* 1.1         13/04/2020	 Juan Carlos Benitez Herrera	Methodo documentos requeridos siniestro Vida
* 1.2	      21/04/2020     Eder Alberto Hernández 		Méthodo información del fallecido
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_SiniestroVida_Controller {
    /*
	* var String Valor boton cancelar
	*/    
    public static final String CANCELAR = 'Cancelar';
    /*
	* var String Valor boton Volver
	*/    
    public static final String VOLVER = 'Volver';
    /*
	* var String Valor boton Crear
	*/    
    public static final String CREAR = 'Crear';
    /*
    * @description constructor privado para evitar singleton
    * @param  void
    */
    private MX_SB_MLT_SiniestroVida_Controller() {
    }
	/*
    * @description Realiza la busqueda de póliza en contratos y retorna el Id del contrato en caso de encontrarla
    * @param String policy
    * @return String idContract
    */
    public static String searchPolicy(String policy) {
        String idContract = [SELECT Id FROM Contract WHERE MX_SB_SAC_NumeroPoliza__c=:policy].Id;
        if (String.isEmpty(idContract)) {
            idContract = '';
        }
        return idContract;
    }
    /*
    * @description Clase que recibe un objeto tipo Task y realiza la inserción asignando el recordType correspondiente
    * @param Task taskCreate
    */
    public static void taskInsertion(Task taskCreate) {
		taskCreate.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_CitaSiniestros').getRecordTypeId();
        taskCreate.ActivityDate = taskCreate.ReminderDateTime.date();
		upsert taskCreate;
    }
    /*
    * @description METHODO busqueda siniestro vida
    * @param String siniId
    */
    @AuraEnabled
    public static Siniestro__c searchSinVida (String siniId) {
       try {
           final List<Siniestro__c> listaVida = [SELECT Id, MX_SB_MLT_Telefono__c, MX_SB_MLT_NombreConductor__c,MX_SB_MLT_APaternoConductor__c, MX_SB_MLT_AMaternoConductor__c,
                                             	MX_SB_MLT_NombreAsegurado__c, MX_SB_MLT_InsuredLastName2__c, MX_SB_MLT_InsuredLastName__c, MX_SB_MLT_CorreoElectronico__c,
                                                CreatedDate, MX_SB_MLT_InsuranceCompany__c, MX_SB_MLT_SubRamo__c, TipoSiniestro__c, MX_SB_MLT_NameDefunct__c, MX_SB_MLT_NamePDefunct__c,
                                                MX_SB_MLT_NameMDefunct__c, MX_SB_MLT_Relationship__c, MX_SB_MLT_Subtipo__c, MX_SB_MLT_DateDifunct__c, MX_SB_MLT_Reembolso__c, MX_SB_MLT_MotivoReembolso__c,
                                                MX_SB_SAC_Contrato__r.MX_SB_SAC_NombreClienteAseguradoText__c, MX_SB_SAC_Contrato__r.MX_WB_apellidoPaternoAsegurado__c,
												MX_SB_SAC_Contrato__r.MX_WB_apellidoMaternoAsegurado__c, MX_SB_SAC_Contrato__r.MX_SB_SAC_EmailAsegurado__c, MX_SB_MLT_FechaNacimiento__c,
                                                MX_SB_MLT_Curp__c, MX_SB_MLT_FechaNacConductor__c,MX_SB_MLT_DireccionDestino__c, MX_SB_MLT_Address__c, Fechasiniestro__c
                                                 FROM Siniestro__c WHERE id=:siniId];
           return listaVida[0];
       } catch(Exception dny) {
               throw new AuraHandledException (System.Label.MX_WB_LG_ErrorBack +' No se encontraron registros con este id '+ JSON.serialize(dny));
       } 
    } 

    /*
    * @description Realiza la consulta del label del recordType para asignación de Linea de Negocio
    * @param String recordId
    * @return String developerNameRT
    */
    @AuraEnabled
    public static String searchRecordType (String recordId) {
        Final Id recordTypeId = [SELECT RecordTypeId FROM Siniestro__c WHERE Id=: recordId].RecordTypeId;
        return [SELECT DeveloperName FROM RecordType WHERE Id =: recordTypeId].DeveloperName;
    }
        /*
    * @description Realiza la consulta del label del recordType para asignación de Linea de Negocio
    * @param String recordId
    * @return String historySearch
    */
    @AuraEnabled
    public static Sobject historyFields (String recordId) {
		Sobject historySearch = new Siniestro__c();
		historySearch = [SELECT CreatedDate, MX_SB_MLT_ComentariosSiniestro__c,CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, 
                         (SELECT OldValue, newvalue FROM histories 
                          WHERE ParentId =: recordId AND 
                          Field = 'MX_SB_MLT_ComentariosSiniestro__c') 
                         FROM Siniestro__c WHERE Id =: recordId];
        return historySearch;
    }

    /*
    * @description Methodo que obtiene los campos necesarios del siniestro para evaluar reglas de kits de vida
    * @param String sinId
    * return Siniestro__c
    */
    @AuraEnabled 
    public static Siniestro__c camposSin (String sinId) {
        return [SELECT Id, MX_SB_SAC_Contrato__r.StartDate,MX_SB_MLT_Subtipo__c, MX_SB_SAC_Contrato__r.EndDate,MX_SB_MLT_SubRamo__c,MX_SB_SAC_Contrato__r.MX_WB_Producto__r.Name, MX_SB_MLT_FechaNacimiento__c,MX_SB_MLT_DateDifunct__c from Siniestro__c where id=:sinId limit 1];
    }
    /*
    * @description Realiza la consulta de documentacion requerida en siniestro Vida
    * @param String product, String subramo, String subtipo,String regla1, String regla2
    * @return String listdocs
    */
    @AuraEnabled
    public static List < MX_SB_MLT_DocumentosVida__mdt > searchkit(Map<String,String> mapa) {
      		final String product =mapa.get('product');
            final String subramo=mapa.get('subramo');
            final String subtipo=mapa.get('subtipo');
            final String regla1=mapa.get('regla1');
	        final String regla2=mapa.get('regla2');
        	final String idSin=mapa.get('idSin');
            final string kit = [SELECT MX_SB_MLT_Kit__c FROM MX_SB_MLT_Matriz_Kit_Vida__mdt
                                                       	WHERE MX_SB_MLT_Producto__c=:product and
                                                        MX_SB_MLT_SubRamo__c=:subramo and MX_SB_MLT_Subtipo__c=:subtipo
                                                        and MX_SB_MLT_Regla1__c=:regla1 
      													and MX_SB_MLT_Regla2__c=:regla2].MX_SB_MLT_Kit__c;
        final string kits = '%'+ kit +'%';
        final siniestro__c siniestro =[select Id,MX_SB_MLT_KitVida__c from siniestro__c where id=:idSin];
        siniestro.MX_SB_MLT_KitVida__c=kit;
        update siniestro;
	return [SELECT MX_SB_MLT_Enum__c , DeveloperName , MX_SB_MLT_DocumentosRequeridos__c, Kits__c
                FROM MX_SB_MLT_DocumentosVida__mdt where Kits__c LIKE :kits ORDER BY MX_SB_MLT_Enum__c  asc ];
    }
    /*
    * @description Cambio de etapa de flujo de vida
    * @param String buttonValue,String strComentarios, String siniId
    * @return String void
    */
    @AuraEnabled
    public static void changeStep(String buttonValue,String strComentarios, String siniId ) {
		MX_SB_MLT_Vida_Controller.changeStep(buttonValue, strComentarios, siniId);
    }
}