/*
autor: VASS, Mario Rodriguez
fecha: 25/06/2018
descripción: Controlador para componente Lightning: Cotizador_ASD, se encarga de crear la URL con los datos del cliente para enviarlos al cotizador.
*/

public with sharing class ASD_GLB_Opp_Cotizador_Ctrl {
    /*Constante para el producto Seguro de Moto Bancomer para ASD*/
    static final String PRIVATE_STR_SMB = Constantes_ASD__c.getInstance().Seguro_Moto_Bancomer__c;

    /*Constante para el producto Seguro Fronterizo para ASD*/
    static final String PRIVATE_STR_SF = Constantes_ASD__c.getInstance().Seguro_Fronterizo__c;

    /*Constante para el producto Auto Seguro Dinámico para ASD*/
    static final String PRIVATE_STR_ASD = Constantes_ASD__c.getInstance().Auto_Seguro_Dinamico__c;

    /*Constante para el producto Auto Seguro Dinámico para ASD*/
    static final String PRIVATE_STR_SN = Constantes_ASD__c.getInstance().Seguro_Nacionalizado__c;

    /*Variable que contiene los datos del usuario actual.*/
    static String strDatosUsuario;

    /*Variable que contiene la creación de Fechas Milis.*/
    static String strFechaMilisApex;

    /*Variable que contiene la creación de la Firma.*/
    static String strFirmaMain;

    private ASD_GLB_Opp_Cotizador_Ctrl() {

    }



    static {
        strDatosUsuario = '';
        strFechaMilisApex = '';
        strFirmaMain = '';
    }

    /*
Crea la URL necesaria para mostrar el Cotizador en el componente lightning  que se utiliza en la página de Cotizaciones.
*/

    @AuraEnabled
    public static String crearURL(Id oppId) {
        final String exceptionError = 'Algo fallo : ';
        String finalURL;
        List<Opportunity> lstOpportunity;
        String strUsuario;
        String strCanal;
        String strSubCanal;
        String strAgencia;
        String strServicio;
        final Map<Id,User> mapUsuario = new Map<Id,User>();

        try {

            final List<User> lstUsuario = [SELECT  Id, Usuario__c, Canal__c, Sub_Canal__c, Agencia__c,Servicio__c FROM User WHERE Id =: UserInfo.getUserId()];

            for(User u :lstUsuario) {
                mapUsuario.put(u.Id,u);
            }

            strUsuario = mapUsuario.get(UserInfo.getUserId()).Usuario__c;
            strCanal = mapUsuario.get(UserInfo.getUserId()).Canal__c;
            strSubCanal = mapUsuario.get(UserInfo.getUserId()).Sub_Canal__c;
            strAgencia = mapUsuario.get(UserInfo.getUserId()).Agencia__c;
            strServicio = mapUsuario.get(UserInfo.getUserId()).Servicio__c;
            strDatosUsuario = '&usuario=' + strUsuario +
                '&servicio=' + strServicio +
                '&agencia=' + strAgencia +
                '&canal=' + strCanal +
                '&subcanal=' + strSubCanal;

            strFechaMilisApex = Utilities.fnGeneradorFechaMilis();

            lstOpportunity = [SELECT Id, Name,RecordType.Name,nombreDelContratante__c,CorreodelCliente__c, CodigoPostal__c,
                              Fecha_Nacimiento_Contratante__c,LeadSource,AccountId,Salvamento__c,Description,Producto__c,Sexo_conductor__c,FolioCotizacion__c,Account.LastName,Telefono__c
                              FROM Opportunity WHERE ID = :oppId];

            strFirmaMain = Utilities.fnGeneradorMD5(strFechaMilisApex, strUsuario,strCanal,strSubCanal,strAgencia,strServicio);

            if(lstOpportunity[0].Producto__c == PRIVATE_STR_SMB) {
                finalURL = ASD_GLB_Opp_Cotizador_Ctrl.crearURLSeguroMotoBancomer(lstOpportunity,strFechaMilisApex,strDatosUsuario,strFirmaMain);
            } else if (lstOpportunity[0].Producto__c == PRIVATE_STR_SF) {
                finalURL = ASD_GLB_Opp_Cotizador_Ctrl.crearURLSeguroFronterizo(lstOpportunity,strFechaMilisApex,strDatosUsuario,strFirmaMain);
            }else if(lstOpportunity[0].Producto__c == PRIVATE_STR_SN) {
                finalURL = ASD_GLB_Opp_Cotizador_Ctrl.crearURLSeguroFronterizo(lstOpportunity,strFechaMilisApex,strDatosUsuario,strFirmaMain);
            } else if(lstOpportunity[0].Producto__c == PRIVATE_STR_ASD) {
                finalURL= ASD_GLB_Opp_Cotizador_Ctrl.crearURLAutoSeguroDinamico(lstOpportunity,strFechaMilisApex,strDatosUsuario,strFirmaMain);
            }
            system.debug('RESPONSE URL: '+finalURL);
            return finalURL;

        } catch (QueryException e) {
            throw new AuraHandledException(Label.MX_WB_ERROR + '\nMensaje: ' + e.getMessage() + '\nLinea: ' + e.getLineNumber() + e);
        }
    }
    /*
descripcion: Crea parte de la URL basados en los datos de la Cotización (oportunidad) y el Producto, en este caso: Seguro de moto
retorno: regresa parte de la URL con los datos de la cotización(oportunidad)
*/
	@TestVisible
    private static String crearURLSeguroMotoBancomer(List<Opportunity> lstOpportunity,String strFechaMilisApex, String strDatosUsuario,String strFirma) {
            String urlSMB;
            String oppCot;
            oppCot = (lstOpportunity[0].CodigoPostal__c == null) ? '' : lstOpportunity[0].CodigoPostal__c;

            final List<URL_Auto_Seguro_Bancomer__mdt> lstMSB = [SELECT DeveloperName,URL__c,tipoUsoVehiculoCot__c FROM URL_Auto_Seguro_Bancomer__mdt WHERE MasterLabel = 'URLMSB' limit 1];
            urlSMB = lstMSB[0].URL__c + '?' + 'fechaMilis=' + strFechaMilisApex +
            strDatosUsuario +
            '&firma=' + strFirma +
            '&nombreCot=' + lstOpportunity[0].nombreDelContratante__c +
            '&emailCot=' + lstOpportunity[0].CorreodelCliente__c+
            '&telefonoCot=' + lstOpportunity[0].Telefono__c +
            '&sexoCot=' + lstOpportunity[0].Sexo_conductor__c +
            '&codigoPostalCot=' + oppCot +
            '&fechaDeNacimientoCot=' + lstOpportunity[0].Fecha_Nacimiento_Contratante__c +
            '&idClienteCot=' + lstOpportunity[0].AccountId +
            '&idOportunidadComercialCot=' + lstOpportunity[0].Id +
            '&origenOportunidadCot=' + lstOpportunity[0].LeadSource +
            '&tipoUsoVehiculoCot=' + lstMSB[0].tipoUsoVehiculoCot__c;
            return urlSMB;
     }

    @TestVisible
    private static String crearURLSeguroFronterizo(List<Opportunity> lstOpportunity,String strFechaMilisApex, String strDatosUsuario,String strFirma) {
            String urlSF;
            String oppCot;
            oppCot = (lstOpportunity[0].CodigoPostal__c == null) ? '' : lstOpportunity[0].CodigoPostal__c;


            final List<URL_Auto_Seguro_Bancomer__mdt> lstSF = [SELECT DeveloperName,URL__c,tipoUsoVehiculoCot__c FROM URL_Auto_Seguro_Bancomer__mdt WHERE MasterLabel = 'URLSFN' limit 1];
            urlSF = lstSF[0].URL__c + '?' + 'fechaMilis=' + strFechaMilisApex +
            strDatosUsuario +
            '&firma=' + strFirma +
            '&nombreCot=' + lstOpportunity[0].nombreDelContratante__c +
            '&emailCot=' + lstOpportunity[0].CorreodelCliente__c+
            '&telefonoCot=' + lstOpportunity[0].Telefono__c +
            '&sexoCot=' + lstOpportunity[0].Sexo_conductor__c +
            '&codigoPostalCot=' + oppCot +
            '&fechaDeNacimientoCot=' + lstOpportunity[0].Fecha_Nacimiento_Contratante__c +
            '&idClienteCot=' + lstOpportunity[0].AccountId +
            '&idOportunidadComercialCot=' + lstOpportunity[0].Id +
            '&origenOportunidadCot=' + lstOpportunity[0].LeadSource +
            '&tipoUsoVehiculoCot=' + lstSF[0].tipoUsoVehiculoCot__c;
            return urlSF;
     }

     /*
        descripcion: Crea parte de la URL basados en los datos de la Cotización (oportunidad) y el Producto, en este caso: Auto Seguro Dinámico
        retorno: regresa parte de la URL con los datos de la cotización(oportunidad)
    */
     @TestVisible
     private static String crearURLAutoSeguroDinamico(List<Opportunity> lstOpportunity,String strFechaMilisApex, String strDatosUsuario,String strFirma) {
            String urlASD;
            String oppCot;
            oppCot = (lstOpportunity[0].CodigoPostal__c == null) ? '' : lstOpportunity[0].CodigoPostal__c;

            final List<URL_Auto_Seguro_Bancomer__mdt> lstASD = [SELECT DeveloperName,URL__c,tipoUsoVehiculoCot__c FROM URL_Auto_Seguro_Bancomer__mdt WHERE MasterLabel = 'URLASB' limit 1];
            urlASD = lstASD[0].URL__c + '?' + 'fechaMilis=' + strFechaMilisApex +
            strDatosUsuario +
            '&firma=' + strFirma +
            '&nombreCot=' + lstOpportunity[0].nombreDelContratante__c +
            '&emailCot=' + lstOpportunity[0].CorreodelCliente__c+
            '&telefonoCot=' + lstOpportunity[0].Telefono__c +
            '&sexoCot=' + lstOpportunity[0].Sexo_conductor__c +
            '&codigoPostalCot=' + oppCot +
            '&fechaDeNacimientoCot=' + lstOpportunity[0].Fecha_Nacimiento_Contratante__c +
            '&idClienteCot=' + lstOpportunity[0].AccountId +
            '&idOportunidadComercialCot=' + lstOpportunity[0].Id +
            '&origenOportunidadCot=' + lstOpportunity[0].LeadSource +
            '&tipoUsoVehiculoCot=' + lstASD[0].tipoUsoVehiculoCot__c;
            return urlASD;
     }

}