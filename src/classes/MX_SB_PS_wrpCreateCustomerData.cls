/*
* BBVA - Mexico - Seguros
* @Author: Daniel Perez  Lopez
* MX_SB_PS_wrpCreateCustomerData
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Daniel perez                 04/11/2020                 Creación de la clase.
1.1                Juan Carlos Benitez           04/01/2021               Se modifican SuppressWarnings
1.2                Juan Carlos Benitez           05/01/2021             Se añade subclass mainContactData , cellphone
*/

@SuppressWarnings('sf:ExcessivePublicCount,sf:ShortVariable,sf:LongVariable,sf:ShortClassName,sf:VariableNamingConventions')
/**maini class of wrapper */
public with sharing class MX_SB_PS_wrpCreateCustomerData {
    /*Public property for wrapper*/
    public header header {get;set;}
    /*Public property for wrapper*/
    public quote quote {get;set;}
    /*Public property for wrapper*/
    public holder holder {get;set;}
    /*Public property for wrapper*/
    public clientType clientType {get;set;}
    /*Public property for wrapper*/
    public Boolean contractingHolderIndicator {get;set;}
    /**public property for wraper */
    public String certificateNumber {get;set;}
    /**public property for wraper */
    public shippingChannel shippingChannel {get;set;}
	/*Public constructor*/
    public MX_SB_PS_wrpCreateCustomerData() {
        this.header = new header();
        this.holder = new holder();
        this.quote = new quote();
        this.contractingHolderIndicator =true;
        this.certificateNumber = '1';
        this.shippingChannel = new shippingChannel();
        this.clientType = new clientType();
    }

    /*public subclass for wraper */
    public class shippingChannel {
         /*Public property for wrapper*/
         public String id {get;set;}
         /**public constructor for wraper */
         public shippingChannel () {
             this.id='EMAIL';
         }
    }
	  /*Public subclass*/
    public class header {
		  /*Public property for wrapper*/
        public String dateRequest {get;set;}
        /*Public property for wrapper*/
        public String aapType {get;set;}
		  /*Public property for wrapper*/
        public String channel {get;set;}
		  /*Public property for wrapper*/
        public String subChannel {get;set;}
           /*Public property for wrapper*/	
        public String managementUnit {get;set;}
            /*Public property for wrapper*/	
        public String user {get;set;}
          /*Public property for wrapper*/
        public String branchOffice {get;set;}
		  /*Public property for wrapper*/
        public String idSession {get;set;}
		  /*Public property for wrapper*/
        public String idRequest {get;set;}
		  /*Public property for wrapper*/
        public String dateConsumerInvocation {get;set;}
		 /*public constructor subclass*/
		public header() {
		 this.aapType = '10000137';
		 this.dateRequest = '2016-08-06 12:33:27.104';
		 this.channel = '4';
		 this.subChannel = '71';
		 this.branchOffice = '23';
		 this.managementUnit = 'VVSH0001';
		 this.user = 'CARLOS';
		 this.idSession = '3232-3232';
		 this.idRequest = '1212-121212-12121-212';
		 this.dateConsumerInvocation = '2016-08-06 12:33:27.1';
        }
    }
        /*
        *public subclass */
        public class quote {
        /*Public property for wrapper*/
            public String idQuote {get;set;}
        /**constructor para wraper */
            public quote() {
                this.idQuote='';
            }
        }/*
        *public subclass */
        public class holder {
            
            /*Public property for wrapper*/
            public String id {get; set;}
            /*Public property for wrapper*/
            public String email {get; set;}
            /*Public property for wrapper*/
            public String rfc {get; set;}
            /*Public property for wrapper*/
            public activity activity {get; set;}
            /*Public property for wrapper*/
            public mainAddress mainAddress {get; set;}
            /*Public property for wrapper*/
            public mainAddress correspondenceAddress {get; set;}
            /*Public property for wrapper*/
            public mainAddress legalAddress {get; set;}
            /*Public property for wrapper*/
            public nacionality nacionality {get; set;}
            /*Public property for wrapper*/
            public mainContactData mainContactData {get; set;}
            /*Public property for wrapper*/
            public mainContactData correspondenceContactData {get; set;}
            /*Public property for wrapper*/
            public mainContactData fiscalContactData {get; set;}
            /*Public property for wrapper*/
            public physicalPersonalityData physicalPersonalityData {get; set;}
            /*Public property for wrapper*/
            public deliveryInformation deliveryInformation {get; set;}
            /**constructor para wraper */
            public holder() {
                this.id='';
                this.email ='';
                this.rfc ='';
                this.activity = new activity();
                this.mainAddress = new mainAddress();
                this.correspondenceAddress = new mainAddress();
                this.legalAddress = new mainAddress();
                this.nacionality = new nacionality();
                this.mainContactData = new mainContactData();
                this.correspondenceContactData = new mainContactData();
                this.fiscalContactData = new mainContactData();
                this.physicalPersonalityData = new physicalPersonalityData();
                this.deliveryInformation = new deliveryInformation();
            }
        }

        /**public class for wrapper  */
        public class nacionality {
            /*Public property for wrapper*/
            public catalogItemBase catalogItemBase {get;set;}
            /**constructor para wraper */
            public nacionality () {
                this.catalogItemBase= new catalogItemBase('052', 'MEXICO');
            }
        }
        /*public subclass */
        public class clientType {
            /*Public property for wrapper*/
            public catalogItemBase catalogItemBase {get;set;}
            /**constructor para wraper */
            public clientType () {
                this.catalogItemBase = new catalogItemBase('P','PERSONA');
            }
        }
        /** public subclass */
        public class activity {
            /*Public property for wrapper*/
            public catalogItemBase catalogItemBase {get;set;}
            /**constructor para wraper */
            public activity() {
                this.catalogItemBase = new catalogItemBase('706','RECURSOS HUMANOS');
            }
        }
        /** public subclass */
        public class mainAddress {
            /*Public property for wrapper*/
            public String zipCode {get;set;}
            /*Public property for wrapper*/
            public String streetName {get;set;}
            /*Public property for wrapper*/
            public String outdoorNumber {get;set;}
            /*Public property for wrapper*/
            public String door {get;set;}
            /*Public property for wrapper*/
            public suburb suburb {get;set;}
            /**constructor para wraper */
            public mainAddress () {
                this.zipCode='';
                this.streetName='';
                this.outdoorNumber='';
                this.door='';
                this.suburb=new suburb();
            }
        }
        /** public subclass */
        public class mainContactData {
            /*Public property for wrapper*/
            public cellphone cellphone {get;set;}
            /*Public property for wrapper*/
            public cellphone phone {get;set;}
            /*Public property for wrapper*/
            public cellphone officePhone {get;set;}
            /**constructor para wraper */
            public mainContactData () {
                this.cellphone= new cellphone();
                this.phone= new cellphone();
                this.officePhone= new cellphone();
            }
            /**constructor para wraper */
            public mainContactData (String celp, String phon, String officep) {
                this.cellphone= new cellphone(celp);
                this.phone= new cellphone(phon);
                this.officePhone= new cellphone(officep);
            }
        }
        /** public subclass */
        public class physicalPersonalityData {
            /*Public property for wrapper*/
            public String name {get;set;}
            /*Public property for wrapper*/
            public String lastName {get;set;}
            /*Public property for wrapper*/
            public String birthDate {get;set;}
            /*Public property for wrapper*/
            public sex sex {get;set;}
            /*Public property for wrapper*/
            public String mothersLastName {get;set;}
            /*Public property for wrapper*/
            public String curp {get;set;}
            /*Public property for wrapper*/
            public civilStatus civilStatus {get;set;}
            /*Public property for wrapper*/
            public occupation occupation {get;set;}
            /*Public property for wrapper*/
            public fiscalPersonality fiscalPersonality {get;set;}
            /**constructor para wraper */
            public physicalPersonalityData() {
                this.name ='';
                this.lastName ='';
                this.birthDate ='';
                this.mothersLastName ='';
                this.curp ='';
                this.sex= new sex();
                this.civilStatus= new civilStatus();
                this.occupation= new occupation();
                this.fiscalPersonality= new fiscalPersonality();
            }
        }
        /** public subclass */
        public class deliveryInformation {
            /*Public property for wrapper*/
            public String referenceStreets {get;set;}
            /*Public property for wrapper*/
            public String deliveryInstructions {get;set;}
            /*Public property for wrapper*/
            public String deliveryTimeStart {get;set;}
            /*Public property for wrapper*/
            public String deliveryTimeEnd {get;set;}
            /**constructor para wraper */
            public deliveryInformation() {
                this.referenceStreets='';
                this.deliveryInstructions='';
                this.deliveryTimeStart='';
                this.deliveryTimeEnd='';
            }
        }
                    
        /**public subclass*/
        public class catalogItemBase {
            /*Public property for wrapper*/
            public String id {get;set;}
            /*Public property for wrapper*/
            public String name {get;set;}
            /**constructor para wraper */
            public catalogItemBase(String ids,String nams) {
                this.id=ids;
                this.name=nams;
            }
        }

        /**public subclass*/
        public class suburb {
            /*Public property for wrapper*/
            public neighborhood neighborhood {get;set;}
            /*pblic consructor for wraper */
            public suburb () {
                this.neighborhood = new neighborhood();
            }
        }
        /**public subclass*/
        public class neighborhood {
            /*Public property for wrapper*/
            public catalogItemBase catalogItemBase {get;set;}
            /*pblic consructor for wraper */
            public neighborhood () {
                this.catalogItemBase = new catalogItemBase ('' ,'');
            }
        }
        /**public subclass*/
        public class cellphone {
            /*Public property for wrapper*/
            public String phoneExtension {get;set;}
            /*Public property for wrapper*/
            public String telephoneNumber {get;set;}
            /*public constructor for wraper*/
            public cellphone() {
                this.phoneExtension='';
                this.telephoneNumber='';
            }
            /*public constructor for wraper*/
            public cellphone(String telpn) {
                this.phoneExtension='';
                this.telephoneNumber=telpn;
            }
        }
        /**public subclass*/
        public class sex {
            /*Public property for wrapper*/
            public catalogItemBase catalogItemBase {get;set;}
            /**public constructor for wraper class */
            public sex () {
                this.catalogItemBase= new catalogItemBase ('M','MASCULINO');
            }
        }
        /**public subclass*/
        public class civilStatus {
            /*Public property for wrapper*/
            public catalogItemBase catalogItemBase {get;set;}
            /**public constructor for wraper class */
            public civilStatus () {
                this.catalogItemBase= new catalogItemBase ('S','SOLTERO');
            }
        }
        /**public subclass*/
        public class occupation {
            /*Public property for wrapper*/
            public catalogItemBase catalogItemBase {get;set;}
            /*public constructor for wraper*/
            public occupation () {
                this.catalogItemBase = new catalogItemBase('461', 'EMPLEADO DE OFICINA');
            }
        }
        /**public subclass*/
        public class fiscalPersonality {
            /*Public property for wrapper*/
            public catalogItemBase catalogItemBase {get;set;}
            /*public constructor for wraper */
            public fiscalPersonality () {
                this.catalogItemBase = new catalogItemBase ('N','FISICA');
            }
        }            
}