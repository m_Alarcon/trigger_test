/**
 * @File Name          : MX_SB_VTS_SendTrackingTelCupon_Test.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernandez Cuamatzi
 * @Last Modified On   : 4/6/2020 14:19:05
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/6/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
@isTest
private class MX_SB_VTS_SendTrackingTelCupon_Test {
    /**Query Opps */
    final static String QUERYOPPS = 'Select Id from Opportunity where Name= \'OppTelemarketing\'';

    /**@description Nombre usuario*/
    private final static string ASESORNAME = 'Asesor';

    @TestSetup
    static void makeData() {
        final User integration = MX_WB_TestData_cls.crearUsuario('Integration', System.Label.MX_SB_VTS_ProfileIntegration);
        insert integration;

        final User admin = MX_WB_TestData_cls.crearUsuario('Integration', System.Label.MX_SB_VTS_ProfileAdmin);
        insert admin;

        final User asesorUser = MX_WB_TestData_cls.crearUsuario(ASESORNAME, 'Telemarketing VTS');
        insert asesorUser;
        
        final Account accToAssign = MX_WB_TestData_cls.crearCuenta('Donnie', 'PersonAccount');
        accToAssign.PersonEmail = 'donnie.test@test.com';
        accToAssign.OwnerId = asesorUser.Id;
        insert accToAssign;
        final List<String> lstValues = new List<String>{
            System.Label.MX_SB_VTS_Hogar,
            'Smart Center',
            'OppTelemarketing',
            'Venta',
            '999999 1 OppTest Close Opp',
            '999990',
            '72'
        };
        System.runAs(asesorUser) {
            MX_SB_VTS_CallCTIs_utility.insertBasicsVTS(lstValues,  System.Label.MX_SB_VTS_Telemarketing_LBL, asesorUser, accToAssign);
        }
    }

    @isTest
    private static void tstOppoHSDTWCupSFC() {
        final User objUser = MX_WB_TestData_cls.crearUsuario('TestUser', System.label.MX_SB_VTS_ProfileAdmin);
        insert objUser;
        System.runAs(objUser) {
            final Account objAccount = MX_WB_TestData_cls.crearCuenta('Hogarhsd', 'PersonAccount');
        	insert objAccount;
        	final Opportunity objOpportunity = MX_WB_TestData_cls.crearOportunidad('Opportunity Cupon SRV', objAccount.Id, objUser.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            objOpportunity.LeadSource = 'Tracking Web';
            objOpportunity.EnviarCTI__c = false;
            objOpportunity.MX_WB_EnvioCTICupon__c = true;
            objOpportunity.StageName = 'Contacto';
            objOpportunity.Producto__c = 'Hogar';
            objOpportunity.FolioCotizacion__c = '';
        	final List<Opportunity> lstOpportunity = new List<Opportunity>();
            lstOpportunity.add(objOpportunity);
            final Map<String, Object> oMapTest = MX_SB_VTS_SendTrackingTelCupon_Service.processQuotesASO(lstOpportunity);
            MX_SB_VTS_SendTrackingTelCupon_Service.processUpdateResult(oMapTest);
            System.assert(!oMapTest.isEmpty(),'Se ejecuto correctamente el proceso');
        }
    }
    
    @isTest
    private static void tstOppoHSDTWCupCFC() {
        final User objUserCFC = MX_WB_TestData_cls.crearUsuario('TestUser', System.label.MX_SB_VTS_ProfileAdmin);        
        insert objUserCFC;
        System.runAs(objUserCFC) {
            final Account objAccountCFC = MX_WB_TestData_cls.crearCuenta('Hogarhsd', 'PersonAccount');
        	insert objAccountCFC;
        	final Opportunity objOpportunityCFC = MX_WB_TestData_cls.crearOportunidad('Opportunity Cupon SRV', objAccountCFC.Id, objUserCFC.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            objOpportunityCFC.LeadSource = 'Tracking Web';
            objOpportunityCFC.EnviarCTI__c = false;
            objOpportunityCFC.MX_WB_EnvioCTICupon__c = true;
            objOpportunityCFC.StageName = 'Contacto';
            objOpportunityCFC.Producto__c = 'Hogar';
            objOpportunityCFC.FolioCotizacion__c = '1234567890';
        	final List<Opportunity> lstOpportunityCFC = new List<Opportunity>();
            lstOpportunityCFC.add(objOpportunityCFC);
            final Map<String, Object> oMapTestCFC = MX_SB_VTS_SendTrackingTelCupon_Service.processQuotesASO(lstOpportunityCFC);
            MX_SB_VTS_SendTrackingTelCupon_Service.processUpdateResult(oMapTestCFC);
            System.assert(!oMapTestCFC.isEmpty(),'Se ejecuto correctamente el proceso');
        }
    }

    @isTest
    private static void updateQuery() {
        final String queryStr = MX_SB_VTS_SendTrackingTelCupon_Service.updateQuery(QUERYOPPS, 1);
        System.assert(queryStr.contains('CreatedDate'), 'Url actualizada');
    }

    @isTest
    private static void findProveed() {
        final Map<Id,MX_SB_VTS_FamliaProveedores__c> mapProveedors = MX_SB_VTS_SendTrackingTelCupon_Service.findProveed();
        System.assertEquals(mapProveedors.size(), 1, 'Mapa de proveedores');
    }

    @isTest
    private static void setFamilyIds() {
        final Set<String> productsName = new Set<String>{System.Label.MX_SB_VTS_Hogar};
        final Set<String> processSac = new Set<String>{'VTS'};
        Test.startTest();
            final Set<Id> setFamilyIds = MX_SB_VTS_SendTrackingTelCupon_Service.setFamilyIds(productsName, processSac);
            System.assertEquals(setFamilyIds.size(), 1, 'Set de Familias');
        Test.stopTest();
    }

    @isTest
    private static void validOppsSend() {
        final List<Opportunity> scopeOpps = MX_SB_VTS_CallCTIs_utility.lstOppsTest();
        final Map<String, EmailNoValidosCotiza__c> mapEmailNoValCtz =  EmailNoValidosCotiza__c.getAll();
        Test.startTest();
            final List<Opportunity> lstOpps = MX_SB_VTS_SendTrackingTelCupon_Service.validOppsSend(scopeOpps, mapEmailNoValCtz);
            System.assertEquals(lstOpps.size(), scopeOpps.size(), 'Oportunidades Procesadas');
        Test.stopTest();
    }

    @isTest
    private static void processOppsSend() {
        MX_SB_VTS_CallCTIs_utility.insertIasoVTSH();
        final Map<Id,MX_SB_VTS_FamliaProveedores__c> mapProveedors = MX_SB_VTS_SendTrackingTelCupon_Service.findProveed();
        final List<Opportunity> validOpps = MX_SB_VTS_CallCTIs_utility.lstOppsTest();
        Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_Integration_MockGenerator());
        iaso.GBL_Mock.setMock(new MX_SB_VTS_Integration_MockGenerator());
        Test.startTest();
            MX_SB_VTS_SendTrackingTelCupon_Service.processOppsSend(mapProveedors, validOpps);
            final List<Opportunity> finalOpps = [Select Id, Name, EnviarCTI__c from Opportunity where Name not In ('')];
            System.assert(finalOpps[0].EnviarCTI__c,'Oportunidad Enviada');
        Test.stopTest();
    }
}