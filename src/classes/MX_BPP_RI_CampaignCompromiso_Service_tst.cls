/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase test de MX_BPP_RI_CampaignCompromiso_Service

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     17/06/2020      Roberto Isaac Soto Granados           Creación Clase
*****************************************************************************************/
@isTest
private class MX_BPP_RI_CampaignCompromiso_Service_tst {
    /*Wrapper de pruebas*/
    private static EU001_cls_CompHandler.OpportunityWrapper testWrapp = new EU001_cls_CompHandler.OpportunityWrapper();
    /*Lead de pruebas*/
    private static Lead testLeadCamp = new Lead();
    /*Account de pruebas*/
    private static Account testAccCamp = new Account();
    /*Miembro de Campaña de pruebas*/
    private static CampaignMember testCampMem = new CampaignMember();
    /*Campaña de pruebas*/
    private static Campaign testCamp = new Campaign();

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testAccCamp.FirstName = 'Cuenta';
        testAccCamp.LastName = 'Test';
        testAccCamp.No_de_Cliente__c = '12345678';
        testAccCamp.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('MX_BPP_PersonAcc_Client').getRecordTypeId();
        insert testAccCamp;
        testLeadCamp.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('MX_BPP_Leads').getRecordTypeId();
        testLeadCamp.MX_ParticipantLoad_id__c = '12345678';
        testLeadCamp.FirstName = 'Lead';
        testLeadCamp.LastName = 'Test';
        testLeadCamp.isConverted = false;
        testLeadCamp.LeadSource = 'Preaprobados';
        testLeadCamp.MX_LeadEndDate__c = Date.today()+5;
        insert testLeadCamp;
        testCamp.Name = 'Campaign';
        insert testCamp;
        testCampMem.LeadId = testLeadCamp.Id;
        testCampMem.CampaignId = testCamp.Id;
        testCampMem.MX_LeadEndDate__c = Date.today().addDays(13);
        insert testCampMem;
    }

    /*Ejecuta la acción para cubrir la clase*/
    static testMethod void getConvertedWrapListTest() {
        testLeadCamp = [SELECT Id FROM Lead LIMIT 1];
        testWrapp.oppId = testLeadCamp.Id;
        final List<EU001_cls_CompHandler.OpportunityWrapper> testWrapper = MX_BPP_RI_CampaignCompromiso_Service.getConvertedWrapList(new List<EU001_cls_CompHandler.OpportunityWrapper>{testWrapp});
        System.assert(!testWrapper.isEmpty(), 'Lead correcto');
    }

    /*Ejecuta para cubrir constructor*/
    @isTest
    private static void testConstructor() {
        final MX_BPP_RI_CampaignCompromiso_Service instanceC = new MX_BPP_RI_CampaignCompromiso_Service();
        System.assertNotEquals(instanceC, null, 'Test contructor');
    }
}