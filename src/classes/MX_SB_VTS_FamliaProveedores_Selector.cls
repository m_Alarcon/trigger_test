/**
 * @File Name          : MX_SB_VTS_FamliaProveedores_Selector.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernandez Cuamatzi
 * @Last Modified On   : 3/6/2020 18:57:10
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    28/5/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_SB_VTS_FamliaProveedores_Selector {
    
    /**
    * @description Recupera Familia de proveedores por set de Ids de familia de productos
    * @author Eduardo Hernandez Cuamatzi | 3/6/2020 
    * @param Set<Id> famProducts Familia de productos
    * @return List<MX_SB_VTS_FamliaProveedores__c> Lista de registros
    **/
    public static List<MX_SB_VTS_FamliaProveedores__c> findFamProvBySetPro(Set<Id> famProducts) {
        return [Select Id, MX_SB_VTS_Familia_de_productos__c,MX_SB_VTS_Identificador_Proveedor__c, 
        Name, MX_SB_VTS_ProveedorCTI__c from MX_SB_VTS_FamliaProveedores__c where MX_SB_VTS_Familia_de_productos__c =: famProducts WITH SECURITY_ENFORCED];
    }
}