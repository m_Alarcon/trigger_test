/*
* BBVA - Mexico
* @Author: Jaime Terrats
* MX_SB_VTS_OpportunityTrigger_Test
* @Version 1.0
* @LastModifiedBy: Francisco Javier
* @ChangeLog
* 1.0 Add coverage methods - Jaime Terrats
*/
@isteST
private class MX_SB_VTS_OpportunityTrigger_Test {
	/*UserNAme*/
    final static String TUSERNAME = 'test Opp Trigger';
	 /*UserNAme*/
    final static String TUSERNAME2 = 'test Opp Trigger 2';
	/* Msj Assert */
    final static String ASSERTMESG = 'Exito';
	/*
	 * Make data method
	 */
    @TestSetup
    static void makeData() {
    	final User tUser = MX_WB_TestData_cls.crearUsuario(TUSERNAME, System.Label.MX_SB_VTS_ProfileAdmin);
    	insert tUser;
        System.runAs(tUser) {
            final Account accToAssign = MX_WB_TestData_cls.crearCuenta('Doe', 'PersonAccount');
            accToAssign.PersonEmail = 'test@test.com.oppTriggerTest';
            insert accToAssign;
            
            final Opportunity oppToTest = MX_WB_TestData_cls.crearOportunidad(TUSERNAME, accToAssign.Id, tUser.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            oppToTest.Producto__c = 'Hogar';
            oppToTest.Reason__c = 'Venta';
            insert oppToTest;
			
			final Opportunity oppToTest2 = MX_WB_TestData_cls.crearOportunidad(TUSERNAME2, accToAssign.Id, tUser.Id, System.Label.MX_SB_VTA_VentaAsistida);
            oppToTest2.LeadSource=  System.label.MX_SB_VTS_SAC_LBL;
            oppToTest2.Reason__c = 'Venta';
            oppToTest2.StageName = 'Cotización';
            insert oppToTest2;
        }
    }
    @isTest
    private static void testSubstractContactTouchOpp() {
        Test.startTest();
        final Opportunity opp = [Select Id, RecordTypeId, MX_SB_VTS_Llamadas_Efectivas__c, MX_SB_VTS_Tipificacion_LV1__c, MX_SB_VTS_Tipificacion_LV2__c, MX_SB_VTS_ContadorRemarcado__c from Opportunity where Name =: TUSERNAME];
        final Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        final List<Opportunity> newList = new List<Opportunity>();
        oldMap.put(opp.Id, opp);
        opp.MX_SB_VTS_Tipificacion_LV1__c = 'No Contacto';
        opp.MX_SB_VTS_Tipificacion_LV2__c = 'No Contacto';
        newlist.add(opp);
        MX_SB_VTS_OpportunityTrigger.substractContactTouchOpp(newList, oldMap);
        System.assertEquals(0, opp.MX_SB_VTS_ContadorRemarcado__c, ASSERTMESG);
        Test.stopTest();
    }
    @isTest
    private static void testSubstractEffectiveContactOpp() {
        Test.startTest();
        final Opportunity opp = [Select Id, RecordTypeId, MX_SB_VTS_Llamadas_Efectivas__c, MX_SB_VTS_Tipificacion_LV1__c, MX_SB_VTS_Tipificacion_LV2__c, MX_SB_VTS_ContadorRemarcado__c from Opportunity where Name =: TUSERNAME];
        final Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        final List<Opportunity> newList = new List<Opportunity>();
        oldMap.put(opp.Id, opp);
        opp.MX_SB_VTS_Tipificacion_LV1__c = 'Contacto';
        opp.MX_SB_VTS_Tipificacion_LV2__c = 'Contacto Efectivo';
        newlist.add(opp);
        MX_SB_VTS_OpportunityTrigger.substractEffectiveContactOpp(newList, oldMap);
        System.assertEquals(0, opp.MX_SB_VTS_Llamadas_Efectivas__c, ASSERTMESG);
        Test.stopTest();
    }
    
     @isTest static void opportunityRunAsUpdate() {
         final UserRole role = new UserRole(DeveloperName = 'AsesorTelefonico', Name = 'AsesorTelefonico');
         insert role;
         final User tUser = MX_WB_TestData_cls.crearUsuario('testUser', System.Label.MX_SB_VTS_ProfileAdmin);
         tUser.UserRoleId = role.Id;
         insert tUser;         
         final Opportunity opp = [Select Id, RecordTypeId, MX_SB_VTS_Llamadas_Efectivas__c, MX_SB_VTS_Tipificacion_LV1__c, MX_SB_VTS_Tipificacion_LV2__c, MX_SB_VTS_ContadorRemarcado__c from Opportunity where Name =: TUSERNAME];
        
         Test.startTest();
            opp.OwnerId = tUser.Id;
            System.runAs(tUser) {
                update opp;
            }
            System.assertEquals(tUser.Id, opp.OwnerId,ASSERTMESG);
        Test.stopTest();
     }

    @isTest
    private static void inserTaskVisit() {
        final Id leadRec = [Select Id from Opportunity where Name =: TUSERNAME].Id;
        final Task taskLead = new Task();
        taskLead.Subject = 'Call';
        taskLead.WhatId = leadRec;
        insert taskLead;
        final dwp_kitv__Visit__c recordVisit = new dwp_kitv__Visit__c();
        final Id recType = Schema.SObjectType.dwp_kitv__Visit__c.getRecordTypeInfosByDeveloperName().get('MX_SB_VTS_Tipifi_Ventas_TLMKT').getRecordTypeId();        
		recordVisit.MX_SB_VTS_OrigenCandidato__c = 'Inbound';
		recordVisit.MX_SB_VTS_Opportunity__c = leadRec;
		recordVisit.dwp_kitv__visit_start_date__c = System.now();
        recordVisit.RecordTypeId = recType;
        recordVisit.dwp_kitv__visit_duration_number__c = '15';
        insert recordVisit;
        taskLead.dwp_kitv__visit_id__c = recordVisit.Id;
        update taskLead;
        final Opportunity oppRecord = new Opportunity();
        oppRecord.Id = leadRec;
        oppRecord.MX_SB_VTS_LookActivity__c = taskLead.Id;
        oppRecord.MX_SB_VTS_Tipificacion_LV1__c = 'Contacto';
        oppRecord.MX_SB_VTS_Tipificacion_LV2__c = 'Contacto Efectivo';       
        oppRecord.MX_SB_VTS_Tipificacion_LV3__c = 'No Interesado';
        oppRecord.MX_SB_VTS_Tipificacion_LV4__c = 'No acepta cotización';
        oppRecord.MX_SB_VTS_Tipificacion_LV5__c = 'No Venta (No acepta cotización)';
        oppRecord.MX_SB_VTS_Tipificacion_LV6__c = System.Label.MX_SB_VTS_OtroIdioma;
        update oppRecord;
        System.assertEquals('Contacto', oppRecord.MX_SB_VTS_Tipificacion_LV1__c, 'Lead actualizado');
    }  
}