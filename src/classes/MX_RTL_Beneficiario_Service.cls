/**
 * @File Name          : MX_RTL_Beneficiario_Service.cls
 * @Description        :
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 5/27/2020, 09:30:34 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/27/2020   Juan Carlos Benitez     Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_RTL_Beneficiario_Service {
    /** Campos de beneficiario **/
    Final static String BENEFIELDS = ' Id, MX_SB_VTS_Contracts__c, Name, MX_SB_VTS_APaterno_Beneficiario__c,MX_SB_SAC_Homoclave__c,MX_SB_SAC_RFC__c,MX_SB_SAC_FechaNacimiento__c,MX_SB_VTS_AMaterno_Beneficiario__c, MX_SB_SAC_Email__c, recordtype.Name, MX_SB_VTS_Quote__c,MX_SB_MLT_NombreContacto__c,MX_SB_VTS_Parentesco__c,MX_SB_PS_CURP__c, MX_SB_PS_Edad__c,MX_SB_SAC_Genero__c ';
    /** Campos de CONDITION1 **/
    Final static String  CONDITION1 ='QuoteId';
    /** Campos de CONDITION1 **/
    Final static String  CONDITION2 ='Id';
    /**
    * @description
    * @author Juan Carlos Benitez | 5/26/2020
    * @param Ids
    * @return List<Siniestro__History>
    **/
    public static List<MX_SB_VTS_Beneficiario__c> getBenefHData(Set<Id> contIds) {
        return MX_RTL_Beneficiario_Selector.getBenefDataByContract(contIds,BENEFIELDS);
    }
    /**
    * @description
    * @author Juan Carlos Benitez | 5/26/2020
    * @param Ids
    * @return List<Siniestro__History>
    **/
    public static List<MX_SB_VTS_Beneficiario__c> getBenefDataById(List<MX_SB_VTS_Beneficiario__c> datas) {
        return MX_RTL_Beneficiario_Selector.getBenefDataById(datas,BENEFIELDS);
    }
       /**
    * @description
    * @author Juan Carlos Benitez | 5/26/2020
    * @param List<MX_SB_VTS_Beneficiario__c> benefData
    * @return 
    **/
    
    public static void updateBenefData(List<MX_SB_VTS_Beneficiario__c> benefData) {
		MX_RTL_Beneficiario_Selector.updateBeneficiario(benefData);
    }
           /**
    * @description
    * @author Juan Carlos Benitez | 5/26/2020
    * @param MX_SB_VTS_Beneficiario__c benefObj
    * @return 
    **/
    
    public static void insertBenef(MX_SB_VTS_Beneficiario__c benefObj) {
        benefObj.RecordTypeId=Schema.SObjectType.MX_SB_VTS_Beneficiario__c.getRecordTypeInfosByDeveloperName().get('MX_SB_PS_Asegurado').getRecordTypeId();
        MX_RTL_Beneficiario_Selector.insertBenef(benefObj);
    }
    /**
    * @description
    * @author Juan Carlos Benitez | 5/26/2020
    * @param MX_SB_VTS_Beneficiario__c benefObj
    * @return 
    **/
    public static List<MX_SB_VTS_Beneficiario__c> getbenefbyValue( String value) {
		return MX_RTL_Beneficiario_Selector.getBenefByAccId(CONDITION1, BENEFIELDS,value);
    }
    /**
    * @description
    * @author Juan Carlos Benitez | 5/26/2020
    * @param String value
    * @return 
    **/
    public static List<MX_SB_VTS_Beneficiario__c> getbenefbyIdValue( String value) {
		return MX_RTL_Beneficiario_Selector.getBenefByAccId(CONDITION2, BENEFIELDS,value);
    }
    /**
    * @description
    * @author Juan Carlos Benitez | 5/26/2020
    * @param string quotId
    * @return 
    **/
    public static Integer getCountBenef(String quotId) {
        return MX_RTL_Beneficiario_Selector.getCountBenef(quotId);
    }
	/**
    * @description
    * @author Juan Carlos Benitez | 5/26/2020
    * @param  MX_SB_VTS_Beneficiario__c benef
    * @return void
    **/
    public static void dltBenef(MX_SB_VTS_Beneficiario__c benef) {
        MX_RTL_Beneficiario_Selector.dltBenef(benef);
    }
}