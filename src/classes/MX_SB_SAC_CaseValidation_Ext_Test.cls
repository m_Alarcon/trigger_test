/**
 * @description       :
 * @author            : Jaime Terrats
 * @group             :
 * @last modified on  : 07-13-2020
 * @last modified by  : Jaime Terrats
 * Modifications Log
 * Ver   Date         Author          Modification
 * 1.0   07-11-2020   Jaime Terrats   Initial Version
**/
@isTest
private class MX_SB_SAC_CaseValidation_Ext_Test {
    /** name variable for admin user and case subject*/
    final static String SUBJECT = 'testExtValidate';
    /** variable for case description */
    final static String ASESOR_SAC = 'sac asesor';
    /** variable for case description */
    final static String LOREM_IPSUM = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit';
    /** error message variable */
    final static String ASSERT_MSG = 'Algo falló.';
    /** tipification value */
    final static String CANCELACION = 'Cancelación';
    /** tipification value */
    final static String RETENCION = 'Retención';
    /** product type value */
    final static String AUTO = 'Auto';

    /**
    * @description
    * @author Jaime Terrats | 07-11-2020
    **/
    @TestSetup
    static void makeData() {
        final User usr = MX_WB_TestData_cls.crearUsuario(SUBJECT, System.Label.MX_SB_VTS_ProfileAdmin);
        insert usr;
        System.runAs(usr) {
            final User sacUsr = MX_WB_TestData_cls.crearUsuario(ASESOR_SAC, System.Label.MX_SB_SAC_AsesorSACProfile);
            final Id rtId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_SAC_Generico).getRecordTypeId();
            final Case tCase = new Case(
                Subject = SUBJECT,
                RecordTypeId = rtId,
                MX_SB_SAC_Nombre__c = SUBJECT,
                MX_SB_SAC_ApellidoPaternoCliente__c = SUBJECT,
                MX_SB_SAC_ApellidoMaternoCliente__c = SUBJECT,
                MX_SB_SAC_EsCliente__c = true,
                Origin = 'Teléfono',
                MX_SB_SAC_SubOrigenCaso__c = 'Inbound',
                MX_SB_SAC_TipoContacto__c = 'Asegurado'
            );

            insert tCase;
            insert sacUsr;
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 07-11-2020
    **/
    @isTest
    static void testValidatExt() {
        final Case tCase = [Select Id, Description, Reason, MX_SB_SAC_FinalizaFlujo__c, MX_SB_SAC_Aplicar_Mala_Venta__c, OwnerId from Case where Subject =: SUBJECT];
        final User tUser = [Select Id from user where LastName =: ASESOR_SAC];
        tCase.MX_SB_SAC_FinalizaFlujo__c = true;
        tCase.Reason = CANCELACION;
        tCase.MX_SB_SAC_Detalle__c = 'Otra Aseguradora';
        tCase.MX_SB_SAC_Aplicar_Mala_Venta__c = 'No';
        tCase.OwnerId = tUser.Id;
        update tCase;
        Test.startTest();
        System.runAs(tUser) {
            try {
                tCase.Description = LOREM_IPSUM;
                update tCase;
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(System.Label.MX_SB_SAC_PreventComments), ASSERT_MSG);
            }
        }
        Test.stopTest();
    }

    /**
    * @description
    * @author Jaime Terrats | 07-11-2020
    **/
    @isTest
    static void testValidateExt1() {
        final Case tCase = [Select Id, Description, Reason, MX_SB_SAC_FinalizaFlujo__c, MX_SB_SAC_Aplicar_Mala_Venta__c, MX_SB_SAC_Finalizar_Retencion__c,
                            OwnerId from Case where Subject =: SUBJECT];
        final User tUser = [Select Id from user where LastName =: ASESOR_SAC];
        tCase.MX_SB_SAC_FinalizaFlujo__c = true;
        tCase.Reason = CANCELACION;
        tCase.MX_SB_SAC_Detalle__c = 'Economía Personal';
        tCase.MX_SB_SAC_Aplicar_Mala_Venta__c = 'No';
        tCase.MX_SB_SAC_Finalizar_Retencion__c = true;
        tCase.OwnerId = tUser.Id;
        update tCase;
        Test.startTest();
        System.runAs(tUser) {
            try {
                tCase.Description = LOREM_IPSUM;
                update tCase;
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(System.Label.MX_SB_SAC_PreventComments), ASSERT_MSG);
            }
        }
        Test.stopTest();
    }

    /**
    * @description
    * @author Jaime Terrats | 07-11-2020
    **/
    @isTest
    static void testValidateExt2() {
        Test.startTest();
        final Case tCase = [Select Id, Reason, Description, OwnerId, MX_SB_SAC_FinalizaFlujo__c, MX_SB_SAC_Ramo__c,
                            MX_SB_SAC_TerminaMalasVentas__c, MX_SB_SAC_Detalle__c, MX_SB_SAC_OcultarArgumentos__c,
                            MX_SB_SAC_InformacionAdicional__c from Case where Subject =: SUBJECT];
        final User tUser = [Select Id from User where LastName =: ASESOR_SAC];
        tCase.Reason = RETENCION;
        tCase.MX_SB_SAC_Ramo__c = AUTO;
        tCase.MX_SB_SAC_FinalizaFlujo__c = true;
        tCase.MX_SB_SAC_TerminaMalasVentas__c = true;
        tCase.MX_SB_SAC_OcultarArgumentos__c = true;
        tCase.OwnerId = tUser.Id;
        update tCase;
        System.runAs(tUser) {
            try {
                tCase.MX_SB_SAC_InformacionAdicional__c = 'Mala venta con siniestro';
                tCase.Description = LOREM_IPSUM;
                update tCase;
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(System.Label.MX_SB_SAC_OnlyNA), ASSERT_MSG);
            }
        }
        Test.stopTest();
    }

    /**
    * @description
    * @author Jaime Terrats | 07-11-2020
    **/
    @isTest
    static void testValidateExt3() {
        Test.startTest();
        final Case tCase = [Select Id, Reason, Description, OwnerId, MX_SB_SAC_FinalizaFlujo__c, MX_SB_SAC_Ramo__c,
                            MX_SB_SAC_TerminaMalasVentas__c, MX_SB_SAC_Detalle__c, MX_SB_SAC_OcultarArgumentos__c,
                            MX_SB_SAC_InformacionAdicional__c from Case where Subject =: SUBJECT];
        final User tUser = [Select Id from User where LastName =: ASESOR_SAC];
        tCase.Reason = CANCELACION;
        tCase.MX_SB_SAC_Ramo__c = AUTO;
        tCase.MX_SB_SAC_FinalizaFlujo__c = true;
        tCase.MX_SB_SAC_TerminaMalasVentas__c = true;
        tCase.MX_SB_SAC_OcultarArgumentos__c = true;
        tCase.OwnerId = tUser.Id;
        update tCase;
        System.runAs(tUser) {
            try {
                tCase.MX_SB_SAC_InformacionAdicional__c = 'N/A';
                tCase.Description = LOREM_IPSUM;
                update tCase;
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(System.Label.MX_SB_SAC_NotNA), ASSERT_MSG);
            }
        }
        Test.stopTest();
    }

    /**
    * @description
    * @author Jaime Terrats | 07-11-2020
    **/
    @isTest
    static void testValidateExt4() {
        Test.startTest();
        final Case tCase = [Select Id, Reason, Description, OwnerId, MX_SB_SAC_FinalizaFlujo__c, MX_SB_SAC_Ramo__c,
                            MX_SB_SAC_TerminaMalasVentas__c, MX_SB_SAC_Detalle__c, MX_SB_SAC_OcultarArgumentos__c,
                            MX_SB_SAC_Informacion_Adicional_Vida__c from Case where Subject =: SUBJECT];
        final User tUser = [Select Id from User where LastName =: ASESOR_SAC];
        tCase.Reason = CANCELACION;
        tCase.MX_SB_SAC_Ramo__c = System.Label.MX_SB_VTS_Vida;
        tCase.MX_SB_SAC_FinalizaFlujo__c = true;
        tCase.MX_SB_SAC_TerminaMalasVentas__c = true;
        tCase.MX_SB_SAC_OcultarArgumentos__c = true;
        tCase.OwnerId = tUser.Id;
        update tCase;
        System.runAs(tUser) {
            try {
                tCase.MX_SB_SAC_Informacion_Adicional_Vida__c = 'N/A';
                tCase.Description = LOREM_IPSUM;
                update tCase;
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(System.Label.MX_SB_SAC_NotNA), ASSERT_MSG);
            }
        }
        Test.stopTest();
    }

    /**
    * @description
    * @author Jaime Terrats | 07-11-2020
    **/
    @isTest
    private static void testValidateExt5() {
        final Case tCase = [Select Id, Description, Reason, MX_SB_SAC_FinalizaFlujo__c, OwnerId from Case where Subject =: SUBJECT];
        final User tUser = [Select Id from user where LastName =: ASESOR_SAC];
        tCase.MX_SB_SAC_FinalizaFlujo__c = true;
        tCase.Reason = 'Cotización';
        tCase.OwnerId = tUser.Id;
        update tCase;
        Test.startTest();
        System.runAs(tUser) {
            try {
                tCase.Description = LOREM_IPSUM;
                update tCase;
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(System.Label.MX_SB_SAC_PreventCmnts), ASSERT_MSG);
            }
        }
        Test.stopTest();
    }

    /**
    * @description
    * @author Jaime Terrats | 07-11-2020
    **/
    @isTest
    static void testValidateExt6() {
        Test.startTest();
        final Case tCase = [Select Id, Reason, Description, OwnerId, MX_SB_SAC_FinalizaFlujo__c, MX_SB_SAC_Ramo__c,
                            MX_SB_SAC_TerminaMalasVentas__c, MX_SB_SAC_Detalle__c, MX_SB_SAC_OcultarArgumentos__c,
                            MX_SB_SAC_Informacion_Adicional_Vida__c from Case where Subject =: SUBJECT];
        final User tUser = [Select Id from User where LastName =: ASESOR_SAC];
        tCase.Reason = RETENCION;
        tCase.MX_SB_SAC_Ramo__c = System.Label.MX_SB_VTS_Vida;
        tCase.MX_SB_SAC_FinalizaFlujo__c = true;
        tCase.MX_SB_SAC_TerminaMalasVentas__c = true;
        tCase.MX_SB_SAC_OcultarArgumentos__c = true;
        tCase.OwnerId = tUser.Id;
        update tCase;
        System.runAs(tUser) {
            try {
                tCase.MX_SB_SAC_Informacion_Adicional_Vida__c = 'Mala venta con siniestro';
                tCase.Description = LOREM_IPSUM;
                update tCase;
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(System.Label.MX_SB_SAC_OnlyNA), ASSERT_MSG);
            }
        }
        Test.stopTest();
    }

}