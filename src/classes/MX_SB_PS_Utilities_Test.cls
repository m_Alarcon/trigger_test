/**
* Indra
* @author           Julio Medellín Oliva
* Project:          Presuscritos
* Description:      Class to test Queries On Opportunity.
*
* Changes (Version)
* ------------------------------------------------------------------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    --------------------------------------
* @version  1.0     2020-05-25      Julio Medellín Oliva.         Creación de la Clase
* @version  1.1     2020-07-09      Juan Carlos Benitez Herrera.  Se agrega methodo testUpsrtOpp
*/

@isTest
@supresswarnings('sf:AvoidFinalLocalVariable')
public class MX_SB_PS_Utilities_Test {
    
    
/**
* @Method Data Setup
* @Description Method para preparar datos de prueba 
* @return void
**/
    @testSetup
    public static void  data() { 
        MX_SB_PS_OpportunityTrigger_test.makeData();
    }
    /**
* @Method getAccountOpportunity_test
* @param String oppid
* @param Sring fields
* @Description method que retorna un objeto User
* @return Objeto User
**/
    public static testMethod void  prueba1() { 
        final Opportunity opp=[SELECT ID,StageName FROM Opportunity LIMIT 1];
        System.assertNotEquals(opp,MX_SB_PS_Utilities.fetchOpp(opp.Id),'Success');
    }
/**
* @Method testUpsrtOpp
* @Description Methodo que prueba upsrtOpp
**/
    @isTest
    static void testUpsrtOpp () {
		final Opportunity oppData=[SELECT ID,StageName FROM Opportunity LIMIT 1];
        oppData.Plan__c='Individual';
        test.startTest();
        	MX_SB_PS_Utilities.upsrtOpp(oppData);
			MX_SB_PS_Utilities.fetchSobject(oppData.Id,'Name','Opportunity');
            system.assert(true,'Se ha actualizado la opportunidad correctamente');
        test.stopTest();
    }
}