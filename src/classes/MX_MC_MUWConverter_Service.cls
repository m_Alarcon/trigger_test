/**
 * @File Name           : MC_MessageUpdateWrapperConverter_MX.cls
 * @Description         :
 * @Author              : eduardo.barrera3@bbva.com
 * @Group               :
 * @Last Modified By    : eduardo.barrera3@bbva.com
 * @Last Modified On    : 11/17/2020, 15:17:19 PM
 * @Modification Log    :
 * =============================================================================
 * Ver          Date                     Author                     Modification
 * =============================================================================
 * 1.0      05/19/2020, 12:50:19 PM  eduardo.barrera3@bbva.com      Initial Version
 **/
global without sharing class MX_MC_MUWConverter_Service extends mycn.MC_VirtualServiceAdapter { //NOSONAR

    /**
    * @Description PARAM_THREAD property
    **/
    private static final String PARAM_THREAD = 'messageThreadId';

    /**
    * @Description PARAM_THREAD_MI property
    **/
    private static final String PARAM_THREAD_MI = 'messageId';

    /**
    * @Description Returns a generic Object based on method call from Class in MC component
    * @author eduardo.barrera3@bbva.com | 05/18/2020
    * @param parameters
    * @return Map<String, Object>
    **/
    global override Map<String, Object> convertMap(Map<String, Object> parameters) {

        final Map<String, Object> inputMX = new Map<String, Object>();

        if (parameters.containsKey(PARAM_THREAD_MI) && parameters.containsKey('content')) {
            inputMX.put(PARAM_THREAD_MI, parameters.get(PARAM_THREAD_MI));
            inputMX.put('message',parameters.get('content'));
            inputMX.put('isDraft',String.valueOf(parameters.get('isDraft')));
            inputMX.put(PARAM_THREAD,parameters.get(PARAM_THREAD));
        } else if (parameters.containsKey(PARAM_THREAD_MI) && parameters.containsKey(PARAM_THREAD)) {
            inputMX.putAll(parameters);
        } else {
            final mycn.MC_MessageThreadWrapper.MessageThread bodyParams = (mycn.MC_MessageThreadWrapper.MessageThread)JSON.deserialize(String.valueOf(parameters.get('messageThreadBody')),mycn.MC_MessageThreadWrapper.MessageThread.class);
            inputMX.put('isSolved',String.valueOf(bodyParams.isSolved));
            inputMX.put(PARAM_THREAD,String.valueOf(parameters.get(PARAM_THREAD)));
        }
        return inputMX;
    }
}