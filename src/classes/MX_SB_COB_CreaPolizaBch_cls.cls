/*
----------------------------------------------------------
* Nombre: MX_SB_COB_CreaPolizaBch_cls
* Autor Angel Nava
* Proyecto: Cobranza - BBVA Bancomer
* Descripción : Clase que para la creacion de polizas
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   			Desripción<p />
* --------------------------------------------------------------------------------
* 1.0           15/04/2019     Angel Nava                		   	Creación
* --------------------------------------------------------------------------------
*/
global without sharing class MX_SB_COB_CreaPolizaBch_cls implements Database.Batchable<Contract>, Database.AllowsCallouts, Database.Stateful {

   /**list idsOpp */
    global List<Id> idsOpp { get;set; }
    /** Map mapOpp*/
    global Map<String,Contract> mapOpp { get;set; }
    /** list Contract oppIn*/
    global List<Contract> oppIn { get;set; }
    /** Map String mapNoPolIdNvas*/
    global Map<String,String> mapNoPolIdNvas { get;set; }
    /** mapa de pagosespejo a actualizar poliza id (oportunidad)*/
    Map<String, PagosEspejo__c> nPolPaEspUp { get;set; }
    /** mapa string PolFPagEsp*/
    Map<String,Map<String,PagosEspejo__c>> PolFPagEsp { get;Set; }

/*
* @description: contructor asignacion de datos
* @param Map<String,Contract> polizas,Map<String,Map<String,PagosEspejo__c>> nuevosEspejos
* @return
*/
    global MX_SB_COB_CreaPolizaBch_cls(Map<String,Contract> polizas,Map<String,Map<String,PagosEspejo__c>> nuevosEspejos) {

        this.oppIn = polizas.values();
        this.PolFPagEsp = nuevosEspejos;

        this.mapNoPolIdNvas = new Map<String,String>();
        this.nPolPaEspUp = new Map<String, PagosEspejo__c>();
        this.idsOpp = new List<Id>();
    }

/*
* @description: contructor asignacion de contrato
* @param
* @return List<Contract>
*/
    global List<Contract> start(Database.BatchableContext batcCon) {
        return this.oppIn;
    }

/*
* @description: consulta de cuentas
* @param List<Contract> scope
* @return void
*/
    global void execute(Database.BatchableContext batcCon, List<Contract> scope) {

        final List<String> correosContratos = new List<String>();
        final Map<String,String> MapCorreoCuenta = new Map<String,String>();
        final List<Account> cuentasNuevas = new List<Account>();
        Boolean nuevaCuentaFlag =false;
    	final String recordtypeCuenta = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta personal').getRecordTypeId(); 

        for(Contract con:scope) {
            correosContratos.add(con.MX_WB_emailAsegurado__c);
        }
        for(Account acc:[select id,PersonEmail from Account where PersonEmail in :correosContratos  limit 100]) {

           MapCorreoCuenta.put(acc.PersonEmail, acc.id);
        }
        for(Contract sc:scope) {
            nuevaCuentaFlag=false;
            if(String.isEmpty(sc.MX_WB_emailAsegurado__c )==false) {
                if(MapCorreoCuenta.containsKey(sc.MX_WB_emailAsegurado__c)) {
                   sc.AccountId =MapCorreoCuenta.get(sc.MX_WB_emailAsegurado__c);
                } else {
                    nuevaCuentaFlag = true;
                }
            } else {
                nuevaCuentaFlag = true;
            }
            if(nuevaCuentaFlag) {
                final Account cuentaNueva = new Account(
                PersonEmail = sc.MX_WB_emailAsegurado__c,
                    recordtypeId = recordtypeCuenta,
                    phone = String.valueOf(sc.MX_WB_telefonoAsegurado__c),
                    AccountSource = 'Other',
                    lastname=sc.MX_WB_apellidoPaternoAsegurado__c
                );

                cuentasNuevas.add(cuentaNueva);
            }
            nuevaCuentaFlag=false;
        }
		CreaCuentas(cuentasNuevas,MapCorreoCuenta,scope);
    }

/*
* @description: crea datos al final de batch
* @param Database.BatchableContext bC
* @return void
*/
    global void finish(Database.BatchableContext batcCon) {

        if(this.idsOpp.size()>0) {
            for (Contract objOpp : [Select id, MX_WB_noPoliza__c From Contract
					Where id IN :this.idsOpp]) {
					mapNoPolIdNvas.put(objOpp.MX_WB_noPoliza__c, objOpp.id);
			}
            for (String sNoPoli : PolFPagEsp.KeySet()) {
				for (String sNoFactPol : PolFPagEsp.get(sNoPoli).KeySet()) {
					if (mapNoPolIdNvas.containsKey(sNoPoli)) {
						final PagosEspejo__c objPagoEspUpd = PolFPagEsp.get(sNoPoli).get(sNoFactPol);
						objPagoEspUpd.MX_SB_COB_Contrato__c = mapNoPolIdNvas.get(sNoPoli);
						nPolPaEspUp.put(sNoFactPol, objPagoEspUpd);
                    }
				}
			}

            final List<PagosEspejo__c> pagosLst = new List<PagosEspejo__c>();
            if(nPolPaEspUp.size()>0) {
                if(nPolPaEspUp.size()==1) {
                    pagosLst.add(nPolPaEspUp.values());
                } else {
                    for(PagosEspejo__c es:nPolPaEspUp.values()) {
                    	pagosLst.add(es);
                	}
                }
            }
        }

    }
    /*
* @description: crea cuentas
* @param List<Account> cuentasNuevas ,Map<String,String> MapCorreoCuenta,LIst<Contract> scope
* @return void
*/
    private void creaCuentas(List<Account> cuentasNuevas ,Map<String,String> mapCorreoCuenta,LIst<Contract> scope) {
		final List<String> idsCuentasNuevas = new List<String>();

        if(cuentasNuevas.size()>0) {
            try {
                final Database.SaveResult[] ctaNvaRes = Database.insert(cuentasNuevas,true);
                for (Database.SaveResult objDtUrsPol : ctaNvaRes) {
                        if (objDtUrsPol.isSuccess()) {
                          idsCuentasNuevas.add(objDtUrsPol.getId());
                        }
                }
            } catch(Exception ex) {
                return;
            }
        }
        if(idsCuentasNuevas.size()>0) {
           creaContratos(MapCorreoCuenta,idsCuentasNuevas,scope);
        }
    }
/*
* @description: crea contratos al final del batch
* @param Map<String,String> MapCorreoCuenta,List<String> idsCuentasNuevas,List<Contract> scope
* @return void
*/ 
    private void creaContratos(Map<String,String> mapCorreoCuenta,List<String> idsCuentasNuevas,List<Contract> scope) {
         for(Account acc:[select id,PersonEmail from Account where id in :idsCuentasNuevas limit 100]) {
               MapCorreoCuenta.put(acc.PersonEmail, acc.id);
            }
        for(Contract cs2:scope) {
            if(cs2.MX_WB_emailAsegurado__c!=null && MapCorreoCuenta.containsKey(cs2.MX_WB_emailAsegurado__c)) {
                    cs2.AccountId = MapCorreoCuenta.get(cs2.MX_WB_emailAsegurado__c);
            }
        }
         try {
            final Database.SaveResult[] lDtUrsPolNvas = Database.insert(scope,true);
            for (Database.SaveResult objDtUrsPol : lDtUrsPolNvas) {
                    if (objDtUrsPol.isSuccess()) {
                        idsOpp.add(objDtUrsPol.getId());
                    }
            }

         } catch(Exception ex) {
             return;
         }
    }
}