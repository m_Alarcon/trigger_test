/**
 * @File Name          : MX_BPP_ConvertLeads_Helper_Test.cls
 * @Description        : Clase para Test de MX_BPP_ConvertLeads_Helper
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 04/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      04/06/2020           Jair Ignacio Gonzalez G.   Initial Version
**/
@isTest
private class MX_BPP_ConvertLeads_Helper_Test {
    /**
     **Descripción: Clase testSetUpData
    **/
    @TestSetup
    private static void testSetUpData() {
        final Account oAcc = new Account(Name='Test Helper Account',No_de_cliente__c = '8316250');
        insert oAcc;
        final Lead oLead = new Lead(LastName='Lead Helper Test', RecordTypeId=Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('MX_BPP_Leads').getRecordTypeId(), 
                MX_ParticipantLoad_id__c=oAcc.No_de_cliente__c, MX_LeadEndDate__c=Date.today().addDays(15), MX_WB_RCuenta__c= oAcc.Id, MX_LeadAmount__c=100, LeadSource='Preaprobados');
        insert oLead;
        final Campaign oCamp = new Campaign(Name = 'Campaign Helper Test');
        insert oCamp;
        final CampaignMember oCampMem = new CampaignMember(CampaignId = oCamp.Id, Status='Sent', LeadId = oLead.Id, MX_LeadEndDate__c=Date.today().addDays(15));
        insert oCampMem;
    }
    /**
     * *Descripción: Clase de prueba para getLeadToConverd; createOpportunity, convertLeads y updateSubProducto
    **/
    @isTest static void testHelperMethods() {
        final List<Id> lsIds = new List<Id>{[SELECT Id FROM Lead LIMIT 1].Id};
        final Id rTypeLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('MX_BPP_Leads').getRecordTypeId();
        Test.startTest();
        final List<CampaignMember> lsCamMembers = MX_BPP_ConvertLeads_Helper.getLeadToConverd(lsIds);
        final Map<Id, Opportunity> mapLOpp = MX_BPP_ConvertLeads_Helper.createOpportunity(lsCamMembers, rTypeLead);
        final Map<String,String> result = MX_BPP_ConvertLeads_Helper.convertLeads(lsCamMembers, rTypeLead, mapLOpp);
        MX_BPP_ConvertLeads_Helper.updateSubProducto(lsCamMembers, result);
        Test.stopTest();
        System.assert(result.containsKey(lsIds[0]), 'Lead not converted');
    }
}