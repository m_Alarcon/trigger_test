@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_RTL_Siniestro_History_Selector_Test
* Autor: Juan Carlos Benitez Herrera
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_RTL_Siniestro_History_Selector

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         26/05/2020   Juan Carlos Benitez Herrera              Creación
* -----------------------------------------------------------------------------------------------
*/
public with sharing class MX_RTL_Siniestro_History_Selector_Test {
    /*
	* var String Valor Subramo Ahorro
	*/
    public static final String SUBRAMO ='Ahorro';
    /** Nombre de usuario Test */
    final static String NAME = 'Sanji';
    /** Condicion ParentId */
    final static String CONDITION ='ParentId';
    /** Condicion2 Id */
    final static String CONDITION2='Id';
    /** Lista de Siniestro__History */
    final static List<Siniestro__History> HISTORYSRCH = new List<Siniestro__History>();
    /** Lista de Sobject */
    final static List<Sobject> LISTHISTORY = new List<Sobject>();
	/** List of records to be returned */
    final static List<SObject> RETURN_SINHISTORY = new List<SObject>();
	    @TestSetup
    static void createData() {
        Final String vidaprofile = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User userVida = MX_WB_TestData_cls.crearUsuario(NAME, vidaprofile);  
        insert userVida;
        System.runAs(userVida) {
            final String rtpeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Proveedores_MA).getRecordTypeId();
            final Account testaccountVida = new Account(RecordTypeId = rtpeId, Name=NAME, Tipo_Persona__c='Física');
            insert testaccountVida;
            
            final Contract vidacontract = new Contract(AccountId = testaccountVida.Id, MX_SB_SAC_NumeroPoliza__c='policyTest');
            insert vidacontract;
            
            Final List<Siniestro__c> sinInsertV = new List<Siniestro__c>();
            for(Integer n = 0; n < 10; n++) {
                Final Siniestro__c nSin = new Siniestro__c();
                nSin.MX_SB_SAC_Contrato__c = vidacontract.Id;
                nSin.MX_SB_MLT_NombreConductor__c = NAME + '_' + n;
                nSin.MX_SB_MLT_APaternoConductor__c = 'LastName for '+NAME;
                nSin.MX_SB_MLT_AMaternoConductor__c = 'LastName2 for '+NAME;
                nSin.MX_SB_MLT_Telefono__c = '5540342107';
                nSin.MX_SB_MLT_Fecha_Hora_Siniestro__c =date.valueof('2020-03-27T22:04:00.000+0000');
                nSin.MX_SB_MLT_AtencionVida__c = 'Agendar Cita';
                nSin.MX_SB_MLT_PreguntaVida__c = false;
                nSin.MX_SB_MLT_SubRamo__c = SUBRAMO;
                nSin.TipoSiniestro__c = 'Siniestros';
                nSin.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_creacion;
                nSin.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoVida).getRecordTypeId();
                sinInsertV.add(nSin);
            }
            Database.insert(sinInsertV);
        }
    }
    
    @isTest
    static void testLoadedHistory() {
        final Id sinId = [SELECT Id from Siniestro__c WHERE MX_SB_MLT_NombreConductor__c  =: NAME +'_1'].Id;
		final Set<Id> ids = new Set<Id>{sinId};
		Final Map<String, String> mapa= new Map<String,String>();
		mapa.put('fields', ' CreatedDate,Id,NewValue,OldValue,ParentId ');
        	mapa.put('orderField', ' order by CreatedDate asc ');
        	mapa.put('limite', ' limit 1 ');
		Test.startTest();
			MX_RTL_Siniestro_History_Selector.getSinHDataByParentId(ids, CONDITION ,LISTHISTORY, mapa);
        	MX_RTL_Siniestro_History_Selector.getSinHDataByParentId(ids, CONDITION2 ,LISTHISTORY, mapa);
        Test.stopTest();
        system.assert(true, 'Exitoso');
    }
}