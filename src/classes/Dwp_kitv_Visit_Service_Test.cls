/*
----------------------------------------------------------------------------------
* Nombre: Dwp_kitv_Visit_Service_Test
* Autor: Marco Antonio Cruz Barboza
* Proyecto: Seguros-Ventas - BBVA Bancomer
* Descripción : Prueba los Methods de la clase Dwp_kitv_Visit_Service

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         07/09/2020   Marco Antonio Cruz Barboza               Creación
* -----------------------------------------------------------------------------------------------
*/
@isTest
private class Dwp_kitv_Visit_Service_Test {
    /*Usuario de pruebas*/
    private static User userTesting = new User();
    /*Visita de pruebas*/
    private static dwp_kitv__Visit__c visitTest = new dwp_kitv__Visit__c();
  	/** Nombre de usuario Test */
    final static String NOMBRE = 'Usuario de Prueba';
   	/** Name Account */
    final static String NAMEACC = 'ACCOUNTNAME TEST';
    /** Name Opp */
    final static String OPPNAME = 'OPPORTUNITYNAME TEST';
    
    /**
     * @description Test SetUp
     * Author: Marco Cruz
	 */
    @TestSetup
    static void createData() {
        userTesting = MX_WB_TestData_cls.crearUsuario(NOMBRE, System.Label.MX_SB_VTS_ProfileAdmin	);  
        insert userTesting;
        System.runAs(userTesting) {
           	Final Account testingAcc = MX_WB_TestData_cls.crearCuenta(NAMEACC, System.Label.MX_SB_VTS_PersonRecord);
            testingAcc.PersonEmail = 'pruebaVts@mxvts.com';
            insert testingAcc;
            
            Final Opportunity testingOpp = MX_WB_TestData_cls.crearOportunidad(OPPNAME, testingAcc.Id, userTesting.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            testingOpp.LeadSource = 'Call me back';
            testingOpp.Producto__c = 'Hogar';
            testingOpp.Reason__c = 'Venta';
            testingOpp.StageName = 'Cotizacion';
            insert testingOpp;
            
            final String leadRT = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
            final Lead leadTest = new Lead(RecordTypeId = leadRT, LastName = NOMBRE);
            insert leadTest;
            
        }
    }
    
     /**
     * @description Method to test insertVisit method from Dwp_kitv_Visit_Service
     * Author: Marco Cruz
	 */
    static testMethod void insertVisitTest() {
        userTesting = [SELECT Id, Title FROM User WHERE Name =: NOMBRE ];
        Final Lead lead4Test = [SELECT Id, Name FROM Lead WHERE LastName =: NOMBRE];
        Final Id recordTVisit = Schema.SObjectType.dwp_kitv__Visit__c.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Tipifi_Ventas_TLMKT).getRecordTypeId();
        System.runAs(userTesting) {
           Test.startTest();
            	Final List<dwp_kitv__Visit__c> listVisTest = new List<dwp_kitv__Visit__c>();
            		Final datetime getDateVal = datetime.now();
            		visitTest.RecordTypeId = recordTVisit;
                    visitTest.dwp_kitv__visit_duration_number__c = '15';
                    visitTest.dwp_kitv__visit_start_date__c = getDateVal;
                    visitTest.Name = 'Llamada Realizada ' + getDateVal;
            		visitTest.dwp_kitv__lead_id__c = lead4Test.Id;
            		listVisTest.add(visitTest);
                    Dwp_kitv_Visit_Service.insertVisit(listVisTest);
            Test.stopTest();
        }
        System.assert(true,'Visita Insertada');
    }


}