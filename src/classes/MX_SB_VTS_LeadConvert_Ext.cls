/**
* BBVA - Mexico
* @Author: Jaime Terrats
* @Description: Controller extension for MX_WB_ConvertirLeadController
* @version 1.0
**/
public virtual without sharing class MX_SB_VTS_LeadConvert_Ext {//NOSONAR

    /**
    * validateNewFlow valida tipificaciones para saber si el lead se puede convertir
    * @param leadData datos del lead
    * @user Id de Usuario
    * @return Boolean para saber si hubo error o no
    *
    **/
    public static Boolean validateNewFlow(Lead leadData, Id user) {
        final Boolean isValid = true;
        if(System.Label.MX_SB_VTS_Interesado.equals(leadData.MX_SB_VTS_Tipificacion_LV3__c) == false) {
            throw new AuraHandledException(System.Label.MX_SB_VTS_EstatusEfectivo);
        }
        if(System.Label.MX_SB_VTS_ResultadoLlamada.equals(leadData.Resultadollamada__c) == false) {
            throw new AuraHandledException(System.Label.MX_SB_VTS_ErrorTipificaciones);
        }
        if(!System.Label.MX_SB_VTS_ContactoEfectivo.equals(leadData.MX_SB_VTS_Tipificacion_LV2__c)) {
            throw new  AuraHandledException(System.Label.MX_SB_VTS_ErrorTipificaciones);
        }
        validateSameFields(leadData, user);
        return isValid;
    }

    /**
    * validateSameFields valida lead
    * @param leadData datos del lead
    * @user Id de Usuario
    * @return Boolean para saber si hubo error o no
    *
    **/
    public static void validateSameFields(Lead leadData, Id user) {
        if (String.isBlank(leadData.Email) && leadData.RecordType.DeveloperName.equals(System.Label.MX_SB_VTS_Telemarketing_LBL) == false) {
            throw new AuraHandledException(Label.MX_SB_VTS_EmailRequerido);
        }
        if(user.equals(leadData.OwnerId) == false) {
            throw new AuraHandledException(Label.MX_SB_VTS_OwnerLead);
        }
        if (String.isBlank(leadData.MobilePhone) && (leadData.RecordType.DeveloperName.equals(System.Label.MX_SB_VTS_RecordTypeASD) || leadData.RecordType.DeveloperName.equals(System.Label.MX_SB_VTS_Telemarketing_LBL))) {
            throw new AuraHandledException(Label.MX_SB_VTS_InvalidPhone);
        }
    }

    /**
    * @Description: creates opp for new business process
    * @params: leadToConvert
    **/
    public static Opportunity createOppExt(Lead leadToConvert, Id accId) {
        final Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
        final Opportunity newOpp = new Opportunity();
        newOpp.AccountId = accId;
        newOpp.Name = leadToConvert.Name;
        newOpp.RecordTypeId = oppRtId;
        final String validProductName = MX_SB_VTS_CierreOpp.validacionproducto(leadToConvert.Producto_Interes__c);
        newOpp.Producto__c = validProductName;
        newOpp.StageName = System.Label.MX_SB_VTS_COTIZACION_LBL;
        newOpp.TelefonoCliente__c = leadToConvert.MobilePhone;
        newOpp.MX_WB_txt_Clave_Texto__c = leadToConvert.MX_WB_txt_Clave_Texto__c;
        newOpp.LeadSource = leadToConvert.LeadSource;
        newOpp.MX_SB_VTS_ContadorLlamadasTotales__c = leadToConvert.MX_SB_VTS_ContadorLlamadasTotales__c;
        newOpp.MX_SB_VTS_ContadorRemarcado__c = leadToConvert.MX_SB_VTS_ContadorRemarcado__c;
        newOpp.MX_SB_VTS_Llamadas_Efectivas__c = leadToConvert.MX_SB_VTS_Llamadas_Efectivas__c;
        newOpp.MX_SB_VTS_Llamadas_No_Efectivas__c = leadToConvert.MX_SB_VTS_Llamadas_No_Efectivas__c;
        newOpp.CloseDate = System.today();
        newOpp.MX_SB_VTS_Tipificacion_LV1__c =  leadToConvert.Resultadollamada__c;
        newOpp.MX_SB_VTS_Tipificacion_LV2__c =  leadToConvert.MX_SB_VTS_Tipificacion_LV2__c;
        newOpp.MX_SB_VTS_Tipificacion_LV3__c =  leadToConvert.MX_SB_VTS_Tipificacion_LV3__c;
        newOpp.Description=leadToConvert.Description;
        newOpp.MX_SB_VTS_LookActivity__c = leadToConvert.MX_SB_VTS_LookActivity__c;
        newOpp.MX_SB_VTS_TrayAttention__c = leadToConvert.MX_SB_VTS_TrayAttention__c;
		newOpp.MX_SB_VTS_CampaFaceName__c = leadToConvert.MX_SB_VTS_CampaFaceName__c;
        newOpp.MX_SB_VTS_DetailCampaFace__c = leadToConvert.MX_SB_VTS_DetailCampaFace__c;
        newOpp.MX_SB_VTS_CodCampaFace__c = leadToConvert.MX_SB_VTS_CodCampaFace__c;							   
        return newOpp;
    }
    /** Crea un email dummy si el lead no cuenta con email */
    public static String createDummyEmail(Lead leadToConvert) {
        return leadToConvert.MX_SB_VTS_Contador_Email__c + '@dummy.com';
    }
}