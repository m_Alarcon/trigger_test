/**
 * @description       : 
 * @author            : Gerardo Mendoza Aguilar
 * @group             : 
 * @last modified on  : 02-23-2021
 * @last modified by  : Gerardo Mendoza Aguilar
 * Modifications Log 
 * Ver   Date         Author                    Modification
 * 1.0   02-11-2021   Gerardo Mendoza Aguilar   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_SAC_1500_ExecutiveAuth {
    /** list of records */
    final static List<Case> CASEDATA = new List<Case>();

    /**
    * @description 
    * @author Gerardo Mendoza Aguilar | 02-11-2021 
    * @param idCase, isAuthenticated 
    **/
    @AuraEnabled
    public static void updateCaseExecutive(Id idCase, Boolean isAuthenticated) {
            final Case con = new Case (
                Id = idCase
                , MX_SB_1500_isAuthenticated__c = isAuthenticated
            );
            CASEDATA.add(con);
            MX_SB_SAC_Case_Service.updateCase(CASEDATA);
    }

    /**
    * @description 
    * @author Alan Santacruz | 02-23-2021 
    * @param caseData 
    **/
    @AuraEnabled
    public static void updateCaseComments(Case caseData) {
        final List<Case> caseList = new List<Case>();
        caseList.add(caseData);
        MX_SB_SAC_Case_Service.updateCase(caseList);
    }
}