/**
 * @File Name          : MX_RTL_DocumentosVida_mdt_Service.cls
 * @Description        :
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 5/26/2020, 05:43:34 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/26/2020   Juan Carlos Benitez     Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_RTL_DocumentosVida_mdt_Service {

    /** 
    * @description
    * @author Juan Carlos Benitez | 5/26/2020
    * @param Kit
    * @return List<MX_SB_MLT_Documentosvida__mdt>
    **/
    public static List<MX_SB_MLT_Documentosvida__mdt> getDocsData(String kit) {
        final string kits = '%'+ kit +'%';
		return MX_RTL_DocumentosVida_mdt_Selector.getDocsDataById(kits);
    }
}