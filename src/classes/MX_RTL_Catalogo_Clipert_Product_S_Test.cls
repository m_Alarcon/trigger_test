/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 08-09-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   08-06-2020   Eduardo Hernandez Cuamatzi   Initial Version
**/
@isTest
private class MX_RTL_Catalogo_Clipert_Product_S_Test {
    /**final list catProds */
    final static Set<String> CATPROD = new Set<String>{System.Label.MX_SB_VTS_Hogar};
    /**Final list process */
    final static Set<String> PROCESSCAT = new Set<String>{'VTS'};
    /**Final list prodCod */
    final static String PRODCODE = '1234';

    @TestSetup
    static void makeData() {
        final MX_WB_FamiliaProducto__c objFamilyPro2 = MX_SB_VTS_CallCTIs_utility.newFamiliy(System.Label.MX_SB_VTS_Hogar);
        insert objFamilyPro2;
        final Product2 proHogar = MX_SB_VTS_CallCTIs_utility.newProduct(System.Label.MX_SB_VTS_Hogar, objFamilyPro2);
        proHogar.MX_SB_SAC_Proceso__c = 'VTS';
        proHogar.IsActive = true;
        insert proHogar;
    }

    @isTest
    private static void insertCatProd() {
        final List<Product2> prdoLst = MX_RTL_Product2_Selector.findProsByNameProces(CATPROD, PROCESSCAT, true);
        final MX_SB_SAC_Catalogo_Clipert_Producto__c prodClip = new MX_SB_SAC_Catalogo_Clipert_Producto__c();
        prodClip.Name = System.Label.MX_SB_VTS_Hogar;
        prodClip.MX_SB_VTS_ProductCode__c = PRODCODE;
        prodClip.MX_SB_SAC_Producto__c = prdoLst[0].Id;
        final List<MX_SB_SAC_Catalogo_Clipert_Producto__c> lstProdClip = new List<MX_SB_SAC_Catalogo_Clipert_Producto__c>{prodClip};
        Test.startTest();
            final List<Database.SaveResult> lstResults = MX_RTL_Catalogo_Clipert_Product_Selector.newProductClipp(lstProdClip, true);
        Test.stopTest();
        System.assert(lstResults[0].isSuccess(),'Producto clipert insertado');
    }

    @isTest
    private static void findCatProd() {
        final List<Product2> prdoLst = MX_RTL_Product2_Selector.findProsByNameProces(CATPROD, PROCESSCAT, true);
        final Set<String> setProdC = new Set<String>{PRODCODE};
        final MX_SB_SAC_Catalogo_Clipert_Producto__c prodClip = new MX_SB_SAC_Catalogo_Clipert_Producto__c();
        prodClip.Name = System.Label.MX_SB_VTS_Hogar;
        prodClip.MX_SB_VTS_ProductCode__c = PRODCODE;
        prodClip.MX_SB_SAC_Producto__c = prdoLst[0].Id;
        prodClip.MX_SB_SAC_Activo__c = true;
        final List<MX_SB_SAC_Catalogo_Clipert_Producto__c> lstProdClip = new List<MX_SB_SAC_Catalogo_Clipert_Producto__c>{prodClip};
        MX_RTL_Catalogo_Clipert_Product_Selector.newProductClipp(lstProdClip, true);
        Test.startTest();
            final List<MX_SB_SAC_Catalogo_Clipert_Producto__c> lstfindClip = MX_RTL_Catalogo_Clipert_Product_Selector.catalogoClipCod(setProdC, PROCESSCAT, true);
        Test.stopTest();
        System.assertEquals(lstfindClip[0].MX_SB_VTS_ProductCode__c, PRODCODE, 'Producto clipert recuperado');
    }
}