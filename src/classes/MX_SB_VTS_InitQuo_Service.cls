/**
* @description       : Clase service que realiza funcionalidad para uso de cotizador Hsd
* @author            : Diego Olvera
* @group             : BBVA
* @last modified on  : 02-17-2021
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   27-11-2020   Diego Olvera   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_InitQuo_Service {
    
    /** String campos de QUERY para Opportunity */
    static final String FIELDSOPPQRY = 'Id, LeadSource, OwnerId, AccountId, StageName, Descripcion_y_comentarios__c, Name';
    /** String para Producto de Hogar */
    static final String PRODNAME = 'Hogar seguro dinámico';
    /** String para Origen */
    static final String OPPSOURCE = 'Outbound';
    /** String para Proceso */
    static final String PROCESSVTS = 'VTS';
    /** String para Proceso Propio */
    static final String PROCESPRO = 'Propio';
    /** String para Proceso Rentado */
    static final String PROCESREN = 'Rentado';
    /** String para referenciar valor de direccion */
    static final String VALDIRECA = 'Domicilio asegurado';
    /** Constructor de la clase */
    private MX_SB_VTS_InitQuo_Service() {}

    /**
    * @description Función que se ejecuta a partir del front del cotizador para crear quote
    * @author Diego Olvera | 27-11-2020 
    * @param idOp, nProRen, Id de la oportunidad recuperada desde el front JS
    * @return void
    **/  
    public static void ctrDataQuote(String idOp, String nProRen) {
        final List<Opportunity> lstRecOpps = new List<Opportunity>();
        final Opportunity lstSelOppRec = MX_RTL_Opportunity_Selector.getOpportunity(idOp, FIELDSOPPQRY);       
        lstRecOpps.add(lstSelOppRec);
        final List<Quote> objQuote = MX_RTL_Quote_Selector.selectSObjectsByOpps(lstRecOpps);
        if(objQuote.isEmpty()) {
            saveDataQuote(lstRecOpps, nProRen);
            for(Opportunity getRecOpps : lstRecOpps) {
                if(getRecOpps.LeadSource == OPPSOURCE && nProRen == PROCESPRO) {
                    updQuoteRec(idOp);       
                }
            }
        }
    }
    
    /**
    * @description Función de apoyo para la creación de quote, quotelineitem, y pricebookEntry
    * @author Diego Olvera | 27-11-2020 
    * @param lstOpps, valPro se recupera lista de tipo Opportunity almacenada en Función ctrDataQuote
    * @return void
    **/  
    public static void saveDataQuote(List<Opportunity> lstOpps, String valPro) {
        final Set<String> valProd = new Set<String>{PRODNAME};
        final Set<String> valProcess = new Set<String>{PROCESSVTS};
        final List<Product2> lstProduct2 = MX_RTL_Product2_Selector.findProsByNameProces(valProd, valProcess, true);
        for(Product2 lstProd2 : lstProduct2 ) {
            if (String.isNotBlank(lstProd2.Id) && String.isNotBlank(lstProd2.MX_WB_Familiaproductos__c)) {
                final String famProd = lstProd2.MX_WB_Familiaproductos__c;
                final String prod2Id = lstProd2.Id;
                final PricebookEntry pricebookItem = getPriceBookSta(lstProduct2);
                final List<Quote> lstQuote = fillDataQuo(pricebookItem, lstOpps, famProd, valPro);
                updQuoteline(lstQuote, pricebookItem, prod2Id);
            }   
        }
    }
    
    /**
    * @description Función que obtiene el status de PriceBookEntry
    * @author Diego Olvera | 27-11-2020 
    * @param lstProd2Status, Lista de tipo Product2 que se almacena en saveDataQuote
    * @return PriceBookEntry priceBkEn
    **/  
    public static PriceBookEntry getPriceBookSta(List<Product2> lstProd2Status) {
        PricebookEntry pricebookItem = null;
        final String[] prod2Ids = new String[]{lstProd2Status[0].Id};
            final List<PriceBookEntry> lstPriceBook = MX_RTL_PricebookEntry_Selector.pricebookEntries('Id, PriceBook2Id', prod2Ids); //Obtener query en soc
        for(PriceBookEntry lstPriceBk : lstPriceBook) {
            pricebookItem = lstPriceBk;
        }
        return pricebookItem;
    }
    
    /**
    * @description Función que recupera listado de quotes activas/creadas en registro de opportunity 
    * Si no existe alguna ejecuta el Función de creación de Quote
    * @author Diego Olvera | 27-11-2020 
    * @param priceBookEn, lstFillOpps, sProds
    * @return List<Quote> QUOTEVAL
    **/   
    public static List<Quote> fillDataQuo(PriceBookEntry priceBookEn, List<Opportunity> lstFillOpps, String famProds, String valRen) {
        List<Quote> objQuote = MX_RTL_Quote_Selector.selectSObjectsByOpps(lstFillOpps);
        objQuote = ctrNwQuo(lstFillOpps, priceBookEn, famProds, valRen);
        return objQuote;
    }
    
    /**
    * @description Función que crea un registro en objeto Quote dependiendo del origen
    * @author Diego Olvera | 27-11-2020 
    * @param lstAllOpps, priceBkEn2, valFamProd, valFinPro
    * @return List<Quote> lstQuoVals
    **/    
    public static List<Quote> ctrNwQuo(List<Opportunity> lstAllOpps, PriceBookEntry valPriceBkEn, String valFamProd, String valFinPro) {
        final List<Quote> lstQuoVals = new List<Quote>();
        for(Opportunity listOpps : lstAllOpps ) {
            if(listOpps.LeadSource == 'Call me back' || listOpps.LeadSource == 'Facebook' || listOpps.LeadSource == 'Tracking Web' || valFinPro == 'Rentado') {
                final Quote nwQuoCmb = new Quote();
                nwQuoCmb.Name = listOpps.Name;
                if(valFinPro == PROCESREN) {
                    nwQuoCmb.QuoteToName = listOpps.LeadSource +'-'+ PROCESREN;   
                } else {
                    nwQuoCmb.QuoteToName = listOpps.LeadSource +'-'+ 'Completa';
                }
                nwQuoCmb.MX_SB_VTS_Familia_productos__c = valFamProd;
                nwQuoCmb.PriceBook2Id = valPriceBkEn.Pricebook2Id;
                nwQuoCmb.OpportunityId = listOpps.Id;
                nwQuoCmb.OwnerId = listOpps.OwnerId;
                lstQuoVals.add(nwQuoCmb);
            } else if(listOpps.LeadSource == OPPSOURCE && valFinPro == PROCESPRO) {
                lstQuoVals.addAll(crtMulQuo(lstAllOpps, valPriceBkEn, valFamProd));
            }
        }
        MX_RTL_Quote_Selector.insertQuotes(lstQuoVals);
        return lstQuoVals;
    }
    
    /**
    * @description Función que crea registro en objeto de Quote
    * @author Diego Olvera | 27-01-2020 
    * @param lstNwOpps, valPriceBkNw, strFamProd
    * @return void
    **/
    public static List<Quote> crtMulQuo(List<Opportunity> lstNwOpps, PriceBookEntry valPriceBkNw, String strFamProd) {
        final List<Quote> lstQuoNwVal = new List<Quote>();
        for(Opportunity listNwOpps : lstNwOpps ) {
            for(integer i=1;i<4;i++) {
                final Quote nwQuoOut = new Quote();
                nwQuoOut.Name = listNwOpps.Name;
                nwQuoOut.QuoteToName = listNwOpps.Name;
                nwQuoOut.MX_SB_VTS_Familia_productos__c = strFamProd;
                nwQuoOut.PriceBook2Id = valPriceBkNw.Pricebook2Id;
                nwQuoOut.OpportunityId = listNwOpps.Id;
                nwQuoOut.OwnerId = listNwOpps.OwnerId;
                lstQuoNwVal.add(nwQuoOut);
            }
        }
        return lstQuoNwVal;
    }
    
    /**
    * @description Función que crea registro en objeto QuoteLineItem
    * @author Diego Olvera | 27-11-2020 
    * @param lstValQuotes, valPriceBkEn2, valProdId
    * @return List<QuoteLineItem> lstSetQuoli
    **/  
    public static List<QuoteLineItem> updQuoteline(List<Quote> lstValQuotes, PriceBookEntry valPriceBkEn2, String valProdId) {
        final Set<Id> valQuoIds = new Set<Id>();
        final List<QuoteLineItem> lstSetQuoli = new List<QuoteLineItem>();
        for(Quote lstQuoteVals : lstValQuotes) {
            valQuoIds.add(lstQuoteVals.Id);
            final List<QuoteLineItem> lstQuoLine = MX_RTL_QuoteLineItems_Selector.selSObjectsByQuotes(valQuoIds);
            final QuoteLineItem nwQuoteline = new QuoteLineItem();
            if(lstQuoLine.isEmpty()) {
                nwQuoteline.Quantity = 1;
                nwQuoteline.UnitPrice = 0;
                nwQuoteline.QuoteId = lstQuoteVals.Id;
                nwQuoteline.Product2Id = valProdId;
                nwQuoteline.PricebookEntryId = valPriceBkEn2.Id;
            } 
            lstSetQuoli.add(nwQuoteline);         
        }
        MX_RTL_QuoteLineItems_Selector.insertQuotesLine(lstSetQuoli);
        return lstSetQuoli;
    }
    
    /**
    * @description Función que actualiza el nombre de la quote después de haber sido insertada
    * @author Diego Olvera | 04-01-2020 
    * @param srtOppId
    * @return void
    **/  
    public static void updQuoteRec(String srtOppId) {
        final Opportunity objOppty = MX_RTL_Opportunity_Selector.getOpportunity(srtOppId, 'Name');
        final List<Opportunity> lstIdOppor = new List<Opportunity>();
        lstIdOppor.add(objOppty);
        final List<Quote> lstQuote = MX_RTL_Quote_Selector.selectSObjectsByOpps(lstIdOppor);
        lstQuote[0].QuoteToName = OPPSOURCE +'-'+ 'Completa';
        lstQuote[1].QuoteToName = OPPSOURCE +'-'+ 'Balanceada';
        lstQuote[2].QuoteToName = OPPSOURCE +'-'+ 'Basica';
        MX_RTL_Quote_Selector.updateResult(lstQuote, false);
    }

    /**
    * @description Actualiza el campo de Quote en registro de Direcciones
    * @author Diego Olvera | 26-01-2020 
    * @param currId Id de registro trabajado (Opportunity)
    * @return Map<String, Object> Datos recuperados
    **/
    public static void updSyncAdd(String currId) {
        final String dirType = VALDIRECA;
        final Opportunity objRecOpp = MX_RTL_Opportunity_Selector.getOpportunity(currId, 'Name, SyncedQuoteId');
        final List<Opportunity> lstRecOpps = new List<Opportunity>();
        lstRecOpps.add(objRecOpp);
        final Set<String> nidSet = new Set<String>();
        final Set<String> ntypeDir = new Set<String>();
        nidSet.add(currId);
        ntypeDir.add(dirType);
        final List<MX_RTL_MultiAddress__c> lstAddress = MX_RTL_MultiAddress_Selector.findAddress(nidSet, ntypeDir);
        for(Opportunity lstVaCuOpp : lstRecOpps) {            
            for(MX_RTL_MultiAddress__c lstUpdAdd : lstAddress) {
                lstUpdAdd.MX_RTL_QuoteAddress__c = lstVaCuOpp.SyncedQuoteId;
            }
        }
        MX_RTL_MultiAddress_Selector.upAddress(lstAddress);
    }
}