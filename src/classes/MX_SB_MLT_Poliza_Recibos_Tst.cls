@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_Poliza_Recibos_Tst
* Autor Daniel Perez Lopez
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_Poliza_Recibos_cls

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           18/02/2020      Daniel Lopez                         Creación
* --------------------------------------------------------------------------------
*/
public class MX_SB_MLT_Poliza_Recibos_Tst {
	@testSetup
    static void setDatosRecibos() {
        final String  tiporegistroReci = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
		final Account ctatest = new Account(recordtypeid=tiporegistroReci,firstname='DANIEL',lastname='PEREZ',Apellido_materno__pc='LOPEZ',RFC__c='PELD920912',PersonEmail='test@gmail.com');
        insert ctatest;
		final Account ctatestluffy = new Account(recordtypeid=tiporegistroReci,lastname='luffy',RFC__c='lufy192312',PersonEmail='luffy@gmail.com');
		insert ctatestluffy;
            final Contract contrato = new Contract(accountid=ctatest.Id,MX_SB_SAC_NumeroPoliza__c='PolizaTest',MX_SB_SAC_RFCAsegurado__c =ctatest.rfc__c,MX_SB_SAC_NombreClienteAseguradoText__c=ctatest.firstname,MX_WB_apellidoPaternoAsegurado__c=ctatest.lastname,MX_WB_apellidoMaternoAsegurado__c=ctatest.Apellido_materno__pc);
            insert contrato;
    }
    @isTest static void cosulaclipertetst() {
        final Contract ctWS = [Select id ,MX_SB_SAC_NumeroPoliza__c,MX_SB_SAC_RFCAsegurado__c from contract  where MX_SB_SAC_NumeroPoliza__c='PolizaTest' and MX_SB_SAC_NombreClienteAseguradoText__c='Daniel' limit 1];
     	test.startTest();
        	final List<Map<String,String>> mpwsr = MX_SB_MLT_Poliza_Recibos_cls.consultaClipert(ctWS.id);
        System.assertEquals(mpWsr[0].get('rfc'),ctWS.MX_SB_SAC_RFCAsegurado__c, 'prueba con exito');
        test.stopTest();
    }
    
        
}