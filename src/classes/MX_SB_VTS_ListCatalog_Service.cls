/**
* @FileName          : MX_SB_VTS_ListCatalog_Service
* @description       : Service class for use de web service of listInsuranceCatalog
* @Author            : Marco Antonio Cruz Barboza  
* @last modified on  : 10-16-2020
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   08-05-2020   Marco Antonio Cruz Barboza           Initial Version
* 1.1   13-01-2021   Marco Antonio Cruz Barbosa           Ajustes a la clase para consumo ASO
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton, sf:AvoidGlobalModifier')
global class MX_SB_VTS_ListCatalog_Service {
    /** List for Colonias */
    public static Final List<Object> COLONIA = new List<Object>();
    /** List for Municipios */
    public static Final List<Object> MUNICIPIO = new List<Object>();
    /** List for Ciudades */
    public static Final List<Object> CIUDAD = new List<Object>();
    /** List for Estados */
    public static Final List<Object> ESTADO = new List<Object>();
    /** @Description Status Code 200  */
    private static final Integer OKCODE = 200;
    
    /**
     * @description : Call an web service apex method to get de ZipCode
     * @Param String CO, String CP
     * @Return Map<String, List<Object>> with colonias an deserialize with a util class
     **/
    @AuraEnabled(cacheable=true)
    public static Map<String, List<Object>> srchZipCode(String serviceSelect, String postalCode) {       
        Final Map<String, Object> Service = new Map<String, Object>{'CO' => 'CO'};
        final HttpRequest requestZP = getRequest(postalCode);
        final HttpResponse response = MX_RTL_IasoServicesInvoke_Selector.callServices('getListInsuraceCustomerCatalog', Service, requestZP);
        final Integer statusCodeInt = response.getStatusCode();
        final Map<String,List<Object>> valuesZipCode = new Map<String,List<Object>>();
        if(statusCodeInt == OKCODE) {
            final MX_SB_VTS_wrpColoniasResp wrapper = (MX_SB_VTS_wrpColoniasResp)JSON.deserialize(response.getBody(),MX_SB_VTS_wrpColoniasResp.class);
            for(MX_SB_VTS_wrpColoniasResp.suburb test : wrapper.iCatalogItem.suburb) {
                COLONIA.add(test.neighborhood.name);
            }
            valuesZipCode.put('Colonia', COLONIA);
            
            for(MX_SB_VTS_wrpColoniasResp.suburb test : wrapper.iCatalogItem.suburb) {
                ESTADO.add(test.state.name);
            }
            valuesZipCode.put('Estado', ESTADO);
            
            for(MX_SB_VTS_wrpColoniasResp.suburb test : wrapper.iCatalogItem.suburb) {
                MUNICIPIO.add(test.county.name);
            }
            valuesZipCode.put('Municipio', MUNICIPIO);
            
            for(MX_SB_VTS_wrpColoniasResp.suburb test : wrapper.iCatalogItem.suburb) {
                CIUDAD.add(test.city.name);
            }
            valuesZipCode.put('Ciudad', CIUDAD);
            final List<Object> lstResponse = new List<Object>();
            lstResponse.add(new Map<String, Object>{'code' => String.valueOf(OKCODE), 'description' => 'Consumo de Servicio Exitoso!'});
            valuesZipCode.put('oResponse', lstResponse);
        } else {
            final List<Object> lstResponse = new List<Object>();
            lstResponse.add(MX_SB_VTS_Codes_Utils.statusCodes(statusCodeInt));
            valuesZipCode.put('oResponse', lstResponse);
        }
        return valuesZipCode;
    }
    
    /**
     * @description : Prepara el objeto HttpRequest para el consumo de servicio ASO
     * @Param String CO, String CP
     * @Return Map<String, List<Object>> with colonias an deserialize with a util class
     **/
    public static HttpRequest getRequest(String postalCode) {
        final Map<String,iaso__GBL_Rest_Services_Url__c> rstServices = iaso__GBL_Rest_Services_Url__c.getAll();
        final iaso__GBL_Rest_Services_Url__c getURLCPS = rstServices.get('getListInsuraceCustomerCatalog');
        final String endPointURLS = getURLCPS.iaso__Url__c;
        final HttpRequest requestPCS = new HttpRequest();
        requestPCS.setTimeout(120000);
        requestPCS.setMethod('POST');
        requestPCS.setHeader('Content-Type', 'application/json');
        requestPCS.setHeader('Accept', '*/*');
        requestPCS.setHeader('Host', 'https://test-sf.bbva.mx');
        requestPCS.setEndpoint(endPointURLS);
        requestPCS.setBody(getBodyZC(postalCode));
        return requestPCS;
    }
    
    /**
     * @description : Arma el requeste necesario para el consumo de servicio ASO
     * @Param String CO, String CP
     * @Return Map<String, List<Object>> with colonias an deserialize with a util class
     **/
    public static String getBodyZC(String postalCode) {
    	String getBodyZC = '';
        getBodyZC += '{';
        getBodyZC += '"header": {';
        getBodyZC += '"aapType": "10000002",';
        getBodyZC += '"dateRequest": "2020-07-03 05:30:00",';
        getBodyZC += '"channel": "66",';
        getBodyZC += '"subChannel": "120",';
        getBodyZC += '"branchOffice": "ASD",';
        getBodyZC += '"managementUnit": "APYME001",';
        getBodyZC += '"user": "CARLOS",';
        getBodyZC += '"idSession": "3232-3232",';
        getBodyZC += '"idRequest": "1212-121212-12121-212",';
        getBodyZC += '"dateConsumerInvocation": "2017-10-03 20:03:00"';
        getBodyZC += '},';
        getBodyZC += '"zipCode":"'+postalCode+'"';
        getBodyZC += '}';
        return getBodyZC;
    }
}