/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 11-02-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   11-02-2020   Eduardo Hernández Cuamatzi   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_SB_VTS_PaymentModule_Helper {

    /**Codigos aceptables de respuesta */
    final static List<Integer> HTTPOK = new List<Integer>{201,200,202,203,204};
    /**Codigos exito */
    final static String QUOTEUPDATED = 'quoteUpdated';

    /**
    * @description Actualiza 
    * @author Eduardo Hernandez Cuamatzi | 10-26-2020 
    * @param Quote quoteData 
    * @return Map<String, Object> 
    **/
    public static Map<String, Object> formQuote(Quote quoteData) {
        final Map<String, Object> respFormQuote = new Map<String, Object>();
        final String createQuote = quoteData.MX_SB_VTS_ASO_FolioCot__c;
        final String bodyRequest2 = '{"quoteId": "'+createQuote+'"}';        
        try {
            final HttpResponse response = MX_RTL_IasoServicesInvoke_Selector.callServices('createQuote', bodyRequest2);
            final Integer statusCodeInt = response.getStatusCode();
            if(HTTPOK.contains(statusCodeInt)) {
                quoteData.Status = 'Formalizada';
                respFormQuote.put('upsrtQuote', MX_RTL_Quote_Selector.upsrtQuote(quoteData));
            }
            respFormQuote.put(QUOTEUPDATED, true);
        } catch (CalloutException callEx) {
            respFormQuote.put(QUOTEUPDATED, false);
            respFormQuote.put('callEx', callEx.getCause());
            respFormQuote.put('callExMsj', callEx.getMessage());
        }
        return respFormQuote;
    }
    
    /**
    * @description Emite Poliza
    * @author Eduardo Hernandez Cuamatzi | 10-26-2020 
    * @param Quote quoteData Datos de cotización
    * @return Map<String, Object> Respuesta de servicio
    **/
    public static Map<String, Object> createdPolyce(Quote quoteData) {
        final Map<String, Object> respFormQuote = new Map<String, Object>();
        final String createQuote = quoteData.MX_SB_VTS_ASO_FolioCot__c;
        final String bodyRequest2 = '{"quoteId": "'+createQuote+'"}';        
        try {
            final HttpResponse response = MX_RTL_IasoServicesInvoke_Selector.callServices('createPolicy', bodyRequest2);
            final Integer statusCodeInt = response.getStatusCode();
            if(HTTPOK.contains(statusCodeInt)) {
                final Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
                final Map<String, Object> responsePolicy = (Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(responseMap.get('data')));
                final Map<String, Object> policyDates = (Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(responsePolicy.get('validityPeriod')));
                quoteData.Status = 'Emitida';
                quoteData.MX_SB_VTS_Numero_de_Poliza__c = (String)responsePolicy.get('policyNumber');
                quoteData.MX_SB_VTS_FechaInicio__c = (String)policyDates.get('startDate');
                quoteData.MX_SB_VTS_FechaFin__c = (String)policyDates.get('endDate');
                respFormQuote.put('upsrtQuote', MX_RTL_Quote_Selector.upsrtQuote(quoteData));
            }
            respFormQuote.put(QUOTEUPDATED, true);
        } catch (CalloutException callEx) {
            respFormQuote.put(QUOTEUPDATED, false);
            respFormQuote.put('callEx', callEx.getCause());
            respFormQuote.put('callExMsj', callEx.getMessage());
        }
        return respFormQuote;
    }
}