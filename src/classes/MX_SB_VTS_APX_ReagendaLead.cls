/*
* BBVA - Mexico
* @Author: Diego Olvera
* MX_SB_VTS_APX_ReagendaLead
* @Version 1.0
* @LastModifiedBy: Diego Olvera
* @ChangeLog
* 1.0 Created class - Diego Olvera
* 1.1 Add validation rules, assignments, fixed issues -  Jaime Terrats
* 1.2 Add logic for opportunity object - Diego Olvera
* 1.3 Refactoring class for avoiding complexity with methods (virtual class)
*/
public without sharing virtual class MX_SB_VTS_APX_ReagendaLead extends MX_SB_VTS_TaskReminder {
    /** String campos de QUERY */
     static final String LEADFIELDS = 'Id, Producto_Interes__c, MX_SB_VTS_IsReagenda__c, LeadSource';
    /**
* constructor
*/
    private MX_SB_VTS_APX_ReagendaLead() {} //NOSONAR
    /*
* @Method: saveTask
* @Description: creates task depending on the business flow
* @Params: myTask: Task Object, segmented: Boolean, leadId
*/
    @AuraEnabled
    public static void saveTask(Task myTask, Boolean segmented, String leadId, String getSource3) {

        try {
            Task mytask2 = myTask;
            final User managerId = [Select ManagerId, Name from User where Id =: UserInfo.getUserId()];
            if(segmented == false) {
                myTask.OwnerId = managerId.ManagerId;
            }
            myTask.IsReminderSet = true;
            myTask.Status = 'No iniciada';
            myTask.MX_SB_VTS_AgendadoPor__c = managerId.Name;
            myTask.ReminderDateTime = myTask.ReminderDateTime.addMinutes(-5);
            myTask.ActivityDate = myTask.ReminderDateTime.date();
            if(leadId.startsWith('00Q')) {
                mytask2 = MX_SB_VTS_TaskReminder.leadReagenda(leadId,myTask,segmented, managerId);
            } else if(leadId.startsWith('006') && getSource3 == 'Ventas Telemarketing') {
                mytask2 = MX_SB_VTS_TaskReminder.oppReagenda(leadId,myTask,segmented, managerId);
            } else {
                mytask2 = MX_SB_VTS_TaskReminder.oppVtaReagenda(leadId,myTask,segmented, managerId);
            }
            upsert mytask2;
        } catch(DmlException dmlEx) {
            throw new AuraHandledException(System.Label.MX_WB_LG_ErrorBack + dmlEx);
        }
    }

     /*
* @Method: getUsers
* @Description: return object data type which contains current user info,
* MX_SB_VTS_ProveedoresCTI__c data and current date
*/
    @AuraEnabled
    public static Object getUsers(String leadId) {
        final List<Object> listValues = new List<Object>();
        updBoolVal(leadId);
        try {
            final User getManager = [Select Id, MX_SB_VTS_ProveedorCTI__c, Manager.ManagerId, Manager.Name  from User WHERE Id =: UserInfo.getUserId()];
            final MX_SB_VTS_ProveedoresCTI__c proveedor = [Select Id, MX_SB_VTS_TieneSegmento__c, MX_SB_VTS_FinishBusinessHoursL_V__c,
                                                           MX_SB_VTS_BusinessHoursL_V__c, MX_SB_VTS_FinalBusinessHoursS__c, MX_SB_VTS_BusinessHoursL_S__c
                                                           From MX_SB_VTS_ProveedoresCTI__c Where MX_SB_VTS_Identificador_Proveedor__c =: getManager.MX_SB_VTS_ProveedorCTI__c];
            final DateTime currentDate = System.now();
            listValues.add(getManager);
            listValues.add(proveedor);
            listValues.add(currentDate);
        } catch(System.QueryException qEx) {
            throw new AuraHandledException(System.Label.MX_WB_LG_ErrorBack + qEx);
        }
        return listValues;
    }
    /**
* @method: updBoolVal: update boolean field according product opportunity name 
* @return: void
**/
    public static void updBoolVal(String leadId) {
        final Set<String> setLeadId = new Set<String>();
        setLeadId.add(leadId);
        if(leadId.startsWith('00Q')) {
            final List<Lead> getValue = MX_SB_VTS_UpNameCampaign_Service.findLeadsById(LEADFIELDS, setLeadId);
            for(Lead updVal : getValue) {
                if(updVal.Producto_Interes__c == 'Hogar seguro dinámico' || updVal.Producto_Interes__c == 'Vida Dinámico') {
                    updVal.MX_SB_VTS_IsReagenda__c = true;
                }
            }
            update getValue;
        } else if(leadId.startsWith('006')) {
            final List<Opportunity> getOppValue = MX_SB_VTS_CotizInitData_Service.findOppsById(setLeadId);
            for(Opportunity updValOpp : getOppValue) {
                if(updValOpp.Producto__c == 'Hogar seguro dinámico' || updValOpp.Producto__c == 'Vida Dinámico') {
                    updValOpp.MX_SB_VTS_IsOppReagenda__c = true;
                }
            }
             update getOppValue;
        }
    }
    /**
* @method: getLvl7Value: get values of tipificacion and update depending of a desicion flow
* @return: boolean value
**/
    @AuraEnabled
    public static Boolean getLvl7Value(String recordId, Boolean isLead) {
        // Contract list to validate if opp can be closed as won
        final List<Contract> getContracts = new List<Contract>();
        getContracts.addAll([Select Id from Contract where MX_WB_Oportunidad__c =: recordId]);
        final User managerId = [Select ManagerId, Name from User where Id =: UserInfo.getUserId()]; //NOSONAR
        if(recordId.startsWith(System.label.MX_SB_VTS_isLead)) {
            final Lead LeadLst = new Lead(Id =recordId, MX_SB_VTS_IsReagenda__c = false, OwnerId = managerId.ManagerId, MX_SB_VTS_Tipificacion_LV7__c = 'N/A');
            Database.update(LeadLst);
        } else if(recordId.startsWith(System.label.MX_SB_VTS_isOpp)) {
            if(getContracts.isEmpty()) {
                final Opportunity OppLst = New Opportunity(Id = recordId, OwnerId = managerId.ManagerId, MX_SB_VTS_IsOppReagenda__c = false, MX_SB_VTS_Tipificacion_LV7__c = 'N/A', StageName = 'Closed Lost');
                Database.update(OppLst);
            } else {
                final Opportunity OppLst1 = New Opportunity(Id = recordId, OwnerId = managerId.ManagerId, MX_SB_VTS_IsOppReagenda__c = false, MX_SB_VTS_Tipificacion_LV7__c = 'N/A', StageName = 'Closed Won');
                Database.update(OppLst1);
            }
        }
        return true;
    }


    /**
* @Method: updateIsReagenda
* @Description: update a checkbox value to identify when a component is executed from a flow or quickAction
* @Params: leadId: Lead Object, Id: leadId
**/
    @AuraEnabled
    public static void updateIsReagenda(String leadId) {
        if(leadId.startsWith('00Q')) {
            final Lead getLeadValue = [select Id, MX_SB_VTS_IsReagenda__c, RecordType.Name from Lead where Id =: leadId];
            getLeadValue.MX_SB_VTS_IsReagenda__c = false;
            update getLeadValue;
        } else if(leadId.startsWith('006')) {
            final Opportunity getOppValue = [Select Id, MX_SB_VTS_IsOppReagenda__c, RecordType.Name FROM Opportunity where Id =: leadId];
            getOppValue.MX_SB_VTS_IsOppReagenda__c = false;
            update getOppValue;
        }
    }
/**
* @method: fetchOpp: get values of Opportunity
* @return: Opp value
**/
    @auraEnabled
	  public static Opportunity fetchOpp(String strId) {
	  final string oppid = strId;
      return MX_SB_PS_Utilities.fetchOpp(OppId);
      }
}