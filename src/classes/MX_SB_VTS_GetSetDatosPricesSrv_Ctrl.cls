/**
 * @File Name          : MX_SB_VTS_GetSetDatosPricesSrv_Ctrl.cls
 * @Description        : Consumo de Servicio - Prices - Cotizador Hogar
 * @Author             : Alexandro Corzo
 * @Group              : 
 * @Last Modified By   : Alexandro Corzo
 * @Last Modified On   : 06-01-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0       06/01/2020      Alexandro Corzo        Initial Version
 * 1.1       24/02/2021      Alexandro Corzo        Se realizan ajustes a clase consumo ASO
**/
@SuppressWarnings('sf:UseSingleton')
public class MX_SB_VTS_GetSetDatosPricesSrv_Ctrl {
    /**
     * @description : Genera el JSON e Invoca al Servicio: createPrices
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    @AuraEnabled
    public static List<Object> setDataPricesCtrl(String strOppoId, String strSumAse) {
        final List<Object> lstDatPart = new List<Object>();
        return MX_SB_VTS_GetSetDatosPricesSrv_Service.setDataPricesSrv(strOppoId, strSumAse, lstDatPart);
    }
    
    /**
     * @description : Genera el JSON e Invoca al Servicio: createPrices para
     *              : realizar una recotización
     * @author      : Eduardo Hernandez Cuamatzi
     * @return      : Map<String, Object> oReturnValues
     */
    @AuraEnabled
    public static void reDataCotizCtrl(String strOppoId, String strProRent) {
    	MX_SB_VTS_GetSetDatosPricesSrv_Service.reDataCotiz(strOppoId, strProRent);
    }

    /**
     * @description : Se encarga de modificar la Lista de Datos Particulares base
     *              : en caso de que se agregue o quite una cobertura
     * @author      : Eduardo Hernandez Cuamatzi
     * @return      : Map<String, Object> oReturnValues
     */
    @AuraEnabled
    public static List<Object> setDataPricCtrlLst(String strOppoId, String strSumAse, List<Object> lstDatPart) {
        final List<Object> lstObjectFi = new List<Object>();
        for(Object partObject : lstDatPart) {
            final Map<String, Object> mapStrVals = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(partObject));
            final Map<String, String> mapStrKeys = new Map<String, String>();
            for(String keyMap : mapStrVals.keySet()) {
                mapStrKeys.put(keyMap, (String)JSON.deserializeUntyped(JSON.serialize(mapStrVals.get(keyMap))));
            }
            lstObjectFi.add(mapStrKeys);
        }
        return MX_SB_VTS_GetSetDatosPricesSrv_Service.setDataPricesSrv(strOppoId, strSumAse, lstObjectFi);
    }
}