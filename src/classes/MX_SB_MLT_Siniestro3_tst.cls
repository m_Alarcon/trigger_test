@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_Siniestro3_tst
* Autor Marco Antonio Cruz Barboza
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_Siniestro_cls

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           03/04/2020     Marco Cruz                        Creación
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_Siniestro3_tst {

    @TestSetup
    static void createData() {
        Final String profileTest = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User userTest = MX_WB_TestData_cls.crearUsuario('PruebaAdminTst', profileTest);  
        insert userTest;
        
        System.runAs(userTest) {
            Final String labelRecordType = Label.MX_SB_MLT_Proveedores_MA;
            final String  idRecordType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(labelRecordType).getRecordTypeId();
            final Account accountTest = new Account(RecordTypeId = idRecordType, Name='Usuario Test',Tipo_Persona__c='Física');
            insert accountTest;
            
            final Contract contractTest = new Contract(AccountId = accountTest.Id, MX_SB_SAC_NumeroPoliza__c='policyTest');
            insert contractTest;
            
            Final Siniestro__c siniVidaTest = new Siniestro__c();
            siniVidaTest.MX_SB_SAC_Contrato__c = contractTest.Id;
            siniVidaTest.MX_SB_MLT_NombreConductor__c = 'JUAN ALBERTO';
            siniVidaTest.MX_SB_MLT_APaternoConductor__c = 'TELLEZ';
            siniVidaTest.MX_SB_MLT_AMaternoConductor__c = 'ALCANTARA';
            siniVidaTest.MX_SB_MLT_Telefono__c = '5534253647';
            Final datetime citaAgenda = datetime.now();
            siniVidaTest.MX_SB_MLT_CitaVidaAsegurado__c = citaAgenda.addDays(2);
            siniVidaTest.MX_SB_MLT_AtencionVida__c = 'Agendar Cita';
            siniVidaTest.MX_SB_MLT_PreguntaVida__c = false;
            siniVidaTest.MX_SB_MLT_SubRamo__c = 'Ahorro';
            siniVidaTest.TipoSiniestro__c = 'Siniestros';
            siniVidaTest.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_RamoGF').getRecordTypeId();
            insert siniVidaTest;
            
            final Siniestro__c siniestroTest= new Siniestro__c();
            siniestroTest.MX_SB_SAC_Contrato__c=contractTest.Id;
            siniestroTest.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoAuto).getRecordTypeId();
            siniestroTest.TipoSiniestro__c = 'Colisión';
            siniestroTest.MX_SB_MLT_AjustadorMoto__c = false;
            siniestroTest.MX_SB_MLT_AjustadorAuto__c = false;
            siniestroTest.MX_SB_MLT_RequiereAsesor__c = false;
            siniestroTest.MX_SB_MLT_URLLocation__c ='19.4004664,-99.1394322';
            siniestroTest.MX_SB_MLT_Address__c ='Asturias 62, Álamos, CDMX, México';
            siniestroTest.MX_SB_MLT_LugarAtencionAuto__c ='Crucero';
            insert siniestroTest;
            
        }
    }
    
    @isTest 
    static void upsertSiniestroTest() {
        Final String rtpI = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoAuto).getRecordTypeId();
        Final Siniestro__c siniTest = [select id, MX_SB_MLT_AjustadorAuto__c,MX_SB_MLT_AjustadorMoto__c, MX_SB_MLT_JourneySin__c, TipoSiniestro__c from Siniestro__c WHERE RecordTypeId=:rtpI and TipoSiniestro__c = 'Colisión' ];
        test.startTest();
        	MX_SB_MLT_Siniestro_cls.upSertsini(siniTest);
        test.stopTest();
        System.assert(true,'Se ha enviado exitosamente');
    }
    
    @isTest
    static void updateSiniTest () {
        final Siniestro__c testSiniestro = [SELECT Id, RecordTypeId, MX_SB_MLT_TipoRobo__c FROM Siniestro__c limit 1];
        testSiniestro.TipoSiniestro__c ='Robo';
        test.startTest();
        	MX_SB_MLT_Siniestro_cls.upSin(testSiniestro);
        test.stopTest();
        system.assertNotEquals(null, testSiniestro.TipoSiniestro__c, 'prueb exitosa');
    }
    
    @isTest
    static void searchPolicyTest () {
        Final String recordTypeTest = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Proveedores_MA).getRecordTypeId();
        Final String idAccountTest = [SELECT Id FROM Account WHERE Name = 'Usuario Test' AND RecordTypeId =: recordTypeTest].Id;
        Final String policyTest = [SELECT MX_SB_SAC_NumeroPoliza__c FROM Contract WHERE Account.id =: idAccountTest].MX_SB_SAC_NumeroPoliza__c;
        Final String idContractTest = [SELECT Id FROM Contract WHERE MX_SB_SAC_NumeroPoliza__c=:policyTest].Id;
        test.startTest();
        	Final String contractReturn = MX_SB_MLT_Siniestro_cls.checkPolicy(policyTest);
        test.stopTest();
        system.assertEquals(idContractTest, contractReturn, 'los valores coinciden');
    }
    
    @isTest
    static void createTaskTest () {
        Final String rtTest = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_RamoGF').getRecordTypeId();
        Final Siniestro__c siniestroVida = [SELECT Id, RecordTypeId, MX_SB_MLT_Telefono__c, MX_SB_MLT_CitaVidaAsegurado__c,MX_SB_MLT_SubRamo__c, 
                                              TipoSiniestro__c FROM Siniestro__c WHERE RecordTypeId=:rtTest];
        Final Task testTaskVida = new Task();
        testTaskVida.WhatId = siniestroVida.Id;
        testTaskVida.Telefono__c = siniestroVida.MX_SB_MLT_Telefono__c;
        testTaskVida.ReminderDateTime = siniestroVida.MX_SB_MLT_CitaVidaAsegurado__c;
        testTaskVida.IsReminderSet = true;
        testTaskVida.Subject = 'Cita Agendada Test';
        test.startTest();
        	MX_SB_MLT_Siniestro_cls.createTask(testTaskVida);
        test.stopTest();
        System.assert(true,'Se ha creado la agenda');
    }
    
}