@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_RTL_personLifeEvent_Selector_Test
* Autor: Daniel Ramirez
* Proyecto: SB Presuscritos - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_RTL_personLifeEvent_Selector

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         23/10/2020      DanielRamirez                          Creación
* -----------------------------------------------------------------------------------------------
*/
public class MX_RTL_personLifeEvent_Selector_Test {
    /** Nombre de usuario Test */
    final static String NAME = 'Mariana';
    /** Apellido Paterno */
    Final static STRING LNAME= 'Esteves';
    /** Campos evento de vida */
    Final static String PERSEVFILDS = ' id, Name ';
    
    @TestSetup
    static void createData() {
        Final String proflName = [SELECT Name from profile where name = 'Asesores Presuscritos' limit 1].Name;
        final User userPSTst = MX_WB_TestData_cls.crearUsuario(NAME, proflName);
        insert userPSTst;
        System.runAs(userPSTst) {
            Final String ContactoRT =Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('IndustriesIndividual').getRecordTypeId();
            Final Contact contTest = new Contact(RecordTypeId = ContactoRT, FirstName=NAME, LastName=LNAME);
            insert contTest;
            Final PersonLifeEvent personlife = new PersonLifeEvent(Name=Name+LNAME, EventType='Job', EventDate=date.today(), PrimaryPersonId=contTest.id);
            insert personlife;
        }
    }
/*
* @description method que prueba PersonLifeEventTest
* @param  void
* @return void
*/   
    @isTest
    static void getPersonLifeEventTest() {
        Final ID conId= [SELECT Id from Contact where Name=: NAME+' '+LNAME limit 1].Id;
        test.startTest();
        Final List <PersonLifeEvent> personT = MX_RTL_PersonLifeEvent_Selector.getPersonLifeEvent(conId, PERSEVFILDS);
        system.assert(!personT.isEmpty(), 'Personal life encontrado');
        test.stopTest();
    }           
 
}