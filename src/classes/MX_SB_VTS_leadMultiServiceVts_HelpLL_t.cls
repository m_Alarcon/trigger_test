/**-------------------------------------------------------------------------
* Nombre: 		MX_SB_VTS_leadMultiServiceVts_HelpLL_t
* @author 		Alexandro Corzo
* Proyecto: 	Clase de Prueba para: MX_SB_VTS_leadMultiServiceVts_HelpLL
* Descripción : Clase de prueba para la clase helper para la API CMB / TW
* --------------------------------------------------------------------------
*                         Fecha           Autor                   Desripción
* --------------------------------------------------------------------------
* @version 1.0            24/09/2020      Alexandro Corzo         Creación de la Clase
* @version 1.1          05/10/2020      Marco Cruz               Se agrega method test validación
* --------------------------------------------------------------------------*/
@isTest
public class MX_SB_VTS_leadMultiServiceVts_HelpLL_t {
    /** Folio Quote Hogar Seguro Dinámico */
    final static String FOLIOHSD = '1234541';
    /** Folio Quote Hogar Seguro Dinámico Diff */
    final Static String FOLIOHSDDIFF = '1234567';
    /** Origen call me back */
    final static String TYPECMB = 'CALLMEBACK';
    /** Telefono del Cliente */
    final static String PHONECLIENT = '1234567890';
    
	@testSetup
    static void makeData() {
        final User objIntUser = MX_WB_TestData_cls.crearUsuario('IntegrationUser', 'IntegrationUser');
        insert objIntUser;
        MX_SB_VTS_CallCTIs_utility.initLeadMulServ();
        MX_SB_VTS_CallCTIs_utility.initLeadMulServHSD();
        makeDataHSDDinamic();
    }
    
    /**
     * @description Genera registro para Hogar Seguro Dinamico TW
     * @author Alexandro Corzo | 24-09-2020 
     **/
    public static void makeDataHSDDinamic() {
    	final User objAdminUser = MX_WB_TestData_cls.crearUsuario('TestUser', System.label.MX_SB_VTS_ProfileAdmin);
        insert objAdminUser;
		System.runAs(objAdminUser) {
            final Account objAccount = MX_WB_TestData_cls.crearCuenta('Hogarhsd', 'PersonAccount');
        	insert objAccount;
        
        	final Opportunity objOpportunity = MX_WB_TestData_cls.crearOportunidad('Opportunity Multiservice', objAccount.Id, objAdminUser.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
        	objOpportunity.TelefonoCliente__c = PHONECLIENT;
        	objOpportunity.EnviarCTI__c = false;
        	objOpportunity.FolioCotizacion__c = FOLIOHSD;
        	objOpportunity.StageName = 'Contacto';
            objOpportunity.LeadSource = 'Tracking Web';
            objOpportunity.Producto__c = 'Hogar seguro dinámico';
        	insert objOpportunity;
        }
    }
    
    @isTest
    private static void tstValidCMBTWCLeads() {
        String strJsonCMB = '{"type":"'+TYPECMB+'","channel":"10000120", "product":{"code": "3002", "plan":{"code": "008"}},';
        strJsonCMB += '"quote":{"id":"'+FOLIOHSD+'"},"coupon":"MA0001","person":{"name":"Juan","firstSurname":"Gutiérrez",';
        strJsonCMB += '"contactDetails":[{"key":"PER-PHONE-1","value":"'+PHONECLIENT+'"},{"key":"PER-EMAIL","value":"test.hogarhsd@gmail.com"}]},';
        strJsonCMB += '"appointment":{"time":"2020-06-06T16:00:00Z"}}';
        Test.startTest();
        	final User objUserCMBTW = [SELECT Id, MX_SB_VTS_ProveedorCTI__c, Name FROM User WHERE Name = 'IntegrationUser'];
        	System.runAs(objUserCMBTW) {
            	final Map<String, Object> listCMBTW = (Map<String, Object>) JSON.deserializeUntyped(strJsonCMB);
                final MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta dataCMBTW = MX_SB_VTS_leadMultiServiceVts_Service.fillDataServs(listCMBTW);
                final Boolean bResultTest = MX_SB_VTS_leadMultiServiceVts_HelpLL.validCMBTWCLeads(dataCMBTW);
                System.assert(bResultTest, 'Se valido correctamente el CMB / TW con Id Cotiza');
        	}
        Test.stopTest();
    }
    
    @isTest
    private static void tstValidCMBTWDiff() {
        String strJsonCMBDiff = '{"type":"'+TYPECMB+'","channel":"10000120", "product":{"code": "3002", "plan":{"code": "008"}},';
        strJsonCMBDiff += '"quote":{"id":"'+FOLIOHSDDIFF+'"},"coupon":"MA0001","person":{"name":"Juan","firstSurname":"Gutiérrez",';
        strJsonCMBDiff += '"contactDetails":[{"key":"PER-PHONE-1","value":"'+PHONECLIENT+'"},{"key":"PER-EMAIL","value":"test.hogarhsd@gmail.com"}]},';
        strJsonCMBDiff += '"appointment":{"time":"2020-06-06T16:00:00Z"}}';
        Test.startTest();
        	final User objUserCMBTWDiff = [SELECT Id, MX_SB_VTS_ProveedorCTI__c, Name FROM User WHERE Name = 'IntegrationUser'];
        	System.runAs(objUserCMBTWDiff) {
                final Map<String, Object> listCMBTWDiff = (Map<String, Object>) JSON.deserializeUntyped(strJsonCMBDiff);
                final MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta dataCMBTWDiff = MX_SB_VTS_leadMultiServiceVts_Service.fillDataServs(listCMBTWDiff);
                final Boolean bResultTest = MX_SB_VTS_leadMultiServiceVts_HelpLL.validCMBTWCLeads(dataCMBTWDiff);
                System.assert(bResultTest, 'Se valido correctamente el CMB / TW con Id Cotiza');
            }
       	Test.stopTest();
    }
    
   	@isTest
    private static void tstValidCLeadsQuote() {
        String jsonCMBTst = '{"type":"'+TYPECMB+'","channel":"10000120", "product":{"code": "3002", "plan":{"code": "008"}},';
        jsonCMBTst += '"quote":{"id":""},"coupon":"MA0001","person":{"name":"ALberto","firstSurname":"Fernandez",';
        jsonCMBTst += '"contactDetails":[{"key":"PER-PHONE-1","value":"'+PHONECLIENT+'"},{"key":"PER-EMAIL","value":"testemail@gmail.com"}]},';
        jsonCMBTst += '"appointment":{"time":"2020-06-06T16:00:00Z"}}';
        Test.startTest();
        	final User objUserCMBTW = [SELECT Id, MX_SB_VTS_ProveedorCTI__c, Name FROM User WHERE Name = 'IntegrationUser'];
        	System.runAs(objUserCMBTW) {
            	final Map<String, Object> lstCMB = (Map<String, Object>) JSON.deserializeUntyped(jsonCMBTst);
                final MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta tstDataCMB = MX_SB_VTS_leadMultiServiceVts_Service.fillDataServs(lstCMB);
                final Boolean resultTest = MX_SB_VTS_leadMultiServiceVts_HelpLL.validCMBTWCLeads(tstDataCMB);
                System.assert(resultTest, 'Se valida el cierre de CMB sin IdQuote');
        	}
        Test.stopTest();
    }
}