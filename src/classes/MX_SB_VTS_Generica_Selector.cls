/**
 * @description       : 
 * @author            : Eduardo Hernandez Cuamatzi
 * @group             : 
 * @last modified on  : 10-08-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   10-02-2020   Eduardo Hernandez Cuamatzi   Initial Version
 * 1.1   24-02-2021   Juan Carlos Benitez Herrera  Se añade busqueda CS por SRC
**/
@SuppressWarnings('sf:UseSingleton, sf:DMLWithoutSharingEnabled')
public class MX_SB_VTS_Generica_Selector {
    /**Recupera registros de custom object MX_SB_VTS_Generica__c*/
    public static List<MX_SB_VTS_Generica__c> findByTypeVal(String query, Set<String> typeVals) {
        return Database.query(String.escapeSingleQuotes('Select '+query+' FROM MX_SB_VTS_Generica__c WHERE MX_SB_VTS_Type__c IN: typeVals'));
    }
    
    /*
    * Description:Recupera los mensajes de IVR fuera de openPay
    * Author: Juan Carlos Benitez Herrera
    * param : Sting mensaje, string query
    */
    public static List<MX_SB_VTS_Generica__c> findIvrbyMsj (String mensaje, string query) {
        return Database.query(sTRING.escapeSingleQuotes('Select '+ query + ' FROM MX_SB_VTS_Generica__c WHERE MX_SB_VTS_SRC__c =: mensaje'));
    }
}