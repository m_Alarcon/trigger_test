/**
 * @description       : 
 * @author            : Arsenio.perez.lopez.contractor@bbva.com
 * @group             : 
 * @last modified on  : 12-01-2020
 * @last modified by  : Gerardo Mendoza Aguilar
 * Modifications Log 
 * Ver   Date         Author                                    Modification
 * 1.0   11-20-2020   Arsenio.perez.lopez.contractor@bbva.com   Initial Version
 * 1.1   12-01-2021   Gerardo Mendoza Aguilar                   Se cambia testsetup.
 * 1.2   12-01-2020   Gerardo Mendoza Aguilar                   Se agrega method testcalculateAge
 * 1.3   12-01-2020   Gerardo Mendoza Aguilar                   Se agrega method testcreateCustomWrp
 * 1.4   12-01-2020   Gerardo Mendoza Aguilar                   Se agrega method  testquoteBeneficiary
 * 1.5   02-24-2021   Juan Carlos Benitez                       Se agrega valor producto__c a querys de opp
**/
@isTest
public class MX_SB_PS_Resumen_Cotizacion_Helper_Test {
    /**
    * @description 
    * @author Arsenio.perez.lopez.contractor@bbva.com | 11-20-2020 
    **/
	@testSetup
    public static void  data() { 
    	MX_SB_PS_OpportunityTrigger_test.makeData();
        Final Product2 prod = new Product2(Name='Respaldo Seguro para Hospitalización', isActive=true);
        insert prod;
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'SendDocumentByEmail_PS',iaso__Cache_Partition__c = 'iaso.ServicesPartition',iaso__Url__c='https://150.250.220.36:18500/corporateCatalogs');
    }
    /**
    * @description 
    * @author Arsenio.perez.lopez.contractor@bbva.com | 11-20-2020 
    **/
    @isTest
    static void testCloOpp() {
        Test.startTest();
            final Opportunity opp = [Select Id, Name, producto__c from Opportunity where Name ='Opp1'];
            final String oppstri= MX_SB_PS_Resumen_Cotizacion_Helper.sCloneOpps(opp.Id, 'Respaldo Seguro para Hospitalización');
            System.assertNotEquals(opp.Id, oppstri);
        Test.stopTest();
    }
    @isTest
    static void testcalculateAge() {
        test.startTest();
        	final Integer age= MX_SB_PS_Resumen_Cotizacion_Helper.calculateAge(Date.today());
        	system.assertEquals(age,0,'Edad');
        test.stopTest();
    }
    @istest
    static void testcreateCustomWrp() {
        final Map<String,String> mapdata= new Map<String,String>();
        Final Opportunity oprtdata= [Select AccountId,Account.Genero__c,Account.PersonContact.FirstName, producto__c, Account.PersonContact.LastName, Account.PersonContact.Name ,SyncedQuoteId,SyncedQuote.BillingStreet,SyncedQuote.BillingCity,SyncedQuote.BillingState,SyncedQuote.BillingPostalCode,SyncedQuote.MX_SB_VTS_Colonia__c,SyncedQuote.MX_SB_PS_PagosSubsecuentes__c from Opportunity limit 1];
        Final Contact cntc = [SELECT Id,AccountId , Email, MailingPostalCode,MX_WB_ph_Telefono1__c, FirstName, LastName, birthdate, Apellido_materno__c, MX_RTL_CURP__c from Contact where AccountId=:oprtdata.AccountId];
        Final MX_SB_VTS_Beneficiario__c beneficiario= [Select id,MX_SB_VTS_Quote__c, Name,MX_SB_VTS_APaterno_Beneficiario__c,MX_SB_SAC_FechaNacimiento__c,MX_SB_SAC_Genero__c,MX_SB_VTS_AMaterno_Beneficiario__c,MX_SB_PS_CURP__c from MX_SB_VTS_Beneficiario__c where MX_SB_VTS_Quote__c =:oprtdata.SyncedQuoteId];
        test.startTest();
        	try {
        		MX_SB_PS_Resumen_Cotizacion_Helper.createCustomWrp(oprtdata, cntc,mapdata,beneficiario);
            } catch(Exception e) {
                final Boolean expectedError = e.getMessage().contains('Exception') ? false : true;
                System.assert(expectedError, 'No se ha podido crear informacion del customer');
            }
        test.stopTest();
    }
    @istest
    static void testquoteBeneficiary() {
        Final Opportunity oprtdata= [Select AccountId,Account.Genero__c,Account.PersonContact.FirstName, Account.PersonContact.LastName, Account.PersonContact.Name ,SyncedQuoteId,SyncedQuote.BillingStreet,SyncedQuote.BillingCity,SyncedQuote.BillingState,SyncedQuote.BillingPostalCode,SyncedQuote.MX_SB_VTS_Colonia__c,SyncedQuote.MX_SB_PS_PagosSubsecuentes__c from Opportunity limit 1];
        Final MX_SB_VTS_Beneficiario__c beneficiario= [Select id,MX_SB_VTS_Parentesco__c,MX_SB_VTS_Quote__c, Name,MX_SB_VTS_APaterno_Beneficiario__c,MX_SB_SAC_FechaNacimiento__c,MX_SB_SAC_Genero__c,MX_SB_VTS_AMaterno_Beneficiario__c,MX_SB_PS_CURP__c from MX_SB_VTS_Beneficiario__c where MX_SB_VTS_Quote__c =:oprtdata.SyncedQuoteId];
        test.startTest();
        	try {
                MX_SB_PS_Resumen_Cotizacion_Helper.quoteBeneficiary(oprtdata.SyncedQuoteId,'002',beneficiario);
            } catch(Exception e) {
	    	final Boolean expectedError = e.getMessage().contains('Exception') ? false : true;
                System.assert(expectedError, 'No se ha podido actualizar benefs asociados');
            }
        test.stopTest();
    }
    @isTest
    static void testmapdata2() {
        Final MX_SB_PS_Resumen_Cotizacion_Ctrl.wrpDocuments mapdata2 = new MX_SB_PS_Resumen_Cotizacion_Ctrl.wrpDocuments();
        final Opportunity opp = [Select Id,AccountId,Account.Name,Account.RFC__c, Name from Opportunity where Name ='Opp1' limit 1];
        test.startTest();
        try {
	        MX_SB_PS_Resumen_Cotizacion_helper.mapdata(mapdata2, opp.id, '203020');
        } catch(Exception e) {
            final Boolean expectedError = e.getMessage().contains('Script') ? false : true;
            System.assert(expectedError, 'Script-Thrown esperada');
            }
		test.stopTest();
    }
}