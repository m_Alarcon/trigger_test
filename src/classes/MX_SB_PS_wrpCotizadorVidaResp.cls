/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpCotizadorVida
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.
1.0					Julio Medellín                 27/07/2020     NoSonar its A service needs al these fields.*/
@SuppressWarnings('sf:TooManyFields,sf:ShortVariable,sf:LongVariable,sf:ExcessivePublicCount,sf:VariableDeclarationHidesAnother,sf:ShortClassName')
public class MX_SB_PS_wrpCotizadorVidaResp { 
	/*Public property for wrapper*/ 
	public data data {get;set;}
	/*Public property for wrapper*/ 
	public class data {
		/*Public property for wrapper*/ 
		public insuredList[] insuredList {get;set;}
		  /*Public property for wrapper*/
        public rateQuote[] rateQuote {get;set;}
		  /*Public property for wrapper*/
		  public string id {get;set;} 	
		/*Public property for wrapper*/
		public string numbr {get;set;} 	
	}
      /*Public subclass for wrapper*/
    public class insuredList {
	     /*Public property for wrapper*/
        public coverages[] coverages {get;set;}
		  /*Public property for wrapper*/
        public String holderIndicator {get;set;}
          /*Public property for wrapper*/		
         public string id {get;set;} 	
        /*public constructor subclass*/
		public insuredList() {
		 this.coverages = new coverages[] {};
		 this.coverages.add(new coverages());
		 this.holderIndicator='';
		 this.id='';
		}
  		
    }
     /*Public subclass for wrapper*/
    public class person {
	  /*Public property for wrapper*/
        public String lastName {get;set;}
      /*Public property for wrapper*/		
        public String secondLastName {get;set;}
      /*Public property for wrapper*/		
        public String firstName {get;set;}
      /*public constructor subclass*/
      public person() {
	   this.firstName = '';
	   this.lastName = '';
	   this.secondLastName = '';
	  }	  
    }
	  /*Public subclass for wrapper*/
    public class relationship {
	  /*Public property for wrapper*/
         public string id {get;set;} 
       /*public constructor subclass*/
       public relationship() {
	   this.id = '';
	   }  	   
    }
	  /*Public subclass for wrapper*/
    public class beneficiaryType {
	  /*Public property for wrapper*/
         public string id {get;set;} 	
	  /*public constructor subclass*/
        public beneficiaryType() {
		this.id = '';
		}	  
    }
      /*Public subclass for wrapper*/
      public class rateQuote {
	    /*Public property for wrapper*/
        public premium paymentWithoutTax {get;set;} 
		/*Public property for wrapper*/
        public premium firstPaymentLocalcurrency {get;set;} 
		/*Public property for wrapper*/
        public premium subsequentPayment {get;set;} 
		  /*Public property for wrapper*/
        public discount discount {get;set;}
		  /*Public property for wrapper*/
        public premium fractionalPaymentFeeLocal {get;set;} 
		  /*Public property for wrapper*/
        public paymentWay paymentWay {get;set;}
		  /*Public property for wrapper*/
        public premium totalPaymentWithoutTax {get;set;} 
		  /*Public property for wrapper*/
        public premium fractionalPaymentFee {get;set;} 
		  /*Public property for wrapper*/
        public premium relatedPolicyFee {get;set;} 
		  /*Public property for wrapper*/
        public premium rightPolicy {get;set;}
		  /*Public property for wrapper*/
        public premium firstPayment {get;set;}
		  /*Public property for wrapper*/
        public premium totalPaymentWithTax {get;set;} 
		  /*Public property for wrapper*/
        public premium totalPaymentWithTaxLocalcurrency {get;set;} 
		  /*Public property for wrapper*/
        public String subsequentPaymentsNumber {get;set;}
		  /*Public property for wrapper*/
        public premium totalPaymentWithoutTaxLocalcurrency {get;set;} 
		  /*Public property for wrapper*/
        public premium taxLocalcurrency {get;set;} 
		  /*Public property for wrapper*/
        public premium relatedPolicyFeeLocal {get;set;} 
		  /*Public property for wrapper*/
        public premium paymentWithoutTaxLocal {get;set;} 
		  /*Public property for wrapper*/
        public premium rightPolicyLocalcurrency {get;set;} 
		  /*Public property for wrapper*/
        public premium subsequentPaymentLocal {get;set;}	 
		 /*Public property for wrapper*/
		 public totalPaymentWithoutDiscountCupons[] totalPaymentWithoutDiscountCupons {get;set;}
		 /*public constructor subclass*/
		 public rateQuote() {
	     this.paymentWithoutTax = new premium();		
         this.firstPaymentLocalcurrency = new premium();		
         this.subsequentPayment = new premium();		  
         this.discount = new discount();		  
         this.fractionalPaymentFeeLocal = new premium();		  
         this.paymentWay = new paymentWay();
         this.totalPaymentWithoutTax = new premium();
         this.fractionalPaymentFee = new premium();
         this.relatedPolicyFee = new premium();
         this.rightPolicy = new premium();
         this.firstPayment = new premium();
         this.totalPaymentWithTax = new premium();
         this.totalPaymentWithTaxLocalcurrency = new premium();
         this.subsequentPaymentsNumber = '';
         this.totalPaymentWithoutTaxLocalcurrency = new premium();
         this.taxLocalcurrency = new premium();
         this.relatedPolicyFeeLocal = new premium();
         this.paymentWithoutTaxLocal = new premium();
         this.rightPolicyLocalcurrency = new premium();
		 this.subsequentPaymentLocal = new premium();
		 this.totalPaymentWithoutDiscountCupons= new totalPaymentWithoutDiscountCupons[]{};
		 }
    }
    
       /*Public subclass for wrapper*/
      public class coverages {
	    /*Public property for wrapper*/
        public catalogItemBase catalogItemBase {get;set;}
		  /*Public property for wrapper*/
        public premium premium {get;set;}
		  /*Public property for wrapper*/
        public premium premiumLocalcurrency {get;set;} 
		  /*Public property for wrapper*/
        public premium premiumWithoutTax {get;set;} 
		/*public constructor subclass*/
		public coverages() {
		this.catalogItemBase = new catalogItemBase();
		this.premium = new premium();
		this.premiumLocalcurrency =  new premium();
		this.premiumWithoutTax = new premium();
		}
    }
      /*Public subclass for wrapper*/
      public class paymentWay {
	    /*Public property for wrapper*/
         public string id {get;set;} 	
		/*Public property for wrapper*/
        public String name {get;set;}
      /*public constructor subclass*/
      public paymentWay() {
	  this.id = '';
	  this.name = '';
	  }	  
    } 
	   /*Public subclass for wrapper*/
      public class catalogItemBase {
	    /*Public property for wrapper*/
         public string id {get;set;} 
	    /*Public property for wrapper*/
        public String name {get;set;}
		/*public constructor subclass*/
		public catalogItemBase() {
		 this.id = '';
	    this.name = '';
		}
    }
        /*Public subclass for wrapper*/
      public class premium {
	    /*Public property for wrapper*/
        public Double amount {get;set;}
          /*Public property for wrapper*/		
        public moneda moneda {get;set;}
		/*public constructor subclass*/
		public premium() {
		 //this.amount = 0;
	     this.moneda = new moneda();
		}
    }
	  /*Public subclass for wrapper*/
    public class moneda {
	  /*Public property for wrapper*/
        public String code {get;set;}
		/*public constructor subclass*/
		public moneda() {
		 this.code = '';
		}
    }
	
	  /*Public subclass for wrapper*/
    public class discount {
	  	/*Public property for wrapper*/
        public String total {get;set;}
        /*Public property for wrapper*/		
        public String authorizedDiscount {get;set;} 
        /*Public property for wrapper*/		
        public String campaign {get;set;}
         /*Public property for wrapper*/		
        public String maximum {get;set;}	
		  /*Public property for wrapper*/
        public String couponValue {get;set;}
		/*public constructor subclass*/
		public discount() {
		 this.total = '';
	     this.authorizedDiscount = '';
		 this.campaign='';
		 this.maximum='';
		 this.couponValue='';
		}		
	}
	 /*Public subclass for wrapper*/
	public class totalPaymentWithoutDiscountCupons {
		/*Public property for wrapper*/
        public String moneda {get;set;}
        /*Public property for wrapper*/
		public String amount {get;set;}
		/*public constructor subclass*/ 
		public totalPaymentWithoutDiscountCupons() {
			this.moneda='';
			this.amount='';
		}
	}
    
    }