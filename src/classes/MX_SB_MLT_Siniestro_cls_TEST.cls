@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_Siniestro_cls_TEST
* Autor Daniel Perez Lopez
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_Siniestro_cls

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           10/12/2019      Daniel Lopez                         Creación
* 1.1           18/02/2020      Marco Cruz                        	 Retrabajo de Clase
* 1.2           02/03/2010      Juan Carlos Benitez Herrera          Se agrega method p/ajustadores
* 1.3           12/03/2020      Angel Nava                           migración campos contract
* 1.4           03/04/2020      Marco Cruz                           se agrega methods nuevos test nuevos
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_Siniestro_cls_TEST {
    @TestSetup
    static void creaDatos() { 
      	Final String nameProfile = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User objUsrTst = MX_WB_TestData_cls.crearUsuario('PruebaAdminTst', nameProfile);  
        insert objUsrTst;
        
        System.runAs(objUsrTst) {
            Final Account acambulancia =new Account(name='Ambulanciatest',recuperacion__c=false,Tipo_Persona__c='Física');
            insert acambulancia;
            Final Account acparamedico =new Account(name='Paramedicotest',recuperacion__c=false,Tipo_Persona__c='Física');
            insert acparamedico;
            final Account accTest = new Account(Name='luffy',Tipo_Persona__c='Física');
            insert accTest;
            
            final Contract contratoTest = new Contract(accountid=accTest.Id,MX_SB_SAC_NumeroPoliza__c='PolizaTest');
            insert contratoTest;

            final Siniestro__c sini= new Siniestro__c();
            sini.Folio__c='sinitest';
            sini.MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro';
            sini.MX_SB_SAC_Contrato__c = contratoTest.Id;
            sini.TipoSiniestro__c = 'Robo';
        	sini.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoAuto).getRecordTypeId();
            insert sini;
            final Siniestro__c siniestroT= new Siniestro__c();
            siniestroT.MX_SB_SAC_Contrato__c=contratoTest.Id;
            siniestroT.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoAuto).getRecordTypeId();
            siniestroT.TipoSiniestro__c = 'Robo';
            siniestroT.MX_SB_MLT_TipoRobo__c = Label.MX_SB_MLT_RoboParcial;
            siniestroT.MX_SB_MLT_ServicioAjAuto__c = false;
            siniestroT.MX_SB_MLT_RequiereAsesor__c = false;
            insert siniestroT;

            final Siniestro__c siniestrT= new Siniestro__c();
            siniestrT.MX_SB_SAC_Contrato__c=contratoTest.Id;
            siniestrT.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoAuto).getRecordTypeId();
            siniestrT.TipoSiniestro__c = 'Colisión';
            siniestrT.MX_SB_MLT_AjustadorMoto__c = false;
            siniestrT.MX_SB_MLT_AjustadorAuto__c = false;
            siniestrT.MX_SB_MLT_RequiereAsesor__c = false;
            siniestrT.MX_SB_MLT_URLLocation__c ='19.4004664,-99.1394322';
            siniestrT.MX_SB_MLT_Address__c ='Asturias 62, Álamos, CDMX, México';
            siniestrT.MX_SB_MLT_LugarAtencionAuto__c ='Crucero';
            insert siniestrT;
            Final WorkOrder workOrderT = new WorkOrder ();
            workOrderT.MX_SB_MLT_TipoServicioAsignado__c = Label.MX_SB_MLT_AsesorRobo;
            workOrderT.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Servicios).getRecordTypeId();
            workOrderT.MX_SB_MLT_Ajustador__c = objUsrTst.Id;
            workOrderT.MX_SB_MLT_EstatusServicio__c = 'Abierto';
            workOrderT.MX_SB_MLT_Siniestro__c = siniestroT.Id;
            workOrderT.MX_SB_MLT_ServicioAsignado__c = Label.MX_SB_MLT_Ajustador;
            workOrderT.MX_SB_MLT_TipoServicioAsignado__c = Label.MX_SB_MLT_Ajustador;
            insert workOrderT;   
        }
    }

    @isTest
    static void getServiciosEnviadosTest() {
        Final String idSini = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoAuto).getRecordTypeId();
        Final String labelRobo = Label.MX_SB_MLT_RoboParcial;
        Final Siniestro__c siniTest = [SELECT Id, RecordTypeId, MX_SB_MLT_TipoRobo__c FROM Siniestro__c WHERE RecordTypeId=:idSini AND MX_SB_MLT_TipoRobo__c =:labelRobo ];
        test.startTest();
        Final List<WorkOrder> woTest = MX_SB_MLT_Siniestro_cls.getServiciosEnviados(siniTest.Id);
        test.stopTest();
        System.assert(!woTest.isEmpty(),'Exito de Prueba');
    }
    
    @isTest
    static void createSinTest() {
        Final String idSini = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoAuto).getRecordTypeId();
        test.startTest();
        Final String idSiniestro = MX_SB_MLT_Siniestro_cls.createSin();
        Final Siniestro__c siniTest = [SELECT Id, RecordTypeId, MX_SB_MLT_Origen__c, TipoSiniestro__c FROM Siniestro__c WHERE RecordTypeId=:idSini AND MX_SB_MLT_Origen__c = 'Phone' AND TipoSiniestro__c NOT IN ('Robo') order by Id desc LIMIT 1 ];
        test.stopTest();        
        System.assertEquals(idSiniestro, siniTest.Id,'Exito en la Test');
    }
    
    @isTest
    static void getRecordTypeTest() {
        Final String devNameTest = Label.MX_SB_MLT_RamoAuto;
        Final String idRT = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoAuto).getRecordTypeId();
        test.startTest();
        Final Id idTest = MX_SB_MLT_Siniestro_cls.getRecordType(devNameTest);
        test.stopTest();
        System.assertEquals(idTest, idRT, 'Los Ids coinciden');
    }
    
    @isTest
    static void getvaluePickRoboTest() {
		Final String idSini = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoAuto).getRecordTypeId();        
        Final String labelRobo = Label.MX_SB_MLT_RoboParcial;
        Final Siniestro__c siniTest = [SELECT Id, RecordTypeId, MX_SB_MLT_TipoRobo__c FROM Siniestro__c WHERE RecordTypeId=:idSini AND MX_SB_MLT_TipoRobo__c =:labelRobo];
        test.startTest();
        Final String idSiniestroRtn = MX_SB_MLT_Siniestro_cls.getvaluePickRobo(siniTest.Id);
        Final String idSiniValue = MX_SB_MLT_Siniestro_cls.getValue(siniTest.Id);
        test.stopTest();
        System.assertEquals(idSiniestroRtn,idSiniValue,'Los valores coinciden');
        
    }
    
    @isTest
    static void getDatosSiniestroTest() {
        Final String idSini = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoAuto).getRecordTypeId();        
        Final String labelRobo = Label.MX_SB_MLT_RoboParcial;
        Final Siniestro__c siniTest = [SELECT Id, RecordTypeId, MX_SB_MLT_TipoRobo__c FROM Siniestro__c WHERE RecordTypeId=:idSini AND MX_SB_MLT_TipoRobo__c =:labelRobo];
        test.startTest();
        Final Siniestro__c siniestroTest = MX_SB_MLT_Siniestro_cls.getDatosSiniestro(siniTest.Id);
        test.stopTest();
        System.assertEquals(siniestroTest.Id,siniTest.Id,'El retorno Coincide');
    }
    
    @isTest
    static void createPartSinTest() {
        Final String idSini = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoAuto).getRecordTypeId();        
        Final Siniestro__c siniTest = [SELECT Id, RecordTypeId, MX_SB_MLT_Tipo_de_Da_o_Diverso__c FROM Siniestro__c WHERE RecordTypeId=:idSini AND MX_SB_MLT_Tipo_de_Da_o_Diverso__c= 'Efectivo Seguro'];
        test.startTest();
        Final Boolean createTest = MX_SB_MLT_Siniestro_cls.createPartSin(siniTest.Id);
        test.stopTest();
        System.assert(createTest,'El retorno es correcto');
    }
    
    @isTest
    static void createServicesTest() {
       Final String idAmbulancia = [SELECT Id, Name FROM Account WHERE Name = 'Ambulanciatest'].Id;
       Final String idParamedico = [SELECT Id, Name FROM Account WHERE Name = 'Paramedicotest'].Id;
       Final String idProveedor = [SELECT Id, Name FROM Account WHERE Name = 'luffy'].Id;
       Final String idSini = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoAuto).getRecordTypeId();
       Final String labelRobo = Label.MX_SB_MLT_RoboParcial;
       String createTest = '';
       Final Siniestro__c siniTest = [SELECT Id, RecordTypeId, MX_SB_MLT_TipoRobo__c FROM Siniestro__c WHERE RecordTypeId=:idSini AND MX_SB_MLT_TipoRobo__c =:labelRobo ];
       test.startTest();
        createTest = MX_SB_MLT_Siniestro_cls.createServices(siniTest.Id, idProveedor, idParamedico, idAmbulancia);
        List<String> getPicklistTest = new List<String> ();
        getPicklistTest = MX_SB_MLT_Siniestro_cls.getPickList();
        if(!getPicklistTest.isEmpty()) {
            createTest=getPicklistTest[0];
        }
        createTest = MX_SB_MLT_Siniestro_cls.createServices(siniTest.Id, idProveedor, idParamedico, idAmbulancia);
       test.stopTest();
        System.assertEquals(createTest,'Exito test no Create','Si coincide'); 
    }

    @isTest 
    static void createAjusCol() {
        Final String rtpI = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoAuto).getRecordTypeId();
        Final Siniestro__c siniAj = [select id, MX_SB_MLT_AjustadorAuto__c,MX_SB_MLT_AjustadorMoto__c, MX_SB_MLT_JourneySin__c, TipoSiniestro__c from Siniestro__c WHERE RecordTypeId=:rtpI and TipoSiniestro__c = 'Colisión' ];
        test.startTest();
        	MX_SB_MLT_Siniestro_cls.enviarWSAjustador(siniAj.Id);
        test.stopTest();
        System.assert(true,'Se ha enviado la ayuda exitosamente');
    }
}