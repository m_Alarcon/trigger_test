/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpTest2
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 11/08/2020     Creación del wrapper.*/
@isTest
public class MX_SB_PS_wrpTest2 {
    /*final string message properties*/
   Final static string LBUILDC = 'Build constructor';

    /*Methodo MX_SB_PS_wrpFormalizarCotizacion */
	public  static testMethod void  init13() {
		final MX_SB_PS_wrpFormalizarCotizacion wrFormCotiza = new MX_SB_PS_wrpFormalizarCotizacion();
		System.assertEquals(wrFormCotiza, wrFormCotiza,LBUILDC);
	}
	/*Methodo MX_SB_PS_wrpFormalizarPago */
	public  static testMethod void  init14() {
		final MX_SB_PS_wrpFormalizarPago wrpFormalizarPago  = new MX_SB_PS_wrpFormalizarPago();
		System.assertEquals(wrpFormalizarPago, wrpFormalizarPago,LBUILDC);
	}
 	/*Methodo MX_SB_PS_wrpGenerateOTP */
	public  static testMethod void  init15() {
		final MX_SB_PS_wrpGenerateOTP wrpGenerateOTP =  new MX_SB_PS_wrpGenerateOTP();
		System.assertEquals(wrpGenerateOTP, wrpGenerateOTP,LBUILDC);
	} 
	/*Methodo MX_SB_PS_wrpGenerarBenificiario */
	public  static testMethod void  init16() {
		final MX_SB_PS_wrpGenerarBenificiario wrpGenBenni = new MX_SB_PS_wrpGenerarBenificiario();
		System.assertEquals(wrpGenBenni, wrpGenBenni,LBUILDC);
	}
	/*Methodo MX_SB_PS_wrpGuardarDatosCliente */
	public  static testMethod void  init17() {
		final MX_SB_PS_wrpGuardarDatosCliente wrpGDC = new MX_SB_PS_wrpGuardarDatosCliente();
		System.assertEquals(wrpGDC, wrpGDC,LBUILDC);
	}
	/*Methodo MX_SB_PS_wrpListCountriesResp */
	public  static testMethod void  init18() {
		final MX_SB_PS_wrpListCountriesResp wrpLCR  = new MX_SB_PS_wrpListCountriesResp();
		System.assertEquals(wrpLCR, wrpLCR,LBUILDC);
	}
	/*Methodo MX_SB_PS_wrpLugaresResp */
	public  static testMethod void  init19() {
		final MX_SB_PS_wrpLugaresResp wrpLugaresResp  = new MX_SB_PS_wrpLugaresResp();
		System.assertEquals(wrpLugaresResp, wrpLugaresResp,LBUILDC);
	}
	

}