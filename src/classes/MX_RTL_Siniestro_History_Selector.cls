/**
 * @File Name          : MX_RTL_Siniestro_History_Selector.cls
 * @Description        :
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 5/26/2020, 05:22:22 AM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/26/2020   Juan Carlos Benitez         Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_RTL_Siniestro_History_Selector {
    /** List of records to be returned */
    final static List<SObject> HISTORYSRCH = new List<SObject>();
	/** List of records to be returned */
    final static List<SObject> RETURN_SINHISTORY = new List<SObject>();
    /**
    * @description
    * @author Juan Carlos Benitez | 5/25/2020
    * @param Set<Id> ids
    * @return HISTORYSRCH
    **/
    public static List<Siniestro__History> getSinHDataByParentId(Set<Id> ids, String condition, List<Sobject> lISTHISTORY, Map<String,String> mapa) {
        switch on condition {
            when 'ParentId' {
                HISTORYSRCH.addAll(Database.query(String.escapeSingleQuotes('SELECT '+ mapa.get('fields') + ' FROM Siniestro__History where ParentId in: ids')));
            }
            when 'Id' {
                HISTORYSRCH.addAll(Database.query(String.escapeSingleQuotes('SELECT '+ mapa.get('fields') + ' FROM Siniestro__History where id=:lISTHISTORY' +mapa.get('orderField') + mapa.get('limite'))));
                
            }
        }
        return HISTORYSRCH;
    }
}