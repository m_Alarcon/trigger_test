/**
 * @description       : Clase que permite la reutilizacion de consultas a la metadata ScriptComponent.
 * @author            : Alan Ricardo Hernandez
 * @group             : 
 * @last modified on  : 22/02/2021
 * @last modified by  : Alan Ricardo Hernandez
 * Modifications Log 
 * Ver   Date         Author                   Modification
 * 1.0   22/02/2021   Alan Ricardo Hernandez   Initial Version
**/
public with sharing class MX_RTL_ScriptComponent_Selector {

    /** Constante seleccion query */
    final static String STR_SELECT = 'SELECT ';

    /** Constante origen query */
    final static String STR_FROM = ' FROM MX_SB_PS_ScriptComponent__mdt';

    /** Constante origen query condicionado */
    final static String STR_FROM_C = ' FROM MX_SB_PS_ScriptComponent__mdt WHERE ';

    /**
    * @description Constructor Privado
    * @author Alan Ricardo Hernandez | 22/02/2021 
    **/
    private MX_RTL_ScriptComponent_Selector() {}

    /**
    * @description Methodo que permite realizar consultas a la metadata.
    * @author Alan Ricardo Hernandez | 22/02/2021 
    * @param queryField 
    * @param conditionField 
    * @param whithCondition 
    * @return List<MX_SB_PS_ScriptComponent__mdt> 
    **/
    public static List<MX_SB_PS_ScriptComponent__mdt> resultadosQueryScript(String queryField, String conditionField, Boolean whithCondition) {
        String query;
        switch on String.valueOf(whithCondition) {
            when 'false' {
                query = STR_SELECT + queryField + STR_FROM;
            }
            when 'true' {
                query = STR_SELECT + queryField + STR_FROM_C + conditionField;
            }
        }
        System.debug('query: '+ query);
        return Database.query(String.escapeSingleQuotes(query).unescapeJava());
    }
}