/**
 * @File Name          : MX_BPP_RelatedLeads_Crtl.cls
 * @Description        : Controlador de LWC MX_BPP_RelatedLeads
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 16/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      16/06/2020           Jair Ignacio Gonzalez G.   Initial Version
**/
public without sharing class MX_BPP_RelatedLeads_Crtl {
    /**
     * @Method MX_BPP_RelatedLeads_Crtl
     * @Description Singletons
    **/
    @TestVisible private MX_BPP_RelatedLeads_Crtl() {
    }

    /**
     * @Method convertLeads
     * @param Id idLead
     * @Description Convierte un Lead
     * @return Map<String,String>
    **/
    @AuraEnabled
    public static Map<String,String> convertLeads(Id idLead) {
        return MX_BPP_RelatedLeads_Service.serConvertLeads(idLead);
    }

    /**
     * @Method relatedLeads
     * @param string idAccount
     * @Description Lista de Leads asociados a la Cuenta
     * @return List<CampaignMember>
    **/
    @AuraEnabled(cacheable=true)
    public static List<CampaignMember> relatedLeads(string idAccount) {
        return MX_BPP_RelatedLeads_Service.serRelatedLeads(idAccount);
    }

    /**
     * @Method buttonMenu
     * @param Id idUser
     * @Description Regresa si el usuario debe de ver los botones o no
     * @return Boolean
    **/
    @AuraEnabled(cacheable=true)
    public static Boolean buttonMenu(Id idUser) {
        return MX_BPP_RelatedLeads_Service.serButtonMenu(idUser);
    }
}