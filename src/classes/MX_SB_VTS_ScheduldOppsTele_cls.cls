/**
 * @File Name          : MX_SB_VTS_ScheduldOppsTele_cls.cls
 * @Description        :
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              :
 * @Last Modified By   : Eduardo Hernandez Cuamatzi
 * @Last Modified On   : 09-22-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    7/10/2019   Eduardo Hernández Cuamatzi     Initial Version
 * 1.1   21/01/2020   Eduardo Hernández Cuamatzi     Fix hora de filtro y oportunidades cerradas
 * 1.1.1 24/02/2020   Eduardo Hernández Cuamatzi     Fix registros duplicados
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_ScheduldOppsTele_cls {
    /**
     * sendOppsHogar envia Oportunidades a SmartCenter
     */
    public static void sendOppsHogar(List<ProcNotiOppNoAten__c> lsNotif, Map<Id,ProcNotiOppNoAten__c> mapOldNotif) {
        for (ProcNotiOppNoAten__c procNoti : lsNotif) {
            if(String.isNotBlank(procNoti.Proceso__c) && procNoti.Proceso__c.equals(System.Label.MX_SB_VTS_SchdProces) && procNoti.Activo__c) {
                createScheduldOpps(procNoti);
            }
        }
    }

    /**
     * createScheduldOpps crear query base de scheduld
     * @param  registro de trabajos programados
     */
    public static void createScheduldOpps(ProcNotiOppNoAten__c lsNotif) {
        final MX_SB_VTS_SendTrackingTelemarketing_sch scheduldOppTlmk = new MX_SB_VTS_SendTrackingTelemarketing_sch();
        final string sQuery = createQueryQuotes();
        final String sch = '0 ' + lsNotif.Minutos__c  + ' 0-23 1-31 * ? *';
        final String sNombreProc = lsNotif.Horario__c + ' : ' + lsNotif.Minutos__c + ' estatus : ' + 'Tracking Telemarketing';
        scheduldOppTlmk.sQuery = sQuery;
        System.schedule(sNombreProc, sch, scheduldOppTlmk);
    }

    /**
    * @description Crea envios de jobs para cupones
    * @author Eduardo Hernandez Cuamatzi | 28/5/2020 
    * @param List<ProcNotiOppNoAten__c> lsNotif 
    * @param Map<Id ProcNotiOppNoAten__c> mapOldNotif 
    * @return void 
    **/
    public static void sendOppsTeleCup(List<ProcNotiOppNoAten__c> lsNotif, Map<Id,ProcNotiOppNoAten__c> mapOldNotif) {
        for (ProcNotiOppNoAten__c procNoti : lsNotif) {
            if(String.isNotBlank(procNoti.Proceso__c) && procNoti.Proceso__c.equals(System.Label.MX_SB_VTS_SchdProcesCup) && procNoti.Activo__c && String.isNotBlank(procNoti.Horario__c)) {
                createSchedOppsCup(procNoti);
            }
        }
    }

     /**
     * createSchedOppsCup crear query base de scheduld pára cupones
     * @param  registro de trabajos programados
     */
    public static void createSchedOppsCup(ProcNotiOppNoAten__c lsNotif) {
        final MX_SB_VTS_SendTrackingCupon_sch scheduldOppCup = new MX_SB_VTS_SendTrackingCupon_sch();
        final string sQuery = createQueryOpps();
        final String sch = '0 ' + lsNotif.Minutos__c  + ' 0-23 1-31 * ? *';
        final String sNombreProc = lsNotif.Horario__c + ' : ' + lsNotif.Minutos__c + ' ' + 'Tracking Cupon';
        scheduldOppCup.sQuery = sQuery;
        System.schedule(sNombreProc, sch, scheduldOppCup);
    }

    /**
    * @description Crea query para Quotes
    * @author Eduardo Hernandez Cuamatzi | 28/5/2020 
    * @return string Query quotes String
    **/
    public static string createQueryQuotes() {
        String sQuery = 'Select Id, AccountId, Account.Name, Account.LastName, Account.FirstName, LeadSource, EnviarCTI__c, LastModifiedDate, ';
        sQuery += ' Account.ApellidoMaterno__c, FolioCotizacion__c, MX_WB_EnvioCTICupon__c, StageName, TelefonoCliente__c, OwnerId, Producto__c, Account.PersonEmail From Opportunity ';
        sQuery += ' Where LeadSource = \'Tracking Web\'';
        sQuery += ' And AccountId != null';
        sQuery += ' And EnviarCTI__c = false';
        sQuery += ' And MX_WB_EnvioCTICupon__c = false';
        sQuery += ' And StageName NOT IN (\'Closed Won\', \'Closed Lost\')';
        sQuery += ' And FolioCotizacion__c != null';
        sQuery += ' And CreatedDate >= ' + System.label.MX_SB_VTS_SCHEDULE_DATE_LBL;
        sQuery += ' And Caso_Relacionado__c = null';
        sQuery += ' And MX_SB_VTS_OppCase__c = null';
        String products = '';
        for(MX_SB_VTS_Generica__c genRec : [Select MX_SB_SAC_ProductFam__c, MX_SB_VTS_Type__c from MX_SB_VTS_Generica__c where MX_SB_VTS_Type__c = 'CP10']) {
            products += '\''+genRec.MX_SB_SAC_ProductFam__c+'\',';
        }
        products = products.substring(0, products.length()-1);
        sQuery += ' And Producto__c IN ('+products+')';
        return sQuery;
    }

    /**
    * @description Crea Query para Opps
    * @author Eduardo Hernandez Cuamatzi | 28/5/2020 
    * @return string queryString
    **/
    public static string createQueryOpps () {
        String sQuery = 'Select Id, AccountId, Account.Name, Account.LastName, Account.FirstName, LeadSource, EnviarCTI__c, LastModifiedDate, Producto__c,';
        sQuery += ' Account.ApellidoMaterno__c, MX_WB_Cupon__c, StageName, TelefonoCliente__c, OwnerId, Account.PersonEmail, Account.PersonMobilePhone, FolioCotizacion__c From Opportunity';
        sQuery += ' Where LeadSource = \'Tracking Web\'';
        sQuery += ' And AccountId != null';
        sQuery += ' And EnviarCTI__c = false';
        sQuery += ' And MX_WB_EnvioCTICupon__c = true';
        sQuery += ' And StageName NOT IN (\'Closed Won\', \'Closed Lost\')';
        sQuery += ' And CreatedDate >= ' + System.label.MX_SB_VTS_SCHEDULE_DATE_LBL;
        sQuery += ' And Opportunity.Caso_Relacionado__c = null';
        sQuery += ' And Opportunity.MX_SB_VTS_OppCase__c = null';
        String products = '';
        for (MX_SB_VTS_Generica__c genRec : [Select MX_SB_SAC_ProductFam__c, MX_SB_VTS_Type__c from MX_SB_VTS_Generica__c where MX_SB_VTS_Type__c = 'CP10']) {
            products += '\'' + genRec.MX_SB_SAC_ProductFam__c + '\',';
        }
        products = products.substring(0, products.length()-1);
        sQuery += ' AND Producto__c IN (' + products + ')';
        return sQuery;
    }
}