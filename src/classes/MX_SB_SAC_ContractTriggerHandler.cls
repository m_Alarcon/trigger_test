/**
 * @File Name          : MX_SB_SAC_ContractTriggerHandler.cls
 * @Description        : 
 * @Author             : Jaime Terrats
 * @Group              : 
 * @Last Modified By   : Jaime Terrats
 * @Last Modified On   : 12/21/2019, 1:55:48 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/21/2019   Jaime Terrats     Initial Version
**/
public without sharing class MX_SB_SAC_ContractTriggerHandler extends TriggerHandler {
    
    /** List for trigger.new records */
    final private List<Contract> newContractList;
    
    /** constructor */
    public MX_SB_SAC_ContractTriggerHandler() {
        super();
        this.newContractList = Trigger.new;
    }

    /**
    * @description 
    * @author Jaime Terrats | 12/21/2019 
    * @return void 
    **/
    protected override void beforeInsert() {
        MX_SB_SAC_MapContractFields.mapFields(newContractList);
    }

    /**
    * @description 
    * @author Jaime Terrats | 12/21/2019 
    * @return void 
    **/
    protected override void beforeUpdate() {
        MX_SB_SAC_MapContractFields.mapFields(newContractList);
    }
}