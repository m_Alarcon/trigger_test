@isTest
/**
 * @File Name          : MX_RTL_DocumentosVida_mdt_Service_Test.cls
 * @Description        : Prueba los methodos de MX_RTL_DocumentosVida_mdt_Service
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 01/06/2020, 09:40:22 AM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/26/2020   Juan Carlos Benitez         Initial Version
**/
public class MX_RTL_DocumentosVida_mdt_Service_Test {
  /** Var Numero de Kit**/
  Final static String KIT ='K01';
    @isTest
    static void testgetDocsData() {
        test.startTest();
    		MX_RTL_DocumentosVida_mdt_Service.getDocsData(KIT);
        system.assert(true,'Busqueda exitosa');
        test.stopTest();
    }
}