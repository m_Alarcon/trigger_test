/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpPlanesProducto
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
 @SuppressWarnings('sf:LongVariable,sf:ShortVariable')
public class MX_SB_PS_wrpPlanesProducto {
	  /*Public property for wrapper*/
    public header header {get;set;}
	  /*Public property for wrapper*/
	public String productCode {get;set;}

   /*Public constructor for wrapper*/
    public MX_SB_PS_wrpPlanesProducto() {
      header = new header(); 
      productCode = '';	  
    }
	
	  /*Public subclass for wrapper*/
	public class header {
	  /*Public property for wrapper*/
		public String aapType {get;set;}	
	  /*Public property for wrapper*/	
		public String dateRequest {get;set;}	
	  /*Public property for wrapper*/	
		public String channel {get;set;}
	  /*Public property for wrapper*/	
		public String subChannel {get;set;}
	  /*Public property for wrapper*/	
		public String branchOffice {get;set;}
	  /*Public property for wrapper*/	
		public String managementUnit {get;set;}
	  /*Public property for wrapper*/	
		public String user {get;set;}
	  /*Public property for wrapper*/	
		public String idSession {get;set;}
	  /*Public property for wrapper*/	
		public String idRequest {get;set;}
	  /*Public property for wrapper*/	
		public String dateConsumerInvocation {get;set;}	
		/*public constructor subclass*/
		public header() {
		 this.aapType='';	
		 this.dateRequest = '';
		 this.Channel = '';
		 this.SubChannel = '';
		 this.branchOffice = '';
		 this.managementUnit = '';
		 this.user = '';
		 this.IdSession = '';
		 this.idRequest = '';
		 this.dateConsumerInvocation = '';
		}		
	}
}