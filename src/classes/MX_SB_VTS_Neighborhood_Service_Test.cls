@isTest
/**
 * @File Name          : MX_SB_VTS_Neighborhood_Service_Test
 * @Description        :
 * @Author             : Marco Antonio Cruz Barboza
 * @Group              :
 * @Last Modified By   : Marco Antonio Cruz Barboza
 * @Last Modified On   : 26/12/2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    26/12/2020    Marco Antonio Cruz Barboza         Initial Version
**/
public class MX_SB_VTS_Neighborhood_Service_Test {

        /** User name testing */
    final static String TESTINGUSR = 'UserTest';
    /** Account Name testing*/
    final static String TESTACCOUNT = 'Test Account';
    /** Opportunity Name Testintg */
    final static String TESTOPPORT = 'Test Opportunity';
    /**URL Mock Test*/
    Final static String TESTURL = 'http://www.example.com';
    /**URL Mock Test*/
    Final static String POSTALCD = '07469';
    /**Body Response Mock*/
    Final static String RESPONSEJS = '[{"comparables": 13,"houseList": [{"comparablesNumber": 13,"price": 4667515.5,"pricem2": 15789.149,"surface": 295.6154,"byRooms": [{"price": 837700.0,"pricem2": 10471.25,"room": 1,"surface": 80.0}],"comparables": [{"bathroom": 5,"description": "Excelente casa espaciosa de 4 niveles  con 12 recámaras , 5 baños, 3 estacionamientos, patio, comed","latitude": 19.3287,"linkportal": "http://www.doomos.com.mx/de/7997057_casa---coyoacan.html","longitude": -99.165,"m2": 445.0,"price": 6800000.0,"rooms": 12}]}]}]';
    
    /* 
    @Method: setupTestN
    @Description: create test data set
    */
    @TestSetup
    static void setupTestN() {
        final User usrTestN = MX_WB_TestData_cls.crearUsuario(TESTINGUSR, System.Label.MX_SB_VTS_ProfileAdmin);
        System.runAs(usrTestN) {
            final Account tstAccN = MX_WB_TestData_cls.crearCuenta(TESTACCOUNT, System.Label.MX_SB_VTS_PersonRecord);
            tstAccN.PersonEmail = 'pruebaVts@mxvts.com';
            insert tstAccN;
            final Opportunity tstOpportN = MX_WB_TestData_cls.crearOportunidad(TESTOPPORT, tstAccN.Id, usrTestN.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            tstOpportN.LeadSource = 'Call me back';
            tstOpportN.Producto__c = 'Hogar';
            tstOpportN.Reason__c = 'Venta';
            tstOpportN.StageName = 'Cotizacion';
            insert tstOpportN;
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'neighborhoodSearch', iaso__Url__c = TESTURL, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
            
        }
    }
    
    /*
    @Method: getNeighborhoodTest
    @Description: return Map<String,Object> to send the latitude and longitude from a service
    */
    @isTest
    static void getNeighborhoodTest() {
        Final Map<String,String> tsecTest = new Map<String,String> {'tsec'=>'1234456789'};
		Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, 'Complete',RESPONSEJS, tsecTest); 
        iaso.GBL_Mock.setMock(mockCallout);
        Final Map<String,Object> testInfo = new Map<String,Object>();
        testInfo.put('latitude','19.328390326852794');
        testInfo.put('longitude','-99.16694122424343');
        testInfo.put('propertyType','flat');
        testInfo.put('isNewDev','true');
        test.startTest();
            Final List<Object> testResponse = MX_SB_VTS_Neighborhood_Service.getNeighborhood(testInfo);
            System.assertNotEquals(testResponse, null,'El mapa esta retornando');
        test.stopTest();
    }
    
    /*
    @Method: badResponseTest
    @Description: test method to check if the web service fail
	@param
    */
    @isTest
    static void badResponseTest() {
        Final Map<String,String> tsecTest = new Map<String,String> {'tsec'=>'1234456789'};
		Final MX_WB_Mock mockCallout = new MX_WB_Mock(500, 'System Error','', tsecTest); 
        iaso.GBL_Mock.setMock(mockCallout);
        Final Map<String,Object> testInfo = new Map<String,Object>();
        testInfo.put('latitude','19.328390326852794');
        testInfo.put('longitude','-99.16694122424343');
        testInfo.put('propertyType','flat');
        testInfo.put('isNewDev','true');
        test.startTest();
            Final List<Object> testResponse = MX_SB_VTS_Neighborhood_Service.getNeighborhood(testInfo);
            System.assertNotEquals(testResponse, null,'El mapa esta retornando');
        test.stopTest();
    }
    
}