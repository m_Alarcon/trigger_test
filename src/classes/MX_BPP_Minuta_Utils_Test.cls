/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Minuta_Utils
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2021-01-14
* @Description 	Test for BPyP Minuta Utils
* @Changes
* 2021-02-22	Edmundo Zacarias	  	New methods added: formatValueTest
* 2021-03-08    Edmundo Zacarias    New methods added: cleanValueTest, hasChangedTest
* 2021-03-29    Héctor Saldaña      Constructor method Test overrided due to new functionality
*                                   related to InsertMinuta method.
*                                   insertMinutaTest() method added.
*/
@isTest
public class MX_BPP_Minuta_Utils_Test {
    
    /*String para folios*/
    private final static Integer MONTHDIGIT = 1;

    @testSetup
    static void setup() {
        User testAdminUser = new User();
        testAdminUser = UtilitysDataTest_tst.crearUsuario('PruebaAdm', Label.MX_PERFIL_SystemAdministrator, 'BBVA ADMINISTRADOR');
        insert testAdminUser;

        System.runAs(testAdminUser) {
            final Account tstAccR = new Account();
            tstAccR.FirstName = 'testAcc';
            tstAccR.LastName = 'testAcc';
            tstAccR.No_de_cliente__c = '1234DJ9';
            tstAccR.OwnerId = testAdminUser.Id;
            insert tstAccR;

            final dwp_kitv__Visit__c tstVisitR = new dwp_kitv__Visit__c();
            tstVisitR.dwp_kitv__visit_duration_number__c = '15';
            tstVisitR.dwp_kitv__visit_status_type__c = '01';
            tstVisitR.dwp_kitv__visit_start_date__c = Date.today() + 4;
            tstVisitR.dwp_kitv__account_id__c = tstAccR.Id;
            insert tstVisitR;
            
            Document docObj;
			docObj = new Document();
			docObj.Body = Blob.valueOf('Some Document Text');
			docObj.ContentType = 'application/pdf';
			docObj.DeveloperName = 'PenguinsDoc';
			docObj.IsPublic = true;
			docObj.Name = 'PenguinsDoc';
			docObj.FolderId = UserInfo.getUserId();
			insert docObj;
        }
    }
    /**
    * @Description 	Test Method for constructor
    * @Return 		NA
    **/
	@isTest
    static void insertMinutaTest() {
        final User userAdmin = [SELECT Id FROM User WHERE Name = 'PruebaAdm'];

        Test.startTest();
        System.runAs(userAdmin) {
            final dwp_kitv__Visit__c visitTest = [SELECT Id, Name FROM dwp_kitv__Visit__c WHERE OwnerId =: userAdmin.Id LIMIT 1];
            final Blob doc = Blob.valueOf('Test 1 for Insert Minuta');
            final ContentVersion cvResult = MX_BPP_Minuta_Utils.insertMinuta(doc, visitTest);
            final ContentVersion cv2Test = new ContentVersion(Title='File 2', PathOnClient ='File_2.pdf', VersionData = Blob.valueOf('Test Insert Doc Link'),ContentLocation = 'S' );
            insert cv2Test;
            final ContentVersion cvWithDoc = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id =: cv2Test.Id LIMIT 1];
            MX_BPP_Minuta_Utils.insertContentDocLink(new List<ContentVersion>{cvWithDoc}, visitTest.Id, true);
            final MX_BPP_Minuta_Utils utilsClass = new MX_BPP_Minuta_Utils();
            System.assertNotEquals(null, utilsClass, 'Error on constructor');
            System.assertEquals('Minuta ' + visitTest.Name, cvResult.Title, 'Nombre incorrecto');
        }
        Test.stopTest();

    }
    
    /**
    * @Description 	Test Method for MX_BPP_Minuta_Utils.fechaToString()
    * @Return 		NA
    **/
	@isTest
    static void fechaToStringTest() {
        final Datetime todayDT = date.today();
        final String todayDTString = MX_BPP_Minuta_Utils.fechaToString(todayDT);
        System.assert(todayDTString.contains('de') && todayDTString.contains('del'), 'Error on fechaToString');
    }
    
    /**
    * @Description 	Test Method for MX_BPP_Minuta_Utils.fechaToString()
    * @Return 		NA
    **/
	@isTest
    static void getMesTest() {
        String monthToString = String.valueOf(date.today().month());
        if(monthToString.length() == MONTHDIGIT) {
            monthToString = '0' + monthToString;
        }
        final String monthDTString = MX_BPP_Minuta_Utils.getMes(monthToString);
        System.assertNotEquals(monthToString, monthDTString, 'Error on getMes');
    }

    /**
    * @Description 	Test Method for MX_BPP_Minuta_Utils.getParameter()
    * @Return 		NA
    **/
    @isTest
    static void testGetParameters() {
        final dwp_kitv__Visit__c testVisit = [SELECT Id, RecordType.Name FROM dwp_kitv__Visit__c WHERE CreatedBy.Name = 'PruebaAdm' LIMIT 1];
        test.startTest();
        final PageReference testPage = Page.MX_BPP_Minuta_BienvenidaDO_VF;
        Test.setCurrentPage(testPage);
        ApexPages.currentPage().getParameters().put('Id', testVisit.Id);
        final String result = MX_BPP_Minuta_Utils.getParameter('Id');
        System.assertEquals(String.valueOf(testVisit.Id).escapeHtml4(), result, 'Error on getParameters');
        test.stopTest();
    }

    /**
	* @Description 	Test Method for MX_BPP_Minuta_Utils.generateURLsTest()
	* @Return 		NA
	**/
    @isTest
    static void generateURLsTest() {
        final User userAdmin = [SELECT Id FROM User WHERE Name = 'PruebaAdm'];
        final List<String> lstStr = new LisT<String>();
        lstStr.add('PenguinsDoc');
        test.startTest();
        Map<String, String> genResult = new Map<String, String>();
        System.runAs(userAdmin) {
            genResult = MX_BPP_Minuta_Utils.generateURLs(lstStr);
        }
        system.assert(!genResult.isEmpty(), 'Error on generateURLs');
        test.stopTest();
    }
    
    /**
	* @Description 	Test Method for MX_BPP_Minuta_Utils.generateURLsTest()
	* @Return 		NA
	**/
    @isTest
    static void formatValueTest() {
        final User userAdmin = [SELECT Id FROM User WHERE Name = 'PruebaAdm'];
        String result;
        test.startTest();
        System.runAs(userAdmin) {
            result = MX_BPP_Minuta_Utils.formatValue('*Texto1\nTexto2');
        }        
        test.stopTest();
        System.assert(result.contains('<li>'), 'Error on formatValue');
    }
    
    /**
	* @Description 	Test Method for MX_BPP_Minuta_Utils.validateNullValues()
	* @Return 		NA
	**/
    @isTest
    static void validateNullValuesTest() {
        Test.startTest();
        final String valFinal = MX_BPP_Minuta_Utils.validateNullValues(null);
        Test.stopTest();
        
        System.assertNotEquals(null, valFinal, 'Error on validateNullValues');
    }
    
    /**
	* @Description 	Test Method for MX_BPP_Minuta_Utils.cleanValue()
	* @Return 		NA
	**/
    @isTest
    static void cleanValueTest() {
        String valInicial;
        Test.startTest();
        valInicial = 'Test1\r\n';
        final String valFinal = MX_BPP_Minuta_Utils.cleanValue(valInicial);
        Test.stopTest();
        
        System.assertNotEquals(valInicial, valFinal, 'Error on cleanValue');
    }
    
    /**
	* @Description 	Test Method for MX_BPP_Minuta_Utils.hasChanged()
	* @Return 		NA
	**/
    @isTest
    static void hasChangedTest() {
        final List<Boolean> results = new List<Boolean>();
        Test.startTest();
        results.add(MX_BPP_Minuta_Utils.hasChanged(null, 'Test'));
        results.add(MX_BPP_Minuta_Utils.hasChanged('Test', null));
        results.add(MX_BPP_Minuta_Utils.hasChanged('Test', 'Test2'));
        results.add(MX_BPP_Minuta_Utils.hasChanged(null, null));
        Test.stopTest();
        
        System.assert(results[0] && results[1] && results[2] && !results[3], 'Error on hasChanged');
    }
}