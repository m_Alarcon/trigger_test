@isTest
/**
* @FileName          : MX_SB_VTS_ProxyValueCipher_Service
* @description       : Service class for use de web service of ProxyValueCipher
* @Author            : Marco Antonio Cruz Barboza  
* @last modified on  : 18-09-2020
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   18-09-2020   Marco Antonio Cruz Barboza          Initial Version
**/
@SuppressWarnings('sf:AvoidGlobalModifier, sf:UseSingleton')
private class MX_SB_VTS_ProxyValueCipher_Service_Test {
    
    /** User name testing */
    final static String USRTOTEST = 'UserTest';
    /** Account Name testing*/
    final static String ACCOTOTEST = 'Test Account';
    /** Opportunity Name Testintg */
    final static String OOPTOTEST = 'Test Opportunity';
    /**URL Mock Test*/
    Final static String URLTOTEST = 'http://www.example.com';
    /**Id Cotiza Test*/
    Final static List<String> IDCOTIZA = new List<String>{'123450'};
    /*Service response mock*/
    Final Static String MOCKRESP = '{"results": ["87yiUyhpiiC27LJUwcMY067LJLdm9tNfwLXucJtWpKw"]}';
    /*TSCE TEST MOCK*/
    Final Static Map<String,String> TSECTST = new Map<String,String>{'TSEC'=>'123456789'};
    
    /* 
    @Method: setupTest
    @Description: create test data set
    */
    @TestSetup
    static void setupTest() {
        final User userToTst = MX_WB_TestData_cls.crearUsuario(USRTOTEST, System.Label.MX_SB_VTS_ProfileAdmin);
        System.runAs(userToTst) {
            final Account accToTest = MX_WB_TestData_cls.crearCuenta(ACCOTOTEST, System.Label.MX_SB_VTS_PersonRecord);
            accToTest.PersonEmail = 'pruebaVts@mxvts.com';
            insert accToTest;
            
            final Opportunity oppToTest = MX_WB_TestData_cls.crearOportunidad(OOPTOTEST, accToTest.Id, userToTst.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            oppToTest.LeadSource = 'Call me back';
            oppToTest.Producto__c = 'Hogar';
            oppToTest.Reason__c = 'Venta';
            oppToTest.StageName = 'Cotizacion';
            insert oppToTest;
            
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'ProxyValueCipher', iaso__Url__c = URLTOTEST, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'GetQuotation', iaso__Url__c = URLTOTEST, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
            
        }
    }
    
    /* 
    @Method: getIdEncryptedTest
    @Description: test the method getIdEncrypted 
	@param: Map<String, String>
    */
    @isTest
    static void getIdEncryptedTest() {
		Final MX_WB_Mock mockCallout = new MX_WB_Mock(200,'Complete',MOCKRESP , TSECTST); 
        iaso.GBL_Mock.setMock(mockCallout);
        test.startTest();
            Final Map<String, String> testResponse = MX_SB_VTS_ProxyValueCipher_Service.getIdEncrypted(IDCOTIZA);
            System.assertNotEquals(testResponse, null,'El mapa tiene contenido');
        test.stopTest();
    }
    
        /* 
    @Method: getIdEncryptedTest
    @Description: test the method getIdEncrypted when the web service fail
	@param: Map<String, String>
    */
    @isTest
    static void getIdEncryptedFail() {
		Final MX_WB_Mock mockCallout = new MX_WB_Mock(500,'Fatal Error','' , TSECTST); 
        iaso.GBL_Mock.setMock(mockCallout);
        test.startTest();
            Final Map<String, String> testResponse = MX_SB_VTS_ProxyValueCipher_Service.getIdEncrypted(IDCOTIZA);
            System.assertNotEquals(testResponse, null,'El mapa tiene contenido');
        test.stopTest();
    }

}