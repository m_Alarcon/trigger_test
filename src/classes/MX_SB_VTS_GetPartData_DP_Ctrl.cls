/**
 * @description       : 
 * @author            : Diego Olvera
 * @group             : 
 * @last modified on  : 10-22-2020
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   10-16-2020   Diego Olvera   Initial Version
 * 1.1   12-01-2021   Diego Olvera   Adecuaciónd de clase consumo ASO
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_GetPartData_DP_Ctrl {
    /**
     * @description: 	Realiza el consumo del servicio mock: DatosParticulares
     * @author: 		Diego Olvera
     * @return: 		Map<String, Object>
     */
    @AuraEnabled(cacheable=true)
    public static Map<String, Object> dptListCarInsCtrl(String sManagmenUnit, String sProductCode ) {
        return MX_SB_VTS_GetPartData_DP_Service.dptListCarInsCatSrvDp(sManagmenUnit, sProductCode);
    }
    /**
     * @description: 	Realiza el consumo del servicio mock: ObtenerCoberturas
     * @author: 		Diego Olvera
     * @return: 		Map<String, Object>
     */
    @AuraEnabled(cacheable=true)
    public static Map<String, Object> obtListCarInsCtrl(Map <String, Object> datosFinales) {
        return MX_SB_VTS_GetPartData_OB_Service.obtListCarInsCatSrvDp(datosFinales);
    }
}