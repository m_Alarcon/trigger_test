/**
 * @File Name          : MX_SB_VTS_GetSetPPForm_Service_tst.cls
 * @Description        : Clase encargada de controlar las acciones para el fomulario: Personaliza tu Protección
 * @Author             : jesusalexandro.corzo.contractor@bbva.com
 * @Group              : BBVA
 * @Last Modified By   : jesusalexandro.corzo.contractor@bbva.com
 * @Last Modified On   : 09/07/2020 09:10:00
 * @Modification Log   : 
 * Ver       Date            Author      		                        Modification
 * 1.0       09/7/2020       jesusalexandro.corzo.contractor@bbva.com   Initial Version
**/
@IsTest
public with sharing class MX_SB_VTS_GetSetPPForm_Service_tst {
    @IsTest
    static void dataCoberturas() {
        final Map<String,List<Object>> lstDataCoberturas = MX_SB_VTS_GetSetPPForm_Service.dataCoberturas();
        System.assert(!lstDataCoberturas.isEmpty(),'Se obtuvo el listado de coberturas');
    }

    @IsTest
    static void dataAmountVal() {
        final Map<String,List<Object>> lstDataAmount = MX_SB_VTS_GetSetPPForm_Service.dataAmountVal();
        System.assert(!lstDataAmount.isEmpty(), 'Se obtuvo el listado de montos');
    }
}