/**
* @File Name          : public class MX_SB_PS_CreateOppForECampaignM_Service.cls
* @Author             : Gerardo Mendoza Aguilar
* @Group              :
* @Last Modified By   : Arsenio.perez.lopez.contractor@bbva.com
* @Last Modified On   : 02-12-2021
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      28/10/2020           Gerardo Mendoza Aguilar          Initial Version
* 1.1      24/02/2021          Juan Carlos Benitez Herrera   Se añaden campos de contacto a mapeo
**/
@SuppressWarnings('sf:UseSingleton, sf:LocalVariableCouldBeFinal sf:DUDataflowAnomalyAnalysis')
public class MX_SB_PS_CreateOppForECampaignM_Service {
       /**
    * @description Valida la creación de oportunidades a miembros de campañas
    * @author Gerardo Mendoza | 28/10/2020
    * @param Map<Id,CampaignMember> triggerNewMap, Map<Id,CampaignMember> triggerOldMap
    * @return void
    **/   
    public static void createOppFromCampMbrs(Map<Id,CampaignMember> triggerNewMap, Map<Id,CampaignMember> triggerOldMap) {
        Final Set<Id> setValidsIds = validsCampaignMembers(triggerNewMap, triggerOldMap);
        Final Map<String, CampaignMember> mapcampa = new Map<String, CampaignMember>();
        List<CampaignMember> lisCampaignMember = MX_RTL_CampaignMember_Selector.getLeadOpportunityInfoByIds('Id, MX_SB_PS_productoOfertado__c, Campaign.Id, Campaign.EndDate, MX_SB_PS_RefenciaCliente__c, Contact.AccountId,Contact.Account.Edad__c,Contact.Account.Genero__c,Contact.MailingState', new List<Id>(setValidsIds));
        FINAL Map<String,String[]> ListupdMember = prepareNameOpp(lisCampaignMember);
        for(CampaignMember tempcamp: lisCampaignMember) {
            if ('Y'.equals(ListupdMember.get(tempcamp.Id)[1])) {
                mapcampa.put(ListupdMember.get(tempcamp.Id)[0],tempcamp);
            }
        }
        if(!mapcampa.isEmpty()) {
            makeDataOpp(mapcampa);
        }
     }
      /**
    * @description Valida y agrega Ids
    * @author Gerardo Mendoza | 28/10/2020
    * @param Map<Id,CampaignMember> triggerNewMap, Map<Id,CampaignMember> triggerOldMap
    * @return returnIds
    **/ 
    public static Set<Id> validsCampaignMembers(Map<Id,CampaignMember> triggerNewMap, Map<Id,CampaignMember> triggerOldMap) {
        Final Set<Id> returnIds = new Set<Id>();
        Final Set<Id> allCampMbrs = triggerNewMap.keySet();
        for (Id oIdCampMbrs : allCampMbrs) {
            if (triggerNewMap.get(oIdCampMbrs).MX_SB_PS_productoOfertado__c !='' && triggerNewMap.get(oIdCampMbrs).MX_SB_PS_productoOfertado__c !=null &&
            triggerNewMap.get(oIdCampMbrs).MX_SB_PS_RefenciaCliente__c !='' && triggerNewMap.get(oIdCampMbrs).MX_SB_PS_RefenciaCliente__c !=null) {
                   returnIds.add(oIdCampMbrs);
               }
        }
        return returnIds;
    }
      /**
    * @description Valida si se crea una oportunidad o no
    * @author Gerardo Mendoza | 28/10/2020
    * @param String cadena
    * @return String[]
    **/ 
    public static Map<String,String[]> prepareNameOpp(List<CampaignMember> tempcampaina) {
       final Map<String,String[]> retmap = new Map<String,String[]>();
       String upd = '';
       String stringNew='';
        for(CampaignMember temforcamp: tempcampaina) {
            upd ='Y';
        	stringNew = temforcamp.MX_SB_PS_RefenciaCliente__c;
            if(String.isNotBlank(stringNew) && temforcamp.MX_SB_PS_RefenciaCliente__c.contains('TMK')) {
                stringNew = stringNew.substring(0, stringNew.length()-3);
                upd = 'N';
            }
            retmap.put(temforcamp.Id,new String[]{stringNew, upd});
         }
       return retmap;
    }
 	/**
    * @description Valida si se crea una oportunidad o no
    * @author Gerardo Mendoza | 28/10/2020
    * @param Map<String, CampaignMember> mapOppIds
    * @return void
    **/     
    public static void makeDataOpp(Map<String, CampaignMember> mapOppIds) {
        List<Opportunity> oppList = new List<Opportunity>();
        List<CampaignMember> updMemberList = new List<CampaignMember>();
        String devName = 'MX_SB_COB_Cotizador_RS_Hospital';
        Map<String,Opportunity> mapOpps = new Map<String,Opportunity>();
        Final Id IdRecordType = MX_RTL_CampaignMember_Selector.getIdRecordTypeParam(devName);
        for(Opportunity oppte: MX_RTL_Opportunity_Selector.resultQueryOppo('Id, Name', 'RecordTypeId='+'\''+ IdRecordType +'\' AND Name IN: iynList ', mapOppIds.keySet())) {
            mapOpps.put(oppte.Name, oppte);
        }
        for(String opptem: mapOppIds.keySet()) {
            if(!mapOpps.containsKey(opptem)) {
                Opportunity opp = new Opportunity(
                    CloseDate =  mapOppIds.get(opptem).Campaign.EndDate
                    , CampaignId = mapOppIds.get(opptem).Campaign.Id
                    , Name = opptem
                    , StageName = System.label.MX_SB_PS_Etapa1
                    , AccountId = mapOppIds.get(opptem).Contact.AccountId
                    , EstadoBenef__c = mapOppIds.get(opptem).Contact.MailingState
                    , EdadBeneficiario__c = mapOppIds.get(opptem).Contact.Account.Edad__c
                    , Sexo_conductor__c=mapOppIds.get(opptem).Contact.Account.Genero__c
                    , LeadSource = System.label.MX_SB_VTS_OUTBOUND_RT_LBL
                    , Plan__c = System.label.MX_SB_PS_Plan
                    , Producto__c = nameProductOpp(mapOppIds.get(opptem).MX_SB_PS_productoOfertado__c)
                    , RecordTypeId  = IdRecordType
                );
                oppList.add(opp);
                CampaignMember memberCollection = new CampaignMember(
                    Id = mapOppIds.get(opptem).Id
                    , MX_SB_PS_RefenciaCliente__c = opptem + 'TMK'
                );
                updMemberList.add(memberCollection);
            }
        }
        if(!oppList.isEmpty()) {
            MX_RTL_Opportunity_Selector.upsrtOppList(oppList);
            MX_RTL_CampaignMember_Selector.updCampMemberList(updMemberList);
        }
    }
    /**
    * @description 
    * @author Gerardo Mendoza Aguilar | 01-11-2021
    * @param nameOpp 
    * @return String 
    **/
    public static String nameProductOpp(String nameOpp) {
        String nameOppParam = '';
        switch on nameOpp {
            when  'RSPH' {
                nameOppParam = 'Respaldo Seguro Para Hospitalización';
            }
            when 'GF' {
                nameOppParam = 'Gastos Funerarios';
            }
        }
        return nameOppParam;
    }
}