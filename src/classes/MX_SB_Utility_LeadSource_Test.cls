/**
 * @File Name          : MX_SB_Utility_LeadSource_Test.cls
 * @Description        : 
 * @Author             : Arsenio.perez.lopez.contractor@bbva.com
 * @Group              : 
 * @Last Modified By   : Arsenio.perez.lopez.contractor@bbva.com
 * @Last Modified On   : 7/11/2019 11:23:48
 * @Modification Log   : 
 * Ver       Date            Author      		    				Modification
 * 1.0    7/11/2019   Arsenio.perez.lopez.contractor@bbva.com     Initial Version
**/
@isTest
private class MX_SB_Utility_LeadSource_Test {

	@testSetup static void setup() {
		final user admintest = MX_WB_TestData_cls.crearUsuario('Admin Creacion', system.label.MX_SB_VTS_ProfileAdmin);
        admintest.FederationIdentifier='MX000019';
		insert admintest;
		System.runAs(admintest) {
			MX_WB_TestData_cls.createStandardPriceBook2();
            final Product2 producto = MX_WB_TestData_cls.productNew('Hogar');
            producto.isActive = true;
            Insert producto;
            final PricebookEntry pbe = MX_WB_TestData_cls.priceBookEntryNew(producto.Id);
            Insert pbe;
			final Account cuenta1 = MX_WB_TestData_cls.crearCuenta('Cuenta de prueba','PersonAccount');
            cuenta1.MX_SB_BCT_Id_Cliente_Banquero__c ='121';
			insert cuenta1;
            final Opportunity opp = MX_WB_TestData_cls.crearOportunidad('OppTest Hogar', cuenta1.Id, admintest.Id, 'ASD');
            opp.Producto__c = 'Hogar';
            opp.Reason__c = 'Venta';
            opp.LeadSource = 'Call me back';
            opp.MX_SB_VTS_Aplica_Cierre__c = true;
            Insert opp;
            final MX_SB_VTS_Generica__c generica = MX_WB_TestData_cls.GeneraGenerica('MX_SB_BCT_user_Banquero', 'CP5');
            generica.MX_SB_BCT_user_Banquero_Default__c=admintest.FederationIdentifier;
            insert generica;
            final MX_SB_VTS_Generica__c generica2 = MX_WB_TestData_cls.GeneraGenerica('MX_SB_BCT_Baja_Banquero_Default', 'CP5');
            generica2.MX_SB_BCT_user_Banquero_Default__c=admintest.FederationIdentifier;
            insert generica2;
			final Banquero__c banquerocObj = new Banquero__c (
                MX_SB_BCT_Id_Cliente__c='121',
                MX_SB_BCT_Id_Banquero__c=admintest.FederationIdentifier,
				MX_SB_BCT_Suspendido__c = False
			);
			insert banquerocObj;
		}
	}
    
	@isTest static void testMethod1() {
        Test.startTest();
		final Opportunity tempopp = [select id,AccountID,leadsource from Opportunity where name = 'OppTest Hogar'];
		final Opportunity ret= MX_SB_Utility_LeadSource_cls.ObtenerLeadSource(tempopp);
        system.assertEquals(ret.LeadSource,'Call me Back ASD Banquero','call correcto');
        test.stopTest();
	}
    @isTest static void testMethod2Leadnull() {
        Test.startTest();
        final Opportunity tempopp = [select id,AccountID,leadsource from Opportunity where name = 'OppTest Hogar'];
        tempopp.LeadSource ='';
		final Opportunity ret= MX_SB_Utility_LeadSource_cls.ObtenerLeadSource(tempopp);
        system.assertEquals(ret.LeadSource,'Call me Back ASD Banquero','mensaje correcto');
        Test.stopTest();
	}
    @isTest static void testMethod3LeadTracking() {
        Test.startTest();
		final Opportunity tempopp = [select id,AccountID,leadsource from Opportunity where name = 'OppTest Hogar'];
        tempopp.LeadSource ='Tracking Web';
		final Opportunity ret= MX_SB_Utility_LeadSource_cls.ObtenerLeadSource(tempopp);
        system.assertEquals(ret.LeadSource,'Tracking Web ASD Banquero','Tracking correcto');
        Test.stopTest();
	}
    @isTest static void testMethod4Leadother() {
		Test.startTest();
        final Opportunity tempopp = [select id,AccountID,leadsource from Opportunity where name = 'OppTest Hogar'];
        tempopp.LeadSource ='Facebook';
		final Opportunity ret=MX_SB_Utility_LeadSource_cls.ObtenerLeadSource(tempopp);
        system.assertEquals(ret.LeadSource,'Facebook','facebook correcto');
        TEst.stopTest();
	}

}