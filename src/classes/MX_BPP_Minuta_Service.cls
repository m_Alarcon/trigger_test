/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Minuta_Service
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-10-06
* @Description 	BPyP Minuta Service Layer
* @Changes      Date		|		Author		|		Description
* 			2021-19-02			Héctor Saldaña	  New method added "recolectarTags()" which functionality
* 		                                          retrieves parsed results from MX_BPP_Minuta_Service_Helper
* 		                                          into each of the inner objects from MX_BPP_MinutaWrapper
* 		    2021-05-03          Héctor Saldaña    Query field in getCatalogoMinuta removed.
*           2021-26-03          Héctor Saldaña    Query field updated in getCatalogoMinuta methods
*/
public with sharing class MX_BPP_Minuta_Service {

	/* Field attach file object name */
    static final String FIELDATTACHFILE = 'dwp_kitv__Attach_file__c';

   	/* Constructor Method */
    private MX_BPP_Minuta_Service() {}

    /**
    * @Description 	Validate if the client email is the righ one to mark the record as 'enviado al cliente'
    * @Params		visita dwp_kitv__Visit__c, email Messaging.SingleEmailMessage
    * @Return 		NA
    **/
    public static void checkClientEmail (dwp_kitv__Visit__c visita, Messaging.SingleEmailMessage email) {
        final List<String> toAllAddresses = new List<String>();
        Set<String> setReceivers;
        if(email.getToAddresses() <> null) {
        	toAllAddresses.addAll(email.getToAddresses());
        }
        if(email.getCcAddresses() <> null) {
            toAllAddresses.addAll(email.getCcAddresses());
        }
        if(email.getBccAddresses() <> null) {
        	toAllAddresses.addAll(email.getBccAddresses());
        }

        setReceivers = new Set<String>(toAllAddresses);
        if (MX_BPP_Minuta_Service_Helper.checkIfClientIsIncluded(setReceivers, visita.dwp_kitv__account_id__c)) {
            MX_BPP_Minuta_Service_Helper.checkVisitedClientIndicator(visita);
        }

    }

    /**
	* @Description Method that retrieves the Visit details based on the Id received
	* @param recordId Visit's Id
    * @return dwp_kitv__Visit__c The visit record details
    **/
    public static dwp_kitv__Visit__c getRecordInfo(String recordId) {
        final Set<Id> visitsId = new Set<Id>();
        String queryfields = 'Id,  Name, dwp_kitv__visit_status_type__c, MX_PYME_ObjetivoBPyP__c, dwp_kitv__account_type__c,RecordType.Name, RecordType.DeveloperName, dwp_kitv__account_id__r.Id, dwp_kitv__visit_start_date__c, Owner.Name, Check_In_Date_Time__c, dwp_kitv__account_id__r.Name, dwp_kitv__disclosure_info_type__c,';
        queryfields += '(SELECT Id, Name, dwp_kitv__contact_id__r.Name, dwp_kitv__contact_id__r.Account.Name from dwp_kitv__Visit_Contacts__r WHERE dwp_kitv__Main_contact__c = true)';
        visitsId.add(recordId);
        final List <dwp_kitv__Visit__c> listVisits = Dwp_kitv_Visit_Selector.getVisitInfoByIds(queryfields, visitsId);

        return listVisits[0];
    }

    /**
	* @Description Method that retrieves Tipo Minuta Metadata info according to Visit combo fields
	* @param dwp_kitv__Visit__c currentVisit
    * @return String Visualforce Name
    **/
    public static List<MX_BPP_TipoMinuta__mdt> getMinutaMetadata(dwp_kitv__Visit__c currentVisit) {
        String queryFields;
        final List<String> listObjetivos = new List<String>();
        final String objetivoVisita = currentVisit.MX_PYME_ObjetivoBPyP__c;
        queryFields = 'ID, DeveloperName, MX_NombreVF__c, MX_ObjetivoVisita__c';
        listObjetivos.add(objetivoVisita);
        
        return MX_BPP_TipoMinuta_Selector.getRecordsByObjetivo(queryFields, listObjetivos);
    }

    /**
	* @Description Method that retrieves 'Catalogo Minuta' record correspondan to the current visit
	* @param ownerId Id from the visit's owner
    * @return String Visualforce Name
    **/
    public static List<MX_BPP_CatalogoMinutas__c> getCatalogoMinuta(String tipoCatalago) {
        final List<String> lstCatalogo = new List<String>();
        lstCatalogo.add(tipoCatalago);
        
        return MX_RTL_CatalogoMinuta_Selector.getRecordsByTipoVisita(lstCatalogo);
    }

    /**
	* @Description Method that validates if the visit recod has enough agreements
	* @param visitId
    * @return Boolean
    **/
    public static Boolean checkNumOfAgreements(Id visitId, Integer maxAgreements) {
        final Set<Id> idSetVisit = new Set<Id>();
        idSetVisit.add(visitId);
        final List<Task> taskResult = MX_RTL_Task_Selector.getTaskDataById(idSetVisit);
        return taskResult.size() >= maxAgreements ? true: false;
    }

    /**
	* @Description Method that retrieves the components needed to be displayed into the LWC based on the
	*              tags set in "MX_Opcionales__c" field
	* @param catalogo MX_BPP_CatalogoMinutas__c
	*        componentes MX_BPP_MinutaWrapper
    * @return void
    **/
    public static void recolectarTags(MX_BPP_CatalogoMinutas__c catalogo, MX_BPP_MinutaWrapper componentes) {
        if(catalogo.MX_HasPicklist__c) {
            componentes.contenedorCmps.cmpPicklist = MX_BPP_Minuta_Service_Helper.parsearTagsSeleccion(catalogo.MX_Opcionales__c);
        }
        if(catalogo.MX_HasText__c) {
            componentes.contenedorCmps.cmpText = MX_BPP_Minuta_Service_Helper.parsearTagsTexto(catalogo.MX_Opcionales__c);
        }
        if(catalogo.MX_HasCheckbox__c) {
            componentes.contenedorCmps.cmpSwitch = MX_BPP_Minuta_Service_Helper.parseoTagsSwitch(catalogo.MX_Opcionales__c);
        }

    }
}