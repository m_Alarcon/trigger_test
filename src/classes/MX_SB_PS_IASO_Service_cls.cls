/*
----------------------------------------------------------
* Nombre: MX_SB_PS_IASO_Service_cls
* Daniel Perez Lopez
* Proyecto: Salesforce-Presuscritos
* Descripción : clase controladora de front System.Label.MX_SB_PS_Etapa1.

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   Descripción
* --------------------------------------------------------------------------------
* 1.1           07/08/2020     Daniel Perez             Creación
* --------------------------------------------------------------------------------
*/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_SB_PS_IASO_Service_cls {
    /*
    * @description obtiene respuesta de un servicio
    * @param String Service, String params
    * @return HttpResponse
    */
    public static HttpResponse getServiceResponse(String service, String params) {
        return iaso.GBL_Integration_GenericService.invoke(service,params);
    }

    /*
    * @description obtiene respuesta de un servicio
    * @param String Service, Map<String,String> params
    * @return HttpResponse
    */
    public static HttpResponse getServiceResponseMap(String service, Map<String,String> params) {
        return iaso.GBL_Integration_GenericService.invoke(service,params);
    }
}