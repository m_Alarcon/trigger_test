/**
 * @File Name          : MX_SB_VTS_GetSetAlianzas_RP_Service.cls
 * @Description        : Realiza el consumo del servicio listCarInsuranceCatalogs
 * @Author             : Alexandro Corzo
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 13/10/2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0       13/10/2020      Alexandro Corzo        Initial Version
 * 1.1       12/01/2021      Alexandro Corzo        Se realizan ajustes para consumo ASO
**/
@SuppressWarnings('sf:UseSingleton')
public class MX_SB_VTS_GetSetAlianzas_PP_Service {
    /**
     * Atributos de la Clase
     */
    private static final Integer OKCODE = 200;
    
	/**
     * @description: Realiza el consumo del servicio listCarInsuranceCatalogs 
     * 				 para obtener: Planes
     * @author: 	 Alexandro Corzo
     * @return: 	 Map<String, Object> mReturnValues
     */
    public static Map<String, Object> obtListCarInsCatSrv(String sManagmenUnit, String sProductCode) {
        final Map<String, Object> mReturnValues = new Map<String, Object>();
        final Map<String, Object> sParamsSrv = new Map<String, Object>{'TYPE' => 'PP', 'ManagmentUnit' => sManagmenUnit, 'ProductCode' => sProductCode};
        final HttpResponse objRespSrv = MX_RTL_IasoServicesInvoke_Selector.callServices('getListCarInsuranceCatalogs', sParamsSrv, obtRequestSrv(sManagmenUnit,sProductCode));
        if(objRespSrv.getStatusCode() ==  OKCODE) {
            final List<Object> lstDetails = new List<Object>();
       		final MX_SB_VTS_wrpServiciosAlianzasPP objWrapper = (MX_SB_VTS_wrpServiciosAlianzasPP) JSON.deserialize(objRespSrv.getBody(), MX_SB_VTS_wrpServiciosAlianzasPP.class);
        	for(MX_SB_VTS_wrpServiciosAlianzasPP.productPlan oItemRow : objWrapper.iCatalogItem.productPlan) {
            	final Map<String, Object> objData = new Map<String, Object>();
            	objData.put('catalogItemBase_id', oItemRow.catalogItemBase.id);
            	objData.put('catalogItemBase_name', oItemRow.catalogItemBase.name);
            	objData.put('planReview', oItemRow.planReview);
            	objData.put('bouquetCode', oItemRow.bouquetCode);
                lstDetails.add(objData);
        	}
            final Map<String, Object> mStatusCode = new Map<String, Object>{'code' => String.valueOf(OKCODE), 'description' => 'Consumo de Servicio Exitoso!'};
            mReturnValues.put('oData', lstDetails);    
            mReturnValues.put('oResponse', mStatusCode);
        } else {
            mReturnValues.put('oResponse', MX_SB_VTS_Codes_Utils.statusCodes(objRespSrv.getStatusCode()));
        }           
        return mReturnValues;
    }

    /**
     * @description : Realiza el armado del objeto Request para el consumo del servicio ASO
     * @author      : Alexandro Corzo
     * @return      : HttpRequest oRequest
     */
    public static HttpRequest obtRequestSrv(String sManagmenUnit, String sProductCode) {
        final Map<String,iaso__GBL_Rest_Services_Url__c> allCodASOPP = iaso__GBL_Rest_Services_Url__c.getAll();
        final iaso__GBL_Rest_Services_Url__c objASOPP = allCodASOPP.get('getListCarInsuranceCatalogs');
        final String strEndPPP = objASOPP.iaso__Url__c;
        final HttpRequest oRqstPP = new HttpRequest();
        oRqstPP.setTimeout(120000);
        oRqstPP.setMethod('POST');
        oRqstPP.setHeader('Content-Type', 'application/json');
        oRqstPP.setHeader('Accept', '*/*');
        oRqstPP.setHeader('Host', 'https://test-sf.bbva.mx');
        oRqstPP.setEndpoint(strEndPPP);
        oRqstPP.setBody(obtBodyService(sManagmenUnit, sProductCode));
        return oRqstPP;
    }
    
    /**
     * @description : Realiza el armado del Body que sera ocupado en el Request del Servicio
     * @author      : Alexandro Corzo
     * @return      : String sBodyService
     */
    public static String obtBodyService(String sManagmenUnit, String sProductCode) {
    	String sBodySrvPP = '';
        sBodySrvPP += '{';
		sBodySrvPP += '"header": {';
        sBodySrvPP += '"aapType": "10000137",';
        sBodySrvPP += '"dateRequest": "2017-10-03 12:33:27.104",';
        sBodySrvPP += '"channel": "4",';
        sBodySrvPP += '"subChannel": "26",';
        sBodySrvPP += '"branchOffice": "INCE",';
        sBodySrvPP += '"managementUnit": "'+sManagmenUnit+'",';
        sBodySrvPP += '"user": "MARCO",';
        sBodySrvPP += '"idSession": "3232-3232",';
        sBodySrvPP += '"idRequest": "1212-121212-12121-212",';
        sBodySrvPP += '"dateConsumerInvocation": "2017-10-03 12:33:27.104"';
        sBodySrvPP += '},';
        sBodySrvPP += '"productCode": "'+sProductCode+'"';
        sBodySrvPP += '}';
        return sBodySrvPP;
    }
}