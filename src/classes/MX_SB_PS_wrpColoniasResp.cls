/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpColoniasResp
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020                 Creación de la wrapper.*/
 @SuppressWarnings('sf:ShortVariable,sf:ShortClassName')
public class  MX_SB_PS_wrpColoniasResp {
      /*Public property for wrapper*/
	public iCatalogItem iCatalogItem {get;set;}
	 /*public constructor subclass*/
		public MX_SB_PS_wrpColoniasResp() {
		 this.iCatalogItem = new iCatalogItem();
		}
	  /*Public subclass for wrapper*/
	public class iCatalogItem {
	      /*Public property for wrapper*/
		public suburb[] suburb {get;set;}
		 /*public constructor subclass*/
		public iCatalogItem() {
		 this.suburb = new suburb[] {};
		 this.suburb.add(new suburb());
		}
	}
	  /*Public subclass for wrapper*/
	public class suburb {
	      /*Public property for wrapper*/
		public city city {get;set;}
		  /*Public property for wrapper*/
		public county county {get;set;}
		  /*Public property for wrapper*/
		public neighborhood neighborhood {get;set;}
		  /*Public property for wrapper*/
		public state state {get;set;}
		 /*public constructor subclass*/
		public suburb() {
		 this.city = new city();
		 this.county = new county();
		 this.neighborhood = new neighborhood();
		 this.state = new state();
		}
	}
	  /*Public subclass for wrapper*/
	public class city {
	      /*Public property for wrapper*/
		public String name {get;set;}
           /*Public property for wrapper*/		
		 public string id {get;set;} 
         /*public constructor subclass*/
		public city() {
		 this.name =  'CDMX';
		 this.id = '';
		}		
	}
	  /*Public subclass for wrapper*/
	public class county {
	  /*Public property for wrapper*/
		public String name {get;set;}
	    /*Public property for wrapper*/ 	
		 public string id {get;set;} 
		 /*public constructor subclass*/
		public county() {
		 this.name =  '';
		 this.id = '';
		}	
	}
	  /*Public subclass for wrapper*/
	public class neighborhood {
	     /*Public property for wrapper*/
		public String name {get;set;}	
		  /*Public property for wrapper*/
		 public string id {get;set;} 
		 /*public constructor subclass*/
		public neighborhood() {
		 this.name =  '';
		 this.id = '';
		}	
	}
	  /*Public subclass for wrapper*/
	public class state {
	  /*Public property for wrapper*/
		public String name {get;set;}
       /*Public property for wrapper*/ 		
		 public string id {get;set;} 
        /*public constructor subclass*/
		public state() {
		 this.name =  '';
		 this.id = '';
		}	 		
	}
 }