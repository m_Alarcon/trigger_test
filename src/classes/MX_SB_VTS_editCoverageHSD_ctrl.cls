/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 01-13-2021
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   01-13-2021   Eduardo Hernández Cuamatzi   Initial Version
 * 1.1   09-03-2021   Eduardo Hernández Cuamatzi   Se agregan consultas de datos particulares
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_editCoverageHSD_ctrl {
    /**
    * @description Edita datos particulares y coberturas afectadas
    * @author Eduardo Hernández Cuamatzi | 01-13-2021 
    * @param oppId Id de Oportunidad
    * @param lstAfectCovers Lista de coberturas afectadas
    * @param valuesCriterial Lista de datos particulares
    * @return Map<String, Object> Mapa de objetos afectados
    **/
    @AuraEnabled
    public static Map<String, Object> updateDynamicList(String oppId, List<String> lstAfectCovers, List<String> valuesCriterial, List<String> lstPartDat) {
        final Map<String, Object> response = new Map<String, Object>();
        try {
            final Set<String> setOpps = new Set<String>{oppId};
            final List<Opportunity> lstOpps = MX_SB_VTS_CotizInitData_Service.findOppsById(setOpps);
            final Map<Id, Quote> mapQuotes = MX_SB_VTS_CotizInitData_Service.findQuotesByNameId(lstOpps[0].SyncedQuoteId);
            final Map<Id, List<Cobertura__c>> mapCrits = MX_SB_VTS_DeleteCoverageHSD_Services.findContractingCriteria(mapQuotes, 'MX_SB_VTS_ContractingCriteria');
            final List<Cobertura__c> mapCoverages = MX_SB_VTS_DeleteCoverageHSD_Helper.findCoverCodes(lstOpps[0].SyncedQuoteId, lstAfectCovers);
            final MX_SB_VTS_editCoverageHSD_Services.WrapEditPorc wrapData = new MX_SB_VTS_editCoverageHSD_Services.WrapEditPorc();
            wrapData.lstCover = mapCoverages;
            wrapData.lstCrit = mapCrits.get(lstOpps[0].SyncedQuoteId);
            wrapData.lstCriterials = valuesCriterial;
            wrapData.lstValues = lstPartDat;
            wrapData.oppData = lstOpps[0];
            final Map<String, Object> bodyEditCovers = MX_SB_VTS_editCoverageHSD_Services.fillEditPorc(wrapData);
            final Map<String, Object> responServices = MX_SB_VTS_DeleteCoverageHSD_Services.processCoverages(JSON.serialize(bodyEditCovers), mapQuotes.get(lstOpps[0].SyncedQuoteId).MX_SB_VTS_ASO_FolioCot__c);
            response.put('isOk', true);
            if((Boolean)responServices.get('responseOk')) {
                final Map<String,Object> responseServ = (Map<String, Object>)Json.deserializeUntyped((String)responServices.get('getData'));
                response.put('dataPayments', MX_SB_VTS_DeleteCoverageHSD_Services.processResponse(JSON.serialize(responseServ.get('data')), lstOpps[0].SyncedQuoteId));
                MX_SB_VTS_editCoverageHSD_Services.updateNewValues(wrapData.lstCrit, wrapData);
                MX_SB_VTS_editCoverageHSD_Services.updateNewValuesCov(wrapData.lstCover, wrapData);
                MX_SB_VTS_GetSetDatosPricesSrv_Helper.fillUpdtDCov((String)responServices.get('getData'), lstOpps[0].SyncedQuoteId);
            }
            response.put('dataCot', MX_SB_VTS_DeleteCoverageHSD_ctrl.fillCotizData(oppId));
            final Map<Id, List<Cobertura__c>> mapAllCrits = MX_SB_VTS_DeleteCoverageHSD_Services.findContractingCriteria(mapQuotes, 'MX_SB_VTS_ContractingCriteria');
            final Map<Id, List<Cobertura__c>> mapAllCoverages = MX_SB_VTS_DeleteCoverageHSD_Services.findContractingCriteria(mapQuotes, 'MX_SB_VTS_CoberturasASO');
            response.put('criterials', MX_SB_VTS_DeleteCoverageHSD_Helper.fillMapCritical(MX_SB_VTS_DeleteCoverageHSD_ctrl.evaluateList(mapAllCrits)));
            response.put('coverages', MX_SB_VTS_FillCoveragesTrades_helper.fillCoverages(MX_SB_VTS_DeleteCoverageHSD_ctrl.evaluateList(mapAllCoverages)));
        } catch (QueryException ex) {
            response.put('isOk', false);
            response.put('messageError', ex.getMessage());
        }
        return response;
    }
}