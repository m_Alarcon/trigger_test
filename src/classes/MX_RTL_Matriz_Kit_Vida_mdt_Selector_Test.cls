@isTest
/**
 * @File Name          : MX_RTL_Matriz_Kit_Vida_mdt_Selector_Test.cls
 * @Description        : Methodos que prueban la clase MX_RTL_Matriz_Kit_Vida_mdt_Selector
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 02/06/2020, 12:42:42 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/26/2020   Juan Carlos Benitez         Initial Version
**/
public class MX_RTL_Matriz_Kit_Vida_mdt_Selector_Test {
	/* var String Valor SUBRAMO	*/
    public static final String SUBRAMO = 'Fallecimiento';
    /* var String Valor PRODUCTO	*/
    public static final String PRODUCTO ='Pyman';
    /* var String Valor SUBTIPO	*/
    public static final String SUBTIPO ='Natural';
    /* var String Valor SUBTIPO	*/
    public static final String REGLA1 ='menos de 2.5 mdp suma asegurada';
    /* var String Valor SUBTIPO	*/
    public static final String REGLA2 ='18 a 55 años';
    
    @isTest
    static void testErrorgetKitDataById() {
    final Map<String,String> mapa= new Map<String, String>();
        mapa.put('product',PRODUCTO);
        mapa.put('subramo',SUBRAMO);
        mapa.put('subtipo',SUBTIPO);
        mapa.put('regla1',REGLA1);
        mapa.put('regla2',REGLA2);
        test.startTest();
        try {
        	MX_RTL_Matriz_Kit_Vida_mdt_Selector.getKitDataById(mapa);
        } catch(QueryException qex) {
            system.assert(qex.getMessage().contains('no viable alternative at character'),'Test de error Exitoso');
        }
        test.stopTest();
    }
     @isTest
    static void testgetKitDataById() {
    	final Map<String,String> mapa= new Map<String, String>();
        mapa.put('product',PRODUCTO);
        mapa.put('subramo',SUBRAMO);
        mapa.put('subtipo',SUBTIPO);
        mapa.put('regla1',REGLA1);
        mapa.put('regla2',REGLA2);
        test.startTest();
        	MX_RTL_Matriz_Kit_Vida_mdt_Selector.getKitDataById(mapa);
        test.stopTest();
        system.assert(true, 'Busqueda existosa');
    }
    @isTest
    static void testConstr() {
        Final MX_RTL_Matriz_Kit_Vida_mdt_Selector constrct = new MX_RTL_Matriz_Kit_Vida_mdt_Selector();
        system.assert(true,constrct);
    }

}