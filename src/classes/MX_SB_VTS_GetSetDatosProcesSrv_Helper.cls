/**
 * @File Name          : MX_SB_VTS_GetSetDatosProcesSrv_Helper.cls
 * @Description        : Clase Encargada de Proporcionar Soporte
 *                       a la clase: MX_SB_VTS_GetSetDatosPricesSrv_Service
 * @Author             : Alexandro Corzo
 * @Group              : 
 * @Last Modified By   : Alexandro Corzo
 * @Last Modified On   : 28/12/2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0       28/12/2020      Alexandro Corzo        Initial Version
 * 1.1       25/02/2021      Alexandro Corzo        Se agregan funciones para armado peticion ASO
 * 1.1.1     09/03/2021      Eduardo Hernández      Mejora para incendio y contenidos dinámico
**/
@SuppressWarnings('sf:UseSingleton')
public class MX_SB_VTS_GetSetDatosProcesSrv_Helper {
    /**
     * @description : Genera la sección Wrapper Datos Generales Petición ASO
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    public static MX_SB_VTS_wrpPricesDataSrv.base fillDataGen(Map<String, Object> mParamsM) {
        final String insuredAmount = String.valueOf(mParamsM.get('insuredAmount'));
        final String insuredPercentage = mParamsM.get('insuredPercentage').toString();
        final String coveredArea = mParamsM.get('coveredArea').toString();
        final List<Object> lstDatPart = (List<Object>) mParamsM.get('lstDatPart');
        final String strCPDP = mParamsM.get('postalcode').toString();
        final MX_SB_VTS_wrpPricesDataSrv.base objWrapperGen = new MX_SB_VTS_wrpPricesDataSrv.base();
        objWrapperGen.allianceCode = 'DHSDT003';
        objWrapperGen.insuredAmount = fillDataInsAmo(insuredAmount);
        objWrapperGen.insuredPercentage = insuredPercentage;
        objWrapperGen.coveredArea = coveredArea;
        objWrapperGen.certifiedNumberRequested = '1';
        objWrapperGen.zipCode = strCPDP;
        objWrapperGen.product =  fillDataProd();
        objWrapperGen.validityPeriod = fillDataValPeriod();
        objWrapperGen.region = 'VCIP';
        objWrapperGen.frequencies = fillDataFrecc();
        objWrapperGen.contractingCriteria = fillDataConCrit(mParamsM,lstDatPart);
        objWrapperGen.trades = fillDataTrades(mParamsM, lstDatPart);
        return objWrapperGen;
    }
    
    /**
     * @description : Genera la sección Wrapper Insured Amount Petición ASO
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    public static MX_SB_VTS_wrpPricesDataSrv.insuredAmount fillDataInsAmo(String strSumAse) {
        final MX_SB_VTS_wrpPricesDataSrv.insuredAmount objWrapper = new MX_SB_VTS_wrpPricesDataSrv.insuredAmount();
        objWrapper.amount = strSumAse;
        objWrapper.currencydata = 'MXN';
        return objWrapper;
    }
    
    /**
     * @description : Genera la sección Wrapper Data Prod Petición ASO
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    public static MX_SB_VTS_wrpPricesDataSrv.product fillDataProd() {
        final MX_SB_VTS_wrpPricesDataSrv.product objWrapper = new MX_SB_VTS_wrpPricesDataSrv.product();
        objWrapper.iddata = '3002';
        objWrapper.plan = fillDataPlan();
        objWrapper.trade = fillDataTrade();
        return objWrapper;
    }
    
    /**
     * @description : Genera la sección Wrapper Data Plan Petición ASO
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    public static MX_SB_VTS_wrpPricesDataSrv.plan fillDataPlan() {
        final MX_SB_VTS_wrpPricesDataSrv.plan objWrapper = new MX_SB_VTS_wrpPricesDataSrv.plan();
        objWrapper.iddata = '008';
        objWrapper.reviewCode = '001';
        return objWrapper;
    }
    
    /**
     * @description : Genera la sección Wrapper Data Trade Petición ASO
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    public static MX_SB_VTS_wrpPricesDataSrv.trade fillDataTrade() {
        final MX_SB_VTS_wrpPricesDataSrv.trade objWrapper = new MX_SB_VTS_wrpPricesDataSrv.trade();
        objWrapper.iddata = 'INCE';
        return objWrapper;
    }
    
    /**
     * @description : Genera la sección Wrapper Data Val Period Petición ASO
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    public static MX_SB_VTS_wrpPricesDataSrv.validityPeriod fillDataValPeriod() {
        final MX_SB_VTS_wrpPricesDataSrv.validityPeriod objWrapper = new MX_SB_VTS_wrpPricesDataSrv.validityPeriod();
        final Datetime dFechaActual = System.now();
        final Datetime dFechaFutura = ((Datetime) System.now()).addDays(365);
        final String strFechaActual = dFechaActual.format('yyyy-MM-dd HH:mm:ss').replaceAll(' ','T') + '.000Z';
        final String strFechaFutura = dFechaFutura.format('yyyy-MM-dd HH:mm:ss').replaceAll(' ','T') + '.000Z';
        objWrapper.startDate = strFechaActual;
        objWrapper.endDate = strFechaFutura;
        objWrapper.iddata = 'ANNUAL';
        return objWrapper;
    }
    
    /**
     * @description : Genera la sección Wrapper Data Frecc Petición ASO
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    public static List<MX_SB_VTS_wrpPricesDataSrv.frequencies> fillDataFrecc() {
        final List<MX_SB_VTS_wrpPricesDataSrv.frequencies> lstObjFrecc = new List<MX_SB_VTS_wrpPricesDataSrv.frequencies>();
		final MX_SB_VTS_wrpPricesDataSrv.frequencies objWrapper = new MX_SB_VTS_wrpPricesDataSrv.frequencies();
        objWrapper.iddata = 'YEARLY';
        lstObjFrecc.add(objWrapper);
        return lstObjFrecc;
    }
    
    /**
     * @description : Genera la sección Wrapper Data Con Crit Petición ASO
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    public static List<MX_SB_VTS_wrpPricesDataSrv.contractingCriteria> fillDataConCrit(Map<String, Object> mParamsM, List<Object> lstDatPartUpd) {
		Map<String, String> mDataDP = null;
        MX_SB_VTS_wrpPricesDataSrv.contractingCriteria objWrapper = null;
        final List<MX_SB_VTS_wrpPricesDataSrv.contractingCriteria> lstObjConCrit = new List<MX_SB_VTS_wrpPricesDataSrv.contractingCriteria>();
        final Map<String, Object> mDataCotiz = MX_SB_VTS_GetSetDatosProcessSrv_HelCFG.fillDataCotiz(mParamsM, lstDatPartUpd);
        final List<Object> lstDatPart = (List<Object>) mDataCotiz.get('DP');
        for(Object oRowDP : lstDatPart) {
            mDataDP = (Map<String,String>) oRowDP;
            objWrapper = new MX_SB_VTS_wrpPricesDataSrv.contractingCriteria();
            objWrapper.criterial = mDataDP.get('criterial');
            objWrapper.iddata = mDataDP.get('id_data');
            objWrapper.value = mDataDP.get('value');
            lstObjConCrit.add(objWrapper);
        }
        return lstObjConCrit;
    }
    
    /**
     * @description : Genera la sección Wrapper Data Trades Petición ASO
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    public static List<MX_SB_VTS_wrpPricesDataSrv.trades> fillDataTrades(Map<String, Object> mParamsM, List<Object> lstDatPartUpd) {
        final Map<String, Object> mDataCotiz = MX_SB_VTS_GetSetDatosProcessSrv_HelCFG.fillDataCotiz(mParamsM, lstDatPartUpd);
        final List<String> lstCovers = (List<String>) mDataCotiz.get('CO');
    	MX_SB_VTS_wrpPricesDataSrv.trades objWrapperTrades = null;
        MX_SB_VTS_wrpPricesDataSrv.categories objWrappperCat = null;
        MX_SB_VTS_wrpPricesDataSrv.goodTypes objWrapperGTypes = null;
        MX_SB_VTS_wrpPricesDataSrv.coverages objWrapperConv = null;
        List<MX_SB_VTS_wrpPricesDataSrv.coverages> lstObjCovers = null;
        List<MX_SB_VTS_wrpPricesDataSrv.goodTypes> lstObjGTypes = null;
        List<MX_SB_VTS_wrpPricesDataSrv.categories> lstObjCat = null;
        final List<MX_SB_VTS_wrpPricesDataSrv.trades> lstObjTrades = new List<MX_SB_VTS_wrpPricesDataSrv.trades>();
        for(String oRowCover : lstCovers) {
        	final String[] sTradeData = oRowCover.split('\\|');
            final String sTradeID = sTradeData[0];
			objWrapperTrades = new MX_SB_VTS_wrpPricesDataSrv.trades();
            objWrapperTrades.iddata = sTradeID;
            final String[] sCatData = sTradeData[1].split('\\#');
            lstObjCat = new List<MX_SB_VTS_wrpPricesDataSrv.categories>();
            for(String oRowCat : sCatData) {
                final String[] sCatDatDet = oRowCat.split('\\@');
                final String sCatID = sCatDatDet[0];
                objWrappperCat = new MX_SB_VTS_wrpPricesDataSrv.categories();
                objWrappperCat.iddata = sCatID;
                final String[] sGTData = sCatDatDet[1].split('\\$');
                lstObjGTypes = new List<MX_SB_VTS_wrpPricesDataSrv.goodTypes>();
                for(String oRowGT : sGTData) {
                    final String[] sGTDataDet = oRowGT.split('\\*');
                    final String sGTID = sGTDataDet[0];
                    objWrapperGTypes = new MX_SB_VTS_wrpPricesDataSrv.goodTypes();
                    objWrapperGTypes.iddata = sGTID;
                    final String[] sCovData = sGTDataDet[1].split('\\,');
                    lstObjCovers = new List<MX_SB_VTS_wrpPricesDataSrv.coverages>();
                    for(String oRowCov : sCovData) {
                    	final String sCovID = oRowCov;
                        objWrapperConv = new MX_SB_VTS_wrpPricesDataSrv.coverages();
                        objWrapperConv.iddata = sCovID;
                        lstObjCovers.add(objWrapperConv);
                    }
                    objWrapperGTypes.coverages = lstObjCovers;
                    lstObjGTypes.add(objWrapperGTypes);
                }
                objWrappperCat.goodTypes = lstObjGTypes;
                lstObjCat.add(objWrappperCat);
            }
            objWrapperTrades.categories = lstObjCat;
            lstObjTrades.add(objWrapperTrades);
        }
        return lstObjTrades;
    }
    
    /**
     * @description : Realiza conversión de TradeID a Codigo Clippert
     * @author      : Alexandro Corzo
     * @return      : String oReturnVal
     */
    public static String getCodeTradeById(String strTradeId) {
        String oReturnVal = '';
        switch on strTradeId {
            when 'FIRE' { oReturnVal = 'INCE'; }
            when 'EARTHQUAKE' { oReturnVal = 'TERR'; }
            when 'HYDROMETEOROLOGICAL_RISKS' { oReturnVal = 'RIHI'; }
            when 'CIVIL_LIABILITY_INSURANCE' { oReturnVal = 'RCPF'; }
            when 'MISCELLANEOUS' { oReturnVal = 'MISC'; }
            when 'TECHNICAL' { oReturnVal = 'TECN'; }
            when else { oReturnVal = 'ND'; }
        }
        return oReturnVal;
    }
}