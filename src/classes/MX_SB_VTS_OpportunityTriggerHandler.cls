/*  @author: Eduardo Hernández Cuamatzi
 *  @description: Opportunity Trigger handler
 *  @version: 1.0
 *
 *__________________________________________________________________________________________________
 * @version                Author                      date								description
	1.1					Jaime Terrats					20/06/2019 					Se agrega un nuevo funcion detonante.
    1.2                 Diego Olvera                    03/10/2019                  Se agrega funcion para accionar contador.
    1.3                 Francisco Javier                28/10/2019                  Se agrega funcion para validar el role del usuario.
    1.4 				Tania Vazquez   				03/01/2020		    		Se agrega función de evaluación para VTA
    1.4.1 				Eduardo Hernandez 				12/02/2020		    		Se agrega funcipon para toques sac
	1.5					Gabriel García					10/03/2020					Se agregan métodos de ejecución para el flujo de BPyP
    1.6                 Alexandro Corzo                 20/05/2020                  Se agrega método de ejecución para campo Clasificación de Solicitud
    1.7 				Edmundo Zacarías				21/05/2020	                Se agrega método de validación para BPyP
    1.8                 Alexandro Corzo                 17/08/2020                  Se mueve invocación de updateCalSolicitud a beforeInsert
    1.9                 Arsenio Perez                   15/12/2020                  Se agrega métHod after insert MX_SB_PS_OpportunityTrigger
    1.10				Alexandro Corzo					18/03/2021					Se mueve la invocación a la función: updateCalSolicitud a BeforeInsert
 */

public without sharing class MX_SB_VTS_OpportunityTriggerHandler extends TriggerHandler {
    /**Lista de Oportunidades*/
	private List<Opportunity> newOppList;

    /** Map con los RecordType de Oportunidades */
	final private map<Id, RecordType> mapRecordTypesOpp;
	/** Lista de Trigger.new */
	final private List<Opportunity> triggerNew;
	/** Map de Trigger.new */
	final private Map<Id,Opportunity> triggerNewMap;
	/** Map de Trigger.old */
	final private Map<Id,Opportunity> triggerOldMap;

    /**
     * MX_SB_VTS_OpportunityTriggerHandler constructor
     * @return   Constructor de clase
     */
    public MX_SB_VTS_OpportunityTriggerHandler() {
        this.newOppList = Trigger.new;

        this.mapRecordTypesOpp = new map<Id, RecordType>([Select Id, DeveloperName from RecordType where sObjectType='Opportunity']);
        this.triggerNew= (list<Opportunity>)(Trigger.new);
        this.triggerNewMap= (Map<Id,Opportunity>)(Trigger.newMap);
        this.triggerOldMap= (Map<Id,Opportunity>)(Trigger.oldMap);
    }

    /**
     * beforeInsert Override de before Insert
     */
    protected override void beforeInsert() {
        MX_SB_VTS_OpportunityTrigger.validateRole(triggerNew);
        MX_SB_VTS_CierreOpp.validNamesOppASD(newOppList);
		MX_SB_VTS_OpportunityTrigger.validaCuentaBanquero(newOppList);
		MX_SB_VTS_OpportunityTrigger.tipificacionInicialVTA(newOppList);

        /*Flujo BPyP*/
        if(Trigger_Oportunidad__c.getInstance().TriggerOportunidad_Master__c) {
            MX_RTL_OppProductForm_cls.bfInsertProd(triggerNew, mapRecordTypesOpp);
            EG009_OP_FinanciamientoPipeline_cls.updateRecordTypeOportunity(triggerNew, mapRecordTypesOpp);
            ContarOppsGrupo_cls.oppUsrTiggerTrig(triggerNew, triggerOldMap);
        }
        MX_SB_VTS_OpportunityTrigger.mayorAvanceTip(newOppList);
        MX_SB_VTS_OpportunityTrigger.updateCalSolicitud(newOppList);
    }

    /*
	 * Process before update logic
	 */
    protected override void beforeUpdate() {
        MX_SB_VTS_OpportunityTrigger.validateRole(triggerNew);
        MX_SB_VTS_CierreOpp.validaCierre(newOppList);
        MX_SB_VTS_OpportunityTrigger.addContactshOpp(triggerNew, triggerOldMap);
        MX_SB_VTS_OpportunityTrigger.substractContactTouchOpp(triggerNew, triggerOldMap);
        MX_SB_VTS_OpportunityTrigger.substractEffectiveContactOpp(triggerNew, triggerOldMap);
        MX_SB_VTS_OpportunityTrigger.substractNoEffectiveContactOpp(triggerNew, triggerOldMap);

        /*Flujo BPyP*/
        if(Trigger_Oportunidad__c.getInstance().TriggerOportunidad_Master__c) {
            ContarOppsGrupo_cls.oppUsrTiggerTrig(triggerNew, triggerOldMap);
            MX_RTL_OppProductForm_cls.bfUpdateProd(triggerNew, mapRecordTypesOpp);
        }

        MX_BPyP_Opportunity_Service.checkStatusUpdate(triggerNew, triggerOldMap);
        MX_SB_VTS_OpportunityTrigger.mayorAvanceTip(newOppList);
    }

    /*
	 * @afterInsert event override en la Clase TriggerHandler
	 * Logica Encargada de los Eventos AfterInsert
	*/
	protected override void afterInsert() {
        if(Trigger_Oportunidad__c.getInstance().TriggerOportunidad_Master__c) {
            MX_RTL_OppProductForm_cls.afInsertProd(triggerNew, mapRecordTypesOpp);
            MX_PYME_AccCampOpp_Cls.addOppToAccCampaign(triggerNewMap,null);
        }
        MX_SB_PS_OpportunityTrigger.createQLT(triggerNew);
    }

	/*
	 * @afterUpdate event override en la Clase TriggerHandler
	 * Logica Encargada de los Eventos afterUpdate
	*/
	protected override void afterUpdate() {
        if(Trigger_Oportunidad__c.getInstance().TriggerOportunidad_Master__c) {
            MX_PYME_AccCampOpp_Cls.addOppToAccCampaign(triggerNewMap,triggerOldMap);
        }
        MX_SB_VTS_OpportunityTrigger.updateVisit(newOppList);
    }
}