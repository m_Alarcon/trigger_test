/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RDB_Utilities
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2019-11-29
* @Group  		N/A
* @Description 	Class used to create records in Test Classes.
* @Changes
* 
*/
public with sharing class MX_RDB_Utilities {
	
    private MX_RDB_Utilities () {}
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description create CustomSetting for WS
    * @param nameWS String for the name, urlWS url for the WS
    * @return N/A
    **/
    public static void createWSRecord(String nameWS, String urlWS) {
        final iaso__GBL_Rest_Services_Url__c customSetting = new iaso__GBL_Rest_Services_Url__c();
        customSetting.Name = nameWS;
        customSetting.iaso__Url__c = urlWS;
        customSetting.iaso__Cache_Partition__c ='iaso.ServicesPartition';
        customSetting.iaso__Timeout__c = 20000;
        customSetting.iaso__cacheTTLInSecs__c = 300;
        customSetting.iaso__isCacheInOrg__c = true;
        insert customSetting;
    }
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description create Account record for RDB
    * @param accRecord Account record to be used as information to be sent
    * @return N/A
    **/
    public static void createAccountRec(String firstName, String lastName, String clientID) {
        final Id personRecordType = [SELECT Id  FROM RecordType WHERE DeveloperName = 'PersonAccount' AND SObjectType = 'Account' LIMIT 1].Id;
        final Account acc = new Account();
        acc.FirstName = firstName;
        acc.LastName = lastName;
        acc.PersonEmail = 'tst' + lastName + clientId + '@testbbva.com';
        acc.PersonMobilePhone = '5551040002';
        acc.PersonHomePhone = '5551040002';
        acc.MX_SB_BCT_Id_Cliente_Banquero__c = clientID;
        acc.RecordTypeId = personRecordType;
        insert acc;
    }
}