/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Account_Service
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-08-06
* @Description 	Service Layer for Account
* @Changes
*  
*/
public with sharing class MX_BPP_Account_Service {
    
    /** String RecordTypeDeveloperName Account */
    static final String RTACC = 'BPyP_tre_Cliente';
    
    /** String RecordTypeDeveloperName PersonAccount */
    static final String RTPACC = 'MX_BPP_PersonAcc_Client';
    
    /*Contructor clase MX_BPP_Account_Service */
    private MX_BPP_Account_Service() {}
    
    /**
    * @Description 	Check if owner where updated and update related
    * @Param		newAccList <Trigger.new>, oldAccMap <Trigger.oldMap>
    * @Return 		NA
    **/
    @SuppressWarnings('sf:DUDataflowAnomalyAnalysis')
    public static void ownerUpdate (List<Account> newAccList, Map<Id, Account> newAccMap, Map<Id, Account> oldAccMap) {
        final Id recordTypeIdAcc = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(RTACC).getRecordTypeId();
        final Id recordTypeIdPAcc = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(RTPACC).getRecordTypeId();
        final List<Account> updatedAccounts = new List<Account>();
        final Set<Id> updatedAccIDs = new set<Id>();
        
        for (Account newAcc : newAccList) {
            if ((newAcc.RecordTypeId.equals(recordTypeIdAcc) || newAcc.RecordTypeId.equals(recordTypeIdPAcc)) && newAcc.OwnerId != oldAccMap.get(newAcc.Id).OwnerId) {
                updatedAccounts.add(newAcc);
                updatedAccIDs.add(newAcc.Id);
            }
        }
        
        if(!updatedAccounts.isEmpty()) {
            MX_BPP_Account_Service_Helper.updateRelatedRecordsOwner(updatedAccounts, updatedAccIDs, newAccMap);
        }
    }

}