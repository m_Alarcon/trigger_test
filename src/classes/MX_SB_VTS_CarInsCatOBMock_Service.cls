/**
 * @description       : 
 * @author            : Diego Olvera
 * @group             : 
 * @last modified on  : 11-04-2020
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   10-23-2020   Diego Olvera   Initial Version
**/
@SuppressWarnings('sf:AvoidGlobalModifier')
global class MX_SB_VTS_CarInsCatOBMock_Service implements iaso.GBL_Integration_Headers {
     /**
     * @description: 	Realiza la configuración de consumo del servicio mock: getInsuranceContracts
     * 				 	el cual se encarga de consultar si el numero de poliza existe en Clippert.
     * @author: 		Diego Olvera
     * @modification:	Diego Olvera 
     * @return: 		HttpRequest
     */
    global HttpRequest modifyRequest (HttpRequest request) {
        final HttpResponse oResponseGT = iaso.GBL_Integration_GenericService.invoke('getGTServicesSF', '{}');
        final String sTsec = oResponseGT.getHeader('tsec');
        request.setMethod('GET');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Accept', '*/*');
        request.setHeader('Host', 'https://test-sf.bbva.mx');
        request.setHeader('tsec', sTsec);
        return request;
    }

}