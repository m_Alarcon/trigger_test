/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RTL_ContentVersion_Selector
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2021-01-14
* @Description 	Selector for ContentVersion Object
* @Changes      Date		|		Author		|		Description
* 			2021-03-26			Héctor Saldaña	  New methdos added: getContentDocLinkByLinkEntityId
* 		                                          getContentVersionByContentDocument,
* 		                                          getLibrariesByContentDocument, getContentDocumentById
*  
*/
public without sharing class MX_RTL_ContentVersion_Selector {
    /*Property to avoid String literal occurrence vulnerability*/
    final static String SELECT_STR = 'SELECT ';
    /** Constructor */
    @TestVisible
    private MX_RTL_ContentVersion_Selector() {}
    
    /**
    * @description
    * @author Edmundo Zacarias Gómez
    * @param String queryfields, List<Id> idsCampaigMembers
    * @return List<CampaignMember>
    **/
    public static List<ContentVersion> getContentVersionByIds(String queryfields, Set<Id> idsContentVersion) {
        return Database.query(SELECT_STR + String.escapeSingleQuotes(queryfields) + ' FROM ContentVersion WHERE Id in :idsContentVersion');
    }

    /**
    * @description Retrieves the records ContenDocumentLink related between a ContentVersion and its EntityId based on the LinkedEntityId from
     *             the ContentDocumentLink records.
    * @author Héctor Saldaña
    * @params String queryfields, Id linkEntityId
    * @return List<ContentDocumentLink>
    **/
    public static List<ContentDocumentLink> getContentDocLinkByLinkEntityId(Id linkEntityId) {
        String query;
        query = 'SELECT ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId =: linkEntityId';
        return Database.query(query);
    }

    /**
    * @description Retrieves ContentVersion records based on the ContentDocumentId specified in params as a  Set collection
    * @author Héctor Saldaña
    * @params String queryfields, Set<Id> idDocs
    * @return List<ContentVersion>
    **/
    public static List<ContentVersion> getContentVersionByContentDocument(Set<Id> idDocs) {
        String queryDocs;
        queryDocs = 'SELECT Id, FileType, Title, IsAssetEnabled, FileExtension, ContentDocument.CreatedBy.Name, ContentDocument.ContentSize, ContentDocumentId, ContentDocument.FileType FROM ContentVersion WHERE ContentDocumentId in :idDocs';
        return Database.query(queryDocs);
    }

    /**
    * @description Retrieves Libraries (ContentWorkspace) records related to a Set of ContentDocuments(s) record(s)
    * @author Héctor Saldaña
    * @params String queryfields, Set<Id> contentDocsIds
    * @return List<ContentWorkspace>
    **/
    public static List<ContentWorkspace> getLibrariesByContentDocument(Set<Id> contentDocsIds) {
        String queryFields;
        queryFields = 'SELECT Id, Name FROM ContentWorkspace WHERE Id IN (SELECT ParentId from ContentDocument WHERE ID IN: contentDocsIds)';
        return Database.query(queryFields);
    }

    /**
    * @description Retrieves ContentDocument(s) record(s) based on the Set of Ids received as parameter
    * @author Héctor Saldaña
    * @params String queryfields, Set<Id> docsIds
    * @return List<ContentDocument>
    **/
    public static List<ContentDocument> getContentDocumentById(Set<Id> docsIds) {
        String queryFields;
        queryFields = 'SELECT Id, Title, ParentId FROM ContentDocument WHERE Id in :docsIds';
        return Database.query(queryFields);
    }

}