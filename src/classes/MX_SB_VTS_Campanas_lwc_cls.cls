/*
  @FileName           : MX_SB_VTS_Campanas_lwc_cls.cls
  @Description        :
  @Author             : tania.vazquez.contractor@BBVA.com
  @Group              :
  @Last Modified By   : Diego Olvera
  @Last Modified On   : 20/2/2020 17:00:56
  @Modification Log   :
  @Group              : 
  @Last Modified By   : Eduardo Hernández Cuamatzi
  @Last Modified On   : 24/2/2020 18:47:53
  @Modification Log   : 
  Ver               Date                Author      		                Modification
  1.0           30/1/2020         tania.vazquez.contractor@BBVA.com         Initial Version
*/
public without sharing class MX_SB_VTS_Campanas_lwc_cls { //NOSONAR
    /*
    * @Method: getcampaigndata
    * @Description: get data about opportunity
    * @Params: oppId
    */
    @AuraEnabled(cacheable=true)
    public static List<SObject> getcampaigndata(String recordId) {
        final List<SObject> returnRecord = new List<SObject>();
        try {
            if(String.isNotBlank(recordId)) {
                final string typeObj = recordId.substring(0,3).toUpperCase();
                String obj;
                switch on typeObj {
                    when '006' {
                        obj = 'Opportunity';
                    }
                    when else {
                        obj ='Lead';
                    }
                }
                returnRecord.addAll(Database.query(string.escapeSingleQuotes('select MX_SB_VTS_CampaFaceName__c, MX_SB_VTS_CodCampaFace__c, MX_SB_VTS_DetailCampaFace__c, id from ' + obj + ' where id=: recordId')));
            }
        } catch(QueryException qEx) {
            throw new AuraHandledException(System.Label.MX_SB_SAC_ErrorBack + qEx);
        }
        return returnRecord;
    }
}