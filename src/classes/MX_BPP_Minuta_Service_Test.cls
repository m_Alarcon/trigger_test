/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Minuta_Service_Test
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-10-06
* @Description 	Test Class for BPyP Minuta Service Layer
* @Changes      Date		|		Author		|		Description
 * 			2021-02-22			Héctor Saldaña	  Added new test method: recolectarTagsTest
*           2021-08-03          Héctor Saldaña    New fields added in setup() as per required
*/
@isTest
public class MX_BPP_Minuta_Service_Test {
    
    /** BPYP Account RecordType */
    static final String STR_RT_BPYPPACC = 'MX_BPP_PersonAcc_Client';
    
    /** Email test for Account and Contact **/
    static final String STR_EMAIL = 'test.minuta.service@test.com';

    @testSetup
    static void setup() {        
        final User usuarioAdm = UtilitysDataTest_tst.crearUsuario('PruebaAdmin', Label.MX_PERFIL_SystemAdministrator, 'BBVA ADMINISTRADOR'); 
        usuarioAdm.Email = 'testUser@testDomain.com';
        insert usuarioAdm;
        
        System.runAs(usuarioAdm) {
            final Account newAccount = new Account();
            newAccount.FirstName = 'BPyP User';
            newAccount.LastName = 'Tst';
            newAccount.RecordTypeId = RecordTypeMemory_cls.getRecType('Account', STR_RT_BPYPPACC);
            newAccount.No_de_cliente__c = 'D2050394';
            newAccount.PersonEmail = STR_EMAIL;
            insert newAccount;

            final User tstUser = EU_cls_TestData.insertUser();
            final dwp_kitv__Visit__c visit = new dwp_kitv__Visit__c();
            visit.Name = 'Visita Prueba';
			visit.MX_BIE_TipoVisitaLLamada__c = 'Comercial';
            visit.MX_PYME_ObjetivoBPyP__c = 'Otro';
            visit.dwp_kitv__visit_start_date__c = Date.today()+4;
            visit.dwp_kitv__account_id__c = newAccount.Id;
            visit.dwp_kitv__visit_duration_number__c = '15';
            visit.dwp_kitv__visit_status_type__c = '01';
            insert visit;
            final dwp_kitv__Visit__c visitBIE = EU_cls_TestData.insertBIEVisit(newAccount.Id);

            EU_cls_TestData.insertTeam(visit.Id, tstUser.Id);

            final List<ContentVersion> ltsCV = new List<ContentVersion>();
            final ContentVersion contVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.pdf',
            VersionData = Blob.valueOf('Test Content'),
            FirstPublishLocationId=visit.Id
            );
            insert contVersion;
            ltsCV.add(contVersion);

            final ContentVersion contVersionBIE = new ContentVersion(
            Title = 'PenguinsBIE',
            PathOnClient = 'PenguinsBIE.pdf',
            VersionData = Blob.valueOf('Test ContentBIE'),
            FirstPublishLocationId=visitBIE.Id
            );
            insert contVersionBIE;
            ltsCV.add(contVersionBIE);

            Document docObj;
            docObj = new Document();
            docObj.Body = Blob.valueOf('Some Document Text');
            docObj.ContentType = 'application/pdf';
            docObj.DeveloperName = 'PenguinsDoc';
            docObj.IsPublic = true;
            docObj.Name = 'PenguinsDoc';
            docObj.FolderId = UserInfo.getUserId();
            insert docObj;
            
            final MX_BPP_CatalogoMinutas__c catalogoRecord = new MX_BPP_CatalogoMinutas__c();
            catalogoRecord.MX_Acuerdos__c = 1;
            catalogoRecord.MX_Asunto__c = 'Prueba Catalogo';
            catalogoRecord.Name = 'BienvenidaDO';
            catalogoRecord.MX_TipoVisita__c = 'BienvenidaDO';
            catalogoRecord.MX_Saludo__c = 'Saludo Test';
            catalogoRecord.MX_Contenido__c = 'Contenido Test';
            catalogoRecord.MX_Firma__c = 'Saludos Cordiales';
            catalogoRecord.MX_ImageHeader__c = 'Imagen_Header_1';
            catalogoRecord.MX_CuerpoCorreo__c = 'Contenido del correo';
            insert catalogoRecord;
            
        }
    }
    
    /**
    * @Description 	Test Method for MX_BPP_Minuta_Service.checkClientEmail()
    * @Return 		NA
    **/
	@isTest
    static void checkClientEmailIncluded() {
        final User userAdmin = [SELECT Id FROM User WHERE Name = 'PruebaAdmin'];
        System.runAs(userAdmin) {
            Test.startTest();
            final dwp_kitv__Visit__c objFinal=[select id,RecordType.name from dwp_kitv__Visit__c where name='Visita Prueba'];
            final List<ContentVersion> ltsD = [SELECT Id FROM ContentVersion where FirstPublishLocationId=:objFinal.Id];
            final String sltsD = '[{"Id":"'+ltsD[0].Id+'","Title":"Penguins","IsAssetEnabled":true}]';
            final String smyCS1 = '{"LastModifiedDate":"2018-06-27T20:47:16.000Z","IsDeleted":false,"dwp_kitv__Attach_file__c":true,"dwp_kitv__Visualforce_Name__c":"minuta","SetupOwnerId":"00D7E000000DWnZUAW","dwp_kitv__Disable_send_field_in_BBVA_team__c":false,"Name":"'+objFinal.RecordType.Name+'","SystemModstamp":"2018-06-27T20:47:16.000Z","dwp_kitv__Minimum_members_BBVA_team__c":0,"CreatedById":"0057E000004vXF5QAM","dwp_kitv__Subject__c":"BBVA Minuta","CreatedDate":"2018-06-26T23:22:44.000Z","Id":"a057E000004y5JiQAI","LastModifiedById":"0057E000004vXF5QAM","dwp_kitv__Disable_send_field_in_contacts__c":false,"dwp_kitv__Minimum_Number_of_Agreement__c":1}';
            ApexPages.currentPage().getParameters().put('Id', String.valueOf(objFinal.Id));
            ApexPages.currentPage().getParameters().put('myCS', smyCS1);
            ApexPages.currentPage().getParameters().put('documents', sltsD);
            final MX_EU_Minuta_Ctrl  testMinutaCtrl = new MX_EU_Minuta_Ctrl();
            testMinutaCtrl.sendMail();
            Test.stopTest();
            
            System.assert(testMinutaCtrl.correoEnviado, 'Problema al enviar mail');
        }
    }
       
    /**
    * @Description 	Test Method for MX_BPP_Minuta_Service.getRecordInfo()
    * @Return 		NA
    **/
    @isTest
    static void testGetRecordInfo() {    
        final dwp_kitv__Visit__c visitTest = [select id,RecordType.name from dwp_kitv__Visit__c where name='Visita Prueba'];
        test.startTest();
        final dwp_kitv__Visit__c resultVisit = MX_BPP_Minuta_Service.getRecordInfo(visitTest.Id);
        System.assertEquals(resultVisit.Id, visitTest.Id, 'Verificar');
        test.stopTest();
    }
    
    /**
    * @Description 	Test Method for MX_BPP_Minuta_Service.getMinutaMetadata()
    * @Return 		NA
    **/
    @isTest
    static void testGetMinutaMetadata() {    
        final dwp_kitv__Visit__c visitTest = [SELECT Id, MX_PYME_ObjetivoBPyP__c FROM dwp_kitv__Visit__c WHERE Name='Visita Prueba'];
        Test.startTest();
        final List<MX_BPP_TipoMinuta__mdt> resultMetaData = MX_BPP_Minuta_Service.getMinutaMetadata(visitTest);
        Test.stopTest();
        
        System.assertEquals(resultMetaData[0].MX_ObjetivoVisita__c, visitTest.MX_PYME_ObjetivoBPyP__c, 'Verificar GetMinutaMetadata');
    }
    
    /**
    * @Description 	Test Method for MX_BPP_Minuta_Service.getCatalogoMinuta()
    * @Return 		NA
    **/
    @isTest
    static void testGetCatalogoMinuta() {    
        final MX_BPP_CatalogoMinutas__c catalogoTest = [SELECT Id, Name, MX_TipoVisita__c  FROM MX_BPP_CatalogoMinutas__c WHERE Name='BienvenidaDO'];
        Test.startTest();
        final List<MX_BPP_CatalogoMinutas__c> resultCatalogo = MX_BPP_Minuta_Service.getCatalogoMinuta(catalogoTest.MX_TipoVisita__c);
        Test.stopTest();
        
        System.assertEquals(resultCatalogo[0].Name, catalogoTest.Name, 'Verificar GetCatalogoMinuta');
    }
    
    /**
    * @Description 	Test Method for MX_BPP_Minuta_Service.checkNumOfAgreements()
    * @Return 		NA
    **/
    @isTest
    static void testCheckNumOfAgreements() {
        final dwp_kitv__Visit__c visitTest = [SELECT Id, MX_PYME_ObjetivoBPyP__c FROM dwp_kitv__Visit__c WHERE Name='Visita Prueba'];
        final MX_BPP_CatalogoMinutas__c catalogoTest = [SELECT Id, Name, MX_Acuerdos__c  FROM MX_BPP_CatalogoMinutas__c WHERE Name='BienvenidaDO'];
        Test.startTest();
        final Boolean resultCheck = MX_BPP_Minuta_Service.checkNumOfAgreements(visitTest.Id, Integer.valueOf(catalogoTest.MX_Acuerdos__c));
        Test.stopTest();
        
        System.assert(!resultCheck, 'Verificar CheckNumOfAgreements');
    }

    /**
    * @Description 	Test Method for MX_BPP_Minuta_Service.recolectarTags()
    * @Return 		NA
    **/
    @IsTest
    static void recolectarTagsTest() {
        final MX_BPP_CatalogoMinutas__c catalogoTst = [SELECT Id, Name, MX_TipoVisita__c  FROM MX_BPP_CatalogoMinutas__c WHERE Name='BienvenidaDO'];
        catalogoTst.MX_HasCheckbox__c = true;
        catalogoTst.MX_HasText__c = true;
        catalogoTst.MX_HasPicklist__c = true;

        catalogoTst.MX_Opcionales__c = '{OpcionalTexto1}Test Text{OpcionalTexto1}';
        catalogoTst.MX_Opcionales__c += '{Switch1}Test Switch{Switch1}';
        catalogoTst.MX_Opcionales__c += '{Seleccion1}TestSeleccion{Opcion1}Test Opt{Opcion1}{Seleccion1}';

        final MX_BPP_MinutaWrapper tstWrpr = new MX_BPP_MinutaWrapper();
        MX_BPP_Minuta_Service.recolectarTags(catalogoTst, tstWrpr);
        System.assert(tstWrpr.contenedorCmps.cmpText[0].label == 'Test Text' && tstWrpr.contenedorCmps.cmpSwitch[0].label == 'Test Switch' && tstWrpr.contenedorCmps.cmpPicklist[0].label == 'TestSeleccion', 'Verificar información');
    }
}