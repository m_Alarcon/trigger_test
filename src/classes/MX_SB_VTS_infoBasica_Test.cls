/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 02-11-2021
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   02-11-2021   Eduardo Hernández Cuamatzi   Initial Version
 * 1.1   09-03-2021   Eduardo Hernández Cuamatzi   Se corrige clase test
**/
@isTest
public class MX_SB_VTS_infoBasica_Test {
    
    /**@description etapa Cotizacion*/
    private final static string COTIZACION = 'Cotización';
    /**Etapa cobro */
    private final static string COBROSTAGE = 'Cobro y resumen de contratación';
    /**@description Nombre usuario*/
    private final static string ASESORNAME = 'AsesorTest';
    /**Variable auxiliar para mapa */
    private final static String ISOKVAL = 'isOk';
    
    @testSetup
    static void makeData() {
        MX_SB_VTS_CallCTIs_utility.initHogarGeneric();
    }

    @isTest
    static void updateCuponCotTest() {
        Test.startTest();
        final Opportunity oppId = [Select Id, Name, StageName from Opportunity where StageName =: COTIZACION];
        final Quote quoData = [Select Id, Name from Quote where OpportunityId =: oppId.Id];
        MX_SB_VTS_PaymentModule_Service_Test.syncQuoteData(oppId, quoData.Id);
        final Quote quoteId = [Select Id, Name from Quote where OpportunityId =: oppId.Id];
        quoteId.MX_SB_VTS_Cupon__c = '';
        update quoteId;
        final Map<String, Object> updateCuponCot = MX_SB_VTS_infoBasica_Ctrl.updateCuponCot(oppId.Id, 'CMX001');
        System.assert((Boolean)updateCuponCot.get(ISOKVAL), 'Cupon Actualizado');
        Test.stopTest();
    }
    
    @isTest
    static void updateCuponCotTestErr() {
        Test.startTest();
        final Opportunity oppId = [Select Id, Name, StageName from Opportunity where StageName =: COTIZACION];
        final Quote quoData = [Select Id, Name from Quote where OpportunityId =: oppId.Id];
        MX_SB_VTS_PaymentModule_Service_Test.syncQuoteData(oppId, quoData.Id);
        final Quote quoteId = [Select Id, Name from Quote where OpportunityId =: oppId.Id];
        quoteId.MX_SB_VTS_Cupon__c = '';
        update quoteId;
        final Map<String, Object> updateCuponCot = MX_SB_VTS_infoBasica_Ctrl.updateCuponCot(oppId.Id, 'CMX00ASAFASASDASDQWEASDAWDASDAEDASDQWEASDSSGHVCZQDAS1');
        System.assert(!(Boolean)updateCuponCot.get(ISOKVAL), 'Cupon invalido');
        Test.stopTest();
    }

    @isTest
    static void updateOppTest() {
        final User asesorUser = [Select Id from User where LastName =: ASESORNAME];
        MX_SB_VTS_CallCTIs_utility.insertPermissionVTS(asesorUser);
        final Opportunity updateOpp = [Select Id, Name, StageName from Opportunity where StageName =: COTIZACION];
        Test.startTest();
        System.runAs(asesorUser) {
            MX_SB_VTS_infoBasica_Ctrl.updateOpp(updateOpp.Id, COBROSTAGE);
            final Opportunity updateOppAfter = [Select Id, Name, StageName from Opportunity where StageName =: COBROSTAGE];
            System.assertEquals(updateOppAfter.StageName, COBROSTAGE, 'Opportunidad Actualizada');
        }
        Test.stopTest();
    }

    @isTest
    static void updateCupon() {
        Test.startTest();
        final Opportunity oppId = [Select Id, Name, StageName from Opportunity where StageName =: COTIZACION];
        final Quote quoData = [Select Id, Name from Quote where OpportunityId =: oppId.Id];
        MX_SB_VTS_PaymentModule_Service_Test.syncQuoteData(oppId, quoData.Id);
        final Quote quoteId = [Select Id, Name from Quote where OpportunityId =: oppId.Id];
        quoteId.MX_SB_VTS_Cupon__c = '';
        update quoteId;
        final Map<String, Object> updateCumAP = MX_SB_VTS_infoBasica_Ctrl.updateCupon(oppId.Id, 'CMX001');
        System.assert((Boolean)updateCumAP.get(ISOKVAL), 'Cupon Actualizado');
        Test.stopTest();
    }

    @isTest
    static void updateCuponErr() {
        Test.startTest();
        final Opportunity oppId = [Select Id, Name, StageName from Opportunity where StageName =: COTIZACION];
        final Quote quoData = [Select Id, Name from Quote where OpportunityId =: oppId.Id];
        MX_SB_VTS_PaymentModule_Service_Test.syncQuoteData(oppId, quoData.Id);
        final Quote quoteId = [Select Id, Name from Quote where OpportunityId =: oppId.Id];
        quoteId.MX_SB_VTS_Cupon__c = '';
        update quoteId;
        final Map<String, Object> updateCumAP = MX_SB_VTS_infoBasica_Ctrl.updateCupon(oppId.Id, 'CMX00ASAFASASDASDQWEASDAWDASDAEDASDQWEASDASDADQDAS1');
        System.assert(!(Boolean)updateCumAP.get(ISOKVAL), 'Cupon error');
        Test.stopTest();
    }

    @isTest
    static void resetQuotes() {
        Test.startTest();
            final Opportunity oppId = [Select Id, Name, StageName from Opportunity where StageName =: COTIZACION];
            final Map<String, Object> lstQuotes = MX_SB_VTS_infoBasica_Ctrl.resetQuotes(oppId.Id);
            System.assert((Boolean)lstQuotes.get(ISOKVAL), 'Cotizaciones eliminadas');
        Test.stopTest();
    }
}