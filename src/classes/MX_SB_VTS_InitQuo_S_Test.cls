/**
* @description       : Clase test que cubre a clase MX_SB_VTS_InitQuo_Service
* @author            : Diego Olvera
* @group             : BBVA
* @last modified on  : 01-27-2021
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   11-01-2020   Diego Olvera   Initial Version
**/
@isTest
public class MX_SB_VTS_InitQuo_S_Test {
    /**@description Nombre usuario*/
    private final static String ASESORCMBNAME = 'AsesorTest';
    /**@description Nombre de la cuenta*/
    private final static String ACCCMBNAME = 'TestCmb';
    /**@description Nombre de Oportunidad*/
    private final static String OPPCMBNAME = 'TestOppCmb';
    /**@description Nombre de Propio/Rentado*/
    private final static String OPPPRO = 'Propio';
    
    @TestSetup
    static void makeData() {
        final User userRecCmb = MX_WB_TestData_cls.crearUsuario(ASESORCMBNAME, 'System Administrator');
        insert userRecCmb;
        final Account accntCmb = MX_WB_TestData_cls.createAccount(ACCCMBNAME, System.Label.MX_SB_VTS_PersonRecord);
        insert accntCmb;
        final Opportunity oppCmb = MX_WB_TestData_cls.crearOportunidad(OPPCMBNAME, accntCmb.Id, userRecCmb.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
        oppCmb.LeadSource = 'Facebook';
        oppCmb.Producto__c = 'Hogar seguro dinámico';	
        insert oppCmb;
        final MX_RTL_MultiAddress__c nwAdd = new MX_RTL_MultiAddress__c();
        nwAdd.MX_RTL_MasterAccount__c = oppCmb.AccountId;
        nwAdd.MX_RTL_Opportunity__c = oppCmb.Id; 
        nwAdd.Name = 'Direccion';
        insert nwAdd;
        final Quote quoteRec = new Quote(OpportunityId=oppCmb.Id, Name='Outbound');
        quoteRec.MX_SB_VTS_Folio_Cotizacion__c = '556433';
        quoteRec.QuoteToName = 'Completa';
        insert quoteRec;
        final Quote quoteRec1 = new Quote(OpportunityId=oppCmb.Id, Name='Cmb');
        quoteRec1.MX_SB_VTS_Folio_Cotizacion__c = '556434';
        quoteRec1.QuoteToName = 'Balanceada';
        insert quoteRec1;
        final Quote quoteRec2 = new Quote(OpportunityId=oppCmb.Id, Name='Fb');
        quoteRec2.MX_SB_VTS_Folio_Cotizacion__c = '556435';
        quoteRec2.QuoteToName = 'Basica';
        insert quoteRec2;
        final MX_WB_FamiliaProducto__c famProd = MX_WB_TestData_cls.createProductsFamily('Hogar dinámico');
        insert famProd;
        final Product2 pro = MX_WB_TestData_cls.productNew('Hogar seguro dinámico');
        pro.IsActive=true;
        pro.MX_WB_Familiaproductos__c = famProd.Id;
        Insert pro;
        final PricebookEntry PbE = MX_WB_TestData_cls.priceBookEntryNew(Pro.Id);
        Insert PbE;
    }

    @isTest
    static void ctrQuote() {
        final User userGetOpp = [Select Id from User where LastName =: ASESORCMBNAME];
        System.runAs(userGetOpp) {
            final Opportunity oppLstVal = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBNAME];
            Test.startTest();
            try {
                MX_SB_VTS_InitQuo_Service.ctrDataQuote(oppLstVal.Id, OPPPRO);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal1');
                System.assertEquals('Fatal1', extError.getMessage(),'No obtuvo datos ctrQuote');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void saveDataQuoteTest() {
        final User userGetOpp = [Select Id from User where LastName =: ASESORCMBNAME];
        System.runAs(userGetOpp) {
            final Opportunity oppLstVal = [Select Id, StageName, LeadSource, OwnerId, Name from Opportunity Where Name =: OPPCMBNAME];
            final List<Opportunity> nwLstOpps = new List<Opportunity>{oppLstVal};
            Test.startTest();
            try {
                MX_SB_VTS_InitQuo_Service.saveDataQuote(nwLstOpps, OPPPRO);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal2');
                System.assertEquals('Fatal2', extError.getMessage(),'No obtuvo datos saveDataQuoteTest');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void getPriceBook() {
        final User userGetOpp = [Select Id from User where LastName =: ASESORCMBNAME];
        System.runAs(userGetOpp) {
            final List<Product2> nwLst = [Select Id, Name FROM Product2 Where Name = 'Hogar seguro dinámico'];
            Test.startTest();
            try {
                MX_SB_VTS_InitQuo_Service.getPriceBookSta(nwLst);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal3');
                System.assertEquals('Fatal3', extError.getMessage(),'No obtuvo datos getPriceBook');
            }
            Test.stopTest();
        }
    }
    
    @isTest
    static void fillDataQuoTest() {
        final User userGetOpp = [Select Id from User where LastName =: ASESORCMBNAME];
        System.runAs(userGetOpp) {
            final Opportunity oppLstVal = [Select Id, StageName, LeadSource, OwnerId, Name from Opportunity Where Name =: OPPCMBNAME];
            final Product2 nwLst = [Select Id, Name,MX_WB_Familiaproductos__c FROM Product2 Where Name = 'Hogar seguro dinámico'];
            final PriceBookEntry pbeVal = [Select Id, Pricebook2Id, Name FROM PriceBookEntry Where product2Id=: nwLst.Id];
            final List<Opportunity> nwListOpp = new List<Opportunity>{oppLstVal};
            Test.startTest();
            try {
                MX_SB_VTS_InitQuo_Service.fillDataQuo(pbeVal, nwListOpp, nwLst.MX_WB_Familiaproductos__c, 'Propio');
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal4');
                System.assertEquals('Fatal4', extError.getMessage(),'No obtuvo datos fillDataQuoTest');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void ctrNwQuoTest() {
        final User userGetOpp = [Select Id from User where LastName =: ASESORCMBNAME];
        System.runAs(userGetOpp) {
            final Opportunity oppLstVal = [Select Id, StageName, LeadSource, OwnerId, Name from Opportunity Where Name =: OPPCMBNAME];
            final List<Opportunity> nwLstOpps = new List<Opportunity>{oppLstVal};
                final Product2 nwLst = [Select Id, MX_WB_Familiaproductos__c, Name FROM Product2 Where Name = 'Hogar seguro dinámico'];
            final PriceBookEntry pbeVal = [Select Id, Name, Pricebook2Id FROM PriceBookEntry Where product2Id=: nwLst.Id];
            
            
            Test.startTest();
            try {
                MX_SB_VTS_InitQuo_Service.ctrNwQuo(nwLstOpps, pbeVal, nwLst.MX_WB_Familiaproductos__c, 'Propio');
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal5');
                System.assertEquals('Fatal5', extError.getMessage(),'No obtuvo datos ctrNwQuoTest');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void crtMulQuoTest() {
        final User userGetMul = [Select Id from User where LastName =: ASESORCMBNAME];
        System.runAs(userGetMul) {
            final Opportunity oppLstMul = [Select Id, StageName, LeadSource, OwnerId, Name from Opportunity Where Name =: OPPCMBNAME];
            final List<Opportunity> nwMulOpp = new List<Opportunity>{oppLstMul};
                final Product2 nwLstVal = [Select Id, MX_WB_Familiaproductos__c, Name FROM Product2 Where Name = 'Hogar seguro dinámico'];
            final PriceBookEntry pbeValMul = [Select Id, Name, Pricebook2Id FROM PriceBookEntry Where product2Id=: nwLstVal.Id];
            
            
            Test.startTest();
            try {
                MX_SB_VTS_InitQuo_Service.crtMulQuo(nwMulOpp, pbeValMul, nwLstVal.MX_WB_Familiaproductos__c);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal9');
                System.assertEquals('Fatal9', extError.getMessage(),'No obtuvo datos crtMulQuoTest');
            }
            Test.stopTest();
        }
    }
    
    @isTest
    static void updQuotelineTest() {
        final User userGetOpp = [Select Id from User where LastName =: ASESORCMBNAME];
        System.runAs(userGetOpp) {
            final Pricebook2 prB2 = MX_WB_TestData_cls.createStandardPriceBook2();
            final Opportunity oppLstVal = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBNAME];
            final Product2 nwLst = [Select Id, Name FROM Product2 Where Name = 'Hogar seguro dinámico'];
            final PriceBookEntry pbeVal = [Select Id, Name FROM PriceBookEntry Where product2Id=: nwLst.Id];
            final String famProd = nwLst.Id;
            final Quote Qtem = new Quote(Name= 'temp', OpportunityId = oppLstVal.Id, Pricebook2Id= prB2.Id,status='Emitida',MX_SB_VTS_Numero_de_Poliza__c='POL1234567');
            insert Qtem;            
            final List<Quote> nwLstQuo = [Select Id, QuoteToName, OpportunityId FROM Quote Where Name = 'temp' AND OpportunityId =: oppLstVal.Id];
            
            Test.startTest();
            try {
                MX_SB_VTS_InitQuo_Service.updQuoteline(nwLstQuo, pbeVal, famProd);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal6');
                System.assertEquals('Fatal6', extError.getMessage(),'No obtuvo datos updQuotelineTest');
            }
            Test.stopTest();
        }
    }
    
    @isTest
    static void updQuoName() {
        final User userGetOpp = [Select Id from User where LastName =: ASESORCMBNAME];
        System.runAs(userGetOpp) {
            final Opportunity oppLstVal = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBNAME];
            final List<Quote> nwLstQuo = [Select Id, QuoteToName, OpportunityId FROM Quote Where OpportunityId =: oppLstVal.Id];
            nwLstQuo[0].QuoteToName = 'Basica';
            nwLstQuo[1].QuoteToName = 'Balanceada';
            nwLstQuo[2].QuoteToName = 'Completa';
            update nwLstQuo;
            Test.startTest();
            try {
                MX_SB_VTS_InitQuo_Service.updQuoteRec(oppLstVal.Id);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal');
                System.assertEquals('Fatal', extError.getMessage(),'No obtuvo datos updQuoName');
            }
            Test.stopTest();
        }
    }
    
}