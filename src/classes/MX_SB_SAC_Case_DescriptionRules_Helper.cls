/**
 * @description       :
 * @author            : Jaime Terrats
 * @group             :
 * @last modified on  : 08-13-2020
 * @last modified by  : Jaime Terrats
 * Modifications Log
 * Ver   Date         Author          Modification
 * 1.0   07-09-2020   Jaime Terrats   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_SAC_Case_DescriptionRules_Helper {
    /**string for other case reasons */
    final static String[] OTHER_REASONS = new String[]{'Consultas', 'Incidencia'};
    /** string for case status ejecución en tramite */
    final static String CASE_STATUS = 'Ejecución de tramite';
    /** list of details for consultas */
    final static String[] DETAILS_QUERY = new String[]{'Información cotizador','Información de cotización','Información de producto',
    'Información de trámite','Llamada de prueba','No es Asegurado/Contratante','Confirma recepción de documentos',
    'No autentica correctamente'};
    /** array of options for case.reason */
    final static String[] REASON_VALUES = new String[]{'Cancelación', 'Retención'};
    /** string for detail */
    final static String[] DETAIL_VALUES = new String[]{'Duplicidad', 'Creditos', 'Error en condiciones de venta', 'Cross sell', 'Promoción/campaña', 'Disminución de suma asegurada'};

    /**
    * @description
    * @author Jaime Terrats | 6/25/2020
    * @param nCase
    * @return void
    **/
    public static void validateMVComments(Case nCase, Case oldCase, String flujo) {
        if(flujo == 'MV Ret' && nCase.MX_SB_SAC_TerminaMalasVentas__c == true && nCase.MX_SB_SAC_OcultarArgumentos__c == false) {
            nCase.Description.addError(System.Label.MX_SB_SAC_RequestComments);
        }
        if(flujo == 'Mala Venta' && oldCase.MX_SB_SAC_TerminaMalasVentas__c == true && nCase.MX_SB_SAC_TerminaMalasVentas__c == true) {
            nCase.Description.addError(System.Label.MX_SB_SAC_RequestComments);
        }
        validateMVComments2(nCase, oldCase, flujo);
    }

    /**
    * @description
    * @author Jaime Terrats | 07-10-2020
    * @param oldCase
    * @param flujo
    **/
    private static void validateMVComments2(Case nCase, Case oldCase, String flujo) {
        if(REASON_VALUES.contains(flujo) && oldCase.MX_SB_SAC_Finalizar_Retencion__c == true && oldCase.MX_SB_SAC_ActivarReglaComentario__c == true) {
            nCase.Description.addError(System.Label.MX_SB_SAC_RequestComments);
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 07-11-2020
    * @param nCase
    * @param oldCase
    **/
    public static void checkIfClosed(Case nCase, Case oldCase) {
        final String[] fields = new String[]{'MX_SB_SAC_CorreoEnviado__c', 'MX_SB_SAC_CorreoEnviadoBO__c','MX_SB_SAC_CorreoEnviadoDevolucion__c', 'MX_SB_SAC_EnvioCorreoCierreCaso__c'};
        for(String field : fields) {
            if(nCase.Status.equals(oldCase.Status)
            && System.Label.MX_SB_MLT_Etapa_cerrado.equals(nCase.Status)
            && !System.Label.MX_SB_VTS_COTIZACION_LBL.equals(nCase.Reason)) {
                if(oldCase.get(field) == false && nCase.get(field) == true) {
                    continue;
                } else {
                    nCase.addError(System.Label.MX_SB_SAC_CaseClosed);
                }
            }
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 07-24-2020
    * @param nCase
    * @param oldCase
    **/
    public static void validateTransferComments(Case nCase, Case oldCase) {
        if(String.isNotBlank(nCase.MX_SB_SAC_Detalle__c) && nCase.MX_SB_1500_is1500__c == true && String.isBlank(nCase.Description) && nCase.MX_SB_1500_isAuthenticated__c == true) {
            nCase.Description.addError(System.Label.MX_SB_SAC_RequestComments);
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 07-30-2020
    * @param nCase
    * @param oldCase
    **/
    public static void validateQueryComments(Case nCase, Case oldCase) {
        if(nCase.MX_SB_1500_isAuthenticated__c == true && nCase.MX_SB_SAC_NumCuenta__c == 'false' && oldCase.MX_SB_SAC_NumCuenta__c == 'false' && 
        String.isNotBlank(oldCase.MX_SB_SAC_Detalle__c) && nCase.MX_SB_1500_is1500__c == true && String.isBlank(nCase.Description) &&
        (String.isBlank(oldCase.MX_SB_SAC_Ramo__c) || nCase.MX_SB_SAC_Ramo__c=='1500 Auto')) {
            nCase.Description.addError(System.Label.MX_SB_SAC_RequestComments);
        }
    }

    /** *
     * @description
     * @author Alan Santacruz | 15-11-2020
     * @param Case
     * @param oldCase
    **/
    public static void validateQueryCommentsVida(Case nCase, Case oldCase) {
        if(nCase.MX_SB_1500_isAuthenticated__c == true && String.isBlank(oldCase.MX_SB_SAC_Detalle__c) && nCase.MX_SB_1500_is1500__c == true
        && String.isBlank(nCase.Description) && nCase.MX_SB_SAC_Ramo__c=='1500 Vida') {
            nCase.Description.addError(System.Label.MX_SB_SAC_RequestComments);
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 07-30-2020
    * @param nCase
    * @param oldCase
    **/
    public static void duplicate1500(Case nCase, Case oldCase) {
        if(oldCase.MX_SB_1500_is1500__c == true && oldCase.MX_SB_SAC_EsCopia__c == true
        && oldCase.MX_SB_SAC_ActivarReglaComentario__c == false
        && !System.Label.MX_SB_MLT_Etapa_cerrado.equals(nCase.Status)) {
            nCase.MX_SB_SAC_ActivarReglaComentario__c = true;
        }
    }

    /**
    * @description
    * @author Miguel Hernandez | 07-30-2020
    * @param nCase
    * @param oldCase
    **/
    public static void preventCommentsOnReasonVida(Case nCase) {
        if(REASON_VALUES.contains(nCase.Reason)
        && String.isNotBlank(nCase.Description)
        && nCase.MX_SB_SAC_FinalizaFlujo__c == true
        && !DETAIL_VALUES.contains(nCase.MX_SB_SAC_Detalles_Vida__c)
        && nCase.MX_SB_SAC_ActivarReglaComentario__c == false
        && nCase.MX_SB_1500_is1500__c == false
        && (nCase.MX_SB_SAC_Finalizar_Retencion__c == false
        || nCase.MX_SB_SAC_TerminaMalasVentas__c == false)) {
            nCase.addError(System.Label.MX_SB_SAC_PreventComments);
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 7/3/2020
    * @param nCase
    * @return String
    **/
    public static String checkIfMVVida(Case nCase) {
        final String[] valuesToEval = new String[] {DETAIL_VALUES[0], DETAIL_VALUES[1], DETAIL_VALUES[3], DETAIL_VALUES[4],  DETAIL_VALUES[5]};
        if(nCase.MX_SB_SAC_FinalizaFlujo__c == true && nCase.MX_SB_SAC_Finalizar_Retencion__c == false && valuesToEval.contains(nCase.MX_SB_SAC_Detalles_Vida__c) && String.isBlank(nCase.Description)) {
            nCase.MX_SB_SAC_ActivarReglaComentario__c = true;
            nCase.MX_SB_SAC_Finalizar_Retencion__c = true;
        }
        return nCase.MX_SB_SAC_EsMalaVenta__c == true ?  'Mala Venta' : 'MV Ret';
    }
}