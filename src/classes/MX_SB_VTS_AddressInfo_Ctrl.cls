/**
 * @description       : Clase controller que sirve de apoyo para la clase service MX_SB_VTS_AddressInfo_Service
 * @author            : Diego Olvera
 * @group             : BBVA
 * @last modified on  : 02-17-2021
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   01-08-2021   Diego Olvera   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_AddressInfo_Ctrl {
     /** Constructor de la clase */
    private MX_SB_VTS_AddressInfo_Ctrl() {}
    /**
    * @description Función que actualiza etapa actual de la oportunidad
    * @author Diego Olvera | 27-11-2020 
    * @param  srtOppId, etapaForm Id de la oportunidad y etapa actual del lwc recuperada desde el front JS
    * @return void
    **/ 
    @AuraEnabled
    public static void updCurrentOppCtrl(String srtOppId, String etapaForm) {
        MX_SB_VTS_AddressInfo_Service.updCurrentOpp(srtOppId, etapaForm);
    }
     /**
    * @description Función que obtiene la etapa/datos guardado en la oportunidad
    * @author Diego Olvera | 27-11-2020 
    * @param  idOpps
    * @return List<Opportunity> lstValsOpp
    **/  
    @AuraEnabled
    public static List<Opportunity> gtOppValsCrtl(String idOpps) {
        return MX_SB_VTS_AddressInfo_Service.gtOppVals(idOpps);
    }
     /**
    * @description Función que crea un registro en el objeto de direcciones
    * @author Diego Olvera | 27-11-2020 
    * @param  srtId, fdataAddress mapa llenado desde el fron JS con valores de direccion
    * @return void
    **/ 
    @AuraEnabled 
    public static void createAddressCtrl(String srtId, Map<String, Object> fdataAddress) {
        MX_SB_VTS_AddressInfo_Service.createAddress(srtId, fdataAddress);
    }

     /**
    * @description Obtiene datos del objeto de Direcciones
    * dependiendo del tipo de direccion que se especifique en el js
    * @author Diego Olvera | 27-11-2020 
    * @param  iddOpps, dirType
    * @return List<MX_RTL_MultiAddress__c> lstAddVals
    **/ 
    @AuraEnabled
    public static List<MX_RTL_MultiAddress__c> getValAddressCtrl(String iddOpps, String dirType) {
        return MX_SB_VTS_AddressInfo_Service.getValAddress(iddOpps, dirType);
    }

     /**
    * @description Actualiza datos del formulario de DatosContratante
    * @author Diego Olvera | 27-11-2020 
    * @param  srtId, mapDataQuote
    * @return void
    **/ 
    @AuraEnabled 
    public static void updQuoLineCtrl(String srtId, Map<String, Object> mapDataQuote) {
        MX_SB_VTS_AddressInfo_Service.updQuoLine(srtId, mapDataQuote);
    }
      /**
    * @description Obtiene datos de las coberturas por cotización
    * @author Diego Olvera | 08-01-2020 
    * @param oppId Id de registro trabajado (Opportunity)
    * @return Map<String, Object> Datos recuperados
    **/
    @AuraEnabled
    public static List<Cobertura__c> fillCoberValsCtrl(String oppId) {
         return MX_SB_VTS_AddressInfo_Service.fillCoberVals(oppId);
    }
     /**
    * @description Obtiene datos de las coberturas por cotización
    * @author Diego Olvera | 08-01-2020 
    * @param oppId Id de registro trabajado (Opportunity)
    * @return Map<String, Object> Datos recuperados
    **/
    @AuraEnabled
    public static void updSyncQuoCtrl(String oppId, String nameQuo) {
        MX_SB_VTS_AddressInfo_Service.updSyncQuo(oppId, nameQuo);
    }
    
    /**
    * @description Guarda las sumas aseguradas de la cotización sobre la oportunidad
    * @author Marco Cruz | 13-01-2020 
    * @param oppId Id de registro trabajado (Opportunity)
    * @return String sumasCoti
    **/
    @AuraEnabled
    public static void updateSumas(String oppId, String insuredAmo) {
        MX_SB_VTS_AddressInfo_Service.udpateAmo(oppId, insuredAmo);
    }
}