/**
* @description       : Clase service que busca en clase selector estados de la republica
* @author            : Diego Olvera
* @group             : BBVA
* @last modified on  : 02-18-2021
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   11-20-2020   Diego Olvera   Initial Version
* 1.1   18-02-2021   Diego Olvera   Se agrega funcion y variable global para especificar nombre de metadata que se quiere buscar
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_ObtStates_Service {
    /** Variable con campos para metadata */
    static final String DIRECCIONFIELDS = 'Id, MX_SB_VTS_States__c, MX_SB_VTS_Abreviacion__c';
     /** Variable de nombre de metadata que se busca */
    static final String FRNAME = 'MX_SB_VTS_StatesMex__mdt';
    /**
    * @description Función que recupera listado de estados de la republica por valor recuperado en front
    * @author Diego Olvera | 18-02-2021 
    * @param searchVal, valor recuperado desde clase controller
    * @return Listado de estados recuperados de clase selector
    **/
    public static List<MX_SB_VTS_StatesMex__mdt> getDirValue(String searchVal) {
        return MX_RTL_CustomMeta_Selector.getDirections(searchVal); 
    }
    /**
    * @description Función que recupera listado de estados de la republica
    * @author Diego Olvera | 18-02-2021 
    * @param void
    * @return Listado de estados recuperados de clase selector
    **/
    public static List<MX_SB_VTS_StatesMex__mdt> obtEstadosVals() {
        return MX_RTL_CustomMeta_Selector.getStates(DIRECCIONFIELDS, FRNAME); 
    }
}