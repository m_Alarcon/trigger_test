/**
 * @File Name          : MX_SB_SAC_IdentificarCliente_Test.cls
 * @Description        :
 * @Author             : Jaime Terrats
 * @Group              :
 * @Last Modified By   : Jaime Terrats
 * @Last Modified On   : 07-10-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    1/9/2020   Jaime Terrats     Initial Version
**/
@isTest @SupressWarnings ('sf:TooManyMethods')
private class MX_SB_SAC_IdentificarCliente_Test {
    /** case subject */
    final static String NAME = 'tSAC';
    /** user data */
    final static String USERNAME = 'uTestSac';
    /** fake id */
    final static String MOCK_ID = '5003B000';
    /** error handler */
    final static String ERR_MSG = 'Script-thrown exception';
    /** assert message */
    final static String ASSERT_MSG = 'Error esperado';
    /** case recordtype */
    final static String RTID = SObjectType.case.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_SAC_Generico).getRecordTypeId();
    /** search filter 1 */
    final static String FILTER_NAME = 'contratante';
    /** search filter 2 */
    final static String FILTER_NAME2 = 'asegurado';
    /** account email */
    final static String EMAILACC = 'test.identificar@sac.com';
    /** beneficiary name */
    final static String BENE_NAME = 'tBeneSac';
    /** generic type */
    final static String TYPE = 'SAC-DQ';
    /** mv type */
    final static String MV_TYPE = 'MV-DQ';
    /** tipification type */
    final static String TIP_TYPE = 'SAC';
    /** policy number */
    final static String MOCK_POLICY = '895W11000Y';
    /** mock body */
    final static String BODY = 'RespRestGetPolizaPorNumeroDePoliza:[Detalle={"nombreContratante":"ATE@TE","apellidoPaternoContratante":"AS","apellidoMaternoContratante":"ATE@T","fechaNacimientoContratante":"17/01/1981","generoContratante":"H","profesionContratante":"EMPLEADO DE OFICINA","nacionalidadContratante":"EXTRANJERO","telefonoCasaContratante":null,"telefonoOficinaContratante":null,"telefonoCelularContratante":"5552123123","correoElectronicoContratante":"rodrigo.rios@development.adesis.com","rfcContratante":"AAAA810117","homoclaveContratante":null,"tipoPersonaContratante":"FISICA","direccionContratante":"MARIANO ESCOBEDO","coloniaContratante":"ADAN CASTELAN","poblacionContratante":"PÁNUCO","estadoContratante":"VERACRUZ","cpContratante":"11320","nombreAsegurado":"ATE@TE","apellidoPaternoAsegurado":"AS","apellidoMaternoAsegurado":"ATE@T","fechaNacimientoAsegurado":"17/01/1981","generoAsegurado":"MASCULINO","profesionAsegurado":"EMPLEADO DE OFICINA","nacionalidadAsegurado":"EXTRANJERO","telefonoOficinaAsegurado":null,"telefonoCelularAsegurado":"5552123123","correoElectronicoAsegurado":"rodrigo.rios@development.adesis.com","rfcAsegurado":"AAAA810117","homoclaveAsegurado":null,"direccionAsegurado":"MARIANO ESCOBEDO","coloniaAsegurado":"ADAN CASTELAN","poblacionAsegurado":"PÁNUCO","estadoAsegurado":"VERACRUZ","cpAsegurado":"11320","nombreConductorAsignado":"Angel","apellidoPaternoConductorAsignado":"Garcia","apellidoMaternoConductorAsignado":null,"fecNacConductorAsignado":null,"numeroPoliza":"895W11000Y","fechaVenta":"2015-09-22 00:00:00.0","fechaAltaPoliza":"2015-09-22 00:00:00.0","fechaInicioVigencia":"2015-09-22 00:00:00.0","fechaFinVigencia":"2016-09-22 00:00:00.0","producto":"WIBE","plan":null,"tipoVehiculo":"SUV","formaPago":"MENSUAL","beneficiarioPreferente":null,"solicitudFactura":"OK","tipoPago":"DEBITO A TARJETAS DE CREDITO","promocion":null,"descuentoFacultado":null,"importe":"801.17","estatusPoliza":"ACTIVA","anio":"2010","marca":"FORD","submarca":"ECOSPORT","version":"4X2 MP3 USB","descripcion":"FORD ECOSPORT 4X2 MP3 USB AA EE CD BA ESTANDAR SUV 4 CIL 5 P 5 OCUP","servicio":"PARTICULAR","agencia":null,"numeroMotor":null,"numeroSerie":"D12D12D12","placas":null,"clavesb":"304GBV","capacidad":"5","zonaCirculacion":"VERACRUZ","operador":"WIBE","canal":"INTERNET","subcanal":"WEB","numeroCuenta":"************6456","noFolio":"1728747","intermediario":"NEGOCIO DIRECTO","subProducto":"AUTO WIBE","renovacion":"0"}, Error=false, JsonRes={"nombreContratante":"ATE@TE","apellidoPaternoContratante":"AS","apellidoMaternoContratante":"ATE@T","fechaNacimientoContratante":"17/01/1981","generoContratante":"H","profesionContratante":"EMPLEADO DE OFICINA","nacionalidadContratante":"EXTRANJERO","telefonoCasaContratante":null,"telefonoOficinaContratante":null,"telefonoCelularContratante":"5552123123","correoElectronicoContratante":"rodrigo.rios@development.adesis.com","rfcContratante":"AAAA810117","homoclaveContratante":null,"tipoPersonaContratante":"FISICA","direccionContratante":"MARIANO ESCOBEDO","coloniaContratante":"ADAN CASTELAN","poblacionContratante":"PÁNUCO","estadoContratante":"VERACRUZ","cpContratante":"11320","nombreAsegurado":"ATE@TE","apellidoPaternoAsegurado":"AS","apellidoMaternoAsegurado":"ATE@T","fechaNacimientoAsegurado":"17/01/1981","generoAsegurado":"MASCULINO","profesionAsegurado":"EMPLEADO DE OFICINA","nacionalidadAsegurado":"EXTRANJERO","telefonoOficinaAsegurado":null,"telefonoCelularAsegurado":"5552123123","correoElectronicoAsegurado":"rodrigo.rios@development.adesis.com","rfcAsegurado":"AAAA810117","homoclaveAsegurado":null,"direccionAsegurado":"MARIANO ESCOBEDO","coloniaAsegurado":"ADAN CASTELAN","poblacionAsegurado":"PÁNUCO","estadoAsegurado":"VERACRUZ","cpAsegurado":"11320","nombreConductorAsignado":null,"apellidoPaternoConductorAsignado":null,"apellidoMaternoConductorAsignado":null,"fecNacConductorAsignado":null,"numeroPoliza":null,"fechaVenta":"2015-09-22 00:00:00.0","fechaAltaPoliza":"2015-09-22 00:00:00.0","fechaInicioVigencia":"2015-09-22 00:00:00.0","fechaFinVigencia":"2016-09-22 00:00:00.0","producto":"WIBE","plan":null,"tipoVehiculo":"SUV","formaPago":"MENSUAL","beneficiarioPreferente":null,"solicitudFactura":"OK","tipoPago":"DEBITO A TARJETAS DE CREDITO","promocion":null,"descuentoFacultado":null,"importe":"801.17","estatusPoliza":"ACTIVA","anio":"2010","marca":"FORD","submarca":"ECOSPORT","version":"4X2 MP3 USB","descripcion":"FORD ECOSPORT 4X2 MP3 USB AA EE CD BA ESTANDAR SUV 4 CIL 5 P 5 OCUP","servicio":"PARTICULAR","agencia":null,"numeroMotor":null,"numeroSerie":"D12D12D12","placas":null,"clavesb":"304GBV","capacidad":"5","zonaCirculacion":"VERACRUZ","operador":"WIBE","canal":"INTERNET","subcanal":"WEB","numeroCuenta":"************6456","noFolio":"1728747","intermediario":"NEGOCIO DIRECTO","subProducto":"AUTO WIBE","renovacion":"0"}, NombreMetodo=obtenerPolizaPorNumeroDePoliza]';
    /**
    * @description
    * @author Jaime Terrats | 1/14/2020
    * @return void
    **/
    @TestSetup
    static void makeData() {
        final User tUser = MX_WB_TestData_cls.crearUsuario(USERNAME, System.Label.MX_SB_VTS_ProfileAdmin);
        insert tUser;
        System.runAs(tUser) {
            final Case tCase = MX_WB_TestData_cls.createSACCase(NAME, RTID);
            Insert tCase;

            final Account tAccount = MX_WB_TestData_cls.createAccount(NAME, System.Label.MX_SB_VTS_PersonRecord);
            tAccount.PersonEmail = EMAILACC;
            tAccount.PersonMobilePhone = '5555555555';
            tAccount.AccountSource = 'Inbound';
            tAccount.PersonBirthdate = System.today();
            Insert tAccount;

            final MX_WB_FamiliaProducto__c famProd = MX_WB_TestData_cls.createProductsFamily(System.Label.MX_SB_VTS_Vida);
            Insert famProd;

            final Product2 tProduct = MX_WB_TestData_cls.productNew(System.Label.MX_SB_VTS_Vida);
            tProduct.isActive = true;
            tProduct.MX_WB_FamiliaProductos__c = famProd.Id;
            Insert tProduct;

            final Contract tContract = MX_WB_TestData_cls.vtsCreateContract(tAccount.Id, tUser.Id, tProduct.Id);
            tContract.MX_SB_SAC_NombreCuentaText__c = NAME;
            tContract.MX_SB_SAC_NombreClienteAseguradoText__c = BENE_NAME;
            Insert tContract;

            final MX_SB_VTS_Generica__c generica = MX_WB_TestData_cls.GeneraGenerica('Q1', TYPE);
            generica.MX_SB_SAC_ProductFam__c = System.Label.MX_SB_VTS_Vida;
            generica.MX_SB_SAC_Question__c = 'Test';
            generica.MX_SB_SAC_Question2__c = 'Test2';
            generica.MX_SB_SAC_Question3__c = 'Test3';
            generica.MX_SB_SAC_Question4__c = 'Test4';
            generica.MX_SB_SAC_RestoreQ__c = 'Restore Q';

            Insert generica;

            final MX_SB_SAC_CatalogoTipificacion__c tip = MX_WB_TestData_cls.createCaseTipifications(tProduct, TIP_TYPE);
            Insert tip;
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 1/9/2020
    * @return void
    **/
    @isTest
    static void testRetrieveUser() {
        final Case tCase = [Select Id from Case where Subject =: NAME];
        final User tUser = [Select Id from User where Name =: USERNAME];
        Test.startTest();
        final Map<String, SObject> mapData = MX_SB_SAC_IdentificarCliente.initialData(tCase.Id, tUser.Id);
        System.assertEquals(tCase.Id, mapData.get('case').Id, 'No hay match');
        Test.stopTest();
    }

    /**
    * @description
    * @author Jaime Terrats | 1/14/2020
    * @return void
    **/
    @isTest
    static void testFailure() {
        Test.startTest();
        try {
            final String mock = MOCK_ID + '0060UiLQAU';
            MX_SB_SAC_IdentificarCliente.initialData(mock, mock);
        } catch(Exception ex) {
            final Boolean expectedError = ex.getMessage().contains('List index out of bounds') ? true : false;
            System.assert(expectedError, ASSERT_MSG);
        }
        Test.stopTest();
    }

    /**
    * @description
    * @author Jaime Terrats | 1/14/2020
    * @return void
    **/
    @isTest
    static void testSearchData() {
        Test.startTest();
        final List<Contract> getContracts = MX_SB_SAC_IdentificarCliente.retrieveRecords(NAME, FILTER_NAME);
        System.assertEquals(getContracts.size(), 1, ASSERT_MSG);
        Test.stopTest();
    }

    /**
    * @description
    * @author Jaime Terrats | 1/14/2020
    * @return void
    **/
    @isTest
    static void testSearchData2() {
        Test.startTest();
        final List<Contract> getContracts = MX_SB_SAC_IdentificarCliente.retrieveRecords(BENE_NAME, FILTER_NAME2);
        System.debug(getContracts);
        System.assertEquals(getContracts.size(), 1, ASSERT_MSG);
        Test.stopTest();
    }

    /**
    * @description
    * @author Jaime Terrats | 1/21/2020
    * @return void
    **/
    @isTest
    static void testQuestions() {
        final Contract tContract = [Select Id, MX_WB_Producto__r.Name, MX_SB_SAC_FechaNacimiento__c from Contract where MX_SB_SAC_NombreCuentaText__c =: NAME];
        final Case tCase = [Select Id, Reason, MX_SB_SAC_Detalle__c, MX_SB_SAC_Aplicar_Mala_Venta__c, MX_SB_1500_is1500__c from Case where Subject =: NAME];
        tCase.MX_SB_SAC_Aplicar_Mala_Venta__c = 'No';
        tCase.Reason = 'Cancelación';
        Update tCase;
        Test.startTest();
        final List<String> fetchQuestions = MX_SB_SAC_IdentificarCliente.retrieveQuestionnaire(tContract.Id, tContract.MX_WB_Producto__r.Name, tCase);
        System.assertNotEquals(fetchQuestions.size(), 0, 'No trajo preguntas');
        Test.stopTest();
    }

    /**
    * @description
    * @author Jaime Terrats | 1/22/2020
    * @return void
    **/
    @isTest
    static void testQuestions2() {
        Test.startTest();
        final MX_WB_FamiliaProducto__c famProd = [Select Id, Name from MX_WB_FamiliaProducto__c where Name =: System.Label.MX_SB_VTS_Vida];
        famProd.Name = System.Label.MX_SB_VTS_FamiliaASD;
        final Product2 prod = [Select Id, Name, MX_WB_FamiliaProductos__c from Product2 where Name =: System.Label.MX_SB_VTS_Vida];
        prod.Name = System.Label.MX_SB_VTS_PRODUCTO_AUTO_DINAMICO_LBL;
        final MX_SB_VTS_Generica__c generica = [Select MX_SB_SAC_ProductFam__c from MX_SB_VTS_Generica__c where MX_SB_VTS_Type__c =: TYPE];
        generica.MX_SB_SAC_ProductFam__c = System.Label.MX_SB_VTS_FamiliaASD;
        final Case tCase = [Select Id, Reason, MX_SB_SAC_Detalle__c, MX_SB_SAC_Aplicar_Mala_Venta__c, MX_SB_1500_is1500__c from Case where Subject =: NAME];
        tCase.MX_SB_SAC_Aplicar_Mala_Venta__c = 'No';
        tCase.Reason = 'Cancelación';
        Update tCase;
        Update famProd;
        Update prod;
        Update generica;
        final Contract tContract = [Select Id, MX_WB_Producto__c, MX_WB_Producto__r.Name from Contract where MX_SB_SAC_NombreCuentaText__c =: NAME];
        final List<String> fetchQuestions = MX_SB_SAC_IdentificarCliente.retrieveQuestionnaire(tContract.Id, tContract.MX_WB_Producto__r.Name, tCase);
        System.assertNotEquals(fetchQuestions.size(), 0, 'Hacen match');
        Test.stopTest();
    }

    /**
    * @description
    * @author Jaime Terrats | 2/4/2020
    * @return void
    **/
    @isTest
    static void testRetrieveTipifications() {
        final Case getCase = [Select Id, Reason from Case where Subject =: NAME];
        getCase.Reason = 'Servicios';
        Update getCase;
        final Product2 getProd = [Select Id from Product2 where Name =: System.Label.MX_SB_VTS_Vida];
        Test.startTest();
        final List<MX_SB_SAC_CatalogoTipificacion__c> tip = MX_SB_SAC_IdentificarCliente.retrieveTipification(getCase.Id, getProd.Id, TIP_TYPE);
        System.assertNotEquals(tip.size(), 0, 'Didnt get any tipification');
        Test.stopTest();
    }

    @isTest
    static void testCallout() {
        Test.startTest();
        final MX_SB_SAC_LoginToken__c logTok = MX_SB_SAC_UtileriasTestCls.creaCredencial();
        logTok.Name = 'Actualiza Polizas';
        Insert logTok;
        final MX_SB_SAC_ParametrosPoliza__c paramPol = MX_SB_SAC_UtileriasTestCls.creaCSParametros();
        Insert paramPol;
        final MX_WB_Mock mock = new MX_WB_Mock(200,'OK', BODY,new Map<String,String>());
        final MX_WB_Mock mockError = new MX_WB_Mock(500,'ERROR', BODY,new Map<String,String>());
        Test.setMock(HttpCalloutMock.class, mock);
        Test.setMock(HttpCalloutMock.class, mockError);
        try {
            MX_SB_SAC_IdentificarCliente.makeCallout(MOCK_POLICY);
        } catch(AuraHandledException aEx) {
            final Boolean expectedException =  aEx.getMessage().contains(ERR_MSG) ? true : false;
            System.AssertNotEquals(expectedException, false, 'Expected Error');
        }
        Test.stopTest();
    }

    @isTest
    static void testQuestions3() {
        Test.startTest();
        final MX_SB_VTS_Generica__c generica = [Select MX_SB_VTS_Type__c, MX_SB_SAC_ProductFam__c from MX_SB_VTS_Generica__c where MX_SB_VTS_Type__c =: TYPE];
        generica.MX_SB_VTS_Type__c = MV_TYPE;
        generica.MX_SB_SAC_ProductFam__c = 'Generico';
        final Case tCase = [Select Id, Reason, MX_SB_SAC_Detalle__c, MX_SB_SAC_Aplicar_Mala_Venta__c, MX_SB_1500_is1500__c from Case where Subject =: NAME];
        tCase.MX_SB_SAC_Aplicar_Mala_Venta__c = 'Si';
        tCase.Reason = 'Cancelación';
        tCase.MX_SB_1500_is1500__c = false;
        Update tCase;
        Update generica;
        final Contract tContract = [Select Id, MX_WB_Producto__c, MX_WB_Producto__r.Name from Contract where MX_SB_SAC_NombreCuentaText__c =: NAME];
        try {
            MX_SB_SAC_IdentificarCliente.retrieveQuestionnaire(tContract.Id, tContract.MX_WB_Producto__r.Name, tCase);
            MX_SB_SAC_IdentificarCliente.searchProductData(System.Label.MX_SB_VTS_Vida);
            MX_SB_SAC_IdentificarCliente.searchContractOrAccount('TestSearchAccCon');
        } catch(Exception ex) {
            System.assert(ex.getMessage().contains('Attempt to de-reference a null object'), 'Hacen match');
        }
        Test.stopTest();
    }
}