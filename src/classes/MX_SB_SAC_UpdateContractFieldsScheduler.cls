/*
*
* @aBBVA Seguros - SAC Team
* @description This class will schedule MX_SB_SAC_UpdateContractFieldsBatch
*
*           No  |     Date     |     Author               |    Description
* @version  1.0    21/12/2019     BBVA Seguros - SAC Team     Created
* @version  1.0.1  22/12/2019     Jaime Terrats             Remove code smells
*
*/
public without sharing class MX_SB_SAC_UpdateContractFieldsScheduler implements Schedulable {
    /**
    * @description 
    * @author Miguel Hernandez | 12/22/2019 
    * @param schCon 
    * @return void 
    **/
    public void execute(SchedulableContext schCon) {
        final MX_SB_SAC_UpdateContractFieldsBatch bId = new MX_SB_SAC_UpdateContractFieldsBatch();
        Database.executebatch(bId, 200);
    }
}