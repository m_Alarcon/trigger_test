/**-------------------------------------------------------------------------
* Nombre: 		MX_SB_VTS_GetSetAlianzas_PP_Ctrl_tst
* @author 		Alexandro Corzo
* Proyecto: 	Servicio - Obtiene Alianza PP (Controller)
* Descripción : Clase de prueba para la clase encargada de recuperar los datos
*				de la alianza PP
* --------------------------------------------------------------------------
*                         Fecha           Autor                   Desripción
* --------------------------------------------------------------------------
* @version 1.0            05/11/2020      Alexandro Corzo         Creación de la Clase
* @version 1.1            12/01/2021      Alexandro Corzo         Se realizan ajustes a la clase consumo ASO
* --------------------------------------------------------------------------*/
@isTest
public class MX_SB_VTS_GetSetAlianzas_PP_Ctrl_tst {
	/** Variable de Apoyo: sUserName */
    Static String sUserName = 'UserOwnerTest01';
    /** Variable de Apoyo: sURL */
    Static String sUrl = 'http://www.example.com';
    /** Variable de Apoyo: sNoEmpty */
    Static String sNoEmpty = 'Lista No Vacia';
    
    /**
     * @description : Función de configuración para la ejecución
     * 				: de la clase de prueba. 
     * @author: 	 Alexandro Corzo
     */  
    @TestSetup
    static void makeData() {
    	insert new iaso__GBL_Rest_Services_Url__c(Name = 'getListCarInsuranceCatalogs', iaso__Url__c = sUrl, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'getGTServicesSF', iaso__Url__c = sUrl, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
    }
    
    /**
     * @description : Función de prueba para obtener los datos de la alianza PP
     * @author      : Alexandro Corzo
     */
    @isTest static void tstObtLstCarInsCatSrv() {
        final User objUserTst = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(objUserTst) {
            final Map<String, String> mHeadersMock = new Map<String, String>();
            mHeadersMock.put('tsec', '1234567890');
            final MX_WB_Mock objMockCallOut = new MX_WB_Mock(200, 'Complete', '{"iCatalogItem":{"productPlan":[{"catalogItemBase":{"id":"005","name":"HOGAR SEGURO DIGITAL"},"planReview":"001","bouquetCode":"INCE"},{"catalogItemBase":{"id":"005","name":"HOGAR SEGURO DIGITAL"},"planReview":"001","bouquetCode":"MISC"},{"catalogItemBase":{"id":"005","name":"HOGAR SEGURO DIGITAL"},"planReview":"001","bouquetCode":"RCPF"},{"catalogItemBase":{"id":"005","name":"HOGAR SEGURO DIGITAL"},"planReview":"001","bouquetCode":"RIHI"},{"catalogItemBase":{"id":"005","name":"HOGAR SEGURO DIGITAL"},"planReview":"001","bouquetCode":"TECN"},{"catalogItemBase":{"id":"005","name":"HOGAR SEGURO DIGITAL"},"planReview":"001","bouquetCode":"TERR"},{"catalogItemBase":{"id":"008","name":"SEGURO DE HOGAR"},"planReview":"001","bouquetCode":"INCE"},{"catalogItemBase":{"id":"008","name":"SEGURO DE HOGAR"},"planReview":"001","bouquetCode":"MISC"},{"catalogItemBase":{"id":"008","name":"SEGURO DE HOGAR"},"planReview":"001","bouquetCode":"RCPF"},{"catalogItemBase":{"id":"008","name":"SEGURO DE HOGAR"},"planReview":"001","bouquetCode":"RIHI"},{"catalogItemBase":{"id":"008","name":"SEGURO DE HOGAR"},"planReview":"001","bouquetCode":"TECN"},{"catalogItemBase":{"id":"008","name":"SEGURO DE HOGAR"},"planReview":"001","bouquetCode":"TERR"}]}}', mHeadersMock);
            iaso.GBL_Mock.setMock(objMockCallOut);
            Test.startTest();
            	final Map<String, Object> mTestData = MX_SB_VTS_GetSetAlianzas_PP_Ctrl.obtListCarInsCatCtrl('DHSDT003','3002');
            Test.stopTest();
            System.assert(!mTestData.isEmpty(), sNoEmpty);
        }
    }
    
    /**
     * @description : Función de prueba para simular una respuesta diferente a 200
     * @author      : Alexandro Corzo
     */
    @isTest static void tstOtherRspSrv() {
        final User objUsrOR = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(objUsrOR) {
            final Map<String, String> mHeadersMockOR = new Map<String, String>();
            mHeadersMockOR.put('tsec', '0987654321');
            final MX_WB_MOck objMockCallOutOR = new MX_WB_MOCK(503, 'Complete', '', mHeadersMockOR);
            iaso.GBL_Mock.setMock(objMockCallOutOR);
            Test.startTest();
            	final Map<String, Object> mTestRstOR = MX_SB_VTS_GetSetAlianzas_PP_Ctrl.obtListCarInsCatCtrl('DHSDT003','3002');
            Test.stopTest();
            System.assert(!mTestRstOR.isEmpty(), sNoEmpty);
        }
    }
}