/*
* @Nombre: MX_SB_MLT_Vida_Ctrl.cls
* @Autor: Eder Alberto Hernández Carbajal
* @Proyecto: Siniestros - BBVA
* @Descripción : Clase de la capa de presentación que almacena los methods usados en la Toma de Reporte de Siniestro Vida
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   	Descripción
* --------------------------------------------------------------------------------
* 1.0         26/05/2020    Eder Alberto Hernández Carbajal		Creación
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_CrearServicios_Ctrl {
    
    /** Lista de registros de Catalogos Claim */
    Final static List<MX_SB_MLT_Catalogos_Claim__c> CATCLAIM_DATA = new List<MX_SB_MLT_Catalogos_Claim__c>();
    
    /** Lista de registros de Catalogos Claim */
    Final static List<Siniestro__c> SIN_DATA = new List<Siniestro__c>();
    
    /** constructor */
    @TestVisible
    private MX_SB_MLT_CrearServicios_Ctrl() { }
    
    /*
    * @description METHODO busqueda siniestro vida
    * @param String siniId
    */
    @AuraEnabled
    public static List<Siniestro__c> searchSinVida (String siniId) {
         try {
            if(String.isNotBlank(siniId)) {
                final Set<Id> SinIds = new Set<Id>{siniId};
                SIN_DATA.addAll(MX_RTL_Siniestro_Service.getSiniestroData(SinIds));
            }
        } catch(QueryException sinEx) {
            throw new AuraHandledException(System.Label.MX_WB_LG_ErrorBack + sinEx);
        }
        return SIN_DATA;
    }
    /*
    * @description Busqueda en catalogos
    * @param String descripcion, String tipo
    */
    @AuraEnabled
    public static List<MX_SB_MLT_Catalogos_Claim__c> getCatalogData(String descripcion, String tipo) {
        try {
            if(String.isNotBlank(descripcion) && String.isNotBlank(tipo)) {
				CATCLAIM_DATA.addAll(MX_SB_MLT_Catalogos_Claim_Service.getCatalogByDesc(descripcion, tipo));
            }
        } catch(QueryException queryEx) {
            throw new AuraHandledException(System.Label.MX_WB_LG_ErrorBack + queryEx);
        }
        return CATCLAIM_DATA;
    }
    /*
    * @description Se inserta información del fallecido
    * @param Siniestro__c fallecido
    */
    @AuraEnabled
    public static void updateSiniestro(Siniestro__c sin) {
        MX_RTL_Siniestro_Service.actualizarSin(sin);
    }
    /*
    * @description Acciones flujo vida
    * @param String sinId
    */
    @AuraEnabled
    public static void siniestroAction(String siniestroAct, String sinId) {
        try {
            if(String.isNotBlank(siniestroAct) && String.isNotBlank(sinId)) {
                final Set<Id> sinIds = new Set<Id>{sinId};
                MX_RTL_Siniestro_Service.sinAction(siniestroAct,sinIds);
            }
        } catch(QueryException sinEx) {
            throw new AuraHandledException(System.Label.MX_WB_LG_ErrorBack + sinEx);
        }
	}
}