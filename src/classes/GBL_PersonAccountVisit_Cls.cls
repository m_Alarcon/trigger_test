/**
* @author bbva.com developers
* @date 2019
*
* @group global_hub_kit_visit
*
* @description Class for Person account assigned contact
**/
public with sharing class GBL_PersonAccountVisit_Cls {
    /*Constructor*/
    @TestVisible private GBL_PersonAccountVisit_Cls() {}
    /*Metodo  relateContactVisit*/
    public static void relateContactVisit(List<dwp_kitv__Visit__c> newVisits) {
        List<dwp_kitv__Visit__c> listVisitPA = new List<dwp_kitv__Visit__c>();//NOSONAR
        for(dwp_kitv__Visit__c visita : newVisits) {
            System.debug('visitaPA ' +  visita.MX_TieneCuentaPersonal__c);
            if(visita.MX_TieneCuentaPersonal__c == true) {
                listVisitPA.add(visita);
            }
        }
        if(!listVisitPA.isEmpty()) {
            addContactToVisit(listVisitPA);
        }
    }
    /*Metodo addContactToVisit*/
    public static void addContactToVisit(List<dwp_kitv__Visit__c> newVisits) {
        final Id userId = Userinfo.getProfileId();
        final PersonAccountVisitKitPermissions__c vkp = PersonAccountVisitKitPermissions__c.getInstance(userId);
        if(GBL_PersonAccountCompatibility_Cls.isPersonAccountEnabled() && vkp != null && vkp.EnablePersonAccount__c ) {
            Set<Id> accountIds = new Set<Id>();
            accountIds = getSetAccount(newVisits);
            final String contactsQuery = 'Select Id, PersonContactId from Account where Id IN :accountIds and isPersonAccount = true';//NOSONAR
            final Map<Id,Account> personAccounts = new Map<Id,Account>((List<Account>)Database.query(contactsQuery));
            List<dwp_kitv__Visit_Contact__c> visitContacts = new List<dwp_kitv__Visit_Contact__c>();
            System.debug('personAccounts '+personAccounts);
            if(!personAccounts.isEmpty()) {
                visitContacts = getListVisitContacts(newVisits, personAccounts);
            }
            System.debug('visitContacts '+ visitContacts);
            if(!visitContacts.isEmpty()) {
                insert visitContacts;
            }
        }
    }
    /*Metodo getSetAccount*/
    public static Set<Id> getSetAccount(List<dwp_kitv__Visit__c> newVisits) {
        final Set<Id> accountIds = new Set<Id>();
        for(dwp_kitv__Visit__c visit : newVisits) {
            accountIds.add(visit.dwp_kitv__account_id__c);
        }
        return accountIds;
    }
        /*Metodo getListVisitContacts*/

    public static List<dwp_kitv__Visit_Contact__c> getListVisitContacts(List<dwp_kitv__Visit__c> newVisits, Map<Id,Account> personAccounts) {
        final List<dwp_kitv__Visit_Contact__c> visitContacts = new List<dwp_kitv__Visit_Contact__c>();
        dwp_kitv__Visit_Contact__c newVisitContact;
        for(dwp_kitv__Visit__c visit : newVisits) {
            if(personAccounts.containsKey(visit.dwp_kitv__account_id__c)) {
                newVisitContact = new dwp_kitv__Visit_Contact__c();
                newVisitContact.dwp_kitv__visit_id__c = visit.Id;
                newVisitContact.dwp_kitv__contact_id__c = (Id)personAccounts.get(visit.dwp_kitv__account_id__c).get('PersonContactId');
                visitContacts.add(newVisitContact);
            }
        }
        return visitContacts;
    }
}