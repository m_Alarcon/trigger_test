/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 10-12-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   09-23-2020   Eduardo Hernández Cuamatzi   Initial Version
**/
@isTest
public class MX_SB_VTS_PaymentModule_Service_Test {
    /**@description etapa Cotizacion*/
    private final static string COTIZACION = 'Cotización';

    /**Folio cotización*/
    private final static string FOLIOCOT = '9999999';
    /**Tipo de domicilio */
    public final static String DOMTYPE = 'Domicilio asegurado';
    
    @testSetup static void setup() {
        MX_SB_VTS_CallCTIs_utility.initHogarGeneric();
    }

    @isTest 
    static void fillAddress() {
        
        Test.startTest();
        final Opportunity oppRecId = [Select Id, Name, AccountId, SyncedQuoteId from Opportunity where StageName =: COTIZACION];
        final Quote quoteData = [Select Id, Name,BillingAddress from Quote where OpportunityId =: oppRecId.Id];
        quoteData.MX_SB_VTS_ASO_FolioCot__c = FOLIOCOT;
        quoteData.BillingCity = 'billing city';
        quoteData.BillingCountry = 'billing country';
        quoteData.BillingPostalCode = '9999';
        quoteData.BillingState = 'billing state';
        quoteData.BillingStreet = 'billing street';
        quoteData.MX_SB_VTS_GCLID__c = '1|2|3|4';
        update quoteData;
        final MX_RTL_MultiAddress__c addressType = new MX_RTL_MultiAddress__c();
        addressType.MX_RTL_MasterAccount__c = oppRecId.AccountId;
        addressType.MX_RTL_QuoteAddress__c = quoteData.Id;
        addressType.MX_RTL_AddressStreet__c = 'test';
        addressType.MX_RTL_AddressType__c = DOMTYPE;
        insert addressType;
        final Quote quoteDataPross = [Select Id, Name,BillingAddress,MX_SB_VTS_Numero_Exterior__c,MX_SB_VTS_Numero_Interior__c from Quote where OpportunityId =: oppRecId.Id];
        final String addresStr = MX_SB_VTS_PaymentModule_Service.fillAddress(quoteDataPross, DOMTYPE);
        System.assert(String.isNotEmpty(addresStr), 'Estatus correcto');
        Test.stopTest();
    }

    @isTest 
    static void fillDataPayment() {
        Test.startTest();
        final Opportunity oppRec = [Select Id, Name, SyncedQuoteId from Opportunity where StageName =: COTIZACION];
        final Quote quoData = [Select Id, Name,MX_SB_VTS_GCLID__c, MX_SB_VTS_TotalMonth__c, MX_SB_VTS_TotalQuarterly__c, MX_SB_VTS_TotalSemiAnual__c,MX_SB_VTS_TotalAnual__c from Quote where OpportunityId =: oppRec.Id];
        quoData.MX_SB_VTS_GCLID__c = '1|2|3|4';
        update quoData;
        syncQuoteData(oppRec, quoData.Id);
        final String oppRecStr = oppRec.Id;
        final List<MX_SB_VTS_PaymentModule_Ctrl.paymentResum> lstPaymentsRem = MX_SB_VTS_PaymentModule_Service.fillDataPayment(oppRecStr);
        System.assertEquals(lstPaymentsRem.size(), 5, 'Lista de valores');
        Test.stopTest();
    }
    
    @isTest 
    static void findDataCodeVals() {
        Test.startTest();
            final List<MX_RTL_IVRCodes__mdt> lstIvrCode = MX_SB_VTS_PaymentModule_Service.findDataCodeVals('3003');
            System.assertEquals(lstIvrCode[0].MX_RTL_TitleMsg__c, 'Tarjeta sin fondos suficientes', 'Mensaje traducido');
        Test.stopTest();
    }

    /**Recupera Mock de Iaso */
    public static String findMock(String developerName) {
        return [Select Id, iaso__Mock_LTA__c from iaso__GBL_integration_service__mdt where DeveloperName =: developerName].iaso__Mock_LTA__c;
    }

    
    /**
    * @description Sincroniza quote
    * @author Eduardo Hernández Cuamatzi | 09-23-2020 
    * @param oppRecId oportunidad
    * @param quoteId quote
    **/
    public static void syncQuoteData(Opportunity oppRecId, String quoteId) {
        oppRecId.SyncedQuoteId = quoteId;
        update oppRecId;
    }
}