/**
* Name: MX_SB_VTS_UpNameCampaign_Service_tst
* @author Ángel Lavana Rosas
* Description : New Class to test class MX_SB_VTS_UpNameCampaign_Service, create records with Name _CPN
*				and records without _CPN
* Ver                  Date            Author                   Description
* @version 1.0         Jun/02/2020     Ángel Lavana Rosas       Initial Version
**/
@isTest
public class MX_SB_VTS_UpNameCampaign_Service_tst {
    /* @Description: Create test data set
	*/
    @isTest
    public static void nameCampaignServiceCPN() {
        final List<Id> leadsId2 = new List<Id>{}; 
            final List<lead> leads2 = new List<Lead>{};    
                
                for(integer contador2 = 0; contador2<=5; contador2++) {  
                    final Lead candidato2 = new Lead(FirstName = 'Campaña' + contador2, 
                                              LastName = 'prueba' + contador2,  
                                              MX_SB_VTS_CampaFaceName__c = 'SEGUROS_WIBE BBVA_CPNWHOOP0' + contador2 ,
                                              MX_SB_VTS_CodCampaFace__c = '',
                                              MX_SB_VTS_DetailCampaFace__c = 'ANUNCIO_POR FACEBOOK ' + contador2);
                    leads2.add(candidato2);
                }
        for(integer contador2 = 0; contador2<=5; contador2++) {    
            final Lead candidato2 = new Lead(FirstName = 'Campaña' + contador2, 
                                      LastName = 'prueba' + contador2,  
                                      MX_SB_VTS_CampaFaceName__c = 'SEGUROS_WIBE BBVA' + contador2 ,
                                      MX_SB_VTS_CodCampaFace__c = '',
                                      MX_SB_VTS_DetailCampaFace__c = 'ANUNCIO_POR FACEBOOK ' + contador2);
            leads2.add(candidato2); 
        }
        insert leads2;   
        for(Lead get2 : leads2) {
            leadsId2.add(get2.Id);
        }
        final List<Lead> valor = MX_SB_VTS_UpNameCampaign_Service.NameCampaignService(leadsId2);
        System.AssertNotEquals(leads2,valor);
    }
}