/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpCotizadorVida
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
* 1.0					Julio Medellín                 27/07/2020     Creación del wrapper.
* 1.1                  Daniel Lopez                    14/12/2020    Se añade constructor subclases validityPeriod, 
* 1.2                  Daniel Lopez                    14/12/2020       paymentWay, coverages,catalogItemBase
*/
 @SuppressWarnings('sf:ShortVariable,sf:LongVariable,sf:ShortClassName,sf:VariableDeclarationHidesAnother')
public class MX_SB_PS_wrpCotizadorVida {
	  /*Public property for wrapper*/
	public technicalInformation technicalInformation {get;set;}
	  /*Public property for wrapper*/
	public string id {get;set;} 
	  /*Public property for wrapper*/
	public productPlan productPlan {get;set;}
	  /*Public property for wrapper*/
	public validityPeriod validityPeriod {get;set;}
	  /*Public property for wrapper*/
	public rateQuote[] rateQuote {get;set;}
	  /*Public property for wrapper*/
	public particularData[] particularData {get;set;}
	  /*Public property for wrapper*/
	public insuredList[] insuredList {get;set;}
	  /*Public property for wrapper*/
	public String region {get;set;}
	    /*Public constructor for wrapper*/
    public  MX_SB_PS_wrpCotizadorVida() {
        this.technicalInformation = new technicalInformation();
        this.productPlan = new productPlan();
        this.validityPeriod = new validityPeriod();
        this.rateQuote =  new rateQuote[] {};
        this.particularData = new particularData[] {};
		particularData.add(new particularData('','','',''));
        this.insuredList = new insuredList[] {};
		insuredList.add(new insuredList());
		this.id = '';
    }
	  /*Public subclass for wrapper*/
    public class technicalInformation {
	     /*Public property for wrapper*/
		public String dateRequest {get;set;}	
		  /*Public property for wrapper*/
		public String technicalChannel {get;set;}
           /*Public property for wrapper*/		
		public String technicalSubChannel {get;set;}	
		  /*Public property for wrapper*/
		public String branchOffice {get;set;}
            /*Public property for wrapper*/		
		public String managementUnit {get;set;}	
		  /*Public property for wrapper*/
		public String user {get;set;}	
		  /*Public property for wrapper*/
		public String technicalIdSession {get;set;}	
		  /*Public property for wrapper*/
		public String idRequest {get;set;}
           /*Public property for wrapper*/		
		public String dateConsumerInvocation {get;set;}
		 /*public constructor subclass*/
		public technicalInformation() {
		 this.dateRequest = '2016-08-06 12:33:27.104';
		 this.technicalChannel = '4';
		 this.technicalSubChannel = '71';
		 this.branchOffice = '23';
		 this.managementUnit = 'VVSH0001';
		 this.user = 'CARLOS';
		 this.technicalIdSession = '3232-3232';
		 this.idRequest = '1212-121212-12121-212';
		 this.dateConsumerInvocation = '2016-08-06 12:33:27.1';
		}
	}
	  /*Public subclass for wrapper*/
	public class id {
	}
	  /*Public subclass for wrapper*/
	public class productPlan {
	    /*Public property for wrapper*/
		public String productCode {get;set;}	
		/*Public property for wrapper*/
		public String planReview {get;set;}
        /*Public property for wrapper*/		
		public planCode planCode {get;set;}
		/*Public property for wrapper*/
		public String bouquetCode {get;set;}	
		 /*public constructor subclass*/
		public productPlan() {
		 this.productCode = '';
		 this.planReview = '';
		 this.planCode = new planCode('');
		 this.bouquetCode = '';
		}
	}
	  /*Public subclass for wrapper*/
	public class planCode {
	   /*Public property for wrapper*/
		 public string id {get;set;} 
		 /*public constructor subclass*/
		public planCode(String plancode) {
		 this.id = plancode;
		}
	}
	  /*Public subclass for wrapper*/
	public class validityPeriod {
	     /*Public property for wrapper*/
		public String startDate {get;set;}	
		  /*Public property for wrapper*/
		public String endDate {get;set;}	
		  /*Public property for wrapper*/
		public type type {get;set;}
		 /*public constructor subclass*/
		 public validityPeriod() {
			Datetime perioddate = System.now();
			perioddate = perioddate.addDays(1);
			this.startDate=DateTime.newInstance(perioddate.year(),perioddate.month(),perioddate.day()).format('yyyy-MM-dd');
			perioddate= perioddate.addYears(1);
			this.endDate=DateTime.newInstance(perioddate.year(),perioddate.month(),perioddate.day()).format('yyyy-MM-dd');
			this.type = new type('A','ANUAL');
		}
	}
	  /*Public subclass for wrapper*/
	public class type {
	  /*Public property for wrapper*/
		 public string id {get;set;} 
      /*Public property for wrapper*/		
		public String name {get;set;}
         /*public constructor subclass*/
		 public type(String id, String name) {
			this.id = id;
			this.name = name;
		}		
	}
	/*Public subclass for wrapper*/
	public class rateQuote {
	   /*Public property for wrapper*/
		public paymentWay paymentWay {get;set;}
		 /*public constructor subclass*/
		public rateQuote() {
		 this.paymentWay = new paymentWay();
		}
	}
	  /*Public subclass for wrapper*/
	public class paymentWay {
	  /*Public property for wrapper*/
		 public string id {get;set;} 
	  /*Public property for wrapper*/
		public String name {get;set;}
 /*public constructor subclass*/
		public paymentWay() {
		 this.id = '';
		 this.name = '';
		}	
		/*public constructor subclass*/
		public paymentWay(String idval,String nameval) {
			this.id = idval;
			this.name = nameval;
		   }
	}
	  /*Public subclass for wrapper*/
	public class particularData {
	    /*Public property for wrapper*/
		public String aliasCriterion {get;set;}
          /*Public property for wrapper*/		
		public transformer transformer {get;set;}
		  /*Public property for wrapper*/
		public String peopleNumber {get;set;}
         /*public constructor subclass*/
		public particularData(String alcriterion,String id, String name, String peopnum) {
		 this.aliasCriterion = alcriterion;
		 this.transformer = new Transformer(id,name);
		 this.peopleNumber = peopnum;
		}		
	}
	  /*Public subclass for wrapper*/
	public class transformer {
	    /*Public property for wrapper*/
		 public string id {get;set;} 	
		/*Public property for wrapper*/
		public String name {get;set;}
		 /*public constructor subclass*/
		public transformer(String id, String name) {
		 this.id = id;
		 this.name =name;
		}
	}
	  /*Public subclass for wrapper*/
	public class insuredList {
	     /*Public property for wrapper*/
		public coverages[] coverages {get;set;}
		 /*public constructor subclass*/
		public insuredList() {
		 this.coverages = new coverages[] {};
		}
	}
	  /*Public subclass for wrapper*/
	public class coverages {
	     /*Public property for wrapper*/
		public catalogItemBase catalogItemBase {get;set;}
		  /*Public property for wrapper*/
		public String peopleNumber {get;set;}
         /*public constructor subclass*/
		public coverages() {
		 this.catalogItemBase = new catalogItemBase();
		 this.peopleNumber = '';
		}
		 /*public constructor subclass*/
		 public coverages(catalogItemBase catib, String peopn) {
			this.catalogItemBase = catib;
			this.peopleNumber = peopn;
		   }
	}
	  /*Public subclass for wrapper*/
	public class catalogItemBase {
	     /*Public property for wrapper*/
		 public string id {get;set;} 
		 /*public constructor subclass*/
		public catalogItemBase() {
		 this.id = '';
		}
		/*public constructor subclass*/
		public catalogItemBase(String catidval) {
			this.id = catidval;
		   }
	}
	
	
}