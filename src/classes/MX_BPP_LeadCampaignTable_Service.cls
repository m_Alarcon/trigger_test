/**
* @File Name          : MX_BPP_LeadCampaignTable_Service.cls
* @Description        :
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 01/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      01/06/2020            Gabriel Garcia Rojas          Initial Version
**/
public class MX_BPP_LeadCampaignTable_Service {

    /**Constant method fetchCampaignMember*/
    final static String QUERYFIELDUSER = ' Id, Name ';
    /**Constant banca variable */
    final static String BANCAUSER = 'Red BPyP';
    /**Constant Profile Estandar variable */
    final static String PROFILESTD = 'BPyP Estandar';
    /**Constant method fetchListSucursales*/
    final static String QUERYFIELDSUC = ' BPyP_ls_NombreSucursal__c ';
    /**Constant method fetchListSucursales*/
    final static String QUERYFILTERSUC = ' WHERE DivisionFormula__c =: filtro0 AND VP_ls_Banca__c =: filtro2 AND IsActive = true AND Profile.Name Like: filtro3';
    /**Constant method fetchListDivisiones*/
    final static String QUERYFIELDDIV = ' DivisionFormula__c ';
        /**Constant method fetchListDivisiones*/
    final static String QUERYFILTERDIV = ' WHERE VP_ls_Banca__c =:filtro2 AND  DivisionFormula__c != null AND BPyP_ls_NombreSucursal__c !=null AND Profile.Name =: filtro3 ';

    /** Constructor*/
    @testVisible
    private MX_BPP_LeadCampaignTable_Service() { }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return List<User>
    **/
    public static List<User> listUser() {
        final Set<Id> userId = new Set<Id>{UserInfo.getUserId()};
        final Id role = UserInfo.getUserRoleId();

        final List<User> managerUser = MX_RTL_User_Selector.getListUserById(userId);
        final Set<Id> staffRole = MX_RTL_UserRole_Selector.getUserRoleLikeName('%BPYP DIRECTOR OFICINA %').keySet();
        final Set<Id> bankerRole = MX_RTL_UserRole_Selector.getUserRoleLikeName('%BPYP BANQUERO %').keySet();
        return MX_BPP_LeadCampaignTable_Helper.getListUderByUserAndRole(staffRole, bankerRole, role, managerUser);
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return List<CampaignMember>
    **/
    public static List<CampaignMember> getInfoMembersByFilter(List<String> param, List<User> listUsers) {
        final String queryString = 'Id, LeadId, '+
        +'Lead.Name, Lead.MX_WB_RCuenta__c, Lead.MX_WB_RCuenta__r.Name, Lead.Description, Lead.LeadSource, Lead.MX_LeadAmount__c, MX_LeadEndDate__c, Lead.Status, Lead.OwnerId, Lead.Owner.Name, '+
        +'Campaign.MX_WB_FamiliaProductos__c, Campaign.MX_WB_FamiliaProductos__r.Name, Campaign.MX_WB_Producto__c, Campaign.MX_WB_Producto__r.Name, Status ';
        String filter = '';
        final String converted = 'false';
        String productLocal = param[4];

        if(String.isBlank(param[3])) {
            productLocal = '';
        }

        final String accountString = 'Lead.MX_WB_RCuenta__r.Name LIKE \'%' + param[0] + '%\'';
        final String leadString = 'Lead.Name LIKE \'%' + param[1] + '%\'';
        final String originString = 'Lead.LeadSource = \'' + param[2] + '\'';
        final String familyString = 'Campaign.MX_WB_FamiliaProductos__r.Name = \'' + param[3] + '\'';
        final String productString = 'Campaign.MX_WB_Producto__r.Name = \'' + productLocal + '\'';
        final String amountString = 'Lead.MX_LeadAmount__c >= MXN' + param[5];
        final String amountStringTwo = 'Lead.MX_LeadAmount__c <= MXN' + param[6];
        final String endDateString = 'MX_LeadEndDate__c >= ' + param[7];
        final String endDateStringTwo = 'MX_LeadEndDate__c <= ' + param[8];
        final String statusString = 'Status = \'' + param[9] + '\'';
        final String convertedString = ' Campaign.MX_WB_Producto__r.Banca__c includes (\'' + BANCAUSER + '\') AND Status != \'Vencida\' AND Lead.IsConverted = FALSE ';

        final List<String> paramList = new List<String>{param[0], param[1], param[2], param[3], productLocal, param[5], param[6], param[7], param[8], param[9], converted};

        final List<String> filterList = new List<String>{accountString, leadString, originString, familyString, productString, amountString, amountStringTwo, endDateString, endDateStringTwo, statusString, convertedString};

        filter = MX_BPP_LeadCampaignTable_Helper.setFilterToCampaignMember(paramList, filterList);
        if(listUsers.isEmpty()) {
            filter += ' AND Lead.OwnerId = \'' + param[10] + '\'';
        } else {
            filter += ' AND Lead.OwnerId IN: listUsers ';
        }

        return MX_RTL_CampaignMember_Selector.getListCampaignMemberByDatabase(queryString, filter, listUsers);
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return List<String>
    **/
    public static List<String> familyDataBySchema() {
        final List<String> pLValuesList = new List<String>();
        final Schema.DescribeFieldResult fieldResult = Opportunity.MX_RTL_Familia__c.getDescribe();
        final List<Schema.PicklistEntry> pickListElement = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry pickListVal : pickListElement) {
            pLValuesList.add(pickListVal.getLabel());
        }

        return pLValuesList;
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return List<String>
    **/
    public static List<String> statusDataBySchema() {
        final List<String> pLValuesList= new List<String>();
        final Schema.DescribeFieldResult fieldResult = Lead.Status.getDescribe();
        final List<Schema.PicklistEntry> pickListElement = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : pickListElement) {
            pLValuesList.add(pickListVal.getLabel());
        }
        return pLValuesList;
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return Map<Object,List<String>
    **/
    public static Map<Object,List<String>> getOppPickListProductValue(String picklistObject) {
        return MX_BPP_LeadCampaignTable_Helper.dependentValuesGet(picklistObject);
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return Opportunity
    **/
    public static Opportunity getOppFromLeadConv(Id leadId) {
        final List<Id> leadsId = new List<Id>();
        leadsId.add(leadId);
        final Map<String,String> mapLeadOpp = MX_BPP_ConvertLeads_Service.convertToLead(leadsId);
        final String fields = 'Id, Name';
        Opportunity oOpp = new Opportunity();
        try {
            oOpp = MX_RTL_Opportunity_Selector.getOpportunity(mapLeadOpp.get(leadId), fields);
        } catch(AuraHandledException err) {
            throw new AuraHandledException(Label.MX_WB_lbl_MsgConvertException + err);
        }


        return oOpp;
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return List<String>
    **/
    public static List<String> fetchListSucursales(String division) {
        final List<String> listFilters = new List<String>{division, '', BANCAUSER, PROFILESTD};

        final Set<String> setSucursales = new Set<String>();
        for(User usuario : MX_RTL_User_Selector.fetchListUserByDataBase(QUERYFIELDSUC, QUERYFILTERSUC, listFilters)) {
            setSucursales.add(usuario.BPyP_ls_NombreSucursal__c);
        }
        return new List<String>(setSucursales);
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return List<String>
    **/
    public static List<String> fetchListDivisiones() {
        final List<String> listFilters = new List<String>{'', '', BANCAUSER, PROFILESTD};

        final Set<String> setDivisiones = new Set<String>();
        for(User usuario : MX_RTL_User_Selector.fetchListUserByDataBase(QUERYFIELDDIV, QUERYFILTERDIV, listFilters)) {
            setDivisiones.add(usuario.DivisionFormula__c);
        }
        return new List<String>(setDivisiones);
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return List<String>
    **/
    @SuppressWarnings('sf:unusedVariable')
    public static List<User> fetchCampaignMember(List<String> params) {
        List<String> listFilters;
        String queryFilters = '';
        List<User> listUsers;
        if(String.isBlank(params[0]) && String.isBlank(queryFilters)) {
            listFilters = new List<String>{'', '', BANCAUSER, PROFILESTD};
            queryFilters = ' WHERE VP_ls_Banca__c =:filtro2 AND Profile.Name =: filtro3  AND IsActive = true Order By Name ';
            listUsers = MX_RTL_User_Selector.fetchListUserByDataBase(QUERYFIELDUSER, queryFilters, listFilters);
        } else if(String.isNotBlank(params[0]) && String.isBlank(params[1])) {
            listFilters = new List<String>{'', params[0], BANCAUSER, PROFILESTD};
            queryFilters = ' WHERE VP_ls_Banca__c =:filtro2 AND Profile.Name =: filtro3 AND DivisionFormula__c =: filtro1  AND IsActive = true Order By Name ';
            listUsers = MX_RTL_User_Selector.fetchListUserByDataBase(QUERYFIELDUSER, queryFilters, listFilters);
        } else if(String.isNotBlank(params[1]) && String.isBlank(params[2])) {
            listFilters = new List<String>{params[1], '', BANCAUSER, PROFILESTD};
            queryFilters = ' WHERE VP_ls_Banca__c =:filtro2 AND Profile.Name =: filtro3 AND BPyP_ls_NombreSucursal__c =: filtro0  AND IsActive = true Order By Name ';
            listUsers = MX_RTL_User_Selector.fetchListUserByDataBase(QUERYFIELDUSER, queryFilters, listFilters);
        } else if(String.isNotBlank(params[2])) {
            listUsers = new List<User>{new User(Id = params[2])};
        }
        return listUsers;
    }
}