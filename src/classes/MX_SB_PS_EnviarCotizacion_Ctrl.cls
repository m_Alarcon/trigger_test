/**
* @File Name          : MX_SB_PS_EnviarCotizacion_Ctrl.cls
* @Description        :
* @Author             : Juan Carlos Benitez
* @Group              :
* @Last Modified By   : Juan Carlos Benitez
* @Last Modified On   : 9/17/2020, 19:00:03 
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    9/17/2020   Juan Carlos Benitez          Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_SB_PS_EnviarCotizacion_Ctrl {
/** list of records to be retrieved */
    final static List<Contact> CNTCT_DATA = new List<Contact>();
/** list of records to be retrieved */
    final static List<QuoteLineItem> QUOTELI = new List<QuoteLineItem>();
/**
* @description
* @author Juan Carlos Benitez | 10/25/2020
* @param oppId
* @return Opportunity
**/
    @AuraEnabled
    public static Opportunity loadOppE(String oppId) {
        return MX_SB_PS_Utilities.fetchOpp(oppId);
    }
    /**
* @description
* @author Juan Carlos Benitez | 10/25/2020
* @param oppId
* @return List<Contact>
**/
    @AuraEnabled
    public static List<Contact> loadContactAcc(Id accId) {
        if(String.isNotBlank(accId)) {
            final Set<Id> ids = new Set<Id>{accId};
                CNTCT_DATA.addAll(MX_RTL_Contact_Service.getContactData(ids));
        }
        return CNTCT_DATA;
    }
/**
* @description
* @author Juan Carlos Benitez | 10/26/2020
* @param quoteId
* @return Quote
**/
    @AuraEnabled
    public static List<QuoteLineItem> getQuoteLneItem(String quoteId) {
        if(String.isNotBlank(quoteId)) {
            final Set<Id> ids = new Set<Id>{quoteId};
                QUOTELI.addAll(MX_RTL_Quote_Service.selSObjectsByQuotes(ids));
        }
        return QUOTELI;
    }
    /**
* @description
* @author Juan Carlos Benitez | 10/26/2020
* @param oppId
* @return Map<String, String>
**/
    @AuraEnabled(cacheable=true)
    public static string sendCotService(Map<String,String> mapdata) {
        String rtrn='';
        HttpResponse resData = new HttpResponse();
        try {
            resData = MX_SB_PS_IASO_Service_cls.getServiceResponseMap('SendDocumentByEmail_PS',mapdata);
            rtrn=''+resData.getStatusCode();
        } catch(Exception e) {
            rtrn='ERROR: '+e.getMessage();
        }
        return rtrn;
    }
}