@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_RTL_Task_Selector_Test
* Autor: Juan Carlos Benitez Herrera
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_RLT_Task_Selector

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         26/05/2020   Juan Carlos Benitez Herrera               Creación
* 1.1         11-18-2020   Daniel Ramirez Islas                      Last task Select y upsertTaskTest
* -----------------------------------------------------------------------------------------------
*/
public class MX_RTL_Task_Selector_Test {
/*
* var String Valor Subramo Ahorro
*/
public static final String SUBRAMO ='Fallecimiento';

/** Nombre de usuario Test */
final static String NAME = 'Panchito Perez';
    
/** current user id */
final static String USR_ID = UserInfo.getUserId();
/*Usuario de pruebas*/
private static User userTst = new User();

    /** Boolean for validate if a record is Upserted */
    private static Boolean isUpsTst = true;

/**
* @description Test SetUp
* Author: Juan Carlos Benitez Herrera
 */
@TestSetup
static void createData() {
Final String profileVida = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
final User userVida = MX_WB_TestData_cls.crearUsuario(NAME, profileVida);  
insert userVida;
System.runAs(userVida) {
    final String rcdTpeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Proveedores_MA).getRecordTypeId();
    final Account accVidaTst = new Account(RecordTypeId = rcdTpeId, Name=NAME, Tipo_Persona__c='Física');
    insert accVidaTst;

    final Contract cntrctVida = new Contract(AccountId = accVidaTst.Id, MX_SB_SAC_NumeroPoliza__c='policyTest');
    insert cntrctVida;
    Final List<Siniestro__c> sinInsert = new List<Siniestro__c>();
    for(Integer i = 0; i < 10; i++) {
        Final Siniestro__c nSin = new Siniestro__c();
        nSin.MX_SB_SAC_Contrato__c = cntrctVida.Id;
        nSin.MX_SB_MLT_NombreConductor__c = NAME + '_' + i;
        nSin.MX_SB_MLT_APaternoConductor__c = 'LastName for '+NAME;
        nSin.MX_SB_MLT_AMaternoConductor__c = 'LastName2 for '+NAME;
        nSin.MX_SB_MLT_Telefono__c = '5534253647';
        nSin.MX_SB_MLT_Fecha_Hora_Siniestro__c =date.valueof('2020-03-27T22:04:00.000+0000');
        nSin.MX_SB_MLT_SubRamo__c = SUBRAMO;
        nSin.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_creacion;
        nSin.TipoSiniestro__c = 'Siniestros';
        nSin.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoVida).getRecordTypeId();
        sinInsert.add(nSin);
    }
    Database.insert(sinInsert);
   
	Final List<Task> tskInsert = new List<Task>();
    		for(Integer i = 0; i < 10; i++) {
                Final Task ntsk = new Task();
            ntsk.whatId =sinInsert[i].Id;
            ntsk.Telefono__c = '5534253647';
	        ntsk.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_PureCloud').getRecordTypeId();
    	    tskInsert.add(ntsk);
    	}
    	Database.insert(tskInsert);
		Final List<Task> tskInsertCita = new List<Task>();
    	for(Integer i = 0; i < 10; i++) {
                Final Task ntskC = new Task();
            ntskC.Telefono__c = '5534253647';
            ntskC.ReminderDateTime=date.valueof('2020-03-27T22:04:00.000+0000');
            ntskC.whatId =sinInsert[i].Id;
	        ntskC.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_CitaSiniestros').getRecordTypeId();
    	    tskInsertCita.add(ntskC);
	}
    	Database.insert(tskInsertCita);
}
}
/*
* @description method que prueba MX_RTL_Task_Selector getTaskDataByWhatId
* @param  void
* @return String 
*/
@isTest
	static void testgetTaskDataById() {
	final List<Siniestro__c>  ids =[SELECT Id from Siniestro__c where  MX_SB_MLT_NombreConductor__c =:  NAME + '_1'];
	Final List<Task> listTsk = [SELECT Id, WhatId,CreatedDate
                   FROM Task WHERE WhatId in :ids and recordtype.DeveloperName='MX_SB_MLT_PureCloud'];
	Final List<Task> listTskC = [SELECT Id, WhatId,CreatedDate,ReminderDateTime
                   FROM Task WHERE WhatId in :ids and recordtype.DeveloperName='MX_SB_MLT_CitaSiniestros'];
	Final Set<Id> sinIds = new Set<Id>();
	for(Task tsk : listTsk) {
	    sinIds.add(tsk.Id);
	}
	test.startTest();
	   MX_RTL_Task_Selector.getTaskDataById(sinIds);
	   MX_RTL_Task_Selector.updateTask(listTsk[0], 'PureCloud');
	   MX_RTL_Task_Selector.updateTask(listTskC[0], 'Cita');
       MX_RTL_Task_Selector.updateTask(listTsk[0], 'Llamada');
	test.stopTest();
	system.assert(true, 'Consulta exitosa');
}
    /*
    * @description method que prueba MX_RTL_Task_Selector insertTask
    * 
    */
    static testMethod void insertTaskTest () {        
        userTst = [SELECT Id, Name FROM User WHERE Name =: NAME];
        system.runAs(userTst) {
            Test.startTest();
                Final datetime getValueDate = datetime.now();
                Final task taskTesting = new Task();
                taskTesting.Subject = 'Llamada Realizada ' + getValueDate;
            	taskTesting.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('MX_SB_VTA_Ventas_Asistida').getRecordTypeId();
                Final List<Task> listTest = new List<Task> {taskTesting};
                MX_RTL_Task_Selector.insertTask(listTest);
            Test.stopTest();
            System.assert(true,'Tarea insertada');
        }
        	
    }

    /*
    * @description method to cover the percent from upsert Method
    */
    static testMethod void upsertTaskTest () {        
        userTst = [SELECT Id, Name FROM User WHERE Name =: NAME];
        system.runAs(userTst) {
            Test.startTest();
                Final datetime getValDate = datetime.now();
                Final task tskTestin = new Task();
                tskTestin.Subject = 'Llamada Realizada ' + getValDate;
            	tskTestin.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('MX_SB_VTA_Ventas_Asistida').getRecordTypeId();
                Final List<Task> lstTskTst = new List<Task> {tskTestin};
                MX_RTL_Task_Selector.upsertTask(lstTskTst);
                Final String idCheck = lstTskTst[0].Id;
                if (String.isEmpty(idCheck)) {
                    isUpsTst = false;
                }
            Test.stopTest();
            System.assert(isUpsTst, 'Task Upserted');
        }	
    }

    /**
     * @description cover the percent for query withSecurity
     * @author Marco Cruz | 10/10/2020
     * @return void
     **/
    @isTest
    static void testGetTaskWithSecurity() {
        final List<Siniestro__c>  tstIds =[SELECT Id from Siniestro__c limit 10];
        Final List<Task> lstTask = [SELECT Id, WhatId,CreatedDate FROM Task WHERE WhatId in :tstIds and recordtype.DeveloperName='MX_SB_MLT_PureCloud'];
        final Set<Id> setIds = new Set<Id>();
        for(Task iterTsk : lstTask) {
            setIds.add(iterTsk.Id);
        }
        final List<Task> testListT = MX_RTL_Task_Selector.getTasksById('Subject', setIds, 'true');
        System.assertEquals(testListT.size(), 10, 'Prueba Fallida');
    }

    /**
     * @description cover the percent for query withSecurity
     * @author Marco Cruz | 10/10/2020
     * @return void
     **/
    @isTest
    private static void testGetTaskWithoutSec() {
        Final List<Siniestro__c>  testIds =[SELECT Id from Siniestro__c limit 10];
        Final List<Task> lstTask = [SELECT Id, WhatId,CreatedDate FROM Task WHERE WhatId in :testIds and recordtype.DeveloperName='MX_SB_MLT_PureCloud'];
        final Set<Id> setIdTst = new Set<Id>();
        for(Task tstTask : lstTask) {
            setIdTst.add(tstTask.Id);
        }
        final List<Task> tstLst = MX_RTL_Task_Selector.getTasksById('Subject', setIdTst, 'false');
        System.assertEquals(tstLst.size(), 10, 'No hay match, prueba fallida');
    }
  /**
    * @description : Methodo para obtener
    * la ultima tarea relacionada a la opp
    * @author Daniel Ramirez Islas | 11-18-2020 
    **/
@isTest
    static void getTaskLastIdTest () {
        Test.startTest();
        Final List <Task> lastTask = MX_RTL_Task_Selector.getTaskLastId('Id','WHERE recordType.DeveloperName=\'MX_SB_MLT_PureCloud\'');
        System.assert(!lastTask.isEmpty(), 'Tarea encontrada');
        Test.stopTest();
    }
    /**
    * @description : Methodo para actualizacion de
    * tarea asociada
    * @author Daniel Ramirez Islas | 11-27-2020 
    **/
@isTest
    static void upsertTaskTest2 () {
		userTst = [SELECT Id, Name FROM User WHERE Name =: NAME];
        system.runAs(userTst) {
            Test.startTest();
                Final datetime getValueDate = datetime.now();
                Final task taskTesting1 = new Task();
                taskTesting1.Subject = 'Llamada Realizada ' + getValueDate;
            	taskTesting1.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('MX_SB_VTA_Ventas_Asistida').getRecordTypeId();
                Final List<Task> listTest1 = new List<Task> {taskTesting1};
                MX_RTL_Task_Selector.upsertTask(listTest1);
            Test.stopTest();
            System.assert(true,'Tarea Actualizada');
    }
   }
}