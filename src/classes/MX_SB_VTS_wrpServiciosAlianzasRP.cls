/**
* @FileName          : MX_SB_VTS_wrpServiciosAlianzasRP
* @description       : Wrapper class for use de web service of listInsuranceInformationCatalogs
* @Author            : Alexandro Corzo
* @last modified on  : 13-10-2020
* Modifications Log 
* Ver   Date         Author                Modification
* 1.0   13-10-2020   Alexandro Corzo       Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton, sf:ExcessivePublicCoun,sf:LongVariable,sf:LongVariable,sf:ExcessivePublicCount, sf:ShortVariable,sf:ShortClassName') 
public class MX_SB_VTS_wrpServiciosAlianzasRP {
	/**
	 * Invocación de Wrapper
	 */
    public iCatalogItem iCatalogItem {get;set;}
    
    /**
    * @description : Contenido de iCatalogItem, el cual contiene un arreglo de revisionPlanList
    * @Param 
    * @Return
    **/
    public class iCatalogItem {
        /**
         * @description: Lista Planes de Revisión
         */
        public revisionPlanList[] revisionPlanList {get;set;}
    }
    
    /**
     * @description: Objeto Wrapper de revisionPlanList para obtener el contenido de la sección
     * @Param 
     * @Return
     **/
    public class revisionPlanList {
        /**
         * @description: Obtiene Objeto: catalogItemBase
         */
        public catalogItemBase catalogItemBase {get;set;}
        /**
         * @description: Obtiene atributo: planReview
         */
        public String planReview {get;set;}
        /**
         * @description: Obtiene atributo: planCode
         */
        public String planCode {get;set;}
        /**
         * @description: Obtiene atributo: productCode
         */
        public String productCode {get;set;}
        /**
         * @description: Obtiene atributo: allianceCode
         */
        public String allianceCode {get;set;}
    }
    
    /**
     * @description: Objeto Wrapper de catalogItemBase para objeter contenido de la sección
     * @Param 
     * @Return
     **/
    public class catalogItemBase {
        /**
         * @description: Obtiene atributo: id
         */
        public String id {get;set;}
    }
}