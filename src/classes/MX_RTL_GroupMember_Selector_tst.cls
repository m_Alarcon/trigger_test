/**
* Name: MX_SB_PC_AsignacionCola_tst
* @author Jose angel Guerrero
* Description : Clase Test para Controlador de componente Lightning de asignación de Colas
*
* Ver              Date            Author                   Description
* @version 1.0     Jul/13/2020     Jose angel Guerrero      Initial Version
*
**/
@isTest
public class MX_RTL_GroupMember_Selector_tst {
    /**
* @description: funcion constructor
*/
    @isTest
    public static void getMembersByIds() {
        final List <String> listaDeId = new list <String> {'miembro'} ;
            final list < GroupMember> miebro = MX_RTL_GroupMember_Selector.getMembersByIds(listaDeId, 'prefijo');
        final list  < GroupMember> lista = new list < GroupMember> ();
        test.startTest();
        System.assertEquals(miebro, lista, 'SUCESS');
        test.stopTest();
    }		 
    
    /**
* @description: funcion constructor
*/
    @isTest
    public static void getMemberByGroupAndID() {
        final list < GroupMember> miebro = MX_RTL_GroupMember_Selector.getMemberByGroupAndID('listaDeId', 'prefijo');
        final list  < GroupMember> lista = new list < GroupMember> ();
        test.startTest();
        System.assertEquals(miebro, lista, 'SUCESS');
        test.stopTest();
    }
    /**
* @description: funcion constructor
*/
    @isTest
    public static void getMemberByGroupId() {
        final list < GroupMember> miebro = MX_RTL_GroupMember_Selector.getMemberByGroupId('colaBuscar');
        final list  < GroupMember> lista = new list < GroupMember> ();
        test.startTest();
        System.assertEquals(miebro, lista, 'SUCESS');
        test.stopTest();
    }
}