/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpTest
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
@isTest
public class MX_SB_PS_wrpTest {
    /*final string message properties*/
	Final static string LBUILDC = 'Build constructor';

	/*Methodo MX_SB_PS_authenticationWraper */
	public  static testMethod void  init0() {
		final MX_SB_PS_authenticationWraper wrAuth = new MX_SB_PS_authenticationWraper(); 
		System.assertEquals(wrAuth, wrAuth,LBUILDC);
	}
	/*Methodo MX_SB_PS_wrpCoberturasResp */
	public  static testMethod void  init1() {	
		final MX_SB_PS_wrpCoberturasResp wrpCoberturasResp  = new MX_SB_PS_wrpCoberturasResp();
		System.assertEquals(wrpCoberturasResp, wrpCoberturasResp,LBUILDC);
	 }
	 /*Methodo MX_SB_PS_wrpColonias */
	public  static testMethod void  init2() {
		final MX_SB_PS_wrpColonias wColonias = new  MX_SB_PS_wrpColonias();
		System.assertEquals(wColonias, wColonias,LBUILDC);
	}
	/*Methodo MX_SB_PS_wrpColoniasResp */
	public  static testMethod void  init3() {
		final MX_SB_PS_wrpColoniasResp wrpColoniasResp = new MX_SB_PS_wrpColoniasResp ();
		System.assertEquals(wrpColoniasResp, wrpColoniasResp,LBUILDC);
	 }
	 /*Methodo MX_SB_PS_wrpContratacionSinCobro */
	public  static testMethod void  init4() {
		final MX_SB_PS_wrpContratacionSinCobro wpCSCobro = new MX_SB_PS_wrpContratacionSinCobro();
		System.assertEquals(wpCSCobro, wpCSCobro,LBUILDC);
	  }
	  /*Methodo MX_SB_PS_wrpContratacionSinCobroResp */
	public  static testMethod void  init5() {
		final MX_SB_PS_wrpContratacionSinCobroResp wrpCSC  = new MX_SB_PS_wrpContratacionSinCobroResp();
		System.assertEquals(wrpCSC, wrpCSC,LBUILDC);
	 }
	 /*Methodo MX_SB_PS_wrpCostumerDocument */
	public  static testMethod void  init6() {
		final MX_SB_PS_wrpCostumerDocument wpCosDoc = new  MX_SB_PS_wrpCostumerDocument();
		System.assertEquals(wpCosDoc, wpCosDoc,LBUILDC);
	 }


}