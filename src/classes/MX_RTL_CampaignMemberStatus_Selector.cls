/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RTL_CampaignMemberStatus_Selector
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-08-10
* @Description 	Selector for CampaignMemberStatus
* @Changes
*  
*/
public without sharing class MX_RTL_CampaignMemberStatus_Selector {
    
    /** constructor */
    @TestVisible private MX_RTL_CampaignMemberStatus_Selector() {}
    
	/**
    *@Description   Method for CampaignMemberStatus insert
    *@author 		Edmundo Zacarias
    *@Date 			2020-08-10
    *@param 		campMStatusList List<CampaignMemberStatus>
    **/
    public static void insertCampaignMemberStatus(List<CampaignMemberStatus> campMStatusList) {
        insert campMStatusList;
    }
}