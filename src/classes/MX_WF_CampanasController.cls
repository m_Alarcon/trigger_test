/**
* ------------------------------------------------------------------------------
* @Nombre: MX_WF_CampanasController
* @Autor: Sandra Ventura García
* @Proyecto: Workflow Campañas
* @Descripción : Apex controller para componentes CmpListaCampanias, CmpListaTarjetas, CmpListaComercios
* ------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ------------------------------------------------------------------------------
* 1.0           18/09/2019     Sandra Ventura García             Desarrollo
* 2.0           21/11/2019     Sandra Ventura García             Se agrega proceso para crear canales de multipromociones. 
* ------------------------------------------------------------------------------
*/
public with sharing class MX_WF_CampanasController {
   
  /**
  * @description: construnctor sin argumentos 
  * @author Sandra Ventura
  */    
    @TestVisible
    private MX_WF_CampanasController() {}

  /**
  * @description: Devuelve lista de campañas.
  * @author Sandra Ventura
  */    
    @AuraEnabled
    public static List<Campaign> getListCampanas(String pbrief) { 
        try { 
             final Date fIniVigCampd = [SELECT MX_WF_Fecha_de_inicio__c FROM CRM_WF_Brief__c WHERE Id=: pbrief].MX_WF_Fecha_de_inicio__c;
             final Date fFinVigCampd = fIniVigCampd.addDays(2);
             return  [SELECT ID, Name, MX_WF_Campa_a_es_regional__c, StartDate,EndDate,MX_WF_Fecha_de_publicacion__c,Status, CRM_WF_FIC__c FROM Campaign 
                      WHERE EndDate >= :fFinVigCampd  AND MX_WF_Fecha_de_publicacion__c<= :fIniVigCampd AND Status='Ejecución' AND MX_WF_Campa_a_es_regional__c='No' AND RecordType.DeveloperName = 'CRM_WF_Alianzas_Comerciales' ORDER BY Name ASC]; 
        } catch (Exception e) {
            throw new AuraHandledException(System.Label.MX_WF_ErrorGeneralWFC + e);
        }
    }
    
  /**
  * @description: Devuelve lista de tarjetas.
  * @author Sandra Ventura
  */ 
    @AuraEnabled
    public static List<MX_WF_Catalogo_tarjetas__c> getListTarjetas(String vfecha) {
        try {
            final Date vfechaH = Date.valueOf(vfecha); 
            return [SELECT id, Name, MX_WF_CAT__c, MX_WF_Marca__c,  MX_WF_Producto__c, MX_WF_Nombre_Producto__c 
                    FROM MX_WF_Catalogo_tarjetas__c WHERE MX_WF_Mes_de_ltima_modificaci_n__c <=: vfechaH ORDER BY MX_WF_Producto__c ASC];
        } catch(Exception e) {
            throw new AuraHandledException(System.Label.MX_WF_ErrorGeneralWFC + e);
        }
    }
    
  /**
  * @description: Devuelve lista de comercios.
  * @author Sandra Ventura
  */ 
     @AuraEnabled
    public static List<Account> getListComercios(String grupoComercial) {
        try {
            final Id idGrupo = Id.valueOf(grupoComercial);
            return [SELECT ID, Name,  GBL_WF_Grupo_Comercial__r.Name  FROM Account WHERE  RecordType.DeveloperName = 'GBL_WF_Comercios' AND GBL_WF_Grupo_Comercial__r.Id =: idGrupo ORDER BY Name ASC];
           } catch(Exception e) {
            throw new AuraHandledException(System.Label.MX_WF_ErrorGeneralWFC+ e);
        } 
    }
  /**
  * @description: Devuelve lista de comercios.
  * @author Sandra Ventura
  */
     @AuraEnabled
 public static boolean getArtesDup(String[] campanias, Id pbrief) {
     try {
     final List<Campaign> listcamp = [SELECT id FROM Campaign WHERE Id IN : campanias];
     final Date fIniVigCampd = [SELECT MX_WF_Fecha_de_inicio__c FROM CRM_WF_Brief__c WHERE Id=: pbrief].MX_WF_Fecha_de_inicio__c;
     final Date ffinal = fIniVigCampd.addDays(2);

     final List<CRM_WF_Artes__c> canalnew = new List<CRM_WF_Artes__c>();
     final List<CRM_WF_Artes__c> canalupdate = new List<CRM_WF_Artes__c>();


     final List<MX_WF_Comercios_participantes__c> comercios = [SELECT MX_WF_Comercio_participante__c, MX_WF_Campana__c
                                                               FROM MX_WF_Comercios_participantes__c WHERE MX_WF_Campana__c in :listcamp];

     final List<CRM_WF_Artes__c> listcanal = [SELECT Id, MX_WF_Campana__c, MX_WF_Comercio__c, CRM_WF_Call_to_Action__c, CRM_WF_Estatus__c, CRM_WF_Legales__c, CRM_WF_Link_Call_To_Action__c,
                                                MX_WF_Fecha_fin_de_vigencia_del_arte__c, MX_WF_Bines_que_participan__c, MX_WF_Subtitulo__c, MX_WF_Texto_del_v_nculo_de_legales__c, MX_WF_Titulo__c, MX_WF_URL_del_legal__c,
                                                MX_WF_Body_para_segmento_patrimonial__c, MX_WF_Call_to_Action_CTA_Segmento_patrim__c, MX_WF_Header_para_segmento_patrimonial__c, MX_WF_Legales_para_segmento_patrimonial__c
                                              FROM CRM_WF_Artes__c
                                              WHERE MX_WF_Campana__c  in :listcamp
                                              AND CRM_WF_Estatus__c = 'Cierre'
                                              AND MX_WF_Tipo_de_registro_autom_tico__c = 'Multipromoción'
                                              AND MX_WF_Fecha_fin_de_vigencia_del_arte__c >= :ffinal
                                              AND MX_WF_Aprobado__c =true];

            for (MX_WF_Comercios_participantes__c comercio : comercios) {
                  final  CRM_WF_Artes__c canales = new CRM_WF_Artes__c (CRM_WF_Estatus__c= 'Generación de Copy',
                           	                                    CRM_WF_Nombre_Del_Brief__c= pbrief,
                                                                MX_WF_Campana__c= comercio.MX_WF_Campana__c,
                                                                MX_WF_Comercio__c = comercio.MX_WF_Comercio_participante__c,
                                                                MX_WF_Tipo_de_registro_autom_tico__c = 'Multipromoción',
                                                                CRM_WF_Call_to_Action__c = '',
                                                                CRM_WF_Legales__c = '',
                                                                CRM_WF_Link_Call_To_Action__c = '',
                                                                MX_WF_Bines_que_participan__c = '',
                                                                MX_WF_Fecha_fin_de_vigencia_del_arte__c = Date.today(),
                                                                MX_WF_Subtitulo__c = '',
                                                                MX_WF_Texto_del_v_nculo_de_legales__c = '',
                                                                MX_WF_Titulo__c = '', 
                                                                MX_WF_URL_del_legal__c = '' 
                                                                );
              canalnew.add(canales);
             }
         for(CRM_WF_Artes__c artesclon: listcanal) {
             final  CRM_WF_Artes__c listclon = new CRM_WF_Artes__c ( CRM_WF_Nombre_Del_Brief__c= pbrief,
                                                                MX_WF_Campana__c= artesclon.MX_WF_Campana__c,
                                                                MX_WF_Comercio__c = artesclon.MX_WF_Comercio__c,
                                                                MX_WF_Tipo_de_registro_autom_tico__c = 'Multipromoción',
                                                                CRM_WF_Call_to_Action__c = artesclon.CRM_WF_Call_to_Action__c, 
                                                                CRM_WF_Estatus__c = artesclon.CRM_WF_Estatus__c,
                                                                CRM_WF_Legales__c = artesclon.CRM_WF_Legales__c,
                                                                CRM_WF_Link_Call_To_Action__c = artesclon.CRM_WF_Link_Call_To_Action__c,
                                                                MX_WF_Bines_que_participan__c = artesclon.MX_WF_Bines_que_participan__c,
                                                                MX_WF_Fecha_fin_de_vigencia_del_arte__c = artesclon.MX_WF_Fecha_fin_de_vigencia_del_arte__c,
                                                                MX_WF_Subtitulo__c = artesclon.MX_WF_Subtitulo__c,
                                                                MX_WF_Texto_del_v_nculo_de_legales__c =artesclon.MX_WF_Texto_del_v_nculo_de_legales__c,
                                                                MX_WF_Titulo__c =artesclon.MX_WF_Titulo__c, 
                                                                MX_WF_URL_del_legal__c =artesclon.MX_WF_URL_del_legal__c,
                                                                MX_WF_Body_para_segmento_patrimonial__c =artesclon.MX_WF_Body_para_segmento_patrimonial__c,
                                                                MX_WF_Call_to_Action_CTA_Segmento_patrim__c =artesclon.MX_WF_Call_to_Action_CTA_Segmento_patrim__c,
                                                                MX_WF_Header_para_segmento_patrimonial__c =artesclon.MX_WF_Header_para_segmento_patrimonial__c,
                                                                MX_WF_Legales_para_segmento_patrimonial__c =artesclon.MX_WF_Legales_para_segmento_patrimonial__c,
                                                                MX_WF_AR_origen__c=artesclon.Id
                                                                );
                 canalupdate.add(listclon);
         }
   final integer num =canalnew.size();
   integer count = 0;
        while ( count < num ) {
         for( CRM_WF_Artes__c canalup : canalupdate) {
         if(canalnew[count].MX_WF_Comercio__c == canalup.MX_WF_Comercio__c && canalnew[count].MX_WF_Campana__c == canalup.MX_WF_Campana__c) {
             canalnew.add(count, canalup);
             canalnew.remove(count+1);
            }
         }
          count++;
        }
         insert canalnew;
         return true;

     } catch(Exception e) {
            throw new AuraHandledException(System.Label.MX_WF_ErrorGeneralWFC+ e);
     }
 }
}