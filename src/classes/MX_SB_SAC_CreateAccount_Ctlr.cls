/**
 * @File Name          : MX_SB_SAC_CreateAccount_Ctlr.cls
 * @Description        :
 * @Author             : Jaime Terrats
 * @Group              :
 * @Last Modified By   : Jaime Terrats
 * @Last Modified On   : 6/30/2020, 5:21:23 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/29/2020   Jaime Terrats     Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_SAC_CreateAccount_Ctlr {
    /**
    * @description
    * @author Jaime Terrats | 6/29/2020
    * @param fields
    * @param devName
    * @param sOType
    * @return Id
    **/
    @AuraEnabled(cacheable=true)
    public static String getRecordTypeByDevName(String fields, String devName, String sOType) {
        try {
            String rtId = '';
            final List<RecordType> rts = MX_SB_SAC_Account_Helper.getPARecordType(fields, devName, sOType);
            rtId = rts[0].Id;
            return rtId;
        } catch(QueryException qEx) {
            throw new AuraHandledException(System.Label.MX_WB_LG_ErrorBack + qEx);
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 6/30/2020
    * @param accData
    * @return Account
    **/
    @AuraEnabled
    public static Account upsertAccount(Account accData) {
        try {
            final Account acc = accData;
            final List<Account> accsToUpsert = new List<Account>();
            accsToUpsert.add(acc);
            final List<Account> upsertRecords = MX_SB_SAC_Account_Helper.upsertAccount(accsToUpsert);
            return upsertRecords[0];
        } catch(DmlException dmlEx) {
            throw new AuraHandledException(System.Label.MX_WB_LG_ErrorBack + dmlEx);
        }
    }
}