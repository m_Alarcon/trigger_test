/*
*
* @aBBVA Seguros - SAC Team
* @description This class will upsert objects from Visual Flows.
*
*           No  |     Date     |     Author               |    Description
* @version  1.0    11/08/2019     BBVA Seguros - SAC Team     Created
*
*/
public without sharing class MX_SB_SAC_InsertAccount_Flows_Cls {

    /** constructor */
    private MX_SB_SAC_InsertAccount_Flows_Cls() {} // NOSONAR
    
    /**
    * @description 
    * @param MyAccounts 
    * @return List<ID> 
    **/
    @InvocableMethod(label='Insert Account' description='Insert Account')
    public static List<ID> insertAccount(List<Account> myAccounts) {

        final List<ID> updIDs = new List<ID>();

        final Database.SaveResult[] results = Database.insert(myAccounts);

        for (Database.SaveResult result : results) {
            if (result.isSuccess()) {
                updIDs.add(result.getId());
            }
        }

        return updIDs;

    }
}