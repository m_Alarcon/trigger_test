/**
* Indra
* @author           Julio Medellín Oliva
* Project:          Presuscritos
* Description:      Class Bringd Dynamic Objects Query.
*
* Changes (Version)
* ------------------------------------------------------------------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    --------------------------------------
* @version  1.0     2020-05-25      Julio Medellín Oliva.         Creación de la Clase
*/
@SuppressWarnings ('sf:UnusedLocalVariable,sf:DUDataflowAnomalyAnalysis')
public with sharing class MX_SB_RTL_SObject_Selector {
/**
    constructor
    **/
private MX_SB_RTL_SObject_Selector() { }
/**
    * @Method bring opp
    * @Description Method Bring Simple Query
    * @return void
    **/
 public static sObject getObject(String oId,String qFields,String strObject) {
     final String sQuery  ='SELECT '+qFields+ ' FROM '+strObject+' WHERE Id =:oId';
     return DataBase.query(String.escapeSingleQuotes(SQuery));
    }

    /**
    * @description: Methodo retorna query sobject
    * @author Daniel Perez Lopez | 12-14-2020 
    * @param qFields 
    * @param firstCon 
    * @param firstConVal 
    * @param strObject 
    * @param whereClause 
    * @return sObject[] 
    **/
    public static sObject[] getObjectWC(Map<String,String> mapdata) {
        final String qFields = mapdata.get('qFields');
        final String firstCon = mapdata.get('firstCon');
        final String firstConVal = mapdata.get('firstConVal');
        final String strObject = mapdata.get('strObject');
        final String whereClause = mapdata.get('whereClause');
        Final String wherecomplete=' WHERE '+firstCon+' =:firstConVal '+ whereClause;
        final String sQuery  ='SELECT '+String.escapeSingleQuotes(qFields)+ ' FROM '+String.escapeSingleQuotes(strObject)+ String.escapeSingleQuotes(wherecomplete);
        return DataBase.query(String.escapeSingleQuotes(SQuery));
    }
}