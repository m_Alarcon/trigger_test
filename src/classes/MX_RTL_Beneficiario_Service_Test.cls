@isTest
/**
* @File Name          : MX_RTL_Beneficiario_Service_Test.cls
* @Description        : Clase que prueba los methods de MX_RTL_Beneficiario_Service
* @Author             : Juan Carlos Benitez
* @Group              :
* @Last Modified By   : Juan Carlos Benitez
* @Last Modified On   : 5/26/2020, 05:22:22 AM
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    5/26/2020   Juan Carlos Benitez         Initial Version
**/
public class MX_RTL_Beneficiario_Service_Test {
    /** Nombre de usuario Test */
    final static String NAME = 'Juan Manuel';
    /** Nombre de usuario Test */
    final static String LNAME = 'Resendiz';
    /** Nombre de usuario Test */
    final static String RECORDTYPEVIDA = 'MX_SB_MLT_RamoVida';
    /** current user id */
    final static String USR_ID = UserInfo.getUserId();
    /**
* @description
* @author Juan Carlos Benitez | 03/06/2020
* @return void
**/
    @TestSetup
    static void createinfo() {
        MX_SB_PS_OpportunityTrigger_test.makeData();
        Final String profileName = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User userInfo = MX_WB_TestData_cls.crearUsuario(NAME, profileName);  
        insert userInfo;
        System.runAs(userInfo) {
            final String recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_Proveedores_MA').getRecordTypeId();
            final Account naccTest = new Account(RecordTypeId = recordTypeId, Name=NAME, Tipo_Persona__c='Física');
            insert naccTest;
            final List<Contract> listContrcts =new List<Contract>();
            for(Integer i = 0; i<10; i++) {
                final Contract ntestContr = new Contract();
                ntestContr.AccountId = naccTest.Id;
                ntestContr.MX_SB_SAC_NumeroPoliza__c='policyTest'+'_'+i;
                listContrcts.add(ntestContr);
            }
            Database.insert(listContrcts);
            final List<MX_SB_VTS_Beneficiario__c> lstBenef = new List<MX_SB_VTS_Beneficiario__c>();
            for(Integer j = 10; j<10; j++) {
                Final MX_SB_VTS_Beneficiario__c benef = new MX_SB_VTS_Beneficiario__c();
                benef.Name = NAME +'_'+j;
                benef.MX_SB_SAC_Email__c = NAME+'_'+j+'@'+'bbva.com';
                benef.MX_SB_VTS_Contracts__c =listContrcts[j].Id;
                benef.RecordType.DeveloperName='MX_SB_MLT_Beneficiario';
                lstBenef.add(benef);
            }
            Database.insert(lstBenef);
            Final List<Siniestro__c> listSin = new List<Siniestro__c>();
            for(Integer k = 0; k<10; k++) {
                Final Siniestro__c nsiniTest = new Siniestro__c();
                nsiniTest.MX_SB_SAC_Contrato__c = listContrcts[k].Id;
                nsiniTest.MX_SB_MLT_NombreConductor__c = NAME + '_' + k;
                nsiniTest.MX_SB_MLT_APaternoConductor__c = 'LastName for '+NAME;
                nsiniTest.MX_SB_MLT_AMaternoConductor__c = 'LastName2 for '+NAME;
                nsiniTest.MX_SB_MLT_Fecha_Hora_Siniestro__c =date.valueof('2020-03-27T22:04:00.000+0000');
                nsiniTest.MX_SB_MLT_Telefono__c = '5534253647';
                Final datetime citaAgenda = datetime.now();
                nsiniTest.MX_SB_MLT_CitaVidaAsegurado__c = citaAgenda.addDays(2);
                nsiniTest.MX_SB_MLT_AtencionVida__c = 'Agendar Cita';
                nsiniTest.MX_SB_MLT_PreguntaVida__c = false;
                nsiniTest.MX_SB_MLT_SubRamo__c = 'Ahorro';
                nsiniTest.TipoSiniestro__c = 'Siniestros';
                nsiniTest.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_creacion;
                nsiniTest.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoVida).getRecordTypeId();
                listSin.add(nsiniTest);
            }
            Database.insert(listSin);
        }
    }
    @isTest
    static void testgetBenefHData () {
        final List<contract> contrctId =[SELECT Id from Contract where MX_SB_SAC_NumeroPoliza__c='policyTest_1'];
        Final Set<Id> cntrct = new Set<Id>();
        for(Contract cnt : contrctId) {
            cntrct.add(cnt.Id);
        }
        MX_RTL_Beneficiario_Service.getBenefHData(cntrct);
        system.assert(true,'He encontrado estos beneficiarios relacionados');
    }
    @isTest
    static void testgetBenefDataById () {
        final List<contract> contractId3 =[SELECT Id from contract where MX_SB_SAC_NumeroPoliza__c='policyTest_1'];
        final List<MX_SB_VTS_Beneficiario__c> benefs3 = [SELECT Id,MX_SB_VTS_Contracts__c,Name, MX_SB_VTS_APaterno_Beneficiario__c,MX_SB_SAC_Email__c,recordtype.Name from MX_SB_VTS_Beneficiario__c where MX_SB_VTS_Contracts__c=:contractId3];
        Final Set<Id> cntrct = new Set<Id>();
        for(Contract cnt : contractId3) {
            cntrct.add(cnt.Id);
        }    
        MX_RTL_Beneficiario_Service.getBenefDataById(benefs3);
        system.assert(true,'Busqueda de beneficiario Exitoso');
    }
    @isTest
    static void testupdateBenefData() {
        final List<contract> contrctId =[SELECT Id from Contract where MX_SB_SAC_NumeroPoliza__c='policyTest_1'];
        final List<MX_SB_VTS_Beneficiario__c> benefData = [SELECT Id,MX_SB_VTS_Contracts__c,Name, MX_SB_VTS_APaterno_Beneficiario__c,MX_SB_SAC_Email__c,recordtype.Name from MX_SB_VTS_Beneficiario__c where MX_SB_VTS_Contracts__c=:contrctId];
        test.startTest();
        MX_RTL_Beneficiario_Service.updateBenefData(benefData);
        system.assert(true,'El beneficiario se ha actualizado correctamente');
        test.stopTest();
    }
    @isTest
    static void testinsertBenef() {
        Final Contract cntrc=[SELECT Id from Contract  limit 1];
        Final MX_SB_VTS_Beneficiario__c benef = new MX_SB_VTS_Beneficiario__c();
        benef.Name = NAME +'_1';
        benef.MX_SB_SAC_Email__c = LNAME+'@'+'bbva.com';
        benef.MX_SB_VTS_Contracts__c =cntrc.Id;
        benef.RecordTypeId=Schema.SObjectType.MX_SB_VTS_Beneficiario__c.getRecordTypeInfosByDeveloperName().get('MX_SB_SAC_RTAsegurado').getRecordTypeId();
        test.startTest();
        MX_RTL_Beneficiario_Service.insertBenef(benef);
        system.assert(true,'El beneficiario se ha creado correctamente');
        test.stopTest();
    }
        @isTest
    static void testgetbenefbyValueE() {
        test.startTest();
        final Quote quotObj = [Select Id from Quote  limit 1];
        try {
        	MX_RTL_Beneficiario_Service.getbenefbyValue(quotObj.Id);
        } catch(Exception e) {
            System.assert(e.getMessage().contains('List'),'Success');
        }
        test.stopTest();
    }
    @isTest
    static void testCount() {
        test.startTest();
        final Quote quotObjec = [SELECT Id from Quote limit 1];
        	MX_RTL_Beneficiario_Service.getCountBenef(quotObjec.Id);
        system.assert(true,'Se han encontrado Beneficiarios');
        test.stopTest();
    }
}