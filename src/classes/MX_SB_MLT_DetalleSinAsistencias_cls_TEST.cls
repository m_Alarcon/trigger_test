@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_DetalleSinAsistencias_cls_TEST
* Autor Juan Carlos Benitez Herrera
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_DetallesSinAsistencias_cls

* --------------------------------------------------------------------------------
* Versión       Fecha                  Autor                        Descripción
* --------------------------------------------------------------------------------
* 1.0           17/12/2019      Juan Carlos Benitez Herrera          Creación
* --------------------------------------------------------------------------------
*/
public class MX_SB_MLT_DetalleSinAsistencias_cls_TEST {
    /*
    * @Field JSON emula el Stringify del controlador del componente
    */
   		Public static final String JSON ='{"ReembAct":"false","defaultValue":"Grua","defaultValueAve":"Mecánica","defaultValueTipoG":"Plataforma","llantas":"false","neutral":"false","Email":"lawbh93@hotmail.com","CondicionesVeh":"No arranca"}';
    /*
    * @Field JSON2 emula el Stringify del controlador del componente
    */
	 	Public static final String JSON2= '{"ReembAct":"false","defaultValue":"Gasolina","Email":"lawbh93@outlook.com","CondicionesVeh":"No arranca"}';
    /*
    * @Field JSON3 emula el Stringify del controlador del componente
    */
     	Public static final String JSON3= '{"ReembAct":"false","defaultValue":"Cambio de llanta","Email":"lawbh93@yahoo.com","CondicionesVeh":"No arranca"}';
    /*
    * @Field JSON4 emula el Stringify del controlador del componente
    */
     	Public static final String JSON4= '{"ReembAct":"false","defaultValue":"Paso de corriente","Email":"lawbh93@aol.com","CondicionesVeh":"No arranca"}';
    /*
    * @Field String campo de estatus
    */
    	Public static final String ABIERTO='Abierto';
    /*
    * @Field String campo de condiciones del vehiculo
    */
    	Public static final String COND = 'El motor no enciende';
    /*
    * @Field String emula evt de boton
    */
    	Public static final String BUTTON = 'false';
	@testSetup
	static void setupTestData() {
        final String nameProfile = [SELECT Id,Name from profile where name in ('Administrador del sistema','System Administrator') limit 1].Name;
        final User objUsrP = MX_WB_TestData_cls.crearUsuario('AdminPruebatst', nameProfile); 
        insert objUsrP;
        System.runAs(objUsrP) {
        final String  tiporegistro = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
		final Account ctatest = new Account(recordtypeid=tiporegistro,firstname='DANIELA',lastname='PEREZ',Apellido_materno__pc='LOPEZ',RFC__c='PELD920911',PersonEmail='test@gmail.com');
        insert ctatest;
        final Contract contrato = new Contract(accountid=ctatest.Id,MX_SB_SAC_NumeroPoliza__c='PolizaTest',MX_SB_SAC_RFCAsegurado__c =ctatest.rfc__c,MX_SB_SAC_NombreClienteAseguradoText__c=ctatest.firstname,MX_WB_apellidoPaternoAsegurado__c=ctatest.lastname,MX_WB_apellidoMaternoAsegurado__c=ctatest.Apellido_materno__pc);
        insert contrato;
		final String  rectyp = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_RamoAuto').getRecordTypeId();
    	final Siniestro__c sinies = new Siniestro__c(recordtypeid=rectyp,MX_SB_SAC_Contrato__c=contrato.Id,TipoSiniestro__c='Asistencia Vial',MX_SB_MLT_JourneySin__c='Identificación del Reportante',MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com');
        insert sinies;
        final WorkOrder woGrua = new WorkOrder(MX_SB_MLT_Siniestro__c=sinies.Id,MX_SB_MLT_Email__c='lawbh93@gmail.com',MX_SB_MLT_TipoSiniestro__c = 'Grua', MX_SB_MLT_EstatusServicio__c =ABIERTO);
		insert woGrua;
        final WorkOrder woGas = new WorkOrder(MX_SB_MLT_Siniestro__c=sinies.Id,MX_SB_MLT_Email__c='lawbh93@outlook.com',MX_SB_MLT_TipoSiniestro__c = 'Gasolina', MX_SB_MLT_EstatusServicio__c =ABIERTO);
        insert woGas;
        final WorkOrder woCambio = new WorkOrder(MX_SB_MLT_Siniestro__c=sinies.Id,MX_SB_MLT_Email__c='lawbh93@aol.com',MX_SB_MLT_TipoSiniestro__c = 'Cambio de llanta', MX_SB_MLT_EstatusServicio__c =ABIERTO);
		insert woCambio;
        final WorkOrder woPasoC = new WorkOrder(MX_SB_MLT_Siniestro__c=sinies.Id,MX_SB_MLT_Email__c='lawbh93@hotmail.com',MX_SB_MLT_TipoSiniestro__c = 'Paso de corriente', MX_SB_MLT_EstatusServicio__c =ABIERTO);
		insert woPasoC;
        }
  }
	@isTest static void updaSini() {
    	final Siniestro__c sinies = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com'  limit 1];
		Test.startTest();
        final Map<String,String> mapa= new Map<String,String>();
        mapa.put('siniId',sinies.id);
        mapa.put('strEmail',sinies.MX_SB_MLT_CorreoElectronico__c);
        mapa.put('strCond',COND);
        mapa.put('button',BUTTON);
        MX_SB_MLT_DetallesSinAsistencias_cls.updateSinies(mapa);
        system.assert(true, '¡Exito!');
		test.stopTest();
       }

    @isTest static void obWGrua() {
        final Siniestro__c sinies = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com' limit 1];
		Test.startTest();
		final String ide = sinies.Id;
        MX_SB_MLT_DetallesSinAsistencias_cls.getGruaWO(ide);
        system.assert(true, 'Exitoso!');
        test.stopTest();
    }
    @isTest static void obWCambioL() {
        final Siniestro__c sinies = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com'];
		Test.startTest();
		final String ide = sinies.Id;
        MX_SB_MLT_DetallesSinAsistencias_cls.getCambioLLWO(ide);
        system.assert(true, 'Exito!');
        test.stopTest();
    }
        @isTest static void obWPasoC() {
        final Siniestro__c sinies = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com'];
		Test.startTest();
		final String ide = sinies.Id;
        MX_SB_MLT_DetallesSinAsistencias_cls.getPasoCWO(ide);
		system.assert(true, '¡Exito');
        test.stopTest();
    }
	@isTest static void obWGas() {
        final Siniestro__c sinies = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com'];
		Test.startTest();
		final String ide = sinies.Id;
        MX_SB_MLT_DetallesSinAsistencias_cls.getGasWO(ide);
        system.assert(true, '¡Exitoso!');
        test.stopTest();
    }
	@isTest static void detallesSin() {
	final Siniestro__c sinies = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com'];
    Test.startTest();
		MX_SB_MLT_DetallesSinAsistencias_cls.getSiniestroD(sinies.Id);
        system.assert(true, 'Exitoso');
	test.stopTest();

    }
	@isTest static void inServ() {
        final Siniestro__c sinies = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com'];
        final String ide= sinies.Id;
        Test.startTest();
        	MX_SB_MLT_DetallesSinAsistencias_cls.insertWOGrua(JSON, ide);
        	MX_SB_MLT_DetallesSinAsistencias_cls.insertWOGas(ide,JSON2);
			MX_SB_MLT_DetallesSinAsistencias_cls.insertWOCambLl(ide,JSON3);
			MX_SB_MLT_DetallesSinAsistencias_cls.insertWOPasoC(ide,JSON4);
        	system.assert(true, 'Se ha creado exitosamente');
    	test.stopTest();
    }
}