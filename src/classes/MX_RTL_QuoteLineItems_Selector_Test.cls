/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 01-14-2021
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   09-03-2020   Eduardo Hernández Cuamatzi   Initial Version
 * 1.1   14-01-2021   Diego Olvera Hernandez       Se agrega función de para cobertura actualización
**/
@isTest
private class MX_RTL_QuoteLineItems_Selector_Test {
    /**Folio cotización */
    final static String FOLIOHSD = '1234541';
    
    @TestSetup
    static void makeData() {
        MX_SB_VTS_CallCTIs_utility.initLeadMulServHSD();
        MX_SB_VTS_leadMultiServiceVts_S_Test.genOppsHSDDinamic();
        final User userTest = MX_WB_TestData_cls.crearUsuario('AsesorTest', 'System Administrator');
        insert userTest;
        final Account accntTest = MX_WB_TestData_cls.createAccount('TestaAcc', System.Label.MX_SB_VTS_PersonRecord);
        insert accntTest;
        final Opportunity oppCmbTest = MX_WB_TestData_cls.crearOportunidad('TestOpp', accntTest.Id, userTest.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
        oppCmbTest.LeadSource = 'Facebook';
        oppCmbTest.Producto__c = 'Hogar seguro dinámico';	
        insert oppCmbTest;
        final Pricebook2 prB2 = MX_WB_TestData_cls.createStandardPriceBook2();
        final Quote quoteTest = new Quote(OpportunityId=oppCmbTest.Id, Name='Outbound');
        quoteTest.MX_SB_VTS_Folio_Cotizacion__c = '556433';
        quoteTest.QuoteToName = 'Completa';
        quoteTest.Pricebook2Id= prB2.Id;
        insert quoteTest;
        final Product2 proTest = MX_WB_TestData_cls.productNew('Hogar seguro dinámico');
        proTest.IsActive=true;
        Insert proTest;
        final PricebookEntry PbeTest = MX_WB_TestData_cls.priceBookEntryNew(proTest.Id);
        Insert PbeTest;
        final QuoteLineItem Qltem = new QuoteLineItem(QuoteId=quoteTest.Id, PricebookEntryId= PbeTest.Id, Quantity=1, UnitPrice=1, Product2Id=proTest.Id);
        Insert Qltem;
    }
    
    @isTest
    static void selSObjectsByQuotes() {
        test.startTest();
        final List<Quote> quoteData = MX_RTL_Quote_Selector.quoteData(FOLIOHSD);
        final List<QuoteLineItem> lstQuoli = MX_RTL_QuoteLineItems_Selector.selSObjectsByQuotes(new Set<Id>{quoteData[0].Id});
        test.stopTest();
        System.assertEquals(quoteData[0].Id, lstQuoli[0].QuoteId, 'Quoli recuperada');
    }
    @isTest
    static void insertQuotesLineTest() {
        test.startTest();
        final Opportunity nwLis = [Select Id, StageName, LeadSource, OwnerId, Name from Opportunity Where Name = 'TestOpp'];
        final Quote quoteData =  [Select Id, QuoteToName, OpportunityId FROM Quote Where OpportunityId =: nwLis.Id];
        final List<QuoteLineItem> lstQuoli = [Select QuoteId, PricebookEntryId, Quantity, Product2Id FROM QuoteLineItem WHERE QuoteId =: quoteData.Id]; 
        final List<QuoteLineItem> lstQLIneRsp = MX_RTL_QuoteLineItems_Selector.insertQuotesLine(lstQuoli);
        System.assert(!lstQLIneRsp.isEmpty(), 'Quoli recuperada');
        test.stopTest();
        
    }
}