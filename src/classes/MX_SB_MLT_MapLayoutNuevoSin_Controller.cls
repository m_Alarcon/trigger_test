/*
-------------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_MapLayoutNuevoSin_Controller
* Autor Juan Carlos Benitez Herrera
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Clase que almacena los methods usados en la
					visualforce del mapa inicial de la toma de reporte.
* -----------------------------------------------------------------------------------
* Versión       Fecha               Autor                               Descripción
* ------------------------------------------------------------------------------------
* 1.0           12/03/2020      Juan Carlos Benitez                         Creación
* ------------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_MapLayoutNuevoSin_Controller {
    /** Variable sin */
    public Siniestro__c sin {get; set;}
    /** Variable urlLocation */
    public String urlLocation {get; set;}
    /** Variable uRLo */
    public String uRLo {get; set;}
     /*
    * @description Method que inserta el siniestro
    * @param ninguno
    * @return null
    */
    public PageReference insin() {
      insert sin;
      return null;
    }
     /*
    * @description Method que actualiza siniestro
    * @param ninguno
    * @return Object null
    */
    public PageReference updateSin() {
        update sin;
         return null;
    }
    /*
    * @description constructor que inicializa variables para el front
    * @param void
    * @return Object Siniestro__c sin
    */
        public MX_SB_MLT_MapLayoutNuevoSin_Controller() {
        urlLocation= Url.getSalesforceBaseUrl().toExternalForm();
        uRLo= urlLocation.replace('--c.visualforce.com', '.lightning.force.com');
       	sin = new Siniestro__c();
         if(sin.Id==null) {
            sin.RecordTypeId=Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_RamoAuto').getRecordTypeId();
            sin.MX_SB_MLT_Origen__c = 'Phone';
            sin.MX_SB_MLT_URLLocation__c = '19.5038795,-99.1802701';
            sin.MX_SB_MLT_Address__c = 'Eje 5 Nte 990, Santa Barbara, 02230 Ciudad de México, CDMX, México';
        } else {
            sin = [SELECT Id, Name, MX_SB_MLT_Origen__c, MX_SB_MLT_Address__c,MX_SB_MLT_URLLocation__c,MX_SB_MLT_NumeroCalle__c,
                MX_SB_MLT_Calle__c,MX_SB_MLT_Colonia__c,MX_SB_MLT_Estado__c,MX_SB_MLT_CodigoPostal__c,MX_SB_SAC_Contrato__c
                FROM Siniestro__C WHERE id =: ApexPages.currentPage().getParameters().get('id') limit 1];
       		}
        }
}