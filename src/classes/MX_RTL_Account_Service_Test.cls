@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_RTL_Account_Service_Test
* Autor: Juan Carlos Benitez Herrera
* Proyecto: SB Presuscritos - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_RTL_Account_Service

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         09/07/2020    Juan Carlos Benitez Herrera              Creación
* -----------------------------------------------------------------------------------------------
*/
public class MX_RTL_Account_Service_Test {
    /** Nombre de usuario Test */
    final static String NAME = 'Vanessa';
    /** Apellido Paterno */
    Final static STRING LNAME= 'Estrada';
    
    @TestSetup
    static void createDataA() {
    	MX_SB_PS_OpportunityTrigger_test.makeData();
        Final Account pacTst = [Select Id from Account limit 1];
        pacTst.FirstName =NAME;
	    pacTst.LastName=LNAME;
        pacTst.Tipo_Persona__c='Física';
        update pacTst;
        Final Opportunity opTsta = [Select id from Opportunity where AccountId=:pacTst.Id];
        opTsta.Name= NAME;
        opTsta.StageName=System.Label.MX_SB_PS_Etapa1;
        opTsta.AccountId=pacTst.Id;
        update opTsta;
    }

    @isTest
    static void testgetSinHData() {
        Final Id accDataId=[SELECT Id FROM Account where FirstName=:NAME and LastName=:LNAME].Id;
		final Set<Id> ids = new Set<Id>{accDataId};
        test.startTest();
        	MX_RTL_Account_Service.getSinHData(ids);
	        system.assert(true, 'Se han encontrado datos de la cuenta');
        test.stopTest();
    }

    @isTest
    static void testupsrtAcc() {
        Final Account pacc2= [SELECT Id from Account where FirstName=:NAME];
        pacc2.MX_RTL_CURP__pc='BEHJ920402MDFNRS23';
        Final Account pacc = new Account();
		pacc.FirstName=NAME;
        pacc.LastName=LNAME;
        pacc.RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
        pacc.MX_RTL_CURP__pc='BEHJ920402MDFNRS23';
        pacc.Tipo_Persona__c='Física';
        test.startTest();
        	MX_RTL_Account_Service.upsrtAcc(pacc);
        	MX_RTL_Account_Service.upsrtAcc(pacc2);
 	        system.assert(true, 'Se ha Actualizado la cuenta correctamente');
        test.stopTest();
    }
}