/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 01-19-2021
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   01-19-2021   Eduardo Hernández Cuamatzi   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_editCoverageHSD_Services {

    /**Variable de Id para mapas*/
    final static String STRID = 'id';

    /**
    * @description Genera Json para editar coberturas y datos particulares
    * @author Eduardo Hernández Cuamatzi | 01-19-2021 
    * @param wrappData Datos necesarios para la generación del json
    * @return Map<String, Object> Json en formato mapa Strings
    **/
    public static Map<String, Object> fillEditPorc(WrapEditPorc wrappData) {
        final Map<String, Object> jsonCriterialP = new Map<String, Object>();
        jsonCriterialP.put('allianceCode', 'DHSDT003');
        jsonCriterialP.put('certifiedNumberRequested', '1');
        final Map<String, String> findProd = MX_SB_VTS_DeleteCoverageHSD_Helper.findProdCodByName(wrappData.oppData.Producto__c);
        jsonCriterialP.put('product', MX_SB_VTS_DeleteCoverageHSD_Helper.fillProductElement(findProd.get(wrappData.oppData.Producto__c.toUpperCase()), wrappData.lstValues[0]));
        jsonCriterialP.put('allianceCode', 'DHSDT003');        
        final Map<String, Object> frecuenciElement = new Map<String, Object>();
        frecuenciElement.put(STRID, 'YEARLY');
        final List<Object> lstFrecEl = new List<Object>{frecuenciElement};
        jsonCriterialP.put('frequencies', lstFrecEl);
        final List<Object> lstContractCrit = new List<Object>();
        for (Cobertura__c criteriaContract : wrappData.lstCrit) { 
            final Map<String, Object> criterialElement = new Map<String, Object>();
            criterialElement.put('criterial', criteriaContract.MX_SB_MLT_Cobertura__c);
            if(wrappData.lstCriterials.contains(criteriaContract.MX_SB_MLT_Cobertura__c)) {
                criterialElement.put(STRID, wrappData.lstValues[2]);
                criterialElement.put('value', wrappData.lstValues[3]);
            } else {
                criterialElement.put(STRID, criteriaContract.MX_SB_MLT_Descripcion__c);
                criterialElement.put('value', criteriaContract.MX_SB_VTS_TradeValue__c);
            }
            lstContractCrit.add(criterialElement);
        }
        jsonCriterialP.put('contractingCriteria', lstContractCrit);
        final Map<String, Object> fillCovers = MX_SB_VTS_FillCoveragesTrades_helper.fillCoverages(wrappData.lstCover);
        final List<Object> lstTrades = new List<Object>();
        for (String keyItem : fillCovers.keySet()) {
            lstTrades.add(fillCategories(fillCovers.get(keyItem)));
        }
        jsonCriterialP.put('trades', lstTrades);
        return jsonCriterialP;
    }

    /**
    * @description Genera elemento de categorias
    * @author Eduardo Hernández Cuamatzi | 01-20-2021 
    * @param tradeItem Elemento a evaluar de trade
    * @return Map<String, Object> Elemento trade para json
    **/
    public static Map<String, Object> fillCategories(Object tradeItem) {
        final Map<String, Object> mapTrades = new Map<String, Object>();
        final Map<String, Object> tradeObject = (Map<String, Object>)tradeItem;
        for (String tradeKey : tradeObject.keySet()) {
            mapTrades.put(STRID, tradeKey);
            final Map<String, Object> tradeScOV = (Map<String, Object>)tradeObject.get(tradeKey);
            final List<Object> lstCategories = new List<Object>();
            for(String keyCategori : tradeScOV.keySet()) {
                final Map<String, Object> mapCats = new Map<String, Object>();
                mapCats.put('id', keyCategori);
                mapCats.put('goodTypes', fillGoodTypes(tradeScOV.get(keyCategori)));
                lstCategories.add(mapCats);
            }
            mapTrades.put('categories', lstCategories);
        }
        return mapTrades;
    }

    /**
    * @description Genera elemento GoodType para json
    * @author Eduardo Hernández Cuamatzi | 01-20-2021 
    * @param goodObject Elemento categoria
    * @return List<Object> Lista de objetos Goodtypes por categoria
    **/
    public static List<Object> fillGoodTypes(Object goodObject) {
        final Map<String, Object> mapGoodTypes = (Map<String, Object>)goodObject;
        final List<Object> lstGoodTypes = new List<Object>();
        for (String goodKey : mapGoodTypes.keySet()) {
            final Map<String, Object> mapGoods = new Map<String, Object>();
            mapGoods.put('id', goodKey);
            mapGoods.put('coverages', fillCovers(mapGoodTypes.get(goodKey)));
            lstGoodTypes.add(mapGoods);
        }
        return lstGoodTypes;
    }

    /**
    * @description Genera elementos de lista de coberturas por goodTypes
    * @author Eduardo Hernández Cuamatzi | 01-20-2021 
    * @param coversCode Lista de coberturas por goodtype
    * @return List<Object> Lista de objetos de coberturas
    **/
    public static List<Object> fillCovers(Object coversCode) {
        final List<Cobertura__c> lstCovers = (List<Cobertura__c>)coversCode;
        final List<Object> lstCoverages = new List<Object>();
        for(Cobertura__c coverage : lstCovers) {
            final Map<String, Object> mapCoverage = new Map<String, Object>();
            mapCoverage.put('id', coverage.MX_SB_VTS_CoverageCode__c);
            lstCoverages.add(mapCoverage);
        }
        return lstCoverages;
    }

    /**
    * @description Actualiza datos de Criterials
    * @author Eduardo Hernández Cuamatzi | 01-19-2021 
    * @param wrapListCover Lista de datos particulares de la cotización
    * @param wrapData Datos generales de la cotizacíon
    **/
    public static void updateNewValues(List<Cobertura__c> wrapListCover, WrapEditPorc wrapData) {
        for(Cobertura__c coverItem : wrapListCover) {
            if(wrapData.lstCriterials.contains(coverItem.MX_SB_MLT_Cobertura__c)) {
                coverItem.MX_SB_VTS_TradeValue__c = wrapData.lstValues[3];
                coverItem.MX_SB_MLT_Descripcion__c = wrapData.lstValues[2];
                coverItem.MX_SB_VTS_SelectedPay__c = wrapData.lstValues[4];
            }
        }
        MX_RTL_Cobertura_Selector.updateCoverages(wrapListCover);
    }
    
    /**
    * @description Actualiza valores nuevos para coberturas afectadas
    * @author Eduardo Hernández Cuamatzi | 01-19-2021 
    * @param wrapListCover Lista de coberturas de la cotización
    * @param wrapData Datos generales para json
    **/
    public static void updateNewValuesCov(List<Cobertura__c> wrapListCover, WrapEditPorc wrapData) {
        for(Cobertura__c coverItem : wrapListCover) {
            coverItem.MX_SB_VTS_SelectedPay__c = wrapData.lstValues[4];
        }
        MX_RTL_Cobertura_Selector.updateCoverages(wrapListCover);
    }

    /**Estructura de datos necesarios para generar json */
    public class WrapEditPorc {
        /**Lista de coberturas*/
        public List<Cobertura__c> lstCover {get;set;}
        /**Lista de datos particulaes*/
        public List<Cobertura__c> lstCrit {get;set;}
        /**Lista de códigos de datos particulares*/
        public List<String> lstCriterials {get;set;}
        /**Lista de datos nuevos para coberturas y datos particulares */
        public List<String> lstValues {get;set;}
        /**Datos de la Oportunidad */
        public Opportunity oppData {get;set;}
    }
}