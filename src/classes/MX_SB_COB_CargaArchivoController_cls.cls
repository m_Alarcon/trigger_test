/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_COB_CargaArchivoController_cls
* Autor Angel Nava
* Proyecto: Cobranza - BBVA Bancomer
* Descripción : controlador de componente lightning cargaarchivo
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Desripción
* --------------------------------------------------------------------------------
* 1.0           01/05/2019     Angel Nava				          Creación
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_COB_CargaArchivoController_cls {
    /** variable para condicion */
    final private static String ROLCOORD = 'Coordinador Cobranza';
    /** variable para condicion */
    final private static String ROLSUP = 'Supervisor Cobranza';
/** constructor */
    private MX_SB_COB_CargaArchivoController_cls() {}
    /** se obtiene datos de archivo  */
	@auraenabled
    public static CargaArchivo__c getCargaArchivo(String idIn) {

            CargaArchivo__c cargaarchivoOut = new CargaArchivo__c();
            final Bloque__c bloque =[select id,name from bloque__c where id =:idIn limit 1];

        	final String userId = UserInfo.getUserId();
        	final List<user> userList =  [select Id, Name, UserRole.Name  from User where id = :userId limit 1];
            if(userList[0].UserRole.Name == ROLCOORD && String.isBlank(bloque.Name)==false  ) {
                   cargaarchivoOut = [select id,name,MX_SB_COB_ContadorConError__c,MX_SB_COB_ContadorSinError__c,MX_SB_COB_Resultado__c from CargaArchivo__c where MX_SB_COB_bloque__c=:bloque.name limit 1];
            } else if(userList[0].UserRole.Name == ROLSUP) {
                 cargaarchivoOut = [select id,MX_SB_COB_ContadorConError__c,MX_SB_COB_ContadorSinError__c,MX_SB_COB_Resultado__c from CargaArchivo__c where MX_SB_COB_bloque__c=:bloque.name limit 1];
            }
            return cargaarchivoOut;
    }
}