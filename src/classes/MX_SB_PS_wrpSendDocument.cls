/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpSendDocument
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
public class  MX_SB_PS_wrpSendDocument {
  
	  /*Public property for wrapper*/
	public customerName customerName {get;set;}
	  /*Public property for wrapper*/
	public String[] listHref {get;set;}
	  /*Public property for wrapper*/
	public String[] mailReceiver {get;set;}
	  /*Public property for wrapper*/
	public String pass {get;set;}	
	  /*Public property for wrapper*/
	public String productId {get;set;}
	  /*Public property for wrapper*/
	public String subject {get;set;}

	/*Public constructor for wrapper*/
    public  MX_SB_PS_wrpSendDocument() {
        listHref = new String[] {};
        mailReceiver = new String[] {}; 
        customerName = new customerName(); 
        pass = '';
        productId = '';
        subject = '';		
    } 
	  /*Public subclass for wrapper*/
	public class customerName {
	  /*Public property for wrapper*/
		public String name {get;set;}
      /*public constructor subclass*/
		public customerName() {
		this.name = '';
		}			
	}
}