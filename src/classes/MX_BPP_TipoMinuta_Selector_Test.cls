/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_TipoMinuta_Selector_Test
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2021-01-19
* @Description 	Test Class for MX_BPP_TipoMinuta_Selector
* @Changes
*  
*/
@isTest
public class MX_BPP_TipoMinuta_Selector_Test {
    
    /**
    * @Description 	Test Method for constructor
    * @Return 		NA
    **/
	@isTest
    static void constructorTest() {
        final MX_BPP_TipoMinuta_Selector tipoMSelector = new MX_BPP_TipoMinuta_Selector();
        System.assertNotEquals(null, tipoMSelector, 'Error on constructor');
    }
    
    /**
    * @Description 	Test Method for MX_RTL_CatalogoMinuta_Selector.getRecordsByTipoVisita()
    * @Return 		NA
    **/
	@isTest
    static void getRecordsByDevNameTest() {
        final List<String> listString = new List<String>();
        listString.add('BienvenidaDO');
    	final List<MX_BPP_TipoMinuta__mdt> listRecords = MX_BPP_TipoMinuta_Selector.getRecordsByObjetivo('Id, MX_NombreVF__c', listString);
        System.assertNotEquals(null, listRecords, 'Error on getRecordsByObjetivo');
    }

}