/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_PS_wrpDatosParticularesResp_Test
* Autor Daniel Perez Lopez
* Proyecto: Salesforce Presuscritos
* Descripción : Prueba los Methods de la clase MX_SB_PS_wrpDatosParticularesResp

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           10/12/2019      Daniel Lopez                         Creación
* --------------------------------------------------------------------------------
*/
 @SuppressWarnings('sf:UseSingleton, sf:NcssMethodCount')
@IsTest
public with sharing class MX_SB_PS_wrpDatosParticularesResp_Test {
    @IsTest
    static void clasetest() {
        final MX_SB_PS_wrpDatosParticularesResp.catalogItemBase[] lcat = new MX_SB_PS_wrpDatosParticularesResp.catalogItemBase[] {};
        final MX_SB_PS_wrpDatosParticularesResp.catalogItemBase catalogItemBase = new MX_SB_PS_wrpDatosParticularesResp.catalogItemBase();
        catalogItemBase.id='';
        catalogItemBase.name='';
        catalogItemBase.description='';
        lcat.add(catalogItemBase);
        
        final MX_SB_PS_wrpDatosParticularesResp.transformer transformer= new MX_SB_PS_wrpDatosParticularesResp.transformer();
        transformer.catalogItemBase=lcat;
        final MX_SB_PS_wrpDatosParticularesResp.transformer[] ltran= new MX_SB_PS_wrpDatosParticularesResp.transformer[] {};
        ltran.add(transformer);

        final MX_SB_PS_wrpDatosParticularesResp.particularData particularData = new MX_SB_PS_wrpDatosParticularesResp.particularData();
        particularData.aliasCriterion='';
        particularData.description='';
        particularData.type='';
        particularData.transformer=ltran;
        final MX_SB_PS_wrpDatosParticularesResp.particularData[] lpar = new MX_SB_PS_wrpDatosParticularesResp.particularData[] {};
        lpar.add(particularData);

        final MX_SB_PS_wrpDatosParticularesResp.productPlan productPlan = new MX_SB_PS_wrpDatosParticularesResp.productPlan();
        productPlan.catalogItemBase=catalogItemBase;
        productPlan.planReview='01';
        productPlan.bouquetCode='';
        final MX_SB_PS_wrpDatosParticularesResp.productPlan[] lproductPlan= new MX_SB_PS_wrpDatosParticularesResp.productPlan[] {};
        lproductPlan.add(productPlan);

        final MX_SB_PS_wrpDatosParticularesResp.planParticularData planParData= new MX_SB_PS_wrpDatosParticularesResp.planParticularData();
        planParData.particularData=lpar;
        planParData.productPlan=productPlan;
        final MX_SB_PS_wrpDatosParticularesResp.planParticularData[] lplanpar= new MX_SB_PS_wrpDatosParticularesResp.planParticularData[] {};
        lplanpar.add(planParData);

        final MX_SB_PS_wrpDatosParticularesResp.iCatalogItem iCatalogItem = new MX_SB_PS_wrpDatosParticularesResp.iCatalogItem();
        iCatalogItem.planParticularData=lplanpar;
        iCatalogItem.productPlan=lproductPlan;

        final MX_SB_PS_wrpDatosParticularesResp miclas = new MX_SB_PS_wrpDatosParticularesResp();
        miclas.iCatalogItem=iCatalogItem;

        
        Test.startTest();
            System.assertEquals(miclas.iCatalogItem.productPlan[0].planReview,'01','Peticion Exitosa');
        Test.stopTest();            
    }
}