/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpColonias
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 25/06/2020                 Creación de la clase.*/
 @SuppressWarnings('sf:LongVariable, sf:ShortClassName, sf:ShortVariable')
public class  MX_SB_PS_wrpColonias {
     /*Public property for wrapper*/
    public header header {get;set;}	
	  /*Public property for wrapper*/
    public String zipCode {get;set;}

	/*Public constructor*/
    public MX_SB_PS_wrpColonias() {
        header = new header();
		zipCode = '';
    }
	 
	 /*Public subclass*/
    public class header {
	     /*Public property for wrapper*/
        public String aapType {get;set;}	
		  /*Public property for wrapper*/
        public String dateRequest {get;set;}	
		  /*Public property for wrapper*/
        public String channel {get;set;}
		  /*Public property for wrapper*/
        public String subChannel {get;set;}
           /*Public property for wrapper*/		
        public String branchOffice {get;set;}
           /*Public property for wrapper*/		
        public String managementUnit {get;set;}
            /*Public property for wrapper*/		
        public String user {get;set;}
		  /*Public property for wrapper*/
        public String idSession {get;set;}
		  /*Public property for wrapper*/
        public String idRequest {get;set;}
		  /*Public property for wrapper*/
        public String dateConsumerInvocation {get;set;}
		 /*public constructor subclass*/
		public header() {
		 this.aapType = '';
		 this.dateRequest = '';
		 this.channel = '';
		 this.subChannel = '';
		 this.branchOffice = '';
		 this.managementUnit = '';
		 this.user = '';
		 this.idSession = '';
		 this.idRequest = '';
		 this.dateConsumerInvocation = '';
		}
    }
}