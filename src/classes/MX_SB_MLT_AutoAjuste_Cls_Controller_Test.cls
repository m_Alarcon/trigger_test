@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_AutoAjuste_Cls_Controller_Test
* Autor Juan Carlos Benitez Herrera
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_AutoAjuste_Cls_Controller

* --------------------------------------------------------------------------------
* Versión       Fecha                  Autor                        Descripción
* --------------------------------------------------------------------------------
* 1.0           24/02/2020      Juan Carlos Benitez Herrera          Creación
* 1.1           12/03/2020      Angel Nava                          migración de campos contrato
* --------------------------------------------------------------------------------
*/
public class MX_SB_MLT_AutoAjuste_Cls_Controller_Test {
    @testSetup
	static void setupTestData() {
        final String nameProfile = [SELECT Id,Name from profile where name in ('Administrador del sistema','System Administrator') limit 1].Name;
        final User objUsrP = MX_WB_TestData_cls.crearUsuario('AdminPruebatst', nameProfile); 
        insert objUsrP;
        System.runAs(objUsrP) {
        final String  tipregistro = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
		final Account ctatst = new Account(recordtypeid=tipregistro,firstname='DANIELA',lastname='PEREZ',Apellido_materno__pc='LOPEZ',RFC__c='PELD920911',PersonEmail='test@gmail.com');
        insert ctatst;
        final Contract contrato = new Contract(accountid=ctatst.Id,MX_SB_SAC_NumeroPoliza__c='PolizaTest',MX_SB_SAC_RFCAsegurado__c =ctatst.rfc__c,MX_SB_SAC_NombreClienteAseguradoText__c=ctatst.firstname,MX_WB_apellidoPaternoAsegurado__c=ctatst.lastname,MX_WB_apellidoMaternoAsegurado__c=ctatst.Apellido_materno__pc);
        insert contrato;
		final String  rctyp = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_RamoAuto').getRecordTypeId();
    	final Siniestro__c sinies = new Siniestro__c(recordtypeid=rctyp,MX_SB_SAC_Contrato__c=contrato.Id,TipoSiniestro__c='Asistencia Vial',MX_SB_MLT_JourneySin__c='Identificación del Reportante',MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com');
        insert sinies;
        }
	}
@isTest static void autoAjust() {
		final Siniestro__c updateSiniestro = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com'  limit 1];
        		updateSiniestro.MX_SB_MLT_JourneySin__c = 'Cerrado';
            	updateSiniestro.MX_SB_MLT_RazonCierre__c = 'AutoAjuste';
                updateSiniestro.MX_SB_MLT_AjustadorAuto__c = false;
                updateSiniestro.MX_SB_MLT_AjustadorMoto__c = false;
            	updateSiniestro.MX_SB_MLT_AutoAjusteAccepted__c = 'true';
        		final String ide = updateSiniestro.Id;
                update updateSiniestro;
		Test.startTest();
			MX_SB_MLT_AutoAjuste_Cls_Controller.Autoajuste(ide,'aceptar');
        	MX_SB_MLT_AutoAjuste_Cls_Controller.Autoajuste(ide,'rechazar');
            system.assert(true, 'Se ha actualizado exitosamente');        
        Test.stopTest();
    }
}