/**
 * @File Name          : MX_BPP_UpdateRelatedLeadOpp_Service_Test.cls
 * @Description        : Clase para Test de MX_BPP_UpdateRelatedLeadOpp_Service
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 26/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      26/06/2020           Jair Ignacio Gonzalez G.   Initial Version
**/
@isTest
private class MX_BPP_UpdateRelatedLeadOpp_Service_Test {
    /**
     **Descripción: Clase makeData
    **/
    @TestSetup static void makeData() {
        final Campaign oCamp = new Campaign(Name = 'Campaign Service Test', CampaignMemberRecordTypeId=Schema.SObjectType.CampaignMember.getRecordTypeInfosByDeveloperName().get('MX_BPP_CampaignMember').getRecordTypeId());
        insert oCamp;
        final Account oAcc = new Account(Name='Test Service Account',No_de_cliente__c = '8897251');
        insert oAcc;
        final Lead oLead = new Lead(LastName='Lead Service Test', RecordTypeId=Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('MX_BPP_Leads').getRecordTypeId(),
                MX_ParticipantLoad_id__c=oAcc.No_de_cliente__c, MX_LeadEndDate__c=Date.today().addDays(13), MX_WB_RCuenta__c= oAcc.Id, MX_LeadAmount__c=250, LeadSource='Preaprobados');
        insert oLead;
        final CampaignMember oCampMem = new CampaignMember(CampaignId = oCamp.Id, Status='Sent', LeadId = oLead.Id, MX_LeadEndDate__c=Date.today().addDays(13));
        insert oCampMem;
        MX_BPP_ConvertLeads_Service.convertToLead(new List<Id>{oLead.Id});
    }
    /**
     * *Descripción: Clase de prueba para updateOppFromCampMbrs, validsCampaignMembers y prepareOpp
    **/
    @isTest static void testUpdateOpp() {
        final CampaignMember oCampMbr = [SELECT Id, Status FROM CampaignMember LIMIT 1];
        final Map<Id,CampaignMember> triggerOldMap = new Map<Id,CampaignMember>();
        triggerOldMap.put(oCampMbr.Id, oCampMbr);
        final Map<Id,CampaignMember> triggerNewMap = new Map<Id,CampaignMember>();
        final CampaignMember oCampMbr2 = [SELECT Id, RecordTypeId, LeadSource, Status FROM CampaignMember LIMIT 1];
        oCampMbr2.Status = 'Con Exito';
        triggerNewMap.put(oCampMbr2.Id, oCampMbr2);
        Test.startTest();
        MX_BPP_UpdateRelatedLeadOpp_Service.updateOppFromCampMbrs(triggerNewMap, triggerOldMap);
        Test.stopTest();
        System.assertEquals('Cerrada ganada', [SELECT Id, StageName FROM Opportunity LIMIT 1].StageName, 'Cant updtate the Opportunity');
    }


}