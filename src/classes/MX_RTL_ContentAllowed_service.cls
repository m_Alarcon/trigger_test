/**
* ---------------------------------------------------------------------------------
* @Autor: Rodrigo Amador Martinez Pacheco
* @Proyecto: Auditoria
* @Descripción : Contiene las validaciones requeridas para validar los archivos 
*                que se intentan cargar en extension y tamaño
* ---------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ---------------------------------------------------------------------------------
* 1.0           18/06/2020     Rodrigo Martinez               Creación de la clase
* 1.1           10/08/2020     Angel                          cambio de propiedad temporal debido a bug 
*                                                             en nuevo release salesforce variable pasada retorna null
* 1.2           15/10/2020     Rodrigo Martinez               fix de tamaño de archivo
* ---------------------------------------------------------------------------------
*/
@SuppressWarnings('sf:UseSingleton,sf:AvoidHardCodedError')
public class MX_RTL_ContentAllowed_service {
    /**
* @Autor: Rodrigo Amador Martinez Pacheco
* @Descripción : Punto que separa una extension de archivo
*/
    private static final String SEPARATOR = '\\.';
    /**
* @Autor: Rodrigo Amador Martinez Pacheco
* @Descripción : Multiplicador para convertir MB a Bytes
*/
    private static final Long BYTES_ALLOWED = 1024 * 1024;
    /**
* @Autor: Rodrigo Amador Martinez Pacheco
* @Descripción : Tamaño no valido de lista de extensiones
*/
    private static final Integer EXTENSION_FIND = 0;
    /**
* @Autor: Rodrigo Amador Martinez Pacheco
* @Descripción : Tamanio minimo de lista que regresa split
*/
    private static final Integer MIN_LIST_SIZE = 1;
    
    /**
* @Autor: Rodrigo Amador Martinez Pacheco
* @Descripción : Recibe el nombre de un archivo y regresa la extension 
* extension permitida 
*/
    public static String separateExtension(String filename) {
        final List<String> palabrasSeparadas= filename.split(SEPARATOR);
        if(palabrasSeparadas.size() == MIN_LIST_SIZE) {
            throw new IllegalArgumentException('El archivo no contiene una extensión');
        }
        return palabrasSeparadas.get(palabrasSeparadas.size()-MIN_LIST_SIZE);
    }
    /**
* @Autor: Rodrigo Amador Martinez Pacheco
* @Descripción : Recibe el nombre de un archivo y valida que tenga una 
* extension permitida 
*/
    public static Boolean validateExtension(String filename) {
        return MX_RTL_Allowed_Extensions_selector.selectByExtension(separateExtension(filename).toLowerCase()).size() <= EXTENSION_FIND;
    }
    /**
* @Autor: Rodrigo Amador Martinez Pacheco
* @Descripción : Recibe un listado de archivos para validar si  
* cumplen en extension y tamaño permitido
*/
    public static void validateFile(List<ContentVersion> newContent) {
        for(ContentVersion contentv:newContent) {
            try {
                validateFileExtension(contentv);
            } catch(IllegalArgumentException ex) {
                contentv.addError(ex.getMessage());
            }
        }
    }
    /**
* @Autor: Rodrigo Amador Martinez Pacheco
* @Descripción : Convierte MB a Bytes  
* 
*/
    public static Decimal getBytesAllowed(Decimal mbAllowed) {
        return mbAllowed * BYTES_ALLOWED;
    }
    /**
* @Autor: Rodrigo Amador Martinez Pacheco
* @Descripción : Valida que un archivo tenga el tamaño permitido 
* si no lo es genera una excepcion del tipo IllegalArgumentException
*/
    public static void validateFileSize(ContentVersion contentv) {
        final Decimal size_allowed_mb = MX_RTL_Maximun_File_Size_selector.selectMaximumFileSizeAllowed();
        if(contentv.ContentSize > getBytesAllowed(size_allowed_mb)) {
            throw new DMLException('El tamaño del archivo no puede ser mayor a '+ size_allowed_mb +' MB');
        }
    }
    /**
* @Autor: Rodrigo Amador Martinez Pacheco
* @Descripción : Recibe un archivo para validar si su extension es permitida 
* si no lo es genera una excepcion del tipo IllegalArgumentException
*/
    public static void validateFileExtension(ContentVersion contentv) {
        if(validateExtension(contentv.PathOnClient)) {
            throw new IllegalArgumentException('Tipo de archivo restringido '+separateExtension(contentv.PathOnClient));
        }
    }
   /**
* @Autor: Rodrigo Amador Martinez Pacheco
* @Descripción : Valida que un archivo tenga el tamaño permitido 
* si no lo es genera una excepcion del tipo DMLException para provocar un rollback
*/   
    public static void validateFileAfter(List<ContentVersion> newContent) {
        for(ContentVersion contentv:newContent) {
            try{    
                validateFileSize(contentv);
            }catch(DMLException ex){
                contentv.addError(ex.getMessage());
            }
        }
    }
}