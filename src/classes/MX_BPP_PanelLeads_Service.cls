/**
* @File Name          : MX_BPP_PanelLeads_Ctrl.cls
* @Description        :
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 07/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      07/06/2020            Gabriel Garcia Rojas          Initial Version
**/
public with sharing class MX_BPP_PanelLeads_Service {

     /**Test Constructor */
    @TestVisible
    private MX_BPP_PanelLeads_Service() { }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return Map<String,Object>
    **/
    public static Map<String,Object> convertLeadToOpp(Id leadId) {
        final Map<String,Object> mapResult = new Map<String,Object>();
        try {
            final Opportunity oportunidad = MX_BPP_LeadCampaignTable_Service.getOppFromLeadConv(LeadId);
            mapResult.put('Success',oportunidad);
        } catch (Exception e) {
            final String listString = 'Error: ' + e.getMessage();
            mapResult.put('Error',listString);
        }
        return mapResult;
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return CampaignMember
    **/
    public static CampaignMember getinfoCampignMembers(Id leadId) {
        return MX_RTL_CampaignMember_Selector.getListCampignMembersByLeadIds(new List<Id>{LeadId})[0];
    }


}