/*
* @Nombre: MX_SB_MLT_BusquedaUsuario_cls
* @Autor: Angel Nava
* @Proyecto: Siniestros - BBVA
* @Descripción : cls busqueda de proveedores
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   	Descripción
* --------------------------------------------------------------------------------
* 1.0         11/10/2019     Angel Nava                 	Creación
* --------------------------------------------------------------------------------
*/
public without sharing class MX_SB_MLT_BusquedaUsuario_cls {//NOSONAR
    /*
    * @description busca ajustador
    * @param nombre
    * @return list user
    */    
    @auraenabled
    public static List<User> buscaUsu(String nombre) {
        final String findTxt = '%'+nombre+'%';
         List<User> usuario = new List<User>();
         usuario = [select Id,Name,MX_SB_MLT_TipoTrasporte__c,UserRole.Name from User where Name like :findTxt limit 500];
        
        return  usuario;
    }
    /*
    * @description busca proveedor
    * @param nombre
    * @return list acc
    */    
    @auraenabled
    public static List<Account> buscaCuenta(String nombre) {
        List<Account> cuentas = new List<Account>();
        final String findTxt = '%'+nombre+'%';
        cuentas = [select Id ,Name,Phone,nombreIntermediario__c from account where name like :findTxt limit 1000];
        return cuentas;
    }
    /*
    * @description genera titulos y filtros
    * @param tipo
    * @return list acc
    */    
    @auraenabled
    public static List<Account> busquedaIni(String tipo) {
        List<Account> cuentas = new List<Account>();
        String busqueda ='';
        String tipoTxt ='';
        tipoTxt = 'Proveedor Cristalera';
        switch on tipo {
    		when 'Proveedor de Gasolina' { 
				busqueda='Gasolina';
    	    } 
            when 'Proveedor de Cambio de llanta' { 
				busqueda='Cambio de llanta';
    	    } 
            when 'Proveedor de Paso de corriente' { 
				busqueda='Paso de corriente';
    	    } 
            when 'Proveedor de Grúa' { 
				busqueda='Grúa';
    	    } 
        }
        if(tipo==tipoTxt && String.isEmpty(busqueda)) {
             cuentas = [select Id ,Name,Phone,nombreIntermediario__c from account limit 1000];
       
        } else {
           cuentas = [select Id ,Name, Phone,nombreIntermediario__c from account limit 1000];
         
        }
        return cuentas;
    }
}