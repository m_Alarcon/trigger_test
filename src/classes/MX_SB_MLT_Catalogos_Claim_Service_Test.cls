@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_Catalogos_Claim_Service_Test
* Autor: Eder Alberto Hernández Carbajal
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_Catalogos_Claim_Service

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         04/06/2020   Eder Alberto Hernández Carbajal              Creación
* -----------------------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_Catalogos_Claim_Service_Test {
    
    /**Tipo de catalogo Ocupacion */
    Final static String OCUPACION = 'Ocupación';
    
    /**Tipo de catalogo Ocupacion */
    Final static String ICD = 'ICD';
    
    /**Descripción Test*/
    Final static String DESC_OCP = 'WINCHERO';
    
    /**Descripción Test*/
    Final static String DESC_ICD = 'SHIGELOSIS';

    /*
    * @description method que prueba MX_SB_MLT_Catalogos_Claim_Service constructor
    * @param  void
    * @return void
    */
    @isTest
    private static void testConstructor() {
        Final MX_SB_MLT_Catalogos_Claim_Service constService = new MX_SB_MLT_Catalogos_Claim_Service();
        system.assert(true,constService);
    }
    /*
    * @description method que prueba MX_SB_MLT_Catalogos_Claim_Service getCatalogByDesc ICD
    * @param  void
    * @return void
    */
    @isTest
    static void testGetCatalogByDescICD() {
        Test.startTest();
        MX_SB_MLT_Catalogos_Claim_Service.getCatalogByDesc(DESC_ICD, ICD);
        Test.stopTest();
        System.assert(true, 'Se encontraron coincidencias.');
    }
    /*
    * @description method que prueba MX_SB_MLT_Catalogos_Claim_Service getCatalogByDesc Ocupacion
    * @param  void
    * @return void
    */
    @isTest
    static void testGetCatalogByDescOcp() {
        Test.startTest();
        MX_SB_MLT_Catalogos_Claim_Service.getCatalogByDesc(DESC_OCP, OCUPACION);
        Test.stopTest();
        System.assert(true, 'Se encontraron coincidencias.');
    }
}