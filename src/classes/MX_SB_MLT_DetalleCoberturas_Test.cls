/**
 * @description       : 
 * @author            : Jaime Terrats
 * @group             : 
 * @last modified on  : 07-15-2020
 * @last modified by  : Jaime Terrats
 * Modifications Log 
 * Ver   Date         Author          Modification
 * 1.0   07-15-2020   Jaime Terrats   Initial Version
**/
@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_DetalleCoberturas_Test
* Autor Daniel Perez Lopez
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_BusquedaPoliza_Cls_Controller

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           14/02/2020      Daniel Lopez                         Creación
* --------------------------------------------------------------------------------
*/

public with sharing class MX_SB_MLT_DetalleCoberturas_Test {

    @testSetup
    static void setDatos() {
        Final String nameProfileDtCbr = [SELECT Name from profile where name in ('Administrador del sistema','System Administrator') limit 1].Name;
        final User objUsrTstDtCbr = MX_WB_TestData_cls.crearUsuario('PruebaAdminTst', nameProfileDtCbr);
        insert objUsrTstDtCbr;

        System.runAs(objUsrTstDtCbr) {
        final Account objAccTstCbr = MX_WB_TestData_cls.crearCuenta ( 'LastName', 'MX_WB_rt_PAcc_Telemarketing' );
            insert objAccTstCbr;
            final Contract contratoCbr = new Contract(accountid=objAccTstCbr.Id);
            insert contratoCbr;
            final Siniestro__c sini2= new Siniestro__c();
            sini2.Folio__c='sinitest2';
            sini2.Poliza__c='folio2';
            sini2.MX_SB_MLT_Certificado_Inciso__c='1';
            sini2.MX_SB_MLT_Placas__c='ldny123';
            sini2.MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro';
            sini2.MX_SB_SAC_Contrato__c=contratoCbr.Id;
            insert sini2;
            final Siniestro__c sini= new Siniestro__c();
            sini.Folio__c='sinitest';
            sini.MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro';
            sini.MX_SB_SAC_Contrato__c=contratoCbr.Id;
            insert sini;
            final Case objCaseTstCbr = new Case(Status='New',Origin='Phone',Priority='Medium',AccountId=objAccTstCbr.Id,MX_SB_MLT_URLLocation__c='19.305819,-99.2115422',MX_SB_MLT_Siniestro__c =sini.id);
            insert objCaseTstCbr;
        }
    }

     @IsTest static void getTableCoberturas() {
        Test.startTest();
         final String[] datoscampos=MX_SB_MLT_DetalleCoberturas.campoCobert;
            final Cobertura__c [] coberts = MX_SB_MLT_DetalleCoberturas.getTableCoberturas(datoscampos[0],datoscampos[1]);
            final boolean cobertexist= coberts.size()>0?true:false;
            System.assert(cobertexist,'Excepción Propagada Correctamente');
        Test.stopTest();
    }

    @IsTest static void getTableCoberturasElse() {
        MX_SB_MLT_DetalleCoberturas.getTableDeducibles('');
        insert new Cobertura__c(MX_SB_MLT_Cobertura__c='CABA',MX_SB_MLT_IdeCobertura__c='1701623');
        Test.startTest();
        final Cobertura__c[] listacob = MX_SB_MLT_DetalleCoberturas.getTableCoberturas('','');
            final boolean vacio = listacob.size()>0 ? true : false;
            System.assert(vacio,'Excepción Propagada Correctamente');
        Test.stopTest();
    }
}