/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_GlosarioMinuta_Selector
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2021-02-19
* @Description 	Selector for MX_BPP_GlosarioMinuta__mdt
* @Changes
* 2021-03-03	Edmundo Zacarias		New method added: getAllRecords
*/
public without sharing class MX_BPP_GlosarioMinuta_Selector {
	
    /** Constructor */
    @TestVisible
    private MX_BPP_GlosarioMinuta_Selector() {}
    
    /**
    * @description Retrieves metadata record from MX_BPP_GlosarioMinuta__mdt custom metadata object
    based on the query fields and Developer Names recieved as arguments.
    * @author Edmundo Zacarias Gómez
    * @param String queryfields, List<String> listDevName
    * @return List<MX_BPP_GlosarioMinuta__mdt>
    **/
    public static List<MX_BPP_GlosarioMinuta__mdt> getRecordsByDevName(String queryfields, Set<String> listDevName) {
        final String query ='SELECT '+ queryfields +' FROM MX_BPP_GlosarioMinuta__mdt WHERE DeveloperName IN: listDevName';
        return Database.query(String.escapeSingleQuotes(query));
    }
    
    /**
    * @description Retrieves all metadata record from MX_BPP_GlosarioMinuta__mdt custom metadata object.
    * @author Edmundo Zacarias Gómez
    * @param String queryfields
    * @return List<MX_BPP_GlosarioMinuta__mdt>
    **/
    public static List<MX_BPP_GlosarioMinuta__mdt> getAllRecords(String queryfields) {
        final String query ='SELECT '+ queryfields +' FROM MX_BPP_GlosarioMinuta__mdt';
        return Database.query(String.escapeSingleQuotes(query));
    }
}