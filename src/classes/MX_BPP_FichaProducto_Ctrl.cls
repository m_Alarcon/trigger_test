/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_FichaProducto_Ctrl
* @Author   	Héctor Israel Saldaña Pérez | hectorisrael.saldana.contractor@bbva.com
* @Date     	Created: 2021-03-29
* @Description 	Controller class dedicated for module Ficha Producto
* @Changes
*
*/
public without sharing class MX_BPP_FichaProducto_Ctrl {

	/** Constructor */
	@TestVisible
	private MX_BPP_FichaProducto_Ctrl() {}

	/**
    * @description Retrieves ContentVersion record(s) related to a LinkedEntityId
    * @author Héctor Saldaña
    * @params String recordId
    * @return JSON.serialize
    **/
	@AuraEnabled
	public static String getContentDetails(String recordId) {
		final List<ContentDocumentLink> cdlLst = MX_RTL_ContentVersion_Selector.getContentDocLinkByLinkEntityId(recordId);
		final Set<Id> contentDocumentId = MX_BPP_FichaProducto_Service.getDocIds(cdlLst);
		final List<ContentVersion> cvLst = MX_RTL_ContentVersion_Selector.getContentVersionByContentDocument(contentDocumentId);

		return JSON.serialize(cvLst);
	}

	/**
    * @description Retrieves ContentVersion records based on the ContentDocumentId specified in params as a  Set collection
    * @author Héctor Saldaña
    * @params String queryfields, Set<Id> idDocs
    * @return List<ContentVersion>
    **/
	@AuraEnabled
	public static String getAssetLibraries(List<Id> cntDocsIds) {
		final Set<Id> setDocIds = new Set<Id>(cntDocsIds);
		final List<ContentWorkspace> libraries = MX_RTL_ContentVersion_Selector.getLibrariesByContentDocument(setDocIds);

		return JSON.serialize(libraries);
	}

	/**
    * @description Retrieves ContentVersion records based on the ContentDocumentId specified in params as a  Set collection
    * @author Héctor Saldaña
    * @params String queryfields, Set<Id> idDocs
    * @return List<ContentVersion>
    **/
	@AuraEnabled
	public static String getContentDocuments(List<Id> docIds) {
		final Set<Id> setDocIds = new Set<Id>(docIds);
		final List<ContentDocument> lstDocs = MX_RTL_ContentVersion_Selector.getContentDocumentById(setDocIds);

		return JSON.serialize(lstDocs);
	}

}