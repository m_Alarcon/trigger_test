/**
* @File Name          : MX_BPP_LeadCampaign_Service_Test.cls
* @Description        : Test class for MX_BPP_LeadCampaign_Service
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 06/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      06/06/2020            Gabriel Garcia Rojas          Initial Version
**/
@isTest
private class MX_BPP_LeadCampaign_Service_Test {
	/*Usuario de pruebas*/
    private static User testUser = new User();

    /** namerole variable for records */
    final static String NAMEROLE = '%BPYP BANQUERO BANCA PERISUR%';
    /** nameprofile variable for records */
    final static String NAMEPROFILE = 'BPyP Estandar';
    /** name variable for records */
    final static String NAME = 'testUser';
    /** error message */
    final static String MESSAGEFAIL = 'Fail method';
    /** name Account*/
    final static String NAMEACC = 'Cuenta Personal BPyP Test';

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testUser = UtilitysDataTest_tst.crearUsuario(NAME, NAMEPROFILE, NAMEROLE);
        insert testUser;
        final Double numCliente = Math.random()*10000000;

        final Account cuenta = new Account();
		cuenta.LastName = NAMEACC;
        cuenta.PersonEmail = 'test@test.com';
		cuenta.No_de_cliente__c = String.valueOf(numCliente.round());
		cuenta.RecordTypeId = RecordTypeMemory_cls.getRecType('Account', 'MX_BPP_PersonAcc_Client');
		insert cuenta;

        final Lead candidato = new Lead();
        candidato.LastName = 'Candidato 1';
        candidato.MX_ParticipantLoad_id__c = cuenta.No_de_cliente__c;
        candidato.RecordTypeId = RecordTypeMemory_cls.getRecType('Lead', 'MX_BPP_Leads');

        insert candidato;

    }

     /**
     * @description constructor test
     * @author Gabriel Garcia | 06/06/2020
     * @return void
     **/
    @isTest
    private static void testConstructor() {
        final MX_BPP_LeadCampaign_Service instanceC = new MX_BPP_LeadCampaign_Service();
        System.assertNotEquals(instanceC, null, MESSAGEFAIL);
    }

    /**
     * @description test method assignOwnerByNumberAccount
     * @author Gabriel Garcia | 06/06/2020
     * @return void
     **/
    @isTest
    private static void assignOwnerByNumberAccountTest() {
		final List<Lead> listLeads = [SELECT Id, MX_ParticipantLoad_id__c, OwnerId, RecordTypeId FROM Lead LIMIT 10];

        Test.startTest();
        MX_BPP_LeadCampaign_Service.assignOwnerByNumberAccount(listLeads);
        Test.stopTest();

        System.assertNotEquals(listLeads, null, MESSAGEFAIL);
    }


}