@isTest
/**
 * @File Name          : MX_SB_MLT_DetallesServiciosEnviados_tst
 * @Description        : Clase test de ejecución de Servicios
 * @Author             : Angel Nava
 * @Last Modified On   : 2/18/2020, 11:28:57 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/18/2020   		Angel Nava           	Initial Version
 * 1.1    2/18/2020   		Angel Nava              Corrige Code Smells 
 * 1.2    2/20/2020         Angel Nava              codesmells minor
 * 1.3    11/03/2020        Angel Nava              migración cotract
**/
public class MX_SB_MLT_DetallesServiciosEnviados_tst {

    @testSetup
    static void creaDatosIniciales() {  
        final String nameProf = [SELECT Name from profile where name in ('Administrador del sistema','System Administrator') limit 1].Name;
        final User usrTest = MX_WB_TestData_cls.crearUsuario('PruebaAdminTst', nameProf);
        usrTest.MX_SB_MLT_ClaimCenterId__c = '22337347373737';
        insert usrTest;
        
        System.runAs(usrTest) {
            String value = '';
            value = 'Abierto';
            final String  tipoRegistro = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            final Account acctest = new Account(recordtypeid=tipoRegistro,firstname='DANIEL',lastname='PEREZ',Apellido_materno__pc='LOPEZ',RFC__c='PELD920912',PersonEmail='test@gmail.com');
            insert acctest;

            final Account ctatestluffy = new Account(Name='luffy',Tipo_Persona__c='Física' );
            insert ctatestluffy;
            
            final Contract contrato = new Contract(accountid=ctatestluffy.Id, MX_SB_SAC_NumeroPoliza__c='PolizaTest',MX_SB_SAC_RFCAsegurado__c =ctatestluffy.rfc__c,MX_SB_SAC_NombreClienteAseguradoText__c=ctatestluffy.firstname,MX_WB_apellidoPaternoAsegurado__c=ctatestluffy.lastname,MX_WB_apellidoMaternoAsegurado__c=ctatestluffy.Apellido_materno__pc);
            insert contrato;
            
            final Siniestro__c siniSetup= new Siniestro__c();
            siniSetup.MX_SB_SAC_Contrato__c=contrato.Id;
            siniSetup.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoAuto).getRecordTypeId();
            siniSetup.TipoSiniestro__c = 'Robo';
            siniSetup.MX_SB_MLT_TipoRobo__c = Label.MX_SB_MLT_RoboParcial;
            siniSetup.MX_SB_MLT_ServicioAjAuto__c = false;
            siniSetup.MX_SB_MLT_RequiereAsesor__c = false;
            siniSetup.MX_SB_MLT_Address__c = 'México';
            Final Datetime myDateTime = Datetime.now();
            siniSetup.MX_SB_MLT_Fecha_Hora_Siniestro__c = myDateTime;
            insert siniSetup;
            
            Final WorkOrder workOrderTest = new WorkOrder ();
            workOrderTest.MX_SB_MLT_TipoServicioAsignado__c = Label.MX_SB_MLT_AsesorRobo;
            workOrderTest.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Servicios).getRecordTypeId();
            workOrderTest.MX_SB_MLT_Ajustador__c = usrTest.Id;
            workOrderTest.MX_SB_MLT_EstatusServicio__c = value;
            workOrderTest.MX_SB_MLT_Siniestro__c = siniSetup.Id;
            workOrderTest.MX_SB_MLT_ServicioAsignado__c = Label.MX_SB_MLT_Ajustador;
            insert workOrderTest;
            
		final String  rectyp = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_RamoAuto').getRecordTypeId();
    	final Siniestro__c sinies = new Siniestro__c(recordtypeid=rectyp,MX_SB_SAC_Contrato__c=contrato.Id,TipoSiniestro__c='Asistencia Vial',MX_SB_MLT_JourneySin__c='Validación de Poliza',MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com'
                                                    ,MX_SB_MLT_Address__c='México');
        insert sinies;
        final WorkOrder woGrua = new WorkOrder(MX_SB_MLT_Siniestro__c=sinies.Id,MX_SB_MLT_Email__c='lawbh93@gmail.com',MX_SB_MLT_TipoSiniestro__c = 'Grua', MX_SB_MLT_EstatusServicio__c = value);
		insert woGrua;
        final WorkOrder woGas = new WorkOrder(MX_SB_MLT_Siniestro__c=sinies.Id,MX_SB_MLT_Email__c='lawbh93@outlook.com',MX_SB_MLT_TipoSiniestro__c = 'Gasolina', MX_SB_MLT_EstatusServicio__c = value);
        insert woGas;
        final WorkOrder woCambio = new WorkOrder(MX_SB_MLT_Siniestro__c=sinies.Id,MX_SB_MLT_Email__c='lawbh93@aol.com',MX_SB_MLT_TipoSiniestro__c = 'Cambio de llanta', MX_SB_MLT_EstatusServicio__c = value);
		insert woCambio;
        final WorkOrder woPasoC = new WorkOrder(MX_SB_MLT_Siniestro__c=sinies.Id,MX_SB_MLT_Email__c='lawbh93@hotmail.com',MX_SB_MLT_TipoSiniestro__c = 'Paso de corriente', MX_SB_MLT_EstatusServicio__c = value);
		insert woPasoC;
            
        }
    }
    
   @isTest
    static void updateSiniTest () {
        final Siniestro__c siniTest = [SELECT Id, RecordTypeId, MX_SB_MLT_TipoRobo__c FROM Siniestro__c limit 1];
        siniTest.TipoSiniestro__c ='Robo';
        test.startTest();
        MX_SB_MLT_DetallesServiciosEnviados_cls.updateSini(siniTest);
        system.assertNotEquals(null, siniTest.TipoSiniestro__c, 'prueb exitosa');
        test.stopTest();
    }
    @isTest
    static void pruebaObtieneSin() {
        final siniestro__c sin = [select id,Name,TipoSiniestro__c from siniestro__c limit 1];
        test.startTest();
        final siniestro__c sinRes = MX_SB_MLT_DetallesServiciosEnviados_cls.getSiniestroD(sin.id);
       system.assertEquals(sin.TipoSiniestro__c, sinRes.TipoSiniestro__c,'prueba de id sin');
        test.stopTest();
    }
     @isTest static void obWGrua() {
        final Siniestro__c sinies = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com' limit 1];
		Test.startTest();
		final String ide = sinies.Id;
         final workorder res = MX_SB_MLT_DetallesServiciosEnviados_cls.getGruaWO(ide);
        system.assertNotEquals(null,res,'grua prueba');
        test.stopTest();
    }
    @isTest static void obWCambioL() {
        final Siniestro__c sinies2 = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com' limit 1];
		Test.startTest();
		final String ide2 = sinies2.Id;
       final workorder res2 =  MX_SB_MLT_DetallesServiciosEnviados_cls.getCambioLLWO(ide2);
        system.assertNotEquals(null,res2,'llanta prueba');
        test.stopTest();
    }
        @isTest static void obWPasoC() {
        final Siniestro__c sinies3 = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com'];
		Test.startTest();
		final String ide3 = sinies3.Id;
         final workorder res3 =  MX_SB_MLT_DetallesServiciosEnviados_cls.getPasoCWO(ide3);
        system.assertNotEquals(null,res3,'paso prueba');
        test.stopTest();
    }
	@isTest static void obWGas() {
        final Siniestro__c sinies4 = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com'];
		Test.startTest();
		final String ide4 = sinies4.Id;
        final workorder res4 =  MX_SB_MLT_DetallesServiciosEnviados_cls.getGasWO(ide4);
        system.assertNotEquals(null,res4,'paso prueba');
        test.stopTest();
    }
    @isTest
    static void updategasCorriente() {
        final account prov = [select id from account where name='luffy' limit 1];
         final Siniestro__c siniGas = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c,MX_SB_MLT_Address__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com'];
		Test.startTest();
		final String ideGas = siniGas.Id;
        final workorder res4 =  MX_SB_MLT_DetallesServiciosEnviados_cls.getGasWO(ideGas);
        MX_SB_MLT_DetallesServiciosEnviados_cls.updateCaseGasolina(res4.id, prov.id, siniGas, res4, true);
        final workorder corrWork =  MX_SB_MLT_DetallesServiciosEnviados_cls.getPasoCWO(ideGas);
        MX_SB_MLT_DetallesServiciosEnviados_cls.updateCasePasoC(corrWork.id, prov.id, corrWork, siniGas, true);
        system.assertNotEquals(null,res4,'paso update');
        test.stopTest();
    }
     @isTest
    static void updatellantaGrua() {
        final account prov2 = [select id from account where name='luffy' limit 1];
         final Siniestro__c siniLlanta = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c,
                                          MX_SB_MLT_Address__c,MX_SB_MLT_Fecha_Hora_Siniestro__c,
                                          MX_SB_MLT_EntreCalles_Destino__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com'];
		Test.startTest();
		final String ideGas2 = siniLlanta.Id;
        final workorder res5 =  MX_SB_MLT_DetallesServiciosEnviados_cls.getCambioLLWO(ideGas2);
        MX_SB_MLT_DetallesServiciosEnviados_cls.updateCaseCambioLL(res5.id, prov2.id, res5, siniLlanta, true);
          final workorder gruaWork =  MX_SB_MLT_DetallesServiciosEnviados_cls.getGruaWO(ideGas2);
        
        MX_SB_MLT_DetallesServiciosEnviados_cls.updateCaseGrua(gruaWork.id, prov2.id, siniLlanta,gruaWork,true);
        system.assertNotEquals(null,res5,'llanta y grua update');
        siniLlanta.MX_SB_MLT_URLLocation__c='-19.993,99.56273';
        update siniLlanta;
        MX_SB_MLT_DetallesServiciosEnviados_cls.updateCaseGrua(gruaWork.id, prov2.id, siniLlanta,gruaWork,true);
        test.stopTest();
    }
}