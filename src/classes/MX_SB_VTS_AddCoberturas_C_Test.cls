/**
* @description       : Clase de cobertura que cubre MX_SB_VTS_AddCoberturas_Ctrl
* @author            : Diego Olvera
* @group             : BBVA
* @last modified on  : 03-19-2021
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   10-21-2020   Diego Olvera   Initial Version
* 1.1   02-03-2021   Diego Olvera   Se agrega mapa con valores para cubrir cobertura
* 1.1.1 19-03-2021   Diego Olvera   Se agrega paremetro faltante en mapa
**/
@isTest
public class MX_SB_VTS_AddCoberturas_C_Test {
    /*User name test*/
    private final static String USERTEST = 'ASESORDATAPART';
    /*Account name test*/
    private final static String ACCOTEST = 'CUENTADATAPART';
    /**@description Nombre de Oportunidad*/
    private final static String OPPCMBNAME = 'TestOppCmb';
    /** Variable de Apoyo: sUserName */
    Static String sUserName = 'UserOwnerTest01';
    /** Variable de Apoyo: sURL */
    Static String sUrl = 'http://www.example.com';
    /** Variable de Apoyo: sNoEmpty */
    Static String sNoEmpty = 'Lista No Vacia';
    
    @TestSetup
    static void makeData() {
        final User tstDpUserTest = MX_WB_TestData_cls.crearUsuario(USERTEST, 'System Administrator');
        insert tstDpUserTest;
        final Account accntCmb = MX_WB_TestData_cls.createAccount(ACCOTEST, System.Label.MX_SB_VTS_PersonRecord);
        insert accntCmb;
        final Opportunity oppCmb = MX_WB_TestData_cls.crearOportunidad(OPPCMBNAME, accntCmb.Id, tstDpUserTest.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
        oppCmb.LeadSource = 'Call me Back';
        oppCmb.Producto__c = 'Hogar seguro dinámico';
        oppCmb.StageName = 'Contacto';
        insert oppCmb;
        final Quote quoteRec = new Quote(OpportunityId=oppCmb.Id, Name='Outbound');
        quoteRec.MX_SB_VTS_Folio_Cotizacion__c = '5564322';
        quoteRec.MX_SB_VTS_ASO_FolioCot__c = '1008378';
        quoteRec.Status = 'Creada';
        insert quoteRec;
        oppCmb.SyncedQuoteId = quoteRec.Id;
        update oppCmb;
        final Cobertura__c crtCober = new Cobertura__c();
        crtCober.MX_SB_VTS_RelatedQuote__c = quoteRec.Id;
        crtCober.MX_SB_VTS_CodeTrade__c = 'TECN';
        crtCober.MX_SB_VTS_TradeValue__c = 'TECHNICAL';
        crtCober.MX_SB_VTS_CategoryCode__c = 'ELECTRONIC_DEVICES';
        crtCober.MX_SB_VTS_GoodTypeCode__c = 'ELECTRONIC_DEVICES';
        crtCober.MX_SB_VTS_CoverageCode__c = 'ELECTRONIC_EQUIPMENT';
        crtCober.MX_SB_MLT_Descripcion__c ='ELECTRONIC_EQUIPMENT';
        crtCober.MX_SB_MLT_Estatus__c = 'Activo';
        insert crtCober;
        final Cobertura__c crtCober2 = new Cobertura__c();
        crtCober2.MX_SB_VTS_RelatedQuote__c = quoteRec.Id;
        crtCober2.MX_SB_VTS_CodeTrade__c = 'TECN';
        crtCober2.MX_SB_VTS_TradeValue__c = 'TECHNICAL';
        crtCober2.MX_SB_VTS_CategoryCode__c = 'ELECTRONIC_DEVICES';
        crtCober2.MX_SB_VTS_GoodTypeCode__c = 'HOME_APPLIANCE';
        crtCober2.MX_SB_VTS_CoverageCode__c = 'APPLIANCE_EQUIPMENT';
        crtCober2.MX_SB_MLT_Descripcion__c ='APPLIANCE_EQUIPMENT';
        crtCober2.MX_SB_MLT_Estatus__c = 'Activo';
        insert crtCober2;
        
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'CalculateQuotePrice', iaso__Url__c = sUrl, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
    }
    
    @isTest
    static void addCoveragesTestN() {
        final User objUserTst = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(objUserTst) {
            final Map<String, String> mHeadersMock = new Map<String, String>();
            mHeadersMock.put('tsec', '1234567890');
            final MX_WB_Mock objMockCallOut = new MX_WB_Mock(200, 'Complete', '{"data":{"trades":[{"id":"EARTHQUAKE","description":"TERREMOTO","categories":[{"id":"BUILDINGS","description":"","goodTypes":[{"id":"BUILDING","code":"16587103","description":"EDIFICIO","coverages":[{"id":"EXTRAORDINARY_EXPENSES","description":"GASTOS EXTRAORDINARIOS","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":0.08,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":0.08,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":30000.02,"currency":"MXN"}]}]},{"id":"DEBRIS_REMOVAL","description":"REMOCIÓN DE ESCOMBROS","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":0.19,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":0.19,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":30000.02,"currency":"MXN"}]}]},{"id":"EARTHQUAKE_VOLCANIC_ERUPTION","description":"TERREMOTO Y ERUPCIÓN VOLCÁNICA","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":330.19,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":330.19,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":150000.1,"currency":"MXN"}]}]}]}]},{"id":"INVENTORY_AND_FURNITURE","description":"","goodTypes":[{"id":"CONTENT","code":"16587104","description":"CONTENIDO","coverages":[{"id":"EXTRAORDINARY_EXPENSES","description":"GASTOS EXTRAORDINARIOS","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":0.04,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":0.04,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":15000.01,"currency":"MXN"}]}]},{"id":"DEBRIS_REMOVAL","description":"REMOCIÓN DE ESCOMBROS","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":0.09,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":0.09,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":15000.01,"currency":"MXN"}]}]},{"id":"EARTHQUAKE_VOLCANIC_ERUPTION","description":"TERREMOTO Y ERUPCIÓN VOLCÁNICA","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":165.09,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":165.09,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":75000.05,"currency":"MXN"}]}]}]}]}]},{"id":"CIVIL_LIABILITY_INSURANCE","description":"SEGURO DE R. C. PRIVADA Y FAMILIAR","categories":[{"id":"CIVIL_LIABILITY","description":"","goodTypes":[{"id":"FAMILY_CIVIL_LIABILITY","code":"16587098","description":"R. C. FAMILIAR","coverages":[{"id":"FAMILY_CIVIL_LIABILITY","description":"R.C. PRIVADA Y FAMILIAR","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":0.95,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":0.95,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":150000.1,"currency":"MXN"}]}]}]}]}]},{"id":"FIRE","description":"INCENDIO","categories":[{"id":"BUILDINGS","description":"","goodTypes":[{"id":"BUILDING","code":"16589121","description":"EDIFICIO","coverages":[{"id":"EXTRAORDINARY_EXPENSES","description":"GASTOS EXTRAORDINARIOS","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":0.08,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":0.08,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":30000.02,"currency":"MXN"}]}]},{"id":"FIRE_LIGHTNING","description":"INCENDIO Y/O RAYO","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":877.55,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":877.55,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":150000.1,"currency":"MXN"}]}]},{"id":"DEBRIS_REMOVAL","description":"REMOCIÓN DE ESCOMBROS","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":0.19,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":0.19,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":30000.02,"currency":"MXN"}]}]}]}]},{"id":"INVENTORY_AND_FURNITURE","description":"","goodTypes":[{"id":"CONTENT","code":"16587093","description":"CONTENIDO","coverages":[{"id":"EXTRAORDINARY_EXPENSES","description":"GASTOS EXTRAORDINARIOS","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":0.04,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":0.04,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":15000.01,"currency":"MXN"}]}]},{"id":"FIRE_LIGHTNING","description":"INCENDIO Y/O RAYO","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":3.17,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":3.17,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":75000.05,"currency":"MXN"}]}]},{"id":"DEBRIS_REMOVAL","description":"REMOCIÓN DE ESCOMBROS","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":0.09,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":0.09,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":15000.01,"currency":"MXN"}]}]}]}]}]},{"id":"TECHNICAL","description":"TECNICO","categories":[{"id":"ELECTRONIC_DEVICES","description":"","goodTypes":[{"id":"HOME_APPLIANCE","code":"16587102","description":"EQUIPO ELECTRODOMESTICO","coverages":[{"id":"APPLIANCE_EQUIPMENT","description":"EQUIPO ELECTRODOMESTICO","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":4.42,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":4.42,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":22500.02,"currency":"MXN"}]}]}]},{"id":"ELECTRONIC_DEVICES","code":"16587101","description":"EQUIPOS ELECTRONICOS","coverages":[{"id":"ELECTRONIC_EQUIPMENT","description":"EQUIPO ELECTRONICO","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":7.36,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":7.36,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":37500.03,"currency":"MXN"}]}]}]}]}]},{"id":"HYDROMETEOROLOGICAL_RISKS","description":"RIESGOS HIDROMETEOROLOGICOS","categories":[{"id":"BUILDINGS","description":"","goodTypes":[{"id":"BUILDING","code":"16587099","description":"EDIFICIO","coverages":[{"id":"EXTRAORDINARY_EXPENSES","description":"GASTOS EXTRAORDINARIOS","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":0.08,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":0.08,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":30000.02,"currency":"MXN"}]}]},{"id":"DEBRIS_REMOVAL","description":"REMOCIÓN DE ESCOMBROS","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":0.19,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":0.19,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":30000.02,"currency":"MXN"}]}]},{"id":"HYDROMETEOROLOGICAL_RISKS","description":"RIESGOS HIDROMETEOROLOGICOS","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":164.81,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":164.81,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":150000.1,"currency":"MXN"}]}]}]}]},{"id":"INVENTORY_AND_FURNITURE","description":"","goodTypes":[{"id":"CONTENT","code":"16587100","description":"CONTENIDO","coverages":[{"id":"EXTRAORDINARY_EXPENSES","description":"GASTOS EXTRAORDINARIOS","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":0.04,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":0.04,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":15000.01,"currency":"MXN"}]}]},{"id":"DEBRIS_REMOVAL","description":"REMOCIÓN DE ESCOMBROS","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":0.09,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":0.09,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":15000.01,"currency":"MXN"}]}]},{"id":"HYDROMETEOROLOGICAL_RISKS","description":"RIESGOS HIDROMETEOROLOGICOS","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":82.41,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":82.41,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":75000.05,"currency":"MXN"}]}]}]}]}]},{"id":"MISCELLANEOUS","description":"MISCELANEOS","categories":[{"id":"PERSONAL_ITEMS","description":"","goodTypes":[{"id":"ART_SCULPTURE","code":"16599063","description":"OBRAS DE ARTE (MUSEOS Y GALERIAS)","coverages":[{"id":"THEFT_WORKS_ART","description":"ROBO OBRAS DE ARTE","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":0.29,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":0.29,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":11250.01,"currency":"MXN"}]}]}]}]},{"id":"ELECTRONIC_DEVICES","description":"","goodTypes":[{"id":"HOME_AMENITIES","code":"16597162","description":"MENAJE DE CASA","coverages":[{"id":"HOME_BURGLARY","description":"ROBO MENAJE DE CASA","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":36.78,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":36.78,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":37500.03,"currency":"MXN"}]}]}]}]},{"id":"VALUABLE_ITEMS","description":"","goodTypes":[{"id":"MONEY_VALUES","code":"16596254","description":"DINERO Y VALORES","coverages":[{"id":"BURGLARY_WITH_VIOLENCE","description":"BASICA, ROBO CON VIOLENCIA  Y/O ASALTO","premiums":[{"id":"YEARLY_PREMIUM","amounts":[{"amount":7,"currency":"MXN","isMajor":true}]},{"id":"PAYMEMT_METHOD","amounts":[{"amount":7,"currency":"MXN","isMajor":true}]},{"id":"INSURED_AMOUNT","amounts":[{"amount":8250.01,"currency":"MXN"}]}]}]}]}]}],"disasterCovered":[{"id":"TERR","value":"2"},{"id":"RIHI","value":"3"}],"frequencies":[{"id":"YEARLY","description":"ANUAL","premiums":[{"id":"YEARLY_NET_PREMIUM","amounts":[{"amount":1681.22,"currency":"MXN","isMajor":true}]},{"id":"SUBSEQUENT_PAYMENT","amounts":[{"amount":0,"currency":"MXN","isMajor":true}]},{"id":"POLICY_FEE","amounts":[{"amount":0,"currency":"MXN","isMajor":true}]},{"id":"FRACTIONAL_PAYMENT","amounts":[{"amount":0,"currency":"MXN","isMajor":true}]},{"id":"TAX","amounts":[{"amount":269,"currency":"MXN","isMajor":true}]},{"id":"YEARLY_TOTAL","amounts":[{"amount":1950.22,"currency":"MXN","isMajor":true}]},{"id":"WITHOUT_DISCOUNT","amounts":[{"amount":2166.91,"currency":"MXN","isMajor":true}]},{"id":"FIRST_PAYMENT","amounts":[{"amount":1950.22,"currency":"MXN","isMajor":true}]}],"numberSubsequentPayments":0},{"id":"MONTHLY","description":"MENSUAL","premiums":[{"id":"YEARLY_NET_PREMIUM","amounts":[{"amount":1834.06,"currency":"MXN","isMajor":true}]},{"id":"SUBSEQUENT_PAYMENT","amounts":[{"amount":177.29,"currency":"MXN","isMajor":true}]},{"id":"POLICY_FEE","amounts":[{"amount":0,"currency":"MXN","isMajor":true}]},{"id":"FRACTIONAL_PAYMENT","amounts":[{"amount":0,"currency":"MXN","isMajor":true}]},{"id":"TAX","amounts":[{"amount":293.45,"currency":"MXN","isMajor":true}]},{"id":"YEARLY_TOTAL","amounts":[{"amount":2127.51,"currency":"MXN","isMajor":true}]},{"id":"WITHOUT_DISCOUNT","amounts":[{"amount":196.99,"currency":"MXN","isMajor":true}]},{"id":"FIRST_PAYMENT","amounts":[{"amount":177.29,"currency":"MXN","isMajor":true}]}],"numberSubsequentPayments":11},{"id":"SEMESTER","description":"SEMESTRAL","premiums":[{"id":"YEARLY_NET_PREMIUM","amounts":[{"amount":1772.92,"currency":"MXN","isMajor":true}]},{"id":"SUBSEQUENT_PAYMENT","amounts":[{"amount":1028.3,"currency":"MXN","isMajor":true}]},{"id":"POLICY_FEE","amounts":[{"amount":0,"currency":"MXN","isMajor":true}]},{"id":"FRACTIONAL_PAYMENT","amounts":[{"amount":0,"currency":"MXN","isMajor":true}]},{"id":"TAX","amounts":[{"amount":283.67,"currency":"MXN","isMajor":true}]},{"id":"YEARLY_TOTAL","amounts":[{"amount":2056.59,"currency":"MXN","isMajor":true}]},{"id":"WITHOUT_DISCOUNT","amounts":[{"amount":1142.54,"currency":"MXN","isMajor":true}]},{"id":"FIRST_PAYMENT","amounts":[{"amount":1028.29,"currency":"MXN","isMajor":true}]}],"numberSubsequentPayments":1},{"id":"QUARTELY","description":"TRIMESTRAL","premiums":[{"id":"YEARLY_NET_PREMIUM","amounts":[{"amount":1834.06,"currency":"MXN","isMajor":true}]},{"id":"SUBSEQUENT_PAYMENT","amounts":[{"amount":531.88,"currency":"MXN","isMajor":true}]},{"id":"POLICY_FEE","amounts":[{"amount":0,"currency":"MXN","isMajor":true}]},{"id":"FRACTIONAL_PAYMENT","amounts":[{"amount":0,"currency":"MXN","isMajor":true}]},{"id":"TAX","amounts":[{"amount":293.45,"currency":"MXN","isMajor":true}]},{"id":"YEARLY_TOTAL","amounts":[{"amount":2127.51,"currency":"MXN","isMajor":true}]},{"id":"WITHOUT_DISCOUNT","amounts":[{"amount":590.98,"currency":"MXN","isMajor":true}]},{"id":"FIRST_PAYMENT","amounts":[{"amount":531.88,"currency":"MXN","isMajor":true}]}],"numberSubsequentPayments":3}],"protectionLevel":4.5}}', mHeadersMock);
            iaso.GBL_Mock.setMock(objMockCallOut);
            Test.startTest();
            	final Opportunity oppLstVal = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBNAME];
	            final List<String> addCober = new List<String>{'ELECTRONIC_EQUIPMENT','APPLIANCE_EQUIPMENT'};
                final Map<String, Object> dataMapVal = new Map <String, Object>();
                dataMapVal.put('ramo', 'TERR');
                dataMapVal.put('isChecked', true);
                dataMapVal.put('categorie', 'INVENTORY_AND_FURNITURE');                                            
                final Map<String, Object> mDataRsp = MX_SB_VTS_AddCoberturas_Ctrl.addCoverages(String.valueOf(oppLstVal.Id), addCober, dataMapVal);
            Test.stopTest();
            System.assert(!mDataRsp.isEmpty(), sNoEmpty);
        }
    }
}