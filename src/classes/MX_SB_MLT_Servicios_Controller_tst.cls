@isTest
/**
 * @File Name          : MX_SB_MLT_Servicios_Controller_tst
 * @Description        : Clase test de ejecución de Servicios
 * @Author             : Marco Antonio Cruz Barboza
 * @Last Modified On   : 2/17/2020, 11:28:57 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/17/2020   Marco Antonio Cruz           Initial Version
 * 1.1    2/17/2020   Marco Cruz                  Corrige Code Smells 
 * 1.2    11/03/2020  Angel Nava                  migración contract
**/
public class MX_SB_MLT_Servicios_Controller_tst {
@testSetup
        static void creaDatos() {  
            final String nameProfileSC = [SELECT Name from profile where name in ('Administrador del sistema','System Administrator') limit 1].Name;
            final User objUsrTstSC = MX_WB_TestData_cls.crearUsuario('PruebaAdminTst', nameProfileSC);
            objUsrTstSC.MX_SB_MLT_ClaimCenterId__c = '22337347373737';
            insert objUsrTstSC;
            System.runAs(objUsrTstSC) {
                final String  tiporegistroSC = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
                final Account ctatestSC = new Account(recordtypeid=tiporegistroSC,firstname='DANIEL',lastname='PEREZ',Apellido_materno__pc='LOPEZ',RFC__c='PELD920912',PersonEmail='test@gmail.com');
                insert ctatestSC;

                final Account ctatestSCluffy = new Account(Name='luffy',Tipo_Persona__c='Física');
                insert ctatestSCluffy;
                
                final Contract contrato = new Contract(accountid=ctatestSCluffy.Id, MX_SB_SAC_NumeroPoliza__c='PolizaTest',MX_SB_SAC_RFCAsegurado__c =ctatestSC.rfc__c,MX_SB_SAC_NombreClienteAseguradoText__c =ctatestSC.firstname,MX_WB_apellidoPaternoAsegurado__c=ctatestSC.lastname,MX_WB_apellidoMaternoAsegurado__c=ctatestSC.Apellido_materno__pc);
                insert contrato;
                
                final Siniestro__c sini2= new Siniestro__c();
                sini2.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoAuto).getRecordTypeId();
                sini2.MX_SB_SAC_Contrato__c =contrato.Id;
                sini2.MX_SB_MLT_TipoRobo__c = Label.MX_SB_MLT_RoboParcial;
                sini2.TipoSiniestro__c = 'Robo';
                sini2.MX_SB_MLT_ServicioAjAuto__c = false;
                sini2.MX_SB_MLT_RequiereAsesor__c = false;
                insert sini2;
                
                Final WorkOrder workOrderTest = new WorkOrder ();
                workOrderTest.MX_SB_MLT_TipoServicioAsignado__c = Label.MX_SB_MLT_AsesorRobo;
                workOrderTest.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Servicios).getRecordTypeId();
                workOrderTest.MX_SB_MLT_Ajustador__c = objUsrTstSC.Id;
                workOrderTest.MX_SB_MLT_EstatusServicio__c = 'Abierto';
                workOrderTest.MX_SB_MLT_Siniestro__c = sini2.Id;
                workOrderTest.MX_SB_MLT_ServicioAsignado__c = Label.MX_SB_MLT_Ajustador;
                insert workOrderTest;  
            }
    }
    
    @isTest
    static void crearServicioTest () {
        Final String labelRobo = Label.MX_SB_MLT_RoboParcial;
        Final String sinId = [SELECT Id, RecordTypeId FROM Siniestro__c WHERE TipoSiniestro__c = 'Robo' AND MX_SB_MLT_TipoRobo__c =:labelRobo LIMIT 1].Id;
        Final String proveedorId = [SELECT Id, RecordTypeId FROM Account WHERE Name ='luffy'].Id;
        test.startTest();
        Final String strServicio = MX_SB_MLT_Servicios_Cls_Controller.crearServicio (sinId,proveedorId);
        test.stopTest();
        System.assertEquals(strServicio ,'Exito test no Create','Exito en la ejecución');
    }
    
    
    
    @isTest
    static void crearServicioRoboTest () {
        Final String tipoRobo = Label.MX_SB_MLT_RoboParcial;
        Final Id userId = [SELECT ID, Name FROM User WHERE Name = 'PruebaAdminTst'].Id;
        Final Siniestro__c siniServicio = [SELECT Id,MX_SB_MLT_TipoRobo__c, RecordTypeId FROM Siniestro__c WHERE TipoSiniestro__c = 'Robo' AND MX_SB_MLT_TipoRobo__c =:tipoRobo LIMIT 1];
        test.startTest();
        Final String asesorTest = MX_SB_MLT_Servicios_Cls_Controller.crearServicioRobo(siniServicio.Id, userId);
        test.stopTest();
        System.assertEquals(asesorTest ,'Reporte registrado exitosamente','Exito en la ejecución');
        
    } 
    
    @isTest static void servicioAsesortest() {
        final account ctatestMth = [select id ,firstname,Name from account where firstname='DANIEL' and lastname='PEREZ' limit 1];
        Final Siniestro__c sinIdtest = [SELECT Id, MX_SB_MLT_ServicioAjAuto__c,RecordType.DeveloperName,RecordTypeId FROM Siniestro__c WHERE TipoSiniestro__c = 'Robo' LIMIT 1];
        
        
        final Map<String,Account> mapCuentas = new Map<String,Account>();
        mapCuentas.put(ctatestMth.id,ctatestMth);
         final List<WorkOrder> newServices = new List<WorkOrder>();
            final List<WorkOrder> updateServices = new List<WorkOrder>();
            final List<WorkOrder> siniServices = [Select id,MX_SB_MLT_ServicioAsignado__c,MX_SB_MLT_TipoServicioAsignado__c,MX_SB_MLT_ProveedorRobo__c  from workorder where MX_SB_MLT_EstatusServicio__c = 'Abierto'];
        test.startTest();
        sinIdtest.MX_SB_MLT_RequiereAsesor__c=true;
        update sinIdtest;

        MX_SB_MLT_Servicios_Cls_Controller.servicioAsesor(newServices,updateServices,siniServices,sinIdtest,mapCuentas,ctatestMth.id,sinIdtest.id);
        sinIdtest.MX_SB_MLT_ServicioAjAuto__c=true;
        update sinIdtest;
        MX_SB_MLT_Servicios_Cls_Controller.servicioAsesor(newServices,updateServices,siniServices,sinIdtest,mapCuentas,ctatestMth.id,sinIdtest.id);
        System.assertEquals(ctatestMth.firstname,'DANIEL','exito');
        test.stopTest();
    } 
}