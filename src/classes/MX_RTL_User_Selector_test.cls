/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase test de User - selector

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     25/05/2020      Roberto Isaac Soto Granados           Creación Clase
*****************************************************************************************/
@isTest
private class MX_RTL_User_Selector_test {

    /*Usuario de pruebas*/
    private static User testUser = new User();
    /** namerole variable for records */
    final static String NAMEROLE = '%BPYP BANQUERO BANCA PERISUR%';
    /** nameprofile variable for records */
    final static String NAMEPROFILE = 'BPyP Estandar';
    /** namesucursal variable for records */
    final static String NAMESUCURSAL = '6343 PEDREGAL';
    /** namedivision variable for records */
    final static String NAMEDIVISION = 'METROPOLITANA';
    /** name variable for records */
    final static String NAME = 'testUser';
    /** error message */
    final static String MESSAGEFAIL = 'Fail method';

    /**
    * @description: funcion constructor privado
    */
    private MX_RTL_User_Selector_test() { }
    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testUser = UtilitysDataTest_tst.crearUsuario(NAME, NAMEPROFILE, NAMEROLE);
        testUser.Title = 'Privado';
        insert testUser;
    }

    /*Ejecuta la acción para cubrir clase*/
    static testMethod void directorIdsTest() {
        List<User> aclaraciones = new List<User>();
        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        System.runAs(testUser) {
            Test.startTest();
            aclaraciones = MX_RTL_User_Selector.directorIds(new Set<Id>{testUser.Id});
            Test.stopTest();
        }
        System.assert(aclaraciones.isEmpty(), 'Sin directores');
    }

    /**
* @description
* @author Gabriel Garcia | 01/06/2020
* @return void
**/
    @isTest
    private static void getListUserByIdTest() {
        final User usuarioId = [SELECT Id FROM User LIMIT 1];
        final List<User> listUsers = MX_RTL_User_Selector.getListUserById(new Set<Id>{usuarioId.Id});
        System.assertEquals(listUsers.size(), 1, MESSAGEFAIL);
    }

    /**
* @description
* @author Gabriel Garcia | 01/06/2020
* @return void
**/
    @isTest
    private static void getUserByDivisionSucursal() {
        final User usuario = [SELECT Id FROM User WHERE LastName =: NAME  LIMIT 1];
        usuario.BPyP_ls_NombreSucursal__c = NAMESUCURSAL;
        usuario.Divisi_n__c = NAMEDIVISION;
        update usuario;

        final List<User> listUsers = MX_RTL_User_Selector.getUserByDivisionSucursal(new Set<String>{NAMEDIVISION},new Set<String>{NAMESUCURSAL});
        System.assertNotEquals(listUsers.size(), 0, MESSAGEFAIL);
    }

    /**
* @description
* @author Gabriel Garcia | 24/06/2020
* @return void
**/
    @isTest
    private static void fetchListUserByDataBaseTest() {
        final List<String> listParams = new List<String>{'', '', '', ''};
            final List<User> listUsers = MX_RTL_User_Selector.fetchListUserByDataBase(' Id ', '', listParams);
        System.assertNotEquals(listUsers.size(), 0, MESSAGEFAIL);
    }

    /**
* @description
* @author Gabriel Garcia | 25/06/2020
* @return void
**/
    @isTest
    private static void resultQueryUserTest() {
        final List<User> listUsers1 = MX_RTL_User_Selector.resultQueryUser('Id', '', false, null);
        System.assertNotEquals(listUsers1.size(), 0, MESSAGEFAIL);
    }

    /**
* @description
* @author Gabriel Garcia | 25/06/2020
* @return void
**/
    @isTest
    private static void resultQueryUserWhereTest() {
        final List<User> listUser2 = new List<User>([SELECT Id FROM User Limit 10]);
        final Set<Id> setId = new Set<Id>{listUser2[0].Id};
            final List<User> listUsers2 = MX_RTL_User_Selector.resultQueryUser('Id', ' Id IN: setId', true, setId);
        System.assertNotEquals(listUsers2.size(), 0, MESSAGEFAIL);
    }
    /**
* @description
* @author Jose Angel Guerrero | 27/07/2020
* @return void
**/
    @isTest
    public static void getUsersByHintYProfile() {
        final list <user> nuevousuario = MX_RTL_User_Selector.getUsersByHintYProfile('nombre','profile');
        final list <User> nueva = new list <User>();
        test.startTest();
        System.assertEquals(nuevousuario, nueva, 'SUCESS');
        test.stopTest();
    }
    /**
* @description
* @author Jose Angel Guerrero | 27/07/2020
* @return void
**/
    @isTest
    public static void getUsersByProfile() {
        final list <user> nuevousuario = MX_RTL_User_Selector.getUsersByProfile('profile');
        final list <User> nueva = new list <User>();
        test.startTest();
        System.assertEquals(nuevousuario, nueva, 'SUCESS');
        test.stopTest();
    }
}