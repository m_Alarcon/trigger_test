/**
 * @File Name          : MX_RTL_Case_Service.cls
 * @Description        :
 * @Author             : Jaime Terrats
 * @Group              :
 * @Last Modified By   : Jaime Terrats
 * @Last Modified On   : 5/26/2020, 11:40:34 AM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/26/2020   Jaime Terrats     Initial Version
**/
global without sharing class MX_RTL_Case_Service { // NOSONAR

    /**
    * @description
    * @author Jaime Terrats | 5/26/2020
    * @param caseFields
    * @param Ids
    * @param enforceSecurity
    * @return List<Case>
    **/
    public static List<Case> getCaseData(String caseFields, Set<Id> ids, String enforceSecurity) {
        return MX_RTL_Case_Selector.getCaseDataById(caseFields, ids, enforceSecurity);
    }
}