/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_WrapperClass_Basic
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
 * @version                Author                      date						description
	1.0					Julio Medellín                 07/04/2020                 Creación de la clase.*/

@SuppressWarnings('unused')public class MX_SB_PS_WrapperClass_Basic {
	/*variable dummy molulo1*/
    @AuraEnabled public string modulo1 {get;set;}
    /*variable dummy molulo1*/
    @AuraEnabled public string modulo2 {get;set;}
    /*variable dummy molulo1*/
	@AuraEnabled public string modulo3 {get;set;} 
    /*variable dummy molulo1*/   
    @AuraEnabled public string modulo4 {get;set;}	
	/*Es un constructor un constructor puede ir vacio*/
    public MX_SB_PS_WrapperClass_Basic() {}   //NOSONAR 
}