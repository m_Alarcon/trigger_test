/**
* Name: MX_SB_PC_AsignacionCola_ctr
* @author Jose angel Guerrero
* Description : Controlador para componente Lightning de asignación de Colas
*
* Ver              Date            Author                   Description
* @version 1.0     Jul/13/2020     Jose angel Guerrero      Initial Version
*
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public without Sharing class MX_SB_PC_AsignacionCola_ctr {
    /**
* @description: funcion para guardar Asignaciones
*/
    private Static boolean exito = TRUE;
    /**
* @description: funcion para guardar Asignaciones
*/
    private Static List <GroupMember> asignaciones ;
    /**
* @description: funcion constructor privado
*/
    private MX_SB_PC_AsignacionCola_ctr() { }
    /**
* @description: funcion buscar usuarios
*/
    @AuraEnabled
    public static List < User > buscarUsuarios(String nombreABuscar) {
        final String nombre = nombreABuscar + '%';
        final List < User > listaARegresar = new List < User > ();
        final List < User > listaDeConsulta = MX_RTL_User_service.serviceBucarUsuario(nombre);
        listaARegresar.addAll(listaDeConsulta );
        return listaARegresar;
    }
    /**
* @description: funcion buscar usuarios por role
*/
    @AuraEnabled
    public static List < User > buscarUsuariosRole(String colaID) {
        final boolean sinAsignaciones = colaID == 'noAsignada';
        final List < User > listaARegresar = new List < User > ();
        final List < User > listaDeConsulta = MX_RTL_User_service.serviceBucarUsuRol('%Multiasistencia');
        if(sinAsignaciones) {
            final List<string> ids = new List<string>();
            for(User usuario : listaDeConsulta) {
                ids.add(usuario.id);
            }
            asignaciones = MX_SB_PC_AsignacionCola_ctr.buscarColas(ids);
        } else {
            asignaciones= MX_SB_PC_AsignacionCola_ctr.buscarMiembroCola(colaID);  
        }
        for(User usuario : listaDeConsulta) {
            boolean match = false;
            for(GroupMember miembro : asignaciones) {
                if(usuario.id == miembro.userorgroupid) {
                    match = true;
                    break;
                }
            }
            if(match) {
                if(!sinAsignaciones) {
                    listaARegresar.add(usuario);
                }
            } else {
                if(sinAsignaciones) {
                    listaARegresar.add(usuario);
                }
            }
        }
        return listaARegresar;
    }
    /**
* @description: funcion que trae lista de id
*/
    @AuraEnabled
    public static List < Group > grupoColas() {
        final List < Group > grupoARegresar = New List < Group > ();
        final MX_SB_PC_Asignacion_Colas__c CustSett = MX_SB_PC_Asignacion_Colas__c.getInstance();
        final String prefijo = CustSett.MX_SB_PC_Prefijo__c +'%';
        final List < Group > listaConsulta = MX_RTL_Group_service.serviceGrupoColas(prefijo);
        grupoARegresar.addAll(listaConsulta);
        return grupoARegresar;
    }
    /**
* @description: funcion para buscar colas
*/
    @AuraEnabled
    public static List < GroupMember > buscarColas(list <String > listaDeId) {
        final List < GroupMember > grupoARegresar = New List < GroupMember > ();
        final MX_SB_PC_Asignacion_Colas__c CustSett = MX_SB_PC_Asignacion_Colas__c.getInstance();
        final String prefijo = CustSett.MX_SB_PC_Prefijo__c +'%';
        final List < GroupMember > listaConsulta = MX_RTL_GroupMember_service.serviceBuscarLasColas(listaDeId,prefijo);
        grupoARegresar.addAll(listaConsulta);
        return grupoARegresar;
    }
    /**
* @description: funcion para los miembros a buscar 
*/
    @AuraEnabled
    public static List < GroupMember > buscarMiembroCola(String colaBuscar) {
        final List < GroupMember > colaARegresar = New List < GroupMember > ();					
        final List < GroupMember > listaConsulta = MX_RTL_GroupMember_service.serviceBuscarCola(colaBuscar);
        colaARegresar.addAll(listaConsulta);
        return colaARegresar;
    }
    /**
* @description: funcion para guardar colas y eliminarlas
*/
    @AuraEnabled
    public static boolean guardarAsignaciones(String idUsuario,String idCola, boolean seAsigna) {
        GroupMember nuevoMiembro;
        try {
            if (seAsigna) {
                nuevoMiembro = new GroupMember(
                    UserOrGroupId = idUsuario,
                    GroupId = idCola
                );
                final List <GroupMember> listaUpsert = new  List <GroupMember>();
                listaUpsert.add(nuevoMiembro);
                MX_RTL_GroupMember_service.upsertAsignaciones( listaUpsert);
            } else {
                final List < GroupMember > listaConsulta = MX_RTL_GroupMember_service.serviceGroupMember(idUsuario, idCola);
                MX_RTL_GroupMember_service.deleteAsignaciones(listaConsulta);
            }
        } catch (Exception e) {
            exito = false;
        }
        return exito;
    }
    /**
* @description: funcion guardar asignaciones
*/
    @AuraEnabled
    public static boolean guardarNuevasAsignaciones(List <String> userIds, String origen,String destino) {
        final boolean onDelete = destino =='noAsignada';
        final List < GroupMember > nuevasAsig = new List < GroupMember > ();
        final List < GroupMember > listaConsulta = MX_RTL_GroupMember_service.serviceGuardarAsig(userIds,origen,destino);
        try {
            MX_RTL_GroupMember_service.deleteAsignaciones(listaConsulta);
            for (String idDeUsuario:userIds) {
                final GroupMember nuevoMiembro = new GroupMember(
                    UserOrGroupId = idDeUsuario,
                    GroupId = destino);
                nuevasAsig.add(nuevoMiembro);
            }
            if(!onDelete) {
                MX_RTL_GroupMember_service.upsertAsignaciones(nuevasAsig);
            }
        } catch (Exception e) {
            exito = false;
        }
        return exito;
    }
}