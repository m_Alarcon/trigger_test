/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 10-12-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   10-12-2020   Eduardo Hernández Cuamatzi   Initial Version
**/
@SuppressWarnings('sf:UseSingleton, sf:DMLWithoutSharingEnabled')
public class MX_RTL_IVRCodes_Selector {

    /**
    * @description Recuperar errores IVR
    * @author Eduardo Hernández Cuamatzi | 10-20-2020 
    * @param query Campos a consultar
    * @param typeVals tipo de registros a filtrar
    * @param ivrCode codigo especifico de IVR
    * @return List<MX_RTL_IVRCodes__mdt> 
    **/
    public static List<MX_RTL_IVRCodes__mdt> findByTypeVal(String query, Set<String> typeVals, String ivrCode) {
        final String fullQuery = 'Select '+query+' FROM MX_RTL_IVRCodes__mdt WHERE MX_RTL_Group__c IN: typeVals AND MX_RTL_isActive__c = True AND MX_RTL_IVRCode__c =: ivrCode';
        return Database.query(String.escapeSingleQuotes(fullQuery));
    }
}