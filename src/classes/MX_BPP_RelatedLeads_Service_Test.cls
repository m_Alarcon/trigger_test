/**
 * @File Name          : MX_BPP_RelatedLeads_Service_Test.cls
 * @Description        : Clase test para la clase Service de LWC MX_BPP_RelatedLeads
 * @author             : Hugo Ivan Carrillo Béjar
 * @Group              : BPyP
 * @Last Modified By   : Hugo Ivan Carrillo Béjar
 * @Last Modified On   : 21/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      16/06/2020           Hugo Ivan Carrillo Béjar   Initial Version
**/
@isTest
public class MX_BPP_RelatedLeads_Service_Test {


    @TestSetup
    static void testData() {

        final Campaign testCamp = new Campaign(Name = 'Campaign Test');
        insert testCamp;

        final Account testAcc = new Account(Name='Test Service Account',No_de_cliente__c = '8316251');
        insert testAcc;

        final Lead testLead = new Lead(LastName='Lead Service Test', RecordTypeId=Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('MX_BPP_Leads').getRecordTypeId(),
                MX_ParticipantLoad_id__c = testAcc.No_de_cliente__c, MX_LeadEndDate__c=Date.today().addDays(13), MX_WB_RCuenta__c = testAcc.Id, MX_LeadAmount__c=250, LeadSource='Preaprobados');
        insert testLead;

        final CampaignMember CampMem = new CampaignMember(CampaignId = testCamp.Id, Status='Sent', LeadId = testLead.Id, MX_LeadEndDate__c=Date.today().addDays(13));
        insert CampMem;
    }

    @isTest
    private static void testConstructor() {
        final MX_BPP_RelatedLeads_Service instanceC = new MX_BPP_RelatedLeads_Service();
        System.assertNotEquals(instanceC, null, 'No Existe coincidencia');
    }


    @isTest
    static void testSerConvertLeads() {
        final List<Id> leadsId = new List<Id>{[SELECT Id FROM Lead LIMIT 1].Id};
        Test.startTest();
        final Map<String,String> result = MX_BPP_RelatedLeads_Service.serConvertLeads(leadsId[0]);
        Test.stopTest();
        System.assert(result.containsKey(leadsId[0]), 'Lead not converted');
    }

    @isTest
    static void testSerRelatedLeads() {
        final List<Id> tAccount = new List<Id>{[SELECT Id FROM Account LIMIT 1].Id};
        Test.startTest();
        final List<CampaignMember> result = MX_BPP_RelatedLeads_Service.serRelatedLeads(tAccount[0]);
        Test.stopTest();
        System.assertEquals(1, result.size(), 'Account not converted');
    }

    @isTest
    static void testSerButtonMenu() {
        final User usuarioAdmin = UtilitysDataTest_tst.crearUsuario('PruebaAdm', Label.MX_PERFIL_SystemAdministrator, 'BBVA ADMINISTRADOR');
        insert usuarioAdmin;
        final User tstUsr = [SELECT Id FROM User Where Name = 'PruebaAdm'];
        Test.startTest();
        final Boolean result = MX_BPP_RelatedLeads_Service.serButtonMenu(tstUsr.Id);
        Test.stopTest();
        System.assert(result, 'El perfil no coincide');
    }
}