/**
 * @File Name          : MX_SB_VTS_SendTrackingTelemarketing_sch.cls
 * @Description        :
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              :
 * @Last Modified By   : Diego Olvera
 * @Last Modified On   : 20/2/2020 13:56:20
 * @Modification Log   :
 * Ver       Date            Author                  Modification
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 20/2/2020 9:51:19
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    27/9/2019   Eduardo Hernández Cuamatzi     Initial Version
 **/
public without sharing class MX_SB_VTS_SendTrackingTelemarketing_sch implements Schedulable {
	/** variable sQuery */
	public String sQuery {get;set;}

    /**
     * execute scheduld execute 
     * @param  schContext contexto scheduld
     */
    public void execute(SchedulableContext schContext) { 
        final MX_SB_VTS_SendTrackingTelemarketing_cls oppACls = new MX_SB_VTS_SendTrackingTelemarketing_cls(sQuery, Integer.valueOf(System.Label.CotizacionCreada));
        Database.executeBatch(oppACls, 100);
    }
}