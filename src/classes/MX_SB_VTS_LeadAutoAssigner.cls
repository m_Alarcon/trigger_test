/*
*
* @author Jaime Terrats
* @description API to auto assign leads from Smart Center to call center agents
*
*           No  |     Date     |     Author      |    Description
* @version  1.0    06/24/2019     Jaime Terrats     Create API for Smart Center
* @version  1.1    06/25/2019     Jaime Terrats     Change logic to work with Opportunities or Leads
* @version  1.2    06/26/2019     Jaime Terrats     Add method to create tasks when api is invoked
* @version  1.2.1  06/28/2019     Jaime Terrats     Remove code smells
* @version  1.3    06/28/2019     julio Medellin    Change Stage On  Opporunity
* @version  1.4    09/23/2019     Jaime Terrats     Add json parse to read values from json object
* @version  1.5    10/03/2019     Jaime Terrats     Clear values on fields
* @version  1.6	   14/05/2020	  Alexandro Corzo	Se agrego la relación de Traea con el objeto de Visita y se realizo el mapeo de campos
*													para el rellenado de información.
*
*/
@RestResource(urlMapping='/leadAutoAssigner/*')
global without sharing class MX_SB_VTS_LeadAutoAssigner { // NOSONAR
    /**Valor  Contacto*/
    public final static String CONTACTO = 'Contacto';
    /**Valor  Contacto Efectivo*/
    public final static String CONTACTOEFECTIVO = 'Contacto Efectivo';
    /**Valor  Interesado*/
    public final static String INTERESADO = 'Interesado';
    
    /** */
    public static Integer checkNullVals (Decimal value) {
        Integer finalVal = 0;
        if(String.isNotBlank(String.valueOf(value))) {
            finalVal = Integer.valueOf(value);
        }
        return finalVal;
    }
   /**
    * @description Recupera banderas para Visitas
    * @author Eduardo Hernandez Cuamatzi | 07-14-2020 
    * @param Lead leadRec Registro a evaluar
    * @return Map<String, Boolean> Mapa de valores Booleanos
    **/
    public static Map<String, Boolean> pendEfectivo(Lead leadRec) {
        boolean penContactEfect = true;
        boolean isInterest = false;
        boolean pendInterest = true;
        boolean aceptContra = false;
        boolean aceptaCotiz = false;
        boolean noAceptCotiz = false;
        boolean isContact = false;

        if(leadRec.Resultadollamada__c.equalsIgnoreCase(CONTACTO)) {
            isContact = true;
        }
        if(leadRec.MX_SB_VTS_Tipificacion_LV2__c.equalsIgnoreCase(CONTACTOEFECTIVO)) {
            penContactEfect = false;
        }
        if(leadRec.MX_SB_VTS_Tipificacion_LV3__c.equalsIgnoreCase(INTERESADO)) {
            isInterest = true;
            pendInterest = false;
        }
        switch on leadRec.MX_SB_VTS_Tipificacion_LV4__c {
            when  'Acepta contratación' {
                aceptContra = true;
            }
            when 'Acepta cotización' { 
                aceptaCotiz = true;
            }
            when 'No acepta cotización' { 
                noAceptCotiz = true;
            }
        }
        final Map<String, Boolean> lstBoolean = new Map<String, Boolean>();
        lstBoolean.put('penContactEfect',penContactEfect);
        lstBoolean.put('isInterest',isInterest);
        lstBoolean.put('pendInterest',pendInterest);
        lstBoolean.put('aceptContra',aceptContra);
        lstBoolean.put('aceptaCotiz',aceptaCotiz);
        lstBoolean.put('noAceptCotiz',noAceptCotiz);
        lstBoolean.put('isContact',isContact);

        return lstBoolean;
    }

    /**
    * @description Recupera banderas para Visitas
    * @author Eduardo Hernandez Cuamatzi | 07-14-2020 
    * @param Opportunity oppRec Registro a evaluar
    * @return Map<String, Boolean> Mapa de valores Booleanos
    **/
    public static Map<String, Boolean> pendEfectivo(Opportunity oppRec) {
        boolean penContEfect = true;
        boolean isInters = false;
        boolean pendInters = true;
        boolean aceptCont = false;
        boolean aceptaCot = false;
        boolean noAceptCot = false;
        boolean isCont = false;

        if(oppRec.MX_SB_VTS_Tipificacion_LV1__c.equalsIgnoreCase(CONTACTO)) {
            isCont = true;
        }
        if(oppRec.MX_SB_VTS_Tipificacion_LV2__c.equalsIgnoreCase(CONTACTOEFECTIVO)) {
            penContEfect = false;
        }
        if(oppRec.MX_SB_VTS_Tipificacion_LV3__C.equalsIgnoreCase(INTERESADO)) {
            isInters = true;
            pendInters = false;
        }
        switch on oppRec.MX_SB_VTS_Tipificacion_LV4__c {
            when  'Acepta contratación' {
                aceptCont = true;
            }
            when 'Acepta cotización' { 
                aceptaCot = true;
            }
            when 'No acepta cotización' { 
                noAceptCot = true;
            }
        }
        final Map<String, Boolean> lstBoolean = new Map<String, Boolean>();
        lstBoolean.put('penContactEfect',penContEfect);
        lstBoolean.put('isInterest',isInters);
        lstBoolean.put('pendInterest',pendInters);
        lstBoolean.put('aceptContra',aceptCont);
        lstBoolean.put('aceptaCotiz',aceptaCot);
        lstBoolean.put('noAceptCotiz',noAceptCot);
        lstBoolean.put('isContact',isCont);
        return lstBoolean;
    }
}