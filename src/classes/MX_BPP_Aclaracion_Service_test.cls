/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase test de aclaraciones - trigger de Aclaraciones y MX_BPP_Aclaracion_Service

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     20/05/2020      Roberto Isaac Soto Granados           Creación Clase
*****************************************************************************************/
@isTest
private class MX_BPP_Aclaracion_Service_test {
    /*Usuario de pruebas*/
    private static User testUserA = new User();
    /*Caso de pruebas*/
    private static Case testCaseA = new Case();
    /*Aclaración de pruebas*/
    private static BPyP_Aclaraciones__c testAclaracion = new BPyP_Aclaraciones__c();
    /*cuenta de pruebas*/
    private static Account testAccs = new Account();
    /*Visita de pruebas*/
    private static dwp_kitv__Visit__c testVisit = new dwp_kitv__Visit__c();
    /*String para folios*/
    private static String testCodes = '1234';
    /*Tema a tratar de pruebas*/
    private static dwp_kitv__Visit_Topic__c testTopic = new dwp_kitv__Visit_Topic__c();

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testUserA = UtilitysDataTest_tst.crearUsuario('testUser', 'BPyP Estandar', 'BPYP BANQUERO BANCA PERISUR');
        testUserA.Title = 'Privado';
        insert testUserA;
        System.runAs(testUserA) {
            testAccs.LastName = 'testAcc';
            testAccs.FirstName = 'testAcc';
            testAccs.OwnerId = testUserA.Id;
            testAccs.No_de_cliente__c = testCodes;
            insert testAccs;
            testCaseA.OwnerId = testUserA.Id;
            testCaseA.Status = 'Nuevo';
            testCaseA.MX_SB_SAC_Folio__c = testCodes;
            testCaseA.Description = 'Description';
            testCaseA.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_EU_Case_Apoyo_General'].Id;
            insert testCaseA;
        }
    }

    /*Ejecuta la acción para cubrir clase*/
    static testMethod void getCasesTest() {
        testCaseA = [SELECT Id, MX_SB_SAC_Folio__c FROM Case WHERE MX_SB_SAC_Folio__c = '1234' LIMIT 1];
        testUserA = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        System.runAs(testUserA) {
            Test.startTest();
                testAclaracion.BPyP_Producto__c = 'TDC';
            	testAclaracion.BPyP_Tarjeta__c = '9876';
            	testAclaracion.BPyP_Folio__c = testCodes;
            	testAclaracion.BPyP_NumeroCliente__c = testCodes;
            	testAclaracion.BPyP_Caso__c = testCaseA.Id;
            	insert testAclaracion;
            Test.stopTest();
        }
        System.assert(!String.isBlank(testAclaracion.Id), 'Aclaración creada correctamente');
    }

    /*Llamada a selector*/
    static testMethod void serviceDirectorIdsTest() {
        testUserA = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        List<User> dirIds = new List<User>();
        System.runAs(testUserA) {
            Test.startTest();
            	dirIds = MX_BPP_Aclaracion_Service.serviceDirectorIds(new Set<Id>{testUserA.Id});
            Test.stopTest();
        }
        System.assert(dirIds.isEmpty(), 'Sin aclaraciones de subordinados');
    }
    /*Llamada a selector*/
    static testMethod void serviceCasosEnAclaracionesTest() {
        testUserA = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        List<Case> cases = new List<Case>();
        System.runAs(testUserA) {
            Test.startTest();
            	cases = MX_BPP_Aclaracion_Service.serviceCasosEnAclaraciones(new Set<Id>{testUserA.Id}, new Set<Id>{testUserA.Id});
            Test.stopTest();
        }
        System.assert(!cases.isEmpty(), 'Casos encontrados');
    }
    /*Llamada a selector*/
    static testMethod void serviceCasosParaVisitasTest() {
        testUserA = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        testCaseA = [SELECT Id, MX_SB_SAC_Folio__c FROM Case WHERE MX_SB_SAC_Folio__c = '1234' LIMIT 1];
        List<Case> cases2 = new List<Case>();
        System.runAs(testUserA) {
            Test.startTest();
            	cases2 = MX_BPP_Aclaracion_Service.serviceCasosParaVisitas(new Set<Id>{testCaseA.Id});
            Test.stopTest();
        }
        System.assert(!cases2.isEmpty(), 'Casos localizados');
    }
    /*Llamada a selector*/
    static testMethod void serviceActualizaCasosTest() {
        testCaseA = [SELECT Id, Status FROM Case WHERE MX_SB_SAC_Folio__c = '1234' LIMIT 1];
        testUserA = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        System.runAs(testUserA) {
            Test.startTest();
            	MX_BPP_Aclaracion_Service.serviceActualizaCasos(new List<Case>{testCaseA});
            Test.stopTest();
        }
        System.assert(!'En proceso'.Equals(testCaseA.Status), 'Actualización correcta');
    }
    /*Llamada a selector*/
    static testMethod void serviceRecordTypeLlamadaBPyPTest() {
        Id rtId;
        testUserA = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        System.runAs(testUserA) {
            Test.startTest();
            	rtId = MX_BPP_Aclaracion_Service.serviceRecordTypeLlamadaBPyP();
            Test.stopTest();
        }
        System.assert(!String.isBlank(rtId), 'Id conseguido');
    }
    /*Llamada a selector*/
    static testMethod void serviceCreaVisitaTest() {
        Id visitId;
        testUserA = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        System.runAs(testUserA) {
            Test.startTest();
                testVisit.dwp_kitv__visit_start_date__c = Date.today()+4;
                testVisit.dwp_kitv__visit_duration_number__c = '15';
                testVisit.dwp_kitv__visit_status_type__c = '04';
            	visitId = MX_BPP_Aclaracion_Service.serviceCreaVisita(new List<dwp_kitv__Visit__c>{testVisit});
            	testTopic.dwp_kitv__visit_id__c = testVisit.Id;
            	testTopic.dwp_kitv__topic_desc__c = 'Ok';
            	MX_BPP_Aclaracion_Service.serviceCreaTemas(new List<dwp_kitv__Visit_Topic__c>{testTopic});
            Test.stopTest();
        }
        System.assert(!String.isBlank(visitId), 'Visita insertada');
    }
}