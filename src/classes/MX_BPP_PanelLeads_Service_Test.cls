/**
* @File Name          : MX_BPP_PanelLeads_Service_Test.cls
* @Description        : Test class for MX_BPP_PanelLeads_Service
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 08/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      08/06/2020            Gabriel Garcia Rojas          Initial Version
**/
@isTest
private class MX_BPP_PanelLeads_Service_Test {
	 /** name variables for records */
    final static String NAME = 'New Campaing Member Test';
    /** name fileds for records */
    final static String FIELDS = 'Id, Name';
    /** error message */
    final static String MESSAGEFAIL = 'Fail method';
    /**
     * @description setup data
     * @author Gabriel Garcia | 08/06/2020
     * @return void
     **/
    @TestSetup
    private static void testSetUpData() {
        final Lead candidato = new Lead(LastName = NAME);
        insert candidato;

        final Campaign campanya = new Campaign(Name = NAME);
        insert campanya;

        final CampaignMember campMem = new CampaignMember(CampaignId = campanya.Id, Status='Sent', LeadId = candidato.Id);
        insert campMem;
    }

    /**
     * @description constructor test
     * @author Gabriel Garcia | 08/06/2020
     * @return void
     **/
    @isTest
    private static void testConstructor() {
        final MX_BPP_PanelLeads_Service instanceC = new MX_BPP_PanelLeads_Service();
        System.assertNotEquals(instanceC, null, MESSAGEFAIL);
    }

    /**
     * @description
     * @author Gabriel Garcia | 08/06/2020
     * @return void
     **/
    @isTest
    private static void convertLeadToOppFailTest() {
        final Lead candidato = [SELECT Id FROM Lead LIMIT 1];
        final Map<String,Object> oppConverted = MX_BPP_PanelLeads_Service.convertLeadToOpp(candidato.Id);
        System.assertEquals(oppConverted.get('Success'), null, MESSAGEFAIL);
    }

    /**
     * @description
     * @author Gabriel Garcia | 08/06/2020
     * @return void
     **/
    @isTest
    private static void getinfoCampignMembersTest() {
        final Lead candidato = [SELECT Id FROM Lead LIMIT 1];
        final CampaignMember CampMemb = MX_BPP_PanelLeads_Service.getinfoCampignMembers(candidato.Id);
        System.assertEquals(CampMemb.LeadId, candidato.Id, MESSAGEFAIL);
    }
}