/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Julio Medellin Oliva
*   @Proyecto:              Bbva
*   @Descripción:           schedule job class 
*
*******************************************************************************/

// Clase schedule de acceso Global para mas información consultar Apex Developer Guide 
global without sharing class MX_SB_VTS_cerradoOportunidades implements Schedulable {//NOSONAR
  
  /**Variable para criterio de busqueda*/
  final static String SETAPA='Closed Lost';
 
 /** Method estándar de un trabajo programado */
  global static void execute(SchedulableContext ctx) {
   closeOpps();
  }

//Methodo estatico de la clase instanciable 
 private static void closeOpps() {
    //Variable para criterio de busqueda
    final String SCRITERIA = '%Closed%';//NOSONAR
   //Variable para criterio de busqueda
    final String SRT='Ventas Asistidas';//NOSONAR
   //Variable para criterio de busqueda
    final String  DAYS = System.Label.MX_SB_VTS_Opportunity_close_days;
     //Variable para construcción de Consulta SOQL
  final String SQUERY='SELECT ID FROM Opportunity WHERE LastModifiedDate <> LAST_N_DAYS:'+DAYS+' AND  (NOT StageName Like :SCRITERIA) AND  MX_SB_VTS_ContadorLlamadas__c =1 AND RecordType.Name =:SRT'; 
    Opportunity[] oppsClose = new Opportunity[]{};
    oppsClose = database.query(String.escapeSingleQuotes(SQUERY));
    for(Opportunity opp : oppsClose) {
      opp.StageName=SETAPA;
    }
    update oppsClose;
 }
  }