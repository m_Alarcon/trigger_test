/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_CatalogoMinutas_TriggerHandler
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-03-08
* @Description 	MX_BPP_CatalogoMinutas Trigger Handler
* @Changes
*
*/
public without sharing class MX_BPP_CatalogoMinutas_TriggerHandler extends TriggerHandler {
    /** List Trigger.new */
	final private List<MX_BPP_CatalogoMinutas__c> triggerNew;
	/** Map Trigger.new */
	final private Map<Id, MX_BPP_CatalogoMinutas__c> triggerNewMap;
	/** Map Trigger.old */
	final private Map<Id, MX_BPP_CatalogoMinutas__c> triggerOldMap;

    /**
    * Constructor
    */
    public MX_BPP_CatalogoMinutas_TriggerHandler() {
		this.triggerNew = (List<MX_BPP_CatalogoMinutas__c>)(Trigger.new);
        this.triggerNewMap= (Map<Id, MX_BPP_CatalogoMinutas__c>)(Trigger.newMap);
        this.triggerOldMap= (Map<Id, MX_BPP_CatalogoMinutas__c>)(Trigger.oldMap);
    }
    
    /**
    * beforeInsert Override de before Insert
    */
    protected override void beforeInsert() {
        MX_BPP_CatalogoMinutas_Service.checkInnerTagsIns(triggerNew);
    }
	
    /**
    * beforeInsert Override de before Insert
    */
	protected override void beforeUpdate() {
		MX_BPP_CatalogoMinutas_Service.checkInnerTagsUpd(triggerNewMap, triggerOldMap);
	}
}