/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 01-14-2021
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   01-14-2021   Eduardo Hernández Cuamatzi   Initial Version
 * 1.1   03-05-2021   Eduardo Hernández Cuamatzi   Se corrige clase test
**/

@isTest
private class MX_SB_VTS_DeleteCoverageHSD_s_test {

    /**@description llave de mapa*/
    private final static string TOKEN = '12345678';
    /**@description llave de ERROR*/
    private final static string TSEC = 'tsec';
    /**@description etapa Cotizacion*/
    private final static string COTIZACION = 'Cotización';
    /**@description Nombre usuario*/
    private final static string COMPLETE = 'Complete';
    /**URL Mock Test*/
    private final static String URLTOTESTASO = 'http://www.example.com';
    /**Particion IASO*/
    private final static String IASOPARTASO = 'local.MXSBVTSCache';
    /**Servicio IASO*/
    private final static String CALCULATEPRICES = 'CalculateQuotePrice';
    /**INCE Trade*/
    private final static String INCE = 'INCE';
    /**GLASS_BREAKAGE coverages*/
    private final static String GLASSBREAKAGE = 'GLASS_BREAKAGE';
    /**CODIGO_CUPON coverages*/
    private final static String CODIGOCUPON = 'CODIGO_CUPON';

    @TestSetup
    static void makeData() {
        MX_SB_VTS_CallCTIs_utility.initHogarGeneric();
        final Opportunity oppIdCrit = [Select Id, Name from Opportunity where StageName =: COTIZACION];
        final Map<Id, Quote> mapQuoteData = new Map<Id, Quote>([Select Id, Name from Quote where OpportunityId =: oppIdCrit.Id]);
        for(Quote quoteItem : mapQuoteData.values()) {
            quoteItem.MX_SB_VTS_ASO_FolioCot__c = '12345';
            oppIdCrit.SyncedQuoteId = quoteItem.Id;
            MX_RTL_Cobertura_Selector_Test.initCoverages(quoteItem.Id);
        }
        MX_SB_VTS_CallCTIs_utility.initLeadMulServHSD();
        update mapQuoteData.values();
        update oppIdCrit;
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'getGTServicesSF', iaso__Url__c = URLTOTESTASO, iaso__Cache_Partition__c = IASOPARTASO);
        insert new iaso__GBL_Rest_Services_Url__c(Name = CALCULATEPRICES, iaso__Url__c = URLTOTESTASO, iaso__Cache_Partition__c = IASOPARTASO);
        final MX_SB_VTS_Generica__c setting = MX_WB_TestData_cls.GeneraGenerica('IASO01', 'IASO01');
        setting.MX_SB_VTS_Type__c = 'IASO01';
        setting.MX_SB_VTS_HEADER__c = URLTOTESTASO;
        insert setting;
    }

    @isTest
    static void findContractingCriteria() {
        final Opportunity oppIdCrit = [Select Id, Name from Opportunity where StageName =: COTIZACION];
        final Map<Id, Quote> mapQuoteData = new Map<Id, Quote>([Select Id, Name from Quote where OpportunityId =: oppIdCrit.Id]);
        Test.startTest();
            final Map<Id, List<Cobertura__c>> mapCovers = MX_SB_VTS_DeleteCoverageHSD_Services.findContractingCriteria(mapQuoteData, 'MX_SB_VTS_ContractingCriteria');
            System.assert(!mapCovers.values()[0].isEmpty(), 'Criterials Recuperados');
        Test.stopTest();
    }

    @isTest
    static void fillRemoveCoverages() {
        final Opportunity oppIdCrit = [Select Id, Name,Producto__c from Opportunity where StageName =: COTIZACION];
        final Quote mapQuoteData = [Select Id, Name from Quote where OpportunityId =: oppIdCrit.Id];
        final List<Cobertura__c> mapCoverages = MX_SB_VTS_DeleteCoverageHSD_Helper.findCoverCodes(mapQuoteData.Id, new List<String>{GLASSBREAKAGE});
        Test.startTest();
            oppIdCrit.Producto__c = System.Label.MX_SB_VTS_PRO_HogarDinamico_LBL;
            final Map<String, Object> bodyDeleteCov = MX_SB_VTS_DeleteCoverageHSD_Services.fillRemoveCoverages(mapCoverages, oppIdCrit, INCE);
            System.assertEquals((String)bodyDeleteCov.get('allianceCode'), 'DHSDT003', 'Body construido');
        Test.stopTest();
    }

    @isTest
    static void processCoverages() {
        final Opportunity oppIdCov = [Select Id, Name,Producto__c from Opportunity where StageName =: COTIZACION];
        final Quote mapQuoteData = [Select Id, Name from Quote where OpportunityId =: oppIdCov.Id];
        final List<Cobertura__c> mapCoverages = MX_SB_VTS_DeleteCoverageHSD_Helper.findCoverCodes(mapQuoteData.Id, new List<String>{GLASSBREAKAGE});
        final String strQuotetions = MX_SB_VTS_ScheduldOppsTele_tst.quotetationStr(CALCULATEPRICES);
        Final Map<String, String> headersMock = new Map<String, String>();
        headersMock.put(TSEC, TOKEN);
        Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, COMPLETE, strQuotetions, headersMock); 
        iaso.GBL_Mock.setMock(mockCallout);
        Test.startTest();
        oppIdCov.Producto__c = System.Label.MX_SB_VTS_PRO_HogarDinamico_LBL;
            final Map<String, Object> bodyDeleteCov = MX_SB_VTS_DeleteCoverageHSD_Services.fillRemoveCoverages(mapCoverages, oppIdCov, INCE);
            final Map<String, Object> processResponse = MX_SB_VTS_DeleteCoverageHSD_Services.processCoverages(JSON.serialize(bodyDeleteCov), '1234551');
            System.assert((Boolean)processResponse.get('responseOk'), 'Petición correcta');
        Test.stopTest();
    }

    @isTest
    static void processResponse() {
        final Opportunity oppIdCrit = [Select Id, Name,Producto__c from Opportunity where StageName =: COTIZACION];
        final Quote mapQuoteData = [Select Id, Name from Quote where OpportunityId =: oppIdCrit.Id];
        final List<Cobertura__c> mapCoverages = MX_SB_VTS_DeleteCoverageHSD_Helper.findCoverCodes(mapQuoteData.Id, new List<String>{GLASSBREAKAGE});
        final String getQuotetions = MX_SB_VTS_ScheduldOppsTele_tst.quotetationStr(CALCULATEPRICES);
        Final Map<String, String> headersMock = new Map<String, String>();
        headersMock.put(TSEC, TOKEN);
        Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, COMPLETE, getQuotetions, headersMock); 
        iaso.GBL_Mock.setMock(mockCallout);
        Test.startTest();
            oppIdCrit.Producto__c = System.Label.MX_SB_VTS_PRO_HogarDinamico_LBL;
            final Map<String, Object> bodyDeleteCov = MX_SB_VTS_DeleteCoverageHSD_Services.fillRemoveCoverages(mapCoverages, oppIdCrit, INCE);
            final Map<String, Object> responServices = MX_SB_VTS_DeleteCoverageHSD_Services.processCoverages(JSON.serialize(bodyDeleteCov), '1234551');
            final Map<String,Object> responseServ = (Map<String, Object>)Json.deserializeUntyped((String)responServices.get('getData'));
            final Map<String, String> mapValues = MX_SB_VTS_DeleteCoverageHSD_Services.processResponse(JSON.serialize(responseServ.get('data')), mapQuoteData.Id);
            System.assertEquals('4537.87', mapValues.get('YEARLY'),'Petición correcta');
        Test.stopTest();
    }

    @isTest
    static void fillEditPets() {
        final Opportunity oppIdCrit = [Select Id, Name,Producto__c from Opportunity where StageName =: COTIZACION];
        final Map<Id, Quote> mapQuoteData = new Map<Id, Quote>([Select Id, Name from Quote where OpportunityId =: oppIdCrit.Id]);
        final Map<Id, List<Cobertura__c>> mapCovers = MX_SB_VTS_DeleteCoverageHSD_Services.findContractingCriteria(mapQuoteData, 'MX_SB_VTS_ContractingCriteria');
        final String getQuotetions = MX_SB_VTS_ScheduldOppsTele_tst.quotetationStr(CALCULATEPRICES);
        Final Map<String, String> headersMock = new Map<String, String>();
        headersMock.put(TSEC, TOKEN);
        Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, COMPLETE, getQuotetions, headersMock); 
        iaso.GBL_Mock.setMock(mockCallout);
        Test.startTest();
            oppIdCrit.Producto__c = System.Label.MX_SB_VTS_PRO_HogarDinamico_LBL;
            final List<String> valuesCriterial = new List<String>{INCE, '002', '240', 'BUILDINGS', 'BUILDING', 'YEARLY'};
            final List<String> critCode = new List<String>{'2008ASMASCOTAS'};
            final Map<String, Object>  mapResponse = MX_SB_VTS_DeleteCoverageHSD_Services.fillEditPets(mapCovers.values()[0], oppIdCrit, valuesCriterial, critCode);
            System.assertEquals((String)mapResponse.get('certifiedNumberRequested'), '1', 'Body Correcto');
        Test.stopTest();
    }

    @isTest
    static void updateCoverage() {
        final Opportunity oppIdCrit = [Select Id, Name,Producto__c from Opportunity where StageName =: COTIZACION];
        final Quote mapQuoteData = [Select Id, Name from Quote where OpportunityId =: oppIdCrit.Id];
        final List<String> critCode = new List<String>{CODIGOCUPON};
        final List<Cobertura__c> mapCoverages = MX_SB_VTS_DeleteCoverageHSD_Helper.findCoverCodes(mapQuoteData.Id, critCode);
        final String getQuotetions = MX_SB_VTS_ScheduldOppsTele_tst.quotetationStr(CALCULATEPRICES);
        Final Map<String, String> headersMock = new Map<String, String>();
        headersMock.put(TSEC, TOKEN);
        Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, COMPLETE, getQuotetions, headersMock); 
        iaso.GBL_Mock.setMock(mockCallout);
        Test.startTest();
            oppIdCrit.Producto__c = System.Label.MX_SB_VTS_PRO_HogarDinamico_LBL;
            final List<String> paramsCov = new List<String>{'', CODIGOCUPON, INCE};
            MX_SB_VTS_DeleteCoverageHSD_Services.updateCoverage(mapCoverages, false, paramsCov);
            System.assert(!mapCoverages.isEmpty(), 'Mapa Coberturas');
        Test.stopTest();
    }

    @isTest
    static void processCupon() {
        final Opportunity oppIdCrit = [Select Id, Name,Producto__c from Opportunity where StageName =: COTIZACION];
        final Quote mapQuoteData = [Select Id, Name from Quote where OpportunityId =: oppIdCrit.Id];
        final List<String> critCode = new List<String>{CODIGOCUPON, 'Test', 'HSDKW112'};
        final List<Cobertura__c> mapCoveragesCodes = MX_SB_VTS_DeleteCoverageHSD_Helper.findCoverCodes(mapQuoteData.Id, critCode);
        final String getQuotetions = MX_SB_VTS_ScheduldOppsTele_tst.quotetationStr(CALCULATEPRICES);
        final Map<String, String> headersMock = new Map<String, String>();
        headersMock.put(TSEC, TOKEN);
        final MX_WB_Mock mockCallout = new MX_WB_Mock(200, COMPLETE, getQuotetions, headersMock); 
        iaso.GBL_Mock.setMock(mockCallout);
        Test.startTest();
            oppIdCrit.Producto__c = System.Label.MX_SB_VTS_PRO_HogarDinamico_LBL;
            MX_SB_VTS_DeleteCoverageHSD_Services.processCupon(getQuotetions, mapCoveragesCodes, critCode);
            System.assert(!mapCoveragesCodes.isEmpty(), 'Cupón actualizado');
        Test.stopTest();
    }
}