/*
*
* @aBBVA Seguros - SAC Team
* @description This class will update fields from contract object
*
*           No  |     Date     |     Author               |    Description
* @version  1.0    21/12/2019     BBVA Seguros - SAC Team     Created
* @version  1.0.1  22/12/2019     Jaime Terrats             Remove code smells
*
*/
@isTest
private class MX_SB_SAC_UpdateContractFieldsBatchTest {
    /** date to run schedule */
    final static String CRONEXPR = '0 0 0 15 3 ? 2022';
    /** number of accounts */
    final static Integer ACC_LENGTH = 10;
    /**
    * @description 
    * @author Miguel Hernandez | 12/22/2019 
    * @return void 
    **/
    @testSetup 
    static void setup() {
        final List<Account> listAccounts = new List<Account>();
        final String rtAcc = Schema.SObjectType.account.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_PersonRecord).getRecordTypeId();
        for(Integer i = 0; i < ACC_LENGTH; i++) {
            listAccounts.add(New Account(
                LastName = 'Test Account' + i,
                PersonEmail = 'test@account'+i+'.com',
                RecordTypeId = rtAcc
            ));
        }
        insert listAccounts;
        final List<Contract> listContract = new List<Contract>();
        for (Integer i=0;i<10;i++) {
            listContract.add(new Contract(AccountId =listAccounts[i].Id));
        }
        insert listContract;
    }
    /**
    * @description 
    * @author Miguel Hernandez | 12/22/2019 
    * @return testmethod 
    **/
    static testmethod void testBatch() { 
        final List<Account> accs = [Select Id from Account where Name like '%Test Account%'];       
        Test.startTest();
        final MX_SB_SAC_UpdateContractFieldsBatch uca = new MX_SB_SAC_UpdateContractFieldsBatch();
        Database.executeBatch(uca);
        Test.stopTest();
        System.assertEquals(10, [Select count() from Contract WHERE AccountId in:(accs)], 'Hacen match?');
    }
    /**
    * @description 
    * @author Miguel Hernandez | 12/22/2019 
    * @return void 
    **/
    @isTest 
    static void testScheduler() {
        Test.startTest();
        System.schedule('myJobTestJobName', CRONEXPR, new MX_SB_SAC_UpdateContractFieldsScheduler());
        Test.stopTest();
        final List<AsyncApexJob> jobsScheduled = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'ScheduledApex'];
        System.assertEquals(1, jobsScheduled.size(), 'expecting one scheduled job');
    }
}