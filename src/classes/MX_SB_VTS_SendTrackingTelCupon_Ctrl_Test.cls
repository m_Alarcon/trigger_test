/**
 * @File Name          : MX_SB_VTS_SendTrackingTelCupon_Ctrl_Test.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 10-06-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/6/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
@isTest
private class MX_SB_VTS_SendTrackingTelCupon_Ctrl_Test {
    /**Batchable context */
    public final static Database.BatchableContext BATCHCONTEXT;
    /**Producto HSD */
    final static String PRODHSD = 'Hogar seguro dinámico';
    
    @TestSetup
    static void makeData() {
        final User asesorUser = MX_WB_TestData_cls.crearUsuario('Asesor', 'Telemarketing VTS');
        insert asesorUser;

        final Account accToAssign = MX_WB_TestData_cls.crearCuenta('Donnie', 'PersonAccount');
        accToAssign.PersonEmail = 'donnie.test@test.com';
        accToAssign.OwnerId = asesorUser.Id;
        insert accToAssign;
        final List<String> lstValues = new List<String>{
            System.Label.MX_SB_VTS_Hogar,
            'Smart Center',
            'OppTelemarketing',
            'Venta',
            '999999 1 OppTest Close Opp',
            '999990',
            '72'
        };
        System.runAs(asesorUser) {
            MX_SB_VTS_CallCTIs_utility.insertBasicsVTS(lstValues,  System.Label.MX_SB_VTS_Telemarketing_LBL, asesorUser, accToAssign);
        }
        final MX_SB_VTS_Generica__c generica = MX_WB_TestData_cls.GeneraGenerica('HogarSeguro', 'CP10');
        generica.MX_SB_SAC_ProductFam__c = PRODHSD;
        insert generica;
    }

    @isTest
    private static void sendOppsSmart() {
        MX_SB_VTS_CallCTIs_utility.insertIasoVTSH();
        Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_Integration_MockGenerator());
        iaso.GBL_Mock.setMock(new MX_SB_VTS_Integration_MockGenerator());
        final String sQuery = MX_SB_VTS_ScheduldOppsTele_cls.createQueryOpps();
        Test.startTest();
        final Id jobId = Database.executeBatch(new MX_SB_VTS_SendTrackingTelCupon_Ctrl(sQuery, 0));
            System.assert(String.isNotBlank(jobId), 'Execute Cupones');
        Test.stopTest(); 
    }

    @isTest
    private static void sendOppsSmartCup() {
        MX_SB_VTS_CallCTIs_utility.insertIasoVTSH();
        Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_Integration_MockGenerator());
        iaso.GBL_Mock.setMock(new MX_SB_VTS_Integration_MockGenerator());
        final String sQueryCup = MX_SB_VTS_ScheduldOppsTele_cls.createQueryOpps();
        final List<Opportunity> validOpps = MX_SB_VTS_CallCTIs_utility.lstOppsTest();
        for(Opportunity opp : validOpps) {
            opp.MX_WB_EnvioCTICupon__c = true;
            opp.TelefonoCliente__c = '5565478919';
        }
        update validOpps;
        final List<Opportunity> validOppsSend = (List<Opportunity>)Database.query(sQueryCup);
        final MX_SB_VTS_SendTrackingTelCupon_Ctrl shCup = new MX_SB_VTS_SendTrackingTelCupon_Ctrl(sQueryCup, 0);
        Test.startTest();
            shCup.execute(BATCHCONTEXT, validOppsSend);
            System.assertEquals(validOpps.size(),1, 'Execute correcto');
        Test.stopTest(); 
    }
}