/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 02-04-2021
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   02-04-2021   Eduardo Hernández Cuamatzi   Initial Version
**/
@isTest
private class MX_SB_VTS_editCoverageHSD_c_Test {
   
   /**URL Mock Test*/
   private final static String URLTOTESTASO = 'http://www.example.com';
   /**Particion IASO*/
   private final static String IASOPARTASO = 'local.MXSBVTSCache';
   /**@description etapa Cotizacion*/
   private final static string COTIZACION = 'Cotización';
   /**@description Nombre usuario*/
   private final static string COMPLETE = 'Complete';
   /**Servicio IASO*/
   private final static String CALCULATEPRICES = 'CalculateQuotePrice';
   /**@description llave de mapa*/
   private final static string TOKEN = '12345678';
   /**@description llave de ERROR*/
   private final static string TSEC = 'tsec';
   /**@description llave de ERROR*/
   private final static string PORCCRISTAL = '2008PORCCRISTAL';

   @TestSetup
    static void makeData() {
        MX_SB_VTS_CallCTIs_utility.initHogarGeneric();
        final Opportunity oppEditCov = [Select Id, Name from Opportunity where StageName =: COTIZACION];
        oppEditCov.Producto__c = System.Label.MX_SB_VTS_PRO_HogarDinamico_LBL;
        oppEditCov.LeadSource = 'Facebook';
        final Map<Id, Quote> mapQuoteData = new Map<Id, Quote>([Select Id, Name from Quote where OpportunityId =: oppEditCov.Id]);
        for(Quote quoteItem : mapQuoteData.values()) {
            quoteItem.MX_SB_VTS_ASO_FolioCot__c = '12345';
            oppEditCov.SyncedQuoteId = quoteItem.Id;
            quoteItem.QuoteToName = 'Facebook-Completa';
            MX_RTL_Cobertura_Selector_Test.initCoverages(quoteItem.Id);
        }
        update mapQuoteData.values();
        update oppEditCov;
        MX_SB_VTS_CallCTIs_utility.initLeadMulServHSD();
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'getGTServicesSF', iaso__Url__c = URLTOTESTASO, iaso__Cache_Partition__c = IASOPARTASO);
        insert new iaso__GBL_Rest_Services_Url__c(Name = CALCULATEPRICES, iaso__Url__c = URLTOTESTASO, iaso__Cache_Partition__c = IASOPARTASO);
        final MX_SB_VTS_Generica__c setting = MX_WB_TestData_cls.GeneraGenerica('IASO01', 'IASO01');
        setting.MX_SB_VTS_Type__c = 'IASO01';
        setting.MX_SB_VTS_HEADER__c = 'http://www.example.com';
        insert setting;
    }

    @isTest
    static void updateDynamicList() {
        final Opportunity oppIdCrit = [Select Id, Name,Producto__c from Opportunity where StageName =: COTIZACION];
        final String calculateStr = MX_SB_VTS_ScheduldOppsTele_tst.quotetationStr(CALCULATEPRICES);
        Final Map<String, String> headersMock = new Map<String, String>();
        headersMock.put(TSEC, TOKEN);
        Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, COMPLETE, calculateStr, headersMock); 
        iaso.GBL_Mock.setMock(mockCallout);
        final List<String> lstAfectCovers = new List<String>{'GLASS_BREAKAGE'};
        final List<String> valuesCriterial = new List<String>{PORCCRISTAL};
        final List<String> lstPartDat = new List<String>{'MISC', PORCCRISTAL, '003', '0.2', '003',PORCCRISTAL,'0.2','$2,000,000.00'};
        Test.startTest();
            final Map<String, Object> updateDynMap = MX_SB_VTS_editCoverageHSD_ctrl.updateDynamicList(oppIdCrit.Id, lstAfectCovers, valuesCriterial, lstPartDat);
            System.assert((Boolean)updateDynMap.get('isOk'), 'Cobertura Editada');
        Test.stopTest();
    }
}