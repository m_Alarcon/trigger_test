@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_Siniestro_Cls_Ctl_Test
* Autor Daniel Perez Lopez
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_Siniestro_cls

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           10/12/2019      Daniel Lopez                         Creación
* 1.1           17/12/2019      Angel Nava                           Cobertura
* 1.2           18/02/2020      Marco Cruz                           CodeSmells
* 1.3           12/03/2020      Angel Nava                           migración de contract
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_Siniestro_Cls_Ctl_Test {
    
    
    @isTest static void createpartsinfallo() {
     	Test.startTest();
        try {
            Final MX_SB_MLT_Siniestro_Cls_Controller clsctrl = new MX_SB_MLT_Siniestro_Cls_Controller();
            clsctrl.createPartSin(null);
        } catch(AuraHandledException e) {
			System.assertEquals('Script-thrown exception', e.getMessage(),'Excepción Propagada Correctamente');
        }
        Test.stopTest();   
    }
       @IsTest 
        static void metodo3() {
        Final Account acparamedico2 =new Account(name='Paramedicotest',recuperacion__c=false,Tipo_Persona__c='Física', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_Proveedores_MA').getRecordTypeId());
        insert acparamedico2;
        final Contract contrato2 = new Contract(AccountId=acparamedico2.id,MX_SB_SAC_NumeroPoliza__c='A4234', name='testcontract');
        insert contrato2;
        Final Siniestro__c sini= new Siniestro__c();
        sini.Folio__c='sinitest';
        sini.MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro';
        sini.MX_SB_MLT_RequiereParamedico__c=true;
        sini.MX_SB_MLT_RequiereAmbulancia__c=true;
        sini.MX_SB_MLT_RequiereGrua__c=true;
        sini.MX_SB_MLT_ServicioParamedico__c=true;
		sini.MX_SB_MLT_ServicioAmbulancia__c=true;
		sini.MX_SB_MLT_ServicioGrua__c=true;
        sini.MX_SB_MLT_Telefono__c='55612783';
        sini.MX_SB_MLT_PhoneTypeAdditional__c='';
        sini.MX_SB_MLT_NombreConductor__c='testdny';
        sini.MX_SB_MLT_APaternoConductor__c='appdny';
        sini.MX_SB_MLT_AMaternoConductor__c='apmdny';
        sini.MX_SB_SAC_Contrato__c = contrato2.id;
        insert sini;
        
        Final MX_SB_MLT_Siniestro_Cls_Controller clsctrl = new MX_SB_MLT_Siniestro_Cls_Controller();
        Test.startTest();
        clsctrl.createPartSin(sini.id);
        test.stopTest();
        system.assertNotEquals(null, sini,'prueba participante');
        }

     @IsTest 
        static void metodo4() {
        Final Account acparamedico =new Account(name='Paramedicotest',recuperacion__c=false,Tipo_Persona__c='Física', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_Proveedores_MA').getRecordTypeId());
        insert acparamedico;
        final Contract contrato = new Contract(AccountId=acparamedico.id,MX_SB_SAC_NumeroPoliza__c='A4234', name='testcontract');
        insert contrato;
        final Siniestro__c sini= new Siniestro__c();
        sini.Folio__c='sinitest';
        sini.MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro';
        sini.MX_SB_MLT_RequiereParamedico__c=true;
        sini.MX_SB_MLT_RequiereAmbulancia__c=true;
        sini.MX_SB_MLT_RequiereGrua__c=true;
        sini.MX_SB_SAC_Contrato__c = contrato.id;
        sini.MX_SB_MLT_LugarAtencionAuto__c = 'Crucero';
        sini.MX_SB_MLT_ServicioParamedico__c = false;
        sini.RecordTypeId =  Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_RamoAuto').getRecordTypeId();
        insert sini;
        Final Account accTest =new Account(name='Grua2',recuperacion__c=false,Tipo_Persona__c='Física', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_Proveedores_MA').getRecordTypeId());
        insert accTest;
        system.assertEquals('Grua2',accTest.name,'Exito en la test');

        Test.startTest();
        final String response = MX_SB_MLT_Siniestro_Cls_Controller.CreateServices(sini.id,accTest.id,accTest.id,accTest.id);
        MX_SB_MLT_Siniestro_Cls_Controller.CreateServices(sini.id,null,accTest.id,accTest.id);
        MX_SB_MLT_Siniestro_Cls_Controller.CreateServices(sini.id,accTest.id,null,accTest.id);
        MX_SB_MLT_Siniestro_Cls_Controller.CreateServices(sini.id,accTest.id,accTest.id,null);
        system.assertNotEquals(null, response,'prueba servicios');
        Test.stopTest();

    }
    
}