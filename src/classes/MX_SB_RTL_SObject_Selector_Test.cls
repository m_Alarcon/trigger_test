/**
 * @description       : 
 * @author            : Gerardo Mendoza Aguilar
 * @group             : 
 * @last modified on  : 12-02-2020
 * @last modified by  : Gerardo Mendoza Aguilar
 * Modifications Log 
 * Ver   Date         Author                    Modification
 * 1.0   12-02-2020   Gerardo Mendoza Aguilar     Initial Version
 * 1.1   04-01-2021   Juan Carlos Benitez Herrera Se modifica Assert
**/
@isTest
public class MX_SB_RTL_SObject_Selector_Test {
    /*
*Constructor de datos para pruebas
*/
    @testSetup
    public static void data4test () {
        final String grtPaC = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
        final Account acctst = new Account(FirstName='Name', RecordTypeId = grtPaC, LastName='LNAME', Tipo_Persona__c='Física');
	    insert acctst;
    }
    /*
* Method1  pruebas sobject
*/    
    @isTest
   	public static void method1 () {
		test.startTest();
        	final Account acoun = [select id from account where FirstName='Name' limit 1];
        	final Account myac = (Account) MX_SB_RTL_SObject_Selector.getObject(acoun.id,' LastName ', ' Account');
        	System.assertEquals('LNAME',myac.LastName,'Exito en validacion');
        test.stopTest();
    }

}