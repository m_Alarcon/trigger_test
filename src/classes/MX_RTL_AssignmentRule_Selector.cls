/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 08-09-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   07-31-2020   Eduardo Hernandez Cuamatzi   Initial Version
**/
@SuppressWarnings('sf:UseSingleton, sf:DMLWithoutSharingEnabled')
public class MX_RTL_AssignmentRule_Selector {
    /**
    * @description Retorna las reglas de asignación 
    * @author Eduardo Hernandez Cuamatzi | 07-31-2020 
    * @param List<String> nameAssigment nombre de reglas de asignación
    * @param Boolean isActive indica si es se buscan reglas activas
    * @param String sObjectName Indica el tipo de sobject al que aplica la regla
    * @return List<AssignmentRule> Lista de reglas de asignación
    **/
    public static List<AssignmentRule> selectAssigmentName(Set<String> nameAssigment, Boolean isActive, String sObjectName) {
        return [select Id,Name from AssignmentRule where SobjectType =: sObjectName and Active =: isActive AND Name IN: nameAssigment];
    }
}