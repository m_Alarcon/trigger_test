/**
* @description       : Clase que contiene el armado para el consumo de servicio de agregar coberturas
* @author            : Diego Olvera
* @group             : BBVA
* @last modified on  : 03-04-2021
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   12-31-2020   Diego Olvera   Initial Version
* 1.1   02-03-2021   Diego Olvera   Se agrega/elimina funcion que recupera lista de coberturas
* 1.1.1 02-03-2021   Eduardo Hernandez Se agregan campos a query de coberturas
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton, sf:AvoidGlobalModifier')
global class MX_SB_VTS_AddCoberturas_Service {
    /** String para guardar valor de tipo de registro DP */
    static final String RECODATOS = 'Datos particulares';
    /** String para guardar valor de tipo de registro COBER */
    static final String RECOCOBER = 'Coberturas';
    /** Código de respuesta del servicio */
    private static final Integer OKCODE = 200;
    /** Fields para lista de coberturas */
    public static final String COBERFIELDS = 'Id, MX_SB_VTS_CodeTrade__c, MX_SB_VTS_RelatedQuote__c, recordType.Name, MX_SB_MLT_Cobertura__c, MX_SB_VTS_CoverageCode__c, MX_SB_VTS_GoodTypeCode__c, MX_SB_VTS_CategoryCode__c, MX_SB_MLT_Descripcion__c, MX_SB_VTS_TradeValue__c';
    
        /**
    * @description Función que consume servicio de calculateQuotePrice
    * @author Diego Olvera | 04-01-2021 
    * @param valResp, valQuoId
    * @return Map<String, Object>
    **/  
    public static Map<String, Object> calculateQuotePrice(String valResp, String valQuoId) {
        final Map<String, Object> returnVals = new Map<String, Object>();
        final Map<String, Object> paramsSrv = new Map<String, Object>{'priceid' => valQuoId};
            final HttpResponse responseSrv = MX_RTL_IasoServicesInvoke_Selector.callServices('CalculateQuotePrice', paramsSrv, getRequestAdd(valResp));
        if(responseSrv.getStatusCode() ==  OKCODE) {
            returnVals.put('success200', true); 
            returnVals.put('getData', responseSrv.getBody());
            final Map<String, Object> addStatusCode = new Map<String, Object>{'code' => String.valueOf(OKCODE), 'description' => 'Consumo de agregar cobertura exitoso'}; 
                returnVals.put('getCode', addStatusCode);
        } else {
            returnVals.put('success200', false);
            returnVals.put('getCode', MX_SB_VTS_Codes_Utils.statusCodes(responseSrv.getStatusCode()));
        }
        return returnVals;
    }

     /**
    * @description Realiza el armado del objeto Request para el consumo del servicio ASO
    * @author Diego Olvera | 04-01-2021 
    * @param valFinRep
    * @return HttpRequest
    **/  
    public static HttpRequest getRequestAdd(String valFinRep) {
        final Map<String,iaso__GBL_Rest_Services_Url__c> allCodesASO = iaso__GBL_Rest_Services_Url__c.getAll();
        final iaso__GBL_Rest_Services_Url__c objASOSrv = allCodesASO.get('CalculateQuotePrice');
        final String strEndoPoint = objASOSrv.iaso__Url__c;
        final HttpRequest requestBo = new HttpRequest();
        requestBo.setTimeout(120000);
        requestBo.setMethod('POST');
        requestBo.setHeader('Content-Type', 'application/json');
        requestBo.setHeader('Accept', '*/*');
        requestBo.setHeader('Host', 'https://test-sf.bbva.mx');
        requestBo.setEndpoint(strEndoPoint);
        requestBo.setBody(valFinRep);
        return requestBo;
    }
    
     /**
    * @description Recupera las coberturas por codigo ligadas a un registro de cotización
    * @author Diego Olvera | 04-01-2021 
    * @param quoteId, coverages
    * @return List<Cobertura__c>
    **/  
    public static List<Cobertura__c> findCoverCodes(Id quoteId, List<String> coverages) {
        final Set<Id> quotesId = new Set<Id>{quoteId};
            return MX_RTL_Cobertura_Selector.findCoverByCode(COBERFIELDS, ' ', quotesId, coverages);
    }

     /**
    * @description Recupera las coberturas por codigo y valor de zona
    * @author Diego Olvera | 04-03-2021 
    * @param quoteId, tradeVal, coverVals
    * @return List<Cobertura__c>
    **/  
    public static List<Cobertura__c> findByTrade(Id quoteId, String tradeVal, List<String> coverVals) {
        final Set<Id> sQuoteIds = new Set<Id>{quoteId};
            return MX_RTL_Cobertura_Selector.findCoverByTrade(COBERFIELDS, tradeVal, sQuoteIds, coverVals);
    }
    
     /**
    * @description Realiza el armado del Body que sera ocupado en el Request del Servicio
    * @author Diego Olvera | 04-01-2021 
    * @param lsCober, oppValues, masterTrade, sQuoId
    * @return Map<String, Object>
    **/  
    public static Map<String, Object> fillAddCoverages(List<Cobertura__c> lsCober,Opportunity oppValues, String masterTrade, Id sQuoId) {
        final Set<Id> nSetId = new Set<Id>();
        nSetId.add(sQuoId);
        final List<Cobertura__c> lsPartData = MX_RTL_Cobertura_Selector.findCoverByQuote(COBERFIELDS, ' ', nSetId);
        final Map<String, Object> fillJson = new Map<String, Object>();
        final Map<String, String> getProd = MX_SB_VTS_DeleteCoverageHSD_Helper.findProdCodByName(oppValues.Producto__c);
        final Map<String, Object> fillProd = MX_SB_VTS_DeleteCoverageHSD_Helper.fillProductElement(getProd.get(oppValues.Producto__c.toUpperCase()), masterTrade);
        final Map<String, Object> frecuenciaVal = new Map<String, Object>();
        final List<Object> lstFrecuencias = new List<Object>{frecuenciaVal};
            fillJson.put('allianceCode', 'DHSDT003');
        fillJson.put('certifiedNumberRequested', '1');       
        fillJson.put('product', fillProd);
        frecuenciaVal.put('id', 'YEARLY');
        fillJson.put('frequencies', lstFrecuencias);
        final List<Object> lstCoverages = new List<Object>();
        final List<Object> lstTrades = new List<Object>();
        for(Cobertura__c lstPart : lsPartData ) { 
            if(lstPart.recordType.Name == RECODATOS) {
                final Map<String, Object> mapSiteria = new Map<String, Object>();
                mapSiteria.put('criterial', lstPart.MX_SB_MLT_Cobertura__c);
                mapSiteria.put('id', lstPart.MX_SB_MLT_Descripcion__c);
                mapSiteria.put('value', lstPart.MX_SB_VTS_TradeValue__c);
                lstCoverages.add(mapSiteria);   
            } 
        }
        for(Cobertura__c lstCober : lsCober ) { 
            if(lstCober.recordType.Name == RECOCOBER) {
                final Map<String, Object> mFillTrades = new Map<String, Object>();
                final Map<String, Object> mFillCategories = new Map<String, Object>();
                final Map<String, Object> mFillGoodTypes = new Map<String, Object>();
                final Map<String, Object> mFillCoverages = new Map<String, Object>();
                mFillTrades.put('id', lstCober.MX_SB_VTS_TradeValue__c);
                mFillCategories.put('id', lstCober.MX_SB_VTS_CategoryCode__c);
                mFillGoodTypes.put('id', lstCober.MX_SB_VTS_GoodTypeCode__c);
                mFillCoverages.put('id', lstCober.MX_SB_VTS_CoverageCode__c);
                final List<Object> lstCover = new List<Object>{mFillCoverages};
                    mFillGoodTypes.put('coverages', lstCover);
                final List<Object> lstGoodTypes = new List<Object>{mFillGoodTypes};
                    mFillCategories.put('goodTypes', lstGoodTypes);
                final List<Object> lstCategories = new List<Object>{mFillCategories};
                    mFillTrades.put('categories', lstCategories);
                lstTrades.add(mFillTrades);
            }
            fillJson.put('contractingCriteria', lstCoverages);
            fillJson.put('trades', lstTrades);
        }
        return fillJson;
    }
}