/**
* @File Name          : MX_BPP_Contactabilidad_Service.cls
* @Description        :
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 01/10/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      01/10/2020        Gabriel García Rojas              Initial Version
* 1.2      25/10/2020        Gabriel García Rojas              Update methods
**/
@SuppressWarnings()
public with sharing class MX_BPP_Contactabilidad_Service {

    /*Variable global que  reemplaza por No contactado*/
    static final String VAR_NCONTACT='No contactado';
    /*Variable global que  reemplaza por contactado*/
    static final String VAR_CONTACT='Contactado';
    /*Variable global que  reemplaza por sm*/
    public static final String STR_SM='sm';
    /*Variable global que  reemplaza por em*/
    public static final String STR_EM='em';
    /*Variable global que  reemplaza por St*/
    public static final String VAR_ST='St';
    /*Variable global que  reemplaza por En*/
    public static final String VAR_EN='En';

    /*Variable global Cliente Moral*/
    static final String RECTRECTE='BPyP_tre_Cliente';
    /*Variable global Cliente Físico*/
    static final String RECPACTE='MX_BPP_PersonAcc_Client';
    /*Variable global vacia*/
    static final String EMPTYVAL = '';
    /*Variable global tipo de visiota 05*/
    static final String STATUS05 = '05';
    /*Variable global tipo de visiota 06*/
    static final String STATUS06 = '06';
    /*Variable global Banca*/
    static final String BANCA = 'Red BPyP';

    /**Constructor */
    @TestVisible
    private MX_BPP_Contactabilidad_Service() { }

    /**
    * @description retorna y procesa la información para la estructura Chart del componente de Contactabilidad
    * @author Gabriel Garcia Rojas
    * @param String tipoConsulta
    * @param String titulo
    * @param List<String> params
    * @param DateTime startDte
    * @param DateTime endDte
    * @return WRP_ChartStacked
    **/
    @SuppressWarnings('sf:DUDataflowAnomalyAnalysis')
    public static WRP_ChartStacked fetchDataService(String tipoConsulta, String titulo, List<String> params, DateTime startDte, DateTime endDte) {
        final String figura = MX_BPP_Contactabilidad_Helper.gtPeriodo(titulo);
        final String periodo = [SELECT periodo__c FROM Periodo_Evaluaci_n_Visita__mdt WHERE Label =: figura].periodo__c;
        final Integer varSm = MX_BPP_Contactabilidad_Helper.retmonth(STR_SM, periodo);
        final Integer varEm = MX_BPP_Contactabilidad_Helper.retmonth(STR_EM, periodo);

        DateTime stDate = MX_BPP_Contactabilidad_Helper.obtStAndEndDate(varSm,VAR_ST);
        DateTime enDate = MX_BPP_Contactabilidad_Helper.obtStAndEndDate(varEm,VAR_EN).addDays(1);

        if(startDte != null) {
            stDate = datetime.newInstance(DateTime.ValueOf(startDte).year(),DateTime.ValueOf(startDte).month(),(DateTime.ValueOf(startDte).day()+1),0,0,0);
        }
        if(endDte != null) {
            enDate = datetime.newInstance(DateTime.ValueOf(endDte).year(),DateTime.ValueOf(endDte).month(),(DateTime.ValueOf(endDte).day()+2),0,0,0);
        }

        String fieldsAcc = '';
        String fieldsVisit = '';
        List<AggregateResult> listAcc = new List<AggregateResult>();
        Map<String, Decimal> mapAcc;
        Map<String, Decimal> mapVisits;

        final String filtro0 = params[0];

        final Map<String, List<String>> mapFields = fetchMapType();

        fieldsAcc = mapFields.get(tipoConsulta)[0];
        fieldsVisit = mapFields.get(tipoConsulta)[1];

        final String filedsAccQuery = ' count(Id) TotalBy, ' + fieldsAcc;
        final String clouseAccQuery = ' WHERE Owner.IsActive = true AND Owner.Title =: filtro1 AND Owner.VP_ls_Banca__c =: filtro2 AND (RecordType.DeveloperName =:filtro3 OR RecordType.DeveloperName =: filtro4 ) '
            + mapFields.get(tipoConsulta)[3] + ' GROUP BY ' + fieldsAcc ;

        final List<String> listFiltersAcc = new List<String>{filtro0, titulo, BANCA, RECTRECTE, RECPACTE};
        listAcc = MX_RTL_Account_Selector.fetchListAggAccByDataBase(filedsAccQuery, clouseAccQuery, listFiltersAcc);

        final String filedsVisitQuery = ' RecordType.DeveloperName, MX_BIE_TipoVisitaLLamada__c, dwp_kitv__account_id__c, ' + fieldsVisit;
        final String clouseVisitQuery = ' WHERE dwp_kitv__account_id__r.Owner.Title =: filtro1 '
          		+ ' AND dwp_kitv__account_id__r.Owner.VP_ls_Banca__c =: filtro2 '
           		+ ' AND (dwp_kitv__visit_status_type__c =: filtro3  OR dwp_kitv__visit_status_type__c =: filtro4) '
                + ' AND (dwp_kitv__account_id__r.RecordType.DeveloperName =: filtro5 OR dwp_kitv__account_id__r.RecordType.DeveloperName =: filtro6) '
                + ' AND (NOT(dwp_kitv__account_id__r.Owner.Divisi_n__c =: filtro7)) '
            	+ mapFields.get(tipoConsulta)[4]
            	+ ' AND LastModifiedDate >=:stDate AND LastModifiedDate <=:enDate ';

        final List<String> listFiltersVisit = new List<String>{filtro0, titulo, BANCA, STATUS05, STATUS06, RECTRECTE, RECPACTE, EMPTYVAL};
        final List<dwp_kitv__Visit__c> listVisitComplete = Dwp_kitv_Visit_Selector.fetchListVisitByParams(filedsVisitQuery, clouseVisitQuery, listFiltersVisit, stDate, enDate);

        mapVisits = gtMapTotalRecordsList(listVisitComplete, new List<String>{mapFields.get(tipoConsulta)[2]});

        mapAcc = gtMapTotalRecords(listAcc, new List<String>{mapFields.get(tipoConsulta)[2],'TotalBy'});

        final Map<String, Decimal> numVis = new Map<String, Decimal>();
        final Map<String, Decimal> lsData = new Map<String, Decimal>();

        Decimal numVisits = 0.0;
        for(String keyMap : mapAcc.keySet()) {
            numVisits = mapVisits != null && mapVisits.containsKey(keyMap) ? mapVisits.get(keyMap) : 0;
            numVis.put(VAR_CONTACT + keyMap , numVisits);
            numVis.put(VAR_NCONTACT + keyMap , mapAcc.get(keyMap) - numVisits);

            lsData.put(VAR_CONTACT + keyMap , ((numVisits*100)/mapAcc.get(keyMap)).setScale(2)); //NOSONAR
            lsData.put(VAR_NCONTACT + keyMap , 100 - lsData.get(VAR_CONTACT + keyMap));
    	}

        final List<String> tempColor = new List<String>{'rgba(216,97,79,1)','rgba(42,128,254,1)','rgba'};
        final List<String> tempTyOpp = new List<String>{VAR_NCONTACT, VAR_CONTACT};
        final List<String> tempLabel = new List<String>(mapAcc.keySet());

        return new WRP_ChartStacked(tempLabel,tempTyOpp,tempColor,lsData,numVis);
    }

    /**
    * @description regresa una lista de cuentas con sólo 1 visita
    * @author Gabriel Garcia Rojas
    * @param List<dwp_kitv__Visit__c> listVisit
    * @param List<String> params
    * @return Map<String, Decimal>
    **/
    public static Map<String, Decimal> gtMapTotalRecordsList(List<dwp_kitv__Visit__c> listVisit, List<String> params) {
        final Map<String, Decimal> mapTotals = new Map<String, Decimal>();
        final Set<String> setCuentas = new Set<String>();
        String campo = '';
        String cuenta = '';
        for(dwp_kitv__Visit__c visita : listVisit) {
            campo = (String) visita.dwp_kitv__account_id__r.Owner.get(params[0]);
			cuenta = visita.dwp_kitv__account_id__c;

            if(!setCuentas.contains(cuenta + campo)) {
                if(mapTotals.containsKey(campo)) {
                    mapTotals.put(campo, mapTotals.get(campo) + 1);
                } else {
                    mapTotals.put(campo, 1);
                }
                setCuentas.add(cuenta + campo);
            }
        }
        return mapTotals;
    }

    /**
    * @description procesa y retorna el número total de registros
    * @author Gabriel Garcia Rojas
    * @param List<AggregateResult> listAggR
    * @param List<String> params
    * @return Map<String, Decimal>
    **/
    public static Map<String, Decimal> gtMapTotalRecords(List<AggregateResult> listAggR, List<String> params) {
        final Map<String, Decimal> mapTotals = new Map<String, Decimal>();
        String division = '';
        Decimal total = 0.0;
        for(AggregateResult aggRes : listAggR) {
            division = String.valueOf(aggRes.get(params[0]));
            total = Decimal.valueOf(String.valueOf(aggRes.get(params[1])));
            if(mapTotals.containsKey(division)) {
                mapTotals.put(division, mapTotals.get(division) + total);
            } else {
                mapTotals.put(division, total);
            }
        }
        return mapTotals;
    }

    /**
    * @description procesa y retorna la lista de cuentas contactadas o no contactadas
    * @author Gabriel Garcia Rojas
    * @param List<String> params
    * @param String contactType
    * @param DateTime startDte
    * @param DateTime endDte
    * @param String titulo
    * @param String limite
    * @param String tipoQuery
    * @return List<Account>
    **/
    public static List<Account> servContactaAcc(List<String> params, DateTime startDte, DateTime endDte) {
        String limitAndOffset = '';

        final String filtro0 = params[0];
        final String contactType = params[1];
        final String titulo = params[2];
        final String limite = params[3];
        final String tipoQuery = params[4];

        final String figuraServ = MX_BPP_Contactabilidad_Helper.gtPeriodo(titulo);
        final String periodoServ = [SELECT periodo__c FROM Periodo_Evaluaci_n_Visita__mdt WHERE Label =: figuraServ].periodo__c;
        final Integer varSm = MX_BPP_Contactabilidad_Helper.retmonth(STR_SM, periodoServ);
        final Integer varEm = MX_BPP_Contactabilidad_Helper.retmonth(STR_EM, periodoServ);

        DateTime enDate = MX_BPP_Contactabilidad_Helper.obtStAndEndDate(varEm,VAR_EN).addDays(1);
        DateTime stDate = MX_BPP_Contactabilidad_Helper.obtStAndEndDate(varSm,VAR_ST);

        if(startDte != null) {
            stDate = datetime.newInstance(DateTime.ValueOf(startDte).year(),DateTime.ValueOf(startDte).month(),(DateTime.ValueOf(startDte).day()+1),0,0,0);
        }
        if(endDte != null) {
            enDate = datetime.newInstance(DateTime.ValueOf(endDte).year(),DateTime.ValueOf(endDte).month(),(DateTime.ValueOf(endDte).day()+2),0,0,0);
        }

        if(String.isNotBlank(limite)) {
            limitAndOffset = ' limit 10 offset ' + limite;
        }

        final Map<String, List<String>> mapFields = fetchMapType();
        String filedsAccQuery = ' Id ';
        String filedsVisitQuery = ' Id ';
        List<String> listFiltersAcc;

        try {
            List<Account> accts= new List<Account>();

            final List<Account> accResult = new List<Account>();
            if(MX_BPP_Contactabilidad_Helper.NOCONTACT.equals(contactType)) {

                filedsAccQuery = ' Id, Name, No_de_cliente__c, Phone, Owner.Name, BPyP_Fecha_de_ultima_visita__c, ';
                filedsVisitQuery = ' (SELECT Id, Name, dwp_kitv__visit_status_type__c FROM dwp_kitv__Visits__r WHERE ((NOT(dwp_kitv__account_id__r.Owner.Divisi_n__c=null)) '+
                    mapFields.get(tipoQuery)[4] +
                    ' AND (NOT(dwp_kitv__account_id__r.Owner.BPyP_ls_NombreSucursal__c=: filtro4))  AND LastModifiedDate >=:stDate AND LastModifiedDate <=:enDate '+
                    ' AND dwp_kitv__account_id__r.Owner.Title=:filtro1  AND (dwp_kitv__account_id__r.RecordType.DeveloperName=:filtro2 '+
                    ' OR dwp_kitv__account_id__r.RecordType.DeveloperName=: filtro3)))';
                final String clouseAccQuery = ' WHERE (RecordType.DeveloperName =: filtro2 '+
                    ' OR RecordType.DeveloperName =: filtro3) AND Owner.Title=:filtro1 ' + mapFields.get(tipoQuery)[3] + limitAndOffset;

                listFiltersAcc = new List<String>{filtro0, titulo, RECTRECTE, RECPACTE, EMPTYVAL};
                accts = MX_RTL_Account_Selector.fetchListAccByDataBase(filedsAccQuery + filedsVisitQuery, clouseAccQuery, listFiltersAcc, stDate, enDate);

                for(Account acc:accts) {
                    final Set<Account> accSet = MX_BPP_Contactabilidad_Helper.processAccounts(acc, contactType);
                    if(!accSet.isEmpty()) {
                        accResult.addAll(accSet);
                    }
                }
            } else {

                filedsVisitQuery = ' dwp_kitv__account_id__c ';
                final String clouseVisitQuery = ' WHERE ((NOT(dwp_kitv__account_id__r.Owner.Divisi_n__c = null)) ' +
                               ' AND (dwp_kitv__visit_status_type__c =: filtro3  OR dwp_kitv__visit_status_type__c =: filtro4) ' +
                               mapFields.get(tipoQuery)[4] +
                               ' AND (NOT(dwp_kitv__account_id__r.Owner.BPyP_ls_NombreSucursal__c =: filtro2)) '+
                               ' AND LastModifiedDate >=: stDate AND LastModifiedDate <=: enDate '+
                               ' AND dwp_kitv__account_id__r.Owner.Title =: filtro1 ' +
                               ' AND (dwp_kitv__account_id__r.RecordType.DeveloperName =: filtro5 ' +
                                 ' OR dwp_kitv__account_id__r.RecordType.DeveloperName =: filtro6 ))';

                final List<String> listFiltersVisit = new List<String>{filtro0, titulo, EMPTYVAL, STATUS05, STATUS06, RECTRECTE, RECPACTE, EMPTYVAL};

                final Set<Id> setIds = MX_BPP_Contactabilidad_Helper.processAccounts(Dwp_kitv_Visit_Selector.fetchListVisitByParams(filedsVisitQuery, clouseVisitQuery, listFiltersVisit, stDate, enDate));
                filedsAccQuery = ' Id, Name, No_de_cliente__c, Phone, Owner.Name, BPyP_Fecha_de_ultima_visita__c, Owner.Divisi_n__c ';
                final String clouseAccQuery = ' WHERE (RecordType.DeveloperName =: filtro3 OR RecordType.DeveloperName =:filtro2 ) ' +
                      ' AND Owner.Title =: filtro1 ' + mapFields.get(tipoQuery)[3] +' AND Id IN: setIds ' + limitAndOffset;
                listFiltersAcc = new List<String>{filtro0, titulo, RECTRECTE, RECPACTE, EMPTYVAL};
                accts = MX_RTL_Account_Selector.fetchListAccByIds(filedsAccQuery, clouseAccQuery, listFiltersAcc, setIds);

                accResult.addAll(accts);
            }
            return accResult;
        } catch(QueryException e) {
            throw new AuraHandledException(System.Label.MX_BPP_PyME_Error_Generico+ ' ' + e + e.getLineNumber());
        }
    }

    /**
    * @description retorna mapa con las condiciones de búsqueda por division, oficina o ejecutivo
    * @author Gabriel Garcia Rojas
    * @return Map<String, List<String>>
    **/
    public static Map<String, List<String>> fetchMapType() {
        final Map<String, List<String>> mapFields = new Map<String, List<String>>();
        mapFields.put('Division', new List<String>{'Owner.Divisi_n__c, Owner.Title','dwp_kitv__account_id__r.Owner.Divisi_n__c, Owner.Title', 'Divisi_n__c', '', ''});
        mapFields.put('Oficina', new List<String>{'Owner.BPyP_ls_NombreSucursal__c, Owner.Title',
            'dwp_kitv__account_id__r.Owner.BPyP_ls_NombreSucursal__c, Owner.Title',
            'BPyP_ls_NombreSucursal__c', ' AND Owner.Divisi_n__c =: filtro0', ' AND dwp_kitv__account_id__r.Owner.Divisi_n__c =: filtro0'});
        mapFields.put('Banquero', new List<String>{'Owner.Name, Owner.Title', 'dwp_kitv__account_id__r.Owner.Name, Owner.Title', 'Name',
            ' AND Owner.BPyP_ls_NombreSucursal__c =: filtro0', ' AND dwp_kitv__account_id__r.Owner.BPyP_ls_NombreSucursal__c =: filtro0'});
        mapFields.put('BanqueroOnly', new List<String>{'Owner.Name, Owner.Title', 'dwp_kitv__account_id__r.Owner.Name, Owner.Title', 'Name',
            ' AND Owner.Name =: filtro0', ' AND dwp_kitv__account_id__r.Owner.Name =: filtro0'});
        return mapFields;
    }

    /**
    * @description retorna la lista de ejecutivos con respecto a su oficina y titulo
    * @author Gabriel Garcia Rojas
    * @param String oficina
    * @param String titulo
    * @return List<User>
    **/
    public static List<User> fetchUserByOffice(String oficina, String titulo) {
        return MX_RTL_User_Selector.fetchListUserByDataBase(' Name ', ' WHERE BPyP_ls_NombreSucursal__c =: filtro0 AND Title =: filtro1 AND IsActive = true ', new List<String>{oficina, titulo});
    }

    /** Wrapper class for chart.js with labels, data and color*/
    public class WRP_ChartStacked {

        /** Mapa lsData*/
        @AuraEnabled public Map<String, Decimal> lsData {get; set;}
        /**Mapa numVis*/
        @AuraEnabled public Map<String, Decimal> numVis {get; set;}
        /**Lista lsLabels */
        @AuraEnabled public list<String> lsLabels {get; set;}
        /**Lista lsColor */
        @AuraEnabled public list<String> lsColor {get; set;}
        /**Lista lsTyOpp */
        @AuraEnabled public list<String> lsTyOpp {get; set;}

        /** Method */
        public WRP_ChartStacked(list<String> label,list<String> tyOpp,list<String> color,Map<String, Decimal> data,Map<String, Decimal> vis) {
            lsTyOpp=tyOpp;
            lsLabels=label;
            lsData=data;
            lsColor=color;
            numVis=vis;
        }
    }
}