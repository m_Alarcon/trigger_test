/**
 * @File Name          : MX_SB_VTS_SendTrackingCupon_sch.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernandez Cuamatzi
 * @Last Modified On   : 3/6/2020 19:43:50
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    28/5/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_SendTrackingCupon_sch implements Schedulable {
	/** variable sQuery */
	public String sQuery {get;set;}

    /**
     * execute scheduld execute 
     * @param  schContext contexto scheduld
     */
    public void execute(SchedulableContext schContext) { 
        final MX_SB_VTS_SendTrackingTelCupon_Ctrl oppCup = new MX_SB_VTS_SendTrackingTelCupon_Ctrl(sQuery, Integer.valueOf(System.Label.CotizacionCreadaCupon));
        Database.executeBatch(oppCup, 200);
    }
}