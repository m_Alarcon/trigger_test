/**
* ---------------------------------------------------------------------------------
* @Autor: Rodrigo Amador Martinez Pacheco
* @Proyecto: Auditoria
* @Descripción : Invoca las validaciones para determinar que un archivo se puede 
   cargar en salesforce
* ---------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ---------------------------------------------------------------------------------
* 1.0           18/06/2020     Rodrigo Martinez               Creación de la clase
* 1.1           15/10/2020     Rodrigo Martinez               Validación after de carga
* ---------------------------------------------------------------------------------
*/

public class MX_RTL_ContentVersion_handler extends TriggerHandler {
 
 /**
* 
* @Descripción :Invoca las validaciones previas a la carga de archivos
* @author Rodrigo Martinez
*/
 protected override void beforeInsert() {
     final List<ContentVersion> newContent=Trigger.new;
     MX_RTL_ContentAllowed_service.validateFile(newContent);
    }    
 /**
* 
* @Descripción :Invoca las validaciones posteriores a la carga de archivos
* @author Rodrigo Martinez
*/
	protected override void afterInsert() {
     final List<ContentVersion> newContent=Trigger.new;
     MX_RTL_ContentAllowed_service.validateFileAfter(newContent);
    }    
}