/*
----------------------------------------------------------
* Nombre: MX_SB_PS_MethodoPagoHelper_cls
* Daniel Perez Lopez
* Proyecto: Salesforce-Presuscritos
* Descripción : Modifica el header para el consumo de los servicios ASO mediante el componente IASO de BBVA

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor               Descripción
* --------------------------------------------------------------------------------
* 1.0           08/07/2020     Daniel Perez		    Creación
* --------------------------------------------------------------------------------
*/
global class MX_SB_PS_MetodoPagoHelper_cls implements iaso.GBL_Integration_Headers {//NOSONAR

    /*
    * @description agrega variable tsec al header del request
    * @param Httprequest
	* @return  Httprequest
    */
    global HttpRequest modifyRequest(HttpRequest request) {
        request.setHeader('tsec','kjhy65rfdsw3456yhjko09876543wasdfghjUyh');
        return request;
    }
}