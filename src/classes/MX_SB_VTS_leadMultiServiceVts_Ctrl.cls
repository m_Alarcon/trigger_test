/**
 * @File Name          : MX_SB_VTS_leadMultiServiceVts_Ctrl.cls
 * @Description        : 
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernandez Cuamatzi
 * @Last Modified On   : 07-16-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/6/2020   Eduardo Hernández Cuamatzi     Initial Version
 * 1.1    16/7/2020   Eduardo Hernández Cuamatzi     Adecuación response service body
**/
@RestResource(urlMapping='/leadMultiServiceVts/*')
@SuppressWarnings('sf:UseSingleton')
global without sharing class MX_SB_VTS_leadMultiServiceVts_Ctrl {    
    
    /**
    * @description Función petición POST
    * @author Eduardo Hernández Cuamatzi | 18/6/2020 
    * @return String Id de registro insertado
    **/
    @HttpPost
    global static void postSalesforceTracking() {
        final Map<String, Object> responseMap = new Map<String, Object>();
        final Map<String, Object> listener = (Map<String, Object>)JSON.deserializeUntyped(RestContext.request.requestBody.toString());
        final MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta dataService = MX_SB_VTS_leadMultiServiceVts_Service.fillDataServs(listener);
        if(MX_SB_VTS_leadMultiServiceVts_Service.validatemethod(dataService)) {
            responseMap.put('id', MX_SB_VTS_leadMultiServiceVts_Service.processPost(dataService));
        } else {
            responseMap.put('id', '');
        }
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseMap));
    }
}