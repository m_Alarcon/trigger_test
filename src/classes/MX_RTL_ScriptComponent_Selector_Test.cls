/**
 * @description       : 
 * @author            : Alan Ricardo Hernandez
 * @group             : 
 * @last modified on  : 22/02/2021
 * @last modified by  : Alan Ricardo Hernandez
 * Modifications Log 
 * Ver   Date         Author                   Modification
 * 1.0   22/02/2021   Alan Ricardo Hernandez   Initial Version
**/
@isTest
public class MX_RTL_ScriptComponent_Selector_Test {

    /** Constante campos query */
    final static String QUERYFIELD = 'Activo__c,DeveloperName,Id,MasterLabel';

    /** Constante condicionante query */
    final static String CONDITION = 'Activo__c = true';

    /** Constante condicionante methodo */
    final static Boolean WCONDITION = true;

    /** Constante condicionante methodo */
    final static Boolean WOCONDITION = false;

    @isTest
    static void getMetadataTestTrue() {
        Test.startTest();
            Final List<MX_SB_PS_ScriptComponent__mdt> mdtScript= MX_RTL_ScriptComponent_Selector.resultadosQueryScript(QUERYFIELD,CONDITION,WCONDITION);
            System.assertEquals(mdtScript.size(),1, 'Datos recuperados');
        Test.stopTest();
    }

    @isTest
    static void getMetadataTestFalse() {
        Test.startTest();
            Final List<MX_SB_PS_ScriptComponent__mdt> mdtScript= MX_RTL_ScriptComponent_Selector.resultadosQueryScript(QUERYFIELD,CONDITION,WOCONDITION);
            System.assertEquals(mdtScript.size(),1, 'Datos recuperados');
        Test.stopTest();
    }
}