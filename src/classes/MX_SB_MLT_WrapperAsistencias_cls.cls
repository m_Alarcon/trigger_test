/**---------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_WrapperAsistencias_cls
* Autor: Juan Carlos Benitez Herrera
* Proyecto: Siniestros - BBVA
* Descripción : Clase Wrapper de insert W Order y get W Order
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                       Desripción
* --------------------------------------------------------------------------------
* 1.0           20/01/2020     Juan Carlos Benitez          Creación
* --------------------------------------------------------------------------------
**/
public  with sharing class MX_SB_MLT_WrapperAsistencias_cls { //NOSONAR
	/*
    * @VAR
    */
    public static final string RTYPE='MX_SB_MLT_Servicios';
    /*
    * @class clase wrapper para methods que incluyan JSON.Stringigy
    */
    public class WrapperCaseGrua {
     /*
    * @var String valor Tipo Siniestro
    */
        public String defaultValue {get;set;}
     /*
    * @var String valor Tipo averia
    */
        public String defaultValueAve {get;set;}
     /*
    * @var String valor solcita reembolso
    */
        public String soliReemb {get;set;}
     /*
    * @var String valor Tipo de grua
    */
        public String defaultValueTipoG {get;set;}
     /*
    * @var String valor correo
    */
        public String email {get;set;}
     /*
    * @var String valor condiciones del vehiculo
    */
        public String condicionesVeh {get;set;}
     /*
    * @var String valor giran 4 llantas
    */
        public String llantas {get;set;}
     /*
    * @var String valor Neutral
    */
        public String neutral {get;set;}
    }
    /*
    * @description Obtiene lo datos de la W order de Grua
    * @param String sinid
    * @return Object WorkOrder
    */
    public static  WorkOrder getGruaCase(String sinid) {
		List <WorkOrder> sinList = new List <WorkOrder> ();
        if (sinid != null) {
			sinList = [SELECT Id, AccountId, MX_SB_MLT_MotivoReembolso__c, Account.Name, MX_SB_MLT_TipodeGrua__c, MX_SB_MLT_Subtipo__c,MX_SB_MLT_DescripcionVehiculo__c, 
                       MX_SB_MLT_TipoSiniestro__c, MX_SB_MLT_Email__c, MX_SB_MLT_Siniestro__c, MX_SB_MLT_TipoServicioAsignado__c, MX_SB_MLT_EstatusServicio__c, MX_SB_MLT_Q_4Llantas__c, MX_SB_MLT_Q_Neutral__c, 
                       MX_SB_MLT_ServicioAsignado__c,MX_SB_MLT_UnidadEnviada__c,MX_SB_MLT_SuppliedName__c,MX_SB_MLT_ClaimId__c,MX_SB_MLT_Reembolso__c,MX_SB_MLT_TotalReembolso__c
					   FROM WorkOrder WHERE MX_SB_MLT_Siniestro__c =: sinid and MX_SB_MLT_TipoSiniestro__c = 'Grua' and MX_SB_MLT_EstatusServicio__c NOT in('Cancelado')  limit 1 ];
		}
		return sinList.get(0);
	}
    /*
    * @description Obtiene lo datos de la W order de Cambio de llanta
    * @param String sinid
    * @return Object WorkOrder
    */
	public static  WorkOrder getCambioLlantaCase(String sinid) {
		List <WorkOrder> sinList = new List <WorkOrder> ();
		if (sinid != null) {
			sinList = [SELECT Id, AccountId,MX_SB_MLT_MotivoReembolso__c, Account.Name, MX_SB_MLT_TipodeGrua__c,MX_SB_MLT_Subtipo__c,MX_SB_MLT_DescripcionVehiculo__c, MX_SB_MLT_TipoSiniestro__c, MX_SB_MLT_Email__c,MX_SB_MLT_SuppliedName__c, MX_SB_MLT_Siniestro__c, MX_SB_MLT_TipoServicioAsignado__c,
                       MX_SB_MLT_EstatusServicio__c, MX_SB_MLT_Q_4Llantas__c, MX_SB_MLT_Q_Neutral__c, MX_SB_MLT_ServicioAsignado__c,MX_SB_MLT_UnidadEnviada__c,MX_SB_MLT_ClaimId__c,MX_SB_MLT_Reembolso__c,MX_SB_MLT_TotalReembolso__c
					   FROM WorkOrder WHERE MX_SB_MLT_Siniestro__c =: sinid and MX_SB_MLT_TipoSiniestro__c = 'Cambio de llanta' and MX_SB_MLT_EstatusServicio__c NOT in('Cancelado') limit 1 ];
        }
        return sinList.get(0);
    }
/*
    * @description Obtiene lo datos de la W order de Paso de corriente
    * @param String sinid
    * @return Object WorkOrder
    */
    public static WorkOrder getPasoCorrienteCase(String sinid) {
        List <WorkOrder> sinList = new List <WorkOrder> ();
        if (sinid != null) {
			sinList = [SELECT Id, MX_SB_MLT_ClaimId__c,AccountId,MX_SB_MLT_MotivoReembolso__c,Account.Name, MX_SB_MLT_TipodeGrua__c, MX_SB_MLT_Subtipo__c,MX_SB_MLT_DescripcionVehiculo__c,
                       MX_SB_MLT_TipoSiniestro__c, MX_SB_MLT_Email__c, MX_SB_MLT_Siniestro__c, MX_SB_MLT_TipoServicioAsignado__c, MX_SB_MLT_EstatusServicio__c, MX_SB_MLT_Q_4Llantas__c, MX_SB_MLT_Q_Neutral__c, MX_SB_MLT_ServicioAsignado__c,MX_SB_MLT_UnidadEnviada__c,MX_SB_MLT_SuppliedName__c,MX_SB_MLT_Reembolso__c,MX_SB_MLT_TotalReembolso__c
					   FROM WorkOrder WHERE MX_SB_MLT_Siniestro__c =: sinid and MX_SB_MLT_TipoSiniestro__c = 'Paso de corriente' and MX_SB_MLT_EstatusServicio__c NOT in('Cancelado')  limit 1 ];
		}
        return sinList.get(0);
    }
    /*
    * @description Obtiene lo datos de la W order de Gasolina
    * @param String sinid
    * @return Object WorkOrder
    */
    public static WorkOrder getGasolinaCase(String sinid) {
        List <WorkOrder> sinList = new List <WorkOrder> ();
        if (sinid != null) {
            sinList = [SELECT Id, MX_SB_MLT_ClaimId__c,AccountId,MX_SB_MLT_MotivoReembolso__c,Account.Name, MX_SB_MLT_TipodeGrua__c,MX_SB_MLT_Subtipo__c,MX_SB_MLT_DescripcionVehiculo__c,
                       MX_SB_MLT_TipoSiniestro__c, MX_SB_MLT_Email__c, MX_SB_MLT_Siniestro__c, MX_SB_MLT_TipoServicioAsignado__c, MX_SB_MLT_EstatusServicio__c, MX_SB_MLT_Q_4Llantas__c, MX_SB_MLT_Q_Neutral__c, MX_SB_MLT_ServicioAsignado__c,MX_SB_MLT_UnidadEnviada__c,MX_SB_MLT_SuppliedName__c ,MX_SB_MLT_Reembolso__c,MX_SB_MLT_TotalReembolso__c
					   FROM WorkOrder WHERE MX_SB_MLT_Siniestro__c =: sinid and MX_SB_MLT_TipoSiniestro__c = 'Gasolina' and MX_SB_MLT_EstatusServicio__c NOT in('Cancelado')  limit 1 ];
        }
        return sinList.get(0);
    }
/*
    * @description Obtiene lo datos del siniestro
    * @param String siniId
    * @return Object Sinieestro__c
    */
    public static Siniestro__c fetchSiniDetails(Id siniId) {
        List <Siniestro__c> lstOfStd = new List <Siniestro__c> ();
        if (siniId != null) {
            lstOfStd = [SELECT Id, TipoSiniestro__c, MX_SB_MLT_Email__c, MX_SB_MLT_CorreoElectronico__c, RecordTypeId, MX_SB_SAC_Contrato__c, MX_SB_MLT_URLLocation__c, 
                        		MX_SB_MLT_Address__c, MX_SB_MLT_LugarAtencionAuto__c, MX_SB_MLT_Condiciones_del_Vehiculo__c, MX_SB_MLT_CalleDestino__c, MX_SB_MLT_CodigoPostalDestino__c,
                        		MX_SB_MLT_ColoniaDestino__c, MX_SB_MLT_CoordenadasDestino__c, MX_SB_MLT_DireccionDestino__c, MX_SB_MLT_EntreCalles_Destino__c, MX_SB_MLT_EstadoDestino__c,
                        		MX_SB_MLT_NumExtDestino__c, MX_SB_MLT_Fecha_Hora_Siniestro__c, MX_SB_MLT_RefVisualesDestino__c FROM Siniestro__c WHERE Id =: siniId];
            }
        return lstOfStd.get(0);
        
        
    }
    /*
    * @description inserta  la W order de Gasolina
    * @param String strGasolina, String strId
    * @return 
    */
    public static void insertCaseGas(String strGasolina, String strId) {
        	WorkOrder[] casoupd = new WorkOrder[] {};
            final WrapperCaseGrua obj = (WrapperCaseGrua) JSON.deserialize(strGasolina, WrapperCaseGrua.class);
                casoupd = existeCaso(obj.defaultValue, strId, Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get(RTYPE).getRecordTypeId());
                if (casoupd.size() > 0) {
                        casoupd[0].MX_SB_MLT_Siniestro__c = strId;
                        casoupd[0].MX_SB_MLT_TipoSiniestro__c = obj.defaultValue;
                        casoupd[0].MX_SB_MLT_Email__c = obj.Email;
                        casoupd[0].MX_SB_MLT_DescripcionVehiculo__c = obj.CondicionesVeh;
                    update casoupd[0];
                } else {
                   final WorkOrder isGas = new WorkOrder();
                        isGas.MX_SB_MLT_Siniestro__c = strId;
                        isGas.MX_SB_MLT_TipoSiniestro__c = obj.defaultValue;
                        isGas.MX_SB_MLT_Email__c = obj.Email;
                        isGas.MX_SB_MLT_DescripcionVehiculo__c = obj.CondicionesVeh;
                        isGas.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get(RTYPE).getRecordTypeId();
					insert isGas;
				}
    }
    /*
    * @description inserta  la W order de Grua
    * @param String strGrua, String strId 
    * @return 
    */
	public static void insertCaseGrua(String strGrua, String strId) {
        	WorkOrder[] casoupd = new WorkOrder[] {};
				final WrapperCaseGrua obj = (WrapperCaseGrua) JSON.deserialize(strGrua, WrapperCaseGrua.class);
				casoupd = existeCaso(obj.defaultValue, strId, Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get(RTYPE).getRecordTypeId());
				if (casoupd.size() > 0) {
                        casoupd[0].MX_SB_MLT_Siniestro__c = strId;
                        casoupd[0].MX_SB_MLT_TipoSiniestro__c = obj.defaultValue;
                        casoupd[0].MX_SB_MLT_Email__c = obj.Email;
                        casoupd[0].MX_SB_MLT_DescripcionVehiculo__c = obj.CondicionesVeh;
                        casoupd[0].MX_SB_MLT_Subtipo__c = obj.defaultValueAve;
                        casoupd[0].MX_SB_MLT_TipodeGrua__c = obj.defaultValueTipoG;
                        casoupd[0].MX_SB_MLT_Q_4Llantas__c = Boolean.valueOf(obj.llantas);
                        casoupd[0].MX_SB_MLT_Q_Neutral__c = Boolean.valueOf(obj.neutral);
                    update casoupd[0];
                } else {
					final WorkOrder isgrua = new WorkOrder();
                        isgrua.MX_SB_MLT_Siniestro__c = strId;
                        isgrua.MX_SB_MLT_TipoSiniestro__c = obj.defaultValue;
                        isgrua.MX_SB_MLT_Email__c = obj.Email;
                        isgrua.MX_SB_MLT_DescripcionVehiculo__c = obj.CondicionesVeh;
                        isgrua.MX_SB_MLT_Subtipo__c = obj.defaultValueAve;
                        isgrua.MX_SB_MLT_TipodeGrua__c = obj.defaultValueTipoG;
                        isgrua.MX_SB_MLT_Q_4Llantas__c = Boolean.valueOf(obj.llantas);
                        isgrua.MX_SB_MLT_Q_Neutral__c = Boolean.valueOf(obj.neutral);
                        isgrua.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get(RTYPE).getRecordTypeId();
					insert isgrua;
                }
            
    }
    /*
    * @description inserta  la W order de Paso de corriente
    * @param String strId, String strPasoCorriente
    * @return 
    */
    public static void insertWOPasoC(String strId, String strPasoCorriente) {
    	WorkOrder[] casoupd = new WorkOrder[] {};
		final WrapperCaseGrua obj = (WrapperCaseGrua) JSON.deserialize(strPasoCorriente, WrapperCaseGrua.class);
                casoupd = existeCaso(obj.defaultValue, strId, Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get(RTYPE).getRecordTypeId());
                if (casoupd.size() > 0) {
                        casoupd[0].MX_SB_MLT_Siniestro__c = strId;
                        casoupd[0].MX_SB_MLT_TipoSiniestro__c = obj.defaultValue;
                        casoupd[0].MX_SB_MLT_Email__c = obj.Email;
                        casoupd[0].MX_SB_MLT_DescripcionVehiculo__c = obj.CondicionesVeh;
                    update casoupd[0];
                } else {
                    final WorkOrder isPasoC = new WorkOrder();
                        isPasoC.MX_SB_MLT_Siniestro__c = strId;
                        isPasoC.MX_SB_MLT_TipoSiniestro__c = obj.defaultValue;
                        isPasoC.MX_SB_MLT_Email__c = obj.Email;
                        isPasoC.MX_SB_MLT_DescripcionVehiculo__c = obj.CondicionesVeh;
                        isPasoC.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get(RTYPE).getRecordTypeId();
					insert isPasoC;
                }
    }
	/*
    * @description inserta  la W order de cambio de llanta
    * @param String strId, String strCambioLlanta
    * @return 
	*/
    public static void insertWOCambLl(String strId, String strCambioLlanta) {
    WorkOrder[] casoupd = new WorkOrder[] {};
		final WrapperCaseGrua obj = (WrapperCaseGrua) JSON.deserialize(strCambioLlanta, WrapperCaseGrua.class);
				casoupd = existeCaso(obj.defaultValue, strId, Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get(RTYPE).getRecordTypeId());
                if (casoupd.size() > 0) {
                        casoupd[0].MX_SB_MLT_Siniestro__c = strId;
                        casoupd[0].MX_SB_MLT_TipoSiniestro__c = obj.defaultValue;
                        casoupd[0].MX_SB_MLT_Email__c = obj.Email;
                        casoupd[0].MX_SB_MLT_DescripcionVehiculo__c = obj.CondicionesVeh;
                    update casoupd[0];
                } else {
                    final WorkOrder isCambio = new WorkOrder();
                        isCambio.MX_SB_MLT_Siniestro__c = strId;
                        isCambio.MX_SB_MLT_TipoSiniestro__c = obj.defaultValue;
                        isCambio.MX_SB_MLT_Email__c = obj.Email;
                        isCambio.MX_SB_MLT_DescripcionVehiculo__c = obj.CondicionesVeh;
                        isCambio.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get(RTYPE).getRecordTypeId();
					insert isCambio;
                }
    }
	/*
    * @description Valida si existe el Work Order
    * @param String tiposini, String siniestroid, String rectype
    * @return WorkOrder[]
	*/
     public static WorkOrder[] existeCaso(String tiposini, String siniestroid, String rectype) {
        return [SELECT id, MX_SB_MLT_Siniestro__c, MX_SB_MLT_TipoSiniestro__c, MX_SB_MLT_Email__c, MX_SB_MLT_DescripcionVehiculo__c, MX_SB_MLT_Subtipo__c, MX_SB_MLT_TipodeGrua__c, MX_SB_MLT_Q_4Llantas__c, MX_SB_MLT_Q_Neutral__c, CreatedDate FROM WorkOrder WHERE MX_SB_MLT_Siniestro__c =: siniestroid AND MX_SB_MLT_TipoSiniestro__c =: tiposini AND RecordtypeId =: rectype ORDER BY createddate DESC];
        
    }
}