/**
* @description       : Clase test que cubre a clase MX_SB_VTS_AddressInfo_Service
* @author            : Diego Olvera
* @group             : BBVA
* @last modified on  : 02-19-2021
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   11-01-2020   Diego Olvera   Initial Version
**/
@isTest
public class MX_SB_VTS_AddressInfo_S_Test {
    /**@description Nombre usuario*/
    private final static String ASESORCMBNAME = 'AsesorTest';
    /**@description Nombre de la cuenta*/
    private final static String ACCCMBNAME = 'TestCmb';
    /**@description Nombre de Oportunidad*/
    private final static String OPPCMBNAME = 'TestOppCmb';
    /**@description Etapa de la Oportunida*/
    private final static String ETAPACMBOPP = 'Contacto';
    /**@description Monto de la Oportunidad a actualiza*/
    private final static String MONTOVAL = '150000';
    
    @TestSetup
    static void makeData() {
        final User userRecCmb = MX_WB_TestData_cls.crearUsuario(ASESORCMBNAME, 'System Administrator');
        insert userRecCmb;
        final Account accntCmb = MX_WB_TestData_cls.createAccount(ACCCMBNAME, System.Label.MX_SB_VTS_PersonRecord);
        insert accntCmb;
        final Opportunity oppCmb = MX_WB_TestData_cls.crearOportunidad(OPPCMBNAME, accntCmb.Id, userRecCmb.Id, System.Label.MX_SB_VTA_VentaAsistida);
        insert oppCmb;
        final MX_RTL_MultiAddress__c nwAdd = new MX_RTL_MultiAddress__c();
        nwAdd.MX_RTL_MasterAccount__c = oppCmb.AccountId;
        nwAdd.MX_RTL_Opportunity__c = oppCmb.Id;
        nwAdd.Name = 'Direccion';
        insert nwAdd;
        final Quote quoteRec = new Quote(OpportunityId=oppCmb.Id, Name='Outbound');
        quoteRec.MX_SB_VTS_Folio_Cotizacion__c = '556432';
        insert quoteRec;
    }

    @isTest
    static void updCopp() {
        final User userUpdOpp = [Select Id from User where LastName =: ASESORCMBNAME];
        System.runAs(userUpdOpp) {
            final Opportunity oppLstUpd = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBNAME];
            final String etapa = ETAPACMBOPP;
            if(etapa == ETAPACMBOPP) {
                oppLstUpd.StageName = 'Contacto';
            } else {
                oppLstUpd.StageName = 'Cobro y resumen de contratación';
            }
            update oppLstUpd;
            Test.startTest();
            try {
                MX_SB_VTS_AddressInfo_Service.updCurrentOpp(oppLstUpd.Id, etapa);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Mistake');
                System.assertEquals('Error', extError.getMessage(),'No Actualizó la opp');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void getCopp() {
        final User userGetOpp = [Select Id from User where LastName =: ASESORCMBNAME];
        System.runAs(userGetOpp) {
            final Opportunity oppLstVal = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBNAME];
            Test.startTest();
            try {
                MX_SB_VTS_AddressInfo_Service.gtOppVals(oppLstVal.Id);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Failed');
                System.assertEquals('Failed', extError.getMessage(),'No obtuvo datos de la oportunidad');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void updAddress() {
        final User userOppty = [Select Id from User where LastName =: ASESORCMBNAME];
        System.runAs(userOppty) {
            final List<MX_RTL_MultiAddress__c> getAddVals = new List<MX_RTL_MultiAddress__c>();
            final MX_RTL_MultiAddress__c getAddVal = [Select Id, MX_RTL_Opportunity__c, Name FROM MX_RTL_MultiAddress__c WHERE Name = 'Direccion'];
            final Map<String, Object> dataMapS = new Map <String, Object>();
            dataMapS.put('checkedId', true);
            dataMapS.put('idAddrPro', getAddVal.Id);
            dataMapS.put('valPropio', 'Propio');
            dataMapS.put('codigoPostal', '52922');
            dataMapS.put('State', 'Gdl');
            dataMapS.put('Municipio', 'Zapopan');
            dataMapS.put('City', 'Mexico');
            dataMapS.put('Reference', 'Casa');
            dataMapS.put('valUso', 'Si');
            dataMapS.put('mtsBuild', '200');
            getAddVals.add(getAddVal);
            Test.startTest();
            try {
                MX_SB_VTS_AddressInfo_Service.updAddress(getAddVals, dataMapS);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Falla con error');
                System.assertEquals('Falla mistakes', extError.getMessage(),'No obtuvo datos');
            }
            Test.stopTest();
        }
        
    }

    @isTest
    static void crtAddress() {
        final User userGetOpp = [Select Id from User where LastName =: ASESORCMBNAME];
        System.runAs(userGetOpp) {
            final MX_RTL_MultiAddress__c getAddressVal = [Select Id, MX_RTL_Opportunity__c, Name FROM MX_RTL_MultiAddress__c WHERE Name = 'Direccion'];
            final Map<String, Object> dataMapCtrl = new Map <String, Object>();
            dataMapCtrl.put('checkedId', false);
            dataMapCtrl.put('idAddrPro', getAddressVal.Id);
            dataMapCtrl.put('valPropio', 'Propio');
            dataMapCtrl.put('codigoPostal', '52927');
            dataMapCtrl.put('State', 'Hidalgo');
            dataMapCtrl.put('Municipio', 'Ixmi');
            dataMapCtrl.put('City', 'Mexico');
            dataMapCtrl.put('Reference', 'Casa');
            dataMapCtrl.put('valUso', 'Si');
            dataMapCtrl.put('mtsBuild', '500');
            Test.startTest();
            try {
                MX_SB_VTS_AddressInfo_Service.createAddress(getAddressVal.MX_RTL_Opportunity__c, dataMapCtrl);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Horror');
                System.assertEquals('Fail', extError.getMessage(),'No obtuvo datos de la dirección');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void getDir() {
        final User userGetOpp = [Select Id from User where LastName =: ASESORCMBNAME];
        System.runAs(userGetOpp) {
            final MX_RTL_MultiAddress__c getAddressVal = [Select Id, MX_RTL_Opportunity__c, Name FROM MX_RTL_MultiAddress__c WHERE Name = 'Direccion'];
            Test.startTest();
            try {
                MX_SB_VTS_AddressInfo_Service.getValAddress(getAddressVal.Id, 'Domicilio cliente');
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal error');
                System.assertEquals('Fatal error', extError.getMessage(),'No obtuvo datos del domicilio del cliente');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void updQuo() {
        final User userGetOpp = [Select Id from User where LastName =: ASESORCMBNAME];
        System.runAs(userGetOpp) {
            final Opportunity oppLstUpd = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBNAME];
            final Map<String, Object> dataMapCtrl = new Map <String, Object>();
            dataMapCtrl.put('NombreCliente', 'Sasuke');
            dataMapCtrl.put('ApellidoPaterno', 'Uchiha');
            dataMapCtrl.put('ApellidoMaterno', 'Sharingan');
            dataMapCtrl.put('SexoHm', 'H');
            dataMapCtrl.put('FechaNac', '02/05/1977');
            dataMapCtrl.put('LugarNac', 'Sinaloa');
            dataMapCtrl.put('rfc', 'MEML8305');
            dataMapCtrl.put('Homoclave', '1H0');
            dataMapCtrl.put('TelefonoCel', '5530698745');
            dataMapCtrl.put('CorreoElect', 'sample@sample.com');
            dataMapCtrl.put('Curp', 'MEML8305DFMLRG01');
            Test.startTest();
            try {
                MX_SB_VTS_AddressInfo_Service.updQuoLine(oppLstUpd.Id, dataMapCtrl);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal failed');
                System.assertEquals('Fatal failed', extError.getMessage(),'No obtuvo datos');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void getCober() {
        final User userGetOpp = [Select Id from User where LastName =: ASESORCMBNAME];
        System.runAs(userGetOpp) {
             final Opportunity oppLstUpd = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBNAME];
            Test.startTest();
            try {
                MX_SB_VTS_AddressInfo_Service.fillCoberVals(oppLstUpd.Id);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal');
                System.assertEquals('Mistakes', extError.getMessage(),'No obtuvo datos de la Cobertura');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void updSyncQu() {
        final User userGetOpp = [Select Id from User where LastName =: ASESORCMBNAME];
        System.runAs(userGetOpp) {
            final Opportunity oppLstUpd = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBNAME];
            Test.startTest();
            try {
                MX_SB_VTS_AddressInfo_Service.updSyncQuo(oppLstUpd.Id, 'Outbound-Balanceada');
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal');
                System.assertEquals('Fatal mistake', extError.getMessage(),'No obtuvo datos de la Quote');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void udpateAmoTest() {
        final User userUpdOpp = [Select Id from User where LastName =: ASESORCMBNAME];
        System.runAs(userUpdOpp) {
            final Opportunity oppLstUpdM = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBNAME];
            final String monto = MONTOVAL;
            update oppLstUpdM;
            Test.startTest();
            try {
                MX_SB_VTS_AddressInfo_Service.udpateAmo(oppLstUpdM.Id, monto);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Mistake failed');
                System.assertEquals('Error fatal', extError.getMessage(),'No Actualizó la opp con el valor');
            }
            Test.stopTest();
        }
    }
}