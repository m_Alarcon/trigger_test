/**
 * @description       : Clase de cobertura que da soporte a MX_SB_VTS_GetSetDatosDCDPFrm_Ctrl
 * @author            : Alexandro Corzo
 * @group             : 
 * @last modified on  : 02-25-2021
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author              Modification
 * 1.0   18-02-2021   Alexandro Corzo     Initial Version
 * 1.1   24-02-2021   Diego Olvera        Se aumenta cobertura de con funciones
**/
@isTest
public class MX_SB_VTS_GetSetDatosDCDPFrm_Test {
    /** Variable de Apoyo: sLeadSource */
    Static String sLeadSource = 'Call me back';
    /** Variable de Apoyo: sProducto */
    Static String sProducto = 'Hogar seguro dinámico';
    /** Variable de Apoyo: sVenta */
    Static String sVenta = 'Venta';
    /** Variable de Apoyo: sNoEmpty */
    Static String sNoEmpty = 'Lista No Vacia';
    /** Variable de Apoyo: sUserName */
    Static String sUserName = 'UserOwnerTest01';
    /** Variable de Apoyo: sContratos */
    Static String sContratos = 'Contratos';
    /** Variables de Apoyo: sPersonAcct */
    Static String sPersonAcct = 'PersonAccount'; 
    /** Variable de Apoyo: sURL */
    Static String sUrl = 'http://www.example.com';
    /**Tipo de domicilio */
    public final static String DOMTYPE = 'Domicilio asegurado';

    /**
     * @description: función de preparación Clase de Prueba 
     * @author: 	 Alexandro Corzo
     */   
    @TestSetup
    static void makeData() {
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'createCustomerData', iaso__Url__c = sUrl, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'modifyHomePolicyTemp', iaso__Url__c = sUrl, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
    }

    /**
     * @description: Instrucción de Clase de Prueba para validar
     *               la clase Ctrl: FormDCDP
     * @author: 	 Alexandro Corzo
     */
    @isTest static void tstDataFormDCDP() {
        final User oUsr = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(oUsr) {
            final Map<String, String> mHeadersMock = new Map<String, String>();
            mHeadersMock.put('tsec','1029384765');
            final MX_WB_Mock objMockCallOut = new MX_WB_Mock(200, 'Complete', '', mHeadersMock);
            iaso.GBL_Mock.setMock(objMockCallOut);
            Test.startTest();
                final Object objDataTest = obtDataTestDCDP();
                final Map<String, Object> mDataTest = (Map<String, Object>) objDataTest;
                final Map<String, Object> oTest = MX_SB_VTS_GetSetDatosDCDPFrm_Ctrl.setDataFormDCDP(mDataTest);
                MX_SB_VTS_GetSetDatosDCDPFrm_Ctrl.setDataFormComSrv(mDataTest);
            Test.stopTest();
            System.assert(!oTest.isEmpty(), sNoEmpty);
        }
    }

    /**
     * @description: Instrucción de Clase de Prueba para validar
     *               la clase Ctrl: FormComSrv
     * @author: 	 Alexandro Corzo
     */
    @isTest static void tstDataFormComSrv() {
        final User oUsrCom = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(oUsrCom) {
            final Map<String, String> mHeadersM = new Map<String, String>();
            mHeadersM.put('tsec','1029384765');
            final MX_WB_Mock objMockCallO = new MX_WB_Mock(200, 'Complete', '', mHeadersM);
            iaso.GBL_Mock.setMock(objMockCallO);
            Test.startTest();
                final Object objDataT = obtDataTestDCDP();
                final Map<String, Object> mDataT = (Map<String, Object>) objDataT;
                final Map<String, Object> oTst = MX_SB_VTS_GetSetDatosDCDPFrm_Ctrl.setDataFormComSrv(mDataT);
            Test.stopTest();
            System.assert(!oTst.isEmpty(), sNoEmpty);
        }
    }

    /**
     * @description: Genera objeto para Datos Contratante
     *             : y Dirección de la Propiedad
     * @author: 	 Alexandro Corzo
     */
    public static Map<String, Object> obtDataTestDCDP() {
        final String strIdOppo = obtOppoId();
        final Map<Object, Object> mDataFrmDC = obtDataFrmDC(strIdOppo);
        final Map<Object, Object> mDataFrmDP = objDataFormDP();
        return new Map<String, Object>{
            'objDataFormDC' => mDataFrmDC,
            'objDataFormDP' => mDataFrmDP,
            'strOppoId' => strIdOppo,
            'isSuccess' => true
        };
    }

    /**
     * @description: Genera escenario para Datos Contratante
     * @author: 	 Alexandro Corzo
     */
    private static Map<Object, Object> obtDataFrmDC(String strIdOppo) {
        final String strIdQuote = obtQuoteId(strIdOppo);
        return new Map<Object, Object>{
            'ApellidoMaterno' => 'Leon',
            'ApellidoPaterno' => 'Corzo',
            'CorreoElect' => 'acorzo83@gmail.com',
            'Curp' => 'COLJ830119HCSRNS02',
            'FechaNac' => '19/01/1983',
            'Homoclave' => 'L81',
            'LugarNac' => 'Chiapas',
            'NombreCliente' => 'Jesus Alexandro',
            'OppoId' => strIdOppo,
            'QuoteId' => strIdQuote,
            'SexoFemenino' => 'false',
            'SexoMasculino' => 'true',
            'TelefonoCel' => '5566105237',
            'rfc' => '"COLJ830119"'
        };
    }

    /**
     * @description: Genera objeto para Dir. Contratante / Propiedad
     * @author: 	 Alexandro Corzo
     */
    private static Map<Object,Object> objDataFormDP() {
        final Map<Object, Object> mDirCont = obtDirCont();
        final Map<Object, Object> mDirProp = obtDirProp();
        return new Map<Object, Object>{
            'DirCont' => mDirCont,
            'DirProp' => mDirProp,
            'EnvFisPoliz' => true,
            'EnvTypeLbl' => 'Dirección de la Propiedad: Pagare, 254, , 26 A HUIPULCO, 14260, TLALPAN, CIUDAD DE MÉXICO, CIUDAD DE MEXICO',
            'EnvTypeVal' => 'dirpropiedad'
        };
    }

    /**
     * @description: Genera escenario para Dir. Contratante
     * @author: 	 Alexandro Corzo
     */
    private static Map<Object, Object> obtDirCont() {
        return new Map<Object, Object>{
            'DirContratante_Alcaldia' => 'TUXTLA GUTIÉRREZ',
            'DirContratante_CP' => '29080',
            'DirContratante_Calle' => 'Perdices',
            'DirContratante_CdP' => true,
            'DirContratante_CityId' => '04',
            'DirContratante_Ciudad' => 'TUXTLA GUTIÉRREZ',
            'DirContratante_ColoniaLbl' => 'BARR EL SABINITO',
            'DirContratante_ColoniaVal' => '1478',
            'DirContratante_CountryId' => '101',
            'DirContratante_Estado' => 'CHIAPAS',
            'DirContratante_NeighbId' => '2514',
            'DirContratante_NumExt' => 'Mz 1',
            'DirContratante_NumInt' => 'Lt 23',
            'DirContratante_StateId' => '07',
            'sameDir' => true    
        };
    }

    /**
     * @description: Genera escenario para Dir. Propiedad
     * @author: 	 Alexandro Corzo
     */
    private static Map<Object, Object> obtDirProp() {
        return new Map<Object, Object>{
            'DirPropiedad_Alcaldia' => 'CIUDAD DE MÉXICO',
            'DirPropiedad_CP' => '14260',
            'DirPropiedad_Calle' => 'Pagare',
            'DirPropiedad_CityId' => '01',
            'DirPropiedad_Ciudad' => 'TLALPAN',
            'DirPropiedad_ColoniaLbl' => '26 A HUIPULCO',
            'DirPropiedad_ColoniaVal' => '2862',
            'DirPropiedad_CountryId' => '012',
            'DirPropiedad_Estado' => 'CIUDAD DE MEXICO',
            'DirPropiedad_NeighbId' => '1424',
            'DirPropiedad_NumExt' => '254',
            'DirPropiedad_NumInt' => '',
            'DirPropiedad_StateId' => '09'
        };
    }

    /**
     * @description: Genera una Opportunity para clase de prueba
     * @author: 	 Alexandro Corzo
     */
    private static String obtOppoId() {
        final User oUsr = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        insert oUsr;
        final Account oAccTest = MX_WB_TestData_cls.crearCuenta(sContratos, sPersonAcct);
        oAccTest.PersonEmail = 'test@test.com';
        oAccTest.PersonMobilePhone = '5555555555';
        insert oAccTest;
        final Opportunity oppVal = MX_WB_TestData_cls.crearOportunidad('Opportunity Valida', oAccTest.Id, oUsr.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
        oppVal.Reason__c = sVenta;
        oppVal.Producto__c = System.Label.MX_SB_VTS_Hogar;
        oppVal.StageName = System.Label.MX_SB_VTS_COTIZACION_LBL;
        oppVal.TelefonoCliente__c = oAccTest.PersonMobilePhone;
        oppVal.LeadSource = System.Label.MX_SB_VTS_OrigenCallMeBack;
        insert oppVal;
        final MX_RTL_MultiAddress__c addressType = new MX_RTL_MultiAddress__c();
        addressType.MX_RTL_MasterAccount__c = oAccTest.Id;
        addressType.MX_RTL_AddressStreet__c = 'test';
        addressType.MX_RTL_AddressType__c = DOMTYPE;
        addressType.Name = 'Direccion Propiedad';
        addressType.MX_RTL_Opportunity__c = oppVal.Id;
        insert addressType;
        return oppVal.Id;
    }

    /**
     * @description: Genera una Quote para clase de prueba
     * @author: 	 Alexandro Corzo
     */
    private static String obtQuoteId(String strOpporId) {
        final Quote objQuote = MX_WB_TestData_cls.crearQuote(strOpporId, 'Quote Test', '1234567890');
        objQuote.MX_SB_VTS_ASO_FolioCot__c = '1234567890';
        insert objQuote;
        final Opportunity objOppo = new Opportunity();
        objOppo.Id = strOpporId;
        objOppo.SyncedQuoteId = objQuote.Id;
        update objOppo;
        return objQuote.Id;
    }
}