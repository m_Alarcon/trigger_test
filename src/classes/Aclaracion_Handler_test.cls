/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase test de aclaraciones - trigger, ctrl y selector

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     20/05/2020      Roberto Isaac Soto Granados           Creación Clase
*****************************************************************************************/
@isTest
private class Aclaracion_Handler_test {
    /*cuenta de pruebas*/
    private static Account testAcc = new Account();
    /*Aclaración de pruebas*/
    private static BPyP_Aclaraciones__c testAclara = new BPyP_Aclaraciones__c();
    /*Caso de pruebas*/
    private static Case testCase = new Case();
    /*Usuario de pruebas*/
    private static User testUser = new User();
    /*String para folios*/
    private static String testCode = '1234';

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testUser = UtilitysDataTest_tst.crearUsuario('testUser', 'BPyP Estandar', 'BPYP BANQUERO BANCA PERISUR');
        testUser.Title = 'Privado';
        insert testUser;

        System.runAs(testUser) {
            testAcc.OwnerId = testUser.Id;
            testAcc.FirstName = 'testAcc';
            testAcc.LastName = 'testAcc';
            testAcc.No_de_cliente__c = testCode;
            insert testAcc;
            testCase.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_EU_Case_Apoyo_General'].Id;
            testCase.Description = 'Description';
            testCase.MX_SB_SAC_Folio__c = testCode;
            testCase.Status = 'Nuevo';
            testCase.OwnerId = testUser.Id;
            insert testCase;
        }
    }

    /*Ejecuta la acción para cubrir methods de la clase*/
    static testMethod void getCasesTest() {
        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        testCase = [SELECT Id, MX_SB_SAC_Folio__c FROM Case WHERE MX_SB_SAC_Folio__c = '1234' LIMIT 1];
        System.runAs(testUser) {
            Test.startTest();
                testAclara.BPyP_Producto__c = 'TDC';
            	testAclara.BPyP_Tarjeta__c = '9876';
            	testAclara.BPyP_Folio__c = testCode;
            	testAclara.BPyP_NumeroCliente__c = testCode;
            	testAclara.BPyP_Caso__c = testCase.Id;
            	insert testAclara;
            Test.stopTest();
        }
        System.assert(!String.isBlank(testAclara.Id), 'Aclaración creada correctamente');
    }

}