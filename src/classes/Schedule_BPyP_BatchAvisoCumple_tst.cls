/*
    Proyecto : ENGMX
    Versión     Fecha           Autor                   Descripción
    ____________________________________________________________________________________________
    1.0         28-Ago-2018     Cristian Espinosa       Creación de la clase de pruebas para el schedule
														del batch BPyP_BatchAvisoCumple_cls.
    1.1         02-Ene-2020     Jair Ignacio Gonzalez   Se cambio el campo MX_BPyP_Proximo_Cumpleanios__c por
                                                        PersonContact.MX_NextBirthday__c por la reingenieria de person account
*/
@isTest
public class Schedule_BPyP_BatchAvisoCumple_tst {

    @isTest
    static void testMethod001() {
        Test.startTest();
      	final String jobId = System.schedule('testBatchEjecutivos','0 0 0 3 9 ? 2022',  new Schedule_BPyP_BatchAvisoCumple_cls());
    	final CronTrigger cront = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(0, cront.TimesTriggered,'Sin ejecuciones');
        Test.stopTest();
    }
}