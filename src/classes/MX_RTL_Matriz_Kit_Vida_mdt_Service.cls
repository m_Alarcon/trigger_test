/**
 * @File Name          : MX_RTL_Matriz_Kit_Vida_mdt_Service.cls
 * @Description        :
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 5/26/2020, 05:43:34 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/26/2020   Juan Carlos Benitez     Initial Version
**/
public with sharing class MX_RTL_Matriz_Kit_Vida_mdt_Service {
	/** constructor */
    @TestVisible
    private MX_RTL_Matriz_Kit_Vida_mdt_Service() { }
    /** List of records to be returned */   
    final static List<MX_SB_MLT_Matriz_Kit_Vida__mdt> RETURNMATRIZKIT = new List<MX_SB_MLT_Matriz_Kit_Vida__mdt>();
    /**
     * 
    * @description
    * @author Juan Carlos Benitez | 5/26/2020
    * @param TaskFields
    * @param Ids
    * @param rtDevName
    * @return List<Task>
    **/
    public static List<MX_SB_MLT_Matriz_Kit_Vida__mdt> getSinHData(Map<String,String> mapa) {
        	return MX_RTL_Matriz_Kit_Vida_mdt_Selector.getKitDataById(mapa);
    }
}