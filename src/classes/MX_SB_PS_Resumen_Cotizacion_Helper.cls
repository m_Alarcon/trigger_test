/**
 * @Methods      1
 * @description       : 
 * @author            : Arsenio.perez.lopez.contractor@bbva.com
 * @group             : 
 * @last modified on  : 02-09-2021
 * @last modified by  : Daniel Perez Lopez
 * Modifications Log 
 * Ver   Date         Author                                    Modification
 * 1.0   11-11-2020   Arsenio.perez.lopez.contractor@bbva.com   Initial Version
 * 1.1   12-15-2020   Daniel Perez Lopez                        Method calculateAge y createCustomWrp
 * 1.2   02-09-2021   Juan Carlos Benitez                       Se remueve . en ProductId y � en subject
 * 1.3   02-24-2021   Juan Carlos Benitez                       Se cambia valor erroneo asignacion producto__c
 * 1.4   03-11-2021   Vincent Juárez                            Se agrega respuesta de Openpay
 * 1.5   03-10-2021   Juan Carlos Benitez                       Se remueve statusCode 400 en evaluacion de servicio
 * 1.6   03-17-2021   Juan Carlos Benitez                       Se añade datacustomer.shippingChannel.id
 * 1.7   03-17-2021   Vincent Juárez                            Se agregan campos de quote para envío de correo
**/
@SuppressWarnings('sf:UseSingleton,sf:DUDataflowAnomalyAnalysis')
public with sharing class MX_SB_PS_Resumen_Cotizacion_Helper {
    /** */
    final static String FIELDCLONE ='id,name,StageName,AccountId, RecordTypeId, Plan__c ';
    /*variable compara response*/
    public static FINAL Integer RESPCODE = 200;
    /**formto fecha */
    public static FINAL String FORMATO = 'yyyy-MM-dd';
    /**Formato salida */
    public static FINAL Integer TWO = 2;
    /** Dato para validacion estatuscode */
    public static FINAL Integer OK204 = 204;
    /** Dato para validacion estatuscode */
    public static FINAL Integer OK400 = 400;
    
    /**
    * @description 
    * @author Arsenio.perez.lopez.contractor@bbva.com | 11-13-2020 
    * @param String oppid 
    * @param String prodName 
    **/
    public static String sCloneOpps(String oppid,String prodName) {
        final Set<Id> oppidList = new Set<Id>();
        String returnid = null;
        oppidList.add(oppid);
        final List<Opportunity> oppsClone = MX_RTL_Opportunity_Selector.clonOpp(oppidList, FIELDCLONE, false, true, false, false);
        for(Opportunity opp: oppsClone) {
            opp.CloseDate = Date.today();
            opp.StageName = System.Label.MX_SB_PS_Etapa1;
            opp.Producto__c = MX_RTL_Product2_Selector.resultQueryProd('Id,Name', 'Name=\''+prodName+'\'', true)[0].Name;
        }
        if(!oppsClone.isEmpty()) {
            returnid= MX_RTL_Opportunity_Selector.insertOpportunity(oppsClone)[0].Id;
        }
        return returnid;
    }

    /**
    * @description 
    * @author Gerardo Mendoza Aguilar | 12-01-2020 
    * @param dateParam 
    * @return Integer 
    **/
    public static Integer calculateAge(Date dateParam) {
        Final Date currentDate = Date.today();
        Integer age = 0;
        if(dateParam.month() < currentDate.month() 
        || (dateParam.month() == currentDate.month() && dateParam.day()<=currentDate.day()) 
        || (dateParam.month() > currentDate.month() && dateParam.year() < currentDate.year())) {
            age = currentDate.year() - dateParam.year();
        } else {
            age = (currentDate.year() - dateParam.year()) -1;
        }
        return age;
    }

    /**
    * @description 
    * @author Daniel Perez Lopez | 12-10-2020 
    * @param oprtdata 
    * @param cntc 
    * @param mapdata 
    * @param beneficiario 
    * @return Map<String, String> 
    **/
    public static Map<String,String> createCustomWrp(Opportunity oprtdata, Contact cntc, Map<String,String> mapdata, MX_SB_VTS_Beneficiario__c beneficiario) {
            Map<String,String>  response = new Map<String,String>();
            final Set<id> ids = new Set<id>();
            ids.add(oprtdata.SyncedQuoteId);
            final MX_SB_PS_wrpCreateCustomerData datacustomer = new MX_SB_PS_wrpCreateCustomerData();
            datacustomer.quote.idQuote=mapdata.get('idcotiza');
            datacustomer.holder.id=mapdata.get('idaseg');
            datacustomer.holder.email=cntc.Email;
            datacustomer.holder.rfc=mapdata.get('rfc');
            final MX_SB_PS_wrpCreateCustomerData.mainAddress mainad = MX_SB_PS_Datos_AseguradoExt_Ctrl.dirwraper(cntc,mapdata);
            datacustomer.holder.mainAddress=mainad;
            datacustomer.holder.legalAddress=mainad;
            final MX_SB_PS_wrpCreateCustomerData.mainAddress mainadcorresp = MX_SB_PS_Datos_AseguradoExt_Ctrl.correspdirwraper(oprtdata,mainad);
            datacustomer.holder.correspondenceAddress=mainadcorresp;
            datacustomer.holder.mainContactData= new MX_SB_PS_wrpCreateCustomerData.mainContactData(cntc.MX_WB_ph_Telefono1__c,cntc.MX_WB_ph_Telefono1__c,cntc.MX_WB_ph_Telefono1__c);
            datacustomer.holder.correspondenceContactData= new MX_SB_PS_wrpCreateCustomerData.mainContactData(cntc.MX_WB_ph_Telefono1__c,cntc.MX_WB_ph_Telefono1__c,cntc.MX_WB_ph_Telefono1__c);
            datacustomer.holder.fiscalContactData= new MX_SB_PS_wrpCreateCustomerData.mainContactData(cntc.MX_WB_ph_Telefono1__c,cntc.MX_WB_ph_Telefono1__c,cntc.MX_WB_ph_Telefono1__c);
            datacustomer.holder.physicalPersonalityData=ficalperson(beneficiario,oprtdata);
            final Datetime datm =System.now();
            final Date bdt = beneficiario.MX_SB_SAC_FechaNacimiento__c;
            final String dateform =DateTime.newInstance(bdt.year(),bdt.month(),bdt.day()).format(FORMATO) +' '+datm.hour()+':'+datm.minute()+':'+datm.second();
            datacustomer.holder.deliveryInformation.referenceStreets=mapdata.get('calle');
            datacustomer.holder.deliveryInformation.deliveryTimeStart=dateform;
            datacustomer.holder.deliveryInformation.deliveryTimeEnd=dateform;
            datacustomer.header.dateRequest=''+System.Now().format(FORMATO)+' '+System.now().format('hh:mm:ss.SSS');
            datacustomer.header.dateConsumerInvocation=''+System.Now().format(FORMATO)+' '+System.now().format('hh:mm:ss'+'.1');
            datacustomer.shippingChannel.id=mapdata.get('emailQ');
            HttpResponse resp = new HttpResponse();
            final Map<String,String> jsonData= new Map<String,String>();
            jsonData.put('createCD',JSON.serialize(datacustomer));
            try {
                resp = MX_SB_PS_IASO_Service_cls.getServiceResponseMap('createCustomerData_PS',jsonData);
                    if(resp.getStatusCode()==OK204 ) {
                        response = quoteBeneficiary(mapdata.get('idcotiza'),mapdata.get('idaseg'),beneficiario);
                    } else {
                        throw new AuraHandledException(System.Label.MX_SB_PS_ErrorGenerico);
                    }
            } catch (Exception exp) {
                throw new AuraHandledException(System.Label.MX_SB_PS_ErrorGenerico+ ' ' + exp);
            }
            return response;
    }
    /**
    * @description 
    * @author Daniel.perez.lopez.contractor@bbva.com | 11-25-2020 
    * @param Contact cntc 
    * @param Opportunity oprtdata 
    * @return MX_SB_PS_wrpCreateCustomerData.physicalPersonalityData 
    **/
    public static MX_SB_PS_wrpCreateCustomerData.physicalPersonalityData ficalperson(MX_SB_VTS_Beneficiario__c cntc, Opportunity oprtdata) {
        final MX_SB_PS_wrpCreateCustomerData.physicalPersonalityData fiscpers = new MX_SB_PS_wrpCreateCustomerData.physicalPersonalityData();
        fiscpers.name=cntc.Name;
        fiscpers.lastName=cntc.MX_SB_VTS_APaterno_Beneficiario__c;
        final Date bdt = cntc.MX_SB_SAC_FechaNacimiento__c;
        final String dateform=DateTime.newInstance(bdt.year(),bdt.month(),bdt.day()).format(FORMATO);
        fiscpers.birthDate=dateform;
        final String genero =cntc.MX_SB_SAC_Genero__c=='Hombre'?'MASCULINO':'FEMENINO';
        fiscpers.sex.catalogItemBase.id=genero.Substring(0,1);
        fiscpers.sex.catalogItemBase.name=genero;
        fiscpers.mothersLastName=cntc.MX_SB_VTS_AMaterno_Beneficiario__c;
        fiscpers.curp=cntc.MX_SB_PS_CURP__c;
        fiscpers.civilStatus.catalogItemBase.id='S';
        fiscpers.civilStatus.catalogItemBase.name='SOLTERO';
        return fiscpers;
    }   
    /**
    * @description 
    * @author Daniel.perez.lopez.contractor@bbva.com | 11-25-2020 
    * @param String idquote 
    * @param String idinsured 
    * @param MX_SB_VTS_Beneficiario__c opport 
    * @return string 
    **/
    public static Map<String,String> quoteBeneficiary(String idquote, String idinsured, MX_SB_VTS_Beneficiario__c opport) {
        Map<String,String>  response=new Map<String,String>();
        final Datetime dtime = System.now();
        final MX_SB_PS_WrpCreateQuoteBeneficiary datacustomer = new MX_SB_PS_WrpCreateQuoteBeneficiary();
        datacustomer.id=idquote;
        final MX_SB_PS_WrpCreateQuoteBeneficiary.insured[] listains = new MX_SB_PS_WrpCreateQuoteBeneficiary.insured[]{};
        final MX_SB_PS_WrpCreateQuoteBeneficiary.insured ins = new MX_SB_PS_WrpCreateQuoteBeneficiary.insured();
        ins.id=idinsured;
        ins.entryDate=DateTime.newInstance(dtime.year(),dtime.month(),dtime.day()).format(FORMATO);
        final MX_SB_PS_WrpCreateQuoteBeneficiary.beneficiary[] benefs = new MX_SB_PS_WrpCreateQuoteBeneficiary.beneficiary[]{};
                final MX_SB_PS_WrpCreateQuoteBeneficiary.beneficiary bene = new MX_SB_PS_WrpCreateQuoteBeneficiary.beneficiary();
                bene.person.lastName=opport.MX_SB_VTS_APaterno_Beneficiario__c;
                bene.person.secondLastName=opport.MX_SB_VTS_AMaterno_Beneficiario__c;
                bene.person.firstName=opport.Name;
                bene.relationship.id='0'+MX_SB_PS_RetrieveDataExt_Ctrl.getCodes(opport.MX_SB_VTS_Parentesco__c,'parentesco');
                bene.percentageParticipation='100';
                bene.beneficiaryType.id='01';
                benefs.add(bene);
        ins.beneficiaries=benefs;
        listains.add(ins);
        datacustomer.insuredList=listains;
        final MX_SB_PS_WrpCreateQuoteBeneficiary.technicalInformation techinfo = new MX_SB_PS_WrpCreateQuoteBeneficiary.technicalInformation();
        datacustomer.technicalInformation= techinfo ;
        datacustomer.technicalInformation.dateRequest=''+System.Now().format(FORMATO)+' '+System.now().format('hh:mm:ss.SSS');
        datacustomer.technicalInformation.dateConsumerInvocation=''+System.Now().format(FORMATO)+' '+System.now().format('hh:mm:ss'+'.1');
        final Map<String,String> jsonData= new Map<String,String>();
        jsonData.put('quotebeneficiary',JSON.serialize(datacustomer));
        response = jsonData;
        return response;
    }
    /**
    * @description 
    * @author Daniel.perez.lopez.contractor@bbva.com | 11-25-2020 
    * @param MX_SB_PS_Resumen_Cotizacion_Ctrl.wrpDocuments mapdata2 
    * @param String oppid 
    * @param String idpoliza 
    * @return Map<String,String>
    **/
    public static Map<String,String> mapdata(MX_SB_PS_Resumen_Cotizacion_Ctrl.wrpDocuments mapdata2, String oppid,String idpoliza) {
        final Opportunity opt = MX_SB_PS_Utilities.fetchOpp(oppId);
        Quote quote = MX_RTL_Quote_Selector.getQuote(new Quote(OpportunityId=oppid))[0];
        quote = MX_RTL_Quote_Selector.findQuoteById(quote.id, ' id,AccountId,Account.Name,Account.PersonContact.Email,Account.RFC__c,Account.Personemail,MX_SB_VTS_TotalMonth__c,MX_SB_PS_PagosSubsecuentes__c,MX_SB_VTS_ASO_FolioCot__c, QuoteToCity, QuoteToCountry, AdditionalCity, AdditionalState ');
        final List<QuoteLineItem> listqtl = MX_RTL_QuoteLineItem_Selector.getQuoteLineitemBy('OpportunityId','id ,UnitPrice,createddate,MX_SB_VTS_Tipo_Plan__c,MX_SB_PS_Dias3a5__c,MX_SB_PS_Dias6a35__c,MX_SB_PS_Dias36omas__c,MX_SB_VTS_Total_Gastos_Funerarios__c,MX_SB_VTS_FormaPAgo__c,MX_WB_noPoliza__c ,MX_WB_Folio_Cotizacion__c ',oppid);
        final Map<String,String> mpd = new Map<String,String>();
        mpd.put('mailreciver',quote.Account.PersonContact.Email);
        mpd.put('sFRECUENCIA_PAGO1',''+listqtl[0].MX_SB_VTS_FormaPAgo__c);
        mpd.put('sFRECUENCIA_PAGO2','');
        mpd.put('sTARJETA',quote.QuoteToCity);
        mpd.put('sPLAN',listqtl[0].MX_SB_VTS_Tipo_Plan__c);
        mpd.put('sCANTIDAD1','$ '+listqtl[0].MX_SB_PS_Dias3a5__c);
        mpd.put('sCANTIDAD2','$ '+listqtl[0].MX_SB_PS_Dias6a35__c);
        mpd.put('sCANTIDAD3','$ '+listqtl[0].MX_SB_PS_Dias36omas__c);
        mpd.put('sCANTIDAD4','$ '+listqtl[0].MX_SB_VTS_Total_Gastos_Funerarios__c);
        mpd.put('sNUMERO_POLIZA',idpoliza);
        mpd.put('sCOBRO','$ '+listqtl[0].UnitPrice);
        mpd.put('sFOLIO',quote.AdditionalCity);
        mpd.put('sFECHA',quote.AdditionalState);
        mpd.put('sHORA',''+quote.QuoteToCountry);
        mpd.put('productId','RS_BIENVENIDA');
        mpd.put('RFC',''+opt.Account.RFC__c);
        mpd.put('subject','Ya cuentas con Respaldo Seguro para Hospitalización');
        mpd.put('sNombre',opt.Account.Name);
        mpd.put('listhref',''+mapdata2.href);
        return mpd;
    }
}