/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 10-08-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   09-09-2020   Eduardo Hernández Cuamatzi   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_PC_TransferComunication_Service {
    /**Recupera Url callcenter para un user */
    public static String findUrlCallCenter(String userId) {
        String urlCallCenter = '';
        final List<User> userRecord = MX_RTL_User_Selector.getListUserById(new Set<Id>{userId});
        if(userRecord.isEmpty() == false && String.isNotBlank(userRecord[0].CallCenterId)) {
            final List<CallCenter> callCenterRecord = MX_SB_RTL_CallCenter_Selector.findCallCenter(new Set<string>{userRecord[0].CallCenterId});
            if(callCenterRecord.isEmpty() == false && String.isNotBlank(callCenterRecord[0].AdapterUrl)) {
                urlCallCenter = callCenterRecord[0].AdapterUrl;
            }
        }
        return urlCallCenter;
    }
}