/**
 * @File Name          : MX_SB_VTS_SendTrackingTelemarketing_cls.cls
 * @Description        :
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              :
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 10-01-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    26/9/2019   Eduardo Hernández Cuamatzi     Initial Version
 * 1.1    03/01/2020  Eduardo Hernández Cuamatzi     envió de cupones TW
 * 1.1.1 21/01/2020   Eduardo Hernández Cuamatzi     Fix hora de filtro y oportunidades cerradas
 * 1.1.2 28/01/2020   Eduardo Hernández Cuamatzi     Fix hora desfase de horas en marcado
 * 1.1.3 24/02/2020   Eduardo Hernández Cuamatzi     Fix registros duplicados
**/
global without sharing class MX_SB_VTS_SendTrackingTelemarketing_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful { //NOSONAR
    /**variable query*/
    public string finalQuery {get;set;}
    /**variable minuto*/
	public Integer minutosCall {get;set;}
    /**variable random creada*/
    public final static Integer RANDOMNUM = 1;
    /**variable ERROR creada*/
    public final static Integer ISERRORPOST = 0;
    /**variable ERROR creada*/
    public final static String FORMATTIME = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
    /**Zona Horaria de org*/
    private final static String TIMEZONEORG = [Select Id, TimeZoneSidKey from User where Id =: UserInfo.getUserId()].TimeZoneSidKey;

    /**
     * MX_SB_VTS_SendTrackingTelemarketing_cls constructor
     * @param  finalQuery  query a ejecutar
     * @param  minutosCall tiempo hacia atras tracking web
     */
    global MX_SB_VTS_SendTrackingTelemarketing_cls(String finalQuery, Integer minutosCall) {
        this.finalQuery = finalQuery;
        this.minutosCall = minutosCall;
    }

    /**
     * start Inicio de batch
     * @param  batchContext contexto batch
     * @return ejecución de base
     */
    global Database.querylocator start(Database.BatchableContext batchContext) {
        final Datetime dateSend = Datetime.now();
        final String tNow = addMinutesTime(dateSend, FORMATTIME, -(minutosCall+10));
        final String tCreada = addMinutesTime(dateSend, FORMATTIME, -(minutosCall-5)).replace(' ', '');
        this.finalQuery += ' And (Opportunity.LastModifiedDate >= ' + tNow+ ')';
        this.finalQuery += ' And (Opportunity.LastModifiedDate <= '+ tCreada +')';        
        return Database.getQueryLocator(this.finalQuery);
    }

    /**
     * execute Ejecución de batch
     * @param  batchContext contexto batch
     * @param  scopeQuotes  Resultado de ejecución base
     */
    global void execute(Database.BatchableContext batchContext, List<Opportunity> scopeQuotes) {
        final Map<String, EmailNoValidosCotiza__c> mapENoValCot = EmailNoValidosCotiza__c.getAll();
        final Map<String, Opportunity> mapSend = new Map<String, Opportunity>();
        Final Map<String,String> folioOpp = new Map<String,String>();
        for (Opportunity objQuoteSend : scopeQuotes) {
            if (!mapENoValCot.containsKey(objQuoteSend.Account.PersonEmail)) {
                mapSend.put(objQuoteSend.Id, objQuoteSend);
                folioOpp.put(objQuoteSend.FolioCotizacion__c, objQuoteSend.Id);
            }
        }
        final List<String> response = MX_SB_VTS_SendTrackingTelemarketing_Ser.encripCots(scopeQuotes);
        validateFolioCot(mapSend, response,folioOpp);
    }

    /**
     * finish ejecución al finalizar
     * @param  batchContext contexto batch
     */
    global void finish(Database.BatchableContext batchContext) { //NOSONAR
    }

    /**
     * sendOppsSmart envio a SmartCenter
     * @param  proveedor  proveedor Smart
     * @param  mapProveer Mapa de registros por proveedor
     * @param  lstTrays   Mapa de bandejas hotleads
     * @return            lista de Oportunidades enviadas
     */
    public static List<Opportunity> validateFolioCot(Map<String, Opportunity> scopeQuotes, List<String> response, Map<String,String> folioOpp) {
        final List<SObject> lstOpps = new List<SObject>();
        final Map<String,Map<String,Object>> responseQuote = MX_SB_VTS_SendTrackingTelemarketing_Ser.findQuoteStatus(response,folioOpp);
        final List<Contract> lstContract = new List<Contract>();
        for(String oppId : responseQuote.keySet()) {
            final Opportunity opps = scopeQuotes.get(oppId);
            if(responseQuote.containsKey(oppId)) {
                final Map<String,Object> mapPolize = responseQuote.get(opps.Id);
                Final Map<String,Object> mapTemp = (Map<String,Object>)mapPolize.get('data');
                if(String.isNotBlank(String.valueOf(mapTemp.get('number')))) {
                    lstContract.add(MX_SB_VTS_SendTrackingTelemarketing_Ser.fillContract(responseQuote, opps));
                    opps.MX_SB_VTS_Aplica_Cierre__c = true;
                    opps.StageName = 'Closed won';
                } else {
                    opps.StageName = 'Contacto';
                    opps.EnviarCTI__c = true;
                }
                lstOpps.add(opps); 
            }
        }
        if(lstContract.isEmpty() == false) {
            MX_RTL_Contract_Selector.insertContracts(lstContract);
        }
        update(lstOpps);
        return lstOpps;
    }

    /**
     * findLocalTime recupera hora local
     * @param  formatTime formato de fecha
     * @return            fecha formateada
     */ 
    public static String findLocalTime (String formatTime) {
        final Datetime timeNow = Datetime.now();
        final String timeZone = [Select Id, TimeZoneSidKey from User where Id =: UserInfo.getUserId()].TimeZoneSidKey;
        return timeNow.addMinutes(Integer.valueOf(System.Label.MX_SB_VTS_SmartCallMinutes)).format(formatTime,timeZone);
    }

    /**
     * addMinutesTime agrega minutos a hora local
     * @param  timeLocal tiempo local
     * @param  format    formato de hora
     * @param  minutes   minutos agregados
     * @return           fecha formateada
     */
    public static String addMinutesTime(DateTime timeLocal, String format, Integer minutes) {
        final DateTime timeUpdate = timeLocal.addMinutes(minutes);
        return timeUpdate.formatGmt(format);
    }

    /**
    * @description valida cupones para marcado
    * @author Eduardo Hernández Cuamatzi | 3/1/2020 
    * @param timeUser zona horaria del usuario
    * @param var_cupon código de cupón
    * @return regresa la fecha alida de marcado
    **/
    public static string validaCuponesreq(String timeUser,String varcupon, List<String> varListName) {
        String vartimRetur = timeUser;
        if(varListName.isEmpty() == false && varListName.contains(varcupon)) {
            final Datetime DateTemporal = Datetime.parse(timeUser).addDays(1);
            final Datetime dateCalls = Datetime.newInstance(DateTemporal.year(), DateTemporal.month(), DateTemporal.day(), 11, 0, 0);
            vartimRetur = dateCalls.format('',TIMEZONEORG);
        }
        return vartimRetur;
    }
}