/**
* @File Name          : MX_BPP_LeadCampaign_Service.cls
* @Description        : Clase para el manejo de datos BPyP en el proceso de candidatos de campaña
* @author             : Gabriel Garcia Rojas
* @Group              : BPyP
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 01/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      01/06/2020           Gabriel Garcia Rojas              Initial Version
**/
public without sharing class MX_BPP_LeadCampaign_Service {

    /** Constructor*/
    @testVisible
    private MX_BPP_LeadCampaign_Service() { }

    /**
    * @Method assignOwnerByNumberAccount
    * @param newList
    * @Description Asignación de Cuenta y Propietario a través del numero de cuenta
    * @return void
    **/
    public static void assignOwnerByNumberAccount(List<Lead> newList) {
        final Map<Id, RecordType> mapRecordType = MX_RTL_RecordType_Selector.getMapRecordType('Lead', 'MX_BPP_Leads');
        final Set<String> idExternalCuentas = MX_BPP_LeadCampaign_Helper.getLeadWithNodeCliente(newList, mapRecordType);

        if(!idExternalCuentas.isEmpty()) {
            final List<Account> listCuentas = MX_RTL_Account_Selector.cuentasPorNoDeCliente(idExternalCuentas);
            final Map<String, Account> mapCuentas = new Map<String, Account>();
            for(Account cuenta : listCuentas) {
                mapCuentas.put(cuenta.No_de_cliente__c, cuenta);
            }
            MX_BPP_LeadCampaign_Helper.assigendAccountToLead(newList, mapCuentas, mapRecordType);
        }
    }
}