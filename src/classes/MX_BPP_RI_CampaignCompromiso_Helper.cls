/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_RI_CampaignCompromiso_Helper
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-06-16
* @Description 	Helper for MX_BPP_RI_CampaignCompromiso Service Layer
* @Changes
*
*/
public with sharing class MX_BPP_RI_CampaignCompromiso_Helper {

    /*Contructor MX_BPP_RI_CampaignCompromiso_Helper */
    @testVisible
    private MX_BPP_RI_CampaignCompromiso_Helper() {}

    /**
    * @Description 	Lista de Campañas de lead del propietario de la RI
    * @Param		String[] {idRI, ownerId, fechaInicio, fechaFin, campoPadre, campoHijo}
    * @Return 		OpportunityWrapper CampaignMember-Lead List
    **/
    public static List<EU001_cls_CompHandler.OpportunityWrapper> getLeadToCompList (String[] leadParams) {
        final List<EU001_cls_CompHandler.OpportunityWrapper> oppWrapperList = new List<EU001_cls_CompHandler.OpportunityWrapper>();
        String auxcampoPadre = '';
        String auxcampoHijo = '';
        String queryfields;
        final String fechaFinRango = ' AND MX_LeadEndDate__c >= ' + leadParams[2] + ' AND MX_LeadEndDate__c <= ' + leadParams[3];
        if(!String.isBlank(leadParams[4])) {
            auxcampoPadre = ' AND Campaign.MX_WB_FamiliaProductos__r.Name = \'' + leadParams[4] + '\'';
        }
        if(!String.isBlank(leadParams[5])) {
            auxcampoHijo = ' AND Campaign.MX_WB_Producto__r.Name = \'' + leadParams[5] + '\'';
        }

        queryfields = 'Id, LeadId, Lead.Name, Lead.Status, Lead.RecordTypeId, Lead.MX_LeadAmount__c, Lead.MX_WB_RCuenta__r.Name, MX_LeadEndDate__c,  Campaign.MX_WB_FamiliaProductos__r.Name, Campaign.MX_WB_Producto__r.Name';
        final String queryFilters = ' WHERE Lead.IsConverted = false AND Lead.OwnerId = \'' + leadParams[1] + '\'' + fechaFinRango + auxcampoPadre + auxcampoHijo;
        final List<CampaignMember> listCampMem = MX_RTL_CampaignMember_Selector.getListCampaignMemberByDatabase(queryfields, queryFilters);
        final Map<Id, RecordType> recordTypeMap = MX_RTL_RecordType_Selector.getMapRecordType('Lead', null);
        for(CampaignMember candidatoM : listCampMem) {
            final EU001_cls_CompHandler.OpportunityWrapper newOpp = MX_BPP_RI_CampaignCompromiso_Helper.getLeadToAdd(candidatoM, recordTypeMap);
            oppWrapperList.add(newOpp);
        }
        return oppWrapperList;
    }

    /**
	* --------------------------------------------------------------------------------------
	* @Author       Gabriel Garcia
	* Date          14-05-2020
	* @Description  Obtain the leads to add for the class .
	* @Comments     Created from the clipped code block of the method getLeadToComp.
	* @param        Lead candidato, Map<Id, RecordType> recordTypeMap
	* @return       OpportunityWrapper The Lead to be added to the list.
	* @example      public static OpportunityWrapper getLeadToAdd(Lead candidato, Map<Id, RecordType> recordTypeMap) {
	**/
    public static EU001_cls_CompHandler.OpportunityWrapper getLeadToAdd(CampaignMember campaignM, Map<Id, RecordType> recordTypeMap) {
        final EU001_cls_CompHandler.OpportunityWrapper newOpp = new EU001_cls_CompHandler.OpportunityWrapper();
        final RecordType recordType = recordTypeMap.get(campaignM.Lead.RecordTypeId);
        newOpp.seleccion = false;
        newOpp.cierre = false;
        newOpp.oppId = campaignM.LeadId;
        newOpp.oppName = campaignM.Lead.Name;
        newOpp.Etapa = campaignM.Lead.Status;
        newOpp.oppCloseDate = campaignM.MX_LeadEndDate__c;
        newOpp.nombCliente = campaignM.Lead.MX_WB_RCuenta__r.Name;
        newOpp.MX_RTL_Familia = campaignM.Campaign.MX_WB_FamiliaProductos__r.Name;
        newOpp.MX_RTL_Producto = campaignM.Campaign.MX_WB_Producto__r.Name;
        newOpp.recordTypeId = campaignM.Lead.recordTypeId;
        newOpp.OppAmount = campaignM.Lead.MX_LeadAmount__c;
        newOpp.recordTypeDevName = recordType.DeveloperName;
        return newOpp;
    }
}