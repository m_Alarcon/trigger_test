/**
* Name: MX_SB_VTS_UpNameCampaign_Ctrl
* @author Ángel Lavana Rosas
* Description : New Class Controller for the SOC model
* Ver                  Date            Author                   Description
* @version 1.0         Jun/02/2020     Ángel Lavana Rosas       Initial version
**/

/**
* @description: Use method NameCampaign_Ctrl to call class MX_SB_VTS_UpNameCampaign_Service.NameCampaign_Service
*/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_UpNameCampaign_Ctrl {
    /**
    * @author	Ángel Lavana Rosas
    * @Description Invoca la clase de servicio
    * @param	IdLead	Recibe el Id del Candidato desde el process [Terminar llamada Telemarketing]
    * @return	Campos consultados al Id o Ids de candidato
    */
    @InvocableMethod    
    public static List<Lead> nameCampaignCtrl(List<Id> idLead) {       
        return MX_SB_VTS_UpNameCampaign_Service.nameCampaignService(idLead);
    }    
}