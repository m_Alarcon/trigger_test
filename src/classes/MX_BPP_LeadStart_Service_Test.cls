/**
* @File Name          : MX_BPP_LeadStart_Service_Test.cls
* @Description        : Test class for MX_BPP_LeadStart_Service
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 24/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      24/06/2020            Gabriel Garcia Rojas          Initial Version
**/
@isTest
private class MX_BPP_LeadStart_Service_Test {

    /** name variable for records */
    final static String NAME_ST = 'testUser';
    /** namerole variable for records */
    final static String NAME_RO = '%BPYP DIRECTOR BANCA PERISUR%';
    /** namesucursal variable for records */
    final static String NAME_SUC = '6343 PEDREGAL';
    /** nameprofile variable for records */
    final static String NAME_PRO = 'BPyP Director Oficina';
    /** namedivision variable for records */
    final static String NAME_DIV = 'METROPOLITANA';
    /** error message */
    final static String MESSAGE_TEST = 'Fail method';

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        final List<User> listUsers = new List<User>();
        final User testUserStarts = UtilitysDataTest_tst.crearUsuario(NAME_ST, NAME_PRO, NAME_RO);
        final User testUDD = UtilitysDataTest_tst.crearUsuario('TestUserDD', 'BPyP Director Divisional', 'BPYP DIRECTOR BANCA PERISUR');
        listUsers.add(testUserStarts);
        listUsers.add(testUDD);
        insert listUsers;

        final List<Lead> listLeads = new List<Lead>();
        final Lead candHelps = new Lead(LastName = NAME_ST, Status = 'Abierto');
        final Lead candHelps2 = new Lead(LastName = NAME_ST+'3', Status = 'Abierto');
        listLeads.add(candHelps);
        listLeads.add(candHelps2);
        insert listLeads;

        final Campaign campHelps = new Campaign(Name = NAME_ST);
        insert campHelps;

        final List<CampaignMember> listCampMems = new List<CampaignMember>();
        final CampaignMember campMemHelps = new CampaignMember(CampaignId = campHelps.Id, Status='Sent', LeadId = candHelps.Id);
        final CampaignMember campMemHelps2 = new CampaignMember(CampaignId = campHelps.Id, Status='Sent', LeadId = candHelps2.Id);
        listCampMems.add(campMemHelps);
        listCampMems.add(campMemHelps2);
        insert listCampMems;
    }

    /**
     * @description constructor test
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void testConstructor() {
        final MX_BPP_LeadStart_Service instServiceTest = new MX_BPP_LeadStart_Service();
        System.assertNotEquals(instServiceTest, null, MESSAGE_TEST);
    }

    /**
     * @description test fetchusdata
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void fetchusdataTest() {
        final List<String> listinfoUser = MX_BPP_LeadStart_Service.fetchusdata();
        System.assertNotEquals(listinfoUser.size(), 0 , MESSAGE_TEST);
    }

    /**
     * @description test fetchDataClass
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void fetchDataClassTest() {
        final MX_BPP_LeadStart_Helper.WRP_ChartStacked chartStacke = MX_BPP_LeadStart_Service.fetchDataClass(new List<String>{'',''}, new List<String>{'',''}, new List<String>{'',''}, 'division');
        System.assertNotEquals(chartStacke.lsColor.size(), 0 , MESSAGE_TEST);
    }

    /**
     * @description test infoLeadByClause
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void infoLeadByClauseTest() {
        final List<CampaignMember> listCMem = MX_BPP_LeadStart_Service.infoLeadByClause(new List<String>{'',''}, new List<String>{'',''}, new List<String>{'',''}, '10');
        System.assertEquals(listCMem.size(), 0 , MESSAGE_TEST);
    }

    /**
     * @description test fetchPgs
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void fetchPgsTest() {
        final Integer totalPag = MX_BPP_LeadStart_Service.fetchPgs(20);
        System.assertEquals(totalPag, 2 , MESSAGE_TEST);
    }

    /**
     * @description test fetchPgs
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void fetchPgsFTest() {
        final Integer totalPag2 = MX_BPP_LeadStart_Service.fetchPgs(null);
        System.assertEquals(totalPag2, 0 , MESSAGE_TEST);
    }

    /**
     * @description test userInfo
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void userInfoTest() {
        final MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper CampStarWrapper = MX_BPP_LeadStart_Service.userInfo();
        System.assertEquals(CampStarWrapper.setDivisiones.size(), 0, MESSAGE_TEST);
    }

    /**
     * @description test userInfo
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void userInfoDOTest() {
        final User usuarioDO = [SELECT Id FROM User WHERE Name=: NAME_ST];
        System.runAs(usuarioDO) {
            final MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper CampStarWrapper = MX_BPP_LeadStart_Service.userInfo();
            System.assertNotEquals(CampStarWrapper.setDivisiones.size(), 0, MESSAGE_TEST);
        }
    }

    /**
     * @description test userInfo
     * @author Gabriel Garcia | 24/06/2020
     * @return void
     **/
    @isTest
    private static void userInfoDDTest() {
        final User usuarioDD = [SELECT Id FROM User WHERE Name= 'TestUserDD'];
        System.runAs(usuarioDD) {
            final MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper CampStarWrapper = MX_BPP_LeadStart_Service.userInfo();
            System.assertNotEquals(CampStarWrapper.setDivisiones.size(), 0, MESSAGE_TEST);
        }
    }
}