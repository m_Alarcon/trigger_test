/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_COB_EnviaCorreoCobranzaSch_tst
* Autor Angel Nava
* Proyecto: Cobranza - BBVA Bancomer
* Descripción : Clase test de clase MX_SB_COB_EnviaCorreoCobranzaSch_cls
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Desripción
* --------------------------------------------------------------------------------
* 1.0           01/05/2019     Angel Nava				          Creación
* --------------------------------------------------------------------------------
*/
@isTest
public class MX_SB_COB_EnviaCorreoCobranzaSch_tst {

    @TestSetup
    static void creaObjetos() {
        final Bloque__c bloque = new Bloque__c(MX_SB_COB_BloqueActivo__c=true);
        insert bloque;
        final cargaarchivo__c archivo = new cargaArchivo__c(MX_SB_COB_Bloque__c=bloque.Name,MX_SB_COB_ContadorConError__c=0,MX_SB_COB_ContadorSinError__c=0,MX_SB_COB_RegistrosTotales__c=0);
        insert archivo;
    }

    @isTest
    static void pruebaCorreo() {
        final cargaArchivo__c archivo = [select id,MX_SB_COB_ContadorConError__c,MX_SB_COB_Bloque__c,MX_SB_COB_ContadorSinError__c,MX_SB_COB_RegistrosTotales__c from cargaArchivo__c limit 1];

        Test.startTest();
        final MX_SB_COB_EnviaCorreoCobranzaSch_cls objCorreo = new MX_SB_COB_EnviaCorreoCobranzaSch_cls();
                    objCorreo.idCargaArchivo = archivo.id;

                    final DateTime dtFechaHoraAct = DateTime.now();
                    final Date dFechaActual = dtFechaHoraAct.date();
                    final Time tmHoraActual = dtFechaHoraAct.Time();
                    final Time tmHoraActualEnv = tmHoraActual.addMinutes(30);// para pruebas 10min  , poductivo30 minutos posible cambiar a 60

                    String sch = '';
                    sch = '0 ' + tmHoraActualEnv.minute() + ' ' + tmHoraActualEnv.hour() + ' ' + dFechaActual.day() + ' ' + dFechaActual.month() + ' ?';
                    final String sNombreProc = tmHoraActualEnv.minute() + ' : ' + tmHoraActualEnv.hour() + ' : ' + dFechaActual.day() + ' : EnviaCorreoCobranza';
                    System.debug('EN ActivaProcNotif_tgr sNombreProc: ' + sNombreProc + ' sch: ' + sch);
          System.schedule(sNombreProc, sch, objCorreo);
        Test.stopTest();
        final List<CronTrigger> trig = [SELECT Id, CronJobDetail.Name, State, TimesTriggered,StartTime FROM CronTrigger where  cronjobdetail.name like '%EnviaCorreoCobranza%'  limit 10];
        system.assertEquals(1, trig.size(), 'job cargado correctacmente');
    }
}