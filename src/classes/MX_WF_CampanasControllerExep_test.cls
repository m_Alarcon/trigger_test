/**
* ------------------------------------------------------------------------------
* @Nombre: MX_WF_CampanasController_test
* @Autor: Sandra Ventura García
* @Proyecto: Workflow Campañas
* @Descripción : Clase de prueba para MX_WF_CampanasController
* ------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ------------------------------------------------------------------------------
* 1.0           18/09/2019     Sandra Ventura García	         test
* 2.0           21/11/2019     Sandra Ventura García             Se agrega test para crear canales de multipromociones.
* ------------------------------------------------------------------------------
*/
@isTest
private class MX_WF_CampanasControllerExep_test {
  /**
  * @description: Datos prueba lista tarjetas
  * @author Sandra Ventura
  */
    public static List<MX_WF_Catalogo_tarjetas__c> createTarjetas(Integer num) {
        final List<MX_WF_Catalogo_tarjetas__c> tar = new List<MX_WF_Catalogo_tarjetas__c>();

        for(Integer i=0;i<num;i++) {
            final MX_WF_Catalogo_tarjetas__c tarj = new MX_WF_Catalogo_tarjetas__c(Name='TestTarjeta'+ i);

            tar.add(tarj);
        }
        insert tar;
        return tar;
    }
  /**
  * @description: test lista tarjetas 
  * @author Sandra Ventura
  */
    @isTest static void tarjetasTest() {

           final MX_WF_Catalogo_tarjetas__c[] tar = MX_WF_CampanasControllerExep_test.createTarjetas(200);
           final String tDate = string.valueofGmt(Date.today());

        test.startTest();
        MX_WF_CampanasController.getListTarjetas(tDate);
         System.assert(tar.size() > 1,'Lista de Tarjetas test.');
        test.stopTest();
        }
  /**
  * @description: test constructor 
  * @author Sandra Ventura
  */
   @isTest static void testConstructorListas() {
        String errorMessage = '';
        test.startTest();
        try {
	        final MX_WF_CampanasController listas = new MX_WF_CampanasController();
            system.debug(listas);
        } catch (Exception e) {
            errorMessage = e.getMessage();
        }
        test.stopTest();
        System.assertEquals('', errorMessage,'No se encuentran registros');
    }
  /**
  * @description: test exception lista campañas
  * @author Sandra Ventura
  */
   @isTest
    static void testCamExep() {
        String errorMessage = '';
        test.startTest();
        try {
	        MX_WF_CampanasController.getListCampanas( 'invalid');
        } catch (Exception e) {
            errorMessage = e.getMessage();
        }
        test.stopTest();
        System.assertEquals('Script-thrown exception', errorMessage,'No se encuentran registros');
    }
  /**
  * @description: test exception lista comercios
  * @author Sandra Ventura
  */
   @isTest
    static void testComExep() {
        String errorMessage = '';
        test.startTest();
        try {
	        MX_WF_CampanasController.getListComercios( 'invalidId');
        } catch (Exception e) {
            errorMessage = e.getMessage();
        }
        test.stopTest();
        System.assertEquals('Script-thrown exception', errorMessage,'El id es inválido');
    }
  /**
  * @description: test exception lista tarjetas
  * @author Sandra Ventura
  */
   @isTest
    static void testTarExep() {
        String errorMessage = '';
        test.startTest();
        try {
	        MX_WF_CampanasController.getListTarjetas('invalidf');
        } catch (Exception e) {
            errorMessage = e.getMessage();
        }
        test.stopTest();
        System.assertEquals('Script-thrown exception', errorMessage,'Parámetro incorrecto');
    }
}