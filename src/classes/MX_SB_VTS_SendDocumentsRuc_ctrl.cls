/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 11-06-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   11-06-2020   Eduardo Hernández Cuamatzi   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_SendDocumentsRuc_ctrl {

    /**
    * @description Recupera documento de poliza
    * @author Eduardo Hernández Cuamatzi | 11-06-2020 
    * @param oppId Id de Oportunidad trabajada
    * @return Map<String, Object> Respuesta de servicio
    **/
    @AuraEnabled
    public static Map<String, Object> findCustomerDocument(String oppId, String docType) {
        final Map<String, Object> resService = new Map<String, Object>();
        final Quote quoteStatus = MX_SB_VTS_PaymentModule_Service.findQuote(oppId);
        Boolean isQuoteValid = false;
        if(validateDataQuote(quoteStatus)) {
            final Set<String> setOppId = new Set<String>{oppId};
            final List<Opportunity> oppData = MX_SB_VTS_CotizInitData_Service.findOppsById(setOppId);            
            final Map<String, Object> resGetDocs = MX_SB_VTS_SendDocumentsRuc_service.sGetCustomerDocument(quoteStatus.MX_SB_VTS_ASO_FolioCot__c, oppData[0], docType);
            if((Boolean)resGetDocs.get('docOk')) {
                final String policyDoc = (String)resGetDocs.get('documentSrc');
                resService.put('hrefDoc', policyDoc);
                isQuoteValid = true;
            } else {
                resService.put('msjError', 'Error al enviar documentos, intente nuevamente mas tarde');        
            }
        }
        resService.put('isOk', isQuoteValid);
        return resService;
    }

    /**
    * @description Valida campos necesarios para realizar el envio
    * @author Eduardo Hernández Cuamatzi | 11-10-2020 
    * @param quoteData Datos de la cotización a enviar
    * @return Boolean Validación correcta
    **/
    public static Boolean validateDataQuote(Quote quoteData) {
        Boolean dataOk = true;
        if(String.isBlank(quoteData.MX_SB_VTS_Email_txt__c) || 
            String.isBlank(quoteData.MX_SB_VTS_Numero_de_Poliza__c) || 
            String.isBlank(quoteData.MX_SB_VTS_RFC__c)) {
            dataOk = false;
        }
        return dataOk;
    }

    /**
    * @description Valida Producto para servicio de envio by email
    * @author Eduardo Hernández Cuamatzi | 11-10-2020 
    * @param oppData Datos de Oportunidad
    * @return List<String> Lista de valores para envio de correo
    **/
    public static List<String> fillProcessDataSends(Opportunity oppData) {
        final List<String> processStr = new List<String>();
        switch on oppData.Producto__c.toUpperCase() {
            when 'HOGAR SEGURO DINÁMICO' {
                processStr.add('HOSETE');
                processStr.add('Hogar Seguro Dinámico');
            }
            when 'VIDA SEGURA DINÁMICO' {
                processStr.add('MSDBBCOM');
                processStr.add('Vida Segura Dinámico');
            }
            when else {
                processStr.add('NA');
                processStr.add('NA');
            }
        }
        return processStr;
    }

    /**
    * @description Recupera documento de poliza
    * @author Eduardo Hernández Cuamatzi | 11-06-2020 
    * @param oppId Id de Oportunidad trabajada
    * @return Map<String, Object> Respuesta de servicio
    **/
    @AuraEnabled
    public static Map<String, Object> sendCustomerDocument(String oppId, List<String> lstDocsUrl) {
        final Map<String, Object> resService = new Map<String, Object>();
        final Quote quoteStatus = MX_SB_VTS_PaymentModule_Service.findQuote(oppId);
        Boolean sendDocResult = false;
        if(validateDataQuote(quoteStatus)) {
            final Set<String> setOppId = new Set<String>{oppId};
            final List<Opportunity> oppData = MX_SB_VTS_CotizInitData_Service.findOppsById(setOppId);            
            final List<String> procProdSend = fillProcessDataSends(oppData[0]);
            final Boolean resSendDocs = MX_SB_VTS_SendDocumentsRuc_service.sendDocumentByEmail(quoteStatus, lstDocsUrl, procProdSend);
            sendDocResult = resSendDocs;
        }
        resService.put('isOk', sendDocResult);
        return resService;
    }
}