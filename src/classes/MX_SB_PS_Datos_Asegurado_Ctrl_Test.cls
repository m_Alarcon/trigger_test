/**
 * @description       : 
 * @author            : Juan Carlos Benitez Herrera
 * @group             : 
 * @last modified on  : 12-09-2020
 * @last modified by  : Daniel Perez Lopez
 * Modifications Log 
 * Ver   Date         Author                                    Modification
* 1.0         09/07/2020    Juan Carlos Benitez Herrera              Creación
* 1.1   	  11-25-2020   Arsenio.perez.lopez.contractor@bbva.com   Version 1.1
* 1.2         12-15-2020   Daniel Perez Lopez                    Se añade method testcreateCustomerData y testCurp 
* 1.3          2-12-2021   Juan Carlos Benitez Herrera           Se cambia product2 de test
* 1.4          2-24-2021   Juan Carlos Benitez Herrera           Se añade producto__c a insert de makedata opp
**/
@isTest
public class MX_SB_PS_Datos_Asegurado_Ctrl_Test {
    /** Nombre de usuario Test */
    final static String NAME = 'Fernanda';
    /** Apellido Paterno */
    Final static STRING LNAME= 'Estrada';
    /** Respaldo Seguro Para Hospitalización */
    FINAL static String RSPH= 'Respaldo Seguro Para Hospitalización';
    @TestSetup
    static void createData() {
        final Account acc =  MX_WB_TestData_cls.crearCuenta('AccountTestPS','PersonAccount');  
        acc.Genero__c='M';
        insert acc;
        final MX_WB_FamiliaProducto__c famil= MX_WB_TestData_cls.createProductsFamily('Vida');
        insert famil;
        final product2  productNew = MX_WB_TestData_cls.productNew(RSPH);
        productNew.MX_SB_SAC_Proceso__c='SAC';
        productNew.Family=famil.id;
        productNew.MX_WB_FamiliaProductos__c=famil.id;
        insert productNew;
        final product2  productNew2 = MX_WB_TestData_cls.productNew(RSPH);
        productNew2.MX_SB_SAC_Proceso__c='SAC';
        productNew2.Family=famil.id;
        productNew2.MX_WB_FamiliaProductos__c=famil.id;
        insert productNew2;
        final PricebookEntry priceBookEntryNew =MX_WB_TestData_cls.priceBookEntryNew(productNew.Id);
        insert priceBookEntryNew;
        final PricebookEntry priceBookEntryN2 =MX_WB_TestData_cls.priceBookEntryNew(productNew2.Id);
        insert priceBookEntryN2;
        final Opportunity opp = MX_WB_TestData_cls.crearOportunidad('Opp1',acc.Id,userInfo.getUserId(), 'MX_SB_COB_Cotizador_RS_Hospital');
        opp.RecordTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_PS_RSHospitalario).getRecordTypeId();
        opp.Producto__c='Respaldo Seguro Para Hospitalización';
        insert opp;
        final Quote quoteobj= new Quote(Name=NAME,OpportunityId=opp.Id);
        insert quoteobj;
        Final String quoteIde=[select id from Quote where OpportunityId =:opp.Id limit 1].Id;
        Final MX_SB_VTS_Beneficiario__c benef = new MX_SB_VTS_Beneficiario__c(Name = NAME +'_1',MX_SB_VTS_Quote__c=quoteIde,MX_SB_MLT_NombreContacto__c=acc.PersonContactId,MX_SB_SAC_Email__c = NAME+'_'+'@'+'bbva.com');
        insert benef;
        final Contract contractVida = new Contract(AccountId = acc.Id, MX_SB_SAC_NumeroPoliza__c='policyTest');
            insert contractVida;
    }
    /*
* @description method que prueba LoadAccountF
* @param  void
* @return void
*/
@isTest
    static void tstLoadAccountF() {
        Final ID idAcc= [SELECT Id from Account limit 1].Id;
        Final ID idOpp =[SELECT Id from Opportunity where AccountId =:idAcc].Id;
        Final Id idPCntct= [SELECT PersonContactId FROM Account limit 1].Id;
        test.startTest();
        String returx= MX_SB_PS_Datos_Asegurado_Ctrl.selectmethod('loadOppF',idOpp);
        returx= MX_SB_PS_Datos_Asegurado_Ctrl.selectmethod('loadAccountF',idAcc);
        returx= MX_SB_PS_Datos_Asegurado_Ctrl.selectmethod('loadContactAcc',idAcc);
        returx= MX_SB_PS_Datos_Asegurado_Ctrl.selectmethod('LoadContactAcc',idPCntct);
        returx= MX_SB_PS_Datos_Asegurado_Ctrl.selectmethod('getContract',idOpp);
        returx= MX_SB_PS_Datos_Asegurado_Ctrl.selectmethod('formaliza',idOpp);
        system.assert(!String.isEmpty(returx), 'Oportunidad cargada');
        test.stopTest();
    }
    /**
    * @description :Methodo prueba actualizacion de contacto
    * @author Arsenio.perez.lopez.contractor@bbva.com | 11-25-2020 
    **/
	@isTest
    static void testupsrtCntc() {
        Final Id idAcc= [SELECT Id FROM Account limit 1].Id;
        Final Id idOpp= [SELECT Id from Opportunity where AccountId =:idAcc].Id;
        Final Contact cntc = [SELECT Id, MX_RTL_CURP__c, LastName  from Contact where AccountId=:idAcc];
        cntc.MX_RTL_CURP__c='BEHJ930405HDFNRN43';
        Final Contract cntrct = new Contract();
		cntrct.AccountId=idAcc;
        cntrct.MX_WB_Oportunidad__c =idOpp;
        cntrct.BillingCity='Alamos';
        cntrct.BillingCountry='Mexico';
        cntrct.BillingPostalCode='03400';
        cntrct.BillingStreet='Asturias, 59';
        cntrct.BillingState='Ciudad de Mexico';
        test.startTest();
            String returx= JSON.serialize(cntc);
            MX_SB_PS_Datos_Asegurado_Ctrl.supsert('contacto',returx);
            returx= JSON.serialize(cntrct);
            MX_SB_PS_Datos_Asegurado_Ctrl.supsert('cotnrato',returx);
            system.assert(!String.isEmpty(returx), 'Oportunidad1 cargada exitosamente');
        test.stopTest();
    }
    /**
    * @description Methodo prueba actualizacion de cotnrato
    * @author Arsenio.perez.lopez.contractor@bbva.com | 11-25-2020 
    **/
        @isTest
    static void testupsertcontract() {
        Final Id idAcc= [SELECT Id FROM Account limit 1].Id;
        Final String idOpp= [SELECT Id FROM Opportunity limit 1].Id;
        final Map<String,String> mapa= new Map<String,String>();
        final Contract cntrcTest = [SELECT Id FROM Contract limit 1];
        mapa.put('AccountId',idAcc);
        mapa.put('oportunidad',idOpp);
        mapa.put('BillingCity','Alamos');
        mapa.put('BillingCountry','Mexico');
        mapa.put('BillingPostalCode','03400');
        mapa.put('BillingStreet','Asturias, 59');
        mapa.put('BillingState','Ciudad de Mexico');
        mapa.put('NumeroPoliza','OTN-120300303');
        mapa.put('Status','Draft');
        mapa.put('StartDate','15/10/2020'); 
        mapa.put('origen','Outbound');
        test.startTest();
        	MX_SB_PS_Datos_Asegurado_Ctrl.contractParam(mapa, RSPH,cntrcTest.Id);
            system.assert(true, 'Contrato creado exitosamente');
        test.stopTest();
    }
    /**
    * @description : methodo prueba busqueda de beneficiarios
    * @author Arsenio.perez.lopez.contractor@bbva.com | 11-25-2020 
    **/
        @isTest 
    static void testsrchBenefs() {
        Final quote quotId = [SELECT Id,OpportunityId from Quote limit 1];
        Final Id idOpp= [SELECT Id FROM Opportunity limit 1].Id;
        test.startTest();
        String returx= MX_SB_PS_Datos_Asegurado_Ctrl.selectmethod('srchBenefs',quotId.Id);
        returx= MX_SB_PS_Datos_Asegurado_Ctrl.selectmethod('getDirData',idOpp);
        MX_SB_PS_Datos_Asegurado_Ctrl.saveDirData(quotId);
        system.assert(!String.isEmpty(returx), 'Oportunidad Exitosament1e');
        test.stopTest();
    }
    /**
    * @description :Methodo prueba actualizacion de cuenta
    * @author Arsenio.perez.lopez.contractor@bbva.com | 11-25-2020 
    **/
    @isTest
    static void testupsrtAcc() {
        Final Account pAcc = [SELECT Id,MX_RTL_CURP__pc from Account limit 1];
        test.startTest();
        MX_SB_PS_Datos_Asegurado_Ctrl.supsert('cuenta',JSON.serialize(pAcc));
        system.assert(true,'Se han encontrado beneficiarios');
        test.stopTest();
    }
    /**
    * @description :Methodo prueba servicio getcolonias
    * @author Arsenio.perez.lopez.contractor@bbva.com | 11-25-2020 
    **/
    @isTest
    static void testgetcolonias() {
        test.startTest();
        try {
            MX_SB_PS_Datos_Asegurado_Ctrl.getcolonias('55764');
            } catch(Exception e) {
            System.assert(e.getMessage().contains('No content'), 'message=' + e.getMessage());
        }
        test.stopTest();
    }
    /**
    * @description: Methodo prueba Servicio createCustomerData
    * @author Arsenio.perez.lopez.contractor@bbva.com | 11-25-2020 
    **/
    @isTest
    static void testcreateCustomerData() {
        final Opportunity opp=[SELECT ID,Name,createdDate FROM Opportunity LIMIT 1];
        Final Map<String,String> mapa = new Map<String,String>();
        mapa.put('rfc','RAID850814JKA');
        mapa.put('calle','Asturias');
        mapa.put('NumExterior','17');
        mapa.put('NumInterior','3');
        mapa.put('MX_WB_txt_Colonia','Fuentes del valle');
        mapa.put('coloniaDefVal','Fuentes del valle');
        Final Contact contacto = [SELECT Id from Contact limit 1];
        test.startTest();
        try {
             MX_SB_PS_Datos_Asegurado_Ctrl.createCustomerData(opp.id,contacto,mapa);
            } catch(Exception i) {
            System.assert(true,'Se crearon datos del cliente');
        }
        test.stopTest();
    }
    /**
    * @description : Methodo prueba consulta de CURP RENAPO
    * @author Arsenio.perez.lopez.contractor@bbva.com | 11-25-2020 
    **/
    @isTest 
    static void testCurp () {
        Final Id idAcc= [SELECT Id FROM Account limit 1].Id;
        Final Contact cntc = [SELECT Id, MX_RTL_CURP__c, LastName, FirstName,birthdate  from Contact where AccountId=:idAcc];
        cntc.MailingPostalCode='77887';
        cntc.birthdate= Date.today().addYears(-8);
        update cntc;
        try {
            MX_SB_PS_Datos_Asegurado_Ctrl.gEstados();
            MX_SB_PS_Datos_Asegurado_Ctrl.getCurp(cntc, 'PUEBLA', 'M');
           } catch(Exception i) {
           System.assert(true,'Se crearon datos del cliente');
       }
    }
}