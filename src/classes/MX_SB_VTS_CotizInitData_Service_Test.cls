/**
 * @File Name          : QuoteLineItemSelector_Test.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernandez Cuamatzi
 * @Last Modified On   : 4/6/2020 17:30:47
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    26/5/2020   Eduardo Hernandez Cuamatzi     Initial Version
 * 1.1    09/03/2021  Eduardo Hernandez Cuamatzi     Se agrega test de cobertura
**/
@isTest
private class MX_SB_VTS_CotizInitData_Service_Test {
    /**@description Nombre usuario*/
    private final static string ASESORNAME = 'AsesorTest';
    /**@description etapa Cotizacion*/
    private final static string COTIZACION = 'Cotización';
    
    @TestSetup
    static void makeData() {
        MX_SB_VTS_CallCTIs_utility.initHogarGeneric();
    }

    @isTest
    static void findOppsById() {
        final User asesorUser = [Select Id from User where LastName =: ASESORNAME];
        MX_SB_VTS_CallCTIs_utility.insertPermissionVTS(asesorUser);
        Test.startTest();
        System.runAs(asesorUser) {
            final Set<String> lstOpps = new Set<String>();
            final Id oppId = [Select Id from Opportunity where Name = 'OppTelemarketing'].Id;
            lstOpps.add(oppId);
            final List<Opportunity> quoliDat = MX_SB_VTS_CotizInitData_Service.findOppsById(lstOpps);
            System.assertEquals(quoliDat.size(),1, 'Datos recuperados');
        }
        Test.stopTest();
    }

    @isTest
    static void findQuolisByQuotMap() {
        final User asesorUser = [Select Id from User where LastName =: ASESORNAME];
        MX_SB_VTS_CallCTIs_utility.insertPermissionVTS(asesorUser);
        Test.startTest();
        System.runAs(asesorUser) {
            final Map<Id, Quote>  mapQuote = new Map<Id, Quote>([Select Id,Name from Quote where MX_SB_VTS_Folio_Cotizacion__c = '999990']);
            final Map<Id, QuoteLineItem> quoliDat = MX_SB_VTS_CotizInitData_Service.findQuolisByQuotMap(mapQuote);
            System.assertEquals(quoliDat.size(),1, 'Datos recuperados');
        }
        Test.stopTest();
    }

    @isTest
    static void findQuotesByOppMap() {
        final User asesorUser = [Select Id from User where LastName =: ASESORNAME];
        MX_SB_VTS_CallCTIs_utility.insertPermissionVTS(asesorUser);
        Test.startTest();
        System.runAs(asesorUser) {
            final List<Opportunity> oppId = [Select Id, Name from Opportunity where Name = 'OppTelemarketing'];
            final Map<Id, Quote> quoliDat = MX_SB_VTS_CotizInitData_Service.findQuotesByOppMap(oppId);
            System.assertEquals(quoliDat.size(),1, 'Datos recuperados');
        }
        Test.stopTest();
    }

    @isTest
    static void findQuotesById() {
        final User asesorUser = [Select Id from User where LastName =: ASESORNAME];
        MX_SB_VTS_CallCTIs_utility.insertPermissionVTS(asesorUser);
        Test.startTest();
        System.runAs(asesorUser) {
            final Id quoteId = [Select Id from Quote where MX_SB_VTS_Folio_Cotizacion__c = '999990'].Id;
            final List<Quote> quoliDat = MX_SB_VTS_CotizInitData_Service.findQuotesById(quoteId);
            System.assertEquals(quoliDat.size(),1, 'Quote recuperado');
        }
        Test.stopTest();
    }

    /**
    * @description Inserta permission sets
    * @author Eduardo Hernandez Cuamatzi | 26/5/2020 
    * @param String permissionName 
    * @param Id userId id de usuario
    * @return void 
    **/
    static void insertPermissionSet(String permissionName, Id userId) {
        final PermissionSet permissions = [SELECT Id FROM PermissionSet WHERE Name =: permissionName];
        insert new PermissionSetAssignment(AssigneeId = userId, PermissionSetId = permissions.Id);
    }

    @isTest
    static void updateFrecuencie() {
		final User userTest = [Select Id from User where LastName =: ASESORNAME];
        System.runAs(userTest) {
        final Opportunity OppRecId = [SELECT Id  FROM Opportunity  where StageName =: COTIZACION];
        Test.startTest();
            final List<String> lstPayments = new List<String>{'Anual', '1', '1'};
            final Map<String, Object> response = MX_SB_VTS_CotizInitData_Ctrl.updateFrecuencie(OppRecId.Id, lstPayments);
            System.assert((Boolean)response.get('isOk'),'Datos correctos');
        }
        Test.stopTest();
    }
}