/**
* @description       : MX_SB_VTS_GetPartData_OB_Service
* @author            : Diego Olvera
* @group             : 
* @last modified on  : 11-04-2020
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   10-15-2020   Diego Olvera   Initial Version
* 1.1   12-01-2021   Diego Olvera   Ajustes a la clase para consumo ASO
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton, sf:AvoidGlobalModifier')
global class MX_SB_VTS_GetPartData_OB_Service {
    /**
     * Atributos de la Clase
     */
    private static final Integer OKCODE = 200;
    /** Lista de Objetos */
    public static final List<Object> MRETURNVALUESOB = new List<Object>();
    
    /* constructor
     */
    private MX_SB_VTS_GetPartData_OB_Service() {}
    
    /**
     * @description: Realiza el consumo del servicio listCarInsuranceCatalogs/OB 
     * 				 para obtener: Obtener Coberturas
     * @author: 	 Diego Olvera
     * @return: 	 Map<String, Object> mReturnVal
     */	
    public static  Map<String, Object> obtListCarInsCatSrvDp(Map <String, Object> datosFinales) {
        final Map<String, Object> mReturnVal = new Map<String, Object>();
        final Map<String, Object> sParamsSrv = new Map<String, Object>{'OB' => 'OB', 
            'manUnit' => datosFinales.get('codigoAlianza'), 
            'productCode' => datosFinales.get('codigoProducto'),
            'planCode' => datosFinales.get('codigoPlan'), 
            'planReview' => datosFinales.get('revisionPlan')};
        final HttpResponse objRespSrvOb = MX_RTL_IasoServicesInvoke_Selector.callServices('getCoverages', sParamsSrv, obtRequestSrv(datosFinales));
        final String getLastVal = String.valueOf(objRespSrvOb.getBody()).replace('group', 'groupx');
        objRespSrvOb.setBody(getLastVal);
        if(objRespSrvOb.getStatusCode() ==  OKCODE) {
            MRETURNVALUESOB.add(objRespSrvOb.getBody());
            final Map<String, Object> mStatusCode = new Map<String, Object>{'code' => String.valueOf(OKCODE), 'description' => 'Consumo de Servicio Exitoso!'};  
                mReturnVal.put('oData', MRETURNVALUESOB);
            mReturnVal.put('oResponse', mStatusCode);
        } else {
            mReturnVal.put('oResponse', MX_SB_VTS_Codes_Utils.statusCodes(objRespSrvOb.getStatusCode()));
        }           
        return mReturnVal;
    }
    
    /**
     * @description : Realiza el armado del objeto Request para el consumo del servicio ASO
     * @author      : Alexandro Corzo
     * @return      : HttpRequest oRequest
     */
    public static HttpRequest obtRequestSrv(Map <String, Object> datosFinales) {
        final Map<String,iaso__GBL_Rest_Services_Url__c> allCodesASO = iaso__GBL_Rest_Services_Url__c.getAll();
        final iaso__GBL_Rest_Services_Url__c objASOSrv = allCodesASO.get('getCoverages');
        final String strEndoPoint = objASOSrv.iaso__Url__c;
        final HttpRequest oRequest = new HttpRequest();
        oRequest.setTimeout(120000);
        oRequest.setMethod('POST');
        oRequest.setHeader('Content-Type', 'application/json');
        oRequest.setHeader('Accept', '*/*');
        oRequest.setHeader('Host', 'https://test-sf.bbva.mx');
        oRequest.setEndpoint(strEndoPoint);
        oRequest.setBody(obtBodyService(datosFinales));
        return oRequest;
    }
    
    /**
     * @description : Realiza el armado del Body que sera ocupado en el Request del Servicio
     * @author      : Alexandro Corzo
     * @return      : String sBodyService
     */
    public static String obtBodyService(Map <String, Object> datosFinales) {
    	String sBodyService = '';
        sBodyService += '{';
        sBodyService += '"header": {';
        sBodyService += '"aapType": "10000137",';
        sBodyService += '"dateRequest": "2017-10-03 12:33:27.104",';
        sBodyService += '"channel": "",';
        sBodyService += '"subChannel": "",';
        sBodyService += '"branchOffice": "",';
        sBodyService += '"managementUnit": "'+datosFinales.get('codigoAlianza').toString()+'",';
        sBodyService += '"user": "",';
        sBodyService += '"idSession": "",';
        sBodyService += '"idRequest": "",';
        sBodyService += '"dateConsumerInvocation": "2017-10-03 12:33:27.104"';
        sBodyService += '},';
        sBodyService += '"productCode" : "'+datosFinales.get('codigoProducto').toString()+'",';
        sBodyService += '"iRequest": {';
        sBodyService += '"productPlan": {';
        sBodyService += '"planCode": "'+datosFinales.get('codigoPlan').toString()+'",';
        sBodyService += '"planReview":"'+datosFinales.get('revisionPlan').toString()+'",';
        sBodyService += '"bouquetCode":"",';
        sBodyService += '"subPlanId": ""';
        sBodyService += '}';
        sBodyService += '}';
        sBodyService += '}';
        return sBodyService;
    }
}