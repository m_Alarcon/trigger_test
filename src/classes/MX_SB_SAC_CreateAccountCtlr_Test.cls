/**
 * @File Name          : MX_SB_SAC_CreateAccountCtlr_Test.cls
 * @Description        :
 * @Author             : Jaime Terrats
 * @Group              :
 * @Last Modified By   : Jaime Terrats
 * @Last Modified On   : 6/30/2020, 2:51:38 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/30/2020   Jaime Terrats     Initial Version
**/
@isTest
private with sharing class MX_SB_SAC_CreateAccountCtlr_Test {
    /** account string name */
    final static String ACCOUNT_NAME = 'fulanito';
    /** string value for sobject type */
    final static String SOBJECT_ACC = 'Account';
    /**
    * @description
    * @author Jaime Terrats | 6/30/2020
    * @return void
    **/
    @TestSetup
    static void makeData() {
        final User usr = MX_WB_TestData_cls.crearUsuario(ACCOUNT_NAME, System.Label.MX_SB_VTS_ProfileAdmin);
        System.runAs(usr) {
            final Account acc = MX_WB_TestData_cls.createAccount(ACCOUNT_NAME, System.Label.MX_SB_VTS_PersonRecord);
            insert acc;
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 6/30/2020
    * @return void
    **/
    @isTest
    static void test1() {
        final String rtAcc = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_PersonRecord).getRecordTypeId();
        final String accRt = MX_SB_SAC_CreateAccount_Ctlr.getRecordTypeByDevName('Id', System.Label.MX_SB_VTS_PersonRecord, SOBJECT_ACC);
        System.assertEquals(accRt, rtAcc, 'No hay coincidencia');
    }

    /**
    * @description
    * @author Jaime Terrats | 6/30/2020
    * @return void
    **/
    @isTest
    static void test2() {
        try {
            MX_SB_SAC_CreateAccount_Ctlr.getRecordTypeByDevName('Nombre', System.Label.MX_SB_VTS_PersonRecord, SOBJECT_ACC);
        } catch(Exception ex) {
            final Boolean exError = ex.getMessage().contains('Script-thrown') ? true : false;
            System.assert(exError, 'no fallo');
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 6/30/2020
    * @return void
    **/
    @isTest
    static void test3() {
        final Account acc = new Account(
            FirstName = ACCOUNT_NAME,
            LastName = ACCOUNT_NAME,
            PersonEmail = ACCOUNT_NAME+'@test.gmail.com',
            RecordTypeId = MX_SB_SAC_CreateAccount_Ctlr.getRecordTypeByDevName('Id', System.Label.MX_SB_VTS_PersonRecord, SOBJECT_ACC)
        );

        final Account createAccount = MX_SB_SAC_CreateAccount_Ctlr.upsertAccount(acc);
        System.assertNotEquals(createAccount.Id, '', 'No creo la cuenta');
    }

        /**
    * @description
    * @author Jaime Terrats | 6/30/2020
    * @return void
    **/
    @isTest
    static void test4() {
        try {
            final Account wrong_acc = new Account(
                FirstName = ACCOUNT_NAME,
                LastName = ACCOUNT_NAME,
                PersonEmail = 'HAHA',
                RecordTypeId = MX_SB_SAC_CreateAccount_Ctlr.getRecordTypeByDevName('Id', System.Label.MX_SB_VTS_PersonRecord, SOBJECT_ACC)
            );
            MX_SB_SAC_CreateAccount_Ctlr.upsertAccount(wrong_acc);
        } catch(Exception ex) {
            final Boolean exError = ex.getMessage().contains('Script-thrown') ? true : false;
            System.assert(exError, 'no fallo');
        }
    }
}