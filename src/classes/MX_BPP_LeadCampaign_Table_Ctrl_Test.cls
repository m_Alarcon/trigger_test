/**
* @File Name          : MX_BPP_LeadCampaign_Table_Ctrl_Test.cls
* @Description        : Test class for MX_BPP_LeadCampaign_Table_Ctrl
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 01/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      01/06/2020            Gabriel Garcia Rojas          Initial Version
**/
@isTest
private class MX_BPP_LeadCampaign_Table_Ctrl_Test {
	/*Usuario de pruebas*/
    private static User testUser = new User();

    /** namerole variable for records */
    final static String NAMEROLE = '%BPYP BANQUERO BANCA PERISUR%';
    /** nameprofile variable for records */
    final static String NAMEPROFILE = 'BPyP Estandar';
    /** namesucursal variable for records */
    final static String NAMESUCURSAL = '6385 BANCA PERISUR';
    /** namedivision variable for records */
    final static String NAMEDIVISION = 'METROPOLITANA';
    /** name variable for records */
    final static String NAME = 'testUser';
    /** error message */
    final static String MESSAGEFAIL = 'Fail method';

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testUser = UtilitysDataTest_tst.crearUsuario(NAME, NAMEPROFILE, NAMEROLE);
        testUser.BPyP_ls_NombreSucursal__c = NAMESUCURSAL;
        testUser.Divisi_n__c = NAMEDIVISION;
        insert testUser;

        final Double numClienteRondom = Math.random()*10000000;

        final Account cuenta = new Account();
		cuenta.LastName = 'Cuenta test';
        cuenta.PersonEmail = 'test@test.com';
		cuenta.No_de_cliente__c = String.valueOf(numClienteRondom.round());
		cuenta.RecordTypeId = RecordTypeMemory_cls.getRecType('Account', 'MX_BPP_PersonAcc_Client');
		insert cuenta;

        final Product2 producto = new Product2(Name = 'Cuenta en Dólares', Banca__c = 'Red BPyP');
        insert producto;

        final Lead candidato = new Lead(LastName = 'Candidato', LeadSource = 'Preaprobado', MX_WB_RCuenta__c = cuenta.Id);
        insert candidato;

        final Campaign campanya = new Campaign(Name = 'Campaña', MX_WB_Producto__c = producto.Id);
        insert campanya;

        final CampaignMember campMem = new CampaignMember(CampaignId = campanya.Id, Status='Sent', LeadId = candidato.Id, MX_LeadEndDate__c=Date.today().addDays(13));
        insert campMem;
    }

    /**
     * @description constructor test
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void testConstructor() {
        final MX_BPP_LeadCampaign_Table_Ctrl instanceC = new MX_BPP_LeadCampaign_Table_Ctrl();
        System.assertNotEquals(instanceC, null, MESSAGEFAIL);
    }

     /**
     * @description Test to method getListUser
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void getListUserTest() {
        final User testUser2 = UtilitysDataTest_tst.crearUsuario(NAME+' Director', 'BPyP Director Oficina', 'BPYP DIRECTOR OFICINA BANCA PERISUR');
        testUser2.BPyP_ls_NombreSucursal__c = NAMESUCURSAL;
        testUser2.Divisi_n__c = NAMEDIVISION;
        insert testUser2;

        List<User> listUsers = new List<User>();
        System.runAs(testUser2) {
            listUsers = MX_BPP_LeadCampaign_Table_Ctrl.userData();
        }
        System.assertNotEquals(listUsers.size(), 0, MESSAGEFAIL);
    }

     /**
     * @description Test to method getInfoMembersByFilter
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void getInfoMembersByFilterTest() {
        final CampaignMember campMem = [SELECT Id, Lead.Name, Lead.MX_WB_RCuenta__r.Name, CampaignId FROM CampaignMember LIMIT 1];
        final List<String> params = new List<String>{campMem.Lead.MX_WB_RCuenta__r.Name, '', 'Preaprobado', '', '', '', '', '', '', '', '' };
        Test.startTest();
        final List<CampaignMember> listCamp = MX_BPP_LeadCampaign_Table_Ctrl.getLeadData(params, new List<User>{testUser});
        Test.stopTest();
        System.assertEquals(listCamp.size(), 0, MESSAGEFAIL);
    }

     /**
     * @description Test to method getFamilyDataBySchema
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void getFamilyDataBySchemaTest() {
        Test.startTest();
        final List<String> listFamilies = MX_BPP_LeadCampaign_Table_Ctrl.familyData();
        Test.stopTest();
        System.assertNotEquals(listFamilies.size(), 0, MESSAGEFAIL);
    }

    /**
     * @description Test to method getStatusDataBySchema
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void getStatusDataBySchemaTest() {
        Test.startTest();
        final List<String> listStatus = MX_BPP_LeadCampaign_Table_Ctrl.statusData();
        Test.stopTest();
        System.assertNotEquals(listStatus.size(), 0, MESSAGEFAIL);
    }

    /**
     * @description Test to method getOppPickListProductValue
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void getOppPickListProductValueTest() {
        Test.startTest();
        final Map<Object,List<String>> mapSchema = MX_BPP_LeadCampaign_Table_Ctrl.getPicklistValues('Opportunity.MX_RTL_Producto__c');
        Test.stopTest();
        System.assertNotEquals(mapSchema.size(), 0, MESSAGEFAIL);
    }

	/**
     * @description Test to method getOppFromLeadConv
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/

    @isTest
    private static void getOppFromLeadConvTest() {
        final Campaign campTest = new Campaign(Name = 'Campaign Service Test');
        insert campTest;
        final Account cuentaTest = new Account(Name='Test Service Account',No_de_cliente__c = '8316251');
        insert cuentaTest;
        final Lead candidatoTest = new Lead(LastName='Lead Service Test', RecordTypeId=Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('MX_BPP_Leads').getRecordTypeId(),
                MX_ParticipantLoad_id__c=cuentaTest.No_de_cliente__c, MX_LeadEndDate__c=Date.today().addDays(13), MX_WB_RCuenta__c= cuentaTest.Id, MX_LeadAmount__c=250, LeadSource='Preaprobados');
        insert candidatoTest;
        final CampaignMember mCampaignTest = new CampaignMember(CampaignId = campTest.Id, Status='Sent', LeadId = candidatoTest.Id, MX_LeadEndDate__c=Date.today().addDays(13));
        insert mCampaignTest;

        Test.startTest();
        final Opportunity oppCreated = MX_BPP_LeadCampaign_Table_Ctrl.leadConv(candidatoTest.Id);
        Test.stopTest();
        System.assertNotEquals(oppCreated, null, MESSAGEFAIL);
    }

    /**
     * @description Test to method fetchListSucursales
     * @author Gabriel Garcia | 01/07/2020
     * @return void
     **/
    @isTest
    private static void fetchListSucurTest() {
        final List<String> listSuc = MX_BPP_LeadCampaign_Table_Ctrl.fetchListSucursales(NAMEDIVISION);
        MX_BPP_LeadCampaign_Table_Ctrl.userInfo();
        MX_BPP_LeadCampaign_Table_Ctrl.fetchListDivisiones();
        MX_BPP_LeadCampaign_Table_Ctrl.fetchCampaignMember(new List<String>{'', '', ''});
        System.assertNotEquals(listSuc, null, MESSAGEFAIL);
    }

}