@isTest
/**
* @File Name          : MX_RTL_Beneficiario_Selector_Test.cls
* @Description        : Clase que prueba los methods de MX_RTL_Beneficiario_Selector
* @Author             : Juan Carlos Benitez
* @Group              :
* @Last Modified By   : Juan Carlos Benitez
* @Last Modified On   : 5/26/2020, 05:22:22 AM
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    5/26/2020   Juan Carlos Benitez         Initial Version
**/
public class MX_RTL_Beneficiario_Selector_Test {
    /** Campos de beneficiario **/
    Final static String BENEFIELDS = ' Id, MX_SB_VTS_Contracts__c, Name, MX_SB_VTS_APaterno_Beneficiario__c, MX_SB_SAC_Email__c, recordtype.Name ';
    /** Nombre tipo de registro */
    final static String RECORDTYPEVIDA = 'MX_SB_MLT_RamoVida';
    /** Nombre de usuario Test */
    final static String NAME = 'El chavo del ';
    
    /** current user id */
    final static String USR_ID = UserInfo.getUserId();
    /** Apellido de  user Test */
    final static String LNAME = 'Star';
    /** VAR CONDITION */
    Final static String CONDITION ='QuoteId';
    
    /**
* @description
* @author Juan Carlos Benitez | 02/06/2020
* @return void
**/
    @TestSetup
    static void dataforTest() {
            MX_SB_PS_OpportunityTrigger_test.makeData();
            final Account cuentaTst = [Select Id from Account limit 1];
            final List<Contract> listContrcts =new List<Contract>();
            for(Integer i = 0; i<10; i++) {
                final Contract testContr = new Contract();
                testContr.AccountId = cuentaTst.Id;
                testContr.MX_SB_SAC_NumeroPoliza__c='policyTest'+'_'+i;
                listContrcts.add(testContr);
            }
            Database.insert(listContrcts);
            final List<MX_SB_VTS_Beneficiario__c> listBenefs = new List<MX_SB_VTS_Beneficiario__c>();
            for(Integer j = 10; j<10; j++) {
                Final MX_SB_VTS_Beneficiario__c benef = new MX_SB_VTS_Beneficiario__c();
                benef.RecordType.DeveloperName='MX_SB_MLT_Beneficiario';
                benef.Name = NAME +'_'+j;
                benef.MX_SB_VTS_Contracts__c =listContrcts[j].Id;
                benef.MX_SB_SAC_Email__c = NAME+'_'+j+'@'+'bbva.com';
                listBenefs.add(benef);
            }
            Database.insert(listBenefs);
            Final List<Siniestro__c> listSin = new List<Siniestro__c>();
            for(Integer k = 0; k<10; k++) {
                Final Siniestro__c sinTst = new Siniestro__c();
                sinTst.MX_SB_SAC_Contrato__c = listContrcts[k].Id;
                sinTst.MX_SB_MLT_Fecha_Hora_Siniestro__c =date.valueof('2020-03-27T22:04:00.000+0000');
                sinTst.MX_SB_MLT_NombreConductor__c = NAME + '_' + k;
                sinTst.MX_SB_MLT_APaternoConductor__c = 'LastName for '+NAME;
                sinTst.MX_SB_MLT_AMaternoConductor__c = 'LastName2 for '+NAME;
                sinTst.MX_SB_MLT_Telefono__c = '5534253647';
                Final datetime citaAgenda = datetime.now();
                sinTst.MX_SB_MLT_SubRamo__c = 'Ahorro';
                sinTst.MX_SB_MLT_CitaVidaAsegurado__c = citaAgenda.addDays(2);
                sinTst.MX_SB_MLT_AtencionVida__c = 'Agendar Cita';
                sinTst.MX_SB_MLT_PreguntaVida__c = false;
                sinTst.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_creacion;
                sinTst.TipoSiniestro__c = 'Siniestros';
                sinTst.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoVida).getRecordTypeId();
                listSin.add(sinTst);
            }
            Database.insert(listSin);
        }
    
    @isTest
    static void testgetBenefByContrct () {
        final List<contract> contractId =[SELECT Id from contract where MX_SB_SAC_NumeroPoliza__c='policyTest_1'];
        Final Set<Id> cntrct = new Set<Id>();
        for(Contract cnt : contractId) {
            cntrct.add(cnt.Id);
        }
        MX_RTL_Beneficiario_Selector.getBenefDataByContract(cntrct,BENEFIELDS);
        system.assert(true,'He encontrado estos beneficiarios relacionados');
    }
    @isTest
    static void testgetBenefDatabyId () {
        final List<contract> contractId2 =[SELECT Id from contract where MX_SB_SAC_NumeroPoliza__c='policyTest_1'];
        final List<MX_SB_VTS_Beneficiario__c> benefs2 = [SELECT Id,MX_SB_VTS_Contracts__c,Name, MX_SB_VTS_APaterno_Beneficiario__c,MX_SB_SAC_Email__c,recordtype.Name from MX_SB_VTS_Beneficiario__c where MX_SB_VTS_Contracts__c=:contractId2];
        Final Set<Id> cntrct = new Set<Id>();
        for(Contract cnt : contractId2) {
            cntrct.add(cnt.Id);
        }
        MX_RTL_Beneficiario_Selector.getBenefDataById(benefs2,BENEFIELDS);
        system.assert(true,'Busqueda exitosa');
    }
    @isTest
    static void testUpdateBeneficiario () {
        final List<contract> contractId3 =[SELECT Id from contract where MX_SB_SAC_NumeroPoliza__c='policyTest_1'];
        final List<MX_SB_VTS_Beneficiario__c> benefs3 = [SELECT Id,MX_SB_VTS_Contracts__c,Name, MX_SB_VTS_APaterno_Beneficiario__c,MX_SB_SAC_Email__c,recordtype.Name from MX_SB_VTS_Beneficiario__c where MX_SB_VTS_Contracts__c=:contractId3];
        Final Set<Id> cntrct = new Set<Id>();
        for(Contract cnt : contractId3) {
            cntrct.add(cnt.Id);
        }
        MX_RTL_Beneficiario_Selector.updateBeneficiario(benefs3);
        system.assert(true,'El beneficiario se ha actualizado correctamente');
    }
    
    @isTest
    static void testingController() {
        Final MX_RTL_Beneficiario_Selector selector = new MX_RTL_Beneficiario_Selector();
        system.assert(true,selector);
    }
    
    @isTest
    static void testinsertBenef() {
        Final Contract cntrct= [SELECT Id from Contract limit 1];
        Final MX_SB_VTS_Beneficiario__c benef = new MX_SB_VTS_Beneficiario__c();
        benef.RecordTypeId=Schema.SObjectType.MX_SB_VTS_Beneficiario__c.getRecordTypeInfosByDeveloperName().get('MX_SB_SAC_RTAsegurado').getRecordTypeId();
        benef.Name = NAME;
        benef.MX_SB_VTS_Contracts__c =cntrct.Id;
        benef.MX_SB_SAC_Email__c = LNAME+'contractor'+'@'+'bbva.com';
        test.startTest();
        MX_RTL_Beneficiario_Selector.insertBenef(benef);
        system.assert(true, 'Se ha agregado el beneficiario correctamente');
        test.stopTest();
    }
    @isTest
    static void testgetBenefByAccId() {
        Final Account pAcc2 =[Select Id from Account limit 1];
        pAcc2.PersonMailingState='MEXICO';
        update pAcc2;
        Final Opportunity oppData =[Select Id from Opportunity where AccountId=:pAcc2.Id limit 1];
        oppData.Name=NAME;
        oppData.StageName=System.Label.MX_SB_PS_Etapa1;
        update oppData;
        final Quote quot = [Select Id from Quote where OpportunityId=:oppData.Id limit 1];
        quot.Name=NAME;
        update quot;
        test.startTest();
        try {
            MX_RTL_Beneficiario_Selector.getBenefByAccId(condition, BENEFIELDS, quot.Id);
        } catch(Exception e) {
            System.assert(e.getMessage().contains('List'),'Success');
        }
        test.stopTest();
    }
}