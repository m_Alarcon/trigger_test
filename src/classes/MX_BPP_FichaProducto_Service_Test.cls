/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_FichaProducto_Ctrl_Test
* @Author   	Héctor Israel Saldaña Pérez | hectorisrael.saldana.contractor@bbva.com
* @Date     	Created: 2021-03-29
* @Description 	Test Class for MX_BPP_FichaProducto_Service code coverage
* @Changes
*
*/
@IsTest
public class MX_BPP_FichaProducto_Service_Test {

	/**
    * @description Test method which covers getDocIds() method from Service class
    * @params NA
    * @return void
    **/
	@IsTest
	static void getDocumentIdsTest() {
		final User usuarioAdmin = UtilitysDataTest_tst.crearUsuario('PruebaAdm', Label.MX_PERFIL_SystemAdministrator, 'BBVA ADMINISTRADOR');
		insert usuarioAdmin;

		Test.startTest();
		System.runAs(usuarioAdmin) {
			final Account acc = new Account();
			acc.FirstName = 'user BPyP';
			acc.LastName = 'Test Acc';
			acc.No_de_cliente__c = 'D2050394';
			acc.RecordTypeId = RecordTypeMemory_cls.getRecType('Account', 'MX_BPP_PersonAcc_Client');
			acc.PersonEmail = 'testusr.minuta@test.com';
			insert acc;

			final dwp_kitv__Visit__c visitTest = new dwp_kitv__Visit__c();
			visitTest.Name = 'Visita Prueba';
			visitTest.dwp_kitv__visit_start_date__c = Date.today()+4;
			visitTest.dwp_kitv__account_id__c = acc.Id;
			visitTest.dwp_kitv__visit_duration_number__c = '15';
			visitTest.dwp_kitv__visit_status_type__c = '01';
			insert visitTest;

			final ContentVersion cvTest = new ContentVersion();
			cvTest.Title = 'PenguinsBIE';
			cvTest.PathOnClient = 'PenguinBIE.pdf';
			cvTest.VersionData = Blob.valueOf('Unit Test ContentVersion');
			cvTest.ContentLocation = 'S';
			insert cvTest;

			final ContentVersion contentVersion2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id =: cvTest.Id LIMIT 1];
			final ContentDocumentLink contentDocLink = new ContentDocumentLink();
			contentDocLink.ContentDocumentId = contentVersion2.ContentDocumentId;
			contentDocLink.LinkedEntityId = visitTest.Id;
			contentDocLink.ShareType = 'V';
			insert contentDocLink;

			final List<ContentDocumentLink> lstCdl = new List<ContentDocumentLink>{contentDocLink};
			final Set<Id> result = MX_BPP_FichaProducto_Service.getDocIds(lstCdl);
			System.assertEquals(1, result.size(), 'No se encontraron registros relacionados');
		}
		Test.stopTest();
	}
}