/**
* @File Name          : MX_RTL_Campaign_Service_Test.cls
* @Description        : Test class for MX_RTL_Campaign_Service
* @Author             : Gerardo Mendoza Aguilar
* @Group              :
* @Last Modified By   : Gerardo Mendoza Aguilar
* @Last Modified On   : 11-18-2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      18/11/2020           Gerardo Mendoza Aguilar          Initial Version
**/
@isTest
public class MX_RTL_Campaign_Service_Test {
    
    /**
    * @description 
    * @author Gerardo Mendoza Aguilar | 11-18-2020 
    **/
    @TestSetup static void makeDataCampaign() {
        final Campaign camp = new Campaign(Name = 'Campaign Test', CampaignMemberRecordTypeId=Schema.SObjectType.CampaignMember.getRecordTypeInfosByDeveloperName().get('MX_BPP_CampaignMember').getRecordTypeId());
        insert camp;
    }

    /**
    * @description test method selectCampaignById
    * @author Gerardo Mendoza Aguilar | 11-18-2020 
    **/
    @isTest
    public static void campaignByIdTest() {
        Test.startTest();
        Final Id idCamp = [SELECT Id FROM Campaign LIMIT 1].Id;
        Final List<Campaign> camp = MX_RTL_Campaign_Service.selectCampaignById(idCamp);
        System.assert(!camp.isEmpty(), 'Campanya encontrada');
        Test.stopTest();

    }
}