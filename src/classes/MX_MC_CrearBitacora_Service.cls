/**
 * @File Name          : MX_MC_CrearBitacora_Service.cls
 * @Description        : Service de LWC MX_MC_CrearBitacora
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 16/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      16/06/2020           Jair Ignacio Gonzalez G.   Initial Version
**/
public without sharing class MX_MC_CrearBitacora_Service {


    /**
     * @Method MX_MC_CrearBitacora_Crtl
     * @Description Singletons
    **/
    @TestVisible private MX_MC_CrearBitacora_Service() {
    }

    /**
     * @Method convertLeads
     * @param Id idLead
     * @Description Convierte un Lead
     * @return Map<String,String>
    **/
    @AuraEnabled(cacheable=true)
    public static Boolean hasBitacora(Id idConv) {
        final List<dwp_kitv__Visit__c> listVisit = [SELECT Id, Name, BPyP_TipoDeLlamada__c, dwp_kitv__visit_status_type__c, Consulta_Saldos__c, BPyP_Comentarios__c FROM dwp_kitv__Visit__c WHERE MX_MC_IdConv__c =:idConv];
        return listVisit.isEmpty();
    }

    /**
     * @Method importBitacora
     * @param Id idConv
     * @Description Regresa lista de bitacoras
     * @return Map<String,String>
    **/
    @AuraEnabled(cacheable=true)
    public static dwp_kitv__Visit__c importBitacora(Id idConv) {
        final List<dwp_kitv__Visit__c> listVisit = [SELECT Id, Name, BPyP_TipoDeLlamada__c, dwp_kitv__visit_status_type__c, Consulta_Saldos__c, BPyP_Comentarios__c FROM dwp_kitv__Visit__c WHERE MX_MC_IdConv__c =:idConv];
        return listVisit[0];
    }
}