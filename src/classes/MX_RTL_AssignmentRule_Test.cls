/**
 * @description       : 
 * @author            : Eduardo Hernandez Cuamatzi
 * @group             : 
 * @last modified on  : 08-09-2020
 * @last modified by  : Eduardo Hernandez Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   08-09-2020   Eduardo Hernandez Cuamatzi   Initial Version
**/
@isTest
public class MX_RTL_AssignmentRule_Test {

    @isTest
    private static void findQueues() {
        final Set<String> lstAssigName = new Set<String>{'ASIGNACION DE COLAS LEADS'};
        final List<AssignmentRule> lstAssig = MX_RTL_AssignmentRule_Selector.selectAssigmentName(lstAssigName, true, 'Lead');
        System.assertEquals(lstAssig.size(),1 , 'Regla encontrada');
    }
}