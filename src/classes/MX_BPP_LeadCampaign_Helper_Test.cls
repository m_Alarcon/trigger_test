/**
* @File Name          : MX_BPP_LeadCampaign_Helper_Test.cls
* @Description        : Test class for MX_BPP_LeadCampaign_Helper
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 06/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      06/06/2020            Gabriel Garcia Rojas          Initial Version
**/
@isTest
private class MX_BPP_LeadCampaign_Helper_Test {
	/*Usuario de pruebas*/
    private static User testUser = new User();

    /** namerole variable for records */
    final static String NAMEROLE = '%BPYP BANQUERO BANCA PERISUR%';
    /** nameprofile variable for records */
    final static String NAMEPROFILE = 'BPyP Estandar';
    /** name variable for records */
    final static String NAME = 'testUser';
    /** error message */
    final static String MESSAGEFAIL = 'Fail method';
    /** name Account*/
    final static String NAMEACC = 'Cuenta Personal BPyP Test';
    /** name Objetc*/
    final static String NAMEOBJECT = 'Lead';
    /** name RT*/
    final static String NAMERECORDTYPE = 'MX_BPP_Leads';

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testUser = UtilitysDataTest_tst.crearUsuario(NAME, NAMEPROFILE, NAMEROLE);
        insert testUser;
        final Double numClienteRondom = Math.random()*10000000;

        final Account cuenta = new Account();
		cuenta.LastName = NAMEACC;
        cuenta.PersonEmail = 'test@test.com';
        cuenta.Phone = '55123456';
		cuenta.No_de_cliente__c = String.valueOf(numClienteRondom.round());
		cuenta.RecordTypeId = RecordTypeMemory_cls.getRecType('Account', 'MX_BPP_PersonAcc_Client');
		insert cuenta;

        final Lead candidato = new Lead();
        candidato.LastName = 'Candidato 1';
        candidato.MX_ParticipantLoad_id__c = cuenta.No_de_cliente__c;
        candidato.RecordTypeId = RecordTypeMemory_cls.getRecType(NAMEOBJECT, NAMERECORDTYPE);

        insert candidato;

    }

     /**
     * @description constructor test
     * @author Gabriel Garcia | 06/06/2020
     * @return void
     **/
    @isTest
    private static void testConstructor() {
        final MX_BPP_LeadCampaign_Helper instanceC = new MX_BPP_LeadCampaign_Helper();
        System.assertNotEquals(instanceC, null, MESSAGEFAIL);
    }

    /**
     * @description test method getLeadWithNodeCliente
     * @author Gabriel Garcia | 06/06/2020
     * @return void
     **/
    @isTest
    private static void getLeadWithNodeClienteTest() {
		final List<Lead> listLeads = [SELECT Id, MX_ParticipantLoad_id__c, OwnerId, RecordTypeId FROM Lead LIMIT 10];
		final Map<Id, RecordType> mapRecordType = MX_RTL_RecordType_Selector.getMapRecordType(NAMEOBJECT, NAMERECORDTYPE);

		final Lead candidato2 = new Lead();
        candidato2.LastName = 'Candidato 2';
        candidato2.RecordTypeId = RecordTypeMemory_cls.getRecType(NAMEOBJECT, NAMERECORDTYPE);

        listLeads.add(candidato2);

        Test.startTest();
        final Set<String> setClientLoadId =MX_BPP_LeadCampaign_Helper.getLeadWithNodeCliente(listLeads, mapRecordType);
        Test.stopTest();

        System.assertNotEquals(setClientLoadId.size(), 0, MESSAGEFAIL);
    }

    /**
     * @description test method assigendAccountToLead
     * @author Gabriel Garcia | 06/06/2020
     * @return void
     **/
    @isTest
    private static void assigendAccountToLeadTest() {
		final List<Lead> listLeads = [SELECT Id, FirstName, LastName, MX_ParticipantLoad_id__c, OwnerId, RecordTypeId FROM Lead LIMIT 10];
        final Map<Id, RecordType> mapRecordType = MX_RTL_RecordType_Selector.getMapRecordType(NAMEOBJECT, NAMERECORDTYPE);
        final List<Account> listAcc = [SELECT Id, LastName, Phone, PersonEmail, No_de_Cliente__c, OwnerId FROM Account WHERE Name=: NAMEACC];

        final Map<String, Account> mapCuentas = new Map<String, Account>();
        for(Account cuenta : listAcc) {
            mapCuentas.put(cuenta.No_de_cliente__c, cuenta);
        }

        final Lead candidato3 = new Lead();
        candidato3.LastName = 'Candidato 3';
        candidato3.MX_ParticipantLoad_id__c = '12345678';
        candidato3.RecordTypeId = RecordTypeMemory_cls.getRecType(NAMEOBJECT, NAMERECORDTYPE);

        listLeads.add(candidato3);

        Test.startTest();
        MX_BPP_LeadCampaign_Helper.assigendAccountToLead(listLeads, mapCuentas, mapRecordType);
        Test.stopTest();

        System.assertNotEquals(listLeads, null, MESSAGEFAIL);
    }


}