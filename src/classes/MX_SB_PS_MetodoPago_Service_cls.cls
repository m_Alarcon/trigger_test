/*
----------------------------------------------------------
* Nombre: MX_SB_PS_MethodPago_Service_cls
* Daniel Perez Lopez
* Proyecto: Salesforce-Presuscritos
* Descripción : clase service para acceder a selector y/o servicios desde controlador

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   Descripción
* --------------------------------------------------------------------------------
* 1.0           08/07/2020     Daniel Perez		        Creación
* 1.1           02/08/2021     Juan Benitez		     Methodo Busqueda de quote e IVRCodes
* 1.2           02/24/2021     Juan Benitez        Se añade methodo de busuqeda CS, y method sobj
* --------------------------------------------------------------------------------
*/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_SB_PS_MetodoPago_Service_cls {
    /**Campos basicos de metadata msjs IVR */
    final static String LSTFIELDSIVR = 'Id, MasterLabel, MX_RTL_IVRCode__c, MX_RTL_Group__c, MX_RTL_MsgIVR__c, MX_RTL_MsgSF__c, MX_RTL_TitleMsg__c';
    /**Campos basicos de custom Setting MX_SB_VTS_Generica__c */ 
    final static string QUERY= ' MX_SB_VTS_Description__c, MX_SB_VTS_HEADER__c, MX_SB_VTS_HREF__c, MX_SB_VTS_SRC__c, MX_SB_VTS_Type__c, Name ';
    /*Lista Generica*/
    Final static List<MX_SB_VTS_Generica__c> LSTGEN = new List<MX_SB_VTS_Generica__c>();
    /*
    * @description Acualiza oportunidad
    * @param Opportunidad opp
    */
    public static void actualizaoportunidad(Opportunity opp) {
        Final Opportunity[] lopp= new Opportunity[]{opp};
        MX_RTL_Opportunity_Selector.updateResult(lopp,true);
    }

    /*
    * @description method getoportunidad
    * @param String oppid
    */
    public static Opportunity getOpportunity(String oppid,String fields) {
        return MX_RTL_Opportunity_Selector.getOpportunity(oppid,fields);
    }
    
    /*
    * @description method getQuoteData
    * @param String idQuote
    */
    public static List<Quote> selectSObjectsById (String idQuote) {
        return MX_RTL_Quote_Selector.selectSObjectsById(idQuote);
    }
    /*
	*@description: method de busqueda codigos IVR
	*@param: ivrCode
	*Author: Juan Carlos Benitez
	*/
    public static List<MX_RTL_IVRCodes__mdt> findByTypeVal(String ivrCode) {
        final Set<String> lstTypes = new Set<String>{'VTS'};
        return MX_RTL_IVRCodes_Selector.findByTypeVal(LSTFIELDSIVR, lstTypes, ivrCode);
    }
    /*
	* Description: Busqueda de mensajes a Custom Setting Generica
	* Author: Juan Carlos Benitez
	* param: String mensaje
	*/
    public static List<MX_SB_VTS_Generica__c> findTGenericaOP(string mensaje) {
        return MX_SB_VTS_Generica_Selector.findIvrbyMsj(mensaje,QUERY);
    }
    
    /*
	* Description: Busqueda Generica de objetos por unificacion de methodos
	* Author: Juan Carlos Benitez Herrera
	* param: string str, string tipo
	*/
    
    public static List<Sobject> findGenericObject(String str, String tipo ) {
        List<Sobject> sobj= new List<Sobject>();
        switch on tipo {
            when 'ivr' {
                sobj=findByTypeVal(str);
            }
            when 'quot' {
                sobj=selectSObjectsById(str);
            }
            when 'outToOP' {
                sobj=findTGenericaOP(str);
            }
        }
        return sobj;
    }
}