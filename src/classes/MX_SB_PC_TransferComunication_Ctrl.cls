/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 10-08-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   09-10-2020   Eduardo Hernández Cuamatzi   Initial Version
 * 1.0   08-10-2020   Omar Gonzalez                Editor
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_PC_TransferComunication_Ctrl {
    /** Variable con el nombre de conjunto de permisos de envio a cobro por IVR */
    private static String permissionIvr = 'MX_RTL_PCI_EnvioIvr';
    /**
     * @description: Recupera los datos de callcenter
     * @return: 	 Url callcenter lightning
     */
    @AuraEnabled
    public static String callCenterUrl() {
        final String userId = UserInfo.getUserId();
        return MX_SB_PC_TransferComunication_Service.findUrlCallCenter(userId);
    }
    /**
     * @description: Recupera idcotiza y token antifraude de la cotizacion actual
     * @return: Quote	 
     */
    @AuraEnabled
    public static Quote getQuoteIvr(String idquoteopp,String quotefol) {
        return MX_RTL_Quote_Service.getRetriveQuote(idquoteopp, quotefol);
    }
    /**
     * @description: Recupera La respuesta del cobro por IVR
     * @return: Quote	 
     */
    @AuraEnabled
    public static Quote getResIvr(String quotefol, String idCotiza) {
        return MX_RTL_Quote_Service.getResIvr(quotefol,idCotiza);
    }
    /**
     * @description: Recupera el estatus de la Custom Setting
     * @return: CustomSetting	 
     */
    @AuraEnabled
    public static Boolean activeLabel() {
        final MX_RTL_PCI_ParametroIvr__c setgCot = MX_RTL_PCI_ParametroIvr__c.getInstance();
        return setgCot.MX_RTL_PCI_Activo__c ? true : false;
    }
    /** 
     * @description: valida si el usuario actual tiene el conjunto de permisos de envio a cobro por IVR
     */
    @AuraEnabled
    public static Boolean userInfPermission() {
        Boolean cotizaIvr;
        final String userid = UserInfo.getUserId();
        final List<PermissionSetAssignment> permissionUser = MX_RTL_PermissionSetAssignment_Selector.selectByUserIdWithName(userid, permissionIvr);
        if(!permissionUser.isEmpty() && userid == permissionUser[0].Assignee.Id) {
            cotizaIvr = true;
        } else {
            cotizaIvr = false;
        }
        return cotizaIvr;
    }
    /***Funcion que retorna las ultimas 5 cotizaciones de una oportunidad */
    @AuraEnabled
    public Static List<Quote> retrieveOpps(String oppsId) {
        return MX_SB_VTS_QuoteFinder.getLast5Quotes(oppsId);
    }
}