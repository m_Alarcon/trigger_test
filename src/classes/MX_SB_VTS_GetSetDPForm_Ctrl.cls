/**
 * @File Name          : MX_SB_VTS_GetSetDPForm_Ctrl.cls
 * @Description        : Formulario - Dirección de la Propiedad
 * @Author             : Alexandro Corzo
 * @Group              :
 * @Last Modified By   : Alexandro Corzo
 * @Last Modified On   : 15/7/2020 10:30:00
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0       15/7/2020       Alexandro Corzo        Initial Version
 * 1.2       12/01/2021      Alexandro Corzo        Ajustes a la clase consumo ASO
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_GetSetDPForm_Ctrl {
    /**
     * @description: Recupera del WS siguiente información: Colonia, Estado, Ciudad y Alcaldia
     *               conforme al CP ingresado.
     * @author: 	 Alexandro Corzo
     * @return: 	 Map<String,List<Object>> oReturnValues
     */
    @AuraEnabled(cacheable=true)
    public static Map<String, List<Object>> dataInputCP(String sCodigoPostal) {
        return MX_SB_VTS_GetSetDPForm_Service.dataInputCPMockService(sCodigoPostal);
    }
}