/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RTL_PermissionSetAssignment_Sel_Test
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-10-19
* @Description 	Test Class for MX_RTL_PermissionSetAssignment_Selector
* @Changes
*  
*/
@isTest
public class MX_RTL_PermissionSetAssignment_Sel_Test {
    
    /*String nombre de PermissionSet BPyP FactSet */
    static final String STR_FSET_PS_NAME = 'MX_BPP_Factset';
    
    @testSetup
    static void setup() {
        final User usuarioAdm = UtilitysDataTest_tst.crearUsuario('PruebaAdmin', Label.MX_PERFIL_SystemAdministrator, 'BBVA ADMINISTRADOR');
        insert usuarioAdm;
    }
    
    /**
    * @Description 	Test Method for MX_RTL_PermissionSetAssignment_Selector.selectByUserIdWithName()
    * @Return 		NA
    **/
	@isTest
    static void selectByUserIdWithNameTest() {
        final User userTest = [SELECT Id FROM User WHERE Name = 'PruebaAdmin' LIMIT 1];
        Test.startTest();
        final List<PermissionSetAssignment> result = MX_RTL_PermissionSetAssignment_Selector.selectByUserIdWithName(userTest.Id, STR_FSET_PS_NAME);
        Test.stopTest();
        
        System.assert(result.isEmpty(), 'Conflicto en PermissionSetassignment');
    }

}