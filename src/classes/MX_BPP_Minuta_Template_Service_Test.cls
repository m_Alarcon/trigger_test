/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Minuta_Template_Service_Test
* @Author   	Héctor Saldaña | hectorisrael.saldana.contractor@bbva.com
* @Date     	Created: 2021-01-22
* @Description 	Test Class for BPyP Minuta Template Service Layer
* @Changes
* 2021-22-02	Edmundo Zacarias		New test methods added constructorTest, getImageResourcesTest, replaceInnerTagsTest
*/
@isTest
private class MX_BPP_Minuta_Template_Service_Test {

	/** BPYP Account RecordType */
	static final String STR_RT_BPYPPACC = 'MX_BPP_PersonAcc_Client';

	/** Email test for Account and Contact **/
	static final String STR_EMAIL = 'test.minuta.service@test.com';

	@testSetup
	static void setup() {
		final User usuarioAdmManager = UtilitysDataTest_tst.crearUsuario('Manager', Label.MX_PERFIL_SystemAdministrator, 'BBVA ADMINISTRADOR');
		usuarioAdmManager.Email = 'managerUser@testDomain.com';
		insert usuarioAdmManager;

		final User usuarioAdm = UtilitysDataTest_tst.crearUsuario('PruebaAdmin', Label.MX_PERFIL_SystemAdministrator, 'BBVA ADMINISTRADOR');
		usuarioAdm.Email = 'testUser@testDomain.com';
		usuarioAdm.ManagerId = usuarioAdmManager.Id;
		insert usuarioAdm;


		System.runAs(usuarioAdm) {
			final gcal__GBL_Google_Calendar_Sync_Environment__c calEnviroment = new gcal__GBL_Google_Calendar_Sync_Environment__c(Name = 'DEV');
			insert calEnviroment;

			final Account newAccount = new Account();
			newAccount.FirstName = 'BPyP User';
			newAccount.LastName = 'Tst';
			newAccount.RecordTypeId = RecordTypeMemory_cls.getRecType('Account', STR_RT_BPYPPACC);
			newAccount.No_de_cliente__c = 'D2050394';
			newAccount.PersonEmail = STR_EMAIL;
			insert newAccount;

			final List<String> contactData = new List<String>{'Jose', 'Perez Garcia', 'Decisor', 'CONTACTO EJECUTIVO', null};
			final Contact contactRec = EU_cls_TestData.generaContactoB(contactData);
			contactRec.Email = STR_EMAIL;
			insert contactRec;

			final User tstUser = EU_cls_TestData.insertUser();
			final RecordType[] ltsRType=[SELECT id,name FROM RecordType where SobjectType='dwp_kitv__Visit__c' ORDER BY Name ASC];
			final dwp_kitv__Visit__c visit = EU_cls_TestData.insertASCVisit(newAccount.Id, ltsRType[0].id);
			final dwp_kitv__Visit__c visitBIE = EU_cls_TestData.insertBIEVisit(newAccount.Id);

			EU_cls_TestData.insertTeam(visit.Id, tstUser.Id);

			final dwp_kitv__Visit_Contact__c contactoRel = new dwp_kitv__Visit_Contact__c();
			contactoRel.dwp_kitv__contact_id__c = contactRec.Id;
			contactoRel.dwp_kitv__visit_id__c = visit.Id;
			insert contactoRel;

			EU_cls_TestData.insertCtmSetting(ltsRType[0].Name);

			final List<ContentVersion> ltsContV = new List<ContentVersion>();
			final ContentVersion cVersion = new ContentVersion(
					VersionData = Blob.valueOf('Test Content'),
                	PathOnClient = 'Penguins.pdf',	
                	Title = 'Penguins',
					FirstPublishLocationId=visit.Id
			);
			insert cVersion;
			ltsContV.add(cVersion);

			final ContentVersion cVersionBIE = new ContentVersion(
					VersionData = Blob.valueOf('Test ContentBIE'),
					PathOnClient = 'PenguinsBIE.pdf',
					Title = 'PenguinsBIE',
					FirstPublishLocationId=visitBIE.Id
			);
			insert cVersionBIE;
			ltsContV.add(cVersionBIE);

			Document docRecord;
			docRecord = new Document();
			docRecord.Body = Blob.valueOf('Some Document Text');
			docRecord.ContentType = 'application/pdf';
			docRecord.DeveloperName = 'PenguinsDoc';
			docRecord.IsPublic = true;
			docRecord.Name = 'PenguinsDoc';
			docRecord.FolderId = UserInfo.getUserId();
			insert docRecord;

			final MX_BPP_CatalogoMinutas__c catalogoTest = createCatalogoMinuta('BienvenidaDO', 'BienvenidaDO');
			insert catalogoTest;

			final dwp_kitv__Template_for_type_of_visit_cs__c myCS = createCS('TestCS');
			insert myCS;
		}
	}

	/**
    * @Description 	Test Method for MX_BPP_Minuta_Template_Service.getUserDetails()
    * @Return 		NA
    **/
	@isTest
	static void testGetUserDetails() {
		final User userAdmin = [SELECT Id FROM User WHERE Name = 'PruebaAdmin'];
		test.startTest();
		final User resultUser = MX_BPP_Minuta_Template_Service.getUserDetails(userAdmin.Id);
		System.assertEquals(resultUser.Id, userAdmin.Id, 'Verificar informacion');
		test.stopTest();
	}

	/**
    * @Description 	Test Method for MX_BPP_Minuta_Template_Service.getCatalogoMinuta()
    * @Return 		NA
    **/
	@isTest
	static void testGetCatalogo() {
		final MX_BPP_CatalogoMinutas__c lclCatalogo = [SELECT Id, Name, MX_Acuerdos__c, MX_Saludo__c, MX_ImageHeader__c FROM MX_BPP_CatalogoMinutas__c WHERE Name= 'BienvenidaDO' LIMIT 1];
		final MX_BPP_CatalogoMinutas__c resultCatalogo = MX_BPP_Minuta_Template_Service.getCatalogoMinuta(lclCatalogo.Id);
		System.assertEquals(lclCatalogo.Name, resultCatalogo.Name, 'Verificar información');
	}

	/**
    * @Description 	Test Method for MX_BPP_Minuta_Template_Service.sendTemplateEmail()
    * @Return 		NA
    **/
	@isTest
	static void testSendEmailTemplate() {
        final MX_BPP_CatalogoMinutas__c catalogoTest = [SELECT Id, Name, MX_Asunto__c, MX_CuerpoCorreo__c,MX_Acuerdos__c, MX_Saludo__c, MX_Contenido__c, MX_Despedida__c, MX_Firma__c, MX_ImageHeader__c FROM MX_BPP_CatalogoMinutas__c WHERE Name= 'BienvenidaDO' LIMIT 1];
		List<ContentVersion> lstDocuments = new List<ContentVersion>();
		final User userAdmin = [SELECT Id, Manager.Id, Email, Manager.Email FROM User WHERE Name = 'PruebaAdmin' LIMIT 1];

		final dwp_kitv__Template_for_type_of_visit_cs__c myCS = [Select Id, Name, dwp_kitv__Attach_file__c, dwp_kitv__Disable_send_field_in_BBVA_team__c,
														  dwp_kitv__Disable_send_field_in_contacts__c,dwp_kitv__Email_Template_Name__c, dwp_kitv__Minimum_Number_of_Agreement__c,
														  dwp_kitv__Minimum_members_BBVA_team__c, dwp_kitv__Multiple_templates_type__c, dwp_kitv__Subject__c, dwp_kitv__validation_main_contact__c,
														  dwp_kitv__Visualforce_Name__c FROM dwp_kitv__Template_for_type_of_visit_cs__c WHERE Name = 'TestCS'];


		final dwp_kitv__Visit__c testVisit = [SELECT Id, RecordType.Name, dwp_kitv__visit_status_type__c, (Select Id from dwp_kitv__Activities__r) FROM dwp_kitv__Visit__c WHERE Name = 'Visita Prueba' LIMIT 1];
		lstDocuments = [SELECT Title, PathOnClient, VersionData, FirstPublishLocationId FROM ContentVersion WHERE FirstPublishLocationId =: testVisit.Id];
		final String strCS = JSON.serialize(myCS);
		final String lstDocsStr = JSON.serialize(lstDocuments);
		Test.startTest();
		final PageReference testPage = Page.MX_BPP_Minuta_BienvenidaDO_VF;
		Test.setCurrentPage(testPage);
		ApexPages.currentPage().getParameters().put('Id', testVisit.Id);
        ApexPages.currentPage().getParameters().put('sCatalogoId', catalogoTest.Id);
		ApexPages.currentPage().getParameters().put('myCS', strCS );
		ApexPages.currentPage().getParameters().put('documents', lstDocsStr);
		ApexPages.currentPage().getParameters().put('textArray', '[]');
		ApexPages.currentPage().getParameters().put('switchArray', '[]');
		ApexPages.currentPage().getParameters().put('seleccionArray', '[]');
		MX_BPP_Minuta_Template_Service.sendTemplateEmail(testVisit, myCS, lstDocuments,  testVisit.dwp_kitv__visit_status_type__c, userAdmin, false, testPage, catalogoTest);
		System.assertEquals('05', testVisit.dwp_kitv__visit_status_type__c, 'Verificar informacion');
		test.stopTest();
	}

    /**
    * @Description 	Test Method for constructor
    * @Return 		NA
    **/
	@isTest
    static void constructorTest() {
        final MX_BPP_Minuta_Template_Service constInstance = new MX_BPP_Minuta_Template_Service();
        System.assert(constInstance <> null, 'Error on constructor');
    }

    /**
    * @Description 	Test Method for getImageResources
    * @Return 		NA
    **/
	@isTest
    static void getImageResourcesTest() {
        final List<String> resourcesNames = new List<String>();
        resourcesNames.add('PenguinsDoc');
        final Map<String, String> urlsMap = MX_BPP_Minuta_Template_Service.getImageResources(resourcesNames);
        System.assert(!urlsMap.isEmpty(), 'Error on getImageResources');
    }

    /**
    * @Description 	Test Method for getImageResources
    * @Return 		NA
    **/
	@isTest
    static void replaceInnerTagsTest() {
        final MX_BPP_CatalogoMinutas__c catalogoTst = [SELECT Id, Name, MX_CuerpoCorreo__c, MX_Acuerdos__c, MX_Saludo__c, MX_Contenido__c, MX_Despedida__c, MX_Firma__c, MX_ImageHeader__c, MX_Opcionales__c, MX_HasCheckbox__c, MX_HasPicklist__c, MX_HasText__c FROM MX_BPP_CatalogoMinutas__c WHERE Name= 'BienvenidaDO' LIMIT 1];
        final dwp_kitv__Visit__c tstVisit = [SELECT Id, RecordType.Name, dwp_kitv__visit_status_type__c FROM dwp_kitv__Visit__c WHERE Name = 'Visita Prueba' LIMIT 1];
        final String contBefore = catalogoTst.MX_Contenido__c;
        Test.startTest();
		final PageReference testPage = Page.MX_BPP_Minuta_BienvenidaDO_VF;
		Test.setCurrentPage(testPage);
		ApexPages.currentPage().getParameters().put('Id', tstVisit.Id);
		ApexPages.currentPage().getParameters().put('sCatalogoId', catalogoTst.Id);
		ApexPages.currentPage().getParameters().put('textArray', '[]');
		ApexPages.currentPage().getParameters().put('switchArray', '[]');
		ApexPages.currentPage().getParameters().put('seleccionArray', '[]');
        MX_BPP_Minuta_Template_Service.replaceInnerTags(catalogoTst, tstVisit.Id);
        Test.stopTest();
        System.assertNotEquals(contBefore, catalogoTst.MX_Contenido__c, 'Error on replaceInnerTags');
    }

	/**
	* @Description 	helper method to create custom settings
	* @Param		String-Name Custom Setting Name
	* @Return 		dwp_kitv__Template_for_type_of_visit_cs__c
	**/
	private static dwp_kitv__Template_for_type_of_visit_cs__c createCS(String name) {
		final dwp_kitv__Template_for_type_of_visit_cs__c myCS = new dwp_kitv__Template_for_type_of_visit_cs__c();
		myCS.Name = name;
		myCS.dwp_kitv__Attach_file__c = true;
		myCS.dwp_kitv__Disable_send_field_in_BBVA_team__c = true;
		myCS.dwp_kitv__Disable_send_field_in_contacts__c = true;
		myCS.dwp_kitv__Email_Template_Name__c = 'Test Template';
		myCs.dwp_kitv__Minimum_Number_of_Agreement__c = 0;
		myCS.dwp_kitv__Minimum_members_BBVA_team__c = 0;
		myCS.dwp_kitv__Multiple_templates_type__c = false;
		myCS.dwp_kitv__Subject__c = 'Test Subject';
		myCS.dwp_kitv__validation_main_contact__c = false;
		myCS.dwp_kitv__Visualforce_Name__c = 'MX_BPP_Minuta_BienvenidaDO_VF';

		return myCS;
	}

	private static MX_BPP_CatalogoMinutas__c createCatalogoMinuta(String nameCatalogo, String tipovisita) {

		final MX_BPP_CatalogoMinutas__c catalogoRecord = new MX_BPP_CatalogoMinutas__c();
		catalogoRecord.MX_Acuerdos__c = 1;
		catalogoRecord.MX_Asunto__c = 'Prueba Catalogo';
		catalogoRecord.Name = nameCatalogo;
		catalogoRecord.MX_TipoVisita__c = tipovisita;
        catalogoRecord.MX_Saludo__c = 'Estimado {NombreCliente}';
        catalogoRecord.MX_Contenido__c = 'Contenido Test {NombreBanquero} {OpcionalTexto1}';
        catalogoRecord.MX_Despedida__c = 'Saludos Cordiales';
        catalogoRecord.MX_Firma__c = 'Firma Test';
        catalogoRecord.MX_HasText__c = true;
        catalogoRecord.MX_HasPicklist__c = true;
        catalogoRecord.MX_HasCheckbox__c = true;
        catalogoRecord.MX_Opcionales__c = '{OpcionalTexto1}TestTexto{OpcionalTexto1}';
		catalogoRecord.MX_ImageHeader__c = 'Imagen_Header1';
		catalogoRecord.MX_CuerpoCorreo__c = 'Cuerpo del correo';
		return catalogoRecord;
	}
}