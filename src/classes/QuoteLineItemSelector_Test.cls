/**
 * @File Name          : QuoteLineItemSelector_Test.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernandez Cuamatzi
 * @Last Modified On   : 26/5/2020 14:04:46
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    26/5/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
@isTest
private class QuoteLineItemSelector_Test {
    /**@description etapa Cotizacion*/
    private final static string COTIZACION = 'Cotización';
    /**@description Nombre usuario*/
    private final static string ASESORNAME = 'AsesorTest';
    /**@description Nombre permiso hogar*/
    private final static string PERMISOHOGAR = 'MX_SB_VTS_Hogar';
    /**@description Nombre permiso vida*/
    private final static string PERMISOVIDA = 'MX_SB_VTS_Vida';
    /**@description Nombre permiso contratos h*/
    private final static string PERMISOHOGARC = 'MX_SB_VTS_Permisos_Contrato_Hogar';
    /**@description Nombre permiso contratos v*/
    private final static string PERMISOVIDAC = 'MX_SB_VTS_Permisos_Contrato_Vida';
    /**@description Nombre permiso contratos v*/
    private final static string CLASESVTS = 'MX_SB_VTS_Ventas';
    /**@description Nombre permiso contratos v*/
    private final static string PERMISOOBJECIONES = 'MX_SB_VTS_Objeciones';

    
    @TestSetup
    static void makeData() {
        final User asesorUser = MX_WB_TestData_cls.crearUsuario(ASESORNAME, 'Telemarketing VTS');
        Insert asesorUser;

        final MX_WB_FamiliaProducto__c objFamilyPro2 = MX_SB_VTS_CallCTIs_utility.newFamiliy('Hogar');
        insert objFamilyPro2;
        
        final Product2 proHogar = MX_SB_VTS_CallCTIs_utility.newProduct(System.Label.MX_SB_VTS_Hogar, objFamilyPro2);
        insert proHogar;
        
        final MX_SB_VTS_ProveedoresCTI__c smartProv = MX_SB_VTS_CallCTIs_utility.newProvee('Smart Center', 'Smart Center');
        insert  smartProv;
        
        final Account accToAssign = MX_WB_TestData_cls.crearCuenta('Donnie', 'PersonAccount');
        accToAssign.PersonEmail = 'donnie.test@test.com';
        insert accToAssign;

        final Opportunity oppHsd = MX_WB_TestData_cls.crearOportunidad('OppTelemarketing', accToAssign.Id, asesorUser.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
        oppHsd.Producto__c = System.Label.MX_SB_VTS_Hogar;
        oppHsd.Reason__c = 'Venta';
        oppHsd.Reason__c = 'Venta';
        oppHsd.StageName = COTIZACION;
        insert oppHsd;
        final Pricebook2 prB2 = MX_WB_TestData_cls.createStandardPriceBook2();
        final Quote quoteTemp = MX_WB_TestData_cls.crearQuote(oppHsd.Id, '999999 1 OppTest Close Opp', '999990');
        quoteTemp.Pricebook2Id= prB2.Id;
        quoteTemp.OwnerId = asesorUser.Id;
        insert quoteTemp;
        final PricebookEntry pbEntry = MX_WB_TestData_cls.priceBookEntryNew(proHogar.Id);
        insert pbEntry;
        final QuoteLineItem qltem = new QuoteLineItem(
            QuoteId=quoteTemp.Id,
            PricebookEntryId=pbEntry.Id,
            Quantity=1,
            UnitPrice=1,
            Product2Id=proHogar.Id);
        insert qltem;
    }

    @isTest
    static void initDataCotiz() {
        final User asesorUser = [Select Id from User where LastName =: ASESORNAME];
        insertPermissionSet(PERMISOHOGAR, asesorUser.Id);
        insertPermissionSet(PERMISOVIDA, asesorUser.Id);
        insertPermissionSet(CLASESVTS, asesorUser.Id);
        insertPermissionSet(PERMISOOBJECIONES, asesorUser.Id);
        insertPermissionSet(PERMISOHOGARC, asesorUser.Id);
        insertPermissionSet(PERMISOVIDAC, asesorUser.Id);
        insertPermissionSet('MX_SB_VTS_VistasTrackingVCIP', asesorUser.Id);
        Test.startTest();
        System.runAs(asesorUser) {
            final Set<Id> lstQuotes = new Set<Id>{};
            final Id quoteId = [Select Id from Quote where MX_SB_VTS_Folio_Cotizacion__c = '999990'].Id;
            lstQuotes.add(quoteId);
            final List<QuoteLineItem> quoliDat = QuoteLineItemsSelector.selSObjectsByQuotes(lstQuotes);
            System.assertEquals(quoliDat.size(),1, 'Datos recuperados');
        }
        Test.stopTest();
    }

    /**
    * @description Inserta permission sets
    * @author Eduardo Hernandez Cuamatzi | 26/5/2020 
    * @param String permissionName 
    * @param Id userId id de usuario
    * @return void 
    **/
    static void insertPermissionSet(String permissionName, Id userId) {
        final PermissionSet permissions = [SELECT Id FROM PermissionSet WHERE Name =: permissionName];
        insert new PermissionSetAssignment(AssigneeId = userId, PermissionSetId = permissions.Id);
    }
}