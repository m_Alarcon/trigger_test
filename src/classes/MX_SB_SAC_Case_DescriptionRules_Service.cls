/**
 * @description       :
 * @author            : Miguel Hernandez
 * @group             :
 * @last modified on  : 08-09-2020
 * @last modified by  : Miguel Hernandez
 * Modifications Log
 * Ver   Date         Author          Modification
 * 1.0   07-14-2020   Miguel Hernandez   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_SAC_Case_DescriptionRules_Service {

    /** array of options for case.reason */
    final static String[] REASON_VALUES = new String[]{'Cancelación', 'Retención'};

    /**
    * @description
    * @author Miguel Hernandez | 08/09/2020
    * @param nCase
    * @param oldCase
    * @return void
    **/
    public static void retencionRules(Case nCase, Case oldCase) {
        switch on nCase.MX_SB_SAC_Ramo__c {
            when 'Vida', 'LBBVA Vida' {
                MX_SB_SAC_Case_DescriptionRules_Helper.checkIfMVVida(nCase);
                MX_SB_SAC_Case_DescriptionRules_Helper.preventCommentsOnReasonVida(nCase);
            }
            when else {
                MX_SB_SAC_CaseValidation_Ext.checkIfMV(nCase);
                MX_SB_SAC_CaseValidation_Ext.preventCommentsOnReason(nCase);
            }
        }
        MX_SB_SAC_CaseValidation_Ext.validateBeforeUserAuth(nCase, oldCase, REASON_VALUES[1]);
        if(String.isBlank(nCase.Description) && nCase.MX_SB_SAC_FinalizaFlujo__c == true) {
            MX_SB_SAC_Case_DescriptionRules_Helper.validateMVComments(nCase, oldCase, REASON_VALUES[1]);
        }
    }
    
            /**
    * @description
    * @author Alan Santacruz | 27/10/2020
    * @param nCase
    * @return void
    **/
    public static void requestCommentsOn1500Vida(Case nCase, Case oldCase) {
        final String[] valuesToExclude = new String[]{System.Label.MX_SB_SAC_Transferencia, System.Label.MX_SB_SAC_Consultas};
        if((String.isBlank(nCase.Description) || nCase.Description.length() < 10)
        && oldCase.MX_SB_1500_isAuthenticated__c == true
        && nCase.MX_SB_SAC_ActivarReglaComentario__c == true
        && nCase.Reason.equals(oldCase.Reason)
        && !valuesToExclude.contains(nCase.Reason)
        && String.isNotBlank(nCase.MX_SB_SAC_Detalles_Vida__c)
        && Trigger.isUpdate) {
            nCase.addError(System.Label.MX_SB_SAC_RequestComments);
        }
    }

}