/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpContratacionSinCobro
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
 @SuppressWarnings('sf:ShortVariable')
public class MX_SB_PS_wrpContratacionSinCobro {
      /*Public property for wrapper*/
	public terms[] terms {get;set;}
	  /*Public subclass for wrapper*/
    public MX_SB_PS_wrpContratacionSinCobro() {
	     /*Public property for wrapper*/
        terms = new terms[] {};
		terms.add(new terms());
    }
	  /*Public subclass for wrapper*/
	public class terms {
	    /*Public property for wrapper*/
		public termType termType {get;set;}
		  /*Public property for wrapper*/
		public document document {get;set;}
		  /*Public property for wrapper*/
		public acceptanceMethod acceptanceMethod {get;set;}
          /*Public subclass for wrapper*/
		  /*Public constructor for wrapper*/
		public terms() {
            termType = new termType();
            document = new document();
            acceptanceMethod  = new acceptanceMethod();
        }
	}
	  /*Public subclass for wrapper*/
	public class termType {
	  /*Public property for wrapper*/
		 public string id {get;set;} 
      	 /*public constructor subclass*/
		public termType() {
		 this.id = '';
		}	
	}
	  /*Public subclass for wrapper*/
	public class document {
	  /*Public property for wrapper*/
		 public string id {get;set;} 
       /*public constructor subclass*/
		public document() {
		 this.id = '';
		}		
	}
	  /*Public subclass for wrapper*/
	public class acceptanceMethod {
	  /*Public property for wrapper*/
		 public string id {get;set;} 
		 /*public constructor subclass*/
		public acceptanceMethod() {
		 this.id = '';
		}
	}

}