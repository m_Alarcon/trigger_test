/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase Selector para objeto dwp_kitv_Visit_Topic

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     20/05/2020      Roberto Isaac Soto Granados           Creación Clase
*****************************************************************************************/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class Dwp_kitv_Visit_Topic_Selector {
    /*Inserta temas a tratar con casos relacionados*/
    public static void creaTemas(List<dwp_kitv__Visit_Topic__c> temas) {
        insert temas;
    }
}