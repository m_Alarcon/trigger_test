/**
 * @File Name          : QuoteLineItemsSelector.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 26/5/2020 15:44:49
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    25/5/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class QuoteLineItemsSelector { 
    /**
    * @description Recuperar Lista de QuoteLineItems
    * @author Eduardo Hernandez Cuamatzi | 25/5/2020 
    * @param Set<Id> lstQuotes lista de Quotes a evaluar
    * @return List<QuoteLineItem> lista de QuoteLineItems resultantes
    **/
    public static List<QuoteLineItem> selSObjectsByQuotes(Set<Id> lstQuotes) {
        return [Select Id,MX_SB_VTS_Total_Gastos_Funerarios__c, MX_WB_Folio_Cotizacion__c , MX_SB_VTS_Plan__c, Product2Id, CreatedDate, LastModifiedDate, TotalPrice,QuoteId,Quantity,MX_SB_PS_Dias6a35__c,MX_SB_PS_Dias3a5__c,MX_SB_PS_Dias36omas__c, 
        MX_SB_VTS_Tipo_Plan__c, MX_SB_VTS_Monto_Mensualidad__c, MX_SB_VTS_Frecuencia_de_Pago__c,
        MX_SB_VTS_FormaPAgo__c, Product2.Name from QuoteLineItem where QuoteId IN: lstQuotes];
    }
}