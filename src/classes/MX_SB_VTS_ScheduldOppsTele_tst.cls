/**
 * @File Name          : MX_SB_VTS_ScheduldOppsTele_tst.cls
 * @Description        : 
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernandez Cuamatzi
 * @Last Modified On   : 10-01-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    7/10/2019   Eduardo Hernández Cuamatzi     Initial Version
**/
@isTest
public class MX_SB_VTS_ScheduldOppsTele_tst { //NOSONAR

    /**Request service */
    final static String TXTPOST = 'POST', URLLOCAL = 'local.MXSBVTSCache',URLEXA='http://www.example.com';
    /**Producto HSD */
    final static String PRODHSD = 'Hogar seguro dinámico';
    
    
    @TestSetup
    static void makeData(){
        initSendTeleOpps();
    }

    /**
    * @description Inicia datos de Opps para envios
    * @author Eduardo Hernandez Cuamatzi | 10-02-2020 
    **/
    public static void initSendTeleOpps() {
        final Id idAgente = UserInfo.getUserId();
        final Account accRec = MX_WB_TestData_cls.crearCuenta('LastName', 'PersonAccount');
        accRec.Phone = '5512345678';
        insert accRec;

        final MX_SB_VTS_Generica__c setting = MX_SB_VTS_CallCTIs_utility.genricaSmart();
        insert setting;
        final MX_WB_FamiliaProducto__c objFamilyPro2 = MX_SB_VTS_CallCTIs_utility.newFamiliy('Hogar');
        insert objFamilyPro2;
        final Product2 proHogar = MX_SB_VTS_CallCTIs_utility.newProduct(PRODHSD, objFamilyPro2);
        insert proHogar;

        final Opportunity opp = MX_WB_TestData_cls.crearOportunidad('LastName', accRec.Id, idAgente, System.Label.MX_SB_VTS_Telemarketing_LBL);
        opp.Producto__c = PRODHSD;
        opp.LeadSource = 'Tracking web';
        opp.TelefonoCliente__c = '5512341231';
        opp.FolioCotizacion__c = '1042695';
        insert opp;

        final ProcNotiOppNoAten__c objProNotO = MX_SB_VTS_CallCTIs_utility.newProcNoct('30', 'Matutino', System.Label.MX_SB_VTS_SchdProces, false);
        insert objProNotO;
        
        final ProcNotiOppNoAten__c objProNotCup = MX_SB_VTS_CallCTIs_utility.newProcNoct('0', 'Matutino', System.Label.MX_SB_VTS_SchdProcesCup, false);
        insert objProNotCup;
        
        final MX_SB_VTS_Generica__c generica = MX_WB_TestData_cls.GeneraGenerica('HogarSeguro', 'CP10');
        generica.MX_SB_SAC_ProductFam__c = PRODHSD;
        insert generica;

        insert new iaso__GBL_Rest_Services_Url__c(Name = 'ProxyValueCipher', iaso__Url__c = URLEXA, iaso__Cache_Partition__c = URLLOCAL);
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'GetQuotation', iaso__Url__c = URLEXA, iaso__Cache_Partition__c = URLLOCAL);
    }

    @isTest
    private static void sendOppsSmart() {
        Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_Integration_MockGenerator());
        iaso.GBL_Mock.setMock(new MX_SB_VTS_Integration_MockGenerator());
        final String sQuery = fillQuery();
        MX_SB_VTS_SendTrackingTelemarketing_cls sh1 = new MX_SB_VTS_SendTrackingTelemarketing_cls(sQuery, 1);
        Database.BatchableContext batchContext; 
        Test.startTest();
        Final Map<String,String> tsecTst = new Map<String,String> {'tsec'=>'2122113'};
        Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, 'Complete', '{"results": ["87yiUyhpiiC27LJUwcMY067LJLdm9tNfwLXucJtWpKw"]}', tsecTst); 
        iaso.GBL_Mock.setMock(mockCallout);
        final List<Opportunity> lsOpps = DataBase.query(sQuery);
            sh1.execute(batchContext, lsOpps);
        System.assertEquals(lsOpps.size(), 1, 'Execute correcto');
        Test.stopTest(); 
    }
    
    @isTest 
    private static void sendOppsSmartBch() {
        final ProcNotiOppNoAten__c programSch = [Select Id, Activo__c from ProcNotiOppNoAten__c where Proceso__c =: System.Label.MX_SB_VTS_SchdProces];
        Test.startTest();
			programSch.Activo__c = true;
			update programSch;	
			System.assert(programSch.Activo__c, 'Se hizo el cambio correctamente');
		Test.stopTest();
    }

    @isTest 
    private static void sendOppsSmartBchCup() {
        final ProcNotiOppNoAten__c objProNotCup = [Select Id, Activo__c from ProcNotiOppNoAten__c where Proceso__c =: System.Label.MX_SB_VTS_SchdProcesCup];
        Test.startTest();
            objProNotCup.Activo__c = true;
			update objProNotCup;	
			System.assert(objProNotCup.Activo__c, 'Cambio correctamente');
		Test.stopTest();
    }

    @isTest 
    private static void fillContract() {
        final String queyrOpp = fillQuery();
        final List<Opportunity> lsOpps = DataBase.query(queyrOpp);
        final Map<String, String> mapOpps = new Map<String, String> ();
        final List<String> response = new List<String>();
        for(Opportunity opp: lsOpps) {
            opp.FolioCotizacion__c = '1042695';
            mapOpps.put(opp.FolioCotizacion__c, opp.Id);
            response.add('ASDQW412DDFASF83');
        }
        Test.startTest();
            final String getQuotetions = quotetationStr('GetQuotation');
        	Final Map<String,String> tsecTst = new Map<String,String> {'tsec'=>'2122113'};
            final MX_WB_Mock mockCallout = new MX_WB_Mock(200, 'Complete', getQuotetions, tsecTst);
            iaso.GBL_Mock.setMock(mockCallout);
            final Map<String,Map<String,Object>> responseQuote = MX_SB_VTS_SendTrackingTelemarketing_Ser.findQuoteStatus(response, mapOpps);
            final Contract fillContractRec = MX_SB_VTS_SendTrackingTelemarketing_Ser.fillContract(responseQuote, lsOpps[0]);
            System.assertEquals(fillContractRec.MX_WB_Oportunidad__c, lsOpps[0].Id, 'Contrato correcto');
		Test.stopTest();
    }

    /**
    * @description Prepara query para Oportunidades batch
    * @author Eduardo Hernandez Cuamatzi | 10-01-2020 
    * @return String Query final
    **/
    public static String fillQuery() {
        String sQuery = 'Select Id, AccountId, EnviarCTI__c, LeadSource, LastModifiedDate, ';
        sQuery += ' Account.FirstName, Account.LastName, Account.ApellidoMaterno__c, FolioCotizacion__c,';
        sQuery += ' StageName, TelefonoCliente__c, OwnerId, Producto__c, Account.PersonEmail From Opportunity ';
        sQuery += ' Where LeadSource = \'Tracking Web\'';
        sQuery += ' And AccountId != null';
        sQuery += ' And EnviarCTI__c = False';
        sQuery += ' And StageName NOT IN (\'Closed Won\', \'Closed Lost\')';
        sQuery += ' And CreatedDate >= ' + System.label.MX_SB_VTS_SCHEDULE_DATE_LBL;
        sQuery += ' And Caso_Relacionado__c = null';
        sQuery += ' And MX_SB_VTS_OppCase__c = null';
        sQuery += ' And Producto__c = \'' + PRODHSD + '\'';
        return sQuery;
    }

    /**
    * @description Recupera Mock Servio de metadata GetQuotetion
    * @author Eduardo Hernandez Cuamatzi | 10-01-2020 
    * @return String Mock GetQuotetion
    **/
    public static String quotetationStr(String serviceName) {
        return [ SELECT DeveloperName, iaso__Mock_LTA__c, iaso__Querystring_Input_Template_LTA__c FROM iaso__GBL_integration_service__mdt
         WHERE DeveloperName =: serviceName].iaso__Mock_LTA__c;
    }
}