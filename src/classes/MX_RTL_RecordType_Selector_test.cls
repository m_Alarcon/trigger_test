/****************************************************************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase test de aclaraciones - selector

*   Cambios (Versiones)
*   ------------------------------------------------------------------------------------------------------------------------*
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ----------------------------------------------------------------*
*   1.0     25/05/2020      Roberto Isaac Soto Granados           Creación Clase
*   1.1     24/06/2020      Hugo Ivan Carrillo Béjar              Se agrega el methodo testGetMapRecordTypeByObjectType
****************************************************************************************************************************/
@isTest
private class MX_RTL_RecordType_Selector_test {
    /*Usuario de pruebas*/
    private static User testUser = new User();

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testUser = UtilitysDataTest_tst.crearUsuario('testUser', 'BPyP Estandar', 'BPYP BANQUERO BANCA PERISUR');
        testUser.Title = 'Privado';
        insert testUser;
    }

    /*Ejecuta la acción para cubrir clase*/
    static testMethod void recordTypeCasosTest() {
        List<RecordType> aclaraciones = new List<RecordType>();
        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        System.runAs(testUser) {
            Test.startTest();
                aclaraciones = MX_RTL_RecordType_Selector.recordTypeLlamadaBPyP();
            Test.stopTest();
        }
        System.assert(!aclaraciones.isEmpty(), 'RecordType encontrados');
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @void
    **/
    @isTest
    private static void getMapRecordTypeTest() {
        Test.startTest();
        final Map<Id, RecordType> mapRecordT = MX_RTL_RecordType_Selector.getMapRecordType('Lead', 'MX_BPP_Leads');
        Test.stopTest();
        System.assertNotEquals(mapRecordT.size(), 0, 'Faild getMapRecordTypeTest');
    }

    /**
    * @description
    * @author Hugo Carrillo
    * @void
    **/
    @isTest
    private static void testGetMapRecordTypeByObjectType() {
        Test.startTest();
        final Map<Id, RecordType> mapRecordTy = MX_RTL_RecordType_Selector.getMapRecordType('Lead', null);
        Test.stopTest();
        System.assertNotEquals(mapRecordTy.size(), 0, 'Faild getMapRecordTypeTest');
    }
}