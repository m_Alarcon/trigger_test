/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_MinutaPreview_Ctrl
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2021-01-11
* @Description 	BPyP Minuta Preview VF Controller
* @Changes      Fecha		|		Autor		|		Descripción
 * 			2021-19-02			Héctor Saldaña	  Nueva llamada a MX_BPP_Minuta_Service.recolectarTags() agregada
 * 		                                          para recolectar la información que se envía al LWC integrado a la VF
*           2021-05-03          Héctor Saldaña    Asignación a campo Número Opcionales eliminada
*/
public without sharing class MX_BPP_MinutaPreview_Ctrl {
    /* Variable identifica si proviene del botón enviar */
    public Boolean enviar {get;set;}
    /** Variable para guardar record Visita*/
    public dwp_kitv__Visit__c previewVisit {get;set;}
    /**Variable de control de errores*/
    public Boolean vfError {get; set;}
    /*Variable para seraliizar el objeto en String para LWC*/
    public String strConfig {get; set;}
    /*Propiedad referente a Objeto con contenida para configuración de parámetros*/
    public MX_BPP_MinutaWrapper minutaWrapper {get;set;}

    /* Constructor clase MX_BPP_MinutaPreview_Ctrl */
    public MX_BPP_MinutaPreview_Ctrl() {
        String errorMessage;
        this.minutaWrapper = new MX_BPP_MinutaWrapper();
        this.vfError = false;
        this.enviar = false;
        this.previewVisit = MX_BPP_Minuta_Service.getRecordInfo(this.minutaWrapper.configParamsPty.currentId);
        this.minutaWrapper.configParamsPty.currentId = ApexPages.currentPage().getParameters().get('Id').escapehtml4();
        this.minutaWrapper.configParamsPty.sMyCS = MX_BPP_Minuta_Utils.getParameter('myCS');
        this.minutaWrapper.configParamsPty.sDocuments = MX_BPP_Minuta_Utils.getParameter('documents');

        final List<MX_BPP_TipoMinuta__mdt> mdtTemp = MX_BPP_Minuta_Service.getMinutaMetadata(this.previewVisit);
        if (mdtTemp.isEmpty() == false) {
            this.minutaWrapper.catalogoPty.sVfName = mdtTemp[0].MX_NombreVF__c;
            final List<MX_BPP_CatalogoMinutas__c> catalogoMin = MX_BPP_Minuta_Service.getCatalogoMinuta(mdtTemp[0].DeveloperName);
            if (catalogoMin.isEmpty() == false) {
                this.minutaWrapper.catalogoPty.sIdCatalogo = catalogoMin[0].Id;
                this.minutaWrapper.catalogoPty.showFichaProducto = catalogoMin[0].MX_Adjuntos__c;
                if(String.isNotBlank(catalogoMin[0].MX_Opcionales__c)) {
                    MX_BPP_Minuta_Service.recolectarTags(catalogoMin[0], this.minutaWrapper);
                }

                if (!MX_BPP_Minuta_Service.checkNumOfAgreements(this.minutaWrapper.configParamsPty.currentId, Integer.valueOf(catalogoMin[0].MX_Acuerdos__c))) {
                    this.vfError = true;
                    errorMessage = 'Número de acuerdos insuficiente';
                }
            } else {
                this.vfError = true;
                errorMessage = 'Tipo de minuta no reconocido';
            }
        } else {
            this.vfError = true;
            errorMessage = 'No se encontró configuración para este tipo de registro';
        }
        
        if (this.vfError) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, errorMessage));
        }

        if(!String.isBlank(this.minutaWrapper.configParamsPty.sMyCS.unescapeHtml4())) {
            this.enviar = true;
        }

        this.strConfig = JSON.serialize(this.minutaWrapper).escapeHtml4();
    }

    /**
    * @description
    * @author Edmundo Zacarias
    * @param String parameterToGet
    * @return dwp_kitv__Template_for_type_of_visit_cs__c 
    **/
    @AuraEnabled(cacheable=true)
    public static dwp_kitv__Template_for_type_of_visit_cs__c getParamaterSetting(String parameterToGet) {
        return (dwp_kitv__Template_for_type_of_visit_cs__c)System.JSON.deserialize(parameterToGet.unescapeHtml4(), dwp_kitv__Template_for_type_of_visit_cs__c.class);
    }

    /**
    * @description
    * @author Edmundo Zacarias
    * @param String parameterToGet
    * @return List<ContentVersion> 
    **/
    @AuraEnabled(cacheable=true)
    public static List<ContentVersion> getParamaterDocs(String parameterToGet) {
        return (List<ContentVersion>)System.JSON.deserialize(parameterToGet.unescapeHtml4(), List<ContentVersion>.class);
    }
}