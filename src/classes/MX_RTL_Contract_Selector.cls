/**
 * @File Name          : MX_RTL_Contract_Selector.cls
 * @Description        :
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 10/07/2020, 12:07:22 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/27/2020    Juan Carlos Benitez         Initial Version
 * 1.1    6/11/2020     Marco Antonio Cruz        Se hace cambio a SOC
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
    public class MX_RTL_Contract_Selector {
    /** Lista de registros de Contract */
    final static List<sObject> RETURN_CNTRCT = new List<sObject>();
    /**Arreglo de contratos para consultas locales */
    public static Contract[] contratosMethod {get; set;}
    /**Variable para SELECT*/
    Final static String SELECTION ='SELECT ';
    /**Variable para FROM*/
    Final static String VFROM =' FROM Contract ';
    /** list of possible filters */
    final static Set<String> CONTRACT_FILTERS = new Set<String>();
    
    /*
    /*
    * @description Busqueda de datos del Contrato por MX_SB_SAC_NumeroPoliza__c
    * @param String policy
	* @return List<SObject>RETURN_CNTRCT
    */

    public static List<Contract> getContractBySACnPoliza(String policy,String cntrctFields, String clause) {
        RETURN_CNTRCT.clear();
        switch on clause {
            when 'MX_SB_SAC_NumeroPoliza__c' {
                RETURN_CNTRCT.addAll(Database.query(string.escapeSingleQuotes(SELECTION + cntrctFields +'FROM Contract WHERE MX_SB_SAC_NumeroPoliza__c=:policy')));
            }
            when 'MX_WB_Oportunidad__c' {
                RETURN_CNTRCT.addAll(Database.query(string.escapeSingleQuotes(SELECTION + cntrctFields +'FROM Contract WHERE MX_WB_Oportunidad__c=:policy')));
            }
        }
        return RETURN_CNTRCT;
    }

    /*
    * @description method para consulta 
    * de contratos por nombre
    * @param String nombre String apaterno
    * @return Contract[]
    */
    public static List<Contract> getContractIdByNombre(String nombre, String apaterno, String srchFields) {
        RETURN_CNTRCT.clear();
        if (nombre !='' && apaterno !='') {
            RETURN_CNTRCT.addAll(Database.query(string.escapeSingleQuotes(SELECTION + srchFields + VFROM +
            ' WHERE (NOT(MX_SB_SAC_RFCAsegurado__c=null)) AND (NOT(MX_WB_apellidoPaternoAsegurado__c=null)) ' +
            ' AND (NOT(MX_WB_apellidoMaternoAsegurado__c=null)) AND (NOT(accountid=null)) ' +
            ' AND MX_SB_SAC_NombreClienteAseguradoText__c =:nombre ' +
            ' AND  MX_WB_apellidoPaternoAsegurado__c =:apaterno ' +
            ' order by createddate  desc limit 200')));
        }
        return RETURN_CNTRCT;
    }
    
    /*
    * @description method para consulta 
    * de contratos por RFC del asegurado
    * @param String rfcAsegurado
    * @return Contract[]
    */
    public static List<Contract> getContractIdByRFC(String rfcAsegurado, String srchFields) {
        RETURN_CNTRCT.clear();
        if(rfcAsegurado !='') {
            RETURN_CNTRCT.addAll(Database.query(string.escapeSingleQuotes(SELECTION + srchFields + VFROM + 
            ' WHERE (NOT(MX_SB_SAC_RFCAsegurado__c=null)) AND (NOT(MX_WB_apellidoPaternoAsegurado__c=null))' +
            ' AND (NOT(MX_WB_apellidoMaternoAsegurado__c=null)) AND (NOT(accountid=null))' +
            ' AND (NOT(MX_SB_SAC_NombreClienteAseguradoText__c =null)) AND (NOT(MX_WB_apellidoPaternoAsegurado__c =null)) ' +
            ' AND MX_SB_SAC_RFCAsegurado__c =:rfcAsegurado order by createddate  desc limit 200')));
        } 
        return RETURN_CNTRCT;
    }

    /*
    * @description method para consulta 
    * de contratos por Cuenta de contratante
    * @param String idCtaContrato
    * @return Contract[]
    */
    public static List<Contract> getContractIdByAccount(String idCtaContrato, String srchFields) {
        RETURN_CNTRCT.clear();
        if(idCtaContrato !=null && idCtaContrato !='') {
            RETURN_CNTRCT.addAll(Database.query(string.escapeSingleQuotes(SELECTION + srchFields + VFROM +
            ' WHERE (NOT(MX_SB_SAC_RFCAsegurado__c=null)) AND (NOT(MX_WB_apellidoPaternoAsegurado__c=null)) ' +
            ' AND (NOT(MX_WB_apellidoMaternoAsegurado__c=null)) AND (NOT(accountid=null)) ' +
            ' AND (NOT(MX_SB_SAC_NombreClienteAseguradoText__c =null)) AND (NOT(MX_WB_apellidoPaternoAsegurado__c =null)) ' +
            ' AND accountid =: idCtaContrato order by createddate  desc limit 200')));
        } 
        return RETURN_CNTRCT;
    }

    /*
    * @description method para consulta 
    * de contratos por poliza de contratante
    * @param String polizaCtr String certificadoCtr
    * @return Contract[]
    */
    public static List<Contract> getContractIdByPoliza (String polizaCtr , String certificadoCtr, String srchFields) {
        RETURN_CNTRCT.clear();
        if (polizaCtr !='' && certificadoCtr !='') {
            RETURN_CNTRCT.addAll(Database.query(string.escapeSingleQuotes(SELECTION + srchFields + VFROM +
            ' WHERE (NOT(MX_SB_SAC_RFCAsegurado__c=null)) AND (NOT(MX_WB_apellidoPaternoAsegurado__c=null)) ' +
            ' AND (NOT(MX_WB_apellidoMaternoAsegurado__c=null)) AND (NOT(accountid=null)) ' +
            ' AND (NOT(MX_SB_SAC_NombreClienteAseguradoText__c =null)) AND (NOT(MX_WB_apellidoPaternoAsegurado__c =null)) ' +
            ' AND MX_SB_SAC_NumeroPoliza__c =:polizaCtr AND MX_SB_MLT_Certificado__c =:certificadoCtr')));
        } 
        return RETURN_CNTRCT;
    }

    /*
    * @description method para actualizar datos del 
    * contrato seleccionado en caso de cambiar en consulta de servicio
    * @param Object Contract 
    * @return String resultado de operacion
    */
    @AuraEnabled
    public static void upsertSinCont (Contract poliza) {
        upsert poliza;
    }
    
    /**
    * @description
    * @author Jaime Terrats | 6/8/2020
    * @param queryParams
    * @return List<Contract>
    **/
    public static List<Contract> getContractsByCustomerData(Map<String, String> queryParams) {
        CONTRACT_FILTERS.add(queryParams.get('searchParam'));
        return  Database.query(String.escapeSingleQuotes('select ' + queryParams.get('contractFields') + ' from Contract where MX_SB_SAC_NombreCuentaText__c in: CONTRACT_FILTERS'
                                                        + ' or MX_SB_SAC_NombreClienteAseguradoText__c in: CONTRACT_FILTERS or MX_WB_apellidoPaternoAsegurado__c in: CONTRACT_FILTERS'
                                                        + ' or MX_WB_apellidoMaternoAsegurado__c in: CONTRACT_FILTERS or MX_SB_SAC_EmailAsegurado__c in: CONTRACT_FILTERS'
                                                        + ' or MX_SB_SAC_RFCAsegurado__c in: CONTRACT_FILTERS'));
    }

    /**
  	 * @description Ejecuta insert de registros para Contratos
  	 * @author Alexandro Corzo | 25/09/2020 
  	 * @param List<Contract> lstContracts  Lista de Contratos a procesar
  	 * @param Boolean allOrNone Indica si se lanza una operacion de exception
  	 * @return List<Contracts> Lista de resultados de la operación DML
  	 **/
    public static List<Contract> insertContracts(List<Contract> lstContracts) {
    	insert lstContracts;
		return lstContracts;        
    }
}