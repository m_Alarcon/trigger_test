/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_COB_CreaPagosSch_tst
* Autor Angel Nava
* Proyecto: Cobranza - BBVA Bancomer
* Descripción : Clase test de clase MX_SB_COB_CreaPagosSch_cls
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Desripción
* --------------------------------------------------------------------------------
* 1.0           01/05/2019     Angel Nava				          Creación
* --------------------------------------------------------------------------------
*/
@isTest
public class MX_SB_COB_CreaPagosSch_tst {

    @TestSetup
    static void setPrueba() {
        final Account cuenta = new Account(firstname='Angel',lastname='Nava');
        insert cuenta;
        final Contract contrato = new Contract(accountid= cuenta.id,MX_WB_noPoliza__c='A4234');
        insert contrato;
        final campaniacobranza__c camp = new  campaniacobranza__c(name='auto');
        insert camp;
        final Bloque__c bloque = new Bloque__c(MX_SB_COB_BloqueActivo__c=true,MX_SB_COB_campaniacobranza__c = camp.id);
        insert bloque;
        final pagosespejo__c espejo = new pagosespejo__c(MX_SB_COB_Orden__c=1,MX_SB_COB_factura__c='3123',MX_SB_COB_contrato__c=contrato.id);
        insert espejo;

    }

    @isTest
    static void PruebaSchedule() {
        final bloque__c bloque = [select id from bloque__c limit 1];
        final pagosespejo__c espejo = [select id from pagosespejo__c limit 1];
        final List<String> espejosIds = new List<String> { espejo.id };
        test.startTest();
        final MX_SB_COB_CreaPagosSch_cls objCreaPagosSch = new MX_SB_COB_CreaPagosSch_cls();
		objCreaPagosSch.sIdBloque = bloque.id;
        objCreaPagosSch.pagosEspejoIds = espejosIds;
		final DateTime dtFechaHoraAct = DateTime.now();
		final Date dFechaActual = dtFechaHoraAct.date();
		final Time tmHoraActual = dtFechaHoraAct.Time();
		final Time tmHoraActualEnv = tmHoraActual.addMinutes(3);
        final Integer randomNumber = Integer.valueof(Math.random() * 100);
        final Integer randomNumber2 = Integer.valueof(Math.random() * 100);
		String sch = '';
		sch = '0 ' + tmHoraActualEnv.minute() + ' ' + tmHoraActualEnv.hour() + ' ' + dFechaActual.day() + ' ' + dFechaActual.month() + ' ?';
        final String sNombreProc = tmHoraActualEnv.minute() + ' : ' + tmHoraActualEnv.hour() + ' : ' + dFechaActual.day() + ' : CreaPagos : '+randomNumber+' : '+randomNumber2;
        System.schedule(sNombreProc, sch, objCreaPagosSch);
        final List<CronTrigger> trig = [SELECT Id, CronJobDetail.Name, State, TimesTriggered,StartTime FROM CronTrigger where cronjobdetail.name like '%CreaPagos%'  limit 10];
        system.assertEquals(1, trig.size(), 'job cargado correctacmente');
        test.stopTest();
    }

    @isTest
    static void correoNotificacion() {
        final bloque__c bloque = [select id,name from bloque__c limit 1];
        final pagosespejo__c espejo = [select id from pagosespejo__c limit 1];
        final cargaarchivo__c archivo = new cargaArchivo__c(MX_SB_COB_Bloque__c=bloque.Name,MX_SB_COB_ContadorConError__c=0,MX_SB_COB_ContadorSinError__c=0,MX_SB_COB_RegistrosTotales__c=0);
        insert archivo;
        final List<String> espejosIds = new List<String> { espejo.id };
        final MX_SB_COB_EnviaCorreoCobranzaSch_cls objCorreo = new MX_SB_COB_EnviaCorreoCobranzaSch_cls();
                    objCorreo.idCargaArchivo = archivo.id;

                    DateTime dtFechaHoraAct = DateTime.now();
                    Date dFechaActual = dtFechaHoraAct.date();
                    Time tmHoraActual = dtFechaHoraAct.Time();
                    Time tmHoraActualEnv = tmHoraActual.addMinutes(30);

                    String sch = '';

                    sch = '0 ' + tmHoraActualEnv.minute() + ' ' + tmHoraActualEnv.hour() + ' ' + dFechaActual.day() + ' ' + dFechaActual.month() + ' ?';
                    String sNombreProc = tmHoraActualEnv.minute() + ' : ' + tmHoraActualEnv.hour() + ' : ' + dFechaActual.day() + ' : EnviaCorreoCobranza';
                  System.schedule(sNombreProc, sch, objCorreo);
        test.startTest();

        final MX_SB_COB_CreaPagosSch_cls objCreaPagosSch = new MX_SB_COB_CreaPagosSch_cls();
		objCreaPagosSch.sIdBloque = bloque.id;
        objCreaPagosSch.pagosEspejoIds = espejosIds;
		 dtFechaHoraAct = DateTime.now();
		 dFechaActual = dtFechaHoraAct.date();
		 tmHoraActual = dtFechaHoraAct.Time();
		 tmHoraActualEnv = tmHoraActual.addMinutes(3);
        final Integer randomNumber = Integer.valueof(Math.random() * 100);
        final Integer randomNumber2 = Integer.valueof(Math.random() * 100);
		sch = '';
		sch = '0 ' + tmHoraActualEnv.minute() + ' ' + tmHoraActualEnv.hour() + ' ' + dFechaActual.day() + ' ' + dFechaActual.month() + ' ?';
        sNombreProc = tmHoraActualEnv.minute() + ' : ' + tmHoraActualEnv.hour() + ' : ' + dFechaActual.day() + ' : CreaPagos : '+randomNumber+' : '+randomNumber2;
        System.schedule(sNombreProc, sch, objCreaPagosSch);
        final List<CronTrigger> trig = [SELECT Id, CronJobDetail.Name, State, TimesTriggered,StartTime FROM CronTrigger where  cronjobdetail.name like '%EnviaCorreoCobranza%' or cronjobdetail.name like '%CreaPagos%'  limit 10];
        system.assertEquals(2, trig.size(), 'job cargado correctacmente');
        test.stopTest();
    }
}