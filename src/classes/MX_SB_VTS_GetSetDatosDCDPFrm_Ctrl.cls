/**
 * @File Name          : MX_SB_VTS_GetSetDatosDCDPFrm_Ctrl.cls
 * @Description        : Formularios - Datos Contratante / Propiedad
 * @Author             : Alexandro Corzo
 * @Group              :
 * @Last Modified By   : Diego Olvera
 * @Last Modified On   : 02-23-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0       17/11/2020      Alexandro Corzo        Initial Version
 * 1.1       28/01/2021      Alexandro Corzo        Se realizan ajustes a clase para consumo ASO
 * 1.2       08/02/2021      Alexandro Corzo        Se realizan ajustes a clase para Codesmells
 * 1.3       23/02/2021      Diego Olvera           Se agrega funcion para guardado/actualizacion
**/
@SuppressWarnings('sf:UseSingleton')
public class MX_SB_VTS_GetSetDatosDCDPFrm_Ctrl {
	/**
     * @description : Recibe del Formulario los Datos del Contratante
     * 				: y Dirección de la Propiedad
     * @author: 	 Alexandro Corzo
     * @return: 	 Map<String, Object> oReturnValues
     */
    @AuraEnabled
    public static Map<String, Object> setDataFormDCDP(Map<String, Object> mDataObjDCDP) {
    	return MX_SB_VTS_GetSetDatosDCDPFrm_Service.setDataFormDCDPSrv(mDataObjDCDP);
    }
    
    /**
     * @description : Recibe del Formulario la Dirección de la Poliza
     *              : en fisico para su envio a Clippert
     * @author: 	 Alexandro Corzo
     * @return: 	 Map<String, Object> oReturnValues
     */
    @AuraEnabled
    public static Map<String, Object> setDataFormComSrv(Map<String, Object> mDataObjDCDP) {
        return MX_SB_VTS_GetSetDatosDCDPFrm_Service.setDataCompSrv(mDataObjDCDP);
    }

     /**
    * @description Función que obtiene mapa del front(Js) para guardado de direccion
    * @author Diego Olvera | 17-02-2021 
    * @param dataAdd, mapa de valores.
    * @return void
    **/ 
    @AuraEnabled
    public static void saveAddReCtrl(Map<String, Object> dataAdd) {
        MX_SB_VTS_GetSetDatosDCDPFrm_Service.saveAddRec(dataAdd);
    }
}