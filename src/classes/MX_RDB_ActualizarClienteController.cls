/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RDB_ActualizarClienteController
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2019-11-29
* @Group  		MX_RDB_ActualizarCliente
* @Description 	Apex Controller from MX_RDB_ActualizarCliente Lightning Component
* @Changes
*  
*/
public without sharing class MX_RDB_ActualizarClienteController {
    /**
    * 
    * @Description field value for ok response from WS.
    **/
    private static final Integer STATUSCODEOK = 200;
    
    private MX_RDB_ActualizarClienteController() {}
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description Returns clientID for the account Record
    * @param idAcc the ID record to be search
    * @return The MX_SB_BCT_Id_Cliente_Banquero__c field from the Account record
    **/
    @AuraEnabled
    public static String getClientID(String idAcc) {
        String clientID = '';
        try {
            clientID = [SELECT MX_SB_BCT_Id_Cliente_Banquero__c FROM Account WHERE Id=: idAcc].MX_SB_BCT_Id_Cliente_Banquero__c;
        } catch(System.QueryException e) {
            throw new AuraHandledException(System.Label.MX_SB_BCT_Error_ListException + e);
        }
        return clientID;
    }
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description Returns clientID for the account Record
    * @param clientID to be send in the WS.
    * @return The OK as update result
    **/
    @AuraEnabled
    public static String updateClientInfo(String clientID) {
        String result = null;
        Map<String, Object> jsonReturnGC;
        Map<String, Object> jsonReturnEIT;
        Map<String, Object> jsonReturnEIC;
        final String serviceNameGC = Test.isRunningTest() ? 'getCustomerMock' : 'getCustomer';
        final String serviceNameEI = Test.isRunningTest() ? 'listElectronicInformationMock' : 'listElectronicInformation';
        final String jsonInputGC = '{"clientNumber":"' + clientID + '"}';
        final String jsonInputEIT = '{"dataType":"T", "clientId":"' + clientID + '"}';
        final String jsonInputEIC = '{"dataType":"M","clientId":"' + clientID + '"}';
        final HttpResponse httpResultGC = iaso.GBL_Integration_GenericService.invoke(serviceNameGC, jsonInputGC);
        final HttpResponse httpResultEIT = iaso.GBL_Integration_GenericService.invoke(serviceNameEI, jsonInputEIT);
        final HttpResponse httpResultEIC = iaso.GBL_Integration_GenericService.invoke(serviceNameEI, jsonInputEIC);
        if( httpResultGC.getStatusCode() == MX_RDB_ActualizarClienteController.STATUSCODEOK && httpResultEIT.getStatusCode() == MX_RDB_ActualizarClienteController.STATUSCODEOK && httpResultEIC.getStatusCode() == MX_RDB_ActualizarClienteController.STATUSCODEOK) {
            result = 'OK';
            jsonReturnGC = (Map<String, Object>)JSON.deserializeUntyped(httpResultGC.getBody());
            jsonReturnEIT = (Map<String, Object>)JSON.deserializeUntyped(httpResultEIT.getBody());
            jsonReturnEIC = (Map<String, Object>)JSON.deserializeUntyped(httpResultEIC.getBody());
            MX_RDB_ActualizarClienteController.updateRecordInfo(clientID, jsonReturnGC, jsonReturnEIT, jsonReturnEIC);
        }
        return result;
    }
    
    private static void updateRecordInfo(String clientID, Map<String, Object> jsonResponseGC, Map<String, Object> jsonResponseEIT, Map<String, Object> jsonResponseEIC) {
        final Account clientRecord = [SELECT FirstName, LastName, Apellido_materno__pc, ApellidoPaterno__c, PersonBirthdate, Phone, RFC__c, PersonEmail, BillingStreet, Numero_Interior__c, Numero_Exterior__c, Colonia__c, BillingPostalCode, BillingCity, BillingState
                                FROM Account 
                               WHERE MX_SB_BCT_Id_Cliente_Banquero__c =: clientID];
        
        final Map<String, Object> jsonPerson = (Map<String, Object>)jsonResponseGC.get('person');
        final Map<String, Object> jsonBranch = (Map<String, Object>)jsonResponseGC.get('branch');
        final List<Object> idDocum = (List<Object>)jsonPerson.get('identityDocument');
        final Map<String, Object> legalAdd = (Map<String, Object>)jsonPerson.get('legalAddress');
        
        clientRecord.FirstName = (String) jsonPerson.get('name');
        clientRecord.LastName = (String)jsonPerson.get('lastName');
        clientRecord.Apellido_materno__pc = (String)jsonPerson.get('mothersLastName');
        clientRecord.ApellidoPaterno__c = (String)jsonPerson.get('lastName');
        clientRecord.CR__c = (String)jsonBranch.get('name');
        clientRecord.Sector__c = (String) ((Map<String, Object>)jsonPerson.get('type')).get('name');
        clientRecord.Segmento__c = (String) ((Map<String, Object>)((Map<String, Object>)jsonPerson.get('segment')).get('value')).get('id');
        
        final String membershipDate = (String)jsonResponseGC.get('membershipDate');
        clientRecord.MX_RDB_Fecha_afiliacion__c = Date.newInstance(Integer.valueOf(membershipDate.substring(0, 4)), Integer.valueOf(membershipDate.substring(5, 7)), Integer.valueOf(membershipDate.substring(8, 10)));
        
        
        final String fechaNac = (String)jsonPerson.get('birthDate');
        final Date fechaN = Date.newInstance(Integer.valueOf(fechaNac.substring(0, 4)), Integer.valueOf(fechaNac.substring(5, 7)), Integer.valueOf(fechaNac.substring(8, 10)));
        clientRecord.PersonBirthdate = fechaN;
        
        Map<String, Object> idDocElem;
        for (Object element : idDocum) {
            final String idDocElemStr = JSON.serialize(element);
            idDocElem = (Map<String, Object>)JSON.deserializeUntyped(idDocElemStr);
            final Map<String, Object> idDocElemType = (Map<String, Object>)idDocElem.get('type');
            if (idDocElemType <> null && idDocElemType.get('id') == 'RFC') {
                clientRecord.RFC__c = (String) idDocElem.get('number');
            }
        }
        
        clientRecord.BillingStreet = (String) legalAdd.get('streetName');
        clientRecord.Numero_Interior__c = (String) legalAdd.get('door');
        clientRecord.Numero_Exterior__c = (String) legalAdd.get('streetNumber');
        clientRecord.Colonia__c = (String) legalAdd.get('neighborhood');
        clientRecord.BillingPostalCode = (String) legalAdd.get('zipCode');
        clientRecord.BillingCity = (String) legalAdd.get('city');
        clientRecord.BillingState = (String) ((Map<String, Object>)legalAdd.get('state')).get('name');
        
        final List<Object> jsonLEDT = (List<Object>)jsonResponseEIT.get('listElectronicData');
        final String listEDTStr = JSON.serialize(jsonLEDT[0]);
        final Map<String, Object> listEDTMap = (Map<String, Object>)JSON.deserializeUntyped(listEDTStr);
        final Map<String, Object> listEDTel = (Map<String, Object>)listEDTMap.get('telephone');
        clientRecord.Phone = String.valueOf(listEDTel.get('telephoneNumber'));
        
        final List<Object> jsonLEDC = (List<Object>)jsonResponseEIC.get('listElectronicData');
        final String listEDCStr = JSON.serialize(jsonLEDC[0]);
        final Map<String, Object> listEDCMap = (Map<String, Object>)JSON.deserializeUntyped(listEDCStr);
        clientRecord.PersonEmail = (String) listEDCMap.get('email');
        
        update clientRecord;
    }
}