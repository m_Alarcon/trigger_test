/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_COB_EnviaCorreoCobranzaSch_cls
* Autor Angel Nava
* Proyecto: Cobranza - BBVA Bancomer
* Descripción : Clase de proceso schedulable para nevio de correo final de resultado carga y cierre de jobs
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Desripción
* --------------------------------------------------------------------------------
* 1.0           01/05/2019     Angel Nava				          Creación
* --------------------------------------------------------------------------------
*/
global with sharing class MX_SB_COB_EnviaCorreoCobranzaSch_cls implements Schedulable {
    /**variable idCargaArchivo */
    global Id idCargaArchivo { get;set; }
    /**execute */
    global void execute (SchedulableContext schCont) {
		final String corDesaPrueb = 'angelignacio.nava.contractor@bbva.com';

        final List<Organization> org = [select Id, IsSandbox from Organization limit 1 ];
        Final Boolean sandbox = org[0].isSandbox;
        String sResPrArchOK = '';
        final String sResulProcArchivo = '';
        List<CargaArchivo__c> cargaArchivoObj = new List<CargaArchivo__c>();
        cargaArchivoObj = [select id,MX_SB_COB_Resultado__c,MX_SB_COB_ContadorConError__c,MX_SB_COB_ContadorSinError__c from cargaArchivo__c where id =  :idCargaArchivo];
        sResPrArchOK = 'Carga de archivo id='+cargaArchivoObj[0].id+':'+cargaArchivoObj[0].MX_SB_COB_Resultado__c;
        final Decimal ContadorTotal = cargaArchivoObj[0].MX_SB_COB_ContadorConError__c +cargaArchivoObj[0].MX_SB_COB_ContadorSinError__c;
        sResPrArchOK+= '---Total de registro='+Integer.valueOf(COntadorTotal)+' : Total de registros con error='+Integer.valueOf(cargaArchivoObj[0].MX_SB_COB_ContadorConError__c)+' : Total de registros sin error = '+Integer.valueOf(cargaArchivoObj[0].MX_SB_COB_ContadorSinError__c) ;

        String[] lEmail = new String[] {};
        if(sandbox) {
            lEmail = new String[] { corDesaPrueb };
        } else {
			lEmail = new String[] { Label.AVX_RecuparaProcesaArchRs, Label.AVX_RecuparaProcesaArchRs2, Label.AVX_RecuparaProcesaArchRs3, Label.AVX_RecuparaProcesaArchRs4, 'larrioja@avanxo.com' };
        }
        final Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(lEmail);
        mail.setTargetObjectId(UserInfo.getUserId());
        mail.setSenderDisplayName('Carga Archivo Recuperación.');
        mail.setUseSignature(false);
        mail.setSaveAsActivity(false);
		mail.setPlainTextBody(sResPrArchOK += sResulProcArchivo);

		if (!Test.isRunningTest()) {
	        final Messaging.SendEmailResult[] repuesta = Messaging.sendEmail(new Messaging.singleemailMessage[] { mail });    
		}
        final List<CronTrigger> trig = [SELECT Id, CronJobDetail.Name, State, TimesTriggered FROM CronTrigger where TimesTriggered=1 and State='WAITING' and (cronjobdetail.name like '%CreaPagosEspejo%' or cronjobdetail.name like '%EliminaPagosEspejo%' or  cronjobdetail.name like '%CreaPolizas%' or cronjobdetail.name like '%EnviaCorreoCobranza%' or  cronjobdetail.name like '%CreaPagos%')]; 
       	 if(trig.size()>0) {
            for(CronTrigger tr:trig ) {
                system.abortJob(tr.id);
            }
        }

    }
}