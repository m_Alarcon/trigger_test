/**
* @FileName          : MX_SB_VTS_wrpCreateCustomerDataSrv
* @description       : Wrapper class for use de web service of createCustomerData
* @Author            : Alexandro Corzo
* @last modified on  : 03-02-2021
* Modifications Log 
* Ver   Date         Author                Modification
* 1.0   27-10-2020   Alexandro Corzo       Initial Version
* 1.1   28-01-2021   Alexandro Corzo       Se realizan ajustes a Wrapper para consumo ASO
* 1.2   08-02-2021   Alexandro Corzo       Se realizan ajustes a clase para Codesmells
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton, sf:ExcessivePublicCoun,sf:LongVariable,sf:LongVariable,sf:ExcessivePublicCount, sf:ShortVariable,sf:ShortClassName') 
public class MX_SB_VTS_wrpCreateCustomerDataSrv {
	/**
	 * Invocación de Wrapper
	 */
    public iCatalogItem iCatalogItem {get;set;}
    
    /**
     * @description : Clase el objeto iCatalogItem del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class iCatalogItem {
        /** Invocación de Clase: header */
        public header header {get;set;}
        /** Invocación de Clase: quote */
        public quote quote {get;set;}
        /** Invocación de Clase: clientType */
        public clientType clientType {get;set;}
        /** Atributo de Clase: contractingHolderIndicator */
        public Boolean contractingHolderIndicator {get;set;}
        /** Atributo de Clase: contractor */
        public String contractor {get;set;}
        /** Atributo de Clase: holder */
        public holder holder {get;set;}
        /** Atributo de Clase: authorizationDate */
        public String authorizationDate {get;set;}
        /** Atributo de Clase: authorizationTime */
        public String authorizationTime {get;set;}
        /** Atributo de Clase: certificateNumber */
        public String certificateNumber {get;set;}
        /** Invocación de Clase: shippingChannel */
        public shippingChannel shippingChannel {get;set;}
        /** Atributo de Clase: updateDataIndicator */
        public boolean updateDataIndicator {get;set;}
        /** Atributo de Clase: isForeign */
        public String isForeign {get;set;}
    }
    
    /**
     * @description : Clase el objeto header del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class header {
        /** Atributo de Clase: aapType */
        public String aapType {get;set;}
        /** Atributo de Clase: dateRequest */
        public String dateRequest {get;set;}
        /** Atributo de Clase: channel */
        public String channel {get;set;}
        /** Atributo de Clase: subChannel */
        public String subChannel {get;set;}
        /** Atributo de Clase: branchOffice */
        public String branchOffice {get;set;}
        /** Atributo de Clase: managementUnit */
        public String managementUnit {get;set;}
        /** Atributo de Clase: user */
        public String user {get;set;}
        /** Atributo de Clase: idSession */
        public String idSession {get;set;}
        /** Atributo de Clase: idRequest */
        public String idRequest {get;set;}
        /** Atributo de Clase: dateConsumerInvocation */
        public String dateConsumerInvocation {get;set;}
    }
    
    /**
     * @description : Clase el objeto quote del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class quote {
        /** Atributo de Clase: idQuote */
        public String idQuote {get;set;}
        /** Atributo de Clase: compensationPlan */
        public String compensationPlan {get;set;}
        /** Atributo de Clase: credit */
        public String credit {get;set;}
        /** Atributo de Clase: dateQuote */
        public String dateQuote {get;set;}
        /** Atributo de Clase: payment */
        public String payment {get;set;}
        /** Atributo de Clase: status */
        public String status {get;set;}
        /** Atributo de Clase: totalPremium */
        public String totalPremium {get;set;}
        /** Atributo de Clase: totalPremiumLocalCurrency */
        public String totalPremiumLocalCurrency {get;set;}
    }
    
    /**
     * @description : Clase el objeto clientType del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class clientType {
        /** Invocación de Clase: catalogItemBase */
        public catalogItemBase catalogItemBase {get;set;}
    }
    
    /**
     * @description : Clase el objeto catalogItemBase del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class catalogItemBase {
        /** Atributo de Clase: id */
        public String id {get;set;}
        /** Atributo de Clase: name */
        public String name {get;set;}
    }
    
    /**
     * @description : Clase el objeto holder del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class holder {
        /** Atributo de Clase: id */
        public String id {get;set;}
        /** Atributo de account: id */
        public String account {get;set;}
        /** Invocación de Clase: activity */
        public activity activity {get;set;}
        /** Atributo de Clase: email */
        public String email {get;set;}
        /** Invocación de Clase: nacionality */
        public nacionality nacionality {get;set;}
        /** Atributo de Clase: rfc */
        public String rfc {get;set;}
        /** Atributo de Clase: moralPersonalityData */
        public String moralPersonalityData {get;set;}
        /** Invocación de Clase: physicalPersonalityData */
        public physicalPersonalityData physicalPersonalityData {get;set;}
        /** Invocación de Clase: mainAddress */
        public mainAddress mainAddress {get;set;}
        /** Invocación de Clase: mainContactData */
        public mainContactData mainContactData {get;set;}
        /** Invocación de Clase: correspondenceAddress */
        public correspondenceAddress correspondenceAddress {get;set;}
        /** Invocación de Clase: correspondenceContactData */
        public correspondenceContactData correspondenceContactData {get;set;}
        /** Invocación de Clase: legalAddress */
        public legalAddress legalAddress {get;set;}
        /** Invocación de Clase: fiscalContactData */
        public fiscalContactData fiscalContactData {get;set;}
        /** Invocación de Clase: deliveryInformation */
        public deliveryInformation deliveryInformation {get;set;}
    }
    
    /**
     * @description : Clase el objeto activity del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class activity {
        /** Invocación de Clase: catalogItemBase */
        public catalogItemBase catalogItemBase {get;set;}
    }
    
    /**
     * @description : Clase el objeto nacionality del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class nacionality {
        /** Invocación de Clase: catalogItemBase */
        public catalogItemBase catalogItemBase {get;set;}
    }
    
    /**
     * @description : Clase el objeto physicalPersonalityData del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class physicalPersonalityData {
        /** Invocación de Clase: fiscalPersonality */
        public fiscalPersonality fiscalPersonality {get;set;}
        /** Atributo de Clase: name */
        public String name {get;set;}
        /** Atributo de Clase: lastName */
        public String lastName {get;set;}
        /** Atributo de Clase: mothersLastName */
        public String mothersLastName {get;set;}
        /** Atributo de Clase: birthDate */
        public String birthDate {get;set;}
        /** Atributo de Clase: curp */
        public String curp {get;set;}
        /** Invocación de Clase: sex */
        public sex sex {get;set;}
        /** Invocación de Clase: civilStatus */
        public civilStatus civilStatus {get;set;}
        /** Invocación de Clase: occupation */
        public occupation occupation {get;set;}
    }
    
    /**
     * @description : Clase el objeto fiscalPersonality del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class fiscalPersonality {
        /** Invocación de Clase: catalogItemBase */
        public catalogItemBase catalogItemBase {get;set;}
    }
    
    /**
     * @description : Clase el objeto sex del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class sex {
        /** Invocación de Clase: catalogItemBase */
        public catalogItemBase catalogItemBase {get;set;}
    }
    
    /**
     * @description : Clase el objeto civilStatus del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class civilStatus {
        /** Invocación de Clase: catalogItemBase */
        public catalogItemBase catalogItemBase {get;set;}
    }
    
    /**
     * @description : Clase el objeto occupation del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class occupation {
        /** Invocación de Clase: catalogItemBase */
        public catalogItemBase catalogItemBase {get;set;}
    }
    
    /**
     * @description : Clase el objeto mainAddress del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class mainAddress {
        /** Atributo de Clase: door */
        public String door {get;set;}
        /** Atributo de Clase: outdoorNumber */
        public String outdoorNumber {get;set;}
        /** Atributo de Clase: streetName */
        public String streetName {get;set;}
        /** Atributo de Clase: suburb */
        public suburb suburb {get;set;}
        /** Atributo de Clase: zipCode */
        public String zipCode {get;set;}
    }
    
    /**
     * @description : Clase el objeto suburb del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class suburb {
        /** Atributo de Clase: city */
        public String city {get;set;}
        /** Atributo de Clase: county */
        public String county {get;set;}
        /** Invocación de Clase: neighborhood */
        public neighborhood neighborhood {get;set;}
        /** Atributo de Clase: state */
        public String state {get;set;}
    }
    
    /**
     * @description : Clase el objeto neighborhood del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class neighborhood {
        /** Invocación de Clase: catalogItemBase */
        public catalogItemBase catalogItemBase {get;set;}
    }
    
    /**
     * @description : Clase el objeto mainContactData del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class mainContactData {
        /** Invocación de Clase: cellphone */
        public cellphone cellphone {get;set;}
        /** Invocación de Clase: officePhone */
        public officePhone officePhone {get;set;}
        /** Invocación de Clase: phone */
        public phone phone {get;set;}
    }
    
    /**
     * @description : Clase el objeto cellphone del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class cellphone {
        /** Atributo de Clase: phoneExtension */
        public String phoneExtension {get;set;}
        /** Atributo de Clase: telephoneNumber */
        public String telephoneNumber {get;set;}
    }
    
    /**
     * @description : Clase el objeto officePhone del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class officePhone {
        /** Atributo de Clase: phoneExtension */
        public String phoneExtension {get;set;}
        /** Atributo de Clase: telephoneNumber */
        public String telephoneNumber {get;set;}
    }
    
    /**
     * @description : Clase el objeto phone del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class phone {
        /** Atributo de Clase: phoneExtension */
        public String phoneExtension {get;set;}
        /** Atributo de Clase: telephoneNumber */
        public String telephoneNumber {get;set;}
    }
    
    /**
     * @description : Clase el objeto correspondenceAddress del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class correspondenceAddress {
        /** Atributo de Clase: door */
        public String door {get;set;}
        /** Atributo de Clase: outdoorNumber */
        public String outdoorNumber {get;set;}
        /** Atributo de Clase: streetName */
        public String streetName {get;set;}
        /** Atributo de Clase: suburb */
        public suburb suburb {get;set;}
        /** Atributo de Clase: zipCode */
        public String zipCode {get;set;}
    }
    
    /**
     * @description : Clase el objeto correspondenceContactData del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class correspondenceContactData {
        /** Invocación de Clase: cellphone */
        public cellphone cellphone {get;set;}
        /** Invocación de Clase: officePhone */
        public officePhone officePhone {get;set;}
        /** Invocación de Clase: phone */
        public phone phone {get;set;}
    }
    
    /**
     * @description : Clase el objeto legalAddress del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class legalAddress {
        /** Atributo de Clase: door */
        public String door {get;set;}
        /** Atributo de Clase: outdoorNumber */
        public String outdoorNumber {get;set;}
        /** Atributo de Clase: streetName */
        public String streetName {get;set;}
        /** Invocación de Clase: suburb */
        public suburb suburb {get;set;}
        /** Atributo de Clase: zipCode */
        public String zipCode {get;set;}
    }
    
    /**
     * @description : Clase el objeto fiscalContactData del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class fiscalContactData {
        /** Invocación de Clase: cellphone */
        public cellphone cellphone {get;set;}
        /** Invocación de Clase: officePhone */
        public officePhone officePhone {get;set;}
        /** Invocación de Clase: phone */
        public phone phone {get;set;}
    }
    
    /**
     * @description : Clase el objeto deliveryInformation del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class deliveryInformation {
        /** Atributo de Clase: deliveryInstructions */
        public String deliveryInstructions {get;set;}
        /** Atributo de Clase: deliveryTimeEnd */
        public String deliveryTimeEnd {get;set;}
        /** Atributo de Clase: deliveryTimeStart */
        public String deliveryTimeStart {get;set;}
        /** Atributo de Clase: referenceStreets */
        public String referenceStreets {get;set;}
    }
    
    /**
     * @description : Clase el objeto shippingChannel del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class shippingChannel {
        /** Atributo de Clase: id */
        public String id {get;set;}
    }
}