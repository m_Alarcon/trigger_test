/**
 * @description       : 
 * @author            : Eduardo Hernandez Cuamatzi
 * @group             : 
 * @last modified on  : 10-01-2020
 * @last modified by  : Eduardo Hernandez Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   09-30-2020   Eduardo Hernandez Cuamatzi   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_SB_VTS_SendTrackingTelemarketing_Ser {
	
    
    /**
    * @description Recupera status de Cotizacion
    * @author Eduardo Hernandez Cuamatzi | 10-01-2020 
    * @param Map<String String> response Folio encriptado por Oportunidad
    * @param Map<String Opportunity> scopeQuotes Oportunidades a trabajar
    * @return Map<String, Map<String, Object>> Mapa de Oportunidad y Respuesta cotizacion
    **/
    public static Map<String, Map<String,Object>> findQuoteStatus(List<String> response, Map<String,String> folioOpp /*, Map<String, Opportunity> scopeQuotes*/) {
        final Map<String, Map<String,Object>> mapCotStatus = new Map<String, Map<String,Object>>();
        for(String quoteEncrypt : response) {
            final String folioEncrip = '{ "EncryptedId": "'+quoteEncrypt+'"}';
            try {
                final HttpResponse responseEncript = MX_RTL_IasoServicesInvoke_Selector.callServices('GetQuotation', folioEncrip);
                final Map<String,Object> mapValuesEncript = (Map<String,Object>) JSON.deserializeUntyped(responseEncript.getBody());
                Final Map<String,Object> mapTemp = (Map<String,Object>) mapValuesEncript.get('data');
                Final String folioQuote = String.valueOf(mapTemp.get('id'));
                Final String idOpp = folioOpp.get(folioQuote);
                mapCotStatus.put(idOpp,mapValuesEncript);
            } catch(System.CalloutException serviceFail) {
                WB_CrearLog_cls.fnCrearLog(serviceFail.getLineNumber() +' '+serviceFail.getMessage() + ' ' + serviceFail.getCause(), 'WebServiceFail', false);
            }
        }
        return mapCotStatus;
    }

    /**
    * @description Encripta folios de cotización
    * @author Eduardo Hernandez Cuamatzi | 09-29-2020 
    * @param Map<String Opportunity> scopeQuotes Oportunidades con folio
    * @return Map<String, String> Mapa de Oportunidad y folio encriptado
    **/
    public static List<String> encripCots(List<Opportunity> scopeQuotes ) {
        Final List<String> folioQuotes = new List<String>();
        for(Opportunity scopeQuote : scopeQuotes) {
            folioQuotes.add (scopeQuote.FolioCotizacion__c);
        }
        final Map<String,String> encryptedList = MX_SB_VTS_ProxyValueCipher_Service.getIdEncrypted(folioQuotes);
        Final List<String> encryptQuotes = new List<String> ();
        encryptQuotes.addAll(encryptedList.values());
        return encryptQuotes;
    }

    /**
    * @description Crea contrato
    * @author Eduardo Hernandez Cuamatzi | 10-01-2020 
    * @param Map<String Map<String Object>> responseOpp Respuesta de Servicio GetQuotetions
    * @param Opportunity oppRec Oportunidad ah actualizar
    * @return Contract Contrato final
    **/
    public static Contract fillContract(Map<String,Map<String,Object>> responseOpp, Opportunity oppRec) {
        final Contract contractTemp = new Contract();
        final Map<String,Object> response = (Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(responseOpp.get(oppRec.Id)));
        Final Map<String,Object> responseData = (Map<String,Object>)response.get('data');
        final String polize = String.valueOf(responseData.get('number'));
        final Map<String,Object> mapValidity = (Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(responseData.get('validityPeriod')));
        final List<Object> lstCertificates = (List<Object>)JSON.deserializeUntyped(JSON.serialize(responseData.get('premiums')));
        final Map<String,Object> mapCertificates = fillCertifies(lstCertificates);
        final Map<String,Object> mapProduct = (Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(responseData.get('product')));
        final Map<String,Decimal> mapPremiumsVal = fillPremiums(mapCertificates);
        contractTemp.MX_SB_SAC_EstatusPoliza__c = '';
        contractTemp.MX_SB_VTS_FechaInicio__c =  String.valueOf(mapValidity.get('startDate'));
        contractTemp.MX_SB_VTS_FechaFin__c = String.valueOf(mapValidity.get('endDate'));
        contractTemp.MX_VTS_TotalPoliza__c = mapPremiumsVal.get('PRIMANETAANUAL');
        contractTemp.MX_WB_noPoliza__c = polize;
        contractTemp.MX_SB_SAC_NumeroPoliza__c = polize;
        contractTemp.MX_WB_Oportunidad__c = oppRec.Id;
        contractTemp.AccountId = oppRec.AccountId;
        final Set<String> lstProductCode= new Set<String>{String.valueOf(mapProduct.get('id'))};
        final Set<String> lstProcess = new Set<String>{'VTS'};
        for(MX_SB_SAC_Catalogo_Clipert_Producto__c clipProdut : MX_RTL_Catalogo_Clipert_Product_Selector.catalogoClipCod(lstProductCode, lstProcess, true)) {
            contractTemp.MX_WB_Producto__c = clipProdut.MX_SB_SAC_Producto__c;
        }
        return contractTemp;
    }

    /**
    * @description Recupera elementos de certificates
    * @author Eduardo Hernandez Cuamatzi | 10-01-2020 
    * @param List<Object> mapCertificates Mapa de valores certificates
    * @return Map<String, Object> Mapa de elementos certificates
    **/
    public static Map<String, Object> fillCertifies(List<Object> mapCertificates) {
        Map<String,Object> lstMapPremiums = new Map<String,Object>();
        for(Object premiumRec : mapCertificates) {
            lstMapPremiums = (Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(premiumRec));
        }
        return lstMapPremiums;
    }

    /**
    * @description Llena valores de montos
    * @author Eduardo Hernandez Cuamatzi | 10-01-2020 
    * @param Map<String Object> mapCertificates Mapa de valores de elementos premiums
    * @return Map<String, Decimal> Montos a pagar
    **/
    public static Map<String, Decimal> fillPremiums(Map<String, Object> mapCertificates) {
        final Map<String,Decimal> valuesPremiums = new Map<String,Decimal>();
        if(mapCertificates.containsKey('premiums')) {
            final List<Object> premiumEntry = (List<Object>)JSON.deserializeUntyped(JSON.serialize(mapCertificates.get('premiums')));
            Map<String,Object> lstEntryPremiums = new Map<String,Object>();
            for(Object premiumRec : premiumEntry) {
                lstEntryPremiums = (Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(premiumRec));
                final Map<String,Object> totalPremium = (Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(lstEntryPremiums.get('local')));
                valuesPremiums.put((String)lstEntryPremiums.get('type'), (Decimal)totalPremium.get('value'));
            }
        } else {
            valuesPremiums.put('PRIMANETAANUAL', 1.00) ;
            valuesPremiums.put('PRIMANETAFP', 1.00) ;
            valuesPremiums.put('PRIMAPRIMERPAGO', 1.00) ;
            valuesPremiums.put('PRIMAPAGOSUB', 1.00) ;
            valuesPremiums.put('DERECHOPOL', 1.00) ;
        }
        return valuesPremiums;
    }
    
}