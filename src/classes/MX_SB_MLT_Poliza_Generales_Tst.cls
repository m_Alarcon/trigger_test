@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_Poliza_Generales_Tst
* Autor Daniel Perez Lopez
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_Poliza_Generales_cls

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           12/02/2020      Daniel Lopez                        Creación
* --------------------------------------------------------------------------------
*/

public class MX_SB_MLT_Poliza_Generales_Tst {
    @testSetup static void setDataPolGral() {
        final String nameProfilePG = [SELECT Name from profile where name in ('Administrador del sistema','System Administrator') limit 1].Name;
        final User objUsrTstPG = MX_WB_TestData_cls.crearUsuario('PruebaAdminTst', nameProfilePG); 
        insert objUsrTstPG;
        System.runAs(objUsrTstPG) {
        final String  tiporegistroPG = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
        final String  recordTypeCase = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_TomaReporte').getRecordTypeId();
		final Account ctatestPG = new Account(recordtypeid=tiporegistroPG,firstname='DANIEL',lastname='PEREZ',Apellido_materno__pc='LOPEZ',RFC__c='PELD920912',PersonEmail='test@gmail.com');
        insert ctatestPG;
		final Account ctatestPGluffy = new Account(recordtypeid=tiporegistroPG,lastname='luffy',RFC__c='lufy192312',PersonEmail='luffy@gmail.com');
		insert ctatestPGluffy;
            final Contract contrato = new Contract(accountid=ctatestPG.Id,MX_SB_SAC_NumeroPoliza__c='PolizaTest',MX_SB_SAC_RFCAsegurado__c =ctatestPG.rfc__c,MX_SB_SAC_NombreClienteAseguradoText__c=ctatestPG.firstname,MX_WB_apellidoPaternoAsegurado__c=ctatestPG.lastname,MX_WB_apellidoMaternoAsegurado__c=ctatestPG.Apellido_materno__pc);
            insert contrato;
           
            final Siniestro__c sini= new Siniestro__c();
            sini.Folio__c='sinitest';
            sini.MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro';
            sini.MX_SB_SAC_Contrato__c=contrato.Id;
            sini.MX_SB_MLT_Placas__c='MSN-000';
            insert sini;
            final Case objCaseTst = new Case(Status='New',Origin='Phone',Priority='Medium',AccountId=ctatestPG.Id,MX_SB_MLT_URLLocation__c='19.305819,-99.2115422',MX_SB_MLT_Siniestro__c =sini.id, recordtypeid = recordTypeCase);
            insert objCaseTst;
        } 
    } 

    @isTest static void methodConsultaPoliza() {
        final Siniestro__c sini = [SELECT id,Poliza__c,MX_SB_MLT_Certificado_Inciso__c,MX_SB_MLT_Placas__c FROM Siniestro__c WHERE MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro' and Folio__c='sinitest' LIMIT 1];
        Test.startTest();
        final Map<String,String> maparesp= MX_SB_MLT_Poliza_Generales_cls.consultaPoliza(sini.id);
        System.assertEquals(maparesp.get('nombre'),'DANIEL','exito en response');
        Test.stopTest();
    } 
}