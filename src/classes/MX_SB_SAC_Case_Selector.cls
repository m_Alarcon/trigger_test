/**
 * @description       : 
 * @author            : Gerardo Mendoza Aguilar
 * @group             : 
 * @last modified on  : 02-17-2021
 * @last modified by  : Gerardo Mendoza Aguilar
 * Modifications Log 
 * Ver   Date         Author                    Modification
 * 1.0   02-11-2021   Gerardo Mendoza Aguilar   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_SAC_Case_Selector {

    /**
    * @description Udpate case
    * @author Gerardo Mendoza Aguilar | 02-11-2021 
    * @param listCase 
    **/
    public static void updateCase(List<Case> listCase) {
        if(!listCase.isEmpty()) {
            update listCase;
        }
    }
}