/*
*
* @author Arsenio Perez Lopez
* @description Banquero Process
*
*           No  |     Date     |     Author      |    Description
* @version  1.0    04/10/2019     Arsenio Perez Lopez     Reassignated Account, Opportunities reassignated to Telemarketing
*/
public without sharing class MX_SB_BCT_Ban_Baja_cls {//NOSONAR
    
     /*Llamado a conf personalizada */
        public static final String IDOWNTLMK_DEF =[SELECT Id, FederationIdentifier FROM User where FederationIdentifier=:MX_SB_VTS_Generica__c.getValues('MX_SB_BCT_Baja_Banquero_Default').MX_SB_BCT_user_Banquero_Default__c limit 1].id;
    
    /**
    *@Autor: Arsenio Perez
    *@Method: searchAccountBanquero
    *@Description: Decide si se tiene que ejecutar el method de carga o el de eliminacion de registros.
    *@Param:BanList -->Lista de trabajo de banquero.
     */
    public static void searchAccountBanquero(List<Banquero__c> banList) {
        
        final List<Banquero__c> alta = new List<Banquero__c>();
        final List<Banquero__c> baja = new List<Banquero__c>();
        for(Banquero__C t: banList) {
            if(t.MX_SB_BCT_Suspendido__c) {
                baja.add(t);
            } else {
                alta.add(t);
            }
        }
            if(alta.isEmpty()==false) {
                MX_SB_BCT_Ban_Convert_cls.searchAccountBanquero(alta);
            }
            if(baja.isEmpty()==false) {
                MX_SB_BCT_Ban_Baja_cls.downAccountBanquero(baja);
            }
    }
    
    /*Realiza el llamado al method searchToAssign() por cada una de las cuentas que se encontraron el Salesforce
        * @param List<sObject> x:  Lista de Cuentas existentas encontradas en SF.*/ 
        public static void assignToTelemarketing(List<sObject> xList) {
             final List<sObject> updateObjTotal = new List<sObject> ();
             for(SObject t: xList) {
                 final List<sObject> updateObj =MX_SB_BCT_Ban_Convert_cls.searchToAssign((String)t.get('Id'),String.valueOf(t.get('OwnerId')),'ASD');
                 updateObj.add(t);
                 if(updateObj.isEmpty() == false) {
                     updateObjTotal.addAll(updateObj);
                 }
               }
             update updateObjTotal;
        }
	
    /** Realiza el proceso de Reasignación de Cuentas y Oportunidades a usuario Default TLMK */
    public static void downAccountBanquero(List<Banquero__c> banList) {
		final Set<String> clien  = MX_SB_BCT_Ban_Convert_cls.getListDataInput(banList, true);
        final List<Account> banAccount = MX_SB_BCT_Ban_Convert_cls.recoverData(clien,'Account','MX_SB_BCT_Id_Cliente_Banquero__c',',OwnerId, MX_SB_BCT_Es_Banquero_Contigo__c');
        for(Account acc: banAccount) {
            acc.OwnerId = IDOWNTLMK_DEF;
            acc.MX_SB_BCT_Es_Banquero_Contigo__c = false;
        }
        assignToTelemarketing(banAccount);
    }
}