/**-------------------------------------------------------------------------
* Name: Utility Quote Class
* @author Julio Medellin
* Proyect: MW SB VTS - BBVA
* Description : Utility class for the Quotes
* --------------------------------------------------------------------------
*                         Date           Author                   Description
* -------------------------------------------------------------------
* @version 1.0           May/22/2019      Julio Medellin            Header
* @version 1.1           May/27/2019      Jaime Terrats         Add new method to get pricebook entry id
* @version 1.1.1         Jun/06/2019      Eduardo Hernández     Fix para cotización Pricebook2Id
* @version 1.2           Jun/19/2019      Jaime Terrats         Se agrega validacion de numero de poliza cuando
*                                                               se emite un quote
* @version 1.2.1         06/28/2019       Eduardo Hernandez     Correccion para emisión de contratos
* @version 1.2.2         06/28/2019       Jaime Terrats         Refactor de method prepareQuoteSync
*--------------------------------------------------------------------------------**/

/**
* @description: update quote for synchronization
*/
public without sharing class MX_SB_VTS_utilityQuote {//NOSONAR

    /*
    * method to sync opportunity with quote
    * @param quoteToSync quote object from service
    */
    public static string prepareQuoteSync(Quote quoteToSync) {
        final String sOpportunityId = quoteToSync.OpportunityId;
        if(quoteToSync.status.equalsIgnoreCase(System.Label.MX_SB_VTS_STATUSEMITIDA_LBL) && String.isNotBlank(quoteToSync.MX_SB_VTS_Numero_de_Poliza__c)) {
            final Opportunity opp = [SELECT Id,SyncedQuoteId FROM Opportunity WHERE Id=: sOpportunityId];
            opp.SyncedQuoteId=quoteToSync.Id;
            Database.update(opp);
        }
        return  sOpportunityId;
    }

    /**
     * getPricebookEntry Obtine el precio
     * @param	presupuesto	Busca el presupuesto
     * @param	quoli	Id del quoli
     * @param	datosCotizacion	Datos de la cotización
     * @return	PriceBookEntryId	Regresa Id
    */
    public static Id getPricebookEntry(Quote presupuesto, QuoteLineItem quoli, List<String> datosCotizacion) {
        Id pbe1;
        for ( PricebookEntry pbe : [select Id,Pricebook2Id FROM PricebookEntry where Product2Id =: datosCotizacion[0] ] ) {
            quoli.PricebookEntryId = pbe.Id;
            pbe1 = pbe.Id;
        }
        return pbe1;
    }

    /**
     * getPricebookEntry Obtine el precio
     * @param	presupuesto	Busca el presupuesto
     * @param	quoli	Id del quoli
     * @param	datosCotizacion	Datos de la cotización
     * @return	PriceBookEntryId	Regresa Id
    */
    public static Id findPriceBook(String productId) {
        Id pbQuote;
        for ( PricebookEntry pbe : [select Id,Pricebook2Id FROM PricebookEntry where Product2Id =: productId] ) {
            pbQuote = pbe.Pricebook2Id;
        }
        return pbQuote;
    }

    /*
    * Returns a boolean value to validate input
    * @params value: incoming string from api
    */
    public static Boolean checkBooleanValues(Boolean value) {
        return String.isNotBlank(String.valueOf(value)) ? true : false;
    }

    /**
    * @description revisa emails y números en lista negra
    * @author Eduardo Hernández Cuamatzi | 5/3/2020 
    * @param emailClient 
    * @param phoneClient 
    * @param movilClient 
    * @return Boolean 
    **/
    public static Boolean validateEmails(String emailClient, String phoneClient, String movilClient) {
        Boolean isBlacklist = false;
        final List<String> qaEmails = new List<String>();
        qaEmails.add(emailClient);
        qaEmails.add(phoneClient);
        qaEmails.add(movilClient);
        final Map<String,String>  mapaBlackList = Utilities.verificarListaNegra(qaEmails);
        for(String valueList : mapaBlackList.values()) {
            if(valueList.equalsIgnoreCase(emailClient) || valueList.equalsIgnoreCase(phoneClient) || valueList.equalsIgnoreCase(movilClient)) {
                isBlacklist = true;
            }
        }
        return isBlacklist;
    }

    /**
    * @description Busca cuentas unicas
    * @author Eduardo Hernández Cuamatzi | 5/3/2020 
    * @param emailClient correo a buscar
    * @return Account regitro de cuenta encontrada
    **/
    public static Account findUniqueAccount(String emailClient) {
        Account accountVal = new Account();
        for(Account account : [Select Id, FirstName, LastName, Apellido_materno__pc, PersonBirthdate,
            PersonEmail, PersonHomePhone, PersonMobilePhone, BillingStreet,
            BillingCity, BillingPostalCode, BillingState, BillingCountry,
            Numero_Exterior__c, Numero_Interior__c,Oportunidad_Totales__c from Account 
            where (NOT RecordType.developername LIKE '%BPP%') AND (NOT RecordType.developername LIKE '%BPyP%') AND AccountSource NOT IN ('') AND 
            AccountSource IN ('Inbound','inbound','Call me back','Facebook','Tracking Web','Outbound','outbound','Telemarketing') 
            AND canal__c NOT IN ('') AND No_de_cliente__c IN ('') AND PersonEmail =: emailClient ORDER BY CREATEDDATE ASC]) {
            accountVal = account;
        }
        return accountVal;
    }

    /**
    * @description recupoera una cuenta mediante el Id indicado
    * @author Eduardo Hernández Cuamatzi | 5/3/2020 
    * @param Id de la cuenta a recuperar 
    * @return Cuenta encontrada
    **/
    public static Account findUniqueAccount(Id accountId) {
       return [Select Id, FirstName, LastName, Apellido_materno__pc, PersonBirthdate,
            PersonEmail, PersonHomePhone, PersonMobilePhone, BillingStreet,
            BillingCity, BillingPostalCode, BillingState, BillingCountry, Num_Opps__c,
            Numero_Exterior__c, Numero_Interior__c,Oportunidad_Totales__c from Account 
            where Id =: accountId];
    }

    /**
    * @description recupera una cotización mediante el folio de cot indicado
    * @author Eduardo Hernández Cuamatzi | 5/3/2020 
    * @param folioCot folio de cotización
    * @return Quote cotización encontrada
    **/
    public static Quote findQuote(String folioCot) {
        Quote newQuote = new Quote();
        for(Quote quot : [Select Id, Name, AccountId, Status, MX_SB_VTS_Folio_Cotizacion__c,MX_SB_VTS_ASO_FolioCot__c, MX_SB_VTS_Numero_de_Poliza__c,
            ShippingStreet, ShippingPostalCode, ShippingCity, ShippingState, OpportunityId, Opportunity.LeadSource,
            ShippingCountry, MX_SB_VTS_Numero_Exterior__c, MX_SB_VTS_Numero_Interior__c,OwnerId
            from Quote where MX_SB_VTS_ASO_FolioCot__c =: folioCot OR Id =: folioCot ORDER BY CREATEDDATE ASC]) {
                newQuote = quot;
        }
        return newQuote;
    }

    /**
    * @description recupera las opp de la cotización entrante
    * @author Eduardo Hernández Cuamatzi | 5/3/2020 
    * @param accountId Id de la cuenta a buscar
    * @param product Producto de la Oportunidad a buscar
    * @return List<Opportunity> lista de Oportunidades encontradas
    **/
    public static List<Opportunity> findOppQuote(String accountId, String product, string origen) {
        String origenStr = '';
        switch on origen {
            when 'inbound' {
                origenStr = 'Tracking web';
            }
            when 'outbound' {                    
                origenStr = 'Outbound TLMK';
            }
        }
        final DateTime systemTime = System.now();
        final DateTime limitTime = systemTime.addHours(-24);
        return [Select Id, Name, StageName, FolioCotizacion__c,
        Reason__c, Producto__c, MX_SB_VTS_Aplica_Cierre__c,Opportunity.AccountId, OwnerId
        from Opportunity where AccountId =: accountId
        and Producto__c =: product
        and Probability > 1 and Probability < 100 AND LeadSource =: origenStr AND CREATEDDATE >=: limitTime order by CreatedDate desc];
    
    }

    /**
    * @description llena los campos base de una cuenta
    * @author Eduardo Hernández Cuamatzi | 5/3/2020 
    * @param accountFill cuenta a llenar
    * @param nombre nombre del cliente
    * @param email correo del cliente
    * @param apPaterno apellido parteno del cliente
    * @param appMaterno apellido materno del cliente
    * @return Account cuenta del cliente
    **/
    public static Account fillAccount(Account accountFill, List<String> vals) {
        final Account fillAccount = accountFill;
        fillAccount.FirstName = vals[0];
        fillAccount.PersonEmail = vals[1];        
        fillAccount.LastName = vals[2];
        fillAccount.Apellido_materno__pc = vals[3];
        fillAccount.RecordTypeId = Schema.SobjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
        return fillAccount;
    }

    /**
    * @description 
    * @author Eduardo Hernández Cuamatzi | 18/3/2020 
    * @param provider proveedor
    * @param lstUsers lista de usuarios
    * @return Id Nuevo owneer
    **/
    public static Id findOwnerProvider(String provider, List<User> lstUsers) {
        Id userOwner = UserInfo.getUserId();
        for(User userApp : lstUsers) {
            if(userApp.MX_SB_VTS_ProveedorCTI__c.equalsIgnoreCase(provider)) {
                userOwner = userApp.Id;
            }
        }
        return userOwner;
    }

    /**
    * @description Compara folios de tracking
    * @author Eduardo Hernández Cuamatzi | 11/5/2020 
    * @param folioCot folio nuevo
    * @param folioTrack  folio existente
    * @return Quote objeto encontrado
    **/
    public static Quote findUniqueQuote(String folioCot, String folioTrack) {
        String folio = folioCot;
        if(String.isNotBlank(folioTrack) && String.isNotEmpty(folioTrack)) {
            folio = folioTrack;
        }
        return MX_SB_VTS_utilityQuote.findQuote(folio);
    }

    /**
    * @description Recupera la Oportunodad que se esta trabajando
    * @author Eduardo Hernandez Cuamatzi | 22/5/2020 
    * @param String oppIdTrack 
    * @return Opportunity 
    **/
    public static Opportunity finalOppToWork(Quote oppIdTrack, List<Opportunity> oppWorking) {
        String oppId = '';
        if(String.isNotBlank(oppIdTrack.OpportunityId)) {
            oppId = oppIdTrack.OpportunityId;
        } else {
            oppId = oppWorking[0].Id;
        }
        return [Select Id, Name, StageName, FolioCotizacion__c,
        Reason__c, Producto__c, MX_SB_VTS_Aplica_Cierre__c,Opportunity.AccountId, OwnerId, CreatedDate,
        EnviarCTI__c, MX_WB_EnvioCTICupon__c, Owner.Name, Owner.MX_SB_VTS_ImplantConnected__c, LastModifiedDate
        from Opportunity where Id =: oppId];
    }
}