/**
* @FileName          : MX_SB_VTS_wrpEncrypted
* @description       : Wrapper class for use de web service of ProxyValueCipher
* @Author            : Marco Antonio Cruz Barboza  
* @last modified on  : 18-09-2020
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   18-09-2020   Marco Antonio Cruz Barboza          Initial Version
**/
@SuppressWarnings('sf:AvoidGlobalModifier, sf:UseSingleton')
public class MX_SB_VTS_wrpEncrypted {
    /** Wrapper Call*/
    public List<String> results {get;set;}

}