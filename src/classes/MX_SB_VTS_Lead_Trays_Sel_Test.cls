/**
 * @File Name          : MX_SB_VTS_Lead_Trays_Sel_Test.cls
 * @Description        : 
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 8/6/2020 11:21:21
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    8/6/2020   Eduardo Hernández Cuamatzi     Initial Version
**/
@isTest
private class MX_SB_VTS_Lead_Trays_Sel_Test {
    
    @TestSetup
    static void makeData() {
        final MX_WB_FamiliaProducto__c objFamilyPro2 = MX_SB_VTS_CallCTIs_utility.newFamiliy(System.Label.MX_SB_VTS_Hogar);
        insert objFamilyPro2;
        final Product2 proHogar = MX_SB_VTS_CallCTIs_utility.newProduct(System.Label.MX_SB_VTS_Hogar, objFamilyPro2);
        insert proHogar;
        final String[] valuesTray = new String[]{'HotLeads', 'BandejHot', '72', 'HotLeads'};
        final MX_SB_VTS_ProveedoresCTI__c smartProv = MX_SB_VTS_CallCTIs_utility.newProvee('Smart Center', 'Smart Center');
        insert  smartProv;
        final MX_SB_VTS_Lead_tray__c hotLeadsSmart =  MX_SB_VTS_CallCTIs_utility.newTrayHotlead(valuesTray, proHogar, smartProv);
        insert hotLeadsSmart;
    }

    @isTest
    private static void fillMaptrays() {
        Test.startTest();
            final List<MX_SB_VTS_Lead_tray__c> lstTray = MX_SB_VTS_Lead_Trays_Selector.findTraysHotLeads();
            System.assertEquals(lstTray.size(), 1, 'Bandeja');
        Test.stopTest();
    }
}