/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_TipoMinuta_Selector
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2021-01-19
* @Description 	Selector for MX_BPP_TipoMinuta__mdt
* @Changes
*  
*/
public without sharing class MX_BPP_TipoMinuta_Selector {
    
    /** Constructor */
    @TestVisible
    private MX_BPP_TipoMinuta_Selector() {}
    
    /**
    * @description
    * @author Edmundo Zacarias Gómez
    * @param String queryfields, String conditionField, Boolean withCondition, List<String> listTVisita
    * @return List<MX_BPP_CatalogoMinutas__c>
    **/
    public static List<MX_BPP_TipoMinuta__mdt> getRecordsByObjetivo(String queryfields, List<String> listTVisita) {
        final String query ='SELECT '+ queryfields +' FROM MX_BPP_TipoMinuta__mdt WHERE MX_ObjetivoVisita__c IN: listTVisita';
        return Database.query(String.escapeSingleQuotes(query));
    }

}