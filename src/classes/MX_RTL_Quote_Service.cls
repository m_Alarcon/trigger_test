/**
* @File Name          : MX_RTL_Quote_Service.cls
* @Description        :
* @Author             : Juan Carlos Benitez
* @Group              :
* @Last Modified By   : Daniel Perez Lopez
* @Last Modified On   : 12-15-2020
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    7/02/2020   Juan Carlos Benitez          Initial Version
* 1.1    19/08/2020    Omar Gonzalez Rubio        Se agregan nuevas funciones
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_RTL_Quote_Service {
    /** Integer ONE*/  
    final static Integer ONE =1; 
    /* Return Quote*/
    final static List<Quote> QUOT = new List<Quote>();
    /* Variable que almacena el mensaje del asesor */
    private static String msjAsesor = '';
    /**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param quoteObj
* @return QuoteObj
**/    
    public static List<Quote> getUpsquot(Quote quoteObj) {
        Final list<Quote> quotr = MX_RTL_Quote_Selector.getQuote(quoteObj);
        if(quotr.size()<ONE) {
            QUOT.add(MX_RTL_Quote_Selector.upsrtQuote(quoteObj));
        } else {
				QUOT.addAll(quotr);
        }
		return QUOT;
    }
    /*
* 	funcion que retorna la cotizacion donde se almacenaran los Datos
*/
    public static List<Quote> getQuoteData(String quoteFol,String quoteProd) {
        return MX_RTL_Quote_Selector.quoteData(quoteFol,quoteProd);
    }
    /*
* 	funcion que retorna la actualizacion de una cotizacion 
*/
    public static Quote upserQuote(Quote cotIvr) {
        return MX_RTL_Quote_Selector.upsrtQuote(cotIvr);
    }
     /*
* 	funcion que retorna la cotizacion donde se almacenaran los Datos
*/
    public static List<Quote> getResponseIvr(String folQuote,String idQuote) {
        return MX_RTL_Quote_Selector.quoteResponseIvr(folQuote,idQuote);
    }
	/*
* 	Obtiene datos de QuoteLineItem
*/
    public static List <QuoteLineItem> selSObjectsByQuotes(Set<Id> ids) {
        return QuoteLineItemsSelector.selSObjectsByQuotes(ids);
    }
    /*
* 	funcion que recupera los datos de Idcotiza y token antifraude
*/
    public static Quote getRetriveQuote(String idOppQuote,String folQuote) {
        return MX_RTL_Quote_Selector.quoteRetrieve(idOppQuote,folQuote);
    }
    /*
* 	funcion que valida el codigo de respuesta del Ivr
*/
    public static Quote getResIvr(String folQuote,String idQuote) {
        final List<Quote> quoteRes = MX_RTL_Quote_Selector.quoteResponseIvr(folQuote,idQuote);
        if(String.isEmpty(quoteRes[0].MX_ASD_PCI_ResponseCode__c) || quoteRes[0].MX_ASD_PCI_ResponseCode__c.contains('""')) {
            quoteRes[0].MX_ASD_PCI_ResponseCode__c = '404';
            quoteRes[0].MX_ASD_PCI_MsjAsesor__c = String.isEmpty(quoteRes[0].MX_ASD_PCI_MsjAsesor__c) || quoteRes[0].MX_ASD_PCI_MsjAsesor__c.contains('""') ? 'Ocurrio un error en el cobro' : quoteRes[0].MX_ASD_PCI_MsjAsesor__c;
        } else {
            quoteRes[0].MX_ASD_PCI_MsjAsesor__c = validCodeResp(quoteRes[0].MX_ASD_PCI_ResponseCode__c, quoteRes[0].MX_ASD_PCI_MsjAsesor__c);
        }
        quoteRes[0].MX_ASD_PCI_DescripcionCode__c = quoteRes[0].MX_ASD_PCI_ResponseCode__c == '0' ? 'OK' : 'NOK';
        return MX_RTL_Quote_Selector.upsrtQuote(quoteRes[0]);
    }
    /*funcion que valida el codigo de respuesta que retorna el IVR*/
    public static String validCodeResp(String codeIvr, String msjIvrResp) {
        final String CODE = msjIvrResp.contains('paymentFailed#') ? msjIvrResp.substring(14,18) : codeIvr;
        final List<MX_RTL_IVRCodes__mdt> codeMsj = MX_RTL_Quote_Selector.getMsjError(CODE);
        if(codeMsj.isEmpty()) {
            msjAsesor = CODE == '0' ? 'El cobro se realizo con exito' : 'Ocurrio un error en el cobro';
        } else {
            msjAsesor = codeMsj[0].MX_RTL_MsgSF__c;
        }
        return msjAsesor;
    }
}