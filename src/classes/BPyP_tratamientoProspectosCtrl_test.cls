/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase test de BPyP_tratamientoProspectosCtrl

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     04/02/2020      Roberto Isaac Soto Granados           Creación Clase
*****************************************************************************************/
@isTest
public class BPyP_tratamientoProspectosCtrl_test {
	/*Usuario de pruebas director*/
    private static User testUserDir = new User();
    /*Usuario de pruebas banquero*/
    private static User testUserBan = new User();
    /*cuenta de pruebas*/
    private static Account testAcc = new Account();

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testUserDir = UtilitysDataTest_tst.crearUsuario('testUserDir', 'BPyP Director Oficina', 'BPYP DIRECTOR OFICINA TORRE BANCOMER');
        insert testUserDir;
        testUserBan = UtilitysDataTest_tst.crearUsuario('testUserBan', 'BPyP Estandar', 'BPYP BANQUERO BANCA PERISUR');
        testUserBan.Director_de_oficina__c = testUserDir.Id;
        insert testUserBan;

        System.runAs(testUserDir){
            testAcc.LastName = 'testAcc';
            testAcc.FirstName = 'testAcc';
            testAcc.OwnerId = testUserDir.Id;
            testAcc.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND DeveloperName = 'MX_BPP_PersonAcc_NoClient' LIMIT 1].Id;
            insert testAcc;
        }
    }

    /*Prueba las cuentas del director de oficina*/
    static testMethod void testGetAccounts() {
        testUserDir = [SELECT Id, Title FROM User WHERE LastName = 'testUserDir'];
        testAcc = [SELECT Id, Name, BPyP_ls_Estado_del_candidato__c, PersonEmail, Origen_del_candidato__c, Oficina_Gestora__c, BPyP_dv_Monto_de_inversi_n__c, Tipo_de_Registro_Manual__c, RecordTypeId FROM Account WHERE LastName = 'testAcc'];
        System.runAs(testUserDir) {
            Test.startTest();
            final Account accTest = BPyP_tratamientoProspectosCtrl.getAccounts()[0];
            Test.stopTest();
            System.assertEquals(testAcc, accTest, 'Cuentas encontradas');
        }
    }

    /*Prueba los usuarios subordinados del director de oficina*/
    static testMethod void testSearchUser() {
        testUserDir = [SELECT Id, Title FROM User WHERE LastName = 'testUserDir'];
        testUserBan = [SELECT Id, Name FROM User WHERE LastName = 'testUserBan'];
        System.runAs(testUserDir) {
            Test.startTest();
            final User userTest = BPyP_tratamientoProspectosCtrl.searchUser('testUserBan')[0];
            Test.stopTest();
            System.assertEquals(testUserBan, userTest, 'Usuarios encontradas');
        }
    }

    /*Prueba la asignación de los prospectos a los banqueros */
    static testMethod void testSaveNewOwner() {
        testUserDir = [SELECT Id, Title FROM User WHERE LastName = 'testUserDir'];
        testUserBan = [SELECT Id, Name FROM User WHERE LastName = 'testUserBan'];
        testAcc = [SELECT Id FROM Account WHERE LastName = 'testAcc'];
        System.runAs(testUserDir) {
            Test.startTest();
            BPyP_tratamientoProspectosCtrl.saveNewOwner(testUserBan.Id, new List<String>{testAcc.Id});
            Test.stopTest();
        }
        testAcc = [SELECT OwnerId FROM Account WHERE OwnerId =: testUserBan.Id];
        System.assertEquals(testUserBan.Id, testAcc.OwnerId, 'Cambio de propietario correcto');
    }

    /*Prueba el descarte de los prospectos*/
    static testMethod void testDiscardAccounts() {
        testUserDir = [SELECT Id, Title FROM User WHERE LastName = 'testUserDir'];
        testAcc = [SELECT Id FROM Account WHERE LastName = 'testAcc'];
        System.runAs(testUserDir) {
            Test.startTest();
            BPyP_tratamientoProspectosCtrl.discardAccounts(new List<String>{testAcc.Id});
            Test.stopTest();
        }
        testAcc = [SELECT BPyP_ls_Estado_del_candidato__c FROM Account WHERE OwnerId =: testUserDir.Id];
        System.assertEquals('Descartado', testAcc.BPyP_ls_Estado_del_candidato__c, 'Descarte correcto');
    }
}