/**
 * @description       : Clase que permite la obtencion del script guia para el asesor.
 * @author            : Alan Ricardo Hernandez
 * @group             : 
 * @last modified on  : 22/02/2021
 * @last modified by  : Alan Ricardo Hernandez
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   15/02/2021   Alan Ricardo Hernandez            Initial Version
**/
@supresswarnings('sf:UseSingleton')

public with sharing class MX_SB_PS_ScriptComponet_Ctrl {

    /** Constante condicionante query */
    final static  String CONDITIONFIELD = 'Activo__c = true';

    /** Constante condicionante methodo */
    final static Boolean WHITHCONDITION = true;

    /**
    * @description Constructor privado
    * @author Alan Ricardo Hernandez | 19/02/2021 
    * @return |private 
    **/
    private MX_SB_PS_ScriptComponet_Ctrl() {}

    /**
    * @description Permite obtener la informacion del script guia que se encuentra en la custom metadata.
    * @author Alan Ricardo Hernandez | 19/02/2021 
    * @return List<MX_SB_PS_ScriptComponent__mdt> 
    **/
    @AuraEnabled(cacheable=true)
    public static List<MX_SB_PS_ScriptComponent__mdt> obtenerDatos() {
        String queryField = 'Activo__c,DeveloperName,Id,';
        queryField += 'MasterLabel,Parrafo1__c,Parrafo2__c,Parrafo3__c,Parrafo4__c,';
        queryField += 'Parrafo5__c,Parrafo6__c,Parrafo7__c,Parrafo7_2__c,Parrafo8__c,';
        queryField += 'Parrafo9__c,Parrafo10__c,Parrafo11__c,Parrafo12__c,Parrafo13__c,';
        queryField += 'Parrafo14__c,Parrafo15__c,Parrafo16__c,Parrafo17__c,Parrafo18__c,';
        queryField += 'Parrafo19__c,Parrafo20__c,Parrafo21__c,Parrafo22__c,Parrafo23__c,';
        queryField += 'Parrafo24__c,Titulo1__c,Titulo2__c,Titulo3__c,Titulo4__c,Titulo5__c,';
        queryField += 'Titulo6__c,Titulo7__c,Titulo8__c,Titulo9__c,Titulo10__c,Titulo11__c,';
        queryField += 'Titulo12__c,Titulo13__c,Titulo14__c,Titulo15__c,Titulo16__c,';
        queryField += 'Titulo17__c,Titulo18__c,Titulo19__c,Titulo20__c,Titulo21__c,';
        queryField += 'Titulo22__c,Titulo23__c,Titulo24__c'; 
        return MX_RTL_ScriptComponent_Selector.resultadosQueryScript(queryField,CONDITIONFIELD,WHITHCONDITION);  
    }

    /**
    * @description Permite obtener la informacion del usuario actual.
    * @author Alan Ricardo Hernandez | 19/02/2021 
    * @param recId 
    * @return User 
    **/
    @AuraEnabled(cacheable=true)
    public static User getUserDetails(String recId) {
        User info;
        if( User.SObjectType.getDescribe().isAccessible() &&
            Schema.SObjectType.User.fields.Id.isAccessible() &&
            Schema.SObjectType.User.fields.Name.isAccessible()) {
            info = [SELECT Id,Name,IsActive FROM User WHERE Id =:recId];
        }
        return info;
    }
}