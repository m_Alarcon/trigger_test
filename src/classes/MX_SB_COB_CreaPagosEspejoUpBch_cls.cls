/**
*
*Clase MX_SB_COB_CreaPAgosEspejoUpBch_cls
*/
global class MX_SB_COB_CreaPagosEspejoUpBch_cls implements Database.Batchable<PagosEspejo__c>,Database.AllowsCallouts, Database.Stateful {
	/**propiedades */
    /**Map<String, PagosEspejo__c> mapNoPolObjPagEspUpd{get;set;} //mapa para upsert*/
    global List<PagosEspejo__c> pagosIn { get;set; }
    /**Integer iCntActOk */
    global Integer iCntActOK { get;set; }
    /** Integer iCntActNoOk*/
    global Integer iCntActNoOk { get;set; }
    /** String sResulProcArchivo*/
    global String sResulProcArchivo { get;set; }
    /** String bloqueId*/
    global String bloqueId { get;set; }
    /** List String pagosEspejoIds*/
    global List<String> pagosEspejoIds { get;set; }

    /**constructor*/
    global MX_SB_COB_CreaPagosEspejoUpBch_cls(List<PagosEspejo__c> parametro) {

        //this.mapNoPolObjPagEspUpd =  new Map<String, PagosEspejo__c>();
        this.iCntActOK =0;
        this.iCntActNoOk=0;
        this.sResulProcArchivo='';
        this.pagosIn = parametro;
        
        this.bloqueId = parametro[0].MX_SB_COB_BloqueLupa__c;//
        this.pagosEspejoIds = new List<String>();//se inicializa la lista de ids nuevos o editados

    }

    /**start*/
    global List<PagosEspejo__c> start(Database.BatchableContext batCont) {
        return this.pagosIn;
    }

    /**Ejecutar multiple veces */
    global void execute(Database.BatchableContext batCont,List<PagosEspejo__c> scope) {

       try {
        final Database.UpsertResult[] lDtUrsNvos = Database.upsert(scope, PagosEspejo__c.MX_SB_COB_Factura__c ,true);

        //iteracion de de guardado
        for (Database.Upsertresult objDtUrsNvo : lDtUrsNvos) {
                if (objDtUrsNvo.isSuccess()) {
                    if(objDtUrsNvo.isCreated()) {
                        //insert
                        System.debug('ENTRO A CargaArchivoRecuperacion_ctr.procesarArchivoFT SI CREO EL PAGO ESPEJO2: ' + objDtUrsNvo.getId());
                        this.pagosEspejoIds.add(objDtUrsNvo.getId());
               		 } else {
                        //update
                        System.debug('ENTRO A CargaArchivoRecuperacion_ctr.procesarArchivoFT SI ACTUALIZO EL PAGO ESPEJO2: ' + objDtUrsNvo.getId());
               		 	this.pagosEspejoIds.add(objDtUrsNvo.getId());
                     }
                	iCntActOK++;
                } else {
                    //error de guardado
                    sResulProcArchivo += objDtUrsNvo.getErrors().get(0).getMessage() + '\n';
					System.debug('ENTRO A CargaArchivoRecuperacion_ctr.procesarArchivoFT NO CREO EL PAGO ESPEJO sResulProcArchivo2: ' + sResulProcArchivo);
                    iCntActNoOk++;
                }//Fin si objDtUrs.isSuccess()
			}//	Fin del for para Database.Upsertresult
       } catch(Exception e) {
           system.debug('Error: '+e.getMessage());
           sResulProcArchivo += e.getMessage() + '\n';
           iCntActNoOk++;
       }

    }//fin execute
    /**Method finish */
    global void finish(Database.BatchableContext batCont) {

        //carga archivo para guardado de log
        final List<bloque__c> bloque = [select id,name from bloque__c where id= :bloqueId];

        List<cargaArchivo__c> arch = [select id,MX_SB_COB_ContadorConError__c,MX_SB_COB_ContadorSinError__c,MX_SB_COB_Bloque__c,MX_SB_COB_Resultado__c from cargaArchivo__c where MX_SB_COB_Bloque__c=:bloque[0].name limit 1];
        //update bloque
        if(arch.isEmpty()==false) {
            system.debug('Se actualiza cargaarchivo con id='+arch[0].id);
            system.debug('errores de batch='+iCntActNoOk);
            system.debug('Sin error de batch='+iCntActOK);
            system.debug('log de error, solo si existe='+sResulProcArchivo);

            //actualizacion de registros
            arch[0].MX_SB_COB_ContadorConError__c +=iCntActNoOk;
            arch[0].MX_SB_COB_ContadorSinError__c +=iCntActOK;
            String resTemp = arch[0].MX_SB_COB_Resultado__c;
            resTemp += sResulProcArchivo;//se añade el resultado de este batch
            if(resTemp.length()>31000) {//se verifica que no sobrepase el tamaño
                resTemp = resTemp.substring(0, 31000);
            }
            arch[0].MX_SB_COB_Resultado__c = resTemp;

            update arch;
        } else {
            system.debug('Error al actualizar cargaarchivo de batch creapagosespejo');
        }//fin if existe cargaarchivo

        //15oct act
     if(pagosEspejoIds.size()>0) {


        final MX_SB_COB_CreaPagosSch_cls objCreaPagosSch = new MX_SB_COB_CreaPagosSch_cls();
		//se agegan parametros
		objCreaPagosSch.sIdBloque = bloqueId;
        objCreaPagosSch.pagosEspejoIds = pagosEspejoIds;

		final DateTime dtFechaHoraAct = DateTime.now();
		final Date dFechaActual = dtFechaHoraAct.date();
		final Time tmHoraActual = dtFechaHoraAct.Time();
		final Time tmHoraActualEnv = tmHoraActual.addMinutes(1);
        final Integer randomNumber = Integer.valueof(Math.random() * 100);
        final Integer randomNumber2 = Integer.valueof(Math.random() * 100);

		String sch = '';

		sch = '0 ' + tmHoraActualEnv.minute() + ' ' + tmHoraActualEnv.hour() + ' ' + dFechaActual.day() + ' ' + dFechaActual.month() + ' ?';
        final String sNombreProc = tmHoraActualEnv.minute() + ' : ' + tmHoraActualEnv.hour() + ' : ' + dFechaActual.day() + ' : CreaPagos : '+randomNumber+' : '+randomNumber2;
        System.debug('EN ActivaProcNotif_tgr sNombreProc: ' + sNombreProc + ' sch: ' + sch);
        if (!Test.isRunningTest()) {
            System.schedule(sNombreProc, sch, objCreaPagosSch);
        }
      }//sin pagoespejo creado no se envia

    }

}