/**
 * @description       : 
 * @author            : Eduardo Hernandez Cuamatzi
 * @group             : 
 * @last modified on  : 09-04-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   08-20-2020   Eduardo Hernandez Cuamatzi   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_leadMultiServiceVts_HelpTW {


    /**
    * @description Recupra Implants de los proveedores
    * @author Eduardo Hernández Cuamatzi | 08-25-2020 
    * @param proveeName Nombre del proveedor
    * @return String Registro del proveedor
    **/
    public static String findOwner(String proveeName) {
        String implantId = '';
        final Set<String> setProvee = new Set<String>{proveeName};
        final List<User> lstImpl = MX_RTL_User_Selector.lstImplants(setProvee);
        if(lstImpl.isEmpty() == false) {
            implantId = lstImpl[0].Id;
        }
        return implantId;
    }

    /**
    * @description Busca o genera una cuenta a apartir de un correo
    * @author Eduardo Hernández Cuamatzi | 08-25-2020 
    * @param consulta datos del servicio
    * @param emailCase correo del servicio
    * @param mobilePhone telefono del servicio
    * @return String Registro de la cuenta encontrada o generada
    **/
    public static String findAccount(MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta, String emailCase, String mobilePhone) {
        final String emailClient = emailCase.toLowerCase();
        Account findAccount = new Account();
        findAccount = MX_SB_VTS_utilityQuote.findUniqueAccount(emailClient);
        String accId = '';
        if(String.isEmpty(findAccount.Id)) {
            final List<String> vals = new List<String>{consulta.person.name, emailClient, consulta.person.firstSurname, ''};
            findAccount = MX_SB_VTS_utilityQuote.fillAccount(findAccount, vals);
            findAccount.AccountSource = 'Inbound';
            final Account newAcc = MX_RTL_Account_Selector.upsrtAccount(findAccount);
            accId = newAcc.Id;
        } else {
            accId = findAccount.Id;
        }
        return accId;
    }

    /**
    * @description Crea una nueva Oportunidad
    * @author Eduardo Hernández Cuamatzi | 08-25-2020 
    * @param accTW Cuenta a la que se asocia la Oportunidad
    * @param consulta Datos del servicio
    * @param product Producto al que se generara el TW
    * @param contactDetails Detalles de contacto
    * @return Opportunity Oportunidad nueva generada
    **/
    public static Opportunity fillOpps(Account accTW, MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta, Product2 product, Map<String, String> contactDetails) {
        final Opportunity cotizTW = new Opportunity();
        cotizTW.Name = accTW.Oportunidad_Totales__c + 1  + ' ' + consulta.person.name + ' ' + MX_SB_VTS_leadMultiServiceVts_Helper.validLastName(consulta);
        cotizTW.CloseDate = System.today();
        cotizTW.AccountId = accTW.Id;
        cotizTW.FolioCotizacion__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(consulta.quote.id);
        cotizTW.Reason__c = 'Venta';
        cotizTW.Producto__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(product.Name);
	    cotizTW.TelefonoCliente__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(contactDetails.get('mobile'));
        cotizTW.Pricebook2Id = MX_SB_VTS_utilityQuote.findPriceBook(product.Id);
        cotizTW.StageName = 'Contacto';
        cotizTW.RecordTypeId = MX_SB_VTS_leadMultiServiceVts_Helper.validRecType(product.Name, false);
        cotizTW.LeadSource = 'Tracking web';
        if(String.isNotBlank(consulta.coupon)) {
            cotizTW.MX_WB_EnvioCTICupon__c = rtwCotizacion.validarCupon(consulta.coupon);
            cotizTw.MX_WB_Cupon__c = consulta.coupon;
        }
        cotizTW.MX_SB_VTS_CanalAPP__c = consulta.channel;
        return cotizTW;
    }

    /**
    * @description Valida que se envien email y telefono para generar TW
    * @author Eduardo Hernández Cuamatzi | 08-25-2020 
    * @param consulta Valores de entrada del servicio
    * @param mapContact Mapa de valores de contacto
    * @return Boolean Validación exitosa
    **/
    public static Boolean validOriginTW(MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta, Map<String, String> mapContact) {
        Boolean isValid = false;
        if(String.isNotBlank(mapContact.get('email')) && String.isNotBlank(mapContact.get('mobile'))) {
            isValid = validQuoteId(consulta);
        }
        return isValid;
    }

    /**
    * @description Valida elemento de Cotización en el servicio
    * @author Eduardo Hernández Cuamatzi | 08-25-2020 
    * @param consulta Datos del servicio
    * @return Boolean Si el elemento Cotización es valido
    **/
    public static Boolean validQuoteId(MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta) {
        Boolean isQuoteId = false;
        if(consulta.quote != null && String.isNotEmpty(consulta.quote.id)) {
            isQuoteId = true;
        }
        return isQuoteId;
    }
}