/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase test de MX_BPP_Aclaraciones

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     20/05/2020      Roberto Isaac Soto Granados           Creación Clase
*****************************************************************************************/
@isTest
private class MX_BPP_Aclaraciones_test {
    /*Usuario de pruebas*/
    private static User testUser = new User();
    /*Caso de pruebas*/
    private static Case testCase = new Case();
    /*Aclaración de pruebas*/
    private static BPyP_Aclaraciones__c testAclara = new BPyP_Aclaraciones__c();
    /*String para folios*/
    private static String testCode = '1234';
    /*cuenta de pruebas*/
    private static Account testAccs = new Account();

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testUser = UtilitysDataTest_tst.crearUsuario('testUser', 'BPyP Estandar', 'BPYP BANQUERO BANCA PERISUR');
        testUser.Title = 'Privado';
        insert testUser;

        System.runAs(testUser) {
            final gcal__GBL_Google_Calendar_Sync_Environment__c calEnviro = new gcal__GBL_Google_Calendar_Sync_Environment__c(Name = 'DEV');
            insert calEnviro;
            testAccs.LastName = 'testAcc';
            testAccs.FirstName = 'testAcc';
            testAccs.OwnerId = testUser.Id;
            testAccs.No_de_cliente__c = testCode;
            insert testAccs;            
            testCase.OwnerId = testUser.Id;
            testCase.Status = 'Nuevo';
            testCase.MX_SB_SAC_Folio__c = testCode;
            testCase.Description = 'Description';
            testCase.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_EU_Case_Apoyo_General'].Id;
            insert testCase;
            testAclara.BPyP_Producto__c = 'TDC';
            testAclara.BPyP_Tarjeta__c = testCode;
            testAclara.BPyP_Folio__c = testCode;
            testAclara.BPyP_NumeroCliente__c = testCode;
            testAclara.BPyP_Caso__c = testCase.Id;
            insert testAclara;
        }
    }

    /*Ejecuta la acción para cubrir Methods de la clase*/
    static testMethod void getCasesTest() {
        List<MX_BPP_Aclaraciones_Ctrl.wrapAclaracion> warpTest = new List<MX_BPP_Aclaraciones_Ctrl.wrapAclaracion>();
        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        testCase = [SELECT Id, MX_SB_SAC_Folio__c FROM Case WHERE MX_SB_SAC_Folio__c =: testCode LIMIT 1];
        System.runAs(testUser) {
            Test.startTest();
                warpTest = MX_BPP_Aclaraciones_Ctrl.retrieveCases();
            Test.stopTest();
        }
        System.assertEquals(testCase.MX_SB_SAC_Folio__c, warpTest[0].caseFolio, 'Wrapper correcto');
    }

    /*Ejecuta la acción para cubrir Methods de la clase*/
    static testMethod void createNewVisitTest() {
        String idTest;
        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        testCase = [SELECT Id, MX_SB_SAC_Folio__c FROM Case WHERE MX_SB_SAC_Folio__c =: testCode LIMIT 1];
        System.runAs(testUser) {
            Test.startTest();
            IdTest = MX_BPP_Aclaraciones_Ctrl.createNewVisit(new List<Id>{testCase.Id});
            Test.stopTest();
        }
        System.assert(!String.isBlank(IdTest), 'Wrapper correcto');
    }
}