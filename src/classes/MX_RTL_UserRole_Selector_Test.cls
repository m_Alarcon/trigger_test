/**
* @File Name          : MX_RTL_UserRole_Selector_Test.cls
* @Description        : Test class for MX_RTL_UserRole_Selector
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 01/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      01/06/2020            Gabriel Garcia Rojas          Initial Version
**/
@isTest
private class MX_RTL_UserRole_Selector_Test {
	/** name variables for records */
    final static String NAME = '%BPYP BANQUERO %';
    /** error message */
    final static String MESSAGEFAIL = 'Fail method';

    /**
     * @description constructor test
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void testConstructor() {
        final MX_RTL_UserRole_Selector instanceC = new MX_RTL_UserRole_Selector();
        System.assertNotEquals(instanceC, null, MESSAGEFAIL);
    }

    /**
     * @description
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void getUserRoleLikeNameTest() {
        final Map<Id,UserRole> mapUserRole = MX_RTL_UserRole_Selector.getUserRoleLikeName(NAME);
        System.assertNotEquals(mapUserRole.size(), 0 , MESSAGEFAIL);
    }
}