/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpFormalizarPago
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
 @SuppressWarnings('sf:LongVariable, sf:ShortClassName, sf:ShortVariable')
public class MX_SB_PS_wrpFormalizarPago {
     /*Public property for wrapper*/
	public header header {get;set;}
	 /*Public property for wrapper*/
	public quote quote {get;set;}
	
	/*public constructor subclass*/
		public MX_SB_PS_wrpFormalizarPago() {
		 this.header = new header();
		 this.quote = new quote();
		}		
      /*Public subclass for wrapper*/
	class quote {
	    /*Public property for wrapper*/
		public String idQuote {get;set;}	
		/*Public property for wrapper*/
		public payment payment {get;set;}
		/*public constructor subclass*/
		public quote() {
		 this.idQuote = '';
		 this.payment = new payment();
		}		
	}	

	/*Public subclass for wrapper*/
	class header {
	  /*Public property for wrapper*/	
		public String user {get;set;}
	  /*Public property for wrapper*/	
		public String idSession {get;set;}
      /*Public property for wrapper*/		
		public String idRequest {get;set;}	
	  /*Public property for wrapper*/	
		public String dateConsumerInvocation {get;set;}
       /*Public property for wrapper*/
		public String aapType {get;set;}	
	  /*Public property for wrapper*/	
		public String dateRequest {get;set;}
      /*Public property for wrapper*/		
		public String channel {get;set;}	
      /*Public property for wrapper*/		
		public String subChannel {get;set;}	
	  /*Public property for wrapper*/	
		public String branchOffice {get;set;}
	  /*Public property for wrapper*/	
		public String managementUnit {get;set;}
       /*public constructor subclass*/
		public header() {
		 this.aapType='';
		 this.user = '';
		 this.IdSession = '';
		 this.idRequest = '';
		 this.dateRequest = '';
		 this.Channel = '';
		 this.managementUnit = '';
		 this.dateConsumerInvocation = '';
		  this.SubChannel = '';
		 this.branchOffice = '';
		}					
	}
	
	  /*Public subclass for wrapper*/
	class payment {
	  /*Public property for wrapper*/
		public document document {get;set;}
		/*public constructor subclass*/
		public payment() {
		 this.document = new document();
		}		
	}
	  /*Public subclass for wrapper*/
	class document {
	  /*Public property for wrapper*/
		public type type {get;set;}
	  /*Public property for wrapper*/
		 public string id {get;set;}  
	  /*Public property for wrapper*/	
		public String expirationDate {get;set;}
      /*public constructor subclass*/
		public document() {
		 this.type = new type();
		 this.id = '';
		 this.expirationDate = '';
		}				
	}
	  /*Public subclass for wrapper*/
	class type {
	  /*Public property for wrapper*/
		 public string id {get;set;}  
      /*public constructor subclass*/
		public type() {
		 this.id = '';
		}				
	}

}