/**
* Name: MX_RTL_GroupMember_service
* @author Jose angel Guerrero
* Description : Nuevo service
* Ver                  Date            Author                   Description
* @version 1.0         04/08/2020     Jose angel Guerrero      Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public Without sharing class MX_RTL_GroupMember_service {
     /**
    * @description: funcion constructor privado
    */
    private MX_RTL_GroupMember_service() { }
    /**
    * @description: funcion constructor
    */
    public static List< GroupMember > serviceGroupMember (String idUsuario,String idCola) {
        return MX_RTL_GroupMember_Selector.getMemberByGroupAndID(idUsuario,idCola);
    }
    /**
    * @description: funcion constructor
    */
    public static List< GroupMember > serviceBuscarCola (String colaBuscar) {
        return MX_RTL_GroupMember_Selector.getMemberByGroupId(colaBuscar);
    }
    /**
    * @description: funcion constructor
    */
    public static List< GroupMember > serviceBuscarLasColas (list <String > listaDeI,String prefijo) {
        return MX_RTL_GroupMember_Selector.getMembersByIds(listaDeI,prefijo);
    }
    /**
    * @description: funcion constructor
    */
    public static List< GroupMember > serviceGuardarAsig (list <String > userIds,String origen,String destino) {
        return MX_RTL_GroupMember_Selector.getMemberByNueAsig(userIds,origen,destino);
    }
    /**
    * @description: funcion upsert
    */
    public static void upsertAsignaciones (list < GroupMember > asignaciones) {
        upsert asignaciones;
    }
    /**
    * @description: funcion delete
    */
    public static void deleteAsignaciones (list < GroupMember > asignaciones) {
        delete asignaciones;
    }
}