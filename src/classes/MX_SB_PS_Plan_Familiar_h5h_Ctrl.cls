/**
* @File Name          : MX_SB_PS_Plan_Familiar_h5h_Ctrl.cls
* @Description        :
* @Author             : Juan Carlos Benitez
* @Group              :
* @Last Modified By   : Juan Carlos Benitez
* @Last Modified On   : 7/02/2020, 9:00:03 AM
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    6/24/2020   Juan Carlos Benitez          Initial Version
* 1.1    2/08/2021   Juan Carlos Benitez          Se añade methodo para update benefs
**/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_SB_PS_Plan_Familiar_h5h_Ctrl {
    /**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param oppId
* @return Opportunity
**/
    @AuraEnabled
    public static Opportunity loadOppF(String oppId) {
        return MX_SB_PS_Utilities.fetchOpp(oppId);
    }
    /**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param quotId
* @return Integer count
**/
    @AuraEnabled
    public static Integer getintN(string quotId) {
        return  MX_RTL_Beneficiario_Service.getCountBenef(quotId);
    }
    /**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param Account pAcc
* @return Account obj
**/
    @AuraEnabled
    public static List<Account> upsrtAcc(Account pAcc) {
        return MX_RTL_Account_Service.upsrtAcc(pAcc);
    }
    /**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param MX_SB_VTS_Beneficiario__c benefObj
* @return void
**/
	@AuraEnabled
    public static void insrtBenef(MX_SB_VTS_Beneficiario__c benefObj) {
        MX_RTL_Beneficiario_Service.insertBenef(benefObj);
    }
/**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param String quotId
* @return Lit<MX_SB_VTS_Beneficiario__c>
**/
    @AuraEnabled
    public static list<MX_SB_VTS_Beneficiario__c> srchBenefs(String quotId) {
        return MX_RTL_Beneficiario_Service.getbenefbyValue(quotId);
    }
/**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param String Id
* @return Lit<MX_SB_VTS_Beneficiario__c>
**/    
    @AuraEnabled
    public static list<MX_SB_VTS_Beneficiario__c> getbenefbyId(String value) {
        return MX_RTL_Beneficiario_Service.getbenefbyIdValue(value);
    }
    /**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param String Id
* @return Lit<MX_SB_VTS_Beneficiario__c>
**/    
    @AuraEnabled
    public static void updateBenefData(List<MX_SB_VTS_Beneficiario__c> benefData) {
        MX_RTL_Beneficiario_Service.updateBenefData(benefData);
    }
}