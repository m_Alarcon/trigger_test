/**
 * @description       : Class test MX_SB_SAC_1500_ExecutiveAuth.cls
 * @author            : Gerardo Mendoza Aguilar
 * @group             : 
 * @last modified on  : 02-17-2021
 * @last modified by  : Gerardo Mendoza Aguilar
 * Modifications Log 
 * Ver   Date         Author                    Modification
 * 1.0   02-16-2021   Gerardo Mendoza Aguilar   Initial Version
**/
@isTest
public class MX_SB_SAC_1500_ExecutiveAuth_Test {

    /**
    * @description 
    * @author Gerardo Mendoza Aguilar | 02-16-2021 
    **/
    @TestSetup
    static void makeData() {
        Final String rType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MX_SB_SAC_RT').getRecordTypeId();
        Final Case caseData =  new Case(
            RecordTypeId =rType
            , MX_SB_SAC_Nombre__c = 'Clientes Test'
            , Origin = 'Teléfono'
            , MX_SB_SAC_TipoContacto__c = 'Asegurado'
            , Reason = 'Cancelación'
            , MX_SB_SAC_Aplicar_Mala_Venta__c = 'No'
            , MX_SB_SAC_Estado__c = 'Ciudad de México'
        );
        insert caseData;
    }

    /**
    * @description
    * @author Gerardo Mendoza Aguilar | 02-16-2021 
    **/
    @isTest
    public static void updateCaseExecutive() {
        Test.startTest();
        Final List<Case> caseList = [Select Id from Case limit 1];
        MX_SB_SAC_1500_ExecutiveAuth.updateCaseExecutive(caseList[0].Id, true);
        System.assert(!caseList.isEmpty(), 'Caso creado');
        Test.stopTest();
    }
    
    /**
    * @description
    * @author Alan Santacruz | 02-25-2021 
    **/
    @isTest
    public static void updateCaseComments() {
        Test.startTest();
        Final List<Case> caseList = [Select Id from Case limit 1];
        Final Case caseData =  new Case(
            Id = caseList[0].Id
            , MX_SB_SAC_Nombre__c = 'Clientes Test'
            , Origin = 'Teléfono'
            , MX_SB_SAC_TipoContacto__c = 'Asegurado'
            , Reason = 'Cancelación'
            , MX_SB_SAC_Aplicar_Mala_Venta__c = 'No'
            , MX_SB_SAC_Estado__c = 'Ciudad de México'
        );
        caseList.add(caseData);
        MX_SB_SAC_1500_ExecutiveAuth.updateCaseComments(caseData);
        System.assert(!caseList.isEmpty(), 'Caso creado');
        Test.stopTest();
    }
}