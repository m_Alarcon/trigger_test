/**
 * @File Name          : MX_RTL_CampaignMembers_Test.cls
 * @Description        : Clase para Test de MX_RTL_CampaignMembers
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 26/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      26/06/2020           Jair Ignacio Gonzalez G.   Initial Version
**/
@isTest
private class MX_RTL_CampaignMembers_Test {
    /**
     **Descripción: Clase makeData
    **/
    @TestSetup static void makeData() {
        final Account oAcc = new Account(Name='Test TriggerHandler Account',No_de_cliente__c = '8897791');
        insert oAcc;
        final Campaign oCamp = new Campaign(Name = 'Campaign TriggerHandler Test', CampaignMemberRecordTypeId=Schema.SObjectType.CampaignMember.getRecordTypeInfosByDeveloperName().get('MX_BPP_CampaignMember').getRecordTypeId());
        insert oCamp;
        final CampaignMemberStatus oMemberStatus = new CampaignMemberStatus(CampaignId=oCamp.Id, Label = 'Con Exito');
        insert oMemberStatus;
        final Lead oLead = new Lead(LastName='Lead TriggerHandler Test', RecordTypeId=Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('MX_BPP_Leads').getRecordTypeId(),
                MX_ParticipantLoad_id__c=oAcc.No_de_cliente__c, MX_LeadEndDate__c=Date.today().addDays(16), MX_WB_RCuenta__c= oAcc.Id, MX_LeadAmount__c=350, LeadSource='Preaprobados');
        insert oLead;
        final CampaignMember oCampMem = new CampaignMember(CampaignId = oCamp.Id, Status='Sent', LeadId = oLead.Id, MX_LeadEndDate__c=Date.today().addDays(16));
        insert oCampMem;
        MX_BPP_ConvertLeads_Service.convertToLead(new List<Id>{oLead.Id});
    }

    /**
     * *Descripción: Clase de prueba para Update
    **/
    @isTest static void testUpdate() {
        final CampaignMember oCampMbr = [SELECT Id, Status FROM CampaignMember LIMIT 1];
        oCampMbr.Status = 'Con Exito';
        oCampMbr.MX_SuccesAmount__c = 100;
        update oCampMbr;
        System.assertEquals(100, [SELECT Id, Amount FROM Opportunity LIMIT 1].Amount, 'Cant updtate the CampaignMember');
    }
}