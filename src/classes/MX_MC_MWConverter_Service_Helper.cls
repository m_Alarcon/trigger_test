/**
 * @File Name           : MC_MessageWrapperAdapter_MX.cls
 * @Description         :
 * @Author              : eduardo.barrera3@bbva.com
 * @Group               :
 * @Last Modified By    : eduardo.barrera3@bbva.com
 * @Last Modified On    : 04/14/2020, 09:33:021 AM
 * @Modification Log    :
 * =============================================================================
 * Ver          Date                     Author                     Modification
 * =============================================================================
 * 1.0      04/07/2020 12:40:01 PM   eduardo.barrera3@bbva.com      Initial Version.
 **/
public without sharing class MX_MC_MWConverter_Service_Helper {

    /**
     * ----------------------------------------------------------------------
     * @Name    BaseWrapper
     * @Author  eduardo.barrera3@bbva.com
     * @Date    Created: 04/14/2020
     * @Group
     * @Description Descriptions
     * @Changes
     *      | 2020-14-04    eduardo.barrera3@bbva.com   Initial Version
     **/

    public virtual class BaseWrapper {

        /**
         * @description
         * @author eduardo.barrera3@bbva.com
         * @return String
         **/
        public String toJson() {
            return JSON.serialize(this, true);
        }
    }

    /**
     * ----------------------------------------------------------------------
     * @Name    MessageThread
     * @Author  eduardo.barrera3@bbva.com
     * @Date    Created: 04/07/2020
     * @Group
     * @Description Descriptions
     * @Changes
     *      | 2020-07-04    eduardo.barrera3@bbva.com   Initial Version
     **/
    public class MessageThread extends BaseWrapper {

        /**
         * @Description message property
         **/
        public String message {get; set;}

        /**
         * @Description isDraft property
         **/
        public Boolean isDraft {get; set;}

        /**
         * @Description attachments property
         **/
        public List<MX_MC_MTWConverter_Service_Helper.Attachment> attachments {get; set;}
    }
}