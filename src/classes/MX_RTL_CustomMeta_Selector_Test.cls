/**
* @description       : Clase de prueba para clase de MX_RTL_CustomMeta_Selector
* @author            : Diego Olvera
* @group             : BBVA
* @last modified on  : 02-19-2021
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   11-20-2020   Diego Olvera   Initial Version
* 1.1   02-18-2020   Diego Olvera   Se agrega función para cubrir cobertura
**/
@isTest
public without sharing class MX_RTL_CustomMeta_Selector_Test {
    /**@description Nombre usuario*/
    private final static String ASESORSELE = 'AsesorTest';
    /**@description Nombre del estado*/
    private final static String ESTADOSELE = 'Jalisco';
    /*Variable que almacena campos a consultar*/
    final static String FIELDS = 'Id, MX_SB_VTS_States__c, MX_SB_VTS_Abreviacion__c';
    /** Variable del nombre de la metadata */
    static final String METNAME = 'MX_SB_VTS_StatesMex__mdt';
    
    @TestSetup
    static void makeData() {
        final User userRecSele = MX_WB_TestData_cls.crearUsuario(ASESORSELE, 'System Administrator');
        insert userRecSele;
    }
    @isTest
    static void obtStatesValue() {
        final User userObtSele = [Select Id from User where LastName =: ASESORSELE];
        System.runAs(userObtSele) {
            Test.startTest();
            try {
                MX_RTL_CustomMeta_Selector.getDirections(ESTADOSELE);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Error');
                System.assertEquals('Error', extError.getMessage(),'no recuperó los valores selector');
            }
            Test.stopTest();
        }
    }
    @isTest
    static void obtDirecValues() {
        final User userObtSele = [Select Id from User where LastName =: ASESORSELE];
        System.runAs(userObtSele) {
            Test.startTest();
            try {
                MX_RTL_CustomMeta_Selector.getStates(FIELDS, METNAME);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Mistake');
                System.assertEquals('Fatal', extError.getMessage(),'no recuperó datos');
            }
            Test.stopTest();
        }
    }
}