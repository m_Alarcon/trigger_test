/**
 * @description       : 
 * @author            : Eduardo Hernandez Cuamatzi
 * @group             : 
 * @last modified on  : 07-27-2020
 * @last modified by  : Eduardo Hernandez Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   07-27-2020   Eduardo Hernandez Cuamatzi   Initial Version
**/
@isTest
private class MX_RTL_Catalogo_Clipert_Product_Test {

    @TestSetup
    static void makeData() {
        final MX_WB_FamiliaProducto__c objFamProd3  = MX_WB_TestData_cls.createProductsFamily(System.Label.MX_SB_VTS_SeguroEstudia_LBL);
        insert objFamProd3;
        
        final Product2 prod3  = MX_WB_TestData_cls.productNew(System.Label.MX_SB_VTS_SeguroEstudia_LBL);
        prod3.MX_WB_FamiliaProductos__c = objFamProd3.Id;
        insert prod3;
    }

    @isTest
    private static void insertClip() {
        final Set<String> lstProdNames = new Set<String>{System.Label.MX_SB_VTS_SeguroEstudia_LBL};
        final Set<String> lstProcess = new Set<String>{'VTS'};
        final List<Product2> lstProducts = MX_RTL_Product2_Selector.findProsByNameProces(lstProdNames, lstProcess, true);
        final MX_SB_SAC_Catalogo_Clipert_Producto__c prodClip = new MX_SB_SAC_Catalogo_Clipert_Producto__c();
        prodClip.MX_SB_SAC_Activo__c = true;
        prodClip.MX_SB_SAC_Producto__c = lstProducts[0].Id;
        prodClip.MX_SB_VTS_ProductCode__c = '1203';
        prodClip.Name = System.Label.MX_SB_VTS_SeguroEstudia_LBL;
        final List<MX_SB_SAC_Catalogo_Clipert_Producto__c> listClip = new List<MX_SB_SAC_Catalogo_Clipert_Producto__c>{prodClip};
        Test.startTest();
            final List<Database.SaveResult> lstResults = MX_RTL_Catalogo_Clipert_Product_Selector.newProductClipp(listClip, true);
        Test.stopTest();
        System.assert(lstResults[0].isSuccess(),'Producto clippert insertado');
    }

    @isTest
    private static void findProductClip() {
        final Set<String> lstProcess = new Set<String>{'VTS'};
        final Set<String> lstProdNames = new Set<String>{System.Label.MX_SB_VTS_SeguroEstudia_LBL};
        final List<Product2> lstProducts = MX_RTL_Product2_Selector.findProsByNameProces(lstProdNames, lstProcess, true);
        final MX_SB_SAC_Catalogo_Clipert_Producto__c prodClip2 = new MX_SB_SAC_Catalogo_Clipert_Producto__c();
        prodClip2.MX_SB_SAC_Activo__c = true;
        prodClip2.MX_SB_SAC_Producto__c = lstProducts[0].Id;
        prodClip2.MX_SB_VTS_ProductCode__c = '1203';
        prodClip2.Name = System.Label.MX_SB_VTS_SeguroEstudia_LBL;
        final List<MX_SB_SAC_Catalogo_Clipert_Producto__c> listClip2 = new List<MX_SB_SAC_Catalogo_Clipert_Producto__c>{prodClip2};
        Test.startTest();
            MX_RTL_Catalogo_Clipert_Product_Selector.newProductClipp(listClip2, true);
            final Set<String> lstProductCode = new Set<String>{'1203'};
            final List<MX_SB_SAC_Catalogo_Clipert_Producto__c> catalogoClipCod = MX_RTL_Catalogo_Clipert_Product_Selector.catalogoClipCod(lstProductCode, lstProcess, true);
        Test.stopTest();
        System.assertEquals(catalogoClipCod[0].Name, System.Label.MX_SB_VTS_SeguroEstudia_LBL, 'Producto clippert recuperado');
    }
}