/**
 * @File Name          : MX_SB_VTS_GetSetInsContract_Service.cls
 * @Description        : Clase Encargada de Proporcionar la Consulta de Polizas
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 08-13-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0       30/10/2020      Alexandro Corzo        Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_GetSetInsContract_Helper {
    /** Variablr de Apoyo: sFldFDateN */
    Static String sFldFDateN = 'ddmmyyyy';
    /** Variable de Apoyo: sFldFDateI */
    Static String sFldFDateI = 'yyyymmdd';
    /** Variable de Apoyo: isExistFld */
    Static String isExistFld = 'isExist';
    /** Variable de Apoyo: sFldPolNumber */
    Static String sFldPolNumber = 'policyNumber';
    /** Variable de Apoyo:  sFldIsContVal */
    Static String sFldIsContVal = 'isContractValid';
    /** Variable de Apoyo: sFldMessage */
    Static String sFldMessage = 'message';
    /** Variable de Apoyo:  NOCONTENT */
    private static final Integer NOCONTENT = 204;
    /** Variable de Apoyo:  SRVNOTAVAL */
    private static final Integer SRVNOTAVAL = 503;
    /** Variable de Apoyo: sFldParamVal */
    Static String sFldParamVal = 'isParamsValid';

    /**
     * @description: Calcula la diferencia de dias fecha actual y fecha final de la poliza para
     * 				 determinar si es valida.
     * @author: Eduardo Hernández Cuamatzi
     * @return: 	 Boolean isValid
     */
    public static Boolean obtDiffDiasFechas(Datetime dFechaIni, Datetime dFechaFin) {
        Boolean isValid = false;
        Integer iDias = 0;
        Double dRestFechas = null;
        Double dDivFechas = null;
        dRestFechas = dFechaFin.getTime() - dFechaIni.getTime();
        dDivFechas = dRestFechas / 86400000;
        iDias = Integer.valueOf(dDivFechas);
        isValid = iDias > 0 ? true : false;
        return isValid;
    }

    /**
     * @description: Convierte formatos de fecha tipo dd/mm/yyyy y yyyy/mm/dd y viceversa.
     * @author: Alexandro Corzo
     * @return: String sReturnValue
     */
    public static String obtFormatDate(List<String> oParams) {   
        String sReturnValue = null;
        final String sFormatDate = oParams[1];
        switch on sFormatDate {
            when 'yyyymmdd' {
                if(oParams[2].equalsIgnoreCase(sFldFDateN)) {
                    final String sDateConvert = oParams[0];
                    final String sSepVal = oParams[3];
                    final String sSepReturn = oParams[4];
                    final String spelvStr = splitStr(sDateConvert, sSepVal);
                	final String[] sDateArray = sDateConvert.split(spelvStr);
                	sReturnValue = sDateArray[2] + sSepReturn + sDateArray[1] + sSepReturn + sDateArray[0];
                }
            }
            when 'ddmmyyyy' {
                if(oParams[2].equalsIgnoreCase(sFldFDateI)) {
                    final String sDateConvert = oParams[0];
                    final String sSepVal = oParams[3];
                    final String sSepReturn = oParams[4];
                    final String spelvStr = splitStr(sDateConvert, sSepVal);
                    final String[] sDateArray = sDateConvert.split(spelvStr);
                    sReturnValue = sDateArray[2] + sSepReturn + sDateArray[1] + sSepReturn + sDateArray[0];
                }
            }
        }
        return sReturnValue;
    }

    /**
     * @description: Se encarga de llevar los valores para el objeto mapa: oReturnValues
     * @author: Alexandro Corzo
     * @return: Map<String,List<Object>> oReturnValues
     */
    public static Map<String, Object> fillDataMapObj(String sPolicyNumber, Integer iStatusCode) {
        final Map<String, Object> oReturnValues = new Map<String, Object>();
        oReturnValues.put(isExistFld, false);
        oReturnValues.put(sFldPolNumber, sPolicyNumber);
        oReturnValues.put(sFldIsContVal, false);
        if(iStatusCode == NOCONTENT) { 
            oReturnValues.put(sFldMessage, 'La poliza no existe en los registros!');
            oReturnValues.put('isAvailableSrv', true);
            oReturnValues.put(sFldParamVal, true);
        }
        if(iStatusCode == SRVNOTAVAL) {
            oReturnValues.put(sFldMessage, 'El servicio no se encuentra disponible, intente nuevamente!!');
            oReturnValues.put('isAvailableSrv', false);
            oReturnValues.put(sFldParamVal, true);
        }
        return oReturnValues;
    }

    /**
    * @description Evaluate split item
    * @author Eduardo Hernández Cuamatzi | 02-08-2021 
    * @param dateStr fecha en formato string
    * @return String elemento separador
    **/
    public static String splitStr(String dateStr, String defaultElement) {
        String evaluateStr = '/';
        if(String.isEmpty(dateStr) == false && dateStr.contains('-')) {
            evaluateStr = '-';
        }
        return evaluateStr;
    }
}