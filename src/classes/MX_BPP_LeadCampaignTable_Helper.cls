/**
* @File Name          : MX_BPP_LeadCampaignTable_Helper.cls
* @Description        :
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 01/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      01/06/2020            Gabriel Garcia Rojas          Initial Version
**/
public class MX_BPP_LeadCampaignTable_Helper {

    /** Constructor*/
    @testVisible
    private MX_BPP_LeadCampaignTable_Helper() { }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param staffRole
    * @param bankerRole
    * @param role
    * @param managerUser
    * @return List<User>
    **/
    public static List<User> getListUderByUserAndRole(Set<Id> staffRole, Set<Id> bankerRole, Id role, List<User> managerUser) {
        List<User> selectedRole = new List<User>();

        if(staffRole.contains(role)) {
            final List<User> bankerUser = MX_RTL_User_Selector.getUserByDivisionSucursal(new Set<String>{managerUser[0].DivisionFormula__c}, new Set<String>{managerUser[0].BPyP_ls_NombreSucursal__c});
            if(!bankerUser.isEmpty()) {
                selectedRole = bankerUser;
            }
        } else if(bankerRole.contains(role)) {
            selectedRole = managerUser;
        }
        return selectedRole;
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param paramList
    * @param filterList
    * @return String
    **/
    public static String setFilterToCampaignMember(List<String> paramList, List<String> filterList) {
        String filter = '';
    	for(Integer i = 0; i < paramList.size(); i++) {
            if(paramList[i] != '' && paramList[i] != null) {
                if(filter.length() > 0) {
                    filter += ' AND ' + filterList[i];
                } else {
                    filter += ' WHERE ' + filterList[i];
                }
            }
        }
        return filter;
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param picklistObject
    * @return Map<Object,List<String>>
    **/
    public static Map<Object,List<String>> dependentValuesGet(String picklistObject) {
        Map<Object, List<String>> dependentValues;
        final List<String> splitString = picklistObject.split('\\.');
        final Schema.SobjectField dependToken = Schema.getGlobalDescribe().get(splitString[0]).getDescribe().fields.getMap().get(splitString[1]);
        final Schema.DescribeFieldResult depend = dependToken.getDescribe();
        final Schema.sObjectField controlToken = depend.getController();

        if ( controlToken != null ) {
            dependentValues = new Map<Object, List<String>>();
            final Schema.DescribeFieldResult control = controlToken.getDescribe();
            final List<Schema.PicklistEntry> controlEntries = (control.getType() == Schema.DisplayType.Boolean ? null : control.getPicklistValues());

            for(Schema.PicklistEntry entry : depend.getPicklistValues() ) {
                if (entry.isActive()) {
                    final List<String> base64chars = String.valueOf(((Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor')).split('');
                    dependentValues = dependentValuesGet(controlEntries, base64chars, dependentValues, entry);
                }
            }
        }
        return dependentValues;
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param picklistObject
    * @return Map<Object,List<String>>
    **/
    public static Map<Object,List<String>> dependentValuesGet(List<Schema.PicklistEntry> controlEntries, List<String> base64chars, Map<Object, List<String>> dependentValues, Schema.PicklistEntry entry) {
    	final String base64map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
        for(Integer i = 0; i < (controlEntries == null ? 2 : controlEntries.size()); i++ ) {
            final Object controlValue;
            if(controlEntries == null) {
                controlValue = (Object) (i == 1);
            } else if (controlEntries[ i ].isActive()) {
                controlValue = (Object) controlEntries[ i ].getLabel();
            }
            final Integer bitIndex = i / 6, bitShift = 5 - Math.mod(i, 6);

            if (controlValue == null || (base64map.indexOf(base64chars[ bitIndex ]) & (1 << bitShift)) == 0) {
                continue;
            }
            if (!dependentValues.containsKey(controlValue)) {
                dependentValues.put( controlValue, new List<String>());
            }
            dependentValues.get(controlValue).add(entry.getLabel());
        }
        return dependentValues;
    }
}