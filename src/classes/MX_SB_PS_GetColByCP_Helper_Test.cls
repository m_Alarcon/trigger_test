/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_PS_GetColByCP_Helper_Test
* Autor Daniel Perez Lopez
* Proyecto: Salesforce Presuscritos
* Descripción : Prueba los Methods de la clase MX_SB_PS_GetColByCP_Helper

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           26/10/2020      Daniel Lopez                         Creación
* 1.1           04/11/2020      Daniel Lopez                        Modificación
* --------------------------------------------------------------------------------
*/
@IsTest
public class MX_SB_PS_GetColByCP_Helper_Test {
        /*User name test*/
    Final static String USEROTP = 'AdminPruebatst';
    /*Account name test*/
    Final static String ACCOTP = 'I AM A ACCOUNTNAME';
    /*Opportunity name test*/
    Final static String OPPOTP = 'I AM A OPPORTUNITYNAME';
    /*URL name test*/
    Final static String URLOTP = 'http://www.ulrexample.com';
    /*Phone name test*/
    Final static String PHONETST = '5544428233';
    /*Fields test*/
    Final static String FIELDS = 'Id, TelefonoCliente__c';
    
    /* 
    @Method: setupTest
    @Description: create test data set
    */
    @TestSetup
    static void testSetup() {
        
			insert new iaso__GBL_Rest_Services_Url__c(Name = 'generarOTP', iaso__Url__c = URLOTP, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'getGTServicesSF', iaso__Url__c = URLOTP, iaso__Cache_Partition__c = 'local.MXSBVTSCache');

            insert new iaso__GBL_Rest_Services_Url__c(Name = 'GTServicesSF',iaso__Url__c='https://150.250.220.36:18500/lifeInsurances',iaso__Cache_Partition__c = 'iaso.ServicesPartition');
        
    }
    
    /* 
    @Method: getStatusOTPTest
    @Description: test method to get the status code from web service
    */
    static testMethod void getTestService() {
            Final Map<String, String> headersMock = new Map<String, String>();
            headersMock.put('tsec', '12345678');
            Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, 'Complete', '{}', headersMock);
            iaso.GBL_Mock.setMock(mockCallout);
        	final MX_SB_PS_GetColByCP_Helper cls = new MX_SB_PS_GetColByCP_Helper();
        	HttpResponse htpres = new HttpResponse();
            test.startTest();
        		Test.setMock(HttpCalloutMock.class, new MX_SB_PS_IASOMock());
        		final HttpRequest thrq = new HttpRequest();
        		cls.modifyRequest(thrq);
        		htpres= MX_SB_PS_IASO_Service_cls.getServiceResponse('GTServicesSF','{"producto":"004"}');
            	System.assertEquals(200,htpres.getStatusCode(),'Exito en validacion');
        	test.stopTest();        
    }
}