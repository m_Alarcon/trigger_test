/**
 * @File Name          : MX_RTL_Contact_Service.cls
 * @Description        :
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Daniel Perez Lopez
 * @Last Modified On   : 12-11-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/26/2020   Juan Carlos Benitez     Initial Version
 * 1.1    2/07/2021   Juan Carlos Benitez     Se añade campo codigo lugar
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_RTL_Contact_Service {
	/** VAR IDE id */    
	Final static String ACCID='AccountId';
	/**
     * 
    * @description
    * @author Juan Carlos Benitez | 5/26/2020
    * @param Ids
    * @return List<Account>
    **/
    public static List<Contact> getContactData(Set<Id> ids) {
        Final Map<String,String> mapa = new Map<String,String>();
        mapa.put('fields', ' Id, Account.Genero__c,FirstName,MX_RTE_Codigo_Lugar__c,Apellido_materno__c,LastName, Birthdate , Email, MX_RTL_CURP__c, MailingStreet, MailingPostalCode,MailingCity,MailingState,MX_WB_ph_Telefono1__c,MX_WB_ph_Telefono2__c ');
        return MX_RTL_Contact_Selector.getContactReusable(ids, ACCID, mapa);
    }
    /**
     * 
    * @description
    * @author Juan Carlos Benitez | 6/26/2020
    * @param Contact cntc
    **/
    public static void upsrtCntc(Contact cntc) {
        MX_RTL_Contact_Selector.upsrtContact(cntc);
    }
}