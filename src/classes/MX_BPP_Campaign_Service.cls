/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Campaign_Service
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-08-10
* @Description 	Service Layer for Campaign
* @Changes
*  
*/
public with sharing class MX_BPP_Campaign_Service {
    
    /** String RecordTypeDeveloperName Campaign */
    static final String RTCAMP = 'BPyP_Campaign';
    
    /*Contructor clase MX_BPP_Campaign_Service */
    private MX_BPP_Campaign_Service() {}
    
    /**
    * @Description 	Create campaign member status for each new campaign
    * @Param		newCampaignList <Trigger.new>
    * @Return 		NA
    **/
    public static void createCampaignMemberStatus(List<Campaign> newCampaignList) {
        final Id recordTypeBPPCamp = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get(RTCAMP).getRecordTypeId();
        final Set<Id> campaignsId = new set<Id>();
        final List<String> strStatusList = new List<String>();
        
        for (Campaign newCampaign : newCampaignList) {
            if (newCampaign.RecordTypeId.equals(recordTypeBPPCamp)) {
                campaignsId.add(newCampaign.Id);
            }
        }
        
        strStatusList.add('Con Exito');
        strStatusList.add('Vencida');
        
        if(!campaignsId.isEmpty()) {
            MX_BPP_Campaign_Service_Helper.createRelatedCMemberStatus(campaignsId, strStatusList);
        }
    }

}