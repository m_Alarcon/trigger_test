/*
    Proyecto : ENGMX
    Versión     Fecha           Autor                   Descripción
    ____________________________________________________________________________________________
    1.0         28-Ago-2018     Cristian Espinosa       Creación de la clase de pruebas para
                                                        el batch BPyP_BatchAvisoCumple_cls.
    1.1         19-Sep-2018     Cristian Espinosa       Adecuación del batch para que no se use el
                                                        campo BPYP_ca_Reset_Casilla__c
    1.2         10-Oct-2018     Ricardo Almanza A       Se limita Batch a Clientes
    1.3         02.Ene-2020     Jair Ignacio Gonzalez   Se cambio el campo MX_BPyP_Proximo_Cumpleanios__c por
                                                        PersonContact.MX_NextBirthday__c por la reingenieria de person account
*/
@isTest
public class BPyP_BatchAvisoCumple_tst {

    /* STRBPYPCLT RecordType DeveloperName */
    final static String STRBPYPCLT = 'MX_BPP_PersonAcc_Client';

    @TestSetup
    static void makeData() {
        final User thisUser = [SELECT Id, Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];

        final User usr = UtilitysDataTest_tst.crearUsuario('Marty McFly', thisUser.Profile.Name, 'BBVA ADMINISTRADOR');

        System.runAs(usr) {

            final String recordTypeId  = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_BPP_PersonAcc_Client'].Id;
            final Account acc4 = new Account(
                RecordTypeId = recordTypeId,
                FirstName='testName',
                LastName='testLastName',
                PersonMailingStreet='test address',
                PersonMailingPostalCode='12345',
                PersonMailingCity='SFO',
                PersonEmail='test@test.com',
                PersonHomePhone='5512345678',
                PersonMobilePhone='5512345678',
                No_de_Cliente__c = '81231238',
                PersonBirthdate = Date.today().addDays(2),
                OwnerId = usr.Id
            );
            insert acc4;
            System.debug('cuenta ***** ' + acc4);
        }
    }

    @isTest
    static void testMethod001() {
        Test.startTest();
        final BPyP_BatchAvisoCumple_cls bat = new BPyP_BatchAvisoCumple_cls();
        Database.executeBatch(bat);
        Test.stopTest();


        final List<Account> accs = [SELECT Id, Name,Owner.Name,PersonContact.MX_NextBirthday__c FROM Account WHERE (Recordtype.DeveloperName  = :STRBPYPCLT) LIMIT 5];

        String strBody = '<p><b>Buen día '+accs[0].Owner.Name+',</b></p>';
        strBody += '<p>En esta semana, los cumpleaños de tus clientes son :</p><table>';
        for(Account acc : accs) {
            strBody += '<tr><td><b>'+acc.Name+'</b></td><td>  </td>';
            strBody += '<td>'+acc.PersonContact.MX_NextBirthday__c.format()+'</td></tr>';
        }
        strBody += '</table><br/><b>¡Recuerda felicitarlos!</b>';
        strBody += '<br/><br/>Atentamente :<br/>';
        strBody += '<p><b>DWP Banca Patrimonial y Privada</b></p>';

        System.assertEquals(strBody,bat.getBody(accs),'Mail generado con éxito');
    }

}