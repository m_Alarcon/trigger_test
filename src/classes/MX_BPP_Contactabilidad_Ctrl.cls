/**
* @File Name          : MX_BPP_Contactabilidad_Ctrl.cls
* @Description        :
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 25/09/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      25/09/2020        Gabriel García Rojas              Initial Version
**/
@SuppressWarnings()
public with sharing class MX_BPP_Contactabilidad_Ctrl {
	 /**Constructor */
    @testVisible
    private MX_BPP_Contactabilidad_Ctrl() { }

    /**
    * @description Obtiene estructura de jerarquía referente al usuario
    * @author Gabriel Garcia Rojas
    * @return
    **/
    @AuraEnabled
    public static MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper fetchInfoByUser() {
        return MX_BPP_LeadStart_Service.userInfo();
    }

    /**
    * @description Obtiene información del usuario
    * @author Gabriel Garcia Rojas
    * @return
    **/
    @AuraEnabled
    public static List<String> fetchUserInfo() {
        return MX_BPP_LeadStart_Service.fetchusdata();
    }

    /**
    * @description retorna la información para la estructura Chart del componente de Contactabilidad
    * @author Gabriel Garcia Rojas
    * @param String tipoConsulta
    * @param String titulo
    * @param List<String> params
    * @param DateTime startDte
    * @param DateTime endDte
    * @return WRP_ChartStacked
    **/
    @AuraEnabled
    public static MX_BPP_Contactabilidad_Service.WRP_ChartStacked fetchData(String tipoConsulta, String titulo, List<String> params, DateTime startDte, DateTime endDte) {
        return MX_BPP_Contactabilidad_Service.fetchDataService(tipoConsulta, titulo, params, startDte, endDte);
    }

     /**
    * @description retorna la lista de cuentas respecto a los parametros de entrada
    * @author Gabriel Garcia Rojas
    * @param List<String> params
    * @param String contactType
    * @param DateTime startDte
    * @param DateTime endDte
    * @param String tittle
    * @param String limite
    * @param String tipoQuery
    * @return List<Account>
    **/
    @AuraEnabled
    public static List<Account> contactabilityAccount(List<String> params, DateTime startDte, DateTime endDte) {
        return MX_BPP_Contactabilidad_Service.servContactaAcc(params, startDte, endDte);
    }

    /**
    * @description return Total of record by Division, Oficina or User
    * @author Gabriel Garcia Rojas
    * @param Integer numRecords
    * @return Map<String, Integer>
    **/
    @AuraEnabled
    public static Integer fetchPgs(Integer numRecords) {
        return MX_BPP_LeadStart_Service.fetchPgs(numRecords);
    }

    /**
    * @description retorna una lista de usuarios con respecto al titulo y la oficina
    * @author Gabriel Garcia Rojas
    * @param String oficina
    * @param String titulo
    * @return List<User>
    **/
    @AuraEnabled
    public static List<User> fetchUserByOfficeServ(String oficina, String titulo) {
    	return MX_BPP_Contactabilidad_Service.fetchUserByOffice(oficina, titulo);
    }

    /*
    *@Descripción   Mthod que extrae datos del owner apartir de su RI.
    *@author Gabriel Garcia Rojas
    *@Date          10/11/2020
    *@Param         idRI (RI del Banquero)
    *@return        String Title bkm
    *@example       BPyP_Contact_Acc.gtRISelect('06m1B000001GaPeQAK');
    */
    @SuppressWarnings('sf:DUDataflowAnomalyAnalysis')
	@AuraEnabled
    public static String gtRISelect(String idRI) {
       String result='';
       final User usuarioRI = BPyP_OppRep.getRISelect(idRI);
       try {
           if(usuarioRI != null) {
               result = usuarioRI.Title;
            }
       } catch (Exception e) {
           throw new AuraHandledException(System.Label.MX_BPP_PyME_Error_Generico+ ' ' + e);
       }
       return result;
    }

}