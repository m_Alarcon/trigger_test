/**
* @File Name          : MX_SB_VTS_GetAmountService_Test.cls
* @Description        :
* @Author             : Diego Olvera
* @Group              :
* @Last Modified By   : Diego Olvera
* @Last Modified On   : 8/5/2020 12:43:02
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    7/5/2020   Diego Olvera     Initial Version
**/
@isTest
public class MX_SB_VTS_GetAmountServiceCrtl_Test {
    /** Name User */
    final static String USERNAME = 'test2';
    /** Name Account */
    final static String ACCOUNTNAME = 'account2';
    /** Name Opp */
    final static String OPPORTUNITYNAME = 'opp2';
      /* @Method: makeData
* @Description: create test data set
*/
     @TestSetup
    public static void makeData() {
        final User testUser = MX_WB_TestData_cls.crearUsuario(USERNAME, System.Label.MX_SB_VTS_ProfileAdmin);
        Insert testUser;
        final Account accnt = MX_WB_TestData_cls.crearCuenta(ACCOUNTNAME, System.Label.MX_SB_VTS_PersonRecord);
        accnt.PersonEmail = 'pruebas@test.com';
        insert accnt;
        final Opportunity opp = MX_WB_TestData_cls.crearOportunidad(OPPORTUNITYNAME, accnt.Id, testUser.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
        opp.LeadSource = 'Facebook';
        opp.Producto__c = 'Vida';
        opp.Reason__c = 'Venta';
        opp.StageName = 'Cotizacion';
        insert opp;
    }
      /* @Method: testGetAmountOpps
* @Description: return json values of fake response
*/
    @isTest
    private static void testGetAmountOpps() {
        final User  cUser=[SELECT ID,Name FROM User  WHERE NAME =: USERNAME LIMIT 1];
        System.runAs(cUser) {
            final object test = MX_SB_VTS_GetAmountServiceCtrl.amountVal();
            System.assertNotEquals(test, null,'El objeto no esta vacio');
        }
    }

}