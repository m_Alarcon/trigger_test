/**
 * @File Name          : MX_SB_SAC_Business_TEST.cls
 * @Description        :
 * @Author             : Saul
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 16/8/2019 11:57:15
 * @Modification Log   :
 *============================================================================================
 * Ver         Date                     Author      		                    Modification
 *============================================================================================
 * 1.0    16/8/2019 11:49:58            Saul                                    Initial Version
 * 2.0    16/8/2019 11:49:58      Ing. Karen Belem Sanchez Ruiz (KB)            Modified
 * 3.0    29/08/2019              José Luis Vargas Lara                         Correccion issues sonar
 **/
@isTest
private class MX_SB_SAC_Business_TEST {
    /** variable para nombre de producto */
    final static String PRODNAME = 'WIBE';
    /** generates mock data */
    @TestSetup
    static void makeData() {
        final User tUser = MX_WB_TestData_cls.crearUsuario(System.Label.MX_SB_VTS_ProfileAdmin, System.Label.MX_SB_VTS_ProfileAdmin);
        insert tUser;
        System.runAs(tUser) {
            final Product2 produc = MX_SB_SAC_UtileriasTestCls.creaProducto();
            insert produc;
            final MX_SB_SAC_Catalogo_Clipert_Producto__c ccp = new MX_SB_SAC_Catalogo_Clipert_Producto__c();
            ccp.Name = PRODNAME;
            ccp.MX_SB_SAC_Producto__c = produc.Id;
            ccp.MX_SB_SAC_Activo__c = true;
            insert ccp;
        }
    }

    /**
    * @description: Obtiene las credenciales para logeo
    * @author Saul | 16/8/2019
    * @return @isTest
    */
    @isTest
    static void getLoginToken() {
        Test.startTest();
        final MX_SB_SAC_LoginToken__c logTok = MX_SB_SAC_UtileriasTestCls.creaCredencial();
        logTok.Name = 'Actualiza Polizas';
        insert logTok;
        final MX_SB_SAC_LoginToken__c login = MX_SB_SAC_Business_Cls.getLoginToken(logTok.Name);
        System.assert(login != null, 'Login generado correctamente');
        Test.stopTest();
    }

    /**
    * @description: Obtiene el parámetro de entrada de la póliza
    * @author Saul | 16/8/2019
    * @return @isTest
    */
    @isTest
    static void getParamsPoliza() {
        Test.startTest();
        final MX_SB_SAC_ParametrosPoliza__c paramPol = MX_SB_SAC_UtileriasTestCls.creaCSParametros();
        insert paramPol;
        final MX_SB_SAC_ParametrosPoliza__c poliza = MX_SB_SAC_Business_Cls.getParamsPoliza(paramPol.Name);
        System.assert(poliza != null, 'Parametros de poliza obtenidos correctamente');
        Test.stopTest();
    }

    /**
    * @description: Obtiene el producto de la póliza
    * @author Saul | 16/8/2019
    * @return @isTest
    */
    @isTest
    static void getProductSAC() {
        Test.startTest();
        final MX_SB_SAC_Catalogo_Clipert_Producto__c ccp = [select Id, Name, MX_SB_SAC_Producto__c from MX_SB_SAC_Catalogo_Clipert_Producto__c where Name =: PRODNAME];
        final String producto = MX_SB_SAC_Business_Cls.getProductSAC(ccp.Name);
        System.assertEquals(producto, ccp.MX_SB_SAC_Producto__c, 'Consulta de producto exitosa');
        Test.stopTest();
    }
}