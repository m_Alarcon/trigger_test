/**
* ------------------------------------------------------------------------------
* @Nombre: MX_WF_CampanasController_test
* @Autor: Sandra Ventura García
* @Proyecto: Workflow Campañas
* @Descripción : Clase de prueba para MX_WF_CampanasController
* ------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ------------------------------------------------------------------------------
* 1.0           18/09/2019     Sandra Ventura García	         test
* 2.0           21/11/2019     Sandra Ventura García             Se agrega test para crear canales de multipromociones.
* ------------------------------------------------------------------------------
*/
@isTest
private class MX_WF_CampanasController_test {

  /**
  * @description: Datos prueba lista campañas
  * @author Sandra Ventura
  */ 
   public static List<Campaign> createCampaign(Integer num) {
        final List<Campaign> camp = new List<Campaign>();
       
       final Id rectypeCamp = [SELECT Id FROM RecordType WHERE DeveloperName = 'CRM_WF_Alianzas_Comerciales' AND sObjectType = 'Campaign'].Id;
       
       final Date vStartDate = Date.newInstance(2019, 1, 1);
       final Date vEndDate = Date.newInstance(2019, 5, 1);
       
       for(Integer i=0;i<num;i++) {
         final Campaign cmps = new Campaign(recordtypeid = rectypeCamp, 
                                      Name='Test camp'+ i,
                                      CRM_WF_Tipo_De_Area_Solicitante__c = 'Squad',
                                      StartDate = vStartDate.addDays(i),
                                      EndDate = vEndDate.addDays(i),
                                      MX_WF_Fecha_de_publicacion__c = vStartDate.addDays(i)+2,
                                      CRM_WF_Area_Solicitante__c='SQUAD_ADQUIRENCIA',
                                      CRM_WF_Con_Criterios__c = 'No', 
                                      Status='Ejecución'
                                      );
            
            
            camp.add(cmps);
            
        }
        insert camp;
       
        return camp;
    }
    
  /**
  * @description: Datos prueba lista comercios
  * @author Sandra Ventura
  */ 
    public static List<Account> createAccount(Integer num) {
       final List<Account> grupc = new List<Account>();
        final Id recTypeGC = [SELECT Id FROM RecordType WHERE DeveloperName = 'GBL_WF_Grupos_Comerciales' AND sObjectType = 'Account'].Id;

        final Account grupoc = new Account(recordtypeid = recTypeGC, Name='Grupotest1');
        
        insert grupc;
        
        final List<Account> acc = new List<Account>();
        final Id recTypeAcc = [SELECT Id FROM RecordType WHERE DeveloperName = 'GBL_WF_Grupos_Comerciales' AND sObjectType = 'Account'].Id;

        for(Integer i=0;i<num;i++) {
            final Account acct = new Account(recordtypeid = recTypeAcc, 
                                    Name='TestAccount'+ i,
                                    GBL_WF_Grupo_Comercial__c= grupoc.id);
            
            acc.add(acct);
            
        }
        insert acc;
       
        return acc;
    } 
  /**
  * @description: Datos prueba brief
  * @author Sandra Ventura
  */   
    public static List<CRM_WF_Brief__c> createBrief(Integer num) {

        final Id recTypemulti = [SELECT Id FROM RecordType WHERE DeveloperName = 'CRM_WF_Campana_Multipromocion' AND sObjectType = 'Campaign'].Id;
        final date finicio= date.today();
        final Campaign multipromo = new Campaign(recordtypeid = recTypemulti,
                                                 Name='MultipromocionTest-'+num,
                                                 StartDate = finicio,
                                                 EndDate = finicio.addYears(1));
        insert multipromo;

        final Id recTypeBri = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_WF_Email_Dinamico' AND sObjectType = 'CRM_WF_Brief__c'].Id;
        final List<CRM_WF_Brief__c> brief = new List<CRM_WF_Brief__c>();

        for(Integer i=0;i<num;i++) {
            final CRM_WF_Brief__c briefmulti = new CRM_WF_Brief__c(recordtypeid = recTypeBri,
                                                                   CRM_WF_Subject__c='TestBSubject'+ i,
                                                                   MX_WF_Fecha_de_inicio__c= Date.today(),
                                                                   CRM_WF_Campana_Brief__c = multipromo.id );

            brief.add(briefmulti);

        }
        insert brief;

        return brief;
    }
  /**
  * @description: Datos prueba comercio participante
  * @author Sandra Ventura
  */
    public static List<MX_WF_Comercios_participantes__c> createComercioP(Campaign[] campanias) {
        final Account[] acccom = MX_WF_CampanasController_test.createAccount(50);

        final List<MX_WF_Comercios_participantes__c> comercio = new List<MX_WF_Comercios_participantes__c>();

        for(Integer i=0;i<campanias.size();i++) {
            final MX_WF_Comercios_participantes__c comerciop = new MX_WF_Comercios_participantes__c(MX_WF_Comercio_participante__c=acccom[i].Id,
                                                                                                    MX_WF_Campana__c = campanias[i].Id);

            comercio.add(comerciop);

        }
        insert comercio;

        return comercio;
    }
  /**
  * @description: Datos prueba canales vigentes
  * @author Sandra Ventura
  */  
    public static List<CRM_WF_Artes__c> createCanalVig(Campaign[] campanias, MX_WF_Comercios_participantes__c[] compart,CRM_WF_Brief__c[] brief) {

        final List<CRM_WF_Artes__c> comercio = new List<CRM_WF_Artes__c>();
        final date ffinal = Date.today();

        for(Integer i=0;i<campanias.size();i++) {
            final CRM_WF_Artes__c comerciop = new CRM_WF_Artes__c(CRM_WF_Nombre_Del_Brief__c = brief[0].Id,
                                                                  MX_WF_Comercio__c=compart[i].MX_WF_Comercio_participante__c,
                                                                  MX_WF_Campana__c = campanias[i].Id,
                                                                  CRM_WF_Estatus__c = 'Cierre',
                                                                  MX_WF_Tipo_de_registro_autom_tico__c = 'Multipromoción',
                                                                  MX_WF_Fecha_fin_de_vigencia_del_arte__c = ffinal.addDays(i),
                                                                  MX_WF_Aprobado__c =true);

            comercio.add(comerciop);

        }
        insert comercio;

        return comercio;
    }
  /**
  * @description: Datos prueba canal duplicado
  * @author Sandra Ventura
  */  
    public static List<CRM_WF_Artes__c> createCanalEx(Campaign[] campanias, MX_WF_Comercios_participantes__c[] compart,CRM_WF_Brief__c[] brief) {

        final List<CRM_WF_Artes__c> artes = new List<CRM_WF_Artes__c>();
        final date ffinal = Date.today();

        for(Integer i=0;i<campanias.size();i++) {
            final CRM_WF_Artes__c artesp = new CRM_WF_Artes__c(CRM_WF_Nombre_Del_Brief__c = brief[0].Id,
                                                                  MX_WF_Comercio__c=compart[0].MX_WF_Comercio_participante__c,
                                                                  MX_WF_Campana__c = campanias[0].Id,
                                                                  CRM_WF_Estatus__c = 'Cierre',
                                                                  MX_WF_Tipo_de_registro_autom_tico__c = 'Multipromoción',
                                                                  MX_WF_Fecha_fin_de_vigencia_del_arte__c = ffinal.addDays(5),
                                                                  MX_WF_Aprobado__c =true);

            artes.add(artesp);

        }
        insert artes;

        return artes;
    }
  /**
  * @description: test lista campañas 
  * @author Sandra Ventura
  */
    @isTest static void campanasTest() {

        final Campaign[] camp = MX_WF_CampanasController_test.createCampaign(100);
        final List<CRM_WF_Brief__c> brief = MX_WF_CampanasController_test.createBrief(1);
        test.startTest();
        MX_WF_CampanasController.getListCampanas(brief[0].Id);
         System.assert(camp.size() > 1,'Lista de campañas test.');
        test.stopTest();
        }
  /**
  * @description: test lista comercios 
  * @author Sandra Ventura
  */ 
    @isTest static void comerciosTest() {

           final Account[] acc = MX_WF_CampanasController_test.createAccount(200);
           final Id grupoc = [SELECT GBL_WF_Grupo_Comercial__r.Id FROM Account WHERE Name LIKE 'TestAccount%' LIMIT 1].Id;

        test.startTest();
        MX_WF_CampanasController.getListComercios(grupoc);
         System.assert(acc.size() > 1,'Lista de comercios test.');
        test.stopTest();
        }
  /**
  * @description: test insert canales 
  * @author Sandra Ventura
  */
    @isTest static void canalesTest() {

           final Campaign[] campanias = MX_WF_CampanasController_test.CreateCampaign(50);
           final CRM_WF_Brief__c[] brief = MX_WF_CampanasController_test.createBrief(1);
           final MX_WF_Comercios_participantes__c[] compart = MX_WF_CampanasController_test.createComercioP(campanias);
           MX_WF_CampanasController_test.createCanalVig(campanias, compart, brief);
           final String[] campmulti = New String[] {};
            for (Campaign camp : campanias) {
                campmulti.add(camp.Id);
               }
        test.startTest();
        final boolean  canal = MX_WF_CampanasController.getArtesDup(campmulti,brief[0].Id);
         System.assert(canal,'Generación de artes test.');
        test.stopTest();
        }
}