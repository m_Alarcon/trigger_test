/**
* Indra
* @author           Julio Medellín Oliva
* Project:          Presuscritos
* Description:      Clase de utilidad que brinda distintas creaciones de objetos para clases test.
*
* Changes (Version)
* ------------------------------------------------------------------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    --------------------------------------
* @version  1.0     2020-05-25      Julio Medellín Oliva.         Creación de la Clase
* @version  1.1     2020-11-18      Gerardo Mendoza Aguilar.      Agrega Method getDataCampaignTest
* @version  1.2     2020-12-15      Juan Carlos Benitez           Se añade method testavanza
*/

@isTest
public class MX_SB_PS_Container1_Ctrl_Test {   
     /**
    * @Method Data Setup
    * @Description Method para preparar datos de prueba 
    * @return void
    **/
	@testSetup
    public static void  data() { 
     MX_SB_PS_OpportunityTrigger_test.makeData();
     Final Opportunity opp  =[SELECT Id from Opportunity limit 1];
     final Task tskOpp = new Task(WhatId=opp.Id,Status='Completed',Priority='Normal');
     insert tskOpp;
        final Campaign camp1 = new Campaign(Name = 'Campaign Test', CampaignMemberRecordTypeId=Schema.SObjectType.CampaignMember.getRecordTypeInfosByDeveloperName().get('MX_BPP_CampaignMember').getRecordTypeId());
        insert camp1;      
    }
    /**
    * @Method prueba1
    * @Description Method para probar la clase 
    * @return void
    **/
    public testMethod static void prueba1() {
        final Opportunity opp = [Select ID, StageName, recordTypeId, AccountId, Name FROM Opportunity LIMIT 1];
        System.assertEquals(opp.Id, MX_SB_PS_Container1_Ctrl.fetchOpp(opp.Id).Id,'Success');
    }
	  /**
    * @Method testupsrt
    * @Description Method para probar el method upsrtOpp 
    * @return void
    **/
    @isTest
    static void testUpsrtSuccss() {
        test.startTest();
        final Opportunity oppObj = [Select Id,StageName from Opportunity limit 1];
        oppObj.StageName=System.Label.MX_SB_PS_Etapa3;
        	MX_SB_PS_Container1_Ctrl.upsrtOpp(oppObj);
        	system.assert(true,'Se ha actualizado correctamente la oportunidad');
        test.stopTest();
    }
      /**
    * @Method upsrtlineitem
    * @Description Method para probar el method upsrtQuoteLI 
    * @return void
    **/
    @isTest
    static void upsrtlineitem() {
        test.startTest();
        final Opportunity oppObj =[Select ID, AccountId, StageName FROM Opportunity LIMIT 1];
        oppObj.StageName=System.Label.MX_SB_PS_Etapa3;
        final Quote qtst = [Select id,Name,OpportunityId from quote where MX_SB_VTS_Folio_Cotizacion__c='12345' limit 1];
        final QuoteLineItem[] listqli = [select id,QuoteId,Product2Id,PricebookEntryId from QuoteLineItem where QuoteId =:qtst.Id];
        	MX_SB_PS_Container1_Ctrl.upsrtQuoteLI(qtst,listqli[0],qtst.OpportunityId);
        	system.assertEquals(qtst.Name,'qtest','Quote validado coreectamente');
        test.stopTest();
    }
    /**
    * @Method getTaskData
    * @Description Method para probar el method getTaskData 
    * @return void
    **/
    @isTest
    static void tstgetTaskData() {
        test.startTest();
        final String oppId =[SELECT ID, AccountId, StageName FROM Opportunity LIMIT 1].Id;
        MX_SB_PS_Container1_Ctrl.getTaskData(oppId);
	system.assert(true,'Se ha encontrado informacion');
        test.stopTest();
    }
        /**
    * @Method getTaskData
    * @Description Method para probar el method getTaskData 
    * @return void
    **/
    @isTest
    static void upsrtTask() {
        test.startTest();
        final Task tskObj =[SELECT ID FROM Task LIMIT 1];
        tskObj.MX_SB_VTA_Resultado_del_contacto__c='Si';
      	MX_SB_PS_Container1_Ctrl.upsrtTask(tskObj,'Llamada');
	system.assert(true,'Se ha ha actualizado la informacion de la tarea');
        test.stopTest();
    }
        /**
    * @Method testloadAccountJ
    * @Description Method para probar el method loadAccountJ 
    * @return void
    **/
    @isTest
    static void testloadAccountJ () {
        final String accId =[SELECT ID, AccountId, StageName FROM Opportunity LIMIT 1].AccountId;
        test.startTest();
       	MX_SB_PS_Container1_Ctrl.loadAccountJ(accId);
	system.assert(true,'Se ha encontrado informacion de la cuenta');
        test.stopTest();
    }
     /**
    * @description test getDataCampaign
    * @author Gerardo Mendoza Aguilar | 11-18-2020 
    **/
    @isTest
    public static void getDataCampaignTest() {
        Test.startTest();
        Final Id idCamp1 = [SELECT Id FROM Campaign LIMIT 1].Id;
        Final List<Campaign> camp1 = MX_SB_PS_Container1_Ctrl.getDataCampaign(idCamp1);
        System.assert(!camp1.isEmpty(), 'Campanya encontrada');
        Test.stopTest();
    }
    @isTest
    static void testavanza() {
        test.startTest();
        Final  List<QuoteLineItem> quoli= [Select Id, QuoteId from QuoteLineItem where MX_WB_Folio_Cotizacion__c='11,11,11'];
        final List<Opportunity> oppId =[SELECT ID, AccountId, StageName FROM Opportunity where SyncedQuoteId=:quoli[0].QuoteId];
        try {
       		MX_SB_PS_Container1_Ctrl.avanza(oppId[0].Id);
        } catch(Exception e) {
            system.assert(!String.isEmpty(e.getMessage()),'Script-thrown esperado');
        }
        test.stopTest();
    }
}