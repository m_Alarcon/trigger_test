/**
 * @File Name          : MX_SB_VTS_LeadMultiCTI_Util.cls
 * @Description        :
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              :
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 28/1/2020 10:19:14
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    30/8/2019 17:12:05   Eduardo Hernández Cuamatzi     Initial Version
 * 1.0.1  28/01/2020 17:12:05   Eduardo Hernández Cuamatzi     Fix desfase de horas
**/
public virtual class MX_SB_VTS_LeadMultiCTI_Util { //NOSONAR
    /** Valor random*/
    final static Integer MAKERANDOM = 1;
    /** Formato de fecha*/
      final static String FORMATDATE = System.Label.MX_SB_VTS_FormatDateSmart;
       /**
     * getRandomProvider Asigna Aleatoriamente un proveedor al producto para cada lead a enviar
     * @param  valsFamPro   lista de Familia proveedores disponibles
     * @param  valsFam      lista de famlia de productos
     * @param  leadProducts lista de productos a enviar
     * @param  leadItemId   Id de lead a enviar
     * @return              Familia proveedor al que se envia lead
     */
    public static MX_SB_VTS_FamliaProveedores__c getRandomProvider(Map<Id, List<MX_SB_VTS_FamliaProveedores__c>> valsFamPro, Map<String, Product2> valsFam, Map<Id, String> leadProducts, Id leadItemId) {
        MX_SB_VTS_FamliaProveedores__c finalProvider = new MX_SB_VTS_FamliaProveedores__c();
        final Product2 famIdProduct = valsFam.get(leadProducts.get(leadItemId));
        final List<MX_SB_VTS_FamliaProveedores__c> lstProvProduct = valsFamPro.get(famIdProduct.MX_WB_FamiliaProductos__c);
        if(lstProvProduct.size() > MAKERANDOM) {
            finalProvider = lstProvProduct[Integer.valueof(math.random() * lstProvProduct.size())];
        } else {
            finalProvider = lstProvProduct[0];
        }
        return finalProvider;
    }

    /**
     * fillFullName Completa nombre completo cliente
     * @param  leadEntry Registro a enviar
     * @return           Nombre completo del cliente
     */
    public static String fillFullName(Lead leadEntry) {
        String fullName = '';
        fullName = leadEntry.FirstName;
        fullName += String.isNotBlank(leadEntry.LastName) ? ' '+leadEntry.LastName: '';
        fullName += String.isNotBlank(leadEntry.Apellido_Materno__c) ? ' '+leadEntry.Apellido_Materno__c : '';
        return fullName;
    }

    /**
     * checkDate evalua formato de fecha
     * @param  leadRecord registro de Lead ah evaluar
     * @return            fecha valida para envio a SmartCenter
     */
    public static String checkDate(Lead leadRecord) {
        String finalDate = '';
        if(String.isBlank(leadRecord.MX_SB_VTS_HoraCallMe__c)) {
            final Datetime myDT = Datetime.now().addMinutes(Integer.valueOf(System.Label.MX_SB_VTS_SmartCallMinutes));
            finalDate = myDT.format(FORMATDATE);
        } else {
             final Datetime arrDate = leadRecord.MX_SB_VTS_FechaHoraAgenda__c.addMinutes(Integer.valueOf(System.Label.MX_SB_VTS_SmartCallMinutes));
             finalDate = arrDate.format(FORMATDATE);
        }
        return finalDate;
    }

    /**
     * findBandeja recupera bandeja HotLead para proveedor
     * @param  wrapperEnvio Lead a enviar
     * @param  lstBandejas  bandejas HotLeads por proveedor
     * @return              bandeja hotLeads
     */
    public static string findBandeja( WrapperEnvioCTI wrapperEnvio, List<MX_SB_VTS_Lead_tray__c> lstBandejas) {
        String idBandeja = '';
        for(MX_SB_VTS_Lead_tray__c bandeja : lstBandejas) {
            //if(bandeja.MX_SB_VTS_Producto__r.Name.equalsIgnoreCase(wrapperEnvio.product) &&
            if(bandeja.MX_SB_VTS_ProveedorCTI__c.equals(wrapperEnvio.proveedor.MX_SB_VTS_ProveedorCTI__c)) {
                idBandeja = bandeja.MX_SB_VTS_ID_Bandeja__c;
            }
        }
        return idBandeja;
    }

    /**
     * sendtoVCIP enviar leads a VCIP
     * @param  envioCTI lista de leads ah enviar
     */
    public static void sendtoVCIP(List<WrapperEnvioCTI> envioCTI) {
        for(WrapperEnvioCTI itemWrapper : envioCTI) {
            MX_WB_CTI_cls.ftProcesaSol(itemWrapper.leadId.Id, itemWrapper.leadId.Folio_Cotizacion__c, evalueteTrayVCIP(itemWrapper.leadId), itemWrapper.leadId.OwnerId, itemWrapper.sNombre, itemWrapper.leadId.MobilePhone, 'Lead', 0, '', '');//NOSONAR
        }
    }

     /**
     * sendtoVCIP enviar leads a VCIP
     * @param  envioCTI lista de leads ah enviar
     */
    public static void sendtoVCIPCall(List<WrapperEnvioCTI> envioCTI) {
        for(WrapperEnvioCTI itemWrapper : envioCTI) {
            MX_WB_CTI_cls.ProcesaSol(itemWrapper.leadId.Id, itemWrapper.leadId.Folio_Cotizacion__c, evalueteTrayVCIP(itemWrapper.leadId), itemWrapper.leadId.OwnerId, itemWrapper.sNombre, itemWrapper.leadId.MobilePhone, 'Lead', 0, '', '');//NOSONAR
        }
    }

    /**
    * @description Evalua el origen del registro para decidir a que bandeja se enviara
    * @author Eduardo Hernández Cuamatzi | 6/1/2020 
    * @param leadId Registro que entro por un webToLead o servicio con origen CMB, Facebook
    * @return Retorna el nombre de la bandeja 
    **/
    public static String evalueteTrayVCIP(Lead leadId) {
        String asdTrayCMB = 'ASD';
        if(leadId.LeadSource.equalsIgnoreCase(System.Label.MX_SB_VTS_OrigenFacebook)) {
            asdTrayCMB = System.Label.MX_SB_VTS_OrigenFacebook;
        }
        return asdTrayCMB;
    }

    /**
    * @description Recupera las tipificaicones de remarcado
    * @author Eduardo Hernández Cuamatzi | 4/11/2019
    * @return Map<String, MX_WB_MotivosNoContacto__c> mapa de tipificaciones
    **/
    public static Map<String, MX_WB_MotivosNoContacto__c> fillMotivos() {
        final Map<String, MX_WB_MotivosNoContacto__c> mapMotivos = new Map<String, MX_WB_MotivosNoContacto__c>();
        for(MX_WB_MotivosNoContacto__c motivo : [Select Id, MX_SB_VTS_Nivel_6__c, MX_SB_VTS_IDACCION__c, MX_SB_VTS_HoraDuracion__c,
            MX_SB_VTS_AplicaBloqueo__c, MX_SB_VTS_ReglaActiva__c, MX_SB_VTS_RecallGroup__c,MX_SB_VTS_AccionUno__c,
            MX_SB_VTS_AccionDos__c,MX_SB_VTS_AccionTres__c, MX_SB_VTS_AccionCuatro__c from MX_WB_MotivosNoContacto__c where MX_SB_VTS_ReglaActiva__c = true]) {
                mapMotivos.put(motivo.MX_SB_VTS_Nivel_6__c, motivo);
        }
        return mapMotivos;
    }


    /**
    * @description Recupera los motivos de no volver a marcar
    * @author Eduardo Hernández Cuamatzi | 6/11/2019
    * @return Map<String, MX_WB_MotivosNoContacto__c>
    **/
    public static Map<String, MX_WB_MotivosNoContacto__c> fillNoReCall() {
        final Map<String, MX_WB_MotivosNoContacto__c> mapNoNoCall = new Map<String, MX_WB_MotivosNoContacto__c>();
        for(MX_WB_MotivosNoContacto__c motivoNo : [Select Id, MX_SB_VTS_Nivel_6__c, MX_SB_VTS_IDACCION__c, MX_SB_VTS_HoraDuracion__c,
            MX_SB_VTS_AplicaBloqueo__c, MX_SB_VTS_ReglaActiva__c from MX_WB_MotivosNoContacto__c where MX_SB_VTS_ReglaActiva__c = true]) {
                mapNoNoCall.put(motivoNo.MX_SB_VTS_Nivel_6__c, motivoNo);
        }
        return mapNoNoCall;
    }


    /**
    * @description recupera las bandjeas de un proveedor
    * @author Eduardo Hernández Cuamatzi | 4/11/2019
    * @param proveedor
    * @return Map<Id, MX_SB_VTS_Lead_tray__c>
    **/
    public static Map<Id, MX_SB_VTS_Lead_tray__c> fillTrays() {
        return new Map<Id, MX_SB_VTS_Lead_tray__c>([Select Id, MX_SB_VTS_ID_Bandeja__c, MX_SB_VTS_ServicioID__c, MX_SB_VTS_ProveedorCTI__c from MX_SB_VTS_Lead_tray__c where MX_SB_VTS_ID_Bandeja__c NOT IN ('')]);
    }

    /**
     * Clase wrapper para control de registros a enviar
     */
    public class WrapperEnvioCTI {
        /** Variable  Id registro*/
        public Lead leadId {get;set;}
        /** Variable  Producto enviado*/
        public String product {get;set;}
        /** Variable  nombre concatenado*/
        public String sNombre {get;set;}
        /** Variable  número telefono*/
        public String sTelefono {get;set;}
        /** Variable  tipo de envio para credenciales*/
        public String typeSend {get;set;}
        /** Variable  tipo de credencial a enviar*/
        public Integer itipoVal {get;set;}
        /** Variable  proveedor al que se envia*/
        public MX_SB_VTS_FamliaProveedores__c proveedor {get;set;}
    }

    /**
    * @description recupera el telefono de carga base
    * @author Eduardo Hernández Cuamatzi | 4/11/2019
    * @param mobilePhone teléfono de call me back
    * @param secondPhone teléfono de telemarketing
    * @return string teléfono movil
    **/
    public static string evalutePhone(String mobilePhone, String secondPhone) {
        String mobilePhoneR = mobilePhone;
        if(String.isBlank(mobilePhoneR) || String.isEmpty(mobilePhoneR)) {
            mobilePhoneR = secondPhone;
        }
        return mobilePhoneR;
    }

    /**
    * @description
    * @author Eduardo Hernández Cuamatzi | 19/11/2019
    * @return Map<Id, MX_SB_VTS_ProveedoresCTI__c>
    **/
    public static Map<Id, MX_SB_VTS_ProveedoresCTI__c> fillProveedores() {
        return new Map<Id, MX_SB_VTS_ProveedoresCTI__c>([Select Id, Name, MX_SB_VTS_BusinessHoursL_V__c,MX_SB_VTS_FinishBusinessHoursL_V__c,
        MX_SB_VTS_BusinessHoursL_S__c,MX_SB_VTS_FinalBusinessHoursS__c,MX_SB_VTS_FranjaHorario__c from MX_SB_VTS_ProveedoresCTI__c
        where MX_SB_VTS_Identificador_Proveedor__c NOT IN ('')]);
    }

    /**
    * @calculateNextC
    * @author Eduardo Hernández Cuamatzi | 19/11/2019
    * @return String nueva accion calculada
    **/
    public static String calculateNextC(Datetime addTime, Integer hoursNAct, Time limitCall) {
        String finalCallDate = addTime.format(FORMATDATE);
        if(hoursNAct > 0) {
            final String dateWeekend = addTime.format('EEEE');
            final DateTime nextActCall = addTime.addHours(hoursNAct);
            finalCallDate = nextActCall.format(FORMATDATE);
            final Time timeNExtCall = Time.newInstance(nextActCall.hour(), nextActCall.minute(), 0, 0);
            if(dateWeekend.equalsIgnoreCase(System.Label.MX_SB_VTS_Saturday)) {
                if(timeNExtCall > limitCall) {
                    final DateTime timeRecall = Datetime.newInstance(addTime.year(), addTime.month(), addTime.day(), 9, 0, 0);
                    finalCallDate = timeRecall.addDays(1).format(FORMATDATE);
                }
            } else {
                finalCallDate = nextActCall.format(FORMATDATE);
                if(timeNExtCall > limitCall) {
                    final Integer addDays = addTime.date().daysBetween(nextActCall.date());
                    DateTime timeRecall = Datetime.newInstance(nextActCall.year(), nextActCall.month(), nextActCall.day(), 9, 0, 0);
                    if(addDays <= 0) {
                        timeRecall = Datetime.newInstance(nextActCall.year(), nextActCall.month(), nextActCall.addDays(1).day(), 9, 0, 0);
                    }
                    finalCallDate = timeRecall.format(FORMATDATE);
                } else if(addTime.date().daysBetween(nextActCall.date()) > 0) {
                    final DateTime nextActCallDay = Datetime.newInstance(nextActCall.year(), nextActCall.month(), nextActCall.day(), 9, 0, 0);
                    finalCallDate = nextActCallDay.format(FORMATDATE);
                }
            }
        } else {
            finalCallDate = '';
        }
        return finalCallDate;
    }

    /**
    * @evaluteSmartValue
    * @author: Diego Olvera | 16/12/2019
    * @return String valor de custom setting
    **/
    public static String evaluteSmartValue() {
        return MX_SB_VTS_Generica__c.getValues('ValorSmart').MX_SB_VTS_TimeValue__c;
    }

    /**
     * Clase wrapper para remarcado
     */
    public class WrapperRecallCTI {
        /** Variable  idORecord*/
        public String idRecord {get;set;}
        /** Variable  login*/
        public String login {get;set;}
         /** Variable serviceId*/
        public Integer serviceId {get;set;}
        /** Variable  loadId*/
        public String loadId {get;set;}
        /** Variable  scheduleDate*/
        public String scheduleDate {get;set;}
        /** Variable  número telefono1*/
        public String sTelefono1 {get;set;}
        /** Variable  telefono2*/
        public String sTelefono2 {get;set;}
        /** Variable  telefono3*/
        public String sTelefono3 {get;set;}
        /** Variable  nextAction*/
        public String nextAction {get;set;}
        /** Variable  para mostrar en pantalla*/
        public String displayName {get;set;}
    }
}