/**
* @description       : Clase Test que cubre MX_SB_VTS_GetPartData_DP_Ctrl
* @author            : Diego Olvera
* @group             : 
* @last modified on  : 10-21-2020
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   10-21-2020   Diego Olvera   Initial Version
* 1.1   12-01-2020   Diego Olvera   Ajustes a la clase consumo ASO
**/
@isTest
public class MX_SB_VTS_GetPartData_DP_C_Test {
   /*User name test*/
    private final static String USERTESTCT = 'ASESORGETDP';
    /*Account name test*/
    private final static String ACCOTESTCT = 'CUENTAGETDP';
    /*URL test*/
    private final static String URLTESTCT = 'http://www.ulrsampleDP.com';
    /* product code variable  */
    private final static String PRODUCTCODETEST = '3004';
    /* Aliance code variable */
    private final static String ALLIANCECODETEST = 'FHSRT004';
    
    /* 
@Method: setupTest
@Description: create test data set
*/
    @TestSetup
    static void testDatosPartCtrl() {
        final User tstDpUser = MX_WB_TestData_cls.crearUsuario(USERTESTCT, 'System Administrator');
        insert tstDpUser;
        
        System.runAs(tstDpUser) {
            
            final Account testDpAccTest = MX_WB_TestData_cls.crearCuenta(ACCOTESTCT, System.Label.MX_SB_VTS_PersonRecord);
            testDpAccTest.PersonEmail = 'pruebaVtsTest@mxsbvts.com';
            insert testDpAccTest;
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'getCoverages', iaso__Url__c = URLTESTCT, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'getParticularData', iaso__Url__c = URLTESTCT, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'getGTServicesSF', iaso__Url__c = URLTESTCT, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
        }        
    }
    
    /* 
@Method: getParticularData
@Description: test method to get the status code from web service
*/	@isTest
    static void getDatosPartTestCtrl() {
        final User sampleUsrCtrl = [SELECT Id FROM User WHERE LastName =: USERTESTCT];
        System.runAs(sampleUsrCtrl) {
            Final Map<String, String> headersMockTest = new Map<String, String>();
            headersMockTest.put('tsec', '368897844');
            Final MX_WB_Mock mockCalloutTest = new MX_WB_Mock(200, 'Complete', '{}', headersMockTest); 
            iaso.GBL_Mock.setMock(mockCalloutTest);
            test.startTest();
            MX_SB_VTS_GetPartData_DP_Ctrl.dptListCarInsCtrl(PRODUCTCODETEST, ALLIANCECODETEST);   
            test.stopTest();
            System.assertNotEquals('Falla', null,'No recuperó datos');
        }
    }
      /* 
@Method: getCoverages
@Description: test method to get the status code from web service
*/
    @isTest
    static void  getDatosPartTstCtrl() {
        final User sampleCtrl = [SELECT Id, Name FROM User WHERE LastName =: USERTESTCT];
        System.runAs(sampleCtrl) {
            final Map<String, String> headersMockCtrl = new Map<String, String>();
            headersMockCtrl.put('tsec', '835779944');
            final Map<String, Object> dataMapCtrl = new Map <String, Object>();
            dataMapCtrl.put('codigoAlianza', 'FHSRT014');
            dataMapCtrl.put('codigoProducto', '123');
            dataMapCtrl.put('codigoPlan', '456');
            dataMapCtrl.put('revisionPlan', '789');
            final MX_WB_Mock mockCallCtrl = new MX_WB_Mock(200, 'Complete', '{}', headersMockCtrl); 
            iaso.GBL_Mock.setMock(mockCallCtrl);
            test.startTest();
            MX_SB_VTS_GetPartData_DP_Ctrl.obtListCarInsCtrl(dataMapCtrl);  
            test.stopTest();
            System.assertNotEquals('No entro al metodo', null,'No recuperó nada datos');
        }
    }
}