/**
* @description       : Clase service que crea registro en objeto de direcciones
* @author            : Diego Olvera
* @group             : BBVA
* @last modified on  : 02-17-2021
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author                       Modification
* 1.0   11-25-2020   Diego Olvera   Initial Version
* 1.0.1 09-03-2021   Eduardo Hernaádez Se agregan metros para rentado
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_AddressInfo_Service {
    /** String campos de QUERY para Opportunity */
    public static final String OPPQRYFIELDS = 'Id, LeadSource, SyncedQuoteId, OwnerId, AccountId, StageName, Descripcion_y_comentarios__c, Name';
    /** Fields para lista de coberturas */
    public static final String COBERQUERY = 'Id, MX_SB_VTS_RelatedQuote__c, MX_SB_MLT_Estatus__c, recordType.Name, MX_SB_MLT_Cobertura__c, MX_SB_VTS_CoverageCode__c, MX_SB_VTS_GoodTypeCode__c, MX_SB_VTS_CategoryCode__c, MX_SB_MLT_Descripcion__c, MX_SB_VTS_TradeValue__c';
    /** String para referenciar valor de cobro */
    static final String VALPAYMENT = 'cobro';
     /** String para referenciar valor de formalizada */
    static final String VALSTAGE = 'formalizada';
    /** String para referenciar valor activo */
    static final String VALCHECKED = 'true';
    /** String para referenciar valor de propio recuperado de un mapa */
    static final String VALOWN = 'valPropio';
    /** String para referenciar valor de propio */
    static final String VALPRO = 'Propio';
    /** String para referenciar valor de direccion */
    static final String VALDIREC = 'Domicilio asegurado';
    /** String para query de monto */
    static final String FIELDAMO = 'Monto_de_la_oportunidad__c';
    /** Date para valor nulo */
    static final Date NVAL = null;
    /** Constructor de la clase */
    private MX_SB_VTS_AddressInfo_Service() {}

     /**
    * @description Función que actualiza etapa actual de la oportunidad
    * @author Diego Olvera | 27-11-2020 
    * @param srtOppId, etapaForm Id de la oportunidad y etapa actual del lwc recuperada desde el front JS
    * @return void
    **/ 
    public static void updCurrentOpp(String srtOppId, String etapaForm) {
        final Opportunity lstOppRecords = MX_RTL_Opportunity_Selector.getOpportunity(srtOppId, OPPQRYFIELDS);
        final List<Opportunity> oppLstRecords = new List<Opportunity>();
        oppLstRecords.add(lstOppRecords);
        for(Opportunity lstValOpps : oppLstRecords) {
            if(etapaForm == VALPAYMENT) {
                lstValOpps.StageName = 'Cobro y resumen de contratación';
            } else if(etapaForm == VALSTAGE) {
                 lstValOpps.StageName = 'Cotización';
                 lstValOpps.Descripcion_y_comentarios__c = etapaForm;
            } else {
                lstValOpps.Descripcion_y_comentarios__c = etapaForm;
            }
        }
        MX_RTL_Opportunity_Selector.updateResult(oppLstRecords, false);
    }

     /**
    * @description Función que obtiene la etapa/datos guardado en la oportunidad
    * @author Diego Olvera | 27-11-2020 
    * @param idOpps
    * @return List<Opportunity> lstValsOpp
    **/  
    public static List<Opportunity> gtOppVals(String idOpps) {
        final List<Opportunity> lstValsOpp = new List<Opportunity>();
        final Opportunity oppRecords = MX_RTL_Opportunity_Selector.getOpportunity(idOpps, OPPQRYFIELDS);
        lstValsOpp.add(oppRecords);
        return lstValsOpp;
    }

    /**
    * @description Función que Crea o Actualiza un registro en el objeto de direcciones
    * @author Diego Olvera | 27-11-2020 
    * @param srtId, fdataAddress mapa llenado desde el fron JS con valores de direccion
    * @return void
    **/  
    public static void createAddress(String srtId, Map<String, Object> fdataAddress) {
        final Set<String> setAddId = new Set<String>{srtId};
        final Set<String> setDirType = new Set<String>{VALDIREC};
        final List<MX_RTL_MultiAddress__c> getAddList = MX_RTL_MultiAddress_Selector.findAddress(setAddId, setDirType);
        final String checkId = fdataAddress.get('checkedId').toString();
        if(checkId == VALCHECKED || !getAddList.isEmpty()) {
            updAddress(getAddList, fdataAddress);
        } else {
            crtAddress(srtId, fdataAddress);
        }
    }
    
     /**
    * @description Función que crea registro de direcciones
    * dependiendo del tipo de direccion que se especifique en el js
    * @author Diego Olvera | 12-02-2021 
    * @param  idOpps, rdataAddress
    * @return void
    **/ 
    public static void crtAddress(String idOpps, Map<String, Object> rdataAddress) {
        final Opportunity lstOpps = MX_RTL_Opportunity_Selector.getOpportunity(idOpps, OPPQRYFIELDS);
         final List<Opportunity> lstOppsIter = new List<Opportunity>{lstOpps};
        final List<MX_RTL_MultiAddress__c> addressList = new List<MX_RTL_MultiAddress__c>();
        for(Opportunity lstIterOpp : lstOppsIter) {
            final MX_RTL_MultiAddress__c addressCreate = new MX_RTL_MultiAddress__c();
            addressCreate.MX_RTL_MasterAccount__c = lstIterOpp.AccountId;
            addressCreate.MX_RTL_Opportunity__c = lstIterOpp.Id;
            addressCreate.Name = 'Direccion Propiedad';
            addressCreate.MX_RTL_PostalCode__c = rdataAddress.get('codigoPostal').toString();
            addressCreate.MX_RTL_AddressState__c = rdataAddress.get('State').toString();
            addressCreate.MX_RTL_AddressMunicipality__c = rdataAddress.get('Municipio').toString();
            addressCreate.MX_RTL_AddressCity__c = rdataAddress.get('City').toString();
            addressCreate.MX_RTL_AddressReference__c = rdataAddress.get('Reference').toString();
            addressCreate.MX_RTL_Uso_Habitacional__c = rdataAddress.get('valUso').toString();
            addressCreate.MX_RTL_AddressType__c = 'Domicilio asegurado';
            if(rdataAddress.get(VALOWN).toString() == VALPRO) {
                addressCreate.MX_RTL_Tipo_Propiedad__c = rdataAddress.get(VALOWN).toString();
                addressCreate.MX_SB_VTS_Metros_Cuadrados__c = rdataAddress.get('mtsBuild').toString();
            } else {
                addressCreate.MX_SB_VTS_Metros_Cuadrados__c = '60';
                addressCreate.MX_RTL_Tipo_Propiedad__c = rdataAddress.get(VALOWN).toString();
            }
            addressList.add(addressCreate);
        }
        MX_RTL_MultiAddress_Selector.insertAddress(addressList);
    }
    
     /**
    * @description Función que actualiza registro de direcciones
    * dependiendo del tipo de direccion que se especifique en el js
    * @author Diego Olvera | 12-02-2021 
    * @param getAddLst, gdataAddress
    * @return void
    **/ 
    public static void updAddress(List<MX_RTL_MultiAddress__c> getAddLst, Map<String, Object> gdataAddress) {
        final List<MX_RTL_MultiAddress__c> lstSetAddre = new List<MX_RTL_MultiAddress__c>();
        for(MX_RTL_MultiAddress__c lstAddre : getAddLst) {
            final MX_RTL_MultiAddress__c addressCrt = new MX_RTL_MultiAddress__c();
            if('NoId'.equals(gdataAddress.get('idAddrPro').toString())) {
                addressCrt.Id = lstAddre.Id;   
            } else {
                addressCrt.Id = gdataAddress.get('idAddrPro').toString(); 
            }
            addressCrt.MX_RTL_PostalCode__c = gdataAddress.get('codigoPostal').toString();
            addressCrt.MX_RTL_AddressState__c = gdataAddress.get('State').toString();
            addressCrt.MX_RTL_AddressMunicipality__c = gdataAddress.get('Municipio').toString();
            addressCrt.MX_RTL_AddressCity__c = gdataAddress.get('City').toString();
            addressCrt.MX_RTL_AddressReference__c = gdataAddress.get('Reference').toString();
            addressCrt.MX_RTL_Uso_Habitacional__c = gdataAddress.get('valUso').toString();
            if(gdataAddress.get(VALOWN).toString() == VALPRO) {
                addressCrt.MX_RTL_Tipo_Propiedad__c = gdataAddress.get(VALOWN).toString();
                addressCrt.MX_SB_VTS_Metros_Cuadrados__c = gdataAddress.get('mtsBuild').toString();
            } else {
                addressCrt.MX_RTL_Tipo_Propiedad__c = gdataAddress.get(VALOWN).toString();
            }
            lstSetAddre.add(addressCrt);
        }
        MX_RTL_MultiAddress_Selector.upAddress(lstSetAddre);
    }

     /**
    * @description Función que obtiene datos del objeto de Direcciones
    * dependiendo del tipo de direccion que se especifique en el js
    * @author Diego Olvera | 27-11-2020 
    * @param iddOpps, dirType
    * @return List<MX_RTL_MultiAddress__c> lstAddVals
    **/ 
    public static List<MX_RTL_MultiAddress__c> getValAddress(String iddOpps, String dirType) {
        final Set<String> idSet = new Set<String>();
        final Set<String> typeDir = new Set<String>();
        idSet.add(iddOpps);
        typeDir.add(dirType);
        return MX_RTL_MultiAddress_Selector.findAddress(idSet, typeDir);
    }

     /**
    * @description Función que actualiza datos del formulario de DatosContratante
    * @author Diego Olvera | 27-11-2020 
    * @param srtId, mapDataQuote
    * @return void
    **/  
    public static void updQuoLine(String srtId, Map<String, Object> mapDataQuote) {
        final Opportunity objOpportunity = MX_RTL_Opportunity_Selector.getOpportunity(srtId, 'Name');
        final List<Opportunity> lstIdOpp = new List<Opportunity>();
        lstIdOpp.add(objOpportunity);
        final List<Quote> lstQuote = MX_RTL_Quote_Selector.selectSObjectsByOpps(lstIdOpp);
        final List<Quote> fLstQuo = new List<Quote>();
        for(Quote lstUpdQuo : lstQuote) {
            lstUpdQuo.MX_SB_VTS_Nombre_Contrante__c = mapDataQuote.get('NombreCliente').toString();
            lstUpdQuo.MX_SB_VTS_Apellido_Paterno_Contratante__c = mapDataQuote.get('ApellidoPaterno').toString();
            lstUpdQuo.MX_SB_VTS_Apellido_Materno_Contratante__c = mapDataQuote.get('ApellidoMaterno').toString();
            lstUpdQuo.MX_SB_VTS_Sexo_Conductor__c = mapDataQuote.get('SexoHm').toString();
            if('NA'.equals(mapDataQuote.get('FechaNac').toString())) {
                lstUpdQuo.MX_SB_VTS_FechaNac_Contratante__c = NVAL;   
            } else {
                lstUpdQuo.MX_SB_VTS_FechaNac_Contratante__c = Date.parse(mapDataQuote.get('FechaNac').toString());   
            }
            lstUpdQuo.MX_SB_VTS_LugarNac_Contratante__c = mapDataQuote.get('LugarNac').toString();
            lstUpdQuo.MX_SB_VTS_RFC__c = mapDataQuote.get('rfc').toString();
            lstUpdQuo.MX_SB_VTS_Homoclave_Contratante__c = mapDataQuote.get('Homoclave').toString();
            lstUpdQuo.Phone = mapDataQuote.get('TelefonoCel').toString();
            lstUpdQuo.MX_SB_VTS_Email_txt__c = mapDataQuote.get('CorreoElect').toString();
            lstUpdQuo.MX_SB_VTS_Curp_Contratante__c = mapDataQuote.get('Curp').toString();
            fLstQuo.add(lstUpdQuo);
        }
        MX_RTL_Quote_Selector.updateResult(fLstQuo, true);
    }

     /**
    * @description Obtiene datos de las coberturas por cotización
    * @author Diego Olvera | 08-01-2020 
    * @param oppId Id de registro trabajado (Opportunity)
    * @return Map<String, Object> Datos recuperados
    **/
    public static List<Cobertura__c> fillCoberVals(String oppId) {
        final Map<String, Object> getCotizData = MX_SB_VTS_DeleteCoverageHSD_ctrl.fillCotizData(oppId);
        final Id quoId = ((List<Opportunity>)getCotizData.get('dataOpp'))[0].SyncedQuoteId;
        final Set<Id> quoSetId = new Set<Id>();
        quoSetId.add(quoId);
        return MX_RTL_Cobertura_Selector.findCoverByQuote(COBERQUERY, ' ', quoSetId);
    }

     /**
    * @description Obtiene datos de las coberturas por cotización
    * @author Diego Olvera | 08-01-2020 
    * @param oppId Id de registro trabajado (Opportunity)
    * @return Map<String, Object> Datos recuperados
    **/
    public static void updSyncQuo(String oppId, String nameQuo) {
        final Opportunity objOpps = MX_RTL_Opportunity_Selector.getOpportunity(oppId, 'Name, SyncedQuoteId');
        final List<Opportunity> lstOppsVal = new List<Opportunity>();
        lstOppsVal.add(objOpps);
        final List<Quote> lstQuote = MX_RTL_Quote_Selector.selectSObjectsByOpps(lstOppsVal);
        for(Opportunity lsrCurrOpp : lstOppsVal) {            
            for(Quote lstUpdQuo : lstQuote) {
                if(lstUpdQuo.QuoteToName == nameQuo) {
                    lsrCurrOpp.SyncedQuoteId = lstUpdQuo.Id;
                } 
            }
        }
        MX_RTL_Opportunity_Selector.updateResult(lstOppsVal, false);
    }

    /**
    * @description Función que actualiza el campo de Monto_de_la_oportunidad__c
    * @author Marco Cruz | 27-11-2020 
    * @param srtOppId, etapaForm Id de la oportunidad y etapa actual del lwc recuperada desde el front JS
    * @return void
    **/ 
    public static void udpateAmo(String srtOppId, String insuredAmo) {
        final Opportunity opportAmo = MX_RTL_Opportunity_Selector.getOpportunity(srtOppId, FIELDAMO);
        final List<Opportunity> recordLst = new List<Opportunity>{opportAmo};
        for(Opportunity oppoIter : recordLst) {
			oppoIter.Monto_de_la_oportunidad__c = decimal.valueOf(insuredAmo);
        }
        MX_RTL_Opportunity_Selector.updateResult(recordLst, false);
    }
}