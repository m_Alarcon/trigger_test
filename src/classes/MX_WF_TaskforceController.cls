/**
* ------------------------------------------------------------------------------
* @Nombre: MX_WF_TaskforceController
* @Autor: Sandra Ventura García
* @Proyecto: Workflow Campañas
* @Descripción : Apex controller para componentes MX_WF_CmpInvitados, MX_WF_CmpEnvioEmail
* ------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ------------------------------------------------------------------------------
* 1.0           01/10/2019     Sandra Ventura García	         Desarrollo
* 1.1           25/06/2020      Selena Rodriguez        Se modifica sendTemplatedEmail para que solo se manden
*                                                       las  minutas a cada invitado que se agregue o a todos cuando los temas 
*                                                       se modifiquen
* ------------------------------------------------------------------------------
*/
public with sharing class MX_WF_TaskforceController {
  /**
  * @description: construnctor sin argumentos
  * @author Sandra Ventura
  */
    @TestVisible
    private MX_WF_TaskforceController() {}
  /**
  * @description: Obtiene lista de invitados recurrentes
  * @author Sandra Ventura
  */
@AuraEnabled
	public static List<Contact> getInvitadosRecurrentes(String rtContact) {
     try {
          final Id recTypeUser = [SELECT Id FROM RecordType WHERE DeveloperName =: rtContact AND sObjectType = 'Contact'].Id;
          return [Select id, Name, Email, MX_RTE_Direccion_General__c, GBL_WF_Nombre_Completo__c FROM Contact WHERE MX_WF_Recurrente_taskforce__c='Sí'AND recordtypeid=:recTypeUser ORDER BY Name ASC];
      	 } catch(Exception e) {
			throw new AuraHandledException(System.Label.MX_WF_ErrorTaskforce+ e);
         }
    }
  /**
  * @description: Obtiene lista de contactos del tipo RTE Eventos GFD
  * @author Sandra Ventura
  */
@AuraEnabled
	public static List<Contact> getInvitados(String rtContact, boolean bqactual) {
     try {
         final Id recTypeUser = [SELECT Id FROM RecordType WHERE DeveloperName =: rtContact AND sObjectType = 'Contact'].Id;
         List<contact> lcontactos = new List<contact>();
         if (bqactual) {
             final Id qactual = [SELECT Id FROM MX_RTE_PI__c WHERE MX_RTE_Fecha_fin__c >= today AND MX_RTE_Fecha_Inicio__c <= today].Id;
             lcontactos =  [Select id, Name, Email, MX_RTE_Direccion_General__c, GBL_WF_Nombre_Completo__c FROM Contact
                  WHERE recordtypeid=:recTypeUser AND Id IN (SELECT MX_RTE_Nombre__c FROM MX_RTE_Equipo__c WHERE MX_RTE_Q_de_Participacion__c =: qactual) ORDER BY Name ASC];
         } else {
             lcontactos =  [Select id, Name, Email, MX_RTE_Direccion_General__c, GBL_WF_Nombre_Completo__c FROM Contact WHERE recordtypeid=:recTypeUser ORDER BY Name ASC];
         }
            Integer contad = 0;
            while (contad < lcontactos.size()) {
                if(lcontactos.get(contad).email == null) {
                lcontactos.remove(contad);
              } else {
                contad++;
              }
            }
         return lcontactos;
      	 } catch(Exception e) {
			throw new AuraHandledException(System.Label.MX_WF_ErrorTaskforce+ e);
         }
    }
  /**
  * @description: Obtiene lista de contactos por rol Eventos GFD
  * @author Sandra Ventura
  */
@AuraEnabled
	public static List<AggregateResult> getInvitadosRol(String rtContact, boolean bqactual) {
     try {
          final Id recTypeUser = [SELECT Id FROM RecordType WHERE DeveloperName =: rtContact AND sObjectType = 'Contact'].Id;
          List<AggregateResult> lroles = new List<AggregateResult>();
         if (bqactual) {
             final Id qactual = [SELECT Id FROM MX_RTE_PI__c WHERE MX_RTE_Fecha_fin__c >= today AND MX_RTE_Fecha_Inicio__c <= today].Id;
             lroles = [SELECT MX_RTE_Rol__c, count(Id) personas FROM Contact WHERE recordtypeid=:recTypeUser AND Id IN (SELECT MX_RTE_Nombre__c FROM MX_RTE_Equipo__c
                  WHERE MX_RTE_Q_de_Participacion__c =: qactual) AND (NOT MX_RTE_Rol__c=NULL) AND (NOT Email=NULL) GROUP BY MX_RTE_Rol__c ORDER BY MX_RTE_Rol__c ASC];
         } else {
             lroles = [SELECT MX_RTE_Rol__c, count(Id) personas FROM Contact WHERE recordtypeid=:recTypeUser
                       AND (NOT MX_RTE_Rol__c=NULL) AND (NOT Email=NULL) GROUP BY MX_RTE_Rol__c ORDER BY MX_RTE_Rol__c ASC];
         }
          return lroles;
      	 } catch(Exception e) {
			throw new AuraHandledException(System.Label.MX_WF_ErrorTaskforce+ e);
         }
    }
  /**
  * @description: Obtiene lista de contactos por iniciativa Eventos GFD
  * @author Sandra Ventura
  */
@AuraEnabled
	public static List<MX_RTE_Iniciativa__c> getInvitadosEquipo(date fecha, boolean bqactual) {
     try {
          List<MX_RTE_Iniciativa__c> lequipos = new List<MX_RTE_Iniciativa__c>();
             if (bqactual) {
                 final Id qactual = [SELECT Id FROM MX_RTE_PI__c WHERE MX_RTE_Fecha_fin__c >=: fecha AND MX_RTE_Fecha_Inicio__c <=: fecha].Id;
                 lequipos = [Select Id, Name FROM MX_RTE_Iniciativa__c WHERE Id IN (SELECT MX_RTE_Iniciativa__c FROM MX_RTE_Equipo__c WHERE MX_RTE_Q_de_Participacion__c =: qactual AND (NOT MX_RTE_Nombre__r.Email=NULL)) ORDER BY Name ASC];
             } else {
                 lequipos = [Select Id, Name FROM MX_RTE_Iniciativa__c WHERE Id IN (SELECT MX_RTE_Iniciativa__c FROM MX_RTE_Equipo__c WHERE (NOT MX_RTE_Nombre__r.Email=NULL)) ORDER BY Name ASC];
             }
         return lequipos;
         } catch(Exception e) {
			throw new AuraHandledException(System.Label.MX_WF_ErrorTaskforce+ e);
         }
    }
  /**
  * @description: Obtiene lista de contactos final Eventos GFD
  * @author Sandra Ventura
  */
@AuraEnabled
	public static List<Contact> getInvitadosSel(String[] listrol, String[] listinvi, String rtContact, Boolean bqactualR, Boolean bqactualE) {
     try {
         final Set<contact> setcontact = new Set<contact>();
         final List<contact> result = new List<contact>();
         final Id recTypeUser = [SELECT Id FROM RecordType WHERE DeveloperName =: rtContact AND sObjectType = 'Contact'].Id;

         List<Contact> roles = new List<Contact>();
         List<Contact> equipos = new List<Contact>();
         final List<Contact> contactos = [SELECT Id, Name, email, MX_RTE_Rol__c FROM Contact WHERE recordtypeid=:recTypeUser AND Id in: listinvi];

         if (bqactualR) {
             final Id qactual = [SELECT Id FROM MX_RTE_PI__c WHERE MX_RTE_Fecha_fin__c >= today AND MX_RTE_Fecha_Inicio__c <= today].Id;
             roles = [SELECT Id, Name, email, MX_RTE_Rol__c FROM Contact
                                              WHERE recordtypeid=:recTypeUser AND MX_RTE_Rol__c=:listrol AND Id in (SELECT MX_RTE_Nombre__c FROM MX_RTE_Equipo__c WHERE MX_RTE_Q_de_Participacion__c =: qactual)];
         } else {
             roles = [SELECT Id, Name, email, MX_RTE_Rol__c FROM Contact WHERE recordtypeid=:recTypeUser AND MX_RTE_Rol__c=:listrol];
         }
         if (bqactualE) {
             final Id qactual = [SELECT Id FROM MX_RTE_PI__c WHERE MX_RTE_Fecha_fin__c >= today AND MX_RTE_Fecha_Inicio__c <= today].Id;
             equipos = [SELECT Id, Name, email, MX_RTE_Rol__c FROM Contact
                                            WHERE recordtypeid=:recTypeUser AND Id in (SELECT MX_RTE_Nombre__c FROM MX_RTE_Equipo__c WHERE MX_RTE_Q_de_Participacion__c =: qactual AND MX_RTE_Iniciativa__c in: listinvi)];
         } else {
             equipos = [SELECT Id, Name, email, MX_RTE_Rol__c FROM Contact WHERE recordtypeid=:recTypeUser AND Id in (SELECT MX_RTE_Nombre__c FROM MX_RTE_Equipo__c WHERE MX_RTE_Iniciativa__c in: listinvi)];
         }
         contactos.addAll(equipos);
         contactos.addAll(roles);
         setcontact.addAll(contactos);
         result.addAll(setcontact);
            Integer contador = 0;
            while (contador < result.size()) {
                if(result.get(contador).email == null||result.get(contador).MX_RTE_Rol__c == null) {
                result.remove(contador);
              } else {
                contador++;
              }
            }
         return result;
      	 } catch(Exception e) {
			throw new AuraHandledException(System.Label.MX_WF_ErrorTaskforce+ e);
         }
    }
  /**
  * @description: Ingresa invitados
  * @author Sandra Ventura
  */
 @AuraEnabled
    public static void insertInv( String[] listInvitados, String idEvent) {
      try {
      final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: idEvent].MX_WF_Taskforce__c;
      final Id idMinuta = [SELECT Id, Name FROM MX_WF_Minuta_Taskforce__c WHERE MX_WF_Taskforce__c=: IdTaskforce].Id;
      final List<MX_WF_Invitados_Taskforce__c> listInvit = new List<MX_WF_Invitados_Taskforce__c>();
      final List<Contact> invselec = [Select Id, Name FROM Contact WHERE id in: listInvitados];

           for(Contact invtask: invselec) {

              final MX_WF_Invitados_Taskforce__c invitf = new MX_WF_Invitados_Taskforce__c(MX_WF_Invitado__c = invtask.Id,
                                                                                           MX_WF_Minuta__c = idMinuta,
                                                                                           Taskforce__c=IdTaskforce);
              listInvit.add(invitf);}
           insert listInvit;
        } catch (Exception e) {
           throw new AuraHandledException(System.Label.MX_WF_ErrorTaskforce + e);
        }
    }
  /**
  * @description: Envía plantilla de minuta a invitados
  * @author Sandra Ventura
  */
@AuraEnabled
     public static void sendTemplatedEmail(String whatId) {

    try {
          final Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

        final String[] adressObjId = New String[] {};
        final List<MX_WF_Invitados_Taskforce__c> updtInvitados = new List<MX_WF_Invitados_Taskforce__c>();
        final Id userId = UserInfo.getUserId();
        final List<MX_WF_Invitados_Taskforce__c> listInv = [SELECT 	MX_WF_Correo_electr_nico__c FROM MX_WF_Invitados_Taskforce__c
                                                            WHERE Taskforce__c IN (SELECT MX_WF_Taskforce__c FROM MX_WF_Minuta_Taskforce__c WHERE Id=:whatId) and MX_RTE_Minuta_Enviada__c =false];
             for (MX_WF_Invitados_Taskforce__c inv : listInv) {
                 inv.MX_RTE_Minuta_Enviada__c=true;
                 updtInvitados.add(inv);
                 adressObjId.add( inv.MX_WF_Correo_electr_nico__c);
             }
        update updtInvitados;

             final Id templateId = [select id, name from EmailTemplate where developername = : 'MX_WF_Envio_de_minuta'].id;

            email.setToAddresses(adressObjId);
            email.setTargetObjectId(userId);
            email.setWhatId(whatId);
            email.setTemplateId(templateId);
            email.saveAsActivity = false;

            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        } catch (Exception e) {
                                 throw new AuraHandledException(System.Label.MX_WF_ErrorTaskforce + e);}
    }
}