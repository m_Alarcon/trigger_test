/**
* @File Name          : MX_BPP_Leads_Table_cls.cls
* @Description        :
* @Author             : Fanny Gabriela Becerra Hernandez
* @Group              :
* @Last Modified By   : Fanny Gabriela Becerra Hernandez
* @Last Modified On   : 25/03/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      25/03/2020        Fanny Gabriela Becerra Hernandez   Initial Version
* 2.0      01/06/2020        Gabriel García Rojas				Ajuste de clase al modelo SoC
**/
@SuppressWarnings()
public class MX_BPP_LeadCampaign_Table_Ctrl {

    /**Constructor */
    @testVisible
    private MX_BPP_LeadCampaign_Table_Ctrl() { }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return List<User>
    **/
	@AuraEnabled
    public static List<User> userData() {
        return MX_BPP_LeadCampaignTable_Service.listUser();
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return List<CampaignMember>
    **/
    @AuraEnabled
    public static List<CampaignMember> getLeadData(List<String> params, List<User> listUsers) {
        return MX_BPP_LeadCampaignTable_Service.getInfoMembersByFilter(params, listUsers);
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return Map<Object,List<String>>
    **/
    @AuraEnabled
    public static Map<Object,List<String>> getPicklistValues(String picklistObject) {
        return MX_BPP_LeadCampaignTable_Service.getOppPickListProductValue(picklistObject);
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return List<String>
    **/
    @AuraEnabled
    public static List<String> familyData() {
        return MX_BPP_LeadCampaignTable_Service.familyDataBySchema();
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return List<String>
    **/
    @AuraEnabled
    public static List<String> statusData() {
        return MX_BPP_LeadCampaignTable_Service.statusDataBySchema();
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return Opportunity
    **/
    @AuraEnabled
    public static Opportunity leadConv(Id leadId) {
        return MX_BPP_LeadCampaignTable_Service.getOppFromLeadConv(leadId);
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return List<String>
    **/
    @AuraEnabled
    public static List<String> fetchListSucursales(String division) {
        return MX_BPP_LeadCampaignTable_Service.fetchListSucursales(division);
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return List<String>
    **/
    @AuraEnabled
    public static List<String> fetchListDivisiones() {
        return MX_BPP_LeadCampaignTable_Service.fetchListDivisiones();
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return List<User>
    **/
    @AuraEnabled
    public static List<User> fetchCampaignMember(List<String> params) {
        return MX_BPP_LeadCampaignTable_Service.fetchCampaignMember(params);
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return List<String>
    **/
    @AuraEnabled
    @SuppressWarnings('sf:DUDataflowAnomalyAnalysis')
    public static List<String> userInfo() {
		List<String> userDataList = new List<String>();
		try {
			userDataList = MX_BPP_CompUtils_cls.getUsrData();
		} catch(Exception e) {
            throw new AuraHandledException(System.Label.MX_BPP_PyME_Error_Generico + ' ' + e);
        }
		return userDataList;
    }
}