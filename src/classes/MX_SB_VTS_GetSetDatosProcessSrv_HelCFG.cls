/**
 * @File Name          : MX_SB_VTS_GetSetDatosProcessSrv_HelCFG.cls
 * @Description        : Clase Encargada de Proporcionar Soporte de CFG
 *                       a la clase: MX_SB_VTS_GetSetDatosPricesSrv_Service
 * @Author             : Alexandro Corzo
 * @Group              : 
 * @Last Modified By   : Alexandro Corzo
 * @Last Modified On   : 10/01/2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0       10/01/2020      Alexandro Corzo        Initial Version
 * 1.1       26/02/2021      Alexandro Corzo        Se hacen ajustes para armar JSON ASO
 * 1.1.1     09/03/2021      Eduardo Hernández      Mejora de picklist dinámica incendio contenido
**/
@SuppressWarnings('sf:UseSingleton')
public class MX_SB_VTS_GetSetDatosProcessSrv_HelCFG {
    /** Trades: Variable de Apoyo: SFIRE */
    private final static String SFIRE = 'FIRE';
    /** Trades: Variable de Apoyo: SEARTH */
    private final static String SEARTH = 'EARTHQUAKE';
    /** Trades: Variable de Apoyo: SHIRISK */
    private final static String SHIRISK = 'HYDROMETEOROLOGICAL_RISKS';
    /** Trades: Variable de Apoyo: SCIVLIN */
    private final static String SCIVLIN = 'CIVIL_LIABILITY_INSURANCE';
    /** Trades: Variable de Apoyo: SMISC */
    private final static String SMISC = 'MISCELLANEOUS';
    /** Trades: Variable de Apoyo: STECH */
    private final static String STECH = 'TECHNICAL';
    
    /** Categories: Variable de Apoyo: SCBUILDS */
    private final static String SCBUILDS = 'BUILDINGS';
    /** Categories: Variable de Apoyo: SCCIVLI */
    private final static String SCCIVLI = 'CIVIL_LIABILITY';
    /** Categories: Variable de Apoyo: SCCRYSIG */
    private final static String SCCRYSIG = 'CRYSTALS_SIGNS';
    /** Categories: Variable de Apoyo: SCELECDEV */
    private final static String SCELECDEV = 'ELECTRONIC_DEVICES';
    /** Categories: Variable de Apoyo: SCVALITE */
    private final static String SCVALITE = 'VALUABLE_ITEMS';
    /** Categories: Variable de Apoyo: SCPERITEM */
    private final static String SCPERITEM = 'PERSONAL_ITEMS';
    
    /** GTypes: Variable de Apoyo: SGTBUILD */
    private final static String SGTBUILD = 'BUILDING';
    /** GTypes: Variable de Apoyo: SGFAMCIVLI */
    private final static String SGFAMCIVLI = 'FAMILY_CIVIL_LIABILITY';
    /** GTypes: Variable de Apoyo: SGFAMCIVLILE */
    private final static String SGFAMCIVLILE = 'CIVIL_LIABILITY_LESSEE';
    /** GTypes: Variable de Apoyo: SGTCONTENT */
    private final static String SGTCONTENT = 'CONTENT';
    /** GTypes: Variable de Apoyo: SGGLASSES */
    private final static String SGGLASSES = 'GLASSES';
    /** GTypes: Variable de Apoyo: SGHOMAME */
    private final static String SGHOMAME = 'HOME_AMENITIES';
    /** GTypes: Variable de Apoyo: SGMONVAL */
    private final static String SGMONVAL = 'MONEY_VALUES';
    /** GTypes: Variable de Apoyo: SGARTSCULP */
    private final static String SGARTSCULP = 'ART_SCULPTURE';
    /** GTypes: Variable de Apoyo: SGELECDEV */
    private final static String SGELECDEV = 'ELECTRONIC_DEVICES';
    /** GTypes: Variable de Apoyo: SGHOMAPL */
    private final static String SGHOMAPL = 'HOME_APPLIANCE';
    
    /** Variable de Apoyo: SINVFUR */
    private final static String SINVFUR = 'INVENTORY_AND_FURNITURE';
    /** Variable de Apoyo: SCOVFIL */
    private final static String SCOVFIL = 'FIRE_LIGHTNING';
    /** Variable de Apoyo: SCOVDER */
    private final static String SCOVDER = 'DEBRIS_REMOVAL';
    /** Variable de Apoyo: SCOVEXE */
    private final static String SCOVEXE = 'EXTRAORDINARY_EXPENSES';
    /** Variable de Apoyo: SCOVEAVE */
    private final static String SCOVEAVE = 'EARTHQUAKE_VOLCANIC_ERUPTION';
    /** Variable de Apoyo: SCOVHIRISK */
    private final static String SCOVHIRISK = 'HYDROMETEOROLOGICAL_RISKS';
    /** Variable de Apoyo: SCOVFAMCIVLI */
    private final static String SCOVFAMCIVLI = 'FAMILY_CIVIL_LIABILITY';
    /** Variable de Apoyo: SCOVFAMCIVLILE */
    private final static String SCOVFAMCIVLILE = 'CIVIL_LIABILITY_LESSEE';
    /** Variable de Apoyo: SCOVGLASSBRE */
    private final static String SCOVGLASSBRE = 'GLASS_BREAKAGE';
    /** Variable de Apoyo: SCOVHOMBUR */
    private final static String SCOVHOMBUR = 'HOME_BURGLARY';
    /** Variable de Apoyo: SCOVBURWVIO */
    private final static String SCOVBURWVIO = 'BURGLARY_WITH_VIOLENCE';
    /** Variable de Apoyo: SCOVTHEWART */
    private final static String SCOVTHEWART = 'THEFT_WORKS_ART';
    /** Variable de Apoyo: SCOVELECTEQU */
    private final static String SCOVELECTEQU = 'ELECTRONIC_EQUIPMENT';
    /** Variable de Apoyo: SCOVAPPEQUIP */
    private final static String SCOVAPPEQUIP = 'APPLIANCE_EQUIPMENT';
    /** Variable de Apoyo: FLDCOMPSTR */
    private final static List<String> FLDCOMPSTR = new List<String>{'Completa'};
    /** Variable de Apoyo: FLDCOMPSTR */
    private final static List<String> LSTBALANCE = new List<String>{'Balanceada'};
    /** Variable de Apoyo: FLDCOMPSTR */
    private final static List<String> LSTBASIC = new List<String>{'Basica'};
    /** Variable de Apoyo: FLDRENSTR */
    private final static List<String> FLDRENSTR = new List<String>{'Rentado'};
    
    /** Variable de Apoyo: FLD001ID */
    private final static String FLD001ID = '001';
    /** Variable de Apoyo: FLD002ID */
    private final static String FLD002ID = '002';
    /** Variable de Apoyo: FLD006ID */
    private final static String FLD006ID = '006';
    /** Variable de Apoyo: FLD007ID */
    private final static String FLD007ID = '007';
    /** Variable de Apoyo: FLD004ID */
    private final static String FLD004ID = '004';
    
    /** Variable de Apoyo: FLDCRITSTR */
    private final static String FLDCRITSTR = 'criterial';
    /** Variable de Apoyo: FLDIDDATASTR */
    private final static String FLDIDDATASTR = 'id_data';
    /** Variable de Apoyo: FLDVALSTR */
    private final static String FLDVALSTR = 'value';
    /** Variable de Apoyo: DP2008INDEDI */
    private final static String DP2008INDEDI = '2008INDEDIFICIO';
    /** Variable de Apoyo: DP2008INDARRE */
    private final static String DP2008INDARRE = '2008INDARRENDADO';
    /** Variable de Apoyo: DP2008PORSAIC */
    private final static String DP2008PORSAIC = '2008PORCSAIC';
    /** Variable de Apoyo: DP2008PORELED */
    private final static String DP2008PORELED = '2008PORCELECTROD';
    /** Variable de Apoyo: DP2008PORQELE */
    private final static String DP2008PORQELE = '2008PORCEQELEC';
    /** Variable de Apoyo: DP2008PORSARC */
    private final static String DP2008PORSARC = '2008PORCSARC';

    /** Variable de Apoyo: SSEPTRADE */
    private final static String SSEPTRADE = '|';
    /** Variable de Apoyo: SSEPCAT */
    private final static String SSEPCAT = '#';
    /** Variable de Apoyo: SSEPGT */
    private final static String SSEPGT = '@';
    /** Variable de Apoyo: SSEPGTREG */
    private final static String SSEPGTREG = '$';
    /** Variable de Apoyo: SSEPCOV */
    private final static String SSEPCOV = '*';
    /** Variable de Apoyo: SSEPCOVREG */
    private final static String SSEPCOVREG = ',';
    /** Variable de Apoyo: SSEPCOVREG */
    private final static String STRCOTIZN = 'strCotiz';
    
    /**
     * @description : Obtiene el tipo de JSON para la Cotización
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    public static Map<String, Object> fillDataCotiz(Map<String, Object> mParamsM, List<Object> lstDatPart) {
        final Map<String, Object> objResponseVals = new Map<String, Object>();
        objResponseVals.put('DP', (lstDatPart.isEmpty() ? obtDatPartInit(mParamsM) : obtDatPartUpd(obtDatPartInit(mParamsM), lstDatPart, mParamsM)));
        final String strTypeCotiz = mParamsM.get('strType').toString();
        switch on strTypeCotiz {
            when 'Outbound' {
                switch on String.valueOf(mParamsM.get(STRCOTIZN)) {
                    when 'Completa' {
                        objResponseVals.put('CO', obtCovsOutComp());
                    }
                    when 'Balanceada' {
                        objResponseVals.put('CO', obtCovsOutBal());
                    }
                    when 'Basica' {
                        objResponseVals.put('CO', obtCovsOutBas());
                    }
                    when 'Rentado' {
                        objResponseVals.put('CO', obtCovsOutBal());
                    }
                }
            }
            when 'Facebook', 'Call me back', 'Tracking Web', 'Call me back plus' {
                switch on String.valueOf(mParamsM.get(STRCOTIZN)) {
                    when 'Completa' {
                        objResponseVals.put('CO', obtCovsOutComp());
                    }
                    when 'Rentado' {
                        objResponseVals.put('CO', obtCovsOutBal());
                    }
                }
            }
        }
        return objResponseVals;
    }
    
    /**
     * @description : Llena una Lista de Mapas, establecen datos particulares de la cotización
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    public static List<Object> obtDatPartInit(Map<String, Object> mParamsM) {
        final String strTypeCotiz = mParamsM.get('strType').toString();
        final List<Object> lstDatPart = obtDatPartBase(mParamsM);
        switch on strTypeCotiz {
            when 'Outbound' {
                final List<String> lstValsOut = new List<String>();
                lstValsOut.addAll(LSTBASIC);
                lstValsOut.addAll(LSTBALANCE);
                lstValsOut.addAll(FLDCOMPSTR);
                if(lstValsOut.contains(String.valueOf(mParamsM.get(STRCOTIZN)))) {
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008INDEDI, FLDIDDATASTR => FLD002ID, FLDVALSTR => '1'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008INDARRE, FLDIDDATASTR => FLD001ID, FLDVALSTR => '0'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008PORSAIC, FLDIDDATASTR => FLD006ID, FLDVALSTR => '.5'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008PORELED, FLDIDDATASTR => FLD006ID, FLDVALSTR => '.5'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008PORQELE, FLDIDDATASTR => FLD006ID, FLDVALSTR => '.5'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008PORSARC, FLDIDDATASTR => FLD007ID, FLDVALSTR => '1'});
                } else {
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008INDEDI, FLDIDDATASTR => FLD001ID, FLDVALSTR => '0'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008INDARRE, FLDIDDATASTR => FLD002ID, FLDVALSTR => '1'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008PORSAIC, FLDIDDATASTR => '011', FLDVALSTR => '1'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008PORELED, FLDIDDATASTR => FLD006ID, FLDVALSTR => '.5'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008PORQELE, FLDIDDATASTR => FLD006ID, FLDVALSTR => '0.5'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008PORCRCARREN', FLDIDDATASTR => FLD002ID, FLDVALSTR => '.5'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008PORSARC, FLDIDDATASTR => FLD007ID, FLDVALSTR => '1'});
                }
            }
            when 'Facebook', 'Call me back', 'Tracking Web', 'Call me back plus' {
                if(FLDCOMPSTR.contains(String.valueOf(mParamsM.get(STRCOTIZN)))) {
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008INDEDI, FLDIDDATASTR => FLD002ID, FLDVALSTR => '1'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008INDARRE, FLDIDDATASTR => FLD001ID, FLDVALSTR => '0'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008PORSAIC, FLDIDDATASTR => FLD006ID, FLDVALSTR => '.5'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008PORELED, FLDIDDATASTR => FLD004ID, FLDVALSTR => '.5'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008PORQELE, FLDIDDATASTR => FLD004ID, FLDVALSTR => '.5'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008PORSARC, FLDIDDATASTR => FLD007ID, FLDVALSTR => '1'});
                } else if(FLDRENSTR.contains(String.valueOf(mParamsM.get(STRCOTIZN)))) {
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008INDEDI, FLDIDDATASTR => FLD001ID, FLDVALSTR => '0'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008INDARRE, FLDIDDATASTR => FLD002ID, FLDVALSTR => '1'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008PORSAIC, FLDIDDATASTR => '011', FLDVALSTR => '1'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008PORELED, FLDIDDATASTR => FLD006ID, FLDVALSTR => '.5'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008PORQELE, FLDIDDATASTR => FLD006ID, FLDVALSTR => '.5'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => DP2008PORSARC, FLDIDDATASTR => FLD007ID, FLDVALSTR => '1'});
                    lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008PORCRCARREN', FLDIDDATASTR => FLD002ID, FLDVALSTR => '.5'});
                }
            }
        }
        return lstDatPart;
    }

    /**
     * @description : Llena una Lista de Mapas, establecen datos particulares base cotización
     * @author      : Alexandro Corzo
     * @return      : MList<Object> lstDatPart
     */
    public static List<Object> obtDatPartBase(Map<String, Object> mParamsM) {
        final String strSumAse = mParamsM.get('strSumAse').toString();
        final String metrosBase = mParamsM.get('metrosCuadrados').toString();
        final List<Object> lstDatPart = new List<Object>();
        lstDatPart.add(new Map<String, String>{FLDCRITSTR => '2008SA', FLDIDDATASTR => FLD001ID, FLDVALSTR => strSumAse});
        lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008SABASE', FLDIDDATASTR => FLD001ID, FLDVALSTR => strSumAse});
        lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008INTIPOHAB', FLDIDDATASTR => FLD002ID, FLDVALSTR => 'CS'});
        lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008ESPISOS', FLDIDDATASTR => FLD001ID, FLDVALSTR => '2'});
        lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008METROSCONST', FLDIDDATASTR => FLD001ID, FLDVALSTR => metrosBase});
        lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008METROSTERR', FLDIDDATASTR => FLD001ID, FLDVALSTR => metrosBase});
        lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008ANTIGUEDAD', FLDIDDATASTR => FLD001ID, FLDVALSTR => '0 a 3 anos'});
		lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008RIESGOHIDRO', FLDIDDATASTR => FLD002ID, FLDVALSTR => 'N'});  
		lstDatPart.add(new map<String, String>{FLDCRITSTR => 'CODIGO_CUPON', FLDIDDATASTR => FLD001ID, FLDVALSTR => 'NA'});
        lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008ESNIVELINMUEBLE', FLDIDDATASTR => FLD001ID, FLDVALSTR => '2'});
        lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008ASCIBERNETICA', FLDIDDATASTR => FLD001ID, FLDVALSTR => '0'});
        lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008ASHANDYMAN', FLDIDDATASTR => FLD001ID, FLDVALSTR => '0'});
        lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008PORCCRISTAL', FLDIDDATASTR => '015', FLDVALSTR => '.15'});
        lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008PORCMENCASA', FLDIDDATASTR => FLD006ID, FLDVALSTR => '.5'});
        lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008PORCOBJPERS', FLDIDDATASTR => FLD004ID, FLDVALSTR => '.3'});
        lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008PORCDINVAL', FLDIDDATASTR => FLD004ID, FLDVALSTR => '.03'});
        lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008ASHOGAR', FLDIDDATASTR => FLD002ID, FLDVALSTR => '120'});
        lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008ASMASCOTAS', FLDIDDATASTR => FLD002ID, FLDVALSTR => '240'});
        lstDatPart.add(new map<String, String>{FLDCRITSTR => '2008ASMEDICA', FLDIDDATASTR => FLD002ID, FLDVALSTR => '120'});
        return lstDatPart;
    }
    
    /**
     * @description : Actualiza la lista de datos particulares de la cotización
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    public static List<Object> obtDatPartUpd(List<Object> lstDatPart, List<Object> lstDatPartUpd, Map<String, Object> mParamsM) {
        final List<Object> lstDatPartM = new List<Object>();
        Boolean isModCrit = false;
        for(Object oRowData : lstDatPart) {
        	final Map<String, String> mDataPart = (Map<String, String>) oRowData;
            isModCrit = false;
            for(Object oRowDataNew : lstDatPartUpd) {
                final Map<String, String> mDataPartN = (Map<String, String>) oRowDataNew;
                if(mDataPart.get(FLDCRITSTR).equals(mDataPartN.get(FLDCRITSTR))) {
                    if(FLDRENSTR.contains(String.valueOf(mParamsM.get(STRCOTIZN)))) {
                        if(mDataPart.get(FLDCRITSTR).equalsIgnoreCase('2008PORCSAIC') == false) {
                            mDataPart.remove(FLDCRITSTR);
                            mDataPart.remove(FLDIDDATASTR);
                            mDataPart.remove(FLDVALSTR);
                            mDataPart.put(FLDCRITSTR, mDataPartN.get(FLDCRITSTR));
                            mDataPart.put(FLDIDDATASTR, mDataPartN.get(FLDIDDATASTR));
                            mDataPart.put(FLDVALSTR, mDataPartN.get(FLDVALSTR));
                            lstDatPartM.add(mDataPart);
                            isModCrit = true;
                        }
                        break;
                    } else {
                        mDataPart.put(FLDCRITSTR, mDataPartN.get(FLDCRITSTR));
                        mDataPart.put(FLDIDDATASTR, mDataPartN.get(FLDIDDATASTR));
                        mDataPart.put(FLDVALSTR, mDataPartN.get(FLDVALSTR));
                        lstDatPartM.add(mDataPart);
                        isModCrit = true;
                        break;
                    }
                }
            }
            if(!isModCrit) {
                lstDatPartM.add(mDataPart);
            }
        }
    	return lstDatPartM;
    }
    
    /**
     * @description : Arma la estructura necesaria para la cotización Propio - Completa
     * @author      : Alexandro Corzo
     * @return      : List<Object> oReturnValues
     */
    public static List<Object> obtCovsOutComp() {
        final List<String> lstCovs = new List<String>();
        lstCovs.add(SFIRE + SSEPTRADE + SCBUILDS + SSEPGT + SGTBUILD + SSEPCOV + SCOVFIL + SSEPCOVREG + SCOVDER + SSEPCOVREG + SCOVEXE + SSEPCAT + SINVFUR + SSEPGT + SGTCONTENT + SSEPCOV + SCOVFIL + SSEPCOVREG + SCOVDER + SSEPCOVREG + SCOVEXE);
        lstCovs.add(SEARTH + SSEPTRADE + SCBUILDS + SSEPGT + SGTBUILD + SSEPCOV + SCOVEAVE + SSEPCOVREG + SCOVDER + SSEPCOVREG + SCOVEXE + SSEPCAT + SINVFUR + SSEPGT + SGTCONTENT + SSEPCOV + SCOVEAVE + SSEPCOVREG + SCOVDER + SSEPCOVREG + SCOVEXE);
        lstCovs.add(SHIRISK + SSEPTRADE + SCBUILDS + SSEPGT + SGTBUILD + SSEPCOV + SCOVHIRISK + SSEPCOVREG + SCOVDER + SSEPCOVREG + SCOVEXE + SSEPCAT + SINVFUR + SSEPGT + SGTCONTENT + SSEPCOV + SCOVHIRISK + SSEPCOVREG + SCOVDER + SSEPCOVREG + SCOVEXE); 
        lstCovs.add(SCIVLIN + SSEPTRADE + SCCIVLI + SSEPGT + SGFAMCIVLI + SSEPCOV + SCOVFAMCIVLI);
        lstCovs.add(SMISC + SSEPTRADE + SCCRYSIG + SSEPGT + SGGLASSES + SSEPCOV + SCOVGLASSBRE + SSEPCAT + SCELECDEV + SSEPGT + SGHOMAME + SSEPCOV + SCOVHOMBUR + SSEPCAT + SCVALITE + SSEPGT + SGMONVAL + SSEPCOV + SCOVBURWVIO + SSEPCAT + SCPERITEM + SSEPGT + SGARTSCULP + SSEPCOV + SCOVTHEWART);
        lstCovs.add(STECH + SSEPTRADE + SCELECDEV + SSEPGT + SGELECDEV + SSEPCOV + SCOVELECTEQU + SSEPGTREG + SGHOMAPL + SSEPCOV + SCOVAPPEQUIP);
        return lstCovs;
    }
    
    /**
     * @description : Arma la estructura necesaria para la cotización Propio - Balanceada
     * @author      : Alexandro Corzo
     * @return      : List<Object> oReturnValues
     */
    public static List<Object> obtCovsOutBal() {
        final List<String> lstCovs = new List<String>();
        lstCovs.add(SFIRE + SSEPTRADE + SINVFUR + SSEPGT + SGTCONTENT + SSEPCOV + SCOVFIL + SSEPCOVREG + SCOVDER + SSEPCOVREG + SCOVEXE);
        lstCovs.add(SEARTH + SSEPTRADE + SINVFUR + SSEPGT + SGTCONTENT + SSEPCOV + SCOVEAVE + SSEPCOVREG + SCOVDER + SSEPCOVREG + SCOVEXE);
        lstCovs.add(SHIRISK + SSEPTRADE + SINVFUR + SSEPGT + SGTCONTENT + SSEPCOV + SCOVHIRISK + SSEPCOVREG + SCOVDER + SSEPCOVREG + SCOVEXE);
        lstCovs.add(SMISC + SSEPTRADE + SCELECDEV + SSEPGT + SGHOMAME + SSEPCOV + SCOVHOMBUR + SSEPCAT + SCVALITE + SSEPGT + SGMONVAL + SSEPCOV + SCOVBURWVIO + SSEPCAT + SCPERITEM + SSEPGT + SGARTSCULP + SSEPCOV + SCOVTHEWART + SSEPCAT + SCCRYSIG + SSEPGT + SGGLASSES + SSEPCOV + SCOVGLASSBRE);
        lstCovs.add(STECH + SSEPTRADE +  SCELECDEV + SSEPGT + SGELECDEV + SSEPCOV + SCOVELECTEQU + SSEPGTREG + SGHOMAPL + SSEPCOV + SCOVAPPEQUIP);
        lstCovs.add(SCIVLIN + SSEPTRADE + SCCIVLI + SSEPGT + SGFAMCIVLILE + SSEPCOV + SCOVFAMCIVLILE);
        return lstCovs;
    }
    
    /**
     * @description : Arma la estructura necesaria para la cotización Propio - Basica
     * @author      : Alexandro Corzo
     * @return      : List<Object> oReturnValues
     */
    public static List<Object> obtCovsOutBas() {
        final List<String> lstCovs = new List<String>();
        lstCovs.add(SFIRE + SSEPTRADE + SCBUILDS + SSEPGT + SGTBUILD + SSEPCOV + SCOVFIL + SSEPCOVREG + SCOVDER + SSEPCOVREG + SCOVEXE);
        lstCovs.add(SEARTH + SSEPTRADE + SCBUILDS + SSEPGT + SGTBUILD + SSEPCOV + SCOVEAVE + SSEPCOVREG + SCOVDER + SSEPCOVREG + SCOVEXE);
        lstCovs.add(SHIRISK + SSEPTRADE + SCBUILDS + SSEPGT + SGTBUILD + SSEPCOV + SCOVHIRISK + SSEPCOVREG + SCOVDER + SSEPCOVREG + SCOVEXE);
        lstCovs.add(SCIVLIN + SSEPTRADE + SCCIVLI + SSEPGT + SGFAMCIVLI + SSEPCOV + SCOVFAMCIVLI + SSEPGT + SGFAMCIVLILE + SSEPCOV + SCOVFAMCIVLILE);
        lstCovs.add(SMISC + SSEPTRADE + SCCRYSIG + SSEPGT + SGGLASSES + SSEPCOV + SCOVGLASSBRE);
        return lstCovs;
    }
}