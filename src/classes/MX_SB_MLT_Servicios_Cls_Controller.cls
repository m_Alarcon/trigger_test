/**
 * @File Name          : MX_SB_MLT_Servicios_Cls_Controller.cls
 * @Description        : 
 * @Author             : Daniel Perez Lopez
 * @Group              : 
 * @Last Modified On   : 2/17/2020, 6:40:57 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/17/2020   Daniel Perez Lopez           Initial Version
 * 1.1    2/17/2020     Marco Cruz             Corrección de Codesmells
**/
public with sharing class MX_SB_MLT_Servicios_Cls_Controller { //NOSONAR
    
    /* 
    * @var INSRT sustituye label insert 
    */
    final static String INSRT = 'insert'; 
    /* 
    * @var UPDT sustituye label update 
    */
    final static String UPDT = 'update';
    /* 
    * @var strAsesor sustituye label Asesor Robo 
    */
    final static String  STRASESOR = Label.MX_SB_MLT_AsesorRobo;
    /* 
    * @var STRAJUSTADOR sustituye label Ajustador 
    */
    final static String  STRAJUSTADOR = Label.MX_SB_MLT_Ajustador;
    /* 
    * @var TIPOROBO sustituye label Robo Parcial 
    */
    final static String  TIPOROBO = Label.MX_SB_MLT_RoboParcial;
    /* 
    * @var String nombreParam{} 
    */
    public static String nombreParam {get;set;}
    /*
    *@var List workorder{} 
    */
    public static List<WorkOrder> newServices {get;set;}
    
    
    /*
    * @description Crea los servicios de Proveedor
    * @param String idSiniestro, String idProveedor
    * @return String
    */
    public static String crearServicio(String idSiniestro, String idProveedor) {
        final Siniestro__c sini = [SELECT id,name,MX_SB_MLT_ProveedorRobo__c,RecordType.DeveloperName,TipoSiniestro__c, MX_SB_MLT_URLLocation__c,MX_SB_MLT_RequiereAsesor__c, MX_SB_MLT_ServicioAjAuto__c FROM Siniestro__c WHERE id=:idSiniestro];
        newServices = new List<WorkOrder>();
        List<WorkOrder> updateServices = new List<WorkOrder>();
        final Map<String, Account> mapCuentas = new Map<String, Account>();
        final Account[] cuentas = [SELECT id,Name FROM Account WHERE id in(:idProveedor)];
        for(Account cuentamapa :cuentas) {
            mapCuentas.put(cuentamapa.id,cuentamapa);
        }
        Final List<WorkOrder> siniServices = [SELECT id,	MX_SB_MLT_ProveedorRobo__c,MX_SB_MLT_ServicioAsignado__c,MX_SB_MLT_TipoServicioAsignado__c,MX_SB_MLT_Siniestro__c,createddate FROM WorkOrder WHERE MX_SB_MLT_Siniestro__c =:idSiniestro order by createddate desc];
        Map<String,List<WorkOrder>> mapaOrder = new Map<String,List<WorkOrder>>();
        mapaOrder = servicioAsesor(newServices,updateServices,siniServices,sini,mapCuentas,idProveedor,idSiniestro);
        newServices = mapaOrder.get(INSRT);
        updateServices = mapaOrder.get(UPDT);
        if(!newServices.isEmpty()) {
            insert newServices;
        }
        if(!updateServices.isEmpty()) {
            update updateServices;
        }
        return 'Exito test no Create';
    }
    
    /*
    * @description Crea los servicios de Proveedor
    * @param List<WorkOrder> newServices,List<WorkOrder> updateServices,List<WorkOrder> siniServices, Siniestro__c sini,Map<String
    * @return Map casos
    */
    public static Map<String,List<WorkOrder>> servicioAsesor(List<WorkOrder> newServices,List<WorkOrder> updateServices,List<WorkOrder> siniServices, Siniestro__c sini,Map<String,Account> mapCuentas,String idProveedor,String idSiniestro) {
        final Map<String,List<WorkOrder>> casosOut = new Map<String,List<WorkOrder>>();
        final List<WorkOrder> casosInR = newServices;
        final List<WorkOrder> casosUpR = updateServices;
        
        
        if(idProveedor!=null && sini.RecordType.DeveloperName=='MX_SB_MLT_RamoAuto'
            && sini.MX_SB_MLT_RequiereAsesor__c==true) {
                nombreParam = mapCuentas.get(idProveedor).Name;
                    if(sini.MX_SB_MLT_ServicioAjAuto__c ==false) {
                        final WorkOrder newService = new WorkOrder(RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_Servicios').getRecordTypeId());
                        newService.MX_SB_MLT_ProveedorRobo__c=nombreParam;
                        newService.MX_SB_MLT_ServicioAsignado__c= STRAJUSTADOR;
                        newService.MX_SB_MLT_Siniestro__c=idSiniestro;
                        newService.MX_SB_MLT_TipoServicioAsignado__c= STRASESOR;
                        newService.MX_SB_MLT_Ajustador__c= idProveedor;
                        casosInR.add(newService);    
                        update new Siniestro__c(id=sini.id ,MX_SB_MLT_ServicioAjAuto__c=true);
                    } else {
                            for(WorkOrder item :SiniServices) {
                                if(item.MX_SB_MLT_ServicioAsignado__c== STRAJUSTADOR && item.MX_SB_MLT_TipoServicioAsignado__c==STRASESOR) {
                                    item.MX_SB_MLT_ProveedorRobo__c=nombreParam;
                                    casosUpR.add(item);
                                break;
                                }
                            }
                    }
                
        }
        
        casosOut.put(INSRT,casosInR);
        casosOut.put(UPDT,casosUpR);
        return casosOut;
    }  
     /*
    * @description Crea los servicios de Robo 
    * @param List<WorkOrder> newServices,List<WorkOrder> updateServices,List<WorkOrder> siniServices, Siniestro__c sini,Map<String
    * @return Map casos
    */
     public static String crearServicioRobo(String idSiniestro, String idProveedor) {
        String respuesta = '';
        newServices = new List<WorkOrder>();
         String tipoAjustador = '';
       	// List<WorkOrder> updateServices = new List<WorkOrder>();
        //consulta de servicio ya creado 
        Final siniestro__c sin = [select id,MX_SB_MLT_ServicioAjAuto__c,MX_SB_MLT_JourneySin__c,MX_SB_MLT_TipoRobo__c from siniestro__c where id=:idSiniestro limit 1 ];
        sin.MX_SB_MLT_ServicioAjAuto__c=true;
        sin.MX_SB_MLT_JourneySin__c='Servicios Enviados';
		if(sin.MX_SB_MLT_TipoRobo__c == TIPOROBO) {
            tipoAjustador = STRAJUSTADOR;
        	} else {
                tipoAjustador = STRASESOR;
                }
         
        Final List<WorkOrder> servicioEnviado = [select id from WorkOrder where MX_SB_MLT_Siniestro__c= :idSiniestro and MX_SB_MLT_TipoServicioAsignado__c=:tipoAjustador and MX_SB_MLT_EstatusServicio__c NOT in('Cancelado') limit 1];
         if(servicioEnviado.isEmpty()) {
             final WorkOrder newService = new WorkOrder(RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_Servicios').getRecordTypeId());
                        newService.MX_SB_MLT_Ajustador__c =idProveedor;
                        newService.MX_SB_MLT_ServicioAsignado__c= STRAJUSTADOR;
                        newService.MX_SB_MLT_Siniestro__c=idSiniestro;
                        newService.MX_SB_MLT_TipoServicioAsignado__c= tipoAjustador;
         newServices.add(newService);
         insert newServices;
         update sin; new Siniestro__c(id=idSiniestro ,MX_SB_MLT_ServicioAjAuto__c=true,MX_SB_MLT_JourneySin__c='Servicios Enviados');
         respuesta = 'Reporte registrado exitosamente';
         } else {
             respuesta ='La ayuda ya habia sido enviada anteriormente';
         }
         return respuesta;

    }
}