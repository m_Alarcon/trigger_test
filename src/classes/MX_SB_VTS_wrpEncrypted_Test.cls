@isTest
/**
* @FileName          : MX_SB_VTS_wrpEncrypted_Test
* @description       : Wrapper class for use de web service of ProxyValueCipher
* @Author            : Marco Antonio Cruz Barboza  
* @last modified on  : 18-09-2020
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   18-09-2020   Marco Antonio Cruz Barboza          Initial Version
**/
public class MX_SB_VTS_wrpEncrypted_Test {
    
    /** User name testing */
    Final static String USERTTST = 'UserForTest';
    /** Account Name testing*/
    Final static String ACCTOTST = 'AccountForTest';
	
    /*
    @Method: setupTest
	@Description: setup for test class
	*/
    @TestSetup
    static void setupTest() {
        final User testToUsr = MX_WB_TestData_cls.crearUsuario(USERTTST, System.Label.MX_SB_VTS_ProfileAdmin);
        System.runAs(testToUsr) {
            final Account testToAcc = MX_WB_TestData_cls.crearCuenta(ACCTOTST, System.Label.MX_SB_VTS_PersonRecord);
            testToAcc.PersonEmail = 'pruebaVts@mxvts.com';
            insert testToAcc;
        }
    }
    
    /*
    @Method: MX_SB_VTS_wrpEncryptedTest
    @Description: Use a wrapper class to deserialize a response from a web service.
	@param List<String>
    */
    @isTest
    static void wrpEncryptedTest() {
        Final List<String> listTest = new List<String>{'test1, test2'};
        test.startTest();
        	final MX_SB_VTS_wrpEncrypted wrapperTest = new MX_SB_VTS_wrpEncrypted();
        	wrapperTest.results = listTest;
        	System.assertNotEquals(wrapperTest, null,'El wrapper esta retornando');
        test.stopTest();
    }
    
}