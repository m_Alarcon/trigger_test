/**
* @File Name          : MX_BPP_Oportunidad_Ctrl.cls
* @Description        :
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 16/10/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      16/10/2020        Gabriel García Rojas              Initial Version
**/
@SuppressWarnings()
public with sharing class MX_BPP_Oportunidad_Ctrl {
	 /**Constructor */
    @testVisible
    private MX_BPP_Oportunidad_Ctrl() { }

    /**
    * @description Obtiene estructura de jerarquía referente al usuario
    * @author Gabriel Garcia Rojas
    * @return
    **/
    @AuraEnabled
    public static MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper fetchInfoByUser() {
        return MX_BPP_LeadStart_Service.userInfo();
    }

    /**
    * @description Obtiene información del usuario
    * @author Gabriel Garcia Rojas
    * @return
    **/
    @AuraEnabled
    public static List<String> fetchUserInfo() {
        return MX_BPP_LeadStart_Service.fetchusdata();
    }

    /**
    * @description return Total of record by Division, Oficina or User
    * @author Gabriel Garcia Rojas
    * @param Integer numRecords
    * @return Map<String, Integer>
    **/
    @AuraEnabled
    public static Integer fetchPgs(Integer numRecords) {
        return MX_BPP_LeadStart_Service.fetchPgs(numRecords);
    }

    /**
    * @description retorna una lista de usuarios con respecto al titulo y la oficina
    * @author Gabriel Garcia Rojas
    * @param String oficina
    * @param String titulo
    * @return List<User>
    **/
    @AuraEnabled
	public static MX_BPP_Oportunidad_Service.WRP_ChartStacked fetchDataOpp(List<String> params, Datetime sDate, Datetime eDate) {
        return MX_BPP_Oportunidad_Service.fetchServiceDataOpp(params, sDate, eDate);

	}

    /**
    * @description retorna una lista oportunidaddes filtradas
    * @author Gabriel Garcia Rojas
    * @param String params [bkm, tyAcc, tyOpp, offset]
    * @param Datetime sDate
    * @param Datetime eDate
    * @return List<Opportunity>
    **/
    @AuraEnabled
	public static List<Opportunity> fetchOpp(List<String> params, Datetime sDate, Datetime eDate) {
        return MX_BPP_Oportunidad_Service.fetchOpp(params, sDate, eDate);

	}


}