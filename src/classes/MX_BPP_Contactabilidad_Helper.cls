/**
* @File Name          : MX_BPP_Contactabilidad_Helper.cls
* @Description        :
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 01/10/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      01/10/2020        Gabriel García Rojas              Initial Version
**/
@SuppressWarnings()
public class MX_BPP_Contactabilidad_Helper {

    /*Variable global que  reemplaza por Privado*/
    static final String VAR_PRD='PRIVADO';
    /*Variable global que reemplaza por Patrimonial*/
    static final String VAR_PATRIM = 'PATRIMONIAL';
    /*Variable global que reemplaza por Patrimonial Sr*/
    static final String VAR_PATRIM_SR='PATRIMONIAL SR';
    /*Variable global periodo patrimonial*/
    static final String PPA='Periodo Patrimonial';
    /*Variable global periodo patrimonial SR*/
    static final String PPS='Periodo Patrimonial SR';
    /*Variable global periodo patrimonial*/
    static final String PPR='Periodo Privado';
    /*Variable global que  reemplaza por Mensual*/
    static final String MENS='Mensual';
    /*Variable global que  reemplaza por Bimestral*/
    static final String BIM='Bimestral';
    /*Variable global que  reemplaza por Trimestral*/
    static final String TRI='Trimestral';
    /*Variable global Contactado*/
    static final String NCONTACT='Contact';
    /*Variable global NO Contactado*/
    public static final String NOCONTACT='NonContact';

    /**Constructor */
    @TestVisible
    private MX_BPP_Contactabilidad_Helper() { }

    /**
    * @description retorna la abreviación del periodo
    * @author Gabriel Garcia Rojas
    * @param String tittle
    * @return String
    **/
    public static String gtPeriodo(String tittle) {
        String nombrePeriodo;
        if(tittle == VAR_PATRIM) {
            nombrePeriodo = PPA;
        } else if(tittle == VAR_PATRIM_SR) {
            nombrePeriodo = PPS;
        } else if(tittle == VAR_PRD) {
            nombrePeriodo = PPR;
        }
        return nombrePeriodo;
    }

    /**
    * @description retorna el mes respecto al periodo y el tipo
    * @author Gabriel Garcia Rojas
    * @param String type
    * @param String periodo
    * @return Integer
    **/
    public static Integer retmonth(String type, String periodo) {
        final Integer month = system.today().month();
        final Map<String, List<Integer>> monthValues = new Map<String, List<Integer>>();
        if (periodo == MENS) {
            monthValues.put(MX_BPP_Contactabilidad_Service.STR_SM, new List<Integer>{1,2,3,4,5,6,7,8,9,10,11,12});
            monthValues.put(MX_BPP_Contactabilidad_Service.STR_EM, new List<Integer>{1,2,3,4,5,6,7,8,9,10,11,12});
        } else if (periodo == BIM) {
            monthValues.put(MX_BPP_Contactabilidad_Service.STR_SM, new List<Integer>{1,1,3,3,5,5,7,7,9,9,11,11});
            monthValues.put(MX_BPP_Contactabilidad_Service.STR_EM, new List<Integer>{2,2,4,4,6,6,8,8,10,10,12,12});
        } else if (periodo == TRI) {
            monthValues.put(MX_BPP_Contactabilidad_Service.STR_SM, new List<Integer>{1,1,1,4,4,4,7,7,7,10,10,10});
            monthValues.put(MX_BPP_Contactabilidad_Service.STR_EM, new List<Integer>{3,3,3,6,6,6,9,9,9,12,12,12});
        }
		return  monthValues.get(type)[month-1];
    }

    /**
    * @description retorna fecha con base al mes y tipo de ejecutivo
    * @author Gabriel Garcia Rojas
    * @param Integer month
    * @param String type
    * @return Datetime
    **/
    public static Datetime obtStAndEndDate(Integer month, String type) {
        Datetime dateToReturn = null;
        if(type == MX_BPP_Contactabilidad_Service.VAR_ST) {
            dateToReturn = datetime.newInstance(system.today().year(),month,1);
        } else if (type == MX_BPP_Contactabilidad_Service.VAR_EN) {
            final Integer numberOfDays = Date.daysInMonth(system.today().year(), month);
            dateToReturn = datetime.newInstance(system.today().year(),month,numberOfDays);
        }
        return dateToReturn;
    }

    /**
    * @description retorna un conjunto de cuentas con base a su tipo de visita
    * @author Gabriel Garcia Rojas
    * @param Account acc
    * @param String contactType
    * @return Set<Account>
    **/
    public static Set<Account> processAccounts(Account acc, String contactType) {
        final Set<Account> accSet = new Set<Account>();
        final Set<String> noption = new Set<String>(new List<String>{'05','06'});
        Boolean contactflag = false;
        for(dwp_kitv__Visit__c rv : acc.dwp_kitv__Visits__r) {
            if(noption.contains(rv.dwp_kitv__visit_status_type__c)) {
                if(NCONTACT.equals(contactType)) {
                    accSet.add(acc);
                }
                contactFlag=true;
            }
        }
        if(NOCONTACT.equals(contactType) && (!contactFlag || acc.dwp_kitv__Visits__r.isEmpty())) {
            accSet.add(acc);
        }
        return accSet;
    }


    /**
    * @description retorna un conjunto de ids de visitas
    * @author Gabriel Garcia Rojas
    * @param Account acc
    * @return Set<dwp_kitv__Visit__c>
    **/
    public static Set<Id> processAccounts(List<dwp_kitv__Visit__c> listVisit) {
        final Set<Id> setIds = new Set<Id>();
        for(dwp_kitv__Visit__c rv : listVisit) {
        	setIds.add(rv.dwp_kitv__account_id__c);
        }
        return setIds;
    }
}