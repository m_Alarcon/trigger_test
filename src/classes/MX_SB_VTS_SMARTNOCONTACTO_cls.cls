/**
*
* @author Arsenio Perez Lopez
* @description API to auto assign leads from Smart Center to call center agents
*
*           No  |     Date     |     Author      |    Description
* @version  1.0    24/06/2019     Arsenio Perez      Create API for Smart Center
* @version  1.0    27/08/2019     Arsenio Perez      Code smell
* @version  1.1    23/06/2020     Alexandro Corzo    Se agrego la relación de Traea con el objeto de Visita y se realizo el mapeo de campos
*                                                    para el rellenado de información.
*
*/
@RestResource(urlMapping='/leadNoContacto/*')
global without sharing class MX_SB_VTS_SMARTNOCONTACTO_cls {
    @TestVisible
    Private MX_SB_VTS_SMARTNOCONTACTO_cls() {

    }
    /**
     * Method: leadActionCont
     * @param LeadId: ID del lead que se va a trabajar
     * @param Tipo: Tipo (motivoNoContacto/Grabacion)
     * @param motivoNoContacto (String)
     * @param idGrabacion  (String)
     * @param certificada  (Verdadera/Falsa)
     */
    @HttpPost
    global static String leadActionCont() {
        String ret ='';
        final Map<String, Object> listener = (Map<String, Object>)JSON.deserializeUntyped(RestContext.request.requestBody.toString());
        final String leadId = String.valueOf(listener.get('LeadId'));
        final String callType = String.valueOf(listener.get('Tipo'));
        final String Tipotwo =leadId;
        final Boolean isLead = Tipotwo.startsWith('00Q');
        if(System.Label.MX_SB_VTS_TipoRegreso.equalsIgnoreCase(callType)) {
            final String idGrabacion = String.valueOf(listener.get('IDGrabacion'));
            ret = motivoNoContacto(leadId, idGrabacion, isLead, callType);
            final SObject resetRec = consiltaWonerRegistro(isLead,leadId);
            resetRec.put('MX_SB_VTS_Tipificacion_LV7__c','N/A');
            update resetRec;
        } else if('Grabacion'.equalsIgnoreCase(callType)) {
            ret = llamdaCertificacion(leadId, '','', isLead, callType);
        } else {
            ret = System.Label.MX_WB_LG_ErrorBack;
        }
        return ret;
    }

    /**
    * @Method:motivoNoContacto
    * @Param: iIDLead: id del lead a contactar
    * @param: motivoNoContacto de no contacto a relacionar con el lead
    */
    private static string motivoNoContacto(id iIDLead, String motivoNoContacto, Boolean isLead, String callType) {
        String retur = '';
        try {
            final SObject resetRec = consiltaWonerRegistro(isLead,iIDLead);
            resetRec.put('MX_SB_VTS_Tipificacion_LV7__c','');
            update resetRec;
            final SObject idOwner = consiltaWonerRegistro(isLead,iIDLead);
            retur = insertaTarea(iIDLead, isLead, motivoNoContacto, callType);
            if(String.isBlank(String.valueOf(IDowner.get('MX_SB_VTS_ContadorLlamadasTotales__c')))) {
                idOwner.put('MX_SB_VTS_ContadorLlamadasTotales__c',1);
            } else {
                idOwner.put('MX_SB_VTS_ContadorLlamadasTotales__c',Integer.valueOf(idOwner.get('MX_SB_VTS_ContadorLlamadasTotales__c'))+1);
            }
            if(isLead) {
                idOwner.put('Resultadollamada__c',System.Label.MX_SB_VTS_Nocontacto);
            } else {
                idOwner.put('MX_SB_VTS_Tipificacion_LV1__c',System.Label.MX_SB_VTS_Nocontacto);
            }
            idOwner.put('MX_SB_VTS_Tipificacion_LV2__c',System.Label.MX_SB_VTS_Nocontacto);
            idOwner.put('MX_SB_VTS_Tipificacion_LV3__c','No Aplica');
            idOwner.put('MX_SB_VTS_Tipificacion_LV4__c','No Aplica');
            idOwner.put('MX_SB_VTS_Tipificacion_LV5__c','No Venta (No contacto)');
            idOwner.put('MX_SB_VTS_Tipificacion_LV6__c',motivoNoContacto);
            database.update(idOwner);

            final dwp_kitv__Visit__c finalVisit = createVisit(iIDLead, isLead);
            insert finalVisit;
            final Task updateTask = updateTaskRecord(retur, finalVisit.Id);
            update updateTask;  
        } catch (DmlException e) {
            retur = System.Label.MX_WB_LG_ErrorBack;
            throw new DmlException(System.Label.MX_WB_LG_ErrorBack + e);
        }
        return retur;
    }

    /*
     * @Method:llamdaCertificacion
     * @Param: iIDLead: id del lead a contactar
     * @param: idGrabacion de no contacto a relacionar con el lead
     * @param: certificada de no contacto a relacionar con el lead
     */
    @TestVisible
    private static string llamdaCertificacion(Id iIDLead, String idGrabacion, String certificada, Boolean isLead, String callType) {
        String retur = '';
        try {
            if(String.isNotEmpty(certificada)) {
                final Task act=[Select id from Task where MX_WB_idGrabacion__c=:idGrabacion];
                act.MX_SB_VTS_certificada__c=certificada;
                update act;
                retur= act.id;
            } else {
                Datetime tiepo = datetime.now();
                tiepo = tiepo.addDays(1);
                retur=insertaTarea(iIDLead, isLead, idGrabacion, callType);
            }
        } catch (DmlException e) {
            retur = System.Label.MX_WB_LG_ErrorBack;
            throw new DmlException(System.Label.MX_WB_LG_ErrorBack + e);
        }
        return retur;
    }

    /**
    * @Method:InsertaTarea
    * @Param: owner: Id del dueño de la tarea
    * @Param: datosHora: Horario de ls siguiente llamda
    * @Param: comentarios: Comentarios del no contact
    * @Param: idOp: Id del sObject que se envie LEAD / Oportunidad
    * @Param: isLead: Valida si es un Lead o Una oportunidad
    */
    private static String insertaTarea(String idOp, Boolean isLead,String idGrabacion, String callType) {
        try {
            final Task objTask = new Task();
            final User managerId = [Select ManagerId, Name from User where Id =: UserInfo.getUserId()];
            objTask.Description = 'Llamada identificada por el marcador';
            objTask.Subject = 'No Contacto Automatico';
            objTask.MX_WB_fechaGrabacionLlamada__c = System.now();
            objTask.Status = 'Completada';
            objTask.MX_SB_VTS_AgendadoPor__c = managerId.Name;
            if(isLead) {
                objTask.WhoId = idOp;
            } else {
                objTask.WhatId = idOp;
            }
            if(String.isNotEmpty(idGrabacion) && System.Label.MX_SB_VTS_TipoRegreso.equalsIgnoreCase(callType)) {
                objTask.MX_SB_VTS_TipificacionNivel6__c= idGrabacion;
                objTask.Motivos_efectivo__c = 'No Aplica';
                objTask.Resultado_llamada__c = System.Label.MX_SB_VTS_Nocontacto;
                objTask.Tipo_de_Contacto__c = System.Label.MX_SB_VTS_Nocontacto;
            }
            insert objTask;
            return objTask.Id;
        } catch(DMLException ex) {
            system.debug('ex: '+ex);
            throw new DmlException(Label.MX_WB_lbl_ReagendamientoException + ex);
        }
    }

    /**
    * @Method:consiltaWonerRegistro
    * @Param: isLead: TIpo de Objeto
    * @Param: iDs: Id del dueño de la tarea
    */
    private static SObject consiltaWonerRegistro(Boolean isLead, String iDs) {
        try {
            final String Obj =isLead ? 'LEAD':'OPPORTUNITY';
            final String tel =isLead ? 'MX_WB_ph_Telefono1__c,MX_WB_ph_Telefono2__c,MX_WB_ph_Telefono3__c':'TelefonoCliente__c';
            final String isds = iDs;//NOSONAR
            final String QUery = 'Select Id, MX_SB_VTS_ContadorLlamadasTotales__c, OwnerId,'+
                tel+' from '+Obj +' where id =:isds';
            return Database.Query(String.escapeSingleQuotes(QUery));
        } catch(DmlException ex) {
            throw new DmlException(Label.MX_WB_lbl_ReagendamientoException + ex);
        }
    }

    /**
     * @method: createVisit
     * @params: Se proporciona una lista de parametros del tipo List<String>
     * @description: Obtiene y almacena los valores para el objeto dwp_kitv__Visit_c
     */
    private static dwp_kitv__Visit__c createVisit(Id idRecord, Boolean isLead) {
        dwp_kitv__Visit__c newVisit = null;
        SObject recordObject = null;
        SObject recordUser = null;
        if (Boolean.valueOf(isLead)) {
            Lead recordLead = null;
            recordLead = [SELECT Id, CreatedById FROM Lead WHERE Id =: idRecord];
        	recordUser = [SELECT ManagerId, Name, Id FROM User WHERE Id =: recordLead.CreatedById];
            recordObject = getRecordLeadOppoToVisit(idRecord, isLead);
            newVisit = fillDataLeadToVisit(recordObject, recordUser, idRecord);
        } else {
            Opportunity recordOpportunity = null;
            recordOpportunity = [SELECT Id, CreatedById FROM Opportunity WHERE Id =: idRecord];
        	recordUser = [SELECT ManagerId, Name FROM User WHERE Id =: recordOpportunity.CreatedById];
            recordObject = getRecordLeadOppoToVisit(idRecord, isLead);
            newVisit = fillDataOpportunityToVisit(recordObject, recordUser, idRecord);
        }
        return newVisit;
    }

    /**
     * @method: getRecordLeadOppoToVisit
     * @param: Se proprciona un String que contiene el Id del registro: recordId - Obtiene el Lead / Oportunidad conforme al Id
     * @description: Obtiene la información almacenada en el objeto Lead / Oportunidad conforme al Id de registro proporcionado. 
     *               Los datos obtenidos serviran para llenar el objeto dwp_kitv__Visit__c
     */
    private static SObject getRecordLeadOppoToVisit(String recordId, Boolean isLead) {
        SObject oData;
        if (Boolean.valueOf(isLead)) {
            final  Lead oDataTipObj = [SELECT Id, MX_SB_VTS_Tipificacion_LV2__c, MX_SB_VTS_Tipificacion_LV3__c, MX_SB_VTS_Tipificacion_LV4__c, MX_SB_VTS_Tipificacion_LV5__c, MX_SB_VTS_Tipificacion_LV6__c, MX_SB_VTS_Tipificacion_LV7__c FROM Lead WHERE Id =: recordId];
			upsert oDataTipObj;
            oData = [SELECT Id, LeadSource, Producto_Interes__c, Resultadollamada__c, MX_SB_VTS_Tipificacion_LV2__c, MX_SB_VTS_Tipificacion_LV3__c, MX_SB_VTS_Tipificacion_LV4__c, MX_SB_VTS_Tipificacion_LV5__c, MX_SB_VTS_Tipificacion_LV6__c, MX_SB_VTS_Tipificacion_LV7__c, MX_SB_VTS_Fue_contacto_efectivo__c, MX_SB_VTS_CodCampaFace__c, Status, MX_SB_VTS_CalificacionSolicitud__c  FROM Lead WHERE Id =: recordId];            
        } else {
            final Opportunity oDataTipObj = [SELECT Id, MX_SB_VTS_Tipificacion_LV1__c, MX_SB_VTS_Tipificacion_LV2__c, MX_SB_VTS_Tipificacion_LV3__c, MX_SB_VTS_Tipificacion_LV4__c, MX_SB_VTS_Tipificacion_LV5__c, MX_SB_VTS_Tipificacion_LV6__c, MX_SB_VTS_Tipificacion_LV7__c FROM Opportunity WHERE Id =: recordId];
            upsert oDataTipObj;
            oData = [SELECT Id, Leadsource, Producto__c, MX_SB_VTS_Tipificacion_LV1__c, MX_SB_VTS_Tipificacion_LV2__c, MX_SB_VTS_Tipificacion_LV3__c, MX_SB_VTS_Tipificacion_LV4__c, MX_SB_VTS_Tipificacion_LV5__c, MX_SB_VTS_Tipificacion_LV6__c, MX_SB_VTS_Tipificacion_LV7__c, MX_WB_Cupon__c, StageName, MX_SB_VTA_Motivo_de_Asistencia__c, MX_SB_VTS_CalificacionSolicitudOp__c  FROM Opportunity WHERE Id =: recordId];
        }
    	return oData;
    }

    /**
     * @method: updateTaskRecord
     * @param: Se proporcionan los identificadores para poder relacionar el objeto de Tarea con el Objeto de Visita: idTask & idVisit
     * @description: Se requiere realizar la actualizacion del Id del objeto de visita creado en el objeto de Tarea, ya que deben estar relacionados
     */
    private static Task updateTaskRecord(String idTask, String idVisit) {
        Task recordTask = null;
        recordTask = new Task();
        recordTask.Id = idTask;
        recordTask.dwp_kitv__visit_id__c = idVisit;
        return recordTask;
    }

    /**
     * @method: fillDataLeadToVisit
     * @param: Se proporcionan objetos que sirven para realizar el llenado del objeto dwp_kitv__Visit__c: record, recordUser, idLead
     * @description: Se encarga de llevar el objeto dwp_kitv__Visit__c, 
     *               ya que es requerido para cuando se consume el registro y se crea una tarea
     *               Lead a Visita
     */
    private static dwp_kitv__Visit__c fillDataLeadToVisit(SObject record, SObject recordUsr, String idLead) {
    	Lead recordLead = null;
        dwp_kitv__Visit__c recordVisit = null;
        User recordUser = null;
        String sNoContacto = '';
        sNoContacto = 'No Contacto';
        recordLead = (Lead) record;
        recordUser = (User) recordUsr;
        String sRecordType = null;
        sRecordType = 'Tipificaciónes de Ventas TLMKT';
        final Recordtype recordType = [SELECT Id FROM Recordtype WHERE Name =: sRecordType];
        recordVisit = new dwp_kitv__Visit__c();
		recordVisit.MX_SB_VTS_OrigenCandidato__c = String.valueOf(recordLead.LeadSource);
        recordVisit.MX_SB_VTS_Producto__c = recordLead.Producto_Interes__c;
        recordVisit.MX_SB_VTS_Contacto__c = sNoContacto;
        recordVisit.MX_SB_VTS_ContactoEfectivo__c = recordLead.MX_SB_VTS_Tipificacion_LV2__c;
        recordVisit.MX_SB_VTS_Interesado__c = sNoContacto;
        recordVisit.MX_SB_VTS_AceptaCotizacionContratacion__c = recordLead.MX_SB_VTS_Tipificacion_LV4__c;
        recordVisit.MX_SB_VTS_Venta__c = String.valueOf(recordLead.MX_SB_VTS_Tipificacion_LV5__c);
        recordVisit.MX_SB_VTS_TipificacionFinal__c = recordLead.MX_SB_VTS_Tipificacion_LV6__c;
        recordVisit.MX_SB_VTS_IsContactEfect__c = recordLead.MX_SB_VTS_Fue_contacto_efectivo__c;
        recordVisit.MX_SB_VTS_Cupon__c = recordLead.MX_SB_VTS_CodCampaFace__c;
		recordVisit.MX_SB_VTS_EtapaOportunidad__c = recordLead.Status;
		recordVisit.MX_SB_VTS_MotivoAsistencia__c = '';
		recordVisit.MX_SB_VTS_EstadoLlamada__c = 'Completado';
		recordVisit.MX_SB_VTS_Asignado__c = recordUser.Id;
		recordVisit.dwp_kitv__lead_id__c = idLead;
        recordVisit.MX_SB_VTS_CalificacionSolicitudVis__c = recordLead.MX_SB_VTS_CalificacionSolicitud__c;
		recordVisit.dwp_kitv__visit_start_date__c = System.now();
        recordVisit.dwp_kitv__visit_duration_number__c = '15';
        recordVisit.RecordTypeId = recordType.Id;
        return recordVisit;
    }

    /**
     * @method: fillDataOpportunityToVisit
     * @param: Se proporcionan objetos que sirven para realizar el llenado del objeto dwp_kitv__Visit__c: record, recordUser, idOpportunity
     * @description: Se encarga de llevar el objeto dwp_kitv__Visit__c, 
     *               ya que es requerido para cuando se consume el registro y se crea una tarea
     *               Opportunity a Visita
     */
    private static dwp_kitv__Visit__c fillDataOpportunityToVisit(SObject record, SObject recordUsr, String idOpportunity) {
        Opportunity recordOpportunity = null;
        dwp_kitv__Visit__c recordVisit = null;
        User recordUser = null;
        String sNoContacto = '';
        sNoContacto = 'No Contacto';
        recordOpportunity = (Opportunity) record;
        recordUser = (User) recordUsr;
        String sRecordType = null;
        sRecordType = 'Tipificaciónes de Ventas TLMKT';
        final Recordtype recordType = [SELECT Id FROM Recordtype WHERE Name =: sRecordType];
        recordVisit = new dwp_kitv__Visit__c();
        recordVisit.MX_SB_VTS_OrigenCandidato__c = String.valueOf(recordOpportunity.LeadSource);
        recordVisit.MX_SB_VTS_Producto__c = recordOpportunity.Producto__c;
        recordVisit.MX_SB_VTS_Contacto__c = sNoContacto;
        recordVisit.MX_SB_VTS_ContactoEfectivo__c = recordOpportunity.MX_SB_VTS_Tipificacion_LV2__c;
        recordVisit.MX_SB_VTS_Interesado__c = sNoContacto;
        recordVisit.MX_SB_VTS_AceptaCotizacionContratacion__c = recordOpportunity.MX_SB_VTS_Tipificacion_LV4__c;
        recordVisit.MX_SB_VTS_Venta__c = String.valueOf(recordOpportunity.MX_SB_VTS_Tipificacion_LV5__c);
        recordVisit.MX_SB_VTS_TipificacionFinal__c = recordOpportunity.MX_SB_VTS_Tipificacion_LV6__c;
        recordVisit.MX_SB_VTS_Cupon__c = recordOpportunity.MX_WB_Cupon__c;
        recordVisit.MX_SB_VTS_EtapaOportunidad__c = recordOpportunity.StageName;
        recordVisit.MX_SB_VTS_MotivoAsistencia__c = recordOpportunity.MX_SB_VTA_Motivo_de_Asistencia__c;
        recordVisit.MX_SB_VTS_EstadoLlamada__c = 'Completado';
        recordVisit.MX_SB_VTS_Asignado__c = recordUser.Id;
        recordVisit.MX_SB_VTS_Opportunity__c = idOpportunity;
        recordVisit.MX_SB_VTS_CalificacionSolicitudVis__c = recordOpportunity.MX_SB_VTS_CalificacionSolicitudOp__c;
        recordVisit.dwp_kitv__visit_start_date__c = System.now();
        recordVisit.dwp_kitv__visit_duration_number__c = '15';
        recordVisit.RecordTypeId = recordType.Id;
        return recordVisit;
    }
}