/*
----------------------------------------------------------
* Nombre: MX_SB_PS_MethodPago_test
* Daniel Perez Lopez
* Proyecto: Salesforce-Presuscritos
* Descripción : clase test para clase MX_SB_PS_MethodPago_cls

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   Descripción
* --------------------------------------------------------------------------------
* 1.0           08/07/2020     Daniel Perez		        Creación
* 1.1           07/02/2021     Daniel Perez		        Methodo para busqueda codeIvr
* 1.2           02/24/2021     Juan Benitez             Methodo test reqOpenpay
* --------------------------------------------------------------------------------
*/
@SuppressWarnings('sf:UseSingleton')
@IsTest
public with sharing class MX_SB_PS_MetodoPago_test {

    /*
    * @description Variable declarada para insertar oportunidades
    */
    public static Final String RTOP='MX_SB_COB_Cotizador_RS_Hospital';

    /*
    * @description Variable declarada para buscar cuentas
    */
    public static Final String DEVNAME='PersonAccount';
    /*
    * @description Variable declarada para etapa de oportunidades
    */
    public static Final String ETAPA='Métodos de Pago';
    /*
    * @description Variable declarada para validar assertequals
    */
    public static Final String VALIDACION='Correcto';
    /**@description Name*/
    private final static string NAME = 'Cotización';
    /*
    * @description Method setup
    * @param Opportunidad opp
    */
    
    @TestSetup
    public static void creadatatest() {
        MX_SB_PS_OpportunityTrigger_test.makeData();
        final Account acctst = [Select Id from Account limit 1];
        acctst.Firstname='op';
        acctst.lastname='test';
        acctst.recuperacion__c=false;
        acctst.Tipo_Persona__c='Física';
        update acctst;
        Final Opportunity oppTestB =[Select id from Opportunity where AccountId=:acctst.Id limit 1]; 
		oppTestB.StageName=System.Label.MX_SB_PS_Etapa1;
        oppTestB.Name= 'NAME';
        update oppTestB;
        Final Quote quotData= [Select Id from Quote where OpportunityId=:oppTestB.Id limit 1];
		quotData.Name='123456789';
        quotData.MX_SB_VTS_Folio_Cotizacion__c='092939';
        update quotData;
        Final Task tskd= new Task(Description='', whatId=oppTestB.Id);
        insert tskd;
        Final MX_SB_VTS_Generica__c genOpenpay= new MX_SB_VTS_Generica__c(Name='OpenPayPresuscritos',
                                                MX_SB_VTS_Type__c='OP',MX_SB_VTS_HREF__c='123');
        insert genOpenpay;
    }

    @isTest static void testupdateop() {
        final opportunity opt= [SELECT Id,Name from opportunity where StageName =:System.Label.MX_SB_PS_Etapa1 limit 1];
        opt.Name='Opp1';
    Test.startTest();
        MX_SB_PS_MetodoPago_Ctrl.actualizaoportunidad(opt);
        System.assertEquals('Opp1',opt.Name,VALIDACION);
    Test.stopTest();
    }
    @isTest static void testGetTask() {
        final opportunity opot= [SELECT Id,name from opportunity where StageName =:System.Label.MX_SB_PS_Etapa1 limit 1];
        test.startTest();
        	Final List<Task> tsk= MX_SB_PS_MetodoPago_Ctrl.getTaskData(opot.Id);
        	MX_SB_PS_MetodoPago_Ctrl.loadOppK(opot.Id);
            System.assert(!tsk.isEmpty(),'Se ha recuperado informacion de task');
        test.stopTest();
    }
    @isTest static void testenviarOtp() {
        final Map<String,String> mapData = new Map<String, String>();
        final Opportunity oppRec = [Select Id, Name, SyncedQuoteId from Opportunity where StageName =:System.Label.MX_SB_PS_Etapa1 limit 1];
        final Quote quoData = [Select Id, Name from Quote where OpportunityId =: oppRec.Id limit 1];
        test.startTest();
        MX_SB_PS_MetodoPago_Ctrl.getQuoteLneItem(quoData.Id);
       	final String mapatest =MX_SB_PS_MetodoPago_Ctrl.enviarOtp(mapData);
        system.assertEquals('0',mapatest,'OTP Enviada');
        test.stopTest();
    }
    /**
    * @description Sincroniza quote
    * @param oppRecId oportunidad
    * @param quoteId quote
    **/
    public static void syncQuoteData(Opportunity oppRecId, String quoteId) {
        oppRecId.SyncedQuoteId = quoteId;
        update oppRecId;
    }
        @isTest 
    static void updateQuoteIVR() {
        Test.startTest();
        final Opportunity oppRec = [Select Id, Name, SyncedQuoteId from Opportunity where StageName =:System.Label.MX_SB_PS_Etapa1 limit 1];
        final Quote quoData = [Select Id, Name from Quote where OpportunityId =: oppRec.Id limit 1];
        syncQuoteData(oppRec, quoData.Id);
        MX_SB_PS_MetodoPago_Ctrl.updateQuoteIVR(oppRec.Id);
        MX_SB_PS_MetodoPago_Ctrl.findGenericObject(quoData.Id,'quot');
        final Quote quoteDataUpdate = [Select Id, Name from Quote where OpportunityId =: oppRec.Id limit 1];
        System.assertEquals(quoteDataUpdate.Id, quoData.Id, 'Quote Actualizada');
        Test.stopTest();
    }
    @isTest
    static void testIvrCodes() {
        test.startTest();
        	Final List<MX_RTL_IVRCodes__mdt> ivr=MX_SB_PS_MetodoPago_Ctrl.findGenericObject ('3003','ivr');
        	system.assertEquals(ivr[0].MX_RTL_IVRCode__c, '3003','Tarjeta sin saldo suficiente');
        test.stopTest();
    }
    
    @isTest
    static void testGenerica() {
        test.startTest();
        	Final MX_SB_VTS_Generica__c gnr=MX_SB_PS_MetodoPago_Ctrl.reqOpenPayCS();
        	system.assertEquals(gnr.MX_SB_VTS_Type__c, 'OP','TRUE');
        test.stopTest();
    }
}