/**
 * @File Name          : MX_SB_VTS_GetAmountServiceCtrl.cls
 * @Description        : Mock generation to create a fake reponse and get values in a JSON
 * @Author             : Diego Olvera
 * @Group              :
 * @Last Modified By   : Diego Olvera
 * @Last Modified On   : 8/5/2020 12:43:32
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    28/4/2020   Diego Olvera     Initial Version
**/
public without sharing class MX_SB_VTS_GetAmountServiceCtrl {
 /* constructor
*/
    private MX_SB_VTS_GetAmountServiceCtrl() {} //NOSONAR
    /*
* @Method: amountVal
* @Description: creates a fake response of a service
*/
    @AuraEnabled(cacheable=true)
   public static Map<String,List<Object>> amountVal() {
        final HttpResponse request = new HttpResponse();
        request.setBody('{"Propietario": ["2,040,000", "1,870,000", "1,700,000", "1,545,000", "1,416,000"], "Rentado": ["1,914,000", "1,626,000", "570,000", "370,000", "170,000"], "Cmb": ["51,400", "26,000", "37,000", "40,800", "50,090"]}');
        final Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(request.getBody());
        final List<Object> sumAsegu = (List<Object>) results.get('Propietario');
        final List<Object> sumAsegu1 = (List<Object>) results.get('Rentado');
        final List<Object> sumAsegu2 = (List<Object>) results.get('Cmb');
        final Map<String, List<Object>> returnVals = new Map<String, List<Object>>();
        returnVals.put('Propietario', sumAsegu);
        returnVals.put('Rentado', sumAsegu1);
        returnVals.put('Cmb', sumAsegu2);
    return returnVals;
    }
}