/**
 * @File Name          : MX_SB_VTS_GetSumInsured_Ctrl.cls
 * @Description        :
 * @Author             : Marco Antonio Cruz Barboza
 * @Group              :
 * @Last Modified By   : Marco Antonio Cruz Barboza
 * @Last Modified On   : 06/06/2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    06/06/2020    Marco Antonio Cruz Barboza         Initial Version
 * 1.1    06/06/2020    Marco Antonio Cruz Barboza         SOC version
 * 2.0    29/01/2021    Marco Antonio Cruz                 add a paremeter when is Propio or Rentado
 * 2.1    09/03/2021    Eduardo Hernández                  Se elimina cacheable de servicios
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_SB_VTS_GetSumInsured_Ctrl {
    /* 
    @Method: amountVal
    @Description: creates a fake response of a service
    */
    @AuraEnabled
    public static Map<String,List<Object>> amountVal() {
        return MX_SB_VTS_OutboundRentado_Service.amountVal();
    }
    
    /* 
    @Method: getAmounts
    @Description: retrieve a response from web service
	@Param: String insAmount
	@Return: List<Object>
    */
    @AuraEnabled
    public static List<Object> getAmounts(String insAmount, String propiedad) {
        return MX_SB_VTS_OutboundRentado_Service.getAmounts(insAmount, propiedad);
    }

}