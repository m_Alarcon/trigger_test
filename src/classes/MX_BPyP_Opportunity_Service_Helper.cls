/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPyP_Opportunity_Service_Helper
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-05-21
* @Description 	Helper for Opportunity Service Layer
* @Changes
*  
*/
public without sharing class MX_BPyP_Opportunity_Service_Helper {
    
    /** String Stage Abierta */
    static final String STABIERTA = 'Abierta';
    
    /** String Stage En Gestion */
    static final String STGESTION = 'En Gestión';
    
    /** String Stage Cerrada ganada */
    static final String STGANADA = 'Cerrada ganada';
    
    /** String Stage Descartada */
    static final String STRECHAZADA = 'Descartada / Rechazada';
    
    /*Contructor clase MX_BPyP_Opportunity_Service */
    private MX_BPyP_Opportunity_Service_Helper() {}

    /**
    * @Description 	Check if new status is valid, otherwise it throws error
    * @Param		opp Opportunity, oldStatus String, newStatus String
    * @Return 		NA
    **/
    public static Boolean validPrevNextStatus (Opportunity opp, String newStatus, String oldStatus) {
        Boolean isValid = false;
        if (oldStatus.equals(STABIERTA) && newStatus.equals(STGESTION)) {
            isValid = true;
        } else if (oldStatus.equals(STGESTION) && (newStatus.equals(STABIERTA) || newStatus.equals(STGANADA) || newStatus.equals(STRECHAZADA))) {
            isValid = true;
        }
        
        return isValid;
    }
}