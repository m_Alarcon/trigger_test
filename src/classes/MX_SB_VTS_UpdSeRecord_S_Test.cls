/**
 * @description       : 
 * @author            : Diego Olvera
 * @group             : 
 * @last modified on  : 09-28-2020
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   09-28-2020   Diego Olvera   Initial Version
**/
@isTest
private class MX_SB_VTS_UpdSeRecord_S_Test {
    /**@description Nombre usuario*/
    private final static String ASESORNAME = 'AsesorTest';
    /**@description Nombre del candidato*/
    private final static String LEADNAME = 'LeadTestSe';
    /**@description Nombre de la cuenta*/
    private final static String ACCNAME = 'TestSe';
    /**@description Nombre de Oportunidad*/
    private final static String OPPNAME = 'OppTestSe';
    /**@description Etapa de la Oportunida*/
    private final static String ETAPAOPP = 'Contacto';
    
    @TestSetup
    static void makeData() {
        final User userRec = MX_WB_TestData_cls.crearUsuario(ASESORNAME, 'System Administrator');
        insert userRec;
        final Lead leadRec = MX_WB_TestData_cls.vtsTestLead(LEADNAME, System.Label.MX_SB_VTS_Telemarketing_LBL);
        leadRec.Producto_Interes__c = 'Seguro Estudia';
        insert leadRec;
        final Account accnt = MX_WB_TestData_cls.createAccount(ACCNAME, System.Label.MX_SB_VTS_PersonRecord);
        insert accnt;
        final Opportunity opp = MX_WB_TestData_cls.crearOportunidad(OPPNAME, accnt.Id, userRec.Id, System.Label.MX_SB_VTA_VentaAsistida);
        insert opp;
        
    }
    @isTest
    static void updSeCurrentRecLead() {
        final User userUpdLead = [Select Id from User where LastName =: ASESORNAME];
        System.runAs(userUpdLead) {
            final Lead leadLst = [Select Id from Lead Where LastName =: LEADNAME];
            leadLst.Resultadollamada__c = '';
            leadLst.MX_SB_VTS_Tipificacion_LV2__c = '';
            leadLst.MX_SB_VTS_Tipificacion_LV3__c = '';
            leadLst.MX_SB_VTS_Tipificacion_LV4__c = '';
            leadLst.MX_SB_VTS_Tipificacion_LV5__c = '';
            leadLst.MX_SB_VTS_Tipificacion_LV6__c = '';
            leadLst.MX_SB_VTS_Tipificacion_LV7__c = '';
            leadLst.Status = ETAPAOPP;
            update leadLst;
            Test.startTest();
            try {
                MX_SB_VTS_UpdSeRecord_Service.updSeCurrentRec(leadLst.Id);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Error');
                System.assertEquals('Error', extError.getMessage(),'no entro a lead');
            }
            Test.stopTest();
        }
    }
    @isTest
    static void updSeCurrentRecOpp() {
        final User userUpd = [Select Id from User where LastName =: ASESORNAME];
        System.runAs(userUpd) {
            final Opportunity oppLst = [Select Id, Name from Opportunity Where Name =: OPPNAME];
            oppLst.Producto__c = 'Seguro Estudia';
            oppLst.Reason__c = 'Venta';
            oppLst.MX_SB_VTS_Tipificacion_LV1__c = '';
            oppLst.MX_SB_VTS_Tipificacion_LV2__c = '';
            oppLst.MX_SB_VTS_Tipificacion_LV3__c = '';
            oppLst.MX_SB_VTS_Tipificacion_LV4__c = '';
            oppLst.MX_SB_VTS_Tipificacion_LV5__c = '';
            oppLst.MX_SB_VTS_Tipificacion_LV6__c = '';
            oppLst.MX_SB_VTS_Tipificacion_LV7__c = '';
            oppLst.StageName = ETAPAOPP;
            oppLst.MX_SB_VTA_SubEtapa__c = ETAPAOPP;
            update oppLst;
            Test.startTest();
            try {
                MX_SB_VTS_UpdSeRecord_Service.updSeCurrentRec(oppLst.Id);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Fatal');
                System.assertEquals('Fatal', extError.getMessage(),'no entro a opp');
            }
            Test.stopTest();
        }
    }
}