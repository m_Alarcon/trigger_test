/**
 * @File Name          : MX_SB_VTS_LeadMultiCTI.cls
 * @Description        : Clase para Handler de Oportunidad
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : BBVA
 * @Last Modified By   : Eduardo Hernandez Cuamatzi
 * @Last Modified On   : 21/4/2020 0:36:25
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    16/6/2019 13:31:59   Eduardo Hernández Cuamatzi     Initial Version
 * 2.0    29/8/2019 15:25:04   Eduardo Hernández Cuamatzi     convivencia envios multi CTI
 * 2.1    22/08/2020           Vincent Juárez				  Se desactivan productos para CTI Purecloud
**/
public without sharing class MX_SB_VTS_LeadMultiCTI extends MX_SB_VTS_LeadMultiCTI_Util {
    
    /**Variable de status peticiones smart */
    final static List<String> LEADSTATUS = new List<String>{'200','201','203','204','205','206','207','208','226'};
    /**
     * MX_SB_VTS_FamliaProveedores__c> description
     * @param  Id Id description
     * @return    return description
     */
    Map<Id, MX_SB_VTS_FamliaProveedores__c> mapProvers = new Map<Id, MX_SB_VTS_FamliaProveedores__c>([Select Id, Name,MX_SB_VTS_Familia_de_productos__c,MX_SB_VTS_ProveedorCTI__c from MX_SB_VTS_FamliaProveedores__c where MX_SB_VTS_ProveedorCTI__c NOT IN ('') AND MX_SB_VTS_ProveedorCTI__r.MX_SB_VTS_IsReadyCTI__c = true]);

    /**
     * validLeadCTI description
     * @param  newEntry mapa de nuevos registros del trigger Lead
     */
    public static void validLeadCTI(List<Lead> newEntryLead, String typeSendV) {
        final Map<Id, String> leadProducts = new Map<Id, String>();
        final Set<String> valProds = new Set<String>();
        final Map<Id, Lead> newEntry = new Map<Id, Lead>();
        final Map<String, Boolean> purecloudProducto = new Map<String, Boolean>();
        
        for(MX_SB_PC_Productos__mdt producto:[Select Activo__c, MasterLabel from MX_SB_PC_Productos__mdt limit 20]) {
            purecloudProducto.put(producto.MasterLabel, producto.Activo__c);
        }

        for(Lead leadVal : newEntryLead) {
            if(String.isNotBlank(leadVal.LeadSource) && leadVal.LeadSource.equalsIgnoreCase(System.Label.MX_SB_VTS_OrigenCallMeBack) && String.isNotBlank(leadVal.MobilePhone)) {
                final String prodCorrect = MX_SB_VTS_ValidCorrectNamesASD.validacionProducto(leadVal.Producto_Interes__c);
                final boolean esProductoPC =  purecloudProducto.get(leadVal.Producto_Interes__c) == NULL? false : purecloudProducto.get(leadVal.Producto_Interes__c) ;
                if(!esProductoPC) {
                leadProducts.put(leadVal.Id, prodCorrect);
                valProds.add(prodCorrect);
                newEntry.put(leadVal.Id, leadVal);
                }
            }
        }
        if(valProds.isEmpty() == false) {
            validFamilysCTI(newEntry, leadProducts, valProds, typeSendV);
        }
    }

    /**
     * validFamilysCTI description
     * @param  newEntry     newEntry description
     * @param  leadProducts leadProducts description
     * @param  valProds     valProds description
     */ 
    public static void validFamilysCTI(Map<Id, Lead> newEntry, Map<Id, String> leadProducts, Set<String> valProds, String typeSendV) {
        final Map<String, Product2> valsFam = new Map<String, Product2>();
        final Set<Id> familiaProds = new Set<Id>();
        for(Product2 product : [Select Id, Name, MX_WB_FamiliaProductos__c from Product2 where Name IN: valProds]) {
            valsFam.put(product.Name, product);
            familiaProds.add(product.MX_WB_FamiliaProductos__c);
        }
        final Map<Id, List<MX_SB_VTS_FamliaProveedores__c>> valsFamPro = MX_SB_VTS_ServicesCTIMethods_cls.mapvalsFamPro(familiaProds);
        if(valsFamPro.keySet().isEmpty() == false) {
            createRequestCTI(newEntry, leadProducts, valsFamPro, valsFam, typeSendV);
        }
    }

    /**
     * createRequestCTI description
     * @param  newEntry     newEntry description
     * @param  leadProducts leadProducts description
     * @param  valsFamPro   valsFamPro description
     * @param  valsFam      valsFam description
     */
    public static void createRequestCTI(Map<Id, Lead> newEntry, Map<Id, String> leadProducts, Map<Id, List<MX_SB_VTS_FamliaProveedores__c>> valsFamPro, Map<String, Product2> valsFam, String typeSendV) {
        final List<WrapperEnvioCTI> lstEnvioCTI = MX_SB_VTS_ServicesCTIMethods_cls.fillItemRequest(newEntry, leadProducts, valsFamPro, valsFam);
        if(lstEnvioCTI.isEmpty() == false) {
            sendToCTI(lstEnvioCTI, typeSendV);
        }
    }

    /**
     * sendToCTI description
     * @param  enviosCTI enviosCTI description
     */
    public static void sendToCTI(List<WrapperEnvioCTI> enviosCTI, String typeSend) {
        final List<MX_SB_VTS_Lead_tray__c> mapBandejas = [Select Id, MX_SB_VTS_Campa_a_relacionada__c,
            MX_SB_VTS_Campa_a_relacionada__r.MX_SB_VTS_FamiliaProducto_Proveedor__r.MX_SB_VTS_ProveedorCTI__r.MX_SB_VTS_Identificador_Proveedor__c ,
            MX_SB_VTS_Campa_a_relacionada__r.MX_SB_VTS_FamiliaProducto_Proveedor__c, MX_SB_VTS_ID_Bandeja__c,
            MX_SB_VTS_Producto__r.Name, MX_SB_VTS_Producto__c, MX_SB_VTS_ProveedorCTI__c from MX_SB_VTS_Lead_tray__c where 
            MX_SB_VTS_Tipo_bandeja__c =: System.Label.MX_SB_VTS_HotLeads];
        final List<WrapperEnvioCTI> vcip = new List<WrapperEnvioCTI>();
        final List<WrapperEnvioCTI> smartCenter = new List<WrapperEnvioCTI>();
        for(WrapperEnvioCTI envioCTI : enviosCTI) {
            switch on envioCTI.proveedor.MX_SB_VTS_ProveedorCTI__r.MX_SB_VTS_Identificador_Proveedor__c {
                when 'SMART CENTER' {
                    smartCenter.add(envioCTI);
                }
                when 'VCIP' {
                    vcip.add(envioCTI);
                }
            }
        }
        if(mapBandejas.isEmpty() == false && smartCenter.isEmpty() == false) {
            sendtoSmartCenter(smartCenter, mapBandejas, typeSend);
        }
        if(typeSend.equalsIgnoreCase('callOut')) {
            sendtoVCIPCall(vcip);
        } else {
            sendtoVCIP(vcip);
        }
        
    }

    /**
     * sendtoSmartCenter enviar leads a SmartCenter
     * @param  envioCTI    leads ah enviar
     * @param  lstBandejas bandjeas de HotLeads
     */
    public static void sendtoSmartCenter(List<WrapperEnvioCTI> envioCTI, List<MX_SB_VTS_Lead_tray__c> lstBandejas, String typeSend) {
        final String idBandeja = findBandeja(envioCTI[0], lstBandejas);
        if(String.isNotBlank(idBandeja)) {
            final Map<String,String> mapsent = new map<String,String>();
            final String serviceId = MX_SB_VTS_LeadMultiCTI_Util.evaluteSmartValue();
            final List<MX_SB_VTS_SendLead_helper_cls.RequestSendLead> lstSend = new List<MX_SB_VTS_SendLead_helper_cls.RequestSendLead>();
            final List<Id> lstLeadIds = new List<Id>();
            final Map<Id, String> mapTrays= new Map<Id, String>();
            for(WrapperEnvioCTI wrapperItem : envioCTI) {
                final MX_SB_VTS_SendLead_helper_cls.RequestSendLead temprest = new MX_SB_VTS_SendLead_helper_cls.RequestSendLead();
                lstLeadIds.add(wrapperItem.leadId.Id);
                temprest.ID_LEAD=MX_SB_VTS_SendLead_helper_cls.returnEmpty(wrapperItem.leadId.Id);
                temprest.LOGIN=MX_SB_VTS_SendLead_helper_cls.returnEmpty('');
                temprest.SERVICEID= Integer.valueOf(MX_SB_VTS_SendLead_helper_cls.returnEmptyphone(serviceId));
                temprest.LOADID= Integer.valueOf(MX_SB_VTS_SendLead_helper_cls.returnEmptyphone(idBandeja));
                temprest.SCHEDULEDATE=MX_SB_VTS_SendLead_helper_cls.returnEmpty(checkDate(wrapperItem.leadId));   
                temprest.PHONE1=MX_SB_VTS_SendLead_helper_cls.returnEmpty(wrapperItem.leadId.MobilePhone);
                temprest.PHONE2=MX_SB_VTS_SendLead_helper_cls.returnEmpty(wrapperItem.leadId.MobilePhone);
                temprest.PHONE3=MX_SB_VTS_SendLead_helper_cls.returnEmpty(wrapperItem.leadId.MobilePhone);
                temprest.NAME= MX_SB_VTS_SendLead_helper_cls.fillFullName((SObject)wrapperItem.leadId, '');
                mapTrays.put(wrapperItem.leadId.Id, MX_SB_VTS_SendLead_helper_cls.returnEmptyphone(idBandeja));
                lstSend.add(temprest);
            }
            mapsent.put('ListLeads',JSON.serialize(lstSend));
            if(typeSend.equalsIgnoreCase('callOut')) {
                MX_SB_VTS_ServicesCTIMethods_cls.sendSmartCall('EntRegistroGestion', mapsent, lstLeadIds, mapTrays);
            } else {
                sendSmart('EntRegistroGestion', mapsent, lstLeadIds, mapTrays);
            }
        }
    }
    /**
     * sendSmart petición post al servicio de Smart
     * @param  Id función ejecutar, mapa de valores, lista Id leads
     */
    @future(callout=true)
    public static void sendSmart(String methodId, Map<String,String> mapsent, List<Id> lstLeadIds, Map<Id,String> mapTrays) {
        final HttpResponse HttpRest = MX_SB_VTS_SendLead_helper_cls.invoke(methodId, mapsent);
        MX_SB_VTS_ServicesCTIMethods_cls.processSmart(lstLeadIds, HttpRest, mapTrays);
    }
}