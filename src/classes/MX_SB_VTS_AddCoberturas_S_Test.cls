/**
* @description       : Clase de cobertura que cubre MX_SB_VTS_AddCoberturas_Service
* @author            : Diego Olvera
* @group             : BBVA
* @last modified on  : 03-04-2021
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   10-21-2020   Diego Olvera   Initial Version
* 1.1   04-03-2021   Diego Olvera   Se agrega/elimina función para aumento de cobertura
**/
@isTest
public class MX_SB_VTS_AddCoberturas_S_Test {
    /*User name test*/
    private final static String USERTEST = 'ASESORDATAPART';
    /*Account name test*/
    private final static String ACCOTEST = 'CUENTADATAPART';
    /*URL test*/
    private final static String URLTEST = 'http://www.ulrsampleDP2.com';
    /*Product code*/
    private static final String PRODUCTCODES = '3015';
    /*Alliance code*/
    private static final String ALLIANCECODES = 'FHSRT014';
    /**@description Nombre de la cuenta*/
    private final static String ACCCMBNAME = 'TestCmb';
    /**@description Nombre de Oportunidad*/
    private final static String OPPCMBNAME = 'TestOppCmb';
    /*Codigo al que pertenece la cobertura a buscar*/
    private final static String VALTRADE = 'TECN';
    
    @TestSetup
    static void makeData() {
        final User tstDpUserTest = MX_WB_TestData_cls.crearUsuario(USERTEST, 'System Administrator');
        insert tstDpUserTest;
        final Account accntCmb = MX_WB_TestData_cls.createAccount(ACCCMBNAME, System.Label.MX_SB_VTS_PersonRecord);
        insert accntCmb;
        final Opportunity oppCmb = MX_WB_TestData_cls.crearOportunidad(OPPCMBNAME, accntCmb.Id, tstDpUserTest.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
        oppCmb.LeadSource = 'Facebook';
        oppCmb.Producto__c = 'Hogar seguro dinámico';	
        insert oppCmb;
        final MX_RTL_MultiAddress__c nwAdd = new MX_RTL_MultiAddress__c();
        nwAdd.MX_RTL_MasterAccount__c = oppCmb.AccountId;
        nwAdd.MX_RTL_Opportunity__c = oppCmb.Id; 
        nwAdd.Name = 'Direccion';
        insert nwAdd;
        final Quote quoteRec = new Quote(OpportunityId=oppCmb.Id, Name='Outbound');
        quoteRec.MX_SB_VTS_Folio_Cotizacion__c = '556433';
        quoteRec.QuoteToName = 'Completa';
        insert quoteRec;
        final Cobertura__c crtCober = new Cobertura__c();
        crtCober.MX_SB_VTS_RelatedQuote__c = quoteRec.Id;
        crtCober.MX_SB_VTS_TradeValue__c = '12365';
        crtCober.MX_SB_MLT_Cobertura__c = '2008SA';
        crtCober.MX_SB_MLT_Descripcion__c ='001';
        insert crtCober;
        System.runAs(tstDpUserTest) {
            final Account testDpAcc = MX_WB_TestData_cls.crearCuenta(ACCOTEST, System.Label.MX_SB_VTS_PersonRecord);
            testDpAcc.PersonEmail = 'pruebaVts@mxvts.com';
            insert testDpAcc;
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'CalculateQuotePrice', iaso__Url__c = URLTEST, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
        }        
    }
    
    @isTest
    static void  calculateQuotePriceTest() {
        final User sampleUsr = [SELECT Id, Name FROM User WHERE LastName =: USERTEST];
        System.runAs(sampleUsr) {
            final Map<String, String> headersMockTest = new Map<String, String>();
            headersMockTest.put('tsec', '845697844');
            final MX_WB_Mock mockCallout = new MX_WB_Mock(200, 'Complete', '{}', headersMockTest); 
            iaso.GBL_Mock.setMock(mockCallout);
            test.startTest();
            MX_SB_VTS_AddCoberturas_Service.calculateQuotePrice(PRODUCTCODES, ALLIANCECODES);  
            test.stopTest();
            System.assertNotEquals('No entro', null,'No recuperó nada datos');
        }
    }
    
    @isTest
    static void updQuoName() {
        final User userGetOpp = [Select Id from User where LastName =: USERTEST];
        System.runAs(userGetOpp) {
            final Opportunity oppLstVal = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBNAME];
            final Quote nwLstQuo = [Select Id, QuoteToName, OpportunityId FROM Quote Where OpportunityId =: oppLstVal.Id];
            final List<String> addCober = new List<String>{'ELECTRONIC_EQUIPMENT','APPLIANCE_EQUIPMENT'};
                Test.startTest();
            try {
                MX_SB_VTS_AddCoberturas_Service.findCoverCodes(nwLstQuo.Id, addCober);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Error');
                System.assertEquals('Mal', extError.getMessage(),'No se accedió a la función');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void findByTradeTst() {
        final User userOpp = [Select Id from User where LastName =: USERTEST];
        System.runAs(userOpp) {
            final Opportunity oppsVals = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBNAME];
            final Quote lstQuoVals = [Select Id, QuoteToName, OpportunityId FROM Quote Where OpportunityId =: oppsVals.Id];
            final List<String> valsCober = new List<String>{'ELECTRONIC_EQUIPMENT','APPLIANCE_EQUIPMENT'};
                Test.startTest();
            try {
                MX_SB_VTS_AddCoberturas_Service.findByTrade(lstQuoVals.Id, VALTRADE, valsCober);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Error fatal');
                System.assertEquals('error', extError.getMessage(),'No se accedió a los datos');
            }
            Test.stopTest();
        }
    }

    @isTest
    static void fillAddCoveragesTest() {
        final User userGetOpp = [Select Id from User where LastName =: USERTEST];
        System.runAs(userGetOpp) {
            final Opportunity oppLstVal = [Select Id, StageName, Producto__c, Name from Opportunity Where Name =: OPPCMBNAME];
            final Quote nwLstQuo = [Select Id, QuoteToName, OpportunityId FROM Quote Where Name = 'Outbound' AND OpportunityId =: oppLstVal.Id];
            final List<Cobertura__c> nwLsCo = [Select Id, MX_SB_VTS_TradeValue__c, MX_SB_VTS_GoodTypeCode__c, 
                                               MX_SB_MLT_Descripcion__c, MX_SB_VTS_CategoryCode__c, RecordTypeId, 
                                               RecordType.Name, MX_SB_MLT_Cobertura__c, MX_SB_VTS_CoverageCode__c  
                                               FROM Cobertura__c WHERE MX_SB_VTS_RelatedQuote__c =: nwLstQuo.Id];
            Test.startTest();
            try {
                MX_SB_VTS_AddCoberturas_Service.fillAddCoverages(nwLsCo, oppLstVal,'INCE', nwLstQuo.Id);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Falla');
                System.assertEquals('Falla', extError.getMessage(),'No Obtuvo infomación');
            }
            Test.stopTest();
        }
    }        
}