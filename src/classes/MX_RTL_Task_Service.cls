/**
 * @File Name          : MX_RTL_Task_Service.cls
 * @Description        :
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Daniel Ramirez Islas
 * @Last Modified On   : 11-13-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/26/2020   Juan Carlos Benitez     Initial Version
 * 1.1   07/09/2020   Marco Cruz	            Add method to insert a task
**/
global without sharing class MX_RTL_Task_Service { // NOSONAR
     /*Property FIEDLS*/
  Final static String QUERYFIELDS = 'Id';

    /**
    * @description
    * @author Juan Carlos Benitez | 5/26/2020
    * @param TaskFields
    * @param Ids
    * @param rtDevName
    * @return List<Task>
    **/
    public static List<Task> getTaskData(Set<Id> ids) {
        return MX_RTL_Task_Selector.getTaskDataById(ids);
    }

    /**
    * @description : Methodo para obtener informacion
    * de la tarea
    * @author Daniel Ramirez Islas | 11-13-2020 
    * @param id 
    * @return List<Task> 
    **/
    public static List<Task> selectTaskById(String idToent) {
      final String qFilt = 'WHERE WhatId = '+'\''+ idToent +'\'';
      return MX_RTL_Task_Selector.getTaskLastId(QUERYFIELDS, qFilt);
    }
    /*
    * @description Actualiza o crea una tarea, de acuerdo al tipo
    * @param Task Object
    * @param String taskType
    */
    public static void updateTask(Task taskPc, String taskType) {
       MX_RTL_Task_Selector.updateTask(taskPc,taskType);
    }
    
    /*
    * @description Insert a task list 
    * @param List<Task> listTask
    */
    public static void insertTask(List<Task> listTask) {
       MX_RTL_Task_Selector.insertTask(listTask);
    }
    /*
    * @description Upsert a task list 
    * @param List<Task> listTask
    */
    public static void upsertTask(List<Task> listTask) {
       MX_RTL_Task_Selector.upsertTask(listTask);
    }
}