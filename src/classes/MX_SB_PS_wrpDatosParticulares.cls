/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpDatosParticulares
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
 @SuppressWarnings('sf:LongVariable')
public class MX_SB_PS_wrpDatosParticulares {
 
	 /*Public property for wrapper*/ 
	public header header {get;set;}
	  /*Public property for wrapper*/
	public String productCode {get;set;}
	  /*Public property for wrapper*/
	public iRequest iRequest {get;set;}
	
	 /*Public constructor for wrapper*/
public MX_SB_PS_wrpDatosParticulares() {
	header = new header();
	iRequest = new iRequest();
	iRequest.productPlan = new productPlan();
}
/*Public subClass for wrapper*/
public class header {
 /*Public property for wrapper*/
	public string aapType {get;set;}
  /*Public property for wrapper*/	
	public string dateRequest {get;set;}
  /*Public property for wrapper*/	
	public string channel {get;set;}
  /*Public property for wrapper*/	
	public string subChannel {get;set;}
  /*Public property for wrapper*/	
	public string branchOffice {get;set;}
  /*Public property for wrapper*/	
	public string managementUnit {get;set;}
  /*Public property for wrapper*/	
	public string user {get;set;}
  /*Public property for wrapper*/	
	public string idSession {get;set;}
  /*Public property for wrapper*/	
	public string idRequest {get;set;}
  /*Public property for wrapper*/	
	public string dateConsumerInvocation {get;set;}
	/*public constructor subclass*/
	public header() {
	 this.idSession = '';
	 this.idRequest = '';
	 this.dateConsumerInvocation = '';	
	 this.aapType = '';
	 this.dateRequest = '';
	 this.channel = '';
	 this.subChannel = '';
	 this.branchOffice = '';
	 this.managementUnit = '';
	 this.user = '';
	}		
}
  /*Public subClass for wrapper*/
public class iRequest {
  /*Public property for wrapper*/
	public productPlan productPlan {get;set;}
	/*public constructor subclass*/
	public iRequest() {
	 this.productPlan = new productPlan();
	}
}
  /*Public subClass for wrapper*/
public class productPlan {
  /*Public property for wrapper*/
	public String subPlanId {get;set;}
	/*public constructor subclass*/
	public productPlan() {
	 this.subPlanId = '';
	}
}
	

}