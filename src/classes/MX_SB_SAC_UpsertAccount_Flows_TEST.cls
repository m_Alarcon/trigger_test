/**
 * @File Name          : MX_SB_SAC_UpsertAccount_Flows_TEST.cls
 * @Description        : Provides coverage to class MX_SB_SAC_InsertAccount_Flows_Cls & MX_SB_SAC_UpdateAccount_Flows
 * @Author             : CBBVA Seguros - SAC Team
 * @Group              : BBVA
 * @Last Modified By   : Jaime Terrats
 * @Last Modified On   : 12/10/2019, 4:55:33 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    10/31/2019      BBVA Seguros - SAC Team   Initial Version
**/
    @isTest
    private class MX_SB_SAC_UpsertAccount_Flows_TEST {
    /** name variable for admin user and cases */
    final static String NAME = 'testSacValidate';
    /** variable for case description */
    final static String ASESOR_SAC = 'asesor sac';
    /** variable for case description */
    final static String LOREM_IPSUM = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum';
    /** error message variable */
    final static String ERR_MSG = 'Script-thrown exception';
    /** error message variable */
    final static String ASSERT_MSG = 'Error esperado.';
    /** value email  */
    final static String MYPHONE = '5556567890';
    /** email name */
    final static String EMAILNAME = 'testClass';
    /** email domain */
    final static String EMAILDOMAIN = '@sacmail.com.test';
    /**
    * @description makeData generates mock data
    * @author Cristian Dominguez  | 11/08/2019
    * @return void
    **/
    @TestSetup
    static void makeDataAccount() {

        final User usr = MX_WB_TestData_cls.crearUsuario(NAME, System.Label.MX_SB_VTS_ProfileAdmin);
        System.runAs(usr) {
            final User sacUsr = MX_WB_TestData_cls.crearUsuario(ASESOR_SAC, System.Label.MX_SB_SAC_AsesorSACProfile);
            insert sacUsr;
            final List<PermissionSet> sacPermSets = [Select Id from PermissionSet where Name in ('MX_SB_SAC_AsesorSAC', 'MX_SB_SAC_accesoCheckListConjuntoPermiso', 'Marketing Cloud Connector', 'MX_SAC_FichaCasos')];
            final List<PermissionSetAssignment> assignPermSets = new List<PermissionSetAssignment>();
            for (PermissionSet prmSet : sacPermSets) {
                final PermissionSetAssignment permSetAssmnt = new PermissionSetAssignment();
                permSetAssmnt.PermissionSetId = prmSet.Id;
                permSetAssmnt.AssigneeId = sacUsr.Id;
                assignPermSets.add(permSetAssmnt);
            }
            insert assignPermSets;
            final List<Account> accountToInsert = new List<Account>();
            final String myrecordType = System.Label.MX_SB_VTS_PersonRecord;

            final String rtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(myrecordType).getRecordTypeId();

            for (Integer i = 0; i < 6; i++) {
                final Account nAccount = new Account();
                nAccount.Apellido_materno__pc = NAME;
                nAccount.ApellidoPaterno__c = NAME;                
                nAccount.FirstName = NAME;
                nAccount.LastName = NAME;
                nAccount.PersonEmail = EMAILNAME + i + EMAILDOMAIN;
                nAccount.Phone = MYPHONE;
                nAccount.RecordTypeId = rtId;
                accountToInsert.add(nAccount);
            }

            insert accountToInsert;
        }
    }
    /**
    * @description provides coverage of function MX_SB_SAC_UpdateAccount_Flows.testUpdateAccount()
    * @author BBVA Seguros - SAC Team  | 11/08/2019
    * @return void
    **/
    @isTest
    private static void testUpdateAccount() {

        Test.startTest();
        final List<Account> uAccount = [Select Id, Apellido_materno__pc, ApellidoPaterno__c, PersonEmail from Account where ApellidoPaterno__c = :NAME];
        final User sacUser = [Select Id from User where LastName = :ASESOR_SAC];

        for (Account aAcc : uAccount) {

            aAcc.Apellido_materno__pc = NAME + 'Perez';
            aAcc.ApellidoPaterno__c = NAME + 'Perez';
            aAcc.PersonEmail = aAcc.Id +'aAcc@bbvasac.org';

        }


        System.runAs(sacUser) {
            try {
             MX_SB_SAC_UpdateAccount_Flows.updateAccount(uAccount);
            } catch (Exception ex) {
                final Boolean expectedError = ex.getMessage().contains(ERR_MSG) ? true : false;
                System.assertNotEquals(expectedError, true, ASSERT_MSG);
            }
        }
        Test.stopTest();
    }

    /**
    * @description provides coverage of function MX_SB_SAC_InsertAccount_Flows_Cls.InsertAccount()
    * @author BBVA Seguros - SAC Team  | 11/08/2019
    * @return void
    **/

    @isTest
    private static void testInsertAccount() {


        Test.startTest();

        final String myrecordType = System.Label.MX_SB_VTS_PersonRecord;
        final String rtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(myrecordType).getRecordTypeId();

        final List<Account> myaccountToInsert = new List<Account>();
        final User sacUser = [Select Id from User where LastName = :ASESOR_SAC];

        for (Integer i = 0; i < 6; i++) {
            final Account nAccount = new Account();
            nAccount.Apellido_materno__pc = NAME + i ;
            nAccount.ApellidoPaterno__c = NAME + i ;            
            nAccount.FirstName = NAME + i;
            nAccount.LastName = NAME + i;
            nAccount.PersonEmail = EMAILNAME + i + EMAILDOMAIN + '.b';
            nAccount.Phone = MYPHONE;
            nAccount.RecordTypeId = rtId;
            myaccountToInsert.add(nAccount);
        }


        System.runAs(sacUser) {
            try {
                MX_SB_SAC_InsertAccount_Flows_Cls.insertAccount(myaccountToInsert);
            } catch (Exception ex) {
                final Boolean expectedError = ex.getMessage().contains(ERR_MSG) ? true : false;
                System.assertNotEquals(expectedError, true, ASSERT_MSG);
            }
        }
        Test.stopTest();
    }
}