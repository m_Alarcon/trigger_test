/*
*Autor:         Gabriel Garcia
*Proyecto:      Retail Migración
*Descripción:   Handler para ejecución de eventos.
*_______________________________________________________________________________________
*Versión    Fecha       Autor                           Descripción
*1.0        11/12/2019  Gabriel García                  Creación
*/

public with sharing class MX_Event_Handler {
    /**
     * Constructor
    */
    public MX_Event_Handler() {
        if (trigger.isAfter && trigger.isInsert) {
            applyForBPYP(Trigger.new, 'insert');
        } else if(trigger.isAfter && trigger.isUpdate) {
            applyForBPYP(Trigger.new, 'update');
        } else if (trigger.isAfter && trigger.isDelete) {
            applyForBPYP(Trigger.old, 'delete');
        }
    }

    /**
    *Descripción:  Valida si aplica para registros de BPyP
    */
    public void applyForBPYP(List<Event> listEventos, String action) {
        Boolean apply = true;
        final Map<Id,RecordType> mapRecodT = new Map<Id,RecordType>([SELECT Id FROM RecordType WHERE SObjectType = 'Event' and DeveloperName = 'Evento_BPyP']);
        for(Event ev : listEventos) {
            if (mapRecodT != null && !mapRecodT.containsKey(ev.RecordTypeId)) {
                apply = false;
            }
        }
        if(apply) {
            executeMethod(action);
        }
    }

    /**
    *Descripción:  Ejecuta método correspondiente
    */
    public void executeMethod(String action) {
        final gcal.GBL_EventHandler gcalHandler = new gcal.GBL_EventHandler();
        switch on action {
            when 'insert' {
                gcalHandler.afterInsert();
            }
            when 'update' {
                gcalHandler.afterUpdate();
            }
            when 'delete' {
                gcalHandler.afterDelete();
            }
        }
    }
}