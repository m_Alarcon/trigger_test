/**
*
*Clase MX_SB_COB_CreaPagosEspejoSch_cls
*/
global with sharing class MX_SB_COB_CreaPagosEspejoSch_cls implements schedulable {
    /**propiedades
    lista de pagos espejo para upsert*/
    global List<PagosEspejo__c> lstPagoEspejo { get;set; }

    /**actualiza pagos espejo*/
    global void execute(SchedulableContext schCont) {

        	system.debug('execute schedule MX_WB_CreaPagosEspejo');

        	//se migra a cada batch TriggerExecutionControl_cls.setAlreadyDone('PagosEspejoTrigger_tgr');
        	TriggerExecutionControl_cls.setAlreadyDone('PagosEspejoTrigger_tgr');
       		Integer iCntPaso = 1;
            //pagos espejo upsert en batch
            final MX_SB_COB_CreaPagosEspejoUpBch_cls espejoBatch1 = new  MX_SB_COB_CreaPagosEspejoUpBch_cls(lstPagoEspejo);
          	final Id IDSecondJob = Database.executeBatch(espejoBatch1);//guardado de pagosespejo
            if(IDSecondJob==null && iCntPaso==1) {
                iCntPaso = lstPagoEspejo.size();
            }
         //se elimina el job actual
        system.abortJob(schCont.getTriggerId());

    }
}