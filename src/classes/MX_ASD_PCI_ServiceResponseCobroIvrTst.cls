/****************************************************************************************************
Información general
------------------------
author: Omar Gonzalez
company: Ids
Project: PCI

Information about changes (versions)
-------------------------------------
Number    Dates           Author            Description
------    --------        ---------------   -----------------------------------------------------------------
1.0      07-08-2020      Omar Gonzalez      Clase que se test para MX_ASD_PCI_ServiceResponseCobroIvr 
****************************************************************************************************/
@isTest
public class MX_ASD_PCI_ServiceResponseCobroIvrTst {
    /*
* Campo que recibe la cotización 
*/
    private static MX_ASD_PCI_WrapperResponseIvr.MX_ASD_PCI_DatosQuote responseIvr;
    /*
* Campo que recibe la cotización 
*/
    private static Quote quoteIvr;
    /*
* Campo que almacena el nombre de la cuenta 
*/
    public static final String NAMES = 'Omar Gonzalez';
    /*
* Campo que almacena el email para la cuenta 
*/
    public static final String EMAILIVR = 'test@gmail.com';
    
    /**
* @test setup que crea los registros
**/
    @TestSetup
    public static void makeDatas() {
        final User usTstIvr = MX_WB_TestData_cls.crearUsuario ( 'TesterLastName',  Label.MX_SB_VTS_ProfileIntegration );
        insert usTstIvr;
        final Account accTstIvr = MX_WB_TestData_cls.crearCuenta( 'LastName', 'PersonAccount' );
        accTstIvr.OwnerId = usTstIvr.Id;
        accTstIvr.PersonEmail = 'testIvr@retail.com';
        accTstIvr.CodigoPostal__c = '09396';
        insert accTstIvr;
        final Opportunity oppTstIvr = MX_WB_TestData_cls.crearOportunidad ( 'Tst', accTstIvr.Id, usTstIvr.Id, 'ASD' );
        oppTstIvr.FolioCotizacion__c = null;
        oppTstIvr.Estatus__c = 'Creada';
        oppTstIvr.correoElectronicoContratante__c = 'test@retail.com';
        oppTstIvr.TelefonoCliente__c = '5569705217';
        oppTstIvr.Producto__c = 'Seguro de Moto Bancomer';
        oppTstIvr.Reason__c = 'Venta';
        oppTstIvr.MX_SB_VTS_Aplica_Cierre__c = true;
        oppTstIvr.StageName=System.Label.MX_SB_PS_Etapa1;
        insert oppTstIvr;       
        final Quote quoteTstIvr = new Quote(OpportunityId=oppTstIvr.Id, Name=NAMES);
        quoteTstIvr.MX_ASD_PCI_IdCotiza__c = '12300912';
        quoteTstIvr.MX_SB_VTS_Folio_Cotizacion__c = '556432';
        quoteTstIvr.MX_ASD_PCI_FolioAntifraude__c = '98120019';
        quoteTstIvr.MX_ASD_PCI_ResponseCode__c = '0';
        quoteTstIvr.MX_ASD_PCI_DescripcionCode__c = 'Pago realizado con exito';
        quoteTstIvr.MX_ASD_PCI_MsjAsesor__c = 'El cliente completo el pago de su poliza';
        insert quoteTstIvr;
    }
    @isTest
    static void responseIvrCobro() {
        responseIvr = new MX_ASD_PCI_WrapperResponseIvr.MX_ASD_PCI_DatosQuote();
        responseIvr.folioDeCotizacion = '556432';
        responseIvr.idCotiza = '12300912';
        quoteIvr = [Select Id,MX_ASD_PCI_ResponseCode__c,MX_ASD_PCI_DescripcionCode__c,MX_ASD_PCI_MsjAsesor__c from Quote Where MX_ASD_PCI_IdCotiza__c = '12300912'];
        test.startTest();
        final List<MX_ASD_PCI_WrapperResponseIvr.MX_ASD_PCI_ResponseCobroIvr> resSFDC = MX_ASD_PCI_ServiceResponseIvr.responseIvrCobro(responseIvr);
        system.assertEquals(quoteIvr.MX_ASD_PCI_ResponseCode__c,resSFDC[0].responseCode,'SUCCESS');
        test.stopTest();
    }
    @isTest
    static void responseIvrCobroTwo() {
        responseIvr = new MX_ASD_PCI_WrapperResponseIvr.MX_ASD_PCI_DatosQuote();
        responseIvr.folioDeCotizacion = '556432';
        test.startTest();
        final List<MX_ASD_PCI_WrapperResponseIvr.MX_ASD_PCI_ResponseCobroIvr> resSFDC = MX_ASD_PCI_ServiceResponseIvr.responseIvrCobro(responseIvr);
        system.assertEquals('Error al buscar la cotización',resSFDC[0].descriptionCode,'ERROR');
        test.stopTest();
    }
}