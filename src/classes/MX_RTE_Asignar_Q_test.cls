/**
* ---------------------------------------------------------------------------------
* @Autor: Selena Yamili Rodriguez Vega , Ricardo Alberto Santos
* @Proyecto: Workflow GFD
* @Descripción : Test Class for asing current Q to new Iniciatives
				 this class sustitute Flow MX_RTE_Actualizar_a_PI_Actual
* ---------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ---------------------------------------------------------------------------------
* 1.0           01/04/2020     Selena Rodriguez               Creación de la clase
* ---------------------------------------------------------------------------------
*/

@isTest
public with sharing class  MX_RTE_Asignar_Q_test {

     /*Variable for create Start Date*/
	static date myDtIni = date.newInstance(2020, 3, 28);
     /*Variable for create End Date */
    static date myDtFin = date.newInstance(2020, 6,28);
    /*List store Iniciatives to upsert*/
    static List<MX_RTE_Iniciativa__c> nwIniciatives =new List<MX_RTE_Iniciativa__c>();
    /*Class constructor*/
    private MX_RTE_Asignar_Q_test() {

    }

    @isTest

   /**
  * @description: Method for get the current Q and assign to the new Iniciative
  * @author Selena Rodriguez
  */

    public static void testGetIniciative() {
        try {


            final MX_RTE_PI__c insQ = new MX_RTE_PI__c(Name='2Q20',MX_RTE_Fecha_Inicio__c=myDtIni,MX_RTE_Fecha_fin__c=myDtFin);
            insert (insQ);

            final MX_RTE_Programas__c insProg = new MX_RTE_Programas__c(Name='Programa Test');
            insert (insProg);

            final MX_RTE_Iniciativa__c insIni = new MX_RTE_Iniciativa__c (Name='Iniciativa Test',MX_RTE_Programa_Global__c=insProg.Id,MX_RTE_PI_Actual__c= null);
            insert insIni;

            final MX_RTE_Iniciativa__c insIni2 = new MX_RTE_Iniciativa__c (Name='Iniciativa Test 2',MX_RTE_Programa_Global__c=insProg.Id,MX_RTE_PI_Actual__c=insQ.id);
            insert insIni2;

                MX_RTE_Asignar_Q.getIniciative();
            	insIni.MX_RTE_PI_Actual__c=insQ.Id;
            	nwIniciatives.add(insIni);

                upsert nwIniciatives;

        } catch(System.DmlException e) {
           System.assertEquals(e.getMessage(),e.getMessage(),'Found Mistake');

        }

    }

}