/**
 * @File Name           : MX_MC_MTWConverter_Service.cls
 * @Description         :
 * @Author              : eduardo.barrera3@bbva.com
 * @Group               :
 * @Last Modified By    : eduardo.barrera3@bbva.com
 * @Last Modified On    : 17/03/2021, 15:39:09 PM
 * @Modification Log    :
 * =============================================================================
 * Ver          Date                     Author                     Modification
 * =============================================================================
 * 1.0      02/27/2020, 14:52:12 PM   eduardo.barrera3@bbva.com      Initial Version
 * 1.1      03/09/2020, 17:35:34 PM   eduardo.barrera3@bbva.com      Redefinition in field naming Wrappers
 * 1.2      03/18/2020, 18:02:09 PM   eduardo.barrera3@bbva.com      Methods segregation, logic for listConversation service.
 * 1.3      17/03/2021, 15:39:09 PM   eduardo.barrera3@bbva.com      Add logic and missing values assignation due to package version upgrade
 **/
global without sharing class MX_MC_MTWConverter_Service extends mycn.MC_VirtualServiceAdapter {//NOSONAR

    /**
    * @Description MSG_THR_ID property
    **/
    private static final String MSG_THR_ID = 'messageThreadId';

    /**
    * @Description Returns a generic Object based on method call from Class in MC component
    * @author eduardo.barrera3@bbva.com | 04/24/2020
    * @param parameters
    * @return Map<String, Object>
    **/
    global override Map<String, Object> convertMap (Map<String, Object> parameters) {

        Map<String, Object> inputMX = new Map<String, Object>();

        if (parameters.containsKey(MSG_THR_ID)) {
            inputMX = parameters;
        } else {
            String fltPattern = '';
            String valueToSend = '';
            String custVal = '';
            final User currentUser = mycn.MC_UserSelector.getUserById(UserInfo.getUserId());
            final String managerId = mycn.MC_UserUtilities.getManagerCode(currentUser);
            final String filterParam = String.valueOf(parameters.get('filter'));
            final String customerId = filterParam.substringBetween('customer.id==', ';'); //NOSONAR
            if (managerId == null || managerId == '') {
                valueToSend = managerId;
            } else {
                custVal = MX_MC_valueCipher_Service.callValueCipher(customerId,true);
                valueToSend = MX_MC_valueCipher_Service.callValueCipher(managerId,true);
            }
            fltPattern += 'customerId=' + valueToSend + '&' + 'receiver.id=' + custVal;
            inputMX.put('filter', fltPattern);
        }

        return inputMX;
    }

    /**
    * @Description Returns a generic Object based on method call from Class in MC component
    * @author eduardo.barrera3@bbva.com | 02/27/2020
    * @param body
    * @param method
    * @return Object
    **/
    global override Object convert (String body, String method, Map<String, Object> parameters) {

        Object mxObject = null;

        if ('getMessageThread'.equals(method)) {
            mxObject = this.mxWrapperToStandardMessage(body, parameters);
        } else if ('getListMessageThreads'.equals(method)) {
            mxObject = this.mxWrapperToStandardConversation(body, parameters);
        }

        return mxObject;
    }

    /**
    * @Description Returns a generic Object
    * @author eduardo.barrera3@bbva.com | 03/17/2020
    * @param body
    * @return Object
    **/
    public Object mxWrapperToStandardMessage (String body, Map<String, Object> parameters) {

        final MX_MC_MTWConverter_Service_Helper.MessageThread mxWrapper = (MX_MC_MTWConverter_Service_Helper.MessageThread)JSON.deserialize(body,MX_MC_MTWConverter_Service_Helper.MessageThread.class);
        final mycn.MC_MessageThreadWrapper.MessageThread standardWrapper = new mycn.MC_MessageThreadWrapper.MessageThread();
        standardWrapper.messages = this.mxWrapperMessageToStandardWrapperMessage(mxWrapper.data, String.valueOf(parameters.get(MSG_THR_ID)));
        standardWrapper.hasDraftMessages = MX_MC_Utils.hasDraftMessages(mxWrapper.data);

        final Case caseConversation = mycn.MC_ConversationsDatabase.getCaseByThreadId(String.valueOf(parameters.get(MSG_THR_ID)));
        Account clientAcc;
        String customerCode = '';

        if (caseConversation.AccountId != null) {
            clientAcc = mycn.MC_Account_Selector.getClientById(caseConversation.AccountId);
            customerCode = MX_MC_Utils.getCustomerCode(clientAcc);
        }

        final mycn.MC_MessageThreadWrapper.Customer customer = new mycn.MC_MessageThreadWrapper.Customer();
        customer.id = customerCode;
        standardWrapper.customer = customer;
        standardWrapper.isDraft = MX_MC_Utils.isConvDraft(mxWrapper.data);
        standardWrapper.id = String.valueOf(parameters.get(MSG_THR_ID));
        standardWrapper.subject = caseConversation.Subject;
        standardWrapper.isSolved = 'Closed'.equals(caseConversation.Status)? true : false;
        return standardWrapper;
    }

    /**
    * @Description Returns a generic Object
    * @author eduardo.barrera3@bbva.com | 03/17/2020
    * @param body
    * @return Object
    **/
    public Object mxWrapperToStandardConversation (String body, Map<String, Object> parameters) {

        final MX_MC_MTWConverter_Service_Helper.MessageThreadPage mxWrapper = (MX_MC_MTWConverter_Service_Helper.MessageThreadPage)JSON.deserialize(body,MX_MC_MTWConverter_Service_Helper.MessageThreadPage.class);
        final mycn.MC_MessageThreadWrapper.MessageThreadPage stdWrapperConv = new mycn.MC_MessageThreadWrapper.MessageThreadPage();
        stdWrapperConv.messageThreads = this.mxWrapToStdWrapConv(mxWrapper.data);
        stdWrapperConv.pagination = this.mxWrapPagToStdWrapPag(mxWrapper.pagination);
        return stdWrapperConv;
    }

    /**
    * @Description Returns a list of MX Wrapper messages
    * @author eduardo.barrera3@bbva.com | 02/27/2020
    * @param mxMessageList
    * @return List<MC_MessageThreadWrapper.Message>
    **/
    public List<mycn.MC_MessageThreadWrapper.Message> mxWrapperMessageToStandardWrapperMessage (List<MX_MC_MTWConverter_Service_Helper.Message> mxMessageList, String messageThreadId) {

        final List<mycn.MC_MessageThreadWrapper.Message> stdMessList = new  List<mycn.MC_MessageThreadWrapper.Message>();

        for (Integer i = mxMessageList.size() - 1; i >= 0 ; i--) {
            final mycn.MC_MessageThreadWrapper.Message standardMessage = new mycn.MC_MessageThreadWrapper.Message();
            standardMessage.id = mxMessageList[i].id;
            standardMessage.content = mxMessageList[i].message;
            standardMessage.readed = mxMessageList[i].wasRead;
            standardMessage.sender = this.mxSenderToStandardWrapperSender(mxMessageList[i].senderType);
            standardMessage.isDraft = mxMessageList[i].isDraft;
            if (mxMessageList[i].attachments != null) {
                standardMessage.attachments = this.mxWrapAttToStdWrapAtt(mxMessageList[i].attachments);
            }
            standardMessage.messageThreadId = messageThreadId;
            if (standardMessage.isDraft) {
               standardMessage.sentDate = DateTime.now();
            }
            stdMessList.add(standardMessage);
        }

        return stdMessList;
    }

    /**
    * @Description Returns a list of MX Wrapper messages
    * @author eduardo.barrera3@bbva.com | 03/17/2020
    * @param mxConvLst
    * @return List<MC_MessageThreadWrapper.Message>
    **/
    public List<mycn.MC_MessageThreadWrapper.MessageThread> mxWrapToStdWrapConv (List<MX_MC_MTWConverter_Service_Helper.MessageThreadConversation> mxConvLst) {

        final List<mycn.MC_MessageThreadWrapper.MessageThread> stdConvList = new  List<mycn.MC_MessageThreadWrapper.MessageThread>();

        for (MX_MC_MTWConverter_Service_Helper.MessageThreadConversation mxConversation : mxConvLst) {

            final mycn.MC_MessageThreadWrapper.MessageThread stdConv = new mycn.MC_MessageThreadWrapper.MessageThread();
            stdConv.id = mxConversation.id;
            stdConv.subject = mxConversation.subject;
            stdConv.creationDate = (Datetime) JSON.deserialize(mxConversation.creationDate, Datetime.class);
            stdConv.lastUpdate = (Datetime) JSON.deserialize(mxConversation.lastUpdate, Datetime.class);
            stdConv.manager = this.mxlookupToStandardWrapperLookup(mxConversation.businessAgent);
            stdConv.customerPendingMessages =  Integer.valueOf(mxConversation.unreadMessages) > 0;
            stdConv.managerPendingMessages = Integer.valueOf(mxConversation.unreadMessages) > 0;
            if (mxConversation.customer == null) {
                stdConv.customer = this.mxCustomerToStandardWrapperCustomer(mxConversation.customer, false);
            } else {
                stdConv.customer = this.mxCustomerToStandardWrapperCustomer(mxConversation.customer, true);
            }
            stdConv.type = this.mxStringToStandardWrapperOptionList(mxConversation.conversationType);
            stdConv.isSolved = Boolean.valueOf(mxConversation.status.description);
            stdConv.isDraft = 'D'.equals(mxConversation.conversationType) ? true : false;
            stdConvList.add(stdConv);
        }

        return stdConvList;
    }

    /**
    * @Description Returns a standard MC_MessageThreadWrapper.Pagination Object
    * @author eduardo.barrera3@bbva.com | 02/27/2020
    * @param mxPagination
    * @return MC_MessageThreadWrapper.Pagination
    **/
    public mycn.MC_MessageThreadWrapper.Pagination mxWrapPagToStdWrapPag (MX_MC_MTWConverter_Service_Helper.Pagination mxPagination) {

        final mycn.MC_MessageThreadWrapper.Pagination stdPag = new  mycn.MC_MessageThreadWrapper.Pagination();
        stdPag.firstPage = mxPagination.links.first;
        stdPag.nextPage = mxPagination.links.next;
        stdPag.page = mxPagination.page;
        stdPag.pageSize = mxPagination.pageSize;
        stdPag.numPages = mxPagination.totalPages;
        return stdPag;
    }

    /**
    * @Description Returns a standard list of MC_MessageThreadWrapper.Attachment Objects
    * @author eduardo.barrera3@bbva.com | 02/27/2020
    * @param mxAttachmentList
    * @return List<MC_MessageThreadWrapper.Attachment>
    **/
    public List<mycn.MC_MessageThreadWrapper.Attachment> mxWrapAttToStdWrapAtt (List<MX_MC_MTWConverter_Service_Helper.Attachment> mxAttachmentList) {

        final List<mycn.MC_MessageThreadWrapper.Attachment> stAttachmentList = new  List<mycn.MC_MessageThreadWrapper.Attachment>();

        for (MX_MC_MTWConverter_Service_Helper.Attachment mxAttachment : mxAttachmentList) {

            if (mxAttachment.name != '' && mxAttachment.url != '') {
            	final mycn.MC_MessageThreadWrapper.Attachment stAttachment = new mycn.MC_MessageThreadWrapper.Attachment();
                stAttachment.name = mxAttachment.name;
                final mycn.MC_MessageThreadWrapper.FileUrl fUrl = new mycn.MC_MessageThreadWrapper.FileUrl();
                fUrl.href = mxAttachment.url;
                stAttachment.fileUrl = fUrl;
                stAttachmentList.add(stAttachment);
            }
        }

        return stAttachmentList;
    }

    /**
    * @Description Returns a standard MC_MessageThreadWrapper.Sender object
    * @author eduardo.barrera3@bbva.com | 02/27/2020
    * @param mxSenderType
    * @return MC_MessageThreadWrapper.Sender
    **/
    public mycn.MC_MessageThreadWrapper.Sender mxSenderToStandardWrapperSender (String mxSenderType) {

        String standarSenderType = '';

        switch on mxSenderType {
            when  'CL'{
                standarSenderType = 'C';
            }
            when 'GE'{
                standarSenderType = 'G';
            }
        }

        final mycn.MC_MessageThreadWrapper.Sender stSender = new mycn.MC_MessageThreadWrapper.Sender();
        final mycn.MC_MessageThreadWrapper.OptionList stSenderOptLt = new mycn.MC_MessageThreadWrapper.OptionList();
        stSenderOptLt.id = standarSenderType;
        stSender.type = stSenderOptLt;
        return stSender;
    }

    /**
    * @Description Returns a standard MC_MessageThreadWrapper.Customer object
    * @author eduardo.barrera3@bbva.com | 03/27/2020
    * @param mxCustomer
    * @return MC_MessageThreadWrapper.Customer
    **/
    public mycn.MC_MessageThreadWrapper.Customer mxCustomerToStandardWrapperCustomer (MX_MC_MTWConverter_Service_Helper.Customer mxCustomer, Boolean useValCipher) {

        final mycn.MC_MessageThreadWrapper.Customer stCustomer = new mycn.MC_MessageThreadWrapper.Customer();
        if (useValCipher) {
            stCustomer.id = MX_MC_valueCipher_Service.callValueCipher(mxCustomer.id,false);
        } else {
            stCustomer.id = mxCustomer.id;
        }

        return stCustomer;
    }

    /**
    * @Description Returns a standard MC_MessageThreadWrapper.Lookup object
    * @author eduardo.barrera3@bbva.com | 03/27/2020
    * @param mxLookup
    * @return MC_MessageThreadWrapper.Lookup
    **/
    public mycn.MC_MessageThreadWrapper.Lookup mxlookupToStandardWrapperLookup (MX_MC_MTWConverter_Service_Helper.Lookup mxLookup) {

        final mycn.MC_MessageThreadWrapper.Lookup stLookup = new mycn.MC_MessageThreadWrapper.Lookup();
        stLookup.id = mxLookup.id;
        return stLookup;
    }

    /**
    * @Description Returns a standard MC_MessageThreadWrapper.OptionList object
    * @author eduardo.barrera3@bbva.com | 03/27/2020
    * @param mxType
    * @return MC_MessageThreadWrapper.OptionList
    **/
    public mycn.MC_MessageThreadWrapper.OptionList mxStringToStandardWrapperOptionList (String mxType) {

        final mycn.MC_MessageThreadWrapper.OptionList stOptionList = new mycn.MC_MessageThreadWrapper.OptionList();
        stOptionList.name = mxType;
        return stOptionList;
    }
}