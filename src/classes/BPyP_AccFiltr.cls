/*
    Versiones       Fecha           Autor               Descripción
    1.0             ?               ?                   ?
    1.1             18/01/2018      Cristian Espinosa   Se cambia el campo telefono__c por Phone. (SHIELD)
	1.2				12/11/2018		Cristian Espinosa	Se hace uso de una clase de utilidades para los componentes de la página de inicio
														de BPyP, que guarda los resultados de las consultas en variables estaticas para no
														llegar al límite de los registros retornados por consulta (50,000).
    1.3             03/07/2019      Hugo Carrillo       Resolución de Code Smells detectados por Sonar.
    1.4             15/08/2019      Cindy Hernández     Se agregan referencias a los tipos de registro
														MX_BPP_PersonAcc_Client y MX_BPP_PersonAcc_NoClient.
    1.5             16/08/2019      Gabriel Garcia      Se agregan campo MX_LastSecondName para cuentas personales
    1.6             27/08/2019      Cindy Hernández     Se utiliza etiqueta NOSONAR para falsos positivos en Potentially unused variable
                                                        found.
	1.7 			02/12/2019      Selena Rodriguez    Se realiza reingenieria en fetchBkMData para optimizar
    1.8             28/07/2020      Tania Vazquez       Se resuelven minors y codesmells
	1.9             21/08/2020      Gabriel García      Corrección de error de filtrado por banquero en fetchBkMData
*/
@SuppressWarnings('sf:DUDataflowAnomalyAnalysis')
public with sharing class BPyP_AccFiltr {

    /*Variables para recordtypes Cientes empresas,Prospectos Empresas*/
    final static String RCTYPECT='BPyP_tre_Cliente',RCTYPENCT='BPyP_tre_noCliente';
    /*Variable de recordtype Cliente Person Account,Prospecto person Account*/
    final static String RCTYPECP='MX_BPP_PersonAcc_Client',RCTYPENCP='MX_BPP_PersonAcc_NoClient';
    /*Variables de recordtype Banca de usuarios*/
    final static String BANCA='Red BPyP';
    /*Variables auxiliar de espacio*/
    final static String BLANK='\'\'';
	  /*Lista Tipos de información*/
    final static String CTCPROS='CP', CONTCS='C',PROSP='P';
    /*Variable entera NUMBCOL*/
    final static Integer NUMBCOL=25;
   /*Variable Auxiliar para crear Queries*/
    static String query;

    /*Constructor de la clase*/
    private BPyP_AccFiltr() {}

    @AuraEnabled
     /**
     * fetchAcc: Obtiene cuentas de un tipo especifico de recordtype
     * @param String bkMn
     * @param String type
     * @param Integer off
     * @return List<Account>
     */
    public static List<Account> fetchAcc(String bkMn,String type,Integer off) {
        List<Account> bpypAcc=new List<Account>();
        String banq='',query2;
        if(String.isNotEmpty(bkMn)) {
            banq='Owner.Name=:bkMn and ';
        }
        query='SELECT Id,Name, AccountNumber, BPyP_Rb_Family_Group_al_que_pertenece__c, BPyP_Rb_Family_Group_al_que_pertenece__r.Name , Phone, RecordType.Name,Owner.Name,BPyP_Fecha_de_ultima_visita__c FROM Account where ' + banq;
        try {
            if(type == CTCPROS) {
                query2= ' RecordType.DeveloperName in (:RCTYPECT,:RCTYPENCT,:RCTYPECP,:RCTYPENCP) Limit 10 Offset :off';
            } else if (type == CONTCS) {
                query2=' (RecordType.DeveloperName=:RCTYPECT OR RecordType.DeveloperName=:RCTYPECP) Limit 10 Offset :off';
            } else if (type == PROSP) {
                query2=' (RecordType.DeveloperName=:RCTYPENCT OR RecordType.DeveloperName=:RCTYPENCP) Limit 10 Offset :off';
            }
            bpypAcc=Database.query(String.escapeSingleQuotes(query+query2)); //NOSONAR
        } catch (QueryException e) {
            throw new AuraHandledException(System.Label.MX_BPP_PyME_Error_Generico+ ' ' + e);
        }
        return bpypAcc;
    }

    @AuraEnabled
    /**
     * @param String bkMn
     * @param String type
     * @return Integer
     */
    public static Integer fetchPgs(String bkMn,String type) {
        AggregateResult resultPgs=null;
        Integer numpgs=null;
        try {
            if(type == CTCPROS) {
                resultPgs=[SELECT Count(Id) FROM Account where Owner.Name=:bkMn and RecordType.DeveloperName in ('BPyP_tre_Cliente','BPyP_tre_noCliente','MX_BPP_PersonAcc_Client','MX_BPP_PersonAcc_NoClient')]; //NOSONAR
            } else if (type == CONTCS) {
                resultPgs=[SELECT Count(Id) FROM Account where Owner.Name=:bkMn and (RecordType.DeveloperName='BPyP_tre_Cliente' OR RecordType.DeveloperName='MX_BPP_PersonAcc_Client')]; //NOSONAR
            } else if (type == PROSP) {
                resultPgs=[SELECT Count(Id) FROM Account where Owner.Name=:bkMn and (RecordType.DeveloperName='BPyP_tre_noCliente' OR RecordType.DeveloperName='MX_BPP_PersonAcc_NoClient')]; //NOSONAR
            }
            final Integer nPags=(Integer)resultPgs.get('expr0');
        	numpgs=math.mod(nPags,10)==0? nPags/10 :(nPags/10)+1;
        } catch (QueryException e) {
            throw new AuraHandledException(System.Label.MX_BPP_PyME_Error_Generico+ ' ' + e);
        }
        return numpgs;
    }

    @AuraEnabled
    /**
     * fetchbaseurl: Obtiene la url apartir de "MX_BPP_CompUtils_cls.getBaseUrl()"
     * @return String
     */
    public static String fetchbaseurl() {
        String url = '';
        try {
            url = MX_BPP_CompUtils_cls.getBaseUrl();
        } catch (Exception e) {
            throw new AuraHandledException(System.Label.MX_BPP_PyME_Error_Generico+ ' ' + e);
        }
        return url;
    }

    @AuraEnabled
     /**
     * fetchusdata: Obtine una lista de Strings apartir de "MX_BPP_CompUtils_cls.getUsrData()"
     * @return List<String>
     */
    public static List<String> fetchusdata() {
        List<String> usrData = new List<String>();
        try {
            usrData = MX_BPP_CompUtils_cls.getUsrData();
        } catch (Exception e) {
            throw new AuraHandledException(System.Label.MX_BPP_PyME_Error_Generico+ ' ' + e);
        }
        return usrData;
    }

    @AuraEnabled
    /**
     * fetchDiv: Obtiene una lista de <<AggregateResult>> apartir de "MX_BPP_CompUtils_cls.getBPyPDivisions()"
     * @return List<AggregateResult>
     */
    public static List<AggregateResult> fetchDiv() {
        List<AggregateResult> bpypDivisions = new List<AggregateResult>();
        try {
            bpypDivisions = MX_BPP_CompUtils_cls.getBPyPDivisions();
        } catch (Exception e) {
            throw new AuraHandledException(System.Label.MX_BPP_PyME_Error_Generico+ ' ' + e);
        }
        return bpypDivisions;
    }

    @AuraEnabled
    /**
     * @param String type
     * @return WRP_Chart
     */
    public static WRP_Chart fetchDivData(String type) {
        List <AggregateResult> divBpyP;
        final List<Integer> red=BPyP_AccFiltr_Utilities.genRedBBVA();
        final List<Integer> green=BPyP_AccFiltr_Utilities.genGreenBBVA();
        final List<Integer> blue=BPyP_AccFiltr_Utilities.genBlueBBVA();
        try {
            if(type == CTCPROS||type == null) {
                divBpyP=[Select  Owner.Divisi_n__c, RecordType.Name, count(Id) RecordCount from Account where Owner.VP_ls_Banca__c = 'Red BPyP' and (NOT (Owner.BPyP_ls_NombreSucursal__c='')) and (NOT (Owner.Divisi_n__c='')) and Owner.IsActive=True and RecordType.DeveloperName in ('BPyP_tre_Cliente','BPyP_tre_noCliente','MX_BPP_PersonAcc_Client','MX_BPP_PersonAcc_NoClient') group by  Owner.Divisi_n__c, RecordType.Name]; //NOSONAR
            } else if (type == CONTCS) {
                divBpyP=[Select  Owner.Divisi_n__c, RecordType.Name, count(Id) RecordCount from Account where Owner.VP_ls_Banca__c = 'Red BPyP' and (NOT (Owner.BPyP_ls_NombreSucursal__c='')) and (NOT (Owner.Divisi_n__c='')) and Owner.IsActive=True and RecordType.DeveloperName in ('BPyP_tre_Cliente','MX_BPP_PersonAcc_Client') group by  Owner.Divisi_n__c, RecordType.Name]; //NOSONAR
            } else if (type == PROSP) {
                divBpyP=[Select  Owner.Divisi_n__c, RecordType.Name, count(Id) RecordCount from Account where Owner.VP_ls_Banca__c = 'Red BPyP' and (NOT (Owner.BPyP_ls_NombreSucursal__c='')) and (NOT (Owner.Divisi_n__c='')) and Owner.IsActive=True and RecordType.DeveloperName in ('BPyP_tre_noCliente','MX_BPP_PersonAcc_NoClient') group by  Owner.Divisi_n__c, RecordType.Name]; //NOSONAR
            }
        } catch (QueryException e) {
            throw new AuraHandledException(System.Label.MX_BPP_PyME_Error_Generico+ ' ' + e);
        }

        final List<String> div=new List<String>();
        final List<String> tempTyAc=new List<String>();
        final Map<String, Integer> num= new Map<String, Integer>();
        final List<String> color= new List<String>();
        Integer idx=0;
        for(AggregateResult aggRes: DivBpyP) {
            final String colour='rgb('+red[Math.mod(idx,NUMBCOL)]+', '+green[Math.mod(idx,NUMBCOL)]+', '+blue[Math.mod(idx,NUMBCOL)]+')';
            final String divNc=(String)aggRes.get('Divisi_n__c');
            final String nombre=(String)aggRes.get('Name');
            final Integer rCount=(Integer)aggRes.get('RecordCount'); idx++;
            div.add(divNc);
            tempTyAc.add(nombre);
            num.put(divNc+nombre,rCount);
            color.add(colour);
        }
        final List<String> tyAc=BPyP_AccFiltr_Utilities.dedupllist(tempTyAc);
        final List<String> tempDiv=BPyP_AccFiltr_Utilities.dedupllist(div);
        color.add('rgb');
        return new WRP_Chart(tempDiv,tyAc,num,color);
    }

    @AuraEnabled
    /**
     * fetchOff: Obtiene una lista de <<AggregateResult>> apartir de MX_BPP_CompUtils_cls.getBPyPOffices(div);
     * @param String div
     * @return List<AggregateResult>
     */
    public static List<AggregateResult> fetchOff(String div) {
        List<AggregateResult> bpypOffices = new List<AggregateResult>();
        try {
            bpypOffices = MX_BPP_CompUtils_cls.getBPyPOffices(div);
        } catch (Exception e) {
            throw new AuraHandledException(System.Label.MX_BPP_PyME_Error_Generico+ ' ' + e);
        }
        return bpypOffices;
    }

    @AuraEnabled
    /**
     * fetchOffData: Obtiene caracteristicas para devolver un WRP_Chart
     * @param String divi
     * @param String type
     * @return WRP_Chart
     */
    public static WRP_Chart fetchOffData(String divi, String type) {
        List <AggregateResult> offBpyP;
        final List<Integer> red=BPyP_AccFiltr_Utilities.genRedBBVA();
        final List<Integer> green=BPyP_AccFiltr_Utilities.genGreenBBVA();
        final List<Integer> blue=BPyP_AccFiltr_Utilities.genBlueBBVA();

        try {
            if(type == CTCPROS) {
                offBpyP=[Select  Owner.BPyP_ls_NombreSucursal__c, RecordType.Name, count(Id) RecordCount from Account where Owner.VP_ls_Banca__c = 'Red BPyP' and (NOT (Owner.BPyP_ls_NombreSucursal__c='')) and (NOT (Owner.Divisi_n__c='')) and Owner.Divisi_n__c=:divi and Owner.IsActive=True and RecordType.DeveloperName in ('BPyP_tre_Cliente','BPyP_tre_noCliente','MX_BPP_PersonAcc_Client','MX_BPP_PersonAcc_NoClient') group by  Owner.BPyP_ls_NombreSucursal__c, RecordType.Name]; //NOSONAR
            } else if (type == CONTCS) {
                offBpyP=[Select  Owner.BPyP_ls_NombreSucursal__c, RecordType.Name, count(Id) RecordCount from Account where Owner.VP_ls_Banca__c = 'Red BPyP' and (NOT (Owner.BPyP_ls_NombreSucursal__c='')) and (NOT (Owner.Divisi_n__c='')) and Owner.Divisi_n__c=:divi and Owner.IsActive=True and (RecordType.DeveloperName='BPyP_tre_Cliente' OR RecordType.DeveloperName='MX_BPP_PersonAcc_Client') group by  Owner.BPyP_ls_NombreSucursal__c, RecordType.Name]; //NOSONAR
            } else if (type == PROSP) {
                offBpyP=[Select  Owner.BPyP_ls_NombreSucursal__c, RecordType.Name, count(Id) RecordCount from Account where Owner.VP_ls_Banca__c = 'Red BPyP' and (NOT (Owner.BPyP_ls_NombreSucursal__c='')) and (NOT (Owner.Divisi_n__c='')) and Owner.Divisi_n__c=:divi and Owner.IsActive=True and (RecordType.DeveloperName='BPyP_tre_noCliente' OR RecordType.DeveloperName='MX_BPP_PersonAcc_NoClient') group by  Owner.BPyP_ls_NombreSucursal__c, RecordType.Name]; //NOSONAR
            }
        } catch (QueryException e) {
            throw new AuraHandledException(System.Label.MX_BPP_PyME_Error_Generico+ ' ' + e);
        }
        final List<String> div=new List<String>();
        final List<String> tempTyAc=new List<String>();
        final Map<String, Integer> num= new Map<String, Integer>();
        final List<String> color= new List<String>();
        Integer idx=0;
        for(AggregateResult aggRes: offBpyP) {final String colour='rgb('+red[Math.mod(idx,NUMBCOL)]+', '+green[Math.mod(idx,NUMBCOL)]+', '+blue[Math.mod(idx,NUMBCOL)]+')'; idx++; final String divNc=(String)aggRes.get('BPyP_ls_NombreSucursal__c'); final String nombre=(String)aggRes.get('Name'); final Integer nCount=(Integer)aggRes.get('RecordCount'); div.add(divNc); tempTyAc.add(nombre); num.put(divNc+nombre,nCount); color.add(colour);}
        final List<String> tyAc=BPyP_AccFiltr_Utilities.dedupllist(tempTyAc);
        final List<String> tempDiv=BPyP_AccFiltr_Utilities.dedupllist(div);
        color.add('rgb');
        final WRP_Chart res=new WRP_Chart(tempDiv,tyAc,num,color);
        return res;
    }

    @AuraEnabled
    /**
     * fetchBankMan: Obtiene una lista de <<AggregateResult>> apartir de "MX_BPP_CompUtils_cls.getBPyPUsrs(div, office)"
     * @param String div
     * @param String office
     * @return List<AggregateResult>
     */
    public static List<AggregateResult> fetchBankMan(String div,String office) {
        List<AggregateResult> bpypUsrs = new List<AggregateResult>();
        try {
            bpypUsrs = MX_BPP_CompUtils_cls.getBPyPUsrs(div, office);
        } catch (Exception e) {
            throw new AuraHandledException(System.Label.MX_BPP_PyME_Error_Generico+ ' ' + e);
        }
    return bpypUsrs;
    }

    @AuraEnabled
     /**
     * fetchBkMData: Obtiene caracteristicas para devolver un WRP_Chart"
     * @param String divi
     * @param String office
     * @param String bkm
     * @param String type
     * @return WRP_Chart
     */
    public static WRP_Chart fetchBkMData(String divi,String office,String bkm,String type) {
        List <AggregateResult> offBpyP;
        final List<Integer> red=BPyP_AccFiltr_Utilities.genRedBBVA();
        final List<Integer> green=BPyP_AccFiltr_Utilities.genGreenBBVA();
        final List<Integer> blue=BPyP_AccFiltr_Utilities.genBlueBBVA();
        String offi='',query2;
        try {
			if(String.isNotBlank(office)) {
                offi ='and Owner.BPyP_ls_NombreSucursal__c=:office ';
                }
			if(String.isNotBlank(bkm)) {
                offBpyP = BpyP_AccFiltr_Utilities.fetchBkMDataBkmEmpty(divi, office, bkm, type);
            } else {
                query='Select  Owner.Name, RecordType.Name rname, count(Id) RecordCount from Account where Owner.VP_ls_Banca__c =:BANCA and (NOT (Owner.BPyP_ls_NombreSucursal__c=:BLANK)) and (NOT (Owner.Divisi_n__c=:BLANK)) '+offi+'and Owner.Divisi_n__c=:divi and Owner.IsActive=True ';
                if(type == CTCPROS) {
                    query2=' and (RecordType.DeveloperName in (:RCTYPECT,:RCTYPENCT,:RCTYPECP,:RCTYPENCP)) group by  Owner.Name, RecordType.Name';
                } else if (type == CONTCS) {
                    query2=' and (RecordType.DeveloperName=:RCTYPECT OR RecordType.DeveloperName=:RCTYPECP) group by  Owner.Name, RecordType.Name';
                } else if (type == PROSP) {
                    query2=' and (RecordType.DeveloperName=:RCTYPENCT OR RecordType.DeveloperName=:RCTYPENCP) group by  Owner.Name, RecordType.Name';
                }
                offBpyP=Database.query(String.escapeSingleQuotes(query+query2));
            }
        } catch (QueryException e) {
            throw new AuraHandledException(System.Label.MX_BPP_PyME_Error_Generico+ ' ' + e);
        }
        final List<String> div=new List<String>();
        final List<String> tempTyAc=new List<String>();
        final Map<String, Integer> num= new Map<String, Integer>();
        final List<String> color= new List<String>();
        Integer idx=0;
        for(AggregateResult ar: offBpyP) {
            final String colour='rgb('+red[Math.mod(idx,NUMBCOL)]+', '+green[Math.mod(idx,NUMBCOL)]+', '+blue[Math.mod(idx,NUMBCOL)]+')';
            idx++; final String divNc=(String)ar.get('Name'); final String nombre=(String)ar.get('rname'); final Integer nCount=(Integer)ar.get('RecordCount');
            div.add(divNc);
            tempTyAc.add(nombre);
            num.put(divNc+nombre,nCount);
            color.add(colour);
        }
        final List<String> tyAc=BPyP_AccFiltr_Utilities.dedupllist(tempTyAc);
        final List<String> tempDiv=BPyP_AccFiltr_Utilities.dedupllist(div);
        color.add('rgb');
        return new WRP_Chart(tempDiv,tyAc,num,color);
    }

    /**Wrapper class for chart.js with labels, data and color */
    public class WRP_Chart {
        /** lista lsLabels*/
        @AuraEnabled public list<String> lsLabels {get; set;}
        /** Lista lsTyAcc*/
        @AuraEnabled public list<String> lsTyAcc {get; set;}
        /** lista lscolor*/
        @AuraEnabled public list<String> lscolor {get; set;}
        /** lsData*/
        @AuraEnabled public Map<String, Integer> lsData {get; set;}
        /** */
        public WRP_Chart(list<String> label,list<String> tyAcc,Map<String, Integer> data,list<String> color) {
            lsLabels=label;
            lsTyAcc=tyAcc;
            lsData=data;
            lscolor=color;
        }

    }
}