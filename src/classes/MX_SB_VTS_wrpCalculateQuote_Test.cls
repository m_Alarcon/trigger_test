/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 03-09-2021
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   03-09-2021   Eduardo Hernández Cuamatzi   Initial Version
**/
@isTest
private class MX_SB_VTS_wrpCalculateQuote_Test {

    @isTest
    static void fillWrapper() {
        final MX_SB_VTS_wrpCalculateQuotePrices.data dataWrapp = new MX_SB_VTS_wrpCalculateQuotePrices.data();
        final List<MX_SB_VTS_wrpCalculateQuotePrices.trades> lstTrades = new List<MX_SB_VTS_wrpCalculateQuotePrices.trades>();
        final MX_SB_VTS_wrpCalculateQuotePrices.trades tradeWrap = new MX_SB_VTS_wrpCalculateQuotePrices.trades();
        tradeWrap.iddata = 'dataTest';
        tradeWrap.description = 'descriptionTest';
        tradeWrap.categories = new List<MX_SB_VTS_wrpCalculateQuotePrices.categories>();
        final MX_SB_VTS_wrpCalculateQuotePrices.categories catEl = new MX_SB_VTS_wrpCalculateQuotePrices.categories();
        catEl.iddata = 'idDataCat';
        catEl.description = 'descriptionCat';
        catEl.goodTypes = new List<MX_SB_VTS_wrpCalculateQuotePrices.goodTypes>();
        final MX_SB_VTS_wrpCalculateQuotePrices.goodTypes goodTyp = new MX_SB_VTS_wrpCalculateQuotePrices.goodTypes();
        goodTyp.iddata = 'idDataCat';
        goodTyp.code = 'codeCat';
        goodTyp.description = 'descriptionGood';
        goodTyp.coverages = new List<MX_SB_VTS_wrpCalculateQuotePrices.coverages>();
        final MX_SB_VTS_wrpCalculateQuotePrices.coverages coverItem = new MX_SB_VTS_wrpCalculateQuotePrices.coverages();
        coverItem.iddata = 'idCover';
        coverItem.description = 'descriptionCover';
        coverItem.premiums = new List<MX_SB_VTS_wrpCalculateQuotePrices.premiums>();
        final MX_SB_VTS_wrpCalculateQuotePrices.premiums premiumItem = new MX_SB_VTS_wrpCalculateQuotePrices.premiums();
        premiumItem.iddata = 'dataCovs';
        premiumItem.amounts = new List<MX_SB_VTS_wrpCalculateQuotePrices.amounts>();
        final MX_SB_VTS_wrpCalculateQuotePrices.amounts amountItem = new MX_SB_VTS_wrpCalculateQuotePrices.amounts();
        amountItem.amount = 10.10;
        amountItem.currencydata = 'MXN';
        amountItem.isMajor = true;
        premiumItem.amounts.add(amountItem);
        coverItem.premiums.add(premiumItem);
        goodTyp.coverages.add(coverItem);
        catEl.goodTypes.add(goodTyp);
        tradeWrap.categories.add(catEl);
        lstTrades.add(tradeWrap);
        dataWrapp.trades = lstTrades;
        dataWrapp.disasterCovered = fillLstDisaster();
        dataWrapp.frequencies = frequenciesLst();
        dataWrapp.protectionLevel  = 10.10;
        System.assertEquals(dataWrapp.protectionLevel, dataWrapp.trades[0].categories[0].goodTypes[0].coverages[0].premiums[0].amounts[0].amount, 'Wrapper completo');
    }

    /**
    * @description Inicializa elemento disasterCovered
    * @author Eduardo Hernández Cuamatzi | 03-08-2021 
    * @return List<MX_SB_VTS_wrpCalculateQuotePrices.disasterCovered> 
    **/
    private static List<MX_SB_VTS_wrpCalculateQuotePrices.disasterCovered> fillLstDisaster() {
        final List<MX_SB_VTS_wrpCalculateQuotePrices.disasterCovered> lstDisaster = new List<MX_SB_VTS_wrpCalculateQuotePrices.disasterCovered>();
        final MX_SB_VTS_wrpCalculateQuotePrices.disasterCovered disasItem = new MX_SB_VTS_wrpCalculateQuotePrices.disasterCovered();
        disasItem.iddata = 'idaDisas';
        disasItem.value = 'disasVal';
        lstDisaster.add(disasItem);
        return lstDisaster;
    }

    /**
    * @description Inicializa elemento frequencies
    * @author Eduardo Hernández Cuamatzi | 03-09-2021 
    * @return List<MX_SB_VTS_wrpCalculateQuotePrices.frequencies> 
    **/
    private static List<MX_SB_VTS_wrpCalculateQuotePrices.frequencies> frequenciesLst() {
        final List<MX_SB_VTS_wrpCalculateQuotePrices.frequencies> lstfrequencies = new List<MX_SB_VTS_wrpCalculateQuotePrices.frequencies>();
        final MX_SB_VTS_wrpCalculateQuotePrices.frequencies frecItem = new MX_SB_VTS_wrpCalculateQuotePrices.frequencies();
        frecItem.iddata = 'frecIdData';
        frecItem.description = 'frecDesc';
        frecItem.premiums = new List<MX_SB_VTS_wrpCalculateQuotePrices.premiums>();
        frecItem.numberSubsequentPayments = 2;
        lstfrequencies.add(frecItem);
        return lstfrequencies;
    }
}