/**
* @description       : 
* @author            : Diego Olvera
* @group             : 
* @last modified on  : 10-22-2020
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   10-15-2020   Diego Olvera   Initial Version
* 1.1   12-01-2021   Diego Olvera   Adecuaciónd de clase consumo ASO
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton, sf:AvoidGlobalModifier')
global class MX_SB_VTS_GetPartData_DP_Service {
    /**
     * Atributos de la Clase
     */
    private static final Integer OKCODE = 200;
    /** Lista de Objetos */
    public static final List<Object> MRETURNVALUES = new List<Object>();
    
    /* constructor
    */
    private MX_SB_VTS_GetPartData_DP_Service() {}
    
    /**
     * @description: Realiza el consumo del servicio listCarInsuranceCatalogs/DP 
     * 				 para obtener: Datos Particulares
     * @author: 	 Diego Olvera
     * @return: 	 List<Object> mReturnValues
     */
    public static Map<String, Object> dptListCarInsCatSrvDp(String sManagmenUnit, String sProductCode) {
        final Map<String, Object> mReturnValue = new Map<String, Object>();
        final Map<String, Object> sParamsSrv = new Map<String, Object>{'TYPE' => 'DP', 'manUnit' => sManagmenUnit, 'productCode' => sProductCode};
        final HttpResponse objRespSrv = MX_RTL_IasoServicesInvoke_Selector.callServices('getParticularData', sParamsSrv, obtRequestSrv(sManagmenUnit, sProductCode));
        if(objRespSrv.getStatusCode() ==  OKCODE) {
            MRETURNVALUES.add(objRespSrv.getBody());
            final Map<String, Object> mStatusCode = new Map<String, Object>{'code' => String.valueOf(OKCODE), 'description' => 'Consumo de Servicio Exitoso!'};  
            mReturnValue.put('oData', MRETURNVALUES);    
            mReturnValue.put('oResponse', mStatusCode);
        } else {
            mReturnValue.put('oResponse', MX_SB_VTS_Codes_Utils.statusCodes(objRespSrv.getStatusCode()));
        }           
        return mReturnValue;
    }

    /**
     * @description : Realiza el armado del objeto Request para el consumo del servicio ASO
     * @author      : Alexandro Corzo
     * @return      : HttpRequest oRequest
     */
    public static HttpRequest obtRequestSrv(String sManagmenUnit, String sProductCode) {
        final Map<String,iaso__GBL_Rest_Services_Url__c> allCodASODP = iaso__GBL_Rest_Services_Url__c.getAll();
        final iaso__GBL_Rest_Services_Url__c objASODP = allCodASODP.get('getParticularData');
        final String strEndPDP = objASODP.iaso__Url__c;
        final HttpRequest oRqstDP = new HttpRequest();
        oRqstDP.setTimeout(120000);
        oRqstDP.setMethod('POST');
        oRqstDP.setHeader('Content-Type', 'application/json');
        oRqstDP.setHeader('Accept', '*/*');
        oRqstDP.setHeader('Host', 'https://test-sf.bbva.mx');
        oRqstDP.setEndpoint(strEndPDP);
        oRqstDP.setBody(obtBodyService(sManagmenUnit, sProductCode));
        return oRqstDP;
    }
    
    /**
     * @description : Realiza el armado del Body que sera ocupado en el Request del Servicio
     * @author      : Alexandro Corzo
     * @return      : String sBodyService
     */
    public static String obtBodyService(String sManagmenUnit, String sProductCode) {
    	String sBodySrvDP= '';
        sBodySrvDP += '{';
        sBodySrvDP += '"header": {';
        sBodySrvDP += '"aapType": "10000137",';
        sBodySrvDP += '"dateRequest": "2017-10-03 12:33:27.104",';
        sBodySrvDP += '"channel": "66",';
        sBodySrvDP += '"subChannel": "120",';
        sBodySrvDP += '"branchOffice": "ASD",';
        sBodySrvDP += '"managementUnit": "'+sManagmenUnit+'",';
        sBodySrvDP += '"user": "CARLOS",';
        sBodySrvDP += '"idSession": "",';
        sBodySrvDP += '"idRequest": "",';
        sBodySrvDP += '"dateConsumerInvocation": "2017-10-03 12:33:27.104"';
        sBodySrvDP += '},';
        sBodySrvDP += '"productCode" : "'+sProductCode+'",';
        sBodySrvDP += '"iRequest": {';
        sBodySrvDP += '"productPlan": {';
        sBodySrvDP += '"subPlanId": ""';
        sBodySrvDP += '}';
        sBodySrvDP += '}';
        sBodySrvDP += '}';
        return sBodySrvDP;
    }
}