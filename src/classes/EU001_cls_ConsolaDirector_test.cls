/**
*Desarrollado por:       Indra
*Autor:                  Javier Ortiz
*Proyecto:               Experiencia Unica
*Descripción:            Clase Test para la clase EU001_cls_ConsolaDirector
*
*Cambios (Versiones) prueba
*-------------------------------------------------------------------------------
*No.         Fecha            Autor                           Descripción
*------   ----------------   --------------------   ----------------------------
*1.0      14-12-2017         Javier Ortiz             Creación
*2.0      01-02-2018         Isaías Velázquez         Modificación
*3.0      06-02-2018         Francisco J Licona       Modificación
*3.1      07-02-2018         Isaías Velázquez         Modificación
*3.2      09-02-2018         Francisco J Licona       Modificación
*3.3      21-03-2018         Abraham Tinajero         Se acutalizan méthods de creación de datos
*4.0      20-04-2018         Francisco J Licona       Cobertura de clase
*4.1      20-04-2018         Francisco J Licona       Cobertura de method de funcionalidad PI10
*5.0      14-08-2018         Luis Fernando R.         Se actualizan parámetros de method
*5.1      13-09-2018         Luis Fernando R.         Se actualizan parámetros de method
*5.2      17/10/2018         Fernando Jaime             Se cambia administrador del sistema por etiqueta personalizada
*6.0      31-10-2018         César Villanueva         Actualización de method TestCompromisos y se añade méthod de prueba
*													  para la actualización de trackings
*6.1	  30-01-2019		 Mario Calderón			  Se actualizan parámetros de method en "testCompromisos"
*7.0	  21-06-2019		 Cindy Hernández		  Se eliminan los method que no se utilizan. Se agregan los method
                                                      getRIMapTest, getRIMapNoStaffTest y getRIMapNoDirTest para
													  cubrir el method getRIMap y se agrega el method consolaDirConstructor.
*7.1	  26-06-2019	     Cindy Hernández		  Se agregan los method getRINoDirector y getRISelectError para aumentar el porcentaje de cobertura.
*7.2      27-07-2020         Angel Nava               Se ajusta codigo duplicado y codesmells
**/
@isTest
public  class EU001_cls_ConsolaDirector_test {
    /**Mapa rolMapa */
    static Map<String, UserRole> rolMapa = new Map<String, UserRole>();
    /**Lista userRole */
    static List<UserRole> userRole;
    
    /**lenguaje */
    final static String LENGUAGE = 'en_US';
    
    /** alias*/
    final static String ALIAS ='UserDos';
    /** perfil */
    final static String PERFIL = 'BPYP BANQUERO PRIVADO y UHN LOMAS';

    static {
        userRole = new List<UserRole>([SELECT Id, Name FROM UserRole WHERE Name = :PERFIL]);
        for(UserRole ur:userRole) { rolMapa.put(ur.Name, ur); }
    }
    @testsetup
    static void preTest() {
        final User userTest = MX_BPP_UserAndRIDataFactory.crearUsuario('Usertest', 'BPyP Estandar', 'BPYP BANQUERO TORRE BANCOMER');
        insert userTest;
    }
    private static List<Id> generaRi(User userT) {
         final List<EU001_RI__c> generaRI= MX_BPP_UserAndRIDataFactory.generaRI();
             final List<Id> riIds = new List<Id>();
             for(EU001_RI__c ri : generaRI) {
                 ri.OwnerId = userT.Id;
                 riIds.add(userT.Id);
             }
             upsert generaRI;
        return riIds;
        
    }
     @isTest static void getRIMapTest() {
         final User user1 = [select id from user limit 1];
         System.runAs(user1) {
            final List<Id> riIds = generaRi(user1);
             test.StartTest();
             final Map<Id, EU001_cls_ConsolaDirector.WRP_Table> riMap = EU001_cls_ConsolaDirector.getRIMap(true, true, riIds);
             
             final Map<Id, EU001_cls_ConsolaDirector.WRP_Table> riMap2 = EU001_cls_ConsolaDirector.getRIMap(true, false, riIds);
             final Map<Id, EU001_cls_ConsolaDirector.WRP_Table> riMap3 = EU001_cls_ConsolaDirector.getRIMap(false, false, riIds);
            System.assertNotEquals(null, riMap.isEmpty()&&riMap2.isEmpty()&&riMap3.isEmpty());
             test.stopTest();
         }
    }

    @isTest static void valoracionRI() {

        final String[] ugVar = new List<String>();
        ugVar.add('789001');
        ugVar.add('789011');

        final List<User> users = MX_BPP_UserAndRIDataFactory.createTestRecordsUser(ugVar, false);

        final EU001_RI__c nuevoRi = new EU001_RI__c();
        nuevoRi.EU001_tx_Estado__c = 'Preparación';
        nuevoRi.EU001_ft_Fecha_Inicio__c = system.now();
        nuevoRi.ownerId = users.get(0).Id;

        insert nuevoRi;
        test.StartTest();

        EU001_cls_ConsolaDirector.getUser(nuevoRi.id);

        nuevoRI.EU001_tl_Comentario_EV__c='HOLA';
        nuevoRI.EU001_tl_Comentario_DO__c='ADIOS';
        nuevoRI.EG_001_ls_Valoracion_de_RI__c='2';
        nuevoRI.EG001_Valoracion_de_RI_DO__c='3';
        update nuevoRI;
        System.assert(true, 'assert');
        test.stopTest();
    }

    @isTest static void testFunctionConFuncionAbajo() {
        final User user2 = MX_BPP_UserAndRIDataFactory.creaUser();
        insert user2;
        final User userX = MX_BPP_UserAndRIDataFactory.crearUsuario(ALIAS, Label.MX_PERFIL_SystemAdministrator, PERFIL);
        userX.Alias = ALIAS;
        userX.Email='ej.empresas@bbva.tst.deploy.com';
        userX.EmailEncodingKey='ISO-8859-1';
        userX.LastName='Prueba';
        userX.LanguageLocaleKey=LENGUAGE;
        userX.LocaleSidKey=LENGUAGE;
        userX.CR__c = '1110';
        userX.UG__c = '0468132';
        userX.Segmento_Ejecutivo__c = 'EMPRESARIAL';
        userX.TimeZoneSidKey='America/Mexico_City';
        userX.UserName='admin2@bbva.tst.deploy.com';
        userX.VP_ls_Banca__c = 'Red BPyP';
        userX.IsActive=true;
        insert userX;

        test.startTest();
        System.runAs(user2) {
            final EU001_RI__c ri2 = MX_BPP_UserAndRIDataFactory.creaRI(user2.Id);
            insert ri2;
            EU001_cls_ConsolaDirector.getRI();
            System.assert(true,'This Works!');
        }
        test.stopTest();
    }

    @isTest
    static void getRINoDirector() {
        final User userX2 = MX_BPP_UserAndRIDataFactory.crearUsuario(ALIAS, Label.MX_PERFIL_SystemAdministrator, PERFIL);
        userX2.Alias = ALIAS;
        userX2.Email='ej.empresas@bbva.tst.deploy.com';
        userX2.EmailEncodingKey='ISO-8859-1';
        userX2.LastName='Prueba';
        userX2.LanguageLocaleKey=LENGUAGE;
        userX2.LocaleSidKey=LENGUAGE;
        userX2.CR__c = '1110';
        userX2.UG__c = '0468132';
        userX2.Segmento_Ejecutivo__c = 'EMPRESARIAL';
        userX2.TimeZoneSidKey='America/Mexico_City';
        userX2.UserName='admin2@bbva.tst.deploy.com';
        userX2.VP_ls_Banca__c = 'Red BPyP';
        userX2.IsActive=true;
        insert userX2;

        test.startTest();
        System.runAs(userX2) {
            final EU001_RI__c ri2 = MX_BPP_UserAndRIDataFactory.creaRI(userX2.Id);
            insert ri2;
            final list<EU001_cls_ConsolaDirector.WRP_Table> riList = EU001_cls_ConsolaDirector.getRI();
            System.assert(!riList.isEmpty(),'Pasa test');
        }
        test.stopTest();
    }

    @isTest static void testFunctionConFuncionAbajo2() {
        final User user3a = MX_BPP_UserAndRIDataFactory.creaUser();
        user3a.UserRoleId = rolMapa.get('BPYP BANQUERO PRIVADO y UHN LOMAS').Id;
        insert user3a;
        test.startTest();
        System.runAs(user3a) {
            final EU001_RI__c ri6 = MX_BPP_UserAndRIDataFactory.creaRI(user3a.Id);
            ri6.EU001_tx_Estado__c = 'Preparación';
            insert ri6;
            EU001_cls_ConsolaDirector.getRISelect(ri6.Id);
            System.assert(true,'This Works !');
        }
        test.stopTest();
    }

  	@isTest
    static void getRISelectError() {
        final User user4a = MX_BPP_UserAndRIDataFactory.creaUser();
        user4a.UserRoleId = rolMapa.get(PERFIL).Id;
        insert user4a;
        test.startTest();
        System.runAs(user4a) {
            final EU001_RI__c ri6 = MX_BPP_UserAndRIDataFactory.creaRI(user4a.Id);
            ri6.EU001_tx_Estado__c = 'Preparación';
            insert ri6;
            String errorMessage = '';
            try {
           		EU001_cls_ConsolaDirector.getRISelect('BadId');
            } catch (Exception e) {
                errorMessage = e.getMessage();
            }
            System.assertEquals('Script-thrown exception', errorMessage,'it work');
        }
        test.stopTest();
    }

    @isTest static void generaNuevoRITest() {
        final User usr = MX_BPP_UserAndRIDataFactory.creaUser();
        insert usr;
        Test.startTest();

        EU001_cls_ConsolaDirector.VerificaDirector();
        EU001_cls_ConsolaDirector.generaNuevoRI();

        Test.stopTest();
        System.assert(true,'This Works!');
    }

    @isTest
    static void consolaDirConstructor() {
        String errorMessage = '';
        test.startTest();
        try {
	         final EU001_cls_ConsolaDirector oppRep = new EU001_cls_ConsolaDirector();
            if(oppRep==null) {
                errorMessage = 'fail';
            }
        } catch (Exception e) {
            errorMessage = e.getMessage();
        }
        test.stopTest();
        System.assertEquals('', errorMessage,'check test');
    }
}