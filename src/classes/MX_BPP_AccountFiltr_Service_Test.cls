/**
* @File Name          : MX_BPP_AccountFiltr_Service_Test.cls
* @Description        : Test class for MX_BPP_AccountFiltr_Service
* @Author             : Jair Ignacio Gonzalez G
* @Group              :
* @Last Modified By   : Jair Ignacio Gonzalez G
* @Last Modified On   : 23/10/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      23/10/2020            Jair Ignacio Gonzalez G          Initial Version
**/
@isTest
private class MX_BPP_AccountFiltr_Service_Test {

    /** error message */
    final static String MESSAGEERR = 'Fail method MX_BPP_AccountFiltr_Service';
    /** String Cuentas */
    final static String CUENTASOSERV = 'Cuentas';
    /*Usuario de pruebas*/
    private static User testUserStartASer = new User();
    /** name variable for records */
    final static String NAMESTSERA = 'testUserAcc';
    /** namerole variable for records */
    final static String NAME_ROLESERLA = '%BPYP BANQUERO BANCA PERISUR%';
    /** nameprofile variable for records */
    final static String NAME_PROFILESSER = 'BPyP Estandar';
    /** namedivision variable for records */
    final static String NAMECOT_DIVSERA = 'METROPOLITANA';
    /** name variable for oficina */
    final static String PRIVSERA ='PRIVADO';
    /** name variable for records */
    final static String NAMECOT_OFFSERA = '6343 PEDREGAL';

    /*Setup para clase de prueba*/
    @testSetup
    static void setupAccCtrl() {

        testUserStartASer = UtilitysDataTest_tst.crearUsuario(NAMESTSERA, NAME_PROFILESSER, NAME_ROLESERLA);
        testUserStartASer.Title = PRIVSERA;
        testUserStartASer.Divisi_n__c = NAMECOT_DIVSERA;
        testUserStartASer.BPyP_ls_NombreSucursal__c = NAMECOT_OFFSERA;
        testUserStartASer.VP_ls_Banca__c = 'Red BPyP';
        insert testUserStartASer;

        final Id rtAccCtrl = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta BPyP').getRecordTypeId();

        final Account cuentaACtrl = new Account(FirstName = 'CuentaService', LastName = 'LastService', No_de_cliente__c = '12345678', RecordTypeId = rtAccCtrl, OwnerId = testUserStartASer.Id );
        insert cuentaACtrl;
    }

    /**
    * @description constructor test
    * @author Jair Ignacio Gonzalez G | 23/10/2020
    * @return void
    **/
    @isTest
    private static void testConstructorServAcc() {
        final MX_BPP_AccountFiltr_Service tmpService = new MX_BPP_AccountFiltr_Service();
        System.assertNotEquals(tmpService, null, MESSAGEERR);
    }

    /**
     * @description test fetchServiceDataAcc
     * @author Jair Ignacio Gonzalez G | 23/10/2020
     * @return void
     **/
    @isTest
    private static void fetchServiceDataAccTst() {
        Test.startTest();
        final MX_BPP_AccountFiltr_Service.WRP_ChartStacked wrpCharTestSer = MX_BPP_AccountFiltr_Service.fetchServiceDataAcc( new List<String>{CUENTASOSERV, 'Division', ''} );
        System.assertNotEquals(wrpCharTestSer, null, MESSAGEERR);

        Test.stopTest();
    }

    /**
     * @description test fetchAcc
     * @author Jair Ignacio Gonzalez G | 23/10/2020
     * @return void
     **/
    @isTest
    private static void fetchServAccTest() {
        Test.startTest();
        final List<Account> listAccServTest = MX_BPP_AccountFiltr_Service.fetchAcc( new List<String>{CUENTASOSERV, 'Division', '', '10'} );
        System.assertNotEquals(listAccServTest, null, MESSAGEERR);

        Test.stopTest();
    }
}