/**
 * @File Name          : MX_SB_VTS_SendTrackingTelCupon_Ctrl.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernandez Cuamatzi
 * @Last Modified On   : 3/6/2020 19:34:05
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    28/5/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
public without sharing class MX_SB_VTS_SendTrackingTelCupon_Ctrl implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    /**variable query*/
    public string finalQuery {get;set;}
    /**variable minuto*/
    public Integer hoursCall {get;set;}
    
    /**
     * MX_SB_VTS_SendTrackingTelCupon_Ctrl constructor
     * @param  finalQuery  query a ejecutar
     * @param  hoursCall tiempo hacia atras tracking web cupon
     */
    public MX_SB_VTS_SendTrackingTelCupon_Ctrl(String finalQuery, Integer hoursCall) {
        this.finalQuery = finalQuery;
        this.hoursCall = hoursCall;
    }

    /**
    * @description Inicio de clase Batch
    * @author Eduardo Hernandez Cuamatzi | 3/6/2020 
    * @param Database.BatchableContext batchContext contexto Batch
    * @return Database.querylocator 
    **/
    public Database.querylocator start(Database.BatchableContext batchContext) {
        this.finalQuery =  MX_SB_VTS_SendTrackingTelCupon_Service.updateQuery(this.finalQuery,  this.hoursCall);
        return Database.getQueryLocator(this.finalQuery);
    }

    /**
    * @description Ejecución de Batch
    * @author Eduardo Hernandez Cuamatzi | 3/6/2020 
    * @param Database.BatchableContext batchContext 
    * @param List<Opportunity> scopeOpps Lista de registros a procesar
    * @return void 
    **/
    public void execute(Database.BatchableContext batchContext, List<Opportunity> scopeOpps) {
        final Map<String, EmailNoValidosCotiza__c> mapENoValCot = EmailNoValidosCotiza__c.getAll();
        final Map<Id,MX_SB_VTS_FamliaProveedores__c> mapProveedors = MX_SB_VTS_SendTrackingTelCupon_Service.findProveed();
        final List<Opportunity> validOpps = MX_SB_VTS_SendTrackingTelCupon_Service.validOppsSend(scopeOpps, mapENoValCot);
        MX_SB_VTS_SendTrackingTelCupon_Service.processOppsSend(mapProveedors, validOpps);
    }

    /**
     * finish ejecución al finalizar
     * @param  batchContext contexto batch
     */
    public void finish(Database.BatchableContext batchContext) {

    }
}