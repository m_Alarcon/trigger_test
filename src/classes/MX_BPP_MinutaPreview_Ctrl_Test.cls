/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_MinutaPreview_Ctrl_Test
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2021-01-13
* @Description 	Test Class for BPyP Minuta Preview VF Controller
* @Changes      Date		|		Author		|		Description
 * 			2021-02-22			Héctor Saldaña	  Modified constructorTest() method due to new
 * 		                                          logic added within it
 * 		    2021-03-08          Héctor Saldaña    New fields added in setup() as per required
 * 		                                          Fields eliminated within Catalogo Minutas query
*  
*/
@isTest
public class MX_BPP_MinutaPreview_Ctrl_Test {
    
    /*Test User instance*/
    private static User testUser = new User();
    
    /*Tes user Admin instance*/
    private static User adminUser = new User();
    
    /*Test Account instance*/
    private static Account testAcc = new Account();
    
    /*Test Visita instance*/
    private static dwp_kitv__Visit__c testVisit = new dwp_kitv__Visit__c();
    
    @testSetup
    static void setup() {
        adminUser = UtilitysDataTest_tst.crearUsuario('AdminBPP', Label.MX_PERFIL_SystemAdministrator,'BBVA ADMINISTRADOR');
        insert adminUser;
        testUser = UtilitysDataTest_tst.crearUsuario('UserTest', 'BPyP Estandar', 'BPYP BANQUERO BANCA PERISUR');
        testUser.Title = 'Privado';
        insert testUser;

        System.runAs(testUser) {
            testAcc.LastName = 'testAcc';
            testAcc.FirstName = 'testAcc';
            testAcc.OwnerId = testUser.Id;
            testAcc.No_de_cliente__c = '1234DJ9';
            insert testAcc;
            
            testVisit.dwp_kitv__visit_start_date__c = Date.today()+4;
            testVisit.dwp_kitv__account_id__c = testAcc.Id;
            testVisit.dwp_kitv__visit_duration_number__c = '15';
            testVisit.dwp_kitv__visit_status_type__c = '01';
            testVisit.MX_BIE_TipoVisitaLLamada__c = 'Comercial';
            testVisit.MX_PYME_ObjetivoBPyP__c = 'Otro';
            insert testVisit;
            
            final ContentVersion contentVersion = new ContentVersion(Title = 'PenguinsBIE', PathOnClient = 'PenguinsBIE.pdf', VersionData = Blob.valueOf('Test ContentBIE'), FirstPublishLocationId=testVisit.Id);
            insert contentVersion;
            
            final MX_BPP_CatalogoMinutas__c catalogoRecord = new MX_BPP_CatalogoMinutas__c();
            catalogoRecord.MX_Acuerdos__c = 1;
            catalogoRecord.MX_Asunto__c = 'Prueba Catalogo';
            catalogoRecord.Name = 'BienvenidaDO';
            catalogoRecord.MX_TipoVisita__c = 'BienvenidaDO';
            catalogoRecord.MX_Saludo__c = 'Saludo Test';
            catalogoRecord.MX_Contenido__c = 'Contenido Test <<Opcional1>>';
            catalogoRecord.MX_Despedida__c = 'Despedida Test';
            catalogoRecord.MX_Firma__c = 'Firma Test';
//            catalogoRecord.MX_Numero_Opcionales__c = 2;
            catalogoRecord.MX_ImageHeader__c = 'Imagen_Test';
            catalogoRecord.MX_CuerpoCorreo__c = 'Correo Cuerpo';
            insert catalogoRecord;
        }
    }
    
    /**
    * @Description 	Test method for constructor
    * @Params		parameterToGet String
    **/
    @isTest
    static void constructorTest() {
        MX_BPP_MinutaPreview_Ctrl ctrlInstance;
        testVisit = [SELECT Id, RecordType.Name FROM dwp_kitv__Visit__c WHERE CreatedBy.Name = 'UserTest' LIMIT 1];
        adminUser = [SELECT Id FROM User WHERE Name = 'AdminBPP' LIMIT 1];
        final MX_BPP_CatalogoMinutas__c catalogoTst = [SELECT Id, Name FROM MX_BPP_CatalogoMinutas__c WHERE MX_TipoVisita__c = 'BienvenidaDO' LIMIT 1];
        testVisit.MX_BIE_TipoVisitaLLamada__c = 'Comercial';
        testVisit.MX_PYME_ObjetivoBPyP__c = 'Otro';
        catalogoTst.MX_Opcionales__c = '{OpcionalTexto1}Test{OpcionalTexto1}';
        catalogoTst.MX_Contenido__c = '{OpcionalTexto1}';
        update catalogoTst;
        update testVisit;
        Test.startTest();
        System.runAs(adminUser) {
            ApexPages.currentPage().getParameters().put('Id', String.valueOf(testVisit.Id));
            final String smyCS = '{"LastModifiedDate":"2018-06-27T20:47:16.000Z","IsDeleted":false,"dwp_kitv__Attach_file__c":true,"dwp_kitv__Visualforce_Name__c":"minuta","SetupOwnerId":"","dwp_kitv__Disable_send_field_in_BBVA_team__c":false,"Name":"'+testVisit.RecordType.Name+'","SystemModstamp":"2018-06-27T20:47:16.000Z","dwp_kitv__Minimum_members_BBVA_team__c":0,"CreatedById":"","dwp_kitv__Subject__c":"BBVA Minuta","CreatedDate":"2018-06-26T23:22:44.000Z","Id":"","LastModifiedById":"","dwp_kitv__Disable_send_field_in_contacts__c":false,"dwp_kitv__Minimum_Number_of_Agreement__c":1}';
            ApexPages.currentPage().getParameters().put('myCS', smyCS);
            ctrlInstance = new MX_BPP_MinutaPreview_Ctrl();
        }
        Test.stopTest();
        
        System.assertEquals(testVisit.Id, ctrlInstance.minutaWrapper.configParamsPty.currentId, 'CurrentId error');
    }
    
    /**
    * @Description 	Test method for constructor
    * @Params		parameterToGet String
    **/
    @isTest
    static void constructorErrorsTest() {
        MX_BPP_MinutaPreview_Ctrl ctrlInstance;
        adminUser = [SELECT Id FROM User WHERE Name = 'AdminBPP' LIMIT 1];
        testVisit = [SELECT Id, RecordType.Name FROM dwp_kitv__Visit__c WHERE CreatedBy.Name = 'UserTest' LIMIT 1];
        final MX_BPP_CatalogoMinutas__c catalogoTest = [SELECT Id FROM MX_BPP_CatalogoMinutas__c WHERE MX_TipoVisita__c = 'BienvenidaDO' LIMIT 1];
        delete catalogoTest;
        Test.startTest();
        System.runAs(adminUser) {
            ApexPages.currentPage().getParameters().put('Id', String.valueOf(testVisit.Id));
            final String smyCS = '{"LastModifiedDate":"2018-06-27T20:47:16.000Z","IsDeleted":false,"dwp_kitv__Attach_file__c":true,"dwp_kitv__Visualforce_Name__c":"minuta","SetupOwnerId":"","dwp_kitv__Disable_send_field_in_BBVA_team__c":false,"Name":"'+testVisit.RecordType.Name+'","SystemModstamp":"2018-06-27T20:47:16.000Z","dwp_kitv__Minimum_members_BBVA_team__c":0,"CreatedById":"","dwp_kitv__Subject__c":"BBVA Minuta","CreatedDate":"2018-06-26T23:22:44.000Z","Id":"","LastModifiedById":"","dwp_kitv__Disable_send_field_in_contacts__c":false,"dwp_kitv__Minimum_Number_of_Agreement__c":1}';
            ApexPages.currentPage().getParameters().put('myCS', smyCS);
            ctrlInstance = new MX_BPP_MinutaPreview_Ctrl();
        }
        Test.stopTest();

        System.assertEquals(testVisit.Id, ctrlInstance.minutaWrapper.configParamsPty.currentId, 'CurrentId error');
    }
    
    @isTest
    static void getParamaterSettingTest() {
        testVisit = [SELECT Id, RecordType.Name FROM dwp_kitv__Visit__c WHERE CreatedBy.Name = 'UserTest' LIMIT 1];
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('Id', String.valueOf(testVisit.Id));
        final String smyCS = '{"LastModifiedDate":"2018-06-27T20:47:16.000Z","IsDeleted":false,"dwp_kitv__Attach_file__c":true,"dwp_kitv__Visualforce_Name__c":"minuta","SetupOwnerId":"","dwp_kitv__Disable_send_field_in_BBVA_team__c":false,"Name":"'+testVisit.RecordType.Name+'","SystemModstamp":"2018-06-27T20:47:16.000Z","dwp_kitv__Minimum_members_BBVA_team__c":0,"CreatedById":"","dwp_kitv__Subject__c":"BBVA Minuta","CreatedDate":"2018-06-26T23:22:44.000Z","Id":"","LastModifiedById":"","dwp_kitv__Disable_send_field_in_contacts__c":false,"dwp_kitv__Minimum_Number_of_Agreement__c":1}';
        ApexPages.currentPage().getParameters().put('myCS', smyCS);
        final dwp_kitv__Template_for_type_of_visit_cs__c paramSettings = MX_BPP_MinutaPreview_Ctrl.getParamaterSetting(smyCS);
        Test.stopTest();
        
        System.assertNotEquals(null, paramSettings, 'getparameterSetting Error');
    }
    
    /**
    * @Description 	Test method for getParamaterDocs
    * @Params		NA
    **/
    @isTest
    static void getParamaterDocsTest() {
        testVisit = [SELECT Id, RecordType.Name FROM dwp_kitv__Visit__c WHERE CreatedBy.Name = 'UserTest' LIMIT 1];
        final ContentVersion document = new ContentVersion(Title = 'PenguinsBIE', PathOnClient = 'PenguinsBIE.pdf', VersionData = Blob.valueOf('Test ContentBIE'), FirstPublishLocationId=testVisit.Id);
        insert document;
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('Id', String.valueOf(testVisit.Id));
        final String sDocuments = '[{"Id":"'+document.Id+'","Title":"Penguins","IsAssetEnabled":true}]';
        ApexPages.currentPage().getParameters().put('documents', sDocuments);
        final List<ContentVersion> contentVList = MX_BPP_MinutaPreview_Ctrl.getParamaterDocs(sDocuments);
        Test.stopTest();
        
        System.assert(!contentVList.isEmpty(), 'getParamaterDocs error');
    }

}