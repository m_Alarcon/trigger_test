/**
 * @File Name          : MX_SB_VTS_OutboundRentado_Service.cls
 * @Description        :
 * @Author             : Marco Antonio Cruz Barboza
 * @Group              :
 * @Last Modified By   : Marco Antonio Cruz Barboza
 * @Last Modified On   : 13/07/2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    13/07/2020    Marco Antonio Cruz Barboza         Initial Version
 * 2.0    29/01/2021    Marco Antonio Cruz Barboza         Add a decision for Rentado o Propio, also create two methods for  web service httpRequest 
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton, sf:AvoidGlobalModifier')
global class MX_SB_VTS_OutboundRentado_Service {
    /*Status Code service*/
    Private static Final Integer STATUSCD = 200;
    /*Static vairable*/
    Public static Final String PROPIO = 'Propio';
    /* 
    @Method: amountVal
    @Description: creates a fake response of a service
    */
    public static Map<String,List<Object>> amountVal() {
        Final HttpResponse respService = new HttpResponse();
        respService.setBody('{"Rentado": ["1,914,000.00", "1,626,000.00", "570,000.00", "370,000.00", "170,000.00"]}');
        final Map<String, Object> jsonResp = (Map<String, Object>) JSON.deserializeUntyped(respService.getBody());
        final List<Object> sumInsured = (List<Object>) jsonResp.get('Rentado');
        final Map<String, List<Object>> rtnValues = new Map<String, List<Object>>();
        rtnValues.put('Rentado', sumInsured);
        return rtnValues;
    }
    
    /* 
    @Method: getAmounts
    @Description: get an amount lists from a web service
	@Param String insAmount
	@Return List<Object>
    */
    public static List<Object> getAmounts(String insAmount, String propiedad) {
        Final Map<String, Object> Service = new Map<String,Object>{
            'AMOUNT' => insAmount
                };
        if(propiedad == PROPIO) {
            Service.put('CATEGORY','BUILDING');
            Service.put('GOODTYPE','BUILDING');
        } else {
            Service.put('CATEGORY','INVENTORY_AND_FURNITURE');
            Service.put('GOODTYPE','CONTENT');
        }
        Final HttpRequest requestMod = requestCreation(Service);
        Final List <Object> insValues = new List<Object>();
        try {
            final HttpResponse response = MX_RTL_IasoServicesInvoke_Selector.callServices('getInsuredAmount', Service, requestMod);
            if (response.getStatusCode() == STATUSCD) {
                final MX_SB_VTS_wrpInsuredAmount_Utils wrapper = (MX_SB_VTS_wrpInsuredAmount_Utils)JSON.deserialize(response.getBody().replace('"currency":', '"currencies":'),MX_SB_VTS_wrpInsuredAmount_Utils.class);
                insValues.add(wrapper.suggestedAmount.amount);
                for(MX_SB_VTS_wrpInsuredAmount_Utils.suggestedCoverages test : wrapper.suggestedCoverages) {
                    insValues.add(test.suggestedAmount.amount);
                }
            } else {
                insValues.add('error');
            }
        } catch(System.CalloutException except) {
            insValues.add('proxy');
        }


        return insValues;
    }
    
    /* 
    @Method: requestCreation
    @Description: create a HttpRequest to web service
	@Param Map<String,Object> service
	@Return HttpRequest 
    */
    public static HttpRequest requestCreation (Map<String,Object> service ) {
        Final Map<String,iaso__GBL_Rest_Services_Url__c> rstServices = iaso__GBL_Rest_Services_Url__c.getAll();
        Final iaso__GBL_Rest_Services_Url__c getUrlLP = rstServices.get('getInsuredAmount');
        Final String endpointService = getUrlLP.iaso__Url__c;
        Final HttpRequest requestPC = new HttpRequest();
        requestPC.setTimeout(120000);
        requestPC.setMethod('POST');
        requestPC.setHeader('Content-Type', 'application/json');
        requestPC.setHeader('Accept', '*/*');
        requestPC.setHeader('Host', 'https://test-sf.bbva.mx/QRVS_A02');
        requestPC.setEndpoint(endpointService);
        requestPC.setBody(bodyCreate(service));
        return requestPC;
    }
    
    /* 
    @Method: bodyCreate
    @Description: create the body for the HttpRequest
	@Param Map<String,Object> service
	@Return String
    */
    public static String bodyCreate(Map<String,Object> service) {
        String bodyRequest = '';
        bodyRequest += '{';
        bodyRequest += '"goods": [';
        bodyRequest += '{';
        bodyRequest += '"category": {';
        bodyRequest += '"id": "'+service.get('CATEGORY').toString()+'"';
        bodyRequest += '},';
        bodyRequest += '"goodType": {';
        bodyRequest += '"id": "'+service.get('GOODTYPE').toString()+'"';
        bodyRequest += '}';
        bodyRequest += '}';
        bodyRequest += '],';
        bodyRequest += '"cadastralValue": {';
        bodyRequest += '"amount": "'+service.get('AMOUNT').toString()+'",';
        bodyRequest += '"currency": "MXN"';
        bodyRequest += '},';
        bodyRequest += '"product": {';
        bodyRequest += '"id": "DHSDT003"';
        bodyRequest += '}';
        bodyRequest += '}';
        return bodyRequest;
    }
}