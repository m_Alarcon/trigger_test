/**
* @FileName          : MX_SB_VTS_Colonias_Ctrl
* @description       : Class controller to get a ZipCode depended of a postal code.
* @Author            : Marco Antonio Cruz Barboza  
* @last modified on  : 08-05-2020
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   08-05-2020   Marco Antonio Cruz Barboza          Initial Version
* 1.0.1 09-03-2021   Eduardo Hernández                   Se eliminan cacheable
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_SB_VTS_Colonias_Ctrl {
    
    /**
    * @description : Call an web service apex method from a Service class
    * @Param zipCode 
    * @Return Map<String, List<Object>> 
    **/
    @AuraEnabled
    public static Map<String, List<Object>> getZipCode(String zipCode) {
        return MX_SB_VTS_ListCatalog_Service.srchZipCode('CO', zipCode);
    }
    
    /**
    * @description : Call an web service apex method from a Service class
    * @Param zipCode 
    * @Return Map<String, List<Object>> 
    **/
    @AuraEnabled
    public static Map<String, List<Object>> getListPlaces(String zipCode) {
        return MX_SB_VTS_ListPlaces_Service.getGeolocation(zipCode);
    }
    
    /**
    * @description : Call an web service apex method from a Service class
    * @Param Map<String,Object> dataNeighborhood
    * @Return List<Object>
    **/
    @AuraEnabled
    public static List<Object> getNeighborhood(Map<String,Object> dataNeighborhood) {
        return MX_SB_VTS_Neighborhood_Service.getNeighborhood(dataNeighborhood);
    }

}