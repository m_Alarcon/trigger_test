/**
* @File Name          : MX_BPP_AccountFiltr_Ctrl_Test.cls
* @Description        : Test class for MX_BPP_AccountFiltr_Ctrl
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 23/10/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      23/10/2020            Gabriel Garcia Rojas          Initial Version
**/
@isTest
private class MX_BPP_AccountFiltr_Ctrl_Test {
    /** error message */
    final static String MESSFAILTEST = 'Fail method';
    /** String Cuentas */
    final static String CUENTASOCTRL = 'Cuentas';
    /** String Todas */
    final static String TODASCTRLO = 'TODAS';
	/*Usuario de pruebas*/
    private static User testUserStartACtr = new User();
    /** namerole variable for records */
    final static String NAME_ROLECTRLA = '%BPYP BANQUERO BANCA PERISUR%';
    /** nameprofile variable for records */
    final static String NAME_PROFILESACTR = 'BPyP Estandar';
    /** name variable for records */
    final static String NAMESTCTRLA = 'testUserAcc';
    /** namedivision variable for records */
    final static String NAMECOT_DIVCTRLA = 'METROPOLITANA';
    /** name variable for oficina */
    final static String PRIVCTRLA ='PRIVADO';
    /** name variable for records */
    final static String NAMECOT_OFFCTRLA = '6343 PEDREGAL';


    /*Setup para clase de prueba*/
    @testSetup
    static void setupAccCtrl() {

        testUserStartACtr = UtilitysDataTest_tst.crearUsuario(NAMESTCTRLA, NAME_PROFILESACTR, NAME_ROLECTRLA);
        testUserStartACtr.Title = PRIVCTRLA;
        testUserStartACtr.Divisi_n__c = NAMECOT_DIVCTRLA;
        testUserStartACtr.BPyP_ls_NombreSucursal__c = NAMECOT_OFFCTRLA;
        testUserStartACtr.VP_ls_Banca__c = 'Red BPyP';
        insert testUserStartACtr;

        final Id rtAccCtrl = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta BPyP').getRecordTypeId();

        final Account cuentaACtrl = new Account(FirstName = 'CuentaTest', LastName = 'LastTest', No_de_cliente__c = '12345678', RecordTypeId = rtAccCtrl, OwnerId = testUserStartACtr.Id );
        insert cuentaACtrl;
    }

    /**
    * @description constructor test
    * @author Gabriel Garcia | 23/10/2020
    * @return void
    **/
    @isTest
    private static void testConstructorCtrlA() {
        final MX_BPP_AccountFiltr_Ctrl instTestAcc = new MX_BPP_AccountFiltr_Ctrl();
        System.assertNotEquals(instTestAcc, null, MESSFAILTEST);
    }

    /**
     * @description test fetchDataOpp
     * @author Gabriel Garcia | 23/010/2020
     * @return void
     **/
    @isTest
    private static void fetchDataOppTest() {
        Test.startTest();
        final MX_BPP_AccountFiltr_Service.WRP_ChartStacked wrpCharTestCtrl = MX_BPP_AccountFiltr_Ctrl.fetchDataAcc( new List<String>{CUENTASOCTRL, 'Division', ''} );
        System.assertNotEquals(wrpCharTestCtrl, null, MESSFAILTEST);

        Test.stopTest();
    }

    /**
     * @description test fetchAcc
     * @author Gabriel Garcia | 23/010/2020
     * @return void
     **/
    @isTest
    private static void fetchAccTest() {
        Test.startTest();
        final List<Account> listAccCtrlTest = MX_BPP_AccountFiltr_Ctrl.fetchAcc( new List<String>{CUENTASOCTRL, 'Division', '', '10'} );
        System.assertNotEquals(listAccCtrlTest, null, MESSFAILTEST);

        Test.stopTest();
    }

    /**
     * @description test fetchPgs
     * @author Gabriel Garcia | 23/010/2020
     * @return void
     **/
    @isTest
    private static void fetchPgsTest() {
        Test.startTest();
        final Integer totalpage = MX_BPP_AccountFiltr_Ctrl.fetchPgs(20);
        System.assertNotEquals(totalpage, 0, MESSFAILTEST);

        Test.stopTest();
    }

    /**
     * @description test fetchInfoByUserAcc
     * @author Gabriel Garcia | 23/10/2020
     * @return void
     **/
    @isTest
    private static void fetchInfoByUserTest() {
        final User usuarioCtrlA = [SELECT Id FROM User WHERE Name=: NAMESTCTRLA];
        System.runAs(usuarioCtrlA) {
            final MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper wrapperCtrlAcc = MX_BPP_AccountFiltr_Ctrl.fetchInfoByUserAcc();
            System.assertNotEquals(wrapperCtrlAcc, null, MESSFAILTEST);
        }
    }

    /**
     * @description test fetchUserInfoAcc
     * @author Gabriel Garcia | 23/10/2020
     * @return void
     **/
    @isTest
    private static void fetchUserInfoTestA() {
        final List<String> listinfoUserAcc = MX_BPP_AccountFiltr_Ctrl.fetchUserInfoAcc();
        System.assertNotEquals(listinfoUserAcc.size(), 0 , MESSFAILTEST);
    }

}