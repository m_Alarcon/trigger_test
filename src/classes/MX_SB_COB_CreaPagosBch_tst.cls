/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_COB_CreaPagosBch_tst
* Autor Angel Nava
* Proyecto: Cobranza - BBVA Bancomer
* Descripción : Clase para pruebas de MX_SB_COB_CreaPagosBch_cls
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Desripción
* --------------------------------------------------------------------------------
* 1.0           01/05/2019     Angel Nava				          Creación
* --------------------------------------------------------------------------------
*/
@isTest
public class MX_SB_COB_CreaPagosBch_tst {

	@Testsetup
    static void setObjetos() {
        final Bloque__c bloque = new Bloque__c();
        insert bloque;
        final CargaArchivo__c archivo = new CargaArchivo__c(MX_SB_COB_Bloque__c=bloque.name,MX_SB_COB_ContadorConError__c=0,MX_SB_COB_ContadorSinError__c=0,MX_SB_COB_resultado__c='');
        insert archivo;
        final pagosespejo__c pago = new pagosespejo__c(MX_SB_COB_BloqueLupa__c =bloque.id,MX_SB_COB_factura__c='1233',MX_SB_COB_InsertCreaPago__c=false);
        insert pago;
    }

    @isTest
    static void creaPagos() {
        test.startTest();
        String queryQ = 'select MX_SB_COB_InsertCreaPago__c, id ';
        queryQ+= 'from PagosEspejo__c';
        final MX_SB_COB_CreaPagosBch_cls batch = new MX_SB_COB_CreaPagosBch_cls(queryQ);
        final Id batchInstanceId = Database.executeBatch(batch, 200);
       system.assertNotEquals(null, batchInstanceId);
        test.stopTest();
    }
}