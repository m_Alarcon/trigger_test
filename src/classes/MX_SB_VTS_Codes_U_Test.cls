/**
* @File Name          : MX_SB_VTS_Codes_U_Test.cls
* @Description        :
* @Author             : Diego Olvera
* @Group              :
* @Last Modified By   : Diego Olvera
* @Last Modified On   : 22/10/2020 12:43:02
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    7/5/2020   Diego Olvera     Initial Version
**/
@isTest
public class MX_SB_VTS_Codes_U_Test {
    /** Name User */
    final static String USERNAMECODE = 'test2';
    /** Name OKCODE */
    final static Integer OKCODES = 200; 
     /** Name EMPTYSTRING */
    final static String EMPTYSTRS = ''; 
    /* @Method: makeData
* @Description: create test data set
*/
    @TestSetup
    public static void makeData() {
        final User testUserCode = MX_WB_TestData_cls.crearUsuario(USERNAMECODE, System.Label.MX_SB_VTS_ProfileAdmin);
        Insert testUserCode;
    }
    /* @Method: testGetAmountOpps
* @Description: return json values of fake response
*/
    @isTest
    private static void testGetCodes() {
        final User  cUser=[SELECT ID,Name FROM User  WHERE NAME =: USERNAMECODE LIMIT 1];
        final Map<Integer, String> statusCode = new Map<Integer, String>();
        statusCode.put(OKCODES, 'Error al validar la información de salida: {0}.');
        System.runAs(cUser) {
            statusCode.get(OKCODES);
            Map<String, Object> test = MX_SB_VTS_Codes_Utils.statusCodes(OKCODES);
     		test = new Map<String, Object>{'code' => OKCODES, 'description' => EMPTYSTRS};
            System.assertNotEquals(test, null,'El mapa(S) esta vacio');
        }
    }
}