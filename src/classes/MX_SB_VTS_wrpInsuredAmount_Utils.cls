/**
* @FileName          : MX_SB_VTS_wrpInsuredAmount_Utils
* @description       : Wrapper class for use de web service of calculateSuggestAmount
* @Author            : Marco Antonio Cruz Barboza  
* @last modified on  : 26-12-2020
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   26-12-2020   Marco Antonio Cruz Barboza          Initial Version
**/
@SuppressWarnings('sf:LongVariable, sf:ShortVariable, sf:VariableDeclarationHidesAnother')
public class MX_SB_VTS_wrpInsuredAmount_Utils {
    /** Wrapper Call*/
    public suggestedAmount suggestedAmount {get;set;}
    /** Wrapper Call List*/
    public suggestedCoverages[] suggestedCoverages {get;set;}
    
    /**
    * @description : Object suggestedAmount to call a web service in an array
    * @Param 
    * @Return
    **/
    public class suggestedAmount {
        /**
        * @description : Suggested Amount
        **/
        public String amount {get;set;}
        /**
        * @description : Suggested Amount Currency
        **/
        public String currencies {get;set;}
    }
    
    
    /**
    * @description : Object suggestedCoverages to call a web service in an array
    * @Param 
    * @Return
    **/
    public class suggestedCoverages {
        /**
        * @description : Suggested Coverages id
        **/
        public String id {get;set;}
        /**
        * @description : SuggestedAmount Object
        **/
        public suggestedAmount suggestedAmount {get;set;}
    }

}