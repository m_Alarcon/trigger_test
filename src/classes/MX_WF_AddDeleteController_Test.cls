/**
* ------------------------------------------------------------------------------
* @Nombre: MX_WF_AddDeleteController
* @Autor: Sandra Ventura García
* @Proyecto: Workflow Campañas
* @Descripción : Clase test de controller para componentes MX_WF_TablaDinamicaPadre
* ------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ------------------------------------------------------------------------------
* 1.0           10/11/2019     Sandra Ventura García	       Creación
* ------------------------------------------------------------------------------
*/
@isTest
public with sharing class MX_WF_AddDeleteController_Test {
/**
  * @description: test constructor 
  * @author Sandra Ventura
  */   
   @isTest static void testConstructortemas() {
        String errorM = '';
        test.startTest();
        try {
	        final MX_WF_AddDeleteController temas = new MX_WF_AddDeleteController();
            system.debug(temas);
        } catch (Exception e) { errorM = e.getMessage(); }
        test.stopTest();
        System.assertEquals('', errorM,'No se encuentran registros');
    }
  /**
  * @description: test lista temas 
  * @author Sandra Ventura
  */
    @isTest static void listtemas() {
      final List<Event> eventTF= MX_RTE_UtilityEventos.createEvento(1);
        final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: eventTF[0].Id].MX_WF_Taskforce__c;
        MX_RTE_UtilityEventos.createTemas(10,IdTaskforce);
        test.startTest();
         final MX_WF_Tema__c[] listtem= MX_WF_AddDeleteController.getTemas(eventTF[0].Id);
         System.assertEquals(listtem.size(),10,'No se encontraron Temas');
        test.stopTest();
        }
  /**
  * @description: test lista contactos invitados recurrentes excepcion 
  * @author Sandra Ventura
  */
   @isTest
    static void listtemasEx() {
        String errorMessage = '';
        final List<Event> eventTF= MX_RTE_UtilityEventos.createEvento(1);
        final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: eventTF[0].Id].MX_WF_Taskforce__c;
        test.startTest();
        try {
	        MX_WF_AddDeleteController.getTemas(IdTaskforce);
        } catch (Exception e) {
            errorMessage = e.getMessage();
            System.debug(e.getMessage());
        }
        test.stopTest();
        System.assertEquals('Script-thrown exception', errorMessage,'Insert failed');
    }
  /**
  * @description: test de metod0 que obtiene la hora de inicio del evento 
  * @author Sandra Ventura
  */
    @isTest static void testgethora() {
      final List<Event> eventTF= MX_RTE_UtilityEventos.createEvento(1);
        test.startTest();
         final time horainicio = MX_WF_AddDeleteController.getHora(eventTF[0].Id);
         final time initime = Time.newInstance(8, 30, 0, 0);
         System.assertEquals(horainicio,initime,'La hora de inicio no se pudo obtener');
        test.stopTest();
        }
  /**
  * @description: test lista temas 
  * @author Sandra Ventura
  */
    @isTest static void testsavetema() {
      final list<Event> eventTF= MX_RTE_UtilityEventos.createEvento(1);
      final Id idportafolio = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: eventTF[0].Id].MX_WF_Taskforce__c;
      final List<MX_WF_Tema__c> ctema = new List<MX_WF_Tema__c>();
      final Time initime = Time.newInstance(8, 30, 0, 0);
        for(Integer i=0;i<5;i++) {
            final MX_WF_Tema__c temalist = new MX_WF_Tema__c(Name = 'Tema '+i,
                                                             MX_WF_Tiempo_Minutos__c = 20,
                                                             MX_WF_Hora_inicio_Tema__c= initime.addMinutes(15),
                                                             MX_WF_Taskforce__c = idportafolio
                                                             );
            
            ctema.add(temalist);
        }
        test.startTest();
         final boolean listtem= MX_WF_AddDeleteController.saveTema(ctema, eventTF[0].Id);
         System.assert(listtem,'Save tema test');
        test.stopTest();
    }
  /**
  * @description: test lista temas 
  * @author Sandra Ventura
  */
    @isTest static void listinvitado() {
      final List<Event> evenTF= MX_RTE_UtilityEventos.createEvento(1);
      final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: evenTF[0].Id].MX_WF_Taskforce__c;
      final Id IdMinuta = [SELECT Id FROM MX_WF_Minuta_Taskforce__c WHERE MX_WF_Taskforce__c=: IdTaskforce].id;
      MX_RTE_UtilityEventos.createInvitados(IdTaskforce);
        test.startTest();
         final MX_WF_Invitados_Taskforce__c[] listtem= MX_WF_AddDeleteController.getInvitados(IdMinuta);
         System.assertEquals(listtem.size(),1,'No se encontraron Invitados');
        test.stopTest();
        }
  /**
  * @description: test lista invitados recurrentes asistencia 
  * @author Sandra Ventura
  */
    @isTest static void listinvitadoEx() {
      String errorMessage = '';
      final List<Event> evenTF= MX_RTE_UtilityEventos.createEvento(1);
        final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: evenTF[0].Id].MX_WF_Taskforce__c;
      MX_RTE_UtilityEventos.createInvitados(IdTaskforce);
        test.startTest();
        try {
             MX_WF_AddDeleteController.getInvitados(IdTaskforce);
        } catch (Exception e) {
            errorMessage = e.getMessage();
            System.debug(e.getMessage());
        }
        test.stopTest();
        System.assertEquals('Script-thrown exception', errorMessage,'List error');
    }
  /**
  * @description: test actualiza asistencia de invitados 
  * @author Sandra Ventura
  */
    @isTest static void testsaveInvitado() {
      final List<Event> evenTF= MX_RTE_UtilityEventos.createEvento(1);
      final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: evenTF[0].Id].MX_WF_Taskforce__c;
      final List<MX_WF_Invitados_Taskforce__c> listinv = MX_RTE_UtilityEventos.createInvitados(IdTaskforce);
      test.startTest();
       final boolean invrecurr = MX_WF_AddDeleteController.saveInvitado(listinv);
      test.stopTest();
       System.assert(invrecurr, 'Actualización asistencia');
        }
  /**
  * @description: test envío de plantilla a invitados exception
  * @author Sandra Ventura
  */
    @isTest static void testPlantillaEx() {
        test.startTest();
        try {
	          final boolean  sendemailtemp = MX_WF_AddDeleteController.sendTemplatedEmail('Invalid-id');
              System.assert(!sendemailtemp,'El mensaje no pudo ser enviado.');
            } catch (Exception e) {
            System.debug(e.getMessage());
            }
        test.stopTest();
        }
  /**
  * @description: test envío de plantilla a invitados
  * @author Sandra Ventura
  */
    @isTest static void testPlantilla() {
      final List<Event> evenTF= MX_RTE_UtilityEventos.createEvento(1);
        test.startTest();
         final boolean  sendemailtemp = MX_WF_AddDeleteController.sendTemplatedEmail(evenTF[0].Id);
        System.assert(sendemailtemp, 'La plantilla no pudo ser enviada');
        test.stopTest();
        }
}