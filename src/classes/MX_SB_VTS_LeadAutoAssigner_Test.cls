/**-------------------------------------------------------------------------
* Nombre: MX_SB_VTS_Hogar_Service_Test
* @author Jaime (JT)
* Proyecto: MX_Ventas - BBVA Bancomer
* Descripción : Clase test que sirve para la cobertura de la clase MX_SB_VTS_LeadAutoAssigner
*
* --------------------------------------------------------------------------
*                         Fecha           Autor                   Desripción
* --------------------------------------------------------------------------
* @version 1.0           19/07/2019      Jaime T                   Creación
* @version 1.0           27/08/2019      Jaime T                   Codesmell
* @version 1.1           12/10/2020      Adrian Cruz               Se Eliminan referencias de campo RFC
* --------------------------------------------------------------------------*/
@isTest
private class MX_SB_VTS_LeadAutoAssigner_Test {
    /**
    * Test setup method
     */
	@testSetup
    public static void makeData() {
       	final User usr = MX_WB_TestData_cls.crearUsuario('Auto Assigner', system.label.MX_SB_VTS_ProfileAdmin);        
        Insert usr;

        System.runAs(usr) {
            final Lead leadToAssign = MX_WB_TestData_cls.createLead('Lead Auto Assigner');
            insert leadToAssign;

            final Account accToAssign = MX_WB_TestData_cls.crearCuenta('Doe', 'PersonAccount');
            accToAssign.PersonEmail = 'test@test.com.leas';
            insert accToAssign;

            final Opportunity oppToAssign = MX_WB_TestData_cls.crearOportunidad('Opp Auto Assigner', accToAssign.Id, usr.Id, 'ASD');
            oppToAssign.Producto__c = 'Hogar';
            oppToAssign.Reason__c = 'Venta';
            insert oppToAssign;
        }
    }
    
    @isTest
    private static void coverageOpp() {
        Opportunity lstOpps = [select id, MX_SB_VTS_Tipificacion_LV1__c, MX_SB_VTS_Tipificacion_LV2__c from opportunity limit 1];
        lstOpps.MX_SB_VTS_Tipificacion_LV1__c='Contacto';
        lstOpps.MX_SB_VTS_Tipificacion_LV2__c='Contacto efectivo';
        lstOpps.MX_SB_VTS_Tipificacion_LV3__c='Interesado';
        lstOpps.MX_SB_VTS_Tipificacion_LV4__c='Acepta cotización';
        Test.startTest();
        MX_SB_VTS_LeadAutoAssigner.pendEfectivo(lstOpps);
        System.assert(lstOpps!=null,'Success');
        Test.stopTest();
    }
    
     @isTest
    private static void coverageLead() {
        Lead lead = [select id, Resultadollamada__c, MX_SB_VTS_Tipificacion_LV2__c from Lead limit 1];
        lead.Resultadollamada__c='Contacto';
        lead.MX_SB_VTS_Tipificacion_LV2__c='Contacto efectivo';
        lead.MX_SB_VTS_Tipificacion_LV3__c='Interesado';
        lead.MX_SB_VTS_Tipificacion_LV4__c='Acepta cotización';
        Test.startTest();
        MX_SB_VTS_LeadAutoAssigner.pendEfectivo(lead);
        System.assert(lead!=null,'Success');
        Test.stopTest();
    }
}