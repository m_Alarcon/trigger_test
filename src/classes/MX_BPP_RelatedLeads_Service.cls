/**
 * @File Name          : MX_BPP_RelatedLeads_Service.cls
 * @Description        : Service de LWC MX_BPP_RelatedLeads
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 16/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      16/06/2020           Jair Ignacio Gonzalez G.   Initial Version
**/
public without sharing class MX_BPP_RelatedLeads_Service {

    /*Administrador string variable*/
    private static String adminEsp = 'Administrador del sistema';
    /*Administrator string variable*/
    private static String admin = 'System Administrator';
    /*Estandar string variable*/
    private static String estandar = 'BPyP Estandar';
    /*Estandar string variable*/
    private static String bankAsoc = 'BPyP Banquero Asociado';
    /*Soporte string variable*/
    private static String soporte = 'Soporte BPyP';

    /**
     * @Method MX_BPP_RelatedLeads_Service
     * @Description Singletons
    **/
    @TestVisible private MX_BPP_RelatedLeads_Service() {
    }
    /**
     * @Method serConvertLeads
     * @param Id idLead
     * @Description Convierte un Lead
     * @return Map<String,String>
    **/
    public static Map<String,String> serConvertLeads(id idLead) {
        return MX_BPP_ConvertLeads_Service.convertToLead(new List<Id>{idLead});
    }

    /**
     * @Method serRelatedLeads
     * @param string idAccount
     * @Description Lista de Leads asociados a la Cuenta
     * @return List<CampaignMember>
    **/
    public static List<CampaignMember> serRelatedLeads(string idAccount) {
        final List<CampaignMember> lsLead = MX_RTL_CampaignMember_Selector.getListCampaignMemberByDatabase('LeadId, Lead.isConverted, Lead.Status, Campaign.MX_WB_Producto__r.Name, Lead.LeadSource,'+
                                ' Lead.Comentarios__c, Campaign.MX_WB_FamiliaProductos__r.Name, Lead.MX_LeadAmount__c, MX_LeadEndDate__c, Lead.Description, Lead.Name',
                                'WHERE Lead.IsConverted = FALSE AND Lead.MX_WB_RCuenta__c =\''+idAccount+'\'');
        for (CampaignMember oCampMemb : lsLead) {
            oCampMemb.Lead.Comentarios__c = '/'+oCampMemb.LeadId;
        }
        return lsLead;
    }

    /**
     * @Method serButtonMenu
     * @param Id idUser
     * @Description Regresa si el usuario debe de ver los botones o no
     * @return Boolean
    **/
    public static Boolean serButtonMenu(Id idUser) {
        String queryfield, conditionField;
        queryfield = ' Id, UserRole.DeveloperName, Name, DivisionFormula__c, BPyP_ls_NombreSucursal__c, Profile.Name ';
        final Set<Id> setId = new Set<Id>{idUser};
        conditionField = ' Id IN: setId ';
        final User oUser = MX_RTL_User_Selector.resultQueryUser(queryfield, conditionField, true, setId)[0];
        return oUser.Profile.Name == adminEsp || oUser.Profile.Name == admin || oUser.Profile.Name == estandar || oUser.Profile.Name == soporte || oUser.Profile.Name == bankAsoc;
    }
}