/**
 Nombre: MX_SB_SAC_AgregarSlasCls
 @author   Karen Sanchez (KB)
 Proyecto: MX_SB_SAC - BBVA Bancomer
 Descripción : Clase que busca el sla con base al motivo del caso para guardarlo en los registros de asignación ejecutado
                a traves de un process builder

            No.    Fecha            Autor             Description

 @version  1.0   2019-04-15      Karen Belem (KB)       Creación
 @version  1.1   2020-01-28      Michelle Valderrama    Version added for Entitlements
 @version  1.2   2020-02-25      Miguel Hernández       Ramo agregado para Entitlements
 @version  1.3   2021-03-18      Alonso Ortiz           Se refactoriza version added for Entitlements
*/
public with sharing class MX_SB_SAC_AgregarSlasCls {//NOSONAR
    /** Arreglo de ramos */
    final static String[] RAMOS = System.Label.MX_SB_PV_Ramos.split(',');
    /** Arreglo de motivos */
    final static String[] MOTIVOS = System.Label.MX_SB_PV_Motivos.split(',');
    /** Valores válidos de Cancelación */
    final static List<String> ENT_CANCELACION = new List<String>{'Incidencia', 'Niveles BO 2', 'Niveles BO'};
    /** String RECORD_TYPE */
    final static String RECORD_TYPE = 'MX_SB_SAC_RTLBBVAVida';
    /** String INFO_ADICIONA */
    final static String INFO_ADICIONAL = 'Sin documentos y no implica devolución';
    /** String CANCELACION */
    final static String CANCELACION = 'Cancelación';
  
   /**
   Función: Ejecuta desde un process builder para asignar los SLAS
   2019-04-15
   Karen Belem Sanchez Ruiz
   */
   @InvocableMethod(label='mAgregaSLA' description='Agrega el sla a las asignaciones correspondientes')
   public static void mAgregaSLA (List<string> lstCasesIds) {
       mAsignaSLA(lstCasesIds);
   }
    /**
    * @description 
    * @author Karen Belem Sanchez Ruiz | 03-19-2021 
    * @param lstCasesIds 
    **/
    public static void mAsignaSLA (List<String> lstCasesIds) {
        List<Case> lstCaso  = new List<Case>();
        final String idForUpdate = lstCasesIds[0];
        lstCaso = [SELECT Id, EntitlementId, Entitlement.slaProcessId, Reason,MX_SB_SAC_Ramo__c, MX_SB_SAC_Detalle__c, MX_SB_SAC_Detalles_Vida__c, 
                            MX_SB_SAC_InformacionAdicional__c, RecordType.DeveloperName, MX_SB_SAC_EstatusBO__c, MX_SB_SAC_Nivel_BO__c, MX_SB_SAC_Estatus_otra_area__c
                    FROM Case
                    WHERE Id =: idForUpdate];
        String nombreEntitlement;
        if (!lstCaso.isEmpty()) { 
            nombreEntitlement = asignarNombreEntitlement(lstCaso[0]);
        } 
        final List<SlaProcess> argSlaAsignado = [SELECT Id, Name FROM SlaProcess WHERE Name =: nombreEntitlement AND IsActive = True AND IsVersionDefault = True LIMIT 1];
        if (!argSlaAsignado.isEmpty()) {
            final Entitlement objEntitlement = new Entitlement();
            objEntitlement.Id = lstCaso[0].EntitlementId;
            objEntitlement.slaProcessId = argSlaAsignado[0].Id;
            Database.update(objEntitlement, false);
        }
    }
    /**
    * @description 
    * @author Alonso Ortiz | 03-19-2021 
    * @param entCase 
    * @return String 
    **/
    public static String asignarNombreEntitlement(Case entCase) { 
        String nombreEntitlement;
        if(RAMOS.contains(entCase.MX_SB_SAC_Ramo__c) && !System.Label.MX_SB_PV_R1500_Auto.equals(entCase.MX_SB_SAC_Ramo__c)) {
            nombreEntitlement = entCase.Reason == CANCELACION ? cancelacionAuto(entCase) : entCase.Reason;
        } else if(System.Label.MX_SB_PV_R1500_Auto.equals(entCase.MX_SB_SAC_Ramo__c) && !MOTIVOS.contains(entCase.Reason)) {
            nombreEntitlement = entCase.Reason;
        } else {
            nombreEntitlement = entCase.Reason == CANCELACION ? cancelacionVida(entCase) : entCase.Reason + ' ' +entCase.MX_SB_SAC_Ramo__c;
        }
        return nombreEntitlement;
    }
    /**
    * @description 
    * @author Alonso Ortiz | 03-19-2021 
    * @param entCase 
    * @return String 
    **/
    public static String cancelacionAuto(Case entCase) { 
        String nombreEntitlement = entCase.Reason;
        if(String.isNotBlank(entCase.MX_SB_SAC_EstatusBO__c) && String.isNotBlank(entCase.MX_SB_SAC_Nivel_BO__c) && String.isNotBlank(entCase.MX_SB_SAC_Estatus_otra_area__c)) {
            nombreEntitlement = ENT_CANCELACION[2];
        }
        if(String.isNotBlank(entCase.MX_SB_SAC_Detalle__c) && entCase.MX_SB_SAC_InformacionAdicional__c == INFO_ADICIONAL) {
            nombreEntitlement = ENT_CANCELACION[0];
        }
        return nombreEntitlement;
    }
    /**
    * @description 
    * @author Alonso Ortiz | 03-19-2021 
    * @param entCase 
    * @return String
    **/
    public static String cancelacionVida(Case entCase) { 
        String nombreEntitlement = entCase.Reason + ' ' + entCase.MX_SB_SAC_Ramo__c;
        if(String.isNotBlank(entCase.MX_SB_SAC_Detalles_Vida__c)) {
            switch on entCase.MX_SB_SAC_Detalles_Vida__c {
                when 'Duplicidad', 'Venta' {
                    nombreEntitlement = ENT_CANCELACION[1];
                } 
                when 'Cobranza', 'Economía personal', 'Mal servicio', 'Meses sin intereses', 'No especifica motivo', 'Otra aseguradora', 'Malas ventas' {
                    if(entCase.RecordType.DeveloperName == RECORD_TYPE) {
                        nombreEntitlement = ENT_CANCELACION[1];
                    } 
                }
            }
        }
        return nombreEntitlement;
    }
}