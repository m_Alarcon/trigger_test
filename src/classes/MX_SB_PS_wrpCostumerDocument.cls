/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpCostumerDocument
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
@SuppressWarnings('sf:ShortVariable')
public class  MX_SB_PS_wrpCostumerDocument {
     /*Public property for wrapper*/
	public String productId {get;set;}	
	  /*Public property for wrapper*/
	public String version {get;set;}	
	  /*Public property for wrapper*/
	public String documentType {get;set;}
      /*Public property for wrapper*/	
	public listContent[] listContent {get;set;}
	  /*Public constructor for wrapper*/
    public MX_SB_PS_wrpCostumerDocument() {
		productId = '';
		version = '';
		documentType = '';
        listContent = new listContent[] {};
		listContent.add(new listContent());
    }
	  /*Public subclass for wrapper*/
	public class listContent {
	  /*Public property for wrapper*/
		public String elementName {get;set;}	
		  /*Public property for wrapper*/
		public listData[] listData {get;set;}
		  /*Public subConstructor for wrapper*/
        public listContent() {
			elementName = '';
            listData = new listData[] {};
			listData.add(new listData());
        }
	}
	  /*Public subclass for wrapper*/
	public class listData {
	  /*Public property for wrapper*/
		 public string id {get;set;} 	
	  /*Public property for wrapper*/	
		public String name {get;set;}
		 /*public constructor subclass*/
		public listData() {
		 this.id = '';
		 this.name = '';
		}
	}
}