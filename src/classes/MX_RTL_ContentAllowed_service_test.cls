/**
* ---------------------------------------------------------------------------------
* @Autor: Rodrigo Amador Martinez Pacheco
* @Proyecto: Auditoria
* @Descripción : Pruebas unitarias MX_RTL_ContentAllowed_service
* ---------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ---------------------------------------------------------------------------------
* 1.0           18/06/2020     Rodrigo Martinez               Creación de la clase
* ---------------------------------------------------------------------------------
*/
@istest
public class MX_RTL_ContentAllowed_service_test {
    
    /**
    * @Autor: Rodrigo Amador Martinez Pacheco
    * @Descripción : Nombre de archivo no permitido
    */
    private static final String EXT_NO_EXISTENTE = 'ejemplo.exe';
    /**
    * @Autor: Rodrigo Amador Martinez Pacheco
    * @Descripción : Nombre de archivo permitido
    */
    private static final String EXT_EXISTENTE = 'ejemplo.pdf';
    
    /**
    * @Autor: Rodrigo Amador Martinez Pacheco
    * @Descripción : validateFileExtension cuando es un archivo no valido
    */
    @istest
    public  static void validateExtensionFileNoExistenteTest() {
        Boolean pruebaValida = false;
        try {
         MX_RTL_ContentAllowed_service.validateFileExtension(getContentVersion(false,EXT_NO_EXISTENTE));
		} catch(IllegalArgumentException ex) {
            pruebaValida = true;
        }  
        System.assert(pruebaValida,'Como la extension no es valida lanza excepcion');
    }
    /**
    * @Autor: Rodrigo Amador Martinez Pacheco
    * @Descripción : validateFileExtension cuando es un archivo es valido
    */      
    @istest
    public  static void validateExtensionFileExistenteTest() {
       MX_RTL_ContentAllowed_service.validateFileExtension(getContentVersion(false,EXT_EXISTENTE));
       System.assert(true,'La extension del archivo se valida correctamente sin fallos');                                   
    }
    /**
    * @Autor: Rodrigo Amador Martinez Pacheco
    * @Descripción : Obtiene un objeto del tipo content version con el nombre 
    * del parametro fileName y si huge es true lo genera con un tamanio mayor al 
    * maximo permitido
    */ 
    public static ContentVersion getContentVersion(Boolean huge, String fileName) {
        final ContentVersion contentv = new ContentVersion();
        contentv.PathOnClient = fileName;
        contentv.Title = 'Test';
        contentv.IsMajorVersion = true;
        contentv.ContentLocation = 'S';
        if(huge) {
             contentv.VersionData=  generateBigBlob();
        } else {
	        contentv.VersionData=  Blob.valueof('Some random String');
        }
        return contentv;
    }
    /**
    * @Autor: Rodrigo Amador Martinez Pacheco
    * @Descripción : Genera un dato blob mayor al limite permitido
    */ 
    public static Blob generateBigBlob() {
        final Decimal maximum = MX_RTL_Maximun_File_Size_selector.selectMaximumFileSizeAllowed();
        final Decimal bytesAllowed = MX_RTL_ContentAllowed_service.getBytesAllowed(maximum);
        String bigString = EXT_NO_EXISTENTE;
        while(bigString.length() <= bytesAllowed) {
            bigString += bigString;
        }
        return  Blob.valueof(bigString);
    }
    /**
    * @Autor: Rodrigo Amador Martinez Pacheco
    * @Descripción : validateFileSize con valores permitidos
    */ 
    @istest              
    public static void validateFileSizeAllowedTest() {
        MX_RTL_ContentAllowed_service.validateFileSize(getContentVersion(false,EXT_EXISTENTE));
        System.assert(true,'Como el tamaño del archivo es muy pequeño la validación pasa correctamente');
    }
    /**
    * @Autor: Rodrigo Amador Martinez Pacheco
    * @Descripción : Genera un listado de documentos con valores correctos
    * para probar el happy path
    */ 
    @istest
    public static void validateFileTest() {
        final List<ContentVersion> newContent= generateContentVersionList();
        newContent.add(getContentVersion(false,EXT_NO_EXISTENTE));
        MX_RTL_ContentAllowed_service.validateFile(newContent);
        System.assert(true,'El listado de archivos tienen extension y tamaño validos , por lo cual termina correctamente');
        }
    /**
    * @Autor: Rodrigo Amador Martinez Pacheco
    * @Descripción : Regresa un listado de 200 archivos con valores permitidos
    */ 
    public static List<ContentVersion> generateContentVersionList() {
        final List<ContentVersion> contents = new List<ContentVersion>();
        for(Integer i=0; i<200;i++) {
            contents.add(getContentVersion(false,EXT_EXISTENTE));
        }
        return contents;
    }
    /**
    * @Autor: Rodrigo Amador Martinez Pacheco
    * @Descripción : Regresa un listado de 200 archivos con valores permitidos
    */ 
     @istest
    public static void testTrigger() {
        Test.startTest();
        boolean pruebaValida = false;
        final ContentVersion contentv = getContentVersion(false,EXT_EXISTENTE);
        try {
        insert contentv;
        Test.setCreatedDate(contentv.Id, DateTime.now());
        } catch(Exception ex) {
            pruebaValida = true;
        }
        Test.stopTest();
        System.assert(pruebaValida,'Trigger handler se ejecuta correctamente');
    }
    

}