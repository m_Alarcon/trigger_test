/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 10-09-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   09-17-2020   Eduardo Hernández Cuamatzi   Initial Version
 * 1.1   03-17-2021   Vincent Juarez               Se añaden variabled globales
 * 1.2   03-17-2021   Juan Carlos Benitez Herrera  Se refactoriza methodo validCallog por npath
**/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_SB_RTL_PureCloudExtends_Service {
    /** */
    final static String QUOTEFIELDS = 'Id, OpportunityId, MX_ASD_PCI_ResponseCode__c, MX_ASD_PCI_MsjAsesor__c, MX_ASD_PCI_DescripcionCode__c';
    /** Campos de Oppotunity */
    final static String OPPFIELDS = 'Id, Campanya__c';
    /*Variable que almacena el codigo de error*/
    private static String responseCode ='';
    /*Variable que almacena el el msj de asesor*/
    private static String msjAsesor ='';
    /*Variable que almacena la descripcion*/
    private static String descripCode ='';
    /*Variable que almacena ultimos digitos de la tarjeta*/
    private static String numeroTarjeta ='';
    /*Variable que almacena la hora de Operacion*/
    private static String horaOperacion ='';
    /*Variable que almacena el folio de Operación*/
    private static String folioOperacion ='';
    /*Variable que almacena la fecha de ZOperacion*/
    private static String fechaOperacion ='';
    /**
    * @description Procesa Log
    * @author Eduardo Hernandez Cuamatzi | 10-02-2020 
    * @param Map<String Object> mapLog Log parseado a Mapa
    * @return String Id de registro trabajado
    **/
    public static String retriveLogId(Map<String, Object> mapLog) {
        final Map<String, Object> callLog = ( Map<String, Object>)mapLog.get('callLog');
        String logId = '';
        if (validateOpenCode('MX_EU_ComentariosEC__c', callLog) || validateOpenCode('id', callLog)) {
            final String recordId = callLog.get('MX_WB_idGrabacion__c') == null ? String.valueOf(callLog.get('id')) : String.valueOf(callLog.get('MX_WB_idGrabacion__c'));
            final Quote quoteUpdate = MX_RTL_Quote_Selector.findQuoteById(recordId, QUOTEFIELDS);
            logId = publishEventBus(quoteUpdate, callLog);
        }
        return logId;
    }

    /**
    * @description Valida entrada y valor de llave del mapa de log
    * @author Eduardo Hernandez Cuamatzi | 10-02-2020 
    * @param String fieldFinding Campo a validar
    * @param Map<String Object> callLog Log parseado a Mapa
    * @return Boolean Resultado de validación
    **/
    public static Boolean validateOpenCode(String fieldFinding, Map<String, Object> callLog) {
        Boolean isOpenExist = false;
        if (callLog.containsKey(fieldFinding) && String.isNotBlank(String.valueOf(callLog.get(fieldFinding)))) {
            isOpenExist = true;
        }
        return isOpenExist;
    }

    /**
    * @description Actualiza registro y publica evento
    * @author Eduardo Hernandez Cuamatzi | 10-02-2020 
    * @param Quote quoteUpdate Registro ah actualizar
    * @param Map<String Object> callLog Log parseado a Mapa
    * @return String Id de registro trabajado
    **/
    public static String publishEventBus(Quote quoteUpdate, Map<String, Object> callLog) {
        String logId = '';
        if (String.isEmpty(quoteUpdate.MX_ASD_PCI_ResponseCode__c)) {
            final Opportunity opprelated = MX_RTL_Opportunity_Selector.getOpportunity(quoteUpdate.OpportunityId, OPPFIELDS);
            validCallog(callLog);
            validCallog2(callLog);
            validCallog3(callLog);
            quoteUpdate.MX_ASD_PCI_ResponseCode__c = responseCode;
            quoteUpdate.MX_ASD_PCI_DescripcionCode__c = descripCode;
            quoteUpdate.MX_ASD_PCI_MsjAsesor__c = msjAsesor;
            quoteUpdate.QuoteToCity = numeroTarjeta;
            quoteUpdate.QuoteToCountry = horaOperacion;
            quoteUpdate.AdditionalCity = folioOperacion;
            quoteUpdate.AdditionalState = fechaOperacion;
            opprelated.Campanya__c = callLog.get('Description') == null ? '' : callLog.get('Description').toString();
            final MX_SB_RTL_PureCloud_IVR__e eventIVR = new MX_SB_RTL_PureCloud_IVR__e();
            eventIVR.MX_SB_RTL_DescriptionCode__c = descripCode;
            eventIVR.MX_SB_RTL_ResponseCode__c = responseCode;
            eventIVR.MX_SB_RTL_IdActividad__c = quoteUpdate.Id;
            eventIVR.MX_SB_RTL_MensajeAsesor__c = msjAsesor;
            final Quote quoteUpd = MX_RTL_Quote_Selector.upsrtQuote(quoteUpdate);
            MX_RTL_Opportunity_Selector.upsrtOpp(opprelated);
            logId = quoteUpd.Id;
            EventBus.publish(eventIVR);
        }
        return logId;
    }
    /* Funcion auxiliar para recuperar datos del callog */
    public static void validCallog(Map<String, Object> mpLog) {
        responseCode = mpLog.get('MX_EU_ComentariosEC__c') == null ? '' : mpLog.get('MX_EU_ComentariosEC__c').toString();
        descripCode = mpLog.get('Resultado_llamada__c') == null ? '' : mpLog.get('Resultado_llamada__c').toString();
        msjAsesor = mpLog.get('MX_SB_VTS_Certificada__c') == null ? '' : mpLog.get('MX_SB_VTS_Certificada__c').toString();
    }

    /**
    * @description Funcion auxiliar para recuperar datos del callog
    * @author Vincent Juarez | 18-02-2021 
    * @param mpLog
    * @return void
    **/
    public static void validCallog2(Map<String, Object> mpLog) {
        numeroTarjeta = mpLog.get('MX_SB_VTA_Cliente_acepta_cotizaci_n__c') == null ? '' : mpLog.get('MX_SB_VTA_Cliente_acepta_cotizaci_n__c').toString();
        horaOperacion = mpLog.get('MX_SB_VTA_Cliente_est_interesado__c') == null ? '' : mpLog.get('MX_SB_VTA_Cliente_est_interesado__c').toString();
    }

    /**
    * @description Funcion auxiliar para recuperar datos del callog 
    * @author Juan Carlos Benitez | 18-02-2021 
    * @param mpLog
    * @return void
    **/
    public static void validCallog3(Map<String, Object> mpLog) {
        folioOperacion = mpLog.get('MX_SB_VTA_Resultado_contactoEfectivo__c') == null ? '' : mpLog.get('MX_SB_VTA_Resultado_contactoEfectivo__c').toString();
        fechaOperacion = mpLog.get('MX_SB_VTA_Resultado_del_contacto__c') == null ? '' : mpLog.get('MX_SB_VTA_Resultado_del_contacto__c').toString();
    }
}