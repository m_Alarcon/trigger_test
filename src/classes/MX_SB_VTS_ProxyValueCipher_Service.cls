/**
* @FileName          : MX_SB_VTS_ProxyValueCipher_Service
* @description       : Service class for use de web service of generarOTP
* @Author            : Marco Antonio Cruz Barboza  
* @last modified on  : 18-09-2020
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   18-09-2020   Marco Antonio Cruz Barboza          Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton, sf:AvoidGlobalModifier')
global class MX_SB_VTS_ProxyValueCipher_Service {
    /*Number for encrypted Id's*/
    private static Integer numberId;
    
   	/**Final code 200 response Ok */
    Final static Integer OKAYSTAT = 200;

    /**
    * @description : Call an web service apex method to get Id Cotiza and encrypted.
    * @Param String IdCotiza
    * @Return Map<String, String> with status and code from the result of webservice use.
    **/
    public static Map<String,String> getIdEncrypted(List<String> idCotizas) {
        Final Map<String,String> getValues = new Map<String,String>();
        Final HttpRequest requestCipher = createReq(idCotizas);
        Final Map<String,Object> test = new Map<String,Object>();
        try {
            final HttpResponse getEncryptedId = MX_RTL_IasoServicesInvoke_Selector.callServices('ProxyValueCipher', test, requestCipher);
            if(getEncryptedId.getStatusCode() == OKAYSTAT) {
                Final MX_SB_VTS_wrpEncrypted responseTest = (MX_SB_VTS_wrpEncrypted)JSON.deserialize(getEncryptedId.getBody(), MX_SB_VTS_wrpEncrypted.class);
                numberId = 0;
                for(String iter : responseTest.results) {
                    getValues.put(String.valueOf(numberId), iter);
                    numberId++;
                }
            } else {
                getValues.put('0', 'error');
            }
            
        } catch (System.CalloutException exc) {
            getValues.put('error', String.valueOf(exc.getLineNumber() + ' ' +exc.getMessage() + ' ' + exc.getCause()));
            WB_CrearLog_cls.fnCrearLog(exc.getLineNumber() + ' ' +exc.getMessage() + ' ' + exc.getCause(), 'WebServiceFail', false);
        }


        
        return getValues;
    }
    
    /**
    * @description : Create a request method to send into a webservice
    * @Param List<String> idQuotes
    * @Return HttpRequest to send it into a GenericService.
    **/
    public static HttpRequest createReq (List<String> idQuotes) {
       	Final Map<String,iaso__GBL_Rest_Services_Url__c> rstServices = iaso__GBL_Rest_Services_Url__c.getAll();
        Final iaso__GBL_Rest_Services_Url__c getUrlLP = rstServices.get('ProxyValueCipher');
        Final String endpointService = getUrlLP.iaso__Url__c;
        Final HttpRequest requestPC = new HttpRequest();
        requestPC.setTimeout(120000);
        requestPC.setMethod('POST');
        requestPC.setHeader('Content-Type', 'application/json');
        requestPC.setHeader('Accept', '*/*');
        requestPC.setHeader('Host', 'https://test-sf.bbva.mx/QRVS_A02');
        requestPC.setEndpoint(endpointService);
        requestPC.setBody(bodyRequest(idQuotes));
        return requestPC;
    }
    
    /**
    * @description : Create a body request to send it into a request 
    * @Param List<String> idQuotes
    * @Return HttpRequest to send it into a GenericService.
    **/
    public static String bodyRequest (List<String> idQuotes) {
        return encryVals(idQuotes);
    }
    
    /**
    * @description : Create a body request to send it into a request 
    * @Param List<String> idQuotes
    * @Return HttpRequest to send it into a GenericService.
    **/
    public static String encryVals(List<String> idQuotes) {
        Final List<String> idCompletes = new List<String>();
        for (String iterQuote : idQuotes) {
            idCompletes.add(idCompletes(iterQuote));
        }
        Final JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName('listSecurityFunctions');
        gen.writeStartArray();
        for(String idQuote : idCompletes) {
            gen.writeStartObject();
			gen.writeStringField('valueEncrypt', idQuote);
            gen.writeEndObject();
        }
        gen.writeEndArray();
        gen.writeStringField('mode', 'ENCRYPT_MODE');
        gen.writeEndObject();
        return gen.getAsString();
    }
    
    /**
    * @description : Create a full id quote for webservice
    * @Param String idQuote
    * @Return String to send it a full idQuote
    **/
    public static String idCompletes(String idQuote) {
        Final Integer addZeros = 14 - idQuote.length();
        return '0'.repeat(addZeros) + idQuote + '0000000001';
    }
    
}