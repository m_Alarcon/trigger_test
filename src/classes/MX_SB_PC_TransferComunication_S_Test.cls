/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 10-08-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   09-24-2020   Eduardo Hernández Cuamatzi   Initial Version
**/
@isTest
public class MX_SB_PC_TransferComunication_S_Test {

    @TestSetup
    static void makeData() {
        MX_SB_PC_TransferComunication_Ctrl_Test.initCallCenter();
    }

    @isTest
    static void findUrlCallCenter() {
        Test.startTest();
        final User userCall = [Select Id, CallCenterId from User where Name = 'Asesor callcenter' limit 1];
        final String callCenUser = MX_SB_PC_TransferComunication_Service.findUrlCallCenter(userCall.Id);
        System.assertEquals('https://apps.mypurecloud.com/crm/index.html?crm=salesforce&color=darkblue', callCenUser, 'Callcenter correcto');
        Test.stopTest();        
    }
}