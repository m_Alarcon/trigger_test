/**
* @File Name          : MX_SB_PS_PlanesFormPag_Ctrl.cls
* @Description        :
* @Author             : Juan Carlos Benitez
* @Group              :
* @Last Modified By   : Juan Carlos Benitez
* @Last Modified On   : 7/02/2020, 9:20:03 AM
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    7/02/2020   Juan Carlos Benitez          Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_SB_PS_PlanesFormPag_Ctrl {
    /**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param oppId
* @return List<Opportunity>
**/
    @AuraEnabled
    public static Opportunity loadOppF(String oppId) {
        return MX_SB_PS_Utilities.fetchOpp(oppId);
    }
    /**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param  Opportunity oppObj
**/ 
    @AuraEnabled 
    public static void upsrtOpp(Opportunity oppObj) {
        MX_SB_PS_Utilities.upsrtOpp(oppObj);
    }
    
    /**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param quoteObj
* @return QuoteObj
**/    
    @AuraEnabled
    public static List<Quote> getUpsrtQuote(Quote quoteObj) {
        return MX_RTL_Quote_Service.getUpsquot(quoteObj);
    }
}