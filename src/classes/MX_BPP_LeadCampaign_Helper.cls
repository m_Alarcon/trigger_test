/**
* @File Name          : MX_BPP_LeadCampaign_Service.cls
* @Description        : Clase para el manejo de datos BPyP en el proceso de candidatos de campaña
* @author             : Gabriel Garcia Rojas
* @Group              : BPyP
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 06/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      06/06/2020           Gabriel Garcia Rojas              Initial Version
**/
public without sharing class MX_BPP_LeadCampaign_Helper {

    /** Constructor*/
    @testVisible
    private MX_BPP_LeadCampaign_Helper() { }

    /**
    * @Method getLeadWithNodeCliente
    * @param candidato
    * @param mapRecordType
    * @Description Asignación de Cuenta y Propietario a través del numero de cuenta
    * @return Set<String>
    **/
    public static Set<String> getLeadWithNodeCliente(List<Lead> newList, Map<Id, RecordType> mapRecordType) {
        final Set<String> idExternalCuentas = new Set<String>();
        for(Lead candidato : newList) {
            if(String.isNotBlank(candidato.MX_ParticipantLoad_id__c) && mapRecordType.containsKey(candidato.RecordTypeId)) {
                idExternalCuentas.add(candidato.MX_ParticipantLoad_id__c);
            } else if(String.isBlank(candidato.MX_ParticipantLoad_id__c) && mapRecordType.containsKey(candidato.RecordTypeId)) {
                candidato.addError(Label.MX_BPP_ParticipantLoad_Lead_Empty);
            }
        }
        return idExternalCuentas;
    }

    /**
    * @Method assigendAccountToLead
    * @param newList
    * @param mapCuentas
    * @param mapRecordType
    * @Description Asignación de Cuenta y Propietario a través del numero de cuenta
    * @return void
    **/
    public static void assigendAccountToLead(List<Lead> newList, Map<String, Account> mapCuentas, Map<Id, RecordType> mapRecordType) {
        for(Lead candidato : newList) {
            if(mapCuentas.containsKey(candidato.MX_ParticipantLoad_id__c) && mapRecordType.containsKey(candidato.RecordTypeId)) {
                candidato.FirstName = mapCuentas.get(candidato.MX_ParticipantLoad_id__c).LastName + ' -';
                candidato.MX_WB_RCuenta__c = mapCuentas.get(candidato.MX_ParticipantLoad_id__c).Id;
                candidato.OwnerId = mapCuentas.get(candidato.MX_ParticipantLoad_id__c).OwnerId;
                if(mapCuentas.get(candidato.MX_ParticipantLoad_id__c).Phone != null) {
                    candidato.Phone = mapCuentas.get(candidato.MX_ParticipantLoad_id__c).Phone;
                }
                if(mapCuentas.get(candidato.MX_ParticipantLoad_id__c).PersonEmail != null) {
                    candidato.Email = mapCuentas.get(candidato.MX_ParticipantLoad_id__c).PersonEmail;
                }
            } else if(mapRecordType.containsKey(candidato.RecordTypeId) && !mapCuentas.containsKey(candidato.MX_ParticipantLoad_id__c)) {
                candidato.addError(Label.MX_BPP_ParticipantLoad_Lead_Wrong);
            }
        }
    }

}