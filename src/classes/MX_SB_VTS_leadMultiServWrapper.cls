/**
* @File Name          : MX_SB_VTS_leadMultiServiceVts_cls.cls
* @Description        : 
* @Author             : Arsenio Perez Lopez
* @Group              : 
* @Last Modified By   : Eduardo Hernández Cuamatzi
* @Last Modified On   : 12/6/2020 20:26:24
* @Modification Log   : 
* Ver       Date            Author      		    Modification
* 1.0    9/3/2020   Arsenio Perez Lopez     Initial Version
**/
@SuppressWarnings('sf:ShortVariable')
public class MX_SB_VTS_leadMultiServWrapper {
    /*Clase de acceso a consulta */
    public class MX_sb_consulta {
        /** Tipo de consulta */
        public String type {get; set;}
        /** Canal de consulta */
        public String channel {get; set;}
        /** Producto de la consulta */
        public Mx_sb_product product {get; set;}
        /** Cotizacion de la Consulta  */
        public Mx_sb_quote quote {get; set;}
        /** Cupon de la consulta */
        public String coupon {get; set;}
        /** Persona de la consulta */
        public Mx_sb_person person {get; set;}
        /** Punto de encuentro de la consulta */
        public Mx_sb_appointment appointment {get; set;}
    }
    /** Clase relacionada con el producto  */
    public class Mx_sb_product {
        /** Codigo de la consulta */
        public String code {get;set;}
        /** Plan de la consulta */
        public Mx_sb_plan plan {get; set;}
    }
    /** Clase relacionada con el plan */
    public class Mx_sb_plan {
        /** Codigo del plan */
        public String code {get;set;}
    }
    /** Clase relacionada con la cotizacion */
    public class Mx_sb_quote {
        /** Id de la cotizacion */
        public String id {get;set;}
    }
    /** Clase de la persona de la consulta */
    public class Mx_sb_person {
        /** Nombre de la persona  */
        public String name {get;set;}
        /** Apellido de la persona */
        public String firstSurname {get;set;}
        /** Datos del contacto */
        public List<Mx_sb_Contacdetails> contactDetails {get;set;}
    }
    /** Clase de detalle del contacto */
    public class Mx_sb_Contacdetails {
        /** Key de identificacion del contacto */
        public String key {get;set;}
        /** Valor del contacto */
        public String value {get;set;}
    }
    /** Clase de appoint */
    public class Mx_sb_appointment {
        /** Tiempo de appoint */
        public String timex {get;set;}
    }
}