/*
  @File Name          : MX_SB_VTS_Carrusel_Ctrl.cls
  @Description        : Clase Controladora: Componente Carrusel
  @Author             : jesusalexandro.corzo.contractor@bbva.com
  @Group              : 
  @Last Modified By   : jesusalexandro.corzo.contractor@bbva.com
  @Last Modified On   : 28/7/2020 00:06:00
  @Modification Log   : 
  Ver               Date                Author      		                    Modification
  1.0           	04/6/2020       jesusalexandro.corzo.contractor@bbva.com    Initial Version
*/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_Carrusel_Ctrl {
    /**
     * @description: Recupera los datos para el componente de Carrusel si es Lead u Opportunidad
     * @author: 	 Alexandro Corzo
     * @return: 	 Map<String,List<Object>>
     */
    @AuraEnabled(cacheable=true)
    public static Map<String, List<Object>> getDataComponent(String sModule, String sId) {
    	return MX_SB_VTS_Carrusel_Service.getDataComponentServ(sModule, sId);
    }
    
    /**
     * @description: Actualiza los datos conforme a la SubEtapa seleccionada en el componente
     * @author: 	 Alexandro Corzo
     * @return: 	 Bolean
     */
    @AuraEnabled
    public static Boolean setDataComponent(Map<String, Object> objParams) {
        return MX_SB_VTS_Carrusel_Service.setDataComponentServ(objParams);
    }
    
    /**
     * @description: Recupera los valores del campo de SubEtapa para el componente
     * @author: 	 Alexandro Corzo
     * @return: 	 Map<String,List<Object>>
     */
	@AuraEnabled(cacheable=true)
    public static Map<String, List<Object>> obtDataSubEtapaOppo() {
        return MX_SB_VTS_Carrusel_Service.obtDataSubEtapaOppoServ();
    }
}