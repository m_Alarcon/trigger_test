/**---------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_DetallesServiciosEnviados_cls
* Autor: Juan Carlos Benitez Herrera
* Proyecto: Siniestros - BBVA
* Descripción : Clase controladora de componente MX_SB_MLT_DetallesServiciosEnviados
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                       Desripción
* --------------------------------------------------------------------------------
* 1.0           20/01/2020     Juan Carlos Benitez          Creación
* 1.1			25/01/2020	   Angel Nava	                modificación para envios
* 1.2           20/02/2020     Angel Nava                   codesmells
* --------------------------------------------------------------------------------
**/
public with sharing class MX_SB_MLT_DetallesServiciosEnviados_cls {//NOSONAR
    /**
	*str sustituye string literal
	**/
    public static final String SERVENV ='Servicios Enviados';
    /**
	*str sustituye string literal
	**/
    public static final String CLAIMID ='A837128937';
     /**
	*str sustituye string literal
	**/
    public static final String REEMB = 'Reembolso';
      /**
	*str sustituye string literal asist
	**/
    public static final String ASIS = 'Asistencia';
      /**
	*str sustituye string literal est
	**/
    public static final String ESTADO = 'México';
    /**
	*Met actualiza siniestro
	**/
    @AuraEnabled
    public static void updateSini(Siniestro__c sini) {
        UPDATE sini;
    }
     /**
	*Met obtiene siniestro
	**/
	@AuraEnabled
        public static Siniestro__c getSiniestroD(Id siniId) {
		Siniestro__c rsltWO;
        rsltWO = MX_SB_MLT_WrapperAsistencias_cls.fetchSiniDetails(siniId);
        return rsltWO;
        }
     /**
	*Met actualiza grua
	**/
    @AuraEnabled
	public static void updateCaseGrua(String caseId, String idProveedor,Siniestro__c strSinData, WorkOrder objGrua,boolean enviar) {
		List<String> coord = new List<String>();
        double lon = 0.0;
        double lat =0.0;
        final Siniestro__c sin = [Select Id, TipoSiniestro__c,MX_SB_MLT_CalleDestino__c,MX_SB_MLT_URLLocation__c,MX_SB_MLT_Address__c, MX_SB_MLT_CodigoPostalDestino__c,MX_SB_MLT_ColoniaDestino__c,MX_SB_MLT_CoordenadasDestino__c,MX_SB_MLT_DireccionDestino__c,MX_SB_MLT_EntreCalles_Destino__c,MX_SB_MLT_EstadoDestino__c,MX_SB_MLT_NumExtDestino__c,MX_SB_MLT_RefVisualesDestino__c from Siniestro__c where id=: strSinData.id limit 1];
		if( sin.MX_SB_MLT_URLLocation__c!=null && coord.isEmpty()) {
            coord = sin.MX_SB_MLT_URLLocation__c.split(',');
            lon =  double.valueOf(coord[1]);
            lat = double.valueOf(coord[0]);
        }
        sin.MX_SB_MLT_Fecha_Hora_Siniestro__c=strSinData.MX_SB_MLT_Fecha_Hora_Siniestro__c;
        sin.MX_SB_MLT_EntreCalles_Destino__c = strSinData.MX_SB_MLT_EntreCalles_Destino__c;
        if(enviar) {
            sin.MX_SB_MLT_JourneySin__c = SERVENV;
        }
		update sin;
		final WorkOrder cse = [SELECT Id,MX_SB_MLT_ClaimId__c,Country,City from WorkOrder where id =: caseId limit 1];
        if(sin.MX_SB_MLT_Address__c.contains(ESTADO)) {
               cse.Country=ESTADO;
               cse.City=ESTADO;
          }
        if(objGrua.MX_SB_MLT_Reembolso__c==true) {
            cse.MX_SB_MLT_Reembolso__c= true;
            cse.MX_SB_MLT_TotalReembolso__c = objGrua.MX_SB_MLT_TotalReembolso__c;
            cse.MX_SB_MLT_TipoServicioAsignado__c = REEMB;
        } else {
            cse.AccountId = idProveedor;
            cse.MX_SB_MLT_TipoServicioAsignado__c = ASIS;
        }
            cse.MX_SB_MLT_MotivoReembolso__c = objGrua.MX_SB_MLT_MotivoReembolso__c;
            cse.MX_SB_MLT_UnidadEnviada__c = objGrua.MX_SB_MLT_UnidadEnviada__c;
            cse.MX_SB_MLT_CitaFecha__c = strSinData.MX_SB_MLT_Fecha_Hora_Siniestro__c;
            cse.MX_SB_MLT_Destino__c= sin.MX_SB_MLT_DireccionDestino__c;
            cse.MX_SB_MLT_ServicioAsignado__c = 'Grua';
            cse.Street = sin.MX_SB_MLT_Address__c;
        	cse.Latitude = lat;
        	cse.Longitude = lon;
        if(enviar) {
            cse.MX_SB_MLT_ClaimId__c=CLAIMID;
        }
        update cse;
	}
     /**
	*Met actualiza gasolina
	**/
    @AuraEnabled
    public static void updateCaseGasolina(String caseId, String idProveedor, Siniestro__c strSinData , WorkOrder objGas,boolean enviar) {
       List<String> coord = new List<String>();
        double lat =0.0;
        double lon = 0.0;
        final Siniestro__c sin = [Select Id,MX_SB_MLT_JourneySin__c, TipoSiniestro__c,MX_SB_MLT_CalleDestino__c,MX_SB_MLT_URLLocation__c,MX_SB_MLT_Address__c, MX_SB_MLT_CodigoPostalDestino__c,MX_SB_MLT_ColoniaDestino__c,MX_SB_MLT_CoordenadasDestino__c,MX_SB_MLT_DireccionDestino__c,MX_SB_MLT_EntreCalles_Destino__c,MX_SB_MLT_EstadoDestino__c,MX_SB_MLT_NumExtDestino__c,MX_SB_MLT_RefVisualesDestino__c from Siniestro__c where id=: strSinData.id limit 1];
            if( sin.MX_SB_MLT_URLLocation__c!=null && coord.isEmpty()) {
            coord = sin.MX_SB_MLT_URLLocation__c.split(',');
            lat = double.valueOf(coord[0]);
            lon =  double.valueOf(coord[1]);
        }	 
        if(enviar) {
            sin.MX_SB_MLT_JourneySin__c = SERVENV;
            update sin;
        }
		final WorkOrder cse1 = [SELECT Id,  MX_SB_MLT_Email__c from WorkOrder where id =: caseId limit 1];
       if(sin.MX_SB_MLT_Address__c.contains(ESTADO)) {
           cse1.Country=ESTADO;
           cse1.City=ESTADO;
          }
        if(objGas.MX_SB_MLT_Reembolso__c==true) {
            cse1.MX_SB_MLT_Reembolso__c= true;
            cse1.MX_SB_MLT_TotalReembolso__c = objGas.MX_SB_MLT_TotalReembolso__c;
            cse1.MX_SB_MLT_TipoServicioAsignado__c = REEMB;
        } else {
            cse1.AccountId = idProveedor;
            cse1.MX_SB_MLT_TipoServicioAsignado__c = ASIS;
        }
            cse1.MX_SB_MLT_MotivoReembolso__c = objGas.MX_SB_MLT_MotivoReembolso__c;
            cse1.MX_SB_MLT_UnidadEnviada__c = objGas.MX_SB_MLT_UnidadEnviada__c;
            cse1.MX_SB_MLT_ServicioAsignado__c = 'Gasolina';
            cse1.Street = sin.MX_SB_MLT_Address__c;
        	cse1.Latitude = lat;
        	cse1.Longitude = lon;
        if(enviar) {
            cse1.MX_SB_MLT_ClaimId__c=CLAIMID;
        }
         update cse1;
        }
     /**
	*Met que regresa la lista de servicios creados en WorkOrder dependiendo el Contrato
	**/
	@AuraEnabled
	public static void updateCaseCambioLL(String caseId, String idProveedor,WorkOrder objCL, Siniestro__c strSinData,boolean enviar) {
    	List<String> coord = new List<String>();
        double lat =0.0;
        double lon = 0.0;
        final Siniestro__c sinies = [Select Id, MX_SB_MLT_JourneySin__c,TipoSiniestro__c,MX_SB_MLT_CalleDestino__c,MX_SB_MLT_URLLocation__c,MX_SB_MLT_Address__c, MX_SB_MLT_CodigoPostalDestino__c,MX_SB_MLT_ColoniaDestino__c,MX_SB_MLT_CoordenadasDestino__c,MX_SB_MLT_DireccionDestino__c,MX_SB_MLT_EntreCalles_Destino__c,MX_SB_MLT_EstadoDestino__c,MX_SB_MLT_NumExtDestino__c,MX_SB_MLT_RefVisualesDestino__c from Siniestro__c where id=: strSinData.id limit 1];
		if( sinies.MX_SB_MLT_URLLocation__c!=null && coord.isEmpty()) {
            coord = sinies.MX_SB_MLT_URLLocation__c.split(',');
            lat = double.valueOf(coord[0]);
            lon =  double.valueOf(coord[1]);
        }	
        if(enviar) {
            sinies.MX_SB_MLT_JourneySin__c = SERVENV;
            update sinies;
        }
        final WorkOrder cse2 = [SELECT Id, MX_SB_MLT_Email__c from WorkOrder where id =: caseId limit 1];
        if(sinies.MX_SB_MLT_Address__c.contains(ESTADO)) {
           cse2.Country=ESTADO;
           cse2.City=ESTADO;
            } 
        if(objCL.MX_SB_MLT_Reembolso__c==true) {
            cse2.MX_SB_MLT_Reembolso__c= true;
            cse2.MX_SB_MLT_TotalReembolso__c = objCL.MX_SB_MLT_TotalReembolso__c;
            cse2.MX_SB_MLT_TipoServicioAsignado__c = REEMB;
        } else {
            cse2.AccountId = idProveedor;
            cse2.MX_SB_MLT_TipoServicioAsignado__c = ASIS;
        }
            cse2.MX_SB_MLT_MotivoReembolso__c = objCL.MX_SB_MLT_MotivoReembolso__c;
            cse2.MX_SB_MLT_UnidadEnviada__c = objCL.MX_SB_MLT_UnidadEnviada__c;
            cse2.MX_SB_MLT_ServicioAsignado__c = 'Cambio de llanta';
            cse2.Street = sinies.MX_SB_MLT_Address__c;
        	cse2.Latitude = lat;
        	cse2.Longitude = lon;
        if(enviar) {
            cse2.MX_SB_MLT_ClaimId__c=CLAIMID;
        }
        update cse2;
    }
     /**
	*Met que regresa la lista de servicios creados en WorkOrder dependiendo el Contrato
	**/
    @AuraEnabled
    public static void updateCasePasoC(String caseId, String idProveedor,WorkOrder objPasoC, Siniestro__c strSinData,boolean enviar) {
        List<String> coord = new List<String>();
        double lat2 =0.0;
        double lon2 = 0.0;
        final Siniestro__c sinPaso = [Select Id, MX_SB_MLT_JourneySin__c,TipoSiniestro__c,MX_SB_MLT_CalleDestino__c,MX_SB_MLT_URLLocation__c,MX_SB_MLT_Address__c, MX_SB_MLT_CodigoPostalDestino__c,MX_SB_MLT_ColoniaDestino__c,MX_SB_MLT_CoordenadasDestino__c,MX_SB_MLT_DireccionDestino__c,MX_SB_MLT_EntreCalles_Destino__c,MX_SB_MLT_EstadoDestino__c,MX_SB_MLT_NumExtDestino__c,MX_SB_MLT_RefVisualesDestino__c from Siniestro__c where id=: strSinData.id limit 1];
		if( sinPaso.MX_SB_MLT_URLLocation__c!=null && coord.isEmpty()) {
            coord = sinPaso.MX_SB_MLT_URLLocation__c.split(',');
            lat2 = double.valueOf(coord[0]);
            lon2 =  double.valueOf(coord[1]);
        }	
        if(enviar) {
            sinPaso.MX_SB_MLT_JourneySin__c = SERVENV;
            update sinPaso;
        }
        final WorkOrder cse3 = [SELECT Id from WorkOrder where id =: caseId limit 1];
        if(sinPaso.MX_SB_MLT_Address__c.contains(ESTADO)) {
           cse3.Country=ESTADO;
           cse3.City=ESTADO;
            } 
         if(objPasoC.MX_SB_MLT_Reembolso__c==true) {
            cse3.MX_SB_MLT_Reembolso__c= true;
            cse3.MX_SB_MLT_TotalReembolso__c = objPasoC.MX_SB_MLT_TotalReembolso__c;
            cse3.MX_SB_MLT_TipoServicioAsignado__c = REEMB;
        } else {
            cse3.AccountId = idProveedor;
            cse3.MX_SB_MLT_TipoServicioAsignado__c = ASIS;
        }
            cse3.MX_SB_MLT_MotivoReembolso__c = objPasoC.MX_SB_MLT_MotivoReembolso__c;
            cse3.MX_SB_MLT_UnidadEnviada__c = objPasoC.MX_SB_MLT_UnidadEnviada__c;
            cse3.MX_SB_MLT_ServicioAsignado__c = 'Paso de corriente';
        	cse3.Street = sinPaso.MX_SB_MLT_Address__c;
        	cse3.Latitude = lat2;
        	cse3.Longitude = lon2;
        if(enviar) {
            cse3.MX_SB_MLT_ClaimId__c=CLAIMID;
        }
        update cse3;
    }
     /**
	*Met obtener grua
	**/
    @AuraEnabled
        public static WorkOrder getGruaWO(String sinid) {
		WorkOrder rsltWO;
        rsltWO = MX_SB_MLT_WrapperAsistencias_cls.getGruaCase(sinid);
        return rsltWO;
        }
     /**
	*Met obtener llanta
	**/
        @AuraEnabled
        public static WorkOrder getCambioLLWO(String sinid) {
		WorkOrder rsltWO;
        rsltWO = MX_SB_MLT_WrapperAsistencias_cls.getCambioLlantaCase(sinid);
        return rsltWO;
        }
     /**
	*Met obtener paso de corriente
	**/
    @AuraEnabled
        public static WorkOrder getPasoCWO(String sinid) {
		WorkOrder rsltWO;
        rsltWO = MX_SB_MLT_WrapperAsistencias_cls.getPasoCorrienteCase(sinid);
        return rsltWO;
        }
     /**
	*Met obtener gas
	**/
    @AuraEnabled
        public static WorkOrder getGasWO(String sinid) {
		WorkOrder rsltWO;
        rsltWO = MX_SB_MLT_WrapperAsistencias_cls.getGasolinaCase(sinid);
        return rsltWO;
        }
}