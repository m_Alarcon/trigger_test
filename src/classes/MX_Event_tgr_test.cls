/**
* ------------------------------------------------------------------------------------------------
* @Name         MX_Event_tgr_test
* @Author       Selena Rodriguez
* @Date         Created: 2019-10-17
* @Group        Test class for Trigger MX_Event_tgr
* @Description  Class that validate methods in Trigger Class
*  Version 1.0
* ------------------------------------------------------------------------------------------------
*/
@isTest
public class MX_Event_tgr_test {
    /*Variable String que almacena los msjes Assert*/
    static String assertMsg;
    /*Metdo que crea un evento*/
 @isTest
    public static void crearEvento() {
        assertMsg = 'Eliminación correcta';
        final gcal__GBL_Google_Calendar_Sync_Environment__c calEnvment = new gcal__GBL_Google_Calendar_Sync_Environment__c(Name = 'DEV');
        final List<RecordType> listRecordType = [SELECT Id FROM RecordType WHERE SObjectType = 'Event' and DeveloperName = 'Evento_BPyP'];
        insert calEnvment;
        final Event evento1 = new Event();
        evento1.Subject='Evento prueba';
        evento1.StartDateTime=System.now();
        evento1.EndDateTime=System.now().addDays(3);
        evento1.CurrencyIsoCode='MXN';
        evento1.RecordTypeId = listRecordType[0].Id;
        insert evento1;

        evento1.EndDateTime=System.now().addDays(5);
        update evento1;

        delete evento1;
        final List<Event> listEvent = [SELECT Id FROM Event LIMIT 1];
        System.assertEquals(0 ,listEvent.size(), assertMsg);

    }
}