/**
* ------------------------------------------------------------------------------------------------
* @Name     	BPyP_LinkFactset_Service
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-10-19
* @Description 	BPyP Link Factset Service Layer
* @Changes
*  
*/
public with sharing class BPyP_LinkFactset_Service {
    
    /*String nombre de permissionSet de factset */
    static final String FACTSET_PSET_NAME = 'MX_BPP_Factset';
    
    /*Contructor clase BPyP_LinkFactset_Service */
    @testVisible
    private BPyP_LinkFactset_Service() {}
    
    /**
    * @Description 	Validate if the current user has the Factest PermissionSet
    * @Params		String userId
    * @Return 		NA
    **/
	public static Boolean validateFactsetPermissionSet (String userId) {
        Boolean access;
        final List<PermissionSetAssignment> listPSAssignment = MX_RTL_PermissionSetAssignment_Selector.selectByUserIdWithName(userId, FACTSET_PSET_NAME);
        access = !listPSAssignment.isEmpty();
        return access;
    }
}