/**
 * @File Name          : MX_RTL_OpportunityLineItem_Selector_Test.cls
 * @Description        : Clase para Test de MX_RTL_OpportunityLineItem_Selector
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 03/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      04/06/2020           Jair Ignacio Gonzalez G.   Initial Version
**/
@isTest
private class MX_RTL_OpportunityLineItem_Selector_Test {

    /**
     * *Descripción: Clase de prueba para getOppLineItembyOppId y updateOpportunityLineItem
    **/
    @isTest static void testMethods1() {
        final Opportunity oOpp = new Opportunity(Name='Test', StageName='Abierta', CloseDate=Date.today().addDays(15),
            RecordTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('MX_BPP_RedBpyp').getRecordTypeId()
            );
        insert oOpp;
        final Product2 objProd = new Product2(Banca__c = 'Red BPyP', CurrencyIsoCode = 'MXN', Family = 'Test', IsActive = true,
        Name = 'ProductTest', ProductCode = 'ProductTest');
        Insert objProd;
        final PricebookEntry objPribkEntry = new PricebookEntry( CurrencyIsoCode = 'MXN', IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), Product2Id = objProd.Id, UseStandardPrice = false, UnitPrice = 0);
        Insert objPribkEntry;
        final OpportunityLineItem oOppLnItem = new OpportunityLineItem(OpportunityId=oOpp.Id, Quantity=1, TotalPrice=10, Product2Id=objProd.Id, PricebookEntryId=objPribkEntry.Id);
        insert oOppLnItem;
        Test.startTest();
        final MX_RTL_OpportunityLineItem_Selector singleton = new MX_RTL_OpportunityLineItem_Selector(); //NOSONAR
        final List<OpportunityLineItem> lsOppLnItem = MX_RTL_OpportunityLineItem_Selector.getOppLineItembyOppId(new List<Id>{oOpp.Id});
        MX_RTL_OpportunityLineItem_Selector.updateOpportunityLineItem(lsOppLnItem);
        Test.stopTest();
        System.assertEquals(oOpp.Id, lsOppLnItem[0].OpportunityId, 'OpportunityLineItem not found');

    }
}