/**
 * @File Name          : MX_RTL_CampaignMembers.cls
 * @Description        : Trigger Handler de CampaignMember
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 24/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      24/06/2020           Jair Ignacio Gonzalez G.   Initial Version
**/
public without sharing class MX_RTL_CampaignMembers extends TriggerHandler {

	/** Map de Trigger.new */
	final private Map<Id,CampaignMember> triggerNewMap;
	/** Map de Trigger.old */
    final private Map<Id,CampaignMember> triggerOldMap;

    /**
     * MX_RTL_CampaignMembers constructor
     */
    public MX_RTL_CampaignMembers() {
        super();
        this.triggerNewMap= (Map<Id,CampaignMember>)(Trigger.newMap);
        this.triggerOldMap= (Map<Id,CampaignMember>)(Trigger.oldMap);
    }

    /*
	 * @afterUpdate event override en la Clase TriggerHandler
	 * Logica Encargada de los Eventos afterUpdate
	*/
	protected override void afterUpdate() {
        MX_BPP_UpdateRelatedLeadOpp_Service.updateOppFromCampMbrs(triggerNewMap, triggerOldMap);
        MX_SB_PS_CreateOppForECampaignM_Service.createOppFromCampMbrs(triggerNewMap, triggerOldMap);
    }
    

    /*
     *@beforeInsert
	*/
    protected override void afterInsert() {
        MX_SB_PS_CreateOppForECampaignM_Service.createOppFromCampMbrs(triggerNewMap, triggerOldMap);
    }
    
}