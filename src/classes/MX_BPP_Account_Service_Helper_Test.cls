/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Account_Service_Helper_Test
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-08-06
* @Description 	Test Class for Account Service Helper
* @Changes
*  
*/
@isTest
public class MX_BPP_Account_Service_Helper_Test {
    
    /** String BPyP Estandar */
	static final String STR_BPYPESTANDAR = 'BPyP Estandar';
    
    /** String RecordType.DeveloperName Account */
    static final String RTACC = 'BPyP_tre_Cliente';
    
    /** String RecordType.DeveloperName PersonAccount */
    static final String RTPACC = 'MX_BPP_PersonAcc_Client';
    
    /** String Account Name */
    static final String STR_ACCNAME = 'userBPyP';
    
    /** String User LastName */
    static final String USRLNAME = 'UsuarioBPyP';
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description TestSetup
    **/
    @testSetup
    static void setup() {
        final User userBanquero = UtilitysDataTest_tst.crearUsuario(USRLNAME, STR_BPYPESTANDAR, Null);
        userBanquero.Segmento_Ejecutivo__c = 'EMPRESARIAL';
        insert userBanquero;
        final Account cliente = UtilitysDataTest_tst.crearCuenta(STR_ACCNAME, RTACC);
        cliente.No_de_cliente__c = '1234';
        insert cliente;
        
        final Lead candidato = new Lead();
        candidato.LeadSource = 'Preaprobados';
        candidato.FirstName = 'Lead';
        candidato.LastName = 'BPPTest';
        candidato.MX_WB_RCuenta__c = cliente.Id;
        candidato.MX_ParticipantLoad_id__c = '1234';
        candidato.OwnerId = userBanquero.Id;
        candidato.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('MX_BPP_Leads').getRecordTypeId();
        insert candidato;
        
        final Case caso = new Case();
        caso.Subject = candidato.Name + ' Lead';
        caso.AccountId = cliente.Id;
        caso.MX_SB_SAC_Folio__c = '1004890';
        caso.Description = 'Test Case';
        caso.Status = 'Nuevo';
        caso.OwnerId = userBanquero.Id;
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MX_EU_Case_Apoyo_General').getRecordTypeId();
        insert caso;
    }
    
    /**
    *@Description   Test Method for getCasesByAccountAndRtype
    *@author 		Edmundo Zacarias
    *@Date 			2020-08-06
    **/
    @isTest
    private static void testUpdateRelatedRecordsOwner() {
        final User newOwner = UtilitysDataTest_tst.crearUsuario(USRLNAME + '2', STR_BPYPESTANDAR, Null);
        newOwner.Segmento_Ejecutivo__c = 'EMPRESARIAL';
        insert newOwner;
        final List<Account> listAcc = [SELECT Id FROM Account WHERE Name =: STR_ACCNAME AND RecordType.DeveloperName =: RTACC];
        final Set<Id> accIds = new Set<Id>();
        for(Account acc : listAcc) {
            acc.OwnerId = newOwner.Id;
            accIds.add(acc.Id);
        }
        Test.startTest();
        update listAcc;
        Test.stopTest();
        
        final Lead updatedLead = [SELECT Id, OwnerId FROM Lead WHERE MX_ParticipantLoad_id__c = '1234' AND LeadSource = 'Preaprobados' AND RecordType.DeveloperName = 'MX_BPP_Leads' LIMIT 1];
        System.assertEquals(newOwner.Id, updatedLead.OwnerId, 'Owner no actualizado');
    }

}