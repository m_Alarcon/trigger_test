/**
* @File Name          : MX_BPP_Oportunidad_Service_Test.cls
* @Description        : Test class for MX_BPP_Oportunidad_Service
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 21/10/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      21/10/2020            Gabriel Garcia Rojas          Initial Version
**/
@isTest
private class MX_BPP_Oportunidad_Service_Test {
	/*Usuario de pruebas*/
    private static User testUserStartOpp = new User();
    /** namerole variable for records */
    final static String NAME_ROLESOPP = '%BPYP BANQUERO BANCA PERISUR%';
    /** nameprofile variable for records */
    final static String NAME_PROFILESOPP = 'BPyP Estandar';
    /** name variable for records */
    final static String NAMESTSOPP = 'testUserOpp';
    /** name variable for records */
    final static String NAMECOT_SUCURSALO = '6343 PEDREGAL';
    /** namedivision variable for records */
    final static String NAMECOT_DIVISIONO = 'METROPOLITANA';
    /** name variable for oficina */
    final static String PRIVADOSTRO ='PRIVADO';
    /** error message */
    final static String MESSAGETESTO = 'Fail method';
    /** String Cuentas */
    final static String CUENTASOSER = 'Cuentas';
    /** String Todas */
    final static String TODASSERO = 'TODAS';

    /*Setup para clase de prueba*/
    @testSetup
    static void setupOppService() {

        testUserStartOpp = UtilitysDataTest_tst.crearUsuario(NAMESTSOPP, NAME_PROFILESOPP, NAME_ROLESOPP);
        testUserStartOpp.Title = PRIVADOSTRO;
        testUserStartOpp.Divisi_n__c = NAMECOT_DIVISIONO;
        testUserStartOpp.BPyP_ls_NombreSucursal__c = NAMECOT_SUCURSALO;
        testUserStartOpp.VP_ls_Banca__c = 'Red BPyP';
        insert testUserStartOpp;

        final Id rtAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta BPyP').getRecordTypeId();
        final Id rtOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('BPyP').getRecordTypeId();

        final Account cuentaOSer = new Account(FirstName = 'CuentaTest', LastName = 'LastTest', No_de_cliente__c = '12345678', RecordTypeId = rtAcc, OwnerId = testUserStartOpp.Id );
        insert cuentaOSer;

        final Date fechaOppServ = System.today();

        System.runAs(testUserStartOpp) {
            final Opportunity oppTestServ = new Opportunity();
            oppTestServ.Name = 'OppTest';
            oppTestServ.MX_RTL_Familia__c = 'Captación';
            oppTestServ.MX_RTL_Producto__c = 'Cuenta en Dólares';
            oppTestServ.op_amountPivote_dv__c = 100000;
            oppTestServ.AccountId = cuentaOSer.Id;
            oppTestServ.RecordTypeId = rtOpp;
            oppTestServ.StageName = 'Abierta';
        	oppTestServ.CloseDate = fechaOppServ.addDays(30);

            insert oppTestServ;
        }
    }

    /**
    * @description constructor test
    * @author Gabriel Garcia | 21/10/2020
    * @return void
    **/
    @isTest
    private static void testConstructorServOpp() {
        final MX_BPP_Oportunidad_Service instaTestOppSer = new MX_BPP_Oportunidad_Service();
        System.assertNotEquals(instaTestOppSer, null, MESSAGETESTO);
    }

    /**
     * @description test fetchServiceDataOpp
     * @author Gabriel Garcia | 21/010/2020
     * @return void
     **/
    @isTest
    private static void fetchServiceDataOppTest() {
        Test.startTest();
        final MX_BPP_Oportunidad_Service.WRP_ChartStacked wrpCharTest = MX_BPP_Oportunidad_Service.fetchServiceDataOpp(new List<String>{CUENTASOSER, TODASSERO, 'Division', ''}, null, null);
        System.assertNotEquals(wrpCharTest, null, MESSAGETESTO);

        Test.stopTest();
    }

    /**
     * @description test fetchOpp
     * @author Gabriel Garcia | 21/010/2020
     * @return void
     **/
    @isTest
    private static void fetchOppTest() {
        Test.startTest();
        final List<Opportunity> listOppSerTest = MX_BPP_Oportunidad_Service.fetchOpp(new List<String>{NAMESTSOPP, CUENTASOSER, TODASSERO, '10'}, null, null);
        System.assertNotEquals(listOppSerTest, null, MESSAGETESTO);

        Test.stopTest();
    }

    /**
     * @description test fetchMapType
     * @author Gabriel Garcia | 21/010/2020
     * @return void
     **/
    @isTest
    private static void fetchMapTypeTest() {
        Test.startTest();
        final Map<String, List<String>> mapOppSert = MX_BPP_Oportunidad_Service.fetchMapType();
        System.assertNotEquals(mapOppSert, null, MESSAGETESTO);

        Test.stopTest();
    }

}