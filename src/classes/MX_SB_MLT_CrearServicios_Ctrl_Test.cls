@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_RTL_Siniestro_Service_Test
* Autor: Eder Alberto Hernández Carbajal
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_RLT_Siniestro_Service

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         02/06/2020   Eder Alberto Hernández Carbajal              Creación
* -----------------------------------------------------------------------------------------------
*/
public class MX_SB_MLT_CrearServicios_Ctrl_Test {
    
    /** var String Valor boton cancelar */
    public static final String CANCEL = 'Cancelar';
    
    /** var String Valor boton volver */
    public static final String BACK = 'Volver';

    /** Nombre de usuario Test */
    final static String NAME = 'Kamaboko Gonpachiro';
    
    /** Tipo de registro en catalogo claim */
    final static String TIPO_CATALOGO = 'ICD';
    
    /** Descripción de registro de catalogo claim */
    final static String DESCRIPCION = 'SHIGELOSIS';
    
    /** current user id */
    final static String USR_ID = UserInfo.getUserId();    
    /**
     * @description
     * @author Eder Hernández | 02/06/2020
     * @return void
     **/
    @TestSetup
    static void createData() {
        Final String profName = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User userTst = MX_WB_TestData_cls.crearUsuario(NAME, profName);  
        insert userTst;
        System.runAs(userTst) {
            final String rTId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Proveedores_MA).getRecordTypeId();
            final Account accTst = new Account(RecordTypeId = rTId, Name=NAME, Tipo_Persona__c='Física');
            insert accTst;
            
            final Contract contractTst = new Contract(AccountId = accTst.Id, MX_SB_SAC_NumeroPoliza__c='policyTest');
            insert contractTst;
            
            
            Final List<Siniestro__c> lstSin = new List<Siniestro__c>();
            for(Integer i = 0; i < 10; i++) {
                Final Siniestro__c tstSin = new Siniestro__c();
                tstSin.MX_SB_SAC_Contrato__c = contractTst.Id;
                tstSin.MX_SB_MLT_NombreConductor__c = NAME + '_' + i;
                tstSin.MX_SB_MLT_APaternoConductor__c = 'LastName for '+NAME;
                tstSin.MX_SB_MLT_AMaternoConductor__c = 'LastName2 for '+NAME;
                tstSin.MX_SB_MLT_Fecha_Hora_Siniestro__c =date.valueof('2020-03-27T22:04:00.000+0000');
                tstSin.MX_SB_MLT_Telefono__c = '5534253647';
                Final datetime citaAgenda = datetime.now();
                tstSin.MX_SB_MLT_CitaVidaAsegurado__c = citaAgenda.addDays(2);
                tstSin.MX_SB_MLT_AtencionVida__c = 'Agendar Cita';
                tstSin.MX_SB_MLT_PreguntaVida__c = false;
                tstSin.MX_SB_MLT_SubRamo__c = 'Ahorro';
                tstSin.TipoSiniestro__c = 'Siniestros';
                tstSin.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_creacion;
                tstSin.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoVida).getRecordTypeId();
                lstSin.add(tstSin);
            }
            Database.insert(lstSin);
        }
    }
    /*
    * @description method que prueba MX_SB_MLT_CrearServicios_Ctrl constructor
    * @param  void
    * @return void
    */
    @isTest
   	static void testConstructor() {
        Final MX_SB_MLT_CrearServicios_Ctrl ctrl = new MX_SB_MLT_CrearServicios_Ctrl();
        system.assert(true,ctrl);
    }
    /*
    * @description method que prueba MX_SB_MLT_CrearServicios_Ctrl searchSinVida
    * @param  void
    * @return void
    */
    @isTest
   	static void testSearchSinVida() {
        Final String srchSin = [SELECT Id FROM Siniestro__c where MX_SB_MLT_NombreConductor__c = :NAME + '_3'].Id;
        Test.startTest();
        	MX_SB_MLT_CrearServicios_Ctrl.searchSinVida(srchSin);
        Test.stopTest();
        System.assert(true,'Se ha encontrado el registro del siniestro!');
    }
    /*
    * @description method que prueba MX_SB_MLT_CrearServicios_Ctrl getCatalogData
    * @param  void
    * @return void
    */
    @isTest
   	static void testGetCatalogData() {
        MX_SB_MLT_CrearServicios_Ctrl.getCatalogData(DESCRIPCION, TIPO_CATALOGO);
        System.assert(true,'Se ha encontrado el registro en el catalogo!');
    }
    /*
    * @description method que prueba MX_SB_MLT_CrearServicios_Ctrl udpateSiniestro
    * @param  void
    * @return void
    */
    @isTest
   	static void testUdpateSiniestro() {
        Final Siniestro__c updateSinTest = [SELECT Id, MX_SB_MLT_Telefono__c FROM Siniestro__c where MX_SB_MLT_NombreConductor__c = :NAME + '_2'];
        updateSinTest.MX_SB_MLT_Telefono__c = '5534253647';
        Test.startTest();
        	MX_SB_MLT_CrearServicios_Ctrl.updateSiniestro(updateSinTest);
        Test.stopTest();
        System.assert(true,'Se ha actualizado el registro!');
    }
    /*
    * @description method que prueba MX_SB_MLT_CrearServicios_Ctrl siniestroAction
    * @param  void
    * @return void
    */
    @isTest
    static void testsiniestroAction() {
        Final String sinTest = [SELECT Id FROM Siniestro__c where MX_SB_MLT_NombreConductor__c = :NAME + '_8'].Id;
        test.startTest();
        	MX_SB_MLT_CrearServicios_Ctrl.siniestroAction(BACK,sinTest);
        	MX_SB_MLT_CrearServicios_Ctrl.siniestroAction(CANCEL,sinTest);
        test.stopTest();
        System.assert(true, 'La etapa cambio con éxito!');
    }
}