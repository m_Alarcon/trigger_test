/**
* @File Name          : MX_BPP_AccountFiltr_Service.cls
* @Description        :
* @Author             : Jair Ignacio Gonzalez G
* @Group              :
* @Last Modified By   : Jair Ignacio Gonzalez G
* @Last Modified On   : 22/10/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      22/10/2020        Jair Ignacio Gonzalez G            Initial Version
**/
@SuppressWarnings()
public with sharing class MX_BPP_AccountFiltr_Service {

    /*Variable global Banca*/
    static final String BANCA = 'Red BPyP';
    /*Variable global cadena vacia*/
    static final String STREMPTY = '';

    /*Variable global TODAS*/
    static final String TODASTR ='TODAS';
    /*Variable global Cuentas*/
    static final String CUENTASTR ='Cuentas';
    /*Variable global Cuentas*/
    static final String PROSPECTOSTR ='Prospectos';
    /*Variable global 25*/
    static final Integer NUMBCOL = 25;

    /**Constructor */
    @TestVisible
    private MX_BPP_AccountFiltr_Service() { }

    /**
    * @description retorna un objeto WRP_ChartStacked para la estructura de gráficas
    * @author Jair Ignacio Gonzalez Gayosso
    * @param String params [tyAcc, tipoConsulta, filtro0]
    * @return WRP_ChartStacked
    **/
    public static WRP_ChartStacked fetchServiceDataAcc(List<String> params) {

        final String tyAcc = params[0]; //Cuentas # Prospectos
        final String tipoConsulta = params [1]; //division # Oficina # Banquero # BanqueroOnly
        final String filtro0 = params[2]; // Oficina # Banquero # BanqueroOnly

        final list<String> tempColor = new List<String>();
		final List<Integer> red = BPyP_AccFiltr_Utilities.genRedBBVA();
		final List<Integer> green = BPyP_AccFiltr_Utilities.genGreenBBVA();
        final List<Integer> blue = BPyP_AccFiltr_Utilities.genBlueBBVA();

        final Map<String,Integer> tempData = new Map<String,Integer>();

        String tipoAcc = '';

        final Map<String, List<String>> mapFields = fetchMapType();

        tipoAcc = mapFields.get(tyAcc)[2];

        final List<String> listParamsAcc = new List<String>{filtro0, BANCA, STREMPTY, mapFields.get(tyAcc)[0], mapFields.get(tyAcc)[1] };

        final String filedsAccQuery = ' count(Id) RecordCount, RecordType.Name RName, ' + mapFields.get(tipoConsulta)[0];
        final String clouseAccQuery = ' WHERE Owner.VP_ls_Banca__c =: filtro1 AND (NOT (Owner.BPyP_ls_NombreSucursal__c =: filtro2))  AND (NOT (Owner.Divisi_n__c =: filtro2)) '
            + ' AND Owner.IsActive = true ' + tipoAcc +
            + mapFields.get(tipoConsulta)[1] + ' GROUP BY RecordType.Name, ' + mapFields.get(tipoConsulta)[0] + ' ORDER BY RecordType.Name limit 50000 ';

        List<AggregateResult> ltyAcc = new List<AggregateResult>();
        if (tyAcc == TODASTR) {
            ltyAcc = MX_RTL_Account_Selector.fetchListAggAccByDataBase(filedsAccQuery, clouseAccQuery, listParamsAcc, new List<String>{'BPyP_tre_Cliente','BPyP_tre_noCliente','MX_BPP_PersonAcc_Client','MX_BPP_PersonAcc_NoClient'});
        } else {
            ltyAcc = MX_RTL_Account_Selector.fetchListAggAccByDataBase(filedsAccQuery, clouseAccQuery, listParamsAcc);
        }

        final Set<String> setLabels = new Set<String>();
        final Set<String> setTyAcc = new Set<String>();

        for(AggregateResult aggR : ltyAcc) {
            setLabels.add( (String) aggR.get(mapFields.get(tipoConsulta)[2]));
            setTyAcc.add( (String) aggR.get('RName'));
            tempData.put( (String) aggR.get(mapFields.get(tipoConsulta)[2]) + (String) aggR.get('RName') , (Integer)aggR.get('RecordCount') );
        }

        for(Integer idx = 0;  idx < setTyAcc.size(); idx++) {
            final String color = 'rgb('+red[Math.mod(idx,NUMBCOL)]+', '+green[Math.mod(idx,NUMBCOL)]+', '+blue[Math.mod(idx,NUMBCOL)]+')';
            tempColor.add(color);
        }

        tempColor.add('rgb');

        return new WRP_ChartStacked(new List<String>(setLabels), new List<String>(setTyAcc), tempData, tempColor);
    }

    /**
    * @description retorna una lista oportunidaddes filtradas
    * @author Jair Ignacio Gonzalez Gayosso
    * @param String params [tyAcc, tipoConsulta, filtro0]
    * @return List<Account>
    **/
    @SuppressWarnings('sf:DUDataflowAnomalyAnalysis, sf:UnusedLocalVariable')
	public static List<Account> fetchAcc(List<String> params) {
        final String tyAcc = params[0]; //Todas # Cuentas # Prospectos
        final String tipoConsulta = params[1]; //division # Oficina # Banquero # BanqueroOnly
        final String filtro0 = params[2]; // Oficina # Banquero # BanqueroOnly
        final String offs = params [3];
        String limitAndOffset = ' limit 50000 ';
        String tipoAcc = '';

        final Map<String, List<String>> mapFields = fetchMapType();

        tipoAcc = mapFields.get(tyAcc)[2];

        if(offs != null && String.isNotBlank(offs)) {
            limitAndOffset = ' limit 10 offset ' + offs;
        }

        final List<String> listParamsAcc = new List<String>{filtro0, BANCA, STREMPTY, mapFields.get(tyAcc)[0], mapFields.get(tyAcc)[1]};
        final String filedsAccQuery = ' Id,Name, AccountNumber, BPyP_Rb_Family_Group_al_que_pertenece__c, BPyP_Rb_Family_Group_al_que_pertenece__r.Name , Phone, RecordType.Name,Owner.Name,BPyP_Fecha_de_ultima_visita__c '+STREMPTY;
        final String clouseAccQuery = ' WHERE Owner.VP_ls_Banca__c =: filtro1 AND (NOT (Owner.BPyP_ls_NombreSucursal__c =: filtro2))  AND (NOT (Owner.Divisi_n__c =: filtro2)) '
            + ' AND Owner.IsActive = true ' + tipoAcc
            + mapFields.get('BanqueroOnly')[1] + limitAndOffset;

        final List<String> lstRecTypes = new List<String>();
        if (tyAcc == TODASTR) {
            lstRecTypes.addAll(new List<String>{'BPyP_tre_Cliente','BPyP_tre_noCliente','MX_BPP_PersonAcc_Client','MX_BPP_PersonAcc_NoClient'});
        }

        return MX_RTL_Account_Selector.fetchListAccByRecType(filedsAccQuery, clouseAccQuery, listParamsAcc, lstRecTypes);
	}

    /**
    * @description retorna mapa con las condiciones de búsqueda por division, oficina o ejecutivo
    * @author Gabriel Garcia Rojas
    * @return Map<String, List<String>>
    **/
    public static Map<String, List<String>> fetchMapType() {
        final Map<String, List<String>> mapFields = new Map<String, List<String>>();
        mapFields.put('Division', new List<String>{'Owner.Divisi_n__c', '', 'Divisi_n__c' });
        mapFields.put('Oficina', new List<String>{'Owner.BPyP_ls_NombreSucursal__c', 'AND Owner.Divisi_n__c =: filtro0', 'BPyP_ls_NombreSucursal__c' });
        mapFields.put('Banquero', new List<String>{'Owner.Name', 'AND Owner.BPyP_ls_NombreSucursal__c =: filtro0', 'Name' });
        mapFields.put('BanqueroOnly', new List<String>{'Owner.Name', 'AND Owner.Name =: filtro0', 'Name' });

        mapFields.put('TODAS', new List<String>{'', '', ' AND RecordType.DeveloperName IN: listRecordTypes ' });
        mapFields.put('Cuentas', new List<String>{'BPyP_tre_Cliente', 'MX_BPP_PersonAcc_Client', ' AND (RecordType.DeveloperName =: filtro3 OR RecordType.DeveloperName =: filtro4) ' });
        mapFields.put('Prospectos', new List<String>{'BPyP_tre_noCliente', 'MX_BPP_PersonAcc_NoClient', ' AND (RecordType.DeveloperName =: filtro3 OR RecordType.DeveloperName =: filtro4) ' });

        return mapFields;
    }

    /** Wrapper class for chart.js with labels, data and color*/
    public class WRP_ChartStacked {
		/**lista lsLabels */
        @AuraEnabled public list<String> lsLabels {get; set;}
		/**lista lsTyAcc */
        @AuraEnabled public list<String> lsTyAcc {get; set;}
		/**lista lsColor */
        @AuraEnabled public list<String> lscolor {get; set;}
		/** Mapa lsData*/
        @AuraEnabled public Map<String, Integer> lsData {get; set;}

		/** mthd */
        public WRP_ChartStacked(list<String> label,list<String> tyAcc,Map<String, Integer> data,list<String> color) {
            lsLabels=label;
            lsTyAcc=tyAcc;
            lsData=data;
            lscolor=color;
        }
    }
}