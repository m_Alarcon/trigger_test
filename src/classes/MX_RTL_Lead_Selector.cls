/**
* Name: MX_SB_VTS_UpNameCampaign_Selector
* @author Ángel Lavana Rosas
* Description : New Class Selector to separate the campaign name from _CPN in Lead
* Ver                  Date            Author                   Description
* @version 1.0         Jun/05/2020     Ángel Lavana Rosas       Initial Version
* @version 1.1         Jun/05/2020     Jair Ignacio Gonzalez    Add leadsConvert
**/

/**
* @description: New class selector to Lead
*/
@SuppressWarnings('sf:UseSingleton, sf:DMLWithoutSharingEnabled')
public class MX_RTL_Lead_Selector {

    /** String Active Status */
	static final String STR_ABIERTO = 'Abierto';
    /** Variable que almacena la palabra Select */
    final static String QRYSEC = 'SELECT ';
    /**
    * @Description Consulta y obtiene Nombre y cupón de la campaña en candidato
    * @author Ángel Lavana Rosas
    * @param List<Id> idLead	Recibe el Id del Candidato
    * @return Campos consultados al Id o Ids de candidatos
    */
    public static List<Lead> leadSelectorCupon(List<Id> idLead) {
        return [Select Id, MX_SB_VTS_CampaFaceName__c, MX_SB_VTS_CodCampaFace__c from Lead where id in : idLead];
    }

    /**
    * @description Ejecuta update de registros de Candidato
    * @author Ángel Lavana Rosas | 05/06/2020
    * @param List<Lead> lstLeads  Lista de Candidatos a procesar
    * @param Boolean allOrNone Indica si se lanza una operacion de exception
    * @return List<Database.SaveResult> Lista de resultados de la operación DML
    **/
    public static List<Database.SaveResult> updateResult(List<Lead> lstLeads, Boolean allOrNone) {
        return Database.update(lstLeads, allOrNone);
    }

    /**
     * @Method leadsConvert
     * @param List<Database.LeadConvert> lsLeadsConvert
     * @Description Convierte una lista de Leads
     * @return List<Database.LeadConvertResult>
    **/
    public static List<Database.LeadConvertResult> leadsConvert(List<Database.LeadConvert> lsLeadsConvert) {
        return Database.convertLead(lsLeadsConvert);
    }

    /**
    * @description Inserta registro de leads
    * @author Eduardo Hernández Cuamatzi | 13/6/2020
    * @param lstLeads Lista de leads a insertar
    * @param allOrNone Indica si se aceptan los registros con o sin error
    * @return List<Database.SaveResult> Lista de resultados
    **/
    public static List<Database.SaveResult> insertLeads(List<Lead> lstLeads, Boolean allOrNone) {
        return Database.insert(lstLeads, allOrNone);
    }

    /**
    * @Description   Method test for return
    *@author Selena Rodriguez Vega
    *@Date 2020-06-11
    *@param String queryfield = field will be needed for query
    *@param String where= DeveloperName's RecordType
    **/
    public static List<Lead> resultQueryLead(String queryfield,String conditionField,boolean whitCondition) {
        String query;
        switch on String.valueOf(whitCondition) {
            when 'false' {
                query= QRYSEC + queryfield +' from Lead';
            }
            when 'true' {
                query= QRYSEC + queryfield +' from Lead where '+conditionField;
            }
        }
        return Database.query(String.escapeSingleQuotes(query).unescapeJava());
    }
    
    /**
    *@Description   Method for Lead query by AccountIDs and RecordType
    *@author 		Edmundo Zacarias
    *@Date 			2020-08-06
    *@param 		String leadFields = fields for query
    *@param 		String accIds = Set<Id> Accounts
	*@param			String strRecordType = recordType.DeveloperName
    **/
    public static List<Lead> getActiveLeadsByAccountAndRtype(String leadFields, Set<Id> accIds, String strRecordType) {
        return Database.query(String.escapeSingleQuotes(QRYSEC + leadFields + ' FROM Lead WHERE MX_WB_RCuenta__c IN: accIds AND RecordType.DeveloperName =:strRecordType AND Status =:STR_ABIERTO'));
    }
     /**
    * @description Recupera Información de Leads por set de Ids
    * @author Diego Olvera | 18/9/2020 
    * @param Set<String> setLeadId Set de Ids en formato String
    * @return List<Lead> Lista de Leads encontrados
    **/
    public static List<Lead> selectSObjectsById(String leadFields, Set<String> setLeadId) {
       return Database.query(String.escapeSingleQuotes(QRYSEC + leadFields + ' FROM Lead WHERE  Id IN: setLeadId'));
    }
}