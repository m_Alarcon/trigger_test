/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_PS_wrpCotizadorVidaResp_Test
* Autor Daniel Perez Lopez
* Proyecto: Salesforce Presuscritos
* Descripción : Prueba los Methods de la clase MX_SB_PS_wrpCotizadorVidaResp

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           10/12/2019      Daniel Lopez                         Creación
* --------------------------------------------------------------------------------
*/
 @SuppressWarnings('sf:UseSingleton, sf:NcssMethodCount')
@IsTest
public with sharing class MX_SB_PS_wrpCotizadorVidaResp_Test {
    @IsTest
    static void obtieneWraper() {
        final MX_SB_PS_wrpCotizadorVidaResp.Data wraper = new MX_SB_PS_wrpCotizadorVidaResp.Data();
        final MX_SB_PS_wrpCotizadorVidaResp.rateQuote rtq= new MX_SB_PS_wrpCotizadorVidaResp.rateQuote();
        rtq.subsequentPaymentsNumber='2';
        wraper.insuredList = new List<MX_SB_PS_wrpCotizadorVidaResp.insuredList>();
        wraper.rateQuote = new List<MX_SB_PS_wrpCotizadorVidaResp.rateQuote>();
        wraper.id='012345';
        wraper.numbr='';
        final MX_SB_PS_wrpCotizadorVidaResp.insuredList insuredList = new MX_SB_PS_wrpCotizadorVidaResp.insuredList();
        insuredList.coverages = new MX_SB_PS_wrpCotizadorVidaResp.coverages[] {};
        insuredList.holderIndicator='';
        insuredList.id='';
        
        final MX_SB_PS_wrpCotizadorVidaResp.person person = new MX_SB_PS_wrpCotizadorVidaResp.person();
        person.lastName='';
        person.secondLastName='';
        person.firstName='';
        final MX_SB_PS_wrpCotizadorVidaResp.relationship relationship = new MX_SB_PS_wrpCotizadorVidaResp.relationship();
        relationship.id='';
        final MX_SB_PS_wrpCotizadorVidaResp.beneficiaryType beneficiaryType = new MX_SB_PS_wrpCotizadorVidaResp.beneficiaryType();
        beneficiaryType.id='';
        final MX_SB_PS_wrpCotizadorVidaResp.coverages coverages = new MX_SB_PS_wrpCotizadorVidaResp.coverages();
        coverages.catalogItemBase =  new MX_SB_PS_wrpCotizadorVidaResp.catalogItemBase();
        coverages.premium = new MX_SB_PS_wrpCotizadorVidaResp.premium();
        final MX_SB_PS_wrpCotizadorVidaResp.paymentWay paymentWay = new MX_SB_PS_wrpCotizadorVidaResp.paymentWay();
        paymentWay.id='';
        paymentWay.name='';
        final MX_SB_PS_wrpCotizadorVidaResp.catalogItemBase catalogItemBase = new MX_SB_PS_wrpCotizadorVidaResp.catalogItemBase();
        catalogItemBase.id='';
        catalogItemBase.name='';
        final MX_SB_PS_wrpCotizadorVidaResp.premium premium = new MX_SB_PS_wrpCotizadorVidaResp.premium();
        premium.amount= 0.0;
        premium.moneda= new MX_SB_PS_wrpCotizadorVidaResp.moneda();
        final MX_SB_PS_wrpCotizadorVidaResp.moneda moneda = new MX_SB_PS_wrpCotizadorVidaResp.moneda();
        moneda.code='';
        final MX_SB_PS_wrpCotizadorVidaResp.discount discount = new MX_SB_PS_wrpCotizadorVidaResp.discount();
        discount.total='';
        discount.authorizedDiscount='';
        discount.campaign='';
        discount.maximum='';
        discount.couponValue='';
        Test.startTest();
        System.assertEquals(wraper.id,'012345','Exito');
        Test.stopTest();
    }
}