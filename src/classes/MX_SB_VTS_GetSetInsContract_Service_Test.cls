/**-------------------------------------------------------------------------
* Nombre: 		MX_SB_VTS_GetSetInsContract_Service_Test
* @author 		Alexandro Corzo
* Proyecto: 	Servicio - Consulta de Poliza
* Descripción : Clase de prueba para la clase encargada de recuperar los datos
*				de la poliza ingresada.
* --------------------------------------------------------------------------
*                         Fecha           Autor                   Desripción
* --------------------------------------------------------------------------
* @version 1.0            20/08/2020      Alexandro Corzo         Creación de la Clase
* @version 1.1			  30/10/2020	  Alexandro Corzo		  Se agregan funciones de prueba adicionales
* --------------------------------------------------------------------------*/
@isTest
public class MX_SB_VTS_GetSetInsContract_Service_Test {
	/** Variable de Apoyo: sUserName */
    Static String sUserName = 'UserOwnerTest01';
    /** Variable de Apoyo: sNoEmpty */
    Static String sNoEmpty = 'Lista No Vacia';
    /** Variable de Apoyo: sURL */
    Static String sUrl = 'http://www.example.com';
    /** Variable de Apoyo: Numero de Poliza */
    Static String sNoPoliza = 'N80E9A0005';
    /** Variable de Apoyo: Numero de Poliza Inv */
    Static String sNoPolizaInv = '189722001R';
    /** Variable de Apoyo: sNoPolizaUS */
    Static String sNoPolizaUS = '18972200US';
    /** Variable de Apoyo: Numero de Poliza Existe */
	Static String sNoPolizaEx = '1234567890';
    /** Variable de Apoyo: sStatusCont */
    Static String sStatusCont = 'Draft';
    /** Variable de Apoyo: sLeadSource */
    Static String sLeadSource = 'Call me back';
    /** Variable de Apoyo: sProducto */
    Static String sProducto = 'Seguro Estudia'; 
    /** Variable de Apoyo: sContratos */
    Static String sContratos = 'Contratos';
    /** Variable de Apoyo: sVenta */
    Static String sVenta = 'Venta';
    /** Variable de Apoyo: sSeguros */
    Static String sSeguros = 'Seguros';
    /** Variables de Apoyo: sPersonAcct */
    Static String sPersonAcct = 'PersonAccount';
    /** Variable de Apoyo: Numero de Poliza No Existe */
	Static String sNoPolizaNEx = '0987654321'; 
    
    /**
     * @description: Instrucción de Clase de Prueba para realizar
     *               la configuracion previa 
     * @author: 	 Alexandro Corzo
     */   
    @TestSetup
    static void makeData() {
        User oUsrTest = null;
        oUsrTest = MX_WB_TestData_cls.crearUsuario('TestUser', System.label.MX_SB_VTS_ProfileAdmin);
        insert oUsrTest;
        System.runAs(oUsrTest) {
        	final Lead oLeadTest = MX_WB_TestData_cls.createLead('Lead Contratos');
            oLeadTest.FirstName = 'Lead';
            oLeadTest.LastName = 'Prueba';
            oLeadTest.LeadSource = sLeadSource;
            insert oLeadTest;
            final Account oAccTest = MX_WB_TestData_cls.crearCuenta(sContratos, sPersonAcct);
            oAccTest.PersonEmail = 'test@test.com';
            oAccTest.PersonMobilePhone = '5555555555';
            insert oAccTest;
            final Opportunity oOppoTest = MX_WB_TestData_cls.crearOportunidad('Opportunity Contratos', oAccTest.Id, oUsrTest.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            oOppoTest.LeadSource = sLeadSource;
            oOppoTest.Producto__c = sProducto;
            insert oOppoTest;
            final Product2 oProduct = MX_WB_TestData_cls.crearProducto(sProducto, sSeguros);
            oProduct.MX_SB_SAC_Proceso__c = 'VTS';
            insert oProduct;
            final Contract oContract = MX_WB_TestData_cls.vtsCreateContract(oAccTest.Id, oUsrTest.Id, oProduct.Id);
            oContract.MX_WB_Oportunidad__c = oOppoTest.Id;
            oContract.MX_WB_noPoliza__c = sNoPolizaEx;
            oContract.MX_SB_SAC_NumeroPoliza__c = sNoPolizaEx;
			oContract.StartDate = Date.valueOf('2020-08-31');
            oContract.Status = sStatusCont;
            insert oContract;
            final Opportunity oppVal = MX_WB_TestData_cls.crearOportunidad('Opportunity Valida', oAccTest.Id, oUsrTest.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            oppVal.Reason__c = sVenta;
            oppVal.Producto__c = System.Label.MX_SB_VTS_Hogar;
            oppVal.StageName = System.Label.MX_SB_VTS_COTIZACION_LBL;
            oppVal.TelefonoCliente__c = oAccTest.PersonMobilePhone;
            oppVal.LeadSource = System.Label.MX_SB_VTS_OrigenCallMeBack;
            insert oppVal;
            final Contract conVal = MX_WB_TestData_cls.vtsCreateContract(oAccTest.Id, oUsrTest.Id, oProduct.Id);
            conVal.MX_WB_Oportunidad__c = oppVal.Id;
            conVal.MX_WB_noPoliza__c = sNoPoliza;
            conVal.MX_SB_SAC_NumeroPoliza__c = sNoPoliza;
			conVal.StartDate = Date.valueOf('2020-09-01');
            conVal.MX_SB_VTS_FechaInicio__c = '30/10/2020';
            conVal.MX_SB_VTS_FechaFin__c = '30/10/2024';
            conVal.Status = sStatusCont;
            conVal.MX_SB_SAC_EstatusPoliza__c = 'Activated';
            insert conVal;
            conVal.Status = 'Activated';
            update conVal;
            tstMakeOppoInv(oProduct);
            tstMakeOppoUnS(oProduct);
        }
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'getInsuranceContracts', iaso__Url__c = sUrl, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'getGTServicesSF', iaso__Url__c = sUrl, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
    }

    /**
     * @description: Instrucción de Clase de Prueba para validar
     *               si el contrato es invalido
     * @author: 	 Alexandro Corzo
     */
    public static void tstMakeOppoInv(Product2 objProduct) {
        User oUsrTestInv = null;
        oUsrTestInv = MX_WB_TestData_cls.crearUsuario('TestUserInv', System.label.MX_SB_VTS_ProfileAdmin);
        insert oUsrTestInv;
        System.runAs(oUsrTestInv) {
            final Account oAccTestInv = MX_WB_TestData_cls.crearCuenta(sContratos, sPersonAcct);
            oAccTestInv.PersonEmail = 'testinv@test.com';
            oAccTestInv.PersonMobilePhone = '5555555552';
            insert oAccTestInv;
            final Opportunity oppInv = MX_WB_TestData_cls.crearOportunidad('Opportunity Invalida', oAccTestInv.Id, oUsrTestInv.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            oppInv.Reason__c = sVenta;
            oppInv.Producto__c = System.Label.MX_SB_VTS_Hogar;
            oppInv.StageName = System.Label.MX_SB_VTS_COTIZACION_LBL;
            oppInv.TelefonoCliente__c = oAccTestInv.PersonMobilePhone;
            oppInv.LeadSource = System.Label.MX_SB_VTS_OrigenCallMeBack;
            insert oppInv;
            final Contract conInv = MX_WB_TestData_cls.vtsCreateContract(oAccTestInv.Id, oUsrTestInv.Id, objProduct.Id);
            conInv.MX_WB_Oportunidad__c = oppInv.Id;
            conInv.MX_WB_noPoliza__c = sNoPolizaInv;
            conInv.MX_SB_SAC_NumeroPoliza__c = sNoPolizaInv;
			conInv.StartDate = Date.valueOf('2020-10-01');
            conInv.MX_SB_VTS_FechaInicio__c = '29/10/2020';
            conInv.MX_SB_VTS_FechaFin__c = '29/10/2024';
            conInv.Status = sStatusCont;
            conInv.MX_SB_SAC_EstatusPoliza__c = 'Invalid';
            insert conInv;
        }
    }

    /**
     * @description: Instrucción de Clase de Prueba para validar
     *               si el servicio esta disponible
     * @author: 	 Alexandro Corzo
     */
    public static void tstMakeOppoUnS(Product2 objProduct) {
        User oUsrTestUnS = null;
        oUsrTestUnS = MX_WB_TestData_cls.crearUsuario('TestUserUnS', System.label.MX_SB_VTS_ProfileAdmin);
        insert oUsrTestUnS;
        System.runAs(oUsrTestUnS) {
            final Account oAccTestUnS = MX_WB_TestData_cls.crearCuenta(sContratos, sPersonAcct);
            oAccTestUnS.PersonEmail = 'testuns@test.com';
            oAccTestUnS.PersonMobilePhone = '5555555553';
            insert oAccTestUnS;
            final Opportunity oppUnServ = MX_WB_TestData_cls.crearOportunidad('Opportunity US', oAccTestUnS.Id, oUsrTestUnS.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            oppUnServ.Reason__c = sVenta;
            oppUnServ.Producto__c = System.Label.MX_SB_VTS_Hogar;
            oppUnServ.StageName = System.Label.MX_SB_VTS_COTIZACION_LBL;
            oppUnServ.TelefonoCliente__c = oAccTestUnS.PersonMobilePhone;
            oppUnServ.LeadSource = System.Label.MX_SB_VTS_OrigenCallMeBack;
            insert oppUnServ;
            final Contract conUnServ = MX_WB_TestData_cls.vtsCreateContract(oAccTestUnS.Id, oUsrTestUnS.Id, objProduct.Id);
            conUnServ.MX_WB_Oportunidad__c = oppUnServ.Id;
            conUnServ.MX_WB_noPoliza__c = sNoPolizaUS;
            conUnServ.MX_SB_SAC_NumeroPoliza__c = sNoPolizaUS;
			conUnServ.StartDate = Date.valueOf('2020-11-01');
            conUnServ.MX_SB_VTS_FechaInicio__c = '28/10/2020';
            conUnServ.MX_SB_VTS_FechaFin__c = '28/10/2024';
            conUnServ.Status = sStatusCont;
            conUnServ.MX_SB_SAC_EstatusPoliza__c = 'Unavailable Service';
            insert conUnServ;
        }
    }
    
    /**
     * @description: Instrucción de Clase de Prueba para validar
     *               la clase Service: Poliza Valida
     * @author: 	 Alexandro Corzo
     */
    @isTest static void validateService() {
        final User oUsr = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(oUsr) {
            final Map<String, String> mHeadersMock = new Map<String, String>();
            mHeadersMock.put('tsec','1029384765');
            final MX_WB_Mock objMockCallOut = new MX_WB_Mock(200, 'Complete', '{"data": [{"id": "Sx0NaPO-BCQAjFNdbvFavA","policyNumber": "N80E9A0005","productType": "LIFE_HEALTH","product": {"id": "4027","name": "VIDA INDIVIDUAL","plan": {"id": "001","planType": {"id": "001","name": "Seguro Estudia"}}},"insuredAmount": {"amount": 0,"currency": "MXN"},"status": {"id": "PENDING_ACTIVATION","description": "Pending Activation","reason": {"id": "INC","description": "INCLUIDA"}},"insuranceCompany": {"id": "02","name": "EMPRESA"},"paymentConfiguration": {"frequency": {"id": "MONTHLY","name": "MENSUAL"},"paymentType": "DIRECT_DEBIT"},"validityPeriod": {"startDate": "2018-11-28","endDate": "2019-11-28"},"currentInstallment": {"period": {}},"renewalPolicy": {"counter": 0},"certificateNumber": "1","subscriptionType": "INDIVIDUAL"}]}', mHeadersMock);
            iaso.GBL_Mock.setMock(objMockCallOut);
            Test.startTest();
	            final Map<String, Object> oTest = MX_SB_VTS_GetSetInsContract_Service.obtQueryInsuranceContract(sNoPoliza);
            Test.stopTest();
            System.assert(!oTest.isEmpty(), sNoEmpty);
        }
    }
    
    /**
     * @description: Instrucción de Clase de Prueba para validar
     *               la clase Service: Poliza Invalida
     * @author: 	 Alexandro Corzo
     */
    @isTest static void invalidateService() {
    	final User oUsrInv = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(oUsrInv) {
            final Map<String, String> mHeaderMockInv = new Map<String, String>();
            mHeaderMockInv.put('tsec','0987654321');
            final MX_WB_Mock objMockCallOutInv = new MX_WB_Mock(200, 'Complete', '{"data": [{"id": "Sx0NaPO-BCQAjFNdbvFavA","policyNumber": "1029384756","productType": "LIFE_HEALTH","product": {"id": "4027","name": "VIDA INDIVIDUAL","plan": {"id": "001","planType": {"id": "001","name": "Seguro Estudia"}}},"insuredAmount": {"amount": 0,"currency": "MXN"},"status": {"id": "PENDING_ACTIVATION","description": "Pending Activation","reason": {"id": "INC","description": "INCLUIDA"}},"insuranceCompany": {"id": "02","name": "EMPRESA"},"paymentConfiguration": {"frequency": {"id": "MONTHLY","name": "MENSUAL"},"paymentType": "DIRECT_DEBIT"},"validityPeriod": {"startDate": "2018-11-28","endDate": "2019-11-28"},"currentInstallment": {"period": {}},"renewalPolicy": {"counter": 0},"certificateNumber": "1","subscriptionType": "INDIVIDUAL"}]}', mHeaderMockInv);
            iaso.GBL_Mock.setMock(objMockCallOutInv);
            Test.startTest();
            	final Map<String, Object> oTestInv = MX_SB_VTS_GetSetInsContract_Service.obtQueryInsuranceContract(sNoPolizaInv);
            Test.stopTest();
            System.assert(!oTestInv.isEmpty(), sNoEmpty);
        }
    }
    
    /**
     * @description : Instrucción de Clase de prueba para validar
     * 				  la clase Service: Sin Contenido
     * @author		: Alexandro Corzo
     */
    @isTest static void noContentService() {
        final User oUsrNCont = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(oUsrNCont) {
        	final Map<String, String> mHeaderMockNCont = new Map<String, String>();
            mHeaderMockNCont.put('tsec','0987654321');
            final MX_WB_Mock objMCallONCont = new MX_WB_Mock(204, 'Complete', '', mHeaderMockNCont);
            iaso.GBL_Mock.setMock(objMCallONCont);
            Test.startTest();
            	final Map<String, Object> oTestNCont = MX_SB_VTS_GetSetInsContract_Service.obtQueryInsuranceContract(sNoPoliza);
            Test.stopTest();
            System.assert(!oTestNCont.isEmpty(), sNoEmpty);
        }
    }
    
    /**
     * @description: Instrucción de Clase de Prueba para validar
     *               la clase Service: Poliza en Contrato No Existe
     * @author: 	 Alexandro Corzo
     */
    @isTest static void valPolizContOppo() {
        final User oUsrVal = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(oUsrVal) {
        	final Opportunity objOpportunity = [SELECT Id FROM Opportunity WHERE Name = 'Opportunity Contratos'];
            final Map<String, Object> oParams = new Map<String, Object>();
            oParams.put('isExist', 'false');
            oParams.put('policyNumber',sNoPolizaNEx);
            oParams.put('startDateContract', '2020-08-31');
            oParams.put('endDateContract', '2024-08-31');
            oParams.put('isAvailableSrv','true');
            Test.startTest();
            final Map<String, Object> objResult = MX_SB_VTS_GetSetInsContract_Service.obtQueryValPolizContractOppo(oParams, objOpportunity.Id);
            Test.stopTest();
            System.assert(!objResult.isEmpty(), sNoEmpty);
        }
    }
    
    /**
     * @description: Instrucción de Clase de Prueba para validar
     *               la clase Service: Poliza en Contrato Existe
     * @author: 	 Alexandro Corzo
     */
    @isTest static void valPolizContOppoEx() {
        final User oUsrValEx = MX_WB_TestData_cls.crearUsuario(sUserName, System.label.MX_SB_VTS_ProfileAdmin);
        System.runAs(oUsrValEx) {
            final Opportunity objOppoExist = [SELECT Id FROM Opportunity WHERE Name = 'Opportunity Contratos'];
            final Map<String, Object> oParamsEx = new Map<String, Object>();
            oParamsEx.put('isExist', 'true');
            oParamsEx.put('policyNumber',sNoPolizaEx);
            oParamsEx.put('startDateContract', '2020-08-31');
            oParamsEx.put('endDateContract', '2024-08-31');
            oParamsEx.put('isAvailableSrv','true');
            Test.startTest();
            final Map<String, Object> objResultEx = MX_SB_VTS_GetSetInsContract_Service.obtQueryValPolizContractOppo(oParamsEx, objOppoExist.Id);
            Test.stopTest();
            System.assert(!objResultEx.isEmpty(), sNoEmpty);
        }
    }
    
    /**
     * @description: Instrucción de Clase de Prueba para validar
     * 				 poliza valida
     * @author: 	 Alexandro Corzo
     */
    @isTest
    static void testObtDataPolCont() {
        Test.startTest();
        final Map<String, Object> mPolizVal = MX_SB_VTS_GetSetInsContract_Service.obtDataPolizContract(sNoPoliza);
        Test.stopTest();
        System.assert(!mPolizVal.isEmpty(), 'Se obtuvieron los registros!');
    }
    
    /**
     * @description: Instrucción de Clase de Prueba para validar
     * 				 poliza Invalida
     * @author: 	 Alexandro Corzo
     */
    @isTest
    static void testObtDataPolContInv() {
        Test.startTest();
        final Map<String, Object> mPolizValInv = MX_SB_VTS_GetSetInsContract_Service.obtDataPolizContract(sNoPolizaInv);
        Test.stopTest();
        System.assert(!mPolizValInv.isEmpty(), 'Se obtuvieron los registros!');
    }
}