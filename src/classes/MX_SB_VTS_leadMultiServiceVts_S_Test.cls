/**
 * @File Name          : MX_SB_VTS_leadMultiServiceVts_S_Test.cls
 * @Description        : 
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 07-15-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    13/6/2020   Eduardo Hernández Cuamatzi     Initial Version
**/
@isTest
public class MX_SB_VTS_leadMultiServiceVts_S_Test {
    /**Origen call me back */
    final static String TYPECMB = 'CALLMEBACK';
    /**Origen call me back plus */
    final static String TYPECMBPR = 'CALLMEBACK';
    /**Origen Tracking web */
    final static String TRACKING = 'TRACKING';
    /**Folio Quote Hogar Seguro Dinámico*/
    final static String FOLIOHSD = '1234541';
    /**Mensaje correcto Seguro Dinámico*/
    final static String MSJSUCCESS = 'Origen correcto';
    /**Variable de Apoyo: TELEPHONE */
    final static String TELEPHONE = '1212345678';
    /**Variable de Apoyo: CORREOE */
    final static String CORREOE = 'japerez@example.com';
    /**Variable de Apoyo: DATEAPPOINT */
    final static String DATEAPPOINT = '2020-06-06T16:00:00Z';

    @TestSetup
    static void makeData() {
        final User IntegrationUser = MX_WB_TestData_cls.crearUsuario('IntegrationUser', 'IntegrationUser');
        insert IntegrationUser;
        MX_SB_VTS_CallCTIs_utility.initLeadMulServ();
        MX_SB_VTS_CallCTIs_utility.initLeadMulServHSD();
        genOppsHSDDinamic();
    }

    /**
    * @description Genera registros para hogar dinamico tw
    * @author Eduardo Hernández Cuamatzi | 09-04-2020 
    **/
    public static void genOppsHSDDinamic() {
        final User integration = MX_WB_TestData_cls.crearUsuario('Integration', System.Label.MX_SB_VTS_ProfileIntegration);
        insert integration;
        //final Pricebook2 prB2 = MX_WB_TestData_cls.createStandardPriceBook2();
        final Map<String, String> mapValsHSD = new Map<String, String>();
        mapValsHSD.put('lastNameUser', 'Hogarhsd');
        mapValsHSD.put('email', 'test.hogarhsd@gmail.com');
        mapValsHSD.put('asesorId', integration.Id);
        mapValsHSD.put('oppName', 'opp Name HSD');
        mapValsHSD.put('oppDevRecId', System.Label.MX_SB_VTS_Telemarketing_LBL);
        mapValsHSD.put('productName', System.Label.MX_SB_VTS_PRO_HogarDinamico_LBL);
        mapValsHSD.put('stageName', 'Cotización');
        mapValsHSD.put('quoteName', 'Quote HSD');
        mapValsHSD.put('folioQuote', FOLIOHSD);
        mapValsHSD.put('prB2Id', Test.getStandardPricebookId());
        MX_SB_VTS_CallCTIs_utility.initDataMulti(mapValsHSD);
    }

    /**
    * @description Genera y Retorna JSON de Prueba
    * @author Alexnadro Corzo | 24-09-2020 
    **/
    public static String otbJsonTest() {
        String tstJson = '{"type":"'+TYPECMB+'","channel":"10000120",';
        tstJson += '"quote":{"id":"123456"},"coupon":"MA0001","person":{"name":"Juan","firstSurname":"Gutiérrez",';
        tstJson += '"contactDetails":[{"key":"PER-PHONE-1","value":"'+TELEPHONE+'"},{"key":"PER-EMAIL","value":"'+CORREOE+'"}]},';
        tstJson += '"appointment":{"time":"'+DATEAPPOINT+'"}}';
        return tstJson;
    }
    
    @isTest
    private static void insertCMB() {
        final String jsoncmb = otbJsonTest();
        Test.startTest();
            final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'IntegrationUser'];
            System.runAs(cUser) {
                final Map<String, Object> listener = (Map<String, Object>)JSON.deserializeUntyped(JSONCMB);
                final MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta dataService = MX_SB_VTS_leadMultiServiceVts_Service.fillDataServs(listener);
                System.assertEquals(TYPECMB, dataService.type, MSJSUCCESS);
            }
        Test.stopTest();
    }

    @isTest
    private static void insertCMBPR() {
        final String jsoncmbPR = otbJsonTest();
        Test.startTest();
            final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'IntegrationUser'];
            System.runAs(cUser) {
                final Map<String, Object> listener = (Map<String, Object>)JSON.deserializeUntyped(jsoncmbPR);
                final MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta dataService = MX_SB_VTS_leadMultiServiceVts_Service.fillDataServs(listener);
                System.assertEquals(TYPECMBPR, dataService.type, MSJSUCCESS);
            }
        Test.stopTest();
    }

    @isTest
    private static void insertCMBPRWoP() {
        final String jsoncmbWop = otbJsonTest();
        Test.startTest();
            final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'IntegrationUser'];
            System.runAs(cUser) {
                final Map<String, Object> listener = (Map<String, Object>)JSON.deserializeUntyped(jsoncmbWop);
                final MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta dataService = MX_SB_VTS_leadMultiServiceVts_Service.fillDataServs(listener);
                MX_SB_VTS_leadMultiServiceVts_Service.processPost(dataService);
                System.assertEquals(TYPECMBPR, dataService.type, MSJSUCCESS);
            }
        Test.stopTest();
    }

    @isTest
    private static void insertCMBNoCotiz() {
        String jsonNoCotiz = '{"type":"'+TYPECMB+'","channel":"10000120", "product":{"code": "3002", "plan":{"code": "008"}},';
        jsonNoCotiz += '"quote":{"id":""},"coupon":"MA0001","person":{"name":"Juan","firstSurname":"Gutiérrez",';
        jsonNoCotiz += '"contactDetails":[{"key":"PER-PHONE-1","value":"'+TELEPHONE+'"},{"key":"PER-EMAIL","value":"'+CORREOE+'"}]},';
        jsonNoCotiz += '"appointment":{"time":"'+DATEAPPOINT+'"}}';
        Test.startTest();
        	final User cUserNoCotiz = [SELECT Id, MX_SB_VTS_ProveedorCTI__c, Name FROM User WHERE Name = 'IntegrationUser'];
        	System.runAs(cUserNoCotiz) {
            	final Map<String, Object> listNoCotiz = (Map<String, Object>) JSON.deserializeUntyped(jsonNoCotiz);
                final MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta dataSrvNoCotiz = MX_SB_VTS_leadMultiServiceVts_Service.fillDataServs(listNoCotiz);
                MX_SB_VTS_leadMultiServiceVts_Service.processPost(dataSrvNoCotiz);
                System.assertEquals(TYPECMB, dataSrvNoCotiz.type, MSJSUCCESS);
        	}
        Test.stopTest();
    }

    @isTest static void originTwHSD() {
        final Map<String, String> mapVals = MX_SB_VTS_leadMultiServiceVts_C_Test.createMapJsonHSD(new List<String>{'', '5512341245', 'test.tracking@bbbva.com', '12345'});
        final String jsonString = MX_SB_VTS_leadMultiServiceVts_C_Test.generateWSbody(mapVals);
        Test.startTest();
            final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'IntegrationUser'];
            System.runAs(cUser) {
                final Map<String, Object> listener = (Map<String, Object>)JSON.deserializeUntyped(jsonString);
                final MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta dataService = MX_SB_VTS_leadMultiServiceVts_Service.fillDataServs(listener);
                MX_SB_VTS_leadMultiServiceVts_Service.processPost(dataService);
                System.assertEquals(TRACKING, dataService.type, MSJSUCCESS);
            }
        Test.stopTest();
    }
}