@isTest
/**
 * @File Name          : MX_SB_VTS_ListPlaces_Service_Test
 * @Description        :
 * @Author             : Marco Antonio Cruz Barboza
 * @Group              :
 * @Last Modified By   : Marco Antonio Cruz Barboza
 * @Last Modified On   : 26/12/2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    26/12/2020    Marco Antonio Cruz Barboza         Initial Version
**/
public class MX_SB_VTS_ListPlaces_Service_Test {
    /** User name testing */
    final static String TESTINGUSR = 'UserTest';
    /** Account Name testing*/
    final static String TESTACCOUNT = 'Test Account';
    /** Opportunity Name Testintg */
    final static String TESTOPPORT = 'Test Opportunity';
    /**URL Mock Test*/
    Final static String TESTURL = 'http://www.example.com';
    /**URL Mock Test*/
    Final static String POSTALCD = '07469';
    
    /* 
    @Method: setupTestL
    @Description: create test data set
    */
    @TestSetup
    static void setupTestL() {
        final User usrTestL = MX_WB_TestData_cls.crearUsuario(TESTINGUSR, System.Label.MX_SB_VTS_ProfileAdmin);
        System.runAs(usrTestL) {
            final Account tstAccL = MX_WB_TestData_cls.crearCuenta(TESTACCOUNT, System.Label.MX_SB_VTS_PersonRecord);
            tstAccL.PersonEmail = 'pruebaVts@mxvts.com';
            insert tstAccL;
            final Opportunity tstOpportL = MX_WB_TestData_cls.crearOportunidad(TESTOPPORT, tstAccL.Id, usrTestL.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            tstOpportL.LeadSource = 'Call me back';
            tstOpportL.Producto__c = 'Hogar';
            tstOpportL.Reason__c = 'Venta';
            tstOpportL.StageName = 'Cotizacion';
            insert tstOpportL;
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'listPlaces', iaso__Url__c = TESTURL, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
            
        }
    }
    
    /*
    @Method: getZipCodeTest
    @Description: return Map with a List of objects from a service
	@param
    */
    @isTest
    static void getZipCodeTest() {
        Final Map<String,String> tsecTest = new Map<String,String> {'tsec'=>'1234456789'};
		Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, 'Complete', '{"data": [{"id": "636|Pedregal de Santo Domingo","name": "Pedregal de Santo Domingo","geolocation": {"latitude": 19.328390326852794,"longitude": -99.16694122424343},"geographicPlaceTypes": [{"id": "MUNICIPALITY","name": "Municipality","value": "Coyoacán"},{"id": "ADMINISTRATIVE_AREA_4","name": "Administrative Area 4"}],"state": {"id": "DF","name": "Ciudad de México"},"country": {"id": "1","name": "México"}}]}', tsecTest); 
        iaso.GBL_Mock.setMock(mockCallout);
        test.startTest();
            Final Map<String,List<Object>> testResponse = MX_SB_VTS_ListPlaces_Service.getGeolocation(POSTALCD);
            System.assertNotEquals(testResponse, null,'El mapa esta retornando');
        test.stopTest();
    }
    
    /*
    @Method: badService
    @Description: test method to check if the webservice Fail
	@param
    */
    @isTest
    static void badService() {
        Final Map<String,String> tsecTest = new Map<String,String> {'tsec'=>'1234456789'};
		Final MX_WB_Mock mockCallout = new MX_WB_Mock(500, 'Error System', '', tsecTest); 
        iaso.GBL_Mock.setMock(mockCallout);
        test.startTest();
            Final Map<String,List<Object>> testResponse = MX_SB_VTS_ListPlaces_Service.getGeolocation(POSTALCD);
            System.assertNotEquals(testResponse, null,'El mapa esta retornando');
        test.stopTest();
    }
    
    /*
    @Method: withoutGeolocation
    @Description: test method to send a response fake without geolocation object
	@param
    */
   	@isTest
    static void withoutGeolocation() {
        Final Map<String,String> tsecTest = new Map<String,String> {'tsec'=>'1234456789'};
		Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, 'Complete', '{"data": [{"id": "636|Pedregal de Santo Domingo","name": "Pedregal de Santo Domingo","geographicPlaceTypes": [{"id": "MUNICIPALITY","name": "Municipality","value": "Coyoacán"},{"id": "ADMINISTRATIVE_AREA_4","name": "Administrative Area 4"}],"state": {"id": "DF","name": "Ciudad de México"},"country": {"id": "1","name": "México"}}]}', tsecTest); 
        iaso.GBL_Mock.setMock(mockCallout);
        test.startTest();
            Final Map<String,List<Object>> testResponse = MX_SB_VTS_ListPlaces_Service.getGeolocation(POSTALCD);
            System.assertNotEquals(testResponse, null,'El mapa esta retornando');
        test.stopTest();
    }
}