/**
 * @description       : Clase test que sirve de apoyo para la clase MX_SB_VTS_InitQuo_Ctrl
 * @author            : Diego Olvera
 * @group             : BBVA
 * @last modified on  : 02-17-2021
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   01-08-2021   Diego Olvera   Initial Version
**/
@isTest
public class MX_SB_VTS_InitQuo_C_Test {
/*User name test*/
    private final static String USERQUO = 'ASESORDATAPART';
    /**@description Nombre de la cuenta ACCCMBQUO*/
    private final static String ACCCMBQUO = 'TestCmb';
    /**@description Nombre de Oportunidad OPPCMBQUO*/
    private final static String OPPCMBQUO = 'TestOppCmb';
    /**@description Nombre de valor de Propio*/
    private final static String VALPRO = 'Propio';
    
    @TestSetup
    static void makeData() {
        final User tstDpUserQUO = MX_WB_TestData_cls.crearUsuario(USERQUO, 'System Administrator');
        insert tstDpUserQUO;
        final Account accntQUO = MX_WB_TestData_cls.createAccount(ACCCMBQUO, System.Label.MX_SB_VTS_PersonRecord);
        insert accntQUO;
        final Opportunity oppCmQuote = MX_WB_TestData_cls.crearOportunidad(OPPCMBQUO, accntQUO.Id, tstDpUserQUO.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
        oppCmQuote.LeadSource = 'Facebook';
        oppCmQuote.Producto__c = 'Hogar seguro dinámico';
        oppCmQuote.StageName = 'Contacto';
        insert oppCmQuote;
    }
    
    @isTest
    static void ctrDataQuoteCtrlTest() {
        final User userGetQUO = [Select Id from User where LastName =: USERQUO];
        System.runAs(userGetQUO) {
            final Opportunity oppLstQUO = [Select Id, StageName, Producto__c, Name from Opportunity Where Name =: OPPCMBQUO];
             final String newVal = VALPRO;
            Test.startTest();
            try {
                MX_SB_VTS_InitQuo_Ctrl.ctrDataQuoteCtrl(oppLstQUO.Id, newVal);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Error');
                System.assertEquals('Mal', extError.getMessage(),'No se accedió a la función');
            }
            Test.stopTest();
        }
    }

     @isTest
    static void updSyncAddTest() {
        final User userNw = [Select Id from User where LastName =: USERQUO];
        System.runAs(userNw) {
            final Opportunity oppLstUpdSy = [Select Id, StageName, Name from Opportunity Where Name =: OPPCMBQUO];
            Test.startTest();
            try {
                MX_SB_VTS_InitQuo_Ctrl.updSyncAddCtrl(oppLstUpdSy.Id);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Mistake fatal');
                System.assertEquals('Error failed', extError.getMessage(),'No obtuvo la opp con el valor');
            }
            Test.stopTest();
        }
    }
}