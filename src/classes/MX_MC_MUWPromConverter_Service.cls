/**
 * @File Name           : MX_MC_MUWPromConverter_Service.cls
 * @Description         :
 * @Author              : Jair Ignacio Gonzalez Gayosso
 * @Group               :
 * @Last Modified By    : Jair Ignacio Gonzalez Gayosso
 * =============================================================================
 * Ver          Date                     Author                     Modification
 * =============================================================================
 * 1.0      24/08/2020  Jair Ignacio Gonzalez Gayosso      Initial Version
 **/
global without sharing class MX_MC_MUWPromConverter_Service extends mycn.MC_VirtualServiceAdapter { //NOSONAR

    /**
    * @Description PARAM_THREAD property
    **/
    private static final String PARAM_THREAD = 'messageThreadId';

    /**
    * @Description CLASSNAME property
    **/
    public static final String CLASSNAME = MX_MC_MUWPromConverter_Service.class.getName();

    /**
    * @Description Returns a generic Object based on method call from Class in MC component
    * @author Jair Ignacio Gonzalez Gayosso | 24/08/2020
    * @param parameters
    * @return Map<String, Object>
    **/
    global override Map<String, Object> convertMap(Map<String, Object> parameters) {

        final Map<String, Object> inputMX = new Map<String, Object>();

        if (parameters.containsKey('messageId')) {
            inputMX.put('messageId', JSON.deserializeUntyped(String.valueOf(parameters.get('messageId'))) );
            inputMX.put(PARAM_THREAD,parameters.get(PARAM_THREAD));
            inputMX.put('message',parameters.get('content'));
            inputMX.put('isDraft', 'false');
        } else {
            final mycn.MC_MessageThreadWrapper.MessageThread bodyParams = (mycn.MC_MessageThreadWrapper.MessageThread)JSON.deserialize(String.valueOf(parameters.get('messageThreadBody')),mycn.MC_MessageThreadWrapper.MessageThread.class);
            inputMX.put(PARAM_THREAD,String.valueOf(parameters.get(PARAM_THREAD)));
            inputMX.put('isSolved',String.valueOf(bodyParams.isSolved));
        }

        return inputMX;
    }
}