/**
* @File Name          : MX_RTL_QuoteLineItem_Service.cls
* @Description        :
* @Author             : Daniel Perez Lopez
* @Group              :
* @Last Modified By   : Daniel Perez Lopez
* @Last Modified On   : 12-15-2020
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    9/06/2020   Daniel Perez Lopez          Initial Version
* 1.1    9/07/2020   Juan Carlos Benitez         Se agrega service para selector
* 1.2    12/15/2020  Juan Carlos Benitez         Se añaden campos en QUOTLINEFIELDS
**/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_RTL_QuoteLineItem_Service {
	/** Campos de beneficiario **/
    Final static String QUOTLINEFIELDS = ' id,MX_SB_PS_Dias36omas__c, MX_SB_PS_Dias3a5__c, MX_SB_PS_Dias6a35__c, MX_SB_PS_SumaAsegurada__c, quote.MX_SB_PS_PagosSubsecuentes__c, MX_SB_VTS_Total_Gastos_Funerarios__c, MX_SB_VTS_FormaPAgo__c, MX_SB_VTS_Plan__c,MX_WB_noPoliza__c,MX_WB_Folio_Cotizacion__c,QuoteId,UnitPrice, Quantity, MX_SB_VTS_Tipo_Plan__c,MX_SB_VTS_Version__c ';
    /** Campos de CONDITION1 **/
    Final static String  CONDITION1 ='QuoteId';
    /**
    * @description
    * @author Juan Carlos Benitez | 9/07/2020
    * @param QuoteId
    * @return List<QuoteLineItem>
    **/
	public static List<QuoteLineItem> getQuoteLIbyQuote( String value) {
		return MX_RTL_QuoteLineItem_Selector.getQuoteLineitemBy(CONDITION1, QUOTLINEFIELDS,value);
    }
}