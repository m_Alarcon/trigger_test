/**
* @File Name          : MX_RTL_Quote_Selector.cls
* @Description        :
* @Author             : Juan Carlos Benitez
* @Group              :
* @Last Modified By   : Diego Olvera
* @Last Modified On   : 02-11-2021
* @Modification Log   :
* Ver       Date            Author      		 Modification
* 1.0    7/02/2020   Juan Carlos Benitez        Initial Version
* 1.1    19/08/2020    Omar Gonzalez Rubio      Se agregan nuevas funciones
* 1.2    14-12-2020   Daniel Perez Lopez        Se añade methodo insertQuotes
* 1.3    03-03-2020   Juan Carlos Benitez       Se agrega campo a QUOTEFIELDS
* 1.4    11-03-2020    Juan Carlos Benitez      Se cambia Fields por fields
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton, sf:TooManyMethods')
public class MX_RTL_Quote_Selector {
    /** list of possible filters */
    final static String SELECT_STR = 'SELECT ';
    /**campos de quote */
    final static string QUOTEFIELDS = 'Status, MX_SB_VTS_Nombre_Contrante__c, ShippingPostalCode, MX_SB_VTS_ASO_FolioCot__c, MX_SB_VTS_Email_txt__c, QuoteToPostalCode, QuoteToName,'+
    'QuoteToAddress, QuoteToCity,MX_SB_VTS_Sexo_Conductor__c, QuoteToCountry, IsSyncing, QuoteToState, MX_SB_VTS_Apellido_Paterno_Contratante__c, MX_SB_VTS_Apellido_Materno_Contratante__c,'+
    'MX_SB_VTS_Numero_de_Poliza__c,MX_SB_VTS_Cupon__c, MX_SB_VTS_LugarNac_Contratante__c, MX_SB_VTS_FechaNac_Contratante__c, MX_SB_VTS_Familia_Productos__c,MX_SB_VTS_Familia_Productos__r.Name,MX_SB_VTS_RFC__c,OpportunityId,'+
    'MX_SB_VTS_Colonia__c, Phone, Email, MX_SB_VTS_Homoclave_Contratante__c, MX_SB_VTS_Curp_Contratante__c, TotalPrice, MX_SB_VTS_TotalAnual__c, MX_SB_VTS_TotalMonth__c, MX_SB_VTS_TotalSemiAnual__c, MX_SB_VTS_TotalQuarterly__c, MX_ASD_PCI_ResponseCode__c, MX_SB_VTS_GCLID__c';
    
    /**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param quoteObj
* @return Quote Id
**/ 
    public static List<Quote> getQuote(Quote quoteObj) {
		return [SELECT Id, OpportunityId from Quote where OpportunityId =:quoteObj.OpportunityId];
    }
/**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param quoteObj
* @return quoteObj
**/ 
    public static Quote upsrtQuote(Quote quoteObj) {
        upsert quoteObj;
        return quoteObj;
    }
/**
* @funcion que retorna una cotizacion con producto
**/ 
    public static List<Quote> quoteData(String quoteFol,String product) {
        return [select Id,Name,Opportunity.Campanya__c,MX_SB_VTS_Folio_Cotizacion__c,(Select Product2.Name from QuoteLineItems where Product2.Name=: product) from Quote 
                where MX_SB_VTS_Folio_Cotizacion__c=: quoteFol];
    }
    /**
* @funcion que retorna una cotizacion
**/ 
    public static List<Quote> quoteData(String quoteFol) {
        return [select Id,Name,MX_SB_VTS_Folio_Cotizacion__c from Quote where MX_SB_VTS_Folio_Cotizacion__c=: quoteFol];
    }
     /**
    * @funcion que retorna una cotizacion
    **/ 
    public static Quote findQuoteById(string quoteId,String fields) {
        final String query = 'SELECT '+fields+' FROM Quote WHERE Id =: quoteId';
        return DataBase.query(String.escapeSingleQuotes(query));
    }
    /**
* @funcion que retorna respuesta de cobro de la cotización
**/ 
    public static List<Quote> quoteResponseIvr(String folQuote, String idQuote) {
        return [SELECT Id, MX_SB_VTS_Folio_Cotizacion__c, Opportunity.Campanya__c, MX_ASD_PCI_IdCotiza__c, MX_ASD_PCI_ResponseCode__c, MX_ASD_PCI_DescripcionCode__c, MX_ASD_PCI_MsjAsesor__c 
                FROM Quote 
                WHERE MX_SB_VTS_Folio_Cotizacion__c =: folQuote 
                AND MX_ASD_PCI_IdCotiza__c =: idQuote];
        
    }
    /**
* @funcion que recupera idCotiza y token antifraude de la cotización
**/ 
    public static Quote quoteRetrieve(String idOppQuote,String folQuote) {
        return [SELECT Id, OpportunityId, MX_ASD_PCI_FolioAntifraude__c, MX_ASD_PCI_IdCotiza__c, MX_SB_VTS_Folio_Cotizacion__c, MX_ASD_PCI_DescripcionCode__c, MX_ASD_PCI_MsjAsesor__c, MX_ASD_PCI_ResponseCode__c FROM Quote
               WHERE OpportunityId =: idOppQuote
               AND MX_SB_VTS_Folio_Cotizacion__c =: folQuote];
    }
    /**
* @funcion que recupera codigo de error que retorna el IVR
**/ 
    public static  List<MX_RTL_IVRCodes__mdt> getMsjError(String codeError) {
        return [SELECT MX_RTL_IVRCode__c, MX_RTL_MsgIVR__c, MX_RTL_MsgSF__c, MX_RTL_TitleMsg__c, MX_RTL_isActive__c 
                FROM MX_RTL_IVRCodes__mdt 
                WHERE MX_RTL_IVRCode__c =: codeError
               ];
    }

    /**
    * @description 
    * @author Julio Medellin | 11-25-2020 
    * @param listQuote 
    * @return List<Quote> 
    **/
    public static List<Quote> insertQuotes(Quote[] listQuote) {
        insert listQuote;
        return listQuote;
    }

    /* @description Actualiza lista de Quotes
    * @author Eduardo Hernández Cuamatzi | 12-10-2020 
    * @param lstQuotes Lista de quotes ah actualizar
    * @param allOrNone Indica si continua upsert si un registro falla
    * @return List<Database.UpsertResult> Lista de Quotes actualizadas
    **/
    public static List<Database.UpsertResult> upsrtQuote(List<Quote> lstQuotes, Boolean allOrNone) {
        return Database.upsert(lstQuotes, allOrNone);
    }
    
     /**
    * @description Lista de quotes
    * @author Eduardo Hernández Cuamatzi | 12-10-2020 
    * @return List<Database.UpsertResult> Lista de Quotes
    **/
    public static List<Quote> resultQryQuote(String queryfield, String conditionField, boolean whitCondition) {
        String sQuery;
        switch on String.valueOf(whitCondition) {
          when 'false' {
              sQuery = SELECT_STR + queryfield +' FROM Quote';
          }
          when 'true' {
              sQuery = SELECT_STR + queryfield +' FROM Quote WHERE '+conditionField;
          }
      	}
        return Database.query(String.escapeSingleQuotes(sQuery).unescapeJava());
    }
    
    /**Elimina lista de cotizaciones */
    public static void deletQuote(List<Quote> lstQuotes) {
        delete lstQuotes;
    }
    
    /**
    * @description Recupera Quotes de Oportunidades
    * @author Eduardo Hernandez Cuamatzi | 25/5/2020 
    * @param List<Opportunity> lstOpps Lista de Oportunidades
    * @return List<Quote> Lista de Cotizaciones
    **/
    public static List<Quote> selectSObjectsByOpps(List<Opportunity> lstOpps) {
        final String quoteQuery = 'Select Id, Name,' + QUOTEFIELDS + ' from Quote where OpportunityId IN: lstOpps';
        return Database.query(String.escapeSingleQuotes(quoteQuery).unescapeJava());
    }

    /**
    * @description Recupera Quotes por Id de Quote
    * @author Eduardo Hernandez Cuamatzi | 25/5/2020 
    * @param String QuoteId Id de Quote
    * @return List<Quote> Lista de Cotizaciones
    **/
    public static List<Quote> selectSObjectsById(String idQuote) {
        final String quoteQuery = 'Select Id, Name,' + QUOTEFIELDS + ' from Quote where (Id =: idQuote OR QuoteToName =: idQuote)';
        return Database.query(String.escapeSingleQuotes(quoteQuery).unescapeJava());
    }
    /**
  * @description Ejecuta update de registros para Quote
  * @author Diego Olvera | 18/12/2020
  * @param List<Quote> lstOpps  Lista de Quotes a procesar
  * @param Boolean allOrNone Indica si se lanza una operacion de exception
  * @return List<Database.SaveResult> Lista de resultados de la operación DML
  **/
  public static List<Database.SaveResult> updateResult(List<Quote> lstQuote, Boolean allOrNone) {
    return Database.update(lstQuote, allOrNone);
}
}