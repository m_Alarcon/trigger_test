/**
 * @description       : 
 * @author            : Daniel Perez Lopez
 * @group             : 
 * @last modified on  : 11-25-2020
 * @last modified by  : Daniel Perez Lopez
 * Modifications Log 
 * Ver   Date         Author               Modification
 * 1.0   11-19-2020   Daniel Perez Lopez   Initial Version
 * 1.1   2-11-2021    Juan Carlos Benitez  Se ajusta product2 y consultas
 * 1.2   2-23-2021    Juan Carlos Benitez  Se ajusta product2 multiproducto
**/
@SuppressWarnings(' sf:UseSingleton ,sf:AvoidUsingTestIsRunningTest,sf:DUDataflowAnomalyAnalysis')
public with sharing class MX_SB_PS_OpportunityTrigger {
/**
* @description 
* @author Daniel.Perez.Lopez.contractor@bbva.com | 11-24-2020 
* @param Opportunity[] opps 
**/
public static void createQLT(Opportunity[] opps) {
    final Id rstype = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_PS_RSHospitalario).getRecordTypeId();
    final List<Opportunity> oppsli = new List<Opportunity>(); 
    for(Opportunity opport: opps) {
        if(rstype==opport.RecordTypeId) {
            oppsli.add(opport);
        }
    }
    if(!oppsli.isEmpty()) {
        createQLText(oppsli);
    }
}
/**
* @description 
* @author Arsenio.perez.lopez.contractor@bbva.com | 11-25-2020 
* @param Opportunity[] opps 
**/
public static void createQLText(Opportunity[] opps) {
    Quote[] listaquoteinsert = new Quote[]{};
        final QuoteLineItem[] listaqlinsert = new QuoteLineItem[]{};
        final map<String,quote> mapdata = new map<String,quote>();
    	final Set<String> process = new Set<String>();
        final Set<String> names = new Set<String>();
        final ID pcb = Test.isRunningTest()? Test.getStandardPricebookId(): [select id, name from Pricebook2 where isStandard = true limit 1].Id;
        for(opportunity item : opps) {
            listaquoteinsert.add(new Quote(Name=item.Name,OpportunityId=item.id,Pricebook2Id=pcb));
            names.add(item.Producto__c);
        }
        process.add('SAC');
    	final Product2[] producto= MX_RTL_Product2_Selector.findProsByNameProces(names,process,true);
        if(!listaquoteinsert.isEmpty()) {
            listaquoteinsert = MX_RTL_Quote_Selector.insertQuotes(listaquoteinsert);
            for (Quote itemq : listaquoteinsert) {
                mapdata.put(itemq.OpportunityId,itemq);
                listaqlinsert.add(obtieneQlt(itemq,producto[0].Id));
            }
        }
        if(!listaqlinsert.isEmpty()) {
            MX_RTL_QuoteLineItem_Selector.upsertQuoteLI(listaqlinsert);
        }
}

/**
* @description 
* @author Daniel.Perez.Lopez.contractor@bbva.com | 11-24-2020 
* @param Quote quote 
* @param String prod 
* @return QuoteLineItem 
**/
public static QuoteLineItem obtieneQlt(Quote quote,String prod) {
    final QuoteLineItem qlt = new QuoteLineItem();
    final PricebookEntry[] pbe= MX_RTL_PricebookEntry_Selector.pricebookEntries('Id,PriceBook2Id',new String[]{prod});
    qlt.QuoteId=quote.Id;
    qlt.Product2Id=prod;
    qlt.PricebookEntryId =pbe[0].Id;
    qlt.Quantity=1;
    qlt.UnitPrice=0;
    qlt.MX_WB_Folio_Cotizacion__c='';
    return qlt;
}
}