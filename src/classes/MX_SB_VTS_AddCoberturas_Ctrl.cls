/**
* @description       : Clase controller que hace referencia a MX_SB_VTS_AddCoberturas_Service
* @author            : Diego Olvera
* @group             : BBVA
* @last modified on  : 03-19-2021
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   01-13-2021   Diego Olvera   Initial Version
* 1.1   02-03-2021   Diego Olvera   Ajuste a funcion para update de registros
* 1.1.1 19-03-2021   Diego Olvera   Ajuste a funcion para recuperar valor de categoria desde front
**/
@SuppressWarnings('sf:UseSingleton')
public  without sharing class MX_SB_VTS_AddCoberturas_Ctrl {
    /** String para guardar valor de tipo de Trade zona Hidro */
    static final String TRARIHI = 'RIHI';
    /** String para guardar valor de tipo de Trade zona Sismica */
    static final String TRATERR = 'TERR';
    /** Constructor de la clase */
    private MX_SB_VTS_AddCoberturas_Ctrl() {}
    /**
* @description Función que consume servicio de calculateQuotePrice
* @author Diego Olvera | 04-01-2021 
* @param oppId, coveragesCodes, masterTrade, coberActive
* @return Map<String, Object>
**/  
    @AuraEnabled
    public static Map<String, Object> addCoverages(String oppId, List<String> coveragesCodes, Map<String, Object> mDataCob) {
        final String masterTrade = mDataCob.get('ramo').toString();
        final Map<String, Object> responseService = new Map<String, Object>();
        final Map<String, Object> getCotizData = MX_SB_VTS_DeleteCoverageHSD_ctrl.fillCotizData(oppId);
        final Id quoId = ((List<Opportunity>)getCotizData.get('dataOpp'))[0].SyncedQuoteId;
        final Opportunity oppVal = ((List<Opportunity>)getCotizData.get('dataOpp'))[0];
        final Quote dataQuote = ((Map<Id, Quote>)getCotizData.get('dataQuote')).get(quoId);
        List<Cobertura__c> nwLstCob = new List<Cobertura__c>();
        if(masterTrade == TRARIHI || masterTrade == TRATERR) {
            final String dCategorie = mDataCob.get('categorie').toString();
            coveragesCodes.add(dCategorie);
            nwLstCob = MX_SB_VTS_AddCoberturas_Service.findByTrade(quoId, masterTrade, coveragesCodes);
        } else {
            nwLstCob = MX_SB_VTS_AddCoberturas_Service.findCoverCodes(quoId, coveragesCodes);   
        }
        final Map<String, Object> bodyAddCob = MX_SB_VTS_AddCoberturas_Service.fillAddCoverages(nwLstCob, oppVal, masterTrade, quoId);
        final Map<String, Object> getServiceResp = MX_SB_VTS_AddCoberturas_Service.calculateQuotePrice(JSON.serialize(bodyAddCob), dataQuote.MX_SB_VTS_ASO_FolioCot__c);
        if((Boolean)getServiceResp.get('success200')) {
            final Map<String,Object> responseServ = (Map<String, Object>)Json.deserializeUntyped((String)getServiceResp.get('getData'));
            responseService.put('dataPayments', MX_SB_VTS_DeleteCoverageHSD_Services.processResponse(JSON.serialize(responseServ.get('data')), dataQuote.Id));
            final List<String> paramsCov = new List<String>();
                MX_SB_VTS_DeleteCoverageHSD_Services.updateCoverage(nwLstCob, true, paramsCov);
        }
        responseService.put('isOk', true);
        responseService.put('dataCot', MX_SB_VTS_DeleteCoverageHSD_ctrl.fillCotizData(oppId));
        return responseService;
    }
}