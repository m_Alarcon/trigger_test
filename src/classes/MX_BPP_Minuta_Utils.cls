/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Minuta_Utils
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2021-01-12
* @Description 	BPyP Minuta Utils
* @Changes
* 2021-02-22	Héctor Saldaña	  	New methods added: formatValue, generateURLs, insertMinuta, insertContentDocLink
* 2021-03-08    Edmundo Zacarias    New methods added: cleanValue, hasChanged
* 2021-03-26    Héctor Saldaña      Methods updated: insertMinuta, insertContentDocLink. Now can handle bulkified DML operations
*/
public with sharing class MX_BPP_Minuta_Utils {
    
    /*Contructor clase MX_BPP_Minuta_Service */
    @TestVisible
    private MX_BPP_Minuta_Utils() {}
    
    /**
    * @description
    * @author Edmundo Zacarias Gómez
    * @param Datetime fechaDT
    * @return String (14 de Enero del 2021)
    **/
    public static String fechaToString(Datetime fechaDT) {
        String fechaString = '';
        if(fechaDT != null) {
            final string[] fechRes = string.valueofGmt(fechaDT).split(' ')[0].split('-');
            fechaString = fechRes[2] + ' de ' + getMes(fechRes[1]) + ' del ' + fechRes[0];
        }
        
        return fechaString;
    }
    
    /**
    * @description
    * @author Edmundo Zacarias Gómez
    * @param String mesIn
    * @return String 
    **/
    public static string getMes(String mesIn) {
        String mesNombre = '';
        final Map<String, String> monthValues = new Map<String, String>();
        monthValues.put('01', 'Enero');
        monthValues.put('02', 'Febrero');
        monthValues.put('03', 'Marzo');
        monthValues.put('04', 'Abril');
        monthValues.put('05', 'Mayo');
        monthValues.put('06', 'Junio');
        monthValues.put('07', 'Julio');
        monthValues.put('08', 'Agosto');
        monthValues.put('09', 'Septiembre');
        monthValues.put('10', 'Octubre');
        monthValues.put('11', 'Noviembre');
        monthValues.put('12', 'Diciembre');
        if(monthValues.containskey(mesIn)) {
            mesNombre = monthValues.get(mesIn);
        }
        return mesNombre;
    }

    /**
    * @Description 	Validate the parameter to get and escapehtml4
    * @Params		parameterToGet String
    * @Return 		String
    **/
    public static String getParameter(String parameterToGet) {
        String param = '';
        param = String.isBlank(ApexPages.currentPage().getParameters().get(parameterToGet)) ? param : ApexPages.currentPage().getParameters().get(parameterToGet);
        return param.escapeHtml4();
    }

    /**
    * @Description Formats the received input in order to be displayed in bullets if needed
    * @Param String contenido
    * @Return String
    **/
    public static String formatValue(String contenido) {
        String result = '';
        for(String item : contenido.split('\\n')) {
            item = item.contains('*') ? '<li>' + item.substringAfter('*') + '</li>' : item + '<br>';
            result += item;
        }
        if (result.contains('<li>')) {
            result = '<ul>' + result + '</ul>';
        }
        return result;
    }

    /**
    * @Description 	Generates the resources URL based for each of the resources name recieved
    * @Param		resourcesNames List<String>
    * @Return 		Map<String, String>
    **/
    public static Map<String, String> generateURLs(List<String> resourcesNames) {
        final List<Document> lstDocument = MX_RTL_Document_Selector.getDocuments('Id, DeveloperName', 'DeveloperName IN: listString', true , resourcesNames);
        final Map<String, String> resources = new Map<String, String>();

        if(!lstDocument.isEmpty()) {
            String fullURL;
            String urlFileString, urlComplement;
            urlFileString = '/servlet/servlet.ImageServer?id=';
            urlComplement = '&oid=';

            for (Document document : lstDocument) {
                fullURL = URL.getSalesforceBaseUrl().toExternalForm() + urlFileString + document.Id + urlComplement + UserInfo.getOrganizationId();
                resources.put(document.DeveloperName, fullURL);
            }
        }

        return resources;
    }

    /**
	* @Description 	Return the pdf to get attached
	* @Param		Blob docBlob, String visitObjNewName
	* @Return 		ContentVersion
	**/
    public static ContentVersion insertMinuta(Blob docBlob, dwp_kitv__Visit__c visitObjNew) {

        final ContentVersion pdfMinuta = new ContentVersion();
        pdfMinuta.ContentLocation = 'S';
        pdfMinuta.VersionData = docBlob;
        pdfMinuta.Title = 'Minuta ' + visitObjNew.Name;
        pdfMinuta.PathOnClient = 'Minuta ' + visitObjNew.Name + '.pdf';
        pdfMinuta.IsAssetEnabled = true;

        insert pdfMinuta;
        final List<ContentVersion> lstContVersion = new List<ContentVersion>{pdfMinuta};
        insertContentDocLink(lstContVersion, visitObjNew.Id, false);

        return pdfMinuta;
    }

    /**
	* @Description 	Insert content document link.
	* @Param		Id pdfMinutaId, Id visitObjNewId
	* @Return 		NA
	**/
    public static void insertContentDocLink(List<ContentVersion> lstcontVersion, Id visitObjNewId, Boolean multipledocs) {

        if(multipledocs) {
            final List<ContentDocumentLink> lstDocLink = new List<ContentDocumentLink>();
            for (ContentVersion cont : lstcontVersion) {
                final ContentDocumentLink cdl = new ContentDocumentLink();
                cdl.ContentDocumentId = cont.ContentDocumentId;
                cdl.LinkedEntityId = visitObjNewId;
                cdl.ShareType = 'V';
                lstDocLink.add(cdl);
            }

            insert lstDocLink;

        } else {
            final Set<Id> idsPdfs = new Set<Id>();
            idsPdfs.add(lstcontVersion[0].Id);
            final ContentDocumentLink cdlSingle = new ContentDocumentLink();
            cdlSingle.ContentDocumentId = MX_RTL_ContentVersion_Selector.getContentVersionByIds('Id, ContentDocumentId', idsPdfs)[0].ContentDocumentId;
            cdlSingle.LinkedEntityId = visitObjNewId;
            cdlSingle.ShareType = 'V';
            insert cdlSingle;
        }
    }

    /**
	* @Description 	Validates Catalogo record content if has any null values within its fields
	* @Param		String value
	* @Return 		String
	**/
    public static String validateNullValues(String value) {
        return value == null ? '' : value;
    }
    
    /**
	* @Description 	Cleans the value to prevent NBSP and /r/n
	* @Param		String value
	* @Return 		String
	**/
    public static String cleanValue(String value) {
        String newValue;
        if(String.isNotEmpty(value)) {
            newValue = value.replace('&nbsp;'.unescapeHtml4(), ' ');
            newValue = newValue.replace('\r\n', '\n');
            newValue = newValue.replace('\r', '\n');
        }
        return newValue;
    }
    
    /**
	* @Description 	Check if value has changed with null handler
	* @Param		String beforVal String afterValue
	* @Return 		String
	**/
    public static Boolean hasChanged(String beforeValue, String afterValue) {
        Boolean changed;
        if (String.isNotBlank(afterValue)) {
            changed = afterValue.equals(beforeValue) ? false : true;
        } else if (String.isNotBlank(beforeValue)) {
            changed = beforeValue.equals(afterValue) ? false : true;
        } else {
            changed = false;
        }
        return changed;
    }
}