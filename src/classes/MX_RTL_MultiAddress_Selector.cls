/**
 * @description       : Clase Selector de apoyo para Objeto MX_RTL_MultiAddress__c
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 02-25-2021
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   11-12-2020   Eduardo Hernández Cuamatzi   Initial Version
 * 1.1   14-01-2021   Diego Olvera Hernandez       Se agrega función de actualización
 * 1.2   08-02-2021   Alexandro Corzo              Se realizan ajustes a clase para Codesmells
 * 1.3   17-02-2021   Diego Olvera Hernandez       Se agrega función de inserción/actualización
 * 1.4   24-02-2021   Diego Olvera Hernandez       Se agrega función de upsert
**/
@SuppressWarnings('sf:UseSingleton, sf:DMLWithoutSharingEnabled')
public class MX_RTL_MultiAddress_Selector {
    /** Variable de Apoyo: STR_SELECT */
    final static String STR_SELECT = 'SELECT ';

    /**
    * @description Recupera los campos de registros de direcciones
    * @author Eduardo Hernández Cuamatzi | 11-12-2020 
    * @param parenstId Id del registros asignado
    * @param addressType Tipo de dirección a consuktar
    * @return List<MX_RTL_MultiAddress__c> Domicilios obtenidos
    **/
    public static List<MX_RTL_MultiAddress__c> findAddress(Set<String> parenstId, Set<String> addressType) {
        String fieldsAddresses = 'SELECT Id, Name, MX_RTL_MasterAccount__c, MX_RTL_AddressCityCode__c, MX_RTL_AddressCity__c,';
        fieldsAddresses +='MX_RTL_AddressCountryCode__c, MX_RTL_AddressCountry__c, MX_RTL_Tipo_Propiedad__c, MX_RTL_AddressExtNumber__c, MX_RTL_AddressIntNumber__c, MX_RTL_AddressMunicipalityCode__c,';
        fieldsAddresses +=' MX_RTL_AddressMunicipality__c, MX_RTL_AddressReference__c, MX_SB_VTS_Metros_Cuadrados__c, MX_RTL_Uso_Habitacional__c, MX_RTL_AddressStateCode__c, MX_RTL_AddressState__c, MX_RTL_AddressStreet__c, ';
        fieldsAddresses +='MX_RTL_AddressType__c, MX_SB_VTS_Colonias__c, MX_RTL_BeneficiaryAddress__c, MX_RTL_Contract__c, MX_RTL_Opportunity__c, MX_RTL_PostalCode__c, MX_RTL_QuoteAddress__c,';
        fieldsAddresses +='MX_RTL_SendPolice__c FROM MX_RTL_MultiAddress__c where (MX_RTL_Opportunity__c IN: parenstId OR ';
        fieldsAddresses +='MX_RTL_MasterAccount__c IN : parenstId OR MX_RTL_Contract__c IN : parenstId OR MX_RTL_QuoteAddress__c IN : parenstId) AND MX_RTL_AddressType__c IN : addressType';
        return DataBase.query(String.escapeSingleQuotes(fieldsAddresses));
    }
    /**
    * @description: Insert a MX_RTL_MultiAddress__c list 
    * @author Diego Olvera | 02/12/2021
    * @param List<MX_RTL_MultiAddress__c> iAddressLst
    **/
    public static void insertAddress (List<MX_RTL_MultiAddress__c> iAddressLst) {
        insert iAddressLst;
    }

     /**
    * @description: Update a MX_RTL_MultiAddress__c list 
    * @author Diego Olvera | 12/28/2020
    * @param List<MX_RTL_MultiAddress__c> uAddressLst
    **/
    public static void upAddress (List<MX_RTL_MultiAddress__c> uAddressLst) {
        update uAddressLst;
    }

    /**
    * @description: Upsert a MX_RTL_MultiAddress__c list 
    * @author Diego Olvera | 12/28/2020
    * @param List<MX_RTL_MultiAddress__c> addressLst
    **/
    public static void upsertAddress (List<MX_RTL_MultiAddress__c> addressLst) {
        upsert addressLst;
    }

    /**
    * @description Devuelve valores de MultiAddress dependiendo de campos y condiciones
    * @author Alexandro Corzo | 28/01/2021
    * @param String qryfield
    * @param String conditionField
    * @param String withCondition
    * @return List<MX_RTL_MultiAddress__c> Lista de Resultados
    **/
    public static List<MX_RTL_MultiAddress__c> rsttQryMulAdd(String qryfield, String conditionField, boolean withCondition) {
        String sQueryM;
        switch on String.valueOf(withCondition) {
            when 'false' {
                sQueryM = STR_SELECT + qryfield + ' FROM MX_RTL_MultiAddress__c';
            }
            when 'true' {
                sQueryM = STR_SELECT + qryfield + ' FROM MX_RTL_MultiAddress__c WHERE ' + conditionField;
            }
        }
        return Database.query(String.escapeSingleQuotes(sQueryM).unescapeJava());
    }

    /**
    * @Method getAddress
    * @param String addId
    * @param Sring addFields
    * @Description method que retorna un objeto MX_RTL_MultiAddress__c
    * @return Objeto User
    **/
    public static MX_RTL_MultiAddress__c getAddress(String addId, String addFields) {
        final String addQuery = 'SELECT '+addFields+' FROM MX_RTL_MultiAddress__c WHERE MX_RTL_Opportunity__c=:addId AND Name = \'Direccion Propiedad\'';
        return Database.query(String.escapeSingleQuotes(addQuery).unescapeJava());
    }   
}