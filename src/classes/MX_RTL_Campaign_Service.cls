/**
* @File Name          : public class MX_RTL_Campaign_Service.cls
* @Author             : Gerardo Mendoza Aguilar
* @Group              :
* @Last Modified By   : Gerardo Mendoza Aguilar
* @Last Modified On   : 11-19-2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      11/11/2020           Gerardo Mendoza Aguilar          Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public class MX_RTL_Campaign_Service {
    /*Property FIEDLS*/
    Final static String QUERYFIELDS = 'Id, EndDate';

    /**
    * @description Return select Campaign
    * @author Gerardo Mendoza | 11/11/2020
    * @param String ids
    * @return List<Campaign>
    **/  
    public static List<Campaign> selectCampaignById(String ids) {
        Final String queryFilters = 'WHERE Id = '+'\''+ ids +'\'';
        return MX_RTL_Campaign_Selector.selectCampaign(QUERYFIELDS, queryFilters);
    }
}