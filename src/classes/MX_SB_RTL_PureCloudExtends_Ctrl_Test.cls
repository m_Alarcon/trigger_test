/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 10-07-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   09-23-2020   Eduardo Hernández Cuamatzi   Initial Version
**/
@isTest
public class MX_SB_RTL_PureCloudExtends_Ctrl_Test {
    /**@description etapa Cotizacion*/
    private final static string COTIZACION = 'Cotización';
    
    @testSetup static void setup() {
        MX_SB_VTS_CallCTIs_utility.initHogarGeneric();
    }

    @isTest 
    static void responseIvrCobroTstOne() {
        Test.startTest();
        final Opportunity oppId = [Select Id, Name, StageName from Opportunity where StageName =: COTIZACION];
        final Quote quoteId = [Select Id, Name, OpportunityId from Quote where OpportunityId =: oppId.Id];
        final String strJsonIVR = createJson((String)quoteId.Id, '3003', '');
        final string statusStr = MX_SB_RTL_PureCloudExtends_Ctrl.onSaveLog(strJsonIVR);
        System.assertEquals(quoteId.Id, statusStr,'SUCCESS');
        Test.stopTest();
    }

    /**Crea JSON respuesta IVR */
    public static String createJson(String recordId, String codeIVR, String msjAsesor) {
        String jsonIvr = '{"callLog":{"whatid":"0063B000009RU3BQAW","MX_WB_telefonoUltimoContactoCTI__c":"2465554247","Resultado_llamada__c":""';
        jsonIvr +=',"MX_RTE_Sala__c":"1234R51F2","MX_EU_ComentariosEC__c":"'+codeIVR+'","MX_WB_idGrabacion__c":"'+recordId+'","MX_SB_VTS_Certificada__c":""},"eventName":"interactionChanged"}';
        return jsonIvr;
    }
}