/**
* @FileName          : MX_SB_VTS_wrpColoniasResp
* @description       : Wrapper class for use de web service of listPlaces
* @Author            : Marco Antonio Cruz Barboza  
* @last modified on  : 26-12-2020
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   26-12-2020   Marco Antonio Cruz Barboza          Initial Version
**/
@SuppressWarnings('sf:LongVariable, sf:ShortVariable, sf:ShortClassName')
public class MX_SB_VTS_wrpListPlaces_Utils {
	/** Wrapper Call*/
    public data[] data {get;set;}
    
    /**
    * @description : Object data to call a web service in an array
    * @Param 
    * @Return
    **/
    public class data {
        /**
        * @description : Geolocation Id
        **/
        public String id {get;set;}
        /**
        * @description : geolocation name
        **/
        public String name {get;set;}
        /**
        * @description : Object geolocation with latitude and longitude
        **/
        public geolocation geolocation {get;set;}
        /**
        * @description : List geographicPlaceTypes with Municipio, State and Id
        **/
        public geographicPlaceTypes[] geographicPlaceTypes {get;set;}
        /**
        * @description : Object State with id and Name
        **/
        public state state {set;get;}
        /**
        * @description : Object country with id and Name
        **/
        public country country {set;get;}
    }
    
    /**
    * @description : Object geolocation to call a web service in an array
    * @Param 
    * @Return
    **/
    public class geolocation {
        /**
        * @description : latitude zone
        **/
        public String latitude {get;set;}
        /**
        * @description : longitude zone
        **/
        public String longitude {get;set;}
    }
    
    /**
    * @description : Object geographicPlaceTypes to call a web service in an array
    * @Param 
    * @Return
    **/
    public class geographicPlaceTypes {
        /**
        * @description : id zone
        **/
        public String id {get;set;}
       	/**
        * @description : name zone
        **/
        public String name {get;set;}
        /**
        * @description : value zone
        **/
        public String value {get;set;}
    }
    
   	/**
    * @description : Object state to call a web service in an array
    * @Param 
    * @Return
    **/
    public class state {
        /**
        * @description : id state zone
        **/
        public String id {get;set;}
        /**
        * @description : name state zone
        **/
        public String name {get;set;}
    }
    
    /**
    * @description : Object country to call a web service in an array
    * @Param 
    * @Return
    **/
    public class country {
        /**
        * @description : id country zone
        **/
        public String id {get;set;}
        /**
        * @description : name country zone
        **/
        public String name {get;set;}
    }
    
}