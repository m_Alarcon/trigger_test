/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase test de dwp_kitv_Visit_Topic - selector

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     25/05/2020      Roberto Isaac Soto Granados           Creación Clase
*****************************************************************************************/
@isTest
private class Dwp_kitv_Visit_Topic_Selector_test {
    /*Usuario de pruebas*/
    private static User testUser = new User();
    /*cuenta de pruebas*/
    private static Account testAcc = new Account();
    /*Tema a tratar de pruebas*/
    private static dwp_kitv__Visit_Topic__c testTopic = new dwp_kitv__Visit_Topic__c();
    /*Visita de pruebas*/
    private static dwp_kitv__Visit__c testVisit = new dwp_kitv__Visit__c();
    /*String para folios*/
    private static String testCode = '1234';

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testUser = UtilitysDataTest_tst.crearUsuario('testUser', 'BPyP Estandar', 'BPYP BANQUERO BANCA PERISUR');
        testUser.Title = 'Privado';
        insert testUser;

        System.runAs(testUser) {
            testAcc.LastName = 'testAcc';
            testAcc.FirstName = 'testAcc';
            testAcc.OwnerId = testUser.Id;
            testAcc.No_de_cliente__c = testCode;
            insert testAcc;
            testVisit.dwp_kitv__visit_start_date__c = Date.today()+4;
            testVisit.dwp_kitv__visit_duration_number__c = '15';
            testVisit.dwp_kitv__visit_status_type__c = '04';
            insert testVisit;
        }
    }

    /*Ejecuta la acción para cubrir clase*/
    static testMethod void creaTemasTest() {
        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        testVisit = [SELECT Id, dwp_kitv__visit_status_type__c, OwnerId FROM dwp_kitv__Visit__c WHERE OwnerId =: testUser.Id];
        System.runAs(testUser) {
            Test.startTest();
            	testTopic.dwp_kitv__visit_id__c = testVisit.Id;
            	testTopic.dwp_kitv__topic_desc__c = 'Ok';
            	Dwp_kitv_Visit_Topic_Selector.creaTemas(new List<dwp_kitv__Visit_Topic__c>{testTopic});
            Test.stopTest();
        }
        System.assert(!String.isBlank(testTopic.dwp_kitv__topic_desc__c), 'Tema creado');
    }

}