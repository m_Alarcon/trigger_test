/**
* @File Name          : MX_BPP_LeadCampaignTable_Helper_Test.cls
* @Description        : Test class for MX_BPP_LeadCampaignTable_Helper
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 01/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      01/06/2020            Gabriel Garcia Rojas          Initial Version
**/
@isTest
private class MX_BPP_LeadCampaignTable_Service_Test {
	/*Usuario de pruebas*/
    private static User usuarioPrueba = new User();

    /** namerole variables for records */
    final static String NAMEROLESER = '%BPYP BANQUERO BANCA PERISUR%';
    /** name variables for records */
    final static String NAMEUSERP = 'usuarioPrueba';
    /** nameprofiles variable for records */
    final static String NAMEPROFSER = 'BPyP Estandar';
    /** namesucursales variable for records */
    final static String NAMESUCSERV = '6385 BANCA PERISUR';
    /** namedivisiones variable for records */
    final static String NAMEDIVISER = 'METROPOLITANA';

    /** error message */
    final static String MESSAGEFAIL = 'Fail method';

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        usuarioPrueba = UtilitysDataTest_tst.crearUsuario(NAMEUSERP, NAMEPROFSER, NAMEROLESER);
        usuarioPrueba.BPyP_ls_NombreSucursal__c = NAMESUCSERV;
        usuarioPrueba.Divisi_n__c = NAMEDIVISER;
        insert usuarioPrueba;

        final Double numClienteRondom = Math.random()*10000000;

        final Product2 productoService = new Product2(Name = 'Cuenta en Dólares', Banca__c = 'Red BPyP');
        insert productoService;

        final Account cuentaService = new Account();
		cuentaService.LastName = 'Cuenta test';
        cuentaService.PersonEmail = 'test@test.com';
		cuentaService.No_de_cliente__c = String.valueOf(numClienteRondom.round());
		cuentaService.RecordTypeId = RecordTypeMemory_cls.getRecType('Account', 'MX_BPP_PersonAcc_Client');
		insert cuentaService;

        final Lead candidatoService = new Lead(LastName = 'Candidato', LeadSource = 'Preaprobado', MX_WB_RCuenta__c = cuentaService.Id);
        insert candidatoService;

        final Campaign campService = new Campaign(Name = 'Campaña', MX_WB_Producto__c = productoService.Id);
        insert campService;

        final CampaignMember campMemService = new CampaignMember(CampaignId = campService.Id, Status='Sent', LeadId = candidatoService.Id, MX_LeadEndDate__c=Date.today().addDays(15));
        insert campMemService;
    }

    /**
     * @description constructor test
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void testConstructor() {
        final MX_BPP_LeadCampaignTable_Service instanceC = new MX_BPP_LeadCampaignTable_Service();
        System.assertNotEquals(instanceC, null, MESSAGEFAIL);
    }

     /**
     * @description Test to method getListUser
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void getListUserTest() {

        final User testUser2 = UtilitysDataTest_tst.crearUsuario(NAMEUSERP+' Director', 'BPyP Director Oficina', 'BPYP DIRECTOR OFICINA BANCA PERISUR');
        testUser2.BPyP_ls_NombreSucursal__c = NAMESUCSERV;
        testUser2.Divisi_n__c = NAMEDIVISER;
        insert testUser2;

        List<User> listUsers;
        System.runAs(testUser2) {
            listUsers = MX_BPP_LeadCampaignTable_Service.listUser();
        }
        System.assertNotEquals(listUsers.size(), 0, MESSAGEFAIL);
    }

     /**
     * @description Test to method getInfoMembersByFilter
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void getInfoMembersByFilterTest() {
        final CampaignMember campMem = [SELECT Id, Lead.Name, Lead.MX_WB_RCuenta__r.Name, CampaignId FROM CampaignMember LIMIT 1];
        final List<String> params = new List<String>{campMem.Lead.MX_WB_RCuenta__r.Name, '', 'Preaprobado', '', '', '', '', '', '', '', '' };
        Test.startTest();
        final List<CampaignMember> listCamp = MX_BPP_LeadCampaignTable_Service.getInfoMembersByFilter(params, new List<User>());
        Test.stopTest();
        System.assertEquals(listCamp.size(), 0, MESSAGEFAIL);
    }

     /**
     * @description Test to method getFamilyDataBySchema
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void getFamilyDataBySchemaTest() {
        Test.startTest();
        final List<String> listFamilies = MX_BPP_LeadCampaignTable_Service.familyDataBySchema();
        Test.stopTest();
        System.assertNotEquals(listFamilies.size(), 0, MESSAGEFAIL);
    }

    /**
     * @description Test to method getStatusDataBySchema
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void getStatusDataBySchemaTest() {
        Test.startTest();
        final List<String> listStatus = MX_BPP_LeadCampaignTable_Service.statusDataBySchema();
        Test.stopTest();
        System.assertNotEquals(listStatus.size(), 0, MESSAGEFAIL);
    }

    /**
     * @description Test to method getOppPickListProductValue
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void getOppPickListProductValueTest() {
        Test.startTest();
        final Map<Object,List<String>> mapSchema = MX_BPP_LeadCampaignTable_Service.getOppPickListProductValue('Opportunity.MX_RTL_Producto__c');
        Test.stopTest();
        System.assertNotEquals(mapSchema.size(), 0, MESSAGEFAIL);
    }

	/**
     * @description Test to method getOppFromLeadConv
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void getOppFromLeadConvTest() {
        final Campaign testCamp = new Campaign(Name = 'Campaign Service Test');
        insert testCamp;
        final Account testCuenta = new Account(Name='Test Service Account',No_de_cliente__c = '8316251');
        insert testCuenta;
        final Lead testCandidato = new Lead(LastName='Lead Service Test', RecordTypeId=Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('MX_BPP_Leads').getRecordTypeId(),
                MX_ParticipantLoad_id__c=testCuenta.No_de_cliente__c, MX_LeadEndDate__c=Date.today().addDays(13), MX_WB_RCuenta__c= testCuenta.Id, MX_LeadAmount__c=250, LeadSource='Preaprobados');
        insert testCandidato;
        final CampaignMember testmCampaignTest = new CampaignMember(CampaignId = testCamp.Id, Status='Sent', LeadId = testCandidato.Id, MX_LeadEndDate__c=Date.today().addDays(13));
        insert testmCampaignTest;

        Test.startTest();
        final Opportunity oppCreated = MX_BPP_LeadCampaignTable_Service.getOppFromLeadConv(testCandidato.Id);
        Test.stopTest();
        System.assertNotEquals(oppCreated, null, MESSAGEFAIL);
    }

    /**
     * @description Test to method fetchListSucursales
     * @author Gabriel Garcia | 01/07/2020
     * @return void
     **/
    @isTest
    private static void fetchListSucursalesTest() {
        final List<String> listSucursales = MX_BPP_LeadCampaignTable_Service.fetchListSucursales(NAMEDIVISER);
        MX_BPP_LeadCampaignTable_Service.fetchListDivisiones();
        MX_BPP_LeadCampaignTable_Service.fetchCampaignMember(new List<String>{'', '', ''});
        MX_BPP_LeadCampaignTable_Service.fetchCampaignMember(new List<String>{NAMEDIVISER, '', ''});
        MX_BPP_LeadCampaignTable_Service.fetchCampaignMember(new List<String>{NAMEDIVISER, NAMESUCSERV, ''});
        System.assertNotEquals(listSucursales, null, MESSAGEFAIL);
    }
}