/**
*
*Clase MX_SB_COB_CreaObjetos_cls para cobranza
*
 */
@isTest
public class MX_SB_COB_CreaObjetos_cls {//NOSONAR	
    
    /** Crea el archivo correspondiente  */
    final private static String CONSTEST='archivoPrueba';
    /** Crea el archivo correspondiente  */
    public static String creaArchivo(Integer numRegistros) {
         
      String strBody = 'SISTEMA,PRODUCTO,CODIGO PRODUCTO,POLIZA,FACTURA,VENCIMIENTO DEL RECIBO,FORMA DE PAGO,ANTIGÜEDAD,IMPORTE PESOS,IMPORTE DOLARES,NOMBRE,APELLIDO PATERNO,RESPUESTA DE COBRANZA,CAUSA DE RECHAZO DE COBRANZA,CLASIFICACION,TELEFONO 1,TELEFONO 2,TELEFONO 3,TELEFONO 4,TELEFONO 5,TELEFONO 6,TELEFONO 7,TELEFONO 8,TELEFONO 9,TELEFONO 10,CORREO1,CORREO2,CORREO3,PUNTOS,SEMAFORO,BLOQUE\n';

        for(Integer i=0;i<numRegistros;i++) {

             strBody += 'CLIPERT,WIBE,,8A6W1100U6'+i+',72456963'+i+',30/06/2017,SEMESTRAL,33,300,10,JOSE,PEREZ'+i+',62,TARJETA RESTRINGIDA,HD,,,5543470582,,,,5543470582,,,,prueba.prueba'+i+'@prueba.com,prueba.prueba@prueba.com,prueba.prueba@prueba.com,,,PRUEBA01\n';

        }
        strBody += 'CLIPERT,WIBE,,8A6W1100U6,72456963,30/06/2017,SEMESTRAL,33,300,0,JOSE,PEREZ,62,TARJETA RESTRINGIDA,HD,,,5543470582,,,,5543470582,,,,prueba.prueba@prueba.com,prueba.prueba@prueba.com,prueba.prueba@prueba.com,,,PRUEBA01';



        final Blob bStrBody = Blob.valueOf(strBody);

        final List<ContentVersion> cVersionList = new List<ContentVersion>();
        final ContentVersion objContVer = new ContentVersion();
        objContVer.VersionData  = bStrBody;
        objContVer.Title = CONSTEST;
        objContVer.PathOnClient =  CONSTEST;
        objContVer.ContentLocation = 'S';
        cVersionList.add(objContVer);

        final Database.SaveResult[] fileInsert = Database.insert(cVersionList,true);
        String idguardado='';
        for (Database.SaveResult objDtUrsNvo : fileInsert) {
                if (objDtUrsNvo.isSuccess()) {
                    	idguardado = objDtUrsNvo.getId();
                }
		}

        return [SELECT id,ContentSize,Title,FileExtension,ContentDocumentId FROM ContentVersion WHERE id = :idguardado limit 1].id;
    }
    /**Crea Contrato */
    public static void creaContrato(id idCuenta,String polizatxt) {
        final Account cuenta = [select PersonEmail,id,lastname,phone from Account where id = :idCuenta limit 1];
        final String tipoContr = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Contrato Seguros').getRecordTypeId();        

        final Contract poliza = new Contract(RecordTypeId = tipoContr,
                            					StartDate=date.today(),
                                       			AccountId=cuenta.id,
                            					MX_WB_noPoliza__c=polizatxt,
                            					MX_WB_emailAsegurado__c=cuenta.PersonEmail,
                                               MX_WB_telefonoAsegurado__c= String.valueOf(cuenta.phone),
                            					MX_WB_nombreAsegurado__c=null,
                            					MX_WB_apellidoPaternoAsegurado__c= cuenta.LastName
                                       );
        insert poliza;


    }
    /**Crea un archivo con faltas */
    public static String creaArchivoConFaltas() {
         //creacion de archivo
      String strBody = 'SISTEMA,PRODUCTO,CODIGO PRODUCTO,POLIZA,FACTURA,VENCIMIENTO DEL RECIBO,FORMA DE PAGO,ANTIGÜEDAD,IMPORTE PESOS,IMPORTE DOLARES,NOMBRE,APELLIDO PATERNO,RESPUESTA DE COBRANZA,CAUSA DE RECHAZO DE COBRANZA,CLASIFICACION,TELEFONO 1,TELEFONO 2,TELEFONO 3,TELEFONO 4,TELEFONO 5,TELEFONO 6,TELEFONO 7,TELEFONO 8,TELEFONO 9,TELEFONO 10,CORREO1,CORREO2,CORREO3,PUNTOS,SEMAFORO,BLOQUE\n';
    strBody += 'CLIPERT,WIBE,,471238,,30/06/2017,SEMESTRAL,33,300,0,JOSE,PEREZ,62,,HD,,,5543470582,,,,5543470582,,,,prueba.prueba@prueba.com,prueba.prueba@prueba.com,prueba.prueba@prueba.com,,,PRUEBA01';
      	 


        final Blob bStrBody = Blob.valueOf(strBody);

        final List<ContentVersion> contentVerL = new List<ContentVersion>();
        final ContentVersion objContVer = new ContentVersion();
        objContVer.VersionData  = bStrBody;
        objContVer.Title = CONSTEST;
        objContVer.PathOnClient =  CONSTEST;
        objContVer.ContentLocation = 'S';
        contentVerL.add(objContVer);

        final Database.SaveResult[] fileInsert = Database.insert(contentVerL,true);
        String idguardado='';
        for (Database.SaveResult objDtUrsNvo : fileInsert) {
                if (objDtUrsNvo.isSuccess()) {
                    	idguardado = objDtUrsNvo.getId();
                }
		}
        return [SELECT id,ContentSize,Title,FileExtension,ContentDocumentId FROM ContentVersion WHERE id = :idguardado limit 1].id;
    }
    /**Crea un bloque  */
     public static String creaBloque() {
        final Bloque__c bloque = new Bloque__c(MX_SB_COB_BloqueActivo__c = True);
        
        final List<bloque__c> bloques= new List<bloque__c>();
        bloques.add(bloque);
        final Database.SaveResult[] ramoIns = Database.insert(bloques,true);
        String idBloque = '';
        for (Database.SaveResult objDtUrsNvo : ramoIns) {
                if (objDtUrsNvo.isSuccess()) {
                    	idBloque = objDtUrsNvo.getId();
                }
		}
        return idBloque;
    }
}