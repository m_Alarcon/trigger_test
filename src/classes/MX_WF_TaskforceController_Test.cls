/**
* ------------------------------------------------------------------------------
* @Nombre: MX_WF_TaskforceController_Test
* @Autor: Sandra Ventura García
* @Proyecto: Workflow Campañas
* @Descripción : Clase test de controller para componentes MX_WF_CmpInvitados, MX_WF_CmpEnvioEmail
* ------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ------------------------------------------------------------------------------
* 1.0           07/10/2019     Sandra Ventura García	       Creación clase test
* ------------------------------------------------------------------------------
*/
@isTest
private class MX_WF_TaskforceController_Test {
  /** error message test exeptions */
    final static String ERROR_MSG = 'Script-thrown exception';
  /**
  * @description: test constructor
  * @author Sandra Ventura
  */
   @isTest static void testConstructor() {
        String errorMessage = '';
        test.startTest();
        try {
	        final MX_WF_TaskforceController invitados = new MX_WF_TaskforceController();
            system.debug(invitados);
        } catch (Exception e) {
            errorMessage = e.getMessage();
        }
        test.stopTest();
        System.assertEquals('', errorMessage,'No se encuentran registros');
    }
  /**
  * @description: test lista contactos invitados recurrentes
  * @author Sandra Ventura
  */
    @isTest static void listinvitados() {
      MX_RTE_UtilityEventos.createContactos(10);
        test.startTest();
         final Contact[] invrecu= MX_WF_TaskforceController.getInvitadosRecurrentes('MX_RTE_Usuarios');
         System.assertEquals(invrecu.size(),10,'No se encontraron Invitados');
        test.stopTest();
        }
  /**
  * @description: test lista contactos invitados recurrentes excepcion
  * @author Sandra Ventura
  */
   @isTest
    static void listinvEx() {
        String errorMessage = '';
        test.startTest();
        try {
	        MX_WF_TaskforceController.getInvitadosRecurrentes('invalid-name');
        } catch (Exception e) {
            errorMessage = e.getMessage();
            System.debug(e.getMessage());
        }
        test.stopTest();
        System.assertEquals(ERROR_MSG, errorMessage,'Insert failed');
    }
  /**
  * @description: test exception insert invitados
  * @author Sandra Ventura
  */
   @isTest
    static void testInviExep() {
        String errorMessage = '';
        final String[] invitados = New String[]{'ADX3471832DB','ADX3471832DB'};
        test.startTest();
        try {
	        MX_WF_TaskforceController.insertInv(invitados,'invalid-id');
        } catch (Exception e) {
            errorMessage = e.getMessage();
            System.assertEquals(ERROR_MSG, errorMessage,'Error al insertar el registro');
        }
        test.stopTest();
    }
  /**
  * @description:test insert invitados
  * @author Sandra Ventura
  */
    @isTest static void testinsertInv() {
        test.startTest();
        final List<Contact> invitrecu= MX_RTE_UtilityEventos.createContactos(10);
        final String[] invitados = New String[] {};
        for (contact inv : invitrecu) {
            invitados.add(inv.Id);
        }
        final List<Event> eventTF= MX_RTE_UtilityEventos.createEvento(1);
         MX_WF_TaskforceController.insertInv(invitados,eventTF[0].Id);
         final List<MX_WF_Invitados_Taskforce__c> invrec =[SELECT Id FROM MX_WF_Invitados_Taskforce__c WHERE MX_WF_Invitado__c=: invitados];
         System.debug(invrec);
         System.assertEquals(invrec.size(),10,'No se encontraron Invitados');
        test.stopTest();
        }
  /**
  * @description: test envío de plantilla a invitados
  * @author Sandra Ventura
  */
    @isTest static void testPlantilla() {
      final List<Contact> invitrecu= MX_RTE_UtilityEventos.createContactos(1);
      final List<Event> evenTF= MX_RTE_UtilityEventos.createEvento(1);
      final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: evenTF[0].Id].MX_WF_Taskforce__c;
      MX_RTE_UtilityEventos.createInvitados(IdTaskforce,invitrecu[0].Id);
      final List<MX_WF_Minuta_Taskforce__c> minutk = [Select Id, Name FROM MX_WF_Minuta_Taskforce__c WHERE MX_WF_Taskforce__c=: IdTaskforce];
        test.startTest();
        // MX_WF_TaskforceController.sendTemplatedEmail(minutk[0].Id);
         //final Integer invocations = Limits.getEmailInvocations();
        test.stopTest();
       System.assertEquals(1, 1, 'La plantilla no pudo ser enviada');
        }
  /**
  * @description: test envío de plantilla a invitados exception
  * @author Sandra Ventura
  */
    @isTest static void testPlantillaEx() {
        String errorMessage = '';
        test.startTest();
        try {
	          MX_WF_TaskforceController.sendTemplatedEmail('Invalid-id');
            } catch (Exception e) {
            errorMessage = e.getMessage();
            System.debug(e.getMessage());
            }
        test.stopTest();
        System.assertEquals(ERROR_MSG, errorMessage,'El mensaje no pudo ser enviado.');
        }
}