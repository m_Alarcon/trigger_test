/**
* ---------------------------------------------------------------------------------
* @Autor: Rodrigo Amador Martinez Pacheco
* @Proyecto: Auditoria
* @Descripcion : Pruebas unitarias para clase MX_RTL_Maximun_File_Size_selector
* ---------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ---------------------------------------------------------------------------------
* 1.0           18/06/2020     Rodrigo Martinez               Creación de la clase
* ---------------------------------------------------------------------------------
*/
@istest
public class MX_RTL_Maximun_File_Size_selector_test {
  
  /**
	* @Autor: Rodrigo Amador Martinez Pacheco
	* @Descripcion : Prueba de consulta del valor maximo permitido de tamanio para archivos
	*/
    @istest
    public  static void selectMaximumFileSizeAllowedTest() {
    	final Decimal maximum = MX_RTL_Maximun_File_Size_selector.selectMaximumFileSizeAllowed();
        System.assert(maximum > 0,'No esta configurado el tamaño maximo en el custom metadata');
    }
}