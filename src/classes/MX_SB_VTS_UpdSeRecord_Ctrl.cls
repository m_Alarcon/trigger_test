/**
 * @description       : 
 * @author            : Diego Olvera
 * @group             : 
 * @last modified on  : 09-25-2020
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   09-25-2020   Diego Olvera   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_UpdSeRecord_Ctrl {
    /**
    * @description Actualiza valor de Lead u Oportunidad desde el front con valor de Propio o rentado
    * dependiendo de lo que el usuario seleccione.
    * @author Diego Olvera | 17-09-2020 
    * @param Id leadId
    * @return void
    **/
    @AuraEnabled
    public static void updCurrRec(String leadId) {
        MX_SB_VTS_UpdSeRecord_Service.updSeCurrentRec(leadId);
    }
}