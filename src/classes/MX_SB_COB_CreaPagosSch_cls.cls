/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_COB_CreaPagosSch_cls
* Autor Angel Nava
* Proyecto: Cobranza - BBVA Bancomer
* Descripción : Clase de proceso schedulable para creacion de pagos
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Desripción
* --------------------------------------------------------------------------------
* 1.0           01/05/2019     Angel Nava                         Creación
* --------------------------------------------------------------------------------
*/
global with sharing class MX_SB_COB_CreaPagosSch_cls implements Schedulable {
    /** String sQuery*/
    global String sQuery { get;set; }
    /** String sIdBloque*/
    global String sIdBloque { get;set; }
    /** List pagosEspejoIds*/
    global List<String> pagosEspejoIds { get;set; }
    /** SchedulableContext ctx*/
    global void execute(SchedulableContext ctx) {

        Integer iCntReg = 200;

        final List<String> idsCod = new List<String>();
        String ids = '';
        Integer cont = 0;
        for(String idIteracion:this.pagosEspejoIds) {
            idsCod.add('\''+idIteracion+'\'');
            if(cont==0) {
                 ids+='\''+idIteracion+'\'';
            } else {
                 ids+=',\''+idIteracion+'\'';
            }
            cont++;
        }

        this.sQuery = 'Select c.Id ';
        this.sQuery += 'From PagosEspejo__c c Where MX_SB_COB_InsertCreaPago__c = false And MX_SB_COB_BloqueLupa__c = \'' + sIdBloque + '\' ';
        this.sQuery += 'And Id in ('+ids+')';
        if (Test.isRunningTest()==false) {
            this.sQuery += ' Order by MX_SB_COB_Factura__c limit 49990';
        } else {
            this.sQuery += 'Limit 1 ';
            iCntReg = 1;
        }

        final List<Bloque__c> bloque = [select id,name from bloque__c where id=:sIdBloque];
        if(bloque.size()>0) {
            final List<CargaArchivo__c> carga = [select id, MX_SB_COB_ContadorConError__c,MX_SB_COB_ContadorSinError__c,MX_SB_COB_RegistrosTotales__c from CargaArchivo__c where MX_SB_COB_Bloque__c=:bloque[0].name];

            if(carga.size()>0) {
                enviaCorreo(carga);
            }
        }
        final MX_SB_COB_CreaPagosBch_cls oppACls = new MX_SB_COB_CreaPagosBch_cls(sQuery);
        final Id batchInstanceId = Database.executeBatch(oppACls, iCntReg);
         if(batchInstanceId==null) {
            iCntReg=200;
        }
        system.abortJob(ctx.getTriggerId());
    }
    /** enviacorreo carga*/
    private void enviaCorreo(List<CargaArchivo__c> carga) {
        final Decimal cuentaTot = carga[0].MX_SB_COB_ContadorConError__c +carga[0].MX_SB_COB_ContadorSinError__c;
            if(carga[0].MX_SB_COB_RegistrosTotales__c==cuentaTot) {
                final List<CronTrigger> trig = [SELECT Id, CronJobDetail.Name, State, TimesTriggered,StartTime FROM CronTrigger where TimesTriggered=0 and cronjobdetail.name like '%EnviaCorreoCobranza%' limit 1]; 
                DateTime hoy = datetime.now();
                hoy = hoy.addMinutes(1);
                if(trig.size()>0) {
                    final MX_SB_COB_EnviaCorreoCobranzaSch_cls objCorreo = new MX_SB_COB_EnviaCorreoCobranzaSch_cls();
                    objCorreo.idCargaArchivo = carga[0].id;
                    final DateTime dtFechaHoraAct = DateTime.now();
                    final Date dFechaActual = dtFechaHoraAct.date();
                    final Time tmHoraActual = dtFechaHoraAct.Time();
                    final Time tmHoraActualEnv = tmHoraActual.addMinutes(190);
                    String sch = '';
                    sch = '0 ' + tmHoraActualEnv.minute() + ' ' + tmHoraActualEnv.hour() + ' ' + dFechaActual.day() + ' ' + dFechaActual.month() + ' ?';
                    final String sNombreProc = tmHoraActualEnv.minute() + ' : ' + tmHoraActualEnv.hour() + ' : ' + dFechaActual.day() + ' : EnviaCorreoCobranza';
                    System.schedule(sNombreProc, sch, objCorreo);
                }
            }
    }

}