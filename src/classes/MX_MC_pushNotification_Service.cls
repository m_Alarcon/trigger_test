/**
 * @File Name          : MX_MC_pushNotification_Service.cls
 * @Description        : Clase para llamar al servicio MC_pushNotification
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 02/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      02/06/2020           Jair Ignacio Gonzalez G.   Initial Version
**/
public without sharing class MX_MC_pushNotification_Service {
    /**
     * @Description Status Code 200
    **/
    private static final Integer OKCODE = 200;
    /**
     * @Method MX_MC_pushNotification_Service
     * @Description Singletons
    **/
    @TestVisible private MX_MC_pushNotification_Service() {
    }
    /**
     * @Method callPushNotification
     * @Description Llama el servicio ASO MC_pushNotification
     * @param String cltNumber
     * @return String
     **/
    public static void callPushNotification (String customerId) {
        final Map<String, Object> parameters = new Map<String, Object>();
        parameters.put('customerId', customerId);
        final Datetime myDT = Datetime.now();
        final String sendDate = myDT.format('dd \'de\' MMMMM YYYY HH:mm \'h\'');
        parameters.put('sendDate', sendDate);
        final HttpResponse resp =iaso.GBL_Integration_GenericService.invoke('MC_pushNotification', parameters);
        if (resp.getStatusCode() != OKCODE) {
            throw new CalloutException(resp.getBody());
        }
    }
}