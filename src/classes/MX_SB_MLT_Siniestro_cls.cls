/*
* @Nombre: MX_SB_MLT_Siniestro_cls
* @Autor: Juan Carlos Benitez Herrera
* @Proyecto: Siniestros - BBVA
* @Descripción : Clase que almacena los methods usados en la Toma de Reporte de Siniestro
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   	Descripción
* --------------------------------------------------------------------------------
* 1.0         11/10/2019     Juan Carlos Benitez Herrera	Creación
* 1.1         20/11/2019     Daniel Perez Lopez				Se refactoriza la clase
* 1.2         26/02/2020     Angel Nava                     se agrega update de siniestro
* 1.3         28/02/2020     Angel Nava                     Se agrega parametro de busqueda en sin
* 1.4         02/03/2010     Juan Carlos Benitez Herrera    Se agrega crear servicio ajustador auto y moto
* 1.5         11/03/2010     Juan Carlos Benitez Herrera    Se agrega clase upsert siniestro
* 1.6         25/03/2020     Marco Antonio Cruz Barboza     Se agrega clase checkPolicy de la clase MX_SB_MLT_SiniestroVida_cls
* 1.7         25/03/2020     Marco Antonio Cruz Barboza     Se modifica crear servicio ajustador Auto y moto
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_Siniestro_cls {//NOSONAR
     /**
	*str sustituye string literal
	**/
    public static final String CLAIMID ='A837128937';
    /*
    * @var Lista workOrder nuevo servicio ajustador Auto
    */
    public static List<WorkOrder> newServices {get;set;}
    /*
    * @var Lista workOrder nuevo servicio ajustador Moto
    */
    public static List<WorkOrder> newServicesM {get;set;}
       /**
	*str sustituye string literal est
	**/
    public static final String PAIS = 'México';
	/*
    * @description Crea el siniestro en ramo Auto para la visualización del mapa dentro de la toma de reporte.
    * @param nameSin
    * @return String siniobj
    */     
    @AuraEnabled
    public static String createSin() {
       final Siniestro__c siniobj = new Siniestro__c();
       siniobj.MX_SB_SAC_Contrato__c=[Select Id from Contract limit 1].id;
       siniobj.MX_SB_MLT_Origen__c= 'Phone';
       siniobj.MX_SB_MLT_JourneySin__c = 'Validación de Poliza';
       siniobj.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_RamoAuto').getRecordTypeId();
	   insert siniobj;
       return siniobj.Id;
    }
	/*
    * @description Obtiene el tipo de registro Ramo Auto,
    * @param 
    * @return Id RecType
    */
    @auraEnabled
	public static Id getRecordType(String devname) {
        return [Select Id From RecordType  Where SobjectType = 'Siniestro__c' and DeveloperName =:devname].Id;
    }
    
    /*
    * @description Obtiene los valores del tipo de Robo
    * @param  siniId
    * @return String
    */
    @auraEnabled
    public static String getvaluePickRobo(String siniId) {
        return [Select MX_SB_MLT_TipoRobo__c from Siniestro__c where id =:siniId].MX_SB_MLT_TipoRobo__c;
    }
    
	/*
    * @description Obtiene los valores del tipo de Robo
    * @param  recId
    * @return String 
    */
    @auraEnabled
    public static String getValue(String recId) {
        return [select MX_SB_MLT_TipoRobo__c from Siniestro__c where Id =: recId].MX_SB_MLT_TipoRobo__c;
    }
    /*
    * @description Obtiene la lista de servicios enviados del Siniestro
    * @param  sinid
    * @return lista de Case
    */
    @auraEnabled
	public static List<WorkOrder> getServiciosEnviados (String sinid) {
        List<WorkOrder> sinList = new List<WorkOrder>();
        if (sinid != null) {
            final Siniestro__c sin= [select id from Siniestro__c where id=:sinid];
            final String acId= sin.id;
            sinList = [SELECT Id, 
                       MX_SB_MLT_Proveedor_Ambulancia__c, 
                       MX_SB_MLT_ProveedorParamedico__c, 
                       MX_SB_MLT_ServicioAsignado__c, 
                       WorkOrderNumber, 
                       MX_SB_MLT_TipoServicioAsignado__c, 
                       MX_SB_MLT_EstatusServicio__c,
                       Account.Name,
                       MX_SB_MLT_NameAjustadorCC__c,
                       MX_SB_MLT_Ajustador__r.Name,
                       MX_SB_MLT_ProveedorRobo__c FROM WorkOrder WHERE MX_SB_MLT_Siniestro__c =:acId];
        }
        return sinList;
    }
    /*
    * @description Obtiene los datos del Siniestro, se añaden solo los campos utilizados.
    * @param  idsini
    * @return Obejeto Siniestro__c
    */
    @AuraEnabled
    public static Siniestro__c getDatosSiniestro(String idsini) {
       try {
           final Siniestro__c[] listares = [SELECT id,MX_SB_MLT_Telefono__c,MX_SB_MLT_Mobilephone__c,MX_SB_MLT_Proveedor_Paramedico__c,Proveedor__c,
                                MX_SB_MLT_NombreConductor__c,MX_SB_MLT_APaternoConductor__c,MX_SB_MLT_ProveedorAmbulancia__c,MX_SB_SAC_Contrato__c, MX_SB_MLT_JourneySin__c,
                                MX_SB_MLT_Acta_averiguacion_carpeta__c,MX_SB_MLT_TipoRobo__c,MX_SB_MLT_Parte_Robada__c,MX_SB_MLT_CristalDa_ado__c,MX_SB_MLT_AutoAjusteAccepted__c,
                                MX_SB_MLT_Solictud_Cristalera__c,MX_SB_MLT_Fecha_Hora_Siniestro__c,MX_SB_MLT_Address__c,TipoSiniestro__c,MX_SB_MLT_LugarAtencionAuto__c,
								MX_SB_MLT_Q_ThirdPresent__c,MX_SB_MLT_PropiedadAjena__c,MX_SB_MLT_Q_AuthorityPresent__c,MX_SB_MLT_AMaternoConductor__c,
                                MX_SB_MLT_PhoneType__c,MX_SB_MLT_PhoneTypeAdditional__c,MX_SB_MLT_InsuranceCompany__c,MX_SB_MLT_Proveedor__c,MX_SB_MLT_LegacySystem__c
                                FROM Siniestro__c where id=:idsini];
           
           return listares[0];
       } catch(Exception dny) {
               throw new AuraHandledException (System.Label.MX_WB_LG_ErrorBack +' No se encontraron registros con id '+ JSON.serialize(dny));
       } 
    } 

    /*
    * @description Crea un registro de participante de Siniestro al crearse la toma de reporte
    * @param  SinId
    * @return Boolean rsltBusqueda
    */
    @AuraEnabled
    public static boolean createPartSin (String sinId) {
        boolean rsltBusqueda = false;
        final MX_SB_MLT_Siniestro_Cls_Controller claseaux = new MX_SB_MLT_Siniestro_Cls_Controller();
        rsltBusqueda = claseaux.CreatePartSin(SinId);
        return rsltBusqueda;
    }
    /*
    * @description Crea los servicios de Grua, Paramedico, Ambulacia que se enviaran en el siniestro de tipo Colision
    * @param  idsini
    * @return String 
    */
    @AuraEnabled
    public static String createServices(String idSiniestro, String idProveedor,String idParamedico,String idAmbulancia) {
        String rsltCreServs ='';
        rsltCreServs = MX_SB_MLT_Siniestro_Cls_Controller.createServices(idSiniestro,idProveedor,idParamedico,idAmbulancia);
        return rsltCreServs; 
    }
    /*
    * @description Obtiene los valores del tipo de Robo
    * @param  recId
    * @return Lista de String
	*/
    @AuraEnabled
    public static List<String> getPickList() {
    	final List<String> pickListValues= new List<String>();
            final Schema.DescribeFieldResult fieldResult = Siniestro__c.MX_SB_MLT_TipoRobo__c.getDescribe();
            final List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for(Schema.PicklistEntry pickListVal : ple) {
                pickListValues.add(pickListVal.getLabel());
		}
		return pickListValues;
    }
    /*
    * @description actualiza/inserta el sin
    * @param  sinIn
    * @return object Siniestro__c
	*/
    @AuraEnabled
    public static Siniestro__c upSertsini(Siniestro__c sinIn) {
		upsert sinIn;
        return sinIn;
    }
    /*
    * @description actualiza el sin
    * @param  recId
    * @return Lista de String
	*/
    @AuraEnabled
    public static void upSin(siniestro__c sinIn) {
        MX_SB_MLT_DetallesServiciosEnviados_cls.updateSini(sinIn);
    }
    /*
    * @description Crea laa WorkOrder de Ajustador en Moto y Auto
    * @param  idSini
    * @return void
	*/
    @AuraEnabled
   	public static void enviarWSAjustador(String idSini) {
        newServices = new List<WorkOrder>();
        newServicesM = new List<WorkOrder>();
        Final siniestro__c sin = [select id,MX_SB_MLT_URLLocation__c,MX_SB_MLT_CodigoPostal__c,MX_SB_MLT_Calle__c,MX_SB_MLT_NumeroCalle__c, MX_SB_MLT_AjustadorAuto__c,MX_SB_MLT_AjustadorMoto__c, MX_SB_MLT_JourneySin__c, TipoSiniestro__c,MX_SB_MLT_Address__c from siniestro__c where
                                  id=:idSini and TipoSiniestro__c='Colisión' and MX_SB_MLT_Address__c NOT IN ('') and
                                  MX_SB_MLT_URLLocation__c NOT IN ('') and MX_SB_MLT_LugarAtencionAuto__c ='Crucero' limit 1 ];
        sin.MX_SB_MLT_AjustadorAuto__c=true;
        sin.MX_SB_MLT_AjustadorMoto__c=true;
    	Final List<WorkOrder> ajustadorAuto = [select id from WorkOrder where MX_SB_MLT_Siniestro__c= :idSini and MX_SB_MLT_TipoServicioAsignado__c='Ajustador-Auto' and MX_SB_MLT_EstatusServicio__c NOT in('Cancelado') limit 1];
        Final List<WorkOrder> ajustadorMoto = [select id from WorkOrder where MX_SB_MLT_Siniestro__c= :idSini and MX_SB_MLT_TipoServicioAsignado__c='Ajustador-Moto' and MX_SB_MLT_EstatusServicio__c NOT in('Cancelado') limit 1];
         if(ajustadorAuto.isEmpty()) {
             Final String latln= sin.MX_SB_MLT_URLLocation__c;
        	 Final List<String> lstlatln = latln.split(',');
             final double lat =double.valueOf(lstlatln[0]);
             final double lon =double.valueOf(lstlatln[0]);
             final WorkOrder newService = new WorkOrder(RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_Servicios').getRecordTypeId());
                        newService.MX_SB_MLT_ServicioAsignado__c= 'Ajustador';
             			newService.MX_SB_MLT_IdAjustadorCC__c =CLAIMID;
             			newService.MX_SB_MLT_NameAjustadorCC__c='Paolo Mendoza';
						newService.Country=PAIS;
             			newService.Latitude=lat;
             			newService.Longitude=lon;
             			newService.postalCode=sin.MX_SB_MLT_CodigoPostal__c;
                        newService.MX_SB_MLT_Siniestro__c=sin.Id;
              			newService.Street = sin.MX_SB_MLT_Calle__c+', '+sin.MX_SB_MLT_NumeroCalle__c;
                        newService.MX_SB_MLT_TipoServicioAsignado__c= 'Ajustador-Auto';
         newServices.add(newService);
         insert newServices;
		}
        if(ajustadorMoto.isEmpty()) {
            Final String latln= sin.MX_SB_MLT_URLLocation__c;
            Final List<String> lstlatln = latln.split(',');
            Final double lat =double.valueOf(lstlatln[0]);
            Final double lon =double.valueOf(lstlatln[0]);
			Final WorkOrder newServiceM = new WorkOrder(RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_Servicios').getRecordTypeId());
						newServiceM.MX_SB_MLT_ServicioAsignado__c= 'Ajustador';
            			newServiceM.MX_SB_MLT_IdAjustadorCC__c =CLAIMID;
            			newServiceM.MX_SB_MLT_NameAjustadorCC__c='Paolo Mendoza';
                        newServiceM.MX_SB_MLT_Siniestro__c=sin.Id;
                        newServiceM.Latitude=lat;
             			newServiceM.Longitude=lon;
            			newServiceM.postalCode=sin.MX_SB_MLT_CodigoPostal__c;
            			newServiceM.Street = sin.MX_SB_MLT_Calle__c+', '+sin.MX_SB_MLT_NumeroCalle__c;
                        newServiceM.MX_SB_MLT_TipoServicioAsignado__c= 'Ajustador-Moto';
						newServiceM.Country=PAIS;
         newServicesM.add(newServiceM);
         insert newServicesM;
        }
    }
    /*
    * @description Realiza la busqueda de Póliza en contratos
    * @param  policy
    * @return String ID del Contrato donde se localizo dicha póliza
	*/
    @AuraEnabled
    public static String checkPolicy(String policy) {
        return MX_SB_MLT_SiniestroVida_Controller.searchPolicy(policy);
    }
    /*
    * @description Realiza la creación de una tarea para agenda
    * @param  taskObject
	*/
    @AuraEnabled
    public static void createTask(Task taskObject) {
        MX_SB_MLT_SiniestroVida_Controller.taskInsertion(taskObject);
    }
    
}