/**
 * @File Name          : MX_SB_VTS_Vida_Service.cls
 * @Description        :
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              :
 * @Last Modified By   : Eduardo Hernandez Cuamatzi
 * @Last Modified On   : 3/6/2020 16:47:47
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    20/5/2019 10:42:28   Eduardo Hernández Cuamatzi     Initial Version
 * 1..1   29/5/2019            Jaime Terrats                Add account validation
 * 1.2    19/06/2019           Jaime Terrats                Add validation for opp closed win
 * 1.2.1  24/02/2020   		Eduardo Hernández Cuamatzi     Fix registros duplicados
 * 1.2.2  27/03/2020   		Eduardo Hernández Cuamatzi     Owner por proveedor
**/
@RestResource(urlMapping='/Vida/*')
global without sharing class MX_SB_VTS_Vida_Service extends MX_SB_VTS_WrapperVida { // NOSONAR    
    /*
    * Variable para status de cotizacion
    */
    private static final String EMITIDA='Emitida';
    /*Implants*/
    final static List<User> LSTIMPLANT = [Select Id, Name, MX_SB_VTS_ProveedorCTI__c from User where MX_SB_VTS_ProveedorCTI__c NOT IN ('') AND MX_SB_VTS_ImplantConnected__c = TRUE AND IsActive = TRUE];
    /**Propietario de la Opp y de la cotización */
    private static String ownerIdV;
    /**
     * cotizarSeguroVida description
     * @param  datosCotizacion datosCotizacion description
     * @return                 return description
     */
    @HttpPost
    global static List<MX_SB_VTS_ResponseSFDC> cotizarSeguroVida(MX_SB_VTS_DatosCotizacion datosCotizacion) {
        final List<MX_SB_VTS_ResponseSFDC> resSFDC = new List<MX_SB_VTS_ResponseSFDC>();
        MX_SB_VTS_ResponseSFDC res = new MX_SB_VTS_ResponseSFDC();
        res.error = '';
        res.message = '';
        res.recordId = '';
        final String emailClient = datosCotizacion.datosCliente.Email.toLowerCase();
        final String phone = datosCotizacion.datosCliente.telefonoCasa.toLowerCase();
        final String movil = datosCotizacion.datosCliente.celular.toLowerCase();
        final Boolean isInBlackList = MX_SB_VTS_utilityQuote.validateEmails(emailClient, phone, movil);
        ownerIdV = MX_SB_VTS_utilityQuote.findOwnerProvider('Smart Center', LSTIMPLANT);
        if(isInBlackList == false) {
            Account findAcco = MX_SB_VTS_utilityQuote.findUniqueAccount(emailClient);
            if(String.isNotBlank(findAcco.Id)) {
                findAcco = upsertAccount(datosCotizacion, true);
                upsertOpportunity(findAcco, datosCotizacion);
                res.message += ' Cuenta localizada: ' + findAcco.id;
                resSFDC.add(res);
            } else {
                findAcco = upsertAccount(datosCotizacion, false);
                upsertOpportunity(findAcco, datosCotizacion);
                res.message = 'Generando nuevo folio para el cliente';
                res.recordId += 'id de la cuenta: ' + findAcco.Id;
                resSFDC.add(res);
            }
        } else {
            res.message = 'Correo o número telefonico en lista negra';
            res.recordId += 'El registro no se creo por pertenecer a lista negra';
            resSFDC.add(res);
        }
        return resSFDC;
    }

     /**
     * upsertAccount Upsert a la cuenta
     * @param	datosCotizacion Datos e la cotización
     * @return	accsToUpsert	Regresa una lista
     */
    private static Account upsertAccount(MX_SB_VTS_DatosCotizacion datosCotizacion, Boolean exist) {
        final String emailClient = datosCotizacion.datosCliente.Email.toLowerCase();
        Account findAcco = MX_SB_VTS_utilityQuote.findUniqueAccount(emailClient);
        if(exist == false) {
            final List<String> listVals = new List<String>{datosCotizacion.datosCliente.nombre, datosCotizacion.datosCliente.email, datosCotizacion.datosCliente.apPaterno, datosCotizacion.datosCliente.apMaterno};
            findAcco = MX_SB_VTS_utilityQuote.fillAccount(findAcco, listVals);
        }
        findAcco = accountInit(findAcco, datosCotizacion);
        switch on datosCotizacion.datosIniciales.origen.toLowerCase() {
            when  'inbound'{
                findAcco.AccountSource = 'Inbound';
            }
            when 'outbound' {
                findAcco.AccountSource = 'Outbound';
            }
        }
        try {
            Database.upsert(findAcco);
        } catch(DmlException dmlEx) {
            throw new DmlException('Err ' + dmlEx);
        }
        return findAcco;
    }

    /**
     * accountInit Init de la cuenta
     * @param	retorig Datos de la cuenta
     * @param	datosCotizacion	Datos de la cotización
     * @return	ret	Regresa datos de la cuenta
     */
    private static Account accountInit(Account retorig, MX_SB_VTS_DatosCotizacion datosCotizacion) {
        final Account ret = retorig;
        ret.PersonBirthdate = date.parse(datosCotizacion.datosCliente.fechaNacimiento);
        ret.PersonHomePhone = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.telefonoCasa);
        ret.PersonMobilePhone = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.celular);
        ret.MX_Gender__pc = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.sexo);
        ret.RFC__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.rfc);
        ret.BillingStreet = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomicilio.calleCliente);
        ret.BillingCity = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomicilio.ciudadCliente);
        ret.BillingPostalCode = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomicilio.cpCliente);
        ret.BillingState = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomicilio.estadoCliente);
        ret.Apellido_materno__pc = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.apMaterno);
        ret.LastName = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.apPaterno);
        ret.BillingCountry = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomicilio.paisCliente);
        ret.Numero_Exterior__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomicilio.numExtCliente);
        ret.Numero_Interior__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomicilio.numIntCliente);
        ret.Colonia__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosDomicilio.coloniaCliente);
        return ret;
    }

    /*
    * Method to upsert opportunity
    * @param account and datosCotizacion
    */
    private static void upsertOpportunity(Account acc, MX_SB_VTS_DatosCotizacion datosCotizacion) {
        final List<Opportunity> lstOpps = MX_SB_VTS_utilityQuote.findOppQuote(acc.Id, datosCotizacion.datosIniciales.producto, datosCotizacion.datosIniciales.origen.toLowerCase());
        Opportunity oppsToUpsert = new Opportunity();
        final Product2 stcProductId = [select Id,MX_WB_FamiliaProductos__c from Product2 where Name =: datosCotizacion.datosIniciales.producto  And MX_SB_SAC_Proceso__c = 'VTS' AND IsActive = true];
        final Quote presupuesVida = MX_SB_VTS_utilityQuote.findUniqueQuote(datosCotizacion.datosIniciales.folioCotizacion, datosCotizacion.datosIniciales.folioTracking);
        if(String.isEmpty(presupuesVida.Id) && lstOpps.isEmpty()) {
            final Account findAccount = MX_SB_VTS_utilityQuote.findUniqueAccount(acc.Id);
            final Opportunity cotizacionVida = new Opportunity();
            cotizacionVida.Name = findAccount.Num_Opps__c + 1  + ' ' + acc.FirstName + ' ' + acc.LastName;
            cotizacionVida.CloseDate = System.today();
            cotizacionVida.AccountId = acc.Id;
            cotizacionVida.FolioCotizacion__c = datosCotizacion.datosIniciales.folioCotizacion;
            cotizacionVida.Reason__c = 'Venta';
	    cotizacionVida.TelefonoCliente__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.celular);
            cotizacionVida.Producto__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosIniciales.producto);
            cotizacionVida.Pricebook2Id = MX_SB_VTS_utilityQuote.findPriceBook(stcProductId.Id);
            if(EMITIDA.equalsIgnoreCase(datosCotizacion.datosIniciales.statusCotizacion)) {
                cotizacionVida.StageName = 'Closed Won';
                cotizacionVida.MX_SB_VTS_Aplica_Cierre__c = true;
            } else {
                cotizacionVida.StageName = 'Cotización';
            }
            cotizacionVida.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
            switch on datosCotizacion.datosIniciales.origen.toLowerCase() {
                when 'inbound' {
                    cotizacionVida.LeadSource = 'Tracking web';
                    cotizacionVida.OwnerId = ownerIdV;
                }
                when 'outbound' {                    
                    cotizacionVida.LeadSource = 'Outbound TLMK';
                }
            }
            oppsToUpsert = cotizacionVida;
        } else {
            oppsToUpsert = MX_SB_VTS_utilityQuote.finalOppToWork(presupuesVida, lstOpps);
            if(EMITIDA.contains(datosCotizacion.datosIniciales.statusCotizacion) &&
            String.isNotBlank(datosCotizacion.datosIniciales.numeroPoliza)) {
                oppsToUpsert.MX_SB_VTS_Aplica_Cierre__c = true;
            }
            upsertQuote(oppsToUpsert, datosCotizacion,stcProductId);
        }
        if(String.isBlank(oppsToUpsert.Id)) {
            upsertOppExt(datosCotizacion, oppsToUpsert, stcProductId);
        }
    }

    /**
    * @description Actualiza opportunidad y genera Quote
    * @author Eduardo Hernandez Cuamatzi | 2/6/2020 
    * @param MX_SB_VTS_DatosCotizacion datosCotizacion 
    * @param Opportunity oppsToUpsert Oportunidad nueva
    * @param Product2 stcProductId producto de interes de la Oportunidad
    * @return void 
    **/
    private static void upsertOppExt(MX_SB_VTS_DatosCotizacion datosCotizacion, Opportunity oppsToUpsert, Product2 stcProductId) {
        try {
            final Quote presVidaTemp = MX_SB_VTS_utilityQuote.findUniqueQuote(datosCotizacion.datosIniciales.folioCotizacion, datosCotizacion.datosIniciales.folioTracking);
            if(String.isNotBlank(presVidaTemp.OpportunityId)) {
                oppsToUpsert.Id = presVidaTemp.OpportunityId;
                oppsToUpsert.LeadSource = presVidaTemp.Opportunity.LeadSource;
            }
            final String cupon = String.isBlank(datosCotizacion.datosPrecio.cupon) ? '' : datosCotizacion.datosPrecio.cupon;
            oppsToUpsert.MX_WB_Cupon__c = cupon;
            oppsToUpsert.MX_WB_EnvioCTICupon__c = rtwCotizacion.validarCupon(cupon);
            Database.Upsert(oppsToUpsert);
            upsertQuote(oppsToUpsert, datosCotizacion,stcProductId);
        } catch(DmlException dmlEx) {
            throw new DmlException('DML:'+dmlEx);
        }
    }

    /**
     * upsertQuote upsert al Quote
     * @param	opp Id de la oportunidad
     * @param	datosCotizacion	Datos de la cotización
     */
    private static void upsertQuote(Opportunity opp, MX_SB_VTS_DatosCotizacion datosCotizacion, Product2 stcProductId) {
        Quote presupuestoVida = MX_SB_VTS_utilityQuote.findUniqueQuote(datosCotizacion.datosIniciales.folioCotizacion, datosCotizacion.datosIniciales.folioTracking);
        if(String.isNotBlank(presupuestoVida.Id)) {
            presupuestoVida.Name = datosCotizacion.datosIniciales.folioCotizacion + ' ' + opp.Name;
            presupuestoVida.MX_SB_VTS_Numero_de_Poliza__c =  MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosIniciales.numeroPoliza);
            presupuestoVida.MX_SB_VTS_Movil_txt__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.celular);
            presupuestoVida.MX_SB_VTS_Email_txt__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.email);
            presupuestoVida = MX_SB_VTS_Vida_Service.quoteValidation(presupuestoVida, datosCotizacion.datosIniciales.statusCotizacion, datosCotizacion.datosIniciales.folioCotizacion, opp);
        } else {
            final Id famProductId = stcProductId.MX_WB_FamiliaProductos__c;
            presupuestoVida.Name = datosCotizacion.datosIniciales.folioCotizacion + ' ' + opp.Name;
            presupuestoVida.MX_SB_VTS_ASO_FolioCot__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosIniciales.folioCotizacion);
            presupuestoVida.MX_SB_VTS_Folio_Cotizacion__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosIniciales.folioCotizacion);
            presupuestoVida.Name = MX_SB_VTS_rtwCotFillData.validEmptyStr(presupuestoVida.MX_SB_VTS_Folio_Cotizacion__c) + ' ' + opp.Name;
            presupuestoVida.OpportunityId = opp.Id;
            presupuestoVida.MX_SB_VTS_Familia_Productos__c = famProductId;
            presupuestoVida.MX_SB_VTS_Movil_txt__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.celular);
            presupuestoVida.MX_SB_VTS_Email_txt__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.email);
            presupuestoVida.MX_SB_VTS_Cupon__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosPrecio.cupon);
            presupuestoVida.MX_SB_VTS_Email_txt__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosCliente.email);
            presupuestoVida.Pricebook2Id = MX_SB_VTS_utilityQuote.findPriceBook(stcProductId.Id);
            presupuestoVida = MX_SB_VTS_Vida_Service.quoteValidation(presupuestoVida, datosCotizacion.datosIniciales.statusCotizacion, datosCotizacion.datosIniciales.folioCotizacion, opp);
        }
        if(String.isBlank(presupuestoVida.Id)) {
            presupuestoVida.OwnerId = [Select Id, OwnerId from Opportunity where Id =:opp.Id].OwnerId;
        }
        try {
            Database.upsert(presupuestoVida);
            upsertQuoteLineItem(presupuestoVida, datosCotizacion, stcProductId.Id);
            upsertBeneficiario(presupuestoVida, datosCotizacion);
        } catch(DmlException dmlEx) {
            throw new DmlException('Error! '+dmlEx);
        }
    }

    /**
     * QuoteValidation Validación de Quote
     * @param	Completar Presupuesto
     * @param	datosCotizacion	Datos de la cotización
     * @param	opp	Id de la oportunidad
     * @return	tempPresupuesto	Status del presupuesto
     */
    public static Quote quoteValidation(Quote completar, String statusCot, String folioCot, Opportunity opp) {
        final Quote tempPresupuesto = completar;
	if(String.isBlank(tempPresupuesto.OpportunityId)) {
		tempPresupuesto.OpportunityId = opp.Id;
	}
        switch on statusCot {
            when 'Creada' {
                    if(String.isEmpty(tempPresupuesto.MX_SB_VTS_Folio_Cotizacion__c)) {
                        tempPresupuesto.MX_SB_VTS_Folio_Cotizacion__c = folioCot;
                    }
                    tempPresupuesto.Status = statusCot;
                }
                when 'Cotizada' {
                    tempPresupuesto.Status = statusCot;
                }
                when 'Tarificada' {
                    tempPresupuesto.Status = statusCot;
                }
                when 'Cobrada' {
                    tempPresupuesto.Status = 'Formalizada';
                }
                when 'Emitida' {
                    if(String.isNotBlank(MX_SB_VTS_rtwCotFillData.validEmptyStr(tempPresupuesto.MX_SB_VTS_Numero_de_Poliza__c))) {
                        tempPresupuesto.Status = statusCot;
                        MX_SB_VTS_utilityQuote.prepareQuoteSync(tempPresupuesto);
                    }
                }
            }
        return tempPresupuesto;
    }

    /*
    * upsertBeneficiario
    * @param presupuesto Id presupuesto
    * @param datosCotizacion
    */
    private static void upsertBeneficiario(Quote presupuesto, MX_SB_VTS_DatosCotizacion datosCotizacion) {
        final List<MX_SB_VTS_Beneficiario__c> lstBenef =  [Select Name From MX_SB_VTS_Beneficiario__c
                                                    where MX_SB_VTS_Quote__c =: presupuesto.Id];
        final List<MX_SB_VTS_Beneficiario__c> benefToUpsert = new List<MX_SB_VTS_Beneficiario__c>();
        if(lstBenef.isEmpty() == false) {
            for(MX_SB_VTS_Beneficiario__c beneficiario : lstBenef) {
                beneficiario = getBeneficiarioInit(beneficiario, datosCotizacion);
                benefToUpsert.add(beneficiario);
            }
        } else {
            MX_SB_VTS_Beneficiario__c beneficiario = new MX_SB_VTS_Beneficiario__c();
            beneficiario = getBeneficiarioInit(beneficiario, datosCotizacion);
            beneficiario.MX_SB_VTS_Quote__c = presupuesto.Id;
            benefToUpsert.add(beneficiario);
        }

        if(benefToUpsert.isEmpty() == false) {
            try {
                Database.upsert(benefToUpsert);
            } catch(DmlException dmlEx) {
                throw new DmlException('Fallo ' + dmlEx);
            }
        }
    }

    /**
    *  getBeneficiarioInit upsert a beneficiario
    */
    private static MX_SB_VTS_Beneficiario__c getBeneficiarioInit(MX_SB_VTS_Beneficiario__c beneficiario, MX_SB_VTS_DatosCotizacion datosCotizacion) {
        for(MX_SB_VTS_Beneficiarios benef: datosCotizacion.beneficiarios) {
            beneficiario.Name = MX_SB_VTS_rtwCotFillData.validEmptyStr(benef.nombre);
            beneficiario.MX_SB_VTS_APaterno_Beneficiario__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(benef.apellidoPaterno);
            beneficiario.MX_SB_VTS_AMaterno_Beneficiario__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(benef.apellidoMaterno);
            beneficiario.MX_SB_VTS_Porcentaje__c = Decimal.valueOf(MX_SB_VTS_rtwCotFillData.validDecimals(MX_SB_VTS_rtwCotFillData.validEmptyStr(benef.procentaje)));
            beneficiario.MX_SB_VTS_Parentesco__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(benef.parentesco);
        }
        return beneficiario;
    }

    /**
     * upsertQuoteLineItem Upsert a QuoteLineItem
     * @param	presupuesto Id presupuesto
     * @param	datosCotizacion	Datos de la cotización
     */
    private static void upsertQuoteLineItem(Quote presupuesto, MX_SB_VTS_DatosCotizacion datosCotizacion, String stcProductId) {
        // Upsert Quote Line Item data
        final List<String> dataCot = new List<String>();
        dataCot.add(stcProductId);
        dataCot.add(datosCotizacion.datosPrecio.precioTotal);
        QuoteLineItem lstQuoli =  new QuoteLineItem();
        for(QuoteLineItem quoteLi : [Select Id, PriceBookEntryId, UnitPrice, Quantity, MX_WB_Descuento_con_cupones__c,
            MX_SB_VTS_Precio_Anual__c,MX_SB_VTS_Frecuencia_de_Pago__c,MX_SB_VTS_Monto_Mensualidad__c,MX_SB_VTS_Cancer_Tumores_Leucemia_Lupus__c,
            MX_SB_VTS_Aneurisma_Trombosis_Embolia__c,MX_SB_VTS_EmficemaBronquitisTuberculosis__c,MX_SB_VTS_Insuficiencias_Cirrosis_Hepati__c,MX_SB_VTS_Plan__c
            from QuoteLineItem where QuoteId =: presupuesto.Id AND MX_WB_Folio_Cotizacion__c =: presupuesto.MX_SB_VTS_ASO_FolioCot__c]) {
            lstQuoli = quoteLi;
        }
        if(String.isNotBlank(lstQuoli.Id)) {
            lstQuoli = getQuoteInit(lstQuoli, datosCotizacion);
        } else {
            QuoteLineItem quoli = new QuoteLineItem();
            quoli=getQuoteInit(quoli, datosCotizacion);
            quoli.QuoteId = presupuesto.Id;
            quoli.Quantity = 1;
            quoli.PriceBookEntryId = MX_SB_VTS_utilityQuote.getPricebookEntry(presupuesto, quoli, dataCot);
            quoli.MX_WB_Folio_Cotizacion__c = presupuesto.MX_SB_VTS_ASO_FolioCot__c;
            lstQuoli = quoli;
        }
        try {
            Database.upsert(lstQuoli);
        } catch(DmlException dmlEx) {
            throw new DmlException('Error: '+dmlEx);
        }
    }
     /**
     * getQuoteInit Obtine el QuoteInit
     * @param	datosCotizacion	Datos de la cotización
     * @return	quoli	Datos de cotización
     */
    private static QuoteLineItem getQuoteInit(QuoteLineItem quoli, MX_SB_VTS_DatosCotizacion datosCotizacion) {
        quoli.MX_SB_VTS_Precio_Anual__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.tipoDeSeguroVida.precioAnual);
        quoli.MX_SB_VTS_Frecuencia_de_Pago__c = getPaymentFreq(MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.tipoDeseguroVida.frequenciaPago), 
        MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.tipoDeSeguroVida.cantidadDePagos));
        quoli.MX_SB_VTS_Monto_Mensualidad__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosPrecio.precioParcialidades);
        quoli.UnitPrice = Decimal.valueOf(MX_SB_VTS_rtwCotFillData.validDecimals(MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosPrecio.precioTotal)));
        quoli.MX_SB_VTS_Cancer_Tumores_Leucemia_Lupus__c = Boolean.valueOf(datosCotizacion.datosAdicionalesVida.cancerTumoresLeucemiaLupus);
        quoli.MX_SB_VTS_Aneurisma_Trombosis_Embolia__c = Boolean.valueOf(datosCotizacion.datosAdicionalesVida.aneurismaTrombosisEmbolia);
        quoli.MX_SB_VTS_EmficemaBronquitisTuberculosis__c = Boolean.valueOf(datosCotizacion.datosAdicionalesVida.emficemaBronquitisTuberculosis);
        quoli.MX_SB_VTS_Insuficiencias_Cirrosis_Hepati__c = Boolean.valueOf(datosCotizacion.datosAdicionalesVida.insuficienciasCirrosisHepatitis);
        quoli.MX_SB_VTS_Total_Fallecimiento__c = Decimal.valueOf(MX_SB_VTS_rtwCotFillData.validDecimals(MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.tipoDeSeguroVida.totalFallecimiento)));
        quoli.MX_SB_VTS_Total_Gastos_Funerarios__c = Decimal.valueOf(MX_SB_VTS_rtwCotFillData.validDecimals(MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.datosIniciales.sumaAsegurada)));
        quoli.MX_SB_VTS_Total_Muerte_Accidental__c = Decimal.valueOf(MX_SB_VTS_rtwCotFillData.validDecimals(MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.tipoDeSeguroVida.totalMuerteAccidental)));
        quoli.MX_SB_VTS_Plan__c = MX_SB_VTS_rtwCotFillData.validEmptyStr(datosCotizacion.tipoDeSeguroVida.tipoPlan);
        return quoli;
    }

    /**
     * getPaymentFreq Obtine el pago
     * @param	datosCotizacion	Datos de la cotización
     * @return	selOption	Opción de tipo de pago
     */
    public static String getPaymentFreq(String frequenciaPago, String cantidadDePagos) {
        String selOption;
        if(String.isNotBlank(frequenciaPago)) {
            selOption = frequenciaPago;
        } else {
            switch on cantidadDePagos {
                when '1' {
                    selOption = 'Anual';
                }
                when '2' {
                    selOption = 'Semestral';
                }
                when '4' {
                    selOption = 'Trimestral';
                }
                when '12' {
                    selOption = 'Mensual';
                }
            }
        }
        return selOption;
    }
}