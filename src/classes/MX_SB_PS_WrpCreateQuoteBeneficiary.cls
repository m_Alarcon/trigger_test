/*
* BBVA - Mexico - Seguros
* @Author: Daniel Perez  Lopez
* MX_SB_PS_wrpCreateCustomeMX_SB_PS_WrpCreateQuoteBeneficiaryrData
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0		        Daniel perez                 08/09/2020                 Creación de la clase.
1.1             Juan Benitez                  25/01/2021                Se añaden subclasses */
@SuppressWarnings('sf:ExcessivePublicCount,sf:LongVariable,sf:, sf:ShortVariable,sf:VariableDeclarationHidesAnother')
/**maini class of wrapper */
public with sharing class MX_SB_PS_WrpCreateQuoteBeneficiary {
    /*Public property for wrapper*/
    public technicalInformation technicalInformation {get;set;}
    /*Public property for wrapper*/
    public String id {get ;set;}
    /*Public property for wrapper*/
    public insured[] insuredList {get;set;}
    /*Public subclass*/
    public class technicalInformation {
        /*Public property for wrapper*/
        public String dateConsumerInvocation {get;set;}
        /*Public property for wrapper*/
        public String idRequest {get;set;}
        /*Public property for wrapper*/
        public String technicalIdSession {get;set;}
        /*Public property for wrapper*/
        public String user {get;set;}
        /*Public property for wrapper*/
        public String managementUnit {get;set;}
        /*Public property for wrapper*/
        public String branchOffice {get;set;}
        /*Public property for wrapper*/
        public String technicalSubChannel {get;set;}
        /*Public property for wrapper*/
        public String technicalChannel {get;set;}
        /*Public property for wrapper*/
        public String dateRequest {get;set;}
        /*Public property for wrapper*/
        public String aapType {get;set;}
        /*public constructor subclass*/
		public technicalInformation() {
            this.technicalSubChannel ='71';
            this.user = 'CARLOS';
            this.aapType ='10000120';
            this.dateRequest ='2016-08-06 12:33:27.104';
            this.technicalChannel = '4';
            this.technicalIdSession = '3232-3232';
            this.branchOffice = '23';
            this.managementUnit = 'VVSH0001';
            this.idRequest = '1212-121212-12121-212';
            this.dateConsumerInvocation = '2016-08-06 12:33:27.1';
        }
    }
    /*Public subclass*/
    public class insured {
        /*Public property for wrapper*/
        public String id {get;set;}
        /*Public property for wrapper*/
        public String entryDate {get;set;}
        /*Public property for wrapper*/
        public beneficiary[] beneficiaries {get;set;}
    }
    /*Public subclass*/
    public class beneficiary {
        /*Public property for wrapper*/
        public person person {get;set;}
        /*Public property for wrapper*/
        public relationship relationship {get;set;}
        /*Public property for wrapper*/
        public String percentageParticipation {get;set;}
        /*Public property for wrapper*/
        public relationship beneficiaryType {get;set;}
        /*public constructor*/
        public beneficiary () {
            this.person = new person();
            this.relationship = new relationship();
            this.percentageParticipation = '';
            this.beneficiaryType= new relationship();
        }
    }
    /*Public subclass*/
    public class person {
        /*Public property for wrapper*/
        public String lastName {get; set;}
        /*Public property for wrapper*/
        public String secondLastName {get; set;}
        /*Public property for wrapper*/
        public String firstName {get; set;}
        /**public costructor */
        public person () {
            this.lastName='';
            this.secondLastName='';
            this.firstName='';
        }
    }
    /*Public subclass*/
    public class relationship {
        /*Public property for wrapper*/
        public String id {get;set;}
        /**public constructor for wraper */
        public relationship () {
            this.id='';
        }
    }
}