/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 09-23-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   09-23-2020   Eduardo Hernández Cuamatzi   Initial Version
**/
@isTest
public class MX_SB_RTL_PureCloudExtends_Service_Test {

    /**@description etapa Cotizacion*/
    private final static string COTIZACION = 'Cotización';
    
    @testSetup static void setup() {
        MX_SB_VTS_CallCTIs_utility.initHogarGeneric();
    }

    @isTest 
    static void responseIvrCobroTstOne() {
        final Opportunity oppRecId = [Select Id, Name from Opportunity where StageName =: COTIZACION];
        final Quote quoteData = [Select Id, Name from Quote where OpportunityId =: oppRecId.Id];
        final String strJsonIVR = MX_SB_RTL_PureCloudExtends_Ctrl_Test.createJson((String)quoteData.Id, '3003', '');
        final Map<String, Object> saveLogData = (Map<String, Object>) JSON.deserializeUntyped(strJsonIVR);
        Test.startTest();
            final string recId = MX_SB_RTL_PureCloudExtends_Service.retriveLogId(saveLogData);
        Test.stopTest();
        System.assertEquals(quoteData.Id, recId,'SUCCESS');
    }
}