/**
 * @File Name          : MX_RTL_Case_Service_Test.cls
 * @Description        :
 * @Author             : Jaime Terrats
 * @Group              :
 * @Last Modified By   : Jaime Terrats
 * @Last Modified On   : 5/26/2020, 11:30:46 AM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/26/2020   Jaime Terrats     Initial Version
**/
@isTest
private class MX_RTL_Case_Service_Test {

    /** test subject for case */
    final static String NAME = 'prueba_service';
    /** error message for asserts */
    final static String ERR_MSG = 'No hacen match, prueba fallida';
    /** current user id */
    final static String USR_ID = UserInfo.getUserId();
   /**
   * @description
   * @author Jaime Terrats | 5/25/2020
   * @return void
   **/
   @TestSetup
   static void makeData() {
        final User usr = MX_WB_TestData_cls.crearUsuario(NAME, System.Label.MX_SB_VTS_ProfileAdmin);
	    System.runAs(usr) {
            final String rtId = Schema.SObjectType.case.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_SAC_Generico).getRecordTypeId();

            final List<Case> casesToInsert = new List<Case>();
            for(Integer iter = 0; iter < 10; iter++) {
                final Case nCase = new Case();
                nCase.Subject = NAME + iter;
                nCase.RecordTypeId = rtId;
                nCase.MX_SB_SAC_Nombre__c = NAME;
                nCase.MX_SB_SAC_ApellidoPaternoCliente__c = NAME;
                nCase.MX_SB_SAC_ApellidoMaternoCliente__c = NAME;
                nCase.MX_SB_SAC_EsCliente__c = true;
                nCase.Origin = 'Teléfono';
                nCase.MX_SB_SAC_SubOrigenCaso__c = 'Inbound';
                nCase.MX_SB_SAC_TipoContacto__c = 'Asegurado';
                nCase.Reason = 'Consultas';
                casesToInsert.add(nCase);
            }
            Database.insert(casesToinsert);
        }
   }

   /**
   * @description
   * @author Jaime Terrats | 5/25/2020
   * @return void
   **/
   @isTest
   private static void testGetCaseDataWithSecurity() {
       final List<Case> caseList = [Select Id from Case where Subject like: '%' + NAME + '%'];
       final Set<Id> ids = new Set<Id>();
       for(Case tCase : caseList) {
           ids.add(tCase.Id);
       }
       final List<Case> testList = MX_RTL_Case_Service.getCaseData('Subject', ids, 'true');

       System.assertEquals(testList.size(), 10, ERR_MSG);
   }

   /**
   * @description
   * @author Jaime Terrats | 5/25/2020
   * @return void
   **/
   @isTest
   private static void testGetCaseDataWithoutSecurity() {
       final List<Case> listCase = [Select Id from Case where Subject =: NAME + '9'];
       final Set<Id> ids = new Set<Id>();
       for(Case tCase : listCase) {
            ids.add(tCase.Id);
       }

       final List<Case> testList = MX_RTL_Case_Service.getCaseData('Subject', ids, 'false');

       System.assertEquals(testList.size(), 1, ERR_MSG);
   }
}