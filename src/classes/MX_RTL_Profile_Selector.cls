/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RTL_Profile_Selector
* @Author   	Héctor Saldaña | hectorisrael.saldana.contractor@bbva.com
* @Date     	Created: 2021-22-02
* @Description 	Selector for MX_BPP_CatalogoMinutas__c Object
* @Changes
*
*/
public with sharing class MX_RTL_Profile_Selector {

    /** Constructor */
    @TestVisible
    private MX_RTL_Profile_Selector() {}

    /**
    * @description Retrieves the Profile Name based on the Id received
    * @author Héctor Saldaña
    * @param String queryfields, Set<Id> idProfiles
    * @return List<Profile>
    **/
    public static List<Profile> getProfileNameById(String queryfields, Set<String> idProfiles) {
        final String query ='SELECT '+ queryfields +' FROM Profile WHERE Id IN: idProfiles';
        return Database.query(String.escapeSingleQuotes(query));
    }
}