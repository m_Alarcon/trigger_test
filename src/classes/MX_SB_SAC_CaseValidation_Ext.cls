/**
 * @File Name          : MX_SB_SAC_CaseValidation_Ext.cls
 * @Description        :
 * @Author             : Jaime Terrats
 * @Group              :
 * @Last Modified By   : Jaime Terrats
 * @Last Modified On   : 08-11-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/2/2020   Jaime Terrats     Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_SAC_CaseValidation_Ext {
    /** string value to validate */
    final static String NOT_APPLICABLE = 'N/A';
    /** string value for picklist */
    final static String NOPE = 'No';
    /** string value for product type auto */
    final static String AUTO = 'Auto';
    /** string value for product type vida */
    final static String VIDA = 'Vida';
    /** array of options for case.reason */
    final static String[] REASON_VALUES = new String[]{'Cancelación', 'Retención'};
    /** string value to validate detail */
    final static String COTIZACION = 'Cotización';
    /** string for detail */
    final static String VENTA = 'Venta';
    /** string for detail */
    final static String[] DETAIL_VALUES = new String[]{'Duplicidad', 'Creditos', 'Error en condiciones de venta', 'Cross sell', 'Promoción/campaña', 'Disminución de suma asegurada'};
    /** string for detail */
    final static String[] DETAIL_1500 = new String[]{'Economía personal', 'Mal servicio', 'Duplicidad', 'Cobranza', 'Otra aseguradora', 'No especifica motivo',
                                                    'Meses sin intereses', 'Error en condiciones de venta', 'Venta condicionada'};
    /**string for other case reasons */
    final static String[] OTHER_REASONS = new String[]{'Consultas', 'Incidencia'};

    /**
    * @description
    * @author Jaime Terrats | 6/2/2020
    * @param nCase
    * @return void
    **/
    public static void preventCommentsOnReason(Case nCase) {
        if(REASON_VALUES.contains(nCase.Reason)
        && String.isNotBlank(nCase.Description)
        && nCase.MX_SB_SAC_FinalizaFlujo__c == true
        && !DETAIL_VALUES.contains(nCase.MX_SB_SAC_Detalle__c)
        && nCase.MX_SB_SAC_ActivarReglaComentario__c == false
        && nCase.MX_SB_1500_is1500__c == false
        && (nCase.MX_SB_SAC_Finalizar_Retencion__c == false
        || nCase.MX_SB_SAC_TerminaMalasVentas__c == false)) {
            nCase.addError(System.Label.MX_SB_SAC_PreventComments);
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 6/4/2020
    * @param nCase
    * @return void
    **/
    public static void preventCommentsOnQuoting(Case nCase) {
        if(COTIZACION.equals(nCase.Reason)
        && String.isBlank(nCase.MX_SB_SAC_Detalle__c)
        && String.isNotBlank(nCase.Description)) {
            nCase.MX_SB_SAC_Detalle__c.addError(System.Label.MX_SB_SAC_PreventCmnts);
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 6/2/2020
    * @param nCase
    * @return void
    **/
    public static void validateC(Case nCase, String type) {
        if(REASON_VALUES[0].equals(nCase.Reason) && String.isNotBlank(nCase.Description)
        && (String.isNotBlank(nCase.MX_SB_SAC_InformacionAdicional__c)
        && NOT_APPLICABLE.equals(nCase.MX_SB_SAC_InformacionAdicional__c)
        || String.isNotBlank(nCase.MX_SB_SAC_Informacion_Adicional_Vida__c)
        && NOT_APPLICABLE.equals(nCase.MX_SB_SAC_Informacion_Adicional_Vida__c))) {
            displayErrorOnField(nCase, type);
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 6/2/2020
    * @param nCase
    * @return void
    **/
    public static void validateR(Case nCase, String type) {
        if(REASON_VALUES[1].equals(nCase.Reason) && String.isNotBlank(nCase.Description)
        && (String.isNotBlank(nCase.MX_SB_SAC_InformacionAdicional__c) && !NOT_APPLICABLE.equals(nCase.MX_SB_SAC_InformacionAdicional__c)
        || String.isNotBlank(nCase.MX_SB_SAC_Informacion_Adicional_Vida__c) && !NOT_APPLICABLE.equals(nCase.MX_SB_SAC_Informacion_Adicional_Vida__c))) {
            displayErrorOnField(nCase, type);
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 6/2/2020
    * @param nCase
    * @param type
    * @return void
    **/
    public static void displayErrorOnField(Case nCase, String type) {
        if(AUTO.equals(type) && REASON_VALUES[0].equals(nCase.Reason)) {
            nCase.MX_SB_SAC_InformacionAdicional__c.addError(System.Label.MX_SB_SAC_NotNA);
        } else if(VIDA.equals(type) && REASON_VALUES[0].equals(nCase.Reason)) {
            nCase.MX_SB_SAC_Informacion_Adicional_Vida__c.addError(System.Label.MX_SB_SAC_NotNA);
        } else if(AUTO.equals(type) && REASON_VALUES[1].equals(nCase.Reason)) {
            nCase.MX_SB_SAC_InformacionAdicional__c.addError(System.Label.MX_SB_SAC_OnlyNA);
        } else if(VIDA.equals(type) && REASON_VALUES[1].equals(nCase.Reason)) {
            nCase.MX_SB_SAC_Informacion_Adicional_Vida__c.addError(System.Label.MX_SB_SAC_OnlyNA);
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 6/4/2020
    * @param nCase
    * @return void
    **/
    public static void checkSubdetail(Case nCase) {
        if(REASON_VALUES[0].equals(nCase.Reason) && VENTA.equals(nCase.MX_SB_SAC_Detalle__c)) {
            nCase.MX_SB_SAC_Subdetalle__c = 'Venta de unidad';
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 7/3/2020
    * @param nCase
    * @param oldCase
    * @return void
    **/
    public static void descriptionRules(Case nCase, Case oldCase) {
        MX_SB_SAC_Case_DescriptionRules_Helper.duplicate1500(nCase, oldCase);
        switch on nCase.Reason {
            when 'Cancelación' {
                final String flujo = checkIfMV(nCase);
                validateBeforeUserAuth(nCase, oldCase, flujo);
                preventCommentsOnReason(nCase);
                if(String.isBlank(nCase.Description) && nCase.MX_SB_SAC_FinalizaFlujo__c == true) {
                    switch on nCase.MX_SB_SAC_Aplicar_Mala_Venta__c {
                        when 'Si' {
                            MX_SB_SAC_Case_DescriptionRules_Helper.validateMVComments(nCase, oldCase, flujo);
                        }
                        when 'No' {
                            MX_SB_SAC_Case_DescriptionRules_Helper.validateMVComments(nCase, oldCase, REASON_VALUES[0]);
                        }
                    }
                }
            }
            when 'Retención' {
                MX_SB_SAC_Case_DescriptionRules_Service.retencionRules(nCase, oldCase);
            }
            when 'Consultas' {
                MX_SB_SAC_Case_DescriptionRules_Helper.validateQueryComments(nCase, oldCase);
                MX_SB_SAC_Case_DescriptionRules_Helper.validateQueryCommentsVida(nCase, oldCase);
            }
            when 'Transferencia' {
                MX_SB_SAC_Case_DescriptionRules_Helper.validateTransferComments(nCase, oldCase);
            }
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 7/3/2020
    * @param nCase
    * @return void
    **/
    public static void validateBeforeUserAuth(Case nCase, Case oldCase, String flujo) {
        if(String.isNotBlank(nCase.Description) && nCase.MX_SB_SAC_FinalizaFlujo__c == false) {
            nCase.Description.addError(System.Label.MX_SB_SAC_PreventCmnts);
        }
        if(nCase.MX_SB_SAC_Finalizar_Retencion__c == true && nCase.MX_SB_SAC_OcultarArgumentos__c == true) {
            nCase.MX_SB_SAC_ActivarReglaComentario__c = true;
        }
        if(flujo == 'Mala Venta' && nCase.MX_SB_SAC_TerminaMalasVentas__c == true && oldCase.MX_SB_SAC_TerminaMalasVentas__c == true) {
            nCase.MX_SB_SAC_ActivarReglaComentario__c = true;
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 7/3/2020
    * @param nCase
    * @return String
    **/
    public static String checkIfMV(Case nCase) {
        final String[] valuesToEval = new String[] {DETAIL_VALUES[0], DETAIL_VALUES[1], DETAIL_VALUES[3], DETAIL_VALUES[4]};
        if(nCase.MX_SB_SAC_FinalizaFlujo__c == true && nCase.MX_SB_SAC_Finalizar_Retencion__c == false && valuesToEval.contains(nCase.MX_SB_SAC_Detalle__c) && String.isBlank(nCase.Description)) {
            nCase.MX_SB_SAC_ActivarReglaComentario__c = true;
            nCase.MX_SB_SAC_Finalizar_Retencion__c = true;
        }
        return nCase.MX_SB_SAC_EsMalaVenta__c == true ?  'Mala Venta' : 'MV Ret';
    }

    /**
    * @description
    * @author Jaime Terrats | 6/2/2020
    * @param nCase
    * @return void
    **/
    public static void requestCommentsOn1500(Case nCase, Case oldCase) {
        final String[] valuesToExclude = new String[]{System.Label.MX_SB_SAC_Transferencia, System.Label.MX_SB_SAC_Consultas};
        if((String.isBlank(nCase.Description) || nCase.Description.length() < 10)
        && oldCase.MX_SB_1500_isAuthenticated__c == true
        && nCase.MX_SB_SAC_ActivarReglaComentario__c == true
        && nCase.Reason.equals(oldCase.Reason)
        && !valuesToExclude.contains(nCase.Reason)
        && String.isNotBlank(nCase.MX_SB_SAC_Detalle__c)
        && Trigger.isUpdate) {
            nCase.addError(System.Label.MX_SB_SAC_RequestComments);
        }
    }
}