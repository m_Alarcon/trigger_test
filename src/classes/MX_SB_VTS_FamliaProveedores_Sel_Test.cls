/**
 * @File Name          : MX_SB_VTS_FamliaProveedores_Sel_Test.cls
 * @Description        : 
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 8/6/2020 11:39:15
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    8/6/2020   Eduardo Hernández Cuamatzi     Initial Version
**/
@isTest
private class MX_SB_VTS_FamliaProveedores_Sel_Test {

    /**Proveedor */
    final static String SMART = 'Smart Center';

    @TestSetup
    static void makeData() {
        final MX_WB_FamiliaProducto__c objFamilyPro2 = MX_SB_VTS_CallCTIs_utility.newFamiliy(System.Label.MX_SB_VTS_Hogar);
        insert objFamilyPro2;
        final Product2 proHogar = MX_SB_VTS_CallCTIs_utility.newProduct(System.Label.MX_SB_VTS_Hogar, objFamilyPro2);
        insert proHogar;
        final MX_SB_VTS_ProveedoresCTI__c smartProv = MX_SB_VTS_CallCTIs_utility.newProvee(SMART, SMART);
        insert  smartProv;
        final MX_SB_VTS_FamliaProveedores__c famProSmart = MX_SB_VTS_CallCTIs_utility.newFamProveedor(objFamilyPro2, SMART, smartProv);
        insert famProSmart;
    }

    @isTest
    private static void findFamProvBySetPro() {
        final Set<Id> setIdProd = new Set<Id>();
        final Id prodId = [Select Id from MX_WB_FamiliaProducto__c where Name =: System.Label.MX_SB_VTS_Hogar].Id;
        setIdProd.add(prodId);
        Test.startTest();
            final List<MX_SB_VTS_FamliaProveedores__c> findFamProvByS = MX_SB_VTS_FamliaProveedores_Selector.findFamProvBySetPro(setIdProd);
            System.assertEquals(1, findFamProvByS.size(), 'Familia recuperada');
        Test.stopTest();
    }
}