/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 01-11-2021
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   01-11-2021   Eduardo Hernández Cuamatzi   Initial Version
**/
@isTest
public class MX_SB_VTS_SendDocumentsRuc_HTest {

    /**Producto Hogar */
    private final static String HSDNAME = 'Hogar seguro dinámico';
    /**Producto vida */
    private final static String VSDNAME = 'Vida segura dinámico';
    /**Elemento del mapa*/
    private final static String NAMEELEMENT = 'nameElement';

    @isTest
    static void fillDocsHSDPolicy() {
        Test.startTest();
            final Opportunity oppHDataDoc = new Opportunity(Producto__c = HSDNAME);
            final Map<String, String> dataDocs = MX_SB_VTS_SendDocumentsRuc_Helper.fillDataDocs(oppHDataDoc, 'policy');
            System.assertEquals('HOGAR', dataDocs.get(NAMEELEMENT), 'Datos Poliza hsd correctos');
        Test.stopTest();
    }

    @isTest
    static void fillDocsHSDKit() {
        Test.startTest();
            final Opportunity oppHDataDoc = new Opportunity(Producto__c = HSDNAME);
            final Map<String, String> dataDocs = MX_SB_VTS_SendDocumentsRuc_Helper.fillDataDocs(oppHDataDoc, 'kit');
            System.assertEquals('GMDKWHO001', dataDocs.get(NAMEELEMENT), 'Datos Kit hsd correctos');
        Test.stopTest();
    }

    @isTest
    static void fillDocsHSDCondi() {
        Test.startTest();
            final Opportunity oppHDataDoc = new Opportunity(Producto__c = HSDNAME);
            final Map<String, String> dataDocs = MX_SB_VTS_SendDocumentsRuc_Helper.fillDataDocs(oppHDataDoc, 'conditions');
            System.assertEquals('GMDCGHO001', dataDocs.get(NAMEELEMENT), 'Datos Condiciones hsd correctos');
        Test.stopTest();
    }

    @isTest
    static void fillDocsVSDPolicy() {
        final Opportunity oppHDataDoc = new Opportunity(Producto__c = VSDNAME);
        final Map<String, String> dataDocs = MX_SB_VTS_SendDocumentsRuc_Helper.fillDataDocs(oppHDataDoc, 'policy');
        System.assertEquals('VIDSE', dataDocs.get(NAMEELEMENT), 'Datos Poliza vsd correctos');
    }

    @isTest
    static void fillDocsVSDKit() {
        final Opportunity oppHDataDoc = new Opportunity(Producto__c = VSDNAME);
        final Map<String, String> dataDocs = MX_SB_VTS_SendDocumentsRuc_Helper.fillDataDocs(oppHDataDoc, 'kit');
        System.assertEquals('WKSEVIDI', dataDocs.get('productId'), 'Datos Kit vsd correctos');
    }

    @isTest
    static void fillDocsVSDCondi() {
        final Opportunity oppHDataDoc = new Opportunity(Producto__c = VSDNAME);
        final Map<String, String> dataDocs = MX_SB_VTS_SendDocumentsRuc_Helper.fillDataDocs(oppHDataDoc, 'conditions');
        System.assertEquals('CSVDBCOM', dataDocs.get('productId'), 'Datos Condiciones vsd correctos');
    }

    @isTest
    static void processExtraElements() {
        final List<Object> lstParent = new List<Object>();
        final List<Object> dataDocs = MX_SB_VTS_SendDocumentsRuc_Helper.processExtraElements('kit', VSDNAME, lstParent);
        final Map<String, Object> extraFill1 = (Map<String, Object>)dataDocs[0];
        System.assertEquals('formato', (String)extraFill1.get('elementName'), 'Datos formato vsd correctos');
    }

    @isTest
    static void processExtraElements2() {
        final List<Object> lstParent = new List<Object>();
        final List<Object> dataDocs2 = MX_SB_VTS_SendDocumentsRuc_Helper.processExtraElements('conditions', VSDNAME, lstParent);
        final Map<String, Object> extraFill2 = (Map<String, Object>)dataDocs2[0];
        System.assertEquals('formato', (String)extraFill2.get('elementName'), 'Datos formato vsd correctos');
    }
}