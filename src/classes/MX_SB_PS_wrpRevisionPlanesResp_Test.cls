/*
----------------------------------------------------------
* Nombre: MX_SB_PS_wrpRevisionPlanesResp_Test
* Julio Medellin
* Proyecto: Salesforce-Presuscritos
* Descripción : clase test para clase MX_SB_PS_wrpRevisionPlanesResp

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   Descripción
* --------------------------------------------------------------------------------
* 1.0           07/08/2020     Daniel Perez             Creación
* --------------------------------------------------------------------------------
*/
@isTest
public with sharing class MX_SB_PS_wrpRevisionPlanesResp_Test {
    @IsTest
    static void obtienePlanes() {
        final MX_SB_PS_wrpRevisionPlanesResp.catalogItemBase catalogItemBase = new MX_SB_PS_wrpRevisionPlanesResp.catalogItemBase();
        catalogItemBase.id='1230';

        final MX_SB_PS_wrpRevisionPlanesResp.revisionPlanList revisionPlanList = new MX_SB_PS_wrpRevisionPlanesResp.revisionPlanList();
        revisionPlanList.catalogItemBase=catalogItemBase;
        revisionPlanList.planReview='';
        revisionPlanList.allianceCode='';
        revisionPlanList.productCode='';
        revisionPlanList.planCode='';

        final MX_SB_PS_wrpRevisionPlanesResp.revisionPlanList[] revlist =new MX_SB_PS_wrpRevisionPlanesResp.revisionPlanList[] {};
        revlist.add(revisionPlanList);

        final MX_SB_PS_wrpRevisionPlanesResp.iCatalogItem iCatalogItem = new MX_SB_PS_wrpRevisionPlanesResp.iCatalogItem();
        iCatalogItem.revisionPlanList = revlist;
        
        final MX_SB_PS_wrpRevisionPlanesResp miclas =  new MX_SB_PS_wrpRevisionPlanesResp();
        miclas.iCatalogItem=iCatalogItem;
        
        Test.startTest();
            System.assert(miclas.iCatalogItem.revisionPlanList.size()>0,'Exito');
        Test.stopTest();
    }
}