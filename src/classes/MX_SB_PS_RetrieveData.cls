/*
----------------------------------------------------------
* Nombre: MX_SB_PS_RetrieveData
* Julio Medellin
* Proyecto: Salesforce-Presuscritos
* Descripción : clase controladora de front planes y formas de pago.

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   Descripción
* --------------------------------------------------------------------------------
* 1.0           08/07/2020     Julio Medellin           Creación
* 1.1           07/08/2020     Daniel Perez             Se añade method obtieneQLI, listaQLI, y idpart
* --------------------------------------------------------------------------------
*/
@SuppressWarnings('sf:UseSingleton,sf:NcssMethodCount,sf:DUDataflowAnomalyAnalysis')
public with sharing class MX_SB_PS_RetrieveData {

    /** Integer CODE001*/
    final static String CODE001 = '001';
    /** Integer CODE003*/
    final static String CODE003 = '003';
    /** Integer INDIVIDUAL*/
    final static String INDIVIDUAL = 'Individual';
    /** Integer CONYUGAL*/
    final static String CONYUGAL = 'Conyugal';
    /** Integer FORMATDATE*/
    final static String FORMATDATE1 = 'yyyy-MM-dd';
    /** Integer NAME*/
    final static String NAME = 'name';
    /** String para conparacion*/
    final static String CONYUG = 'Cónyuge';
    /*
    * @description obtiene los planes por producto de clipert
    * @param areacode
    * @return String
    */
    @AuraEnabled(cacheable=true)
    public static String obtieneplanes(String areacode) {
        MX_SB_PS_wrpRevisionPlanesResp wRevPlanRes;
        MX_SB_PS_wrpDatosParticularesResp wDatParRes;
        HttpResponse rsRvPl = new HttpResponse();
        try {
            rsRvPl = MX_SB_PS_IASO_Service_cls.getServiceResponse('GetRevisionPlanes','{"areacode":"'+areacode+'"}');
            wRevPlanRes = (MX_SB_PS_wrpRevisionPlanesResp)JSON.deserialize(rsRvPl.getBody(), MX_SB_PS_wrpRevisionPlanesResp.class);
            for (MX_SB_PS_wrpRevisionPlanesResp.revisionPlanList item : wRevPlanRes.iCatalogItem.revisionPlanList) {
                switch on item.allianceCode {
                    when 'VVSH0001' {
                        wDatParRes= obtienePrtData(item.productCode);
                        break;
                    }
                }
            }
        } catch(Exception exp) {
            throw new AuraHandledException(System.Label.MX_SB_BCT_Error_ListException + exp);
        }
        return JSON.serialize( wDatParRes);
    }

    /*
    * @description obtiene los datos sobre el plan de rshopitalario de clipert
    * @param String productode
    * @return MX_SB_PS_wrpDatosParticularesResp wraper class
    */
    @AuraEnabled(cacheable=true)
    public static  MX_SB_PS_wrpDatosParticularesResp obtienePrtData(String productCode) {
        HttpResponse rsPrtData = new HttpResponse();
        MX_SB_PS_wrpDatosParticularesResp wDatParRes ;
        try {
            rsPrtData = MX_SB_PS_IASO_Service_cls.getServiceResponse('GetParticularData_PS','{"producto":"'+productCode+'"}');
            wDatParRes = (MX_SB_PS_wrpDatosParticularesResp)JSON.deserialize(rsPrtData.getBody(), MX_SB_PS_wrpDatosParticularesResp.class);
        } catch (Exception exp) {
            throw new AuraHandledException(System.Label.MX_SB_BCT_Error_ListException + exp);
        }
        return wDatParRes;
    }
    /*
    * @description obtiene los lapsos de pago del plan seleccionado
    * @param String planselected
    * @return String
    */
    @AuraEnabled (cacheable=false)
        public static string getLapsos(Map<String,String> mapdata1, Map<String,String> mapdata) {
            final String planselected = mapdata1.get('planselected');
            final String formapago = mapdata1.get('formapago');
            final String oppid = mapdata1.get('oppid');
            final String nombreplan = mapdata1.get('nombreplan');
            final QuotelineItem[] lqli = listaQLI(oppid);
            final String idcotiza=lqli[0].MX_WB_Folio_Cotizacion__c;
        HttpResponse rsPrtData = new HttpResponse();
        final MX_SB_PS_wrpCotizadorVida wrpdata = new MX_SB_PS_wrpCotizadorVida();
        wrpdata.productPlan =productplan();
        wrpdata.id = idcotiza==''?null:idcotiza;
        wrpdata.validityPeriod= new MX_SB_PS_wrpCotizadorVida.validityPeriod();
        final MX_SB_PS_wrpCotizadorVida.rateQuote rateq = new MX_SB_PS_wrpCotizadorVida.rateQuote();
        rateq.paymentWay = new MX_SB_PS_wrpCotizadorVida.paymentWay(formapago.Substring(0,1),formapago);
        wrpdata.rateQuote.add(rateq);
        final MX_SB_PS_wrpCotizadorVida.catalogItemBase cati = new MX_SB_PS_wrpCotizadorVida.catalogItemBase('VIIN');
        final MX_SB_PS_wrpCotizadorVida.coverages[] lcovers = new MX_SB_PS_wrpCotizadorVida.coverages[]{};
        final MX_SB_PS_wrpCotizadorVida.particularData[] partdata = new MX_SB_PS_wrpCotizadorVida.particularData[]{};
        if(nombreplan==INDIVIDUAL) {
            partdata.add(new MX_SB_PS_wrpCotizadorVida.particularData('VHSEDAD',CODE003,'30','1'));
            partdata.add(new MX_SB_PS_wrpCotizadorVida.particularData('VHSSUMA',mapdata.get('id'),mapdata.get(NAME),'1'));
            partdata.add(new MX_SB_PS_wrpCotizadorVida.particularData('VHSPLAN',planselected,planselected,'1'));
            partdata.add(new MX_SB_PS_wrpCotizadorVida.particularData('GFLPARENT',CODE001,'1','1'));
            lcovers.add(new MX_SB_PS_wrpCotizadorVida.coverages(cati,'1'));
        } else {
            final MX_SB_VTS_Beneficiario__c[] listaben= MX_SB_PS_RetrieveDataExt_Ctrl.ordenabeneficiario(lqli[0].QuoteId);
            Integer peoplenumber=1;
            partdata.add(new MX_SB_PS_wrpCotizadorVida.particularData('VHSEDAD',CODE003,'30','1'));
            partdata.add(new MX_SB_PS_wrpCotizadorVida.particularData('VHSSUMA',mapdata.get('id'),mapdata.get(NAME),'1'));
            partdata.add(new MX_SB_PS_wrpCotizadorVida.particularData('VHSPLAN',planselected,planselected,'1'));
            partdata.add(new MX_SB_PS_wrpCotizadorVida.particularData('GFLPARENT',CODE001,'1','1'));
            peoplenumber++;
            
            lcovers.add(new MX_SB_PS_wrpCotizadorVida.coverages(cati,'1'));
            for (MX_SB_VTS_Beneficiario__c itben : listaben ) {
                partdata.add(new MX_SB_PS_wrpCotizadorVida.particularData('VHSEDAD',MX_SB_PS_RetrieveDataExt_Ctrl.getCodes(itben.MX_SB_PS_Edad__c,'edad'),itben.MX_SB_PS_Edad__c,''+peoplenumber));
                partdata.add(new MX_SB_PS_wrpCotizadorVida.particularData('VHSSUMA',mapdata.get('id'),mapdata.get(NAME),''+peoplenumber));
                partdata.add(new MX_SB_PS_wrpCotizadorVida.particularData('VHSPLAN',planselected,planselected,''+peoplenumber));
                partdata.add(new MX_SB_PS_wrpCotizadorVida.particularData('GFLPARENT',MX_SB_PS_RetrieveDataExt_Ctrl.getCodes(itben.MX_SB_VTS_Parentesco__c,'parentesco'),'0',''+peoplenumber));
                peoplenumber++;
                lcovers.add(new MX_SB_PS_wrpCotizadorVida.coverages(cati,''+ (peoplenumber-1)));
            }
        }
        wrpdata.particularData=partdata;
        final MX_SB_PS_wrpCotizadorVida.insuredList insl = new MX_SB_PS_wrpCotizadorVida.insuredList();
        insl.coverages=lcovers;
        wrpdata.insuredList=new MX_SB_PS_wrpCotizadorVida.insuredList[]{insl};
        wrpdata.region='';
        wrpdata.technicalInformation.dateRequest=''+System.Now().format('YYYY-MM-dd')+' '+System.now().format('hh:mm:ss.SSS');
        wrpdata.technicalInformation.dateConsumerInvocation=''+System.Now().format('YYYY-MM-dd')+' '+System.now().format('hh:mm:ss'+'.1');
        MX_SB_PS_wrpCotizadorVidaResp wDatParRes  = new MX_SB_PS_wrpCotizadorVidaResp();
        final Map<String,String> mpa = new Map<String,String>();
        mpa.put('cotizacionvida',JSON.Serialize(wrpdata));
        try {
            final String[] idAsegBenef= new String[]{};
            rsPrtData = MX_SB_PS_IASO_Service_cls.getServiceResponseMap('GetQuotes',mpa);
            String replace4 = rsPrtData.getBody();
            replace4=replace4.replaceAll('"number','"numbr');
            wDatParRes = (MX_SB_PS_wrpCotizadorVidaResp)JSON.deserialize(replace4.replaceAll('"currency','"moneda'), MX_SB_PS_wrpCotizadorVidaResp.class);
            switch on wDatParRes.data.insuredList.size() {
                when 1 {
                    lqli[0].MX_SB_VTS_Version__c=wDatParRes.data.insuredList[0].id;
                }
                when else {
                    for(MX_SB_PS_wrpCotizadorVidaResp.insuredList itemlist :wDatParRes.data.insuredList) {
                        idAsegBenef.add(itemlist.id);
                    }
                    lqli[0].MX_SB_VTS_Version__c=String.join(idAsegBenef,',');
                }
            }            
            lqli[0].MX_WB_Folio_Cotizacion__c=wDatParRes.data.numbr;
            lqli[0].MX_WB_subMarca__c=mapdata.get(NAME);
            lqli[0].MX_WB_origen__c=mapdata.get('id');
            lqli[0].MX_WB_placas__c=''+planselected;
            MX_RTL_QuoteLineItem_Selector.upsertQuoteLI(lqli);
        } catch (Exception exp) {
            throw new AuraHandledException(System.Label.MX_SB_BCT_Error_ListException + exp);
        }   
        return JSON.serialize(wDatParRes);
    }
    
    /*
    * @description obtiene la confifguracion producto plan
    * @param void
    * @return MX_SB_PS_wrpCotizadorVida.productPlan subclase wrapper
    */
    public static MX_SB_PS_wrpCotizadorVida.productPlan productplan() {
        final MX_SB_PS_wrpCotizadorVida.productPlan prdp =new MX_SB_PS_wrpCotizadorVida.productPlan();
        prdp.productCode='4035'; 
        prdp.planReview='001';
        prdp.planCode = new MX_SB_PS_wrpCotizadorVida.planCode('002');
        prdp.bouquetCode= 'VIIN';
        return prdp;
    }
    
    /**
    * @description 
    * @author Gerardo Mendoza Aguilar | 11-25-2020 
    * @param oppid 
    * @return String 
    **/
    @AuraEnabled(cacheable=true)
    public static String obtieneQLI(String oppid) {
        final QuotelineItem[] qlt = [select id,MX_WB_Folio_Cotizacion__c from quotelineitem where quote.opportunityid=:oppid];
        return qlt[0].MX_WB_Folio_Cotizacion__c;
    }
    /**
    * @description 
    * @author Gerardo Mendoza Aguilar | 11-25-2020 
    * @param oppid 
    * @return QuotelineItem[] 
    **/
    @AuraEnabled(cacheable=true)
    public static QuotelineItem[] listaQLI(String oppid) {
        return [select id,QuoteId,MX_WB_subMarca__c,MX_WB_origen__c,MX_WB_placas__c,MX_WB_Folio_Cotizacion__c from quotelineitem where quote.opportunityid=:oppid];
    }
    /**
    * @description 
    * @author Gerardo Mendoza Aguilar | 11-25-2020 
    * @param plan 
    * @return String 
    **/
    public static String getIdPart(String plan) {
        return CODE003;
    }
}