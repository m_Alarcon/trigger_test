/**
* Name: MX_SB_VTS_UpNameCampaign_Service
* @author Ángel Lavana Rosas
* Description : New Class Service for the SOC model
* Ver                  Date            Author                   Description
* @version 1.0         Jun/02/2020     Ángel Lavana Rosas       Initial version
**/

/**
* @description: Use method NameCampaign_Service to call class MX_SB_VTS_UpNameCampaign_Selector.NameCampaign_Selector
*/
public without sharing class MX_SB_VTS_UpNameCampaign_Service {
	/*constructor*/
    @SuppressWarnings('sf:UseSingleton')
    private MX_SB_VTS_UpNameCampaign_Service() {}
    
    /**
    * @author	Ángel Lavana Rosas | 05/06/2020 
    * @Description Invoca la clase de selector y procesa el resultado (Separa nombre y cupón de la campaña)
    * @param	IdLead	Recibe el Id del Candidato
    * @return	Campos consultados al Id o Ids de candidato
    */    
    public static List<Lead> nameCampaignService(List<Id> idLead) {
        final List<Lead> cadena = MX_RTL_Lead_Selector.leadSelectorCupon(idLead);
        
        final List<Lead> leadUpdate = new List<Lead>{};
            for(Lead valor : cadena) { 
                final Integer cupon = valor.MX_SB_VTS_CampaFaceName__c.indexOf('_CPN');         
                if(cupon == -1) {  
                    valor.MX_SB_VTS_CodCampaFace__c = 'Sin cupón';  
                    leadUpdate.add(valor);
                } else {
                    valor.MX_SB_VTS_CodCampaFace__c = valor.MX_SB_VTS_CampaFaceName__c.substringAfter('_CPN');
                    valor.MX_SB_VTS_CampaFaceName__c = valor.MX_SB_VTS_CampaFaceName__c.substringBefore('_CPN');  
                    leadUpdate.add(valor);
                }             
            }        
        final Database.SaveResult[] srList = MX_RTL_Lead_Selector.updateResult(leadUpdate, false);
        processSaveResult(srList);
        return MX_RTL_Lead_Selector.leadSelectorCupon(idLead);
    }
    
    /**
    * @description Procesa resultados dmlException
    * @author Ángel Lavana Rosas | 05/06/2020  
    * @param List<Database.SaveResult> lstResults Resultado de operación DML
    * @return void Ejecuta validación
    **/
    public static void processSaveResult(List<Database.SaveResult> lstResults) {
        for (Database.SaveResult sr : lstResults) {
            if (sr.isSuccess() == false) {
                for(Database.Error err : sr.getErrors()) {
                    throw new DMLException(err.getStatusCode() + ': ' + err.getMessage());
                }
            }
        }
    }
     /**
    * @description Recupera Leads por set de Ids
    * @author Diego Olvera | 18/09/2020 
    * @param Set<String> setLeadId Set de Ids enviados desde el front
    * @return List<Lead> Lista de Oportunidades resultantes
    **/
    public static List<Lead> findLeadsById(String leadFields, Set<String> setLeadId) {
        return MX_RTL_Lead_Selector.selectSObjectsById(leadFields, setLeadId);
    }
     /**
    * @description Recupera Lista de objeto Grupo
    * @author Diego Olvera | 18/09/2020 
    * @param n/a
    * @return List<Group> Lista de Grupos
    **/
    public static List<Group> findGroupByName(String prefijo) {
        return MX_RTL_Group_Selector.getQueuesByPrefijo(prefijo);
    }
}