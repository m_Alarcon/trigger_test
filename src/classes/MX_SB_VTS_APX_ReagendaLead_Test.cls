/*
* BBVA - Mexico
* @Author: Diego Olvera
* MX_SB_VTS_APX_ReagendaLead_Test
* @Description: Provides test coverage for apex class MX_SB_VTS_APX_ReagendaLead
* @LastModifiedBy: Diego Olvera
* @Version: 1.0
* @ChangeLog
* 1.0 Created class - Diego Olvera
* 1.1 Refactoring class for avoiding complexity with methods (virtual class)
* 1.2 Revert SAC 08/10/2020
*/
@isTest
private  class MX_SB_VTS_APX_ReagendaLead_Test {
    /** variable operaciones */
    static String txtPost = 'POST', txtDescription = 'DESCRIPCION', txtServicio='SERVICEID', txtMsj='NOT iS EMPTY',
        txtUrl='http://www.example.com', txttelf='5511223344', txtUsername='UserOwnerTest01';
    /** lead name */
    final static String LEADNAME = 'Test Lead Hogar';
    /** nombre de usuario */
    final static String USERNAME = 'testHogar';
    /** nombre de manager */
    final static String MANAGER = 'Manager';
    /** email para usuarios */
    final static String EMAIL = 'test@bbva.com.smart';
    /** cadena vacia */
    final static string EMPTYSTR='';

    /* @Method: makeData
* @Description: create test data set
*/
    @TestSetup
    public static void makeData() {
        final MX_SB_VTS_Generica__c setting = new MX_SB_VTS_Generica__c();
        setting.Name = 'ValorSmart';
        setting.MX_SB_VTS_TimeValue__c = '104';
        setting.MX_SB_VTS_Type__c = 'CP6';
        insert setting;
        final User tManager = MX_WB_TestData_cls.crearUsuario(MANAGER, 'System Administrator');
        final User tUser = MX_WB_TestData_cls.crearUsuario(USERNAME, 'System Administrator');
        tUser.MX_SB_VTS_ProveedorCTI__c = 'SMART CENTER';
        insert tManager;
        tUser.ManagerId = tManager.Id;
        insert tUser;
        final MX_SB_VTS_ProveedoresCTI__c Proveedor = MX_WB_TestData_cls.crearProveedor(tUser.MX_SB_VTS_ProveedorCTI__c);
        Proveedor.MX_SB_VTS_Identificador_Proveedor__c = 'SMART CENTER';
        insert proveedor;
        final MX_SB_VTS_Lead_tray__c Bandeja = MX_WB_TestData_cls.crearBandeja(System.Label.MX_SB_VTS_HotLeads,'1');
        insert Bandeja;
        final Group queu = new Group(Name = 'MX_SB_VTS_Reagenda', type='Queue');
        insert queu;
        final Lead myLead = MX_WB_TestData_cls.vtsTestLead(LEADNAME, System.Label.MX_SB_VTS_Telemarketing_LBL);
        myLead.LeadSource = Label.MX_SB_VTS_OrigenCallMeBack;
        myLead.MX_SB_VTS_IsReagenda__c = true;
        myLead.Email = EMAIL;
        myLead.Resultadollamada__c = 'Contacto';
        myLead.MX_SB_VTS_Tipificacion_LV2__c = 'Contacto Efectivo';
        myLead.MX_SB_VTS_Tipificacion_LV3__c = 'Interesado';
        myLead.MX_SB_VTS_Tipificacion_LV4__c = 'Acepta cotización';
        myLead.MX_SB_VTS_Tipificacion_LV5__c = 'No Venta (acepta cotización)';
        myLead.MX_SB_VTS_Tipificacion_LV6__c = 'Agendar';
        myLead.MX_SB_VTS_Tipificacion_LV7__c = 'Reagenda';
        insert myLead;
        final Task newTask  = MX_WB_TestData_cls.crearTarea(MyLead.ID,'Tarea en Lead test');
        newTask.IsReminderSet = true;
        newTask.ReminderDateTime=System.now()+1;
        newTask.ReminderDateTime=newTask.ReminderDateTime.addMinutes(-5);
        insert newTask;
        myLead.MX_SB_VTS_LookActivity__c=newTask.Id;
        update mylead;
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'bbvaMexSmartCenters_Crear_Carga', iaso__Url__c = txtUrl, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'Authentication', iaso__Url__c = 'https://validation/ok', iaso__Cache_Partition__c = 'local.MXSBVTSCache');
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'bbvaMexSmartCenter', iaso__Url__c = 'https://bbvacifrado.smartcenterservicios.com/ws_salesforce_desarrollo/api/login/authenticate', iaso__Cache_Partition__c = 'local.MXSBVTSCache');
    }
    /* @Method: testGetUser
* @Description: provides coverage for method finUsers
*/
    @isTest
    private static void testGetUsers() {
        final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'testHogar' LIMIT 1];
        System.runAs(cUser) {
            final Id leadId = [SELECT Id FROM Lead WHERE LastName = 'Test Lead Hogar'].Id;
            final object test = MX_SB_VTS_APX_ReagendaLead.getUsers(leadId);
            System.assertNotEquals(test, null,'El objeto no esta vacio1');
        }
    }
    /** Recupera Lead */
    @isTest
    private static void testGetLead() {
        final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'testHogar' LIMIT 1];
        System.runAs(cUser) {
            Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_APX_ReagendaLeadMockClass());
            iaso.GBL_Mock.setMock(new MX_SB_VTS_APX_ReagendaLeadMockClass());
            final String recordVTA1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
            final Id leadId = [SELECT Id FROM Lead LIMIT 1].ID;
            final Task testTask =[SELECT ID,FechaHoraReagenda__c, ReminderDateTime, Subject, ActivityDate,
                                  MX_SB_VTS_AgendadoPor__c, IsReminderSet
                                  FROM Task WHERE Subject = 'Tarea en Lead test' LIMIT 1];
            Test.startTest();
            MX_SB_VTS_APX_ReagendaLead.saveTask(testTask, false,leadId, recordVTA1);
            Test.stopTest();
            System.assertEquals(testTask.WhoId, leadId, 'Lead Id');
        }
    }
    /** Recupera Lead */
    @isTest
    private static void testGetLeadTask() {
        final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'testHogar' LIMIT 1];
        System.runAs(cUser) {
            Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_APX_ReagendaLeadMockClass());
            iaso.GBL_Mock.setMock(new MX_SB_VTS_APX_ReagendaLeadMockClass());
            final Id leadId = [SELECT Id FROM Lead LIMIT 1].ID;
            final Task testTask =[SELECT ID,FechaHoraReagenda__c, ReminderDateTime, Subject, ActivityDate,
                                  MX_SB_VTS_AgendadoPor__c, IsReminderSet
                                  FROM Task WHERE Subject = 'Tarea en Lead test' LIMIT 1];
            final User managerId = [Select Id, ManagerId, Name from User WHERE Manager.Name =: MANAGER LIMIT 1];
            Test.startTest();
            MX_SB_VTS_TaskReminder.leadReagenda(leadId, testTask, true, managerId);
            Test.stopTest();
            System.assertEquals(testTask.WhoId, leadId, 'Lead Id1');
        }
    }
    /** Get MX_SB_VTS_IsReagenda__c field value  */
    @isTest
    public static void updateIsReagendaTest() {
        final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'testHogar' LIMIT 1];
        System.runAs(cUser) {
            Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_APX_ReagendaLeadMockClass());
            iaso.GBL_Mock.setMock(new MX_SB_VTS_APX_ReagendaLeadMockClass());
            final Id leadId = [SELECT Id FROM Lead LIMIT 1].ID;
            final Lead testUpdIsReagenda =[SELECT ID, MX_SB_VTS_IsReagenda__c
                                           FROM Lead LIMIT 1];
            Test.startTest();
            try {
                MX_SB_VTS_APX_ReagendaLead.updateIsReagenda(leadId);
            } catch(System.AuraHandledException extError) {
                extError.setMessage('fallo inesperado');
            }
            Test.stopTest();
            System.assertNotEquals(testUpdIsReagenda, null,'El objeto no esta vacio3');
        }

    }
    /** Get tipificacionlvl7 field value  */
    @isTest
    private static boolean testValorNivelSiete() {
        final Lead myLead2 = MX_WB_TestData_cls.vtsTestLead('Prueba', System.Label.MX_SB_VTS_Telemarketing_LBL);
        myLead2.LeadSource = Label.MX_SB_VTS_OrigenCallMeBack;
        myLead2.Resultadollamada__c = 'Contacto';
        myLead2.MX_SB_VTS_Tipificacion_LV2__c = 'Contacto Efectivo';
        myLead2.MX_SB_VTS_Tipificacion_LV3__c = 'Interesado';
        myLead2.MX_SB_VTS_Tipificacion_LV4__c = 'Acepta contratación';
        myLead2.MX_SB_VTS_Tipificacion_LV5__c = 'No Venta';
        myLead2.MX_SB_VTS_Tipificacion_LV6__c = 'No cumple política de edad';
        myLead2.MX_SB_VTS_Tipificacion_LV7__c = 'N/A';
        insert myLead2;
        final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'testHogar' LIMIT 1];
        System.runAs(cUser) {
            Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_APX_ReagendaLeadMockClass());
            iaso.GBL_Mock.setMock(new MX_SB_VTS_APX_ReagendaLeadMockClass());
            final Id leadId = [SELECT Id FROM Lead WHERE LastName = 'Prueba' LIMIT 1].ID;
            final Lead testgetLvl7 =[SELECT ID FROM Lead WHERE LastName = 'Prueba' LIMIT 1];
            Test.startTest();
            try {
                MX_SB_VTS_APX_ReagendaLead.getLvl7Value(testgetLvl7.Id, true);
            } catch(System.AuraHandledException extError) {
                extError.setMessage('fallo inesperado');
            }
            Test.stopTest();
            System.assertEquals(testgetLvl7.Id, leadId, 'Lead Id2');
        }
        return true;
    }
    /*
* @Method: testGetUser
* @Description: provides coverage for method finUsers
*/
    /** Recupera Lead 2 */
    @isTest
    private static void testGetLead2() {
        final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'testHogar' LIMIT 1];
        System.runAs(cUser) {
            Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_APX_ReagendaLeadMockClass());
            iaso.GBL_Mock.setMock(new MX_SB_VTS_APX_ReagendaLeadMockClass());
            final String recordVTA1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
            final Id leadId = [SELECT Id FROM Lead LIMIT 1].ID;
            final Task testTask =[SELECT ID,FechaHoraReagenda__c, ReminderDateTime, Subject, ActivityDate,
                                  MX_SB_VTS_AgendadoPor__c, IsReminderSet
                                  FROM Task WHERE Subject = 'Tarea en Lead test' LIMIT 1];
            Test.startTest();
            MX_SB_VTS_APX_ReagendaLead.saveTask(testTask, false,leadId, recordVTA1);
            Test.stopTest();
            System.assertEquals(testTask.WhoId, leadId, 'Lead Id3');
        }
    }
    /** Error en Lead */
    @isTest
    private static void testGetLeadError() {
        final AuraHandledException err = new AuraHandledException(EMPTYSTR);
        final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'testHogar' LIMIT 1];
        System.runAs(cUser) {
            Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_APX_ReagendaLeadMockClass());
            iaso.GBL_Mock.setMock(new MX_SB_VTS_APX_ReagendaLeadMockClass());
            final String recordVTA1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
            final Lead leadObj = [SELECT Id FROM Lead LIMIT 1];
            final Task testTask =[SELECT ID,FechaHoraReagenda__c, ReminderDateTime, Subject, ActivityDate,
                                  MX_SB_VTS_AgendadoPor__c FROM Task WHERE Subject = 'Tarea en Lead test' LIMIT 1];
            testTask.FechaHoraReagenda__c=Date.valueOf('12-12-12');
            testTask.ReminderDateTime=System.now()+1;
            testTask.ReminderDateTime=testTask.ReminderDateTime.addMinutes(-5);
            test.startTest();
            try {
                MX_SB_VTS_APX_ReagendaLead.saveTask(testTask, true,leadObj.Id, recordVTA1);
            } catch(Exception ex) {
                System.assertEquals(ex.getMessage(), err.getMessage(), 'Error esperado');
            }
            test.StopTest();
        }
    }
    /** Error al recuperar usuario */
    @isTest
    private static void testGetUserError() {
        final AuraHandledException err = new AuraHandledException(EMPTYSTR);
        try {
            final Id leadId = [SELECT Id, Apellido_Materno__c, Name, MX_WB_ph_Telefono2__c, MX_WB_ph_Telefono3__c, OwnerId,
                               MX_SB_VTS_Tipificacion_LV7__c FROM Lead LIMIT 1].ID;
            MX_SB_VTS_APX_ReagendaLead.getUsers(leadId);
        } catch(System.AuraHandledException eAhEx) {
            System.assertEquals(eAhEx.getMessage() , err.getMessage(), 'No funciono');
        }
    }
    /** Recupera Lead */
    @isTest
    private static void testGetOppTask() {
        final User testManager2 = MX_WB_TestData_cls.crearUsuario('Manager2', 'System Administrator');
        final User testUser2 = MX_WB_TestData_cls.crearUsuario('TestTask2', System.Label.MX_SB_VTS_ProfileAdmin);
        testUser2.MX_SB_VTS_ProveedorCTI__c = 'SMART CENTER';
        insert testManager2;
        testUser2.ManagerId = testManager2.Id;
        insert testUser2;
        final Account accnt = MX_WB_TestData_cls.createAccount('Test account2', System.Label.MX_SB_VTS_PersonRecord);
        insert accnt;
        final Opportunity opp = MX_WB_TestData_cls.crearOportunidad('Test Reagenda2', accnt.Id, testUser2.Id, System.Label.MX_SB_VTA_VentaAsistida);
        opp.Producto__c = 'Seguro Estudia';
        opp.Reason__c = 'Venta';
        opp.MX_SB_VTS_Tipificacion_LV1__c = '';
        opp.MX_SB_VTS_Tipificacion_LV2__c = '';
        opp.MX_SB_VTS_Tipificacion_LV3__c = '';
        opp.MX_SB_VTS_Tipificacion_LV4__c = '';
        opp.MX_SB_VTS_Tipificacion_LV5__c = '';
        opp.MX_SB_VTS_Tipificacion_LV6__c = '';
        opp.MX_SB_VTS_Tipificacion_LV7__c = '';
        opp.LeadSource = 'SAC';
        opp.Motivosnoventa__c = 'Cliente molesto';
        opp.StageName = 'Contacto';
        insert opp;
        final Task newTask = MX_WB_TestData_cls.crearTarea(opp.Id,'Tarea en Opportunity test2');
        newTask.FechaHoraReagenda__c=date.newInstance(2001, 3, 21);
        newTask.IsReminderSet = true;
        newTask.ReminderDateTime=System.now()+1;
        newTask.ReminderDateTime=newTask.ReminderDateTime.addMinutes(-5);
        insert newTask;
        opp.MX_SB_VTS_LookActivity__c=newTask.Id;
        opp.OwnerId = testUser2.ManagerId;
        update opp;
        final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'TestTask2' LIMIT 1];
        System.runAs(cUser) {
            Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_APX_ReagendaLeadMockClass());
            iaso.GBL_Mock.setMock(new MX_SB_VTS_APX_ReagendaLeadMockClass());
            final Id OppId = [SELECT Id FROM Opportunity  where Name = 'Test Reagenda2' LIMIT 1].ID;
            final Task testTask =[SELECT ID,FechaHoraReagenda__c, WhoId, ReminderDateTime, Subject, ActivityDate,
                                  MX_SB_VTS_AgendadoPor__c, IsReminderSet
                                  FROM Task WHERE Subject = 'Tarea en Opportunity test2' LIMIT 1];
            final User managerId = [Select Id, ManagerId, Name from User WHERE Manager.Name = 'Manager2' LIMIT 1];
            Test.startTest();
            try {
                MX_SB_VTS_TaskReminder.oppVtaReagenda(OppId, testTask, true, managerId);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Error');
                System.assertEquals('Error', extError.getMessage(),'no entra');
            }
            Test.stopTest();
        }
    }
}