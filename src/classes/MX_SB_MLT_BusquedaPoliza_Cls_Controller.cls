/**
* -------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_BusquedaPoliza_Cls_Controller
* Autor: Juan Carlos Benitez Herrera
* Proyecto: Siniestros - BBVA
* Descripción : Clase que almacena el methode de Busqueda de Poliza
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   	Descripción
* --------------------------------------------------------------------------------
* 1.0         09/12/2019     Juan Carlos Benitez Herrera	Creación
* 1.1		  18/12/2019	 Marco Antonio Cruz Barboza		Solución de Issues 
* --------------------------------------------------------------------------------
**/
public with sharing class MX_SB_MLT_BusquedaPoliza_Cls_Controller { //NOSONAR
    /*
    *Variable ocupada para respuesta de methods al front
    */
    public static String respuesta {get; set;}
    /*
    *Arreglo de contratos para consultas locales
    */
    public static Contract[] contratosMethod {get; set;}
    /*
    *Variable Boolean resultado de method
    */
    public static Boolean rsltBusqueda {get; set;}
    /*
    *Variable mapa resultado de method
    */
    public static Map<String,SObject[]> mapaobjetos {get;set;}
    /*
    *Variable String para DML
    */
    Final static String SRCHFIELDS = 'Id,MX_SB_MLT_Certificado__c,MX_SB_SAC_FechaFinContrato__c,MX_SB_SAC_EstatusPoliza__c,MX_WB_Producto__c,createddate,accountid,MX_WB_celularAsegurado__c,MX_SB_SAC_EmailAsegurado__c,' +
                                        'MX_SB_SAC_RFCAsegurado__c,MX_SB_SAC_NumeroPoliza__c,MX_SB_SB_Placas__c,MX_SB_SAC_NombreClienteAseguradoText__c, ' +
                                        'MX_WB_apellidoMaternoAsegurado__c,MX_WB_apellidoPaternoAsegurado__c,MX_SB_SAC_Segmento__c, ' +
                                        'Description,StartDate,EndDate,MX_SB_SAC_TipoMoneda__c';
   	/*
    *Variable String para DML
    */
    Final static String SRCHFIELDSPD = 'Id, MX_SB_MLT_Certificado__c,MX_SB_SAC_FechaFinContrato__c,MX_SB_SAC_EstatusPoliza__c,MX_WB_Producto__r.Name, ' +
        								'createddate,accountid,MX_WB_celularAsegurado__c,MX_SB_SAC_EmailAsegurado__c, ' +
            							'MX_SB_SAC_RFCAsegurado__c,MX_SB_SAC_NumeroPoliza__c,MX_SB_SB_Placas__c,MX_SB_SAC_NombreClienteAseguradoText__c, ' +
            							'MX_WB_apellidoMaternoAsegurado__c,MX_WB_apellidoPaternoAsegurado__c,MX_SB_SAC_Segmento__c, ' +
            							'Description,StartDate,EndDate,MX_SB_SAC_TipoMoneda__c';
    /*
    * @description method consulta siniestro y regresa true-false en base a ello
    * @param SinId
    * @return Object siniestro__c
    */ 
    @AuraEnabled
    public static Siniestro__c siniestroActual (String sinId) {
        final Set<Id> ids = new Set<Id>{sinId};
 	    return MX_RTL_Siniestro_Service.getSiniestroData(ids)[0];
    }
    /*
    * @description method consulta de Póliza
    * @param String datos de la Póliza
    * @return Object Contrato
    */
    @AuraEnabled
    public static Map<String,SObject[]> srchCtt (String dataPol,Boolean servicioPersona, String idcuenta) { 
        Map<String,SObject[]> resp = new Map<String,SObject[]>();
        if(servicioPersona) {
            resp = srchCttlocal(dataPol,servicioPersona, idcuenta);
        } else {
            resp = srchCttlocal(dataPol,servicioPersona, idcuenta);
        } 
        return resp;
    }
    /*
    * @description method consulta de Póliza
    * @param String dataPol json de busqueda Póliza,
    * boolean serviciooPersona , String idcuenta
    * @return Map<String,SObject[]> datos de cuenta y contrato
    */
    public static Map<String,SObject[]> srchCttlocal (String dataPol,Boolean servicioPersona, String idcuenta) { 
		mapaobjetos = new Map<String,SObject[]>();
        Map<String,String> datajson = new Map<String,String>();
    	try {
            if(datapol!=null && datapol != '' ) {
                datajson = (Map<String,String>) JSON.deserializeStrict(dataPol,Map<String,String>.class);
            }
            final Contract[] cts = new Contract[]{};
            final Account[] ctas = new Account[]{};
                cts.addAll(MX_RTL_Contract_Service.getContractIdByNombre(datajson.get('nombreAsegurado'),datajson.get('aPaterno'), SRCHFIELDS) );
                cts.addAll(MX_RTL_Contract_Service.getContractIdByRFC(datajson.get('rfcSrch'), SRCHFIELDS));
                cts.addAll(MX_RTL_Contract_Service.getContractIdByAccount(idcuenta, SRCHFIELDS));
                cts.addAll(MX_RTL_Contract_Service.getContractIdByPoliza(datajson.get('polizaSrch'), datajson.get('certificado'), SRCHFIELDS));
                if (datajson.get('nombreCta')!=''  && datajson.get('aPaternoCta')!='') {
                    ctas.addAll(MX_RTL_Account_Selector.getAccountByNombre(datajson));
                } 
            mapaobjetos.put('contratos',cts);
            mapaobjetos.put('cuentas',ctas);
        } catch(exception e) {
            mapaobjetos= new Map<String,SObject[]>();
        } 
        return mapaobjetos;
    } 
    /*
    * @description method para actualizar datos del 
    * siniestro por contrato seleccionado
    * @param Object datos de la Póliza String id del siniestro
    * @return String resultado de operacion
    */
    @AuraEnabled
    public static String updateSinCont (Map<String,String> data ,String idsin) {
        respuesta='Exito';
        try {
            final Contract contr = MX_RTL_Contract_Service.getContractIdBySACnPoliza(data.get('numpoliza'),'Id,MX_SB_SAC_NumeroPoliza__c,StartDate,EndDate,MX_SB_SAC_TipoMoneda__c ')[0];
            contr.MX_SB_SAC_NumeroPoliza__c=data.get('numpoliza');
            contr.MX_SB_SAC_TipoMoneda__c=data.get('moneda');
            MX_RTL_Contract_Service.upsertSinCont(contr);
            MX_RTL_Siniestro_Service.upsertSini(new Siniestro__c (id=idsin,MX_SB_SAC_Contrato__c=contr.id));
            
		} catch (exception e) {
        	respuesta =e.getMessage();
		}
        return respuesta;
    }
    /* */
    @AuraEnabled
    public static Contract getContractByPolCert(String poliza ,String cert) {
        return MX_RTL_Contract_Service.getContractIdByPoliza(poliza, cert, SRCHFIELDSPD)[0];
    }

    /*
    * @description method para actualizar siniestro 
    * @param Sineistro__c data 
    * @return void, y cambio de pagina en automatico
    */
    @AuraEnabled
    public static void upds (Siniestro__c data) {
        MX_RTL_Siniestro_Service.upsertSini(data);
    }
    
    /*
     *consulta Siniestros por producto y poliza
     @params Contract objeto polliza del front
     */
    @AuraEnabled
    public static Siniestro__c[] getSiniestroByProducto(Contract polizadata,String producto) {
        return MX_RTL_Siniestro_Service.getSinByProduct('Id,MX_SB_SAC_Contrato__r.MX_SB_SAC_NumeroPoliza__c,MX_SB_SAC_Contrato__r.MX_WB_Producto__r.Name ',polizadata.MX_SB_SAC_NumeroPoliza__c,producto);
    }
    
}