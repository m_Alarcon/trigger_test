/************************************************************************************************************************************************
*   @Desarrollado por:      Indra                                                                                                               *
*   @Autor:                 Arsenio Perez Lopez                                                                                                 *
*   @Proyecto:              Bancomer                                                                                                            *
*   @Descripción:           Trigger de usuarios                                                                                                 *
*                                                                                                                                               *
*   Cambios (Versiones)                                                                                                                         *
*   -----------------------------------------------------------------------------------------------------------------------------               *
*   No.     Fecha                   Autor                                  Descripción                                                          *
*   ------  ----------      ----------------------              -----------------------------------------------------------------               *
*   1.0     05/10/2017      Arsenio perez lopez                 Creación Trigger                                                                *
*   1.01    03/03/2018      Ricardo Almanza Angeles             Agregar camio grupo opps trigger para Bpyp family                               *
*   1.02    26/03/2018      Ricardo Almanza Angeles             Mejorá para eliminar quey en for                                                *
*   1.03    06/04/2018      José Mario Rodríguez Sánchez        Se cambia a sentecncia upsert ADQ_ReasignaActividadesGoogle_beforeUpdate        *
*   1.04    02/05/2018      Carlos Ricardo Hernandez            Se incluye RV al cambio de Propietario y condiciones a la Query                 *
*   1.05    08/05/2018		Carlos Ricardo Hernandez            Se cambia el list por Map para las actividades Google                           *
*   1.06    11/09/2018		Jhovanny De La Cruz Cruz            Carterización de Acciones Comerciales (GBL_CommercialAction__c)                 *
*   1.07    02/11/2018      Jhovanny De La Cruz Cruz            Carterización de Tarifario (Tarif_rio__c)                                       *
*   1.08	01/15/2019      Mario A. Calderón Muñoz             Se modifica "clientePropietarioAfterUpdate" y su respectivo query para   *
*                                                               reasignar un CPC en caso de que un cliente BPyP cambie de propietario           *
*  	1.09	20/08/2019      Cindy Hernández                     Se agregan referencias al tipo de registro MX_BPP_PersonAcc_Client.             *
*   1.10    28/07/2020      Tania Vazquez                       Se resuelven minors y codesmells                                                *
*************************************************************************************************************************************************/


public without sharing class Account_cls {

    /** variable clientes*/
    static set<Id> clientes;
    /** Lista lsUpdate*/
    static List<SObject> lsUpdate;
    /*Variable PERSACC_CLIENT*/
    final static String PERSACC_CLIENT = 'MX_BPP_PersonAcc_Client';
    /*Variable redBPP*/
    final static String REDBPP='Red BPyP';
    /*Variable BPYP_TRE_CLIENTE default*/
    public static final String BPYP_TRE_CLIENTE= 'BPyP_tre_Cliente';
    /*Variable Estadi default*/
    public static final String ESTADO= 'Group Family Aprobado';

    /*Constructor*/
    private Account_cls() {

    }

     /**
     * cambiarGrupoOppsAfterUpdate: Cambia el grupo al que pertenece la cuenta
     * @param List<Account> triNew
     * @param Map<Id,Account> neMap
     * @param Map<id,Account> oldMap
     */
    public static void cambiarGrupoOppsAfterUpdate(List<Account> triNew, Map<Id,Account> neMap,  Map<id,Account> oldMap) {
        clientes = new set<Id>();
        final List<Id> AcctoRej = new List<Id>(); //NOSONAR
        final List<Id> AcctoDel = new List<Id>(); //NOSONAR
        final List<Id> AcctoRec = new List<Id>(); //NOSONAR
        final List<String> recTypes = new List<String>{'BPyP_tre_Cliente', 'BPyP_tre_familyGroup', 'BPyP_tre_noCliente', 'MX_BPP_PersonAcc_Client', 'MX_BPP_PersonAcc_NoClient'};
        String strRecordDevName;
        Boolean control=false;
        for(Account acc:triNew) {
            strRecordDevName = Schema.SObjectType.Account.getRecordTypeInfosById().get(acc.RecordTypeId).getDeveloperName();
            if(recTypes.contains(strRecordDevName)) {
                control=true;
                if(acc.MX_GroupItBelongs__c != oldMap.get(acc.Id).MX_GroupItBelongs__c) {
                    clientes.add(acc.Id);
                }
                if(acc.MX_GroupItBelongs__c != oldMap.get(acc.Id).MX_GroupItBelongs__c && acc.MX_GroupItBelongs__c==null) {
                    AcctoDel.add(acc.Id);
                }
                if(acc.BPyP_ca_FamilyGaCliente__c) {
                    AcctoRej.add(acc.Id);
                } else if(acc.BPyP_ca_RecuperarFamilyG__c) {
                    AcctoRec.add(acc.Id);
                }
            }
        }
        
        if(control) {
                manageDeletedParentAccount(AcctoDel, oldMap);
                manageApprovedParentChanges(AcctoRej, AccToDel);//NOSONAR
                manageSaveFamilyGroup(AcctoRec, AcctoDel);//NOSONAR
                manageParentChanges(clientes, neMap);
        }
    }

     /**
     * manageDeletedParentAccount: Se quita de la Cuenta el MX_GroupItBelongs__c y BPyP_Rb_Family_Group_al_que_pertenece__c
     * @param List<Id> acctoDel
     * @param Map<id,Account> oldMap
     */
    private static void manageDeletedParentAccount(List<Id> acctoDel, Map<id,Account> oldMap) {
        if(!acctoDel.isEmpty()) {
            final List<Account> backFamAcc = [select Id, BPyP_Rb_Family_Group_al_que_pertenece__c,  BPyP_Rb_Family_Group_Candidato__c,
                                              MX_GroupItBelongs__c,BPyP_ca_FamilyGaCliente__c from Account where Id in :AcctoDel];
            for(Account a: backFamAcc) {
                a.BPyP_Rb_Family_Group_Candidato__c = oldMap.get(a.Id).MX_GroupItBelongs__c;
                a.put('MX_GroupItBelongs__c','');
                a.BPyP_ca_FamilyGaCliente__c = false;
                a.put('BPyP_Rb_Family_Group_al_que_pertenece__c','');
            }
            update backFamAcc;
        }
    }

    /**
     * manageApprovedParentChanges: Se resetean los valores de Cuenta
     * @param List<Id> acctoRej -- Se utiliza para el proceso de aprobación para asignar un Family Group a un Cliente o Prospecto exitente
     * @param List<Id> acctoDel
     */
    private static void manageApprovedParentChanges(List<Id> acctoRej, List<Id> acctoDel) {
        if(!acctoRej.isEmpty()) {
            List<Account> rejFamAcc = [select Id, BPyP_Rb_Family_Group_al_que_pertenece__c, BPyP_Rb_Family_Group_Candidato__c,MX_GroupItBelongs__c,
                                       BPyP_ca_FamilyGaCliente__c from Account where Id in :AcctoRej];
            if(!AcctoDel.isEmpty()) {
                rejFamAcc= [select Id, BPyP_Rb_Family_Group_al_que_pertenece__c, BPyP_Rb_Family_Group_Candidato__c,MX_GroupItBelongs__c,
                            BPyP_ca_FamilyGaCliente__c from Account where Id not in :AcctoDel];
            }
            for(Account a: rejFamAcc) {
                a.put('BPyP_Rb_Family_Group_al_que_pertenece__c','');
                a.put('BPyP_Rb_Family_Group_Candidato__c','');
                a.put('MX_GroupItBelongs__c','');
                a.BPyP_ca_FamilyGaCliente__c=false;
            }
            update rejFamAcc;
        }
    }

     /**
     * manageSaveFamilyGroup: Actualización de los valores especificos en Cuentas
     * @param List<Id> acctoRec -- Campo utilizado para el proceso de aprobacion y recupera el Family Group cuando es rechazado del borrado
     * @param List<Id> acctoDel
     */
    private static void manageSaveFamilyGroup(List<Id> acctoRec, List<Id> acctoDel) {
        if(!acctoRec.isEmpty()) {
            List<Account> recFamAcc = [select Id, BPyP_ca_RecuperarFamilyG__c,BPyP_Rb_Family_Group_al_que_pertenece__c,
                                       BPyP_Rb_Family_Group_Candidato__c,MX_GroupItBelongs__c,BPyP_ca_FamilyGaCliente__c from Account where Id in :AcctoRec];
            if(!AcctoDel.isEmpty()) {
                recFamAcc = [select Id, BPyP_Rb_Family_Group_al_que_pertenece__c, BPyP_Rb_Family_Group_Candidato__c,MX_GroupItBelongs__c,
                             BPyP_ca_FamilyGaCliente__c from Account where Id not in :AcctoDel];
            }
            for(Account a: recFamAcc) {
                a.BPyP_Rb_Family_Group_al_que_pertenece__c=a.BPyP_Rb_Family_Group_Candidato__c;
                a.MX_GroupItBelongs__c= a.BPyP_Rb_Family_Group_Candidato__c;
                a.put('BPyP_Rb_Family_Group_Candidato__c','');
                a.BPyP_ca_RecuperarFamilyG__c=false;
            }
            update recFamAcc;
        }
    }

     /**
     * manageParentChanges: Se actualizan los propietarios de las cuentas y de las oportunidades dependiendo del valor de VP_ls_Banca__c
     * @param Set<Id> clientes
     * @param Map<Id,Account>neMap
     */
    private static void manageParentChanges(Set<Id> clientes, Map<Id,Account>neMap) {
        if(!clientes.isEmpty()) {
            final List<Account> grupos= [SELECT Id, MX_GroupItBelongs__c, Owner.VP_ls_Banca__c,
                                         (SELECT Id, Grupo__c, AccountId FROM Opportunities WHERE isClosed=false)
                                         FROM Account
                                         WHERE Id IN :clientes];
            lsUpdate = new List<SObject>();
            for(Account acc: grupos) {
                final String propietarioNuevo = neMap.get(acc.Id).MX_GroupItBelongs__c;
                for(Opportunity opp : acc.Opportunities) {
                    if(opp.AccountId == acc.Id && acc.Owner.VP_ls_Banca__c != REDBPP) {
                        opp.Grupo__c = propietarioNuevo;
                        lsUpdate.add(opp);
                    }
                }

                if(acc.Owner.VP_ls_Banca__c==REDBPP) {
                    acc.BPyP_Rb_Family_Group_al_que_pertenece__c=propietarioNuevo;
                    lsUpdate.add(acc);
                }
            }
            if(!lsUpdate.isEmpty()) {
                update lsUpdate;
            }
        }
    }

    /**
     * clientePropietarioAfterUpdate: Se crea la lista de Accounts a actualizar el owner.
     * @param List<Account> triNew ----- lista de clientes donde se modifico el propietario
     * @param Map<Id,Account> neMap
     * @param Map<id,Account> oldMap
     */
    public static void clientePropietarioAfterUpdate(List<Account> triNew, Map<Id,Account> neMap, Map<id,Account> oldMap) {
        clientes = new set<Id>();
        for(Account clienteNew :triNew) {
            if(clienteNew.OwnerId != oldMap.get(clienteNew.Id).OwnerId) {
                clientes.add(clienteNew.Id);
            }
        }
        final Map<Id,List<SObject>> relatedRecords = new Map<Id,List<SObject>>();
        if(!clientes.isEmpty()) {
            final Set<String> taskStatus = new Set<String>();
            taskStatus.addAll(EU001_cls_PickListController.getPickListValuesIntoList('Task','Status',''));
            taskStatus.remove('Completada');
            for(Account cliente: [SELECT Id, Name, RecordTypeId,RecordType.DeveloperName,
                                  (SELECT Id, Name, OwnerId, Recordtype.Developername FROM ReporteVisita__r WHERE EG_ft_HoraVisitaLlamada__c >= TODAY AND (NOT Recordtype.Developername like 'CF%') AND HI001_ft_Fecha_Hora_CheckIn__c = NULL),
                                  (SELECT Id, Name, OwnerId FROM Contacts),
                                  (SELECT Id, OwnerId, Name FROM Fichas_de_conocimiento__r),
                                  (SELECT Id, OwnerId, Subject, RecurrenceActivityId, ActivityDate
                                   FROM Tasks WHERE IsRecurrence = false AND Status in :taskStatus),
                                  (SELECT Id, OwnerId FROM Events WHERE EndDateTime > TODAY),
                                  (SELECT Id, OwnerId, AccountId FROM Opportunities WHERE isClosed=false),
                                  (SELECT Id, OwnerId, RecordType.DeveloperName FROM ConocimientoProf__r WHERE RecordType.DeveloperName Like 'BPyP%')
                                  FROM Account
                                  WHERE Id IN :clientes]) {
                                      relatedRecords.put(cliente.Id, new List<SObject>());
                                      relatedRecords.get(cliente.Id).addAll(cliente.Fichas_de_conocimiento__r);
                                      relatedRecords.get(cliente.Id).addAll(cliente.Tasks);
                                      relatedRecords.get(cliente.Id).addAll(cliente.Events);
                                      relatedRecords.get(cliente.Id).addAll(cliente.Opportunities);
                                      if(BPYP_TRE_CLIENTE.equals(cliente.RecordType.DeveloperName) || PERSACC_CLIENT.equals(cliente.RecordType.DeveloperName)) {
                                          relatedRecords.get(cliente.Id).addAll(cliente.ConocimientoProf__r);
                                      }
                                  }
        }
        if (!relatedRecords.isEmpty()) {
            updateRelatedOwners(relatedRecords, neMap);
        }
    }

    /**
     * updateRelatedOwners: Actualiza los owner de Accounts apartir de una lista.
     * @param Map<Id,List<SObject>> relatedRecords
     * @param  Map<Id,Account> newMap
     */
    private static void updateRelatedOwners(Map<Id,List<SObject>> relatedRecords, Map<Id,Account> newMap) {
        final List<Sobject> recordsToUpdate = new List<Sobject>();
        String propietarioNuevo;
        List<SObject> records;
        for(Id clientId : relatedRecords.keySet()) {
            propietarioNuevo = newMap.get(clientId).OwnerId;
            records = relatedRecords.get(clientId);
            for(Sobject record : records) {
                record.put('OwnerId', propietarioNuevo);
            }
            recordsToUpdate.addAll(records);
        }
        if(!recordsToUpdate.isEmpty()) {
            update recordsToUpdate;
        }
    }

    /**
     * accUsrTriggerbeforeUpdate: Actualiza campos referentes a Usuarios en Account
     * @param Map<Id, RecordType> mapRecordTypes
     * @param List<Account> triNew
     * @param Map<id,Account> oldMap
     */
    public static void accUsrTriggerbeforeUpdate(Map<Id, RecordType> mapRecordTypes, List<Account> triNew, Map<id,Account> oldMap) {
        validateBPYPParents(mapRecordTypes, triNew);
            final set<Id> sUsr = new set<Id>();
            for(Account acc : triNew) {
                final Account oldAcc = oldMap.get(acc.ID);
                if(acc.OwnerId != oldAcc.OwnerId) {
                    sUsr.add(acc.OwnerId);
                }
            }
            if (!sUsr.isEmpty()) {
                final map<Id,User> mpUsr = new map<Id,User>([SELECT Id,CR__c,Oficina__c,Divisi_n__c FROM User WHERE Id IN: sUsr]);
                User usr = new User();
                for(Account act : triNew) {
                    usr=mpUsr.get(act.OwnerId);
                    if(usr!=null) {
                        act.CR__c=usr.CR__c;
                        act.Oficina__c=usr.Oficina__c;
                        act.Division__c=usr.Divisi_n__c;
                    }
                }
            }
    }

    /**
     * accUsrTriggerbeforeUpdate: se utiliza addError si la verificacion de la cuenta  en el campo Estado__c no es Group Family Aprobado
     * @param Map<Id, RecordType> mapRecordTypes
     * @param List<Account> triNew
     */
    private static void validateBPYPParents(Map<Id, RecordType> mapRecordTypes, List<Account> triNew) {
        final Map<Account,Id> parents =getBPYPParents(mapRecordTypes, triNew);
        final Map<Id,Account> families=BPyP_cls_obtienePorPlanCuenta.familyGroupAprobado(parents.values());
        for(Account acc:parents.keySet()) {
            if(families.get(parents.get(acc)).Estado__c != ESTADO && !Test.isRunningTest()) { //NOSONAR
                acc.addError(System.Label.FamilyGroupNotApproved);
            }
        }
    }

    /**
     * @param Map<Id, RecordType> mapRecordTypes
     * @param List<Account> triNew
     */
    private static Map<Account,Id> getBPYPParents(Map<Id, RecordType> mapRecordTypes, List<Account> triNew) {
        final Map<String,RecordType> recordtypeBpyP=new Map<String,RecordType>();
        for(RecordType rt: mapRecordTypes.values()) {
            recordtypeBpyP.put(rt.DeveloperName, rt);
        }
        final Map<Account,Id> parents=new Map<Account,Id>();
        for(Account acc : triNew) {
            if((acc.RecordTypeId == recordtypeBpyP.get('BPyP_tre_Cliente').Id || acc.RecordTypeId == recordtypeBpyP.get('BPyP_tre_noCliente').Id || acc.RecordTypeId == recordtypeBpyP.get('BPyP_tre_familyGroup').Id
            || acc.RecordTypeId == recordtypeBpyP.get('MX_BPP_PersonAcc_Client').Id || acc.RecordTypeId == recordtypeBpyP.get('MX_BPP_PersonAcc_NoClient').Id) && acc.MX_GroupItBelongs__c != null) {
                parents.put(acc,acc.MX_GroupItBelongs__c);
            }
        }
        return parents;
    }
}