/**
* @File Name          : MX_BPP_LeadStart_Ctrl.cls
* @Description        :
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 23/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      23/06/2020        Gabriel García Rojas              Initial Version
**/
@SuppressWarnings()
public with sharing class MX_BPP_LeadStart_Ctrl {

    /**Constructor */
    @testVisible
    private MX_BPP_LeadStart_Ctrl() { }

    /**
    * @description Get Info by user
    * @author Gabriel Garcia Rojas
    * @return InfoTabCampaignStartWrapper
    **/
    @AuraEnabled
    public static MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper userInfo() {
        return MX_BPP_LeadStart_Service.userInfo();
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return List<String>
    **/
	@AuraEnabled
    public static List<String> fetchusdata() {
        return MX_BPP_LeadStart_Service.fetchusdata();
    }

    /**
    * @description Get wrapper to chart
    * @author Gabriel Garcia Rojas
    * @return MX_BPP_LeadStart_Helper.WRP_ChartStacked
    **/
    @AuraEnabled
    public static MX_BPP_LeadStart_Helper.WRP_ChartStacked fetchDataClass(List<String> paramsLead, List<String> paramsUser, List<String> paramsDate, String expression) {
        return MX_BPP_LeadStart_Service.fetchDataClass(paramsLead, paramsUser, paramsDate, expression);
    }

    /**
    * @description Get CampaingMember list
    * @author Gabriel Garcia Rojas
    * @return List<CampaignMember>
    **/
    @AuraEnabled
    public static List<CampaignMember> infoLeadByClause(List<String> paramsLead, List<String> paramsUser, List<String> paramsDate, String limite) {
        return MX_BPP_LeadStart_Service.infoLeadByClause(paramsLead, paramsUser, paramsDate, limite);
    }

    /**
    * @description return Total of record by Division, Oficina or User
    * @author Gabriel Garcia Rojas
    * @return Map<String, Integer>
    **/
    @AuraEnabled
    public static Integer fetchPgs(Integer numRecords) {
        return MX_BPP_LeadStart_Service.fetchPgs(numRecords);
    }

}