@isTest
/**
* @FileName          : MX_SB_VTS_GetQuotation_Service_Test
* @description       : Service class for use de web service of GetQuotation
* @Author            : Marco Antonio Cruz Barboza  
* @last modified on  : 02-09-2020
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   02-09-2020   Marco Antonio Cruz Barboza          Initial Version
* 1.1   02-22-2021   Eduardo Hernández Cuamatzi          Se agrega token antifraude a consumo de otp
**/
public class MX_SB_VTS_GenerarOTP_Service_Test {

    /*User name test*/
    Final static String USEROTP = 'I AM A USERNAME';
    /*Account name test*/
    Final static String ACCOTP = 'I AM A ACCOUNTNAME';
    /*Opportunity name test*/
    Final static String OPPOTP = 'I AM A OPPORTUNITYNAME';
    /*URL name test*/
    Final static String URLOTP = 'http://www.ulrexample.com';
    /*Phone name test*/
    Final static String PHONETST = '5544428233';
    /*Fields test*/
    Final static String FIELDS = 'Id, TelefonoCliente__c';
   	/*User Test*/
    private static User userTstOt = new User();
    
    /* 
    @Method: setupTest
    @Description: create test data set
    */
    @TestSetup
    static void testOTP() {
        Final User tstOtpUser = MX_WB_TestData_cls.crearUsuario(USEROTP, System.Label.MX_SB_VTS_ProfileAdmin);
        
        System.runAs(tstOtpUser) {
            
            Final Account testOtpAcc = MX_WB_TestData_cls.crearCuenta(ACCOTP, System.Label.MX_SB_VTS_PersonRecord);
            testOtpAcc.PersonEmail = 'pruebaVts@mxvts.com';
            insert testOtpAcc;
			
            Final Opportunity testOtpOpp = MX_WB_TestData_cls.crearOportunidad(OPPOTP, testOtpAcc.Id, tstOtpUser.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            testOtpOpp.LeadSource = 'Call me back';
            testOtpOpp.Producto__c = 'Hogar';
            testOtpOpp.Reason__c = 'Venta';
            testOtpOpp.StageName = 'Cotizacion';
            testOtpOpp.TelefonoCliente__c = PHONETST;
            insert testOtpOpp;
            final Pricebook2 prB2 = MX_WB_TestData_cls.createStandardPriceBook2();
            final Quote quoteTemp = MX_WB_TestData_cls.crearQuote(testOtpOpp.Id, 'OTP Test', '51512');
            quoteTemp.Pricebook2Id= prB2.Id;
            quoteTemp.OwnerId = tstOtpUser.Id;
            quoteTemp.MX_SB_VTS_ASO_FolioCot__c = '251921';
            insert quoteTemp;
            testOtpOpp.SyncedQuoteId = quoteTemp.Id;
            update testOtpOpp;
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'GrantingTicketsDev', iaso__Url__c = URLOTP, iaso__Cache_Partition__c = 'iaso.ServicesPartition');
	    insert new iaso__GBL_Rest_Services_Url__c(Name = 'generarOTP', iaso__Url__c = URLOTP, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'getGTServicesSF', iaso__Url__c = URLOTP, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
        }        
    }
    
    /* 
    @Method: getStatusOTPTest
    @Description: test method to get the status code from web service
    */
    static testMethod void getStatusOTPTest() {
        userTstOt = [SELECT Id, Name FROM User WHERE Name=:USEROTP];
        System.runAs(userTstOt) {
            Final Map<String, String> headersMock = new Map<String, String>();
            headersMock.put('tsec', '12345678');
            Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, 'Complete', '{}', headersMock); 
            iaso.GBL_Mock.setMock(mockCallout);
            test.startTest();
                final Quote quoteData = new Quote(MX_SB_VTS_ASO_FolioCot__c = '5551131131');
            	final Map<String,String> getValues = MX_SB_VTS_GenerarOTP_Service.getStatusOTP(PHONETST, quoteData, '1234');
        	test.stopTest();
            System.assertNotEquals(getValues, null,'Envio de OTP');
        }
    }
    
    /* 
    @Method: getPhoneTest
    @Description: test method to get Opportunity Phone from the currently record
    */
    static testMethod void getPhoneTest() {
        userTstOt = [SELECT Id, Name FROM User WHERE Name=:USEROTP];
        System.runAs(userTstOt) {
            test.startTest();
            	Final String testOppId = [SELECT Id, TelefonoCliente__c FROM Opportunity WHERE TelefonoCliente__c=: PHONETST].Id;
            	Final String getPhone = MX_SB_VTS_GenerarOTP_Service.getPhone(testOppId, FIELDS);
        	test.stopTest();
            System.assertNotEquals(getPhone, null,'Recupero el n?mero telefonico');
        }
    }

    /* 
    @Method: statusOTPCtrl
    @Description: test method to get the status code from web service
    */
    static testMethod void statusOTPCtrl() {
        final User userOtpTst = [SELECT Id, Name FROM User WHERE Name=:USEROTP];
        System.runAs(userOtpTst) {
            Final Map<String, String> mockHeaders = new Map<String, String>();
            mockHeaders.put('tsec', '847392344');
            Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, 'Complete', '{}', mockHeaders);
            iaso.GBL_Mock.setMock(mockCallout);
            test.startTest();
                final Opportunity oppData = [Select Id,TelefonoCliente__c from Opportunity where Name =: OPPOTP];
            	final Map<String,String> valuesMock = MX_SB_VTS_PaymentModule_Ctrl.getCodeStatus(oppData.Id,'123');
        	test.stopTest();
            System.assertNotEquals(valuesMock, null,'Envio de OTP');
        }
    }
}