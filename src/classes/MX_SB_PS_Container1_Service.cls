/**
* @author           Daniel perez lopez
* Project:          Presuscritos
* Description:      Clase service para llamar a selectores QuoteLineItem
*
* Changes (Version)
* ------------------------------------------------------------------------------------------------
*           No.     Date            Author          1        Description
*           -----   ----------      --------------------    --------------------------------------
* @version  1.0     2020-09-05      Daniel perez lopez.     Creación de la Clase
*/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_SB_PS_Container1_Service {
    
       /**
    * @description method para actualizar QuotelineItems
    * @author Daniel Perez Lopez | 04/09/2020
    * @param String Fields
    * @param Set<ID> lstQuotes
    * @return QuoteLineItem[]
    **/
    public static QuoteLineItem[] upsrtQuoteLineItem(Quote quote,QuoteLineItem[] quoteli,String prod) {
        final Set<String> names = new Set<String>();
        final Set<Id> qids = new Set<Id>();
        final Set<String> process = new Set<String>();
        names.add(prod);
        process.add('SAC');
        qids.add(quote.Id);
            final Product2[] producto= MX_RTL_Product2_Selector.findProsByNameProces(names,process,true);
            final String[] ids = new String[]{producto[0].Id};
            final PricebookEntry[] pbe= MX_RTL_PricebookEntry_Selector.pricebookEntries('Id,PriceBook2Id',ids);
            quote.Pricebook2Id= pbe[0].PriceBook2Id;
            final QuoteLineItem[] qliExistente= MX_RTL_QuoteLineItem_Selector.quoteliItems(' Id,Product2Id,QuoteId ',qids);
            if(qliExistente.size()>0) {
                quoteli[0].Id=qliExistente[0].Id;
            } else {
                quoteli[0].Product2Id=producto[0].Id;
            	quoteli[0].PricebookEntryId =pbe[0].Id;
            }
            MX_RTL_Quote_Selector.upsrtQuote(quote);
        return MX_RTL_QuoteLineItem_Selector.upsertQuoteLI(quoteli);
    }
}