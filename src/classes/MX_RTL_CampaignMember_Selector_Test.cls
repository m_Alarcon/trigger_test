/**
* @File Name          : MX_RTL_CampaignMember_Selector_Test.cls
* @Description        : Test class for MX_RTL_CampaignMember_Selector
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 01/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      01/06/2020            Gabriel Garcia Rojas          Initial Version
* 1.1      21/10/2020            Gerardo Mendoza Aguilar       Se agrego methodo getIdRecordTypeParamTest, updCampMemberListTest
 * 1.2     24/03/2021            Héctor Saldaña                Se cambiaron cláusulas en métodos getListCampaignMemberByDatabaseTest
 *                                                              updCampMemberListTest, getListCampaignMemberByDatabaseUserTest
**/
@isTest
private class MX_RTL_CampaignMember_Selector_Test {
	 /** name variables for records */
    final static String NAME = 'New Campaing Member Test';
    /** name fileds for records */
    final static String FIELDS = 'Id, Name';
    /** error message */
    final static String MESSAGEFAIL = 'Fail getListCampaignMemberByDatabaseTest method';
    /**
     * @description setup data
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @TestSetup
    private static void testSetUpData() {
        final Lead candidato = new Lead(LastName = NAME);
        insert candidato;

        final Campaign campanya = new Campaign(Name = NAME, EndDate= Date.today());
        insert campanya;

        final CampaignMember campMem = new CampaignMember(CampaignId = campanya.Id, Status='Sent', LeadId = candidato.Id);
        insert campMem;

    }

    /**
     * @description constructor test
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void testConstructor() {
        final MX_RTL_CampaignMember_Selector instanceC = new MX_RTL_CampaignMember_Selector();
        System.assertNotEquals(instanceC, null, MESSAGEFAIL);
    }

    /**
     * @description
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void getListCampaignMemberByDatabaseTest() {
        final String clausula = 'WHERE Campaign.Name = \'' + NAME + '\'';
        final List<CampaignMember> listCampM = MX_RTL_CampaignMember_Selector.getListCampaignMemberByDatabase(FIELDS, clausula);
        MX_RTL_CampaignMember_Selector.getLeadOpportunityInfoByIds('Id', new List<Id>{listCampM[0].Id});
        System.assertEquals(listCampM.size(), 1, MESSAGEFAIL);
    }

    /**
     * @description
     * @author Gabriel Garcia | 01/07/2020
     * @return void
     **/
    @isTest
    private static void getListCampaignMemberByDatabaseUserTest() {
        final User testUser = UtilitysDataTest_tst.crearUsuario(NAME, 'BPyP Estandar', 'BPYP BANQUERO BANCA PERISUR');
        testUser.BPyP_ls_NombreSucursal__c = '6385 BANCA PERISUR';
        testUser.Divisi_n__c = 'METROPOLITANA';
        insert testUser;
        final String clausula = 'WHERE Campaign.Name = \'' + NAME + '\'';
        final List<CampaignMember> listCampM = MX_RTL_CampaignMember_Selector.getListCampaignMemberByDatabase(FIELDS, clausula, new List<User>{testUser});
        System.assertEquals(listCampM.size(), 1, MESSAGEFAIL);
    }

    /**
     * @description
     * @author Gabriel Garcia | 01/06/2020
     * @return void
     **/
    @isTest
    private static void getListCampignMembersByLeadIdsTest() {
       	final Lead candidato = [SELECT Id FROM Lead LIMIT 1];
        final List<CampaignMember> listCampM = MX_RTL_CampaignMember_Selector.getListCampignMembersByLeadIds(new List<Id>{candidato.Id});
        System.assertEquals(listCampM.size(), 1, MESSAGEFAIL);
    }
    /**
     * @description
     * @author Gerardo Mendoza | 21/10/2020
     * @return void
     **/
    @isTest
    private static void getIdRecordTypeParamTest() {
        test.startTest();
        Final Id recordType = MX_RTL_CampaignMember_Selector.getIdRecordTypeParam('MX_SB_COB_Cotizador_RS_Hospital');
        system.assert(recordType!=null, 'RecordType recuperado');
        test.stopTest();


    }
    /**
     * @description
     * @author Gerardo Mendoza | 21/10/2020
     * @return void
     **/
    @isTest
    public static void updCampMemberListTest() {
        test.startTest();
        Final List <CampaignMember> memberUdp = new List <CampaignMember>();
        Final Id idOld = [SELECT Id FROM CampaignMember where Campaign.Name = 'New Campaing Member Test'].Id;
        Final CampaignMember member = new CampaignMember(Id=idOld, MX_SB_PS_productoOfertado__c = 'RSPHTEST');
        memberUdp.add(member);
        MX_RTL_CampaignMember_Selector.updCampMemberList(memberUdp);
        system.assert(!memberUdp.isEmpty(), 'Miembro de campanya actualizado');
        test.stopTest();
    }
}