/**
 * @File Name          : MX_RTL_Matriz_Kit_Vida_mdt_Selector.cls
 * @Description        :
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 5/26/2020, 05:22:22 AM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/26/2020   Juan Carlos Benitez         Initial Version
**/
public with sharing class MX_RTL_Matriz_Kit_Vida_mdt_Selector {
     /** List of records to be returned */
    final static List<SObject> RTRN_MATRZKITV = new List<SObject>();

    /** constructor */
    @TestVisible
    private MX_RTL_Matriz_Kit_Vida_mdt_Selector() { }

    /**
    * @description
    * @author Juan Carlos Benitez | 5/25/2020
    * @param mdtFields
    * @return RTRN_MATRZKITV
    **/

    public static List<Sobject> getKitDataById(Map<String,String> mapa) {
        	final String product = mapa.get('product');
            final String subramo= mapa.get('subramo');
            final String subtipo= mapa.get('subtipo');
            final String regla1= mapa.get('regla1');
	        final String regla2= mapa.get('regla2');
	    RTRN_MATRZKITV.addAll([SELECT MX_SB_MLT_Kit__c FROM MX_SB_MLT_Matriz_Kit_Vida__mdt WHERE MX_SB_MLT_Producto__c=: product and MX_SB_MLT_SubRamo__c=:subramo and MX_SB_MLT_Subtipo__c=:subtipo and MX_SB_MLT_Regla1__c=:regla1 and MX_SB_MLT_Regla2__c=:regla2]);
        return RTRN_MATRZKITV;
    }
}