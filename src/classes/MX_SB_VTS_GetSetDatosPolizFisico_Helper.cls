/**
 * @File Name          : MX_SB_VTS_GetSetDatosPolizFisico_Helper.cls
 * @Description        : Clase Encargada de Proporcionar Soporte
 *                       a la clase: MX_SB_VTS_GetSetDatosDCDPFrm_Helper
 * @Author             : Alexandro Corzo
 * @Group              : 
 * @Last Modified By   : Alexandro Corzo
 * @Last Modified On   : 20/01/2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0       20/01/2021      Alexandro Corzo        Initial Version
 * 1.1       08/02/2021      Alexandro Corzo        Se realizan ajustes a clase para Codesmells
**/
@SuppressWarnings('sf:UseSingleton')
public class MX_SB_VTS_GetSetDatosPolizFisico_Helper {
    /**
     * @description : Construye los datos generales para realizar la petición
     *              : para el consumo del servicio ASO
     * @author      : Alexandro Corzo
     * @return      : MX_SB_VTS_wrpModHomePolTempDataSrv.base objWrapper
     */
    public static MX_SB_VTS_wrpModHomePolTempDataSrv.base fillDataGen(Map<String, String> mDataPolFis) {
        final MX_SB_VTS_wrpModHomePolTempDataSrv.base objWrapper = new MX_SB_VTS_wrpModHomePolTempDataSrv.base();
        objWrapper.header = fillDataHeader();
        objWrapper.homeInsurancePolicies = fillHomeInsPol(mDataPolFis);
        return objWrapper;
    }
    
    /**
     * @description : Construye los datos Header para realizar la petición
     *              : para el consumo del servicio ASO
     * @author      : Alexandro Corzo
     * @return      : MX_SB_VTS_wrpModHomePolTempDataSrv.base objWrapper
     */
    public static MX_SB_VTS_wrpModHomePolTempDataSrv.header fillDataHeader() {
        final MX_SB_VTS_wrpModHomePolTempDataSrv.header objWrapperHeader = new MX_SB_VTS_wrpModHomePolTempDataSrv.header();
        objWrapperHeader.aapType = '10000137';
        objWrapperHeader.dateRequest = '2017-10-03 12:33:27.104';
        objWrapperHeader.channel = '4';
        objWrapperHeader.subChannel = '26';
        objWrapperHeader.branchOffice = 'INCE';
        objWrapperHeader.managementUnit = 'DHSDT003';
        objWrapperHeader.user = 'MARCO';
        objWrapperHeader.idSession = '3232-3232';
        objWrapperHeader.idRequest = '1212-121212-12121-212';
        objWrapperHeader.dateConsumerInvocation = '2017-10-03 12:33:27.104';
        return objWrapperHeader;
    }
    
    /**
     * @description : Construye los datos HomeInsPol para realizar la petición
     *              : para el consumo del servicio ASO
     * @author      : Alexandro Corzo
     * @return      : MX_SB_VTS_wrpModHomePolTempDataSrv.base objWrapper
     */
    public static MX_SB_VTS_wrpModHomePolTempDataSrv.homeInsurancePolicies fillHomeInsPol(Map<String, String> mDataHomInsPol) {
        final MX_SB_VTS_wrpModHomePolTempDataSrv.homeInsurancePolicies objWrapperHIPol = new MX_SB_VTS_wrpModHomePolTempDataSrv.homeInsurancePolicies();
        final MX_SB_VTS_wrpModHomePolTempDataSrv.property objWrapperProp = new MX_SB_VTS_wrpModHomePolTempDataSrv.property();
        final MX_SB_VTS_wrpModHomePolTempDataSrv.address objWrapperAddr = new MX_SB_VTS_wrpModHomePolTempDataSrv.address();
        final MX_SB_VTS_wrpModHomePolTempDataSrv.country objWrpCountry = new MX_SB_VTS_wrpModHomePolTempDataSrv.country();
        final MX_SB_VTS_wrpModHomePolTempDataSrv.state objWrpState = new MX_SB_VTS_wrpModHomePolTempDataSrv.state();
        final MX_SB_VTS_wrpModHomePolTempDataSrv.city objWrpCity = new MX_SB_VTS_wrpModHomePolTempDataSrv.city();
        final MX_SB_VTS_wrpModHomePolTempDataSrv.township objWrpTownship = new MX_SB_VTS_wrpModHomePolTempDataSrv.township();
        final MX_SB_VTS_wrpModHomePolTempDataSrv.neighborhood objWrpNeigh = new MX_SB_VTS_wrpModHomePolTempDataSrv.neighborhood();
        final MX_SB_VTS_wrpModHomePolTempDataSrv.postalData objWrpPostData = new MX_SB_VTS_wrpModHomePolTempDataSrv.postalData();
		objWrapperHIPol.quoteId = mDataHomInsPol.get('quote_id');
        objWrapperHIPol.certifiedNumber = '1';
        objWrapperProp.iddata = '0001';
        objWrpCountry.iddata = mDataHomInsPol.get('county_id');
        objWrpState.iddata = ' ' + mDataHomInsPol.get('state_id');
        objWrpCity.iddata = mDataHomInsPol.get('city_id');
        objWrpTownship.iddata = ' ' + mDataHomInsPol.get('township_id');
        objWrpNeigh.iddata = mDataHomInsPol.get('neighborhood_id');
        objWrpPostData.code = mDataHomInsPol.get('postal_code');
		objWrapperAddr.country = objWrpCountry;
        objWrapperAddr.state = objWrpState;
        objWrapperAddr.city = objWrpCity;
        objWrapperAddr.township = objWrpTownship;
        objWrapperAddr.neighborhood = objWrpNeigh;
        objWrapperAddr.postalData = objWrpPostData;        
        objWrapperAddr.streetName = mDataHomInsPol.get('streetName');
        objWrapperAddr.streetNumber = mDataHomInsPol.get('streetNumber');
        objWrapperAddr.door = mDataHomInsPol.get('door');
        objWrapperAddr.condominium = mDataHomInsPol.get('condominium');
		objWrapperProp.address = objWrapperAddr;
        objWrapperHIPol.property = objWrapperProp;
        return objWrapperHIPol;
    }
}