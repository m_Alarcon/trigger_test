/**
* -------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_PolizaWsController
* Autor: Daniel Perez Lopez
* Proyecto: Siniestros - BBVA
* Descripción : Clase que almacena el method para listado de coberturas 
* disponibles
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   	Descripción
* --------------------------------------------------------------------------------
* 1.0         09/12/2019     	Daniel Perez Lopez         	Creación
* 
* --------------------------------------------------------------------------------
**/
public with sharing class MX_SB_MLT_PolizaWsController { //NOSONAR
   /*
    * @getTableCertificados Obtiene los datos para la tabla certificados
    * @param Object
    * @return List<Map<String,String>>
    */ 
    @AuraEnabled
    public static List<Map<String,String>> getTableCertificados (String ideuno, String test) {
        final List<Map<String,String>> siniarray = new List<Map<String,String>>();
        final Map<String,string> datos1 = new Map<String,string>();
            datos1.put('status','Act');
            datos1.put('certificado','88713');
            datos1.put('codigo','AUAR');
            datos1.put('segClas','BBVA');
            datos1.put('ramo','Automoviles Ress');
            datos1.put('plan','consumer finance unificafo');
            datos1.put('rev','006');
        siniarray.add(datos1);
        final Map<String,String> datos2 = new Map<String,String>();
            datos2.put('status','Act');
            datos2.put('certificado','88699');
            datos2.put('codigo','AUAR');
            datos2.put('segClas','BBVA');
            datos2.put('ramo','Automoviles Ress');
            datos2.put('plan','conumer finance unificafo');
            datos2.put('rev','001');
        siniarray.add(datos2);
        return siniarray;
    } 
}