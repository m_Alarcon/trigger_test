/**
 * @File Name          : MX_SB_VTS_leadMultiServiceVts_C_Test.cls
 * @Description        : 
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 09-04-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    13/6/2020   Eduardo Hernández Cuamatzi     Initial Version
 * 1.1    16/7/2020   Eduardo Hernández Cuamatzi     Adecuación response service body
**/
@isTest
public class MX_SB_VTS_leadMultiServiceVts_C_Test {
    /**Origen call me back */
    final static String TYPECMB = 'CALLMEBACK';
    /**Origen Tracking web */
    final static String TRACKING = 'TRACKING';
    /**Canal de petición */
    final static String CHANNEL10000120 = '10000120';
    /**Codigo de producto */
    final static String PRODUCTCODE = '008';
    /**Codigo de producto */
    final static String PRODUCTCODEHSD = '3002';
    /**PRODUCTPLAN producto de petiión */
    final static String PRODUCTPLAN = '008';
    /**NAMECLIENT nombre del cliente */
    final static String NAMECLIENT = 'TEST';
    /**LASTNAMECLIENT apelllido cliente */
    final static String LASTNAMECLIENT = 'TEST';
    
    @TestSetup
    static void makeData() {
        final User IntegrationUser = MX_WB_TestData_cls.crearUsuario('IntegrationUser', 'IntegrationUser');
        insert IntegrationUser;
        MX_SB_VTS_CallCTIs_utility.initLeadMulServ();
    }


    /**
    * @description Genera Json de servicio
    * @author Eduardo Hernández Cuamatzi | 08-25-2020 
    * @param valuesServices datos del servicio
    * @return String Json concatenado
    **/
    public static String generateWSbody(Map<String, String> valuesServices) {
        String jsoncmb = '{"type":"'+valuesServices.get('type')+'","channel":"'+valuesServices.get('channel')+'","product":{"code":"'+valuesServices.get('productC')+'","plan":{"code":"'+valuesServices.get('productP')+'"}},';
        jsoncmb += '"quote":{"id":"'+valuesServices.get('quoteId')+'"},"coupon":"'+valuesServices.get('cupon')+'","person":{"name":"'+valuesServices.get('name')+'","firstSurname":"'+valuesServices.get('lastName')+'",';
        jsoncmb += '"contactDetails":[{"key":"PER-PHONE-1","value":"'+valuesServices.get('mobile')+'"},{"key":"PER-EMAIL","value":"'+valuesServices.get('email')+'"}]},';
        jsoncmb += '"appointment":{"time":"'+valuesServices.get('attempDate')+'"}}';
        return jsoncmb;
    }
    
    @isTest
    private static void insertServices() {
        String jsoncmb = '{"type":"'+TYPECMB+'","channel":"10000120","product":{"code":"008","plan":{"code":"008"}},';
        jsoncmb += '"quote":{"id":"123456"},"coupon":"MA0001","person":{"name":"Juan","firstSurname":"Gutiérrez",';
        jsoncmb += '"contactDetails":[{"key":"PER-PHONE-1","value":"1212345678"},{"key":"PER-EMAIL","value":"japerez@example.com"}]},';
        jsoncmb += '"appointment":{"time":"2020-06-06T16:00:00Z"}}';
        final RestRequest req = new RestRequest();
        req.addHeader('Content-Type', 'application/json');
        req.requestURI = '/services/apexrest/leadMultiServiceVts';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(jsoncmb);
        RestContext.request = req;
        Test.startTest();
            final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'IntegrationUser'];
            System.runAs(cUser) {
                final RestResponse res = new RestResponse();
                RestContext.response = res;
                MX_SB_VTS_leadMultiServiceVts_Ctrl.PostSalesforceTracking();
                System.assert(String.isNotEmpty(String.valueOf(RestContext.response.responseBody)), 'Id esperado');
            }
        Test.stopTest();
    }

    /**
    * @description Genera mapa de quote
    * @author Eduardo Hernández Cuamatzi | 09-04-2020 
    * @param valsMap valores para quote
    * @return Map<String, String> mapa de valores para quote
    **/
    public static Map<String,String> createMapJsonHSD(List<String> valsMap) {
        final Map<String, String> mapVals = new Map<String, String>();
        mapVals.put('type', TRACKING);
        mapVals.put('channel', CHANNEL10000120);
        mapVals.put('productC', PRODUCTCODEHSD);
        mapVals.put('productP', PRODUCTCODE);
        mapVals.put('quoteId', valsMap[3]);
        mapVals.put('cupon', '');
        mapVals.put('name', NAMECLIENT);
        mapVals.put('lastName', LASTNAMECLIENT);
        mapVals.put('mobile', valsMap[1]);
        mapVals.put('email', valsMap[2]);
        mapVals.put('attempDate', valsMap[0]);
        return mapVals;
    }
}