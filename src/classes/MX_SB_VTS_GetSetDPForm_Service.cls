/**
 * @File Name          : MX_SB_VTS_GetSetDPForm_Service.cls
 * @Description        : Formulario - Dirección de la Propiedad
 * @Author             : Alexandro Corzo
 * @Group              : 
 * @Last Modified By   : Alexandro Corzo
 * @Last Modified On   : 15/07/2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0       15/07/2020      Alexandro Corzo        Initial Version
 * 1.1       12/01/2021      Alexandro Corzo        Ajustes a la clase para consumo ASO
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_GetSetDPForm_Service {
    /**
     * Atributos de la Clase
     */
    private static final Integer OKCODE = 200;
    
    /**
     * @description: Recupera del WS siguiente información: Colonia, Estado, Ciudad y Alcaldia
     *               conforme al CP ingresado.
     * @author:      Eduardo Hernandez Cuamatzi
     * @return: 	 Map<String,List<Object>> oReturnValues
     */
    public static Map<String,List<Object>> dataInputCPMockService(String sCodigoPostal) {
        Map<String, List<Object>> oReturnValues = null;
        Map<String, Object> sBodyMetaService = null;
        oReturnValues = new Map<String, List<Object>>();
        sBodyMetaService = new Map<String, Object>{'CO' => 'CO'};
        final HttpResponse oResponseSrv = MX_RTL_IasoServicesInvoke_Selector.callServices('getListInsuraceCustomerCatalog', sBodyMetaService, obtRequestSrv(sCodigoPostal));
        if(oResponseSrv.getStatusCode() ==  OKCODE) {
            final Map<String, Object> oReturnValuesSrv = (Map<String,Object>) JSON.deserializeUntyped(oResponseSrv.getBody());
            final List<Object> oValuesCP = (List<Object>) ((Map<String,Object>) oReturnValuesSrv.get('iCatalogItem')).get('suburb');
            final List<Object> oValueGeneral = new List<Object>();
            final List<Object> oValueColonias = new List<Object>();
            for (Object oRow: oValuesCP) {
                String sWsFieldName = null;
                String sNeight = null;
                sWsFieldName  = 'name';
                sNeight  = 'neighborhood';
                if (oValueGeneral.isEmpty()) {
                    String sWSFieldId = null;
                    sWSFieldId = 'id';
                    final Map<String, Object> oDataGeneral = new Map<String, Object>();
                    oDataGeneral.put('state', ((Map<String,Object>)(((Map<String, Object>) oRow).get('state'))).get(sWsFieldName));
                    oDataGeneral.put('city', ((Map<String,Object>)(((Map<String, Object>) oRow).get('city'))).get(sWsFieldName));
                    oDataGeneral.put('country', ((Map<String,Object>)(((Map<String, Object>) oRow).get('county'))).get(sWsFieldName));
                    oDataGeneral.put('neighb', ((Map<String,Object>)(((Map<String, Object>) oRow).get(sNeight))).get(sWsFieldName));
                    oDataGeneral.put('zipcode', sCodigoPostal);
                    oDataGeneral.put('stateId', ((Map<String,Object>)(((Map<String, Object>) oRow).get('state'))).get(sWSFieldId));
                    oDataGeneral.put('cityId', ((Map<String,Object>)(((Map<String, Object>) oRow).get('city'))).get(sWSFieldId));
                    oDataGeneral.put('countryId', ((Map<String,Object>)(((Map<String, Object>) oRow).get('county'))).get(sWSFieldId));
                    oDataGeneral.put('neighbId', ((Map<String,Object>)(((Map<String, Object>) oRow).get(sNeight))).get(sWSFieldId));
                    oValueGeneral.add(oDataGeneral);
                }
                if(((Map<String,Object>)(((Map<String, Object>) oRow).get(sNeight))).containsKey(sWsFieldName)) {
                    final Map<String, Object> oDataColonias = new Map<String, Object>();
                    oDataColonias.put('label', ((Map<String,Object>)(((Map<String, Object>) oRow).get(sNeight))).get(sWsFieldName).toString().replace('"',''));
                    oDataColonias.put('value', ((Map<String,Object>)(((Map<String, Object>) oRow).get(sNeight))).get('id'));
                    oValueColonias.add(oDataColonias);
                }
            }
            final Map<String, Object> mStatusCode = new Map<String, Object>{'code' => String.valueOf(OKCODE), 'description' => 'Consumo de Servicio Exitoso!'};
            final List<Object> lstResponseMsg = new List<Object>();
            lstResponseMsg.add(mStatusCode);
            oReturnValues.put('SFDatosGenerales', oValueGeneral);
            oReturnValues.put('SFDatosColonias', oValueColonias);
            oReturnValues.put('oResponse', lstResponseMsg);
        } else {
            final Map<String, Object> mStatusCode = MX_SB_VTS_Codes_Utils.statusCodes(oResponseSrv.getStatusCode());
            final List<Object> lstResponseMsg = new List<Object>();
            lstResponseMsg.add(mStatusCode);
            oReturnValues.put('oResponse', lstResponseMsg);
        }
        return oReturnValues;
    }
    
    /**
     * @description : Realiza el armado del objeto Request para el consumo del servicio ASO
     * @author      : Alexandro Corzo
     * @return      : HttpRequest oRequest
     */
    public static HttpRequest obtRequestSrv(String sCodigoPostal) {
        final Map<String,iaso__GBL_Rest_Services_Url__c> allCodASOCP = iaso__GBL_Rest_Services_Url__c.getAll();
        final iaso__GBL_Rest_Services_Url__c objASOCP = allCodASOCP.get('getListInsuraceCustomerCatalog');
        final String strEndPntCP = objASOCP.iaso__Url__c;
        final HttpRequest oRqstCP = new HttpRequest();
        oRqstCP.setTimeout(120000);
        oRqstCP.setMethod('POST');
        oRqstCP.setHeader('Content-Type', 'application/json');
        oRqstCP.setHeader('Accept', '*/*');
        oRqstCP.setHeader('Host', 'https://test-sf.bbva.mx');
        oRqstCP.setEndpoint(strEndPntCP);
        oRqstCP.setBody(obtBodyService(sCodigoPostal));
        return oRqstCP;
    }
    
    /**
     * @description : Realiza el armado del Body que sera ocupado en el Request del Servicio
     * @author      : Alexandro Corzo
     * @return      : String sBodyService
     */
    public static String obtBodyService(String sCodigoPostal) {
    	String sBodySrvCP = '';
        sBodySrvCP += '{';
        sBodySrvCP += '"header": {';
        sBodySrvCP += '"aapType": "10000002",';
        sBodySrvCP += '"dateRequest": "2020-07-03 05:30:00",';
        sBodySrvCP += '"channel": "66",';
        sBodySrvCP += '"subChannel": "120",';
        sBodySrvCP += '"branchOffice": "ASD",';
        sBodySrvCP += '"managementUnit": "APYME001",';
        sBodySrvCP += '"user": "CARLOS",';
        sBodySrvCP += '"idSession": "3232-3232",';
        sBodySrvCP += '"idRequest": "1212-121212-12121-212",';
        sBodySrvCP += '"dateConsumerInvocation": "2017-10-03 20:03:00"';
        sBodySrvCP += '},';
        sBodySrvCP += '"zipCode":"'+sCodigoPostal+'"';
        sBodySrvCP += '}';
        return sBodySrvCP;
    }
}