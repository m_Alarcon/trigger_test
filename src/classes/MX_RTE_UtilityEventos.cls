/**
* ------------------------------------------------------------------------------
* @Nombre: MX_RTE_UtilityEventos
* @Autor: Sandra Ventura García
* @Proyecto: Workflow GFD
* @Descripción : Clase para generar datos de prueba para componentes Invitados
* ------------------------------------------------------------------------------
* @Versión       Fecha           Autor                         Descripción
* ------------------------------------------------------------------------------
* 1.0           04/01/2020     Sandra Ventura               Creación
* 1.1           11/06/2020     Selena Rodriguez              Restore methods for delete
*                                                            System debug
* ------------------------------------------------------------------------------
*/
@isTest
public class MX_RTE_UtilityEventos {

    @isTest static void testMethodUtilityTwo() {
        final Integer MAXIQUERY = Limits.getLimitQueries();
        final Integer NUMEQUERY = MAXIQUERY - 1;
        System.assert(Limits.getQueries() < NUMEQUERY, 'numero de querys dentro del rango  permitido');
    }

  /**
  * @description: Datos prueba lista Contactos
  * @author Sandra Ventura
  */
    public static List<Contact> createContactos(Integer num) {

        final List<Contact> cInvitado = new List<Contact>();
        final Id recTypeUser = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_RTE_Usuarios' AND sObjectType = 'Contact'].Id;

        for(Integer i=0;i<num;i++) {
            final Contact cInvRecurrente = new Contact(recordtypeid = recTypeUser,
                                                       FirstName='ContactTest'+ i,
                                                       LastName='LastNameTest'+ i,
                                                       Email = i+'test@test.com',
                                                       MX_WF_Recurrente_taskforce__c='Sí',
                                                       MX_RTE_Rol__c='Product Owner');

            cInvitado.add(cInvRecurrente);
        }
        insert cInvitado;
        return cInvitado;
}
  /**
  * @description: Datos prueba evento taskforce
  * @author Sandra Ventura
  */
    public static List<Event> createEvento(Integer num) {
        final gcal__GBL_Google_Calendar_Sync_Environment__c calEnviro = new gcal__GBL_Google_Calendar_Sync_Environment__c(Name = 'DEV');
        insert calEnviro;
        final datetime initime = datetime.newInstance(2020,1,1,8,30,0);
        final List<Event> cEvent = new List<Event>();
        final Id recTypeEvt = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_WF_Agenda_Taskforce' AND sObjectType = 'Event'].Id;
        final Event eventTF = new Event(recordtypeid = recTypeEvt,
                                        Subject='Otro',
                                        StartDateTime=initime,
                                        EndDateTime=initime.addHours(1)
                                        );
        cEvent.add(eventTF);
        insert cEvent;

        final Id IdTaskforce = [SELECT MX_WF_Taskforce__c FROM Event WHERE Id=: cEvent[0].Id].MX_WF_Taskforce__c;
        if (IdTaskforce == null) {
            final MX_WF_Agenda_Taskforce__c lportafolio = new MX_WF_Agenda_Taskforce__c(MX_F_Piso__c=19,
                                                                          MX_WF_Fecha__c=datetime.now(),
                                                                          MX_WF_Portafolios__c = 'Seguros',
                                                                          MX_WF_Ubicacion__c = 'Torre BBVA'
                                                                          );
            insert lportafolio;
            final MX_WF_Minuta_Taskforce__c lminuta = new MX_WF_Minuta_Taskforce__c(MX_WF_Taskforce__c=lportafolio.Id,
                                                                          MX_WF_Piso__c = 19,
                                                                          MX_WF_Lugar__c = 'Torre BBVA'
                                                                          );
            insert lminuta;
            final Event eventup = new Event(Id = cEvent[0].Id,
                                            MX_WF_Taskforce__c = lportafolio.Id
                                            );
           update eventup;
        }
        return cEvent;
}
  /**
  * @description: Datos prueba invitados taskforce
  * @author Sandra Ventura
  */
    public static List<MX_WF_Invitados_Taskforce__c> createInvitados(Id idtask, Id idinvit) {
           final List<MX_WF_Invitados_Taskforce__c> cInvit = new List<MX_WF_Invitados_Taskforce__c>();
           final MX_WF_Invitados_Taskforce__c invitf = new MX_WF_Invitados_Taskforce__c(MX_WF_Invitado__c = idinvit,
                                                                                        Taskforce__c=idtask);
           cInvit.add(invitf);
        insert cInvit;
        return cInvit;
   }
  /**
  * @description: Datos prueba Q actual
  * @author Sandra Ventura
  */
    public static List<MX_RTE_PI__c> createQs(date inicio, date fin) {
           final List<MX_RTE_PI__c> listqs = new List<MX_RTE_PI__c>();
           final MX_RTE_PI__c proglistaqs = new MX_RTE_PI__c(Name = '1Q20',
                                                             MX_RTE_Fecha_Inicio__c= inicio,
                                                             MX_RTE_Fecha_fin__c = fin);
           listqs.add(proglistaqs);
        insert listqs;
        return listqs;
    }
  /**
  * @description: Datos prueba iniciativa
  * @author Sandra Ventura
  */
    public static List<MX_RTE_Iniciativa__c> createIniciativa() {
        List<MX_RTE_PI__c> qactual = new List<MX_RTE_PI__c>();
            final date inicio2 = date.today();
            final date fin2 = date.today().addDays(30);
            qactual =MX_RTE_UtilityEventos.createQs(inicio2, fin2);

           final List<Contact> cprogram = new List<Contact>();
           final Id recTypeUser = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_RTE_Usuarios' AND sObjectType = 'Contact'].Id;

           final Contact program = new Contact(recordtypeid = recTypeUser,
                                               FirstName='ProgramTest',
                                               LastName='ManagerTest',
                                               Email = 'testpm@test.com',
                                               MX_RTE_Rol__c='Program Manager');

        cprogram.add(program);

         insert cprogram;

           final List<MX_RTE_Programas__c> cprograma = new List<MX_RTE_Programas__c>();
           final MX_RTE_Programas__c programa = new MX_RTE_Programas__c(Name = 'Test-Programa',
                                                                        MX_RTE_Program_Manager__c= cprogram[0].Id,
                                                                        MX_RTE_SDATOOL__c = 'SDATOOL-1234'
                                                                       );
           cprograma.add(programa);
           insert cprograma;

           final List<MX_RTE_Iniciativa__c> ciniciativa = new List<MX_RTE_Iniciativa__c>();

           final MX_RTE_Iniciativa__c iniciativa = new MX_RTE_Iniciativa__c(Name = 'Test-Iniciativa',
                                                                            MX_RTE_Programa_Global__c=cprograma[0].Id,
                                                                            MX_RTE_Product_Owner_Participante__c=UserInfo.getUserId(),
                                                                            MX_RTE_SDATOOL__c = 'SDATOOL-1234');
           ciniciativa.add(iniciativa);
        insert ciniciativa;

           final List<MX_RTE_Equipo__c  > cequipo = new List<MX_RTE_Equipo__c >();
           final MX_RTE_Equipo__c equipo = new MX_RTE_Equipo__c (MX_RTE_Nombre__c = cprogram[0].Id,
                                                                 MX_RTE_Iniciativa__c= ciniciativa[0].Id,
                                                                 MX_RTE_Q_de_Participacion__c = qactual[0].Id);
           cequipo.add(equipo);
           insert cequipo;

        return ciniciativa;
   }
  /**
  * @description: Datos prueba lista temas
  * @author Sandra Ventura
  */
    public static List<MX_WF_Tema__c> createTemas(Integer num, Id idportafolio) {

        final List<MX_WF_Tema__c> ctema = new List<MX_WF_Tema__c>();
        final Time initime = Time.newInstance(8, 30, 0, 0);
        for(Integer i=0;i<num;i++) {
            final MX_WF_Tema__c temalist = new MX_WF_Tema__c(Name = 'Tema '+i,
                                                             MX_WF_Tiempo_Minutos__c = 30,
                                                             MX_WF_Hora_inicio_Tema__c= initime.addMinutes(30),
                                                             MX_WF_Taskforce__c = idportafolio
                                                             );

            ctema.add(temalist);
        }
        insert ctema;
        return ctema;
}
  /**
  * @description: Datos prueba invitados taskforce
  * @author Sandra Ventura
  */
    public static List<MX_WF_Invitados_Taskforce__c> createInvitados(Id idtask) {

        final List<Contact> cInvitado = new List<Contact>();
        final Id recTypeUser = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_RTE_Usuarios' AND sObjectType = 'Contact'].Id;

        for(Integer i=0;i<5;i++) {
            final Contact cInvRecurrente = new Contact(recordtypeid = recTypeUser,
                                                       FirstName='ContactTest'+ i,
                                                       LastName='LastNameTest'+ i,
                                                       Email = 'test@test.com',
                                                       MX_WF_Recurrente_taskforce__c='Sí');

            cInvitado.add(cInvRecurrente);
        }
        insert cInvitado;

           final List<MX_WF_Invitados_Taskforce__c> cInvit = new List<MX_WF_Invitados_Taskforce__c>();
           final MX_WF_Invitados_Taskforce__c invitf = new MX_WF_Invitados_Taskforce__c(MX_WF_Invitado__c = cInvitado[0].Id,
                                                                                        Taskforce__c=idtask);
           cInvit.add(invitf);
        insert cInvit;
        return cInvit;
   }
}