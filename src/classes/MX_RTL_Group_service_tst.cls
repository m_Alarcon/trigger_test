/**
* Name: MX_SB_PC_AsignacionCola_tst
* @author Jose angel Guerrero
* Description : Clase Test para Controlador de componente Lightning de asignación de Colas
*
* Ver              Date            Author                   Description
* @version 1.0     Jul/13/2020     Jose angel Guerrero      Initial Version
*
**/
@isTest
public class MX_RTL_Group_service_tst {
   /**
* @description: funcion constructor
*/
    @isTest
    public static void serviceGrupoColas() {
        final list <Group> prefijo = MX_RTL_Group_service.serviceGrupoColas('');
        final list <Group> exito = new list <Group> ();
        test.startTest();
        System.assertEquals(exito, prefijo, 'SUCESS');
        test.stopTest();
    }

}