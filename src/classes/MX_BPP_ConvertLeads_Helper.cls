/**
 * @File Name          : MX_BPP_ConvertLeads_Helper.cls
 * @Description        :
 * @Author             : Jair Ignacio Gonzalez Gayosso
 * @Group              :
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 03/03/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      03/03/2020           Jair Ignacio Gonzalez G.   Initial Version
**/
public without sharing class MX_BPP_ConvertLeads_Helper {
    /**
     * @Method getLeadToConverd
     * @Description Singletons
    **/
    @TestVisible private MX_BPP_ConvertLeads_Helper() {
    }
    /**
    * @Method getLeadToConverd
    * @param Id idLead
    * @Description obtiene los Campaign members con informacion del Lead
    * @return List<CampaignMember>
    **/
    public static List<CampaignMember> getLeadToConverd(List<Id> idsLeads) {
        return MX_RTL_CampaignMember_Selector.getListCampignMembersByLeadIds(idsLeads);
    }

    /**
    * @Method createOpportunity
    * @param List<CampaignMember> lsCampMembers
    * @param Id bppypLead
    * @Description Crea una oportunidad por Lead
    * @return Map<Id, Opportunity>
    **/
    public static Map<Id, Opportunity> createOpportunity(List<CampaignMember>lsCampMembers, Id bppypLead) {
        final Map<Id, Opportunity> mapLeadOpp = new Map<Id, Opportunity>();
        final List<Opportunity> lsOpp = new List<Opportunity>();
        for (CampaignMember oCampMem : lsCampMembers) {
            if (oCampMem.Lead.RecordTypeId == bppypLead && oCampMem.Lead.isConverted == false) {
                lsOpp.add( new Opportunity(
                    RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('MX_BPP_RedBpyp').getRecordTypeId(),
                    Name = oCampMem.Lead.MX_WB_RCuenta__r.Name+' '+oCampMem.Campaign.MX_WB_Producto__r.Name,
                    AccountId = oCampMem.Lead.MX_WB_RCuenta__c,
                    Description = oCampMem.Lead.Description,
                    LeadSource = oCampMem.Lead.LeadSource,
                    op_amountPivote_dv__c = oCampMem.Lead.MX_LeadAmount__c,
                    CloseDate = oCampMem.MX_LeadEndDate__c,
                    MX_RTL_Producto__c = oCampMem.Campaign.MX_WB_Producto__r.Name,
                    StageName = 'En Gestión',
                    MX_RTL_BanderaOpp__c = true,
                    MX_RTL_Familia__c = oCampMem.Campaign.MX_WB_FamiliaProductos__r.Name,
                    NextStep = String.valueOf(oCampMem.LeadId)
                ));
            }
        }
        for (Opportunity oOpp : MX_RTL_Opportunity_Selector.insertOpportunity(lsOpp)) {
            mapLeadOpp.put(oOpp.NextStep, oOpp);
        }
        return mapLeadOpp;
    }

    /**
    * @Method convertLeads
    * @param List<CampaignMember> lsCampMembers
    * @param Id bppypLead
    * @param Map<Id,Opportunity> mapLeadOpp
    * @Description Convierte los Leads y regresa un Map<IdLead, IdOpportunity> o Map<IdLead, Error>
    * @return Map<String,String>
    **/
    public static Map<String,String> convertLeads(List<CampaignMember> lsCampMembers, Id bppypLead, Map<Id,Opportunity> mapLeadOpp) {
        final Map<String,String> convertResult = new Map<String,String>();
        final List<Database.LeadConvert> bulkLeadconvert = new List<Database.LeadConvert>();
        for (CampaignMember oCampMem : lsCampMembers) {
            if (oCampMem.Lead.RecordTypeId == bppypLead && oCampMem.Lead.isConverted == false) {
                final Database.LeadConvert Leadconvert = new Database.LeadConvert();
                Leadconvert.setLeadId(oCampMem.LeadId);
                Leadconvert.setConvertedStatus('Convertido');
                Leadconvert.setOpportunityId(mapLeadOpp.get(oCampMem.LeadId).Id);
                Leadconvert.setAccountId(oCampMem.Lead.MX_WB_RCuenta__c);
                bulkLeadconvert.add(Leadconvert);
            }
        }
        for (Database.LeadConvertResult oConv : MX_RTL_Lead_Selector.leadsConvert(bulkLeadconvert)) {
            if (oConv.isSuccess()) {
                convertResult.put(oConv.getLeadId(),oConv.getOpportunityId());
            } else {
                convertResult.put(oConv.getLeadId(),oConv.getErrors()[0].getMessage());
            }
        }
        return convertResult;
    }

    /**
    * @Method updateSubProducto
    * @param List<CampaignMember> lsCampMembers
    * @param Map<String,String> convertResult
    * @Description Actualiza los campos de subproducto del OpportunityLineItem
    * @return Map<String,String>
    **/
    public static void updateSubProducto(List<CampaignMember> lsCampMembers,  Map<String,String> convertResult) {
        final Map<Id, CampaignMember> mapOppLead = new Map<Id, CampaignMember>();
        for (CampaignMember oCampMem : lsCampMembers) {
            mapOppLead.put(convertResult.get(oCampMem.LeadId),oCampMem);
        }
        final List<OpportunityLineItem> lsOppLnItem = MX_RTL_OpportunityLineItem_Selector.getOppLineItembyOppId(convertResult.values());
        for (OpportunityLineItem oOppLnItem : lsOppLnItem) {
            oOppLnItem.MX_RTL_SubProducto__c = mapOppLead.get(oOppLnItem.OpportunityId).Lead.MX_SubProductoCampaing__c;
            oOppLnItem.MX_RTL_DetalleSubproducto__c = mapOppLead.get(oOppLnItem.OpportunityId).Lead.MX_DetalleSubProductoCampaing__c;
        }
        MX_RTL_OpportunityLineItem_Selector.updateOpportunityLineItem(lsOppLnItem);
    }
}