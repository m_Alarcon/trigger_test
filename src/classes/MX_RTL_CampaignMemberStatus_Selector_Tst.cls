/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RTL_CampaignMemberStatus_Selector_Tst
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-08-10
* @Description 	Test Class for MX_RTL_CampaignMemberStatus_Selector
* @Changes
*  
*/
@IsTest
public class MX_RTL_CampaignMemberStatus_Selector_Tst {
    /** String RecordTypeDeveloperName Campaign */
    static final String RTBPPCAMP = 'BPyP_Campaign';
    
    @TestSetup
    static void setup() {
        final Campaign newCampaign= new Campaign();
        newCampaign.Name = 'TestCampaignBPP';
        newCampaign.IsActive = true;
        newCampaign.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get(RTBPPCAMP).getRecordTypeId();
        insert newCampaign;
    }

    /**
    *@Description   Test Method for constructor
    *@author 		Edmundo Zacarias
    *@Date 			2020-08-10
    **/
    @isTest
    private static void testConstructor() {
        Test.startTest();
        final MX_RTL_CampaignMemberStatus_Selector selectorClass = new MX_RTL_CampaignMemberStatus_Selector();
        Test.stopTest();     
        
        System.assertNotEquals(null, selectorClass, 'Problema en constructor');
    }
    
    /**
    *@Description   Test Method for insertCampaignMemberStatus
    *@author 		Edmundo Zacarias
    *@Date 			2020-08-10
    **/
    @isTest
    private static void testInsertCampaignMemberStatus() {
        final Campaign baseCampaign = [SELECT Id FROM Campaign WHERE Name = 'TestCampaignBPP' AND RecordType.DeveloperName =:RTBPPCAMP LIMIT 1];
        final List<CampaignMemberStatus> campMSList = new List<CampaignMemberStatus>();
        final CampaignMemberStatus newCMS = new CampaignMemberStatus();
        newCMS.CampaignId = baseCampaign.Id;
        newCMS.Label = 'Test Status';
        campMSList.add(newCMS);
        
        Test.startTest();
        MX_RTL_CampaignMemberStatus_Selector.insertCampaignMemberStatus(campMSList);
        Test.stopTest();
        
        final List<CampaignMemberStatus> results = [SELECT Id FROM CampaignMemberStatus WHERE CampaignId = :baseCampaign.Id];
        System.assertNotEquals(null, results.size(), 'Problema en insert');
    }
}