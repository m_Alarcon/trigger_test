/**
* @File Name          : MX_RTL_Task_Selector.cls
* @Description        :
* @Author             : Juan Carlos Benitez
* @Group              :
* @Last Modified By   : Gerardo Mendoza Aguilar
* @Last Modified On   : 11-19-2020
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    5/26/2020   Juan Carlos Benitez         Initial Version
* 1.1    07/09/2020        Marco Cruz               Add method to insert Task
* 1.2    13/11/2020        Daniel Ramirez           Add method to Seelect last task
* 1.3	 17/02/2021		   Edmundo Zacarias			Add field to query on getTaskDataById
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public without sharing class MX_RTL_Task_Selector {
/** List of records to be returned */
final static List<SObject> RETURN_TASKS = new List<SObject>();



/**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param TaskFields
* @param recordId
* @return List<Task>
**/
public static List<Task> getTaskDataById(Set<Id> ids) {
RETURN_TASKS.addAll([SELECT Id, WhatId, ActivityDate, CreatedDate,LastModifiedBy.Name, MX_WB_telefonoUltimoContactoCTI__c, Subject, Description
                   FROM Task WHERE WhatId in :ids]);
return RETURN_TASKS;
}
/**
* @description 
* @author Daniel Ramirez Islas | 11-13-2020 
* @param queryFields 
* @param queryFilters 
* @return List<Task> 
**/
public static List<Task> getTaskLastId(String queryFields, String queryFilters) {
    return Database.query('SELECT ' + String.escapeSingleQuotes(queryFields) + ' FROM Task ' + (String.escapeSingleQuotes(queryFilters)).unescapeJava() + ' ORDER BY CreatedDate DESC LIMIT 1');
}

/**
* @description
* @author Juan Carlos Benitez | 06/05/2020
* @param Task Id
* @param taskType
* @return 
**/
public static void updateTask(Task taskPc, String taskType) {
switch on taskType {
    when 'PureCloud'{
			update taskPc;                
    }
    when 'Cita' {
    taskPc.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_CitaSiniestros').getRecordTypeId();
	taskPc.ActivityDate = taskPc.ReminderDateTime.date();
	upsert taskPc;
    }
    when 'Llamada' {
    	taskPc.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Llamada').getRecordTypeId();
        taskPc.Subject ='Intento de Venta';
     	upsert taskPc;
    }
}
}
    
    /**
    * @description: Insert a task list 
    * @author Marco Cruz | 07/09/2020
    * @param List<Task> listTask
    **/
    public static void insertTask (List<Task> listTask) {
        Insert listTask;
    }
    /**
    * @description: Upsert a task list 
    * @author Marco Cruz | 10/10/2020
    * @param List<Task> listTask
    **/
    public static void upsertTask (List<Task> listTask) {
        upsert listTask;
    }
    /**
    * @Description Return a list of tasks
    *@Date 2020-10-10
    *@param String taskFields: String with all the fields that you will return
    *@param Set<Id>: set with id that you will need to consult
    *@param String enforceSecurity: if you query need WITH SECURITY_ENFORCED or not.
    **/
    public static List<Task> getTasksById(String taskFields, Set<Id> recordId, String enforceSecurity) {
        switch on enforceSecurity {
            when 'true' {
                RETURN_TASKS.addAll(Database.query(String.escapeSingleQuotes('SELECT ' + taskFields + ' FROM Task WHERE Id in: recordId WITH SECURITY_ENFORCED')));
            }
            when 'false' {
                RETURN_TASKS.addAll(Database.query(String.escapeSingleQuotes('SELECT ' + taskFields + ' FROM Task WHERE Id in: recordId')));
            }
        }
        return RETURN_TASKS;
    }
}