/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpCostumerDocumentResp
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
public class  MX_SB_PS_wrpCostumerDocumentResp {
      /*Public property for wrapper*/
	public document document {get;set;}
	  /*Public property for wrapper*/
	public String href {get;set;}
      /*public constructor*/
       public MX_SB_PS_wrpCostumerDocumentResp() {
	   this.document = new document();
	   this.href = '';
	   }	  
	  /*Public subClass for wrapper*/
	public class  document {
	  /*Public property for wrapper*/
		public String data {get;set;}
      /*Public property for wrapper*/		
		public String size {get;set;}
      /*public constructor subclass*/
        public document() {
		 this.data = '';
		 this.size = '';
		}	  
	}
}