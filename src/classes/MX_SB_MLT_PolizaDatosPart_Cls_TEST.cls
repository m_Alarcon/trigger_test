@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_PolizaDatosPart_Cls_TEST
* Juan Carlos Benitez Herrera
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_PolizaDatosParticulares_Cls

* --------------------------------------------------------------------------------
* Versión       Fecha               Autor                     Descripción
* --------------------------------------------------------------------------------
* 1.0        12/02/2020    Juan Carlos Benitez Herrera         Creación
* 1.1        12/03/2020    Juan Carlos Benitez Herrera       Se cambian campos contrato SAC
* --------------------------------------------------------------------------------
*/

public with sharing class MX_SB_MLT_PolizaDatosPart_Cls_TEST {
  @testSetup
  static void setupTestData() {
        final String nameProfile = [SELECT Name from profile where name in ('Administrador del sistema','System Administrator') limit 1].Name;
        final User objUsrTst = MX_WB_TestData_cls.crearUsuario('RobertAdminTst', nameProfile); 
        insert objUsrTst;
        System.runAs(objUsrTst) {
        final String  tiporegistro = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
		final Account ctatest = new Account(recordtypeid=tiporegistro,firstname='Robert',lastname='PEREZ',Apellido_materno__pc='LOPEZ',RFC__c='PELR920912',PersonEmail='test@gmail.com');
        insert ctatest;
        final Contract contrato2 = new Contract(accountid=ctatest.Id,MX_WB_noPoliza__c='PolizaTesting',MX_SB_SAC_RFCAsegurado__c =ctatest.rfc__c,MX_WB_nombreAsegurado__c=ctatest.firstname,MX_WB_apellidoPaternoAsegurado__c=ctatest.lastname,MX_WB_apellidoMaternoAsegurado__c=ctatest.Apellido_materno__pc);
        insert contrato2;
        final Contract contrato = new Contract(accountid=ctatest.Id,MX_WB_noPoliza__c='PolizaTest',MX_SB_SAC_RFCAsegurado__c =ctatest.rfc__c,MX_WB_nombreAsegurado__c=ctatest.firstname,MX_WB_apellidoPaternoAsegurado__c=ctatest.lastname,MX_WB_apellidoMaternoAsegurado__c=ctatest.Apellido_materno__pc);
        insert contrato;
        final Contract contrato1 = new Contract(accountid=ctatest.Id,MX_WB_noPoliza__c='PolizaTest4',MX_SB_SAC_RFCAsegurado__c =ctatest.rfc__c,MX_WB_nombreAsegurado__c=ctatest.firstname,MX_WB_apellidoPaternoAsegurado__c=ctatest.lastname,MX_WB_apellidoMaternoAsegurado__c=ctatest.Apellido_materno__pc);
        insert contrato1;
        }
  }
    
       @IsTest static void obDatosContratoExito() {
        final Contract contrato = [SELECT id, MX_SB_MLT_ColorVehiculo__c,MX_SB_MLT_DescripcionVehiculo__c, MX_WB_Marca__c,MX_WB_Modelo__c,MX_WB_noMotor__c,MX_WB_noPoliza__c, MX_WB_numeroSerie__c,MX_WB_origen__c,MX_WB_placas__c,StartDate,MX_SB_SAC_ClaveSB__c FROM Contract where MX_WB_noPoliza__c='PolizaTest4' LIMIT 1];
        Test.startTest();
            contrato.MX_WB_noPoliza__c='PolizaTest4';
            MX_SB_MLT_PolizaDatosParticulares_Cls.getDatosContrato(contrato.MX_WB_noPoliza__c);
        Test.stopTest();
           System.assertEquals('PolizaTest4', contrato.MX_WB_noPoliza__c,'Exitosamente');
    }   
    @IsTest static void obDatosContratoFallo() {
        try {
            MX_SB_MLT_PolizaDatosParticulares_Cls.getDatosContrato('afgsdihujl');
        } catch(Exception e) {
            System.assert(e.getMessage().contains('JSON'),'Error JSON');
        }
    }
     @IsTest static Map<String,String> obTabEquipamentoFallo() {
  		final Map<String,String> datajson = new Map<String,String>();
        datajson.put('Descripcion','Aire Acondicionado');
        datajson.put('codigo','001');
        final Map<String,String> objeto =MX_SB_MLT_PolizaDatosParticulares_Cls.obTableEquipamento();
        System.assertEquals(objeto,datajson,'Exito!');
        return  datajson;
        }
    @IsTest static Map<String,String> obDatosParticularesExitoso() {
        final Map<String,String> datajson = new Map<String,String>();
        datajson.put('OrigenC', 'N');
        datajson.put('Origen', 'Nacional');
        datajson.put('Marca', 'VOLKSWAGEN');
        datajson.put('MarcaC', '165');
        datajson.put('SubMarcaC', '38');
        datajson.put('SubMarca', 'VENTO');
        datajson.put('TipoPropC', '3');
        datajson.put('TipoProp', 'PARTICULAR');
        datajson.put('VersionC', '11');
        datajson.put('Version', '1.6L COMFORTLINE');
        datajson.put('TipoValorC', 'FACTURA');
        datajson.put('ValorN', '100000');
        datajson.put('TipoVehiculoC', 'A');
        datajson.put('TipoVehiculo', 'AUTOMOVIL');
        datajson.put('TipoTransmisionC', '8');
        datajson.put('TipoTransmision', 'ESTANDAR');
        datajson.put('TipoCargaC', 'A');
        datajson.put('TipoCarga', 'REDUCIDA PELIGROSIDAD');
        datajson.put('CarroceriaC', '19');
        datajson.put('Carroceria', 'SEDAN');
        datajson.put('TipoRegVeh', 'NACIONAL NACIONAL');
        datajson.put('TipoRegVehC', '1');
        datajson.put('RemolqueC', '0');
        datajson.put('Remolque', 'NO');
        datajson.put('ZonaCirC', '15');
        datajson.put('ZonaCir', 'ESTADO DE MEXICO');
        datajson.put('NumPuertas', '4');
        datajson.put('NumOcupantes', '5');
        datajson.put('NumCilindros', '4');
        datajson.put('Modelo', '2018');
        datajson.put('ColorC', 'NES');
        datajson.put('Color', 'ESPECIFICADO');
        datajson.put('noSerie', 'MEX5H2604JT037156');
        datajson.put('SerialMotor', 'CLS580785');
        datajson.put('Placas', 'MSN-2389');
        datajson.put('TipoServC', 'PAR');
        datajson.put('TipoServC', 'PARTICULAR');
		datajson.put('TipoUsoC', '6');
        datajson.put('TipoUso', 'PARTICULAR');
        final Map<String,String> objetos=MX_SB_MLT_PolizaDatosParticulares_Cls.obDatosParticular(); 
        system.assertEquals(datajson,objetos ,'Exito');
        return datajson;
    }
    @IsTest static Map<String,String> obDatosParticularesExito() {
     final Map<String,String> datajson = new Map<String,String>();
        datajson.put('Poliza1', '2002');
        datajson.put('Poliza2', '8B7CA0009T');
        datajson.put('Certificado', '1');
        datajson.put('Cliente', 'GRUPO TIGRE SEGURIDAD PRIVADA SA DE CV');
        datajson.put('SegClasif', 'BBVA');
        datajson.put('ClaveSb', 'VEN020');
        datajson.put('Descripcion', 'VOLKSWAGEN VENTO 1.6L COMFORTLINE AA EE CD BA ESTANDARD SEDAN');
        datajson.put('FechaInicio', '43069');
        final Map<String,String> objetos=MX_SB_MLT_PolizaDatosParticulares_Cls.obDatosParticulares();
        system.assertEquals(datajson,objetos ,'Exito');
        return  datajson;
    }
}