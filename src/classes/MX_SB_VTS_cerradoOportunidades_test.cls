/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Julio Medellin Oliva
*   @Proyecto:              Bbva
*   @Descripción:           test de schedule job class 
*
*******************************************************************************/
@isTest
private class MX_SB_VTS_cerradoOportunidades_test {

    /** variable de uso final*/ 
    private final static String CRON_EXP = '0 0 0 15 3 ? *';
    
    //clase de prueba escenario 1
    static testmethod void testScheduledJob() {
        // variable para crear usuario 
        final user TUSR = MX_WB_TestData_cls.crearUsuario ( 'TestLastName', System.Label.MX_SB_VTS_ProfileAdmin);
        insert TUSR;
        // variable para crear cuenta        
	    final Account Acct = MX_WB_TestData_cls.crearCuenta('cuentaTest','PersonAccount');     
        Insert Acct;
         // variable para crear oportunidad 
		final Opportunity opp = MX_WB_TestData_cls.crearOportunidad('oppTest',Acct.Id,TUSR.ID,'MX_SB_VTA_VentaAsistida');
        opp.MX_SB_VTS_ContadorRemarcado__c=1; 
        insert opp;
        
        Test.setCreatedDate(opp.id, DateTime.newInstance(2012,12,12)); 
        Test.startTest();

        final String jobId = System.schedule('ScheduledApexTest', CRON_EXP, new MX_SB_VTS_cerradoOportunidades());    
        test.stopTest();
        System.assertNotEquals('',jobid);
    }
}