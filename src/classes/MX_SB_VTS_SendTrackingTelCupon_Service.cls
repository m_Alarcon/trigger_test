/**
 * @File Name          : MX_SB_VTS_SendTrackingTelCupon_Service.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernandez Cuamatzi
 * @Last Modified On   : 3/6/2020 19:44:01
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    28/5/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_SendTrackingTelCupon_Service {
    /**variable formato tiempo*/
    public final static String FORMATTIME = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
    /**Constructor */
    private MX_SB_VTS_SendTrackingTelCupon_Service() {}
    
    /**
    * @description Actualiza query para Batch
    * @author Eduardo Hernandez Cuamatzi | 3/6/2020 
    * @param String queryStr Query original
    * @param Integer totalHours Horas totales hacia atras para evaluar Opps
    * @return String Query modificada
    **/
    public static String updateQuery(String queryStr, Integer totalHours) {
        String finalQuery = queryStr;
        final Datetime timeSend = Datetime.now();
        final Integer hoursTimeCalls = totalHours*60;
        final String tNowCupon = MX_SB_VTS_SendTrackingTelemarketing_cls.addMinutesTime(timeSend, FORMATTIME, -(hoursTimeCalls));
        finalQuery += ' And (CreatedDate <= ' + tNowCupon+ ')';
        return finalQuery;
    }

    /**
    * @description Recupera familia de proveedores activos
    * @author Eduardo Hernandez Cuamatzi | 3/6/2020 
    * @param Map<Id Opportunity> mapOpps 
    * @return Map<Id, MX_SB_VTS_FamliaProveedores__c> 
    **/
    public static Map<Id,MX_SB_VTS_FamliaProveedores__c> findProveed() {
        final Set<String> productsName = new Set<String>{System.Label.MX_SB_VTS_Hogar, System.Label.MX_SB_VTS_VIDA};
        final Set<String> processSac = new Set<String>{'VTS'};
        final Set<Id> familyProducts = setFamilyIds(productsName, processSac);
        return new Map<Id,MX_SB_VTS_FamliaProveedores__c>(MX_SB_VTS_FamliaProveedores_Selector.findFamProvBySetPro(familyProducts));
    }

    /**
    * @description Recupera Ids de Familia de productos por proceso de venta
    * @author Eduardo Hernandez Cuamatzi | 3/6/2020 
    * @param Set<String> productsName Nombres de productos de interes
    * @param Set<String> processSac Filtro de proceso
    * @return Set<Id> Ids de familia de productos
    **/
    public static Set<Id> setFamilyIds(Set<String> productsName, Set<String> processSac) {
        final Set<Id> familyProducts = new Set<Id>();
        for(Product2 productCode : MX_RTL_Product2_Selector.findProsByNameProces(productsName, processSac, true)) {
            familyProducts.add(productCode.MX_WB_FamiliaProductos__c);
        }
        return familyProducts;
    }

    /**
     * findMapSend recupera mapa de registros
     * @param  scopeQuotes             registros a trabajar
     * @param  mapProvers              mapa de proveedores
     * @param  mapEmailNoValCtz emails a excluir
     * @return                         lista de registros a enviar
     */
    public static List<Opportunity> validOppsSend(List<Opportunity> scopeOpps, Map<String, EmailNoValidosCotiza__c> mapEmailNoValCtz) {
        final List<Opportunity> mapSend = new List<Opportunity>();
        for (Opportunity oppSend : scopeOpps) {
            if (mapEmailNoValCtz.containsKey(oppSend.Account.PersonEmail) == false) {
                mapSend.add(oppSend);
            }
        }
        return mapSend;
    }
    
    /**
    * @description Procesa envio de oportunidades para los proveedores
    * @author Eduardo Hernandez Cuamatzi | 3/6/2020 
    * @param Map<Id MX_SB_VTS_FamliaProveedores__c> mapProveedors Familia de proveedores disponibles
    * @param List<Opportunity> validOpps Lista de Oportunidades a enviar
    * @return void Ejecuta proceso
    **/
    public static void processOppsSend(Map<Id,MX_SB_VTS_FamliaProveedores__c> mapProveedors, List<Opportunity> validOpps) {
        final List<Opportunity> updateOpps = new List<Opportunity>();
        final Set<String> lstProvee = new Set<String>();
        for (MX_SB_VTS_FamliaProveedores__c proveer : mapProveedors.values()) {
            lstProvee.add(proveer.MX_SB_VTS_Identificador_Proveedor__c);
        }

        for(String strProve : lstProvee) {
            switch on strProve.toUpperCase() {
                when 'SMART CENTER' {
                    final Map<String, MX_SB_VTS_Lead_tray__c> mapLeadsTry = MX_SB_VTS_SendTrackingTelCupon_Helper.fillMapTrays();
                    updateOpps.addAll(sendOpps(strProve, mapProveedors, mapLeadsTry, validOpps));
                }
            }
        }
        final Database.SaveResult[] srList = MX_RTL_Opportunity_Selector.updateResult(updateOpps, false);
        processSaveResult(srList);
    }

    /**
    * @description Realiza el envio al proveedor Smart center
    * @author Eduardo Hernandez Cuamatzi | 3/6/2020 
    * @param String proveedor Proveedor de call center
    * @param Map<Id MX_SB_VTS_FamliaProveedores__c> mapProveedors Proveedores activos
    * @param Map<String MX_SB_VTS_Lead_tray__c> mapLeadsTry Bandeja de leads para el proveedor
    * @param List<Opportunity> validOpps Lista de Oportunidades a enviar
    * @return List<Opportunity> Lista de Oportunidades enviadas al proveedor
    **/
    public static List<Opportunity> sendOpps(String proveedor, Map<Id,MX_SB_VTS_FamliaProveedores__c> mapProveedors, Map<String, MX_SB_VTS_Lead_tray__c> mapLeadsTry, List<Opportunity> validOpps) {
        final Map<String,Object> sendOppsSmart = MX_SB_VTS_SendTrackingTelCupon_Helper.sendOppsSmart(proveedor, mapProveedors, mapLeadsTry, validOpps);
        return (List<Opportunity>)MX_SB_VTS_SendTrackingTelCupon_Helper.processResponseOpps(sendOppsSmart, mapLeadsTry, validOpps, proveedor);
    }

    /**
    * @description Procesa resultados dmlException
    * @author Eduardo Hernandez Cuamatzi | 3/6/2020 
    * @param List<Database.SaveResult> lstResults Resultado de operación DML
    * @return void Ejecuta validación
    **/
    public static void processSaveResult(List<Database.SaveResult> lstResults) {
        for (Database.SaveResult sr : lstResults) {
            if (sr.isSuccess() == false) {
                for(Database.Error err : sr.getErrors()) {
                    throw new DMLException(err.getStatusCode() + ': ' + err.getMessage());
                }
            }
        }
    }

    /**
    * @description Procesa resultados servicio ASO
    * @author Alexandro Corzo | 17/09/2020 
    * @param List<Opportunity> lstResultData Resultado de consumo de serivio ASO
    * @return Map<String, Object> Devuelve: Listas procesadas Contract y Opportunity
    **/
    public static Map<String, Object> processQuotesASO(List<Opportunity> lstOpportunity) {
    	final Map<String, Object> lstResultData = new Map<String, Object>();
        final List<Contract> lstContract = new List<Contract>();
        final List<Opportunity> lstOppoChanges = new List<Opportunity>();
        for(Opportunity oRowOppo : lstOpportunity) {
            if(String.isBlank(oRowOppo.FolioCotizacion__c)) {
                final Set<String> strProdNames = new Set<String>{oRowOppo.Producto__c};
                final set<String> strProcNames = new Set<String>{'VTS'};
                final List<Product2> lstProduct = MX_RTL_Product2_Selector.findProsByNameProces(strProdNames, strProcNames, true);
                final Product2 objProduct = lstProduct[0];
            	final Contract objContract = new Contract();
                objContract.AccountId = oRowOppo.AccountId;
            	objContract.MX_WB_Oportunidad__c = oRowOppo.Id;
            	objContract.MX_WB_Producto__c = objProduct.Id;
            	objContract.MX_SB_SAC_NumeroPoliza__c = '1234567890';
            	objContract.Status = 'Draft';
                lstContract.add(objContract);
                oRowOppo.StageName = 'Closed Won';
                oRowOppo.MX_SB_VTS_Aplica_Cierre__c = true;
                lstOppoChanges.add(oRowOppo);
                lstResultData.put('lstContracts', lstContract);
                lstResultData.put('lstOpportunity', lstOppoChanges);
            } else {
                oRowOppo.EnviarCTI__c = true;
                lstOppoChanges.add(oRowOppo);
                lstResultData.put('lstContracts', lstContract);
                lstResultData.put('lstOpportunity', lstOppoChanges);
            }
        }
        return lstResultData;
    }
    
    /**
    * @description Procesa resultados generados ASO
    * @author Alexandro Corzo | 17/09/2020 
    * @param Map<String, Object> objDataProcess Mapa con listas de objetos procesados
    * @return void Ejecuta update de resultados
    **/
    public static void processUpdateResult(Map<String, Object> objDataProcess) {        
        final List<Contract> lstContract = (List<Contract>) objDataProcess.get('lstContracts');
        final List<Opportunity> lstOpportunity = (List<Opportunity>) objDataProcess.get('lstOpportunity');
        if(!lstContract.isEmpty()) {
            MX_RTL_Contract_Selector.insertContracts(lstContract);
        }
        if(!lstOpportunity.isEmpty()) {
            MX_RTL_Opportunity_Selector.updateResult(lstOpportunity, false);
        }
    }
}