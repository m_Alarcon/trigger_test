/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_COB_CargaArchivoController_tst
* Autor Angel Nava
* Proyecto: Cobranza - BBVA Bancomer
* Descripción : test para MX_SB_COB_CargaArchivoController_cls
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Desripción
* --------------------------------------------------------------------------------
* 1.0           01/05/2019     Angel Nava				          Creación
* --------------------------------------------------------------------------------
*/
@isTest
private class MX_SB_COB_CargaArchivoController_tst {
/** set de carga  */
    @TestSetup
    static void setMethod() {
                
        final UserRole fun = [select developername,id from UserRole where developerName='MX_SB_COB_FCoord' limit 1];
        
        final Profile userPrf         = [select id from profile where name in ('Administrador del sistema','System Administrator') limit 1];
        
        final User    userCrt         = new User(emailencodingkey     = 'UTF-8',
                                            languagelocalekey   = 'en_US',
                                            localesidkey        = 'en_US',
                                            timezonesidkey      = 'America/Los_Angeles',
                                            alias               = 'Tstr', 
                                            lastname            = 'Monkey',
                                            email               = 'luffy@testorg.com',
                                            profileid           = userPrf.Id, 
                                            username            = 'luffy@testorg.com',
        									userRoleId			= fun.id);
        insert userCrt;
    }
	/**  carga de clase  */
	@isTest
    static void cargaClase() {
        final User usu = [select id from user where email= 'luffy@testorg.com' limit 1 ];
        final Bloque__c bloque = new Bloque__c(MX_SB_COB_BloqueActivo__c = True);
        final List<bloque__c> bloques= new List<bloque__c>();
        bloques.add(bloque);
        final Database.SaveResult[] ramoIns = Database.insert(bloques,true);
        String idBloque = '';
        for (Database.SaveResult objDtUrsNvo : ramoIns) {
                if (objDtUrsNvo.isSuccess()) {
                    	idBloque = objDtUrsNvo.getId();
                }
		}
        system.runAs(usu) {
        final Bloque__c bloque2 = [select id,name from Bloque__c where id =:idBloque limit 1];
        test.startTest();
        final CargaArchivo__c cargaIteracion = new CargaArchivo__c(MX_SB_COB_bloque__c= bloque2.name,MX_SB_COB_ContadorConError__c=0,MX_SB_COB_ContadorSinError__c=3000,MX_SB_COB_Resultado__c='Registro log');

        insert cargaIteracion;

        final CargaArchivo__c carga = MX_SB_COB_CargaArchivoController_cls.getCargaArchivo(bloque2.id);

       final List<CargaArchivo__c> comparacion = [select id,MX_SB_COB_bloque__c from CargaArchivo__c where id=:carga.id limit 1];

       system.assertEquals(comparacion[0].MX_SB_COB_bloque__c, bloque2.name,'la entrada y salida no son iguales');
        }
       test.stopTest();
    }
/** prueba de carga con error  */
    @isTest
    static void errorCarga() {
        final Bloque__c bloque0 = new Bloque__c(MX_SB_COB_BloqueActivo__c = True);
        final List<bloque__c> bloques= new List<bloque__c>();
        bloques.add(bloque0);
        insert bloques;
        test.startTest();
        try {
             final CargaArchivo__c carga = MX_SB_COB_CargaArchivoController_cls.getCargaArchivo('idfalso');
            system.assertEquals(null, carga, 'Se espera nulo');
        } catch(exception e) {
            system.assertNotEquals(null, e.getMessage(),'Se esperaba el error');
        }
       test.stopTest();
    }
    /** prueba de carga supervisor  */
	@isTest
    static void cargaClaseSupervisor() {
        
        final UserRole fun = [select developername,id from UserRole where developerName='MX_SB_COB_FSupervisor' limit 1];
        
        final User usu = [select id,userRoleId from user where email= 'luffy@testorg.com' limit 1 ];
        usu.userRoleId = fun.id;
        update usu;
        system.runAs(usu) {    
        
        final String bloqueId = MX_SB_COB_CreaObjetos_cls.creaBloque();
        final Bloque__c bloque2 = [select id,name from Bloque__c where id =:bloqueId limit 1];
        
        
        test.startTest();
        final CargaArchivo__c cargaIteracion = new CargaArchivo__c(MX_SB_COB_bloque__c= bloque2.Name,MX_SB_COB_ContadorConError__c=0,MX_SB_COB_ContadorSinError__c=3000,MX_SB_COB_Resultado__c='Registro log');
        
        insert cargaIteracion;
            
        final CargaArchivo__c carga = MX_SB_COB_CargaArchivoController_cls.getCargaArchivo(bloqueId);
            
       final List<CargaArchivo__c> comparacion = [select id,MX_SB_COB_bloque__c from CargaArchivo__c where id=:carga.id limit 1];
        test.stopTest();
      system.assertEquals(comparacion[0].MX_SB_COB_bloque__c, bloque2.name,'la entrada y salida no son iguales');
        }
       
    }
}