/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase con el funcionamiendo del módulo de asiganción de prospectos

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     04/02/2020      Roberto Isaac Soto Granados           Creación Clase
*****************************************************************************************/
public with sharing class BPyP_tratamientoProspectosCtrl {
    /*Variable con el id del usuario en sesión*/
    private static final string USERID = System.UserInfo.getUserId();

    /*Contructor clase BPyP_tratamientoProspectosCtrl*/
    private BPyP_tratamientoProspectosCtrl() {}

    /*Método que consulta los prospectos del usuario en sesión*/
    @AuraEnabled
    public static List<Account> getAccounts() {
        return [SELECT Id, Name, BPyP_at_Comentarios__c, BPyP_ls_Estado_del_candidato__c, PersonEmail, Origen_del_candidato__c, Oficina_Gestora__c, BPyP_dv_Monto_de_inversi_n__c, Tipo_de_Registro_Manual__c FROM Account WHERE OwnerId =: USERID AND RecordType.DeveloperName = 'MX_BPP_PersonAcc_NoClient' AND BPyP_ls_Estado_del_candidato__c = 'Abierto'];
    }

    /*Método que consulta los usuarios subordinados del director*/
    @AuraEnabled
    public static List<User> searchUser(String userName) {
        return [SELECT Id, Name FROM User WHERE Name LIKE: userName AND Director_de_oficina__c =: USERID];
    }

    /*Método que realiza la asignación de los prospectos selecionados a los nuevos propietarios*/
    @AuraEnabled
    public static void saveNewOwner(String newOwnerId, List<String> accIds) {
		final List<Account> updatedAcc = new List<Account>();
        for(Account acc : [SELECT Id, OwnerId FROM Account WHERE Id IN: accIds]) {
            acc.OwnerId = newOwnerId;
            updatedAcc.add(acc);
        }
        update updatedAcc;
    }

    /*Método que realiza el descarte de los prospectos selecionados*/
    @AuraEnabled
    public static void discardAccounts(List<String> accIds) {
		final List<Account> updatedAcc = new List<Account>();
        for(Account acc : [SELECT Id, OwnerId FROM Account WHERE Id IN: accIds]) {
            acc.BPyP_ls_Estado_del_candidato__c = 'Descartado';
            acc.BPyP_ls_Motivo__c = 'Otros factores de descarte';
            updatedAcc.add(acc);
        }
        update updatedAcc;
    }
}