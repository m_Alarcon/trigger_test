/**
 * @File Name          : MX_SB_VTS_leadMultiServiceVts_Service.cls
 * @Description        : 
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 09-03-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/6/2020   Eduardo Hernández Cuamatzi     Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_leadMultiServiceVts_Service {    
    /**Origen call me back plus*/
    final private static String CALLMEBACKPR = 'Call me back';
    /**Origen call me back plus */
    final private static String CALLMEBACKPRPLUS = 'Call me back plus';
    /**Texto para valor de telefono*/
    final private static String PERSONPHONE = 'PER-PHONE-1';
    /**Lista de reglas de asignación */
    final static Set<String> LSTASSIGNAME = new Set<String>{'ASIGNACION DE COLAS LEADS'};
    /**Regla de asignación */
    final static List<AssignmentRule> ASSIGNMENTRULE = MX_RTL_AssignmentRule_Selector.selectAssigmentName(LSTASSIGNAME, true, 'Lead');

    /**
    * @description Parseo de json de entrada
    * @author Eduardo Hernández Cuamatzi | 13/6/2020 
    * @param listener Valores de request
    * @return MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta Wrapper parseado
    **/
    public static MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta fillDataServs(Map<String, Object> listener) {
        final MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta dataWrapp = new MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta();
        dataWrapp.type = String.valueOf(listener.get('type'));
        dataWrapp.channel = String.valueOf(listener.get('channel'));
        dataWrapp.coupon = String.valueOf(listener.get('coupon'));
        final String products = JSON.serialize(listener.get('product'));
        final String quote = JSON.serialize(listener.get('quote'));
        final String person = JSON.serialize(listener.get('person'));
        dataWrapp.product = new MX_SB_VTS_leadMultiServWrapper.Mx_sb_product();
        dataWrapp.product = (MX_SB_VTS_leadMultiServWrapper.Mx_sb_product)JSON.deserializeStrict(products, MX_SB_VTS_leadMultiServWrapper.Mx_sb_product.class);
        dataWrapp.quote = new MX_SB_VTS_leadMultiServWrapper.Mx_sb_quote();
        dataWrapp.quote = (MX_SB_VTS_leadMultiServWrapper.Mx_sb_quote)JSON.deserializeStrict(quote, MX_SB_VTS_leadMultiServWrapper.Mx_sb_quote.class);
        dataWrapp.person = new MX_SB_VTS_leadMultiServWrapper.Mx_sb_person();
        dataWrapp.person = (MX_SB_VTS_leadMultiServWrapper.Mx_sb_person)JSON.deserializeStrict(person, MX_SB_VTS_leadMultiServWrapper.Mx_sb_person.class);
        final String appoint = JSON.serialize(listener.get('appointment'));
        final Map<String, Object> listener2 = (Map<String, Object>)JSON.deserializeUntyped(appoint);
        if(listener2 != null) {
            dataWrapp.appointment = new MX_SB_VTS_leadMultiServWrapper.Mx_sb_appointment();
            dataWrapp.appointment.timex = String.valueof(listener2.get('time'));
        }
        return dataWrapp;
    }
    
    /**
    * @description Valida TIPO de entrada en la petición
    * @author Eduardo Hernández Cuamatzi | 13/6/2020 
    * @param seDataimp datos del servicio parseado
    * @return boolean Retorna True si le type en la petición es valido
    **/
    public static boolean validatemethod(MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta seDataimp) {
        Boolean ret = false;
        if(MX_SB_VTS_leadMultiServiceVts_Helper.validameth(seDataimp.type) && MX_SB_VTS_leadMultiServiceVts_Helper.validameth(seDataimp.channel) && 
        MX_SB_VTS_leadMultiServiceVts_Helper.validameth(seDataimp.person.name) && validaContadetail(seDataimp.person.contactDetails)) {
            ret= true;
        }
        return ret;
    }    

    /**
    * @description Valida los datos de contacto
    * @author Eduardo Hernández Cuamatzi | 13/6/2020 
    * @param condeta detalle de contactos
    * @return boolean True si los valores de contacto son correctos
    **/
    private static boolean validaContadetail(List<MX_SB_VTS_leadMultiServWrapper.Mx_sb_Contacdetails> condeta) {
        Boolean ret = false;
        for(MX_SB_VTS_leadMultiServWrapper.Mx_sb_Contacdetails contq: condeta) {
            if(PERSONPHONE.Equals(contq.key)) {
                ret = true;
            }
        }
        return ret;
    }

    /**
    * @description Procesa datos de la peticón segun el type de origen
    * @author Eduardo Hernández Cuamatzi | 13/6/2020 
    * @param consulta Datos wrapper de la petición
    * @return String Id del registro resultante
    **/
    public static String processPost(MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta) {
        String response  = '';
        final String originType = consulta.type.toUpperCase();
        switch on originType {
            when  'CALLMEBACK' {
                if(MX_SB_VTS_leadMultiServiceVts_HelpLL.validCMBTWCLeads(consulta)) {
                    response = processOrigin(consulta, CALLMEBACKPRPLUS);
                } else {
                    response = processOrigin(consulta, CALLMEBACKPR);
                }
            }
            when 'CALLMEBACKPR' {
                response = processOrigin(consulta, CALLMEBACKPR);
            }
            when 'FORM' {
                response = processOrigin(consulta, CALLMEBACKPR);
            }
            when 'TRACKING' {
                response = processOriginTW(consulta, CALLMEBACKPR);
            }
            when 'RETARGETING' {
                response = processOrigin(consulta, CALLMEBACKPR);
            }
        }
        return response;
    }

    /**
    * @description Procesa origen de la petición
    * @author Eduardo Hernández Cuamatzi | 08-09-2020 
    * @param consulta json de entrada
    * @param origin origen a evaluar
    * @return string origen resultante
    **/
    private static string processOrigin(MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta, String origin) {
        String response  = '';
        final String productName = MX_SB_VTS_leadMultiServiceVts_Helper.findProduct(consulta);
        final String productUpC = productName.toUpperCase();
        switch on productUpC {
            when  'SEGURO ESTUDIA' {
                response = creaLead(consulta, productName, origin);
            }
            when 'HOGAR SEGURO DINÁMICO', 'VIDA DINÁMICO' {
                response = creaLead(consulta, productName, origin);
            }
            when else {
                response = creaLeadWPro(consulta, 'SAC');
            }
        }
        return response;
    }

        /**
    * @description Procesa origen de la petición
    * @author Eduardo Hernández Cuamatzi | 08-09-2020 
    * @param consulta json de entrada
    * @param origin origen a evaluar
    * @return string origen resultante
    **/
    private static string processOriginTW(MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta, String origin) {
        String response  = '';
        final String productName = MX_SB_VTS_leadMultiServiceVts_Helper.findProduct(consulta);
        final String productUpC = productName.toUpperCase();
        switch on productUpC {
            when 'HOGAR SEGURO DINÁMICO', 'VIDA DINÁMICO' {
                response = MX_SB_VTS_leadMultiServiceVts_Helper.processOriginTW(consulta, origin, productName);
            }
        }
        return response;
    }    

    /**
    * @description Inserta un nuevo Lead con los datos de la petición
    * @author Eduardo Hernández Cuamatzi | 13/6/2020 
    * @param consulta Datos parseados del json 
    * @return String Id del Lead creado
    **/
    private static String creaLead (MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta, String productName, String origin) {
        String leadId = '';
        if(String.isNotBlank(productName) && MX_SB_VTS_leadMultiServiceVts_Helper.validAppoit(consulta)) {
            final List<Lead> lstInLeads = new List<Lead>();
            final Lead leadOr = new Lead();
            leadOr.FirstName = consulta.person.Name;
            leadOr.lastName = MX_SB_VTS_leadMultiServiceVts_Helper.validLastName(consulta);
            for(MX_SB_VTS_leadMultiServWrapper.Mx_sb_Contacdetails temo:consulta.person.contactDetails) {
                leadOr.Email=temo.key=='PER-EMAIL'?temo.value:leadOr.Email;
                leadOr.Phone=temo.key==PERSONPHONE?temo.value:leadOr.Phone;
                leadOr.MobilePhone=temo.key==PERSONPHONE?temo.value:leadOr.MobilePhone;
            }
            leadOr.LeadSource=origin;
            leadOr.MX_SB_VTS_CodCampaFace__c = consulta.coupon;
            leadOr.Folio_Cotizacion__c = MX_SB_VTS_leadMultiServiceVts_Helper.validateQuoteId(consulta);
            leadOr.RecordTypeId = MX_SB_VTS_leadMultiServiceVts_Helper.validRecType(productName,true);
            leadOr.Status = 'Contacto';
            leadOr.Producto_Interes__c = productName;
            leadOr.MX_SB_VTS_CanalAPPLead__c = consulta.channel;
            final Lead leadOrOpt = getDateTimeValue(consulta, leadOr);
            final Database.DMLOptions dmlOpts = new Database.DMLOptions();
            dmlOpts.assignmentRuleHeader.assignmentRuleId= ASSIGNMENTRULE[0].Id;
            leadOrOpt.setOptions(dmlOpts);
            lstInLeads.add(leadOrOpt);
            final List<Database.SaveResult> lstResults = MX_RTL_Lead_Selector.insertLeads(lstInLeads, true);
            leadId = MX_SB_VTS_leadMultiServiceVts_Helper.processUniqueLead(lstResults);
        }
        return leadId;
    }

    /**
    * @description Inserta un nuevo Lead con los datos de la petición
    * @author Eduardo Hernandez Cuamatzi | 08-19-2020 
    * @param MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta 
    * @param String origin 
    * @return String Id resultante
    **/
    private static String creaLeadWPro(MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta, String origin) {
        String leadId = '';
        final List<Lead> lstInLead = new List<Lead>();
        final Lead leadHome = new Lead();
        leadHome.FirstName=consulta.person.Name;
        leadHome.lastName = MX_SB_VTS_leadMultiServiceVts_Helper.validLastName(consulta);
        for(MX_SB_VTS_leadMultiServWrapper.Mx_sb_Contacdetails contact : consulta.person.contactDetails) {
            leadHome.Email=contact.key=='PER-EMAIL'?contact.value:leadHome.Email;
            leadHome.Phone=contact.key==PERSONPHONE?contact.value:leadHome.Phone;
            leadHome.MobilePhone=contact.key==PERSONPHONE?contact.value:leadHome.MobilePhone;
        }
        leadHome.LeadSource=origin;
        leadHome.Status = 'Contacto';
        leadHome.Producto_Interes__c = '';
        final Database.DMLOptions dmlLead = new Database.DMLOptions();
        dmlLead.assignmentRuleHeader.assignmentRuleId= ASSIGNMENTRULE[0].Id;
        leadHome.setOptions(dmlLead);
        lstInLead.add(leadHome);
        final List<Database.SaveResult> lstResults = MX_RTL_Lead_Selector.insertLeads(lstInLead, true);
        leadId = (String)MX_SB_VTS_leadMultiServiceVts_Helper.processUniqueLead(lstResults);
        return leadId;
    }

    /**
    * @description Obtiene fecha / hora del elemento Appointent
    * @author Diego Olvera | 17-09-2020 
    * @param String timex: Fecha del elemento Appoitment
    * @return Map<String, String> Retorna un mapa con la fecha / hora del Appoiment
    **/
    public static Lead getDateTimeValue(MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta, Lead objLead) {
        if(MX_SB_VTS_leadMultiServiceVts_HelpLL.validateAppTime(consulta)) {
            final String timex = consulta.appointment.timex;
            final String [] sDateTime = timex.replace('Z','').split('T');
            final String dtFecha = sDateTime[0];
            final String dhHora = sDateTime[1].substring(0,5);
            final DateTime timeZonenow = DateTime.now();
            final Long offset = DateTime.newInstance(timeZonenow.date(), timeZonenow.time()).getTime()
                - DateTime.newInstance(timeZonenow.dateGmt(), timeZonenow.timeGmt()).getTime();
            final List<String> setHours = dhHora.split(':');
            final Time timeTHour = Time.newInstance(Integer.valueOf(setHours[0]), Integer.valueOf(setHours[1]), 0, 0);
            final Integer offsetPosit = Integer.valueOf(offset * -1);
            final Time timeCorrectTZ = timeTHour.addMilliseconds(offsetPosit);
            final String horaTZ = String.valueOf(timeCorrectTZ);
            final String strZoneTZ = horaTZ.substring(0, 5);
            objLead.MX_SB_VTS_FechaCallme__c = dtFecha;
            objLead.MX_SB_VTS_HoraCallMe__c = strZoneTZ;
        }
        return objLead;
    }
}