/**
 * @File Name          : MX_SB_SAC_CaseValidation.cls
 * @Description        : Class to prevent changes on case fields
 * @Author             : Jaime Terrats
 * @Group              : BBVA
 * @Last Modified By   : Jaime Terrats
 * @Last Modified On   : 07-30-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    10/30/2019   Jaime Terrats    Initial Version
 * 1.1    11/11/2019   Jaime Terrats    Update logic to exclude detalle value cotizacion
 * 2.0    12/05/2019   Jaime Terrats    Add format to customer name to set first letter to upper case
 * 2.1    29/01/2020   Michelle Valderrama RT for LBBVA Auto Added at checkRecordType
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_SAC_CaseValidation {
    /** set object name */
    final static String OBJECTNAME = 'Case';
    /** array of options for mx_sb_sac_detalle__c */
    final static String[] DETAILS = new String[]{'Información cotizador', 'Información de cotización', 'Información de producto', 'Llamada de prueba', 'No es Asegurado/Contratante', 'Confirma recepción de documentos', 'Sistema no disponible', 'Cotizador web' };
    /** string value for exclude case reason */
    final static String TRANSFERENCIA = 'Transferencia';
    /** array of options for case.reason */
    final static String[] REASON_VALUES = new String[]{'Cancelación', 'Retención'};
    /** string value for product type vida */
    final static String VIDA = 'Vida';
    /** string value for product type auto */
    final static String AUTO = 'Auto';

    /**
    * @description filter case by recordtype
    * @author Jaime Terrats | 10/30/2019
    * @param oldMap
    * @param newList
    * @return void
    **/
    public static void filterData(Map<Id, Case> oldMap, List<Case> newList) {
        final List<Case> casesToValidate = checkRecordType(newList);
        if(casesToValidate.isEmpty() == false) {
            validateChanges(casesToValidate, oldMap);
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 12/5/2019
    * @param newList
    * @return List<Case>
    **/
    public static List<Case> checkRecordType(List<Case> newList) {
        final List<Case> casesToValidate = new List<Case>();
        final String rt1 = Schema.SObjectType.case.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_SAC_Generico).getRecordTypeId();
        final String rt2 = Schema.SObjectType.case.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_SAC_RT).getRecordTypeId();
        final String rt3 = Schema.SObjectType.case.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_SAC_RTAccidentesPersonales).getRecordTypeId();
        final String rt4 = Schema.SObjectType.case.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_SAC_RTDanos).getRecordTypeId();
        final String rt5 = Schema.SObjectType.case.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_SAC_RTSalud).getRecordTypeId();
        final String rt6 = Schema.SObjectType.case.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_SAC_RTVida).getRecordTypeId();
        final String rt7 = Schema.SObjectType.case.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_SAC_RTLBBVAAuto).getRecordTypeId();
        final String rt8 = Schema.SObjectType.case.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_SAC_RTLBBVAVida).getRecordTypeId();
        final String rt9 = Schema.SObjectType.case.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_1500_Auto).getRecordTypeId();
        final String rt10 = Schema.SObjectType.case.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_1500_Vida).getRecordTypeId();
        final List<String> rtIds = new List<String>{rt1, rt2, rt3, rt4, rt5, rt6, rt7, rt8, rt9, rt10};
        for(Case nCase : newList) {
            if(rtIds.contains(nCase.RecordTypeId)) {
                    nCase.MX_SB_SAC_Nombre__c = formatName(nCase.MX_SB_SAC_Nombre__c);
                    nCase.MX_SB_SAC_ApellidoPaternoCliente__c = formatName(nCase.MX_SB_SAC_ApellidoPaternoCliente__c);
                    nCase.MX_SB_SAC_ApellidoMaternoCliente__c = formatName(nCase.MX_SB_SAC_ApellidoMaternoCliente__c);
                    nCase.MX_SB_1500_M__c = formatName(nCase.MX_SB_1500_M__c);
                    nCase.MX_SB_1500_ExecutiveSureName__c = formatName(nCase.MX_SB_1500_ExecutiveSureName__c);
                    nCase.MX_SB_1500_ExecutiveGivenName__c = formatName(nCase.MX_SB_1500_ExecutiveGivenName__c);
                    nCase.MX_SB_1500_Email__c = formatName(nCase.MX_SB_1500_Email__c);
                    nCase.MX_SB_1500_is1500__c = validate1500(nCase);
                    MX_SB_SAC_CaseValidation_Ext.checkSubdetail(nCase);
                    casesToValidate.add(nCase);
            }
        }
        return casesToValidate;
    }

    /**
    * @description will validate if the record has a description, if the field is filled
    * it wont allow to change its value or other speficied fields
    * @author Jaime Terrats | 11/12/2019
    * @param newCases
    * @param oldMap
    * @return void
    **/
    private static void validateChanges(List<Case> newCases, Map<Id, Case> oldMap) {
        final Id profileId = userinfo.getProfileId();
        final String profileName = [Select Id,Name from Profile where Id =:profileId].Name;
        final List<String> fieldsToValidate = new List<String>{'Description', 'Reason', 'MX_SB_SAC_Detalle__c', 'MX_SB_SAC_Subdetalle__c', 'MX_SB_SAC_InformacionAdicional__c', 'MX_SB_SAC_MotivoDeNoVenta__c', 'MX_SB_SAC_EstatusVenta__c'
                                                               ,'MX_SB_1500_CR__c', 'MX_SB_1500_Email__c','MX_SB_1500_ExecutiveGivenName__c','MX_SB_1500_ExecutiveSureName__c', 'MX_SB_1500_M__c', 'MX_SB_1500_Phone__c', 'MX_SB_1500_PhoneExt__c'};
        for(Case nCase : newCases) {
            final Case oldRecord = oldMap.get(nCase.Id);
            final String description = MX_SB_SAC_Utils_Cls.validaDatoVacio(String.valueOf(oldRecord.get(fieldsToValidate[0])));
            preventComments(nCase);
            MX_SB_SAC_CaseValidation_Ext.preventCommentsOnQuoting(nCase);
            for(String str : fieldsToValidate) {
                if(String.isNotBlank(description)) {
                    final String oldValue = MX_SB_SAC_Utils_Cls.validaDatoVacio(String.valueOf(oldRecord.get(str)));
                    final String newValue = MX_SB_SAC_Utils_Cls.validaDatoVacio(String.valueOf(nCase.get(str)));
                    if(System.Label.MX_SB_SAC_AsesorSACProfile.equals(profileName) || System.Label.MX_SB_SAC_GerenteSuperN2.equals(profileName)) {
                        processData(newValue, oldValue, nCase, str);
                    }
                }
            }
            validateAdditionalInformation(nCase);
            MX_SB_SAC_CaseValidation_Ext.descriptionRules(nCase, oldRecord);
            MX_SB_SAC_CaseValidation_Ext.requestCommentsOn1500(nCase, oldRecord);
            MX_SB_SAC_Case_DescriptionRules_Service.requestCommentsOn1500Vida(nCase, oldRecord);
       	}
    }

    /**
    * @description processData to validate if changes are applied or not
    * @author Jaime Terrats | 11/12/2019
    * @param newValue
    * @param oldValue
    * @param nCase
    * @return void
    **/
    private static void processData(String newValue, String oldValue, Case nCase, String str) {
        if(!newValue.equalsIgnoreCase(oldValue) && nCase.MX_SB_SAC_FlujoCotizacion__c == false) {
            final String typeField = returnFieldLabel(OBJECTNAME, str);
            nCase.addError(System.Label.MX_SB_SAC_ErrorEdicion + typeField);
        }
    }

    /**
    * @description returns a fieldLabel
    * @author Jaime Terrats | 10/30/2019
    * @param objectName i.e: Case
    * @param fieldName  i.e: Description
    * @return String
    **/
    private static String returnFieldLabel(String objectName, String fieldName) {
        return Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap().get(fieldName).getDescribe().getLabel();
    }

    /**
    * @description
    * @author Jaime Terrats | 12/5/2019
    * @param value
    * @return String
    **/
    private static String formatName(String value) {
        return String.isNotBlank(value) ? value.toUpperCase() : '';
    }

    /**
    * @description
    * @author Jaime Terrats | 12/11/2019
    * @param nCase
    * @return void
    **/
    private static void preventComments(Case nCase) {
        if(TRANSFERENCIA!= nCase.Reason
        && String.isNotBlank(nCase.Description)
        && nCase.MX_SB_SAC_FinalizaFlujo__c == true
        && Details.contains(nCase.MX_SB_SAC_Detalle__c)
        && nCase.MX_SB_SAC_Ocultartipificacion__c == false
        && nCase.Origin != System.Label.MX_SB_SAC_Correo_electronico) {
            nCase.addError(System.Label.MX_SB_SAC_PreventComments);
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 3/27/2020
    * @param nCase
    * @return Boolean
    **/
    private static Boolean validate1500(Case nCase) {
        return String.isNotBlank(nCase.MX_SB_1500_M__c) ? true : false;
    }

    /**
    * @description
    * @author Jaime Terrats | 6/2/2020
    * @param nCase
    * @return void
    **/
    private static void validateAdditionalInformation(Case nCase) {
        if(REASON_VALUES.contains(nCase.Reason)) {
            switch on nCase.Reason {
                when 'Cancelación' {
                    switch on nCase.MX_SB_SAC_Ramo__c {
                        when 'Auto', 'LBBVA Auto', '1500 Auto' {
                            MX_SB_SAC_CaseValidation_Ext.validateC(nCase, AUTO);
                        }
                        when 'Vida', 'LBBVA Vida', '1500 Vida' {
                            MX_SB_SAC_CaseValidation_Ext.validateC(nCase, VIDA);
                        }
                    }
                }
                when 'Retención' {
                    switch on nCase.MX_SB_SAC_Ramo__c {
                        when 'Auto', 'LBBVA Auto', '1500 Auto' {
                            MX_SB_SAC_CaseValidation_Ext.validateR(nCase, AUTO);
                        }
                        when 'Vida', 'LBBVA Vida', '1500 Vida' {
                            MX_SB_SAC_CaseValidation_Ext.validateR(nCase, VIDA);
                        }
                    }
                }
            }
        }
    }
}