/****************************************************************************************************
Información general
------------------------
author: Omar Gonzalez
company: Ids
Project: PCI

Information about changes (versions)
-------------------------------------
Number    Dates           Author            Description
------    --------        ---------------   -----------------------------------------------------------------
1.0      07-08-2020      Omar Gonzalez      Clase Wrapper que se recibe IdCotiza y FolioAntifraude de una cotización 
****************************************************************************************************/
@SuppressWarnings('sf:AvoidGlobalModifier, sf:UseSingleton')

global without sharing class MX_ASD_PCI_WrapperResponseIvr {
    /*
* Clase Wrapper que recibe los datos del service
*/
    @SuppressWarnings('sf:AvoidGlobalModifier')
    global class MX_ASD_PCI_DatosQuote {
        /* llamada a variable folioDeCotizacion del servicio REST*/
        public String  folioDeCotizacion {get;set;}
        /* llamada a variable idCotiza del servicio REST*/
        public String  idCotiza {get;set;}
    }
    
    /*
* Clase Wrapper para respuesta del service
*/
    @SuppressWarnings('sf:AvoidGlobalModifier')
    global class MX_ASD_PCI_ResponseCobroIvr {
        /* codigo de respuesta */
        public String responseCode {get;set;}
        /* descripcion del codigo */
        public String descriptionCode {get;set;}
        /* Mensaje asesor */
        public String msjAsesor {get;set;}
        /* Campaña seleccionada */
        public String campania {get;set;}
    }
}