/**
* @File Name          : MX_BPP_Contactabilidad_Helper_Test.cls
* @Description        : Test class for MX_BPP_Contactabilidad_Helper
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 07/10/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      07/10/2020            Gabriel Garcia Rojas          Initial Version
**/
@isTest
private  class MX_BPP_Contactabilidad_Helper_Test {

    /*Usuario de pruebas*/
    private static User testUserStart = new User();
    /** namerole variable for records */
    final static String NAME_ROLE = '%BPYP BANQUERO BANCA PERISUR%';
    /** nameprofile variable for records */
    final static String NAME_PROFILE = 'BPyP Estandar';
    /** name variable for records */
    final static String NAMEST = 'testUser';

    /*Variable test PRIVADOS*/
    static final String PRIVADO_STR = 'PRIVADO';
    /*Variable test sm*/
    static final String STR_SM = 'sm';
    /*Variable test En*/
    static final String VAR_EN='En';
    /*Variable test 15*/
    static final String NUM15='15';
    /** error message */
    final static String MESSAGETESTH = 'Fail method';

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testUserStart = UtilitysDataTest_tst.crearUsuario(NAMEST, NAME_PROFILE, NAME_ROLE);
        insert testUserStart;

        final DateTime fechaTest = System.today();

        System.runAs(testUserStart) {
            final List<dwp_kitv__Visit__c> listVisita = new List<dwp_kitv__Visit__c>();

           	final Account cuenta = new Account(FirstName = 'CuentaTest', LastName = 'LastTest');
        	insert cuenta;

            final dwp_kitv__Visit__c visita = new dwp_kitv__Visit__c(Name = 'VisitaTest', dwp_kitv__visit_status_type__c = '05', dwp_kitv__account_id__c = cuenta.Id,
                                                              dwp_kitv__visit_duration_number__c = NUM15, dwp_kitv__visit_start_date__c = fechaTest);
            final dwp_kitv__Visit__c visita2 = new dwp_kitv__Visit__c(Name = 'VisitaTest2', dwp_kitv__visit_status_type__c = '01', dwp_kitv__account_id__c = cuenta.Id,
                                                               dwp_kitv__visit_duration_number__c = NUM15, dwp_kitv__visit_start_date__c = fechaTest);

            listVisita.add(visita);
            listVisita.add(visita2);

            insert listVisita;
        }


    }

    /**
     * @description constructor test
     * @author Gabriel Garcia | 07/10/2020
     * @return void
     **/
    @isTest
    private static void testConstructor() {
        final MX_BPP_Contactabilidad_Helper instanceTestH = new MX_BPP_Contactabilidad_Helper();
        System.assertNotEquals(instanceTestH, null, MESSAGETESTH);
    }

    /**
     * @description test gtPeriodo
     * @author Gabriel Garcia | 07/010/2020
     * @return void
     **/
    @isTest
    private static void gtPeriodoTest() {
        Test.startTest();
        final String privadoStr =  MX_BPP_Contactabilidad_Helper.gtPeriodo(PRIVADO_STR);
        MX_BPP_Contactabilidad_Helper.gtPeriodo('PATRIMONIAL');
        MX_BPP_Contactabilidad_Helper.gtPeriodo('PATRIMONIAL SR');

        System.assertEquals(privadoStr, 'Periodo Privado', MESSAGETESTH);

        Test.stopTest();
    }


    /**
     * @description test retmonth
     * @author Gabriel Garcia | 07/010/2020
     * @return void
     **/
    @isTest
    private static void retmonthTest() {
        Test.startTest();
        MX_BPP_Contactabilidad_Helper.gtPeriodo(PRIVADO_STR);
        final Integer numMes =  MX_BPP_Contactabilidad_Helper.retmonth(STR_SM, 'Mensual');
        MX_BPP_Contactabilidad_Helper.retmonth(STR_SM, 'Bimestral');
        MX_BPP_Contactabilidad_Helper.retmonth(STR_SM, 'Trimestral');

        System.assertNotEquals(numMes, 0, MESSAGETESTH);

        Test.stopTest();
    }

     /**
     * @description test retmonth
     * @author Gabriel Garcia | 07/010/2020
     * @return void
     **/
    @isTest
    private static void obtStAndEndDateTest() {
        Test.startTest();
        final Datetime fecha = System.now();
        final Datetime varmonth =  MX_BPP_Contactabilidad_Helper.obtStAndEndDate(10, VAR_EN);
        MX_BPP_Contactabilidad_Helper.obtStAndEndDate(10, 'St');
        System.assertNotEquals(varmonth, fecha.addMonths(1), MESSAGETESTH);

        Test.stopTest();
    }

    /**
     * @description test processAccounts
     * @author Gabriel Garcia | 07/010/2020
     * @return void
     **/
    @isTest
    private static void processAccountsTest() {
        Test.startTest();
        final List<Account> cuentaT = [SELECT Id, Name, (SELECT Id, dwp_kitv__visit_status_type__c FROM dwp_kitv__Visits__r ) FROM Account LIMIT 1];
        final Set<Account> setAccT = MX_BPP_Contactabilidad_Helper.processAccounts(cuentaT[0], 'Contact');

        final List<dwp_kitv__Visit__c> listVisit = [SELECT Id, dwp_kitv__account_id__c FROM dwp_kitv__Visit__c LIMIT 10];
        MX_BPP_Contactabilidad_Helper.processAccounts(listVisit);

        System.assertNotEquals(setAccT, null, MESSAGETESTH);

        Test.stopTest();
    }
}