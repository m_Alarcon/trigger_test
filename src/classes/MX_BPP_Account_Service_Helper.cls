/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Account_Service_Helper
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-08-06
* @Description 	Helper for Account Service Layer
* @Changes
*  
*/
public without sharing class MX_BPP_Account_Service_Helper {
    
    /*Contructor clase MX_BPyP_Opportunity_Service */
    private MX_BPP_Account_Service_Helper() {}
    
    /**
    * @Description 	Update related cases and leads
    * @Param		opp Opportunity, oldStatus String, newStatus String
    * @Return 		NA
    **/
    public static void updateRelatedRecordsOwner(List<Account> updatedAccList, Set<Id> updatedAccIDs, Map<Id, Account> newAccMap) {
        String caseFields;
        String leadFields;
        final List<Case> casesToUpdate = new List<Case>();
        final List<Lead> leadsToUpdate = new List<Lead>();
        caseFields = 'Id, OwnerId, AccountId';
        leadFields = 'Id, OwnerId, MX_WB_RCuenta__c';
        
        for (Case caseToUpd : MX_RTL_Case_Selector.getCasesByAccountAndRtype(caseFields, updatedAccIDs, 'MX_EU_Case_Apoyo_General')) {
            caseToUpd.OwnerId = newAccMap.get(caseToUpd.AccountId).OwnerId;
            casesToUpdate.add(caseToUpd);
        }
        
        for (Lead leadToUpd : MX_RTL_Lead_Selector.getActiveLeadsByAccountAndRtype(leadFields, updatedAccIDs, 'MX_BPP_Leads')) {
            leadToUpd.OwnerId = newAccMap.get(leadToUpd.MX_WB_RCuenta__c).OwnerId;
            leadsToUpdate.add(leadToUpd);
        }
        
        MX_RTL_Case_Selector.actualizaCasos(casesToUpdate);
        MX_RTL_Lead_Selector.updateResult(leadsToUpdate, false);
    }

}