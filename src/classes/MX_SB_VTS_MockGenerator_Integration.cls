/**-------------------------------------------------------------------------
* Nombre: 		MX_SB_VTS_MockGenerator_Integration
* @author 		Alexandro Corzo
* Proyecto: 	Mock Formulario - Dirección de la Propiedad
* Descripción : Clase Mock para la clase de recuperación de colonias conforme al
*				CP ingresado
* --------------------------------------------------------------------------
*                         Fecha           Autor                   Desripción
* --------------------------------------------------------------------------
* @version 1.0            23/07/2020      Alexandro Corzo         Creación de la Clase
* --------------------------------------------------------------------------*/
@isTest
@SuppressWarnings('sf:AvoidGlobalModifier')
global class MX_SB_VTS_MockGenerator_Integration implements HttpCalloutMock {
	/** Variable de Apoyo: sTypePost */
    Static String sTypePost = 'POST';
    
    /** Implementación de Interface */
    global HTTPResponse respond(HTTPRequest request) {
        final HttpResponse oResponse = new HttpResponse();
        oResponse.setHeader('Content-Type', 'application/json');
        oResponse.setBody('{ "iCatalogItem": { "suburb": [{ "neighborhood": { "id":"0196", "name": "UNIDAD HABITACIONAL, LA PATERA VALLEJO" }, "county":{ "id":"005", "name":"GUSTAVO A. MADERO" }, "city":{ "id":"01", "name":"CIUDAD DE MÉXICO" }, "state":{ "id":"09", "name":"CIUDAD DE MEXICO" }}]}}');
        oResponse.setStatusCode(200);
        return oResponse;
    }
}