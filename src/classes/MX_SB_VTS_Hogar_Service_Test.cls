/**-------------------------------------------------------------------------
* Nombre: MX_SB_VTS_Hogar_Service_Test
* @author Jaime (JT)
* Proyecto: MX_Ventas - BBVA Bancomer
* Descripción : Clase test que sirve para la cobertura de la clase MX_SB_VTS_Hogar_Service_cls
*
* --------------------------------------------------------------------------
*                         Fecha           Autor                   Desripción
* --------------------------------------------------------------------------
* @version 1.0           30/04/2019      Jaime T                   Creación
* @version 1.1           09/05/2019      Jaime T                   Refactor
* @version 1.2           17/10/2019      Jaime T                 Se agrega actualiza clase de prueba
* @version 1.3 		     11/03/2020     Francisco Javier        Fix cotizador seguros
* --------------------------------------------------------------------------*/
@isTest
private class MX_SB_VTS_Hogar_Service_Test {
    /*
    * Variable empleada para Folio de Cotizacion
    */
    static final String FOLIO_COT = '99999';
    /*
    * Variable Empleada para el Email
    */
    static final String PERSON_EMAIL = 'john.doe@test.hogar.com';
    /*
    * Variable que contiene el origen de la oportunidad 
    */
    static final String ACCOUNT_SOURCE = 'Inbound';

    @TestSetup
    static void makeData() {
        final User admin = MX_WB_TestData_cls.crearUsuario('test hogar', system.label.MX_SB_VTS_ProfileIntegration);
        Insert admin;

        System.runAs(admin) {
            MX_WB_TestData_cls.createStandardPriceBook2();

            final MX_WB_FamiliaProducto__c objFamilyPro2 = MX_WB_TestData_cls.createProductsFamily ( 'Daños' );
            insert objFamilyPro2;

            final MX_WB_FamiliaProducto__c objFamilyProVida = MX_WB_TestData_cls.createProductsFamily ( 'Vida' );
            insert objFamilyProVida;

            final Product2 producto = MX_WB_TestData_cls.productNew(System.Label.MX_SB_VTS_Hogar);
            producto.isActive = true;
            producto.MX_WB_FamiliaProductos__c = objFamilyPro2.Id;
            Insert producto;

            final Product2 productoVida = MX_WB_TestData_cls.productNew('Vida');
            productoVida.isActive = true;
            productoVida.MX_WB_FamiliaProductos__c = objFamilyPro2.Id;
            Insert productoVida;

            final PricebookEntry pbeVida = MX_WB_TestData_cls.priceBookEntryNew(productoVida.Id);
            Insert pbeVida;

            final PricebookEntry pbe = MX_WB_TestData_cls.priceBookEntryNew(producto.Id);
            Insert pbe;

            final Account acc = MX_WB_TestData_cls.crearCuenta('Doe', 'PersonAccount');
            acc.PersonEmail = PERSON_EMAIL;
            acc.FirstName = 'John';
            acc.accountSource = ACCOUNT_SOURCE;
            Insert acc;

            final Opportunity opp = MX_WB_TestData_cls.crearOportunidad('OppTest '+System.Label.MX_SB_VTS_Hogar, acc.Id, admin.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            opp.Reason__c = 'Venta';
            opp.Producto__c = System.Label.MX_SB_VTS_Hogar;
            opp.MX_SB_VTS_Aplica_Cierre__c = true;
            opp.StageName = 'Cotización';
            Insert opp;

            final OpportunityLineItem oli = MX_WB_TestData_cls.oppLineItmNew(opp.Id, pbe.Id, producto.Id, 1, 1);
            Insert oli;

            final Quote quo = MX_WB_TestData_cls.crearQuote(opp.Id, '99999 1 OppTest '+System.Label.MX_SB_VTS_Hogar, FOLIO_COT);
            Insert quo;

            MX_WB_TestData_cls.createStandardPriceBook2 ();
        }
    }


    static MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion prepareJSON(String[] data) {
        final MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion datosCotizacion = new MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion();
        final MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosIniciales datosIniciales = new MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosIniciales();
        final MX_SB_VTS_HogarWrapper.MX_SB_VTS_TipoDeSeguro tipoDeSeguro = new MX_SB_VTS_HogarWrapper.MX_SB_VTS_TipoDeSeguro();
        final MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosAdicionales datosAdicionales = new MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosAdicionales();
        final MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCliente datosCliente = new MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCliente();


        datosIniciales.codigoPostal = '43800';
        datosIniciales.folioCotizacion = data[0];
        datosIniciales.valorHogar = '10000';
        datosIniciales.statusCotizacion = data[1];
        datosIniciales.origen = data[2];
        datosIniciales.producto = System.Label.MX_SB_VTS_Hogar;
        datosIniciales.numeroPoliza = data[7];
        datosIniciales.folioTracking = '';

        datosCotizacion.datosIniciales = datosIniciales;

        tipoDeSeguro.conOSinSismo = 'con';
        tipoDeSeguro.danosInmueble = '';
        tipoDeSeguro.danosAContenidos = '';
        tipoDeSeguro.porRobo = '';
        tipoDeSeguro.roturaDeCristales = '';
        tipoDeSeguro.responsabilidadPrivadaFamiliar = '';
        tipoDeSeguro.danosEquipoElectrico = '';
        tipoDeSeguro.precioAnual = '';
        tipoDeSeguro.frequenciaPago = data[6];
        tipoDeSeguro.cantidadDePagos = data[5];

        datosCotizacion.tipoDeSeguro = tipoDeSeguro;

        datosCotizacion.datosPrecio = constructordatosPrecio();

        datosAdicionales.cercaniaMantosAquiferos = false;
        datosAdicionales.murosTabiqueLadrilloBlock = true;
        datosAdicionales.techoTabiqueLadrilloBlock = true;
        datosAdicionales.casaODepartamento = 'casa';
        datosAdicionales.noPisosInmueble = '2';
        datosAdicionales.pisoHabitado = '';

        datosCotizacion.datosAdicionales = datosAdicionales;

        datosCliente.nombre = 'John';
        datosCliente.apPaterno = data[4];
        datosCliente.apMaterno = 'C.';
        datosCliente.email = data[3];
        datosCliente.fechaNacimiento = '09/03/1991';
        datosCliente.telefonoCasa = '571095432';
        datosCliente.celular = '5571095432';
        datosCliente.sexo = 'm';
        datosCliente.rfc = 'bafl890410';

        datosCotizacion.datosCliente = datosCliente;
        datosCotizacion.datosDomicilio = constructorDatosDomicilio();
        datosCotizacion.datosDomAsegurado = constructorDatosDomicilio();

        return datosCotizacion;
    }
    private static MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosDomicilio constructorDatosDomicilio() {
        final MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosDomicilio datosDomicilio = new MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosDomicilio();
            datosDomicilio.calleCliente = 'pensamiento';
            datosDomicilio.cpCliente = '43800';
            datosDomicilio.coloniaCliente = 'NuevoTizayuca';
            datosDomicilio.codColoniaCliente = '';
            datosDomicilio.ciudadCliente = '';
            datosDomicilio.codCiudad = '';
            datosDomicilio.numExtCliente = '67';
            datosDomicilio.numIntCliente = '';
            datosDomicilio.estadoCliente = 'Hidalgo';
            datosDomicilio.codEstadoCliente = '';
            datosDomicilio.paisCliente = '';
            datosDomicilio.codPaisCliente = '';
        return datosDomicilio;
    }
    private static MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosPrecio constructordatosPrecio() {
        final MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosPrecio datosPrecio = new MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosPrecio();
        datosPrecio.precioParcialidades = '612.09';
        datosPrecio.precioTotal = '10000';
        datosPrecio.msiBancomer = '';
        datosPrecio.cupon = 'No';
        return datosPrecio;
    }

    @isTest
    static void findQuote() {
        List<MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC> resSF = new List<MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC>();
        final String[] mockData = new String[]{FOLIO_COT, 'Creada', ACCOUNT_SOURCE, PERSON_EMAIL, 'Doe', '', 'Semestral', ''};
        final MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion datosCotizacion = prepareJSON(mockData);
        Test.startTest();
        	resSF = MX_SB_VTS_Hogar_Service.cotizarSeguroHogar(datosCotizacion);
        	System.assertNotEquals(resSF, null, 'Invalid response from service');
        Test.stopTest();
    }

    @isTest
    static void createNewQuote() {
        Test.startTest();
        List<MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC> resSF = new List<MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC>();
        final String[] mockData = new String[]{'99991', 'Cotizada', 'Outbound', 'john.doe@test.hogar.com', 'D', '4', '', ''};
        final MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion datosCotizacion = prepareJSON(mockData);
        resSF = MX_SB_VTS_Hogar_Service.cotizarSeguroHogar(datosCotizacion);
        System.assertNotEquals(resSF, null, 'Success new quote created');
        Test.stopTest();
    }
    @isTest
    static void findQuoteAccountfalse() {
        Test.startTest();
        List<MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC> resSF = new List<MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC>();
        final String[] mockData = new String[]{'1999', 'Cobrada', ACCOUNT_SOURCE, 'j.d@test.hogar.com', 'Do', '', 'Trimestral', ''};
        final MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion datosCotizacion = prepareJSON(mockData);
        resSF = MX_SB_VTS_Hogar_Service.cotizarSeguroHogar(datosCotizacion);
        System.assertNotEquals(resSF, null, 'Created new Account');
        Test.stopTest();
    }

    @isTest
    static void updateExistingQuote() {
        Test.startTest();
        List<MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC> resSF = new List<MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC>();
        final String[] mockData = new String[]{FOLIO_COT, 'Cotizada', ACCOUNT_SOURCE, PERSON_EMAIL, 'De', '12', '', ''};
        final MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion datosCotizacion = prepareJSON(mockData);
        resSF = MX_SB_VTS_Hogar_Service.cotizarSeguroHogar(datosCotizacion);
        System.assertNotEquals(resSF, null, 'Update Quote Status');
        Test.stopTest();
    }

    @isTest
    static void updateExistingQuote2() {
        Test.startTest();
        List<MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC> resSF = new List<MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC>();
        final String[] mockData = new String[]{FOLIO_COT, 'Tarificada', ACCOUNT_SOURCE, PERSON_EMAIL, 'LN', '', 'Anual', ''};
        final MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion datosCotizacion = prepareJSON(mockData);
        resSF = MX_SB_VTS_Hogar_Service.cotizarSeguroHogar(datosCotizacion);
        System.assertNotEquals(resSF, null, 'Updated Quote Status');
        Test.stopTest();
    }

    @isTest
    static void updateExistingQuote3() {
        Test.startTest();
        List<MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC> resSF = new List<MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC>();
        final String[] mockData = new String[]{FOLIO_COT, 'Cobrada', ACCOUNT_SOURCE, PERSON_EMAIL, 'L', '1', '', ''};
        final MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion datosCotizacion = prepareJSON(mockData);
        resSF = MX_SB_VTS_Hogar_Service.cotizarSeguroHogar(datosCotizacion);
        System.assertNotEquals(resSF, null, 'Updated Quote Status');
        Test.stopTest();
    }

   @isTest
    static void updateExistingQuote4() {
        Test.startTest();
        List<MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC> resSF = new List<MX_SB_VTS_HogarWrapper.MX_SB_VTS_ResponseSFDC>();
        final String[] mockData = new String[]{FOLIO_COT, 'Emitida', ACCOUNT_SOURCE, PERSON_EMAIL, 'L', '2', '', '123456789'};
        final MX_SB_VTS_HogarWrapper.MX_SB_VTS_DatosCotizacion datosCotizacion = prepareJSON(mockData);
        resSF = MX_SB_VTS_Hogar_Service.cotizarSeguroHogar(datosCotizacion);
        System.assertNotEquals(resSF, null, 'Updated Quote Status');
        Test.stopTest();
    }

    @isTest
    static void creatURLeNewQuote() {
        final Id accountId = [Select Id from Account where PersonEmail = 'john.doe@test.hogar.com'].Id;
        final Id userId = [Select Id from User where LastName = 'test hogar'].Id;
        final Id prodId = [Select Id from Product2 where Name = 'Hogar'].Id;
        final Opportunity opp = MX_WB_TestData_cls.crearOportunidad('OppTest Hogar URL', accountId, userId, System.Label.MX_SB_VTS_Telemarketing_LBL);
        opp.Estatus__c = 'Contacto';
        opp.Reason__c = 'Venta';
        opp.Producto__c = System.Label.MX_SB_VTS_Hogar;
        insert opp;
        final Quote presupuesto = new Quote();
        final QuoteLineItem quoli = new QuoteLineItem();
        presupuesto.Name = 'QuoteHogarTest';
        presupuesto.OpportunityId = opp.Id;
        final Id familt = [Select Id from MX_WB_FamiliaProducto__c where Name = 'Daños'].Id;
        presupuesto.MX_SB_VTS_Familia_Productos__c = familt;
        presupuesto.Status = 'Creada';
        for ( PricebookEntry pbe : [select Id,Pricebook2Id FROM PricebookEntry where Product2Id =: prodId] ) {
            quoli.PricebookEntryId = pbe.Id;
            presupuesto.Pricebook2Id = pbe.Pricebook2Id;
        }
        insert presupuesto;
        quoli.QuoteId = presupuesto.Id;
        quoli.UnitPrice = 1.00;
        quoli.Quantity = 1;
        insert quoli;
        final String urlTst = MX_WB_TM_Cotizador_cls.crearURL ( presupuesto.Id );
        final Integer sizeUrl = urlTst.length();
        System.assertEquals(sizeUrl, urlTst.length(), 'Url generada');
    }


    @isTest
    static void creatURLeNewQuoteVida() {
        final Id accountId = [Select Id from Account where PersonEmail = 'john.doe@test.hogar.com'].Id;
        final Id userId = [Select Id from User where LastName = 'test hogar'].Id;
        final Id prodId = [Select Id from Product2 where Name = 'Vida'].Id;
        final Opportunity opp = MX_WB_TestData_cls.crearOportunidad('OppTest Vida URL', accountId, userId, System.Label.MX_SB_VTS_Telemarketing_LBL);
        opp.Estatus__c = 'Contacto';
        opp.Reason__c = 'Venta';
        opp.Producto__c = 'Vida';
        insert opp;
        final Quote presupuesto = new Quote();
        final QuoteLineItem quoli = new QuoteLineItem();
        presupuesto.Name = 'QuoteVidaTest';
        presupuesto.OpportunityId = opp.Id;
        final Id familt = [Select Id from MX_WB_FamiliaProducto__c where Name = 'Vida'].Id;
        presupuesto.MX_SB_VTS_Familia_Productos__c = familt;
        presupuesto.Status = 'Creada';
        for ( PricebookEntry pbe : [select Id,Pricebook2Id FROM PricebookEntry where Product2Id =: prodId] ) {
            quoli.PricebookEntryId = pbe.Id;
            presupuesto.Pricebook2Id = pbe.Pricebook2Id;
        }
        insert presupuesto;
        quoli.QuoteId = presupuesto.Id;
        quoli.UnitPrice = 1.00;
        quoli.Quantity = 1;
        insert quoli;
        final MX_SB_VTS_Beneficiario__c beneficiario = new MX_SB_VTS_Beneficiario__c();
        beneficiario.Name = 'tEST';
        beneficiario.MX_SB_VTS_AMaterno_Beneficiario__c = 'TESTaM';
        beneficiario.MX_SB_VTS_APaterno_Beneficiario__c = 'testAp';
        beneficiario.MX_SB_VTS_Quote__c = presupuesto.Id;
        insert beneficiario;
        final String urlTst = MX_WB_TM_Cotizador_cls.crearURL ( presupuesto.Id );
        final Integer sizeUrl = urlTst.length();
        System.assertEquals(sizeUrl, urlTst.length(), 'Url generada');
    }
}