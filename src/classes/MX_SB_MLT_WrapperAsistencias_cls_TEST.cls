@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_WrapperAsistencias_cls_TEST
* Autor Juan Carlos Benitez Herrera
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_WrapperAsistencias_cls

* --------------------------------------------------------------------------------
* Versión       Fecha                  Autor                        Descripción
* --------------------------------------------------------------------------------
* 1.0           17/12/2019      Juan Carlos Benitez Herrera          Creación
* 1.1           11/03/2020      Angel Nava                           migración campos contract
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_WrapperAsistencias_cls_TEST {
    
    /*
    * @Field JSON emula el Stringify del controlador del componente
    */
   		Public static final String JSON ='{"ReembAct":"false","defaultValue":"Grua","defaultValueAve":"Mecánica","defaultValueTipoG":"Plataforma","llantas":"false","neutral":"false","Email":"lawbh93@hotmail.com","CondicionesVeh":"No arranca"}';
    /*
    * @Field JSON2 emula el Stringify del controlador del componente
    */
	 	Public static final String JSON2= '{"ReembAct":"false","defaultValue":"Gasolina","Email":"lawbh93@outlook.com","CondicionesVeh":"No arranca"}';
    /*
    * @Field JSON3 emula el Stringify del controlador del componente
    */
     	Public static final String JSON3= '{"ReembAct":"false","defaultValue":"Cambio de llanta","Email":"lawbh93@yahoo.com","CondicionesVeh":"No arranca"}';
    /*
    * @Field JSON4 emula el Stringify del controlador del componente
    */
     	Public static final String JSON4= '{"ReembAct":"false","defaultValue":"Paso de corriente","Email":"lawbh93@aol.com","CondicionesVeh":"No arranca"}';
    /* 
    
    /*
	* var String correo electronico
	*/    
    public static final String CORREO = 'lawbh93@gmail.com';
    /*
	* var String estado abierto
	*/    
    public static final String ABIERTO = 'Abierto';
	@testSetup
	static void setupTestData() {
        final String nameProfile = [SELECT Name from profile where name in ('Administrador del sistema','System Administrator') limit 1].Name;
        final User objUsrTst = MX_WB_TestData_cls.crearUsuario('PruebaAdminUsTst', nameProfile); 
        insert objUsrTst;
        System.runAs(objUsrTst) {
        final String  tiporegistro = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
		final Account ctatest = new Account(recordtypeid=tiporegistro,firstname='DANIEL',lastname='LOPEZ',Apellido_materno__pc='LOPEZ',RFC__c='PELD920912',PersonEmail='test@gmail.mx');
        insert ctatest;
        final Contract contrato = new Contract(accountid=ctatest.Id,MX_SB_SAC_NumeroPoliza__c='PolizaTest',MX_SB_SAC_RFCAsegurado__c =ctatest.rfc__c,MX_SB_SAC_NombreClienteAseguradoText__c =ctatest.firstname,MX_WB_apellidoPaternoAsegurado__c=ctatest.lastname,MX_WB_apellidoMaternoAsegurado__c=ctatest.Apellido_materno__pc);
        insert contrato;
		final String  rectpe = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_RamoAuto').getRecordTypeId();
    	final Siniestro__c sinies = new Siniestro__c(MX_SB_SAC_Contrato__c=contrato.Id,recordtypeid=rectpe,TipoSiniestro__c='Asistencia Vial',MX_SB_MLT_JourneySin__c='Identificación del Reportante',MX_SB_MLT_CorreoElectronico__c=correo);
        insert sinies;
		final String woRt= Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_Servicios').getRecordTypeId();
        final WorkOrder woGrua = new WorkOrder(MX_SB_MLT_Siniestro__c=sinies.Id,MX_SB_MLT_Email__c=CORREO,MX_SB_MLT_TipoSiniestro__c = 'Grua', MX_SB_MLT_EstatusServicio__c =ABIERTO,recordTypeId=woRt);
		insert woGrua;
        final WorkOrder woGas = new WorkOrder(MX_SB_MLT_Siniestro__c=sinies.Id,MX_SB_MLT_Email__c=CORREO,MX_SB_MLT_TipoSiniestro__c = 'Gasolina', MX_SB_MLT_EstatusServicio__c =ABIERTO,recordTypeId=woRt);
        insert woGas;
        final WorkOrder woCambio = new WorkOrder(MX_SB_MLT_Siniestro__c=sinies.Id,MX_SB_MLT_Email__c=CORREO,MX_SB_MLT_TipoSiniestro__c = 'Cambio de llanta', MX_SB_MLT_EstatusServicio__c =ABIERTO,recordTypeId=woRt);
		insert woCambio;
        final WorkOrder woPasoC = new WorkOrder(MX_SB_MLT_Siniestro__c=sinies.Id,MX_SB_MLT_Email__c=CORREO,MX_SB_MLT_TipoSiniestro__c = 'Paso de corriente', MX_SB_MLT_EstatusServicio__c =ABIERTO,recordTypeId=woRt);
		insert woPasoC;
        final Siniestro__c sinies2 = new Siniestro__c(MX_SB_SAC_Contrato__c=contrato.Id,recordtypeid=rectpe,TipoSiniestro__c='Asistencia Vial',MX_SB_MLT_JourneySin__c='Identificación del Reportante',MX_SB_MLT_CorreoElectronico__c='lavitz93@gmail.com');
        insert sinies2;
        }
  }
	@isTest static void fetchSini() {
    	final Siniestro__c sinies = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com'  limit 1];
		Test.startTest();
		final String ide = sinies.id;
        MX_SB_MLT_WrapperAsistencias_cls.fetchSiniDetails(ide);
        system.assert(true, '¡Exito!');
		test.stopTest();
       }
    @isTest static void obWServ() {
        final Siniestro__c sinies = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com' limit 1];
        Test.startTest();
        final String ide = sinies.Id;
		MX_SB_MLT_WrapperAsistencias_cls.getGruaCase(ide);
		MX_SB_MLT_WrapperAsistencias_cls.getCambioLlantaCase(ide);	
        MX_SB_MLT_WrapperAsistencias_cls.getPasoCorrienteCase(ide);
        MX_SB_MLT_WrapperAsistencias_cls.getGasolinaCase(ide);
		system.assert(true, '¡se obtuvieron las WO !');
        test.stopTest();
    }
	@isTest static void detallesSin() {
	final Siniestro__c sinies = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com'];
    Test.startTest();
		MX_SB_MLT_WrapperAsistencias_cls.fetchSiniDetails(sinies.Id);
        system.assert(true, 'Se obtuvieron los detalles de siniestro');
	test.stopTest();

    }
	@isTest static void inserWOExito() {
        final Siniestro__c sinies = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lawbh93@gmail.com'];
        final String ide= sinies.Id;
        Test.startTest();
		MX_SB_MLT_WrapperAsistencias_cls.insertCaseGrua(JSON, ide);
		MX_SB_MLT_WrapperAsistencias_cls.insertCaseGas(JSON2, ide);
        MX_SB_MLT_WrapperAsistencias_cls.insertWOCambLl(ide, JSON3);
        MX_SB_MLT_WrapperAsistencias_cls.insertWOPasoC(ide, JSON4);
        system.assert(true, 'Se ha actualizado con exito');
    	test.stopTest();
    }
    @isTest static void inserWOElse() {
        final Siniestro__c sinies2 = [SELECT Id, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c FROM Siniestro__c  where MX_SB_MLT_CorreoElectronico__c='lavitz93@gmail.com'];
        final String ide2= sinies2.Id;
        Test.startTest();
		MX_SB_MLT_WrapperAsistencias_cls.insertCaseGrua(JSON, ide2);
        MX_SB_MLT_WrapperAsistencias_cls.insertCaseGas(JSON2, ide2);
        MX_SB_MLT_WrapperAsistencias_cls.insertWOCambLl(ide2, JSON3);
        MX_SB_MLT_WrapperAsistencias_cls.insertWOPasoC(ide2, JSON4);
        system.assert(true, 'Se han creado las Work Orders');
    	test.stopTest();
    }

    @isTest static void existe() {
        final WorkOrder woGru = [SELECT Id, MX_SB_MLT_Siniestro__c, MX_SB_MLT_TipoSiniestro__c, recordTypeId FROM WorkOrder  where MX_SB_MLT_TipoSiniestro__c = 'Grua'];
			test.startTest();
        	MX_SB_MLT_WrapperAsistencias_cls.existeCaso(woGru.MX_SB_MLT_TipoSiniestro__c, woGru.MX_SB_MLT_Siniestro__c, woGru.recordTypeId);
        	system.assert(true, 'Existe!');
			test.stopTest();
    }
    
    @isTest static void wrappGrua() {
        test.startTest();
        final MX_SB_MLT_WrapperAsistencias_cls.WrapperCaseGrua wrapper = new mx_sb_mlt_wrapperasistencias_cls.WrapperCaseGrua();
		wrapper.defaultValue='Grua';
        wrapper.condicionesVeh='No enciende';
        wrapper.defaultValueAve='Mecánica';
        wrapper.defaultValueTipoG='Plataforma';
        wrapper.email='lawbh93@gmail.com';
		wrapper.llantas='true';
        wrapper.neutral='true';
        wrapper.soliReemb ='false';
        system.assert(true, 'Wrapper Exito!');
        test.stopTest();
    }
}