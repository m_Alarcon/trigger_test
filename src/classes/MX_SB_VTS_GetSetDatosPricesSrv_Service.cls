/**
 * @File Name          : MX_SB_VTS_GetSetDatosPricesSrv_Service.cls
 * @Description        : Consumo de Servicio - Prices - Cotizador Hogar
 * @Author             : Alexandro Corzo
 * @Group              : 
 * @Last Modified By   : Alexandro Corzo
 * @Last Modified On   : 24-12-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0       24/12/2020      Alexandro Corzo        Initial Version
*  1.1       24/02/2021      Alexandro Corzo        Se realizan ajustes a clase consumo ASO
*  1.1.1     09/03/2021      Eduardo Hernández      Se corrige clase para Suma dinámica
*  1.1.2     12/03/2021      Alexandro Corzo        Se agrega esta condición
**/
@SuppressWarnings('sf:UseSingleton')
public class MX_SB_VTS_GetSetDatosPricesSrv_Service {
	/** Variable de Apoyo **/
    private static final Integer OKCODE = 200;
    /** Variable de Apoyo: SRVNOTAVAL */
    private static final Integer SRVNOTAVAL = 503;
    /** Variable de Apoyo: STRLBLIDDATA */
    private static final String STRLBLIDDATA = 'iddata';
    /** Variable de Apoyo: SRTOBJNULL */
    private static final String SRTOBJNULL = null;
    /** Variable de Apoyo: FLDCRITSTR */
    private final static String FLDCRITSTR = 'criterial';
    /** Variable de Apoyo: FLDIDDATASTR */
    private final static String FLDIDDATASTR = 'id_data';
    /** Variable de Apoyo: FLDVALSTR */
    private final static String FLDVALSTR = 'value';
    /** Variable de Apoyo: FLDCRITSTR */
    private final static String FLDCRITSTRM = 'Criterial';
    /** Variable de Apoyo: FLDIDDATASTR */
    private final static String FLDIDDATASTRM = 'IdData';
    /** Variable de Apoyo: FLDVALSTR */
    private final static String FLDVALSTRM = 'Value';
    /** Variable de Apoyo: FLDRENSTR */
    private final static List<String> FLDRENSTR = new List<String>{'Rentado'};
    /**Variable de apoyo para UnitsAmountIns*/
    Private Final Static String UNITSAMOUNTINS = 'UnitsAmountIns';
    
    /**
     * @description : Genera el JSON e Invoca al Servicio: createPrices
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    public static List<Object> setDataPricesSrv(String strOppoId, String strSumAse, List<Object> lstDatPart) {
        final List<Object> lstParamSD = new List<Object>();
        Map<String, Object> mParamsWrp = new Map<String, Object>();
        final List<Opportunity> lstOpps = MX_RTL_Opportunity_Selector.selectSObjectsById(new Set<String>{strOppoId});
        final List<Quote> lstQuotes = MX_RTL_Quote_Selector.resultQryQuote('Id, Name, QuoteToName', 'OpportunityId = \'' + strOppoId + '\'', true);
        Map<String, Object> paramsQuote = new Map<String, Object>();
        for(Quote oRowQuote : lstQuotes) {
            final MX_RTL_MultiAddress__c objMulAddr = MX_RTL_MultiAddress_Selector.getAddress(strOppoId, 'Id, MX_RTL_PostalCode__c, MX_SB_VTS_Metros_Cuadrados__c, MX_RTL_AddressType__c, MX_RTL_Tipo_Propiedad__c');
            final String[] oDataValues = oRowQuote.QuoteToName.split('-');
            final String strType = oDataValues[0];
            final String strCotiz = oDataValues[1];
            final String strIdQuote = oRowQuote.Id;
            final String insuredPerStr = MX_SB_VTS_GetSetDatosPricesAux_Helper.fillCorrectInsured(lstDatPart, strCotiz);
            final String coveredAreStr = MX_SB_VTS_GetSetDatosPricesAux_Helper.fillCorrectAreaStr(objMulAddr);
            final Map<String, Object> mParamsM = new Map<String, Object>{'strType' => strType,  'strCotiz' => strCotiz, 'strSumAse' => strSumAse, 'lstDatPart' => lstDatPart, 
                'postalcode' => objMulAddr.MX_RTL_PostalCode__c, 'metrosCuadrados' => objMulAddr.MX_SB_VTS_Metros_Cuadrados__c, 'insuredAmount' => lstOpps[0].Monto_de_la_oportunidad__c,
                'insuredPercentage' => insuredPerStr, 'coveredArea' => coveredAreStr};
           	final MX_SB_VTS_wrpPricesDataSrv.base objWrapper = MX_SB_VTS_GetSetDatosProcesSrv_Helper.fillDataGen(mParamsM);
            final String sWrapperJSON = String.valueOf(JSON.serialize(objWrapper)).replaceAll(STRLBLIDDATA, 'id').replaceAll('currencydata','currency').replaceAll('parent_data','parentdata');
            final Map<String, Object> oResultSrv = callInvokeService(sWrapperJSON);
            final MX_SB_VTS_wrpPricesDataSrvRsp objWrapperRsp = (MX_SB_VTS_wrpPricesDataSrvRsp) oResultSrv.get('content');
            mParamsWrp = new Map<String, Object>();
            mParamsWrp.put('objWrapper', objWrapper);
	        mParamsWrp.put('objWrapperRsp', objWrapperRsp);
            mParamsWrp.put('idQuote', strIdQuote);
            mParamsWrp.put('bodyrsp', oResultSrv.get('body').toString());
            lstParamSD.add(mParamsWrp);
            paramsQuote = mParamsM;
        }
        fillSaveData(lstParamSD, lstDatPart, paramsQuote);
        return getDataCotiz(strOppoId);
    }
    
    /**
     * @description : Se encarga de obtener datos de la cotización
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    public static List<Object> getDataCotiz(String strOppoId) {
        final List<Object> oResponseValues = new List<Object>();
        final List<Quote> lstQuotes = MX_RTL_Quote_Selector.resultQryQuote('Id, QuoteToName', 'OpportunityId = \'' + strOppoId + '\'', true);
        for(Quote oRowQuote : lstQuotes) {
            final Set<Id> setQuoteId = new Set<Id>();
        	setQuoteId.add(oRowQuote.Id);
            final List<Cobertura__c> lstCovers = MX_RTL_Cobertura_Selector.findCoverByQuote('Id, MX_SB_VTS_RelatedQuote__c, MX_SB_MLT_Cobertura__c, MX_SB_MLT_Descripcion__c, MX_SB_VTS_CodeTrade__c, MX_SB_VTS_TradeValue__c, MX_SB_VTS_CategoryCode__c, MX_SB_VTS_GoodTypeCode__c, MX_SB_VTS_CoverageCode__c, MX_SB_VTS_PayList__c,  MX_SB_VTS_CoversAmount__c, MX_SB_MLT_SumaAsegurada__c, RecordType.DeveloperName, MX_SB_MLT_Estatus__c, MX_SB_VTS_RelatedQuote__r.Name', '', setQuoteId);
            final List<Object> lstMDPDet = new List<Object>();
            final List<Object> lstMCoverDet = new List<Object>();
            for(Cobertura__c oRowCover : lstCovers) {
            	if(oRowCover.MX_SB_MLT_Cobertura__c == SRTOBJNULL) {
                    final Map<String, Object> mCoverDet = new Map<String, Object>();
                    mCoverDet.put('CovRamo', oRowCover.MX_SB_VTS_CodeTrade__c);
                    mCoverDet.put('CovTrade', oRowCover.MX_SB_VTS_TradeValue__c);
                    mCoverDet.put('CovCat', oRowCover.MX_SB_VTS_CategoryCode__c);
                    mCoverDet.put('CovGoodT', oRowCover.MX_SB_VTS_GoodTypeCode__c);
                    mCoverDet.put('CovCode', oRowCover.MX_SB_VTS_CoverageCode__c);
                    mCoverDet.put('CovPayList', oRowCover.MX_SB_VTS_PayList__c);
                    mCoverDet.put('CovCoverAmo', oRowCover.MX_SB_VTS_CoversAmount__c);
                    mCoverDet.put('CovSumAse', oRowCover.MX_SB_MLT_SumaAsegurada__c);
                    mCoverDet.put('CovDevName', oRowCover.RecordType.DeveloperName);
                    mCoverDet.put('CovEtsatus', oRowCover.MX_SB_MLT_Estatus__c);
                    lstMCoverDet.add(mCoverDet);
            	} else {
                    final Map<String, Object> mDPDet = new Map<String, Object>();
                    mDPDet.put('DPCode', oRowCover.MX_SB_MLT_Cobertura__c);
                    mDPDet.put('DPId', oRowCover.MX_SB_MLT_Descripcion__c);
                    mDPDet.put('DPTrade', oRowCover.MX_SB_VTS_TradeValue__c);
                    lstMDPDet.add(mDPDet);
            	}
        	}
            if(!lstCovers.isEmpty()) {
                final Map<String, Object> mContCotiz = new Map<String, Object>();
                mContCotiz.put('DP',lstMDPDet);
            	mContCotiz.put('COV',lstMCoverDet);
                mContCotiz.put('TYPE',oRowQuote.QuoteToName);
            	oResponseValues.add(mContCotiz);
            }
        }
        return oResponseValues;
    }
    
    /**
     * @description : Realiza el consumo del servicio: createPrices enviando los datos a Clippert
     * @author      : Alexandro Corzo
     * @return      : Map<String, Object> oReturnValues
     */
    public static Map<String, Object> callInvokeService(String sJsonBody) {
        Map<String, Object> oReturnValues = null;
        try {
            final HttpResponse oResponseSrv = MX_RTL_IasoServicesInvoke_Selector.callServices('createPrices', new Map<String, Object>(), obtRequestSrv(sJsonBody));
            if(oResponseSrv.getStatusCode() == OKCODE) {
                final String strRspJSON = oResponseSrv.getBody().replaceAll('id',STRLBLIDDATA).replaceAll('currency','currencydata').replaceAll('parent','parentdata');
                final MX_SB_VTS_wrpPricesDataSrvRsp objWrapper = (MX_SB_VTS_wrpPricesDataSrvRsp) JSON.deserialize(strRspJSON, MX_SB_VTS_wrpPricesDataSrvRsp.class);
                final Map<String, Object> mStatusCode = new Map<String, Object>{'code' => String.valueOf(oResponseSrv.getStatusCode()), 'description' => 'Consumo de Servicio Exitoso!', 'content' => objWrapper, 'body' => JSON.serialize(objWrapper.data)};
                oReturnValues = mStatusCode;    
            } else {
                final Map<String, Object> mStatusCode = MX_SB_VTS_Codes_Utils.statusCodes(oResponseSrv.getStatusCode());
                oReturnValues = mStatusCode;
            }
        } catch (Exception e) {
			final Map<String, Object> mStatusCode = new Map<String, Object>{'code' => String.valueOf(SRVNOTAVAL), 'description' => 'El servicio no se encuentra disponible, intente nuevamente!!'};
            oReturnValues = mStatusCode;         
        }
        return oReturnValues;
    }
    
    /**
     * @description : Realiza el armado del objeto Request para el consumo del servicio ASO
     * @author      : Alexandro Corzo
     * @return      : HttpRequest oRequest
     */
    public static HttpRequest obtRequestSrv(String sCotizJSON) {
        final Map<String,iaso__GBL_Rest_Services_Url__c> allCodASOPri = iaso__GBL_Rest_Services_Url__c.getAll();
        final iaso__GBL_Rest_Services_Url__c objASOPrices = allCodASOPri.get('createPrices');
        final String strEndPntPri = objASOPrices.iaso__Url__c;
        final HttpRequest oRqstPri = new HttpRequest();
        oRqstPri.setTimeout(120000);
        oRqstPri.setMethod('POST');
        oRqstPri.setHeader('Content-Type', 'application/json');
        oRqstPri.setHeader('Accept', '*/*');
        oRqstPri.setHeader('Host', 'https://test-sf.bbva.mx');
        oRqstPri.setEndpoint(strEndPntPri);
        oRqstPri.setBody(sCotizJSON);
        return oRqstPri;
    }
    
    /**
     * @description : Llena coberturas y datos particulares en Salesforce
     * @author      : Alexandro Corzo
     * @return      : List<Object> lstCovers
     */
    public static void fillSaveData(List<Object> lstParamSD, List<Object> lstDatPartMod, Map<String, Object> mParamsM ) {
        for(Object oRowData : lstParamSD) {
        	final Map<String, Object> mParamsWrp = (Map<String, Object>) oRowData;
            final List<Object> lstDisCov = MX_SB_VTS_GetSetDatosPricesAux_Helper.fillDisasCover(mParamsWrp);
            final List<Object> lstCovers = MX_SB_VTS_GetSetDatosPricesAux_Helper.fillObjCovers(mParamsWrp);
            final List<Object> lstDatPart = MX_SB_VTS_GetSetDatosPricesAux_Helper.fillObjDatPart(mParamsWrp);
            final String strIdQuote = mParamsWrp.get('idQuote').toString();
            updDataDisCov(lstDisCov, strIdQuote);
            saveDataCovers(lstCovers, strIdQuote);
            saveDataDP(lstDatPart, strIdQuote, lstDatPartMod, mParamsM);
            String strData = mParamsWrp.get('bodyrsp').toString();
            strData = strData.replaceAll(STRLBLIDDATA, 'id').replaceAll('currencydata', 'currency');
            MX_SB_VTS_DeleteCoverageHSD_Services.processResponse(strData, strIdQuote);
        }
    }
    
    /**
     * @description : Actualiza Riesgos en Salesforce
     * @author      : Alexandro Corzo
     * @return      : None
     */
    public static void updDataDisCov(List<Object> lstDisCov, String strIdQuote) {
        final Quote objQuote = MX_RTL_Quote_Selector.findQuoteById(strIdQuote, 'Id');
        for(Object oRowDisCov : lstDisCov) {
            final Map<String, String> mDisCovData = (Map<String, String>) oRowDisCov;
            if(!String.isEmpty(mDisCovData.get('TERR'))) {
				objQuote.QuoteToPostalCode = mDisCovData.get('TERR');             
            }
            if(!String.isEmpty(mDisCovData.get('RIHI'))) {
                objQuote.ShippingPostalCode = mDisCovData.get('RIHI');
            }
        }
        MX_RTL_Quote_Selector.upsrtQuote(objQuote);
    }
    
    /**
     * @description : Almacena coberturas en Salesforce
     * @author      : Alexandro Corzo
     * @return      : None
     */
    public static void saveDataCovers(List<Object> lstCovers, String strIdQuote) {
        final List<Cobertura__c> lstDataCovers = new List<Cobertura__c>();
        String strQuoteID = null;
        for(object oRowCover : lstCovers) {
            final Map<String, Object> mCoverData = (Map<String, Object>) oRowCover;
            if(!''.equals(mCoverData.get(UNITSAMOUNTINS).toString()) && !'0'.equals(mCoverData.get(UNITSAMOUNTINS).toString())) {
                final Cobertura__c objCovers = new Cobertura__c();
                strQuoteID = mCoverData.get('QuoteID').toString();
                objCovers.MX_SB_VTS_RelatedQuote__c = strIdQuote;
                objCovers.MX_SB_VTS_CodeTrade__c = mCoverData.get('RamoID').toString();
                objCovers.MX_SB_VTS_TradeValue__c = mCoverData.get('TradeID').toString();
                objCovers.MX_SB_VTS_CategoryCode__c = mCoverData.get('CategID').toString();
                objCovers.MX_SB_VTS_GoodTypeCode__c = mCoverData.get('GoodTypeID').toString();
                objCovers.MX_SB_VTS_CoverageCode__c = mCoverData.get('CoversID').toString();
                objCovers.MX_SB_VTS_PayList__c = mCoverData.get('UnitsList').toString();
                objCovers.MX_SB_VTS_CoversAmount__c = mCoverData.get('UnitsAmount').toString();
                objCovers.MX_SB_MLT_SumaAsegurada__c = mCoverData.get(UNITSAMOUNTINS).toString() == '' ? 0.00 : Decimal.valueOf(mCoverData.get(UNITSAMOUNTINS).toString());
                objCovers.CurrencyIsoCode = 'MXN';
                objCovers.MX_SB_MLT_Estatus__c = 'Activo';
                objCovers.RecordTypeId = Schema.SObjectType.Cobertura__c.getRecordTypeInfosByDeveloperName().get('MX_SB_VTS_CoberturasASO').getRecordTypeId();
                lstDataCovers.add(objCovers);
            }
        }
		MX_RTL_Cobertura_Selector.upsertCoverages(lstDataCovers);
        MX_RTL_Quote_Selector.upsrtQuote(new Quote(Id = strIdQuote, MX_SB_VTS_ASO_FolioCot__c = strQuoteID));
    }
    
    /**
     * @description : Almacena datos particulares en Salesforce
     * @author      : Alexandro Corzo
     * @return      : None
     */
    public static void saveDataDP(List<Object> lstDatosP, String strIdQuote, List<Object> lstDatPart, Map<String, Object> mParamsM) {
        final String strNameCotiz = mParamsM.get('strCotiz').toString();
		final List<Cobertura__c> lstDataP = new List<Cobertura__c>();
        for(object oRowDP : lstDatosP) {
            final Map<String, String> mDPData = (Map<String, String>) oRowDP;
            if(FLDRENSTR.contains(strNameCotiz) && mDPData.get(FLDCRITSTRM).equalsIgnoreCase('2008PORCSAIC')) {
                for(Object oRowDataNew : lstDatPart) {
                    final Map<String, String> mDataPartN = (Map<String, String>)oRowDataNew;
                    if(mDataPartN.get(FLDCRITSTR).equalsIgnoreCase('2008PORCSAIC')) {
                        mDPData.remove(FLDCRITSTRM);
                        mDPData.remove(FLDIDDATASTRM);
                        mDPData.remove(FLDVALSTRM);
                        mDPData.put(FLDCRITSTRM, mDataPartN.get(FLDCRITSTR));
                        mDPData.put(FLDIDDATASTRM, mDataPartN.get(FLDIDDATASTR));
                        mDPData.put(FLDVALSTRM, mDataPartN.get(FLDVALSTR));
                        break;
                    }
                }
            }
            final Cobertura__c objCovers = new Cobertura__c();
            objCovers.MX_SB_VTS_RelatedQuote__c = strIdQuote;
            objCovers.MX_SB_VTS_TradeValue__c = mDPData.get('Value');
            objCovers.MX_SB_MLT_Cobertura__c = mDPData.get('Criterial');
            objCovers.MX_SB_MLT_Descripcion__c = mDPData.get('IdData');
            objCovers.CurrencyIsoCode = 'MXN';
            objCovers.MX_SB_MLT_Estatus__c = 'Activo';
            objCovers.RecordTypeId = Schema.SObjectType.Cobertura__c.getRecordTypeInfosByDeveloperName().get('MX_SB_VTS_ContractingCriteria').getRecordTypeId();
            lstDataP.add(objCovers);
        }
        MX_RTL_Cobertura_Selector.upsertCoverages(lstDataP);
    }
    
    /**
     * @description : Regenera Registros de Cotización asociados Oportunidad
     * @author      : Alexandro Corzo
     * @return      : Nothing
     */
    public static void reDataCotiz(String strOppoId, String strProRent) {
    	final List<Quote> lstQuotesDel = MX_RTL_Quote_Selector.resultQryQuote('Id, Name, QuoteToName', 'OpportunityId = \'' + strOppoId + '\'', true);
        MX_RTL_Quote_Selector.deletQuote(lstQuotesDel);
        MX_SB_VTS_InitQuo_Service.ctrDataQuote(strOppoId, strProRent);
    }
}