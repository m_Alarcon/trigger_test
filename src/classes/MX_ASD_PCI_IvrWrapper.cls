/****************************************************************************************************
Información general
------------------------
author: Omar Gonzalez
company: Ids
Project: PCI

Information about changes (versions)
-------------------------------------
Number    Dates           Author            Description
------    --------        ---------------   -----------------------------------------------------------------
1.0      07-08-2020      Omar Gonzalez      Clase Wrapper que se recibe IdCotiza y FolioAntifraude de una cotización 
****************************************************************************************************/
@SuppressWarnings('sf:AvoidGlobalModifier, sf:UseSingleton')
global without sharing class MX_ASD_PCI_IvrWrapper {
    
    /*
* Clase Wrapper que recibe los datos del service
*/
    @SuppressWarnings('sf:AvoidGlobalModifier')
    global class MX_ASD_PCI_DatosCotizacion {
        /* llamada a variable folioDeCotizacion del servicio REST*/
        public String  folioDeCotizacion {get;set;}
        /* llamada a variable idCotiza del servicio REST*/
        public String  idCotiza {get;set;}
        /* llamada a variable folioAntiFraude del servicio REST*/
        public String  folioAntiFraude {get;set;}
        /* llamada a variable producto del servicio REST*/
        public String producto {get;set;}
        /* llamada a variable para el proveedor */
        public String idproveedor {get;set;}
    }
    /*
* Clase Wrapper para respuesta del service
*/
    @SuppressWarnings('sf:AvoidGlobalModifier')
    global class MX_ASD_PCI_ResponseSFDC {
        /* displays error message if something fails*/
        public String error { get; set; }
        /* success message*/
        public String message { get; set; }
        /* account Id returned by the service*/
        public String quoteId { get; set; }
    }
}