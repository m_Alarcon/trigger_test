/**
*
* @author Daniel Perez Lopez 
* @description Generador de mock para servicios presuscritos
*
*           No  |     Date     |     Author      |    Description
* @version  1.0    09/08/2020     Daniel Perez         Creación
*
*/
@SuppressWarnings('sf:AvoidGlobalModifier')
/*
* clase mock para servicios 
*/
@isTest
global class MX_SB_PS_IntegrationServiceMock implements HttpCalloutMock {
    /* 
    *Variables  para method en response
    */
    Static Final string TXTPOST='POST', TXTSESSION = '**************sessionToken*************';
    /*
    * Method for response element to mock services
    */
    global HTTPResponse respond(HTTPRequest req) {
        
        // Optionally, only send a mock response for a specific endpoint
        final HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        
        switch on req.getEndpoint() {
            when 'https://150.250.220.36:18500/corporateCatalogs' {
                System.assertEquals('https://validation/ok', req.getEndpoint());
                System.assertEquals(TXTPOST, req.getMethod());
                res.setHeader('access_token', TXTSESSION);
                res.setHeader('tsec', TXTSESSION);
                res.setBody('{}');
                res.setStatusCode(200);
            }

            when 'https://150.250.220.36:18500/lifeInsurances' {
                System.assertEquals('https://validation/ok', req.getEndpoint());
                System.assertEquals(TXTPOST, req.getMethod());
                res.setHeader('access_token', TXTSESSION);
                res.setHeader('tsec', TXTSESSION);
                res.setBody('{}');
                res.setStatusCode(200);
            }
            
            when else {
                //nothing
                throw new DmlException('req.exeption getpoin'+req.getEndpoint());
            }
        }
        return res;
    }
}