/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase Selector para objeto User

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     20/05/2020      Roberto Isaac Soto Granados           Creación Clase
*   1.1     06/06/2020      Gabriel Garcia Rojas                  Metodos getListUserById
                                                                  getUserByDivisionSucursal
    1.2     29/06/2020      Jair Ignacio Gonzalez Gayosso         Se agrega campo a getListUserById
*   1.2     13/07/2020      Jose Angel Guerrero                   Método getUsersByHint
*****************************************************************************************/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_RTL_User_Selector {

    /*Consulta los subordinados del director de oficina*/
    public static List<User> directorIds(Set<Id> ownerIds) {
        return [SELECT Id FROM User WHERE Director_de_oficina__c IN: ownerIds];
    }
    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param userId
    * @return List<User>
    **/
    public static List<User> getListUserById(Set<Id> userId) {
    	return [SELECT Id, UserRole.DeveloperName, Name, DivisionFormula__c, BPyP_ls_NombreSucursal__c, Profile.Name, CallCenterId
                                                 FROM User
                                                 WHERE Id =: userId
                                                 ORDER BY Name ASC NULLS LAST];
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param divisiones
    * @param sucursales
    * @return List<User>
    **/
    public static List<User> getUserByDivisionSucursal(Set<String> divisiones, Set<String> sucursales) {
        return [SELECT Id, UserRole.DeveloperName, Name
                FROM User
                WHERE DivisionFormula__c IN: divisiones AND
                BPyP_ls_NombreSucursal__c IN: sucursales
                ORDER BY Name ASC NULLS LAST];
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param queryfields
    * @param queryFilters
    * @param nameDivision
    * @param nameOffice
    * @param banca
    * @param namePerfil
    * @return List<User>
    **/
    @SuppressWarnings('sf:DUDataflowAnomalyAnalysis, sf:UnusedLocalVariable')
    public static List<User> fetchListUserByDataBase(String queryfields, String queryFilters, List<String> listFilters) {
        List<String> listToFilters = new List<String>(4);
        Integer posicion = 0;
        for(String filtro : listFilters) {
            listToFilters[posicion] = filtro;
            posicion++;
        }

        final String filtro0 = listToFilters[0];
        final String filtro1 = listToFilters[1];
        final String filtro2 = listToFilters[2];
        final String filtro3 = listToFilters[3];
        return Database.query('SELECT ' + String.escapeSingleQuotes(queryfields) + ' FROM User ' + String.escapeSingleQuotes(queryFilters));
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param fields, condiciones, boolean, id
    * @return List<User>
    **/
    public static List<User> resultQueryUser(String queryfield,String conditionField, boolean whitCondition, Set<Id> setId) {
        List<User> resultQueryUsers;
        String query;
        switch on String.valueOf(whitCondition) {
            when 'false' {
                query='Select '+ queryfield +' from user';
            }
            when 'true' {
                query='Select '+ queryfield +' from user where '+conditionField;
            }
        }
        resultQueryUsers = Database.query(String.escapeSingleQuotes(query));
        return resultQueryUsers;
    }
     /**
* @description: funcion que trae busqueda de usuario.
* @author Jose Angel Gurerrero.
*/
    public static User[] getUsersByHintYProfile (String nombre,String profile) {
        final list <String> idsProfile = new list <String> ();
        final List<Profile> profileIDs=[Select ID from Profile where name like :profile];
        for (Profile prof : profileIDs) {
            idsProfile.add(prof.id);
        }
        return [SELECT Id, Name, FederationIdentifier, Email from User
                where isActive = TRUE AND
                profileid in:idsProfile AND
                (Name LIKE: nombre or FederationIdentifier LIKE: nombre or Email LIKE: nombre) LIMIT 500];
    }
    /**
* @description: funcion que trae busqueda de usuario.
* @author Jose Angel Gurerrero.
*/
    public static User[] getUsersByProfile (String profile) {
        final list <String> idsProfile = new list <String> ();
        final List<Profile> profileIDs=[Select ID from Profile where name like :profile];
        for (Profile prof : profileIDs) {
            idsProfile.add(prof.id);
        }
        return [SELECT  Id, Name, FederationIdentifier, Email, userrole.name FROM user where profileid in:idsProfile and isactive=true limit 500];
    }

    /**
    * @description Recupera usuarios Implant asignados en los proveedores de Call Center
    * @author Eduardo Hernandez Cuamatzi | 08-20-2020 
    * @param Set<String> proveeNames Proveedores a recuperar
    * @return List<User> Lista de Implants
    **/
    public static List<User> lstImplants(Set<String> proveeNames) {
        return [Select Id, Name, MX_SB_VTS_ProveedorCTI__c from User where MX_SB_VTS_ProveedorCTI__c IN: proveeNames AND MX_SB_VTS_ImplantConnected__c = TRUE AND IsActive = TRUE];
    }
}