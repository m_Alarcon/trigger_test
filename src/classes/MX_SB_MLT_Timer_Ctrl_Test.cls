@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_Timer_Ctrl_Test
* Autor Juan Carlos Benitez Herrera
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_Timer_Ctrl

* --------------------------------------------------------------------------------
* Versión       Fecha                  Autor                        Descripción
* --------------------------------------------------------------------------------
* 1.0           01/06/2020      Juan Carlos Benitez Herrera          Creación
* --------------------------------------------------------------------------------
*/
public class MX_SB_MLT_Timer_Ctrl_Test {
        /*
	* var String Valor Subramo Ahorro
	*/
    public static final String SUBRAMO ='Ahorro';
    /*
	* var String Valor boton cancelar
	*/    
    public static final String CANCELAR = 'Cancelar';
    /*
	* var String Valor boton Volver
	*/    
    public static final String VOLVER = 'Volver';
    /*
	* var String Valor boton Crear
	*/    
    public static final String CREAR = 'Crear';
    
	/** Nombre de usuario Test */
    final static String NAME = 'Monkey D. Luffy';
    
    /** current user id */
    final static String USR_ID = UserInfo.getUserId();
    /**
     * @description
     * @author Juan Carlos Benitez | 5/26/2020
     * @return void
     **/
    @TestSetup
    static void createData() {
        Final String profileVid = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User userVid = MX_WB_TestData_cls.crearUsuario(NAME, profileVid);  
        insert userVid;
        System.runAs(userVid) {
            final String recordTpeIde = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Proveedores_MA).getRecordTypeId();
            final Account accountVidaTest = new Account(RecordTypeId = recordTpeIde, Name=NAME, Tipo_Persona__c='Física');
            insert accountVidaTest;

            final Contract contractVida = new Contract(AccountId = accountVidaTest.Id, MX_SB_SAC_NumeroPoliza__c='policyTest');
            insert contractVida;

            Final List<Siniestro__c> sinInsertVid = new List<Siniestro__c>();
            for(Integer l = 0; l < 10; l++) {
                Final Siniestro__c nSin = new Siniestro__c();
                nSin.MX_SB_MLT_NombreConductor__c = NAME + '_' + l;
                nSin.MX_SB_MLT_APaternoConductor__c = 'LastName for '+NAME;
                nSin.MX_SB_MLT_AMaternoConductor__c = 'LastName2 for '+NAME;
                nSin.MX_SB_SAC_Contrato__c = contractVida.Id;
                nSin.MX_SB_MLT_Fecha_Hora_Siniestro__c =date.valueof('2020-03-27T22:04:00.000+0000');
                nSin.MX_SB_MLT_Telefono__c = '5534253647';
                nSin.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_creacion;
                Final datetime citaAgenda = datetime.now();
                nSin.MX_SB_MLT_CitaVidaAsegurado__c = citaAgenda.addDays(2);
                nSin.MX_SB_MLT_AtencionVida__c = 'Agendar Cita';
                nSin.TipoSiniestro__c = 'Siniestros';
                nSin.MX_SB_MLT_PreguntaVida__c = false;
                nSin.MX_SB_MLT_SubRamo__c = SUBRAMO;
                nSin.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoVida).getRecordTypeId();
                sinInsertVid.add(nSin);
            }
            Database.insert(sinInsertVid);
        }
    }
    @isTest
    static void testonSuccess() {
       final string recId=[SELECT Id from Siniestro__c WHERE MX_SB_MLT_NombreConductor__c LIKE: '%' + NAME + '%'+ '_3' ].Id;
        test.startTest();
        	MX_SB_MLT_Timer_Ctrl.fetchSinCrtDate(recId);
        	MX_SB_MLT_Timer_Ctrl.fetchSinLastMDate(recId);
        system.assert(True,'Success');
        test.stopTest();
    }
    @isTest
    static void testonError() {
        Test.startTest();
        try {
            MX_SB_MLT_Timer_Ctrl.fetchSinCrtDate('');
        } catch(Exception e) {
      		System.assertNotEquals('Invalid id:',e.getMessage(),'Fallo con exito');
        } 
        Test.stopTest();
    }
}