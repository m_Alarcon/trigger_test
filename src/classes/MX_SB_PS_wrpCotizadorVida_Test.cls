/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_PS_wrpCotizadorVidaResp_Test
* Autor Daniel Perez Lopez
* Proyecto: Salesforce Presuscritos
* Descripción : Prueba los Methods de la clase MX_SB_PS_wrpCotizadorVidaResp

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           10/12/2019      Daniel Lopez                         Creación
* --------------------------------------------------------------------------------
*/
@IsTest
 @SuppressWarnings('sf:ShortVariable')
public with sharing class MX_SB_PS_wrpCotizadorVida_Test {
    @IsTest
    static void obtieneWraper() {
        final MX_SB_PS_wrpCotizadorVida miclas = new MX_SB_PS_wrpCotizadorVida();
        miclas.id='';
        miclas.region='';
        final MX_SB_PS_wrpCotizadorVida.catalogItemBase catalogItemBase=  new MX_SB_PS_wrpCotizadorVida.catalogItemBase();
        catalogItemBase.id='';
        final MX_SB_PS_wrpCotizadorVida.coverages coverages = new MX_SB_PS_wrpCotizadorVida.coverages();
        coverages.catalogItemBase=catalogItemBase;
        coverages.peopleNumber='';
        final MX_SB_PS_wrpCotizadorVida.insuredList insuredList = new MX_SB_PS_wrpCotizadorVida.insuredList();
        insuredList.coverages= new MX_SB_PS_wrpCotizadorVida.coverages[] {};
        final MX_SB_PS_wrpCotizadorVida.transformer transformer = new MX_SB_PS_wrpCotizadorVida.transformer('', '');
        transformer.id='';
        transformer.name='';
        final MX_SB_PS_wrpCotizadorVida.particularData particularData= new MX_SB_PS_wrpCotizadorVida.particularData('','','','');
        particularData.aliasCriterion='';
        particularData.transformer=transformer;
        particularData.peopleNumber='';
        final MX_SB_PS_wrpCotizadorVida.paymentWay paymentWay= new MX_SB_PS_wrpCotizadorVida.paymentWay();
        paymentWay.id='';
        paymentWay.name='';
        final MX_SB_PS_wrpCotizadorVida.rateQuote rateQuote = new MX_SB_PS_wrpCotizadorVida.rateQuote();
        rateQuote.paymentWay=paymentWay;
        final MX_SB_PS_wrpCotizadorVida.type type = new MX_SB_PS_wrpCotizadorVida.type('','');
        type.id='';
        type.name='';
        final MX_SB_PS_wrpCotizadorVida.validityPeriod validityPeriod = new MX_SB_PS_wrpCotizadorVida.validityPeriod();
        validityPeriod.startDate='';
        validityPeriod.endDate='';
        final MX_SB_PS_wrpCotizadorVida.productPlan productPlan = new MX_SB_PS_wrpCotizadorVida.productPlan();
            productPlan.productCode='';
            productPlan.planReview='';
            productPlan.bouquetCode='';
        Test.startTest();
            System.assertEquals(miclas.technicalInformation.technicalChannel, '4','Exito');
        Test.stopTest();
    }
    
}