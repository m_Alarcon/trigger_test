/**
* @FileName          : MX_SB_VTS_GenerarOTP_Service
* @description       : Service class for use de web service of generarOTP
* @Author            : Marco Antonio Cruz Barboza  
* @last modified on  : 02-09-2020
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   02-09-2020   Marco Antonio Cruz Barboza          Initial Version
* 1.1   02-22-2021   Eduardo Hernández Cuamatzi          Se agrega token antifraude a consumo de otp
**/

@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton, sf:AvoidGlobalModifier')
Global class MX_SB_VTS_GenerarOTP_Service {

    /*String to get phone number*/
    private static String getNumber;

    /**
    * @description : Call an web service apex method to get de Status from OTP Service
    * @Param String Phone
    * @Return Map<String, String> with status and code from the result of webservice use.
    **/
    public static Map<String,String> getStatusOTP(String phone, Quote quoteData, String tokenId) {
        Final Map<String,String> getValues = new Map<String,String>();
        Final String OtpService = JSON.serialize(new Map<String,Object> {
            'PHONE' => phone, 'IDQUOTE' => quoteData.MX_SB_VTS_ASO_FolioCot__c, 'TOKENID' => tokenId
        });
        final HttpResponse getCodeStatus = MX_RTL_IasoServicesInvoke_Selector.callServices('generarOTP', OtpService);
        getValues.put('Code', String.valueOf(getCodeStatus.getStatusCode()));
        getValues.put('Status', getCodeStatus.getStatus());
        return getValues;
    }
	
    /**
    * @description : Search a phone from the selected opportunity record
    * @Param String recordId, String fields
    * @Return String getNumber
    **/
    public static String getPhone(String recordId, String fields) {
        Final Opportunity getOppor = MX_RTL_Opportunity_Selector.getOpportunity(recordId, fields);
        getNumber = getOppor.TelefonoCliente__c;
        return getNumber;
    }
}