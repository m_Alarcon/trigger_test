@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_SiniestroVida_tst
* Autor Marco Antonio Cruz Barboza
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_SiniestroVida_cls

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0           03/04/2020        Marco Cruz                        Creación
* 1.1           18/04/2020     Juan Carlos Benítez Herrera    Cobertura methodos Validacion Docs.
* 1.2			22/04/2020	  	Eder Alberto Hernández C.	  Cobertura a methodo Información del Fallecido
* -----------------------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_SiniestroVida_tst {
    /*
	* var String Valor boton cancelar
	*/    
    public static final String CANCELAR = 'Cancelar';
    /*
	* var String Valor boton Volver
	*/    
    public static final String VOLVER = 'Volver';
    /*
	* var String Valor boton Crear
	*/    
    public static final String CREAR = 'Crear';
    /*
	* var String Valor Subramo Ahorro
	*/
    public static final String SUBRAMO ='Ahorro';
    /*
	* var String Valor Subramo Fallecimiento
	*/
    public static final String SUBRAMOF ='Fallecimiento';
    /*
	* var String Valor Producto
	*/
    public static final string PRODUCT ='Familia Segura';
    /*
	* var String Valor subtipo Natural
	*/
	public static final String SUBTYPE='Natural';
    /*
	* var String Valor regla 1
	*/
    public static final String REGLA1='más/= de 2 años fallecimiento';
    /*
	* var String Valor regla 2
	*/
    public static final String REGLA2= '-';

    @TestSetup
    static void createData() {
        Final String profileTest = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User userTest = MX_WB_TestData_cls.crearUsuario('PruebaAdminTst', profileTest);  
        insert userTest;
        System.runAs(userTest) {
            final String  idRecordType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Proveedores_MA).getRecordTypeId();
            final Account accTestVida = new Account(RecordTypeId = idRecordType, Name='Usuario Test',Tipo_Persona__c='Física');
            insert accTestVida;
            final Contract contractTest = new Contract(AccountId = accTestVida.Id, MX_SB_SAC_NumeroPoliza__c='policyTest');
            insert contractTest;
            Final Siniestro__c siniVidaTest = new Siniestro__c();
            siniVidaTest.MX_SB_MLT_Telefono__c = '5534253647';
            siniVidaTest.MX_SB_MLT_NombreConductor__c = 'JUAN ALBERTO';
            siniVidaTest.MX_SB_MLT_APaternoConductor__c = 'TELLEZ';
            siniVidaTest.MX_SB_MLT_AMaternoConductor__c = 'ALCANTARA';
            siniVidaTest.MX_SB_SAC_Contrato__c = contractTest.Id;
            siniVidaTest.MX_SB_MLT_Fecha_Hora_Siniestro__c =date.valueof('2020-03-27T22:04:00.000+0000');
            Final datetime citaAgenda = datetime.now();
            siniVidaTest.MX_SB_MLT_CitaVidaAsegurado__c = citaAgenda.addDays(2);
            siniVidaTest.MX_SB_MLT_AtencionVida__c = 'Agendar Cita';
            siniVidaTest.MX_SB_MLT_PreguntaVida__c = false;
            siniVidaTest.MX_SB_MLT_SubRamo__c = SUBRAMO;
            siniVidaTest.TipoSiniestro__c = 'Siniestros';
            siniVidaTest.MX_SB_MLT_JourneySin__c = label.MX_SB_MLT_Etapa_creacion;
            siniVidaTest.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoVida).getRecordTypeId();
            insert siniVidaTest;
        }
    }
		/*
    * @description method que prueba MX_SB_MLT_SiniestroVida_Controller searchPolicy
    * @param  void
    * @return String 
    */
    @isTest
    static void busquedaPolizaTest () {
        Final String idRTAccount = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Proveedores_MA).getRecordTypeId();
        Final String idCuentaTest = [SELECT Id FROM Account WHERE Name = 'Usuario Test' AND RecordTypeId =: idRTAccount].Id;
        Final String policyTest = [SELECT MX_SB_SAC_NumeroPoliza__c FROM Contract WHERE Account.id =: idCuentaTest].MX_SB_SAC_NumeroPoliza__c;
        Final String contratoIdTest = [SELECT Id FROM Contract WHERE MX_SB_SAC_NumeroPoliza__c=:policyTest].Id;
        test.startTest();
        Final String idContratoReturn = MX_SB_MLT_SiniestroVida_Controller.searchPolicy(policyTest);
        test.stopTest();
        system.assertEquals(contratoIdTest, idContratoReturn, 'los valores coinciden');
    }
    	/*
    * @description method que prueba MX_SB_MLT_SiniestroVida_Controller taskInsertion
    * @param  void
    * @return String 
    */
    @isTest
    static void crearAgendaTest () {
        Final String recordTypeTestId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoVida).getRecordTypeId();
        Final Siniestro__c siniestroVidaTest = [SELECT Id, RecordTypeId, MX_SB_MLT_Telefono__c, MX_SB_MLT_CitaVidaAsegurado__c,MX_SB_MLT_SubRamo__c, 
                                              TipoSiniestro__c FROM Siniestro__c WHERE RecordTypeId=:recordTypeTestId];
        Final Task agendaTest = new Task();
        agendaTest.WhatId = siniestroVidaTest.Id;
        agendaTest.Telefono__c = siniestroVidaTest.MX_SB_MLT_Telefono__c;
        agendaTest.ReminderDateTime = siniestroVidaTest.MX_SB_MLT_CitaVidaAsegurado__c;
        agendaTest.IsReminderSet = true;
        agendaTest.Subject = 'Cita Agendada Test';
        test.startTest();
        	MX_SB_MLT_SiniestroVida_Controller.taskInsertion(agendaTest);
        test.stopTest();
        System.assert(true,'Se ha creado la agenda');
    }
    	/*
    * @description method que prueba MX_SB_MLT_SiniestroVida_Controller changeStep
    * @param  void
    * @return String 
    */
    @isTest
    static void changeStepSuccess() {
        Final String sinStep =[Select Id,MX_SB_MLT_JourneySin__c FROM Siniestro__c where MX_SB_MLT_Subramo__c='Ahorro' limit 1].Id;
        test.startTest();
        	MX_SB_MLT_SiniestroVida_Controller.changeStep(CANCELAR, '', sinStep);
            MX_SB_MLT_SiniestroVida_Controller.changeStep(CREAR, '', sinStep);
            MX_SB_MLT_SiniestroVida_Controller.changeStep(VOLVER, '', sinStep);
        test.stopTest();
        System.assert(true, 'Cambio de Etapa exitoso!');
    }
    	/*
    * @description method que prueba MX_SB_MLT_SiniestroVida_Controller camposSin
    * @param  void
    * @return String 
    */
    @isTest
    static void camposSiniestro () {
        Final String siniestroTst = [SELECT Id FROM Siniestro__c where MX_SB_MLT_SubRamo__c = 'Ahorro'].Id;
        test.startTest();
        MX_SB_MLT_SiniestroVida_Controller.camposSin(siniestroTst);
        MX_SB_MLT_SiniestroVida_Controller.searchSinVida(siniestroTst);
        test.stopTest();
        system.assert(true, 'Completado');
    }
    /*
    * @description method que prueba MX_SB_MLT_SiniestroVida_Controller searchRecordType
    * @param  void
    * @return String 
    */
    @isTest
    static void srchRTSccss() {
        final string sinrec = [SELECT Id,recordTypeId FROM Siniestro__c where MX_SB_MLT_SubRamo__c = 'Ahorro' limit 1].Id;
		test.startTest();
        	MX_SB_MLT_SiniestroVida_Controller.searchRecordType(sinrec);
        test.stopTest();
        system.assert(true, 'Tipo de registro encontrado');
    }
	/*
    * @description method que prueba MX_SB_MLT_SiniestroVida_Controller historyFields
    * @param  void
    * @return String 
    */
    @isTest
    static void historySin() {
        final String recordId = [SELECT Id FROM Siniestro__c where MX_SB_MLT_SubRamo__c = 'Ahorro' limit 1].Id;
           test.startTest();
        		MX_SB_MLT_SiniestroVida_Controller.historyFields(recordId);
           test.stopTest();
        system.assert(true, 'Tracking exitoso');
    }
          /*
    * @description method que prueba MX_SB_MLT_SiniestroVida_Controller searchkit
    * @param  void
    * @return String 
    */
    @IsTest
    static void srchktSccErr() {
        final String idSin = [SELECT Id FROM Siniestro__c where MX_SB_MLT_SubRamo__c = 'Ahorro' limit 1].Id;
        test.startTest();
        final Map<String,String> mapa= new Map<String,String>();
        mapa.put('product',PRODUCT);
        mapa.put('subramo',SUBRAMOF);
        mapa.put('subtipo',SUBTYPE);
        mapa.put('regla1',REGLA1);
        mapa.put('regla2',REGLA2);
        mapa.put('idSin',idSin);
	        MX_SB_MLT_SiniestroVida_Controller.searchkit(mapa);
		test.stopTest();
        system.assert(true, 'Lista de documentos encontrada');
    }
}