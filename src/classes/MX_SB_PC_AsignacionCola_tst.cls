/**
* Name: MX_SB_PC_AsignacionCola_tst
* @author Jose angel Guerrero
* Description : Clase Test para Controlador de componente Lightning de asignación de Colas
*
* Ver              Date            Author                   Description
* @version 1.0     Jul/13/2020     Jose angel Guerrero      Initial Version
*
**/

@isTest
public class MX_SB_PC_AsignacionCola_tst {
    /**
* @description: funcion para guardar Asignaciones
*/
    public Final Static String IDUSUARIO = 'id1';
    /**
* @description: funcion para guardar Asignaciones
*/
    public Final Static String IDCOLA= 'id2';
    /**
* @description: funcion para guardar Asignaciones
*/
    public Final Static boolean SEASIGNA = true;
    /**
* @description: funcion para guardar Asignaciones
*/
    public Final Static boolean SEASIGNA2 = false;
    /**
* @description: funcion para guardar Asignaciones
*/
    public final Static boolean SINASIGNACIONES = true;
    /**
* @description: funcion para guardar Asignaciones
*/
    public Final Static String NUEVASCOLAS = 'nuevasColas';
    /**
* @description: funcion para guardar Asignaciones
*/
    public Final Static String DESTINOS = 'destinos';
    /**
* @description: funcion para guardar Asignaciones
*/
    public Final Static String ORIGENES = 'origenes';
    /**
* @description: funcion para guardar Asignaciones
*/
    public Final Static String LASCOLAS = 'lascolas';
     /**
* @description: Mensaje para assert
*/
    public Final Static String MENSAJEASSERT = 'Exito al guardar';
    
    /**
* @description: funcion constructor
*/
    @isTest
    public static void buscarUsuarios() {
        final User[] usrs = MX_SB_PC_AsignacionCola_ctr.buscarUsuarios('test1');
        test.startTest();
        System.assertEquals(usrs.size(), 0, 'aprobado ');
        test.stopTest();
    }
    /**
* @description: funcion constructor
*/
    @isTest
    public static void grupoColas() {
        final Group[] usrs = MX_SB_PC_AsignacionCola_ctr.grupoColas();
        test.startTest();
        System.assertEquals(usrs.size(), 0, 'aprobado ');
        test.stopTest();
    }
    /**
* @description: funcion constructor
*/
    @isTest
    public static void buscarColas() {
        final list <String > listaId = new list <String >();
        listaId.add('test');
        final GroupMember[] usrs = MX_SB_PC_AsignacionCola_ctr.buscarColas(listaId);
        test.startTest();
        System.assertEquals(usrs.size(), 0, 'exito');
        test.stopTest();
    }
    /**
* @description: funcion constructor
*/
    @isTest
    public static void guardarAsignaciones() {
        final boolean exito = MX_SB_PC_AsignacionCola_ctr.guardarAsignaciones(IdUsuario,IdCola,false);
        test.startTest();
        System.assert(exito, true);
        test.stopTest();
    }
    /**
* @description: funcion constructor
*/
    @isTest
    public static void buscarMiembroCola() {
        final List <GroupMember> miebroGrupo = new list <GroupMember>();
        final List <GroupMember> exito = MX_SB_PC_AsignacionCola_ctr.buscarMiembroCola(NUEVASCOLAS);
        test.startTest();
        System.assertEquals(exito, miebroGrupo, 'SUCESS');
        test.stopTest();
    }
    /**
* @description: funcion constructor
*/
    @isTest
    public static void guardarNuevasAsignaciones() {
        final List <String> userIds = new list <String> {'usuarios'};
        final boolean exito = !MX_SB_PC_AsignacionCola_ctr.guardarNuevasAsignaciones(userIds,ORIGENES,DESTINOS);
        test.startTest();
        System.assert(exito,MENSAJEASSERT);
        test.stopTest();
    }
    /**
* @description: funcion constructor
*/
    @isTest
    public static void guardarAsignacionesTst() {
        final boolean exito = !MX_SB_PC_AsignacionCola_ctr.guardarAsignaciones(IdUsuario,IdCola,true);
        test.startTest();
        System.assert(exito,MENSAJEASSERT);
        test.stopTest();
    }
    /**
* @description: funcion constructor
*/
    @isTest
    public static void buscarUsuariosRole() {
        MX_SB_PC_AsignacionCola_ctr.buscarUsuariosRole(NUEVASCOLAS);
        test.startTest();
        System.assert(sinAsignaciones, 'true');
        test.stopTest();
    }
    /**
* @description: funcion constructor
*/
    @isTest
    public static void buscarUsuariosRoleTwo() {
        final List <User> usuarioRol = new list <User>();
        final List <User> exito = MX_SB_PC_AsignacionCola_ctr.buscarUsuariosRole('noAsignada');
        test.startTest();
        System.assert(exito != usuarioRol, 'true');
        test.stopTest();
    }
}