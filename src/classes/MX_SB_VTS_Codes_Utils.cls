/**
* @description       : 
* @author            : Diego Olvera
* @group             : 
* @last modified on  : 10-20-2020
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   10-13-2020   Diego Olvera   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_Codes_Utils {
     /**
    * @description Actualiza valores de lead u Oportunidad para flujo Seguro Estudia
    * @author Diego Olvera | 25/09/2020 
    * @param String leadId recuperado de componente MX_SB_VTS_CloseWon
    * @return void
    **/
    public static Map<String, Object> statusCodes(Integer code) {
        Map<String, Object> mStatusResponse = new Map<String, Object>();
        final Map<Integer, String> statusCode = new Map<Integer, String>();
        String codeFinal;
        statusCode.put(400, 'Error al validar la información de salida: {0}.');
        statusCode.put(401, 'No autorizado');
        statusCode.put(403, 'TSEC caducado');
        statusCode.put(404, 'Failed : HTTP error code : <!doctype html><html lang=\"en\"><head><title>HTTP Status 404 – Not Found</title>');
        statusCode.put(405, 'Method Not Allowed al agregar un parametro al endpoint solo si es pathparam ');
        statusCode.put(409, 'Failed : HTTP error code : {\"errorCode\":1001,\"errorDescription\":\"Parameters cant be null');
        statusCode.put(500, 'Servicio no disponible');
        statusCode.put(503, 'Falló el servidor');
        if(statusCode.containskey(code)) {
            codeFinal = statusCode.get(code);
            mStatusResponse = new Map<String, Object>{'code' => code, 'description' => codeFinal};
        }
        return mStatusResponse;
    }
}