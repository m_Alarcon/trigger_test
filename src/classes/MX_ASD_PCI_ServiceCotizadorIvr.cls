/****************************************************************************************************
Información general
------------------------
author: Omar Gonzalez
company: Ids
Project: PCI

Information about changes (versions)
-------------------------------------
Number    Dates           Author            Description
------    --------        ---------------   -----------------------------------------------------------------
1.0      07-08-2020      Omar Gonzalez      Clase que se utiliza para guardar IdCotiza y FolioAntifraude de una cotización 
****************************************************************************************************/
@RestResource(urlMapping='/IvrCobro/*')
global without sharing class MX_ASD_PCI_ServiceCotizadorIvr {
    /**
* Valor para Opportunity OppCobro 
*/
    private static List<Quote> cotizacion;
    /**
* Variable para limpar la respuesta de IVR 
*/
    private static String resClear='';
    /**
* Constructor privado
*/
    private MX_ASD_PCI_ServiceCotizadorIvr() { }
    /**
* Clase para almacenar IdCotiza y FolioAntifraude
*/
    @HttpPost
    global static List<MX_ASD_PCI_IvrWrapper.MX_ASD_PCI_ResponseSFDC> serviceEnlaceIvr(MX_ASD_PCI_IvrWrapper.MX_ASD_PCI_DatosCotizacion sendCobro) {
        final List<MX_ASD_PCI_IvrWrapper.MX_ASD_PCI_ResponseSFDC> resSFDC = new List<MX_ASD_PCI_IvrWrapper.MX_ASD_PCI_ResponseSFDC>();
        MX_ASD_PCI_IvrWrapper.MX_ASD_PCI_ResponseSFDC res ; 
        try {
            cotizacion = MX_RTL_Quote_Service.getQuoteData(sendCobro.folioDeCotizacion,sendCobro.producto);
            if(cotizacion.isEmpty() || sendCobro.idCotiza == NULL || sendCobro.folioAntiFraude == NULL) {
                res = new MX_ASD_PCI_IvrWrapper.MX_ASD_PCI_ResponseSFDC();
                res.message = 'Error los datos son incorrectos';
                resSFDC.add(res);
            } else {
                final List<Quote> cotList = new List<Quote>(); 
                for(Quote cotIvr:cotizacion) {
                    res = new MX_ASD_PCI_IvrWrapper.MX_ASD_PCI_ResponseSFDC();
                    cotIvr.MX_SB_VTS_Folio_Cotizacion__c = sendCobro.folioDeCotizacion;
                    cotIvr.MX_ASD_PCI_IdCotiza__c = sendCobro.idCotiza;
                    cotIvr.MX_ASD_PCI_FolioAntifraude__c = sendCobro.folioAntiFraude;
                    cotIvr.MX_ASD_PCI_ResponseCode__c = resClear;
                    cotIvr.MX_ASD_PCI_MsjAsesor__c = resClear;
                    cotIvr.Opportunity.Campanya__c = resClear;
                    cotIvr.MX_ASD_PCI_DescripcionCode__c = sendCobro.idproveedor;
                    res.quoteId=cotIvr.Id;
                    res.message = 'Exitoso los datos se almacenarón correctamente '+cotizacion[0].Id;
                    resSFDC.add(res);
                    cotList.add(MX_RTL_Quote_Service.upserQuote(cotIvr));
                }
            }
        } catch(Exception e) {
            res = new MX_ASD_PCI_IvrWrapper.MX_ASD_PCI_ResponseSFDC();
            res.error = e.getMessage()+' '+ e.getLineNumber();
            res.message = 'No se encontro ninguna cotizacion';
            resSFDC.add(res);
        }
        return resSFDC;
    }
}