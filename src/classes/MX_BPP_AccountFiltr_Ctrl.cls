/**
* @File Name          : MX_BPP_AccountFiltr_Ctrl.cls
* @Description        :
* @Author             : Jair Ignacio Gonzalez G
* @Group              :
* @Last Modified By   : Jair Ignacio Gonzalez G
* @Last Modified On   : 19/10/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      19/10/2020        Jair Ignacio Gonzalez G            Initial Version
**/
public with sharing class MX_BPP_AccountFiltr_Ctrl {
    /**Constructor */
    @testVisible
    private MX_BPP_AccountFiltr_Ctrl() { }

    /**
    * @description Obtiene estructura de jerarquía referente al usuario
    * @author Gabriel Garcia Rojas
    * @return
    **/
    @AuraEnabled
    public static MX_BPP_LeadStart_Service.InfoTabCampaignStartWrapper fetchInfoByUserAcc() {
        return MX_BPP_LeadStart_Service.userInfo();
    }

    /**
    * @description Obtiene información del usuario
    * @author Gabriel Garcia Rojas
    * @return
    **/
    @AuraEnabled
    public static List<String> fetchUserInfoAcc() {
        return MX_BPP_LeadStart_Service.fetchusdata();
    }

    /**
    * @description return Total of record by Division, Oficina or User
    * @author Gabriel Garcia Rojas
    * @param Integer numRecords
    * @return Map<String, Integer>
    **/
    @AuraEnabled
    public static Integer fetchPgs(Integer numRecords) {
        return MX_BPP_LeadStart_Service.fetchPgs(numRecords);
    }

    /**
    * @description retorna una lista de usuarios con respecto al titulo y la oficina
    * @author Jair Ignacio Gonzalez Gayosso
    * @param String params [tyAcc, tipoConsulta, filtro0]
    * @return MX_BPP_AccountFiltr_Service.WRP_ChartStacked
    **/
    @AuraEnabled
    public static MX_BPP_AccountFiltr_Service.WRP_ChartStacked fetchDataAcc(List<String> params) {
        return MX_BPP_AccountFiltr_Service.fetchServiceDataAcc(params);
    }

    /**
    * @description retorna una lista oportunidaddes filtradas
    * @author Gabriel Garcia Rojas
    * @param String params [bkm, tyAcc, offset]
    * @return List<Account>
    **/
    @AuraEnabled
	public static List<Account> fetchAcc(List<String> params) {
        return MX_BPP_AccountFiltr_Service.fetchAcc(params);
	}
}