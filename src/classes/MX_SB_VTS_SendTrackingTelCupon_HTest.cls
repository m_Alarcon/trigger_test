/**
 * @File Name          : MX_SB_VTS_SendTrackingTelCupon_HTest.cls
 * @Description        : 
 * @Author             : Eduardo Hernández Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 8/6/2020 11:19:47
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    8/6/2020   Eduardo Hernández Cuamatzi     Initial Version
**/
@isTest
private class MX_SB_VTS_SendTrackingTelCupon_HTest {

    /**Proveedor */
    public final static String PROVEEDOR = 'SMART CENTER';
    /**@description Nombre usuario*/
    private final static string ASESORNAME = 'Asesor';

    @TestSetup
    static void makeData() {
        final User asesorUser = MX_WB_TestData_cls.crearUsuario(ASESORNAME, 'Telemarketing VTS');
        insert asesorUser;
        final Account accToAssign = MX_WB_TestData_cls.crearCuenta('Donnie', 'PersonAccount');
        accToAssign.PersonEmail = 'donnie.test@test.com';
        accToAssign.OwnerId = asesorUser.Id;
        insert accToAssign;
        final List<String> lstValues = new List<String>{
            System.Label.MX_SB_VTS_Hogar,
            'Smart Center',
            'OppTelemarketing',
            'Venta',
            '999999 1 OppTest Close Opp',
            '999990',
            '72'
        };
        System.runAs(asesorUser) {
            MX_SB_VTS_CallCTIs_utility.insertBasicsVTS(lstValues,  System.Label.MX_SB_VTS_Telemarketing_LBL, asesorUser, accToAssign);
        }
    }

    @isTest
    private static void fillMapTrays() {
        Test.startTest();
        final Map<String, MX_SB_VTS_Lead_tray__c> mapTrays = MX_SB_VTS_SendTrackingTelCupon_Helper.fillMapTrays();
        System.assertEquals(mapTrays.size(), 1, 'Bandeja encontrada');
        Test.stopTest();
    }
    
    @isTest
    private static void sendOppsSmart() {
        final Map<Id,MX_SB_VTS_FamliaProveedores__c> mapProveedors = MX_SB_VTS_SendTrackingTelCupon_Service.findProveed();
        final Map<String, MX_SB_VTS_Lead_tray__c> mapTrays = MX_SB_VTS_SendTrackingTelCupon_Helper.fillMapTrays();
        final List<Opportunity> validOpps = MX_SB_VTS_CallCTIs_utility.lstOppsTest();
        MX_SB_VTS_CallCTIs_utility.insertIasoVTSH();
        Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_Integration_MockGenerator());
        iaso.GBL_Mock.setMock(new MX_SB_VTS_Integration_MockGenerator());
        Test.startTest();
        final Map<String,Object> mapResponse = MX_SB_VTS_SendTrackingTelCupon_Helper.sendOppsSmart(PROVEEDOR, mapProveedors, mapTrays,validOpps );
        System.assertEquals(mapResponse.size(), 2, 'EnviosOpps encontrada');
        Test.stopTest();
    }

    @isTest
    private static void processResponseSmart() {
        final Map<Id,MX_SB_VTS_FamliaProveedores__c> mapProveedors = MX_SB_VTS_SendTrackingTelCupon_Service.findProveed();
        final Map<String, MX_SB_VTS_Lead_tray__c> mapTrays = MX_SB_VTS_SendTrackingTelCupon_Helper.fillMapTrays();
        final List<Opportunity> validOpps = MX_SB_VTS_CallCTIs_utility.lstOppsTest();
        MX_SB_VTS_CallCTIs_utility.insertIasoVTSH();
        Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_Integration_MockGenerator());
        iaso.GBL_Mock.setMock(new MX_SB_VTS_Integration_MockGenerator());
        Test.startTest();
        final Map<String,Object> responseSmart = MX_SB_VTS_SendTrackingTelCupon_Helper.sendOppsSmart(PROVEEDOR, mapProveedors, mapTrays,validOpps );
        final List<SObject>  pResponseOpps = MX_SB_VTS_SendTrackingTelCupon_Helper.processResponseOpps(responseSmart, mapTrays, validOpps, PROVEEDOR);
        System.assertEquals(pResponseOpps.size(), 1, 'Envio correcto');
        Test.stopTest();
    }
}