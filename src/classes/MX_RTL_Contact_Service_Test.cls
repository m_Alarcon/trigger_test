@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_RTL_Contact_Service_Test
* Autor: Juan Carlos Benitez Herrera
* Proyecto: SB Presuscritos - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_RTL_Contact_Service

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         10/07/2020    Juan Carlos Benitez Herrera              Creación
* -----------------------------------------------------------------------------------------------
*/
public class MX_RTL_Contact_Service_Test {
    /** Nombre de usuario Test */
    final static String NAME = 'Maria';
    /** Apellido Paterno */
    Final static STRING LNAME= 'Meneses';
    
    @TestSetup
    static void createData() {
        MX_SB_PS_OpportunityTrigger_test.makeData();
            final Account cuentTst = [Select Id from Account limit 1];
            cuentTst.FirstName=NAME;
            cuentTst.LastName=LNAME;
            cuentTst.Tipo_Persona__c='Física';
            update cuentTst;
            Final Opportunity oppoTest = [Select Id from Opportunity where AccountId=:cuentTst.Id  limit 1];
            oppoTest.Name= NAME;
            oppoTest.StageName=System.Label.MX_SB_PS_Etapa1;
            update oppoTest;
        }
    
    @isTest
    static void testgetContactData() {
        Final Id recId =[SELECT PersonContactId from Account where FirstName=:NAME].PersonContactId;
        Final set<Id> ids = new set<Id>{recId};
        test.startTest();
        	MX_RTL_Contact_Service.getContactData(ids);
            system.assert(true,'Hemos encontrado un registro');
        test.stopTest();
    }

    @isTest
    static void testupsrtCntc() {
        Final Id paccId =[SELECT PersonContactId from Account where FirstName=:NAME].PersonContactId;
        Final Contact cntc=[SELECT Id FROM Contact where Id=:paccId];
        cntc.Apellido_materno__c='Mendez';
        test.startTest();
        	MX_RTL_Contact_Service.upsrtCntc(cntc);
            system.assert(true,'Hemos actualizado un registro');
        test.stopTest();
    }
    
}