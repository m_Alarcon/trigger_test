@isTest
/**
 * @File Name          : MX_SB_MLT_Servicios_tst
 * @Description        : Clase test de ejecución de Servicios
 * @Author             : Marco Antonio Cruz Barboza
 * @Last Modified On   : 2/17/2020, 11:28:57 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/17/2020   Marco Antonio Cruz           Initial Version
 * 1.1    2/17/2020   Marco Cruz                  Corrige Code Smells 
 * 1.2    2/20/2020   Angel Nava                  codesmells minor
 * 1.3    11/03/2020  Angel Nava                Migración campos contract
**/
public with sharing class MX_SB_MLT_Servicios_tst {


    
@testSetup
    static void creaDatos() {  
        final String nameProfile = [SELECT Name from profile where name in ('Administrador del sistema','System Administrator') limit 1].Name;
        final User objUsrTst = MX_WB_TestData_cls.crearUsuario('PruebaAdminTst', nameProfile);
        objUsrTst.MX_SB_MLT_ClaimCenterId__c = '22337347373737';
        insert objUsrTst;
        System.runAs(objUsrTst) {
        final String  tiporegistro = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
		final Account ctatest = new Account(recordtypeid=tiporegistro,firstname='DANIEL',lastname='PEREZ',Apellido_materno__pc='LOPEZ',RFC__c='PELD920912',PersonEmail='test@gmail.com');
        insert ctatest;
		final Account ctatestluffy = new Account(Name='luffy',Tipo_Persona__c='Física');
		insert ctatestluffy;
            final Contract contrato = new Contract(accountid=ctatestluffy.Id, MX_SB_SAC_NumeroPoliza__c='PolizaTest',MX_SB_SAC_RFCAsegurado__c =ctatest.rfc__c,MX_SB_SAC_NombreClienteAseguradoText__c=ctatest.firstname,MX_WB_apellidoPaternoAsegurado__c=ctatest.lastname,MX_WB_apellidoMaternoAsegurado__c=ctatest.Apellido_materno__pc);
            insert contrato;
            
            final Siniestro__c sini2= new Siniestro__c();
            sini2.MX_SB_SAC_Contrato__c=contrato.Id;
            sini2.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoAuto).getRecordTypeId();
            sini2.TipoSiniestro__c = 'Robo';
            sini2.MX_SB_MLT_TipoRobo__c = Label.MX_SB_MLT_RoboParcial;
            sini2.MX_SB_MLT_ServicioAjAuto__c = false;
            sini2.MX_SB_MLT_RequiereAsesor__c = false;
            insert sini2;
            
            Final WorkOrder workOrderTest = new WorkOrder ();
            workOrderTest.MX_SB_MLT_TipoServicioAsignado__c = Label.MX_SB_MLT_AsesorRobo;
            workOrderTest.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Servicios).getRecordTypeId();
            workOrderTest.MX_SB_MLT_Ajustador__c = objUsrTst.Id;
			workOrderTest.MX_SB_MLT_EstatusServicio__c = 'Abierto';
            workOrderTest.MX_SB_MLT_Siniestro__c = sini2.Id;
            workOrderTest.MX_SB_MLT_ServicioAsignado__c = Label.MX_SB_MLT_Ajustador;
            insert workOrderTest;
            
    	}
    }
    
    @isTest
    static void getRTId () {
        Final String developerName = Label.MX_SB_MLT_RamoAuto;
        test.startTest();
            Final Id idRT = MX_SB_MLT_Servicios_cls.getRecType(developerName);
        test.stopTest();
        System.assertNotEquals(null,idRT,'Exito al Validar');
    }
    
    @isTest
    static void getTipoRoboTest() {
        Map<String, String> testResponse = new Map<String, String>();
        Final String tipoRobo = Label.MX_SB_MLT_RoboParcial;
        Final String devname = Label.MX_SB_MLT_RamoAuto;
        Final Siniestro__c sini2 = [SELECT Id, RecordTypeId FROM Siniestro__c WHERE TipoSiniestro__c = 'Robo' AND MX_SB_MLT_TipoRobo__c =:tipoRobo];
        test.startTest();
            testResponse = MX_SB_MLT_Servicios_cls.getTipoRobo(devname, sini2.Id);
            test.stopTest();
        System.assertNotEquals(null,testResponse.get('tipo'),'Exito en ejecución');
    }
    
    @isTest
    static void servicioCreadoTest() {
        Final String tipoRobo = Label.MX_SB_MLT_RoboParcial;
        Final Id userId = [SELECT ID, Name FROM User WHERE Name = 'PruebaAdminTst'].Id;
        Final Siniestro__c siniServicio = [SELECT Id, RecordTypeId FROM Siniestro__c WHERE TipoSiniestro__c = 'Robo' AND MX_SB_MLT_TipoRobo__c =:tipoRobo];
        test.startTest();
        Final String resServicio = MX_SB_MLT_Servicios_cls.servicioCreado(siniServicio.Id, userId);
        test.stopTest();
        System.assertNotEquals('',resServicio,'Exito en la ejecución');
    }
    
    @isTest
    static void getBoolsTest() {
        Final String tipoRobo = Label.MX_SB_MLT_RoboParcial;
        Final Siniestro__c siniBools = [SELECT Id, RecordTypeId FROM Siniestro__c WHERE TipoSiniestro__c = 'Robo' AND MX_SB_MLT_TipoRobo__c =:tipoRobo];
        test.startTest();
        Final Map<String,Boolean> boolsTest = MX_SB_MLT_Servicios_cls.getBools(siniBools.Id);
		test.stopTest();
        System.assertNotEquals(True,boolsTest.get('ambulancia'),'Exito dentro de la Ejecución');      
    }
    
    @isTest
    static void enviarWSRoboTest () {
        Final Id userId = [SELECT ID, Name FROM User WHERE Name = 'PruebaAdminTst'].Id;
        Final String tipoRobo = Label.MX_SB_MLT_RoboParcial;
        Final Siniestro__c siniWS = [SELECT Id, RecordTypeId FROM Siniestro__c WHERE TipoSiniestro__c = 'Robo' AND MX_SB_MLT_TipoRobo__c =:tipoRobo];
        test.startTest();
        Final String responseWS = MX_SB_MLT_Servicios_cls.enviarWSRobo(siniWS.Id, userId);
        test.stopTest();
        System.assertNotEquals('',responseWS,'Respuesta de response');
    }
    @isTest
    static void getRoboWOTest () {
        Map <string,string> roboWOTest = new Map<string,string> ();
        Final String tipoRobo = Label.MX_SB_MLT_RoboParcial;
        Final Siniestro__c sinWOTest = [SELECT Id, RecordTypeId FROM Siniestro__c WHERE TipoSiniestro__c = 'Robo' AND MX_SB_MLT_TipoRobo__c =:tipoRobo];
        test.startTest();
        RoboWOTest = MX_SB_MLT_Servicios_cls.getRoboWO(sinWOTest.Id);
        test.stopTest();
        System.assertNotEquals('123',roboWOTest.get('ajustadorCCId'),'Error en número');
    }
}