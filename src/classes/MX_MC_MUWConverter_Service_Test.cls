/**
 * @File Name          : MX_MC_MUWConverter_Service_Test.cls
 * @Description        : Clase para Test de MX_MC_MUWConverter_Service
 * @author             : Eduardo Barrera Martínez
 * @Group              : BPyP
 * @Last Modified By   : Eduardo Barrera Martínez
 * @Last Modified On   : 17/11/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      27/07/2020           Eduardo Barrera Martínez.       Initial Version
**/
@isTest
public with sharing class MX_MC_MUWConverter_Service_Test {

    /**
    * @Description PARAM_THREAD_MI property
    **/
    private static final String PARAM_THREAD_MI = 'messageId';

    /**
     * @Description Clase de prueba para MX_MC_MUWConverter_Service
     * @author eduardo.barrera3@bbva.com | 27/07/2020
    **/
    @isTest
    static void testInputUpdMsg() {
        final MX_MC_MUWConverter_Service updateConv = new MX_MC_MUWConverter_Service();
        final Map<String, Object> inputMx =  new Map<String, Object>();
        inputMx.put(PARAM_THREAD_MI, 'testMsg123');
        inputMx.put('content','Test message');
        inputMx.put('isDraft',true);
        inputMx.put('messageThreadId','threadTest123');
        final Map<String, Object> outputMx =  updateConv.convertMap(inputMx);
        System.assertEquals(inputMx.get('content'), outputMx.get('message'), 'El resultado no es esperado');
    }

    /**
     * @Description Clase de prueba para MX_MC_MUWConverter_Service
     * @author eduardo.barrera3@bbva.com | 11/17/2020
    **/
    @isTest
    static void testInputMarkAsReaded() {
        final MX_MC_MUWConverter_Service updateConv = new MX_MC_MUWConverter_Service();
        final Map<String, Object> inputMx =  new Map<String, Object>();
        inputMx.put(PARAM_THREAD_MI, 'testMsg123');
        inputMx.put('messageThreadId','threadTest123');
        final Map<String, Object> outputMx =  updateConv.convertMap(inputMx);
        System.assertEquals(inputMx.get(PARAM_THREAD_MI), outputMx.get(PARAM_THREAD_MI), 'El resultado no es esperado');
    }

    /**
     * @Description Clase de prueba para MX_MC_MUWConverter_Service
     * @author eduardo.barrera3@bbva.com | 27/07/2020
    **/
    @isTest
    static void testInputUpdConv() {
        final MX_MC_MUWConverter_Service updateConv = new MX_MC_MUWConverter_Service();
        final Map<String, Object> inputMx =  new Map<String, Object>();
        inputMx.put('messageThreadBody', '{"isSolved":true,"id":"MS0031231"}');
        inputMx.put('messageThreadId','TestThreadId');
        final Map<String, Object> outputMx =  updateConv.convertMap(inputMx);
        System.assertEquals('true', outputMx.get('isSolved'), 'El resultado no es esperado');
    }
}