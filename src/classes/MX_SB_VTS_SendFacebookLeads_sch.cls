/**
 * @File Name          : MX_SB_VTS_SendFacebookLeads_sch.cls
 * @Description        : 
 * @Author             : Eduardo Hernandez Cuamatzi
 * @Group              : 
 * @Last Modified By   : Eduardo Hernandez Cuamatzi
 * @Last Modified On   : 17/4/2020 9:21:49
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    16/4/2020   Eduardo Hernandez Cuamatzi     Initial Version
**/
public with sharing class MX_SB_VTS_SendFacebookLeads_sch implements Schedulable {

    /** variable sQuery */
	public String sQuery {get;set;}

    /**
     * execute scheduld execute 
     * @param  schContext contexto scheduld
     */
    public void execute(SchedulableContext schContext) { 
        final MX_SB_VTS_SendFacebookLeads_cls faceLeads = new MX_SB_VTS_SendFacebookLeads_cls(sQuery);
        Database.executeBatch(faceLeads, 1);
    }
}