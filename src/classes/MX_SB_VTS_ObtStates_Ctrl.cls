/**
* @description       : Clase controller que recupera listado por valor escrito o completo
* @author            : Diego Olvera
* @group             : BBVA
* @last modified on  : 02-18-2021
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   11-20-2020   Diego Olvera   Initial Version
* 1.1   18-02-2021   Diego Olvera   Se agrega función que recupera listado de estados 
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_ObtStates_Ctrl {
    /**
    * @description Función que por medio de un valor escrito en el front recupera una lista de resultados.
    * @author Diego Olvera | 11-20-2020 
    * @param  searchKey, valor recuperado desde el front JS
    * @return Lista de valores en metadata
    **/  
    @AuraEnabled(cacheable=true)
    public static List<MX_SB_VTS_StatesMex__mdt> getDirValueCtrl(String searchKey) {
        return MX_SB_VTS_ObtStates_Service.getDirValue(searchKey); 
    }
    /**
    * @description Función que recupera listado de estados de la republica
    * @author Diego Olvera | 18-02-2021 
    * @param void
    * @return void
    **/
    @AuraEnabled
    public static List<MX_SB_VTS_StatesMex__mdt> obtEstadosCtrl() {
        return MX_SB_VTS_ObtStates_Service.obtEstadosVals(); 
    }  

}