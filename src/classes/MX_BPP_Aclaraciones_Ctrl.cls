/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase con el funcionamiendo del módulo de aclaraciones

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     07/05/2020      Roberto Isaac Soto Granados           Creación Clase
*****************************************************************************************/
public with sharing class MX_BPP_Aclaraciones_Ctrl {
    /*Variable con el id del usuario en sesión*/
    private static final string USERID = System.UserInfo.getUserId();

    /*Contructor clase MX_BPP_Aclaraciones*/
    private MX_BPP_Aclaraciones_Ctrl() {}

    /*Method que consulta los prospectos del usuario en sesión*/
    @AuraEnabled
    public static List<wrapAclaracion> retrieveCases() {
        Final List<wrapAclaracion> aclaraciones = new List<wrapAclaracion>();
        Final Set<Id> dirIds = new Set<Id>();
        Final List<User> directores = MX_BPP_Aclaracion_Service.serviceDirectorIds(new Set<Id>{USERID});
        for(User director : directores) {
            dirIds.add(director.Id);
        }
        Final List<Case> casos = MX_BPP_Aclaracion_Service.serviceCasosEnAclaraciones(new Set<Id>{USERID}, dirIds);
        for(Case caso : casos) {
            Final wrapAclaracion wrapAclara = new wrapAclaracion(caso);
            aclaraciones.add(wrapAclara);
        }
        return aclaraciones;
    }

    /*Method que realiza la creación de la visita con los casos relacionados*/
    @AuraEnabled
    public static String createNewVisit(List<Id> caseIds) {
        Final List<Case> cases = MX_BPP_Aclaracion_Service.serviceCasosParaVisitas(new Set<Id>(caseIds));
		Final Map<Id, Case> caseInfo = new Map<Id, Case>(cases);
        Final List<dwp_kitv__Visit_Topic__c> newTopics = new List<dwp_kitv__Visit_Topic__c>();
        Final List<Case> updatedCases = new List<Case>();
        Final dwp_kitv__Visit__c newVisit = new dwp_kitv__Visit__c();
        Id visitId;

        for(Case caso : caseInfo.values()) {
            caso.Status = 'En proceso';
            updatedCases.add(caso);
        }

        MX_BPP_Aclaracion_Service.serviceActualizaCasos(updatedCases);

        newVisit.Name = caseInfo.values()[0].Aclaraciones__r.isEmpty() ? 'Aclaración - '+caseInfo.values()[0].MX_SB_SAC_Folio__c : 'Aclaración - '+ caseInfo.values()[0].Aclaraciones__r[0].BPyP_Producto__c +'-'+caseInfo.values()[0].Aclaraciones__r[0].BPyP_Tarjeta__c;
        newVisit.dwp_kitv__account_id__c = caseInfo.values()[0].AccountId;
        newVisit.dwp_kitv__visit_status_type__c = '01';
        newVisit.dwp_kitv__visit_start_date__c	= DateTime.now();
        newVisit.dwp_kitv__visit_duration_number__c = '15';
        newVisit.RecordTypeId = MX_BPP_Aclaracion_Service.serviceRecordTypeLlamadaBPyP();

        visitId = MX_BPP_Aclaracion_Service.serviceCreaVisita(new List<dwp_kitv__Visit__c>{newVisit});
        for(String caseId : caseIds) {
            Final dwp_kitv__Visit_Topic__c newTopic = new dwp_kitv__Visit_Topic__c();
            newTopic.dwp_kitv__Case__c = caseId;
            newTopic.dwp_kitv__topic_desc__c = String.isBlank(caseInfo.get(caseId).Description) ? 'Aclaración' : caseInfo.get(caseId).Description;
            newTopic.dwp_kitv__visit_id__c = visitId;
            newTopics.add(newTopic);
        }

        if(!newTopics.isEmpty()) {
            MX_BPP_Aclaracion_Service.serviceCreaTemas(newTopics);
        }

        return visitId;
    }

	/*Wrapper para casos y aclaraciones en un solo objeto*/
    public class wrapAclaracion {
        /**Id de la cuenta*/
        @AuraEnabled public Id accId {get; set;}
        /**Nombre de la cuenta*/
        @AuraEnabled public String accNombre {get; set;}
        /**Id del caso*/
        @AuraEnabled public Id caseId {get; set;}
        /**Folio del caso*/
        @AuraEnabled public String caseFolio {get; set;}
        /**Fecha creación del caso*/
        @AuraEnabled public DateTime caseFechaCrea {get; set;}
        /**Descripción del caso*/
        @AuraEnabled public String caseDesc {get; set;}
        /**Producto de la aclaración*/
        @AuraEnabled public String aclaraProd {get; set;}
        /**Terminación del producto de la aclaración*/
        @AuraEnabled public String aclaraTerm {get; set;}

        /*Wrapper para casos y aclaraciones en un solo objeto*/
        public wrapAclaracion(Case caso) {
            accId = caso.AccountId;
            accNombre = caso.Account.Name;
            caseId = caso.Id;
            caseFolio = caso.MX_SB_SAC_Folio__c;
            caseFechaCrea = caso.MX_SB_MLT_CreateDateCC__c;
            caseDesc = caso.Description;
            aclaraProd = caso.Aclaraciones__r.isEmpty() ? ' ' : caso.Aclaraciones__r[0].BPyP_Producto__c;
            aclaraTerm = caso.Aclaraciones__r.isEmpty() ? ' ' : caso.Aclaraciones__r[0].BPyP_Tarjeta__c;
        }
    }
}