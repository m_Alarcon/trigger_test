@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_RTL_Siniestro_History_Service_Test
* Autor: Juan Carlos Benitez Herrera
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_RTL_Siniestro_History_Service

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         26/05/2020   Juan Carlos Benitez Herrera               Creación
* -----------------------------------------------------------------------------------------------
*/
public class MX_RTL_Siniestro_History_Service_Test {
    /** Lista SOBJECT */
    final static List<SObject> LISTHISTORY = new List<SObject>();
    /** Nombre de usuario Test */
    final static String NAME = 'Caza Recompensas Zoro';
    /** current user id */
    final static Set<Id> USR_ID = new Set<Id>{UserInfo.getUserId()};
        /*
* var String Valor Subramo Ahorro
*/
        public static final String SUBRAMO ='Ahorro';
    @TestSetup
    static void createData() {
        Final String profileVida = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User usuarioVida = MX_WB_TestData_cls.crearUsuario(NAME, profileVida);  
        insert usuarioVida;
        System.runAs(usuarioVida) {
            final String recType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_Proveedores_MA).getRecordTypeId();
            final Account accountVTest = new Account(Name=NAME, Tipo_Persona__c='Física',RecordTypeId = recType);
            insert accountVTest;
            
            final Contract contrVida = new Contract(AccountId = accountVTest.Id, MX_SB_SAC_NumeroPoliza__c='policyTest');
            insert contrVida;

            Final List<Siniestro__c> insrtSin = new List<Siniestro__c>();
            for(Integer m = 0; m < 10; m++) {
                Final Siniestro__c nSin = new Siniestro__c();
                nSin.MX_SB_SAC_Contrato__c = contrVida.Id;
                nSin.MX_SB_MLT_NombreConductor__c = NAME + '_' + m;
                nSin.MX_SB_MLT_APaternoConductor__c = 'LastName for '+NAME;
                nSin.MX_SB_MLT_AMaternoConductor__c = 'LastName2 for '+NAME;
                nSin.MX_SB_MLT_Fecha_Hora_Siniestro__c =date.valueof('2020-03-27T22:04:00.000+0000');
                nSin.MX_SB_MLT_Telefono__c = '5534253647';
                nSin.MX_SB_MLT_AtencionVida__c = 'Agendar Cita';
                nSin.MX_SB_MLT_PreguntaVida__c = false;
                nSin.MX_SB_MLT_SubRamo__c = SUBRAMO;
                nSin.TipoSiniestro__c = 'Siniestros';
                nSin.MX_SB_MLT_JourneySin__c = 'Detalles del Siniestro o Asistencia';
                Final datetime citaAgenda = datetime.now();
                nSin.MX_SB_MLT_CitaVidaAsegurado__c = citaAgenda.addDays(2);
                nSin.RecordTypeId = Schema.SObjectType.Siniestro__c.getRecordTypeInfosByDeveloperName().get(Label.MX_SB_MLT_RamoVida).getRecordTypeId();
                insrtSin.add(nSin);
            }
            Database.insert(insrtSin);
        }
    }
    
    @isTest
    static void testgetSinHData () {
        final Siniestro__c  sini =[SELECT Id from Siniestro__c where  MX_SB_MLT_NombreConductor__c =:  NAME +'_2' limit 1];
        sini.MX_SB_MLT_JourneySin__c='Cerrado';
        update sini;
        Final Id ids=[SELECT Id from Siniestro__c where  MX_SB_MLT_JourneySin__c='Cerrado'].Id;
        final Set<Id> sinId = new Set<Id>{ids};
            test.startTest();
        MX_RTL_Siniestro_History_Service.getSinHData(sinId);
        test.stopTest();
        system.assert(true, 'Exitoso');
    }

    @isTest
    private static void testConstructor() {
        final MX_RTL_Siniestro_History_Service controller = new MX_RTL_Siniestro_History_Service();
        system.assert(true,controller);
    }
}