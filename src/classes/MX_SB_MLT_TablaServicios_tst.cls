@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_TablaServicios_tst
* Autor Marco BArboza
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : clase test de MX_SB_MLT_TablaServicios_cls

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           10/12/2019      Marco Barboza                       Creación
* --------------------------------------------------------------------------------
*/
public with sharing  class MX_SB_MLT_TablaServicios_tst {
     /**
	*Met que regresa la lista de servicios creados en WorkOrder dependiendo el Contrato
	**/
      @TestSetup
    static void creaDatos() {
        final Account acgrua =new Account(name='Gruatest',recuperacion__c=false,Tipo_Persona__c='Física');
        insert acgrua;
        final Contract contrato = new Contract(AccountId=acgrua.id,MX_SB_SAC_NumeroPoliza__c='A4234', name='testcontract');
        insert contrato;

            final Siniestro__c sini= new Siniestro__c();
            sini.Folio__c='sinitest';
            sini.MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro';
            sini.MX_SB_SAC_Contrato__c = contrato.Id;
            insert sini;
        final workorder work = new workOrder(MX_SB_MLT_Siniestro__c=sini.id,MX_SB_MLT_TipoSiniestro__c = 'Gasolina');
        insert work;
    }
     /**
	*prueba servicios de poliza
	**/
    @isTest
    static void pruebaServicios() {
        Integer numRes;
        numRes = 1;
        final siniestro__c sin = [select id from siniestro__c limit 1];
        test.startTest();
        final List<WorkOrder> work = MX_SB_MLT_TablaServicios_cls.getServicio(sin.id);
        test.stopTest();
        system.assertEquals(numRes, work.size(), 'contiene valores');
    }
     /**
	*prueba contador
	**/
    @isTest
    static void pruebaContador() {
        Integer numLoc;
        numLoc = 4;
        final siniestro__c sini = [select id from siniestro__c limit 1];
        test.startTest();
        final Map<String,Boolean> contador= MX_SB_MLT_TablaServicios_cls.countServicio(sini.id);
        test.stopTest();
        system.assertEquals(numLoc, contador.size(), 'contiene valores contador');
    } 
    

}