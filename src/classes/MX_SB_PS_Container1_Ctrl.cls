/**
* Indra
* @author           Julio Medellín Oliva
* Project:          Presuscritos
* Description:      Clase de utilidad que brinda distintas creaciones de objetos para clases test.
*
* Changes (Version)
* ------------------------------------------------------------------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    --------------------------------------
* @version  1.0     2020-05-25      Julio Medellín Oliva.         Creación de la Clase
* @version  1.1     2020-05-25      Gerardo Mendoza Aguilar       Se agrega methodo getDataCampaign
* @version  1.1     2020-05-25      Gerardo Mendoza Aguilar       Se agrega methodo avanza
*/
@SuppressWarnings('sf:UseSingleton, sf:DUDataflowAnomalyAnalysis')
public with sharing class MX_SB_PS_Container1_Ctrl {
/** list of records to be retrieved */
	final static List<Task> TASK_DATA = new List<Task>();
/** list of records to be retrieved */
    final static List<Account> ACC_DATA = new List<Account>();
/** list of records to be retrieved */
    final static List<Campaign> CAMPDATA = new List<Campaign>();

     /**
    * @Method fecthOpp1
    * @Description Method para obtner la Oportunidad
    * @return void
    **/
      @auraEnabled
	  public static Opportunity fetchOpp(String strId) {
	  final string oppid = strId;
      return MX_SB_PS_Utilities.fetchOpp(OppId);	 
      }
         /**
    * @Method upsrtOpp
    * @Description Method para upsert la Oportunidad
    * @return void
    **/
     @AuraEnabled
      public static void upsrtOpp(Opportunity opp) {
            MX_SB_PS_Utilities.upsrtOpp(opp);
      }
	 /**
    * @Method upsrtQuoteLI
    * @Description Method para upsert a quotelineitem
    * @return void
    **/
      @AuraEnabled
      public static QuoteLineItem[] upsrtQuoteLI(Quote quote, QuoteLineItem quoteLI , String prod) {
        final QuotelineItem qlt = [select id,quoteId,MX_WB_Folio_Cotizacion__C from quotelineitem where quote.opportunityid=:prod limit 1];
        quote.id=qlt.quoteId;
        quoteLI.id=qlt.id;
            final QuoteLineItem[] listQuoteLi = new QuoteLineItem[]{};
            listQuoteLi.add(quoteLI);
          MX_RTL_Quote_Selector.upsrtQuote(quote);
          return MX_RTL_QuoteLineItem_Selector.upsertQuoteLI(listQuoteLi);
      }
   /**
    * @Method getTaskData
    * @Description Method obtiene datos de la tara
    * @return void
    **/
    @AuraEnabled
    public static List<Task> getTaskData(String oppId) {
        if(String.isNotBlank(oppId)) {
                final Set<Id> ids = new Set<Id>{oppId};
             	TASK_DATA.addAll(MX_RTL_Task_Service.getTaskData(ids));
            	}
            return TASK_DATA;
    }
   /**
    * @Method upsrtTask
    * @Description actualiza la tarea asociada
    * @return void
    **/    
    @AuraEnabled
    public static void upsrtTask(Task taskObj, String typo) {
        MX_RTL_Task_Service.updateTask(taskObj,typo);
    }
   /**
    * @Method loadAccountJ
    * @Description Method obtiene valores de cuenta
    * @return void
    **/
    @AuraEnabled
    public static list<Account> loadAccountJ(String recId) {
        if(String.isNotBlank(recId)) {
            final Set<Id> ids = new Set<Id>{recId};
                ACC_DATA.addAll(MX_RTL_Account_Service.getSinHData(ids));
        }
        return ACC_DATA;
    }
     /**
    * @Method getDataCampaign
    * @Description Method obtiene valores de cuenta
    * @return List<Campaign>
    **/
    @AuraEnabled
    public static List<Campaign> getDataCampaign(String idCamp) {
        if(String.isNotBlank(idCamp)) {
           CAMPDATA.addAll(MX_RTL_Campaign_Service.selectCampaignById(idCamp));
        }
        return CAMPDATA;
    }
    /**
    * @description: Methodo de desicion para avanzar
    * en flujo respecto a respuesta del servicio
    * @author  | 12-09-2020 
    * @param idopp 
    * @return Boolean 
    **/
    public static Boolean avanza(String idopp) {
            Boolean continua=false;
            final quotelineitem data4service = [select id,MX_WB_placas__c, MX_SB_VTS_Tipo_Plan__c,MX_WB_subMarca__c,MX_WB_origen__c,MX_SB_VTS_FormaPAgo__c from quotelineitem where quote.opportunityid =:idopp];
            Final Map<String,String> mapdat = new Map<String,String>();
            mapdat.put('name',data4service.MX_WB_subMarca__c);
            mapdat.put('id',data4service.MX_WB_origen__c);
            final Map<String,String> mapdt = new Map<String,String>();
            mapdt.put('planselected',data4service.MX_WB_placas__c);
            mapdt.put('formapago',data4service.MX_SB_VTS_FormaPAgo__c);
            mapdt.put('intval','0');
            mapdt.put('oppid',idopp);
            mapdt.put('nombreplan',data4service.MX_SB_VTS_Tipo_Plan__c);
            try {
                final MX_SB_PS_wrpCotizadorVidaResp datares = (MX_SB_PS_wrpCotizadorVidaResp) Json.deserialize(MX_SB_PS_RetrieveData.getLapsos(mapdt,mapdat),MX_SB_PS_wrpCotizadorVidaResp.class);
                if (String.isNotEmpty(datares.data.id)) {
                    continua=true;
                }
            } catch (Exception e) {
                throw new AuraHandledException(System.Label.MX_SB_BCT_Error_ListException + e);
            }
        return continua;
    }
}