/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 08-11-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   07-24-2020   Eduardo Hernandez Cuamatzi   Initial Version
**/
@SuppressWarnings('sf:UseSingleton, sf:DMLWithoutSharingEnabled')
public class MX_RTL_Catalogo_Clipert_Product_Selector {

    /**
    * @description Recupera catalogo por codigo de producto
    * @author Eduardo Hernandez Cuamatzi | 07-27-2020 
    * @param Set<String> lstProductCode lista de codigos
    * @param Set<String> lstProcess lista de procesos Product2
    * @param Boolean isActive Producto activo
    * @return List<MX_SB_SAC_Catalogo_Clipert_Producto__c>  Lista de productos clippert
    **/
    public static List<MX_SB_SAC_Catalogo_Clipert_Producto__c> catalogoClipCod(Set<String> lstProductCode, Set<String> lstProcess, Boolean isActive) {
        return [Select Id,Name, MX_SB_VTS_ProductCode__c, MX_SB_SAC_Producto__c, MX_SB_SAC_Producto__r.Name,MX_SB_SAC_Activo__c from MX_SB_SAC_Catalogo_Clipert_Producto__c
        where MX_SB_SAC_Activo__c = TRUE AND MX_SB_VTS_ProductCode__c IN: lstProductCode AND MX_SB_SAC_Producto__r.MX_SB_SAC_Proceso__c IN: lstProcess AND MX_SB_SAC_Producto__r.IsActive =: isActive];
    }

    
    /**
    * @description Inserta registros MX_SB_SAC_Catalogo_Clipert_Producto__c
    * @author Eduardo Hernandez Cuamatzi | 07-27-2020 
    * @param List<MX_SB_SAC_Catalogo_Clipert_Producto__c> lstClippert Lista de productos clippert
    * @param Boolean allOrNone Indica si acepta los registros si falla
    * @return List<Database.SaveResult> resultado de la inserción 
    **/
    public static List<Database.SaveResult> newProductClipp(List<MX_SB_SAC_Catalogo_Clipert_Producto__c> lstClippert, Boolean allOrNone) {
        return Database.insert(lstClippert, allOrNone);
    }

    /**
    * @description Recupera catalogo por codigo de producto
    * @author Eduardo Hernandez Cuamatzi | 12-28-2020 
    * @param Set<String> lstProductName lista nombres de producto
    * @param Set<String> lstProcess lista de procesos Product2
    * @param Boolean isActive Producto activo
    * @return List<MX_SB_SAC_Catalogo_Clipert_Producto__c>  Lista de productos clippert
    **/
    public static List<MX_SB_SAC_Catalogo_Clipert_Producto__c> catalogoClipName(Set<String> lstNameProd, Set<String> lstProcess, Boolean isActive) {
        return [Select Id,Name, MX_SB_VTS_ProductCode__c, MX_SB_SAC_Producto__c, MX_SB_SAC_Producto__r.Name,MX_SB_SAC_Activo__c from MX_SB_SAC_Catalogo_Clipert_Producto__c
        where MX_SB_SAC_Activo__c = TRUE AND MX_SB_SAC_Producto__r.Name IN: lstNameProd AND MX_SB_SAC_Producto__r.MX_SB_SAC_Proceso__c IN: lstProcess AND MX_SB_SAC_Producto__r.IsActive =: isActive];
    }
}