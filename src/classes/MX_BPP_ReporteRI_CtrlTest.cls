/********************************************************************************
*	@Desarrollado por:		Softtek					 							*
*	@Autor:					Hugo Ivan Carrillo Béjar           					*
*	@Proyecto:				Clientes BPyP										*
*	@Descripción:			Clase test para el controlador MX_BPP_ReporteRI_Ctrl*
*																				*
*	Cambios (Versiones)															*
*	--------------------------------------------------------------------------	*
*	No.		Fecha				Autor					Descripción				*
*	------  ----------  ----------------------  -----------------------------	*
*	1.0		19/11/2019	Hugo I. Carrillo Béjar     Creación de clase Test       *
*
*********************************************************************************/
@isTest
public class MX_BPP_ReporteRI_CtrlTest {

    /*Accesor String myURL*/
    Private static String myURL;

    /**
    * ------------------------------------------------------------------
    * @Name     setup
    * @Author   Hugo Carrillo
    * @Date     Created: 19/11/2019
    * @Group
    * @Description Método para crear la información necesaria para las pruebas.
    * @Changes
    *     | 19/11/2019  Hugo Carrillo Initial Version
    **/
    @testSetup
    static void setup() {
        createDOF('DirectorTest', 'BPyP Director Oficina', 'BPYP_DIRECTOR_OFICINA_BANCA_PERISUR');
        createBanker('BanqueroTest', 'BPyP Estandar', 'BPYP_BANQUERO_BANCA_PERISUR', 'DirectorTest');
        createRI('BanqueroTest');

    }

    /**
    * ------------------------------------------------------------------
    * @Name     setup
    * @Author   Hugo Carrillo
    * @Date     Created: 19/11/2019
    * @Group
    * @Description Método para crear un usuario Director de Oficina.
    * @Changes
    *     | 19/11/2019  Hugo Carrillo Initial Version
    **/
    private static void createDOF(String lastName, String profile, String role) {
        User newUser;
        newUser = UtilitysDataTest_tst.crearUsuario(lastName, profile, role);
        newUser.Divisi_n__c = 'METROPOLITANA';
        newUser.BPyP_ls_NombreSucursal__c = '6385 BANCA PERISUR';
        newUser.VP_ls_Banca__c = 'Red BPyP';
        insert newUser;
    }

    /**
    * ------------------------------------------------------------------
    * @Name     setup
    * @Author   Hugo Carrillo
    * @Date     Created: 19/11/2019
    * @Group
    * @Description Método para crear un usuario Banquero.
    * @Changes
    *     | 19/11/2019  Hugo Carrillo Initial Version
    **/
    private static void createBanker(String lastName, String profile, String role, String dofLastName) {
        User newUser;
        final User dof = [SELECT Id FROM User WHERE LastName =: dofLastName];
        System.debug('DOF: ' + dof);
        newUser = UtilitysDataTest_tst.crearUsuario(lastName, profile, role);
        newUser.Divisi_n__c = 'METROPOLITANA';
        newUser.BPyP_ls_NombreSucursal__c = '6385 BANCA PERISUR';
        newUser.VP_ls_Banca__c = 'Red BPyP';
        newUser.Director_de_oficina__c = dof.Id;
        insert newUser;
    }

    /**
    * ------------------------------------------------------------------
    * @Name     setup
    * @Author   Hugo Carrillo
    * @Date     Created: 19/11/2019
    * @Group
    * @Description Método para crear una RI.
    * @Changes
    *     | 19/11/2019  Hugo Carrillo Initial Version
    **/
    private static void createRI(String bnkmnLN) {
        final EU001_RI__c testRI = new EU001_RI__c();
        final User riOwner = [SELECT Id FROM User WHERE LastName =: bnkmnLN];
        System.debug('RI OWNER: ' + riOwner.Id);
        testRI.OwnerId = riOwner.Id;
        insert testRI;
    }


    /**
    * ------------------------------------------------------------------
    * @Name     setup
    * @Author   Hugo Carrillo
    * @Date     Created: 19/11/2019
    * @Group
    * @Description Método Test que engloba toda la clase MX_BPP_REporteRI_Ctrl.
    * @Changes
    *     | 19/11/2019  Hugo Carrillo Initial Version
    **/
	@isTest
    static void testRporteConstructor() {
        String testDate;
        final datetime day = date.newInstance(2019, 11, 19);
        final MX_BPP_ReporteRI_Ctrl testRep = new MX_BPP_ReporteRI_Ctrl();
        Test.startTest();
        testReport();
        testDate = testRep.getFecha(day);
        System.assertEquals('19 de Noviembre del 2019', testDate, 'No se está generando una fecha correcta');
        Test.stopTest();
    }

    /**
    * ------------------------------------------------------------------
    * @Name     setup
    * @Author   Hugo Carrillo
    * @Date     Created: 19/11/2019
    * @Group
    * @Description Método Test que engloba toda la clase MX_BPP_REporteRI_Ctrl.
    * @Changes
    *     | 19/11/2019  Hugo Carrillo Initial Version
    **/
    static void testReport() {
        final User riOwner = [SELECT Id FROM User WHERE LastName = 'BanqueroTest'];
        final EU001_RI__c newRI =[SELECT id FROM EU001_RI__c WHERE OwnerId=:riOwner.Id];
        ApexPages.currentPage().getParameters().put('Id', String.valueOf(newRI.Id));
        MX_BPP_ReporteRI_Ctrl.sendMail(newRI.Id);
        myURL = MX_BPP_ReporteRI_Ctrl.returnURL();
        System.assertNotEquals(null, myURL, 'URL vacía');
    }

}