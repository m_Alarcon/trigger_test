/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPyP_Opportunity_Service_Helper_Test
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-05-25
* @Description 	Test Class for Opportunity Service Helper
* @Changes
*  
*/
@isTest
public class MX_BPyP_Opportunity_Service_Helper_Test {
	/** String BPyP Estandar */
	static final String STR_BPYPESTANDAR = 'BPyP Estandar';
    
    /** Colocación */
	static final String STR_COLOCACION = 'Colocación';
    
	/** Collares */
	static final String STR_COLLARES = 'Collares';
    
	/** BPYP Opportunity RecordType */
	static final String STR_RT_BPYPOPP = 'MX_BPP_RedBpyp';
    
    /** BPYP Account RecordType */
    static final String STR_RT_BPYPACC = 'BPyP_tre_Cliente';
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test method trying to change the Opportunity Status from Open to Close Win
    **/
	@isTest
    static void testPrevNextstatusBad() {
        String stageName1, stageName2;
        Boolean result;
        
		final User user = UtilitysDataTest_tst.crearUsuario('Usuario', STR_BPYPESTANDAR, Null);
        user.Segmento_Ejecutivo__c = 'EMPRESARIAL';
        insert user;
        
		System.runAs(user) {
			final Account cliente = UtilitysDataTest_tst.crearCuenta('userBPyP', STR_RT_BPYPACC);
            cliente.MX_BPP_CPC_CheckClienteCPC__c = false;
            insert cliente;
            UtilitysDataTest_tst.crearProdForm(STR_COLOCACION, STR_COLLARES);
			final Opportunity opp = UtilitysDataTest_tst.crearOportunidad('OppTest 1', cliente.id, user.id, STR_COLOCACION, STR_COLLARES, null, STR_RT_BPYPOPP);
        	insert opp;
            stageName1 = 'Abierta';
            stageName2 = 'Cerrada ganada';

	    	Test.startTest();
            result = MX_BPyP_Opportunity_Service_Helper.validPrevNextStatus(opp, stageName2, stageName1);
            Test.stopTest();
            
    		System.assert(!result, 'Falló validación');
		}
    }
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test method trying to change the Opportunity Status from Open to Close Win
    **/
	@isTest
    static void testPrevNextstatusOK() {
        String stageName1, stageName2, stageName3;
        Boolean result;
        
		final User user = UtilitysDataTest_tst.crearUsuario('Usuario', STR_BPYPESTANDAR, Null);
        user.Segmento_Ejecutivo__c = 'EMPRESARIAL';
        insert user;
        
		System.runAs(user) {
			final Account cliente = UtilitysDataTest_tst.crearCuenta('userBPyP', STR_RT_BPYPACC);
            cliente.MX_BPP_CPC_CheckClienteCPC__c = false;
            insert cliente;
            UtilitysDataTest_tst.crearProdForm(STR_COLOCACION, STR_COLLARES);
			final Opportunity opp = UtilitysDataTest_tst.crearOportunidad('OppTest 1', cliente.id, user.id, STR_COLOCACION, STR_COLLARES, null, STR_RT_BPYPOPP);
        	insert opp;
            stageName1 = 'Abierta';
            stageName2 = 'En Gestión';
            stageName3 = 'Cerrada ganada';

	    	Test.startTest();
            result = MX_BPyP_Opportunity_Service_Helper.validPrevNextStatus(opp, stageName2, stageName1);
            result = MX_BPyP_Opportunity_Service_Helper.validPrevNextStatus(opp, stageName3, stageName2);
            Test.stopTest();
            
            System.assert(result, 'Falló validación');
		}
    }
}