/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Minuta_Template_Service_Helper_Test
* @Author   	Héctor Saldaña | hectorisrael.saldana.contractor@bbva.com
* @Date     	Created: 2021-01-22
* @Description 	Test Class for BPyP Minuta Template Service Layer Helper
* @Changes		Date		|		Author		|		Description
 * 			2021-22-02			Héctor Saldaña	  New methods added: replaceStaticValuesTest(),
 * 																	 replaceTagsTest(),
*																     getMetadataTagsTest(),
*																     replaceProfileTagsTest(),
*																     procesaAcuerdosTest(),
 *																     getVisitTest()
*
*/
@isTest
private class MX_BPP_Minuta_Template_Service_HelperTst {

	/** BPYP Account RecordType */
	static final String STR_RT_BPYPACC = 'MX_BPP_PersonAcc_Client';

	/** Email test for Account and Contact **/
	static final String STR_EMAIL = 'test.minuta@test.com';

	@testSetup
	static void setup() {
		final User usuarioAdmManager = UtilitysDataTest_tst.crearUsuario('Manager', Label.MX_PERFIL_SystemAdministrator, 'BBVA ADMINISTRADOR');
		usuarioAdmManager.Email = 'managerUser@testDomain.com';
		insert usuarioAdmManager;

		final User usuarioAdmin = UtilitysDataTest_tst.crearUsuario('PruebaAdm', Label.MX_PERFIL_SystemAdministrator, 'BBVA ADMINISTRADOR');
		usuarioAdmin.Email = 'testUser@testDomain.com';
		usuarioAdmin.ManagerId = usuarioAdmManager.Id;
		usuarioAdmin.Divisi_n__c = 'METROPOLITANA';
		usuarioAdmin.BPyP_ls_NombreSucursal__c = '0680 LOMAS';
		usuarioAdmin.Title = 'Admin Title';
		insert usuarioAdmin;

		System.runAs(usuarioAdmin) {
			final Account newAccount = new Account();
			newAccount.FirstName = 'userBPyP';
			newAccount.LastName = 'Test Acc';
			newAccount.No_de_cliente__c = 'D2050394';
			newAccount.RecordTypeId = RecordTypeMemory_cls.getRecType('Account', STR_RT_BPYPACC);
			newAccount.PersonEmail = STR_EMAIL;
			insert newAccount;

            final dwp_kitv__Visit__c testVisit = new dwp_kitv__Visit__c();
            testVisit.Name = 'Visita Prueba';
            testVisit.dwp_kitv__visit_start_date__c = Date.today()+4;
            testVisit.dwp_kitv__account_id__c = newAccount.Id;
            testVisit.dwp_kitv__visit_duration_number__c = '15';
            testVisit.dwp_kitv__visit_status_type__c = '01';
			insert testVisit;

			final ContentVersion contVersion = new ContentVersion(
					Title = 'Penguins',
					PathOnClient = 'Penguins.pdf',
					VersionData = Blob.valueOf('Test Content'),
					FirstPublishLocationId=testVisit.Id
			);
			insert contVersion;

			Document docObj;
			docObj = new Document();
			docObj.Body = Blob.valueOf('Some Document Text');
			docObj.ContentType = 'application/pdf';
			docObj.DeveloperName = 'PenguinsDoc';
			docObj.IsPublic = true;
			docObj.Name = 'PenguinsDoc';
			docObj.FolderId = UserInfo.getUserId();
			insert docObj;
            
            createTestRecords();
		}
	}

	/**
	* @Description 	Test Method for MX_BPP_Minuta_Template_Service_Helper.saveEmailAsTemplateTest()
	* @Return 		NA
	**/
	@isTest
	static void saveEmailAsTemplateTest() {
		List<ContentVersion> lstDocuments = new List<ContentVersion>();
		final User userAdmin = [SELECT Id, Name, Email, Manager.Id, Manager.Email FROM User WHERE Name = 'PruebaAdm' LIMIT 1];

		final dwp_kitv__Template_for_type_of_visit_cs__c myCS = [Select Id, Name, dwp_kitv__Attach_file__c, dwp_kitv__Disable_send_field_in_BBVA_team__c,
														  dwp_kitv__Disable_send_field_in_contacts__c,dwp_kitv__Email_Template_Name__c, dwp_kitv__Minimum_Number_of_Agreement__c,
														  dwp_kitv__Minimum_members_BBVA_team__c, dwp_kitv__Multiple_templates_type__c, dwp_kitv__Subject__c, dwp_kitv__validation_main_contact__c,
														  dwp_kitv__Visualforce_Name__c FROM dwp_kitv__Template_for_type_of_visit_cs__c WHERE Name = 'TestCS'];


		final dwp_kitv__Visit__c testVisit = [SELECT Id, Name, RecordType.Name, dwp_kitv__visit_status_type__c FROM dwp_kitv__Visit__c WHERE Name = 'Visita Prueba' LIMIT 1];
		final MX_BPP_CatalogoMinutas__c tstCatalogo = [SELECT Id, Name, MX_Acuerdos__c, MX_Saludo__c, MX_Contenido__c, MX_Despedida__c, MX_Firma__c, MX_ImageHeader__c, MX_CuerpoCorreo__c, MX_Asunto__c FROM MX_BPP_CatalogoMinutas__c WHERE Name= 'BienvenidaDO' LIMIT 1];
		lstDocuments = [SELECT Title, PathOnClient, VersionData, FirstPublishLocationId FROM ContentVersion WHERE FirstPublishLocationId =: testVisit.Id];
		final String strCS = JSON.serialize(myCS);
		final String lstDocsStr = JSON.serialize(lstDocuments);
		Test.startTest();
		final PageReference testPage = Page.MX_BPP_Minuta_BienvenidaDO_VF;
		Test.setCurrentPage(testPage);
		ApexPages.currentPage().getParameters().put('Id', testVisit.Id);
		ApexPages.currentPage().getParameters().put('sCatalogoId', tstCatalogo.Id);
		ApexPages.currentPage().getParameters().put('myCS', strCS );
		ApexPages.currentPage().getParameters().put('documents', lstDocsStr);
		final MX_BPP_Minuta_Template_Service_Helper serviceHelperTest = new MX_BPP_Minuta_Template_Service_Helper('Test', lstDocuments);
		serviceHelperTest.saveTemplateAsDocument(testVisit, lstDocuments, myCS, userAdmin, testPage, tstCatalogo);
		final Messaging.SendEmailResult[] resultsMail = serviceHelperTest.sendMail(myCS, testVisit, userAdmin, tstCatalogo);
		System.assert(resultsMail[0].isSuccess(), 'Error on saveEmailAsTemplate');
		test.stopTest();
	}

	/**
    * @Description 	Test method for MX_BPP_Minuta_Template_Service_Helper.replaceStaticValues()
    * @Params		Na
    * @return 		void
    **/
	@IsTest
	static void replaceStaticValuesTest() {
		final MX_BPP_CatalogoMinutas__c tstCatalogo = [SELECT Id, Name, MX_Acuerdos__c, MX_Saludo__c, MX_Contenido__c, MX_Despedida__c, MX_Firma__c, MX_ImageHeader__c, MX_CuerpoCorreo__c, MX_Asunto__c FROM MX_BPP_CatalogoMinutas__c WHERE Name= 'BienvenidaDO' LIMIT 1];
		final dwp_kitv__Visit__c visitTst = [SELECT Id, Name, RecordType.Name, dwp_kitv__visit_status_type__c, Owner.Name, MX_BPyP_Oficina__c  FROM dwp_kitv__Visit__c WHERE Name = 'Visita Prueba' LIMIT 1];
		tstCatalogo.MX_Contenido__c ='{OficinaBanquero}';
		tstCatalogo.MX_Firma__c = '{NombreBanquero}';
		update tstCatalogo;
		final List<MX_BPP_MinutaWrapper.tagsEstaticos> wraprEstaticosLst = new List<MX_BPP_MinutaWrapper.tagsEstaticos>();
		final MX_BPP_MinutaWrapper.tagsEstaticos wrapperObj1 = new MX_BPP_MinutaWrapper.tagsEstaticos();
		wrapperObj1.campo = 'MX_BPyP_Oficina__c';
		wrapperObj1.relacionado = false;
		wrapperObj1.tagInicio = '{OficinaBanquero}';
		wraprEstaticosLst.add(wrapperObj1);
		final MX_BPP_MinutaWrapper.tagsEstaticos wrapperObj2 = new MX_BPP_MinutaWrapper.tagsEstaticos();
		wrapperObj2.relacionado = true;
		wrapperObj2.tagInicio = '{TestNombre}';
		wrapperObj2.objeto = 'Owner';
		wrapperObj2.campo = 'Name';
		wraprEstaticosLst.add(wrapperObj2);

		MX_BPP_Minuta_Template_Service_Helper.replaceStaticValues(wraprEstaticosLst, tstCatalogo, visitTst);
		System.assertEquals('0680 LOMAS', tstCatalogo.MX_Contenido__c, 'Error on replaceStaticValues');

	}

	/**
    * @Description 	Test method for MX_BPP_Minuta_Template_Service_Helper.replaceDynamicTags()
    * @Params		Na
    * @return 		void
    **/
	@IsTest
	static void replaceTagsTest() {
		final MX_BPP_CatalogoMinutas__c tstCatalogo = [SELECT Id, Name, MX_Acuerdos__c, MX_Saludo__c, MX_Contenido__c, MX_Despedida__c, MX_Firma__c, MX_ImageHeader__c FROM MX_BPP_CatalogoMinutas__c WHERE Name= 'BienvenidaDO' LIMIT 1];
		final List<MX_BPP_MinutaWrapper.componenteText> lstTextCmp = new List<MX_BPP_MinutaWrapper.componenteText>();
		final MX_BPP_MinutaWrapper.componenteText txtCmp = new MX_BPP_MinutaWrapper.componenteText();
		txtCmp.label = '*Test Text';
		txtCmp.tagInicio = '{OpcionalTexto1}';
		lstTextCmp.add(txtCmp);
		tstCatalogo.MX_Contenido__c = '{OpcionalTexto1}';
		MX_BPP_Minuta_Template_Service_Helper.replaceDynamicTags(tstCatalogo,lstTextCmp);

		final List<MX_BPP_MinutaWrapper.componenteSwitch> lstSwitchCmp = new List<MX_BPP_MinutaWrapper.componenteSwitch>();
		final MX_BPP_MinutaWrapper.componenteSwitch switchCmp = new MX_BPP_MinutaWrapper.componenteSwitch();
		switchCmp.label = 'Test Switch';
		switchCmp.tagInicio = '{Switch1}';
		lstSwitchCmp.add(switchCmp);
		tstCatalogo.MX_Contenido__c = '{Switch1}';
		MX_BPP_Minuta_Template_Service_Helper.replaceDynamicTags(tstCatalogo,lstSwitchCmp);

		final List<MX_BPP_MinutaWrapper.componentePicklist> lstPickListCmp = new List<MX_BPP_MinutaWrapper.componentePicklist>();
		final MX_BPP_MinutaWrapper.componentePicklist pickListCmp = new MX_BPP_MinutaWrapper.componentePicklist();
		pickListCmp.label = 'Test Picklist';
		pickListCmp.tagInicio = '{Seleccion1}';
		lstPickListCmp.add(pickListCmp);
		tstCatalogo.MX_Contenido__c = '{Seleccion1}';
		MX_BPP_Minuta_Template_Service_Helper.replaceDynamicTags(tstCatalogo,lstPickListCmp);

		System.assertEquals('Test Picklist', tstCatalogo.MX_Contenido__c, 'Error on replaceTags');
	}

	/**
    * @Description 	Test method for MX_BPP_Minuta_Template_Service_Helper.getMetadataTags()
    * @Params		Na
    * @return 		void
    **/
	@IsTest
	static void getMetadataTagsTest() {
		final dwp_kitv__Visit__c visitTst = [SELECT Id, Name, RecordType.Name, dwp_kitv__visit_status_type__c, Owner.Name, MX_BPyP_Oficina__c  FROM dwp_kitv__Visit__c WHERE Name = 'Visita Prueba' LIMIT 1];
		final MX_BPP_CatalogoMinutas__c tstCatalogo = [SELECT Id, Name, MX_Acuerdos__c, MX_Saludo__c, MX_Contenido__c, MX_Despedida__c, MX_Firma__c, MX_ImageHeader__c, MX_CuerpoCorreo__c FROM MX_BPP_CatalogoMinutas__c WHERE Name= 'BienvenidaDO' LIMIT 1];
		Test.startTest();
		tstCatalogo.MX_Contenido__c = '{OficinaBanquero}';
		tstCatalogo.MX_Despedida__c = '{CargoBanquero}';
		update tstCatalogo;
		final Set<String> tagsLst = new Set<String>{'OficinaBanquero', 'CargoBanquero'};
		MX_BPP_Minuta_Template_Service_Helper.getMetadataTags(tagsLst, tstCatalogo, visitTst.Id);
		Test.stopTest();
        System.assertEquals(visitTst.MX_BPyP_Oficina__c, tstCatalogo.MX_Contenido__c, 'Error on getMetadataTags');
	}

	/**
    * @Description 	Test method for MX_BPP_Minuta_Template_Service_Helper.replaceProfileTags()
    * @Params		Na
    * @return 		void
    **/
	@IsTest
	static void replaceProfileTagsTest() {
		final MX_BPP_CatalogoMinutas__c tstCatalogo = [SELECT Id, Name, MX_Acuerdos__c, MX_Saludo__c, MX_Contenido__c, MX_Despedida__c, MX_Firma__c, MX_ImageHeader__c FROM MX_BPP_CatalogoMinutas__c WHERE Name= 'BienvenidaDO' LIMIT 1];
		tstCatalogo.MX_Contenido__c = 'Test{Banquero} Sobrante{Banquero}';
		final Map<String,String> maptags= new Map<String,String>();
		maptags.put('Banquero','Sobrante');
		MX_BPP_Minuta_Template_Service_Helper.replaceProfileTags(tstCatalogo,maptags);
		System.assertEquals('Test Sobrante',tstCatalogo.MX_Contenido__c, 'Error on replaceProfileTag');
	}

	/**
    * @Description 	Test method for MX_BPP_Minuta_Template_Service_Helper.procesarAcuerdos()
    * @Params		Na
    * @return 		void
    **/
	@IsTest
	static void procesaAcuerdosTest() {
		final dwp_kitv__Visit__c visitTst = [SELECT Id, Name, RecordType.Name, dwp_kitv__visit_status_type__c, Owner.Name, MX_BPyP_Oficina__c  FROM dwp_kitv__Visit__c WHERE Name = 'Visita Prueba' LIMIT 1];
		final MX_BPP_CatalogoMinutas__c tstCatalogo = [SELECT Id, Name, MX_Acuerdos__c, MX_Saludo__c, MX_Contenido__c, MX_Despedida__c, MX_Firma__c, MX_ImageHeader__c FROM MX_BPP_CatalogoMinutas__c WHERE Name= 'BienvenidaDO' LIMIT 1];
		tstCatalogo.MX_Contenido__c = '{FechaAcuerdos}';
		Test.startTest();
		final Task acuerdo = new Task();
		acuerdo.Subject = 'Test';
		acuerdo.WhatId = visitTst.Id;
		acuerdo.ActivityDate = System.today();
		acuerdo.Description = 'Description Test';
		insert  acuerdo;
		MX_BPP_Minuta_Template_Service_Helper.procesarAcuerdos(tstCatalogo, '{FechaAcuerdos}', visitTst.Id);
		final String result = '<ul><li>' + MX_BPP_Minuta_Utils.fechaToString(acuerdo.ActivityDate) + ' - ' + acuerdo.Description + '</li></ul>';
		System.assertEquals(result, tstCatalogo.MX_Contenido__c, 'Error on procesaAcuerdos');
		test.stopTest();

	}

	/**
    * @Description 	Test method for MX_BPP_Minuta_Template_Service_Helper.getVisitByID()
    * @Params		Na
    * @return 		void
    **/
	@IsTest
	static void getVisitTest() {
		final dwp_kitv__Visit__c visitTst = [SELECT Id, Name, RecordType.Name, dwp_kitv__visit_status_type__c, Owner.Name, MX_BPyP_Oficina__c  FROM dwp_kitv__Visit__c WHERE Name = 'Visita Prueba' LIMIT 1];
		final dwp_kitv__Visit__c resultVisit  = MX_BPP_Minuta_Template_Service_Helper.getVisitByID(visitTst.iD, 'Id, Name');
		System.assertEquals('Visita Prueba', resultVisit.Name, 'Error on getVisit');
	}
    
    /**
	* @Description 	helper method to create test records
	* @Param		String-Name Custom Setting Name
	* @Return 		dwp_kitv__Template_for_type_of_visit_cs__c
	**/
    
    private static void createTestRecords() {
        final dwp_kitv__Template_for_type_of_visit_cs__c myCS = new dwp_kitv__Template_for_type_of_visit_cs__c();
		myCS.Name = 'TestCS';
		myCS.dwp_kitv__Attach_file__c = true;
		myCS.dwp_kitv__Disable_send_field_in_BBVA_team__c = true;
		myCS.dwp_kitv__Disable_send_field_in_contacts__c = true;
		myCS.dwp_kitv__Email_Template_Name__c = 'Test Template';
		myCs.dwp_kitv__Minimum_Number_of_Agreement__c = 0;
		myCS.dwp_kitv__Minimum_members_BBVA_team__c = 0;
		myCS.dwp_kitv__Multiple_templates_type__c = false;
		myCS.dwp_kitv__Subject__c = 'Test Subject';
		myCS.dwp_kitv__validation_main_contact__c = false;
		myCS.dwp_kitv__Visualforce_Name__c = 'MX_BPP_Minuta_BienvenidaDO_VF';
		insert myCS;
        
        final MX_BPP_CatalogoMinutas__c catalogoRecord = new MX_BPP_CatalogoMinutas__c();
		catalogoRecord.MX_Acuerdos__c = 1;
		catalogoRecord.MX_Asunto__c = 'Prueba Catalogo';
		catalogoRecord.Name = 'BienvenidaDO';
		catalogoRecord.MX_TipoVisita__c = 'BienvenidaDO';
		catalogoRecord.MX_Saludo__c = 'Saludo Test';
		catalogoRecord.MX_Contenido__c = 'Contenido Test <<Opcional1>>';
		catalogoRecord.MX_Despedida__c = 'Despedida Test';
		catalogoRecord.MX_Firma__c = 'Firma Test';
		catalogoRecord.MX_ImageHeader__c  = 'Imagen_Header_1';
		catalogoRecord.MX_CuerpoCorreo__c = 'Cuerpo del correo';
        insert catalogoRecord;
    }

}