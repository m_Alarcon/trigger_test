/**
 * -------------------------------------------------------------------------------
 * @File Name          : MX_SB_MLT_ProveedorCita_cls.cls
 * @Description        : Clase para la creación de Servicios de Critalera.
 * @Author             : Daniel Perez Lopez
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/12/2020   Daniel Perez Lopez         Initial Version
 * 1.1    2/12/2020   Marco Cruz Barboza         Creación Method Genera Servicios
**/
public with sharing class MX_SB_MLT_ProveedorCita_cls {
    /**
	* @description 
	* @var Ajustador sustituye label Ajustador 
	*/
    final static String AJUSTADOR = 'Ajustador';
    /**
	* @description 
	* Method Constructor
	**/
    private MX_SB_MLT_ProveedorCita_cls() {
        
    }
    
    /**
	* @description 
	* Method Wrapper para la creación de Servicio con Cita
	*/
    public class ServiceDateData {
        /**
        * @var fecha que almacena la fecha proveniente del JSON
        */
        public String fecha {get; set;}
        /**
        * @var fecha que almacena el ID de siniestro proveniente del JSON
        */
        public String idSin {get; set;}        
    }

	/**
	* @description 
	* Method que obtiene recordType Id
	* @param devname 
	* @return Id 
	*/
   	@auraEnabled
	public static Id getRecordType(String devname) {
      	return MX_SB_MLT_Siniestro_cls.getRecordType(devname);
    }
    
    /**
    * @description 
    * Method que obtiene los valores del siniestro.
    * @param idsini 
    * @return Siniestro__c 
    */
    @AuraEnabled
    public static Siniestro__c getDatosSiniestro(String idsini) {
      	return MX_SB_MLT_Siniestro_cls.getDatosSiniestro(idsini);
    } 
    
    /**
    * @description 
    * Method que genera servicio para cita
    * @param data 
    * @return Map<String, Object> 
    */
    @AuraEnabled
    public static Map<String,Object> creaServicio (String data) {
        Final MX_SB_MLT_ProveedorCita_cls.ServiceDateData data4service = (MX_SB_MLT_ProveedorCita_cls.ServiceDateData) JSON.deserializeStrict(data,MX_SB_MLT_ProveedorCita_cls.ServiceDateData.class);
        
        Final Map<String ,Object> responseMap = new Map<String ,Object>();
        responseMap.put('showMessage',true);
        responseMap.put('arrStr',new String[]{'uno','dos'});
        Final String rectypeserv = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_Servicios').getRecordTypeId();
        Final WorkOrder[] existCase = [SELECT id,MX_SB_MLT_ServicioAsignado__c,MX_SB_MLT_Siniestro__c,MX_SB_MLT_TipoServicioAsignado__c,createddate FROM WorkOrder WHERE
                         MX_SB_MLT_ServicioAsignado__c =: AJUSTADOR AND MX_SB_MLT_Siniestro__c=:data4service.idSin AND MX_SB_MLT_TipoServicioAsignado__c=:AJUSTADOR AND recordtypeid=:rectypeserv order by createddate Desc ];
        if (existCase.size() == 0) {
            Final WorkOrder newService = new WorkOrder(RecordTypeId = rectypeserv,MX_SB_MLT_ServicioAsignado__c= AJUSTADOR,MX_SB_MLT_Siniestro__c=data4service.idSin,MX_SB_MLT_TipoServicioAsignado__c= AJUSTADOR);
            insert newService;
        } 
        return responseMap;
    }

    /**
    * @description 
    * Method que genera el servicio de cristalera si es ajustador o proveedor
    * @param recordId 
    * @param proveedorId 
    * @return Boolean 
    */
    @AuraEnabled
    public static Boolean generaServicioProveedor (String recordId, String proveedorId) {
        Boolean resultado = false;
        Final String atencion = [SELECT MX_SB_MLT_Lugar_de_Atencion_Cristalera__c FROM Siniestro__c WHERE Id=:recordId].MX_SB_MLT_Lugar_de_Atencion_Cristalera__c;
        Final String siniestro = [SELECT TipoSiniestro__c FROM Siniestro__c WHERE Id=:recordId].TipoSiniestro__c;
        Final String rectypeserv = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_Servicios').getRecordTypeId();
        WorkOrder[] existeServicio = new List<WorkOrder>();
        
        if(existeServicio.size() == 0 && !String.isEmpty(atencion) && !String.isEmpty(siniestro) && !String.isEmpty(rectypeserv) ) {
            switch on atencion {

                when 'Ajustador', 'Reembolso' {
                    existeServicio = [SELECT Id,MX_SB_MLT_ServicioAsignado__c,MX_SB_MLT_Siniestro__c,MX_SB_MLT_TipoServicioAsignado__c,createddate FROM WorkOrder WHERE
                                        MX_SB_MLT_ServicioAsignado__c=:AJUSTADOR AND MX_SB_MLT_EstatusServicio__c = 'Abierto' AND MX_SB_MLT_Siniestro__c=:recordId AND MX_SB_MLT_TipoServicioAsignado__c =: AJUSTADOR AND recordtypeid=:rectypeserv order by createddate Desc ];
                    if (existeServicio.size() == 0) {
                        Final WorkOrder newService = new WorkOrder(RecordTypeId = rectypeserv,MX_SB_MLT_ServicioAsignado__c= AJUSTADOR, MX_SB_MLT_Siniestro__c=recordId, MX_SB_MLT_TipoServicioAsignado__c= AJUSTADOR, MX_SB_MLT_Ajustador__c = proveedorId, MX_SB_MLT_TipoSiniestro__c = Siniestro);
                        insert newService;
                        Final Siniestro__c upSini = [SELECT MX_SB_MLT_JourneySin__c FROM Siniestro__c WHERE Id=:recordId LIMIT 1];
                        upSini.MX_SB_MLT_JourneySin__c = 'Servicios Enviados';
                        update upSini;
                        resultado = true;
                    }
                }

                when 'Cristalera' {
                    existeServicio = [SELECT Id,MX_SB_MLT_ServicioAsignado__c,MX_SB_MLT_Siniestro__c,MX_SB_MLT_TipoServicioAsignado__c,createddate FROM WorkOrder WHERE
                                        MX_SB_MLT_ServicioAsignado__c= 'Cristalera' AND MX_SB_MLT_EstatusServicio__c = 'Abierto' AND MX_SB_MLT_Siniestro__c=:recordId AND MX_SB_MLT_TipoServicioAsignado__c='Cristalera' AND recordtypeid=:rectypeserv order by createddate Desc ];
                    if (existeServicio.size() == 0) {
                        Final WorkOrder newService = new WorkOrder(RecordTypeId = rectypeserv,MX_SB_MLT_ServicioAsignado__c= 'Cristalera',MX_SB_MLT_Siniestro__c=recordId,MX_SB_MLT_TipoServicioAsignado__c= 'Cristalera', AccountId = proveedorId, MX_SB_MLT_TipoSiniestro__c = Siniestro);
                        insert newService;
                        resultado = true;
                        Final Siniestro__c upSini = [SELECT MX_SB_MLT_JourneySin__c FROM Siniestro__c WHERE Id=:recordId ];
                        upSini.MX_SB_MLT_JourneySin__c = 'Servicios Enviados';
                        update upSini;
                    }

                }
            }
        }
        return resultado;
    }
    
}