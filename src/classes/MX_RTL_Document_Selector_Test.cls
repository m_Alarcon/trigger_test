/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RTL_Document_Selector_Test
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2021-01-14
* @Description 	Test Class for MX_RTL_Document_Selector
* @Changes
*  
*/
@isTest
public class MX_RTL_Document_Selector_Test {
    
    @testSetup
    static void setup() {
        Document documentObj;
        documentObj = new Document();
        documentObj.Body = Blob.valueOf('Some Document Text');
        documentObj.ContentType = 'application/pdf';
        documentObj.DeveloperName = 'PenguinsDoc';
        documentObj.IsPublic = true;
        documentObj.Name = 'PenguinsDoc';
        documentObj.FolderId = UserInfo.getUserId();
        insert documentObj;
    }
    
    /**
    * @Description 	Test Method for constructor
    * @Return 		NA
    **/
	@isTest
    static void constructorTest() {
        final MX_RTL_Document_Selector documentSelector = new MX_RTL_Document_Selector();
        System.assertNotEquals(null, documentSelector, 'Error on constructor');
    }
    
    /**
    * @Description 	Test Method for MX_RTL_Document_Selector.getDocuments()
    * @Return 		NA
    **/
	@isTest
    static void getDocumentsTestWithCondition() {
        final List<String> listString = new List<String>();
        listString.add('PenguinsDoc');
    	final List<Document> listDocuments = MX_RTL_Document_Selector.getDocuments('Id, DeveloperName', 'DeveloperName IN: listString', true , listString);
        System.assertNotEquals(null, listDocuments, 'Error on getDocuments');
    }
    
    /**
    * @Description 	Test Method for MX_RTL_Document_Selector.getDocuments()
    * @Return 		NA
    **/
	@isTest
    static void getDocumentsTestWithoutCondition() {
    	final List<Document> listDocuments = MX_RTL_Document_Selector.getDocuments('Id, DeveloperName', null, false , null);
        System.assertNotEquals(null, listDocuments, 'Error on getDocuments');
    }

}