/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Trigger para aclaraciones - handler

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     20/05/2020      Roberto Isaac Soto Granados           Creación Clase
*****************************************************************************************/
public without sharing class Aclaracion_Handler_cls extends TriggerHandler {
    /** Lista lstNewAclara*/
    List<BPyP_Aclaraciones__c> lstNewAclara   = (list<BPyP_Aclaraciones__c>)(Trigger.new);
    /** Lista lstOldAclara*/
    List<BPyP_Aclaraciones__c> lstOldAclara   = (list<BPyP_Aclaraciones__c>)(Trigger.old);
    /** Mapa mapNewAclara*/
    Map<Id,BPyP_Aclaraciones__c> mapNewAclara = (Map<Id,BPyP_Aclaraciones__c>)(Trigger.newMap);
    /** Mapa mapOldAclara*/
    Map<Id,BPyP_Aclaraciones__c> mapOldAclara = (Map<Id,BPyP_Aclaraciones__c>)(Trigger.oldMap);

    /*@beforeInsert event override en la Clase TriggerHandler
    Logica Encargada de los Eventos beforeInsert */
    protected override void beforeInsert() {
        MX_BPP_Aclaracion_Service.linkCases(lstNewAclara);
    }

}