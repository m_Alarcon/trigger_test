/**
* Indra
* @author           Julio Medellín Oliva
* Project:          Presuscritos
* Description:      Class to test Queries On Opportunity.
*
* Changes (Version)
* ------------------------------------------------------------------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    --------------------------------------
* @version  1.0     2020-05-25      Julio Medellín Oliva.         Creación de la Clase
* @version  1.1     2020-10-29      Gerardo Mendoza Aguilar       Creación methodo upsrtOppList
*/
@isTest
@supresswarnings('sf:AvoidFinalLocalVariable')
public class MX_RTL_Opportunity_Selector_Test {
    /*Property FIEDLS*/
    final static String FIELDS = 'Id,Name,createdDate';
    /**@description Nombre usuario*/
    private final static string ASESORNAME = 'AsesorTest';
    /**@description Nombre usuario*/
    private final static string OPPNAME = 'OppTelemarketing';

    /**
    * @Method Data Setup
    * @Description Method para preparar datos de prueba
    * @return void
    **/
    @testSetup
    public static void  data() {
        MX_SB_PS_OpportunityTrigger_test.makeData();
        MX_SB_VTS_CallCTIs_utility.initHogarGeneric();
    }
    /**
    * @Method getOpportunity_test
    * @param String oppid
    * @param Sring fields
    * @Description method que retorna un objeto User
    * @return Objeto User
    **/
    public static testMethod void  prueba1() {
       final Opportunity opp=[SELECT ID,Name,createdDate FROM Opportunity LIMIT 1];
        System.assertEquals(opp,MX_RTL_Opportunity_Selector.getOpportunity(opp.id, FIELDS),'Success');
    }

    @isTest
    static void initDataCotiz() {
        final User asesorUser = [Select Id from User where LastName =: ASESORNAME];
        MX_SB_VTS_CallCTIs_utility.insertPermissionVTS(asesorUser);
        Test.startTest();
        System.runAs(asesorUser) {
            final Set<String> lstOpps = new Set<String>();
            final Id oppId = [Select Id from Opportunity where Name =: OPPNAME].Id;
            lstOpps.add(oppId);
            final List<Opportunity> quoliDat = MX_RTL_Opportunity_Selector.selectSObjectsById(lstOpps);
            System.assertEquals(quoliDat.size(),1, 'Datos recuperados');
        }
        Test.stopTest();
    }

    @isTest
    static void findOppsTray() {
        final User asesorUser = [Select Id from User where LastName =: ASESORNAME];
        MX_SB_VTS_CallCTIs_utility.insertPermissionVTS(asesorUser);
        Test.startTest();
        System.runAs(asesorUser) {
            final Set<String> lstOpps = new Set<String>();
            final Id oppId = [Select Id from Opportunity where Name =: OPPNAME].Id;
            lstOpps.add(oppId);
            final List<Opportunity> quoliDat = MX_RTL_Opportunity_Selector.selectSObTrayById(lstOpps);
            System.assertEquals(quoliDat.size(),1, 'Datos Bandeja');
        }
        Test.stopTest();
    }

    /**
     * *Descripción: Clase de prueba para selectBppSObById
    **/
    @isTest
    static void testBppSObById() {
        final Set<Id> lstOpps = new Set<Id>();
        final Id oppId = [SELECT ID,Name FROM Opportunity LIMIT 1].Id;
        lstOpps.add(oppId);
        System.assertEquals(MX_RTL_Opportunity_Selector.selectBppSObById(lstOpps)[0].Id, oppId, 'Datos recuperados');
    }

    @isTest
    static void updateOpps() {
        final User asesorUser = [Select Id from User where LastName =: ASESORNAME];
        MX_SB_VTS_CallCTIs_utility.insertPermissionVTS(asesorUser);
        Test.startTest();
        System.runAs(asesorUser) {
            final List<Opportunity> lstOpps = new List<Opportunity>();
            final Opportunity oppId = [Select Id,EnviarCTI__c from Opportunity where Name =: OPPNAME];
            oppId.EnviarCTI__c = true;
            lstOpps.add(oppId);
            final List<Database.SaveResult> resultDatabase = MX_RTL_Opportunity_Selector.updateResult(lstOpps, false);
            System.assertEquals(resultDatabase.size(),1, 'Datos actualziados');
        }
        Test.stopTest();
    }

    /**
     * *Descripción: Clase de prueba para insertOpportunity
    **/
    @isTest static void testInsert() {
        final Opportunity oOpp = new Opportunity(Name='Test', StageName='Abierta', CloseDate=Date.today().addDays(15),
            RecordTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('MX_BPP_RedBpyp').getRecordTypeId()
            );
        Test.startTest();
        final List<Opportunity> lsOpp = MX_RTL_Opportunity_Selector.insertOpportunity(new List<Opportunity>{oOpp});
        Test.stopTest();
        System.assertNotEquals(lsOpp[0].Id, null, 'Insert fail');
    }

    /**
     * Descripción: Función de prueba para recuperación de datos de Opportunity
     * 				sin condiciones
     **/
    @isTest static void testQryOppoNCond() {
    	Test.startTest();
        final List<Opportunity> lstOpp = MX_RTL_Opportunity_Selector.resultQueryOppo('Id, Name', '', false);
        Test.stopTest();
        System.assert(!lstOpp.isEmpty(),'Se obtuvieron los datos');
    }

    /**
     * Descripción: Función de prueba para recuperación de datos de Opportunity
     * 				con condiciones
     **/
    @isTest static void testQryOppoCond() {
        Test.startTest();
        final List<Opportunity> lstOpoCond = MX_RTL_Opportunity_Selector.resultQueryOppo('Id, Name',  'Name = \'Opp1\'',true);
        Test.stopTest();
        System.assert(!lstOpoCond.isEmpty(), 'Se obtuvieron los datos');
    }

    /*
    * @description method que obtiene agregateResult
    * @param  void
    */
    @isTest
    @SuppressWarnings('sf:UnusedLocalVariable')
    static void fetchAROppByFiltersTest() {
        final List<Opportunity> listOppSel = [SELECT Id FROM Opportunity LIMIT 1];
        final List<Opportunity> listOpps = MX_RTL_Opportunity_Selector.fetchListOppFilters(' Id ', ' WHERE Id =: filtro0', new List<String>{listOppSel[0].Id,'','','','',''}, System.today(), System.today());
        final List<AggregateResult> listAggOpp = MX_RTL_Opportunity_Selector.fetchAROppByFilters(' count(Id) opp', ' WHERE Id =: filtro0', new List<String>{listOpps[0].Id,'','','','',''}, System.today(), System.today());
        System.assertEquals(listAggOpp[0].get('opp'), 1, 'Error SOQL');
    }
}