/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 01-14-2021
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   08-27-2020   Eduardo Hernández Cuamatzi   Initial Version
 * 1.1   14-01-2021   Diego Olvera Hernandez       Se agrega función de actualización
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_RTL_QuoteLineItems_Selector {
    
    /**
    * @description Recuperar Lista de QuoteLineItems
    * @author Eduardo Hernandez Cuamatzi | 25/5/2020 
    * @param Set<Id> lstQuotes lista de Quotes a evaluar
    * @return List<QuoteLineItem> lista de QuoteLineItems resultantes
    **/
    public static List<QuoteLineItem> selSObjectsByQuotes(Set<Id> lstQuotes) {
        return [Select Id, Product2Id, CreatedDate, LastModifiedDate, TotalPrice,QuoteId,Quantity, 
        MX_SB_VTS_Tipo_Plan__c, MX_SB_VTS_Monto_Mensualidad__c, MX_SB_VTS_Frecuencia_de_Pago__c,
        MX_SB_VTS_FormaPAgo__c, Product2.Name, MX_SB_PS_SumaAsegurada__c from QuoteLineItem where QuoteId IN: lstQuotes];
    }
     /**
    * @Method insertQuote
    * @param List<QuoteLineItem> lsQuosLine
    * @Description Inserta una lista de QuoteLineItem
    * @return List<QuoteLineItem>
    **/
    public static List<QuoteLineItem> insertQuotesLine(List<QuoteLineItem> lsQuosLine) {
        upsert lsQuosLine;
        return lsQuosLine;
    }
}