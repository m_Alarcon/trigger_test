/**
* BBVA - Mexico
* @Author: Jaime Terrats
* @Description: Controller for lwc mX_SB_VTS_GetAcccountContracts to retrieve all
*               the contracts from the account and show them as a related list in the
*               Opportunity Layout
* @Version: 1.0
**/
public without sharing class MX_SB_VTS_GetAccountContracts {
    /** Variable de Apoyo: sFldStatusPol */
    Static String sFldStatusPol = 'estatus_poliza_ws';
    /** Variable de Apoyo: sFldFeIniPol */
    Static String sFldFeIniPol = 'fecha_inicio_poliza_ws';
    /** Variable de Apoyo: sFldFeFinPol */
    Static String sFldFeFinPol = 'fecha_fin_poliza_ws';
    /** Variable de Apoyo: sFldSinDatos */
    Static String sFldSinDatos = 'Sin Datos!';


    /**
    * constructor
    */
    private MX_SB_VTS_GetAccountContracts() {} //NOSONAR

    /**
    * @method: fetchActiveContractsByAccount
    * @description returns a list of contracts
    * @param: oppId
    **/
    @AuraEnabled
    public static List<Object> fetchActiveContractsByAccount(Id oppId) {
        List<Contract> lstActCont = new List<Contract>();
        Map<String, Object> oReturnValues = null;
        Map<String, Object> oReturnValuesWs = null;
        List<Object> lstValCont = null;
        Opportunity objOpportunity = null;
        try {
            oReturnValuesWs = new Map<String, Object>();
            lstValCont = new List<Object>();
            objOpportunity = [Select AccountId, Account.Name from Opportunity where Id =: oppId];
            lstActCont = [Select Id, MX_WB_noPoliza__c, MX_SB_SAC_NumeroPoliza__c, MX_WB_Producto__r.Name, CreatedDate from Contract where AccountId =: objOpportunity.AccountId and Status IN ('Activated', 'Draft')];
            for(Contract oRow: lstActCont) {
                oReturnValues = new Map<String,Object>();
            	oReturnValues.put('nombre_cliente', objOpportunity.Account.Name);
            	oReturnValues.put('no_poliza', oRow.MX_SB_SAC_NumeroPoliza__c);
                oReturnValues.put('tipo_produto', oRow.MX_WB_Producto__r.Name);
                oReturnValues.put('fecha_origen_compra', oRow.CreatedDate);
                oReturnValuesWs = MX_SB_VTS_GetSetInsContract_Service.obtDataPolizContract(oRow.MX_SB_SAC_NumeroPoliza__c);
                if(Boolean.valueOf(oReturnValuesWs.get('isExist'))) {
                    if(Boolean.valueOf(oReturnValuesWs.get('isContractValid'))) {
                    	oReturnValues.put(sFldStatusPol, 'Poliza Incluida!');
                        oReturnValues.put(sFldFeIniPol, oReturnValuesWs.get('startDateContract'));
                        oReturnValues.put(sFldFeFinPol, oReturnValuesWs.get('endDateContract'));
                    } else {
                    	oReturnValues.put(sFldStatusPol, 'Poliza Incluida!');
                        oReturnValues.put(sFldFeIniPol, oReturnValuesWs.get('startDateContract'));
                        oReturnValues.put(sFldFeFinPol, oReturnValuesWs.get('endDateContract'));
                    }
                } else {
                    if(Boolean.valueOf(oReturnValuesWs.get('isServiceAvailable'))) {
                    	oReturnValues.put(sFldStatusPol, 'Poliza No Existe!');
                    	oReturnValues.put(sFldFeIniPol, sFldSinDatos);
                    	oReturnValues.put(sFldFeFinPol, sFldSinDatos);
                    } else {
                    	oReturnValues.put(sFldStatusPol, 'Servicio No Disponible: Poliza no Valida!');
                    	oReturnValues.put(sFldFeIniPol, sFldSinDatos);
                    	oReturnValues.put(sFldFeFinPol, sFldSinDatos);
                    }
                }
                lstValCont.add(oReturnValues);
            }
        } catch(System.QueryException qEx) {
            throw new AuraHandledException(System.Label.MX_WB_LG_ErrorBack + qEx);
        }
        return lstValCont;
    }
}