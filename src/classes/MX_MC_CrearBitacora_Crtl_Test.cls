/**
 * @File Name          : MX_MC_CrearBitacora_Crtl_Test.cls
 * @Description        : Clase para Test de MX_MC_CrearBitacora_Crtl_Test
 * @author             : Eduardo Barrera Martínez
 * @Group              : BPyP
 * @Last Modified By   : Eduardo Barrera Martínez
 * @Last Modified On   : 11/11/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		       Modification
 *==============================================================================
 * 1.0      11/11/2020           Eduardo Barrera Martínez        Initial Version
**/
@isTest
private class MX_MC_CrearBitacora_Crtl_Test {

    /*Usuario de pruebas*/
    private static User testUser = new User();

    /**
     * @Descripción: Clase de prueba para MX_MC_CrearBitacora_Crtl_Test
     * @author: eduardo.barrera3@bbva.com | 11/11/2020
    **/
    @TestSetup
    static void makeData() {

        testUser = UtilitysDataTest_tst.crearUsuario('testUser', 'BPyP Estandar', 'BPYP BANQUERO BANCA PERISUR');
        testUser.Title = 'Privado';
        insert testUser;

       System.runAs(testUser) {
            final gcal__GBL_Google_Calendar_Sync_Environment__c calEnvment = new gcal__GBL_Google_Calendar_Sync_Environment__c(Name = 'DEV');
            insert calEnvment;

            final Case convTest = new Case();
            convTest.mycn__MC_ConversationId__c = 'test_thradId_123';
            convTest.RecordTypeId = mycn.MC_ConversationsDatabase.getCaseConversationRecordType();
            convTest.Status = 'New';
            convTest.Subject = 'MC Message example';
            insert convTest;

            final dwp_kitv__Visit__c testVisit = new dwp_kitv__Visit__c();
            testVisit.dwp_kitv__visit_start_date__c = Date.today()+4;
            testVisit.dwp_kitv__visit_duration_number__c = '15';
            testVisit.dwp_kitv__visit_status_type__c = '05';
            testVisit.MX_MC_IdConv__c = convTest.id;
            insert testVisit;
       }
    }

    /**
     * @Descripción: Clase de prueba para MX_MC_CrearBitacora_Crtl_Test
     * @author: eduardo.barrera3@bbva.com | 11/11/2020
    **/
    @isTest
    static void testSearchAndRetrieve() {

        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        dwp_kitv__Visit__c visita = null;

        System.runAs(testUser) {

            final Case testCase = [SELECT ID FROM Case WHERE mycn__MC_ConversationId__c = 'test_thradId_123' LIMIT 1];
            final Boolean hasBitacora = MX_MC_CrearBitacora_Crtl.hasBitacora(testCase.id);
            if(!hasBitacora) {
              visita =  MX_MC_CrearBitacora_Crtl.importBitacora(testCase.Id);
            }
        }

        System.assert(visita != null, 'Error: visita no encontrada');
    }
}