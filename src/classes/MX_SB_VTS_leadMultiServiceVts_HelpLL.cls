/**
 * @description       : 
 * @author            : Jesus Alexandro Corzo Leon
 * @group             : 
 * @last modified on  : 14-09-2020
 * @last modified by  : Jesus Alexandro Corzo Leon
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   14-09-2020   Jesus Alexandro Corzo Leon   Initial Version
 * 1.1   22-09-2020   Marco Cruz                   Se agrega method de validación sin idQuote
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_leadMultiServiceVts_HelpLL {
    /** Variable de Apoyo: PERSONPHONE */
    final private static String PERSONPHONE = 'PER-PHONE-1';
    /** Variable de Apoyo: PEREMAIL */
    final private static String PEREMAIL = 'PER-EMAIL';
    /** Variable de Apoyo: NOCONTACTO */
    final private static String NOCONTACTO = 'No Contacto';
    /** Variable de Apoyo: NOAPLICA */
    final private static String NOAPLICA = 'No Aplica';
    /**Opportunity List */
    Final Public static List<Opportunity> LISTOPPUP = new List<Opportunity>();
    
    
    /**
    * @description Valida petición CMB conforme al Id de Cotización
    * @author Jesus Alexandro Corzo Leon | 24-09-2020 
    * @param consulta Valores de entrada del servicio
    * @return Boolean isSuccess
    **/
    
    public static Boolean validCMBTWCLeads(MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta) {
    	Boolean isSuccess = false;
        if(MX_SB_VTS_leadMultiServiceVts_HelpTW.validQuoteId(consulta)) {
            
			final Map<String, String> lstParamsContDet = obtParamsContactDetail(consulta);
            final String sPhoneNumber = lstParamsContDet.get('telefono');
            final List<Opportunity> lstOpportunitysTW = obtRecordsTWNoMark(sPhoneNumber);
            
            if(lstOpportunitysTW.isEmpty()) {
                isSuccess = true;
            } else {
                final String sIdCotizSrv = consulta.quote.id;
                final List<Opportunity> lstOppoDML = new List<Opportunity>();
                
                for(Opportunity oRowOppo : lstOpportunitysTW) {
                    if (sIdCotizSrv.equalsIgnoreCase(oRowOppo.FolioCotizacion__c)) {
                   		oRowOppo.StageName = 'Closed Lost';
                        oRowOppo.MX_SB_VTS_Tipificacion_LV1__c = NOCONTACTO;
                        oRowOppo.MX_SB_VTS_Tipificacion_LV2__c = NOCONTACTO;
                        oRowOppo.MX_SB_VTS_Tipificacion_LV3__c = NOAPLICA;
                        oRowOppo.MX_SB_VTS_Tipificacion_LV4__c = NOAPLICA;
                        oRowOppo.MX_SB_VTS_Tipificacion_LV5__c = 'No Venta (No Contacto)';
                        oRowOppo.MX_SB_VTS_Tipificacion_LV6__c = 'Atendido por Call me Back';
                        oRowOppo.Motivosnoventa__c = 'Agendar';
                        lstOppoDML.add(oRowOppo); 
                        isSuccess = true;
                    }
                    
                    if (!sIdCotizSrv.equalsIgnoreCase(oRowOppo.FolioCotizacion__c)) {
                   		oRowOppo.StageName = 'Closed Lost';
                        oRowOppo.MX_SB_VTS_Tipificacion_LV1__c = NOCONTACTO;
                        oRowOppo.MX_SB_VTS_Tipificacion_LV2__c = NOCONTACTO;
                        oRowOppo.MX_SB_VTS_Tipificacion_LV3__c = NOAPLICA;
                        oRowOppo.MX_SB_VTS_Tipificacion_LV4__c = NOAPLICA;
                        oRowOppo.MX_SB_VTS_Tipificacion_LV5__c = 'No Venta (No Contacto)';
                        oRowOppo.MX_SB_VTS_Tipificacion_LV6__c = 'Atendido por Call me Back';
                        oRowOppo.Motivosnoventa__c = 'Agendar'; 
                        lstOppoDML.add(oRowOppo); 
                        isSuccess = true;
                    }
                    
                }
                MX_RTL_Opportunity_Selector.updateResult(lstOppoDML,false);
            }
            
        } else {

           isSuccess = MX_SB_VTS_leadMultiServiceVts_HelpLL.notIdQuote(consulta);
        
        }
        return isSuccess;
    }
    
    /**
    * @description Check if a Open opportunity has the same phone as a lead and copy de idQoute on the lead which is related.
    * @author Marco Antonio Cruz Barboza
    * @param consulta Valores de entrada del servicio
    * @return Boolean isCompleted
    **/
    Public static Boolean notIdQuote (MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta) {
        Boolean isCompleted = false;
        Final Map<String,String> lstParamsContDet = obtParamsContactDetail(consulta);
        final String sPhoneNumber = lstParamsContDet.get('telefono');
        Final List<Opportunity> lstOpportunitysTW = obtRecordsTWNoMark(sPhoneNumber);
        
        if(!lstOpportunitysTW.isEmpty()) {
            for(Opportunity iterOpp : lstOpportunitysTW) {
                
                iterOpp.StageName = 'Closed Lost';
                iterOpp.MX_SB_VTS_Tipificacion_LV1__c = NOCONTACTO;
                iterOpp.MX_SB_VTS_Tipificacion_LV2__c = NOCONTACTO;
                iterOpp.MX_SB_VTS_Tipificacion_LV3__c = NOAPLICA;
                iterOpp.MX_SB_VTS_Tipificacion_LV4__c = NOAPLICA;
                iterOpp.MX_SB_VTS_Tipificacion_LV5__c = 'No Venta (No Contacto)';
                iterOpp.MX_SB_VTS_Tipificacion_LV6__c = 'Atendido por Call me Back';
                iterOpp.Motivosnoventa__c = 'Agendar'; 
                if(MX_SB_VTS_leadMultiServiceVts_HelpTW.validQuoteId(consulta)) {
                    consulta.quote.id = iterOpp.FolioCotizacion__c;
                }
                LISTOPPUP.add(iterOpp);
                
            }
            
            MX_RTL_Opportunity_Selector.updateResult(LISTOPPUP,false);
            isCompleted = true;
        }
        
        return isCompleted;
    }
	
	/**
    * @description Obtiene registros de TW por: Por: Numero Telefonico y Envio CTI[false]
    * @author Jesus Alexandro Corzo Leon | 14-09-2020 
    * @param consulta Valores de entrada del servicio
    * @return List<Opportunity> Lista de Oportunidades TW
    **/    
    public static List<Opportunity> obtRecordsTWNoMark(String sPhoneNumber) {  
        List<Opportunity> lstOpportunitysTW = new List<Opportunity>();
        final Integer iNumRecords = database.countQuery('SELECT COUNT() FROM Opportunity WHERE TelefonoCliente__c = \'' + sPhoneNumber + '\' AND EnviarCTI__c = false');
        if(iNumRecords > 0) {
            lstOpportunitysTW = MX_RTL_Opportunity_Selector.resultQueryOppo('Id, Name, TelefonoCliente__c, EnviarCTI__c, FolioCotizacion__c, StageName, MX_SB_VTS_Tipificacion_LV6__c', 'TelefonoCliente__c = \'' + sPhoneNumber + '\' AND EnviarCTI__c = false AND StageName NOT IN (\'Closed Lost\')', true);
        }
        return lstOpportunitysTW;
    }
    
    /**
    * @description Obtiene parametros: Telefono y Correo Electronico de la Sección: Contact Details
    * @author Jesus Alexandro Corzo Leon | 15-09-2020 
    * @param consulta Valores de entrada del servicio
    * @return Map<String, String> Lista de Strings con parametros
    **/
    public static Map<String, String> obtParamsContactDetail(MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta) {
        Final Map<String, String> lstParams = new Map<String, String>();
        for(MX_SB_VTS_leadMultiServWrapper.Mx_sb_Contacdetails temo : consulta.person.contactDetails) {
       		if(temo.key == PERSONPHONE) {
            	lstParams.put('telefono', temo.value);   
            }
            if (temo.key == PEREMAIL) {
				lstParams.put('correo', temo.value);                
            }
        }
        return lstParams;
    }

    /**
    * @description Valida si se agrega elemento AppointmentTime
    * @author Eduardo Hernández Cuamatzi | 10-22-2020 
    * @param consulta Datos de solicitud
    * @return Boolean Indica si el elemento esta declarado y correcto
    **/
    public static Boolean validateAppTime(MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta) {
        Boolean isAppTime = false;
        if(consulta.appointment != null && String.isNotEmpty(consulta.appointment.timex)) {
            isAppTime = true;
        }
        return isAppTime;
    }
}