/**
 * @description       : Class test MX_SB_SAC_Entitlement_Selector
 * @author            : Gerardo Mendoza Aguilar
 * @group             : 
 * @last modified on  : 03-05-2021
 * @last modified by  : Gerardo Mendoza Aguilar
 * Modifications Log 
 * Ver   Date         Author                    Modification
 * 1.0   03-04-2021   Gerardo Mendoza Aguilar   Initial Version
**/
@isTest
public with sharing class MX_SB_SAC_Entitlement_Selector_Test {
    /** Account string name */
    final static String ACCOUNT_NAME = 'Test';
    /** Entitlement string type*/
    final static String TYPEENT = 'Asistencia por Internet';
    
    /**
    * @description data Setup
    * @author Gerardo Mendoza Aguilar | 03-05-2021 
    **/
    @TestSetup
    static void makeData() {
        final Account acc = MX_WB_TestData_cls.createAccount(ACCOUNT_NAME, System.Label.MX_SB_VTS_PersonRecord);
        insert acc;
    }
    /**
    * @description 
    * @author Gerardo Mendoza Aguilar | 03-05-2021 
    **/
    @isTest
    public static void insertEntitlement() {
        Test.startTest();
        final Account acct = [Select Id, Name From Account limit 1];
        final Entitlement ent = new Entitlement(Type = TYPEENT, Name = acct.Name, AccountId = acct.Id);
        final List<Entitlement> entitlementList = new List<Entitlement>();
        entitlementList.add(ent);
        final List<Entitlement> insertResult = MX_SB_SAC_Entitlement_Selector.insertEntitlement(entitlementList);
        System.assertEquals(insertResult[0].Name, 'Test Test', 'Registro creado');
        Test.stopTest();
    }
}