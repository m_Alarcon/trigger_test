/**
 * @File Name          : MX_SB_VTS_MatrizRemarcado_Opp_cls.cls
 * @Description        : 
 * @Author             : Tania Vázquez Hernández
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 26/2/2020 11:34:08
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    18/11/2019   Tania Vázquez Hernández     Initial Version
**/
public without sharing class MX_SB_VTS_MatrizRemarcado_Opp_cls extends MX_SB_VTS_CalculateReCalls_cls {//NOSONAR
    /** Bandejas de marcado*/
    private final static Map<Id, MX_SB_VTS_Lead_tray__c> MAPTRAY = fillTrays();
    /** Bandejas de marcado*/
    private final static Map<String,MX_SB_VTS_Lead_tray__c> TRAYSEND = MX_SB_VTS_CalculateReCallsUtil_cls.findTrySend(MAPTRAY);
    /** PROVEEDORES */
    private final static Map<Id, MX_SB_VTS_ProveedoresCTI__c> PROVEEDORES = MX_SB_VTS_CalculateReCallsUtil_cls.fillProveedores();
    /** Mapa de llamadas */
    private final static Map<String,String> IDWRAPRECALL = new map<String,String>();
    /** Mapa de listas llamadas smart*/
    private final static List<WrapperRecallCTI> LSTWRAPSEND = new List<WrapperRecallCTI>();
    /**
    @remarcado remarcado para Oportunidades
    */
    @InvocableMethod
    public static void remarcado(List<Opportunity> lstRecOpps) {
        final Set<Id> lstOppsId = new Set<Id>();
        for(Opportunity opportunityRecord : lstRecOpps) {
            lstOppsId.add(opportunityRecord.Id);
        }
        final List<Opportunity> opportunityRe = [Select Id, MX_SB_VTS_Tipificacion_LV2__c, MX_SB_VTS_Tipificacion_LV3__c, Producto__c, MX_SB_VTS_Tipificacion_LV4__c, LeadSource,
        MX_SB_VTS_Tipificacion_LV5__c, MX_SB_VTS_Tipificacion_LV6__c,MX_SB_VTS_Tipificacion_LV7__c,MX_SB_VTS_RecallValue__c, MX_SB_VTS_TrayAttention__c, MX_SB_VTS_NextActionCall__c, AccountId,
        Account.PersonMobilePhone, Account.MX_WB_ph_Telefono1__pc,Account.MX_WB_ph_Telefono2__pc,Account.MX_WB_ph_Telefono3__pc, Account.Name, Account.Apellido_materno__pc, TelefonoCliente__c,Account.FirstName, Account.LastName
        from Opportunity where Id IN: lstOppsId];
        for(Opportunity opportunityRecord : opportunityRe) {
            if(String.isNotBlank(opportunityRecord.MX_SB_VTS_Tipificacion_LV7__c) && String.isNotBlank(opportunityRecord.MX_SB_VTS_TrayAttention__c)) {
                final MX_SB_VTS_Lead_tray__c traySending = TRAYSEND.get(opportunityRecord.MX_SB_VTS_TrayAttention__c);
                final MX_SB_VTS_ProveedoresCTI__c proveedor = PROVEEDORES.get(traySending.MX_SB_VTS_ProveedorCTI__c);
                final Map<String, String> recordVals = new Map<String, String>();
                recordVals.put('Id', opportunityRecord.Id);
                recordVals.put('nextAction', String.valueOf(Integer.valueOf(opportunityRecord.MX_SB_VTS_NextActionCall__c)));
                recordVals.put('level6', opportunityRecord.MX_SB_VTS_RecallValue__c);
                recordVals.put('phone1', evalutePhone(opportunityRecord.Account.PersonMobilePhone,opportunityRecord.TelefonoCliente__c ));
                recordVals.put('phone2', evalutePhone(opportunityRecord.Account.PersonMobilePhone,opportunityRecord.TelefonoCliente__c ));
                recordVals.put('phone3', evalutePhone(opportunityRecord.Account.PersonMobilePhone,opportunityRecord.TelefonoCliente__c ));
                recordVals.put('displayName', MX_SB_VTS_SendLead_helper_cls.fillFullName((SObject)opportunityRecord,' Remarc'));
                final WrapperRecallCTI wrappItem = calculateRules(recordVals, traySending, proveedor, opportunityRecord.MX_SB_VTS_Tipificacion_LV7__c, System.now());
                IDWRAPRECALL.put(wrappItem.idRecord, wrappItem.nextAction);
                lstWrapSend.add(wrappItem);    
            }
        }
        processWrap(lstWrapSend);
    }
	
	/**
	@Method processWrap procesa wrapper
	*/
	public static void processWrap(List<WrapperRecallCTI> lstWrapSendP) {
		final List<WrapperRecallCTI> lstWrapSendFinal = new List<WrapperRecallCTI>();
		final List <Opportunity> updateEl = new List <Opportunity> ();
        for(WrapperRecallCTI wrappTest : lstWrapSendP) {
            if(String.isNotBlank(wrappTest.idRecord)) {
                if(String.isNotBlank(wrappTest.scheduleDate)) {
                    lstWrapSendFinal.add(wrappTest);
                } else {
                    final Opportunity opp =new opportunity(id=wrappTest.idRecord, xmlEnvio__c = 'Fecha de remarcado:"", Accion siguiente:' +wrappTest.nextAction);
                    updateEl.add(opp);
                }
            }
        }
		updateRecs(updateEl, lstWrapSendFinal);
	}
	
	/**
	@Method updateRecs actualiza registros
	*/
	public static void updateRecs(List<Opportunity> updateEl, List<WrapperRecallCTI> lstWrapSendFinal) {
		if(updateEl.isEmpty() == false) {
			update updateEl;
		}
        if(lstWrapSendFinal.isEmpty() == false) {
            MX_SB_VTS_CalculateReCallsUtil_cls.sendSmart(lstWrapSendFinal,IDWRAPRECALL, true);
        }
	}	
}