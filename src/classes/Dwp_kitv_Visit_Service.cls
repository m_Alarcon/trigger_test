/**
 * @File Name          : Dwp_kitv_Visit_Service.cls
 * @Description        : Service class for Visit object
 * @Author             : Marco Antonio Cruz
 * @Group              :
 * @Last Modified By   : Marco Antonio Cruz
 * @Last Modified On   : 07/09/2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    07/09/2020      Marco Antonio Cruz       Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton, sf:AvoidGlobalModifier')
global class Dwp_kitv_Visit_Service {
    
    /**
    * @description: Insert a Visit list 
    * @author Marco Cruz | 07/09/2020
    * @param List<dwp_kitv__Visit__c> listVisit
    **/
    public static void insertVisit(List<dwp_kitv__Visit__c> listVisit) {
        Dwp_kitv_Visit_Selector.insertVisit(listVisit);
    }

}