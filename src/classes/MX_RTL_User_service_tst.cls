/**
* Name: MX_SB_PC_AsignacionCola_tst
* @author Jose angel Guerrero
* Description : Clase Test para Controlador de componente Lightning de asignación de Colas
*
* Ver              Date            Author                   Description
* @version 1.0     Jul/13/2020     Jose angel Guerrero      Initial Version
*
**/
@isTest
public class MX_RTL_User_service_tst {
    /**
* @description: funcion constructor
*/
    @isTest
    public static void serviceBucarUsuario() {
        final list <user> nuevousuario = MX_RTL_User_service.serviceBucarUsuario('%Multiasistencia');
        final list <User> nueva = new list <User>();
        test.startTest();
        System.assertEquals(nuevousuario, nueva, 'SUCESS');
        test.stopTest();
    }
    /**
* @description: funcion constructor
*/
    @isTest
    public static void serviceBucarUsuRol() {
        final list <user> nuevousuario = MX_RTL_User_service.serviceBucarUsuRol('colaID');
        final list <User> nueva = new list <User>();
        test.startTest();
        System.assertEquals(nuevousuario, nueva, 'SUCESS');
        test.stopTest(); 
    }
}