/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase Selector para objeto dwp_kitv_Visit

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     20/05/2020      Roberto Isaac Soto Granados           Creación Clase
*   1.1     07/09/2020      Marco Cruz				              Se agrega method para inserción de Lista de Visitas
*   1.1     07/10/2020      Gabriel García				          Se agregan method de AggregateResult y clausula con filtros
*	1.2 	13/01/2021		Edmundo Zacarias					  Se agrega method getVisitInfoByIds
*****************************************************************************************/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class Dwp_kitv_Visit_Selector {
	/*Inserta UNA nueva visita y devuelve Id*/
    public static Id creaVisita(List<dwp_kitv__Visit__c> visitas) {
        insert visitas;
        return visitas[0].Id;
    }

    /**
    * @description: Insert a Visit list
    * @author Marco Cruz | 07/09/2020
    * @param List<dwp_kitv__Visit__c> listVisit
    **/
    public static void insertVisit(List<dwp_kitv__Visit__c> listVisit) {
        insert listVisit;
    }

    /**
    * @description: update a Visit List
    * @author Edmundo Zacarias | 10/02/2020
    * @param List<dwp_kitv__Visit__c> listVisit
    **/
    public static void updteVisit (List<dwp_kitv__Visit__c> listVisit) {
        update listVisit;
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param String queryfields
    * @param String queryFilters
    * @param List<String> listFilteros
    * @param Datetime stDate
    * @param Datetime enDate
    * @return List<AggregateResult>
    * @example Query SELECT Id, Name FROM dwp_kitv__Visit__c WHERE Id =:filtro0 AND Name =: filtro1 AND CreatedDate >=: stDate;
    **/
    @SuppressWarnings('sf:DUDataflowAnomalyAnalysis, sf:UnusedLocalVariable')
    public static List<AggregateResult> fetchListAggVisitByDataBase(String queryfields, String queryFilters, List<String> listFilters, DateTime stDate, Datetime enDate) {
        List<String> lsFil = new List<String>(8);
        Integer posicion = 0;
        for(String filtro : listFilters) {
            lsFil[posicion] = filtro;
            posicion++;
        }

        final String filtro0 = lsFil[0];
        final String filtro1 = lsFil[1];
        final String filtro2 = lsFil[2];
        final String filtro3 = lsFil[3];
        final String filtro4 = lsFil[4];
        final String filtro5 = lsFil[5];
        final String filtro6 = lsFil[6];
        final String filtro7 = lsFil[7];
        return Database.query('SELECT ' + String.escapeSingleQuotes(queryfields) + ' FROM dwp_kitv__Visit__c ' + String.escapeSingleQuotes(queryFilters));
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @param String queryfields
    * @param String queryFilters
    * @param List<String> listFilters
    * @param Datetime stDate
    * @param Datetime enDate
    * @return List<dwp_kitv__Visit__c>
    * @example Query SELECT Id, Name FROM dwp_kitv__Visit__c WHERE Id =:filtro0 AND Name =: filtro1 AND CreatedDate >=: stDate;
    **/
    @SuppressWarnings('sf:DUDataflowAnomalyAnalysis, sf:UnusedLocalVariable')
    public static List<dwp_kitv__Visit__c> fetchListVisitByParams(String queryfields, String queryFilters, List<String> listFilters, DateTime stDate, Datetime enDate) {
        List<String> lstToFilters = new List<String>(8);
        Integer posicion = 0;
        for(String filtro : listFilters) {
            lstToFilters[posicion] = filtro;
            posicion++;
        }

        final String filtro0 = lstToFilters[0];
        final String filtro1 = lstToFilters[1];
        final String filtro2 = lstToFilters[2];
        final String filtro3 = lstToFilters[3];
        final String filtro4 = lstToFilters[4];
        final String filtro5 = lstToFilters[5];
        final String filtro6 = lstToFilters[6];
        final String filtro7 = lstToFilters[7];
        return Database.query('SELECT ' + String.escapeSingleQuotes(queryfields) + ' FROM dwp_kitv__Visit__c ' + String.escapeSingleQuotes(queryFilters));
    }
    
    /**
    * @description
    * @author Edmundo Zacarias Gómez
    * @param String queryfields, List<Id> idsCampaigMembers
    * @return List<CampaignMember>
    **/
    public static List<dwp_kitv__Visit__c> getVisitInfoByIds(String queryfields, Set<Id> idsVisits) {
        return Database.query('SELECT ' + String.escapeSingleQuotes(queryfields) + ' FROM dwp_kitv__Visit__c WHERE Id in :idsVisits');
    }
}