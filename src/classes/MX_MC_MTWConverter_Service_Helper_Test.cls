/**
 * @File Name          : MX_MC_MTWConverter_Service_Helper_Test.cls
 * @Description        : Clase para Test de MX_MC_MTWConverter_Service_Helper
 * @author             : Eduardo Barrera Martínez
 * @Group              : BPyP
 * @Last Modified By   : Eduardo Barrera Martínez
 * @Last Modified On   : 10/07/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      10/07/2020           Eduardo Barrera Martínez.       Initial Version
**/
@isTest
private class MX_MC_MTWConverter_Service_Helper_Test {

    /**
     * @Description Clase de prueba para MX_MC_MTWConverter_Service_Helper
     * @author eduardo.barrera3@bbva.com | 10/07/2020
    **/
    @isTest
    static void createWrapperListConversations() {
        final MX_MC_MTWConverter_Service_Helper serviceHelper = new MX_MC_MTWConverter_Service_Helper();
        final List<MX_MC_MTWConverter_Service_Helper.MessageThreadConversation> listThreadConv = new List<MX_MC_MTWConverter_Service_Helper.MessageThreadConversation>();
        final MX_MC_MTWConverter_Service_Helper.MessageThreadConversation conv = new MX_MC_MTWConverter_Service_Helper.MessageThreadConversation();
        conv.id = 'test_conv_id';
        listThreadConv.add(conv);
        final MX_MC_MTWConverter_Service_Helper.Link link = new MX_MC_MTWConverter_Service_Helper.Link();
        final MX_MC_MTWConverter_Service_Helper.Pagination pagConv = new MX_MC_MTWConverter_Service_Helper.Pagination();
        pagConv.links = link;
        final MX_MC_MTWConverter_Service_Helper.MessageThreadPage threadPage =  new MX_MC_MTWConverter_Service_Helper.MessageThreadPage();
        threadPage.data = listThreadConv;
        threadPage.pagination = pagConv;
        threadPage.toJson();
        threadPage.toList();
        System.assert(serviceHelper != null, 'El resultado no es el esperado');
    }

    /**
     * @Description Clase de prueba para MX_MC_MTWConverter_Service_Helper
     * @author eduardo.barrera3@bbva.com | 10/07/2020
    **/
    @isTest
    static void createWrapperListMessages() {
        final List<MX_MC_MTWConverter_Service_Helper.Message> listThreadMess = new List<MX_MC_MTWConverter_Service_Helper.Message>();
        final MX_MC_MTWConverter_Service_Helper.Message message = new MX_MC_MTWConverter_Service_Helper.Message();
        message.id = 'test_message_id';
        listThreadMess.add(message);
        final MX_MC_MTWConverter_Service_Helper.Link link = new MX_MC_MTWConverter_Service_Helper.Link();
        final MX_MC_MTWConverter_Service_Helper.Pagination pagConv = new MX_MC_MTWConverter_Service_Helper.Pagination();
        pagConv.links = link;
        final MX_MC_MTWConverter_Service_Helper.MessageThread messageThread = new MX_MC_MTWConverter_Service_Helper.MessageThread();
        messageThread.data = listThreadMess;
        messageThread.pagination = pagConv;
        messageThread.toJson();
        messageThread.toList();
        System.assertEquals(1, messageThread.data.size(), 'El resultado no es el esperado');
    }
}