/**
* @FileName          : MX_SB_VTS_wrpModHomePolTempDataSrv
* @description       : Wrapper class for use de web service of modifyHomePolicyTemp
* @Author            : Alexandro Corzo
* @last modified on  : 20-01-2021
* Modifications Log 
* Ver   Date         Author                Modification
* 1.0   20-01-2021   Alexandro Corzo       Initial Version
* 1.1   28-01-2021   Alexandro Corzo       Se realizan ajustes a Wrapper para consumo ASO
* 1.2   08-02-2021   Alexandro Corzo       Se realizan ajustes a clase para Codesmells
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton, sf:ExcessivePublicCoun,sf:LongVariable,sf:LongVariable,sf:ExcessivePublicCount, sf:ShortVariable,sf:ShortClassName') 
public class MX_SB_VTS_wrpModHomePolTempDataSrv {
	/**
	 * Invocación de Wrapper
	 */
    public base base {get; set;}
    
    /**
     * @description : Clase del objeto base del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class base {
        /** Invocación de Clase: header */
        public header header {get;set;}
        /** Invocación de Clase: homeInsurancePolicies */
        public homeInsurancePolicies homeInsurancePolicies {get;set;}
    }
    
    /**
     * @description : Clase del objeto header del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class header {
        /** Atributo de Clase: aapType */
        public String aapType {get;set;}
        /** Atributo de Clase: dateRequest */
        public String dateRequest {get;set;}
        /** Atributo de Clase: channel */
        public String channel {get;set;}
        /** Atributo de Clase: subChannel */
        public String subChannel {get;set;}
        /** Atributo de Clase: branchOffice */
        public String branchOffice {get;set;}
        /** Atributo de Clase: managementUnit */
        public String managementUnit {get;set;}
        /** Atributo de Clase: user */
        public String user {get;set;}
        /** Atributo de Clase: idSession */
        public String idSession {get;set;}
        /** Atributo de Clase: idRequest */
        public String idRequest {get;set;}
        /** Atributo de Clase: dateConsumerInvocation */
        public String dateConsumerInvocation {get;set;}
    }
    
    /**
     * @description : Clase del objeto homeInsurancePolicies del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class homeInsurancePolicies {
        /** Atributo de Clase: quoteId */
        public String quoteId {get;set;}
        /** Atributo de Clase: certifiedNumber */
        public String certifiedNumber {get;set;}
        /** Invocación de Clase: property */
        public property property {get;set;}
    }
    
    /**
     * @description : Clase del objeto property del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class property {
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Invocación de Clase: address */
        public address address {get; set;}
    }
    
    /**
     * @description : Clase del objeto address del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class address {
        /** Invocación de Clase: country */
        public country country {get;set;}
        /** Invocación de Clase: state */
        public state state {get;set;}
        /** Invocación de Clase: city */
        public city city {get;set;}
        /** Invocación de Clase: township */
        public township township {get;set;}
        /** Invocación de Clase: neighborhood */
        public neighborhood neighborhood {get;set;}
        /** Invocación de Clase: postalData */
        public postalData postalData {get;set;}
        /** Atributo de Clase: streetName */
        public String streetName {get;set;}
        /** Atributo de Clase: streetNumber */
        public String streetNumber {get;set;}
        /** Atributo de Clase: door */
        public String door {get;set;}
        /** Atributo de Clase: condominium */
        public String condominium {get;set;}
    }
    
    /**
     * @description : Clase del objeto country del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class country {
        /** Atributo de Clase: iddata */
        public String iddata {get;set;}
    }
    
    /**
     * @description : Clase del objeto state del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class state {
        /** Atributo de Clase: iddata */
        public String iddata {get;set;}
    }
    
    /**
     * @description : Clase del objeto city del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class city {
        /** Atributo de Clase: iddata */
        public String iddata {get;set;}
    }
    
    /**
     * @description : Clase del objeto township del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class township {
        /** Atributo de Clase: iddata */
        public String iddata {get;set;}
    }
    
    /**
     * @description : Clase del objeto neighborhood del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class neighborhood {
        /** Atributo de Clase: iddata */
        public String iddata {get;set;}
    }
    
    /**
     * @description : Clase del objeto postalData del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class postalData {
        /** Atributo de Clase: code */
        public String code {get;set;}
    }
}