/*
----------------------------------------------------------
* Nombre: MX_SB_PS_wrpRevisionPlanesResp
* Julio Medellin
* Proyecto: Salesforce-Presuscritos
* Descripción : clase controladora de front System.Label.MX_SB_PS_Etapa1.

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   Descripción
* --------------------------------------------------------------------------------
* 1.0           08/07/2020     Julio Medellin           Creación
* 1.1           07/08/2020     Daniel Perez             Documentación
* --------------------------------------------------------------------------------
*/
 @SuppressWarnings('sf:LongVariable,sf:ShortVariable')
public class MX_SB_PS_wrpRevisionPlanesResp {
    /*
    *Variable para clase wraper revision de planes
    */    
    /*
    *Variable iCatalogItem para subclase de response wraper 
    */
    public iCatalogItem iCatalogItem {get;set;}
    
    /*
    *@decription subclase para clase wraper revision de planes
    */
    public class iCatalogItem {
        /*
        *Variable revisionPlanList para subclase de response wraper 
        */
        public revisionPlanList[] revisionPlanList {get;set;}
    }
    
    /*
    *@decription subclase para clase wraper revision de planes
    */
    public class revisionPlanList {
        /*
        *Variable catalogItemBase para subclase de response wraper 
        */
        public catalogItemBase catalogItemBase {get;set;}
        /*
        *Variable planReview para subclase de response wraper 
        */
        public string planReview {get;set;}
        /*
        *Variable allianceCode para subclase de response wraper 
        */
        public string allianceCode {get;set;} 
        /*
        *Variable productCode para subclase de response wraper 
        */
        public string productCode {get;set;}
        /*
        *Variable planCode para subclase de response wraper 
        */
        public string planCode {get;set;}
        
    }
    
    /*
    *@decription subclase para clase wraper revision de planes
    */ 
    public class catalogItemBase {
        /*
        *Variable id para subclase de response wraper 
        */
         public string id {get;set;} 
    }
    
    
    }