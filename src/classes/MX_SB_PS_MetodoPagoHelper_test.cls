/*
----------------------------------------------------------
* Nombre: MX_SB_PS_MethodoPagoHelper_test
* Daniel Perez Lopez
* Proyecto: Salesforce-Presuscritos
* Descripción : clase test para clase MX_SB_PS_MethodoPagoHeader_cls

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   Descripción
* --------------------------------------------------------------------------------
* 1.0           08/07/2020     Daniel Perez		        Creación
* --------------------------------------------------------------------------------
*/
@IsTest
public with sharing class MX_SB_PS_MetodoPagoHelper_test {

    
    /*
    * @description prueba method modifyrequest
    */
    @isTest static void modReqTest() {
        HttpRequest req = new HttpRequest();
        req.setEndPoint('https://salty-reaches-97112.herokuapp.com/servicetest123');
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json;charset=UTF-8');
        Test.startTest();
            Final MX_SB_PS_MetodoPagoHelper_cls mpreq = new MX_SB_PS_MetodoPagoHelper_cls();
            req = mpreq.modifyRequest(req);
            System.assertEquals(req.getHeader('tsec'),'kjhy65rfdsw3456yhjko09876543wasdfghjUyh','Exito en validacion');
        Test.stopTest();
    }
}