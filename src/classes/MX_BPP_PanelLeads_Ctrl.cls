/**
* @File Name          : MX_BPP_PanelLeads_Ctrl.cls
* @Description        :
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 07/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      07/06/2020            Gabriel Garcia Rojas          Initial Version
**/
public with sharing class MX_BPP_PanelLeads_Ctrl {

    /**Test Constructor */
    @TestVisible
    private MX_BPP_PanelLeads_Ctrl() { }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return Map<String,Object>
    **/
    @AuraEnabled(cacheable=false)
    public static Map<String,Object> convertLead(Id leadId) {
        return MX_BPP_PanelLeads_Service.convertLeadToOpp(leadId);
    }

    /**
    * @description
    * @author Gabriel Garcia Rojas
    * @return CampaignMember
    **/
    @AuraEnabled(cacheable=true)
    public static CampaignMember getCampignMembers(Id leadId) {
        return MX_BPP_PanelLeads_Service.getinfoCampignMembers(LeadId);
    }
}