/**
* @author bbva.com developers
* @date 2019
*
* @group global_hub_kit_visit
*
* @description Test Class for GBL_PersonAccountCompatibility_Cls
**/
@isTest
private with sharing class GBL_PersonAccountCompatibility_Test {
    @isTest
    static void testConstructor() {
        final GBL_PersonAccountCompatibility_Cls testConstructor = new GBL_PersonAccountCompatibility_Cls();
        System.assert(testConstructor != null, 'Constructor created');
    }
    @isTest
    static void testmethodIsPersonAccountEnabled() {
        Boolean isPerAccVal = false;
        isPerAccVal = GBL_PersonAccountCompatibility_Cls.isPersonAccountEnabled();
        System.assert(isPerAccVal, 'Person Account is enable');
    }
}