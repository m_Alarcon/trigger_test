/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_TablaServicios_cls
* Autor Marco BArboza
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : servicios en poliza

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           10/12/2019      Marco Barboza                       Creación
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_TablaServicios_cls {//NOSONAR
	 /**
	*Met que regresa la lista de servicios creados en WorkOrder dependiendo el Contrato
	**/
    public class getCases {
        /** caseId */
        public String caseId { get;set; }
        /** nameProveedor */
        public String nameProveedor { get;set; }
        /** nameServicio */
        public String nameServicio { get;set; }
    }
    /**
	*Met que regresa la lista de servicios creados en WorkOrder dependiendo el Contrato
	**/
    @auraEnabled
    public static List<WorkOrder> getServicio (String sinId) {
       List<WorkOrder> serviceList = new List<WorkOrder>();
       final String contratoId = [SELECT MX_SB_SAC_Contrato__c FROM Siniestro__c WHERE Id=:sinId].MX_SB_SAC_Contrato__c;
        if(!String.isEmpty(contratoId)) {
           serviceList = [SELECT CreatedDate,
                                            Id,
                                            WorkOrderNumber,
                                            MX_SB_MLT_TipoSiniestro__c,
                                            MX_SB_MLT_EstatusServicio__c,
                                            MX_SB_MLT_SegmentClient__c
                                            FROM WorkOrder WHERE MX_SB_MLT_Siniestro__c IN (SELECT Id 
                                                                   FROM Siniestro__c 
                                                                   WHERE (MX_SB_SAC_Contrato__c=:contratoId)) AND 
                                                                   MX_SB_MLT_TipoSiniestro__c IN ('Gasolina', 'Grua', 'Cambio de llanta', 'Paso de corriente') limit 5000];
         	}
           return serviceList;
    }
    /**
	*Met para el conteo de servicios para muestreo de creación de servicios en tabla
	**/
    @auraEnabled
    public static Map<String,Boolean> countServicio (String sinId) {
                
        final List<WorkOrder> listaServicios = [SELECT MX_SB_MLT_TipoSiniestro__c FROM WorkOrder WHERE MX_SB_MLT_Siniestro__c=:sinId AND MX_SB_MLT_EstatusServicio__c NOT IN('Cancelado') limit 5000];
        final Map<String,Boolean> listaServicio = new Map<String,Boolean>();
            listaServicio.put('Grua',false);
            listaServicio.put('Cambio de llanta',false);
            listaServicio.put('Corriente',false);
            listaServicio.put('Gasolina',false);
                for(WorkOrder item : listaServicios) {
                        listaServicio.put(item.MX_SB_MLT_TipoSiniestro__c,true);  
                }
        return listaServicio;
    }
}