/**
 * @description       : Clase que prueba los methodos de MX_SB_PS_RetrieveDataExt_Ctrl
 * @author            : Daniel.perez.lopez.contractor@bbva.com
 * @group             : 
 * @last modified on  : 12-01-2020
 * @last modified by  : Juan Carlos Benitez
 * Modifications Log 
 * Ver   Date         Author                                    Modification
 * 1.0   11-20-2020   Daniel.perez.lopez.contractor@bbva.com   Initial Version
**/
@isTest
public class MX_SB_PS_RetrieveDataExt_Ctrl_Test {
/** Nombre de usuario Test */
    final static String EDAD = 'edad';
/** Nombre de usuario Test */
    final static String PARNT = 'parentesco';
@istest
    static void testgetcodes() {
        test.startTest();
        	MX_SB_PS_RetrieveDataExt_Ctrl.getcodes('TITULAR',PARNT);
        	MX_SB_PS_RetrieveDataExt_Ctrl.getcodes('CONYUGE',PARNT);
        	MX_SB_PS_RetrieveDataExt_Ctrl.getcodes('HIJO',PARNT);
        	MX_SB_PS_RetrieveDataExt_Ctrl.getcodes('23',EDAD);
        	MX_SB_PS_RetrieveDataExt_Ctrl.getcodes('17',EDAD);
        	MX_SB_PS_RetrieveDataExt_Ctrl.getcodes('57',EDAD);
        	Final string code =MX_SB_PS_RetrieveDataExt_Ctrl.getcodes('77',EDAD);
        	system.assertEquals('004',code ,'Codigo encontrado');
        test.stopTest();
    }
}