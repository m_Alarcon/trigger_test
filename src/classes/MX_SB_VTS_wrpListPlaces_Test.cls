@isTest
/**
* @FileName          : MX_SB_VTS_wrpListPlaces_Test
* @description       : Test class from MX_SB_VTS_wrpListPlaces_Utils
* @Author            : Marco Antonio Cruz Barboza
* @last modified on  : 12-01-2021
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   12-01-2021   Marco Antonio Cruz Barboza          Initial Version
**/
public class MX_SB_VTS_wrpListPlaces_Test {

    /** Username for an apex test */
    final static String NAMESR = 'UserTest';
    /** Account Name for an apex test*/
    final static String TSTCUENTA = 'Test Account';
    
    @TestSetup
    static void setupTest() {
        final User usuarioTst = MX_WB_TestData_cls.crearUsuario(NAMESR, System.Label.MX_SB_VTS_ProfileAdmin);
        System.runAs(usuarioTst) {
            final Account cuentaTst = MX_WB_TestData_cls.crearCuenta(TSTCUENTA, System.Label.MX_SB_VTS_PersonRecord);
            cuentaTst.PersonEmail = 'pruebaVts@mxvts.com';
            insert cuentaTst;
        }
    }
    
        /*
    @Method: wrpColoniasRespTest
    @Description: Use a wrapper class to deserialize a response from a web service.
	@param
    */
    @isTest
    static void wrpListPlaces() {
        test.startTest();
        	Final MX_SB_VTS_wrpListPlaces_Utils.country country = new MX_SB_VTS_wrpListPlaces_Utils.country();
        	Final MX_SB_VTS_wrpListPlaces_Utils.state state = new MX_SB_VTS_wrpListPlaces_Utils.state();
        	Final MX_SB_VTS_wrpListPlaces_Utils.geographicPlaceTypes geographicTps = new MX_SB_VTS_wrpListPlaces_Utils.geographicPlaceTypes();
        	Final MX_SB_VTS_wrpListPlaces_Utils.geolocation location = new MX_SB_VTS_wrpListPlaces_Utils.geolocation();
        	Final MX_SB_VTS_wrpListPlaces_Utils.geographicPlaceTypes[] geographicLst = new MX_SB_VTS_wrpListPlaces_Utils.geographicPlaceTypes[]{};
            Final MX_SB_VTS_wrpListPlaces_Utils.data data = new MX_SB_VTS_wrpListPlaces_Utils.data();
        	Final MX_SB_VTS_wrpListPlaces_Utils.data[] dataLst = new MX_SB_VTS_wrpListPlaces_Utils.data[]{};
            Final MX_SB_VTS_wrpListPlaces_Utils wrapperGnl = new MX_SB_VTS_wrpListPlaces_Utils();
                country.id = '12';
        		country.name = 'MEXICO';
        		state.id = '01';
        		state.name = 'CIUDAD DE MEXICO';
        		geographicTps.id = '01';
        		geographicTps.name = 'COYOACAN';
        		geographicTps.value = '13';
        		location.latitude = '213242524432';
        		location.longitude = '-323245253452';
        		geographicLst.add(geographicTps);
        		data.id = '20';
        		data.name = 'test';
        		data.country = country;
        		data.state = state;
        		data.geolocation = location;
        		data.geographicPlaceTypes = geographicLst;
        		dataLst.add(data);
        		wrapperGnl.data = dataLst;
        		
        	System.assertNotEquals(wrapperGnl, null,'El wrapper esta retornando');
        test.stopTest();
    }
    
}