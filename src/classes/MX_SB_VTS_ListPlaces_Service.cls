/**
 * @File Name          : MX_SB_VTS_ListPlaces_Service
 * @Description        :
 * @Author             : Marco Antonio Cruz Barboza
 * @Group              :
 * @Last Modified By   : Marco Antonio Cruz Barboza
 * @Last Modified On   : 26/12/2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    26/12/2020    Marco Antonio Cruz Barboza         Initial Version
**/
@SuppressWarnings('sf:AvoidGlobalModifier, sf:UseSingleton')
global class MX_SB_VTS_ListPlaces_Service {
	/** Statuscode for Service */
    Private static final Integer OKSTATUS = 200;
   	/** List for Geolocation */
    public static Final List<Object> LATITUDE = new List<Object>();
   	/** List for Geolocation */
    public static Final List<Object> LONGITUDE = new List<Object>();
    /** List for STATUSCODE */
    public static Final List<Object> STATUSCODE = new List<Object>();
    
    /*
    @Method: getGeolocation
    @Description: Return a Map<String, List<Object>> with laittude and longitude from the web service listPlaces
    @Param: String zipCode
    @Return: Map<String, List<Object>>
    */
    public static Map<String,List<Object>> getGeolocation(String zipCode) {
        Final Map<String,Object> serviceParam = new Map<String,Object>{'ZIPCODE' => zipCode};
        Final HttpRequest requestList = createRequest();
        final Map<String,List<Object>> valuesZipCode = new Map<String,List<Object>>();
        try {
            final HttpResponse response = MX_RTL_IasoServicesInvoke_Selector.callServices('listPlaces', serviceParam, requestList);
            if(response.getStatusCode() == OKSTATUS) {
                
                final Map<String, Object> mapGeolo = (Map<String, Object>)Json.deserializeUntyped(response.getBody());
                Final Map<String,Object> casteo = (Map<String,Object>)((List<Object>)mapGeolo.get('data'))[0];
                if(casteo.containsKey('geolocation') == false) {
                    LATITUDE.add(0);
                    LONGITUDE.add(0);
                    valuesZipCode.put('latitude',LATITUDE);
                    valuesZipCode.put('longitude',LONGITUDE);
                } else {
                    final MX_SB_VTS_wrpListPlaces_Utils wrapper = (MX_SB_VTS_wrpListPlaces_Utils)JSON.deserialize(response.getBody(),MX_SB_VTS_wrpListPlaces_Utils.class);
                    for(MX_SB_VTS_wrpListPlaces_Utils.data test : wrapper.data) {
                        LATITUDE.add(test.geolocation.latitude);
                    }
                    valuesZipCode.put('latitude', LATITUDE);
                    
                    for(MX_SB_VTS_wrpListPlaces_Utils.data test : wrapper.data) {
                        LONGITUDE.add(test.geolocation.longitude);
                    }
                    valuesZipCode.put('longitude', LONGITUDE);
                    
                    STATUSCODE.add(true);
                    valuesZipCode.put('statusCode', STATUSCODE);	                       
                }
                
            } else {
                STATUSCODE.add(false);
                valuesZipCode.put('statusCode', STATUSCODE);
            }
        } catch (System.CalloutException except) {
            STATUSCODE.add(false);
            valuesZipCode.put('statusCode', STATUSCODE);
        }


        return valuesZipCode;
    }
    
    /*
    @Method: createRequest
    @Description: Create a request body and return it
    @Param: 
    @Return: HttpRequest
    */
    public static HttpRequest createRequest() {
        Final Map<String,iaso__GBL_Rest_Services_Url__c> rstServices = iaso__GBL_Rest_Services_Url__c.getAll();
        Final iaso__GBL_Rest_Services_Url__c getUrlLP = rstServices.get('listPlaces');
        Final String endpointService = getUrlLP.iaso__Url__c;
        Final HttpRequest requestPC = new HttpRequest();
        requestPC.setTimeout(120000);
        requestPC.setMethod('GET');
        requestPC.setHeader('Content-Type', 'application/json');
        requestPC.setHeader('Accept', '*/*');
        requestPC.setHeader('Host', 'https://test-sf.bbva.mx/QRVS_A02');
        requestPC.setEndpoint(endpointService);
        return requestPC;
    }
    
}