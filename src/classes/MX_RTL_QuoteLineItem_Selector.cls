/**
* @author           Daniel perez lopez
* Project:          Presuscritos
* Description:      Clase Selector para objeto QuoteLineItem
*
* Changes (Version)
* ------------------------------------------------------------------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    --------------------------------------
* @version  1.0     2020-09-05      Daniel perez lopez.     Creación de la Clase
*           1.1     2020-12-15      Daniel Perez Lopez      Se añade condicion wn switch y methodo quoteliItemsordr
*/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_RTL_QuoteLineItem_Selector {
    /** Record to be returned */
    final static List<SObject> RET_QLITEM = new List<SObject>();
    
    /**
    * @description method para obtener QuotelineItems
    * @author Daniel Perez Lopez | 04/09/2020
    * @param String Fields
    * @param Set<ID> lstQuotes
    * @return QuoteLineItem[]
    **/
    public static QuoteLineItem[] quoteliItems(String fields,Set<Id> lstQuotes) {
        final String query = 'SELECT '+fields+' from QuoteLineItem where QuoteId IN: lstQuotes WITH SECURITY_ENFORCED';
        return DataBase.query(String.escapeSingleQuotes(query));
    }
    
    /**
    * @description 
    * @author Daniel Perez Lopez | 12-15-2020 
    * @param fields 
    * @param lstQuotes 
    * @return QuoteLineItem[] 
    **/
    public static QuoteLineItem[] quoteliItemsordr(String fields,Set<Id> lstQuotes) {
        final String query = 'SELECT '+fields+' ,createddate from QuoteLineItem where QuoteId IN: lstQuotes WITH SECURITY_ENFORCED order by createddate desc';
        return DataBase.query(String.escapeSingleQuotes(query));
    }
    /**
    * @description Method para actualizar o insert QuotelineItem
    * @author Daniel Perez Lopez | 04/09/2020
    * @param String Fields
    * @param Set<ID> lstQuotes
    * @return QuoteLineItem[]
    **/
    public static QuoteLineItem[] upsertQuoteLI (QuoteLineItem[] listQuoteLI) {
        upsert listQuoteLI;
        return listQuoteLI;
    }
        /**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param List<MX_SB_VTS_Beneficiario__c> datas
* @param String benFields
* @return R_BENEF
**/
    public static List<QuoteLineItem> getQuoteLineitemBy(String condition, String quotLIfiedls, String value ) {
        switch on condition {
            when 'QuoteId' {
                RET_QLITEM.addAll(dataBase.query(string.escapeSingleQuotes(' SELECT ' +quotLIfiedls + ' FROM QuoteLineItem where QuoteId=:value')));
            }
            when 'OpportunityId' {
                RET_QLITEM.addAll(dataBase.query(string.escapeSingleQuotes(' SELECT ' +quotLIfiedls + ' FROM QuoteLineItem where Quote.OpportunityId=:value')));
            }
        }
        return RET_QLITEM;
    } 
    
}