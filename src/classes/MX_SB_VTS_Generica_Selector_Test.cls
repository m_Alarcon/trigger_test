/**
 * @description       : 
 * @author            : Eduardo Hernandez Cuamatzi
 * @group             : 
 * @last modified on  : 10-08-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   10-02-2020   Eduardo Hernandez Cuamatzi   Initial Version
 * 1.1   23-02-2021   Juan Carlos Benitez Herrera  Se añade busqueda por SRC
**/
@isTest
public class MX_SB_VTS_Generica_Selector_Test {
    /** Campos a recuperar de generica*/
    final static String LSTFIELDS = 'Id, Name, MX_SB_VTS_Description__c, MX_SB_SAC_ProductFam__c';
    
    @TestSetup
    static void makeData() {
        final MX_SB_VTS_Generica__c setting = MX_WB_TestData_cls.GeneraGenerica('OpenPay', 'CP21');
        setting.MX_SB_SAC_ProductFam__c = 'False';
        insert setting;
        Final MX_SB_VTS_Generica__c genrica= new MX_SB_VTS_Generica__c(MX_SB_VTS_Description__c='DC', 
                                             MX_SB_VTS_HEADER__c='Se cortó llamada', MX_SB_VTS_HREF__c='Llamada finalizada', 
                                             MX_SB_VTS_SRC__c='Disconnected', 
                                             MX_SB_VTS_Type__c='OPM', Name='mensaje');
        insert genrica;
    }

    @isTest
    static void findByTypeVal() {
        Test.startTest();
            final Set<String> lstTypes = new Set<String>{'CP21'};
            final List<MX_SB_VTS_Generica__c> lstgene =MX_SB_VTS_Generica_Selector.findByTypeVal(LSTFIELDS, lstTypes);
            System.assertEquals(lstgene.size(), 1, 'valore recuperado');
        Test.stopTest();
    }

    @isTest 
    static void findIvrbyMsjtest() {
        test.startTest();
        Final List<MX_SB_VTS_Generica__c> genMessage= MX_SB_VTS_Generica_Selector.findIvrbyMsj('Disconnected', LSTFIELDS);
        System.assertEquals(genMessage[0].MX_SB_VTS_Description__c,'DC','Se ha encotnrado mensaje IVR fuera de openPay');
        test.stopTest();
    }
}