/**
* @author Daniel Perez Lopez
*
* @description Mock response para usar en autentication helper mock class
**/
@SuppressWarnings('sf:AvoidGlobalModifier')
/*
* Clase mock para llamado a servicios
*/
@isTest
global class MX_SB_PS_IASOMock implements HttpCalloutMock {
    /*
    * method para implementacion ed response
    */
    global HTTPResponse respond(HTTPRequest req) {
        final HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
            System.assertEquals('POST', req.getMethod());
            res.setHeader('tsec', '12345');
            res.setBody('{' +
                    '"authenticationData": [],' +
                    '"authenticationState": "OK",' +
                    '"user": {' +
                    '"id": "ES0182099802136C",' +
                    '"authenticationType": "02",' +
                    '"alias": "000000034B",' +
                    '"role": "O",' +
                    '"otherUserInfo": {' +
                    '"MEDICOS_BARCELONA": "N",' +
                    '"FILE_OPERATION": "N",' +
                    '"ORIGIN_CX": "N",' +
                    '"ORIGIN_UNNIM": "N",' +
                    '"COLLABORATOR": "N"' +
                    '},' +
                    '"personalization": {' +
                    '"channelCode": "04",' +
                    '"personalizationCode": "T"' +
                    '},' +
                    '"person": {' +
                    '"id": "ES0182099802136C"' +
                    '}' +
                    '}' +
                    '}');
            res.setStatusCode(200);
        return res;
    }
}