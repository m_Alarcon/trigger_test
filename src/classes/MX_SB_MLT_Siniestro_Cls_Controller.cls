/**
* -------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_Siniestro_Cls_Controller
* Autor: Daniel Lopez Perez
* Proyecto: Siniestros - BBVA
* Descripción : Clase que almacena los methods usados en la Toma de Reporte de Siniestro
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   	Descripción
* --------------------------------------------------------------------------------
* 1.0         02/12/2019     Daniel Lopez Perez				Creacion
* 1.1         18/12/2019     Angel Nava                     CodeSmell
* 1.2         19/12/2019     Marco Cruz                     Codesmell
* 1.3         24/02/2020     Angel Nava                     cambio workorder
* --------------------------------------------------------------------------------
*/
public with sharing virtual class MX_SB_MLT_Siniestro_Cls_Controller {
    /* 
    * @var INSRT sustituye label insert 
    */
    final static String INSRT = 'insert'; 
    /* 
    * @var UPDT sustituye label update 
    */
    final static String UPDT = 'update';
    /* 
    * @var STRGRUA sustituye label Grua 
    */
    final static String  STRGRUA = 'Grua';
    /* 
    * @var STRASISM sustituye label Asistencia Medica 
    */
    final static String  STRASISM = 'Asistencia Médica';

    /*
    * @description Crea el participante de Siniestro al crearse el Siniestro
    * @param sinId
    * @return Object Participante Siniestro
    */
    
    public virtual boolean createPartSin(String sinId) {
        boolean rsltBusqueda = false;
        try {
            final Siniestro__c objSinActual = [Select Id, MX_SB_MLT_Telefono__c, MX_SB_MLT_PhoneType__c, MX_SB_MLT_Mobilephone__c,MX_SB_MLT_PhoneTypeAdditional__c, MX_SB_MLT_NombreConductor__c,MX_SB_MLT_Product__c,MX_SB_MLT_APaternoConductor__c,MX_SB_MLT_InsuranceCompany__c,MX_SB_MLT_ParticipanteCreado__c, MX_SB_MLT_AMaternoConductor__c from Siniestro__c where id =:sinId];
            if(objSinActual.MX_SB_MLT_ParticipanteCreado__c==false) {
                final ParticipanteSiniestro__c partiSin = new ParticipanteSiniestro__c();
                    
                partiSin.MX_SB_MLT_Phone__c = '5540342107';
                partiSin.MX_SB_MLT_PhoneAd__c = '';
                partiSin.MX_SB_MLT_NombreConductor__c = 'Juan Carlos';
                partiSin.MX_SB_MLT_APaternoConductor__c ='Benitez';
                partiSin.MX_SB_MLT_AMaternoConductor__c = 'Herrera';
                
                if(String.isBlank(objSinActual.MX_SB_MLT_Telefono__c)==false) {
                    partiSin.MX_SB_MLT_Phone__c = objSinActual.MX_SB_MLT_Telefono__c;
                }
				if(String.isBlank(objSinActual.MX_SB_MLT_PhoneTypeAdditional__c)==false) {
                    partiSin.MX_SB_MLT_PhoneAd__c = objSinActual.MX_SB_MLT_PhoneTypeAdditional__c;
                }
                if(String.isBlank(objSinActual.MX_SB_MLT_NombreConductor__c)==false) {
                    partiSin.MX_SB_MLT_NombreConductor__c = objSinActual.MX_SB_MLT_NombreConductor__c;
                }
                if(String.isBlank(objSinActual.MX_SB_MLT_APaternoConductor__c)==false) {
                    partiSin.MX_SB_MLT_APaternoConductor__c = objSinActual.MX_SB_MLT_APaternoConductor__c;
                }
                if(String.isBlank(objSinActual.MX_SB_MLT_AMaternoConductor__c)==false) {
                    partiSin.MX_SB_MLT_AMaternoConductor__c = objSinActual.MX_SB_MLT_AMaternoConductor__c;
                }
			   insert partiSin;
               update new Siniestro__c(id=sinId,MX_SB_MLT_ParticipanteCreado__c=true);
               update new Siniestro__c(id=sinId,Participante_Siniestro__c=partiSin.id);
               rsltBusqueda= true;
            
            }
        return rsltBusqueda;
        } catch(Exception ce) { throw new AuraHandledException ( System.Label.MX_WB_LG_ErrorBack + ce); }
    }
    
      /*
    * @description Crea los servicios de Proveedor Grua, Paramedico, Ambulancia
    * @param String IdSiniestro, String IdProveedor,String idParamedico,String idAmbulancia
    * @return Object Participante de Siniestro
    */
    public static String createServices(String idSiniestro, String idProveedor,String idParamedico,String idAmbulancia) {
        final Siniestro__c sini = [SELECT id,name,Proveedor__c,MX_SB_MLT_Proveedor_Paramedico__c,MX_SB_MLT_RequiereParamedico__c,MX_SB_MLT_ServicioParamedico__c,MX_SB_MLT_LugarAtencionAuto__c, MX_SB_MLT_ProveedorAmbulancia__c,MX_SB_MLT_RequiereAmbulancia__c,MX_SB_MLT_ServicioAmbulancia__c,MX_SB_MLT_RequiereGrua__c,MX_SB_MLT_AjustadorAuto__c,RecordType.DeveloperName,TipoSiniestro__c, MX_SB_MLT_URLLocation__c,MX_SB_MLT_ServicioAjustadorAuto__c,MX_SB_MLT_ServicioGrua__c FROM Siniestro__c WHERE id=:idSiniestro];
        List<WorkOrder> newServices = new List<WorkOrder>();
        List<WorkOrder> updateServices = new List<WorkOrder>();
        final Map<String, Account> mapCuentas = new Map<String, Account>();
        final Account[] cuentas = [SELECT id,Name FROM Account WHERE id in(:idProveedor,:idParamedico,:idAmbulancia)];
        for(Account cuentamapa :cuentas) {
            mapCuentas.put(cuentamapa.id,cuentamapa);
        }
        Final List<WorkOrder> siniServices = [SELECT id,MX_SB_MLT_ServicioAsignado__c,MX_SB_MLT_TipoServicioAsignado__c,MX_SB_MLT_Siniestro__c,createddate FROM WorkOrder WHERE MX_SB_MLT_Siniestro__c =:idSiniestro order by createddate desc];
        
        Map<String,List<WorkOrder>> mapaCase = new Map<String,List<WorkOrder>>();
        mapaCase = servicioParam(newServices,updateServices,siniServices,sini,mapCuentas,idParamedico,idSiniestro);
        newServices=mapaCase.get(INSRT);
        updateServices = mapaCase.get(UPDT);
        mapaCase = new Map<String,List<WorkOrder>>();
        mapaCase = servicioAmb(newServices,updateServices,siniServices,sini,mapCuentas,idParamedico,idSiniestro);
        newServices = mapaCase.get(INSRT);
        updateServices = mapaCase.get(UPDT);
        mapaCase = new Map<String,List<WorkOrder>>();
        mapaCase = servicioProv(newServices,updateServices,siniServices,sini,mapCuentas,idProveedor,idSiniestro);
        newServices = mapaCase.get(INSRT);
        updateServices = mapaCase.get(UPDT);
        if(!newServices.isEmpty()) {
            insert newServices;
        }
        if(!updateServices.isEmpty()) {
            update updateServices;
        }
        return 'Exito test no Create';
    }
     /*
    * @description Crea los servicios de Proveedor Grua, Paramedico, Ambulancia
    * @param String IdSiniestro, String IdProveedor,String idParamedico,String idAmbulancia
    * @return Object Participante de Siniestro
    */
    private static Map<String,List<WorkOrder>> servicioParam(List<WorkOrder> newServices,List<WorkOrder> updateServices,List<WorkOrder> siniServices, Siniestro__c sini,Map<String,Account> mapCuentas,String idParamedico,String idSiniestro) {
        final Map<String,List<WorkOrder>> casosOut = new Map<String,List<WorkOrder>>();
        final List<WorkOrder> casosInR = newServices;
        final List<WorkOrder> casosUpR = updateServices;
        if(idParamedico!=null && sini.RecordType.DeveloperName=='MX_SB_MLT_RamoAuto' 
            && sini.MX_SB_MLT_RequiereParamedico__c==true && sini.MX_SB_MLT_LugarAtencionAuto__c=='Crucero') {
            if(sini.MX_SB_MLT_ServicioParamedico__c==false) {
                final WorkOrder newService = new WorkOrder(RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_Servicios').getRecordTypeId());
                newService.MX_SB_MLT_ProveedorParamedico__c=mapCuentas.get(idParamedico).Name;
                newService.MX_SB_MLT_ServicioAsignado__c=STRASISM;
                newService.MX_SB_MLT_Siniestro__c=idSiniestro;
                newService.MX_SB_MLT_TipoServicioAsignado__c='Moto-Ambulancia'; 
                casosInR.add(newService);
                update new Siniestro__c(id=sini.id ,MX_SB_MLT_ServicioParamedico__c=true);
            } else {
                    for(WorkOrder item :siniServices) {
                        if(item.MX_SB_MLT_ServicioAsignado__c==STRASISM && item.MX_SB_MLT_TipoServicioAsignado__c=='Moto-Ambulancia') {
                           item.MX_SB_MLT_ProveedorParamedico__c=mapCuentas.get(idParamedico).Name;
                            casosUpR.add(item);
                        break;
                        }
                    }
            }
        }
        casosOut.put(INSRT,casosInR);
        casosOut.put(UPDT,casosUpR);
        return casosOut;
    }
     /*
    * @description Crea los servicios de Proveedor Grua, Paramedico, Ambulancia
    * @param String IdSiniestro, String IdProveedor,String idParamedico,String idAmbulancia
    * @return Object Participante de Siniestro
    */
    private static Map<String,List<WorkOrder>> servicioAmb(List<WorkOrder> newServices,List<WorkOrder> updateServices,List<WorkOrder> siniServices, Siniestro__c sini,Map<String,Account> mapCuentas,String idAmbulancia,String idSiniestro) {
        final Map<String,List<WorkOrder>> casosOut = new Map<String,List<WorkOrder>>();
        final List<WorkOrder> casosInR = newServices;
        final List<WorkOrder> casosUpR = updateServices;
         if(idAmbulancia!=null && sini.RecordType.DeveloperName=='MX_SB_MLT_RamoAuto'
            && sini.MX_SB_MLT_RequiereAmbulancia__c==true && sini.MX_SB_MLT_LugarAtencionAuto__c=='Crucero') {
            if(sini.MX_SB_MLT_ServicioAmbulancia__c ==false) {
                final WorkOrder newService = new WorkOrder(RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_Servicios').getRecordTypeId());
                newService.MX_SB_MLT_Proveedor_Ambulancia__c=mapCuentas.get(idAmbulancia).Name;
                newService.MX_SB_MLT_ServicioAsignado__c=STRASISM;
                newService.MX_SB_MLT_Siniestro__c=idSiniestro;
                newService.MX_SB_MLT_TipoServicioAsignado__c='Ambulancia'; 
                casosInR.add(newService);
                update new Siniestro__c(id=sini.id ,MX_SB_MLT_ServicioAmbulancia__c=true);
            } else {
                    for(WorkOrder item :SiniServices) {
                        if(item.MX_SB_MLT_ServicioAsignado__c==STRASISM && item.MX_SB_MLT_TipoServicioAsignado__c=='Ambulancia') {
                           item.MX_SB_MLT_Proveedor_Ambulancia__c=mapCuentas.get(idAmbulancia).Name;
                            casosUpR.add(item);
                        break;
                        }
                    }
            }
        }
        casosOut.put(INSRT,casosInR);
        casosOut.put(UPDT,casosUpR);
        return casosOut;
    }
      /*
    * @description Crea los servicios de Proveedor
    * @param List<Case> newServices,List<Case> updateServices,List<Case> siniServices, Siniestro__c sini,Map<String
    * @return Map casos
    */
    private static Map<String,List<WorkOrder>> servicioProv(List<WorkOrder> newServices,List<WorkOrder> updateServices,List<WorkOrder> siniServices, Siniestro__c sini,Map<String,Account> mapCuentas,String idProveedor,String idSiniestro) {
        final Map<String,List<WorkOrder>> casosOut = new Map<String,List<WorkOrder>>();
        final List<WorkOrder> casosInR = newServices;
        final List<WorkOrder> casosUpR = updateServices;
        casosUpR.add(new workorder(Subject=String.valueOf(mapCuentas.size())));
        casosUpR.clear();
        if( idProveedor!=null && sini.RecordType.DeveloperName=='MX_SB_MLT_RamoAuto'
            && sini.MX_SB_MLT_RequiereGrua__c==true && sini.MX_SB_MLT_LugarAtencionAuto__c=='Crucero') {
            if(sini.MX_SB_MLT_ServicioGrua__c ==false) {
                final WorkOrder newService = new WorkOrder(RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_Servicios').getRecordTypeId());
                newService.MX_SB_MLT_ServicioAsignado__c= STRGRUA;
                newService.MX_SB_MLT_Siniestro__c=idSiniestro;
                newService.MX_SB_MLT_TipoServicioAsignado__c= STRGRUA;
                casosInR.add(newService);    
                update new Siniestro__c(id=sini.id ,MX_SB_MLT_ServicioGrua__c=true);
            } else {
                    for(WorkOrder item :SiniServices) {
                        if(item.MX_SB_MLT_ServicioAsignado__c==STRGRUA && item.MX_SB_MLT_TipoServicioAsignado__c==STRGRUA) {
                            casosUpR.add(item);
                        break;
                        }
                    }
            }
        }
        
        casosOut.put(INSRT,casosInR);
        casosOut.put(UPDT,casosUpR);
        return casosOut;
    }
}