/**
* @FileName          : MX_SB_VTS_wrpServiciosAlianzasPP
* @description       : Wrapper class for use de web service of listCarInsuranceCatalogs
* @Author            : Alexandro Corzo
* @last modified on  : 13-10-2020
* Modifications Log 
* Ver   Date         Author                Modification
* 1.0   13-10-2020   Alexandro Corzo       Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton, sf:ExcessivePublicCoun,sf:LongVariable,sf:LongVariable,sf:ExcessivePublicCount, sf:ShortVariable,sf:ShortClassName') 
public class MX_SB_VTS_wrpServiciosAlianzasPP {
	/**
	 * Invocación de Wrapper
	 */
    public iCatalogItem iCatalogItem {get;set;}
    
    /**
     * @description: Contenido de iCatalogItem, el cual contiene un arreglo de productPlan
     * @Param 
     * @Return
     **/
    public class iCatalogItem {
        /**
         * @description: Lista Planes de Producto
         */
        public productPlan[] productPlan {get;set;}
    }
    
    /**
     * @description: Objeto Wrapper de revisionPlanList para obtener el contenido de la sección
     * @Param 
     * @Return
     **/
    public class productPlan {
        /**
         * @description: Obtiene Objeto: catalogItemBase
         */
        public catalogItemBase catalogItemBase {get;set;}
        /**
         * @description: Obtiene atributo: planReview
         */
        public String planReview {get;set;}
        /**
         * @description: Obtiene atributo: bouquetCode
         */
        public String bouquetCode {get;set;}
    }
    
    /**
     * @description: Objeto Wrapper de catalogItemBase para objeter contenido de la sección
     * @Param 
     * @Return
     **/
    public class catalogItemBase {
        /**
         * @description: Obtiene atributo: id
         */
        public String id {get;set;}
        /**
         * @description: Obtiene atributo: name
         */
        public String name {get;set;}
    }
}