/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_COB_CreaPolizaSch_tst
* Autor Angel Nava
* Proyecto: Cobranza - BBVA Bancomer
* Descripción : Clase test de clase MX_SB_COB_CreaPolizaSch_cls
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Desripción
* --------------------------------------------------------------------------------
* 1.0           01/05/2019     Angel Nava				          Creación
* --------------------------------------------------------------------------------
*/
@isTest
public class MX_SB_COB_CreaPolizaSch_tst {

    @Testsetup
    static void setObjetos() {
        final Bloque__c bloque = new Bloque__c();
        insert bloque;
        final CargaArchivo__c archivo = new CargaArchivo__c(MX_SB_COB_Bloque__c=bloque.name,MX_SB_COB_ContadorConError__c=0,MX_SB_COB_ContadorSinError__c=0,MX_SB_COB_resultado__c='');
        insert archivo;
        final pagosespejo__c pago = new pagosespejo__c(MX_SB_COB_BloqueLupa__c =bloque.id,MX_SB_COB_factura__c='aa23',MX_SB_COB_InsertCreaPago__c=false);
        insert pago;
    }

     @isTest
    static void pruebaSchedule() {
        final String tCotizaEmit = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Contrato Seguros').getRecordTypeId(); 
        String polizaNom = 'aa23';
        test.startTest();
        
        final PagosEspejo__c[] pago = [select id,MX_SB_COB_factura__c,MX_SB_COB_BloqueLupa__c from pagosespejo__c limit 1];
        final Map<String,Map<String,PagosEspejo__c>> mapPolFPE = new Map<String,Map<String,PagosEspejo__c>>();
        final Map<String,pagosEspejo__c> pagoMap = new Map<String,pagosEspejo__c>();
        pagoMap.put(pago[0].MX_SB_COB_factura__c,pago[0]);
        final Map<String,Contract> mapPolNvas = new Map<String,Contract>();
        
         mapPolNvas.put(polizaNom, new Contract(
                        						RecordTypeId = tCotizaEmit,
                            					StartDate=date.today(),AccountId=null,
                            					MX_WB_noPoliza__c=polizaNom,
                            					MX_WB_emailAsegurado__c='a@a.com',
                                                MX_WB_telefonoAsegurado__c= '31231231',
                            					MX_WB_nombreAsegurado__c='Luffy',
                            MX_WB_apellidoPaternoAsegurado__c='Monkey'
                        					)
                                       );
         mapPolFPE.put(polizaNom,pagoMap);
        
        
        
        final MX_SB_COB_CreaPolizaSch_cls objCreaPolizas = new MX_SB_COB_CreaPolizaSch_cls();
		objCreaPolizas.mapPolNvas = mapPolNvas;
        objCreaPolizas.mapPolFactPagoEspejo = mapPolFPE;
        
		final DateTime dtFechaHoraAct = DateTime.now();       	
		final Date dFechaActual = dtFechaHoraAct.date();				
		final Time tmHoraActual = dtFechaHoraAct.Time();
		final Time tmHoraActualEnv = tmHoraActual.addMinutes(1);
        final Integer randomNumber = Integer.valueof(Math.random() * 100);
        final Integer randomNumber2 = Integer.valueof(Math.random() * 100);
        
		String sch = '';
		
		sch = '0 ' + tmHoraActualEnv.minute() + ' ' + tmHoraActualEnv.hour() + ' ' + dFechaActual.day() + ' ' + dFechaActual.month() + ' ?';
        final String sNombreProc = tmHoraActualEnv.minute() + ' : ' + tmHoraActualEnv.hour() + ' : ' + dFechaActual.day() + ' : CreaPolizas : '+randomNumber+' : '+randomNumber2;
        final String jobId =  System.schedule(sNombreProc, sch, objCreaPolizas);
        System.assertNotEquals(null, jobId, 'no se ejecuto el chron job');
        test.stopTest();
    }
    
}