/**
 * @File Name          : MX_SB_VTS_CalculateReCallsUtil2_cls.cls
 * @Description        : 
 * @Author             : Tania Vázquez Hernández 
 * @Group              : 
 * @Last Modified By   : Eduardo Hernández Cuamatzi
 * @Last Modified On   : 3/12/2019 15:04:28
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0     03/12/2019   Tania Vázquez Hernández     Initial Version
**/
public without sharing class MX_SB_VTS_CalculateReCallsUtil2_cls { //NOSONAR
	 
    /** variable que contiene el string de la variable addTime*/
    final static String ADDTIMES= 'addTime';
       /** variable que contiene el string de la variable calcnextact*/
    final static String CALCNEXTACTS = 'calcnextact';
       /** variable que contiene el string de la variable datecalls*/
    final static String DATECALLSS = 'dateCALLS';
      /** variable que contiene el string de la variable limitcall*/
    final static String LITMITCALLS = 'limitCall';
       /** variable que contiene el string de la variable newtimecall*/
    final static String NESTIMECALLS = 'newTimeCall';
       /** variable que contiene el string de la variable nextact*/
    final static String NEXTACTS = 'nextact';
     /** variable auxiliar para el method isNextCalc*/
    final static Integer UNO = 1;
     /** variable auxiliar para el method evalueteNOg4*/
    final static Integer CUATRO = 4;
     /** variable auxiliar para el method evalueteNOg4*/
    final static Integer VEINT4 = 24;
    
    
    
     /**
    * @description calcula accion siguiente
    * @author Eduardo Hernández Cuamatzi | 03/12/2019
    * @map mapIsNextCalc contains all params:
        * @param limitCall hora limite
        * @param newTimeCall nueva hora calculada
        * @param timeCalls hora de marcado
        * @param addTime fecha calculada
        * @param currentAct acción actual
        * @param isG4 pertenece a bulce
        * @return Integer nueva acción
    **/
    public static Integer isNextCalc(map<String,Object> mapIsNextCalc, MX_WB_MotivosNoContacto__c motivoFlow) {
        Integer nextAct = Integer.valueOf(mapIsNextCalc.get('nextAction'));
        if((Boolean)mapIsNextCalc.get('isG4') == false) {
            final Integer calcnextAct = nextAct+1;
            final map<String,Object> mapEvaNOg4 = new  map<String,Object>{CALCNEXTACTS => calcnextAct,DATECALLSS => (Date)mapIsNextCalc.get(DATECALLSS), ADDTIMES => (Date)mapIsNextCalc.get(ADDTIMES),LITMITCALLS => (Time)mapIsNextCalc.get(LITMITCALLS), NESTIMECALLS => (Time)mapIsNextCalc.get(NESTIMECALLS), NEXTACTS => nextAct};
            nextAct = evalueteNOg4(mapEvaNOg4,motivoFlow);
        }
        if((Boolean)mapIsNextCalc.get('isG4') &&  String.isNotEmpty(String.valueOf(motivoFlow)) && Integer.valueOf(motivoFlow.MX_SB_VTS_AccionCuatro__c) > 0) {
            nextAct = Integer.valueOf(String.valueOf(motivoFlow.MX_SB_VTS_AccionCuatro__c))+1;
        }
        if(nextAct == UNO) {
            nextAct =  nextAct+ UNO;
        }        
        return nextAct;
    }
    
    /**
    * @description Evalua si la acción siguiente es 4 
    * @author Eduardo Hernández Cuamatzi | 03/12/2019 
    * @map mapEvaNOg4 contains all params:
        * @param calcnextAct accion siguiente a calcular
        * @param dateCALLS hora de calculo
        * @param addTime hora nueva calculada por tipificacion
        * @param limitCall hora limiti de marcado
        * @param newTimeCall nueva hora de marcado
        * @param nextAct acción actual
        * @return Integer 
    **/
    public static Integer evalueteNOg4(map<String,Object> mapEvaNOg4, MX_WB_MotivosNoContacto__c motivoFlow) {
        Integer reCalnextAct1 = (Integer)mapEvaNOg4.get(NEXTACTS);
         if( (Integer)mapEvaNOg4.get(CALCNEXTACTS) < CUATRO ) {
          	 reCalnextAct1 = evalNOg4p3(mapEvaNOg4,motivoFlow,reCalnextAct1);
        } else {
             reCalnextAct1 = evalNOg4p2(mapEvaNOg4,motivoFlow,reCalnextAct1);
        }
        return reCalnextAct1;
    }
     /**
    * @description decisión sobre evalueteNOg4
    * @author Tania Vázquez Hernández  | 03/12/2019
    * @map mapEvaNOg4 contains all params:
        * @param calcnextAct accion siguiente a calcular
        * @param dateCALLS hora de calculo
        * @param addTime hora nueva calculada por tipificacion
        * @param limitCall hora limiti de marcado
        * @param newTimeCall nueva hora de marcado
        * @param nextAct acción actual
        * @return Integer 
    **/
    public static Integer evalNOg4p2(map<String,Object> mapEvaNOg4, MX_WB_MotivosNoContacto__c motivoFlow, Integer reCalnextAct1) {
        integer reCallnext = reCalnextAct1;
        if((Time)mapEvaNOg4.get(NESTIMECALLS) <(Time)mapEvaNOg4.get(LITMITCALLS) && MX_SB_VTS_CalculateReCallsUtil_cls.evaluteforNextA(String.valueOf((Integer)mapEvaNOg4.get(NEXTACTS)), motivoFlow) < 24) {
                reCallnext = (Integer)mapEvaNOg4.get(NEXTACTS);
            } else {
                reCallnext = 4;
            }
            
            if((Time)mapEvaNOg4.get(NESTIMECALLS) <(Time)mapEvaNOg4.get(LITMITCALLS) && ((Date)mapEvaNOg4.get(DATECALLSS)).daysBetween((Date)mapEvaNOg4.get(ADDTIMES)) > 0 && MX_SB_VTS_CalculateReCallsUtil_cls.evaluteforNextA(String.valueOf((Integer)mapEvaNOg4.get(NEXTACTS)), motivoFlow) < 24) {
                reCallnext = 4;
            }
         return reCallnext;
    }
         /**
    * @description decisión sobre evalueteNOg4
    * @author Tania Vázquez Hernández  | 03/12/2019
    * @map mapEvaNOg4 contains all params:
        * @param calcnextAct accion siguiente a calcular
        * @param dateCALLS hora de calculo
        * @param addTime hora nueva calculada por tipificacion
        * @param limitCall hora limiti de marcado
        * @param newTimeCall nueva hora de marcado
        * @param nextAct acción actual
        * @return Integer 
    **/
    public static Integer evalNOg4p3(map<String,Object> mapEvaNOg4, MX_WB_MotivosNoContacto__c motivoFlow, Integer reCalnextAct1) {
        integer reCallnext = reCalnextAct1;
          if(((Date)mapEvaNOg4.get(DATECALLSS)).daysBetween((Date)mapEvaNOg4.get(ADDTIMES)) > 0 || (Time)mapEvaNOg4.get(NESTIMECALLS) > (Time)mapEvaNOg4.get(LITMITCALLS) && (Integer)mapEvaNOg4.get(NEXTACTS) > 1) {
                reCallnext = (Integer)mapEvaNOg4.get(CALCNEXTACTS);
            }
            if(MX_SB_VTS_CalculateReCallsUtil_cls.evaluteforNextA(String.valueOf((Integer)mapEvaNOg4.get(CALCNEXTACTS)), motivoFlow) < VEINT4) {
                reCallnext = (Integer)mapEvaNOg4.get(CALCNEXTACTS);
            }
         return reCallnext;
    }
}