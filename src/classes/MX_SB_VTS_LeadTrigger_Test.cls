/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 07-17-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   07-16-2020   Eduardo Hernandez Cuamatzi   Initial Version
**/
@isteST
private class MX_SB_VTS_LeadTrigger_Test {
    /*UserNAme*/
    final static String TUSERNAME = 'test Opp Trigger';
    /** Variable de Apoyo: CONCTLLAM */
    final static String CONCTLLAM = 'Contacto';
    
    @TestSetup
    static void makeData() {
        final User tUser = MX_WB_TestData_cls.crearUsuario(TUSERNAME, System.Label.MX_SB_VTS_ProfileAdmin);
        insert tUser;
        final Lead leadOr = new Lead();
        leadOr.FirstName= 'Test trigger';
        leadOr.LastName= 'TriggerTest';
        leadOr.Email= 'TestTrigger_Email@test.com';
        leadOr.Phone= '5512364854';
        leadOr.MobilePhone='5512364854';
        leadOr.LeadSource = 'Inbound';
        leadOr.Status = 'Cotizada';
        leadOr.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
        insert leadOr;
    }

    @isTest
    private static void inserTask() {
        final Id leadRec = [Select Id from Lead where LastName = 'TriggerTest'].Id;
        final Task taskLead = new Task();
        taskLead.Subject = 'Call';
        taskLead.whoId = leadRec;
        insert taskLead;
        final Lead leadRecord = new Lead();
        leadRecord.Id = leadRec;
        leadRecord.MX_SB_VTS_LookActivity__c = taskLead.Id;
        leadRecord.Resultadollamada__c = '';
        leadRecord.MX_SB_VTS_Tipificacion_LV2__c = '';
        leadRecord.MX_SB_VTS_Tipificacion_LV3__c = '';
        leadRecord.MX_SB_VTS_Tipificacion_LV4__c = '';
        leadRecord.MX_SB_VTS_Tipificacion_LV5__c = '';
        leadRecord.MX_SB_VTS_Tipificacion_LV6__c = '';
        leadRecord.MX_SB_VTS_Tipificacion_LV7__c = '';
        update leadRecord;
        System.assertEquals('Call', taskLead.Subject, 'Tarea correcta');
    }

    @isTest
    private static void inserTaskVisit() {
        final Id leadRec = [Select Id from Lead where LastName = 'TriggerTest'].Id;
        final Task taskLead = new Task();
        taskLead.Subject = 'Call';
        taskLead.whoId = leadRec;
        insert taskLead;
        final dwp_kitv__Visit__c recordVisit = new dwp_kitv__Visit__c();
        final Id recType = Schema.SObjectType.dwp_kitv__Visit__c.getRecordTypeInfosByDeveloperName().get('MX_SB_VTS_Tipifi_Ventas_TLMKT').getRecordTypeId();        
		recordVisit.MX_SB_VTS_OrigenCandidato__c = 'Inbound';
		recordVisit.dwp_kitv__lead_id__c = leadRec;
		recordVisit.dwp_kitv__visit_start_date__c = System.now();
        recordVisit.RecordTypeId = recType;
        recordVisit.dwp_kitv__visit_duration_number__c = '15';
        insert recordVisit;
        taskLead.dwp_kitv__visit_id__c = recordVisit.Id;
        update taskLead;
        final Lead leadRecord = new Lead();
        leadRecord.Id = leadRec;
        leadRecord.MX_SB_VTS_LookActivity__c = taskLead.Id;
        leadRecord.Resultadollamada__c = CONCTLLAM;
        leadRecord.MX_SB_VTS_Tipificacion_LV2__c = 'Contacto Efectivo';       
        leadRecord.MX_SB_VTS_Tipificacion_LV3__c = 'No Interesado';
        leadRecord.MX_SB_VTS_Tipificacion_LV4__c = 'No acepta cotización';
        leadRecord.MX_SB_VTS_Tipificacion_LV5__c = 'No Venta (No acepta cotización)';
        leadRecord.MX_SB_VTS_Tipificacion_LV6__c = System.Label.MX_SB_VTS_OtroIdioma;
        update leadRecord;
        System.assertEquals('Contacto', leadRecord.Resultadollamada__c, 'Lead actualizado');
    }

    @isTest
    private static void insertLeadContCE() {
        final Id leadRec = [SELECT Id FROM Lead WHERE LastName = 'TriggerTest'].Id;
        final Lead leadRecord = new Lead();
        leadRecord.Id = leadRec;
        leadRecord.Producto_Interes__c = 'Seguro Estudia';
        leadRecord.Resultadollamada__c = CONCTLLAM;
        leadRecord.MX_SB_VTS_Tipificacion_LV2__c = 'Contacto Efectivo';
        leadRecord.MX_SB_VTS_Tipificacion_LV3__c = '';
        leadRecord.MX_SB_VTS_Tipificacion_LV4__c = '';
        leadRecord.MX_SB_VTS_Tipificacion_LV5__c = '';
        leadRecord.MX_SB_VTS_Tipificacion_LV6__c = '';
        update leadRecord;
        System.assertEquals('Contacto', leadRecord.Resultadollamada__c, 'Lead actualizado');
    }
}