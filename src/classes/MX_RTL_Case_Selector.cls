/**
 * @File Name          : MX_RTL_Case_Selector.cls
 * @Description        :
 * @Author             : Jaime Terrats
 * @Group              :
 * @Last Modified By   : Jaime Terrats
 * @Last Modified On   : 5/26/2020, 3:30:09 AM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/25/2020   Jaime Terrats     Initial Version
 * 1.1    05/25/2020		Roberto Soto Granados		Adding methods for aclaraciones.
 * 1.2    04/06/2020   Selena Yamili Rodriguez          Adding methods for generic query and DML Update
**/
public without sharing class MX_RTL_Case_Selector {
    /** List of records to be returned */
    final static List<SObject> RETURN_CASES = new List<SObject>();
    /** error message for exceptions */
    static String mjsReturn;

    /** constructor */
    private MX_RTL_Case_Selector() { }

    /**
    * @description
    * @author Jaime Terrats | 5/25/2020
    * @param caseFields
    * @param recordId
    * @param enforceSecurity
    * @return List<Case>
    **/
    public static List<Case> getCaseDataById(String caseFields, Set<Id> recordId, String enforceSecurity) {
        switch on enforceSecurity {
            when 'true' {
                RETURN_CASES.addAll(Database.query(String.escapeSingleQuotes('select ' + caseFields + ' from Case where Id in: recordId WITH SECURITY_ENFORCED')));
            }
            when 'false' {
                RETURN_CASES.addAll(Database.query(String.escapeSingleQuotes('select ' + caseFields + ' from Case where Id in: recordId')));
            }
        }

        return RETURN_CASES;
    }

    /*Llamada a casos para aclaraciones abiertas con el OwnerID*/
    public static List<Case> casosEnAclaraciones(Set<Id> ownerIds, Set<Id> dirIds) {
        return [SELECT Id, MX_SB_SAC_Folio__c, AccountId, Account.Name, MX_SB_MLT_CreateDateCC__c, Description, (SELECT BPyP_Producto__c, BPyP_Tarjeta__c FROM Aclaraciones__r) FROM Case WHERE (OwnerId IN: ownerIds OR OwnerId IN: dirIds) AND RecordType.DeveloperName = 'MX_EU_Case_Apoyo_General' AND Status = 'Nuevo'];
    }

    /*Llamada a casos por folios*/
    public static List<Case> casosPorFolios(Set<String> folios) {
        return [SELECT Id, MX_SB_SAC_Folio__c, AccountId FROM Case WHERE RecordType.DeveloperName = 'MX_EU_Case_Apoyo_General' AND MX_SB_SAC_Folio__c IN: folios];
    }

        /*Llamada a casos para actualizar*/
    public static List<Case> casosParaVisitas(Set<Id> caseIds) {
        return [SELECT Id, AccountId, MX_SB_SAC_Folio__c, Description, Status, (SELECT BPyP_Producto__c, BPyP_Tarjeta__c FROM Aclaraciones__r) FROM Case WHERE Id IN: caseIds];
    }
    /*Actualiza casos*/
    public static void actualizaCasos(List<Case> casos) {
        update casos;
    }
    
    /**
    *@Description   Method for Case query by AccountIDs and RecordType
    *@author 		Edmundo Zacarias
    *@Date 			2020-08-06
    *@param 		String caseFields = fields for query
    *@param 		String accIds = Set<Id> Accounts
	*@param			String strRecordType = recordType.DeveloperName
    **/
    public static List<Case> getCasesByAccountAndRtype(String caseFields, Set<Id> accIds, String strRecordType) {
        return Database.query(String.escapeSingleQuotes('SELECT' + ' ' + caseFields + ' FROM Case WHERE AccountId IN: accIds AND RecordType.DeveloperName =:strRecordType'));
    }
}