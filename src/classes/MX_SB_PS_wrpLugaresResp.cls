/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpLugaresResp
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
 @SuppressWarnings('sf:ShortClassName, sf:LongVariable, sf:ShortVariable')
public class  MX_SB_PS_wrpLugaresResp {
    /*Public property for wrapper*/
	public data[] data {get;set;}
	  /*Public constructor for wrapper*/
    public  MX_SB_PS_wrpLugaresResp() {
        data = new data[] {};
		data.add(new data());
    }
	  /*Public subclass for wrapper*/
    public class data {
	  /*Public property for wrapper*/
		public country country {get;set;}
	  /*Public property for wrapper*/	
		public String name {get;set;}
	  /*Public property for wrapper*/	
		 public string id {get;set;} 
	  /*Public property for wrapper*/	
		public state state {get;set;}
	  /*Public property for wrapper*/	
		public geographicPlaceTypes[] geographicPlaceTypes {get;set;}
	  /*Public property for wrapper*/	
		public geolocation geolocation {get;set;}
	  /*Public subconstructor for wrapper*/	
        public data() {
            this.country = new country();
			this.name = '';
			this.id = '';
            this.state = new state();
            this.geographicPlaceTypes = new geographicPlaceTypes[] {};
            this.geographicPlaceTypes.add(new geographicPlaceTypes());
			this.geolocation = new geolocation();    
        }
	}
	  /*Public class for wrapper*/
	public class country {
	  /*Public property for wrapper*/
		public String name {get;set;}
	  /*Public property for wrapper*/	
		 public string id {get;set;} 
      /*public constructor subclass*/
		public country() {
		 this.name = '';
		 this.id = '';
		}		
	}
	  /*Public subclass for wrapper*/
	public class state {
	  /*Public property for wrapper*/
		public String name {get;set;}
	  /*Public property for wrapper*/	
		 public string id {get;set;} 
	  /*public constructor subclass*/
		public state() {
		 this.name = '';
		 this.id = '';
		}		
	}
	  /*Public subclass for wrapper*/
	public class geographicPlaceTypes {
	  /*Public property for wrapper*/
		public String name {get;set;}
	  /*Public property for wrapper*/	
		 public string id {get;set;} 
	  /*Public property for wrapper*/	
		public String value {get;set;}
		 /*public constructor subclass*/
		public geographicPlaceTypes() {
		 this.name = '';
		 this.id = '';
		 this.value = '';
		}	
	}
	  /*Public subclass for wrapper*/
	public class geolocation {
	  /*Public property for wrapper*/
		public String latitude {get;set;}
	  /*Public property for wrapper*/	
		public String longitude {get;set;}
      /*public constructor subclass*/
		public geolocation() {
		 this.latitude = '';
		 this.longitude = '';
		}			
	}
	
	}