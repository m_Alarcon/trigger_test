/**
 * @File Name          : MX_RTL_Case_Selector_Test.cls
 * @Description        :
 * @Author             : Jaime Terrats
 * @Group              :
 * @Last Modified By   : Jaime Terrats
 * @Last Modified On   : 5/26/2020, 11:30:37 AM
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    5/25/2020   Jaime Terrats     Initial Version
 * 1.1      25/05/2020      Roberto Soto        Se agregan methods para aclaraciones
**/
@isTest
private with sharing class MX_RTL_Case_Selector_Test {
    /** test subject for case */
    final static String NAME = 'prueba_bulk';
    /** error message for asserts */
    final static String ERR_MSG = 'No hacen match, prueba fallida';
    /** error message for exceptions */
    static String mjsReturn;
    /** current user id */
    final static String USR_ID = UserInfo.getUserId();
    /*Caso de pruebas*/
    private static Case testCase = new Case();
    /*cuenta de pruebas*/
    private static Account testAcc = new Account();
    /*Usuario de pruebas*/
    private static User testUser = new User();
    /*String para folios*/
    private static String testCode = '1234';
    /*Variable which store fields to query */
    static String queryFields;
    /**
     * @description
     * @author Jaime Terrats | 5/25/2020
     * @return void
     **/
    @TestSetup
    static void makeData() {
            final User usrAdm = MX_WB_TestData_cls.crearUsuario(NAME, System.Label.MX_SB_VTS_ProfileAdmin);
        testUser = UtilitysDataTest_tst.crearUsuario('testUser', 'BPyP Estandar', 'BPYP BANQUERO BANCA PERISUR');
        testUser.Title = 'Privado';
        insert testUser;
            System.runAs(usrAdm) {
                testAcc.LastName = 'testAcc';
                testAcc.FirstName = 'testAcc';
                testAcc.OwnerId = testUser.Id;
                testAcc.No_de_cliente__c = testCode;
                insert testAcc;
                testCase.OwnerId = testUser.Id;
                testCase.Status = 'Nuevo';
                testCase.MX_SB_SAC_Folio__c = testCode;
                testCase.Description = 'Description';
                testCase.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'MX_EU_Case_Apoyo_General'].Id;
                testCase.AccountId = testAcc.Id;
                insert testCase;
                final String rtId = Schema.SObjectType.case.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_SAC_Generico).getRecordTypeId();

                final List<Case> casesToIn = new List<Case>();
                for(Integer ite = 0; ite < 10; ite++) {
                    final Case nCase = new Case();
                    nCase.Subject = NAME + ite;
                    nCase.MX_SB_SAC_Nombre__c = NAME;
                    nCase.RecordTypeId = rtId;
                    nCase.MX_SB_SAC_ApellidoPaternoCliente__c = NAME;
                    nCase.MX_SB_SAC_EsCliente__c = true;
                    nCase.MX_SB_SAC_ApellidoMaternoCliente__c = NAME;
                    nCase.MX_SB_SAC_SubOrigenCaso__c = 'Inbound';
                    nCase.Origin = 'Teléfono';
                    nCase.Reason = 'Consultas';
                    nCase.MX_SB_SAC_TipoContacto__c = 'Asegurado';
                    casesToIn.add(nCase);
                }
                Database.insert(casesToIn);
            }    
    }

    /**
     * @description
     * @author Jaime Terrats | 5/25/2020
     * @return void
     **/
    @isTest
    private static void testGetCaseDataWithSecurityT() {
        final List<Case> caseL = [Select Id from Case where Subject like: '%' + NAME + '%'];
        final Set<Id> ids = new Set<Id>();
        for(Case tCase : caseL) {
            ids.add(tCase.Id);
        }
        final List<Case> testListC = MX_RTL_Case_Selector.getCaseDataById('Subject', ids, 'true');
        System.assertEquals(testListC.size(), 10, ERR_MSG);
    }

    /**
     * @description
     * @author Jaime Terrats | 5/25/2020
     * @return void
     **/
    @isTest
    private static void testGetCaseDataWithoutSecurityT() {
        final List<Case> listCaseTs = [Select Id from Case where Subject =: NAME + '9'];
        final Set<Id> ids = new Set<Id>();
        for(Case tCase : listCaseTs) {
            ids.add(tCase.Id);
        }
        final List<Case> testL = MX_RTL_Case_Selector.getCaseDataById('Subject', ids, 'false');
        System.assertEquals(testL.size(), 1, ERR_MSG);
    }

    /*Ejecuta la acción para cubrir clase*/
    static testMethod void casosParaVisitasTest() {
        List<Case> aclaraciones = new List<Case>();
        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        testCase = [SELECT Id, MX_SB_SAC_Folio__c FROM Case WHERE MX_SB_SAC_Folio__c =: testCode LIMIT 1];
        System.runAs(testUser) {
            Test.startTest();
                aclaraciones = MX_RTL_Case_Selector.casosParaVisitas(new Set<Id>{testCase.Id});
            Test.stopTest();
        }
        System.assert(!aclaraciones.isEmpty(), 'Casos encontrados');
    }

    /*Ejecuta la acción para cubrir clase*/
    static testMethod void casosEnAclaracionesTest() {
        List<Case> aclaraciones = new List<Case>();
        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        System.runAs(testUser) {
            Test.startTest();
                aclaraciones = MX_RTL_Case_Selector.casosEnAclaraciones(new Set<Id>{testUser.Id}, new Set<Id>{testUser.Id});
            Test.stopTest();
        }
        System.assert(!aclaraciones.isEmpty(), 'Casos encontrados');
    }

    /*Ejecuta la acción para cubrir clase*/
    static testMethod void casosPorFoliosTest() {
        List<Case> aclaraciones = new List<Case>();
        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        testCase = [SELECT Id, MX_SB_SAC_Folio__c FROM Case WHERE MX_SB_SAC_Folio__c =: testCode LIMIT 1];
        System.runAs(testUser) {
            Test.startTest();
                aclaraciones = MX_RTL_Case_Selector.casosPorFolios(new Set<String>{testCase.MX_SB_SAC_Folio__c});
            Test.stopTest();
        }
        System.assert(!aclaraciones.isEmpty(), 'Casos encontrados');
    }

    /*Ejecuta la acción para cubrir clase*/
    static testMethod void actualizaCasosTest() {
        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        testCase = [SELECT Id, MX_SB_SAC_Folio__c FROM Case WHERE MX_SB_SAC_Folio__c =: testCode LIMIT 1];
        System.runAs(testUser) {
            Test.startTest();
                testCase.Description = 'New description';
                MX_RTL_Case_Selector.actualizaCasos(new List<Case>{testCase});
            Test.stopTest();
        }
        System.assert(!String.isBlank(testCase.Description), 'Caso actualizado');
    }

    
    /**
    *@Description   Test Method for getCasesByAccountAndRtype
    *@author        Edmundo Zacarias
    *@Date          2020-08-06
    **/
    @isTest
    private static void testGetCasesByAccountAndRtype() {
        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        List<Case> caseResults;
        System.runAs(testUser) {
            final List<Account> listAcc = [SELECT Id FROM Account WHERE No_de_cliente__c =: testCode];
            final Set<Id> accIds = new Set<Id>();
            queryFields = 'Id, OwnerId, AccountId';
            for(Account acc : listAcc) {
                accIds.add(acc.Id);
            }
            Test.startTest();
            caseResults = MX_RTL_Case_Selector.getCasesByAccountAndRtype(queryFields, accIds, 'MX_EU_Case_Apoyo_General');
            Test.stopTest();
        }
        System.assertEquals(1, caseResults.size(), ERR_MSG);
    }
}