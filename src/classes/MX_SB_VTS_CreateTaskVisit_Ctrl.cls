/**
* @File Name          : MX_SB_VTS_CreateTaskVisit_Ctrl.cls
* @Description        :
* @Author             : Marco Antonio Cruz Barboza
* @Group              :
* @Last Modified By   : Marco Antonio Cruz Barboza
* @Last Modified On   : 5/26/2020, 05:22:22 AM
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    07/09/2020   Marco Antonio Cruz Barboza    Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_SB_VTS_CreateTaskVisit_Ctrl {
	
    /**
    * @description: Create a Visit and Task when a flow execute this class
    * @author Marco Cruz | 07/09/2020
    * @param List<Lead> getLead
    **/
    @InvocableMethod(label='Create a Task and Visit' description='Create a task and a visit')
    public static void createVisitTask(List<Lead> getLead) {
        Final Id recordTypeVis = Schema.SObjectType.dwp_kitv__Visit__c.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Tipifi_Ventas_TLMKT).getRecordTypeId();
        Final Id taskRecordT = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('MX_SB_VTA_Ventas_Asistida').getRecordTypeId();
        Final List<Task> inseTask = new List<Task>();
        Final List<dwp_kitv__Visit__c> listVisit = new List<dwp_kitv__Visit__c>();
        for(Lead iterLead : getLead) {
            Final datetime myDateTime = datetime.now();
            
            Final task createTask = new Task();
            createTask.WhoId = iterLead.Id;
            createTask.Subject = 'Llamada Realizada ';
            createTask.RecordTypeId = taskRecordT;
            createTask.MX_SB_VTS_TipificacionNivel6__c = String.valueOf(iterLead.MX_SB_VTS_Tipificacion_LV6__c);
            inseTask.add(createTask);
            
            final dwp_kitv__Visit__c visitCreate = new dwp_kitv__Visit__c();
            visitCreate.dwp_kitv__lead_id__c = iterLead.Id;
            visitCreate.RecordTypeId = recordTypeVis;
            visitCreate.dwp_kitv__visit_duration_number__c = '15';
            visitCreate.dwp_kitv__visit_start_date__c = myDateTime;
            visitCreate.Name = 'Llamada Realizada ';
            visitCreate.MX_SB_VTS_EtapaOportunidad__c = iterLead.Status;
            visitCreate.MX_SB_VTS_Producto__c = iterLead.Producto_Interes__c;
            visitCreate.MX_SB_VTS_OrigenCandidato__c = iterLead.LeadSource;
            visitCreate.MX_SB_VTS_FechaCreacion__c = myDateTime;
            visitCreate.MX_SB_VTS_Contacto__c = String.valueOf(iterLead.Resultadollamada__c);
            visitCreate.MX_SB_VTS_ContactoEfectivo__c = String.valueOf(iterLead.MX_SB_VTS_Tipificacion_LV2__c);
            visitCreate.MX_SB_VTS_Interesado__c = String.valueOf(iterLead.MX_SB_VTS_Tipificacion_LV3__c);
            visitCreate.MX_SB_VTS_AceptaCotizacionContratacion__c = String.valueOf(iterLead.MX_SB_VTS_Tipificacion_LV4__c);
            visitCreate.MX_SB_VTS_Venta__c = String.valueOf(iterLead.MX_SB_VTS_Tipificacion_LV5__c);
            visitCreate.MX_SB_VTS_TipificacionFinal__c =  String.valueOf(iterLead.MX_SB_VTS_Tipificacion_LV6__c);
            visitCreate.MX_SB_VTS_Cupon__c = iterLead.MX_SB_VTS_CodCampaFace__c;
            visitCreate.MX_SB_VTS_GradoAvance__c =  String.valueOf(iterLead.MX_SB_VTS_Grado_de_Avance__c);
            visitCreate.MX_SB_VTS_MotivoAvance__c = iterLead.MX_SB_VTS_Motivo_de_mayor_avance__c;

            listVisit.add(visitCreate);
        }
        MX_RTL_Task_Service.insertTask(inseTask);
        Dwp_kitv_Visit_Service.insertVisit(listVisit);
    }
    
}