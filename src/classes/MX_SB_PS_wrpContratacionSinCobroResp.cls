/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpContratacionSinCobroResp
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación de la wrapper.*/
 @SuppressWarnings('sf:ShortClassName, sf:ShortVariable')
public  class MX_SB_PS_wrpContratacionSinCobroResp {
      /*Public property for wrapper*/
	public data data {get;set;}
	/*public constructor */
	public MX_SB_PS_wrpContratacionSinCobroResp () {
      this.data = new Data();  
    }
	  /*Public subclass for wrapper*/
	public class data {
	      /*Public property for wrapper*/
		public validityPeriod validityPeriod {get;set;}
		  /*Public property for wrapper*/
		public terms[] terms {get;set;}
		  /*Public property for wrapper*/
		public String policyNumber {get;set;}
          /*Public property for wrapper*/ 		
		public holder holder {get;set;}
		/*public constructor subclass*/
		public data() {
            this.validityPeriod = new validityPeriod();
            this.policyNumber='';
            this.holder = new holder();
            this.terms =  new terms[] {};
            this.terms.add(new terms());    
        }
	}
	  /*Public subclass for wrapper*/
	public class validityPeriod {
	      /*Public property for wrapper*/
		public String endDate {get;set;}	
		  /*Public property for wrapper*/
		public String startDate {get;set;}
        /*public constructor subclass*/
        public validityPeriod() {
            this.endDate='';
            this.startDate='';
        } 		
	}
	  /*Public subclass for wrapper*/
	public class terms {
	  /*Public property for wrapper*/
		public document document {get;set;}
		  /*Public property for wrapper*/
		public acceptanceMethod acceptanceMethod {get;set;}
		  /*Public property for wrapper*/
		public termType termType {get;set;}
		/*public constructor subclass*/
		public terms() {
            this.document = new document();
            this.acceptanceMethod = new acceptanceMethod();
            this.termType = new termType();
        }
	}
	  /*Public subclass for wrapper*/
	public class document {
	      /*Public property for wrapper*/
		 public string id {get;set;} 	
		/*public constructor subclass*/
		public document() {
            this.id='';
        }
	}
	  /*Public subclass for wrapper*/
	public class acceptanceMethod {
	     /*Public property for wrapper*/
		 public string id {get;set;} 
		/*Public constructor subclass*/
		public acceptanceMethod() {
            this.id='';
        }
	}
	  /*Public subclass for wrapper*/
	public class termType {
	  /*Public property for wrapper*/
		 public string id {get;set;} 	
	  /*Public subclass constructor*/
		public termType() {
            this.id='';
        }
	}
	  /*Public subclass for wrapper*/
	public class holder {
	  /*Public property for wrapper*/
		public String firstName {get;set;}
      /*Public property for wrapper*/		
		public String lastName {get;set;}
      /*Public property for wrapper*/		
		public String secondLastName {get;set;}
	  /*Public property for wrapper*/	
		public contactDetails[] contactDetails {get;set;}
	  /*public constructor subclass*/
	  public holder() {
            this.firstName='';
            this.lastName='';
            this.secondLastName='';
            this.contactDetails  = new contactDetails[] {}; 
            this.contactDetails.add(new contactDetails());  
        }
	}
	/*Public subclass for wrapper*/
	public class contactDetails {
	  /*Public property for wrapper*/
		public String contact {get;set;}	
      /*Public property for wrapper*/		
		public contactType contactType {get;set;}
	  /*public constructor subclass*/
	  public contactDetails() {
            this.contact='';
            this.contactType = new ContactType();
        } 
	}
	  /*Public subclass for wrapper*/
	public class contactType {
	  /*Public property for wrapper*/
		 public string id {get;set;} 	
	  /*public constructor subclass*/
	  public contactType() {
            this.Id ='';
        }
	}

}