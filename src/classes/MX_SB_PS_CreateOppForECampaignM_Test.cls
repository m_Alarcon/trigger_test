/**
* @File Name          : MX_SB_PS_CreateOppForECampaignM_Test.cls
* @Description        : Test class for MX_SB_PS_CreateOppForECampaignM_Service
* @Author             : Gerardo Mendoza Aguilar
* @Group              :
* @Last Modified By   :Gerardo Mendoza Aguilar
* @Last Modified On   : 28/10/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      28/10/2020           Gerardo Mendoza Aguilar          Initial Version
**/
@isTest
public class MX_SB_PS_CreateOppForECampaignM_Test {
    /** Nombre */
    final static String NAME = 'Mariana';
    /** Apellido Paterno */
    final static STRING LNAME= 'Esteves';
/**
 * @description setup data
 * @author Gerardo Mendoza Aguilar | 28/10/2020
 * @return void
 **/
@TestSetup
private static void testSetUpData() {
MX_SB_PS_OpportunityTrigger_test.makeData();
    Final String conRType =Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('IndustriesIndividual').getRecordTypeId();
    Final String campRType =Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('BPyP_Campaign').getRecordTypeId();
    Final Contact contData = new Contact(RecordTypeId = conRType, FirstName=NAME, LastName=LNAME);
        insert contData;
    Final Campaign campData = new Campaign(Name = 'Campaign Test', RecordTypeId=campRType, EndDate = Date.today());
        insert campData;
    Final CampaignMember campMData = new CampaignMember(
        CampaignId = campData.Id, ContactId = contData.Id, Status='Respondida'
        , MX_SB_PS_RefenciaCliente__c = '01REFTEST', MX_SB_PS_productoOfertado__c = 'RSPH'
        );
        insert campMData;
    
}
/**
 * @description Consulta de oportunidad creada
 * @author Gerardo Mendoza Aguilar | 28/10/2020
 * @return void
 **/
    @isTest
    public static void createOppFromCampMbrsTest() {
        Test.startTest();
        Final List<Opportunity> oppList = [Select Id From Opportunity where Name = '01REFTEST'];
        System.assert(!oppList.isEmpty(), 'Oportunidad creada');
        Test.stopTest();
    }
}