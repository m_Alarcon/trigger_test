/**
 * @description       : Clase selector que busca valores sobre custom metadata
 * @author            : Diego Olvera
 * @group             : BBVA
 * @last modified on  : 02-22-2021
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   11-20-2020   Diego Olvera   Initial Version
 * 1.1   18-02-2021   Diego Olvera   Se agrega función que busca por nombre de metadata y recupera listado
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_RTL_CustomMeta_Selector {
    /** list of possible filters */
    final static String SELECT_STRING = 'SELECT ';
    /**
    * @description Función que recupera listado de estados de la republica por medio de valor ingresado
    * @author Diego Olvera | 11-20-2020 
    * @param searchValue: valor recuperado de clase service (valor a buscar)
    * @return Listado de estados recuperados de clase selector
    **/
    public static List<MX_SB_VTS_StatesMex__mdt> getDirections(String searchValue) {
        final String keyVal = '%' + String.escapeSingleQuotes(searchValue) + '%';
        return [SELECT Id, MX_SB_VTS_States__c, MX_SB_VTS_Abreviacion__c FROM MX_SB_VTS_StatesMex__mdt WHERE MX_SB_VTS_States__c LIKE:keyVal];
    }
     /**
    * @description Función que recupera listado de estados de la republica
    * @author Diego Olvera | 18-02-2021 
    * @param  qryFields, mdName: valores recuperados de clase service (campos y nombre de metadata a buscar)
    * @return Listado de estados recuperados de clase selector
    **/
    public static List<MX_SB_VTS_StatesMex__mdt> getStates(String qryFields, String mdName) {
       final String addStrQr = SELECT_STRING + qryFields + ' FROM ' + mdName;
       return (List<MX_SB_VTS_StatesMex__mdt>)DataBase.query(String.escapeSingleQuotes(addStrQr)); 
    }
}