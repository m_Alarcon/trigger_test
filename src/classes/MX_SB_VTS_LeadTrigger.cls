/**
 * @File Name          : MX_SB_VTS_LeadTrigger.cls
 * @Description        :
 * @Author             : Diego Olvera
 * @Group              :
 * @Last Modified By   : Diego Olvera
 * @Last Modified On   : 09-09-2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      9/8/2019 10:59:50           Aixef                       Initial Version
 * 1.1      28/10/2019                  Francisco Javier            Se agrega funcion para validar el role del usuario.
 * 1.2		12/05/2020					Alexandro Corzo				Se agrega función para actualizar el valor del campo "Calificación de Solicitud"
 * 																	dependiendo del horario en que se registro el Lead. El cual puede ser Hot Lead o Cold Lead.
 * 1.3      17/08/2020                  Alexandro Corzo             Se realizan ajustes de funcionalidad para la función: updateCalSolicitud ya que 
                                                                    se debe validar el record type para su ejecución.
**/
public without sharing class MX_SB_VTS_LeadTrigger { //NOSONAR
    /** Valida contador de llamadas*/
    final static Integer VALIDCALLS = 1;
    /**
     * evaluateLeads valida los leads de Call me back
     */
    public static void evaluateLeads(List<Lead> newEntryLead) {
        final List<String> lstListaNegra = new List<String>();
        for (Lead leadItem : newEntryLead) {
            if( leadItem.LeadSource == System.Label.MX_SB_VTS_OrigenCallMeBack || leadItem.LeadSource == System.Label.MX_SB_VTS_OrigenFacebook) {
                evaluateWebtoLead(leadItem);
                if(String.isBlank(leadItem.MX_SB_VTS_CampaFaceName__c) || String.isBlank(leadItem.MX_SB_VTS_CodCampaFace__c)) {
                    leadItem.MX_SB_VTS_CodCampaFace__c = 'Sin cupón';
                }
                if(String.isNotBlank(leadItem.Email)) {
                    lstListaNegra.add(leadItem.Email);
                }
                final String validPhone = evaluatePhone(leadItem);
                if (String.isNotBlank(validPhone)) {
                    lstListaNegra.add(validPhone);
                }
            }
        }
        if(lstListaNegra.isEmpty() == false) {
            evaluateBlackList(newEntryLead, lstListaNegra);
        }
    }

    /**
     * evaluateWebtoLead asigna la fecha de contacto
     */
    public static void evaluateWebtoLead(Lead leadItem) {
        if (String.isEmpty(leadItem.Priority__c) && String.isBlank(String.valueOf(leadItem.Hora_contacto__c))) {
            leadItem.Priority__c = '0';
        }
        if(String.isNotBlank(String.valueOf(leadItem.Hora_contacto__c))) {
            leadItem.Hora_contacto__c = DateTime.now();
        }
        evaluetePhoneNumber(leadItem);
    }

    /**
     * evaluateBlackList asigna el número de contacto estandar y personalizado
     */
    public static String evaluatePhone(Lead leadItem) {
        String validPhone = '';
        if(String.isNotEmpty(leadItem.TelefonoCelular__c)) {
            leadItem.MobilePhone = leadItem.TelefonoCelular__c;
            validPhone = leadItem.TelefonoCelular__c;
        } else if (String.isNotBlank(leadItem.MobilePhone)) {
            leadItem.TelefonoCelular__c = leadItem.MobilePhone;
            validPhone = leadItem.MobilePhone;
        }
        return validPhone;
    }

    /**
     * evaluateBlackList validar número teléfonico en lista negra
     */
    public static void evaluateBlackList(List<Lead> newEntryLead, List<String> lstListaNegra) {
        final Map<String,String> mapListaNegra = Utilities.verificarListaNegra(lstListaNegra);
        for (Lead leadItem : newEntryLead) {
            if(mapListaNegra.containsKey(leadItem.Email) || mapListaNegra.containsKey(leadItem.TelefonoCelular__c)) {
                leadItem.addError(System.Label.MX_SB_VTS_ErrorListaNegra);
            }
        }
    }

    /**
     * evaluetePhoneNumber validar número teléfonico
     */
    public static void evaluetePhoneNumber(Lead leadItem) {
        String sTelefono = '';
        if(String.isNotEmpty(leadItem.TelefonoCelular__c)) {
            sTelefono = leadItem.TelefonoCelular__c;
        } else if(String.isNotEmpty(leadItem.MobilePhone)) {
            sTelefono = leadItem.MobilePhone;
        }
        if (String.isNotBlank(sTelefono) && (sTelefono.isNumeric() == false || (sTelefono.length() == 10) == false)) {
            leadItem.addError(System.Label.MX_SB_VTS_ErrorValidarLead);
        }
    }
    /**
    * resta contador de toques
    */
    public static void substractContactTouch(List<Lead> leadItem, Map<Id, Lead> oldMap) {
        final Id recordType = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
        for(Lead lst : leadItem) {

            final Lead oldLead = oldMap.get(lst.Id);
            final Integer contador = MX_SB_VTS_LeadAutoAssigner.checkNullVals(lst.MX_SB_VTS_ContadorRemarcado__c);
            if (lst.RecordTypeId.equals(recordType) && contador > 0 && lst.Resultadollamada__c == 'No Contacto' && lst.MX_SB_VTS_Tipificacion_LV2__c == 'No Contacto' && String.isBlank(oldLead.MX_SB_VTS_Tipificacion_LV2__c)) {
                lst.MX_SB_VTS_ContadorRemarcado__c = contador-1;
            }
        }
    }

    /**
    * aumenta contador cuando es contacto efectivo
    */
    public static void substractEffectiveContact (List<Lead> leadItem,  Map<Id, Lead> oldMap) {
        final Id recordType = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
        for(Lead lst: leadItem) {
            final Lead oldLead = oldMap.get(lst.Id);
            final Integer contador1 = MX_SB_VTS_LeadAutoAssigner.checkNullVals(lst.MX_SB_VTS_ContadorRemarcado__c);
            final Integer contador2 = MX_SB_VTS_LeadAutoAssigner.checkNullVals(lst.MX_SB_VTS_Llamadas_Efectivas__c);
            final Decimal contador3 = MX_SB_VTS_LeadAutoAssigner.checkNullVals(lst.MX_SB_VTS_ContadorLlamadasTotales__c);
            if(lst.RecordTypeId.equals(recordType) && lst.MX_SB_VTS_Tipificacion_LV2__c == 'Contacto Efectivo' && String.isBlank(oldLead.MX_SB_VTS_Tipificacion_LV2__c)) {
                lst.MX_SB_VTS_Llamadas_Efectivas__c = contador2+1;
            }
            if(lst.RecordTypeId.equals(recordType) && lst.Producto_Interes__c == 'Seguro Estudia' && lst.MX_SB_VTS_Tipificacion_LV2__c == 'Contacto Efectivo' && String.isBlank(oldLead.MX_SB_VTS_Tipificacion_LV2__c)) {
                lst.MX_SB_VTS_Llamadas_Efectivas__c = contador2 + 1;
                lst.MX_SB_VTS_ContadorRemarcado__c = contador1 + 1;
                lst.MX_SB_VTS_ContadorLlamadasTotales__c = contador3+1;
            }
        }
    }

     /**
    * aumenta contador cuando se tipifica algun motivo de no contacto, interes en seguro estudia
    */
    public static void addsContactCounter (List<Lead> leadSegEs,  Map<Id, Lead> oldMapSeg) {
        final Id recordType = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
        for(Lead lstSeg: leadSegEs) {
            final Lead oldLeadSeg = oldMapSeg.get(lstSeg.Id);
            final Decimal contadorSeg = lstSeg.MX_SB_VTS_ContadorLlamadasTotales__c;
            if(lstSeg.RecordTypeId.equals(recordType) && lstSeg.Producto_Interes__c == 'Seguro Estudia' && lstSeg.MX_SB_VTS_Tipificacion_LV2__c != 'Contacto efectivo' && String.isNotBlank(lstSeg.MX_SB_VTS_Tipificacion_LV6__c) && String.isBlank(oldLeadSeg.MX_SB_VTS_Tipificacion_LV6__c)) {
                lstSeg.MX_SB_VTS_ContadorLlamadasTotales__c = contadorSeg+1;
            }
        }
    }
    /**
    * aumenta cuando es contcto no efectivo
    **/
    public static void substractNoEffectiveContact(List<Lead> leadItem, Map<Id, Lead> oldMap) {
        final Id recordType = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
        for(Lead lst: leadItem) {
            final Lead oldLead = oldMap.get(lst.Id);
            final Integer contador3 = MX_SB_VTS_LeadAutoAssigner.checkNullVals(lst.MX_SB_VTS_Llamadas_No_Efectivas__c);
            if(lst.RecordTypeId.equals(recordType) && lst.MX_SB_VTS_Tipificacion_LV2__c == 'No Contacto' && String.isBlank(oldLead.MX_SB_VTS_Tipificacion_LV2__c)) {
                lst.MX_SB_VTS_Llamadas_No_Efectivas__c = contador3+1;
            }
        }
    }
    /**
	* @method: validateRole
    * @description: Valida que el role del usuario sea valido para editar el registro.
    * @params: List<Lead> leads
	*/
    public static void validateRole(List<Lead> leads) {
        if(String.isNotBlank(UserInfo.getUserRoleId())) {
            final Id recordTypeLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
            final List<UserRole> lstRole = [Select Id, Name from UserRole where id =: UserInfo.getUserRoleId()];
            final String role = (lstRole.isEmpty()?'':lstRole.get(0).Name);
            final List<MX_SB_VTS_Generica__c> lstRoleInvalid = [Select Id from MX_SB_VTS_Generica__c where Name =: role];
            for(Lead lead : leads) {
                if(!lstRoleInvalid.isEmpty() && recordTypeLead.equals(lead.RecordTypeId)) {
                    lead.addError(System.Label.MX_SB_VTS_ErrorRoleInvalido);
                }
            }
        }
    }

    /**
     * @method: updateCalSolicitud
     * @description: Actualiza campo "Clasificación de Solicitud" conforme a hora de insercion.
     * @params: List<Lead> leadItems, Map<Id, Lead> oldMap
     */
    public static void updateCalSolicitud(List<Lead> leadItems, Map<Id, Lead> oldMap) {
        DateTime dtCreatedDate = null;
        List<Lead> updLeads = null;
        Lead updLead = null;
        String sCalificacionSol = null;
        updLeads = new List<Lead>();
        for(Lead item: leadItems) {
            dtCreatedDate = item.CreatedDate;
            if (dtCreatedDate.hour() >= 9 && dtCreatedDate.hour() <= 21) {
                sCalificacionSol = 'Hot Lead';
            } else {
                sCalificacionSol = 'Cold Lead';
            }
            updLead = new Lead(
                Id = item.Id,
                MX_SB_VTS_CalificacionSolicitud__c = sCalificacionSol
            );
            updLeads.add(updLead);
        }
        update updLeads;
    }
    

    /**
    * @method: mayorAvanceTip
    * @description: EValuacion de la Yipificación de mayor avance
    * @params: String tipi6, String tipi5
    */
    public static void mayorAvanceTip(List<Lead> leadItem) {
        final Id recordType = Schema.SObjectType.lead.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
        for(Lead lst :leadItem) {
            if(String.isNotBlank(lst.RecordTypeId) && lst.RecordTypeId.equals(recordType) && String.isNotBlank(lst.MX_SB_VTS_Tipificacion_LV6__c) && String.isNotBlank(lst.MX_SB_VTS_Tipificacion_LV5__c)) {
                final MX_SB_VTS_Avance_Tipificaci_n__mdt gradoAvanUp = recoAvanceTip(lst.MX_SB_VTS_Tipificacion_LV6__c, lst.MX_SB_VTS_Tipificacion_LV5__c);
                if(lst.MX_SB_VTS_Grado_de_Avance__c <= Integer.valueof(gradoAvanUp.MX_SB_VTS_Ponderaci_n__c) || String.isBlank(String.valueOf(lst.MX_SB_VTS_Grado_de_Avance__c))) {
                    lst.MX_SB_VTS_Motivo_de_mayor_avance__c = lst.MX_SB_VTS_Tipificacion_LV6__c;
                    lst.MX_SB_VTS_Grado_de_Avance__c = gradoAvanUp.MX_SB_VTS_Ponderaci_n__c;
                }
            }
        }
    }

    /**
    * @method: recoAvanceTip
    * @description: Recuperación el grado de mayor avance
    * @params: List<Opportunity> oppItem
    */
    public static MX_SB_VTS_Avance_Tipificaci_n__mdt recoAvanceTip(String tipi6, String tipi5) {
        return [Select MX_SB_VTS_Nombre_Tipificaci_n__c, MX_SB_VTS_Ponderaci_n__c, MX_SB_VTS_Valores_Tipificaciones_Niv5__c
        from MX_SB_VTS_Avance_Tipificaci_n__mdt where MX_SB_VTS_Nombre_Tipificaci_n__c=: tipi6 AND MX_SB_VTS_Valores_Tipificaciones_Niv5__c =: tipi5];
    }

    /**
    * @author: Diego Olvera
    * @method: updateVisit
    * @description: Recupera valores de Candidato cuando se actualiza el campo Tipificacion_LV6, los procesa y el resultado lo inserta en el objeto visita.
    * @params: List<Lead> leadItems
    */
    public static void updateVisit(List<Lead> leadItems) {
        final Id recordType = Schema.SObjectType.lead.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
        final Set<Id> lstTask = new Set<Id>();
        final Set<Id> lstVisit = new Set<Id>();
        final Map<Id, Id> mapLeadId = new Map<Id, Id>();
        final Map<Id, Id> mapTaskId = new Map<Id, Id>();
        for(Lead leadItem: leadItems) {
            if(String.isNotBlank(leadItem.MX_SB_VTS_LookActivity__c)) {
                lstTask.add(leadItem.MX_SB_VTS_LookActivity__c);
                mapLeadId.put(leadItem.Id, leadItem.MX_SB_VTS_LookActivity__c);
            }
        }
        for(Task visit : [Select Id,dwp_kitv__visit_id__c from Task where Id IN: lstTask AND dwp_kitv__visit_id__c NOT IN ('')]) {
            mapTaskId.put(visit.Id, visit.dwp_kitv__visit_id__c);
            lstVisit.add(visit.dwp_kitv__visit_id__c);
        }
        final Map<Id, dwp_kitv__Visit__c> visitObjects = new Map<Id, dwp_kitv__Visit__c>([SELECT Id, MX_SB_VTS_recordNC__c, MX_SB_VTS_recordCE__c, MX_SB_VTS_recordCNE__c,  dwp_kitv__lead_id__c, MX_SB_VTS_PenContactEfect__c, MX_SB_VTS_IsInterest__c, MX_SB_VTS_PendInterest__c,
            MX_SB_VTS_NoAceptCotiz__c, MX_SB_VTS_AceptaCotiz__c, MX_SB_VTS_AceptContra__c FROM dwp_kitv__Visit__c where Id IN: lstVisit]);

        final List<dwp_kitv__Visit__c> lstVisits = new List<dwp_kitv__Visit__c>();
        for(Lead lst: leadItems) {
            if(lst.RecordTypeId.equals(recordType) && String.isNotBlank(lst.MX_SB_VTS_Tipificacion_LV6__c) && String.isNotBlank(lst.MX_SB_VTS_LookActivity__c) && visitObjects.containsKey(mapTaskId.get(mapLeadId.get(lst.Id)))) {
                final dwp_kitv__Visit__c visitObject = evaluateVisit(lst, mapTaskId, visitObjects, mapLeadId);
                lstVisits.add(visitObject);
            }
        }
        update lstVisits;
    }

    /**
    * @description Evalua registros
    * @author Eduardo Hernandez Cuamatzi | 07-14-2020
    * @param Lead lst Lead a evaluar
    * @param Map<Id Id> mapTaskId Mapa de tareas de leads
    * @param Map<Id dwp_kitv__Visit__c> visitObjects Mapa de Visitas de Tareas
    * @param Map<Id Id> mapLeadId  Mapa de Leads con tareas
    * @return dwp_kitv__Visit__c Registro de visita ah actualizar
    **/
    public static dwp_kitv__Visit__c evaluateVisit(Lead lst, Map<Id, Id> mapTaskId, Map<Id, dwp_kitv__Visit__c> visitObjects, Map<Id, Id> mapLeadId) {
        Boolean noContact = false;
        decimal noContacto = 0;
        decimal noLlamaEfect = 0;
        decimal llamaEfect = 0;
        string gradoAvance = '', motivoAvance = '';
        noContacto = lst.MX_SB_VTS_ContadorLlamadasTotales__c - lst.MX_SB_VTS_ContadorRemarcado__c;
        final Map<String, Boolean> efectivos = MX_SB_VTS_LeadAutoAssigner.pendEfectivo(lst);
        if(lst.MX_SB_VTS_ContadorLlamadas__c >= VALIDCALLS) {
            noLlamaEfect = 1;
        }
        if(lst.MX_SB_VTS_Llamadas_Efectivas__c >= VALIDCALLS) {
            llamaEfect = 1;
        }
        if(noContacto>=0 && noLlamaEfect ==0 && llamaEfect ==0) {
            noContact = true;
        }
        gradoAvance = String.valueOf(lst.MX_SB_VTS_Grado_de_Avance__c);
        motivoAvance = lst.MX_SB_VTS_Motivo_de_mayor_avance__c;
        final dwp_kitv__Visit__c visitObject = visitObjects.get(mapTaskId.get(mapLeadId.get(lst.Id)));
        visitObject.MX_SB_VTS_recordNC__c = noContacto;
        visitObject.MX_SB_VTS_recordCE__c = llamaEfect;
        visitObject.MX_SB_VTS_recordCNE__c = noLlamaEfect;
        visitObject.MX_SB_VTS_PenContactEfect__c = efectivos.get('penContactEfect');
        visitObject.MX_SB_VTS_IsInterest__c = efectivos.get('isInterest');
        visitObject.MX_SB_VTS_PendInterest__c = efectivos.get('pendInterest');
        visitObject.MX_SB_VTS_AceptContra__c = efectivos.get('aceptContra');
        visitObject.MX_SB_VTS_AceptaCotiz__c = efectivos.get('aceptaCotiz');
        visitObject.MX_SB_VTS_NoAceptCotiz__c = efectivos.get('noAceptCotiz');
        visitObject.MX_SB_VTS_GradoAvance__c = gradoAvance;
        visitObject.MX_SB_VTS_MotivoAvance__c = motivoAvance;
        visitObject.MX_SB_VTS_IsContact__c = efectivos.get('isContact');
        visitObject.MX_SB_VTS_NoContact__c = noContact;
        return visitObject;
    }
}