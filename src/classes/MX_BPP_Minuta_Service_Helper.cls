/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Minuta_Service
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-10-06
* @Description 	Helper for Minuta BPyP Service Layer
* @Changes      Date		|		Author		|		Description
 * 			2021-02-19			Héctor Saldaña	  New method added "parsearTagsSeleccion" to parse any kind of
 * 		                                          picklist tag identified in Opcionales__c field.
 * 		                                          New method added "parsearTagsTexto" to parse any kind of
 * 		                                          Text tag identified in Opcionales__c field.
 * 		                                          New method added "parseoTagsSwitch" to parse any kind of
 * 		                                          Boolean tag identified in Opcionales__c field.
 * 		    2021-03-05          Héctor Saldaña    Logic added for required components during fields parsing
*  
*/
public without sharing class MX_BPP_Minuta_Service_Helper {
    /* Field attach file object name */
    static final String FIELDATTACHFILE = 'dwp_kitv__Attach_file__c';
    /* Field templConvertedL */
    String templConvertedL {get; set;}
    /* Field lstDocuments */
    List<ContentVersion> lstDocuments {get; set;}
    
    /* Constructor Method */
    @TestVisible
    private MX_BPP_Minuta_Service_Helper() {}
    
    /* Constructor Method */
    @TestVisible
    public MX_BPP_Minuta_Service_Helper(String templateConv, List<ContentVersion> documents) {
        this.templConvertedL = templateConv;
        this.lstDocuments = documents;
    }    
    
    /**
    * @Description 	Check if client email is included in receivers
    * @Param		setReceivers Set<String>, clientId String
    * @Return 		isIncluded Boolean
    **/
    public static Boolean checkIfClientIsIncluded (Set<String> setReceivers, String clientId) {
        Boolean isIncluded = false;
        final Set<Id> setClientId = new Set<Id> {clientId};
        final Map<String, String> mapClientFields = new Map<String, String>();
        mapClientFields.put('fields', 'PersonEmail');
        
        final List<Account> lstClientResult = MX_RTL_Account_Selector.getAccountReusable(setClientId, 'Id', mapClientFields);
        
        for (String receiverEmail : setReceivers) {
            if (receiverEmail.equalsIgnoreCase(lstClientResult[0].PersonEmail)) {
                isIncluded = true;
            }
        }

        return isIncluded;
    }
    
    /**
    * @Description 	Set the indicator when client is on toAddress list
    * @Param		visita dwp_kitv__Visit__c
    * @Return 		NA
    **/
    public static void checkVisitedClientIndicator (dwp_kitv__Visit__c visita) {
        final List<dwp_kitv__Visit__c> listVisit = new List<dwp_kitv__Visit__c>();
        
        visita.MX_BPP_Minuta_al_Cliente__c = true;
        listVisit.add(visita);
        
        Dwp_kitv_Visit_Selector.updteVisit(listVisit);
    }

    /**
    * @Description 	Parse MX_Opcionales__c field in order to build an object with Picklist and options attributes
     *              to be set later into the LWC
    * @Param		camposOpcionales String
    * @Return 		List<MX_BPP_MinutaWrapper.componentePicklist>
    **/
    public static List<MX_BPP_MinutaWrapper.componentePicklist> parsearTagsSeleccion(String camposOpcionales) {
		String tagSeleccion;
        final List<MX_BPP_MinutaWrapper.componentePicklist> lstCmpPicklist = new List<MX_BPP_MinutaWrapper.componentePicklist>();
        final Map<String, String> mapPicklists = new Map<String, String>();
        tagSeleccion = 'Seleccion\\d';
        final Pattern regexPat = Pattern.compile('([{]+(?i)' + tagSeleccion + '[}]+)([a-zA-ZÀ-ÿ\\u00f1\\u00d1\\s:*,]+)(.*?)([{]+(?i)' + tagSeleccion + '[}]+)');
        final Matcher regexMat = regexPat.matcher(camposOpcionales);

        while (regexMat.find()) {
            if(regexMat.group(1) == regexMat.group(4)) {
                mapPicklists.put(regexMat.group(2).normalizeSpace(), regexMat.group(3));
            }
        }

        if (!mapPicklists.isEmpty()) {
            String tagOP;
            Integer index; //NOSONAR
            tagOP = 'Opcion\\d';
            final Pattern regexOps = Pattern.compile('([{]+(i?)' + tagOP + '[}]+)(.*?)([{]+(i?)' + tagOP + '[}]+)');
            index = 1; //NOSONAR
            for (String llave : mapPicklists.keySet()) {
                final Matcher rmOps = regexOps.matcher(mapPicklists.get(llave));
                final List<MX_BPP_MinutaWrapper.componentePicklistValues> lstValues = new List<MX_BPP_MinutaWrapper.componentePicklistValues>();
                while (rmOps.find()) {
                    final MX_BPP_MinutaWrapper.componentePicklistValues optionValue = new MX_BPP_MinutaWrapper.componentePicklistValues();
                    optionValue.label = rmOps.group(3).normalizeSpace();
                    optionValue.value = rmOps.group(3).normalizeSpace();
                    lstValues.add(optionValue);
                }
                final MX_BPP_MinutaWrapper.componentePicklist picklist = new MX_BPP_MinutaWrapper.componentePicklist();
                if (!lstValues.isEmpty()) {
                    picklist.cmpRequired = llave.startsWith('*');
                    picklist.options = lstValues;
                    picklist.label = llave.remove('*');
                    picklist.name = llave.remove('*');
                    picklist.tagInicio = '{Seleccion' + index + '}';
                    index++; //NOSONAR
                }

                lstCmpPicklist.add(picklist);

            }
        }

        return lstCmpPicklist;

    }

    /**
    * @Description 	Parse MX_Opcionales__c field in order to build an object with Text attributes
     *              to be set later into the LWC
    * @Param		camposOpcionales String
    * @Return 		List<MX_BPP_MinutaWrapper.componenteText>
    **/
    public static List<MX_BPP_MinutaWrapper.componenteText>  parsearTagsTexto(String camposOpcionales) {

        final Pattern regexPat = Pattern.compile('([{]+(?i)OpcionalTexto\\d[}]+)(.*?)([{]+(?i)OpcionalTexto\\d[}]+)');
        final Matcher regexMat = regexPat.matcher(camposOpcionales);
        final List<MX_BPP_MinutaWrapper.componenteText> lstCamposText = new List<MX_BPP_MinutaWrapper.componenteText>();

        while(regexMat.find()) {
            if(regexMat.group(1) == regexMat.group(3)) {
                final MX_BPP_MinutaWrapper.componenteText campotext = new MX_BPP_MinutaWrapper.componenteText();
                campotext.name = regexMat.group(1).remove('{').remove('}').remove('*');
                campotext.label = regexMat.group(2).remove('*');
                campotext.tagInicio = regexMat.group(1);
                campotext.cmpRequired = regexMat.group(2).startsWith('*');
                lstCamposText.add(campotext);
            }
        }

        return lstCamposText;
    }

    /**
    * @Description 	Parse MX_Opcionales__c field in order to build an object with Switch attributes
     *              to be set later into the LWC
    * @Param		camposOpcionales String
    * @Return 		List<MX_BPP_MinutaWrapper.componenteSwitch>
    **/
    public static List<MX_BPP_MinutaWrapper.componenteSwitch> parseoTagsSwitch(String camposOpcionales) {

        final Pattern regexPat = Pattern.compile('([{]+(?i)Switch\\d[}]+)(.*?)([{]+(?i)Switch\\d[}]+)');
        final Matcher regexMat = regexPat.matcher(camposOpcionales);
        final List<MX_BPP_MinutaWrapper.componenteSwitch> lstSwitch = new List<MX_BPP_MinutaWrapper.componenteSwitch>();

        while(regexMat.find()) {
            if(regexMat.group(1) == regexMat.group(3)) {
                final MX_BPP_MinutaWrapper.componenteSwitch campoSwitch = new MX_BPP_MinutaWrapper.componenteSwitch();
                campoSwitch.name = regexMat.group(1).remove('{').remove('}').remove('*');
                campoSwitch.label = regexMat.group(2).remove('*');
                campoSwitch.tagInicio = regexMat.group(1);
                campoSwitch.cmpRequired = regexMat.group(2).startsWith('*');
                lstSwitch.add(campoSwitch);
            }
        }

        return  lstSwitch;
    }

}