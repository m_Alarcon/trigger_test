/**
* @FileName          : MX_SB_VTS_wrpColoniasResp
* @description       : Wrapper class for use de web service of listInsuranceCatalog
* @Author            : Marco Antonio Cruz Barboza  
* @last modified on  : 08-05-2020
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   08-05-2020   Marco Antonio Cruz Barboza          Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_SB_VTS_wrpColoniasResp {
	/** Wrapper Call*/
    public iCatalogItem iCatalogItem {get;set;}
    /**
    * @description : Object iCatalogItem to call a web service in an array
    * @Param 
    * @Return
    **/
    public class iCatalogItem {
        /**
        * @description : suburb list for get neighbourhoods
        **/
        public suburb[] suburb {get;set;}
    }
    /**
    * @description : Wrapper Object suburb to get de content of PostalCodes Service
    * @Param 
    * @Return
    **/
    public class suburb {
        /**
        * @description : Object of cities 
        **/
        public citys city {get;set;}
        /**
        * @description : Object of countries
        **/
        public county county {get;set;}
        /**
        * @description : Object of neighborhood
        **/
        public neighborhood neighborhood {get;set;}
        /**
        * @description : Object of states
        **/
        public state state {get;set;}
    }
    
    /**
    * @description : Wrapper Object city to get de content of PostalCodes Service
    * @Param 
    * @Return
    **/
    public class citys {
        /**
        * @description : City Name
        **/
        public String name {get;set;}   
        /**
        * @description : City Id from web service
        **/ 
        public String idCity {get;set;}  
    }
    
    /**
    * @description : Wrapper Object county to get de content of PostalCodes Service
    * @Param 
    * @Return
    **/
    public class county {
        /**
        * @description : Name Country
        **/
        public String name {get;set;}   
        /**
        * @description : Country Id from web service
        **/ 
        public String idCounty {get;set;}   
    }
    
    /**
    * @description : Wrapper Object neighborhood to get de content of PostalCodes Service
    * @Param 
    * @Return
    **/
    public class neighborhood {
        /**
        * @description : Neighbourhood Name
        **/
        public String name {get;set;}   
        /**
        * @description : Neighbourhood Id from web service
        **/ 
        public String idNeighbor {get;set;}  
    }
    
    /**
    * @description : Wrapper Object state to get de content of PostalCodes Service
    * @Param 
    * @Return
    **/
    public class state {
        /**
        * @description : State Name
        **/
        public String name {get;set;}   
        /**
        * @description : State Id from web service
        **/ 
        public String idState {get;set;}  
    }
    
}