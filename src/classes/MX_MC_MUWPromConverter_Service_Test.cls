/**
 * @File Name          : MX_MC_MUWPromConverter_Service_Test.cls
 * @Description        : Clase para Test de MX_MC_MUWPromConverter_Service
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 24/08/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      24/08/2020           Jair Ignacio Gonzalez Gayosso.       Initial Version
**/
@isTest
public with sharing class MX_MC_MUWPromConverter_Service_Test {

    /**
     * @Description Clase de prueba para MX_MC_MUWPromConverter_Service
     * @author Jair Ignacio Gonzalez Gayosso | 24/08/2020
    **/
    @isTest
    static void testInputMsg() {
        final Map<String, Object> inputMx =  new Map<String, Object>();
        final MX_MC_MUWPromConverter_Service updateConv = new MX_MC_MUWPromConverter_Service();
        inputMx.put('content','Test promote message');
        inputMx.put('messageId', '"testInputMsg"');
        inputMx.put('messageThreadId','threadPromote123');
        inputMx.put('isDraft',true);
        final Map<String, Object> outputMx =  updateConv.convertMap(inputMx);
        System.assertEquals(inputMx.get('content'), outputMx.get('message'), 'El content no es esperado');
    }

    /**
     * @Description Clase de prueba para MX_MC_MUWPromConverter_Service
     * @author Jair Ignacio Gonzalez Gayosso | 24/08/2020
    **/
    @isTest
    static void testInputConv() {
        final MX_MC_MUWPromConverter_Service updateConv = new MX_MC_MUWPromConverter_Service();
        final Map<String, Object> inputMx =  new Map<String, Object>();
        inputMx.put('messageThreadId','TestThreaId');
        inputMx.put('messageThreadBody', '{"isSolved":true,"id":"MP0031231"}');
        final Map<String, Object> outputMx =  updateConv.convertMap(inputMx);
        System.assertEquals('true', outputMx.get('isSolved'), 'El content no es esperado');
    }
}