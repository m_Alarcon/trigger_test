@isTest
/**
* @FileName          : MX_SB_VTS_GetQuotation_Service_Test
* @description       : Service class for use de web service of GetQuotation
* @Author            : Marco Antonio Cruz Barboza  
* @last modified on  : 18-09-2020
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   18-09-2020   Marco Antonio Cruz Barboza          Initial Version
**/
public class MX_SB_VTS_GetQuotation_Service_Test {
	
    /*User name test*/
    Final static String USRTST = 'I AM A USERNAME TEST';
    /*Account name test*/
    Final static String ACCNMTST = 'I AM A ACCOUNT NAME TEST';
    /*Opportunity name test*/
    Final static String OPPTST = 'I AM A OPPORTUNITY NAME TEST';
    /*URL name test*/
    Final static String TSTURL = 'http://www.ulrexample.com';
    /*Encryoted value*/
    Final static String ENCRYPTED = '87yiUyhpiiC27LJUwcMY067LJLdm9tNfwLXucJtWpKw';
   	/*User Test*/
    private static User userToTst = new User();
    
    /* 
    @Method: setupTest
    @Description: create test data set
    */
    @TestSetup
    static void setupTest() {
        Final User testUsers = MX_WB_TestData_cls.crearUsuario(USRTST, System.Label.MX_SB_VTS_ProfileAdmin);
        insert testUsers;
        System.runAs(testUsers) {
            Final Account testToAcc = MX_WB_TestData_cls.crearCuenta(ACCNMTST, System.Label.MX_SB_VTS_PersonRecord);
            testToAcc.PersonEmail = 'pruebaVts@mxvts.com';
            insert testToAcc;
            
            Final Opportunity testToOpp = MX_WB_TestData_cls.crearOportunidad(OPPTST, testToAcc.Id, testUsers.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            testToOpp.LeadSource = 'Call me back';
            testToOpp.Producto__c = 'Hogar';
            testToOpp.Reason__c = 'Venta';
            testToOpp.StageName = 'Cotizacion';
            insert testToOpp;
       		
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'ProxyValueCipher', iaso__Url__c = TSTURL, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
			insert new iaso__GBL_Rest_Services_Url__c(Name = 'GetQuotation', iaso__Url__c = TSTURL, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
        }
    }
    
    /*
    @Method: getQuotationTest
    @Description: return Map with a List of Strings from a service
	@param String ZipCode
    */
    static testMethod void getQuotationTest() {
        userToTst = [SELECT ID, Name FROM User WHERE Name=:USRTST];
        System.runAs(userToTst) {
            Final MX_WB_Mock mockCallout = new MX_WB_Mock(200, 'Complete', '{"quotation": {"id": 616892, "number": null, "expirationDate": "2019-16-08" }, "policy": {"number": null,"validity": {"period": {"startDate": "2018-12-28", "endDate": "2019-01-28"}, "type": { "code": "L", "description": "LIBRE"}}}}', null); 
            iaso.GBL_Mock.setMock(mockCallout);
            test.startTest();
            	Final Map<String, String> encryptedData = new Map<String, String>();
            	encryptedData.put('0', ENCRYPTED);
            	Final Map<String,String> getValues = MX_SB_VTS_GetQuotation_Service.getQuotation(encryptedData);
        	test.stopTest();
            System.assertNotEquals(getValues, null,'El mapa tiene contenido');
        }
    }
    
}