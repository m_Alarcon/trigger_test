/**
 * @File Name          : MX_SB_VTS_GetSetAlianzas_RP_Ctrl.cls
 * @Description        : Realiza el consumo del servicio getListInsuranceInformationCatalogs
 * @Author             : Alexandro Corzo
 * @Group              :
 * @Last Modified By   : Alexandro Corzo
 * @Last Modified On   : 14/10/2020 10:30:00
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0       14/10/2020      Alexandro Corzo        Initial Version
 * 1.1       12/01/2021      Alexandro Corzo        Se agrega request consumo ASO
**/
@SuppressWarnings('sf:UseSingleton')
public class MX_SB_VTS_GetSetAlianzas_RP_Ctrl {
	/**
     * @description: Realiza el consumo del servicio getListInsuranceInformationCatalogs
     * @author: 	 Alexandro Corzo
     * @return: 	 Map<String, Object> oReturnValues
     */
    @AuraEnabled(cacheable=true)
    public static Map<String, Object> obtListInsInfoCatCtrl() {
        return MX_SB_VTS_GetSetAlianzas_RP_Service.obtListInsInfoCatSrv();
    }
}