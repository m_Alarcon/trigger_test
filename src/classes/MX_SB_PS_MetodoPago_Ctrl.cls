/*
----------------------------------------------------------
* Nombre: MX_SB_PS_MethodPago_Ctrl
* Daniel Perez Lopez
* Proyecto: Salesforce-Presuscritos
* Descripción : clase controladora de front methoddepago

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                   Descripción
* --------------------------------------------------------------------------------
* 1.0           08/07/2020     Daniel Perez		        Creación
* 1.1           15/01/2021    Juan Carlos Benitez      Actualizar Quoli
* 1.2           07/02/2021    Juan Carlos Benitez      Methodos busqueda quote y codeIvr
* 1.3           18/02/2021    Vincent Juárez           Se agrega méthodo para obtener custom metadata generica
* 1.4           22/02/2021    Juan Carlos Benitez      Se combinan methodos por limite de 10 por clase
* --------------------------------------------------------------------------------
*/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_SB_PS_MetodoPago_Ctrl {
    /*
     * @desciption Variable ocupada para seleccionar campos en selector
     */
    public static FINAL String CAMPOSOP=' Id,Name ';
    /**Llave respuesta ok */
    final static String ISOK = 'isOk';
	/** list of records to be retrieved */
	final static List<Task> TASK_DATA = new List<Task>();
    /** list of records to be retrieved */
    final static List<QuoteLineItem> QUOTELI = new List<QuoteLineItem>();
    /*
    * @description method actualizaoportunidad
    * @param Opportunity opp
    */
    @auraEnabled
    public static void actualizaoportunidad(Opportunity opp) {
        MX_SB_PS_MetodoPago_Service_cls.actualizaoportunidad(opp);
    }
       /*
    * @description method getTaskData
    * @param String oppId
    */ 
    @AuraEnabled
    public static List<Task> getTaskData(String oppId) {
        if(String.isNotBlank(oppId)) {
                final Set<Id> ids = new Set<Id>{oppId};
             	TASK_DATA.addAll(MX_RTL_Task_Service.getTaskData(ids));
            	}
            return TASK_DATA;
    }
    /*
    * @description method enviarOTP
    * @param String oppId
    */ 
    @AuraEnabled(cacheable=true)
    public static string enviarOtp(Map<String,String> mapdata) {
        String rtrn='';
        HttpResponse resData = new HttpResponse();
        try {
            resData = MX_SB_PS_IASO_Service_cls.getServiceResponseMap('GetValidateOTP',mapdata);
            rtrn=''+resData.getStatusCode();
        } catch(Exception e) {
            rtrn=''+resData.getStatusCode();
        }
        return rtrn;
    }
    /**
* @description
* @author Juan Carlos Benitez | 10/25/2020
* @param oppId
* @return Opportunity
**/
    @AuraEnabled
    public static Opportunity loadOppK (String oppId) {
        return MX_SB_PS_Utilities.fetchOpp(oppId);
    }
/**
* @description
* @author Juan Carlos Benitez | 10/25/2020
* @param quoteId
* @return Quote
**/
    @AuraEnabled
    public static List<QuoteLineItem> getQuoteLneItem(String quoteId) {
        if(String.isNotBlank(quoteId)) {
            final Set<Id> ids = new Set<Id>{quoteId};
                QUOTELI.addAll(MX_RTL_Quote_Service.selSObjectsByQuotes(ids));
        }
        return QUOTELI;
    }
    /**
    * @description Actualiza Quote previo al envio a IVR
    * @param String oppId Id del registro a actualizar
    **/
  @AuraEnabled
    public static void updateQuoteIVR(String oppId) {
    final Quote quoteData = MX_SB_VTS_PaymentModule_Service.findQuote(oppId);
        quoteData.MX_ASD_PCI_DescripcionCode__c = '';
        quoteData.MX_ASD_PCI_ResponseCode__c = '';
        quoteData.MX_ASD_PCI_MsjAsesor__c = '';
        MX_SB_VTS_PaymentModule_Service.updateQuote(quoteData);
    }
    /**
    * @description 
    * @author Daniel Perez Lopez | 11-24-2020 
    * @param oppObj 
    * @return String 
    **/
    @AuraEnabled (cacheable=false)
    public static String emisionPoliza (Opportunity oppObj) {
        return MX_SB_PS_Resumen_Cotizacion_Ctrl.emiteQuoteNoCobro(oppObj.Id);
    }
    /**
    * @description : Methodo actualiza y retorna QuotelineItem
    *                relacionado.
    * @author Daniel Perez Lopez | 11-24-2020 
    * @param quoli 
    * @return String 
    **/
     @AuraEnabled
    public static QuoteLineItem[] actualizaQuoli (QuoteLineItem quoli) {
        Final List<QuotelineItem> quoliList= new List<QuotelineItem>();
        quoliList.add(quoli);
		return MX_RTL_QuoteLineItem_Selector.upsertQuoteLI(quoliList);
    }
    /*
	* Methodo que sustituye findByTypeVal y getQuoteInfo
	* Description: Busqueda generica de objetos
	* param: String str, string tipo
	* return: List<SObject>
	*/

    @AuraEnabled
    public static List<Sobject> findGenericObject (String str,String tipo) {
        return MX_SB_PS_MetodoPago_Service_cls.findGenericObject(str,tipo);
    }
        
    /*
    * @description method getOpenPayCS
    * @param 
    */
    @AuraEnabled
    public static MX_SB_VTS_Generica__c reqOpenPayCS() {
        return MX_SB_VTS_Generica__c.getValues('OpenPayPresuscritos');
    }
}