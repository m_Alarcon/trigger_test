@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_BusquedaPoliza_Cls_TEST
* Autor Daniel Perez Lopez
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_BusquedaPoliza_Cls_Controller

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           17/12/2019      Daniel Lopez                         Creación
* 1.1			18/12/2019		Marco Cruz							 Solución de Issues
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_BusquedaPoliza_Cls_TEST {
    /*
    *Variable mapa resultado de method
    */
    private static Map<String,String> mapaupdate {get;set;}
     /*
    *Variable ocupada para respuesta de methods al front
    */
    private static String respuesta {get;set;}

    @testSetup
    static void setDatos() {
        final String nameProfile = [SELECT Name from profile where name in ('Administrador del sistema','System Administrator') limit 1].Name;
        final User objUsrTst = MX_WB_TestData_cls.crearUsuario('PruebaAdminTst', nameProfile); 
        insert objUsrTst;
        System.runAs(objUsrTst) {
        final String  tiporegistro = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
		final Account ctatest = new Account(recordtypeid=tiporegistro,firstname='DANIEL',lastname='PEREZ',Apellido_materno__pc='LOPEZ',RFC__c='PELD920912',PersonEmail='test@gmail.com');
        insert ctatest;
		final Account ctatestluffy = new Account(recordtypeid=tiporegistro,lastname='luffy',RFC__c='lufy192312',PersonEmail='luffy@gmail.com');
		insert ctatestluffy;
            final Contract contrato = new Contract(accountid=ctatest.Id,MX_SB_SAC_NumeroPoliza__c='PolizaTest',MX_SB_SAC_RFCAsegurado__c =ctatest.rfc__c,MX_SB_SAC_NombreClienteAseguradoText__c=ctatest.firstname,MX_WB_apellidoPaternoAsegurado__c=ctatest.lastname,MX_WB_apellidoMaternoAsegurado__c=ctatest.Apellido_materno__pc);
            insert contrato;
            final Siniestro__c sini2= new Siniestro__c();
            sini2.Folio__c='sinitest2';
            sini2.Poliza__c='folio2';
            sini2.MX_SB_MLT_Certificado_Inciso__c='1';
            sini2.MX_SB_MLT_Placas__c='ldny123';
            sini2.MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro';
            sini2.MX_SB_SAC_Contrato__c=contrato.Id;
            
            insert sini2;
            final Siniestro__c sini= new Siniestro__c();
            sini.Folio__c='sinitest';
            sini.MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro';
            sini.MX_SB_SAC_Contrato__c=contrato.Id;
            sini.MX_SB_MLT_Placas__c='MSN-000';
            insert sini;
            final Case objCaseTst = new Case(Status='New',Origin='Phone',Priority='Medium',AccountId=ctatest.Id,MX_SB_MLT_URLLocation__c='19.305819,-99.2115422',MX_SB_MLT_Siniestro__c =sini.id);
            insert objCaseTst;
        }
    } 

     @IsTest static void metodosrchCtrExito() {
        final Siniestro__c sini = [SELECT id,Folio__c,Poliza__c,MX_SB_MLT_Certificado_Inciso__c,MX_SB_MLT_Placas__c FROM Siniestro__c WHERE MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro' and Folio__c='sinitest' LIMIT 1];
        Test.startTest();
            final Siniestro__c respsrchCtr = MX_SB_MLT_BusquedaPoliza_Cls_Controller.siniestroActual(sini.id);
            sini.Poliza__c='OTN009897';
            sini.MX_SB_MLT_Certificado_Inciso__c='003232';
            sini.MX_SB_MLT_Placas__c='';
            update sini;
        Test.stopTest();
        System.assertEquals(respsrchCtr.Folio__c,'sinitest','No es el dato que se esperaba');
    }   

    @IsTest static void metodosrchCtt() {
		Final Map<String,String> mapadatapol = new Map<String,String>();
        	mapadatapol.put('polizaSrch','polizaSrch');
            mapadatapol.put('certificado','certificado');
            mapadatapol.put('producto','producto');
            mapadatapol.put('fecocurrido','fecocurrido');
            mapadatapol.put('nombreAsegurado','nombreAsegurado');
            mapadatapol.put('rfcSrch','rfcSrch');
            mapadatapol.put('aPaterno','aPaterno');
            mapadatapol.put('aMaterno','aMaterno');
            mapadatapol.put('nombreCta','nombreCta');
            mapadatapol.put('aPaternoCta','aPaternoCta');
            mapadatapol.put('aMaternoCta','aMaternoCta');
            mapadatapol.put('nEmpleado','nEmpleado');
            mapadatapol.put('telefono','telefono');
            mapadatapol.put('nCredito','nCredito');
            mapadatapol.put('serie','serie');
            mapadatapol.put('placas','placas');
        final String bsqpoliza =JSON.serialize(mapadatapol);
        final account cuentadny = [Select id from account where firstname='daniel' and lastname='perez' limit 1];
        
        Test.StartTest();
            final Map<String,SObject[]> objetos =MX_SB_MLT_BusquedaPoliza_Cls_Controller.srchCtt(bsqpoliza,true,cuentadny.id); 
            MX_SB_MLT_BusquedaPoliza_Cls_Controller.srchCtt(bsqpoliza,false,null);
            System.assert(objetos.get('contratos').size()>0, 'La información no es la esperada');
        Test.stopTest();
    } 

    @IsTest static void metodosrchCtterror() {
        Test.StartTest();
            try {
                MX_SB_MLT_BusquedaPoliza_Cls_Controller.srchCtt('errorcampo',false,null);
            } catch(exception e) {
                respuesta=e.getMessage();
                System.assert(respuesta.contains('Unrecognized'),'Excepcion probocada con exito');
            } 
        Test.stopTest();
    } 

    @IsTest static void metodoupdateSinCont() {
        respuesta='';
        final Siniestro__c sinitab = [SELECT id FROM Siniestro__c WHERE MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro' and Folio__c='sinitest' LIMIT 1];
            mapaupdate = new Map<String, String>();
            mapaupdate.put('numpoliza','PolizaTest');
            mapaupdate.put('moneda','MXN');
        test.startTest();
            try {
                respuesta =MX_SB_MLT_BusquedaPoliza_Cls_Controller.updateSinCont(mapaupdate,sinitab.id); 
                System.assertEquals('Exito',respuesta,'Exito en respuesta');
            } catch(AuraHandledException e) {
                respuesta= e.getMessage();
            } 
        test.stopTest();
    } 
    @IsTest static void metodoupdateSinContfail() {
        respuesta='';
            mapaupdate = new Map<String, String>();
            mapaupdate.put('numpoliza','PolizaTest');
            mapaupdate.put('moneda','MXN');
        test.startTest();
            try {
                respuesta =MX_SB_MLT_BusquedaPoliza_Cls_Controller.updateSinCont(mapaupdate,'asd');
            } catch(AuraHandledException e) {
                respuesta = e.getMessage();
                System.assert(respuesta.contains('xception'),'excepcion generada con exito');
            }
        test.stopTest();
    } 
}