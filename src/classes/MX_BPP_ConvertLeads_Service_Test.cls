/**
 * @File Name          : MX_BPP_ConvertLeads_Service_Test.cls
 * @Description        : Clase para Test de MX_BPP_ConvertLeads_Service_Test
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 03/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      03/06/2020           Jair Ignacio Gonzalez G.   Initial Version
**/
@isTest
private class MX_BPP_ConvertLeads_Service_Test {
    /**
     **Descripción: Clase makeData
    **/
    @TestSetup static void makeData() {
        final Campaign oCamp = new Campaign(Name = 'Campaign Service Test');
        insert oCamp;
        final Account oAcc = new Account(Name='Test Service Account',No_de_cliente__c = '8316251');
        insert oAcc;
        final Lead oLead = new Lead(LastName='Lead Service Test', RecordTypeId=Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('MX_BPP_Leads').getRecordTypeId(),
                MX_ParticipantLoad_id__c=oAcc.No_de_cliente__c, MX_LeadEndDate__c=Date.today().addDays(13), MX_WB_RCuenta__c= oAcc.Id, MX_LeadAmount__c=250, LeadSource='Preaprobados');
        insert oLead;
        final CampaignMember oCampMem = new CampaignMember(CampaignId = oCamp.Id, Status='Sent', LeadId = oLead.Id, MX_LeadEndDate__c=Date.today().addDays(13));
        insert oCampMem;
    }
    /**
     * *Descripción: Clase de prueba para convertToLead
    **/
    @isTest static void testServiceMethods() {
        final List<Id> lsIds = new List<Id>{[SELECT Id FROM Lead LIMIT 1].Id};
        Test.startTest();
        final MX_BPP_ConvertLeads_Service singleton = new MX_BPP_ConvertLeads_Service(); //NOSONAR
        final Map<String,String> result = MX_BPP_ConvertLeads_Service.convertToLead(lsIds);
        Test.stopTest();
        System.assert(result.containsKey(lsIds[0]), 'Lead not converted');
    }
    /**
     * *Descripción: Clase de prueba para convertToLead
    **/
    @isTest static void testCatch1() {
        final CampaignMember oLead = [SELECT Id, MX_LeadEndDate__c, LeadId FROM CampaignMember LIMIT 1];
        oLead.MX_LeadEndDate__c = null;
        update oLead;
        final List<Id> lsIds = new List<Id>{oLead.LeadId};
        Boolean oAssert = false;
        Test.startTest();
        try {
            MX_BPP_ConvertLeads_Service.convertToLead(lsIds);
        } catch (AuraHandledException err) {
            oAssert = true;
        }
        Test.stopTest();
        System.assert(oAssert, 'Test catch for convertToLead');
    }
}