/**
* @File Name          : MX_BPP_Contactabilidad_Service_Test.cls
* @Description        : Test class for MX_BPP_Contactabilidad_Service
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 07/10/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      07/10/2020            Gabriel Garcia Rojas          Initial Version
**/
@isTest
private class MX_BPP_Contactabilidad_Service_Test {

    /*Usuario de pruebas*/
    private static User testUserStartS = new User();
    /** namerole variable for records */
    final static String NAME_ROLES = '%BPYP BANQUERO BANCA PERISUR%';
    /** nameprofile variable for records */
    final static String NAME_PROFILES = 'BPyP Estandar';
    /** name variable for records */
    final static String NAMESTS = 'testUser';
    /** name variable for records */
    final static String NAMECOT_SUCURSAL = '6343 PEDREGAL';
    /** namedivision variable for records */
    final static String NAMECOT_DIVISION = 'METROPOLITANA';
    /** name variable for oficina */
    final static String PRIVADOSTR ='PRIVADO';

    /*Variable test 15*/
    static final String NUMS15='15';
    /** error message */
    final static String MESSAGETESTS = 'Fail method';

     /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testUserStartS = UtilitysDataTest_tst.crearUsuario(NAMESTS, NAME_PROFILES, NAME_ROLES);
        testUserStartS.Title = PRIVADOSTR;
        testUserStartS.Divisi_n__c = NAMECOT_DIVISION;
        testUserStartS.BPyP_ls_NombreSucursal__c = NAMECOT_SUCURSAL;
        testUserStartS.VP_ls_Banca__c = 'Red BPyP';
        insert testUserStartS;

        final DateTime fechaTestSer = System.today();

        final Id rtAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta BPyP').getRecordTypeId();

        final Account cuentaSer = new Account(FirstName = 'CuentaTest', LastName = 'LastTest', No_de_cliente__c = '12345678', RecordTypeId = rtAcc, OwnerId = testUserStartS.Id );
        insert cuentaSer;

        System.runAs(testUserStartS) {
            final List<dwp_kitv__Visit__c> listVisitaS = new List<dwp_kitv__Visit__c>();
            final dwp_kitv__Visit__c visit1 = new dwp_kitv__Visit__c(Name = 'VisitaTest', dwp_kitv__visit_status_type__c = '05', dwp_kitv__account_id__c = cuentaSer.Id,
                                                              dwp_kitv__visit_duration_number__c = NUMS15, dwp_kitv__visit_start_date__c = fechaTestSer);
            final dwp_kitv__Visit__c visit2 = new dwp_kitv__Visit__c(Name = 'VisitaTest2', dwp_kitv__visit_status_type__c = '01', dwp_kitv__account_id__c = cuentaSer.Id,
                                                               dwp_kitv__visit_duration_number__c = NUMS15, dwp_kitv__visit_start_date__c = fechaTestSer);

            listVisitaS.add(visit1);
            listVisitaS.add(visit2);

            insert listVisitaS;
        }
    }

    /**
     * @description constructor test
     * @author Gabriel Garcia | 07/10/2020
     * @return void
     **/
    @isTest
    private static void testConstructor() {
        final MX_BPP_Contactabilidad_Service instanceTestS = new MX_BPP_Contactabilidad_Service();
        System.assertNotEquals(instanceTestS, null, MESSAGETESTS);
    }

    /**
     * @description test fetchDataService
     * @author Gabriel Garcia | 07/010/2020
     * @return void
     **/
    @isTest
    private static void fetchDataServiceTest() {
        Test.startTest();
        final MX_BPP_Contactabilidad_Service.WRP_ChartStacked wrpCharTest = MX_BPP_Contactabilidad_Service.fetchDataService('Division', PRIVADOSTR, new List<String>{''}, null, null);
        System.assertNotEquals(wrpCharTest, null, MESSAGETESTS);

        Test.stopTest();
    }

    /**
     * @description test servContactaAcc
     * @author Gabriel Garcia | 07/10/2020
     * @return void
     **/
    @isTest
    private static void servContactaAccTest() {
        Test.startTest();
        final List<Account> listAccTSer = MX_BPP_Contactabilidad_Service.servContactaAcc(new List<String>{'', 'Contact', PRIVADOSTR, '10', 'Division'}, null, null);
        MX_BPP_Contactabilidad_Service.servContactaAcc(new List<String>{'', 'NonContact', PRIVADOSTR, '10', 'Division'}, null, null);
        System.assertNotEquals(listAccTSer, null, MESSAGETESTS);

        Test.stopTest();
    }

    /**
     * @description test fetchUserByOffice
     * @author Gabriel Garcia | 07/010/2020
     * @return void
     **/
    @isTest
    private static void fetchUserByOfficeTest() {
        Test.startTest();
        final List<User> listUserTSer = MX_BPP_Contactabilidad_Service.fetchUserByOffice(NAMECOT_SUCURSAL, PRIVADOSTR);
        System.assertNotEquals(listUserTSer, null, MESSAGETESTS);

        Test.stopTest();
    }
}