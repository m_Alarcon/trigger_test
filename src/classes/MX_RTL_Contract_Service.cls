/**
 * @File Name          : MX_RTL_Contract_Service.cls
 * @Description        :
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 06/06/2020, 06:07:22 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/27/2020    Juan Carlos Benitez         Initial Version
 * 1.1    10/07/2020    Gerardo Mendoza Aguilar    fields were added to variable CNTRCTFIELDS
**/
public class MX_RTL_Contract_Service {
    /** Campos consulta de contract */
    public static Final String CNTRCTFIELDS=' Id ,StartDate ,MX_SB_SAC_NumeroPoliza__c ';
    /** Clausula campo SAC_NumeroPoliza */
    public static Final String SAC_NO_POLIZA ='MX_SB_SAC_NumeroPoliza__c';
    /** Clausula campo MX_WB_Oportunidad__c */
    public static Final String OPPID ='MX_WB_Oportunidad__c';
    /** Nombre del Proceso para el producto */
    public static Final Set<String> PROCESS = new Set<String>{'SAC'};
    /**Constructor privado**/
    private MX_RTL_Contract_Service() { }
	/*
    * @description Busqueda de datos del siniestro
    * @param Set<Id> SinIds
    */
    public static List<Contract> getContractIdBySACnPoliza(String policy) {
        return MX_RTL_Contract_Selector.getContractBySACnPoliza(policy, CNTRCTFIELDS,SAC_NO_POLIZA);
    }

    /*
    * @description 
    * @param String Policy
    */
    public static List<Contract> getContractIdByOppId(String policy) {
        return MX_RTL_Contract_Selector.getContractBySACnPoliza(policy, CNTRCTFIELDS,OPPID);
    }
    /*
    * @description Busqueda de datos del contrato
    * @param policy, fields
    */
    public static List<Contract> getContractIdBySACnPoliza(String policy,String fields) {
        return MX_RTL_Contract_Selector.getContractBySACnPoliza(policy, fields,SAC_NO_POLIZA);
    }

    /*
    * @description Busqueda de datos del contrato por nombre
    * @param String nombre, String apaterno
    */
    public static List<Contract> getContractIdByNombre(String nombre, String apaterno, String srchFields) {
        return MX_RTL_Contract_Selector.getContractIdByNombre(nombre, apaterno, srchFields);
    }

    /*
    * @description Busqueda de datos del contrato por RFC
    * @param String rfcAsegurado
    */
    public static List<Contract> getContractIdByRFC(String rfcAsegurado, String srchFields) {
        return MX_RTL_Contract_Selector.getContractIdByRFC(rfcAsegurado, srchFields);
    }

    /*
    * @description Busqueda de datos del contrato por nombre de la cuenta
    * @param String idCtaContrato
    */
    public static List<Contract> getContractIdByAccount(String idCtaContrato, String srchFields) {
        return MX_RTL_Contract_Selector.getContractIdByAccount(idCtaContrato, srchFields);
    }

    /*
    * @description Busqueda de datos del contrato por póliza y Certificado
    * @param String polizaCtr, String certificadoCtr
    */
    public static List<Contract> getContractIdByPoliza (String polizaCtr , String certificadoCtr, String srchFields) {
        return MX_RTL_Contract_Selector.getContractIdByPoliza(polizaCtr ,certificadoCtr, srchFields);
    }

    /*
    * @description Realiza upsert de un contrato
    * @param Contract poliza
    */
    public static void upsertSinCont (Contract poliza) {
        MX_RTL_Contract_Selector.upsertSinCont(poliza);
    }
    /*
    * @description Realiza upsert de un contrato
    * @param String idprod
    */
    public static void upsertContract (Contract contrObj, String producto, integer periodo, String ide) {
        final Set<String> RSPH = new Set <String> {producto};
		final List<Product2> productoLst = MX_RTL_Product2_Selector.findProsByNameProces(RSPH,PROCESS,true);
        contrObj.MX_WB_Producto__c = productoLst[0].id;
        contrObj.ContractTerm = periodo;
        ContrObj.Id=ide;
        MX_RTL_Contract_selector.upsertSinCont(contrObj);
	}
}