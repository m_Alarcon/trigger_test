/**
* @File Name          : MX_SB_PS_InformacionCuenta_Ctrl.cls
* @Description        :
* @Author             : Juan Carlos Benitez
* @Group              :
* @Last Modified By   : Juan Carlos Benitez
* @Last Modified On   : 6/24/2020, 12:00:03 PM
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    6/24/2020   Juan Carlos Benitez          Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_SB_PS_InformacionCuenta_Ctrl {
    /** list of records to be retrieved */
    final static List<Account> ACC_DATA = new List<Account>();
    
    /** list of records to be retrieved */
    final static List<Contact> CNTCT_DATA = new List<Contact>();
    
    /** list of records to be retrieved */
    final static List<Opportunity> OPP_DATA = new List<Opportunity>();
    /**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param recId
* @return List<Account>
**/
    @AuraEnabled
    public static list<Account> loadAccountF(String recId) {
        if(String.isNotBlank(recId)) {
            final Set<Id> ids = new Set<Id>{recId};
                ACC_DATA.addAll(MX_RTL_Account_Service.getSinHData(ids));
        }
        return ACC_DATA;
    }
    /**
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param oppId
* @return List<Opportunity>
**/
    @AuraEnabled
    public static Opportunity loadOppF(String oppId) {
        return MX_SB_PS_Utilities.fetchOpp(oppId);
    }
        /**
* @description
* @author Daniel Ramirez | 10/22/2020
* @param 
* @return 
**/
    @AuraEnabled
    public static List<PersonLifeEvent> loadPersLiEvent(String idContact) {
        return MX_RTL_personLifeEvent_Service.getLifeEventbyContactId(idContact);
    }
}