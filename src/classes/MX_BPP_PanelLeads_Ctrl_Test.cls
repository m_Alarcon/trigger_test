/**
* @File Name          : MX_BPP_PanelLeads_Ctrl_Test.cls
* @Description        : Test class for MX_BPP_PanelLeads_Ctrl
* @Author             : Gabriel Garcia Rojas
* @Group              :
* @Last Modified By   : Gabriel Garcia Rojas
* @Last Modified On   : 08/06/2020
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		        Modification
*==============================================================================
* 1.0      08/06/2020            Gabriel Garcia Rojas          Initial Version
**/
@isTest
private class MX_BPP_PanelLeads_Ctrl_Test {
    /** error message */
    final static String MESSAGEFAIL = 'Fail method';
    /**
     * @description setup data
     * @author Gabriel Garcia | 08/06/2020
     * @return void
     **/
    @TestSetup
    private static void testSetUpData() {
        final Campaign campaignTest = new Campaign(Name = 'Test Campaign');
        insert campaignTest;
        final Lead leadTest = new Lead(LastName = 'Test Canidato');
        insert leadTest;
        final CampaignMember miembroCampania = new CampaignMember(CampaignId = campaignTest.Id, Status='Sent', LeadId = leadTest.Id);
        insert miembroCampania;
    }

    /**
     * @description constructor test
     * @author Gabriel Garcia | 08/06/2020
     * @return void
     **/
    @isTest
    private static void testConstructor() {
        final MX_BPP_PanelLeads_Ctrl instanceC = new MX_BPP_PanelLeads_Ctrl();
        System.assertNotEquals(instanceC, null, MESSAGEFAIL);
    }

    /**
     * @description
     * @author Gabriel Garcia | 08/06/2020
     * @return void
     **/
    @isTest
    private static void convertLeadTest() {
        final Lead CandidatoCamp = [SELECT Id FROM Lead LIMIT 1];
        final Map<String,Object> leadConverted = MX_BPP_PanelLeads_Ctrl.convertLead(CandidatoCamp.Id);
        System.assertEquals(leadConverted.get('Success'), null, MESSAGEFAIL);
    }

    /**
     * @description
     * @author Gabriel Garcia | 08/06/2020
     * @return void
     **/
    @isTest
    private static void getCampignMembersTest() {
        final Lead testCandidato = [SELECT Id FROM Lead LIMIT 1];
        final CampaignMember cmTestVar = MX_BPP_PanelLeads_Ctrl.getCampignMembers(testCandidato.Id);
        System.assertEquals(cmTestVar.LeadId, testCandidato.Id, MESSAGEFAIL);
    }
}