/****************************************************************************************************
Información general
------------------------
author: Omar Gonzalez
company: Ids
Project: PCI

Information about changes (versions)
-------------------------------------
Number    Dates           Author            Description
------    --------        ---------------   -----------------------------------------------------------------
1.0      07-08-2020      Omar Gonzalez      Clase que se utiliza para enviar la respuesta de cobro al cotizador embebido 
****************************************************************************************************/
@RestResource(urlMapping='/ResponseIvr/*')
global with sharing class MX_ASD_PCI_ServiceResponseIvr {
    /* Valor para Opportunity OppCobro */
    private static List<Quote> quoteCobro;
    /* Constructor privado*/
    private MX_ASD_PCI_ServiceResponseIvr() { }
    /* Clase que retorna la respuesta de cobro al cotizador */
    @HttpPost
    global static List<MX_ASD_PCI_WrapperResponseIvr.MX_ASD_PCI_ResponseCobroIvr> responseIvrCobro(MX_ASD_PCI_WrapperResponseIvr.MX_ASD_PCI_DatosQuote resCobro) {
        final List<MX_ASD_PCI_WrapperResponseIvr.MX_ASD_PCI_ResponseCobroIvr> resSFDC = new List<MX_ASD_PCI_WrapperResponseIvr.MX_ASD_PCI_ResponseCobroIvr>();
        MX_ASD_PCI_WrapperResponseIvr.MX_ASD_PCI_ResponseCobroIvr res;
        quoteCobro = MX_RTL_Quote_Service.getResponseIvr(resCobro.folioDeCotizacion,resCobro.idCotiza);
        if(quoteCobro.isEmpty()) {
            res = new MX_ASD_PCI_WrapperResponseIvr.MX_ASD_PCI_ResponseCobroIvr();
            res.descriptionCode = 'Error al buscar la cotización';
            resSFDC.add(res);
        } else {
            for(Quote quoteRes : quoteCobro) {
                res = new MX_ASD_PCI_WrapperResponseIvr.MX_ASD_PCI_ResponseCobroIvr();
                res.responseCode = quoteRes.MX_ASD_PCI_ResponseCode__c;
                res.descriptionCode = quoteRes.MX_ASD_PCI_DescripcionCode__c;
                res.msjAsesor = quoteRes.MX_ASD_PCI_MsjAsesor__c;
                res.campania = quoteRes.Opportunity.Campanya__c;
                resSFDC.add(res);
            }
        }
        return resSFDC;
    }
}