/**
 * @File Name           : MX_MC_MWPromConverter_Service.cls
 * @Description         :
 * @Author              : Jair Ignacio Gonzalez Gayosso
 * @Group               :
 * @Modification Log    :
 * =============================================================================
 * Ver          Date                     Author                     Modification
 * =============================================================================
 * 1.0      24/08/2020      Jair Ignacio Gonzalez Gayosso      Initial Version
 **/
global without sharing class MX_MC_MWPromConverter_Service extends mycn.MC_VirtualServiceAdapter { //NOSONAR

    /**
    * @Description Returns a generic Object based on method call from Class in MC component
    * @author Jair Ignacio Gonzalez Gayosso | 24/08/2020
    * @param parameters
    * @return Map<String, Object>
    **/
    global override Map<String, Object> convertMap (Map<String, Object> parameters) {
        final Map<String, Object> inputMX = new Map<String, Object>();
        inputMX.put('messageThreadId', JSON.deserializeUntyped(String.valueOf(parameters.get('threadId'))));
        inputMX.put('isDraft', 'false');
        return inputMX;
    }

    /**
    * @Description Returns a generic Object
    * @author Jair Ignacio Gonzalez Gayosso | 24/08/2020
    * @return Object
    **/
    global override Object convert(String body, String method, Map<String, Object> parameters) {

        final MX_MC_MWConverter_Service helper = new MX_MC_MWConverter_Service();
        return helper.mxToStandardCreatedConversation(body, parameters);
    }
}