@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_RTL_Matriz_Kit_Vida_mdt_Service_Test
* Autor: Juan Carlos Benitez Herrera
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_RTL_Matriz_Kit_Vida_mdt_Service

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         02/06/2020   Juan Carlos Benitez Herrera               Creación
* -----------------------------------------------------------------------------------------------
*/
public class MX_RTL_Matriz_Kit_Vida_mdt_Service_Test {
	@isTest
    static void testgetSinHData() {
        final Map<String,String> mapa= new Map<String, String>();
				mapa.put('product','Pyman');
                mapa.put('subramo','Fallecimiento');
                mapa.put('subtipo','Natural');
                mapa.put('regla1','menos de 2.5 mdp suma asegurada');
                mapa.put('regla2','mayor a 55 años');
        test.startTest();
        	MX_RTL_Matriz_Kit_Vida_mdt_Service.getSinHData(mapa);
            system.assert(true, 'Se han econtrado registros');
        test.stopTest();
    }
    @isTest
    static void testConstructor () {
        Final MX_RTL_Matriz_Kit_Vida_mdt_Service construct= new MX_RTL_Matriz_Kit_Vida_mdt_Service();
        system.assert(true,construct);
    }
}