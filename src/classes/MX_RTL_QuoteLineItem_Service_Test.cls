@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_RTL_QuoteLineItem_Service_Test
* Autor: Juan Carlos Benitez Herrera
* Proyecto: SB Presuscritos - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_RTL_QuoteLineItem_Service

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         09/07/2020    Juan Carlos Benitez Herrera              Creación
* -----------------------------------------------------------------------------------------------
*/
public class MX_RTL_QuoteLineItem_Service_Test {
    /** Test Setup CreateData*/
    @TestSetup
    static void createData() {
        MX_SB_PS_Container1_Ctrl_Test.data();
        }
    /** Test getQuoteLIbyQuote*/
	@isTest
    static void testgetQuoteLIbyQuote() {
        test.startTest();
        final Quote qtst1 = [Select id,Name from quote where MX_SB_VTS_Folio_Cotizacion__c='12345' limit 1];
        Final String value = [Select id,QuoteId,Product2Id,PricebookEntryId from QuoteLineItem where QuoteId =:qtst1.Id].QuoteId;
        	MX_RTL_QuoteLineItem_Service.getQuoteLIbyQuote(value);
        	system.assert(true,'Se ha encontrado una QuoteLineItem asociada');
        test.stopTest();
    }
}