/**
 * @File Name          : MX_SB_MLT_ValidacionDocumentos_Ctrl.cls
 * @Description        :
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 5/26/2020, 05:22:22 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/26/2020   Juan Carlos Benitez         Initial Version
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
public class MX_SB_MLT_ValidacionDocumentos_Ctrl {
    /** Lista de registros de Siniestro */
    Final static List<Siniestro__c> SIN_DATA = new List<Siniestro__c>();
    
    /** Lista de registros de Beneficiario */
    Final static List<MX_SB_VTS_Beneficiario__c> BENEF_DATA = new List<MX_SB_VTS_Beneficiario__c>();
    
    /** Lista de registros de Documentos Vida */
    final static List<MX_SB_MLT_DocumentosVida__mdt> DOCS_DATA = new List<MX_SB_MLT_DocumentosVida__mdt>();
    
    /*
    * @description METHODO busqueda siniestro vida
    * @param String siniId
    */
    @AuraEnabled
    public static List<Siniestro__c> searchSinVida (String siniId) {
            if(String.isNotBlank(siniId)) {
                final Set<Id> SinIds = new Set<Id>{siniId};
                SIN_DATA.addAll(MX_RTL_Siniestro_Service.getSiniestroData(SinIds));
            }
        return SIN_DATA;
    }

    /*
    * @description METHODO busqueda kit vida
    * @param Map<String,String> mapa
    */
    @AuraEnabled
		public static List < MX_SB_MLT_DocumentosVida__mdt > searchkit(Map<String,String> mapa) {
                 final List<MX_SB_MLT_Matriz_Kit_Vida__mdt> kit = MX_RTL_Matriz_Kit_Vida_mdt_Service.getSinHData(mapa);
                 DOCS_DATA.addAll(MX_RTL_DocumentosVida_mdt_Service.getDocsData(kit[0].MX_SB_MLT_Kit__c));
            return DOCS_DATA;
    }
    /*
    * @description METHODO obtiene el Kit
    * @param Map<String,String> mapa
    * return string Kit
    */
    @AuraEnabled
    public static String kit(Map<String,String> mapa) {
            final List<MX_SB_MLT_Matriz_Kit_Vida__mdt> kit = MX_RTL_Matriz_Kit_Vida_mdt_Service.getSinHData(mapa);
	        return kit[0].MX_SB_MLT_Kit__c;
    }
    /*
    * @description METHODO actualiza el siniestro
    * @param Siniestro__c sin
    */
    @AuraEnabled
    public static void updateSin(Siniestro__c sin) {
        MX_RTL_Siniestro_Service.actualizarSin(sin);
    }
    
    /*
    * @description Acciones flujo vida
    * @param String sinId
    */
    @AuraEnabled
    public static void siniestroAction(String siniestroAct, String sinId) {
            if(String.isNotBlank(siniestroAct) && String.isNotBlank(sinId)) {
                final Set<Id> sinIds = new Set<Id>{sinId};
                MX_RTL_Siniestro_Service.sinAction(siniestroAct,sinIds);
            }
	}

    /*
    * @description Busqueda de Beneficiarios Asociados a la poliza
    * @param String contractId
    */
    @AuraEnabled
    public static List <MX_SB_VTS_Beneficiario__c> benefactor(String contractId) {
            if(String.isNotBlank(contractId)) {
                final Set<Id> contIds = new Set<Id>{contractId};
                BENEF_DATA.addAll(MX_RTL_Beneficiario_Service.getBenefHData(contIds));
            }
        return BENEF_DATA;
    }
	/*
    * @description Actualizacion de beneficiario
    * @param String data
    * @param String recId
    */
    @AuraEnabled
    public static void updtBenef(List<MX_SB_VTS_Beneficiario__c> data, String recId) {
        if(String.isNotBlank(recId)) {
                final Set<Id> SinIds = new Set<Id>{recId};
                SIN_DATA.addAll(MX_RTL_Siniestro_Service.getSiniestroData(SinIds));
            }
        if(!data.isEmpty()) {
                BENEF_DATA.addAll(MX_RTL_Beneficiario_Service.getBenefDataById(data));
            }
		for(MX_SB_VTS_Beneficiario__c obj : BENEF_DATA ) {
            obj.MX_SB_MLT_Q_EnvioKitCorreo__c = 'Si';
            obj.MX_SB_MLT_Kit__c = SIN_DATA[0].MX_SB_MLT_KitVida__c;
        }
          if(!BENEF_DATA.isEmpty() && SIN_DATA[0].MX_SB_MLT_Q_EnvioCorreo__c!='Si') {
			SIN_DATA[0].MX_SB_MLT_Q_EnvioCorreo__c='Si';
            MX_RTL_Beneficiario_Service.updateBenefData(BENEF_DATA);
            MX_RTL_Siniestro_Service.upsertSini(SIN_DATA[0]);
        }
    }
}