/**
* @File Name          : MX_SB_PS_Resumen_Cotizacion_Ctrl.cls
* @Description        :
* @Author             : Juan Carlos Benitez
* @Group              :
* @Last Modified By   : Daniel Perez Lopez
* @Last Modified On   : 12-16-2020
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    9/04/2020   Juan Carlos Benitez          Initial Version
* 1.1    12/16/2020  Daniel Perez Lopez           Se añade methodo getDocuments
* 1.2    2/09/2021   Juan Carlos Benitez          Se añade campo Quote.OpportunityId
* 1.3    3/18/2021   Juan Carlos Benitez          Se desancla envío de caratula para continuar emision
**/
@SuppressWarnings('sf:UseSingleton,sf:DUDataflowAnomalyAnalysis')
public class MX_SB_PS_Resumen_Cotizacion_Ctrl {
    /** list of records to be retrieved */
    final static List<Account> ACC_DATA = new List<Account>();
    /** list of records to be retrieved */
    final static List<Opportunity> OPP_DATA = new List<Opportunity>();
    /** list of records to be retrieved */
    final static List<Contact> CNTCT_DATA = new List<Contact>();
    /** Campos de beneficiario **/
    Final static String QUOTLINEFIELDS = ' id,MX_SB_PS_Dias36omas__c, MX_SB_PS_Dias3a5__c, MX_SB_PS_Dias6a35__c, MX_SB_PS_SumaAsegurada__c, quote.MX_SB_PS_PagosSubsecuentes__c, MX_SB_VTS_Total_Gastos_Funerarios__c, MX_SB_VTS_FormaPAgo__c, MX_SB_VTS_Plan__c,QuoteId,UnitPrice, Quantity, MX_SB_VTS_Tipo_Plan__c ';
    /** Campos de CONDITION1 **/
    Final static String  CONDITION1 ='QuoteId';
    /** Campos de CONDITION1 **/
    final static List<Contract> CON_DATA = new List<Contract>();
    /** Dato para validacion estatuscode */
    final static integer OKSTAT =200;
    /** Dato para validacion estatuscode */
    final static String SUCCESSTAT='SUCCESS';
/**
* @description
* @author Juan Carlos Benitez | 9/05/2020
* @param oppId
* @return List<Opportunity>
**/
    @AuraEnabled
    public static Opportunity loadOppF(String oppId) {
        return MX_SB_PS_Utilities.fetchOpp(oppId);
    }
/**
* @description
* @author Juan Carlos Benitez | 9/07/2020
* @param string value
* @return List<QuoteLineItem>
**/
    
  @AuraEnabled
    public static List<QuoteLineItem> loadQuoteLineItem(String value) {
        return MX_RTL_QuoteLineItem_Service.getQuoteLIbyQuote(value);
    }

/**
* @description
* @author Juan Carlos Benitez | 9/08/2020
* @param oppId
* @return List<Contact>
**/
    @AuraEnabled
    public static List<Contact> loadContactAcc(Id accId) {
        if(String.isNotBlank(accId)) {
            final Set<Id> ides = new Set<Id>{accId};
                CNTCT_DATA.addAll(MX_RTL_Contact_Service.getContactData(ides));
        }
        return CNTCT_DATA;
    }
/**
* @description
* @author Juan Carlos Benitez | 9/07/2020
* @param recId
* @return List<Account>
**/
    @AuraEnabled
    public static list<Account> loadAccountF(String recId) {
        if(String.isNotBlank(recId)) {
            final Set<Id> ides = new Set<Id>{recId};
                ACC_DATA.addAll(MX_RTL_Account_Service.getSinHData(ides));
        }
        return ACC_DATA;
    }
/**
* @description
* @author Gerardo Mendoza | 07/10/2020
* @param idOpp
* @return List<Contract>
**/
    @AuraEnabled
    public static list<Contract> getContractData(String idOpp) {
        if(String.isNotBlank(idOpp)) {
            CON_DATA.addAll(MX_RTL_Contract_Service.getContractIdByOppId(idOpp));
        }
        return CON_DATA;
    
    }
    
    /*
    * @description obtiene los planes por producto de clipert
    * @param areacode
    * @return String
    */
    @AuraEnabled(cacheable=true)
    public static  String emiteQuoteNoCobro(String opp) {
        HttpResponse resq = new HttpResponse();
        Dtresp dtres = new Dtresp();
        final List<QuoteLineItem> listqtl = MX_RTL_QuoteLineItem_Selector.getQuoteLineitemBy('OpportunityId',' id ,createddate,UnitPrice,MX_SB_VTS_Tipo_Plan__c,MX_SB_PS_Dias3a5__c,MX_SB_PS_Dias6a35__c,MX_SB_PS_Dias36omas__c,MX_SB_VTS_Total_Gastos_Funerarios__c,MX_SB_VTS_FormaPAgo__c,MX_WB_noPoliza__c ,MX_WB_Folio_Cotizacion__c,Quote.OpportunityId ',opp);
        final String folioq = listqtl[0].MX_WB_Folio_Cotizacion__c.Split(',')[0];
        try {
            resq = MX_SB_PS_IASO_Service_cls.getServiceResponse('GetContratacionSinCobro_PS','{"idcotiza":"'+folioq+'"}');
            dtres = (Dtresp)JSON.deserialize(resq.getBody(),Dtresp.class);
            if ( resq.getStatusCode()==OKSTAT) {
                getDocuments(opp,dtres.data.policyNumber);
            }
        } catch (Exception e) {
            resq = MX_SB_PS_IASO_Service_cls.getServiceResponse('GetContratacionSinCobro_PS','{"idcotiza":"'+folioq+'"}');
            dtres = (Dtresp)JSON.deserialize(resq.getBody(),Dtresp.class);
            if ( resq.getStatusCode()!=OKSTAT) {
                throw new AuraHandledException(System.Label.MX_SB_PS_ErrorGenerico+' : ' +e);                
            }
        }
        return dtres.data.policyNumber;
    }

    /*
    * @description obtiene los planes por producto de clipert
    * @param areacode
    * @return String
    */
    @AuraEnabled(cacheable=true)
    public static  String getDocuments(String oppid,String idpoliza) {
        Quote quote = MX_RTL_Quote_Selector.getQuote(new Quote(OpportunityId=oppid))[0];
        quote = MX_RTL_Quote_Selector.findQuoteById(quote.id, ' id,Account.Name,Account.PersonContact.Email,Account.RFC__c,Account.Personemail,MX_SB_VTS_TotalMonth__c,MX_SB_PS_PagosSubsecuentes__c,MX_SB_VTS_ASO_FolioCot__c ');
        final List<QuoteLineItem> listqtl = MX_RTL_QuoteLineItem_Selector.getQuoteLineitemBy('OpportunityId','id ,UnitPrice,createddate,MX_SB_VTS_Tipo_Plan__c,MX_SB_PS_Dias3a5__c,MX_SB_PS_Dias6a35__c,MX_SB_PS_Dias36omas__c,MX_SB_VTS_Total_Gastos_Funerarios__c,MX_SB_VTS_FormaPAgo__c,MX_WB_noPoliza__c ,MX_WB_Folio_Cotizacion__c ',oppid);
        HttpResponse htpr = new HttpResponse();
        String statusCodes= '';
        try {
            htpr = MX_SB_PS_IASO_Service_cls.getServiceResponse('GetCustomerDocument_PS','{"vidse":"VIDSE","quoteid":"'+listqtl[0].MX_WB_Folio_Cotizacion__c+'"}');
            final wrpDocuments mapdata2 = (wrpDocuments) Json.deserialize(htpr.getBody(),wrpDocuments.class);
            Final Map<String, String> mpd = MX_SB_PS_Resumen_Cotizacion_Helper.mapdata(mapdata2, oppid, idpoliza);
            try {
                MX_SB_PS_IASO_Service_cls.getServiceResponseMap('SendDocumentByEmailEmitido_PS',mpd);
            } catch (Exception e) {
                throw new AuraHandledException(System.Label.MX_SB_PS_ErrorGenerico+' : ' +e);
            }    
        } catch(Exception e) {
            statusCodes='404';
            throw new AuraHandledException(System.Label.MX_SB_PS_ErrorGenerico+' : ' +e);
        }
        return statusCodes;
    }
    /**
    * @description 
    * @author Arsenio.perez.lopez.contractor@bbva.com | 11-12-2020 
    * @param String oppid 
    * @return String 
    **/
    @AuraEnabled
    public static String getCloneOpp(String oppid, String product) {
        return MX_SB_PS_Resumen_Cotizacion_Helper.sCloneOpps(oppid,product);
    }       
    /**wrapper para respuesta de servicio */
    public class wrpDocuments {
        /**variable para wraper de servicios*/
        public wrpinfo document {get;set;}
        /**variable para wraper de servicios*/
        public String href {get;set;}
    }
    /**wrapper para respuesta de servicio */
    public class wrpinfo {
        /**variable para wraper de servicios*/
        public String href {get;set;}
        /**variable para wraper de servicios*/
        public String data {get;set;}
    }
    /**wrapper para respuesta de servicio */
    public class Dtresp {
        /**variable para wraper de servicios*/
        public datas data {get;set;}
    }
    /**wrapper para respuesta de servicio */
    public class datas {
        /**variable para wraper de servicios*/
        public String policyNumber {get;set;}
    }

}