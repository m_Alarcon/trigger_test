/*
*
* @author Tania Vazquez
* @description This class will be used to test MX_SB_BCT_BanqueroTriggerHandler
*
*           No  |     Date     |     Author      |    Description
* @version  1.0    08/10/2019     Tania Vazquez     Create Banquero__c, cuentas
*
*/
@isTest
public class MX_SB_BCT_BanqueroTriggerHandler_Test {
    @testSetup
    static void testData() {
		final User admin = MX_WB_TestData_cls.crearUsuario('admin hogar', system.label.MX_SB_VTS_ProfileAdmin);
        Insert admin;
        final User admin3 = MX_WB_TestData_cls.crearUsuario('admin hogar3', system.label.MX_SB_VTS_ProfileAdmin);
        admin3.FederationIdentifier='MX000012';
        Insert admin3;
		final User admin2 = MX_WB_TestData_cls.crearUsuario('admin hogar2', system.label.MX_SB_VTS_ProfileAdmin);
        admin2.FederationIdentifier='MX000011';
        Insert admin2;
        System.runAs(admin) {
        MX_WB_TestData_cls.createStandardPriceBook2();
            final Product2 producto = MX_WB_TestData_cls.productNew('Hogar');
            producto.isActive = true;
            Insert producto;
            final PricebookEntry pbe = MX_WB_TestData_cls.priceBookEntryNew(producto.Id);
            Insert pbe;
            final Account acc = MX_WB_TestData_cls.crearCuenta('Doe', 'MX_WB_rt_PAcc_Telemarketing');
            acc.MX_SB_BCT_Id_Cliente_Banquero__c ='991';
            Insert acc;
            final Opportunity opp = MX_WB_TestData_cls.crearOportunidad('OppTest Hogar', acc.Id, admin.Id, 'ASD');
            opp.Producto__c = 'Hogar';
            opp.Reason__c = 'Venta';
            opp.MX_SB_VTS_Aplica_Cierre__c = true;
            final Contract contr = MX_WB_TestData_cls.vtsCreateContract(acc.Id, admin.Id, producto.Id);
            Insert contr;
			Insert opp;
            final MX_SB_VTS_Generica__c generica02 = MX_WB_TestData_cls.GeneraGenerica('MX_SB_BCT_Baja_Banquero_Default', 'CP5');
            generica02.MX_SB_BCT_user_Banquero_Default__c=admin3.FederationIdentifier;
            insert generica02;
			final MX_SB_VTS_Generica__c generica = MX_WB_TestData_cls.GeneraGenerica('MX_SB_BCT_user_Banquero', 'CP5');
            generica.MX_SB_BCT_user_Banquero_Default__c=admin2.FederationIdentifier;
            insert generica;
      }
    }
    
    @isTest static void TestValues() {
        Test.startTest();
            final User admin = MX_WB_TestData_cls.crearUsuario('test hogar2', system.label.MX_SB_VTS_ProfileAdmin);
            Insert admin;
            final Opportunity idcompara = [select id, OwnerId from Opportunity where name='OppTest Hogar'];
            System.runAs(admin) {
            final List<Banquero__c> ban = new List<Banquero__c>();
            final Banquero__c banque = new Banquero__c(MX_SB_BCT_Id_Banquero__c='990099',MX_SB_BCT_Id_Cliente__c='991');
            final Banquero__c banque1 = new Banquero__c(MX_SB_BCT_Id_Banquero__c='1234',MX_SB_BCT_Id_Cliente__c='7');
			ban.add(banque);
            ban.add(banque1);
            final Banquero__c banque2 = new Banquero__c(MX_SB_BCT_Id_Banquero__c='1234',MX_SB_BCT_Id_Cliente__c='5');
            final Banquero__c banque3 = new Banquero__c(MX_SB_BCT_Id_Banquero__c='1234',MX_SB_BCT_Id_Cliente__c='878');
			ban.add(banque2);
            ban.add(banque3);
            insert ban;
            final Opportunity idcomparafuture = [select id, OwnerId from Opportunity where name='OppTest Hogar'];  
            system.assertNotEquals(idcomparafuture.OwnerId, idcompara.OwnerId, 'Flujo Correcto');
        Test.stopTest();
        }
    }
     @isTest static void TestupdAccQuickActio() {
        Test.startTest();
         String result2;
             try {
                  result2 = MX_SB_BCT_UpdateAccountcla.UpdateAcc('123');
             } catch(System.AuraHandledException e) {
                  system.assert(String.isEmpty(result2),'Funcionó');
             }
		Test.stopTest();
     }
             
}