/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RTL_Document_Selector
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2021-01-14
* @Description 	Selector for Documents Object
* @Changes
*  
*/
public without sharing class MX_RTL_Document_Selector {
    
    /** Constructor */
    @TestVisible
    private MX_RTL_Document_Selector() {}
    
    /**
    * @description
    * @author Edmundo Zacarias Gómez
    * @param String queryfields, List<Id> idsCampaigMembers
    * @return List<CampaignMember>
    **/
    public static List<Document> getDocuments(String queryfield,String conditionField, boolean whitCondition, List<String> listString) {
        List<Document> resultQueryD;
        String query;
        switch on String.valueOf(whitCondition) {
            when 'false' {
                query='SELECT '+ queryfield +' FROM Document';
            }
            when 'true' {
                query='SELECT '+ queryfield +' FROM Document WHERE '+conditionField;
            }
        }
        resultQueryD = Database.query(String.escapeSingleQuotes(query));
        return resultQueryD;
    }

}