/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_CatalogoMinutas_Service_Helper
* @Author   	Héctor Saldaña | hectorisrael.saldana.contractor@bbva.com
* @Date     	Created: 2020-03-08
* @Description 	MX_BPP_CatalogoMinutas Service Helper
* @Changes
*
*/
public without sharing class MX_BPP_CatalogoMinutas_Service_Helper {
    /** List with valid string tags */
    final private static List<String> LST_TAGS = new List<String> {'OpcionalTexto', 'Switch','Seleccion', 'Opcion'};
	private MX_BPP_CatalogoMinutas_Service_Helper() {}
    /**
    * @Description 	Check the syntax for those dynamic tags
    * @Patams		List<MX_BPP_CatalogoMinutas__c> lstCatalogos
    * @Return 		NA
    **/
    public static void validateOpTagSyntax(List<MX_BPP_CatalogoMinutas__c> lstCatalogos) {
		for(MX_BPP_CatalogoMinutas__c catalogo : lstCatalogos) {
			final Pattern regexPat = Pattern.compile('(([{]*)([a-zA-Z]+)(\\d*)([}]*))([^{}][a-zA-ZÀ-ÿ\\u00f1\\u00d1\\s:,]*)*((([{])[a-zA-Z]*(\\d+)*([}]))(.*?)(([{])[a-zA-Z]*(\\d+)*([}])))*(([{]*)([a-zA-Z]+)(\\d*)([}]*))');
			final Matcher regexMat = regexPat.matcher(catalogo.MX_Opcionales__c);

			while(regexMat.find()) {
				if(wrongTag(regexMat)) {
					catalogo.addError(System.Label.BPP_Minuta_Error_Etiqueta);
					break;
				}
				if(!catalogo.MX_Contenido__c.contains(regexMat.group(1))) {
					catalogo.addError(System.Label.BPP_Minuta_Error_Etiqueta_NoUsada);
					break;
				}
                if(!LST_TAGS.contains(regexMat.group(3)) || !LST_TAGS.contains(regexMat.group(19))) {
                    catalogo.addError(System.Label.BPP_Minuta_Error_Etiqueta_NoValida);
					break;
                }
				if(wrongSelectionTag(regexMat)) {
					catalogo.addError(System.Label.BPP_Minuta_Error_Etiqueta_Seleccion);
					break;
				}

			}
            updateCustomTagFlags(catalogo);
		}
	}
    
    /**
    * @Description 	Checks the syntax and conditions for picklist tags used
    * @Params 		String seleccionStr
    * @Return 		NA
    **/
    private static Boolean validatePicklist(String seleccionStr) {
		final Pattern regexOps = Pattern.compile('([{]+([a-zA-Z]*)(\\d)*[}]+)(.*?)([{]+([a-zA-Z]*)(\\d)*[}]+)');
		final Matcher regexMat = regexOps.matcher(seleccionStr);
		Boolean result = false;
		while(regexMat.find()) {
			result = validateRegexPickList(regexMat);
			if(result) {
				break;
			}
		}

		return result;
	}    
    
    /**
    * @Description 	Update the value of the flags according to the internal tags
    * @Params 		MX_BPP_CatalogoMinutas__c catalogo
    * @Return 		NA
    **/
    public static void updateCustomTagFlags(MX_BPP_CatalogoMinutas__c catalogo) {
        catalogo.MX_HasText__c = catalogo.MX_Opcionales__c.contains(LST_TAGS[0]);
        catalogo.MX_HasCheckbox__c = catalogo.MX_Opcionales__c.contains(LST_TAGS[1]);
        catalogo.MX_HasPicklist__c = catalogo.MX_Opcionales__c.contains(LST_TAGS[2]);
    }
	
    /**
    * @Description 	Check the syntax for those static tags using records from MX_BPP_GlosarioMinuta__mdt
    * @Param		List<MX_BPP_CatalogoMinutas__c> lstCatalogos
    * @Return 		NA
    **/
    public static void validateStTagSyntax(List<MX_BPP_CatalogoMinutas__c> lstCatalogos) {
        final List<MX_BPP_GlosarioMinuta__mdt> lstGlosario = MX_BPP_GlosarioMinuta_Selector.getAllRecords('DeveloperName');
        final Pattern tagPattern = Pattern.compile('(\\{+([a-zA-Z]+)\\}+)');
        Matcher tagMatcher;
        final Set<String> setTags = new Set<String>();
        for (MX_BPP_GlosarioMinuta__mdt glosario : lstGlosario) {
            setTags.add(glosario.DeveloperName);
        }
        setTags.add('Acuerdos');
        setTags.add('FechaAcuerdos');
        
		for(MX_BPP_CatalogoMinutas__c catalogo : lstCatalogos) {
            tagMatcher = tagPattern.matcher(catalogo.MX_Saludo__c + ' ' + catalogo.MX_Contenido__c + ' ' + catalogo.MX_Despedida__c + ' ' + catalogo.MX_Firma__c + ' ' + catalogo.MX_CuerpoCorreo__c);
            while(tagMatcher.find()) {
                if(!setTags.contains(tagMatcher.group(2))) {
                    catalogo.addError(System.Label.BPP_Minuta_Error_Etiqueta_NoValida);
                }
            }
        }
    }
    
    /**
    * @Description 	Clean the values from fields calling functions
    * @Param		MX_BPP_CatalogoMinutas__c catalog
    * @Return 		NA
    **/
    public static void cleanTextFields(MX_BPP_CatalogoMinutas__c catalogo) {
        catalogo.MX_Saludo__c = MX_BPP_Minuta_Utils.cleanValue(catalogo.MX_Saludo__c);
        catalogo.MX_Despedida__c = MX_BPP_Minuta_Utils.cleanValue(catalogo.MX_Despedida__c);
        catalogo.MX_Firma__c = MX_BPP_Minuta_Utils.cleanValue(catalogo.MX_Firma__c);
        catalogo.MX_Contenido__c = MX_BPP_Minuta_Utils.cleanValue(catalogo.MX_Contenido__c);
        catalogo.MX_CuerpoCorreo__c = MX_BPP_Minuta_Utils.cleanValue(catalogo.MX_CuerpoCorreo__c);
    }

	/**
    * @Description 	Evaluates regex composition received in inner tags in Picklist values
    * @Param		Matcher regex
    * @Return 		Boolean
    **/
	private static Boolean validateRegexPickList(Matcher regex) {
		final Set<String> setTags = new Set<String>{'Opcion'};
		Boolean result = false;
		final Boolean notEquals = !regex.group(1).equals(regex.group(5));
		final Boolean noValue = String.isBlank(regex.group(4));
		final Boolean noNumbers = String.isBlank(regex.group(3)) || String.isBlank(regex.group(7));
		final Boolean noValidTags = !setTags.contains(regex.group(2)) || !setTags.contains(regex.group(6));
		result = notEquals || noValue || noNumbers || noValidTags;

		return result;
	}

	/**
    * @Description 	Evaluates regex composition received in inner tags in regards to existing tags
    * @Param		Matcher regex
    * @Return 		Boolean
    **/
	private static Boolean wrongTag(Matcher regex) {
		final Boolean notEquals = !regex.group(1).equals(regex.group(17));
		final Boolean noValue = String.isBlank(regex.group(6));
		final Boolean noNumber = String.isBlank(regex.group(4)) || String.isBlank(regex.group(20));
		final Boolean missingBrackets = !regex.group(2).contains('{') || !regex.group(5).contains('}') || !regex.group(18).contains('{') || !regex.group(21).contains('}');
		return notEquals || noValue || noNumber || missingBrackets;
	}

	/**
    * @Description 	Evaluates regex composition received in inner tags in Picklist-Options values
    * @Param		Matcher regex
    * @Return 		Boolean
    **/
	private static Boolean wrongSelectionTag(Matcher regex) {
		final Boolean validTag = regex.group(3).equals('Seleccion') && String.isBlank(regex.group(7));
		Boolean wrongValues = false;
		if(String.isNotBlank(regex.group(7))) {
			wrongValues = validatePickList(regex.group(0).replace(regex.group(1), ''));
		}
		return validTag || wrongValues;
	}

	/**
    * @Description 	Evaluates if any field of Catalogo record changed
    * @Params		MX_BPP_CatalogoMinutas__c newCatalogo, MX_BPP_CatalogoMinutas__c oldCatalogo
    * @Return 		Boolean anychange
    **/
	public static Boolean checkBodyTemplate(MX_BPP_CatalogoMinutas__c newCatalogo, MX_BPP_CatalogoMinutas__c oldCatalogo) {
		Boolean anychange = false;
		final Boolean bodyChange = MX_BPP_Minuta_Utils.hasChanged(oldCatalogo.MX_CuerpoCorreo__c, newCatalogo.MX_CuerpoCorreo__c);
		final Boolean contenidoChange = MX_BPP_Minuta_Utils.hasChanged(oldCatalogo.MX_Contenido__c, newCatalogo.MX_Contenido__c);
		final Boolean despedidaChange = MX_BPP_Minuta_Utils.hasChanged(oldCatalogo.MX_Despedida__c, newCatalogo.MX_Despedida__c);
		final Boolean saludoChange = MX_BPP_Minuta_Utils.hasChanged(oldCatalogo.MX_Saludo__c, newCatalogo.MX_Saludo__c);
		final Boolean firmaChange = MX_BPP_Minuta_Utils.hasChanged(oldCatalogo.MX_Firma__c, newCatalogo.MX_Firma__c);

		anychange = bodyChange || contenidoChange || despedidaChange || saludoChange || firmaChange;

		return anychange;
	}

}