/**
 * @description       : Class test MX_SB_SAC_Entitlement_Service
 * @author            : Gerardo Mendoza Aguilar
 * @group             : 
 * @last modified on  : 03-05-2021
 * @last modified by  : Gerardo Mendoza Aguilar
 * Modifications Log 
 * Ver   Date         Author                    Modification
 * 1.0   03-04-2021   Gerardo Mendoza Aguilar   Initial Version
**/
@isTest
public with sharing class MX_SB_SAC_Entitlement_Service_Test {
    /** Account string name */
    final static String ACCOUNTNAME = 'Test';
    /** Entitlement string type*/
    final static String TYPEENTI = 'Asistencia por Internet';
    
    /**
    * @description data Setup
    * @author Gerardo Mendoza Aguilar | 03-05-2021 
    **/
    @TestSetup
    static void makeData() {
        final Account acc = MX_WB_TestData_cls.createAccount(ACCOUNTNAME, System.Label.MX_SB_VTS_PersonRecord);
        insert acc;
    }
    /**
    * @description 
    * @author Gerardo Mendoza Aguilar | 03-05-2021 
    **/
    @isTest
    public static void insertEntitlement() {
        Test.startTest();
        final Account resultAcc = [Select Id, Name From Account limit 1];
        final Entitlement ent1 = new Entitlement(Type = TYPEENTI, Name = resultAcc.Name, AccountId = resultAcc.Id);
        final List<Entitlement> entitlementList1 = new List<Entitlement>();
        entitlementList1.add(ent1);
        final List<Entitlement> insertResult = MX_SB_SAC_Entitlement_Service.insertEntitlement(entitlementList1);
        System.assertEquals(insertResult[0].Name, 'Test Test', 'Registro creado');
        Test.stopTest();
    }
}