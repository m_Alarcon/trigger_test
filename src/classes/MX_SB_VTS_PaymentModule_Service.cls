/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 10-30-2020
 * @last modified by  : Eduardo Hernandez Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   08-27-2020   Eduardo Hernández Cuamatzi   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_SB_VTS_PaymentModule_Service {

    /**Campos basicos de generica */
    final static String LSTFIELDS = 'Id, Name, MX_SB_VTS_Description__c, MX_SB_SAC_ProductFam__c';
    /**Campos basicos de metadata msjs IVR */
    final static String LSTFIELDSIVR = 'Id, MasterLabel, MX_RTL_IVRCode__c, MX_RTL_Group__c, MX_RTL_MsgIVR__c, MX_RTL_MsgSF__c, MX_RTL_TitleMsg__c';
    /**Campos basicos de Oportunidad */
    final static String FIELDSOPP = 'Id, Name, Producto__c, TotalOpportunityQuantity, SyncedQuoteId, Monto_de_la_oportunidad__c';
    /**Campos TOTALPAY de Oportunidad */
    final static String TOTALPAY = 'Total';
    /**Campos YearlyTotal de Oportunidad */
    final static String TOTALYEARLY = 'TotalAnual';


    /**
    * @description Crea Lista de valores de pago
    * @author Eduardo Hernandez Cuamatzi | 10-02-2020 
    * @param String oppId Id de registro de Oportunidad
    * @return List<MX_SB_VTS_PaymentModule_Ctrl.paymentResum> Resumen de pago
    **/
    public static List<MX_SB_VTS_PaymentModule_Ctrl.paymentResum> fillDataPayment(String oppId) {
        List<MX_SB_VTS_PaymentModule_Ctrl.paymentResum> lstPayments = new List<MX_SB_VTS_PaymentModule_Ctrl.paymentResum>();
        final Opportunity oppData = MX_RTL_Opportunity_Selector.getOpportunity(oppId, FIELDSOPP);
        if (String.isNotBlank(oppData.Id) && String.isNotBlank(oppData.SyncedQuoteId)) {
            final String quoteFields = fillQueryQuote(', MX_SB_VTS_Nombre_Contrante__c, MX_SB_VTS_Apellido_Paterno_Contratante__c, MX_SB_VTS_Apellido_Materno_Contratante__c');
            final Quote quoteData = MX_RTL_Quote_Selector.findQuoteById(oppData.SyncedQuoteId, quoteFields);
            final Set<Id> lstQuotes = new Set<Id>{quoteData.Id};
            final List<QuoteLineItem> lstQuoli = MX_RTL_QuoteLineItems_Selector.selSObjectsByQuotes(lstQuotes);
            lstPayments = fillListPayment(oppData, quoteData, lstQuoli[0]);
        }
        return lstPayments;
    }

    /**
    * @description LLena valores de pagos
    * @author Eduardo Hernandez Cuamatzi | 10-02-2020 
    * @param Opportunity oppData Datos Oportunidad
    * @param Quote quoteData Datos Cotizacion
    * @param QuoteLineItem quoli Datos pago cotización
    * @return List<MX_SB_VTS_PaymentModule_Ctrl.paymentResum> Estructura pago
    **/
    public static List<MX_SB_VTS_PaymentModule_Ctrl.paymentResum> fillListPayment(Opportunity oppData, Quote quoteData, QuoteLineItem quoli) {
        final List<MX_SB_VTS_PaymentModule_Ctrl.paymentResum> lstPayments = new List<MX_SB_VTS_PaymentModule_Ctrl.paymentResum>();
        final String lstPaysStr = String.valueOf(quoteData.MX_SB_VTS_GCLID__c);
        final List<String> lstPays = lstPaysStr.split('\\|', 5);
        final Map<String, String> fillFrecueMap = fillFrecuencies(quoteData, quoli, lstPays);
        lstPayments.add(fillItem('frecuencia','Frecuencia de pago', MX_SB_VTS_rtwCotFillData.validEmptyStr(fillFrecueMap.get('frecc'))));
        lstPayments.add(fillItem('pagos','Número de pagos y monto', MX_SB_VTS_rtwCotFillData.validEmptyStr(fillFrecueMap.get(TOTALPAY))));
        lstPayments.add(fillItem('totalPagar','Total a pagar', MX_SB_VTS_rtwCotFillData.validEmptyStr(fillFrecueMap.get(TOTALYEARLY))));
        final String productUper = oppData.Producto__c.toUpperCase();
        switch on productUper {
            when 'HOGAR SEGURO DINÁMICO' {
                final String billingAddress = fillAddress(quoteData, 'Domicilio asegurado');
                lstPayments.add(fillItem('direcInm','Dirección del inmueble a asegurar',MX_SB_VTS_rtwCotFillData.validEmptyStr(String.valueOf(billingAddress))));
                lstPayments.add(fillItem('cobertura','Tipo construcción', 'Construcción y contenidos'));
            }
            when 'VIDA SEGURA DINÁMICO' {
                lstPayments.add(fillItem('direcInm','Nombre del cliente', MX_SB_VTS_rtwCotFillData.validEmptyStr(quoteData.MX_SB_VTS_Nombre_Contrante__c)+ ' '+ 
                MX_SB_VTS_rtwCotFillData.validEmptyStr(quoteData.MX_SB_VTS_Apellido_Paterno_Contratante__c) + ' '+ MX_SB_VTS_rtwCotFillData.validEmptyStr(quoteData.MX_SB_VTS_Apellido_Materno_Contratante__c)));
                lstPayments.add(fillItem('cobertura','', ''));
            }
        }
        lstPayments.add(fillItem('correoEmail','Correo electrónico', MX_SB_VTS_rtwCotFillData.validEmptyStr(quoteData.MX_SB_VTS_Email_txt__c)));
        lstPayments.add(fillItem('sumaAseg','Suma asegurada', MX_SB_VTS_rtwCotFillData.validEmptyStr(String.valueOf(oppData.Monto_de_la_oportunidad__c))));
        return lstPayments;
    }

    /**
    * @description LLena lista de pagos
    * @author Eduardo Hernández Cuamatzi | 03-11-2021 
    * @param quoteData Datos de cotizacion
    * @param quoli datos de partida
    * @return Map<String, String> 
    **/
    public static Map<String, String> fillFrecuencies(Quote quoteData, QuoteLineItem quoli, List<String> lstPays) {
        final Map<String, String> frecuencies = new Map<String, String>();
        frecuencies.put('frecc', quoli.MX_SB_VTS_Frecuencia_de_Pago__c);
        switch on quoli.MX_SB_VTS_Frecuencia_de_Pago__c {
            when  'Mensual' {
                frecuencies.put(TOTALPAY, lstPays[3]);
                frecuencies.put(TOTALYEARLY, String.valueOf(quoteData.MX_SB_VTS_TotalMonth__c));
            }
            when 'Trimestral' {
                frecuencies.put(TOTALPAY, lstPays[2]);
                frecuencies.put(TOTALYEARLY, String.valueOf(quoteData.MX_SB_VTS_TotalQuarterly__c));
            }
            when 'Semestral' {
                frecuencies.put(TOTALPAY, lstPays[1]);
                frecuencies.put(TOTALYEARLY, String.valueOf(quoteData.MX_SB_VTS_TotalSemiAnual__c));
            }
            when 'Anual' {
                frecuencies.put(TOTALPAY, lstPays[0]);
                frecuencies.put(TOTALYEARLY, String.valueOf(quoteData.MX_SB_VTS_TotalAnual__c));
            }
        }
        return frecuencies;
    }

    /**
    * @description Elemento de pago
    * @author Eduardo Hernandez Cuamatzi | 10-02-2020 
    * @param String idValue Llave del elemento
    * @param String textVal texto a mostrar
    * @param String dataValue Valor a mostrar
    * @return MX_SB_VTS_PaymentModule_Ctrl.paymentResum Data de pago
    **/
    public static MX_SB_VTS_PaymentModule_Ctrl.paymentResum fillItem(String idValue, String textVal, String dataValue) {
        final MX_SB_VTS_PaymentModule_Ctrl.paymentResum itemResum = new MX_SB_VTS_PaymentModule_Ctrl.paymentResum();
        itemResum.idValue = idValue;
        itemResum.textVal = textVal;
        itemResum.dataValue = dataValue;
        return itemResum;
    }

    /**
    * @description Concatena datos de dirección
    * @author Eduardo Hernandez Cuamatzi | 10-02-2020 
    * @param Quote quoteData Quote a Validar
    * @return String Direccion concatenada
    **/
    public static String fillAddress(Quote quoteData, String addresType) {
        final Set<String> setParentId = new Set<String>{quoteData.Id};
        final Set<String> addresTypes = new Set<String>{addresType};
        final MX_RTL_MultiAddress__c lstAddress = MX_RTL_MultiAddress_Selector.findAddress(setParentId, addresTypes)[0];
        String addressStr = '';
        addressStr += lstAddress.MX_RTL_AddressStreet__c;
        if(String.isNotBlank(MX_SB_VTS_rtwCotFillData.validEmptyStr(lstAddress.MX_RTL_AddressExtNumber__c))) {
            addressStr += ' Exterior: '+lstAddress.MX_RTL_AddressExtNumber__c;
        }
        if(String.isNotBlank(MX_SB_VTS_rtwCotFillData.validEmptyStr(lstAddress.MX_RTL_AddressIntNumber__c)) && lstAddress.MX_RTL_AddressIntNumber__c != 'N/A') {
            addressStr += ', Interior: '+lstAddress.MX_RTL_AddressIntNumber__c;
        }
        addressStr += ' '+lstAddress.MX_RTL_AddressCity__c + ', CP: '+lstAddress.MX_RTL_PostalCode__c;

        return addressStr;
    }

    /**
    * @description Recupera Quote para pago
    * @author Eduardo Hernandez Cuamatzi | 10-02-2020 
    * @param String oppId Id de Oportunidad
    * @return Quote Quote recuperada
    **/
    public static Quote findQuote(String oppId) {
        final Opportunity oppData = MX_RTL_Opportunity_Selector.getOpportunity(oppId, FIELDSOPP);
        String quoteFieldSet = ',MX_SB_VTS_ASO_FolioCot__c, MX_ASD_PCI_DescripcionCode__c, Status, MX_SB_VTS_FechaInicio__c, MX_SB_VTS_Numero_de_Poliza__c';
        quoteFieldSet += ', MX_SB_VTS_RFC__c, MX_SB_VTS_Nombre_Contrante__c, MX_SB_VTS_Apellido_Paterno_Contratante__c, MX_SB_VTS_Apellido_Materno_Contratante__c';
        final String quoteFields = fillQueryQuote(quoteFieldSet);
        return MX_RTL_Quote_Selector.findQuoteById(oppData.SyncedQuoteId, quoteFields);
    }

    /**
    * @description Prepara Query para Quote
    * @author Eduardo Hernandez Cuamatzi | 10-02-2020 
    * @param String fields Campos extra
    * @return String Queyr construida
    **/
    public static String fillQueryQuote(String fields) {
        String quoteFields = 'Id, Name, TotalPrice, GrandTotal, BillingAddress, MX_SB_VTS_Email_txt__c,'; 
        quoteFields += 'MX_SB_VTS_TotalMonth__c, MX_SB_VTS_TotalSemiAnual__c,MX_SB_VTS_TotalQuarterly__c, MX_SB_VTS_TotalAnual__c, MX_SB_VTS_Numero_Interior__c, MX_SB_VTS_Numero_Exterior__c, MX_SB_VTS_GCLID__c';
        quoteFields += fields;
        return quoteFields;
    }

    /**
    * @description Datos de objeto generica
    * @author Eduardo Hernandez Cuamatzi | 10-02-2020 
    * @return List<MX_SB_VTS_Generica__c> Lista de registros
    **/
    public static List<MX_SB_VTS_Generica__c> findGenericaVals(String typeGene) {
        final Set<String> lstTypes = new Set<String>{typeGene};
        return MX_SB_VTS_Generica_Selector.findByTypeVal(LSTFIELDS, lstTypes);
    }

    /**
    * @description Datos de objeto MX_RTL_IVRCodes__mdt
    * @return List<MX_RTL_IVRCodes__mdt> Lista de registros codigos IVR
    **/
    public static List<MX_RTL_IVRCodes__mdt> findDataCodeVals(String ivrCode) {
        final Set<String> lstTypes = new Set<String>{'VTS'};
        return MX_RTL_IVRCodes_Selector.findByTypeVal(LSTFIELDSIVR, lstTypes, ivrCode);
    }

    /**
    * @description Actualiza registros quote
    * @author Eduardo Hernández Cuamatzi | 10-22-2020 
    * @param quoteData Datos de quote ah actualizar
    **/
    public static void updateQuote(Quote quoteData) {
        MX_RTL_Quote_Selector.upsrtQuote(quoteData);
    }
}