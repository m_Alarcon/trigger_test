/**
* Indra
* @author           Daniel perez lopez
* Project:          Presuscritos
* Description:      Clase test para methods de clase MX_SB_PS_Container1_Service
*
* Changes (Version)
* ------------------------------------------------------------------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    --------------------------------------
* @version  1.0     2020-09-04      Daniel perez lopez.     Creación de la Clase
*/

@isTest
public class MX_SB_PS_Container1_Service_tst {   
     /**
    * @Method Data Setup
    * @Description Method para preparar datos de prueba 
    * @return void
    **/
	@testSetup
    public static void  data() { 
        MX_SB_PS_Container1_Ctrl_Test.data();
        final product2[] pro =[Select id ,name,MX_SB_SAC_Proceso__c from product2 where name='Respaldo Seguro Para Hospitalización']; 
        pro[0].MX_SB_SAC_Proceso__c='SAC';
        update pro;
    }
    
    @isTest
    static void upsrtlineitem() {
        test.startTest();
        final Opportunity oppObj =[SELECT ID, AccountId, StageName FROM Opportunity LIMIT 1];
        oppObj.StageName=System.Label.MX_SB_PS_Etapa3;
        final Quote qtst1 = [Select id,Name from quote where MX_SB_VTS_Folio_Cotizacion__c='12345' limit 1];
        final QuoteLineItem[] listqli = [select id,QuoteId,Product2Id,PricebookEntryId from QuoteLineItem where QuoteId =:qtst1.Id];
        	MX_SB_PS_Container1_Service.upsrtQuoteLineItem(qtst1,listqli,'Respaldo Seguro Para Hospitalización');
        	system.assertEquals(qtst1.Name,'qtest','Quote validado coreectamente');
        test.stopTest();
    }
    
}