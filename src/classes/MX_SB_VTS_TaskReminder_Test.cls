/*
* BBVA - Mexico
* @Author: Diego Olvera
* MX_SB_VTS_TaskReminder_Test
* @Version 1.0
* @LastModifiedBy: Diego Olvera
* @ChangeLog
* 1.0 Created class - Diego Olvera
* 1.1 Logic added to cover extended class MX_SB_VTS_ReagendaLead
* @LastModifiedBy: Adrian Cruz Islas
* 1.2 Logic added to cover class method testLeadReagenda
*/
@isTest
private class MX_SB_VTS_TaskReminder_Test {
    /** Name User */
    final static String USERNAME = 'testTask';
    /** User Name 2*/
    final static String USERNAME2 = 'testHogar';
    /** lead name*/
    final static String LEADNAME = 'Test Lead Hogar';
    /** email para usuarios */
    final static String EMAIL = 'test@bbva.com.smart';
    /** Manager */
    final static String MANAGER = 'Manager';
    /** Proveedor CTI */
    final static String PROVEEDORSMART='SMART CENTER';
    /** variable operaciones */
    static String txtPost = 'POST', txtDescription = 'DESCRIPCION', txtServicio='SERVICEID', txtMsj='NOT iS EMPTY',
        txtUrl='http://www.example.com', txttelf='5511223344', txtUsername='UserOwnerTest01';
    /** cadena vacia */
    final static string EMPTYSTR='';

/* @Method: makeData
* @Description: create test data set
*/
    @TestSetup
    static void makeData() {
        final MX_SB_VTS_Generica__c setting = new MX_SB_VTS_Generica__c();
        setting.Name = 'ValorSmart';
        setting.MX_SB_VTS_TimeValue__c = '104';
        setting.MX_SB_VTS_Type__c = 'CP6';
        insert setting;
        final MX_SB_VTS_Lead_tray__c Bandeja = MX_WB_TestData_cls.crearBandeja(System.Label.MX_SB_VTS_HotLeads,'1');
        insert Bandeja;
        final Group queu = new Group(Name = 'MX_SB_VTS_Reagenda', type='Queue');
        insert queu;
        final User testManager = MX_WB_TestData_cls.crearUsuario(MANAGER, 'System Administrator');
        final User testUser = MX_WB_TestData_cls.crearUsuario(USERNAME, System.Label.MX_SB_VTS_ProfileAdmin);
        testUser.MX_SB_VTS_ProveedorCTI__c = PROVEEDORSMART;
        insert testManager;
        testUser.ManagerId = testManager.Id;
        insert testUser;
        final MX_SB_VTS_ProveedoresCTI__c Proveedor = MX_WB_TestData_cls.crearProveedor(testUser.MX_SB_VTS_ProveedorCTI__c);
        Proveedor.MX_SB_VTS_Identificador_Proveedor__c = PROVEEDORSMART;
        insert proveedor;
        final Account accnt = MX_WB_TestData_cls.createAccount('Test account', System.Label.MX_SB_VTS_PersonRecord);
        insert accnt;
        final Opportunity opp = MX_WB_TestData_cls.crearOportunidad('Test Reagenda', accnt.Id, testUser.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
        opp.Producto__c = 'Hogar';
        opp.Reason__c = 'Venta';
        opp.MX_SB_VTS_IsOppReagenda__c = true;
        opp.MX_SB_VTS_Tipificacion_LV1__c = 'Contacto';
        opp.MX_SB_VTS_Tipificacion_LV2__c = 'Contacto Efectivo';
        opp.MX_SB_VTS_Tipificacion_LV3__c = 'No Interesado';
        opp.MX_SB_VTS_Tipificacion_LV4__c = 'No acepta cotización';
        opp.MX_SB_VTS_Tipificacion_LV5__c = 'No Venta (No acepta cotización)';
        opp.MX_SB_VTS_Tipificacion_LV6__c = 'Otros Servicios Banco';
        opp.MX_SB_VTS_Tipificacion_LV7__c = 'N/A';
        opp.LeadSource = 'Facebook';
        opp.Motivosnoventa__c = 'Cliente molesto';
        opp.StageName = 'Closed Lost';
        insert opp;
        Task newTask = MX_WB_TestData_cls.crearTarea(opp.Id,'Tarea en Opportunity test');
        newTask.FechaHoraReagenda__c=date.newInstance(2001, 3, 21);
        newTask.IsReminderSet = true;
        newTask.ReminderDateTime=System.now()+1;
        newTask.ReminderDateTime=newTask.ReminderDateTime.addMinutes(-5);
        insert newTask;
        opp.MX_SB_VTS_LookActivity__c=newTask.Id;
        update opp;
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'bbvaMexSmartCenters_Crear_Carga', iaso__Url__c = txtUrl, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'Authentication', iaso__Url__c = 'https://validation/ok', iaso__Cache_Partition__c = 'local.MXSBVTSCache');
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'bbvaMexSmartCenter', iaso__Url__c = 'https://bbvacifrado.smartcenterservicios.com/ws_salesforce_desarrollo/api/login/authenticate', iaso__Cache_Partition__c = 'local.MXSBVTSCache');

        final Lead myLead = MX_WB_TestData_cls.vtsTestLead(LEADNAME, System.Label.MX_SB_VTS_Telemarketing_LBL);
        myLead.LeadSource = Label.MX_SB_VTS_OrigenCallMeBack;
        myLead.MX_SB_VTS_IsReagenda__c = true;
        myLead.Email = EMAIL;
        myLead.Resultadollamada__c = 'Contacto';
        insert myLead;
        newTask  = MX_WB_TestData_cls.crearTarea(MyLead.ID,'Tarea en Lead test');
        newTask.IsReminderSet = true;
        insert newTask;
        myLead.MX_SB_VTS_LookActivity__c=newTask.Id;
        update mylead;
    }
    /* @Method: testGetUser
* @Description: provides coverage for method finUsers
*/
    @isTest
    private static void testGetUsersOpp() {
        final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'testTask' LIMIT 1];
        System.runAs(cUser) {
            final Id OppId = [SELECT Id FROM Opportunity WHERE Name = 'Test Reagenda'].Id;
            final object test = MX_SB_VTS_APX_ReagendaLead.getUsers(OppId);
            System.assertNotEquals(test, null,'El objeto no esta vacio2');
        }
    }
    /** Get MX_SB_VTS_IsReagenda__c from Opportunity field value  */
    @isTest
    public static void updateOppIsReagendaTest() {
        final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'testTask' LIMIT 1];
        System.runAs(cUser) {
            Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_APX_ReagendaLeadMockClass());
            iaso.GBL_Mock.setMock(new MX_SB_VTS_APX_ReagendaLeadMockClass());
            final Id OppId = [SELECT Id FROM Opportunity LIMIT 1].ID;
            final Opportunity tstUpdOppAgenda =[SELECT ID, MX_SB_VTS_Tipificacion_LV7__c
                                                     FROM Opportunity LIMIT 1];
            Test.startTest();
            try {
                MX_SB_VTS_APX_ReagendaLead.updateIsReagenda(OppId);
            } catch(System.AuraHandledException extError) {
                extError.setMessage('fallo inesperado');
            }
            Test.stopTest();
            System.assertNotEquals(tstUpdOppAgenda, null,'El objeto no esta vacio4');
        }
    }
    @isTest
    private static void testfetchTasks() {
        final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'testTask' LIMIT 1];
        System.runAs(cUser) {
            final object test = MX_SB_VTS_TaskReminder.fetchTasks();
            System.assertNotEquals(test, null,'El objeto no esta vacio');
        }
    }
    /** Get tipificacionlvl7 field value  */
    @isTest
    private static boolean testOppValorNivelSiete() {
        final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'testTask' LIMIT 1];
        System.runAs(cUser) {
            Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_APX_ReagendaLeadMockClass());
            iaso.GBL_Mock.setMock(new MX_SB_VTS_APX_ReagendaLeadMockClass());
            final Id OppId = [SELECT Id FROM Opportunity LIMIT 1].ID;
            final Opportunity testgetLvl7 =[SELECT ID, MX_SB_VTS_Tipificacion_LV7__c
                                            FROM Opportunity LIMIT 1];
            Test.startTest();
            try {
                MX_SB_VTS_APX_ReagendaLead.getLvl7Value(testgetLvl7.Id, true);
            } catch(System.AuraHandledException extError) {
                extError.setMessage('fallo inesperado');
            }
            Test.stopTest();
            System.assertEquals(testgetLvl7.Id, OppId, 'Opp Id');
        }
        return true;
    }
    /** Recupera oppty */
    @isTest
    private static void testGetOpportunity2() {
        final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'testTask' LIMIT 1];
        System.runAs(cUser) {
            Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_APX_ReagendaLeadMockClass());
            iaso.GBL_Mock.setMock(new MX_SB_VTS_APX_ReagendaLeadMockClass());
            final String recordVTA1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
            final Id oppObj = [SELECT Id, Name, LeadSource FROM Opportunity where Name = 'Test Reagenda' LIMIT 1].ID;
            final Task testTask = [SELECT ID, IsReminderSet, ReminderDateTime, FechaHoraReagenda__c FROM Task WHERE Subject = 'Tarea en Opportunity test' LIMIT 1];
            test.startTest();
            try {
                MX_SB_VTS_APX_ReagendaLead.saveTask(testTask, true,oppObj, recordVTA1);
            } catch(System.AuraHandledException extError) {
                extError.setMessage('Error1');
                System.assertEquals('Error1', extError.getMessage(),'no entra');
            }
            test.StopTest();
        }
    }
    /** Fallo en opty */
    @isTest
    private static void testGetOpportunityError() {
        final AuraHandledException err = new AuraHandledException(EMPTYSTR);
        final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'testTask' LIMIT 1];
        System.runAs(cUser) {
            Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_APX_ReagendaLeadMockClass());
            iaso.GBL_Mock.setMock(new MX_SB_VTS_APX_ReagendaLeadMockClass());
            final String recordVTA1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
            final Opportunity oppObj = [SELECT Id, Name, LeadSource FROM Opportunity where Name = 'Test Reagenda' LIMIT 1];
            final Task testTask =[SELECT ID,FechaHoraReagenda__c, ReminderDateTime, Subject, ActivityDate,
                                  MX_SB_VTS_AgendadoPor__c FROM Task WHERE Subject = 'Tarea en Opportunity test' LIMIT 1];
            testTask.FechaHoraReagenda__c=date.newInstance(2001, 3, 21);
            testTask.ReminderDateTime=System.now()+1;
            testTask.ReminderDateTime=testTask.ReminderDateTime.addMinutes(-5);
            test.startTest();
            try {
                MX_SB_VTS_APX_ReagendaLead.saveTask(testTask, true,oppObj.Id, recordVTA1);
            } catch(Exception ex) {
                System.assertEquals(ex.getMessage(), err.getMessage(), 'Fallo esperado');
            }
            test.StopTest();
        }
    }
    /** Recupera Lead */
    @isTest
    private static void testGetOppTask() {
        final User testManager2 = MX_WB_TestData_cls.crearUsuario('Manager2', 'System Administrator');
        final User testUser2 = MX_WB_TestData_cls.crearUsuario('TestTask2', System.Label.MX_SB_VTS_ProfileAdmin);
        testUser2.MX_SB_VTS_ProveedorCTI__c = PROVEEDORSMART;
        insert testManager2;
        testUser2.ManagerId = testManager2.Id;
        insert testUser2;
        final Account accnt = MX_WB_TestData_cls.createAccount('Test account2', System.Label.MX_SB_VTS_PersonRecord);
        insert accnt;
        final Opportunity opp = MX_WB_TestData_cls.crearOportunidad('Test Reagenda2', accnt.Id, testUser2.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
        opp.Producto__c = 'Hogar';
        opp.Reason__c = 'Venta';
        opp.MX_SB_VTS_Tipificacion_LV1__c = 'Contacto';
        opp.MX_SB_VTS_Tipificacion_LV2__c = 'Contacto Efectivo';
        opp.MX_SB_VTS_Tipificacion_LV3__c = 'Interesado';
        opp.MX_SB_VTS_Tipificacion_LV4__c = 'Acepta cotización';
        opp.MX_SB_VTS_Tipificacion_LV5__c = 'No Venta (acepta cotización)';
        opp.MX_SB_VTS_Tipificacion_LV6__c = 'Agendar';
        opp.MX_SB_VTS_Tipificacion_LV7__c = 'Reagenda';
        opp.LeadSource = 'Facebook';
        opp.Motivosnoventa__c = 'Cliente molesto';
        opp.StageName = 'Closed Lost';
        insert opp;
        final Task newTask = MX_WB_TestData_cls.crearTarea(opp.Id,'Tarea en Opportunity test2');
        newTask.FechaHoraReagenda__c=date.newInstance(2001, 3, 21);
        newTask.IsReminderSet = true;
        newTask.ReminderDateTime=System.now()+1;
        newTask.ReminderDateTime=newTask.ReminderDateTime.addMinutes(-5);
        insert newTask;
        opp.MX_SB_VTS_LookActivity__c=newTask.Id;
        opp.OwnerId = testUser2.ManagerId;
        update opp;
        final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'TestTask2' LIMIT 1];
        System.runAs(cUser) {
            Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_APX_ReagendaLeadMockClass());
            iaso.GBL_Mock.setMock(new MX_SB_VTS_APX_ReagendaLeadMockClass());
            final Id OppId = [SELECT Id FROM Opportunity  where Name = 'Test Reagenda2' LIMIT 1].ID;
            final Task testTask =[SELECT ID,FechaHoraReagenda__c, WhoId, ReminderDateTime, Subject, ActivityDate,
                                  MX_SB_VTS_AgendadoPor__c, IsReminderSet
                                  FROM Task WHERE Subject = 'Tarea en Opportunity test2' LIMIT 1];
            final User managerId = [Select Id, ManagerId, Name from User WHERE Manager.Name = 'Manager2' LIMIT 1];
            Test.startTest();
            try {
                MX_SB_VTS_TaskReminder.oppReagenda(OppId, testTask, true, managerId);
            } catch (System.AuraHandledException extError) {
                extError.setMessage('Error2');
                System.assertEquals('Error2', extError.getMessage(),'no entra');
            }
            Test.stopTest();
        }
    }
    /*
	* @Method: testLeadReagenda
	* @Description: provides coverage for method leadReagenda
	* copy from testGetLeadTask method of MX_SB_VTS_APX_ReagendaLead_Test class
	* @author Adrian Cruz Islas
	*/
    @isTest
    private static void testLeadReagenda() {
        final User  cUser=[SELECT ID,MX_SB_VTS_ProveedorCTI__c,Name FROM User  WHERE NAME = 'testTask' LIMIT 1];
        System.runAs(cUser) {
            Test.setMock(HttpCalloutMock.class, new MX_SB_VTS_APX_ReagendaLeadMockClass());
            iaso.GBL_Mock.setMock(new MX_SB_VTS_APX_ReagendaLeadMockClass());
            final Id leadId = [SELECT Id FROM Lead LIMIT 1].ID;
            final Task testTask =[SELECT ID,FechaHoraReagenda__c, ReminderDateTime, Subject, ActivityDate,
                                  MX_SB_VTS_AgendadoPor__c, IsReminderSet
                                  FROM Task WHERE Subject = 'Tarea en Lead test' LIMIT 1];
            final User managerId = [Select Id, ManagerId, Name from User WHERE Manager.Name =: MANAGER LIMIT 1];
            Test.startTest();
            MX_SB_VTS_TaskReminder.leadReagenda(leadId, testTask, true, managerId);
            Test.stopTest();
            System.assertEquals(testTask.WhoId, leadId, 'Lead Id1');
        }
    }
}