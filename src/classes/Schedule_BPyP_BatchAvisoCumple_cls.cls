/*
    Proyecto : ENGMX
    Versión     Fecha           Autor                   Descripción
    ____________________________________________________________________________________________
    1.0         28-Ago-2018     Cristian Espinosa       Creación de la clase de schedule para
														programar el batch BPyP_BatchAvisoCumple_cls.
														* Clase de pruebas : Schedule_BPyP_BatchAvisoCumple_tst
	1.1			19-Sep-2018		Cristian Espinosa		Se aumenta el batchsize, debido a que el batch ya no
														actualiza las cuentas.
    1.2         02-Ene-2020   Jair Ignacio Gonzalez     Se cambio el campo MX_BPyP_Proximo_Cumpleanios__c por
                                                        PersonContact.MX_NextBirthday__c por la reingenieria de person account
*/
public with sharing class Schedule_BPyP_BatchAvisoCumple_cls implements Schedulable {

  /*
        @Descripción : Se manda llamar la clase batch BPyP_BatchAvisoCumple_cls
  */
  public void execute(SchedulableContext schCtx) {
      final BPyP_BatchAvisoCumple_cls bpp = new BPyP_BatchAvisoCumple_cls();
      database.executebatch(bpp,100);
  }
}