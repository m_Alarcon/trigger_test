/**
* Name: MX_RTL_User_service
* @author Jose angel Guerrero
* Description : Nuevo service
* Ver                  Date            Author                   Description
* @version 1.0         04/08/2020     Jose angel Guerrero      Initial Version
**/
public class MX_RTL_User_service {
    /**
* @description: funcion constructor privado
*/
    private MX_RTL_User_service() { }
    /**
* @description: funcion constructor
*/
    public static List< User > serviceBucarUsuRol (String colaID) {
        return MX_RTL_User_Selector.getUsersByProfile(colaID);
    }
    /**
* @description: funcion constructor
*/
    public static List< User > serviceBucarUsuario (String nombre) {
        return MX_RTL_User_Selector.getUsersByHintYProfile(nombre,'%Multiasistencia');
    }
}