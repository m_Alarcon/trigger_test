/**
 * @File Name          : MX_RTL_Siniestro_History_Selector.cls
 * @Description        :
 * @Author             : Juan Carlos Benitez
 * @Group              :
 * @Last Modified By   : Juan Carlos Benitez
 * @Last Modified On   : 5/26/2020, 05:22:22 AM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/26/2020   Juan Carlos Benitez         Initial Version
**/
public with sharing class MX_RTL_DocumentosVida_mdt_Selector {
	     /** List of records to be returned */
    final static List<SObject> RETURN_DOCSVIDA = new List<SObject>();

    /** constructor */
    @TestVisible
    private MX_RTL_DocumentosVida_mdt_Selector() { }

    /**
    * @description
    * @author Juan Carlos Benitez | 5/25/2020
    * @param mdtFields
    * @return RETURN_DOCSVIDA
    **/

    public static List<Sobject> getDocsDataById(String kit) {
        RETURN_DOCSVIDA.addAll([SELECT MX_SB_MLT_Enum__c , DeveloperName , MX_SB_MLT_DocumentosRequeridos__c, Kits__c
                FROM MX_SB_MLT_DocumentosVida__mdt where Kits__c LIKE :kit ORDER BY MX_SB_MLT_Enum__c  asc]);
        return RETURN_DOCSVIDA;
    }
}