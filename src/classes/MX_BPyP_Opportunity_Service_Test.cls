/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPyP_Opportunity_Service_Test
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-05-25
* @Description 	Test Class for Opportunity Service Layer
* @Changes
*  
*/
@isTest
public class MX_BPyP_Opportunity_Service_Test {
    
	/** String BPyP Estandar */
	static final String STR_BPYPESTANDAR = 'BPyP Estandar';
    
    /** Colocación */
	static final String STR_COLOCACION = 'Colocación';
    
	/** Collares */
	static final String STR_COLLARES = 'Collares';
    
	/** BPYP Opportunity RecordType */
	static final String STR_RT_BPYPOPP = 'MX_BPP_RedBpyp';
    
    /** BPYP Account RecordType */
    static final String STR_RT_BPYPACC = 'BPyP_tre_Cliente';
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description TestSetup
    **/
    @testSetup
    static void setup() {
        final User userBanquero = UtilitysDataTest_tst.crearUsuario('UsuarioSTBPyP', STR_BPYPESTANDAR, Null);
        userBanquero.Segmento_Ejecutivo__c = 'EMPRESARIAL';
        insert userBanquero;
        
		System.runAs(userBanquero) {
			final Account cliente = UtilitysDataTest_tst.crearCuenta('userBPyP', STR_RT_BPYPACC);
            cliente.MX_BPP_CPC_CheckClienteCPC__c = false;
            insert cliente;
            UtilitysDataTest_tst.crearProdForm(STR_COLOCACION, STR_COLLARES);
			final Opportunity opp = UtilitysDataTest_tst.crearOportunidad('OppTest 1', cliente.id, userBanquero.id, STR_COLOCACION, STR_COLLARES, null, STR_RT_BPYPOPP);
        	insert opp;
        }
    }
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test method trying to change the Opportunity Status from Open to Close Win
    **/
	@isTest
    static void testOppAbiertaCerrada() {
        Exception noUpdExc;
        final User user = [SELECT Id, Name FROM User WHERE LastName = 'UsuarioSTBPyP' AND Profile.Name =:STR_BPYPESTANDAR LIMIT 1];
        
		System.runAs(user) {
            final Opportunity opp = [SELECT Id, Name FROM Opportunity WHERE Name = 'OppTest 1' AND OwnerId =: user.Id LIMIT 1];
			final Account cliente = [SELECT Id, Name, No_de_cliente__c FROM Account WHERE CreatedById =: user.Id LIMIT 1];
	    	Test.startTest();
            try {
				BpyP_Adv_Opp.updOpp(opp.Id, 'Cerrada Ganada', cliente.No_de_cliente__c, 123, true, '', '');
            } catch (Exception e) {
                noUpdExc = e;
            }
            Test.stopTest();
            
    		System.assertNotEquals(null, noUpdExc, 'Oportunidad se actualizó sin validación');
		}
    }
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test method trying to change the Opportunity Status from Open to Close Win
    **/
	@isTest
    static void testOppAbiertaGestionCerrada() {
        Exception noUpdExc;
        final User user = [SELECT Id, Name FROM User WHERE LastName = 'UsuarioSTBPyP' AND Profile.Name =:STR_BPYPESTANDAR LIMIT 1];
        
		System.runAs(user) {
            final Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'OppTest 1' AND OwnerId =: user.Id LIMIT 1];
			final Account cliente = [SELECT Id, No_de_cliente__c FROM Account WHERE CreatedById =: user.Id LIMIT 1];

	    	Test.startTest();
            try {
                BpyP_Adv_Opp.updOpp(opp.Id, 'En Gestión', cliente.No_de_cliente__c, 123, true, '', '');
				BpyP_Adv_Opp.updOpp(opp.Id, 'Cerrada Ganada', cliente.No_de_cliente__c, 123, true, '', '');
            } catch (Exception e) {
                noUpdExc = e;
            }
            Test.stopTest();
            
    		System.assertEquals(null, noUpdExc, 'Oportunidad se actualizó considerando validación');
		}
    }
}