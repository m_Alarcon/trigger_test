/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 10-12-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   09-23-2020   Eduardo Hernández Cuamatzi   Initial Version
 * 1.1   02-11-2021   Diego Olvera Hernandez       Se agrega función que valida el proceso update
 * 1.1.1 02-22-2021   Eduardo Hernández Cuamatzi   Se agrega token antifraude a consumo de otp
**/
@isTest
public class MX_SB_VTS_PaymentModule_Ctrl_Test {
    /**@description etapa Cotizacion*/
    private final static string COTIZACION = 'Cotización';
    /**Folio cotización*/
    private final static string FOLIOCOT = '9999999';
    /**URL Mock Test*/
    private final static String URLTOTEST = 'http://www.example.com';
    /**Particion IASO*/
    private final static String IASOPART = 'local.MXSBVTSCache';
    /**Tipo de domicilio */
    public final static String DOMTYPE = 'Domicilio asegurado';
     /**@description Etapa de la Oportunidad*/
    private final static String ETAPAOPP = 'Contacto';
    
    @testSetup static void setup() {
        MX_SB_VTS_CallCTIs_utility.initHogarGeneric();
        final MX_SB_VTS_Generica__c setting = MX_WB_TestData_cls.GeneraGenerica('OpenPay', 'CP21');
        setting.MX_SB_VTS_TimeValue__c = '104';
        setting.MX_SB_VTS_Type__c = 'CP21';
        setting.MX_SB_SAC_ProductFam__c = 'False';
        insert setting;
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'GrantingTicketsDev', iaso__Url__c = URLTOTEST, iaso__Cache_Partition__c = IASOPART);
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'getGTServicesSF', iaso__Url__c = URLTOTEST, iaso__Cache_Partition__c = IASOPART);
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'createQuote', iaso__Url__c = URLTOTEST, iaso__Cache_Partition__c = IASOPART);
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'createPolicy', iaso__Url__c = URLTOTEST, iaso__Cache_Partition__c = IASOPART);
        insert new iaso__GBL_Rest_Services_Url__c(Name = 'generarOTP', iaso__Url__c = URLTOTEST, iaso__Cache_Partition__c = IASOPART);
    }

    @isTest 
    static void findDataQuote() {
        Test.startTest();
        final Opportunity oppRecId = [Select Id, Name,AccountId, SyncedQuoteId from Opportunity where StageName =: COTIZACION];
        final Quote quoteData = findQuote(oppRecId.Id);
        quoteData.MX_SB_VTS_GCLID__c = '1|2|3|4';
        update quoteData;
        final MX_RTL_MultiAddress__c addressType = new MX_RTL_MultiAddress__c();
        addressType.MX_RTL_MasterAccount__c = oppRecId.AccountId;
        addressType.MX_RTL_QuoteAddress__c = quoteData.Id;
        addressType.MX_RTL_AddressStreet__c = 'test';
        addressType.MX_RTL_AddressType__c = DOMTYPE;
        insert addressType;
        MX_SB_VTS_PaymentModule_Service_Test.syncQuoteData(oppRecId, quoteData.Id);
        final Map<String, Object> respone = MX_SB_VTS_PaymentModule_Ctrl.findDataQuote(oppRecId.Id);
        System.assert((Boolean)respone.get('isOk'), 'Estatus correcto');
        Test.stopTest();
    }

    /**Recupera Quote a sincronizar */
    public static Quote findQuote(Id oppId) {
        final Quote quoteData = [Select Id, Name from Quote where OpportunityId =: oppId];
        quoteData.MX_SB_VTS_ASO_FolioCot__c = FOLIOCOT;
        quoteData.BillingCity = 'billing city';
        quoteData.BillingCountry = 'billing country';
        quoteData.BillingPostalCode = '9999';
        quoteData.BillingState = 'billing state';
        quoteData.BillingStreet = 'billing street';
        return quoteData;
    }

    @isTest 
    static void updateQuoteIVR() {
        Test.startTest();
        final Opportunity oppRec = [Select Id, Name, SyncedQuoteId from Opportunity where StageName =: COTIZACION];
        final Quote quoData = [Select Id, Name from Quote where OpportunityId =: oppRec.Id];
        MX_SB_VTS_PaymentModule_Service_Test.syncQuoteData(oppRec, quoData.Id);
        MX_SB_VTS_PaymentModule_Ctrl.updateQuoteIVR(oppRec.Id);
        final Quote quoteDataUpdate = [Select Id, Name from Quote where OpportunityId =: oppRec.Id];
        System.assertEquals(quoteDataUpdate.Id, quoData.Id, 'Quote Actualizada');
        Test.stopTest();
    }
    

    @isTest 
    static void findGenerica() {
        Test.startTest();
        final Map<String, Object> mapResponse = MX_SB_VTS_PaymentModule_Ctrl.findGenerica();        
        System.assert(!(Boolean)mapResponse.get('generica'), 'Quote Actualizada');
        Test.stopTest();
    }

    @isTest 
    static void findIvrCode() {
        Test.startTest();
            final Map<String, Object> mapResponse = MX_SB_VTS_PaymentModule_Ctrl.findDataCode('3003');
            System.assertEquals((String)mapResponse.get('title'), 'Tarjeta sin fondos suficientes', 'Mensaje traducido');
        Test.stopTest();
    }

    @isTest
    static void updateStatuscOT() {
        final Map<String, String> headersMock = new Map<String, String>();
        headersMock.put('tsec', '368897844');
        final MX_WB_Mock mockCalloutTest = new MX_WB_Mock(200, 'Complete', '{}', headersMock);
        iaso.GBL_Mock.setMock(mockCalloutTest);
        final Opportunity oppRec = [Select Id, Name, SyncedQuoteId from Opportunity where StageName =: COTIZACION];
        final Quote findingQuote = findQuote(oppRec.Id);
        findingQuote.MX_SB_VTS_ASO_FolioCot__c = FOLIOCOT;
        findingQuote.Status = 'Tarificada';
        update findingQuote;
        MX_SB_VTS_PaymentModule_Service_Test.syncQuoteData(oppRec, findingQuote.Id);
        Test.startTest();
            final Map<String, Object> mapResponse = MX_SB_VTS_PaymentModule_Ctrl.updateStatuscOT(oppRec.Id);
            final Map<String, Object> serviceResponse = (Map<String, Object>)mapResponse.get('updateQuote');
            System.assert((Boolean)serviceResponse.get('quoteUpdated'), 'Cotización Formalizada');
        Test.stopTest();
    }

    @isTest
    static void createdPolyce() {
        final Map<String, String> headersMock = new Map<String, String>();
        headersMock.put('tsec', '368897845');
        final MX_WB_Mock mockCalloutTest = new MX_WB_Mock(200, 'Complete', MX_SB_VTS_PaymentModule_Service_Test.findMock('createPolicy'), headersMock);        
        iaso.GBL_Mock.setMock(mockCalloutTest);
        final Opportunity oppRec = [Select Id, Name, SyncedQuoteId from Opportunity where StageName =: COTIZACION];
        final Quote fQuote = findQuote(oppRec.Id);
        fQuote.MX_SB_VTS_ASO_FolioCot__c = FOLIOCOT;
        fQuote.Status = 'Formalizada';
        update fQuote;
        MX_SB_VTS_PaymentModule_Service_Test.syncQuoteData(oppRec, fQuote.Id);
        Test.startTest();
            final Map<String, Object> mapResponse = MX_SB_VTS_PaymentModule_Ctrl.createdPolyce(oppRec.Id);
            final Map<String, Object> policyResponse = (Map<String, Object>)mapResponse.get('polyceStatus');
            System.assert((Boolean)policyResponse.get('quoteUpdated'), 'Cotización Emitida');
        Test.stopTest();
    }

    /* 
    @Method: statusOTPCtrl
    @Description: test method to get the status code from web service
    */
    static testMethod void statusOTPCtrl() {
        final Map<String, String> mockHeaders = new Map<String, String>();
        mockHeaders.put('tsec', '847392344');
        final MX_WB_Mock mockCallout = new MX_WB_Mock(200, 'Complete', '{}', mockHeaders);
        iaso.GBL_Mock.setMock(mockCallout);
        final Opportunity oppRec = [Select Id, Name, SyncedQuoteId from Opportunity where StageName =: COTIZACION];
        final Quote fQuote = findQuote(oppRec.Id);
        oppRec.TelefonoCliente__c = '2345671234';
        fQuote.MX_SB_VTS_ASO_FolioCot__c = FOLIOCOT;
        fQuote.Status = 'Formalizada';
        fQuote.MX_SB_VTS_Movil_txt__c = '2345671234';
        update fQuote;
        MX_SB_VTS_PaymentModule_Service_Test.syncQuoteData(oppRec, fQuote.Id);
        Test.startTest();
            final Map<String,String> valuesMock = MX_SB_VTS_PaymentModule_Ctrl.getCodeStatus(oppRec.Id, 'TESTTOKEN');
        Test.stopTest();
        System.assertNotEquals(valuesMock, null,'Envio de OTP');
    }
    @isTest
    static void updCoppRec() {
        final Opportunity oppLstUpd = [Select Id, StageName, Name from Opportunity Where StageName =: COTIZACION];
        final String etapa = ETAPAOPP;
        if(etapa == ETAPAOPP) {
            oppLstUpd.StageName = 'Contacto';
        } else {
            oppLstUpd.StageName = 'Cobro y resumen de contratación';
        }
        update oppLstUpd;
        Test.startTest();
        try {
            MX_SB_VTS_PaymentModule_Ctrl.updCurrentOppCtrl(oppLstUpd.Id, etapa);
        } catch (System.AuraHandledException extError) {
            extError.setMessage('Fallo');
            System.assertEquals('Error', extError.getMessage(),'No Actualizó el stage');
        }
        Test.stopTest();
    }
}