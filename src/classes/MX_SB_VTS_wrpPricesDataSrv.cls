/**
* @FileName          : MX_SB_VTS_wrpPricesDataSrv
* @description       : Wrapper class for use de web service of Prices
* @Author            : Alexandro Corzo
* @last modified on  : 23-12-2020
* Modifications Log 
* Ver   Date         Author                Modification
* 1.0   23-12-2020   Alexandro Corzo       Initial Version
* 1.1   25-02-2020   Alexandro Corzo       Se agregan comentarios a la clase
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton, sf:ExcessivePublicCoun,sf:LongVariable,sf:LongVariable,sf:ExcessivePublicCount, sf:ShortVariable,sf:ShortClassName')
public class MX_SB_VTS_wrpPricesDataSrv {
	/**
	 * Invocación de Wrapper
	 */
    public base base {get;set;}
    
    /**
     * @description : Clase del objeto base del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class base {
        /** Atributo de Clase: allianceCode */
        public String allianceCode {get; set;}
        /** Invocación de Clase: insuredAmount */
        public insuredAmount insuredAmount {get; set;}
        /** Atributo de Clase: insuredPercentage */
        public String insuredPercentage {get; set;}
        /** Atributo de Clase: coveredArea */
        public String coveredArea {get; set;}
        /** Atributo de Clase: certifiedNumberRequested */
        public String certifiedNumberRequested {get; set;}
        /** Atributo de Clase: zipCode */
        public String zipCode {get; set;}
        /** Invocación de Clase: product */
        public product product {get; set;}
        /** Invocación de Clase: validityPeriod */
        public validityPeriod validityPeriod {get; set;}
        /** Atributo de Clase: region */
        public String region {get; set;}
        /** Invocación de Clase: frequencies */
        public frequencies[] frequencies {get; set;}
        /** Invocación de Clase: contractingCriteria */
        public contractingCriteria[] contractingCriteria {get; set;}
        /** Invocación de Clase: trades */
        public trades[] trades {get; set;}
    }
    
    /**
     * @description : Clase del objeto insuredAmount del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class insuredAmount {
        /** Atributo de Clase: amount */
        public String amount {get; set;}
        /** Atributo de Clase: currencydata */
        public String currencydata {get; set;}
    }
    
    /**
     * @description : Clase del objeto product del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class product {
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Invocación de Clase: plan */
        public plan plan {get; set;}
        /** Invocación de Clase: trade */
        public trade trade {get; set;}
    }
    
    /**
     * @description : Clase del objeto plan del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class plan {
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Atributo de Clase: reviewCode */
        public String reviewCode {get; set;}
    }
    
    /**
     * @description : Clase del objeto trade del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class trade {
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
    }
    
    /**
     * @description : Clase del objeto validityPeriod del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class validityPeriod {
        /** Atributo de Clase: startDate */
        public String startDate {get; set;}
        /** Atributo de Clase: endDate */
        public String endDate {get; set;}
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
    }
    
    /**
     * @description : Clase del objeto frequencies del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class frequencies {
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
    }
    
    /**
     * @description : Clase del objeto contractingCriteria del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class contractingCriteria {
        /** Atributo de Clase: criterial */
        public String criterial {get; set;}
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Atributo de Clase: value */
        public String value {get; set;}
    }
    
    /**
     * @description : Clase del objeto trades del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class trades {
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Invocación de Clase: categories */
        public categories[] categories {get; set;}
    }
    
    /**
     * @description : Clase del objeto categories del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class categories {
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Invocación de Clase: goodTypes */
        public goodTypes[] goodTypes {get; set;}
    }
    
    /**
     * @description : Clase del objeto goodTypes del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class goodTypes {
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
        /** Invocación de Clase: coverages */
        public coverages[] coverages {get; set;}
    }
    
    /**
     * @description : Clase del objeto coverages del Wrapper
     * @author: 	  Alexandro Corzo
     */
    public class coverages {
        /** Atributo de Clase: iddata */
        public String iddata {get; set;}
    }
}