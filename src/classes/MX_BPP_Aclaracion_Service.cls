/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Trigger para aclaraciones - control

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     20/05/2020      Roberto Isaac Soto Granados           Creación Clase
*****************************************************************************************/
public class MX_BPP_Aclaracion_Service {

    /*Contructor clase Aclaracion_ctrl*/
    private MX_BPP_Aclaracion_Service() {}

    /*Method que liga la aclaración con el caso por medio del folio*/
    public static void linkCases(List<BPyP_Aclaraciones__c> aclaraciones) {
        Final Map<String, Id> casesByFolio = new Map<String, Id>();
        Final Map<String, String> accsInAclara = new Map<String, String>();
        Final List<String> folios = new List<String>();

        for(BPyP_Aclaraciones__c aclara : aclaraciones) {
            folios.add(aclara.BPyP_Folio__c);
        }

        Final List<Case> cases = MX_RTL_Case_Selector.casosPorFolios(new Set<String>(folios));

        for(Case caseA : cases) {
            casesByFolio.put(caseA.MX_SB_SAC_Folio__c, caseA.Id);
        }

        for(BPyP_Aclaraciones__c aclaracion : aclaraciones) {
            aclaracion.BPyP_Caso__c = casesByFolio.containsKey(aclaracion.BPyP_Folio__c) ? casesByFolio.get(aclaracion.BPyP_Folio__c) : aclaracion.BPyP_Caso__c;
            accsInAclara.put(aclaracion.BPyP_Folio__c, aclaracion.BPyP_NumeroCliente__c);
        }

        updateCases(accsInAclara, cases);
    }

    /*Method que liga el caso con la cuenta por medio del número de cliente*/
    public static void updateCases(Map<String, String> folioClienteAcl, List<Case> cases) {
        Final Map<String, Account> accIds = new Map<String, Account>();
        Final List<Case> updatedCases = new List<Case>();
        List<Account> accounts;
        final Set<String> accsInAclara = new Set<String>();
        String tempCliente;

        for (String key : folioClienteAcl.keySet()) {
            accsInAclara.add(folioClienteAcl.get(key));
        }
        
        accounts = MX_RTL_Account_Selector.cuentasPorNoDeCliente(accsInAclara);

        for(Account acc : accounts) {
            accIds.put(acc.No_de_cliente__c, acc);
        }
		
        for(Case caseU : cases) {
            if(folioClienteAcl.containsKey(caseU.MX_SB_SAC_Folio__c)) {
                tempCliente = folioClienteAcl.get(caseU.MX_SB_SAC_Folio__c);
                if(accIds.get(tempCliente) != null) {
                    caseU.AccountId = accIds.get(tempCliente).Id;
                    caseU.OwnerId = accIds.get(tempCliente).OwnerId;
                    updatedCases.add(caseU);
                }
            }
        }

        if(!updatedCases.isEmpty()) {
            MX_RTL_Case_Selector.actualizaCasos(updatedCases);
        }
    }
    
    /*Llamada a selector*/
    public static List<User> serviceDirectorIds(Set<Id> userIds) {
        return MX_RTL_User_Selector.directorIds(userIds);
    }
    /*Llamada a selector*/
    public static List<Case> serviceCasosEnAclaraciones(Set<Id> userIds, Set<Id> dirIds) {
        return MX_RTL_Case_Selector.casosEnAclaraciones(userIds, dirIds);
    }
    /*Llamada a selector*/
    public static List<Case> serviceCasosParaVisitas(Set<Id> caseIds) {
        return MX_RTL_Case_Selector.casosParaVisitas(caseIds);
    }
    /*Llamada a selector*/
    public static void serviceActualizaCasos(List<Case> updatedCases) {
        MX_RTL_Case_Selector.actualizaCasos(updatedCases);
    }
    /*Llamada a selector*/
    public static Id serviceRecordTypeLlamadaBPyP() {
        return MX_RTL_RecordType_Selector.recordTypeLlamadaBPyP()[0].Id;
    }
    /*Llamada a selector*/
    public static Id serviceCreaVisita(List<dwp_kitv__Visit__c> newVisit) {
        return Dwp_kitv_Visit_Selector.creaVisita(newVisit);
    }
    /*Llamada a selector*/
    public static void serviceCreaTemas(List<dwp_kitv__Visit_Topic__c> newTopics) {
        Dwp_kitv_Visit_Topic_Selector.creaTemas(newTopics);
    }
}