/**
* -------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_PolizaDatosParticulares_Cls
* Autor: Juan Carlos Benitez Herrera
* Proyecto: Siniestros - BBVA
* Descripción : Clase que almacena los methods usados en datos particulares de la poliza
* --------------------------------------------------------------------------------
* Versión       Fecha                Autor                    	Descripción
* --------------------------------------------------------------------------------
* 1.0         12/02/2019     Juan Carlos Benitez Herrera	     Creacion
* 1.1         13/02/2020     Juan Carlos Benitez Herrera	Corrección CodeSmells
* 1.2         12/03/2020     Juan Carlos Benitez Herrera	Se cambian campos de contrato SAC
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_PolizaDatosParticulares_Cls { //NOSONAR
  	 /*
    * @description Obtiene el los datos de la poliza
    * @param String Poliza
    * @return Object Contract 
    */
    @AuraEnabled
    public static Contract getDatosContrato(String poliza) {
       try {
           final Contract[] listares = [SELECT id,MX_SB_MLT_ColorVehiculo__c,MX_SB_MLT_DescripcionVehiculo__c, MX_WB_Marca__c,MX_WB_Modelo__c,MX_WB_noMotor__c,MX_WB_noPoliza__c, MX_WB_numeroSerie__c,MX_WB_origen__c,MX_WB_placas__c,StartDate,MX_SB_SAC_ClaveSB__c FROM Contract where MX_WB_noPoliza__c=:Poliza];
           return listares[0];
       } catch(Exception dny) {
           throw new AuraHandledException (System.Label.MX_WB_LG_ErrorBack +' No se encontraron registros con id '+ JSON.serialize(dny));
       }
    }
      /*
    * @description Muestra datos particulares de la poliza
    * @param void
    * @return SObject
    */
    @AuraEnabled
	public static Map<String,String> obDatosParticulares () {
        final Map<String,String> datajson = new Map<String,String>();
        datajson.put('Poliza1', '2002');
        datajson.put('Poliza2', '8B7CA0009T');
        datajson.put('Certificado', '1');
        datajson.put('Cliente', 'GRUPO TIGRE SEGURIDAD PRIVADA SA DE CV');
        datajson.put('SegClasif', 'BBVA');
        datajson.put('ClaveSb', 'VEN020');
        datajson.put('Descripcion', 'VOLKSWAGEN VENTO 1.6L COMFORTLINE AA EE CD BA ESTANDARD SEDAN');
        datajson.put('FechaInicio', '43069');
        return datajson;
    }

          /*
    * @description Muestra datos particulares de la poliza
    * @param void
    * @return SObject
    */
    @AuraEnabled    
	public static Map<String,String> obDatosParticular () {
		final Map<String,String> datajson = new Map<String,String>();
        datajson.put('OrigenC', 'N');
        datajson.put('Origen', 'Nacional');
        datajson.put('Marca', 'VOLKSWAGEN');
        datajson.put('MarcaC', '165');
        datajson.put('SubMarcaC', '38');
        datajson.put('SubMarca', 'VENTO');
        datajson.put('TipoPropC', '3');
        datajson.put('TipoProp', 'PARTICULAR');
        datajson.put('VersionC', '11');
        datajson.put('Version', '1.6L COMFORTLINE');
        datajson.put('TipoValorC', 'FACTURA');
        datajson.put('ValorN', '100000');
        datajson.put('TipoVehiculoC', 'A');
        datajson.put('TipoVehiculo', 'AUTOMOVIL');
        datajson.put('TipoTransmisionC', '8');
        datajson.put('TipoTransmision', 'ESTANDAR');
        datajson.put('TipoCargaC', 'A');
        datajson.put('TipoCarga', 'REDUCIDA PELIGROSIDAD');
        datajson.put('CarroceriaC', '19');
        datajson.put('Carroceria', 'SEDAN');
        datajson.put('TipoRegVeh', 'NACIONAL NACIONAL');
        datajson.put('TipoRegVehC', '1');
        datajson.put('RemolqueC', '0');
        datajson.put('Remolque', 'NO');
        datajson.put('ZonaCirC', '15');
        datajson.put('ZonaCir', 'ESTADO DE MEXICO');
        datajson.put('NumPuertas', '4');
        datajson.put('NumOcupantes', '5');
        datajson.put('NumCilindros', '4');
        datajson.put('Modelo', '2018');
        datajson.put('ColorC', 'NES');
        datajson.put('Color', 'ESPECIFICADO');
        datajson.put('noSerie', 'MEX5H2604JT037156');
        datajson.put('SerialMotor', 'CLS580785');
        datajson.put('Placas', 'MSN-2389');
        datajson.put('TipoServC', 'PAR');
        datajson.put('TipoServC', 'PARTICULAR');
		datajson.put('TipoUsoC', '6');
        datajson.put('TipoUso', 'PARTICULAR');
        return datajson;
    } 
    /*
    * @description Muestra datos Tabla de Equipamento en datos particulares de la poliza
    * @param void
    * @return SObject
    */
    @AuraEnabled
    public static Map<String,String> obTableEquipamento () {
        final Map<String,String> datajson = new Map<String,String>();
        datajson.put('Descripcion', 'Aire Acondicionado');
		datajson.put('codigo', '001');
        return datajson;
    }
}