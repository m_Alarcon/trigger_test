/**
 * @description       : 
 * @author            : Daniel Perez Lopez
 * @group             : 
 * @last modified on  : 02-02-2021
 * @last modified by  : Arsenio.perez.lopez.contractor@bbva.com
 * Modifications Log 
 * Ver   Date         Author               Modification
 * 1.0   11-11-2020   Daniel Perez Lopez   Initial Version
 * 1.1   25-01-2021   Arsenio Perez Lopez   Correction 
 * 1.2   10-03-2021   Juan Carlos Benitez  Se añaden variables globales
 * 1.3   10-03-2021   Juan Carlos Benitez  Se refactoriza methodo por complejidad
 * 1.4   17-03-2021   Juan Carlos Benitez  Se agrega datacustomer.shippingChannel
**/
@SuppressWarnings('sf:UseSingleton,sf:NcssMethodCount,sf:DUDataflowAnomalyAnalysis')
public virtual class MX_SB_PS_Datos_AseguradoExt_Ctrl {
    /*
    * variable compara response
    */
    public static FINAL Integer RESPCODE = 200;
    /**formto fecha */
    public static FINAL String FORMATO = 'yyyy-MM-dd';
    /**Formato salida */
    public static FINAL Integer TWO = 2;
    /**Integer 1 */
    public static FINAL Integer ONE = 1;
    /*var cconyuge para comparacion*/
    public static FINAL String CONYUGE = 'Cónyuge';
    /*var EMPYTTEXT comparacion*/
    public static FINAL String EMPYTTEXT = '';
    /*var StatusCode 204 */
    public static FINAL Integer OK204 =204;
    /* VAR String TipoR*/
    public static FINAL String TIPOR='regular';
    /* VAR String TipoR*/
    public static FINAL String TIPOI='iregular';
    /* Var String BeneficiariesCreated */
    public static Final String CRBEN='CRBEN';
    /* Var String Formalizada */
    public static Final String FRM ='FRM';
    /* Var String Validada */
    public static Final String VAL ='VAL';
    
    /**
    * @description: Methodo creacion
    * informacion de cliente 
    * @author Daniel.perez.lopez.contractor@bbva.com | 11-25-2020 
    * @param String oprt 
    * @param Contact cntc 
    * @param Map<String String> mapdata 
    * @return string 
    **/
    public static List<Map<String,String>> createCustomerData(String oprt,Contact cntc, Map<String,String> mapdata) {
        final List<Map<String,String>> listaReqBen= new List<Map<String,String>>();
        final Opportunity oprtdata = MX_RTL_Opportunity_Selector.getOpportunity(oprt,' AccountId,Account.Genero__c,Account.PersonContact.FirstName, Account.PersonContact.LastName, Account.PersonContact.Name ,SyncedQuoteId,SyncedQuote.BillingStreet,SyncedQuote.BillingCity,SyncedQuote.BillingState,SyncedQuote.BillingPostalCode,SyncedQuote.MX_SB_VTS_Colonia__c,SyncedQuote.MX_SB_PS_PagosSubsecuentes__c ');
        final Set<id> ids = new Set<id>();
            ids.add(oprtdata.SyncedQuoteId);
            final QuoteLineItem[] qliExistente= MX_RTL_QuoteLineItem_Service.getQuoteLIbyQuote(oprtdata.SyncedQuoteId);
            final MX_SB_PS_wrpCreateCustomerData datacustomer = new MX_SB_PS_wrpCreateCustomerData();
            datacustomer.quote.idQuote=qliExistente[0].MX_WB_Folio_Cotizacion__c;
            datacustomer.holder.id=qliExistente[0].MX_SB_VTS_Version__c.Split(',')[0];
            datacustomer.holder.email=cntc.Email;
            datacustomer.holder.rfc=mapdata.get('rfc');
            final MX_SB_PS_wrpCreateCustomerData.mainAddress mainad = dirwraper(cntc,mapdata);
            datacustomer.holder.mainAddress=mainad;
            datacustomer.holder.legalAddress=mainad;
            final MX_SB_PS_wrpCreateCustomerData.mainAddress mainadcorresp = correspdirwraper(oprtdata,mainad);
            datacustomer.holder.correspondenceAddress=mainadcorresp;
            datacustomer.holder.mainContactData= new MX_SB_PS_wrpCreateCustomerData.mainContactData(cntc.MX_WB_ph_Telefono1__c,cntc.MX_WB_ph_Telefono1__c,cntc.MX_WB_ph_Telefono1__c);
            datacustomer.holder.correspondenceContactData= new MX_SB_PS_wrpCreateCustomerData.mainContactData(cntc.MX_WB_ph_Telefono1__c,cntc.MX_WB_ph_Telefono1__c,cntc.MX_WB_ph_Telefono1__c);
            datacustomer.holder.fiscalContactData= new MX_SB_PS_wrpCreateCustomerData.mainContactData(cntc.MX_WB_ph_Telefono1__c,cntc.MX_WB_ph_Telefono1__c,cntc.MX_WB_ph_Telefono1__c);
            datacustomer.holder.physicalPersonalityData=ficalperson(cntc,oprtdata);
            final Datetime datm =System.now();
            final Date bdt = cntc.birthdate;
            final String dateform =DateTime.newInstance(bdt.year(),bdt.month(),bdt.day()).format(FORMATO) +' '+datm.hour()+':'+datm.minute()+':'+datm.second();
            datacustomer.holder.deliveryInformation.referenceStreets=mapdata.get('calle');
            datacustomer.holder.deliveryInformation.deliveryTimeStart=dateform;
            datacustomer.holder.deliveryInformation.deliveryTimeEnd=dateform;
            datacustomer.header.dateRequest=EMPYTTEXT+System.Now().format(FORMATO)+' '+System.now().format('hh:mm:ss.SSS');
            datacustomer.header.dateConsumerInvocation=EMPYTTEXT+System.Now().format(FORMATO)+' '+System.now().format('hh:mm:ss'+'.1');
            datacustomer.shippingChannel.id=mapdata.get('emailQ');
            HttpResponse resp = new HttpResponse();
            final Map<String,String> jsonData= new Map<String,String>();
            jsonData.put('createCD',JSON.serialize(datacustomer));
            final MX_SB_VTS_Beneficiario__c[] listaben = ordeBeneficiarios(oprtdata.syncedquoteid);
            mapdata.put('idcotiza',qliExistente[0].MX_WB_Folio_Cotizacion__c);
	        Final String [] idsaseg =qliExistente[0].MX_SB_VTS_Version__c.Split(',');
            Integer count4map=1;
            for(MX_SB_VTS_Beneficiario__c itb : listaben) {
                if(idsaseg.size()>ONE) { 
                    mapdata.put('idaseg',idsaseg[count4map]);
                } else { 
                    mapdata.put('idaseg','');
                }
                listaReqBen.add(MX_SB_PS_Resumen_Cotizacion_Helper.createCustomWrp(oprtdata,cntc,mapdata,itb));
                count4map++;
            }
            try {
                resp = MX_SB_PS_IASO_Service_cls.getServiceResponseMap('createCustomerData_PS',jsonData);
                if(resp.getStatusCode()==OK204 ) {
                        listaReqBen.add(createQuoteBeneficiary(qliExistente[0].MX_WB_Folio_Cotizacion__c,qliExistente[0].MX_SB_VTS_Version__c.Split(',')[0],oprtdata));
                    } else {
                        throw new AuraHandledException(System.Label.MX_SB_PS_ErrorGenerico);
                    }
            } catch (Exception exp) { 
                throw new AuraHandledException(System.Label.MX_SB_PS_ErrorGenerico+ ' ' + exp);
            }
        return listaReqBen;
    }
    /**
    * @description: Methodo wrp
    * para creacion de beneficiarios
    * de cotizacion 
    * @author Daniel.perez.lopez.contractor@bbva.com | 11-25-2020 
    * @param String idquote 
    * @param String idinsured 
    * @param Opportunity opport 
    * @return string 
    **/
    public static Map<String,String> createQuoteBeneficiary(String idquote, String idinsured, Opportunity opport) {
        final Contact[] conts = MX_RTL_Contact_Service.getContactData(new Set<Id>{opport.AccountId});
        Map<String,String>  response = new Map<String,String>();
        final Datetime dtime2 = System.now();
        final MX_SB_PS_WrpCreateQuoteBeneficiary datacustomer = new MX_SB_PS_WrpCreateQuoteBeneficiary();
        datacustomer.id=idquote;
        final MX_SB_PS_WrpCreateQuoteBeneficiary.insured[] listain = new MX_SB_PS_WrpCreateQuoteBeneficiary.insured[]{};
        final MX_SB_PS_WrpCreateQuoteBeneficiary.insured ins = new MX_SB_PS_WrpCreateQuoteBeneficiary.insured();
        ins.id=idinsured;
        ins.entryDate=DateTime.newInstance(dtime2.year(),dtime2.month(),dtime2.day()).format(FORMATO);
	    Final MX_SB_PS_WrpCreateQuoteBeneficiary.beneficiary[] benefs = new MX_SB_PS_WrpCreateQuoteBeneficiary.beneficiary[]{};
	    Final MX_SB_PS_WrpCreateQuoteBeneficiary.beneficiary bene = new MX_SB_PS_WrpCreateQuoteBeneficiary.beneficiary();
                bene.person.lastName=conts[0].FirstName;
                bene.person.firstName=conts[0].LastName;
                bene.person.secondLastName=conts[0].Apellido_Materno__c;
                bene.percentageParticipation='100';
                bene.relationship.id='0001';
                bene.beneficiaryType.id='01';
                benefs.add(bene);
        ins.beneficiaries=benefs;
        listain.add(ins);
        datacustomer.insuredList=listain;
        final MX_SB_PS_WrpCreateQuoteBeneficiary.technicalInformation techinfo = new MX_SB_PS_WrpCreateQuoteBeneficiary.technicalInformation();
        datacustomer.technicalInformation= techinfo ;
        datacustomer.technicalInformation.dateRequest=EMPYTTEXT+System.Now().format(FORMATO)+' '+System.now().format('hh:mm:ss.SSS');
        datacustomer.technicalInformation.dateConsumerInvocation=EMPYTTEXT+System.Now().format(FORMATO)+' '+System.now().format('hh:mm:ss'+'.1');
        final Map<String,String> jsonData= new Map<String,String>();
        jsonData.put('quotebeneficiary',JSON.serialize(datacustomer));
        response =jsonData;
        return response;
    }
    /**
    * @description: Methodo 
    *   de direccion principal
    *   de cliente
    * @author Daniel.perez.lopez.contractor@bbva.com | 11-25-2020 
    * @param Contact cntc 
    * @param Map<String String> mapdata 
    * @return MX_SB_PS_wrpCreateCustomerData.mainAddress 
    **/
    public static MX_SB_PS_wrpCreateCustomerData.mainAddress dirwraper (Contact cntc,Map<String,String> mapdata) {
        final MX_SB_PS_wrpCreateCustomerData.mainAddress mainad = new MX_SB_PS_wrpCreateCustomerData.mainAddress();
        mainad.zipCode=cntc.MailingPostalCode;
        mainad.streetName=mapdata.get('calle');
        mainad.outdoorNumber=mapdata.get('NumExterior')==EMPYTTEXT? 'SN':mapdata.get('NumExterior');
        mainad.door=mapdata.get('NumInterior')==EMPYTTEXT?'SN':mapdata.get('NumInterior');
        mainad.suburb.neighborhood.catalogItemBase.name=mapdata.get('MX_WB_txt_Colonia');
        mainad.suburb.neighborhood.catalogItemBase.id=mapdata.get('coloniaDefVal');
        return mainad; 
    }    
    /**
    * @description: Methodo 
    *   de direccion principal
    *   de cliente
    * @author Arsenio.perez.lopez.contractor@bbva.com | 11-25-2020 
    * @param Opportunity oprtdata 
    * @param MX_SB_PS_wrpCreateCustomerData.mainAddress mainad 
    * @return MX_SB_PS_wrpCreateCustomerData.mainAddress 
    **/
    public static MX_SB_PS_wrpCreateCustomerData.mainAddress correspdirwraper (Opportunity oprtdata,MX_SB_PS_wrpCreateCustomerData.mainAddress mainad) {
        MX_SB_PS_wrpCreateCustomerData.mainAddress mainadcorresp = new MX_SB_PS_wrpCreateCustomerData.mainAddress();
        if (String.isNotBlank(oprtdata.SyncedQuote.BillingStreet)) {
            Final String[] datastreet = oprtdata.SyncedQuote.BillingStreet.Split(',');
            if (datastreet.size()<TWO) {
                datastreet.add(EMPYTTEXT);
                datastreet.add(EMPYTTEXT);
                datastreet.add(EMPYTTEXT);
            }
            Final String[] datascol = oprtdata.SyncedQuote.MX_SB_VTS_Colonia__c.Split(',');
            if (datascol.size()<TWO) {
                datascol.add(EMPYTTEXT);
                datascol.add(EMPYTTEXT); 
            }
            mainadcorresp.zipCode=oprtdata.SyncedQuote.BillingPostalCode;
            mainadcorresp.streetName=datastreet[0];
            mainadcorresp.outdoorNumber=datastreet[1];
            mainadcorresp.door=datastreet[2];
            mainadcorresp.suburb.neighborhood.catalogItemBase.name=datascol[0];
            mainadcorresp.suburb.neighborhood.catalogItemBase.id=datascol[1];
        } else {
            mainadcorresp=mainad;
        }
        return mainadcorresp;
    }
    /**
    * @description :Methodo para
    * creacion de personasFisicas
    * @author Daniel.perez.lopez.contractor@bbva.com | 11-25-2020 
    * @param Contact cntc 
    * @param Opportunity oprtdata 
    * @return MX_SB_PS_wrpCreateCustomerData.physicalPersonalityData 
    **/
    public static MX_SB_PS_wrpCreateCustomerData.physicalPersonalityData ficalperson(Contact cntc, Opportunity oprtdata) {
        final MX_SB_PS_wrpCreateCustomerData.physicalPersonalityData fiscpers = new MX_SB_PS_wrpCreateCustomerData.physicalPersonalityData();
        fiscpers.name=cntc.FirstName;
        fiscpers.lastName=cntc.LastName;
        final Date bdt = cntc.birthdate;
        final String dateform=DateTime.newInstance(bdt.year(),bdt.month(),bdt.day()).format(FORMATO);
        fiscpers.birthDate=dateform;
        final String genero =oprtdata.Account.Genero__c=='M'?'MASCULINO':'FEMENINO';
        fiscpers.sex.catalogItemBase.id=genero.Substring(0,1);
        fiscpers.sex.catalogItemBase.name=genero;
        fiscpers.mothersLastName=cntc.Apellido_materno__c;
        fiscpers.curp=cntc.MX_RTL_CURP__c;
        fiscpers.civilStatus.catalogItemBase.id='S';
        fiscpers.civilStatus.catalogItemBase.name='SOLTERO';
        return fiscpers;
    }   
    /**
    * @description :Methodo para crear
    * cliente y benefs de servicio
    * @author Daniel.perez.lopez.contractor@bbva.com | 11-25-2020 
    * @param Contact cntc 
    * @param Opportunity oprtdata 
    * @param MX_SB_VTS_Beneficiario__c benef 
    * @return MX_SB_PS_wrpCreateCustomerData.physicalPersonalityData 
    **/
    public static MX_SB_PS_wrpCreateCustomerData.physicalPersonalityData ficalperson(Contact cntc, Opportunity oprtdata,MX_SB_VTS_Beneficiario__c benef) {
        final MX_SB_PS_wrpCreateCustomerData.physicalPersonalityData fiscpers = new MX_SB_PS_wrpCreateCustomerData.physicalPersonalityData();
        fiscpers.name=benef.Name;
        fiscpers.lastName=benef.MX_SB_VTS_APaterno_Beneficiario__c;
        final Date bdt = cntc.birthdate;
        final String dateform=DateTime.newInstance(bdt.year(),bdt.month(),bdt.day()).format(FORMATO);
        fiscpers.birthDate=dateform;
        final String genero =oprtdata.Account.Genero__c=='M'?'MASCULINO':'FEMENINO';
        fiscpers.sex.catalogItemBase.id=genero.Substring(0,1);
        fiscpers.sex.catalogItemBase.name=genero;
        fiscpers.mothersLastName=benef.MX_SB_VTS_AMaterno_Beneficiario__c;
        fiscpers.curp=benef.MX_SB_PS_CURP__c ;
        fiscpers.civilStatus.catalogItemBase.id='S';
        fiscpers.civilStatus.catalogItemBase.name='SOLTERO';
        return fiscpers;
    }  

    /**
    * @description :Methodo para creacion
    * de beeficiarios por consumo
    * @author Daniel Perez Lopez | 12-10-2020 
    * @param mapas 
    * @param opids 
    * @return Boolean 
    **/
    @AuraEnabled
    public static String createBeneficiaries(List<Map<String,String>> mapas, String opids,String tipo) {
        String response=VAL;
        final QuoteLineItem[] qliExistente =MX_RTL_QuoteLineItem_Selector.getQuoteLineitemBy('OpportunityId','id ,UnitPrice,createddate,MX_SB_VTS_Tipo_Plan__c,MX_SB_PS_Dias3a5__c,MX_SB_PS_Dias6a35__c,MX_SB_PS_Dias36omas__c,MX_SB_VTS_Total_Gastos_Funerarios__c,MX_SB_VTS_FormaPAgo__c,MX_WB_noPoliza__c ,MX_WB_Folio_Cotizacion__c ',opids);
        Final List<Quote> quoteLst = MX_RTL_Quote_Selector.resultQryQuote(' Id, Status ','OpportunityId = \'' +opids  + '\'', true);
        if(tipo==TIPOR) {
            HttpResponse httpres = new HttpResponse();
            for (Map<String,String> itm : mapas) {
                httpres = MX_SB_PS_IASO_Service_cls.getServiceResponseMap('CreateQuoteBeneficiary',itm);
                if (httpres.getStatusCode()==RESPCODE) {
                    response=CRBEN;
                }
            }
            if (response==CRBEN) {
                response=createBeneficiariesHelp(opids,tipo);
            }
            if (response==VAL) {
                quoteLst[0].Status='Cotizada';
                MX_RTL_Quote_Selector.upsrtQuote(quoteLst[0]);
                
            }
        } else if(tipo==TIPOI) {
            response=createBeneficiariesHelp(opids,tipo);
        }
        return response;
    }

    /**
    * @description methodo para ordenar los beneficiarios
    * @author Daniel Perez Lopez | 12-10-2020 
    * @param idquote 
    * @return MX_SB_VTS_Beneficiario__c[] 
    **/
    public static MX_SB_VTS_Beneficiario__c[] ordeBeneficiarios (String idquote) {
        final MX_SB_VTS_Beneficiario__c[] listaben = MX_RTL_Beneficiario_Selector.getBenefByAccId('QuoteId',' Id, Name,MX_SB_VTS_Quote__c , MX_SB_VTS_AMaterno_Beneficiario__c, MX_SB_VTS_APaterno_Beneficiario__c, MX_SB_VTS_Porcentaje__c, MX_SB_VTS_Parentesco__c, MX_SB_PS_CURP__c,MX_SB_SAC_Genero__c, MX_SB_SAC_FechaNacimiento__c ',idquote);
        MX_SB_VTS_Beneficiario__c removed=new MX_SB_VTS_Beneficiario__c();
        Integer count4remove=0;
        Integer removeIndex=0;
        Boolean doremove =false;
        for(MX_SB_VTS_Beneficiario__c itb : listaben) {
            if(itb.MX_SB_VTS_Parentesco__c==CONYUGE) {
                removeIndex=count4remove;
                doremove=true;
            }
            count4remove++;
        }
        if (doremove) {
            removed= (MX_SB_VTS_Beneficiario__c) listaben.remove(removeIndex);
            if(listaben.size()==0) {
                listaben.add(removed);    
            } else {
                listaben.add(0,removed);
            }
        }
        return listaben;
    }
    /**
    * @description methodo de ayuda para evitar complejidad y bloques duplicados
    * @author Juan Carlos Benitez Herrera | 10-03-2021
    * @param opids 
    * @param tipo
    * @return response 
    **/
        public static string createBeneficiariesHelp(String opids,String tipo) {
        final QuoteLineItem[] qliExistente =MX_RTL_QuoteLineItem_Selector.getQuoteLineitemBy('OpportunityId','id ,UnitPrice,createddate,MX_SB_VTS_Tipo_Plan__c,MX_SB_PS_Dias3a5__c,MX_SB_PS_Dias6a35__c,MX_SB_PS_Dias36omas__c,MX_SB_VTS_Total_Gastos_Funerarios__c,MX_SB_VTS_FormaPAgo__c,MX_WB_noPoliza__c ,MX_WB_Folio_Cotizacion__c ',opids);
        Final List<Quote> quoteLst = MX_RTL_Quote_Selector.resultQryQuote(' Id, Status ','OpportunityId = \'' +opids  + '\'', true);
        final MX_SB_PS_Datos_Asegurado_Ctrl.wrapercls wrprs =(MX_SB_PS_Datos_Asegurado_Ctrl.wrapercls) JSON.Deserialize(MX_SB_PS_Datos_Asegurado_Ctrl.selectmethod('formaliza',qliExistente[0].MX_WB_Folio_Cotizacion__c),MX_SB_PS_Datos_Asegurado_Ctrl.wrapercls.class);
                Final String response=wrprs.cadena=='ERROR'?CRBEN:FRM;
                if(response==CRBEN) {
                    quoteLst[0].Status='Tarificada';
                } else if(response==FRM) {
                    quoteLst[0].Status='Formalizada';
                }
                MX_RTL_Quote_Selector.upsrtQuote(quoteLst[0]);
        return response;
    }
	    /**
    * @description methodo de ayuda para evitar complejidad y bloques duplicados
    * @author Juan Carlos Benitez Herrera | 10-03-2021
    * @param data 
    * @return responsemethod 
    **/
    public static string selectmethodHelper(String data) {
        String responsemethod ='';
        HttpResponse resq = new HttpResponse();
        try {
            resq = MX_SB_PS_IASO_Service_cls.getServiceResponse('CreateQuote_PS','{"idquote":"'+data+'"}');
                if(resq.getStatusCode()==RESPCODE) {
                    responsemethod=JSON.serialize(resq.getBody());
                    } else {
                        responsemethod= 'ERROR';
                    }
                } catch (Exception e) {
                    responsemethod= 'ERROR';
                }
        return responsemethod;
    }
}