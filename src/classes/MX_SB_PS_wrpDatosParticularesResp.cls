/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_PS_wrpDatosParticularesResp
* Autor Daniel Perez Lopez
* Proyecto: Salesforce Presuscritos
* Descripción : Clase wraper para respuesta de servicio datos particulares  

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           10/12/2019      Daniel Lopez                         Creación
* --------------------------------------------------------------------------------
*/
 @SuppressWarnings('sf:LongVariable, sf:ShortVariable,sf:ShortVariable')
public class MX_SB_PS_wrpDatosParticularesResp {
    /*
    *variable para clase wraper de servicio particular data response
    */
    public iCatalogItem iCatalogItem {get;set;}

    /*
    *Subclase para clase wraper de servicio particulardata response 
    */
    public class iCatalogItem {
        /*
        *Variable planParticularData para subclase de response wraper 
        */
        public planParticularData[] planParticularData {get;set;}   
        /*
        *Variable productPlan para subclase de response wraper 
        */
        public productPlan[] productPlan {get;set;}
    }
    
    
    /*
    *Subclase para clase wraper de servicio particulardata response 
    */
    public class catalogItemBase {
        /*
        *Variable id para subclase de response wraper 
        */
         public string id {get;set;} 
        /*
        *Variable name para subclase de response wraper 
        */
        public String name {get;set;}
        /*
        *Variable description para subclase de response wraper 
        */
        public String description {get;set;}
    }
    /*
    *Subclase para clase wraper de servicio particulardata response 
    */
    public class planParticularData {
        /*
        *Variable particularData para subclase de response wraper 
        */
        public particularData[] particularData {get;set;}
        /*
        *Variable productPlan para subclase de response wraper 
        */
        public productPlan productPlan {get;set;}
    }
    /*
    *Subclase para clase wraper de servicio particulardata response 
    */
    public class particularData {
        /*
        *Variable aliasCriterion para subclase de response wraper 
        */
        public String aliasCriterion {get;set;}	
        /*
        *Variable transformer para subclase de response wraper 
        */
        public transformer[] transformer {get;set;}
        /*
        *Variable description para subclase de response wraper 
        */
        public String description {get;set;}	
        /*
        *Variable type para subclase de response wraper 
        */
        public String type {get;set;}

    }
    
    /*
    *Subclase para clase wraper de servicio particulardata response 
    */
    public class productPlan {
        /*
        *Variable catalogItemBase para subclase de response wraper 
        */
        public catalogItemBase catalogItemBase {get;set;}
        /*
        *Variable planReview para subclase de response wraper 
        */
        public String planReview {get;set;}	
        /*
        *Variable bouquetCode para subclase de response wraper 
        */
        public String bouquetCode {get;set;}
     
    }
    
    /*
    *Subclase para clase wraper de servicio particulardata response 
    */
    public class transformer {
        /*
        *Variable catalogItemBase para subclase de response wraper 
        */
        public catalogItemBase[] catalogItemBase {get;set;}
    }
}