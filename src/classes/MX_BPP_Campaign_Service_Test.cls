/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Campaign_Service_Helper_Test
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-08-10
* @Description 	Test Class for MX_BPP_Campaign_Service_Helper
* @Changes
*  
*/
@IsTest
public class MX_BPP_Campaign_Service_Test {
    
    /** String RecordTypeDeveloperName Campaign */
    static final String RTCAMPAIGNBPP = 'BPyP_Campaign';
    
    /**
    *@Description   Test Method for createCampaignMemberStatus
    *@author 		Edmundo Zacarias
    *@Date 			2020-08-10
    **/
    @isTest
    private static void testCreateCampaignMemberStatus() {
        final Campaign newCampaign= new Campaign();
        newCampaign.Name = 'CampaignBPYP Test';
        newCampaign.IsActive = true;
        newCampaign.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get(RTCAMPAIGNBPP).getRecordTypeId();
        
        Test.startTest();
        insert newCampaign;
        Test.stopTest();
        
        final List<CampaignMemberStatus> results = [SELECT Id FROM CampaignMemberStatus WHERE CampaignId = :newCampaign.Id];
        System.assert(results != null, 'Problema en insert CampaignMemberStatus');
    }

}