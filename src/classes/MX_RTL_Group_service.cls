/**
* Name: MX_RTL_Group_service
* @author Jose angel Guerrero
* Description : Nuevo service 
* Ver                  Date            Author                   Description
* @version 1.0         04/08/2020     Jose angel Guerrero      Initial Version
**/
/**
* @description: funcion constructor
*/
@SuppressWarnings('sf:UseSingleton, sf:DMLWithoutSharingEnabled')
public class MX_RTL_Group_service {
    /**
* @description: funcion constructor
*/
    public static List< Group > serviceGrupoColas (String prefijo) {
        return MX_RTL_Group_Selector.getQueuesByPrefijo(prefijo);
    }
}