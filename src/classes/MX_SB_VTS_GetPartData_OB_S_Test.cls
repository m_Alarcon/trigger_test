/**
* @description       : 
* @author            : Diego Olvera
* @group             : 
* @last modified on  : 11-04-2020
* @last modified by  : Diego Olvera
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   10-21-2020   Diego Olvera   Initial Version
* 1.1   12-01-2021   Diego Olvera   Ajustes a la clase para consumo ASO
**/
@isTest
public class MX_SB_VTS_GetPartData_OB_S_Test {
    /*User name test*/
    private final static String USERTESTOB = 'ASESORDATAPART';
    /*Account name test*/
    private final static String ACCOTESTOB = 'CUENTADATAPART';
    /*URL test*/
    private final static String URLTESTOB = 'http://www.ulrsampleDP2.com';
    /*Product code*/
    private static final String PRODUCTCODESOB = '321';
    /*Alliance code*/
    private static final String ALLIANCECODESOB = 'FHSRT014';
    /*Product code*/
    private static final String PLANCODESOB = '215';
    /*Alliance code*/
    private static final String PLANREVIEWOB = '129';
    
    /* 
     @Method: setupTest
     @Description: create test data set
     */
    @TestSetup
    static void testDatosPartOB() {
        final User tstDpUserTestOB = MX_WB_TestData_cls.crearUsuario(USERTESTOB, 'System Administrator');
        insert tstDpUserTestOB;
        
        System.runAs(tstDpUserTestOB) {
            
            final Account testDpAccOB = MX_WB_TestData_cls.crearCuenta(ACCOTESTOB, System.Label.MX_SB_VTS_PersonRecord);
            testDpAccOB.PersonEmail = 'pruebaVts@mxvts.com';
            insert testDpAccOB;
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'getCoverages', iaso__Url__c = URLTESTOB, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'getGTServicesSF', iaso__Url__c = URLTESTOB, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
        }        
    }
    
    /* 
     @Method: getStatusOTPTest
     @Description: test method to get the status code from web service
     */
    @isTest
    static void  getDatosPartTestOB() {
        final User sampleUsrob = [SELECT Id, Name FROM User WHERE LastName =: USERTESTOB];
        System.runAs(sampleUsrob) {
            final Map<String, String> headersMockTestOB = new Map<String, String>();
            headersMockTestOB.put('tsec', '845697844');
            final Map<String, Object> dataMap = new Map <String, Object>();
            dataMap.put('codigoAlianza', ALLIANCECODESOB);
            dataMap.put('codigoProducto', PRODUCTCODESOB);
            dataMap.put('codigoPlan', PLANCODESOB);
            dataMap.put('revisionPlan', PLANREVIEWOB);
            final MX_WB_Mock mockCalloutOB = new MX_WB_Mock(200, 'Complete', '{}', headersMockTestOB); 
            iaso.GBL_Mock.setMock(mockCalloutOB);
            test.startTest();
            MX_SB_VTS_GetPartData_OB_Service.obtListCarInsCatSrvDp(dataMap);  
            test.stopTest();
            System.assertNotEquals('No entro al metodo', null,'No recuperó nada datos');
        }
    }
}