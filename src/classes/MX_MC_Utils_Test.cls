/**
 * @File Name          : MX_MC_Utils_Test.cls
 * @Description        : Clase para Test de MX_MC_Utils
 * @author             : Eduardo Barrera Martínez
 * @Group              : BPyP
 * @Last Modified By   : Eduardo Barrera Martínez
 * @Last Modified On   : 17/03/2021
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      08/05/2020           Eduardo Barrera Martínez.       Initial Version
 * 1.1      17/03/2021           eduardo.barrera3@bbva.com       Test method for case update
**/
@isTest
public without sharing class MX_MC_Utils_Test {

    /**
     * @Description Clase de prueba para MX_MC_Utils
     * @author eduardo.barrera3@bbva.com | 08/06/2020
    **/
    @TestSetup
    static void makeData () {
         final Account client = new Account();
         client.Name = 'Test Account MC';
         client.No_de_cliente__c = 'test123mc';
         client.Description = 'MC Account for test purposes';
         insert client;
    }

    /**
     * @Description Clase de prueba para MX_MC_Utils
     * @author eduardo.barrera3@bbva.com | 08/05/2020
    **/
    @isTest
    static void testHasDftMsg () {
        final List<MX_MC_MTWConverter_Service_Helper.Message> mxMessageList = new List<MX_MC_MTWConverter_Service_Helper.Message>();
        mxMessageList.add(MX_MC_Utils_Test.createMessage('Mensaje no Draft 1', false));
        mxMessageList.add(MX_MC_Utils_Test.createMessage('Mensaje Draft', true));
        mxMessageList.add(MX_MC_Utils_Test.createMessage('Mensaje no Draft 2', false));
        final Boolean hasDraftMsg = MX_MC_Utils.hasDraftMessages(mxMessageList);
        System.assert(hasDraftMsg,'La lista debe contener mensajes Draft');
    }

    /**
     * @Description Clase de prueba para MX_MC_Utils
     * @author eduardo.barrera3@bbva.com | 08/05/2020
    **/
    @isTest
    static void testIsConvDraft () {
        final List<MX_MC_MTWConverter_Service_Helper.Message> mxMessageList =  new List<MX_MC_MTWConverter_Service_Helper.Message>();
        mxMessageList.add(MX_MC_Utils_Test.createMessage('Test msj no draft 1', false));
        mxMessageList.add(MX_MC_Utils_Test.createMessage('Test msj no draft 2', false));
        mxMessageList.add(MX_MC_Utils_Test.createMessage('Test msj draft', true));
        final Boolean allDraftMessage = MX_MC_Utils.isConvDraft(mxMessageList);
        System.assert(!allDraftMessage, 'La conversación no debe ser Draft');
    }

    /**
     * @Description Clase de prueba para MX_MC_Utils
     * @author eduardo.barrera3@bbva.com | 08/05/2020
    **/
    @isTest
    static void testGetHeaderLocation () {
        final String idResult = MX_MC_Utils.getHeaderLocation('https://www.test.com/conv/v0/conv/message/12');
        System.assertEquals('12', idResult, 'Id de recurso parseado incorrectamente');
    }

    /**
     * @Description Clase de prueba para MX_MC_Utils
     * @author eduardo.barrera3@bbva.com | 08/06/2020
    **/
    @isTest
    static void testCustomerCode () {
        final Account client = [SELECT ID, Name, No_de_Cliente__c FROM Account WHERE Name = 'Test Account MC'];
        final String noClient = MX_MC_Utils.getCustomerCode(client);
        System.assertEquals('TEST123MC', noClient, 'No se ha obtenido el dato correcto del cliente');
    }

    /**
     * @Description Clase de prueba para Case_Handler_cls
     * @author eduardo.barrera3@bbva.com | 17/03/2021
    **/
    @isTest
    static void testChangeCaseStatus () {
        final Case conversation = mycn.MC_ConversationsDatabase.createNewCaseConversation();
        conversation.Status = 'In progress';
        conversation.mycn__MC_ConversationId__c = '1';
        insert conversation;
        conversation.Status = 'Closed';
        update conversation;
        System.assert(!conversation.mycn__MC_ActiveEntitlement__c, 'Campo no actualizado correctamente');
    }

    /**
     * @Description Clase de prueba para MX_MC_Utils
     * @author eduardo.barrera3@bbva.com | 08/05/2020
    **/
    private static MX_MC_MTWConverter_Service_Helper.Message createMessage(String content, Boolean isDraft) {
        final MX_MC_MTWConverter_Service_Helper.Message msg = new MX_MC_MTWConverter_Service_Helper.Message();
        msg.message = content;
        msg.isDraft = isDraft;
        return msg;
    }
}