/**
 * @description       :
 * @author            : Jaime Terrats
 * @group             :
 * @last modified on  : 08-03-2020
 * @last modified by  : Jaime Terrats
 * Modifications Log
 * Ver   Date         Author          Modification
 * 1.0   07-14-2020   Jaime Terrats   Initial Version
**/
@isTest
private without sharing class MX_SB_SAC_Case_DescriptionRules_Test {
    /** assert condition string */
    final static string ASSERT_CONDITION = 'Exito';
    /** error value in assert */
    final static String ASSERT_MSG_ERR = 'Algo fallo en la prueba';
    /** error from vr */
    final static String ERR_MSG = 'Se requieren agregar comentarios para poder guardar';
    /** user name */
    final static String USR_NAME = 'asesor sac';
    /** product type value */
    final static String STATUS = 'Aceptado';
    /** variable for case description */
    final static String LOREM_IPSUM = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum';
    /**
    * @description
    * @author Jaime Terrats | 07-14-2020
    **/
    @TestSetup
    static void makeData() {
        final Account tAccount = new Account(LastName = 'Test');
        insert tAccount;
        final Contract tContract = new Contract(StartDate = System.Today(), AccountId = tAccount.Id);
        insert tContract;
    }

    /**
    * @description
    * @author Jaime Terrats | 08-03-2020
    **/
    @isTest
    static void invokeTest() {
        MX_SB_SAC_CaseValidation_Test.makeData();
        MX_SB_SAC_CaseValidation_Test.testValidateChange();
        System.assert(String.isNotBlank(ASSERT_CONDITION), '');
    }

    /**
    * @description
    * @author Jaime Terrats | 07-14-2020
    **/
    @isTest
    static void invokeTest2() {
        MX_SB_SAC_CaseValidation_Test.makeData();
        MX_SB_SAC_CaseValidation_Test.testValidateChange2();
        System.assert(String.isNotBlank(ASSERT_CONDITION), '');
    }

    /**
    * @description
    * @author Jaime Terrats | 07-14-2020
    **/
    @isTest
    static void invokeTest3() {
        MX_SB_SAC_CaseValidation_Test.makeData();
        MX_SB_SAC_CaseValidation_Test.testValidateChange3();
        System.assert(String.isNotBlank(ASSERT_CONDITION), '');
    }

    /**
    * @description
    * @author Jaime Terrats | 07-14-2020
    **/
    @isTest
    static void invokeTest4() {
        MX_SB_SAC_CaseValidation_Test.makeData();
        MX_SB_SAC_CaseValidation_Test.testValidateChange4();
        System.assert(String.isNotBlank(ASSERT_CONDITION), '');
    }

    /**
    * @description
    * @author Jaime Terrats | 07-14-2020
    **/
    @isTest
    static void invokeTest5() {
        MX_SB_SAC_CaseValidation_Test.makeData();
        MX_SB_SAC_CaseValidation_Test.testValidateChange5();
        System.assert(String.isNotBlank(ASSERT_CONDITION), '');
    }

    /**
    * @description
    * @author Jaime Terrats | 07-14-2020
    **/
    @isTest
    static void invokeTest6() {
        MX_SB_SAC_CaseValidation_Test.makeData();
        MX_SB_SAC_CaseValidation_Test.testValidateChange6();
        System.assert(String.isNotBlank(ASSERT_CONDITION), '');
    }

    /**
    * @description
    * @author Jaime Terrats | 07-14-2020
    **/
    @isTest
    static void invokeTest7() {
        MX_SB_SAC_CaseValidation_Test.makeData();
        final List<Case> tCase = [Select Status, Reason, OwnerId, Description, MX_SB_SAC_FinalizaFlujo__c, MX_SB_SAC_CorreoEnviado__c,
                            MX_SB_SAC_CorreoEnviadoBO__c, MX_SB_SAC_CorreoEnviadoDevolucion__c, MX_SB_SAC_ActivarReglaComentario__c,
                            MX_SB_SAC_EnvioCorreoCierreCaso__c from Case limit 1];
        final List<User> tUser = [Select Id from User where Name =: USR_NAME];
        tCase[0].Status = 'Cerrado';
        tCase[0].MX_SB_SAC_FinalizaFlujo__c = true;
        tCase[0].MX_SB_SAC_ActivarReglaComentario__c = true;
        tCase[0].Reason  = 'Cancelación';
        tCase[0].Description = 'Lorem Ipsum';
        tCase[0].OwnerId = tUser[0].Id;
        update tCase[0];
        System.runAs(tUser[0]) {
            try {
                final Case oldCase = tCase[0];
                tCase[0].Description = '';
                MX_SB_SAC_Case_DescriptionRules_Helper.checkIfClosed(tCase[0], oldCase);
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(System.Label.MX_SB_SAC_CaseClosed), ASSERT_MSG_ERR);
            }
        }
    }

    /**
    * @description
    * @author Jaime Terrats | 07-14-2020
    **/
    @isTest
    static void invokeTest8() {
        MX_SB_SAC_CaseValidation_Test.makeData();
        final List<Case> tCase = [Select Reason, OwnerId, Description, MX_SB_1500_isAuthenticated__c, MX_SB_SAC_Detalle__c,
                            MX_SB_1500_is1500__c, MX_SB_SAC_Contrato__c from Case limit 1];
        final List<User> tUser = [Select Id from User where Name =: USR_NAME];
        final List<Contract> tContract = [Select Id from Contract where CreatedDate = Today limit 1];
        tCase[0].MX_SB_SAC_ActivarReglaComentario__c = true;
        tCase[0].MX_SB_1500_isAuthenticated__c = true;
        tCase[0].MX_SB_1500_is1500__c = true;
        tCase[0].OwnerId = tUser[0].Id;
        update tCase;
        System.runAs(tUser[0]) {
            try {
                tCase[0].MX_SB_SAC_Contrato__c = tContract[0].Id;
                tCase[0].Reason = 'Transferencia';
                tCase[0].MX_SB_SAC_Detalle__c = 'CATEL';
                update tCase[0];
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains(ERR_MSG), ASSERT_MSG_ERR);
            }
        }
    }
}