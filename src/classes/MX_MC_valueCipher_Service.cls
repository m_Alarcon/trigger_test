/**
 * @File Name          : MX_MC_valueCipher_Service.cls
 * @Description        : Clase para llamar al servicio MC_valueCipher
 * @author             : Jair Ignacio Gonzalez Gayosso
 * @Group              : BPyP
 * @Last Modified By   : Jair Ignacio Gonzalez Gayosso
 * @Last Modified On   : 02/06/2020
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		        Modification
 *==============================================================================
 * 1.0      02/06/2020           Jair Ignacio Gonzalez G.   Initial Version
**/
public without sharing class MX_MC_valueCipher_Service {
    /**
     * @Description Status Code 200
    **/
    private static final Integer OKCODE = 200;
    /**
     * @Method MX_MC_valueCipher_Service
     * @Description Singletons
    **/
    @TestVisible private MX_MC_valueCipher_Service() {
    }
    /**
     * @Description manda a llamar al servicio MC_valueCipher y regresa un string
     * @param String cltNumber
     * @return String
     **/
    public static String callValueCipher (String cltNumber, Boolean encrypt) {
        final Map<String, Object> parameters = new Map<String, Object>();
        if (encrypt) {
            parameters.put('type', 'valueEncrypt');
            parameters.put('value', cltNumber);
            parameters.put('mode', 'ENCRYPT_MODE');
        } else {
            parameters.put('type', 'valueDecrypt');
            parameters.put('value', cltNumber);
            parameters.put('mode', 'DECRYPT_MODE');
        }
        final HttpResponse resp =iaso.GBL_Integration_GenericService.invoke('MC_valueCipher', parameters);
        String cltCipher;
        if (resp.getStatusCode() == OKCODE) {
            final MC_valueCipher clsCipher = (MC_valueCipher)JSON.deserialize(resp.getBody(), MC_valueCipher.class);
            cltCipher = clsCipher.results[0];
        } else {
            throw new CalloutException(resp.getBody());
        }
        return cltCipher;
    }

    /**
     * @Description clase para la deserealizacion
     **/
    public class MC_valueCipher {
        /**
         * @Description Lista de resultados
        **/
        private List<String> results;
    }
}