@isTest
/**
* @File Name          : MX_SB_VTS_OutboundRentado_Service_Test.cls
* @Description        :
* @Author             : Marco Antonio Cruz Barboza
* @Group              :
* @Last Modified By   : Marco Antonio Cruz Barboza
* @Last Modified On   : 13/07/2020
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    13/07/2020     Marco Antonio              Initial Version
* 2.0   29/01/2021      Marco Cruz                 Add a test method to check webservice with two cases (success and fail)
**/
@SuppressWarnings('sf:DMLWithoutSharingEnabled, sf:UseSingleton')
private class MX_SB_VTS_OutboundRentado_Service_Test {
    /** Name User */
    final static String USERTEST = 'USERTEST';
    /** Name Account */
    final static String ACCTST = 'TEST ACCOUNT';
    /** Name Opp */
    final static String OPPTST = 'OPP TEST';
    /**URL Mock*/
    Final static String URLTST = 'http://www.example.com';
    /** Mock **/
    Final Static String MOCKRESP = '{"suggestedAmount": {"amount": 9500000,"currency": "MXN"},"suggestedCoverages": [{"id": "SAMINEINCE","suggestedAmount": {"amount": 100000,"currency": "MXN"}}]}';
    /**Insured Amount**/
    Final Static String SAMOCK = '2400000';
    /**Propiedad Tipo Propio**/
    Final Static String PROPIOO = 'Propio';
    /**Propiedad tipo Rentado**/
    Final Static String RENTADOO = 'Rentado';
    
    /* @Method: setUpData
    @Description: create test data set
    */
    @TestSetup
    static void setUpData() {
        final User testerUser = MX_WB_TestData_cls.crearUsuario(USERTEST, System.Label.MX_SB_VTS_ProfileAdmin);
        System.runAs(testerUser) {
            final Account accountTst = MX_WB_TestData_cls.crearCuenta(ACCTST, System.Label.MX_SB_VTS_PersonRecord);
            accountTst.PersonEmail = 'pruebaVts@mxvts.com';
            insert accountTst;
            final Opportunity opportTst = MX_WB_TestData_cls.crearOportunidad(OPPTST, accountTst.Id, testerUser.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            opportTst.LeadSource = 'Call me back';
            opportTst.Producto__c = 'Hogar';
            opportTst.Reason__c = 'Venta';
            opportTst.StageName = 'Cotizacion';
            insert opportTst;
            insert new iaso__GBL_Rest_Services_Url__c(Name = 'getInsuredAmount', iaso__Url__c = URLTST, iaso__Cache_Partition__c = 'local.MXSBVTSCache');
        }
    }
    /*
    @Method: amountValTest
    @Description: return json values of fake response
    */
    @isTest
    static void amountValTest() {
        test.startTest();
            Final object testResponse = MX_SB_VTS_OutboundRentado_Service.amountVal();
            System.assertNotEquals(testResponse, null,'El objeto no se encuentra vacio');
        test.stopTest();
    }
    
    /* 
    @Method: getAmountsTest
    @Description: get an amount lists from a web service
	@Param String insAmount
	@Return List<Object>
    */
    @isTest
    static void getAmountsTest() {
        Final Map<String,String> mockTsec = new Map<String,String> {'tsec'=>'1234456789'};
       	Final MX_WB_Mock calloutTst = new MX_WB_Mock(200, 'Complete',MOCKRESP, mockTsec); 
        iaso.GBL_Mock.setMock(calloutTst);
        test.startTest();
            Final List<object> testResponse = MX_SB_VTS_OutboundRentado_Service.getAmounts(SAMOCK, PROPIOO);
            System.assertNotEquals(testResponse, null,'El objeto no se encuentra vacio');
        test.stopTest();
    }
    
    @isTest
    static void getAmountRentTest() {
        Final Map<String,String> tsecMck = new Map<String,String> {'tsec'=>'1234456789'};
       	Final MX_WB_Mock callTest = new MX_WB_Mock(200, 'Complete',MOCKRESP, tsecMck); 
        iaso.GBL_Mock.setMock(callTest);
        test.startTest();
            Final List<object> testResRent = MX_SB_VTS_OutboundRentado_Service.getAmounts(SAMOCK, RENTADOO);
            System.assertNotEquals(testResRent, null,'El objeto no esta vacio');
        test.stopTest();
    }
    
    @isTest
    static void getAmountTestFail() {
        Final Map<String,String> mckTst = new Map<String,String> {'tsec'=>'1234456789'};
       	Final MX_WB_Mock callTest = new MX_WB_Mock(500, 'Fatal Error','', mckTst); 
        iaso.GBL_Mock.setMock(callTest);
        test.startTest();
            Final List<object> testResRent = MX_SB_VTS_OutboundRentado_Service.getAmounts(SAMOCK, RENTADOO);
            System.assertNotEquals(testResRent, null,'El objeto no esta vacio');
        test.stopTest();
    }
}