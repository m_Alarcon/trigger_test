/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RDB_FichaClienteServiciosTest
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2019-11-29
* @Group  		MX_RDB_FichaCliente
* @Description 	Test class for MX_RDB_FichaClienteServicios.
* @Changes
* 
*/

@isTest
public class MX_RDB_FichaClienteServiciosTest {
	/**
    * --------------------------------------------------------------------------------------
    * @Description Setup method for the Test Class
    * @return N/A
    **/
    @testSetup
    static void setup() {
        //CustomSetting for WS
        MX_RDB_Utilities.createWSRecord('updateElectronicInformation', 'https://maxwebsealdesa.prev.bancomer.mx:6443/customers/');
        MX_RDB_Utilities.createWSRecord('updateCustomer', 'https://maxwebsealdesa.prev.bancomer.mx:6443/customers/');
        
        MX_RDB_Utilities.createAccountRec('TestACRDB', 'TestACRDBLast', 'D0111028');
    }
    
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test method for the class MX_RDB_FichaClienteServicios
    * @return N/A
    **/
	@isTest
    static void fullClassTest() {
        final Account clienteTest = [SELECT Id, FirstName, LastName, MX_SB_BCT_Id_Cliente_Banquero__c, PersonEmail, Phone
                                     FROM Account 
                                     WHERE FirstName = 'TestACRDB' AND LastName = 'TestACRDBLast' LIMIT 1];
        Test.startTest();
        MX_RDB_FichaClienteServicios.updateElectronicInformation(clienteTest);
        Test.stopTest();
        
        System.assertEquals('D0111028', clienteTest.MX_SB_BCT_Id_Cliente_Banquero__c, 'Conflicto con el registro de cliente');
    }
}