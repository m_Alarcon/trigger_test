/**
 * @File Name          : MX_SB_MLT_Servicios_cls
 * @Description        : Clase de ejecución de Servicios
 * @Author             : Daniel Perez Lopez
 * @Last Modified On   : 2/17/2020, 11:14:34 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/17/2020   Daniel Perez Lopez          Initial Version
 * 1.1    2/17/2020   Marco Antonio Cruz          Se agrega method de Robo
 * 1.2    2/17/2020   Marco Cruz                  Corrige Code Smells 
**/
public with sharing class MX_SB_MLT_Servicios_cls { //NOSONAR

    /*
    * Inicialización de lista WorkOrder
    */
    Public static List <WorkOrder> sinList {get;set;}
    
    /*
    * @description Obtiene el tipo de registro Ramo Auto,
    * @param 
    * @return Id RecType
    */
    @auraEnabled
	public static Id getRecType(String devname) {
        return [Select Id From RecordType  Where SobjectType = 'Siniestro__c' and DeveloperName =:devname].Id;
    }
    
    /*
    * @description Obtiene el tipo de Robo
    * @param String Devname, String idSiniestro
    * @return Map<String,String>
    */
    @auraEnabled
	public static Map<String,String> getTipoRobo(String devname,String idSiniestro) {
        Final Map<String,String> respuesta = new Map<String,String>();
        Final Siniestro__c sin = [select id,MX_SB_MLT_TipoRobo__c from siniestro__c where id= :idSiniestro limit 1];
        respuesta.put('tipo',sin.MX_SB_MLT_TipoRobo__c);
        Final String idSin=  [Select Id From RecordType  Where SobjectType = 'Siniestro__c' and DeveloperName =:devname].Id;
        respuesta.put('recordTypeId',idSin);
        
        return respuesta;
    }

    /*
    * @description Crea el servicio de Robo
    * @param String idSiniestro, String idProveedor
    * @return String
    */
    @AuraEnabled
    public static String servicioCreado(String idSiniestro,String idProveedor) {
        String rsltService ='';
        rsltService = MX_SB_MLT_Servicios_Cls_Controller.crearServicio(idSiniestro,idProveedor);
        return rsltService;
    }
    
    /*
    * @description Obtiene mediante booleanos los servicios creados.
    * @param String recId
    * @return Map<string,boolean>
    */
    @AuraEnabled
   	public static Map<String,Boolean> getBools(String recId) {
        Final Map<String,Boolean> valores = new Map<String,Boolean>();
               
        Final Siniestro__c siniBol = [SELECT MX_SB_MLT_RequiereAmbulancia__c, MX_SB_MLT_RequiereGrua__c, MX_SB_MLT_RequiereParamedico__c FROM Siniestro__c WHERE Id=:recId];
        Final Boolean ambulancia = siniBol.MX_SB_MLT_RequiereAmbulancia__c;
        Final Boolean grua = siniBol.MX_SB_MLT_RequiereGrua__c;
        Final Boolean paramedico = siniBol.MX_SB_MLT_RequiereParamedico__c;
        
        valores.put('ambulancia', ambulancia);
        valores.put('grua', grua);
        valores.put('paramedico', paramedico);
        
        return valores;
    }

    /*
    * @description Genera el servicio de asesor robo llamando a Servicios_cls_controlller
    * @param String idSiniestro, String idAjustador
    * @return String
    */
    @AuraEnabled
   	public static String enviarWSRobo(String idSiniestro,String idAjustador) {
        String rsltService ='';
        rsltService = MX_SB_MLT_Servicios_Cls_Controller.crearServicioRobo(idSiniestro,idAjustador);
        return rsltService;
    }

    /*
    * @description Obtiene los datos del servicio de la workorder generada
    * @param String sinid
    * @return Map<String,String>
    */
    @AuraEnabled
    public static Map<String,String> getRoboWO(Id sinid) {
		sinList = new List <WorkOrder> ();
        Final Map <string,string> values = new Map<string,string> ();
        if (sinid != null) {
			sinList = [SELECT Id, MX_SB_MLT_Ajustador__c,MX_SB_MLT_Ajustador__r.Name,MX_SB_MLT_Ajustador__r.MX_SB_MLT_ClaimCenterId__c
                       FROM WorkOrder WHERE MX_SB_MLT_Siniestro__c =: sinid and MX_SB_MLT_EstatusServicio__c NOT in('Cancelado') limit 1 ];
                if(sinList.isEmpty()) {
                    values.put('ajustadorCCId', '');
                    values.put('ajustadorNombre', '');
                    values.put('ajustadorSFId','');
                } else {
                    values.put('ajustadorNombre',sinList[0].MX_SB_MLT_Ajustador__r.Name);
                    values.put('ajustadorCCId', sinList[0].MX_SB_MLT_Ajustador__r.MX_SB_MLT_ClaimCenterId__c);
                    values.put('ajustadorSFId', sinList[0].MX_SB_MLT_Ajustador__c);
                }
		}
		return values;
	}

}