@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_Siniestro2_tst
* Autor Daniel Perez Lopez
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_Siniestro_cls

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           10/12/2019      Daniel Lopez                        Creación
* 1.1           12/03/2020      Angel Nava                        Migración campos contract
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_Siniestro2_tst {
    /*
    *Variable para comparar assert
    */
    final static String NAMES='Gruatest';
    @TestSetup
    static void creaDatos() {
           
            final Account gruaAcc =new Account(name='Gruatest',recuperacion__c=false,Tipo_Persona__c='Física');
            insert gruaAcc;
            final Account ambulanciaAcc =new Account(name='Ambulanciatest',recuperacion__c=false,Tipo_Persona__c='Física');
            insert ambulanciaAcc;
            final Account paramedicoAcc =new Account(name='Paramedicotest',recuperacion__c=false,Tipo_Persona__c='Física');
            insert paramedicoAcc;
            final Contract contrato = new Contract(AccountId=gruaAcc.id,MX_SB_SAC_NumeroPoliza__c='A4234', name='testcontract');
            insert contrato;

            final Siniestro__c sinies= new Siniestro__c();
            sinies.Folio__c='sinitest';
            sinies.MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro';
            sinies.MX_SB_SAC_Contrato__c = contrato.Id;
            insert sinies;

            final Siniestro__c sinies2= new Siniestro__c();
            sinies2.Folio__c='sinitest2';
            sinies2.Poliza__c='folio2';
            sinies2.MX_SB_MLT_Certificado_Inciso__c='1';
            sinies2.MX_SB_MLT_Placas__c='ldny123';
            sinies2.MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro';
            sinies2.MX_SB_MLT_TipoRobo__c='Robo Total';
            sinies2.MX_SB_SAC_Contrato__c = contrato.Id;
            insert sinies2;
            Final String rTCase = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MX_SB_MLT_TomaReporte').getRecordTypeId();
             Final String nameProfile = [SELECT Name from profile where name = 'Asesores Multiasistencia' limit 1].Name;
        final User objUsrTst = MX_WB_TestData_cls.crearUsuario('PruebaAdminTst', nameProfile);  
        insert objUsrTst;
        System.runAs(objUsrTst) {
        	
             final Case objCaseTst = new Case(Nombre_Proveedor__c=gruaAcc.name,MX_SB_MLT_Siniestro__c =sinies2.id,RecordTypeId = rTCase);
            insert objCaseTst;
        } 
    } 

    @isTest static void getValuesFallo() {
        Test.startTest();
        try {
            MX_SB_MLT_Siniestro_cls.getValue(null);
        } catch(Exception e) {
      		System.assertEquals('List has no rows for assignment to SObject',e.getMessage(),'Fallo con exito');
        } 
        Test.stopTest();
    } 
    
    @isTest static void getServiciosEnviadosExito() {
			final Siniestro__c sinies= [SELECT Id,MX_SB_MLT_TipoRobo__c from Siniestro__c where Folio__c='sinitest2' AND MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro' limit 1];        
        Test.startTest();
            final WorkOrder[] casoarray = MX_SB_MLT_Siniestro_cls.getServiciosEnviados(sinies.id);
            MX_SB_MLT_Siniestro_cls.getDatosSiniestro(sinies.id);
        
        	System.assertNotEquals(null,casoarray.size(),'Correcto servicio enviado');
        Test.stopTest();
    } 
    
    @isTest static void getDatosSiniestroFallo() {
        Test.startTest();
        try {
            MX_SB_MLT_Siniestro_cls.getDatosSiniestro(null);
        } catch(Exception e) {
            System.assert(e.getMessage().contains('System.ListException'),'error controlado');
        } 
        Test.stopTest();
    } 
    
    @isTest static void createPartSinExito() {
        	final Siniestro__c sinies= [SELECT Id,MX_SB_MLT_TipoRobo__c from Siniestro__c where Folio__c='sinitest2' AND MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro' limit 1];        
        Test.startTest();
            final Boolean resultado = MX_SB_MLT_Siniestro_cls.CreatePartSin(sinies.id);
        	System.assert(resultado,'Creacion de servicio correcto');
        Test.stopTest();
    } 
        
    @isTest static void createServicesExito() {
        	final Siniestro__c sinies= [SELECT Id,MX_SB_MLT_TipoRobo__c from Siniestro__c where Folio__c='sinitest2' AND MX_SB_MLT_Tipo_de_Da_o_Diverso__c='Efectivo Seguro' limit 1];        
        	final Account prov = [Select id,name from Account where name='Gruatest' limit 1];
        Test.startTest();
            MX_SB_MLT_Siniestro_cls.createServices(sinies.id,prov.id,'','');	
            System.assertNotEquals(null,MX_SB_MLT_Siniestro2_tst.NAMES,' Asert con exito ');
        Test.stopTest();
    } 
    
    @isTest static void getPickList() {
        Test.startTest();
           final String[] respuesta = MX_SB_MLT_Siniestro_cls.getPickList();
        System.assert(respuesta.contains('Robo Total'),'Opcion valida en lista');
        Test.stopTest();
    } 
    
}