/**
 * @description       : Clase de cobertura que da soporte a MX_RTL_MultiAddress_Selector
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 02-24-2021
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   11-12-2020   Eduardo Hernández Cuamatzi   Initial Version
 * 1.1   14-01-2021   Diego Olvera Hernandez       Se agrega función de actualización
 * 1.2   18-02-2021   Alexandro Corzo              se agregan funciones para Cobertura de Seleccion
**/
@isTest
public class MX_RTL_MultiAddress_Selector_Test {

    /**Tipo de domicilio */
    public final static String DOMTYPE = 'Domicilio asegurado';
    /** Variable de Apoyo: sContratos */
    Static String sContratos = 'Contratos';
    /** Variables de Apoyo: sPersonAcct */
    Static String sPersonAcct = 'PersonAccount';
    /** Variable de Apoyo: sVenta */
    Static String sVenta = 'Venta';

    @TestSetup
    static void makeData() {
        final User oUsrTest = MX_WB_TestData_cls.crearUsuario('TestUser', System.label.MX_SB_VTS_ProfileAdmin);
        insert oUsrTest;
        System.runAs(oUsrTest) {
            final Account accToAssign = MX_WB_TestData_cls.crearCuenta('Donnie', 'PersonAccount');
            accToAssign.PersonEmail = 'donnie.test@test.com';
            accToAssign.PersonMobilePhone = '5555555555';
            insert accToAssign;

            final Opportunity oppVal = MX_WB_TestData_cls.crearOportunidad('Opportunity Selector', accToAssign.Id, oUsrTest.Id, System.Label.MX_SB_VTS_Telemarketing_LBL);
            oppVal.Reason__c = sVenta;
            oppVal.Producto__c = System.Label.MX_SB_VTS_Hogar;
            oppVal.StageName = System.Label.MX_SB_VTS_COTIZACION_LBL;
            oppVal.TelefonoCliente__c = accToAssign.PersonMobilePhone;
            oppVal.LeadSource = System.Label.MX_SB_VTS_OrigenCallMeBack;
            insert oppVal;

            final MX_RTL_MultiAddress__c addressType = new MX_RTL_MultiAddress__c();
            addressType.MX_RTL_MasterAccount__c = accToAssign.Id;
            addressType.MX_RTL_AddressStreet__c = 'test';
            addressType.MX_RTL_AddressType__c = DOMTYPE;
            addressType.Name = 'Direccion Propiedad';
            addressType.MX_RTL_Opportunity__c = oppVal.Id;
            insert addressType;
        }
    }

    @isTest
    static void findAddress() {
        final Account accTest = [Select Id, Name from Account where LastName = 'Donnie'];
        final Set<String> setTypes = new Set<String>{DOMTYPE};
        final Set<String> setRecords = new Set<String>{accTest.Id};
        Test.startTest();
            final List<MX_RTL_MultiAddress__c> lstMulAdd = MX_RTL_MultiAddress_Selector.findAddress(setRecords, setTypes);
            System.assertEquals(DOMTYPE, lstMulAdd[0].MX_RTL_AddressType__c, DOMTYPE);
        Test.stopTest();
    }
    
    @isTest
    static void insertAddressTest() {
        final List<MX_RTL_MultiAddress__c> nlstMulAddre = new List<MX_RTL_MultiAddress__c>();
        final MX_RTL_MultiAddress__c addType = new MX_RTL_MultiAddress__c();
        final Account accToAs = MX_WB_TestData_cls.crearCuenta('Taylor', 'PersonAccount');
        accToAs.PersonEmail = 'taylor.test@test.com';
        insert accToAs;
        addType.MX_RTL_MasterAccount__c = accToAs.Id;
        addType.MX_RTL_AddressStreet__c = 'test2';
        addType.MX_RTL_AddressType__c = 'Domicilio cliente';
        nlstMulAddre.add(addType);
        Test.startTest();
        try {
            MX_RTL_MultiAddress_Selector.insertAddress(nlstMulAddre);
        } catch (System.AuraHandledException extError) {
            extError.setMessage('Error');
            System.assertEquals('Fatal Error', extError.getMessage(),'No inserto datos insertAddressTest');
        }
        Test.stopTest();
    }

    @isTest
    static void updateAddressTest() {
        final Account accTest = [Select Id, Name from Account where LastName = 'Donnie'];
        final List<MX_RTL_MultiAddress__c> lstMulAddre = [Select Id, MX_RTL_AddressStreet__c FROM MX_RTL_MultiAddress__c WHERE MX_RTL_MasterAccount__c =: accTest.Id];
        update lstMulAddre;
        Test.startTest();
        try {
            MX_RTL_MultiAddress_Selector.upAddress(lstMulAddre);
        } catch (System.AuraHandledException extError) {
            extError.setMessage('Fatal');
            System.assertEquals('Fatal mistake', extError.getMessage(),'No obtuvo datos updateAddressTest');
        }
        Test.stopTest();
    }

    @isTest
    static void rsttQryMulAddTestNC() {
        Test.startTest();
            final List<MX_RTL_MultiAddress__c> lstMAddrNC = MX_RTL_MultiAddress_Selector.rsttQryMulAdd('Id', '',false);
            System.System.assert(!lstMAddrNC.IsEmpty(), 'Se obtuvieron resultados en la Lista');
        Test.stopTest();
    }

    @isTest
    static void rsttQryMulAddTestYC() {
        Test.startTest();
            final List<MX_RTL_MultiAddress__c> lstMAddrYC = MX_RTL_MultiAddress_Selector.rsttQryMulAdd('Id', 'Name = \'Direccion Propiedad\'',true);
            System.System.assert(!lstMAddrYC.IsEmpty(), 'Se obtuvieron resultados en la Lista');
        Test.stopTest();
    }

    @isTest
    static void upsertAddressTest() {
        final Account accTst = [Select Id, Name from Account where LastName = 'Donnie'];
        final List<MX_RTL_MultiAddress__c> upsMulAddre = [Select Id, MX_RTL_AddressStreet__c FROM MX_RTL_MultiAddress__c WHERE MX_RTL_MasterAccount__c =: accTst.Id];
        update upsMulAddre;
        Test.startTest();
        try {
            MX_RTL_MultiAddress_Selector.upsertAddress(upsMulAddre);
        } catch (System.AuraHandledException extError) {
            extError.setMessage('Error');
            System.assertEquals('Falla mistake', extError.getMessage(),'No obtuvo datos upsertAddressTest');
        }
        Test.stopTest();
    }
}