@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_PS_PlanesFormPag_Ctrl_Test
* Autor: Juan Carlos Benitez Herrera
* Proyecto: SB Presuscritos - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_PS_PlanesFormPag_Ctrl

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         09/07/2020    Juan Carlos Benitez Herrera              Creación
* -----------------------------------------------------------------------------------------------
*/
public class MX_SB_PS_PlanesFormPag_Ctrl_Test {
	/** Nombre de usuario Test */
    final static String NAME = 'Karla';
    /** Apellido Paterno */
    Final static STRING LNAME= 'Santillan';
    
    @TestSetup
    static void createData() {
        MX_SB_PS_OpportunityTrigger_test.makeData();
       	Final Account accTst = [Select Id from Account limit 1];
        accTst.FirstName=NAME;
      	accTst.LastName=LNAME;
       	accTst.Tipo_Persona__c='Física';
        update accTst;
        Final Opportunity oppTest = [Select id from Opportunity where AccountId=:accTst.Id];
        oppTest.Name= NAME;
      	oppTest.StageName=System.Label.MX_SB_PS_Etapa1;
        update oppTest;
    }
    /*
* @description method que prueba LoadOppF
* @param  void
* @return void
*/
    @isTest
    static void testLoadOppF() {
        Final ID idAcc= [SELECT Id from Account where Name=: NAME+' '+LNAME limit 1].Id;
        Final ID idOpp =[SELECT Id from Opportunity where AccountId =:idAcc].Id;
        test.startTest();
        	MX_SB_PS_PlanesFormPag_Ctrl.LoadOppF(idOpp);
            system.assert(true,'Se ha encontrado la oportunidad exitosamente');
        test.stopTest();
    }
    
    @isTest
    static void testupsrtOpp() {
        Final Opportunity oppObj =[SELECT Id, StageName from Opportunity where StageName='Planes y Formas de pago' limit 1];
        oppObj.Plan__c='Individual';
        Test.startTest();
			MX_SB_PS_PlanesFormPag_Ctrl.upsrtOpp(oppObj);
            system.assert(true,'Se ha actualizado la oportunidad corectamente');
        test.stopTest();
    }
    
    @isTest
    static void testgetUpsrtQuote() {
        Final ID idAcc= [SELECT Id from Account where Name=: NAME+' '+LNAME limit 1].Id;
        Final ID idOpp =[SELECT Id from Opportunity where AccountId =:idAcc limit 1].Id;
        Final Quote quotTest = new Quote(Name=NAME ,OpportunityId =idOpp);
        test.startTest();
        	MX_SB_PS_PlanesFormPag_Ctrl.getUpsrtQuote(quotTest);
            system.assert(true,'Se ha actualizado la cotizacion correctamente');
        test.stopTest();
    }
}