/*******************************************************************************
*   @Desarrollado por:      Indra
*   @Autor:                 Roberto Soto
*   @Proyecto:              Bancomer BPyP Retail
*   @Descripción:           Clase test de BPyP_LinkFactset

*   Cambios (Versiones)
*   -------------------------------------------------------------------------- *
*   No.     Fecha               Autor                               Descripción
*   ------  ----------      ----------------------          ---------------------------*
*   1.0     14/10/2019      Roberto Isaac Soto Granados           Creación Clase
*****************************************************************************************/
@isTest
private class BPyP_CalculaAvance_test {
    /*Usuario de pruebas*/
    private static User testUser = new User();
    /*Tema a tratar de pruebas*/
    private static dwp_kitv__Visit_Topic__c testTopic = new dwp_kitv__Visit_Topic__c();
    /*Visita de pruebas*/
    private static dwp_kitv__Visit__c testVisit = new dwp_kitv__Visit__c();
    /*Oportunidad de pruebas*/
    private static Opportunity testOpp = new Opportunity();
    /*Cuenta de pruebas*/
    private static Account testAcc = new Account();
    /*Conjunto de permisos para ejecutar clase*/
    private static PermissionSet permission = new PermissionSet();

    /*Setup para clase de prueba*/
    @testSetup
    static void setup() {
        testUser = UtilitysDataTest_tst.crearUsuario('testUser', 'BPyP Estandar', 'BPYP BANQUERO BANCA PERISUR');
        testUser.Title = 'Privado';
        insert testUser;
        permission = [SELECT Id FROM PermissionSet WHERE Name = 'MX_BPyP_Accesos_SRV'];
        insert new PermissionSetAssignment(AssigneeId = testUser.id, PermissionSetId = permission.Id);
        System.runAs(testUser) {
            testVisit.dwp_kitv__visit_start_date__c = Date.today()+4;
            testVisit.dwp_kitv__visit_duration_number__c = '15';
            testVisit.dwp_kitv__visit_status_type__c = '04';
            insert testVisit;
            testAcc.FirstName = 'Vero';
            testAcc.LastName = 'Torres';
            testAcc.Email_personal__pc = 'vero@test.com';
            insert testAcc;
            testOpp.AccountId = testAcc.Id;
            testOpp.Name = 'Opp test';
            testOpp.StageName = 'Abierta';
            testOpp.CloseDate = Date.today()+4;
            testOpp.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'BPyP_tre_BPyP'].Id;
            insert testOpp;
            testTopic.dwp_kitv__visit_id__c = testVisit.Id;
            testTopic.dwp_kitv__opportunity_id__c = testOpp.Id;
            testTopic.dwp_kitv__topic_desc__c = 'Ok';
            insert testTopic;
        }
    }

    /*Ejecuta la acción para cubrir métodos de la clase*/
    static testMethod void testUserConnectionTrue() {
        testUser = [SELECT Id, Title FROM User WHERE LastName = 'testUser'];
        testVisit = [SELECT Id, dwp_kitv__visit_status_type__c, OwnerId FROM dwp_kitv__Visit__c WHERE OwnerId =: testUser.Id];
        testVisit.dwp_kitv__visit_status_type__c = '05';
        testVisit.OwnerId = testUser.Id;
        //update testVisit;
            System.runAs(testUser) {
            Test.startTest();
                BPyP_CalculaAvance.updateAdvance(new List<String>{testUser.Id});
                update testVisit;
            Test.stopTest();
        }
        testUser = [SELECT Id, BPyP_AvanceMetaProspectos__c FROM User WHERE LastName = 'testUser'];
        System.assertEquals(0, testUser.BPyP_AvanceMetaProspectos__c, 'Conteo correcto');
    }

}