/**
* Indra
* @author           Julio Medellín Oliva
* Project:          Presuscritos
* Description:      Clase de utilidad que brinda distintas creaciones de objetos para clases test.
*
* Changes (Version)
* ------------------------------------------------------------------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    --------------------------------------
* @version  1.0     2020-05-25      Julio Medellín Oliva.         Creación de la Clase
* @version  1.1     2020-10-16      Gerardo Mendoza Aguilar.      Se agregó el campo Producto__c a QFIELDS
* @version  1.2     2021-02-06      Arsenio Perez Lopez.          Se agregó el campo RFC a QFIELDS
*/
@supresswarnings('sf:AvoidFinalLocalVariable')
public with sharing class MX_SB_PS_Utilities { //NOSONAR
 
    /*Property Name Qields*/
      final static String QFIELDS = ' ID, StageName, Producto__c, RecordType.Name, AccountId, Plan__c, GeneroAsegurado__c,Name, SyncedQuoteId, Fecha_Nacimiento__c, Contract.StartDate, MX_SB_VTS_Tipificacion_LV1__c, MX_SB_VTS_Tipificacion_LV2__c, MX_SB_VTS_Tipificacion_LV3__c, MX_SB_VTS_Tipificacion_LV4__c, MX_SB_VTS_Tipificacion_LV5__c, MX_SB_VTS_Tipificacion_LV6__c, MX_SB_VTS_Tipificacion_LV7__c, Account.RFC__c,Account.Name ';
    
  /**
    * @Method bring opp
    * @Description Method para preparar datos de prueba 
    * @return void
    **/ 
   public static Opportunity  fetchOpp(String oppId) {
     return MX_RTL_Opportunity_Selector.getOpportunity(oppId, QFIELDS);       
    }
    /**
    * @Method upsrtOpp
    * @Description Upsert Opportunity 
    * @return void
    **/ 
   public static void upsrtOpp(Opportunity oppObj) {
     MX_RTL_Opportunity_Selector.upsrtOpp(oppObj);
    }
    
    
    /**
    * @Method bring Dynamic Object
    * @Description Method para preparar datos de prueba 
    * @return void
    **/ 
   public static sObject  fetchSobject(String oId,String qFields,String strObject) {
     return MX_SB_RTL_SObject_Selector.getObject(oId, qFields,strObject);       
    }
    
}