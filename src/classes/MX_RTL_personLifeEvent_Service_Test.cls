@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_RTL_personLifeEvent_Service_Test
* Autor: Daniel Ramirez
* Proyecto: SB Presuscritos - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_RTL_personLifeEvent_Service

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         23/10/2020      DanielRamirez                          Creación
* -----------------------------------------------------------------------------------------------
*/
public class MX_RTL_personLifeEvent_Service_Test {
    /** Nombre de usuario Test */
    final static String NAME = 'Mariana';
    /** Apellido Paterno */
    Final static STRING LNAME= 'Esteves';
    
    @TestSetup
    static void createData() {
        Final String proflNameSr = [SELECT Name from profile where name = 'Asesores Presuscritos' limit 1].Name;
        final User userPSTstSr = MX_WB_TestData_cls.crearUsuario(NAME, proflNameSr);
        insert userPSTstSr;
        System.runAs(userPSTstSr) {
            Final String ContactoRTSr =Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('IndustriesIndividual').getRecordTypeId();
            Final Contact contTestSr = new Contact(RecordTypeId = ContactoRTSr, FirstName=NAME, LastName=LNAME);
            insert contTestSr;
            Final PersonLifeEvent personlifeSr = new PersonLifeEvent(Name=Name+LNAME, EventType='Job', EventDate=date.today(), PrimaryPersonId=contTestSr.id);
            insert personlifeSr;
        }
    }
/*
* @description method que prueba getLifeEventbyContactId
* @param  void
* @return void
*/   
    @isTest
    static void getLifeEventbyContactIdTest() {
        Final ID conId= [SELECT Id from Contact where Name=: NAME+' '+LNAME limit 1].Id;
        test.startTest();
        Final List <PersonLifeEvent> personS = MX_RTL_personLifeEvent_Service.getLifeEventbyContactId(conId);
        system.assert(!personS.isEmpty(), 'Personal life encontrado');
        test.stopTest();
    }       
}