/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_FichaProducto_Service
* @Author   	Héctor Israel Saldaña Pérez | hectorisrael.saldana.contractor@bbva.com
* @Date     	Created: 2021-03-29
* @Description 	Service Class for FichaProducto Controller
* @Changes
*
*/
public without sharing class MX_BPP_FichaProducto_Service {

	/** Constructor */
	@TestVisible
	private MX_BPP_FichaProducto_Service() {}

	/*
	 * @description Processes a incoming ContentDocumentLink List and returns a new Set of ContentDocumentId
	 * @param List<ContentDocumentLink> contentDocumentList
	 * @return Set<Id>
	 * */
	public static Set<Id> getDocIds(List<ContentDocumentLink> contentDocLst) {
		final Set<Id> contentDocumentId = new Set<Id>();
		if(!contentDocLst.isEmpty()) {
			for (ContentDocumentLink cdl : contentDocLst) {
				contentDocumentId.add(cdl.ContentDocumentId);
			}
		}

		return contentDocumentId;
	}
}