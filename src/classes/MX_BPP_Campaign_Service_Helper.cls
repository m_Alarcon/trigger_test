/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_Campaign_Service
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-08-10
* @Description 	Service Layer for Campaign
* @Changes
*  
*/
public without sharing class MX_BPP_Campaign_Service_Helper {
    
    /*Contructor clase MX_BPyP_Opportunity_Service */
    private MX_BPP_Campaign_Service_Helper() {}
    
    /**
    * @Description 	Create campaign member status for each new campaign
    * @Param		campaignsId Set<Id>, strStatusList List<String>
    * @Return 		NA
    **/
    public static void createRelatedCMemberStatus(Set<Id> campaignsId, List<String> strStatusList) {
        final List<CampaignMemberStatus> cMemberStatusList = new List<CampaignMemberStatus>();
        CampaignMemberStatus cMemberStatus;
        
        for (Id campaignId : campaignsId) {
            for (String status : strStatusList) {
                cMemberStatus = new CampaignMemberStatus();
                cMemberStatus.CampaignId = campaignId;
                cMemberStatus.Label = status;
                cMemberStatusList.add(cMemberStatus);
            }
        }
        MX_RTL_CampaignMemberStatus_Selector.insertCampaignMemberStatus(cMemberStatusList);
    }
    
}