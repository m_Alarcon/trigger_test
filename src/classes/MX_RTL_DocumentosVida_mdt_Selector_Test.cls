@isTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_RTL_DocumentosVida_mdt_Selector_Test
* Autor: Juan Carlos Benitez Herrera
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_RTL_DocumentosVida_mdt_Selector

* -----------------------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* -----------------------------------------------------------------------------------------------
* 1.0         02/06/2020   Juan Carlos Benitez Herrera               Creación
* -----------------------------------------------------------------------------------------------
*/
public class MX_RTL_DocumentosVida_mdt_Selector_Test {
    /** Var Numero de Kit**/
    Final static String KIT='K01';
    @isTest
    static void testgetDocsDataById() {
        test.startTest();
		MX_RTL_DocumentosVida_mdt_Selector.getDocsDataById(KIT);
	        system.assert(true,'Se han encontrado documentos requeridos');
        test.stopTest();
    }
    @isTest
    static void testConstructor() {
        Final MX_RTL_DocumentosVida_mdt_Selector controlador = new MX_RTL_DocumentosVida_mdt_Selector();
        system.assert(true,controlador);
	}
}