/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 03-19-2021
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   01-22-2021   Eduardo Hernández Cuamatzi   Initial Version
 * 1.1   03-04-2021   Diego Olvera                 Se agrega función para aumentar cobertura
 * 1.1.1 19-03-2021   Diego Olvera                 Se ajusta lista de valores a recuperar para cubrir funcion
**/
@isTest
public class MX_RTL_Cobertura_Selector_Test {

    /**@description etapa Cotizacion*/
    private final static string COTIZACION = 'Cotización';
    /**@description etapa Cotizacion*/
    private final static string RECTYPECRIT = 'MX_SB_VTS_ContractingCriteria';
    /**Campos base de coberturas */
    public final static String LSTFIELDSCOBER = 'MX_SB_VTS_TradeValue__c, MX_SB_VTS_GoodTypeCode__c, MX_SB_VTS_RelatedQuote__c, MX_SB_VTS_CoveragePercentage__c, MX_SB_VTS_CoverageCode__c, MX_SB_VTS_ContractCriterial__c,'+
    'MX_SB_VTS_CodeTrade__c, MX_SB_VTS_CategoryCode__c, MX_SB_MLT_SumaAsegurada__c, MX_SB_MLT_IdeCobertura__c, MX_SB_MLT_Estatus__c, MX_SB_MLT_Descripcion__c, MX_SB_MLT_Cobertura__c, Id, MX_SB_VTS_SelectedPay__c, MX_SB_VTS_PayList__c, MX_SB_VTS_CoversAmount__c, CurrencyIsoCode';
    /*Codigo al que pertenece la cobertura a buscar*/
    private final static String VALTRAD = 'MISC';

    @TestSetup
    static void makeData() {
        MX_SB_VTS_CallCTIs_utility.initHogarGeneric();
        final Opportunity oppIdCrit = [Select Id, Name from Opportunity where StageName =: COTIZACION];
        final Map<Id, Quote> mapQuoteData = new Map<Id, Quote>([Select Id, Name from Quote where OpportunityId =: oppIdCrit.Id]);
        for(Quote quoteItem : mapQuoteData.values()) {
            initCoverages(quoteItem.Id);
        }
        MX_SB_VTS_CallCTIs_utility.initLeadMulServHSD();
    }

    /**
    * @description Incia coberturas basicas
    * @author Eduardo Hernández Cuamatzi | 01-22-2021 
    * @param quoteId Quote Id para coberturas
    **/
    public static void initCoverages(String quoteId) {
        final List<Cobertura__c> lstInitCovers= new List<Cobertura__c>();
        lstInitCovers.add(insertCoverages(quoteId, coverageCode(new List<String>{quoteId, 'MISCELLANEOUS', '300000', 'ELECTRONIC_DEVICES', 'MISC' , 'HOME_BURGLARY', 
        '326.94', 'HOME_AMENITIES', '002,2008PORCMENCASA,0.1,120000.0|003,2008PORCMENCASA,0.2,240000.0', ''} , true), true));
        lstInitCovers.add(insertCoverages(quoteId, coverageCode(new List<String>{quoteId, 'MISCELLANEOUS', '180000', 'CRYSTALS_SIGNS', 'MISC' , 'GLASS_BREAKAGE', 
        '38.73', 'GLASSES', '001,2008PORCCRISTAL,0.11,132000.0|001,2008PORCCRISTAL,0.1,120000.0', ''} , true), true));
        lstInitCovers.add(insertCoverages(quoteId, coverageCode(new List<String>{quoteId, 'Fire', '2000000', 'FIRE', 'INCE' , 'FIRE_LIGHTNING', 
        '6.94', 'CONTENT', '006,2008PORCMENCASA,0.5,120000.0|003,2008PORCMENCASA,0.2,240000.0', ''} , true), true));
        lstInitCovers.add(insertCoverages(quoteId, coverageCode(new List<String>{quoteId, '0.5', '2008PORCSAIC', '006'} , false), false));
        lstInitCovers.add(insertCoverages(quoteId, coverageCode(new List<String>{quoteId, '0.15', '2008PORCCRISTAL', '001'} , false), false));
        lstInitCovers.add(insertCoverages(quoteId, coverageCode(new List<String>{quoteId, '1200000', '2008SA', '001'} , false), false));
        lstInitCovers.add(insertCoverages(quoteId, coverageCode(new List<String>{quoteId, 'NA', 'CODIGO_CUPON', '001'} , false), false));
        MX_RTL_Cobertura_Selector.upsertCoverages(lstInitCovers);
    }

    /**
    * @description Genera coberturas y datos particulares
    * @author Eduardo Hernández Cuamatzi | 01-22-2021 
    * @param quoteId 
    * @param valuesCover 
    * @param isCoverage 
    * @return Cobertura__c 
    **/
    public static Cobertura__c insertCoverages(String quoteId, Map<String, String> valuesCover, Boolean isCoverage) {
        final Cobertura__c cover = new Cobertura__c();
        if(isCoverage) {
            cover.MX_SB_MLT_SumaAsegurada__c = Decimal.valueOf(valuesCover.get('MX_SB_MLT_SumaAsegurada'));
            cover.MX_SB_VTS_CategoryCode__c = valuesCover.get('MX_SB_VTS_CategoryCode');
            cover.MX_SB_VTS_CodeTrade__c = valuesCover.get('MX_SB_VTS_CodeTrade');
            cover.MX_SB_VTS_CoverageCode__c = valuesCover.get('MX_SB_VTS_CoverageCode');
            cover.MX_SB_VTS_CoversAmount__c = valuesCover.get('MX_SB_VTS_CoversAmount');
            cover.MX_SB_VTS_GoodTypeCode__c = valuesCover.get('MX_SB_VTS_GoodTypeCode');
            cover.MX_SB_VTS_PayList__c = valuesCover.get('MX_SB_VTS_PayList');
            cover.MX_SB_VTS_SelectedPay__c = valuesCover.get('MX_SB_VTS_SelectedPay');
            cover.RecordTypeId = Schema.SObjectType.Cobertura__c.getRecordTypeInfosByDeveloperName().get('MX_SB_VTS_CoberturasASO').getRecordTypeId();
        } else {
            cover.MX_SB_MLT_Cobertura__c = valuesCover.get('MX_SB_MLT_Cobertura');
            cover.MX_SB_MLT_Descripcion__c = valuesCover.get('MX_SB_MLT_Descripcion');
            cover.RecordTypeId = Schema.SObjectType.Cobertura__c.getRecordTypeInfosByDeveloperName().get('MX_SB_VTS_ContractingCriteria').getRecordTypeId();
        }
        cover.MX_SB_MLT_Estatus__c = valuesCover.get('MX_SB_MLT_Estatus');
        cover.MX_SB_VTS_RelatedQuote__c = quoteId;
        cover.MX_SB_VTS_TradeValue__c = valuesCover.get('MX_SB_VTS_TradeValue');
        return cover;
    }

    /**
    * @description Mapea datos para coberturas y datos particulares
    * @author Eduardo Hernández Cuamatzi | 01-22-2021 
    * @param latValues Lista de valores
    * @param isCoverage Indica si es cobertura o dato particular
    * @return Map<String, String> 
    **/
    public static Map<String, String> coverageCode(List<String> latValues, Boolean isCoverage) {
        final Map<String, String> mapValues = new Map<String, String>();
        if(isCoverage) {
            mapValues.put('MX_SB_MLT_SumaAsegurada', latValues[2]);
            mapValues.put('MX_SB_VTS_CategoryCode', latValues[3]);
            mapValues.put('MX_SB_VTS_CodeTrade', latValues[4]);
            mapValues.put('MX_SB_VTS_CoverageCode', latValues[5]);
            mapValues.put('MX_SB_VTS_CoversAmount', latValues[6]);
            mapValues.put('MX_SB_VTS_GoodTypeCode', latValues[7]);
            mapValues.put('MX_SB_VTS_PayList', latValues[8]);
            mapValues.put('MX_SB_VTS_SelectedPay', latValues[9]);
            
        } else {
            mapValues.put('MX_SB_MLT_Cobertura', latValues[2]);
            mapValues.put('MX_SB_MLT_Descripcion', latValues[3]);

        }
        mapValues.put('MX_SB_MLT_Estatus', 'Activo');
        mapValues.put('MX_SB_VTS_RelatedQuote', latValues[0]);
        mapValues.put('MX_SB_VTS_TradeValue', latValues[1]);
        return mapValues;
    }

    @isTest
    private static void findCoverByQuote() {
        final Opportunity oppIdCrit = [Select Id, Name from Opportunity where StageName =: COTIZACION];
        final Map<Id, Quote> mapQuoteData = new Map<Id, Quote>([Select Id, Name from Quote where OpportunityId =: oppIdCrit.Id]);
        final String extraQuery = ' AND RecordType.DeveloperName = \''+RECTYPECRIT+'\'';
        Test.startTest();
            final List<Cobertura__c> lstCovers = MX_RTL_Cobertura_Selector.findCoverByQuote(LSTFIELDSCOBER, extraQuery, mapQuoteData.keySet());
            System.assert(!lstCovers.isEmpty(), 'Coberturas recuperadas');
        Test.stopTest();
    }

    @isTest
    private static void findCoverByCode() {
        final Opportunity oppIdCrit = [Select Id, Name from Opportunity where StageName =: COTIZACION];
        final Map<Id, Quote> mapQuoteData = new Map<Id, Quote>([Select Id, Name from Quote where OpportunityId =: oppIdCrit.Id]);
        final List<String> coverageCodes = new List<String>{'FIRE_LIGHTNING'};
        Test.startTest();
            final List<Cobertura__c> lstCovers = MX_RTL_Cobertura_Selector.findCoverByCode(LSTFIELDSCOBER, '', mapQuoteData.keySet(), coverageCodes);
            System.assert(!lstCovers.isEmpty(), 'Coberturas existentes');
        Test.stopTest();
    }

    @isTest
    private static void updateCoverages() {
        final Opportunity oppId = [Select Id, Name from Opportunity where StageName =: COTIZACION];
        final Map<Id, Quote> mapData = new Map<Id, Quote>([Select Id, Name from Quote where OpportunityId =: oppId.Id]);
        final List<String> coverageCodes = new List<String>{'FIRE_LIGHTNING'};
        Test.startTest();
            final List<Cobertura__c> lstCovers = MX_RTL_Cobertura_Selector.findCoverByCode(LSTFIELDSCOBER, '', mapData.keySet(), coverageCodes);
            for(Cobertura__c cover : lstCovers) {
                cover.MX_SB_VTS_SelectedPay__c = 'testSelected';
            }
            MX_RTL_Cobertura_Selector.updateCoverages(lstCovers);
            System.assert(!lstCovers.isEmpty(), 'Coberturas actualizadas');
        Test.stopTest();
    }

    @isTest
    private static void findCoverByTrade() {
        final Opportunity oppIdName = [Select Id, Name from Opportunity where StageName =: COTIZACION];
        final Map<Id, Quote> mapData = new Map<Id, Quote>([Select Id, Name from Quote where OpportunityId =: oppIdName.Id]);
        final List<String> coverCode = new List<String>{'CRYSTALS_SIGNS'};
        coverCode.add('GLASS_BREAKAGE');
        Test.startTest();
            final List<Cobertura__c> lstCob = MX_RTL_Cobertura_Selector.findCoverByTrade(LSTFIELDSCOBER, VALTRAD, mapData.keySet(), coverCode);
            System.assert(!lstCob.isEmpty(), 'Cobertura existente');
        Test.stopTest();
    }
}