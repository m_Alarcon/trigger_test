/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpListCountriesResp
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
 @SuppressWarnings('sf:ShortClassName, sf:ShortVariable')
public class MX_SB_PS_wrpListCountriesResp {
  /*Public property for wrapper*/
	public data[] data {get;set;}
	/*public constructor subclass*/
		public MX_SB_PS_wrpListCountriesResp() {
		 this.data =  new data[] {};
		this.data.add(new data());
		}
  /*Public subclass for wrapper*/
public class data {
        /*Public property for wrapper*/ 
		public String name {get;set;}
		/*Public property for wrapper*/
		 public string id {get;set;} 
		/*public constructor subclass*/
		public data() {
		 this.name = '';
		 this.id = '';
		}
	}    
}