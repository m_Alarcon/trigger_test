/*
* BBVA - Mexico - Seguros
* @Author: Julio Medellín Oliva
* MX_SB_PS_wrpRevisonPlanes
* @Version 1.0
* @ChangeLog
*__________________________________________________________________________________________________
* @version                Author                      date						description
1.0					Julio Medellín                 27/07/2020     Creación del wrapper.*/
 @SuppressWarnings('sf:ShortVariable,sf:LongVariable')
public class MX_SB_PS_wrpRevisonPlanes {
      /*Public property for wrapper*/
        public header header {get;set;}
	  /*Public property for wrapper*/	
        public string areaCode {get;set;}
    /*Public constructor for wrapper*/
    public  MX_SB_PS_wrpRevisonPlanes() {
        header = new header();
		areaCode = '';
    }		
      /*Public subclass for wrapper*/
   public class header {
     /*Public property for wrapper*/
        public string aapType {get;set;}
	  /*Public property for wrapper*/	
        public string dateRequest {get;set;}
	  /*Public property for wrapper*/	
        public string channel {get;set;}
	  /*Public property for wrapper*/	
        public string subChannel {get;set;}
	  /*Public property for wrapper*/	
        public string branchOffice {get;set;}
	  /*Public property for wrapper*/	
        public string managementUnit {get;set;}
	  /*Public property for wrapper*/	
        public string user {get;set;}
	  /*Public property for wrapper*/	
        public string idSession {get;set;}
	  /*Public property for wrapper*/	
        public string idRequest {get;set;}
	  /*Public property for wrapper*/	
        public string dateConsumerInvocation {get;set;}
		/*public constructor subclass*/
		public header() {
		 this.aapType='';	
		 this.dateRequest = '';
		 this.Channel = '';
		 this.SubChannel = '';
		 this.branchOffice = '';
		 this.managementUnit = '';
		 this.user = '';
		 this.IdSession = '';
		 this.idRequest = '';
		 this.dateConsumerInvocation = '';
		}		
        
    }
}