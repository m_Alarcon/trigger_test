/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 02-08-2021
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   02-08-2021   Eduardo Hernández Cuamatzi   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_FillCoveragesTrades_helper {
   /**
    * @description Agrupa coberturas por sus trades, categorias y goodtypes
    * @author Eduardo Hernández Cuamatzi | 02-04-2021 
    * @param lstCovers Lista de coberturas ah agrupar
    * @return Map<String, Object> Mapa de coberturas agrupadas
    **/
    public static Map<String, Object> fillCoverages(List<Cobertura__c> lstCovers) {
        final Map<String,Object> mapCoverages = new Map<String,Object>();
        for (Cobertura__c coverageRec : lstCovers) {
            if(mapCoverages.containsKey(coverageRec.MX_SB_VTS_CodeTrade__c)) {
                final Map<String, Object> mapTrades = (Map<String, Object>)mapCoverages.remove(coverageRec.MX_SB_VTS_CodeTrade__c);
                final Map<String, Object> fillmapTrades = processTrades(coverageRec, mapTrades);
                mapCoverages.put(coverageRec.MX_SB_VTS_CodeTrade__c, fillmapTrades);
            } else {
                final Map<String, Object> mapTrades = new Map<String, Object>();
                final Map<String, Object> fillmapTrades = processTrades(coverageRec, mapTrades);
                mapCoverages.put(coverageRec.MX_SB_VTS_CodeTrade__c, fillmapTrades);
            }
        }
        return mapCoverages;
    }

    /**
    * @description Ordena elementos de coberturas por Trades
    * @author Eduardo Hernández Cuamatzi | 02-04-2021 
    * @param coverageRec Registro de cobertura ah evaluar
    * @param mapTradesCodes Mapa de Coberturas y trades
    * @return Map<String, Object> Mapa actualizado de coberturas y trades
    **/
    public static Map<String, Object> processTrades(Cobertura__c coverageRec, Map<String, Object> mapTradesCodes) {
        final Map<String,Object> mapTrades = mapTradesCodes.clone();
        if(mapTradesCodes.containsKey(coverageRec.MX_SB_VTS_TradeValue__c)) {
            final Map<String, Object> mapCategories = (Map<String, Object>)mapTradesCodes.remove(coverageRec.MX_SB_VTS_TradeValue__c);
            final Map<String, Object> fillmapCategories = processCategories(coverageRec, mapCategories);
            mapTrades.put(coverageRec.MX_SB_VTS_TradeValue__c, fillmapCategories);
        } else {
            final Map<String, Object> mapCategories = new Map<String, Object>();
            final Map<String, Object> fillmapCategories = processCategories(coverageRec, mapCategories);
            mapTrades.put(coverageRec.MX_SB_VTS_TradeValue__c, fillmapCategories);
        }
        return mapTrades;
    }

    /**
    * @description Ordena elementos de coberturas por Categoria
    * @author Eduardo Hernández Cuamatzi | 02-04-2021 
    * @param coverageRec Registro de cobertura ah evaluar
    * @param mapTradesCodes Mapa de Coberturas y trades
    * @return Map<String, Object> Mapa actualizado de coberturas y trades
    **/
    public static Map<String, Object> processCategories(Cobertura__c coverageRec, Map<String, Object> mapTradesCodes) {
        final Map<String,Object> mapGoodTypes = mapTradesCodes.clone();
        if(mapTradesCodes.containsKey(coverageRec.MX_SB_VTS_CategoryCode__c)) {
            final Map<String, Object> mapCategories = (Map<String, Object>)mapTradesCodes.remove(coverageRec.MX_SB_VTS_CategoryCode__c);
            final Map<String, Object> fillmapGoodType = processGoodTypes(coverageRec, mapCategories);
            mapGoodTypes.put(coverageRec.MX_SB_VTS_CategoryCode__c, fillmapGoodType);
        } else {
            final Map<String, Object> mapCategories = new Map<String, Object>();
            final Map<String, Object> fillmapGoodType = processGoodTypes(coverageRec, mapCategories);
            mapGoodTypes.put(coverageRec.MX_SB_VTS_CategoryCode__c, fillmapGoodType);
        }
        return mapGoodTypes;
    }

    /**
    * @description Ordena elementos de coberturas por goodtype
    * @author Eduardo Hernández Cuamatzi | 02-04-2021 
    * @param coverageRec Registro de cobertura ah evaluar
    * @param mapTradesCodes Mapa de Coberturas y trades
    * @return Map<String, Object> Mapa actualizado de coberturas y trades
    **/
    public static Map<String, Object> processGoodTypes(Cobertura__c coverageRec, Map<String, Object> mapTradesCodes) {
        final Map<String,Object> mapGoodTypes = mapTradesCodes.clone();
        if(mapTradesCodes.containsKey(coverageRec.MX_SB_VTS_GoodTypeCode__c)) {
            final List<Cobertura__c> mapCategories = (List<Cobertura__c>)mapTradesCodes.remove(coverageRec.MX_SB_VTS_GoodTypeCode__c);
            mapCategories.add(coverageRec);
            mapGoodTypes.put(coverageRec.MX_SB_VTS_GoodTypeCode__c, mapCategories);
        } else {
            final List<Cobertura__c> mapCategories = new List<Cobertura__c>{coverageRec};
            mapGoodTypes.put(coverageRec.MX_SB_VTS_GoodTypeCode__c, mapCategories);
        }
        return mapGoodTypes;
    }
}