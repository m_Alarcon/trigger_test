/**
* @FileName          : MX_SB_VTS_wrpColoniasResp
* @description       : Wrapper class for use de web service of neighborhoodSearch
* @Author            : Marco Antonio Cruz Barboza  
* @last modified on  : 26-12-2020
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   26-12-2020   Marco Antonio Cruz Barboza          Initial Version
**/
@SuppressWarnings('sf:ShortVariable, sf:VariableDeclarationHidesAnother')
public class MX_SB_VTS_wrpNeighborhood_Utils {
	/** Wrapper Call*/
    Public String comparables {get;set;}
    /** Wrapper HouseList*/
    public houseList[] houseList {get;set;}
    
    /**
    * @description : Object houseList to call a web service in an array
    * @Param 
    * @Return
    **/
    public class houseList {
       	/**
        * @description : String comparablesNumber
        **/
        public string comparablesNumber {get;set;}
        /**
        * @description : String house price
        **/
        public string price {get;set;}
        /**
        * @description : String m2 price on the house
        **/
        public string pricem2 {get;set;}
        /**
        * @description : String surface
        **/
        public String surface {get;set;}
        /**
        * @description : Rooms List
        **/
        public byRooms[] byRooms {get;set;}
        /**
        * @description : comparables house List
        **/
        public comparables[] comparables {get;set;}
    }
    
    /**
    * @description : Object byRooms to call a web service in an array
    * @Param 
    * @Return
    **/
    public class byRooms {
        /**
        * @description : price by room
        **/
        public String price {get;set;}
        /**
        * @description : m2 price by room
        **/
        public String pricem2 {get;set;}
        /**
        * @description : rooms number on the house
        **/
        public String room {get;set;}
        /**
        * @description : surface room
        **/
        public String surface {get;set;}
    }
    
    /**
    * @description : Object comparables to call a web service in an array
    * @Param 
    * @Return
    **/
    public class comparables {
        /**
        * @description :  bathroom rooms number
        **/
        public string bathroom {get;set;}
        /**
        * @description : house description
        **/
        public string description {get;set;}
        /**
        * @description : latitude position
        **/
        public string latitude {get;set;}
        /**
        * @description : longitude position
        **/
        public string longitude {get;set;}
        /**
        * @description : webpage
        **/
        public String linkportal {get;set;}
        /**
        * @description : m2 number
        **/
        public string m2 {get;set;}
        /**
        * @description : house Price
        **/
        public string price {get;Set;}
        /**
        * @description : rooms number
        **/
        public string rooms {get;set;}
        
    }
    
}