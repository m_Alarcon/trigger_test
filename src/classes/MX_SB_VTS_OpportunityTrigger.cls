/*
* BBVA - Mexico
* @Author: Diego Olvera
* MX_SB_VTS_OpportunityTrigger
* @Version 1.0
 * @LastModifiedBy: Diego Olvera
 * @ChangeLog
 * 1.0 Created class - Diego Olvera
 *__________________________________________________________________________________________________
 * @version                Author                 date            description
 1.0                    Diego Olvera              03/10/2019      Creación de la clase.
 1.1                    Francisco Javier          28/10/2019      Se agrega funcion para validar el role del usuario.
 1.2                    Tania Vázquez             02/01/2020      Se agrega method para tipificación inicial de Venta Asistida
 1.2.1                  Eduardo Hernández         12/02/2020      Se agrega funcioón para toques en flujo sac
 1.3 				    Alexandro Corzo		      12/05/2020	  Se agrega función para actualizar el valor del campo "Calificación de Solicitud"
																  dependiendo del horario en que se registro el Lead. El cual puede ser Hot Lead o Cold Lead.
 1.4                    Alexandro Corzo           17/08/2020      Se realizan ajustes de funcionalidad para la función: updateCalSolicitud
 1.5					Alexandro Corzo			  18/03/2021	  e realizan ajustes a la función: updateCalSolicitud por tema de ciclicidad
 */

public without sharing class MX_SB_VTS_OpportunityTrigger {//NOSONAR

    /** Valida contador de llamadas*/
    final static Integer VALIDCALL = 1;

    /**
    * @method: susbtractContactTouchOpp
    * @description: substract counter
    * @params: oppItem <Trigger.new>, oldMap <Trigger.oldMap>
    */
    public static void substractContactTouchOpp(List<Opportunity> oppItem, Map<Id, Opportunity> oldMap) {
        final Id recordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
        for(Opportunity lst : oppItem) {
            final Opportunity oldOpp = oldMap.get(lst.Id);
            final Integer contador = MX_SB_VTS_LeadAutoAssigner.checkNullVals(lst.MX_SB_VTS_ContadorRemarcado__c);
            if (lst.RecordTypeId.equals(recordType) && contador>0 && System.Label.MX_SB_VTS_Nocontacto.equals(lst.MX_SB_VTS_Tipificacion_LV2__c) && String.isBlank(oldOpp.MX_SB_VTS_Tipificacion_LV2__c)) {
                lst.MX_SB_VTS_ContadorRemarcado__c = contador-1;
            }
        }
    }
	/**
	* @method: substracteffectiveContactOpp
    * @description: adds +1 to counter
    * @params: oppItem <Trigger.new>, oldMap <Trigger.oldMap>
	*/
    public static void substractEffectiveContactOpp(List<Opportunity> oppItem, Map<Id, Opportunity> oldMap) {
		final Id recordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
		final Id recorTypeVTA = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTA_VentaAsistida).getRecordTypeId();
		for(Opportunity lst :oppItem) {
			final Opportunity oldOpp = oldMap.get(lst.Id);
			final Integer contador2 = MX_SB_VTS_LeadAutoAssigner.checkNullVals(lst.MX_SB_VTS_Llamadas_Efectivas__c);
			final Integer contador = MX_SB_VTS_LeadAutoAssigner.checkNullVals(lst.MX_SB_VTS_ContadorRemarcado__c);
			if((lst.RecordTypeId.equals(recordType) || lst.recordTypeId.equals(recorTypeVTA)) && System.Label.MX_SB_VTS_ContactoEfectivo.equals(lst.MX_SB_VTS_Tipificacion_LV2__c) && String.isBlank(oldOpp.MX_SB_VTS_Tipificacion_LV2__c)) {
				lst.MX_SB_VTS_Llamadas_Efectivas__c = contador2 + 1;
			}
			if(lst.recordTypeId.equals(RecorTypeVTA) && lst.MX_SB_VTS_Tipificacion_LV1__c == 'Contacto' && String.isBlank(oldOpp.MX_SB_VTS_Tipificacion_LV1__c)) {
				lst.MX_SB_VTS_ContadorRemarcado__c = contador + 1;
			}
		}
	}

	/**
	* @method: substractNoEffectiveContactOpp
    * @description: adds +1 to counter to MX_SB_VTS_Llamadas_No_Efectivas
    * @params: oppItem <Trigger.new>, oldMap <Trigger.oldMap>
	*/
    public static void substractNoEffectiveContactOpp(List<Opportunity> oppItem, Map<Id, Opportunity> oldMap) {
        final Id recordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
        for(Opportunity lst: oppItem) {
            final Opportunity oldOpp = oldMap.get(lst.Id);
            final Integer contador3 = MX_SB_VTS_LeadAutoAssigner.checkNullVals(lst.MX_SB_VTS_Llamadas_No_Efectivas__c);
            if(lst.RecordTypeId.equals(recordType) && System.Label.MX_SB_VTS_Nocontacto.equals(lst.MX_SB_VTS_Tipificacion_LV2__c)  && String.isBlank(oldOpp.MX_SB_VTS_Tipificacion_LV2__c)) {
                lst.MX_SB_VTS_Llamadas_No_Efectivas__c = contador3 + 1;
             }
        }
    }
    /**
	* @method: validateRole
    * @description: Valida que el role del usuario sea valido para editar el registro.
    * @params: List<Opportunity> opps
	*/
    public static void validateRole(List<Opportunity> opps) {
        if(String.isNotBlank(UserInfo.getUserRoleId())) {
            final Id recordTypeOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
            final List<UserRole> lstRole = [Select Id, Name from UserRole where id =: UserInfo.getUserRoleId()];
            final String role = (lstRole.isEmpty()?'':lstRole.get(0).Name);
            final List<MX_SB_VTS_Generica__c> lstRoleInvalid = [Select Id from MX_SB_VTS_Generica__c where Name =: role];
            for(Opportunity opp : opps) {
                if(!lstRoleInvalid.isEmpty() && recordTypeOpp.equals(opp.RecordTypeId)) {
                    opp.addError(System.Label.MX_SB_VTS_ErrorRoleInvalido);
                }
            }
        }
    }

	 /**
	* @method: validaCuentaBanquero
    * @description: valida la cuenta enlazada a la oportunidad para identificar si pertenece a Banquero
    * @params: List<Opportunity> triggerOpportunity
	*/
    public static void validaCuentaBanquero(List<Opportunity> triggerOpportunity) {
        final Set<Id> VarListAccount  = new Set<Id>();
        for(Opportunity vartempAc: triggerOpportunity) {
            VarListAccount.add(vartempAc.AccountId);
        }
        Map<id,account> vartemp2Account = new Map<id,account>([Select Id, ownerid from Account where MX_SB_BCT_Es_Banquero_Contigo__c = true and Id IN :VarListAccount]);
        for(Opportunity oppItem: triggerOpportunity) {
            if(vartemp2Account.keySet().contains(oppItem.AccountId)) {
                oppItem.ownerId = vartemp2Account.get(oppItem.AccountId).ownerId;
                if( !oppItem.RecordTypeId.equals(schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('MX_RDB_Opp_Preaprobados').getRecordTypeId())
                   && !oppItem.RecordTypeId.equals(schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('MX_RDB_Opp_NoPreaprobados').getRecordTypeId()) ) {
                	oppItem.RecordTypeId = schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_BCT_Banquero).getRecordTypeId();
                }
            }
        }
	}

	 /**
	 /**
	* @method: tipificacionInicialVTA
    * @description: Actualiza la tipificación de las Oportunidades de VTA y
    * @params: List<Opportunity> triggerOpps
	*/
        public static void tipificacionInicialVTA(List<Opportunity> triggerOpps) {
        final Id recordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTA_VentaAsistida).getRecordTypeId();
        for(Opportunity vartempAc: triggerOpps) {
             if(String.isNotEmpty(vartempAc.Producto__c) && vartempAc.Producto__c.equals(label.MX_SB_VTS_SeguroEstudia_LBL) && vartempAc.RecordTypeId.equals(recordType) && vartempAc.StageName.equals(label.MX_SB_VTS_COTIZACION_LBL)) {
                vartempAc.MX_SB_VTS_Tipificacion_LV1__c = 'Contacto';
                varTempAc.MX_SB_VTS_Tipificacion_LV2__c = 'Contacto Efectivo';
                varTempAc.MX_SB_VTS_Tipificacion_LV3__c = 'Interesado';
                vartempAc.MX_SB_VTS_M_todo_de_Venta__c = 'Venta Asistida Digital';
				vartempAc.MX_WB_comentariosCierre__c = 'Ninguno';
				if(vartempAc.LeadSource.equals(label.MX_SB_VTS_SAC_LBL)) {
                    vartempAc.MX_SB_VTS_ContadorLlamadasTotales__c = 1;
                    vartempAc.MX_SB_VTS_ContadorRemarcado__c  = 1;
                    vartempAc.MX_SB_VTS_Llamadas_Efectivas__c = 1;
                }
            }
        }
	}

	/**
	 * @method: addContactshOpp
	 * @description: add counter
	 * @params: oppItem <Trigger.new>, oldMap <Trigger.oldMap>
	 */
	public static void addContactshOpp(List<Opportunity> oppItem, Map<Id, Opportunity> oldMap) {
		final Id recordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTA_VentaAsistida).getRecordTypeId();
		for(Opportunity lst :oppItem) {
			final Opportunity oldOpp = oldMap.get(lst.Id);
			final Integer contador = MX_SB_VTS_LeadAutoAssigner.checkNullVals(lst.MX_SB_VTS_ContadorLlamadasTotales__c);
			if(lst.RecordTypeId.equals(recordType) && String.isBlank(oldOpp.MX_SB_VTS_Tipificacion_LV1__c) && String.isNotBlank(lst.MX_SB_VTS_Tipificacion_LV1__c)) {
				lst.MX_SB_VTS_ContadorLlamadasTotales__c = (contador + 1);
			}
		}
	}

    /**
     * @method: updateCalSolicitud
     * @description: Actualiza campo "Clasificación de Solicitud" conforme a hora de insercion.
     * @params: List<Opportunity> oppItems
     */
    public static void updateCalSolicitud(List<Opportunity> oppItem) {
        for(Opportunity oRowOpp :  oppItem) {
            final DateTime dtCreatedDate = System.now();
            if (dtCreatedDate.hour() >=9 && dtCreatedDate.hour() <=21) {
                oRowOpp.MX_SB_VTS_CalificacionSolicitudOp__c = 'Hot Lead';
            } else {
                oRowOpp.MX_SB_VTS_CalificacionSolicitudOp__c = 'Cold Lead';
            }
        }
    }

    /**
    * @method: mayorAvanceTip
    * @description: EValuacion de la Yipificación de mayor avance
    * @params: List<Opportunity> oppItem
    */
    public static void mayorAvanceTip(List<Opportunity> oppItem) {
        final Id recordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
        for(Opportunity oppVal :oppItem) {
            if(String.isNotBlank(oppVal.RecordTypeId) && oppVal.RecordTypeId.equals(recordType) && String.isNotBlank(oppVal.MX_SB_VTS_Tipificacion_LV6__c) && String.isNotBlank(oppVal.MX_SB_VTS_Tipificacion_LV5__c)) {
                final MX_SB_VTS_Avance_Tipificaci_n__mdt gradoAvanUp = MX_SB_VTS_LeadTrigger.recoAvanceTip(oppVal.MX_SB_VTS_Tipificacion_LV6__c, oppVal.MX_SB_VTS_Tipificacion_LV5__c);
                if(oppVal.MX_SB_VTS_Grado_de_Avance__c <= Integer.valueof(gradoAvanUp.MX_SB_VTS_Ponderaci_n__c) || String.isBlank(String.valueOf(oppVal.MX_SB_VTS_Grado_de_Avance__c))) {
                    oppVal.MX_SB_VTS_Motivo_de_mayor_avance__c = oppVal.MX_SB_VTS_Tipificacion_LV6__c;
                    oppVal.MX_SB_VTS_Grado_de_Avance__c = gradoAvanUp.MX_SB_VTS_Ponderaci_n__c;
                }
            }
        }
    }

        /**
    * @author: Diego Olvera
    * @method: updateVisit
    * @description: Recupera valores de Oportunidad al actualizar Tipificacion_LV6, los procesa y el resultado lo inserta en el objeto visita.
    * @params: List<Opportunity> oppItem
    */
    public static void updateVisit(List<Opportunity> oppItems) {
        final Id recordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
        final Set<Id> lstTask = new Set<Id>();
        final Set<Id> lstVisit = new Set<Id>();
        final Map<Id, Id> mapLeadId = new Map<Id, Id>();
        final Map<Id, Id> mapTaskId = new Map<Id, Id>();
        for(Opportunity oppItem: oppItems) {
            if(String.isNotBlank(oppItem.MX_SB_VTS_LookActivity__c)) {
                lstTask.add(oppItem.MX_SB_VTS_LookActivity__c);
                mapLeadId.put(oppItem.Id, oppItem.MX_SB_VTS_LookActivity__c);
            }
        }

        for(Task visitOpp : [Select Id,dwp_kitv__visit_id__c from Task where Id IN: lstTask AND dwp_kitv__visit_id__c NOT IN ('')]) {
            mapTaskId.put(visitOpp.Id, visitOpp.dwp_kitv__visit_id__c);
            lstVisit.add(visitOpp.dwp_kitv__visit_id__c);
        }

        final Map<Id, dwp_kitv__Visit__c> visitOpps = new Map<Id, dwp_kitv__Visit__c>([SELECT Id, MX_SB_VTS_recordNC__c, MX_SB_VTS_recordCE__c, MX_SB_VTS_recordCNE__c,  dwp_kitv__lead_id__c, MX_SB_VTS_PenContactEfect__c, MX_SB_VTS_IsInterest__c, MX_SB_VTS_PendInterest__c,
            MX_SB_VTS_NoAceptCotiz__c, MX_SB_VTS_AceptaCotiz__c, MX_SB_VTS_AceptContra__c FROM dwp_kitv__Visit__c where Id IN: lstVisit]);
        final List<dwp_kitv__Visit__c> lstVisits = new List<dwp_kitv__Visit__c>();
        for(Opportunity oppItem: oppItems) {
            if(oppItem.RecordTypeId.equals(recordType) && String.isNotBlank(oppItem.MX_SB_VTS_Tipificacion_LV6__c) && String.isNotBlank(oppItem.MX_SB_VTS_LookActivity__c) && visitOpps.containsKey(mapTaskId.get(mapLeadId.get(oppItem.Id)))) {
                final dwp_kitv__Visit__c visitOpp = evauateVisit(oppItem, mapTaskId, visitOpps, mapLeadId);
                lstVisits.add(visitOpp);
            }
        }
        update lstVisits;
    }

    /**
    * @description Valida las tipificaciones
    * @author Eduardo Hernandez Cuamatzi | 07-15-2020
    * @param Opportunity oppItem Opportunity ah evaluar
    * @param Map<Id Id> mapTaskId Mapa de tareas
    * @param Map<Id dwp_kitv__Visit__c> visitObjects mapa de visitas
    * @param Map<Id Id> mapLeadId Mapa de leads y tareas
    * @return dwp_kitv__Visit__c Visita actualizada
    **/
    public static dwp_kitv__Visit__c evauateVisit(Opportunity oppItem, Map<Id, Id> mapTaskId, Map<Id, dwp_kitv__Visit__c> visitObjects, Map<Id, Id> mapLeadId) {
        decimal noContact = 0;
        decimal noLlaefect = 0;
        decimal llaEfect = 0;
        string gradoAvance = '', motivoAvance = '',stageName= '';
        Boolean noCtact = false;
        noContact = oppItem.MX_SB_VTS_ContadorLlamadasTotales__c - oppItem.MX_SB_VTS_ContadorRemarcado__c;
        if(oppItem.MX_SB_VTS_ContadorLlamadas__c >= VALIDCALL) {
            noLlaEfect = 1;
        }
        if(oppItem.MX_SB_VTS_Llamadas_Efectivas__c >= VALIDCALL) {
            llaEfect = 1;
        }
        if(noContact>=0 && noLlaefect ==0 && llaEfect ==0) {
            noCtact = true;
        }
        final Map<String, Boolean> efectivos = MX_SB_VTS_LeadAutoAssigner.pendEfectivo(oppItem);
        gradoAvance = String.valueOf(oppItem.MX_SB_VTS_Grado_de_Avance__c);
        motivoAvance = oppItem.MX_SB_VTS_Motivo_de_mayor_avance__c;
        stageName = oppItem.StageName;
        final dwp_kitv__Visit__c visitOpp = visitObjects.get(mapTaskId.get(mapLeadId.get(oppItem.Id)));
        visitOpp.MX_SB_VTS_recordNC__c = noContact;
        visitOpp.MX_SB_VTS_recordCE__c = llaEfect;
        visitOpp.MX_SB_VTS_recordCNE__c = noLlaefect;
        visitOpp.MX_SB_VTS_PenContactEfect__c = efectivos.get('penContactEfect');
        visitOpp.MX_SB_VTS_IsInterest__c = efectivos.get('isInterest');
        visitOpp.MX_SB_VTS_PendInterest__c = efectivos.get('pendInterest');
        visitOpp.MX_SB_VTS_AceptContra__c = efectivos.get('aceptContra');
        visitOpp.MX_SB_VTS_AceptaCotiz__c = efectivos.get('aceptaCotiz');
        visitOpp.MX_SB_VTS_NoAceptCotiz__c = efectivos.get('noAceptCotiz');
        visitOpp.MX_SB_VTS_GradoAvance__c = gradoAvance;
        visitOpp.MX_SB_VTS_MotivoAvance__c = motivoAvance;
        visitOpp.MX_SB_VTS_IsContact__c = efectivos.get('isContact');
        visitOpp.MX_SB_VTS_NoContact__c = noCtact;
        visitOpp.MX_SB_VTS_EtapaOportunidad__c = stageName;
        return visitOpp;
    }
}