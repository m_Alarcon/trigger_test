/**
* @File Name          : MX_SB_PS_Datos_Asegurado_Ctrl.cls
* @Description        :
* @Author             : Juan Carlos Benitez
* @Group              :
* @Last Modified By   : Arsenio.perez.lopez.contractor@bbva.com
* @Last Modified On   : 01-27-2021
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    6/24/2020   Juan Carlos Benitez          Initial Version
* 1.1    1/27/2021   Arsenio Perez Lopez         Se añade campo codigo lugar
* 1.2    3/08/2021   Juan Carlos Benitez         Se añade valor QuoteLineItem en wrappercls
* 1.3    3/08/2021   Juan Carlos Benitez         Se añade Status  en getDirData
* 1.4    3/08/2021   Juan Carlos Benitez         Se cambia tipo de retorno de boolean a string 
* 1.5    3/10/2021   Juan Carlos Benitez         Se refactoriza when formaliza por complejidad
**/
@SuppressWarnings('sf:UseSingleton')
public with sharing class MX_SB_PS_Datos_Asegurado_Ctrl extends MX_SB_PS_Datos_AseguradoExt_Ctrl {
    /** list of records to be retrieved */
    final static List<Account> ACC_DATA = new List<Account>();
    /** list of records to be retrieved */
    final static List<Opportunity> OPP_DATA = new List<Opportunity>();
    /** list of records to be retrieved */
    final static List<Contact> CNTCT_DATA = new List<Contact>();
    /*Variable PERIODO*/
    final static integer PERIOD = 12;
    /*Variable STATUS 200*/
    final static integer OKSTATUS = 200;
    /*Variable PERIODO*/
    final static String FSTNAME = 'fname';
    /*
    * wraper de clase para respuesta a front
    */
    public class wrapercls {
        /*variable de retorno para front */
        public Opportunity opport {get;set;}
        /*variable de retorno para front */
        public Account[] accounts {get;set;}
        /*variable de retorno para front */
        public Contact[] contacts {get; set;}
        /*variable de retorno para front */
        public Contract[] contracts {get;set;}
        /*variable de retorno para front */
        public MX_SB_VTS_Beneficiario__c[] benes {get;set;}
        /*variable de retorno para front */
        public String cadena {get;set;}
        /*variable de retorno para front */
        public Quote[] quotes {get;set;}
        /*variable de retorno para front*/
        public QuoteLineItem[] quolis {get;set;}
    }
    
    /**
    * @description 
    * @author Daniel Perez Lopez | 11-11-2020 
    * @param mtname 
    * @param data 
    * @return String 
    **/
    @AuraEnabled
    public static String selectmethod (String mtname , String data) {
        final wrapercls resp = new wrapercls();
        final Set<Id> idsquery = new Set<Id>();
        idsquery.clear();
        Switch on mtname {
            when 'loadOppF' {
                resp.opport= MX_SB_PS_Utilities.fetchOpp(data);
            }
            when 'loadAccountF' {
                idsquery.add(data);
                resp.accounts = MX_RTL_Account_Service.getSinHData(idsquery);
            }
            when 'loadContactAcc' {
                idsquery.add(data);
                resp.contacts = MX_RTL_Contact_Service.getContactData(idsquery);
            }
            when 'getContract' {
                resp.contracts = MX_RTL_Contract_Service.getContractIdByOppId(data);
            }
            when 'srchBenefs' {
                resp.benes = MX_RTL_Beneficiario_Service.getbenefbyValue(data);
            }
            when 'getDirData' {
                Quote[] listq = MX_RTL_Quote_Service.getUpsquot(new quote(OpportunityId=data));
                listq[0] =MX_RTL_Quote_Selector.findQuoteById(listq[0].id,' BillingStreet,BillingCity,BillingState,BillingPostalCode,MX_SB_VTS_Colonia__c,Status ');
                resp.quotes = listq;
            }
            when 'getQuoli' {
                resp.quolis= MX_RTL_QuoteLineItem_Service.getQuoteLIbyQuote(data);
            }
            when 'formaliza' {
                resp.cadena= MX_SB_PS_Datos_AseguradoExt_Ctrl.selectmethodHelper(data);
            }
        }
        return JSON.serialize(resp);
    }

    /**
    * @description 
    * @author Juan Carlos Benitez | 20-11-2020 
    * @param obj 
    * @param cntc 
    * @param cntrct 
    **/
    @AuraEnabled
    public static void supsert(String tipo, String objct) {
       Switch on tipo{
            when 'contacto' {
                 final Contact contac =(Contact)JSON.deserialize(objct, Contact.Class);
                 final Contact contac2 = new Contact(id = contac.id, FirstName = contac.FirstName, LastName=contac.LastName,MX_RTE_Codigo_Lugar__c=contac.MX_RTE_Codigo_Lugar__c,Birthdate=contac.Birthdate,MX_RTL_CURP__c=contac.MX_RTL_CURP__c,
                                                     Apellido_materno__c=contac.Apellido_materno__c,Email=contac.Email,MailingPostalCode=contac.MailingPostalCode,
                                                    MailingCity=contac.MailingCity,MailingState=contac.MailingState,MX_WB_ph_Telefono1__c=contac.MX_WB_ph_Telefono1__c,
                                                    MX_WB_ph_Telefono2__c=contac.MX_WB_ph_Telefono2__c,MailingStreet=contac.MailingStreet);
                 MX_RTL_Contact_Service.upsrtCntc(contac2);
            }
            when 'contrato' {
                final Contract contrat = (Contract)JSON.deserialize(objct, Contract.Class);
                MX_RTL_Contract_Service.upsertSinCont(contrat);
            }
            when 'cuenta' {
                final Account cuentas = (Account)JSON.deserialize(objct, Account.Class);
                MX_RTL_Account_Selector.updteAccount(cuentas);
            }
        } 
    }
 
    /*
* @description
* @author Juan Carlos Benitez | 5/25/2020
* @param String zipcode
**/
    @AuraEnabled
    public static String getcolonias(String zipcode) {
        MX_SB_PS_wrpColoniasResp respCol;
        final Map<String,String> retmap = new Map<String,String>();
        HttpResponse resp = new HttpResponse();
        try {
            resp = MX_SB_PS_IASO_Service_cls.getServiceResponse('GetListDirectionByCP','{"ZIPCODE":"'+zipcode+'"}');
        } catch(Exception exp) {
            respCol = new MX_SB_PS_wrpColoniasResp();
        }
        respCol = ( MX_SB_PS_wrpColoniasResp)JSON.deserialize(resp.getBody(), MX_SB_PS_wrpColoniasResp.class);
        retmap.put('city',JSON.serialize(respCol.iCatalogItem.suburb[0].city));
        retmap.put('county',JSON.serialize(respCol.iCatalogItem.suburb[0].county));
        retmap.put('state',JSON.serialize(respCol.iCatalogItem.suburb[0].state));
        final MX_SB_PS_wrpColoniasResp.neighborhood[] colonias = new MX_SB_PS_wrpColoniasResp.neighborhood[]{};
            for (MX_SB_PS_wrpColoniasResp.suburb item : respCol.iCatalogItem.suburb) {
                colonias.add(item.neighborhood);
            }
        retmap.put('colonias',JSON.serialize(colonias));
        return JSON.serialize(respCol);
    }
    /**
    * @description 
    * @author Daniel Perez Lopez | 11-11-2020 
    * @param oprt 
    * @param cntc 
    * @param mapdata 
    * @return string 
    **/
    @AuraEnabled
    public static List<Map<String,String>> createCustomData(String oprt,Contact cntc, Map<String,String> mapdata) {
        return MX_SB_PS_Datos_AseguradoExt_Ctrl.createCustomerData(oprt, cntc, mapdata);
    }
    /**
* @description
* @author Juan Carlos Benitez | 04/09/2020
**/
    @AuraEnabled
    public static Quote saveDirData (Quote newdir) {
        final Quote[] listq = MX_RTL_Quote_Service.getUpsquot(newdir);
        if (listq.size()>0) {
            newdir.id=listq[0].id;
        }
        return MX_RTL_Quote_Service.upserQuote(newdir);
    }
    /**
    * @description 
    * @author Daniel Perez Lopez | 11-11-2020 
    * @return String 
    **/
    @AuraEnabled
    public static String gEstados() {
        MX_SB_PS_wrpEstadosResp respEstado;
        HttpResponse resp = new HttpResponse();
        try {
            resp = MX_SB_PS_IASO_Service_cls.getServiceResponse('GetListStates','{"countryId":"MX"}');
            respEstado = (MX_SB_PS_wrpEstadosResp) JSON.deserialize(resp.getBody(),MX_SB_PS_wrpEstadosResp.class);
        } catch(Exception e) {
            respEstado = new MX_SB_PS_wrpEstadosResp();
        }
        return JSON.serialize(respEstado);
    }
    /**
* @description
* @author Daniel Alberto Ramirez Islas | 15/10/2020
**/
    @AuraEnabled
    public static void contractParam(Map<String,String> mapa, String productName,String ide) {
        final Contract cont = new Contract();
            cont.StartDate = Date.parse(mapa.get('StartDate'));
       	    cont.BillingCity = mapa.get('BillingCity');
            cont.BillingCountry = mapa.get('BillingCountry');
            cont.BillingPostalCode = mapa.get('BillingPostalCode');
            cont.BillingState = mapa.get('BillingState');
            cont.BillingStreet = mapa.get('BillingStreet');
            cont.AccountId = mapa.get('AccountId');
            cont.MX_SB_SAC_NumeroPoliza__c = mapa.get('NumeroPoliza');
            cont.MX_WB_Oportunidad__c = mapa.get('oportunidad');
            cont.Status = mapa.get('Status');
            cont.MX_WB_origen__c = mapa.get('origen');
            MX_RTL_Contract_Service.upsertContract(cont, productName, PERIOD,ide);
    }

    /*
    * @description obtiene el curp
    * @param Contact cntc String estadonac
    * @return String
    */
    @AuraEnabled
    public static String getCurp(Contact cntc,String estadonac,String genero) {
        String strresp='';
        HttpResponse resp = new HttpResponse();
        final Map<String,String> mapd = new Map<String,String>();
        mapd.put('lname',cntc.LastName);
        mapd.put('sex',genero.toUppercase().startsWith('M')?'H':'M');
        mapd.put('state',estadonac);
        mapd.put(FSTNAME,cntc.FirstName);
        if(mapd.get(FSTNAME).contains('')) {
            String name = mapd.get(FSTNAME);
            name =name.replace(' ','%20');
            mapd.put(FSTNAME,name);
        }
        final Date bdt = cntc.birthdate;
        final String dateform=DateTime.newInstance(bdt.year(),bdt.month(),bdt.day()).format('yyyy-MM-dd');
        mapd.put('birthDate',dateform);
        resp = MX_SB_PS_IASO_Service_cls.getServiceResponseMap('getCurp',mapd);
        final Map<String,String> rsmap = new Map<String,String>();
        if(resp.getStatusCode()==OKSTATUS) {
            rsmap.put('code',''+resp.getStatusCode());
            rsmap.put('estatus',resp.getStatus());
            rsmap.put('body',resp.getBody());
        } else {
            rsmap.put('code',''+resp.getStatusCode());
            rsmap.put('estatus',resp.getStatus());
        }
        strresp=JSON.serialize(rsmap);
        return strresp;
    }

    /**
    * @description 
    * @author Daniel Perez Lopez | 12-10-2020 
    * @param idoportunidad 
    * @return Boolean 
    **/
    @AuraEnabled
    public static Boolean actQuoteData(String idoportunidad) {
        return MX_SB_PS_Container1_Ctrl.avanza(idoportunidad);
    }

    /**
    * @description 
    * @author Daniel Perez Lopez | 12-10-2020 
    * @param mapas 
    * @param opotunidad 
    * @return Boolean 
    **/
    @AuraEnabled
    public static String createBens(List<Map<String,String>> mapas, String opotunidad, String tipo) {
        return MX_SB_PS_Datos_AseguradoExt_Ctrl.createBeneficiaries(mapas,opotunidad,tipo);
    }
}