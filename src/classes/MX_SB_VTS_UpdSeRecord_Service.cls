/**
 * @description       : 
 * @author            : Diego Olvera
 * @group             : 
 * @last modified on  : 17-11-2020
 * @last modified by  : Diego Olvera
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   09-25-2020   Diego Olvera   Initial Version
**/
public without sharing class MX_SB_VTS_UpdSeRecord_Service {
    
    /** @description esta variable nos ayuda hacer el contador de toques **/
    private static final Integer NUMERO = 6;
     /** String campos de QUERY para Lead */
     STATIC FINAL String LEADFIELDS = 'Id, Status, Producto_Interes__c, Resultadollamada__c, MX_SB_VTS_Tipificacion_LV2__c, MX_SB_VTS_Tipificacion_LV3__c, MX_SB_VTS_Tipificacion_LV4__c, MX_SB_VTS_Tipificacion_LV5__c, MX_SB_VTS_Tipificacion_LV6__c, MX_SB_VTS_ContadorLlamadasTotales__c,MX_SB_VTS_ContadorRemarcado__c, DoNotCall';
     /** String campos de QUERY para Opportunity */
     STATIC FINAL String OPPFIELDS = 'Id, StageName, RecordTypeId, MX_SB_VTA_SubEtapa__c, MX_SB_VTS_Tipificacion_LV1__c, MX_SB_VTS_Tipificacion_LV2__c, MX_SB_VTS_Tipificacion_LV3__c, MX_SB_VTS_Tipificacion_LV4__c, MX_SB_VTS_Tipificacion_LV5__c, MX_SB_VTS_Tipificacion_LV6__c, MX_SB_VTS_ContadorLlamadasTotales__c, MX_SB_VTS_ContadorRemarcado__c';   
     /** RecordtypeId de Venta asistida */
	 STATIC FINAL Id RECORTYPEVTA = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTA_VentaAsistida).getRecordTypeId();
     /*constructor*/
    @SuppressWarnings('sf:UseSingleton')
    private MX_SB_VTS_UpdSeRecord_Service() {}

     /**
    * @description Actualiza valores de lead u Oportunidad para flujo Seguro Estudia
    * @author Diego Olvera | 25/09/2020 
    * @param String leadId recuperado de componente MX_SB_VTS_CloseWon
    * @return void
    **/
    public static void updSeCurrentRec(String leadId) {
        final Set<String> setLeadOppId = new Set<String>();
        setLeadOppId.add(leadId);
        if(leadId.startsWith('00Q')) {
            final List<Lead> leadRecords = MX_RTL_Lead_Selector.selectSObjectsById(LEADFIELDS, setLeadOppId);
            for(Lead upLeadVal : leadRecords) {
                if(upLeadVal.Producto_Interes__c.equals(label.MX_SB_VTS_SeguroEstudia_LBL)) {
                    if(upLeadVal.MX_SB_VTS_ContadorRemarcado__c < NUMERO) {
                        upLeadVal.Resultadollamada__c = '';
                        upLeadVal.MX_SB_VTS_Tipificacion_LV2__c = '';
                        upLeadVal.MX_SB_VTS_Tipificacion_LV3__c = '';
                        upLeadVal.MX_SB_VTS_Tipificacion_LV4__c = '';
                        upLeadVal.MX_SB_VTS_Tipificacion_LV5__c = '';
                        upLeadVal.MX_SB_VTS_Tipificacion_LV6__c = '';
                        upLeadVal.Status = Label.MX_SB_VTS_CONTACTO_LBL;
                    } else {
                        upLeadVal.DoNotCall = true;
                    } 
                } else {
                    upLeadVal.Status = Label.MX_SB_VTS_CONTACTO_LBL;
                }
            }
            MX_RTL_Lead_Selector.updateResult(leadRecords, false);
        } else if(leadId.startsWith('006')) {
            final Opportunity oppRecord = MX_RTL_Opportunity_Selector.getOpportunity(leadId, OPPFIELDS);
            final List<Opportunity> oppLstRecord = new List<Opportunity>();
            oppLstRecord.add(oppRecord);
            for(Opportunity upOppVal : oppLstRecord) {
                if(upOppVal.RecordTypeId.equals(RECORTYPEVTA)) {
                    if(upOppVal.MX_SB_VTS_ContadorRemarcado__c < NUMERO) {
                        upOppVal.MX_SB_VTS_Tipificacion_LV1__c = '';
                        upOppVal.MX_SB_VTS_Tipificacion_LV2__c = '';
                        upOppVal.MX_SB_VTS_Tipificacion_LV3__c = '';
                        upOppVal.MX_SB_VTS_Tipificacion_LV4__c = '';
                        upOppVal.MX_SB_VTS_Tipificacion_LV5__c = '';
                        upOppVal.MX_SB_VTS_Tipificacion_LV6__c = '';
                        upOppVal.StageName = Label.MX_SB_VTS_CONTACTO_LBL;
                        upOppVal.MX_SB_VTA_SubEtapa__c = Label.MX_SB_VTS_CONTACTO_LBL;
                    } else {
                        upOppVal.StageName = 'Closed Lost';
                    }
                } else {
                    upOppVal.StageName = Label.MX_SB_VTS_CONTACTO_LBL;
                }
            }
            MX_RTL_Opportunity_Selector.updateResult(oppLstRecord, false);
        }
    }
}