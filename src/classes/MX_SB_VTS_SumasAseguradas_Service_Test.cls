@isTest
/**
* @description       : Test class for MX_SB_VTS_SumasAseguradas_Service
* @author            : Marco Antonio Cruz Barboza
* @group             : BBVA
* @last modified on  : 01-27-2021
* @last modified by  : Marco Antonio Cruz Barboza
* Modifications Log 
* Ver   Date         Author                       Modification
* 1.0   01-27-2021   Marco Antonio Cruz Barboza   Initial Version
**/
public class MX_SB_VTS_SumasAseguradas_Service_Test {

    /**@description Nombre usuario*/
    private final static String ASESORNAME = 'AsesorTes';
    /**@description Nombre de la cuenta*/
    private final static String ACCCMBNAM = 'TestCm';
    /** String para query de monto */
    Public static final String FIELDAMO = 'PrimerPropietario__c';
    /** String para query de monto */
    Public static final String FIELDSRCH = 'Monto_de_la_oportunidad__c';
    /**@description Nombre de Oportunidad*/
    private final static String OPPCMBNAM = 'TestOppCm';
    /**@Description: Mock picklist Values**/
    Private Final static String VALPICK = '400000,420000,440000,460000,480000,500000,520000,540000,560000,580000,600000';
    /**@description Monto de la Oportunidad a actualiza*/
    private final static String MONTOVA = '15000';
    
    /**
	@Description: Setup for test class
	**/
    @TestSetup
    static void makeData() {
        Final User userTstIA = MX_WB_TestData_cls.crearUsuario(ASESORNAME, 'System Administrator');
        insert userTstIA;
        System.runAs(userTstIA) {
            Final Account accountIA = MX_WB_TestData_cls.createAccount(ACCCMBNAM, System.Label.MX_SB_VTS_PersonRecord);
            insert accountIA;
            final Opportunity oppoIA = MX_WB_TestData_cls.crearOportunidad(OPPCMBNAM, accountIA.Id, userTstIA.Id, System.Label.MX_SB_VTA_VentaAsistida);
            oppoIA.PrimerPropietario__c = VALPICK;
            oppoIA.Monto_de_la_oportunidad__c = Decimal.valueOf(MONTOVA);
            insert oppoIA;
        }
    }
    
    /**
    * @description Method to test getInsured method from MX_SB_VTS_SumasAseguradas_Ctrl
    * @author Marco Cruz | 01-27-2021 
    * @param srtOppId id Oppportunity, String FIELDSRCH
    * @return String getInsuAmo
    **/ 
    @isTest
    static void getInsuredTest() {
        final User userIT = [SELECT Id FROM User WHERE LastName =: ASESORNAME];
        System.runAs(userIT) {
            Final String oppIdIt = [SELECT Id FROM Opportunity WHERE Name =:OPPCMBNAM].Id;
            test.startTest();
            	Final String getInsuAmo = MX_SB_VTS_SumasAseguradas_Service.getInsured(oppIdIt, FIELDSRCH);
            	System.assertEquals(getInsuAmo,MONTOVA,'No se obtuvo la misma suma asegurada');
            test.stopTest();
            
        }
    }
    
    /**
    * @description Method to test getPick method from MX_SB_VTS_SumasAseguradas_Service
    * @author Marco Cruz | 01-27-2021 
    * @param srtOppId id Oppportunity, String FIELDAMO
    * @return String oppIdGP
    **/ 
    @isTest
    static void getPickTest() {
        final User userGP = [SELECT Id FROM User WHERE LastName =: ASESORNAME];
        System.runAs(userGP) {
            Final String oppIdGP = [SELECT Id FROM Opportunity WHERE Name =:OPPCMBNAM].Id;
            test.startTest();
            	Final String getData = MX_SB_VTS_SumasAseguradas_Service.getPick(oppIdGP, FIELDAMO);
            	System.assertEquals(getData,VALPICK,'No coinciden los valores del picklist');
            test.stopTest();
            
        }
    }
    
    /**
    * @description Method to test updatePick method from MX_SB_VTS_SumasAseguradas_Service
    * @author Marco Cruz | 01-27-2021 
    * @param srtOppId id Oppportunity, String insuredAmo, String FIELDAMO
    * @return void
    **/ 
    @isTest
    static void updatePickTest() {
        final User userUP = [SELECT Id FROM User WHERE LastName =: ASESORNAME];
        System.runAs(userUP) {
            Final String oppId = [SELECT Id FROM Opportunity WHERE Name =:OPPCMBNAM].Id;
            test.startTest();
            	MX_SB_VTS_SumasAseguradas_Service.updatePick(oppId,VALPICK,FIELDAMO);
            	Final String pickUpt = [SELECT Id, PrimerPropietario__c FROM Opportunity WHERE Id =:oppId].PrimerPropietario__c;
            	System.assertEquals(pickUpt,VALPICK,'No actualizo la Suma asegurada');
            test.stopTest();
            
        }
    }
    
}