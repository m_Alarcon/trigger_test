/**
 * @description       : 
 * @author            : Eduardo Hernández Cuamatzi
 * @group             : 
 * @last modified on  : 09-04-2020
 * @last modified by  : Eduardo Hernández Cuamatzi
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   08-09-2020   Eduardo Hernández Cuamatzi   Initial Version
**/
@SuppressWarnings('sf:UseSingleton')
public without sharing class MX_SB_VTS_leadMultiServiceVts_Helper {

    /**Id de recordType ASD*/
    final static Id ASDID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_RecordTypeASD).getRecordTypeId();
    /**Id de recordType Telemarketing VTS*/
    final static Id TELEMARKID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
    /**Id de recordType ASD*/
    final static Id ASDIDOPP = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_RecordTypeASD).getRecordTypeId();
    /**Id de recordType Telemarketing VTS*/
    final static Id TELEMARKIDOPP = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(System.Label.MX_SB_VTS_Telemarketing_LBL).getRecordTypeId();
    /**Texto para valor de telefono*/
    final private static String PERSONPHONEH = 'PER-PHONE-1';
    
    /**
    * @description Valida la lista de rultados DML
    * @author Eduardo Hernández Cuamatzi | 13/6/2020 
    * @param lstResults lista de resultados de operación DML
    * @return String Id del registro o Cadena de error resultante
    **/
    public static String processUniqueLead(List<Database.SaveResult> lstResults) {
        String recordId = '';
        for(Database.SaveResult resultDML : lstResults) {
            if(resultDML.isSuccess()) {
                recordId = resultDML.id;
            } else {
                recordId = resultDML.errors[0].message;
            }
        }
        return recordId;
    }

    /**
    * @description Recupera producto por código de clippert
    * @author Eduardo Hernandez Cuamatzi | 08-04-2020 
    * @param MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta parametros de entrada
    * @return String Nombre del producto asociado
    **/
    public static String findProduct(MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta) {
        String productName = '';
        if(consulta.product != null) {
            final Set<String> lstProductCode= new Set<String>{consulta.product.code};
            final Set<String> lstProcess= new Set<String>{'VTS'};
            for(MX_SB_SAC_Catalogo_Clipert_Producto__c clipProdut : MX_RTL_Catalogo_Clipert_Product_Selector.catalogoClipCod(lstProductCode, lstProcess, true)) {
                productName = clipProdut.MX_SB_SAC_Producto__r.Name;
            }
        }
        return productName;
    }

    /**
    * @description Producto a evaluar para recordtype
    * @author Eduardo Hernandez Cuamatzi | 08-04-2020 
    * @param String productName Nombre del product
    * @return Id 
    **/
    public static Id validRecType(String productName, Boolean isLead) {
        Id recordId = TELEMARKID;
        if(isLead == true) {
            if(productName.containsIgnoreCase('Auto seguro')) {
                recordId = ASDID;
            }
        } else {
            recordId = TELEMARKIDOPP;
            if(productName.containsIgnoreCase('Auto seguro')) {
                recordId = ASDIDOPP;
            }
        }
        return recordId;
    }

    /**
    * @description Valida atributo quote
    * @return String Id valido de quote
    **/
    public static String validateQuoteId(MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta) {
        String quoteId = '';
        if (consulta.quote != null) {
            quoteId = consulta.quote.id;
        }
        return quoteId;
    }

    /**
    * @description Procesa origen TW
    * @author Eduardo Hernández Cuamatzi | 08-25-2020 
    * @param consulta valores de entrada del servicio
    * @param origin Origen de la petición
    * @param product Producto asociado a la petición
    * @return String Id de la Oportunidad encontrada o registrada para TW
    **/
    public static String processOriginTW(MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta, String origin, String product) {
        String oppId = '';
        final Map<String, String> mapContact = fillContacts(consulta);
        if (MX_SB_VTS_leadMultiServiceVts_HelpTW.validOriginTW(consulta, mapContact)) {
            final Opportunity oppFind = findOppsByAso(consulta.quote.id);
            if(String.isEmpty(oppFind.Id)) {
                final String accountId = MX_SB_VTS_leadMultiServiceVts_HelpTW.findAccount(consulta, mapContact.get('email'), mapContact.get('mobile'));
                final Account accRec = MX_SB_VTS_utilityQuote.findUniqueAccount((Id)accountId);
                final Set<String> process = new Set<String>{'VTS'};
                final Set<String> namesProd = new Set<String>{product};
                final List<Product2> lstProd = MX_RTL_Product2_Selector.findProsByNameProces(namesProd, process, true);
                final Opportunity oppTw = MX_SB_VTS_leadMultiServiceVts_HelpTW.fillOpps(accRec, consulta, lstProd[0], mapContact);
                final String ownerId = MX_SB_VTS_leadMultiServiceVts_HelpTW.findOwner('VCIP');
                if(String.isNotBlank(ownerId)) {
                    oppTw.OwnerId = ownerId;
                }
                final List<Opportunity> lsTOpssIn = new List<Opportunity>{oppTw};
                final List<Opportunity> lsTOpss = MX_RTL_Opportunity_Selector.insertOpportunity(lsTOpssIn);
                oppId = lsTOpss[0].Id;
            } else {
                oppId = oppFind.Id;
            }
        }
        return oppId;
    }

    /**
    * @description Busca Oportunidades con Origen TW mediante folio de cotización
    * @author Eduardo Hernández Cuamatzi | 08-25-2020 
    * @param folioCot Folio de cotización enviado por aso
    * @return Opportunity Oportunidad encontrada en caso de existir
    **/
    public static Opportunity findOppsByAso(String folioCot) {
        Opportunity findOpp = new Opportunity();
        final List<Opportunity> lstOpps = MX_RTL_Opportunity_Selector.findOppsByTw(folioCot);
        if(lstOpps.isEmpty() == false) {
            findOpp = lstOpps[0];
        }
        return findOpp;
    }

    /**
    * @description Fill data contact
    * @author Eduardo Hernandez Cuamatzi | 08-21-2020 
    * @param MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta Datos entrada WS
    * @return Map<String, String> Mapa de valores contacto
    **/
    public static Map<String, String> fillContacts(MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta) {
        String phoneStr = '';
        String mobileStr = '';
        String emailStr = '';
        for(MX_SB_VTS_leadMultiServWrapper.Mx_sb_Contacdetails contactTemp : consulta.person.contactDetails) {
            phoneStr = contactTemp.key == PERSONPHONEH ? contactTemp.value : phoneStr;
            mobileStr = contactTemp.key== PERSONPHONEH ? contactTemp.value : mobileStr;
            emailStr = contactTemp.key == 'PER-EMAIL' ? contactTemp.value : emailStr;
        }
        final Map<String, String> fillContacts = new Map<String, String>();
        fillContacts.put('phone', phoneStr);
        fillContacts.put('mobile', mobileStr);
        fillContacts.put('email', emailStr);
        return fillContacts;
    }

        /**
    * @description Valida si se proporciono Apellido
    * @author Eduardo Hernández Cuamatzi | 08-26-2020 
    * @param consulta datos de json
    * @return String Apellido valor dafult en caso de no proporcionarse
    **/
    public static String validLastName(MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta) {
        String lastNameStr = 'No proporcionado';
        if(consulta.person.firstSurname != null && String.isNotBlank(consulta.person.firstSurname)) {
            lastNameStr = consulta.person.firstSurname;
        }
        return lastNameStr;
    }

    /**
    * @description Valida Elemento appoitment
    * @author Eduardo Hernández Cuamatzi | 09-03-2020 
    * @param consulta Datos wrapper
    * @return Boolean Retorna exitoso en caso de cumplirse las reglas
    **/
    public static Boolean validAppoit(MX_SB_VTS_leadMultiServWrapper.MX_sb_consulta consulta) {
        Boolean appoitOk = false;
        if(consulta.appointment == null) {
            appoitOk = true;
        } else if(validameth(consulta.appointment.timex)) {
            appoitOk = true;
        }
        return appoitOk;
    }

    /**
    * @description Validata si la variable contiene un valor
    * @author Eduardo Hernández Cuamatzi | 13/6/2020 
    * @param Variable variable de texto a evaluar
    * @return boolean True si es un texto diferente de vacio
    **/
    public static boolean validameth (String variable) {
        return String.isEmpty(variable)? false:true;
    }
}