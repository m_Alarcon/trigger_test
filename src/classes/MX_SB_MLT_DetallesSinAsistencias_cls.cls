/**---------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_DetallesSinAsistencias_cls
* Autor: Juan Carlos Benitez Herrera
* Proyecto: Siniestros - BBVA
* Descripción : Clase controladora de componente MX_SB_MLT_DetallesdelIncidente
* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                       Desripción
* --------------------------------------------------------------------------------
* 1.0           20/01/2020     Juan Carlos Benitez          Creación
* 1.1			25/01/2020	   Angel Nava	                modificación para envios
* 1.2           16/02/2020     Juan Carlos Benitez          Se refactoriza clase
* --------------------------------------------------------------------------------
**/
public with sharing class MX_SB_MLT_DetallesSinAsistencias_cls {//NOSONAR
    /*
    * @var final String verdadero
    */
    public static final string AFIRMA='true';
    /*
    * @description Actualiza los campos de siniestro
    * @param Mapa<String,String> mapa
    */
    @AuraEnabled
    public static void updateSinies(Map<String,String> mapa) {
            final String siniId =mapa.get('siniId');
            final String strEmail=mapa.get('strEmail');
            final String strCond=mapa.get('strCond');
            final String button=mapa.get('button');
            final Siniestro__c siniestro = [SELECT Id, MX_SB_MLT_JourneySin__c, MX_SB_MLT_CorreoElectronico__c,MX_SB_MLT_Condiciones_del_Vehiculo__c FROM Siniestro__c where id =: siniId limit 1];
        	siniestro.MX_SB_MLT_CorreoElectronico__c = strEmail;
        	siniestro.MX_SB_MLT_Condiciones_del_Vehiculo__c= strCond;
        if(button==AFIRMA) {
            siniestro.MX_SB_MLT_JourneySin__c='Crear Servicios';
        }
    	update siniestro;            
        }
        /*
    * @description Obtiene la Worder de grua
    * @param String sinid
    * @return  rsltWO
    */
    @AuraEnabled
        public static WorkOrder getGruaWO(String sinid) {
		WorkOrder rsltWO;
        rsltWO = MX_SB_MLT_WrapperAsistencias_cls.getGruaCase(sinid);
        return rsltWO;
        }
      /*
    * @description Obtiene la Worder de Cambio de llanta
    * @param String sinid
    * @return  rsltWO
    */  
        @AuraEnabled
        public static WorkOrder getCambioLLWO(String sinid) {
		WorkOrder rsltWO;
        rsltWO = MX_SB_MLT_WrapperAsistencias_cls.getCambioLlantaCase(sinid);
        return rsltWO;
        }
        /*
    * @description Obtiene la Worder de paso de corriente
    * @param String sinid
    * @return  rsltWO
    */
    @AuraEnabled
        public static WorkOrder getPasoCWO(String sinid) {
		WorkOrder rsltWO;
        rsltWO = MX_SB_MLT_WrapperAsistencias_cls.getPasoCorrienteCase(sinid);
        return rsltWO;
        }
        /*
    * @description Obtiene la Worder de gasolina
    * @param String sinid
    * @return  rsltWO
    */
    @AuraEnabled
        public static WorkOrder getGasWO(String sinid) {
		WorkOrder rsltWO;
        rsltWO = MX_SB_MLT_WrapperAsistencias_cls.getGasolinaCase(sinid);
        return rsltWO;
        }
        /*
    * @description Obtiene los datos del siniestro
    * @param String siniId
    * @return  rsltWO
    */
     @AuraEnabled
        public static Siniestro__c getSiniestroD(Id siniId) {
		Siniestro__c rsltWO;
        rsltWO = MX_SB_MLT_WrapperAsistencias_cls.fetchSiniDetails(siniId);
        return rsltWO;
        }
        /*
    * @description Crea la Worder de grua
    * @param String sinid
    */
    @AuraEnabled
        public static void insertWOGrua(String strGrua, String strId) {
        MX_SB_MLT_WrapperAsistencias_cls.insertCaseGrua(strGrua, strId);
        }
     /*
    * @description Crea la Worder de gasolina
    * @param String sinid
    */
    @AuraEnabled
        public static void insertWOGas(String strId, String strGasolina) {
        MX_SB_MLT_WrapperAsistencias_cls.insertCaseGas(strGasolina, strId);
        }
           /*
    * @description Crea la Worder de paso de corriente
    * @param String sinid
    */
    @AuraEnabled
    public static void insertWOPasoC(String strId, String strPasoCorriente) {
		MX_SB_MLT_WrapperAsistencias_cls.insertWOPasoC(strId, strPasoCorriente);
    }
            /*
    * @description Crea la Worder de cambio de llanta
    * @param String sinid
    */
    @AuraEnabled
    public static void insertWOCambLl(String strId, String strCambioLlanta) {
		MX_SB_MLT_WrapperAsistencias_cls.insertWOCambLl(strId, strCambioLlanta);
    }
}