@IsTest
/*
----------------------------------------------------------------------------------
* Nombre: MX_SB_MLT_PolizaWsController_Tst
* Autor Daniel Perez Lopez
* Proyecto: SB Siniestros - BBVA Bancomer
* Descripción : Prueba los Methods de la clase MX_SB_MLT_PolizaWsController

* --------------------------------------------------------------------------------
* Versión       Fecha           Autor                               Descripción
* --------------------------------------------------------------------------------
* 1.0           13/02/2020      Daniel Lopez                         Creación
* --------------------------------------------------------------------------------
*/
public with sharing class MX_SB_MLT_PolizaWsController_Tst {
    @IsTest static void tablaCertifiacado () {
  		final List<Map<String,String>> siniarray = MX_SB_MLT_PolizaWsController.getTableCertificados('test','method');
        test.startTest();
        	System.assertEquals('Act',siniarray[0].get('status'),'Exito en dato');
        test.stopTest();
    } 
}