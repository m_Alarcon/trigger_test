import { LightningElement, api, track, wire} from 'lwc';
import {NavigationMixin} from 'lightning/navigation';
import undecodeSetting from '@salesforce/apex/MX_BPP_MinutaPreview_Ctrl.getParamaterSetting';
import undecodeDocs from '@salesforce/apex/MX_BPP_MinutaPreview_Ctrl.getParamaterDocs';

export default class MX_BPP_Minuta extends NavigationMixin(LightningElement) {

    @api currentId;

    @api sMyCS;

    @api sDocuments;

    @api sVfName;

    @api sIdCatalogo;

    @api sPreviewParams;

    @api sShowFichaProducto;

    @wire(undecodeSetting, {
        parameterToGet: '$sMyCS'
    })
    unMyCS;

    unDocuments;
    @wire(undecodeDocs, {
        parameterToGet: '$sDocuments'
    }) wiredContacts({
                         error,
                         data
                     }) {
        if (data) {
            this.unDocuments = data;
            this.error = null;
        } else if (error) {
            this.error = error;
            this.contacts = null;
        }
    }

    picklistsArray;
    opcionalesTextArray;
    switchesArray;

    @track previewUrl;
    @track showCargaOpcionales;
    @track showEnviarMinuta = false;

    connectedCallback() {
        this.setParams(this.sPreviewParams);
        this.loadButtons();
        this.makeURL(true);
    }

    makeURL(callback) {
        var arraysStr = '&textArray='+this.parametersText(callback) + '&seleccionArray=' + this.parametersSeleccion(callback) + '&switchArray=' + this.parametersSwitch(callback);
        var url = this.sVfName + "?Id=" + this.currentId + '&sCatalogoId='+ this.sIdCatalogo + arraysStr;
        this.loadButtons();
        this.previewUrl = url;
    }

    setParams(params) {
        try{
            const parsedObject = JSON.parse(params.replace(/\&quot\;/g,'"')
                .replace(/\&amp\;/g,'&')
                .replace(/\&aacute\;/g, "á")
                .replace(/\&eacute\;/g, "é")
                .replace(/\&iacute\;/g, "í")
                .replace(/\&oacute\;/g, "ó")
                .replace(/\&uacute\;/g, "ú")
                .replace(/\&ntilde\;/g, "ñ"));
            this.currentId = parsedObject.configParamsPty.currentId;
            this.sMyCS = parsedObject.configParamsPty.sMyCS;
            this.sDocuments = parsedObject.configParamsPty.sDocuments;
            this.sIdCatalogo = parsedObject.catalogoPty.sIdCatalogo;
            this.sVfName = parsedObject.catalogoPty.sVfName;
            this.picklistsArray = parsedObject.contenedorCmps.cmpPicklist || [];
            this.opcionalesTextArray = parsedObject.contenedorCmps.cmpText || [];
            this.switchesArray = parsedObject.contenedorCmps.cmpSwitch || [];
            this.sShowFichaProducto = parsedObject.catalogoPty.showFichaProducto || false;
        }
        catch(error){}

    }

    handleTextChange(event) {

        if (this.areFieldsValid()) {
            this.setParams(this.sPreviewParams);
            this.makeURL(false);
            this.showEnviarMinuta = true;
        } else {

            this.showEnviarMinuta = false;
        }

    }

    parametersText(callback) {
        var parametersText = '';
        var paramsTextArray = [];
        if(this.opcionalesTextArray.length > 0) {
            for (let field of this.opcionalesTextArray) {
                var parameter = "&" + field.tagInicio + "=";
                if (!callback) {
                    parametersText = parametersText + parameter + this.getValue(field.name);
                    paramsTextArray.push({"tagInicio" : `${field.tagInicio}`, "label":`${this.getValue(field.name)}`});
                } else {
                    parametersText = parametersText + parameter + field.label;
                    paramsTextArray.push({"tagInicio" : `${field.tagInicio}`, "label":""});
                }
            }
        }
        return JSON.stringify(paramsTextArray);
    }

    parametersSeleccion(callback) {
        var parametersSeleccion = '';
        var paramsSelecionArr = [];
        if(this.picklistsArray.length > 0) {
            for (let field of this.picklistsArray) {
                var parameter = "&" + field.tagInicio + "=";
                if (!callback) {
                    parametersSeleccion = parametersSeleccion + parameter + this.getValue(field.name);
                    paramsSelecionArr.push({"tagInicio" : `${field.tagInicio}`, "label": `${this.getValue(field.name)}`});
                } else {
                    parametersSeleccion = parametersSeleccion + parameter + "";
                    paramsSelecionArr.push({"tagInicio" : `${field.tagInicio}`, "label": ""});
                }
            }
        }
        return JSON.stringify(paramsSelecionArr);
    }

    parametersSwitch(callback) {
        var parametersSwitch = '';
        var paramsSwitchArr = [];
        if(this.switchesArray.length > 0) {
            for (let field of this.switchesArray) {
                var parameter = "&" + field.tagInicio + "=";
                if (!callback && this.getChecked(field.name)) {
                    parametersSwitch = parametersSwitch + parameter + this.getValue(field.name);
                    paramsSwitchArr.push({"tagInicio" : `${field.tagInicio}`, "label":`${field.label}`});
                } else {
                    parametersSwitch = parametersSwitch + parameter + field.label;
                    paramsSwitchArr.push({"tagInicio" : `${field.tagInicio}`, "label":""});
                }
            }
        }
        return JSON.stringify(paramsSwitchArr);
    }

    getValue(dataId) {
        var inputCmp = this.template.querySelector('[data-id="' + dataId + '"]');
        return inputCmp.value;
    }

    loadButtons() {
        this.showCargaOpcionales = (this.switchesArray.length > 0) || (this.opcionalesTextArray.length > 0) || (this.picklistsArray.length > 0);
        this.showEnviarMinuta = (this.switchesArray.length <= 0) && (this.opcionalesTextArray.length <= 0) && (this.picklistsArray.length <= 0);

    }

    areFieldsValid() {
        const allValid = [...this.template.querySelectorAll('lightning-textarea, lightning-combobox, lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                return validSoFar && inputCmp.checkValidity();
            }, true);

        return allValid;
    }

    getChecked(dataId){
        var inputCmp = this.template.querySelector('[data-id="' + dataId + '"]');
        return inputCmp.checked;
    }

    handleAttachFiles(event) {
        let data = event.detail;
        this.unDocuments = JSON.parse(data);
    }

    get sendBtnUrl() {
        var sendUrl = '/apex/' + this.previewUrl + '&myCS=' + JSON.stringify(this.unMyCS) + '&documents=' + JSON.stringify(this.unDocuments);
        return sendUrl;
    }
}