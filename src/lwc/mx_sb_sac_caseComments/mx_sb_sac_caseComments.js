import { LightningElement, api } from 'lwc';
import Id from '@salesforce/user/Id';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import updateCaseComments from '@salesforce/apex/MX_SB_SAC_1500_ExecutiveAuth.updateCaseComments';

export default class Mx_sb_sac_caseComments extends LightningElement {
    @api record;
    @api caseId;
    @api userName;
    @api clientName;
    @api customText;
    @api rows;
    userId = Id;

    handleUpdateCase() {
        let desc = this.template.querySelector("[data-field='Description']").value;
        if(desc === undefined || desc === '' || desc.length < 10) {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error',
                    message: 'Por favor llenar comentarios',
                    variant: 'error'
                })
            );
        } else {
            const dataCase = { 'sObjectType' : 'Case' };
            dataCase.Id = this.caseId;
            dataCase.MX_SB_SAC_FinalizaFlujo__c = true;
            dataCase.Reason = 'Consultas';
            dataCase.MX_SB_SAC_ProductosParaVenta__c = 'Consultas Sin Póliza';
            dataCase.MX_SB_SAC_EsCliente__c = true;
            dataCase.MX_SB_SAC_FinalizarCancelacion__c = true;
            dataCase.MX_SB_SAC_Subdetalle__c = '';
            dataCase.MX_SB_SAC_Subdetalle_Vida__c = '';
            dataCase.MX_SB_SAC_TerminaMalasVentas__c = true;
            dataCase.MX_SB_SAC_Finalizar_Retencion__c = true;
            dataCase.MX_SB_SAC_Ocultartipificacion__c = true;
            let origin = '';
            if(this.record.MX_SB_1500_is1500__c === true) {
                origin = '#1500';
            } else {
                origin = this.record.Origin;
            }
            updateCaseComments({ caseData : dataCase }).then(() => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Exito',
                        message: 'Caso actualizado',
                        variant: 'success'
                    })
                );
            }).then(() => {
                switch(origin) {
                    case 'Phone':
                    case 'Linea BBVA':
                    case 'Ventas':
                        dataCase.MX_SB_SAC_Detalle__c = 'No es Asegurado/Contratante';
                        dataCase.MX_SB_SAC_Aplicar_Mala_Venta__c = '';
                        break;
                    case '#1500':
                        dataCase.MX_SB_SAC_Detalle__c = 'Ejecutivo no autentica correctamente'
                        dataCase.MX_SB_1500_isAuthenticated__c = true;
                        break;
                    default:
                        break;
                }
                dataCase.Description = this.template.querySelector("[data-field='Description']").value;
                const upRecord = { dataCase };
                console.log(upRecord);
                updateRecord(upRecord);
                eval("$A.get('e.force:refreshView').fire();");
            }).catch(error => {
                console.log(error);
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: error.enhancedErrorType,
                        message: 'Error al actualizar el caso',
                        variant: 'error'
                    })
                );
            });
        }
    }
}