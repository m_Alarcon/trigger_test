import {
    LightningElement,
    api,
    wire,
    track
} from 'lwc';
import getSumValue from '@salesforce/apex/MX_SB_VTS_GetSetPPForm_Ctrl.dataAmountValues';
import mGetCoberturas from '@salesforce/apex/MX_SB_VTS_GetSetPPForm_Ctrl.dataCoberturas';
export default class MxSbVtsPersonalizaProteccionALR extends LightningElement {
    activeSections = ['Asistencias', 'Contenidos', 'Construccion', 'AsistenciasOpt', 'ContenidoOpt', 'AsistenciasOptOne', 'ContruccionOptOne'];
    casaPro = 'Cuentas con ayuda de especialistas por si en tu día a día necesitas: plomería, electricidad, cerrajería, albañilería, jardinería y más.';
    medicaPro = 'Cuentas con asesoría médica telefónica, médico a tu domicilio y traslados en ambulancia (terrestre o aérea).';
    mascotasPro = 'Tienes ayuda para tu perro o gato en servicios como: estética, hospedaje, vacunas, asesoría telefónica y más.'
    incendioPro = 'Cubre daños ocasionados a las cosas que estén dentro de tu casa por: incendio o rayo.';
    terremotoPro = 'Cubre daños ocasionados a las cosas que estén dentro de tu casa por: terremoto o erupción volcánica';
    hidroPro = 'Cubre daños ocasionados a las cosas que estén dentro de tu casa por: inundación por lluvia, granizo, huracán entre otros.';
    roboPro = '- Protege objetos dentro de tu hogar como: Televisiones, LapTops, celulares, joyas, obras de arte, artículos deportivos, dinero en efectivo y más.\n';
    roboPerPro = '- Protege objetos dentro de tu hogar como: joyas, obras de arte, artículos deportivos y más.\n';
    roboEfectPro = '- Cubre el dinero en efectivo dentro del Hogar.';
    comRobPro = '';
    linea_blancaPro = 'Protege los daños de tu pantalla, tablet, equipo de audio, consola de videojuego, centro de lavado, refrigerador, cafetera, entre otros.';
    cristalesPro = 'Cubre la rotura accidental de vidrios o cristales interiores y exteriores como: ventanas, canceles, espejos y más.';
    tercerosPro = 'Cubrimos los daños ocasionados por ti, tu familia o hasta tus mascotas a bienes o personas dentro y fuera de tu hogar';
    activeSectionsMessage = '';
    valueCompleta = '';
    valueBalanceada = '';
    valueBasica = '';
    @api cp;
    @track valorCincuentaPro = 0.50;
    @track valorVeinticincoPro = 0.25;
    @track valorQuincePro = 0.15;
    @track valorCienPro = 0.100;
    @api wrapperParent = {}
    @track val2;
    @track attrValue34;
    @track itemsProPro = [];
    @track valuePro = '';
    @track chosenValuePro = '';
    @track infoPro = [];
    @track activeSectionMessage = '';
    @track optOneInce = 0;
    @track optOneTerre = 0;
    buttonClicked = false;
    @track vCoberturaCompleta;
    @track vCoberturaBalanceada;
    @track vCoberturaBasica;
    renderedCallback() {
        this.comRob = this.roboPro + this.roboPerPro + this.roboEfectPro;
    }
    get optsCompleta() {
        return [{
            label: '',
            value: 'optCompleta'
        }];
    }
    get optsBalanceada() {
        return [{
            label: '',
            value: 'optBalanceada'
        }];
    }
    get optsBasica() {
        return [{
            label: '',
            value: 'optBasica'
        }];
    }
    handleToggleSection(event) {
        this.activeSectionMessage =
            'Open section name:  ' + event.detail.openSections;
    }
    handleSectionToggle(event) {
        const openSections = event.detail.openSections;
        if (openSections.length === 0) {
            this.activeSectionsMessage = 'All sections are closed';
        } else {
            this.activeSectionsMessage =
                'Open sections: ' + openSections.join(', ');
        }
    }
    @wire(mGetCoberturas)
    wireQueryCoberturas({
        error,
        data
    }) {
        if (data) {
            this.vCoberturaCompleta = data['Cobertura-Opt1'];
            this.vCoberturaBalanceada = data['Cobertura-Opt2'];
            this.vCoberturaBasica = data['Cobertura-Opt3'];
        } else if (error) {
            throw new Error('Ocurrio un error al momento de obtener las coberturas');
        }
    }
    @wire(getSumValue)
    handleJson({
        error,
        data
    }) {
        this.infoPro = [];
        if (data) {
            const infoPro = data;
            var attrValue34 = infoPro['Propietario'];
            for (let keyIn in attrValue34) {
                const val2 = attrValue34[1];
                this.chosenValuePro = val2;
                this.calculateValue();
                const option = {
                    label: attrValue34[keyIn],
                    value: attrValue34[keyIn]
                };
                this.itemsPro = [...this.itemsPro, option];
            }
        } else if (error) {
            this.error = error;
            this.sumAseg = [];
        }
    }
    get roleOptions() {
        return this.itemsPro;
    }
    formatCry(amountPro, decimalCountPro = 0, decimalPro = ".", thousandsPro = ",") {
        decimalCountPro = Math.abs(decimalCountPro);
        decimalCountPro = isNaN(decimalCountPro) ? 2 : decimalCountPro;
        const negativeSignPro = amountPro < 0 ? "-" : "";
        var valorAmountPro = parseInt(amountPro);
        let iPro = valorAmountPro.toFixed(decimalCountPro).toString();
        let jPro = (iPro.length > 3) ? iPro.length % 3 : 0;
        const opPrePro = negativeSignPro + (jPro ? iPro.substr(0, jPro) + thousandsPro : '');
        const opFinalPro = opPrePro + i.substr(jPro).replace(/(\d{3})(?=\d)/g, "$1" + thousandsPro);
        const opLastPro = opFinalPro + (decimalCountPro ? decimalPro + Math.abs(amountPro - iPro).toFixed(decimalCountPro).slice(2) : "");
        return opLastPro;
    }
    handleChange(event) {
        const selectedOption = event.detail.value;
        this.chosenValuePro = selectedOption;
        this.calculateValue();
    }
    calculateValue() {
        const valSeleccionado = this.chosenValuePro;
        const numeroPro = Number(valSeleccionado.replace(/[^0-9.-]+/g, ""));
        const valorConvertPro = parseInt(numeroPro);
        const optValThree = valorConvertPro * this.valorVeinticincoPro;
        const valOptThree = this.formatCry(optValThree);
        this.optOneRobPer = '$ ' + valOptThree;
        const optValFour = valorConvertPro * this.valorQuincePro;
        const valOptFour = this.formatCry(optValFour);
        this.optOneRobAr = '$ ' + valOptFour;
        const optValFive = valorConvertPro * this.valorQuincePro;
        const valOptFive = this.formatCry(optValFive);
        this.optOneLinea = '$ ' + valOptFive;
        const optValSix = valorConvertPro * this.valorCienPro;
        const valOptSix = this.formatCry(optValSix);
        this.optOneInce1 = '$ ' + valOptSix;
        const optValSeven = valorConvertPro * this.valorCienPro;
        const valOptSeven = this.formatCry(optValSeven);
        this.optOneTerre1 = '$ ' + valOptSeven;
        const optVal = valorConvertPro * this.valorCincuentaPro;
        const valOpt = this.formatCry(optVal);
        this.optOneInce = '$ ' + valOpt;
        const optValOne = valorConvertPro * this.valorCincuentaPro;
        const valOptOne = this.formatCry(optValOne);
        this.optOneTerre = '$ ' + valOptOne;
        const optValTwo = valorConvertPro * this.valorCincuentaPro;
        const valOptTwo = this.formatCry(optValTwo);
        this.optOneAgua = '$ ' + valOptTwo;
        const optValEight = valorConvertPro * this.valorCienPro;
        const valOptEight = this.formatCry(optValEight);
        this.optOneAgua1 = '$ ' + valOptEight;
        const optValNine = valorConvertPro * this.valorCienPro;
        const valOptNine = this.formatCry(optValNine);
        this.optOneCivil = '$ ' + valOptNine;
        const optValTen = valorConvertPro * this.valorCienPro;
        const valOptTen = this.formatCry(optValTen);
        this.optOneCrital = '$ ' + valOptTen;
        const optValEle = valorConvertPro * this.valorCincuentaPro;
        const valOptEle = this.formatCry(optValEle);
        this.optTwoInce = '$ ' + valOptEle;
        const optValTwe = valorConvertPro * this.valorCincuentaPro;
        const valOptTwe = this.formatCry(optValTwe);
        this.optTwoTerre = '$ ' + valOptTwe;
        const optValThre = valorConvertPro * this.valorCincuentaPro;
        const valOptThre = this.formatCry(optValThre);
        this.optTwoAgua = '$ ' + valOptThre;
        const optValFo = valorConvertPro * this.valorVeinticincoPro;
        const valOptFo = this.formatCry(optValFo);
        this.optTwoRobPer = '$ ' + valOptFo;
        const optValFiv = valorConvertPro * this.valorQuincePro;
        const valOptFiv = this.formatCry(optValFiv);
        this.optTwoRobAr = '$ ' + valOptFiv;
        const optValSi = valorConvertPro * this.valorQuincePro;
        const valOptSi = this.formatCry(optValSi);
        this.optTwoLinea = '$ ' + valOptSi;
        const optValSev = valorConvertPro * this.valorCienPro;
        const valOptSev = this.formatCry(optValSev);
        this.optThreeInce1 = '$ ' + valOptSev;
        const optValEig = valorConvertPro * this.valorCienPro;
        const valOptEig = this.formatCry(optValEig);
        this.optThreeTerre1 = '$ ' + valOptEig;
        const optValNin = valorConvertPro * this.valorCienPro;
        const valOptNin = this.formatCry(optValNin);
        this.optThreeAgua1 = '$ ' + valOptNin;
        const optValTwen = valorConvertPro * this.valorCienPro;
        const valOptTwen = this.formatCry(optValTwen);
        this.optThreeCivil = '$ ' + valOptTwen;
        const optValTwenO = valorConvertPro * this.valorQuincePro;
        const valOptTwenO = this.formatCry(optValTwenO);
        this.optThreeCrital = '$ ' + valOptTwenO;
    }
    handledBackAction() {
        let upWrapStage = Object.assign({}, this.wrapperContext, {
            etapa: "newCot"
        });
        this.wrapperContext = upWrapStage;
        this.backAction();
    }
    handledNextAction() {
        let upWrapStage = Object.assign({}, this.wrapperContext, {
            etapa: "emitida"
        });
        this.wrapperContext = upWrapStage;
        this.nextAction();
    }
    backAction() {
        const jsonDetail = {cotizObject:this.wrapperContext, zipCode : this.cp};
        const backAction = new CustomEvent("backaction", {
            detail: jsonDetail,
        });
        this.dispatchEvent(backAction);
    }
    nextAction() {
        const jsonDetail = {cotizObject:this.wrapperContext, zipCode : this.cp};
        const nextAction = new CustomEvent("nextaction", {
            detail: jsonDetail,
        });
        this.dispatchEvent(nextAction);
    }
    changeRob() {
        const varBtn = this.buttonClicked;
        if (varBtn === false) {
            this.buttonClicked = true;
            this.changeRobAddOne();
        } else {
            this.buttonClicked = false;
            this.changeRobRemOne();
        }
    }
    changeRobo() {
        const varBtnTwo = this.buttonClicked;
        if (varBtnTwo === false) {
            this.buttonClicked = true;
            this.changeRobAddTwo();
        } else {
            this.buttonClicked = false;
            this.changeRobRemTwo();
        }
    }
    changeRobOne() {
        const varBtnOneThree = this.buttonClicked;
        if (varBtnOneThree === false) {
            this.buttonClicked = true;
            this.changeRobAddThree();
        } else {
            this.buttonClicked = false;
            this.changeRobRemThree();
        }
    }
    changeRobTwo() {
        const varBtnFour = this.buttonClicked;
        if (varBtnFour === false) {
            this.buttonClicked = true;
            this.changeRobAddFour();
        } else {
            this.buttonClicked = false;
            this.changeRobRemFour();
        }
    }
    changeRobThree() {
        const varBtnFive = this.buttonClicked;
        if (varBtnFive === false) {
            this.buttonClicked = true;
            this.changeRobAddFive();
        } else {
            this.buttonClicked = false;
            this.changeRobRemFive();
        }
    }
    changeRobFour() {
        const varBtnSix = this.buttonClicked;
        if (varBtnSix === false) {
            this.buttonClicked = true;
            this.changeRobAddSix();
        } else {
            this.buttonClicked = false;
            this.changeRobRemSix();
        }
    }
    changeRobFive() {
        const varBtnSeven = this.buttonClicked;
        if (varBtnSeven === false) {
            this.buttonClicked = true;
            this.changeRobAddSeven();
        } else {
            this.buttonClicked = false;
            this.changeRobRemSeven();
        }
    }
    changeRobSix() {
        const varBtnEight = this.buttonClicked;
        if (varBtnEight === false) {
            this.buttonClicked = true;
            this.changeRobAddEight();
        } else {
            this.buttonClicked = false;
            this.changeRobRemEight();
        }
    }
    changeRobSeven() {
        const varBtnNine = this.buttonClicked;
        if (varBtnNine === false) {
            this.buttonClicked = true;
            this.changeRobAddNine();
        } else {
            this.buttonClicked = false;
            this.changeRobRemNine();
        }
    }
    changeRobRemOne() {
        this.template.querySelector('[data-name="myButtonName"]').classList.remove('class1');
    }
    changeRobAddOne() {
        this.template.querySelector('[data-name="myButtonName"]').classList.add('class1');
    }
    changeRobRemTwo() {
        this.template.querySelector('[data-name="myButtonName1"]').classList.remove('class1');
    }
    changeRobAddTwo() {
        this.template.querySelector('[data-name="myButtonName1"]').classList.add('class1');
    }
    changeRobRemThree() {
        this.template.querySelector('[data-name="myButtonName2"]').classList.remove('class1');
    }
    changeRobAddThree() {
        this.template.querySelector('[data-name="myButtonName2"]').classList.add('class1');
    }
    changeRobRemFour() {
        this.template.querySelector('[data-name="myButtonName3"]').classList.remove('class1');
    }
    changeRobAddFour() {
        this.template.querySelector('[data-name="myButtonName3"]').classList.add('class1');
    }
    changeRobRemFive() {
        this.template.querySelector('[data-name="myButtonName4"]').classList.remove('class1');
    }
    changeRobAddFive() {
        this.template.querySelector('[data-name="myButtonName4"]').classList.add('class1');
    }
    changeRobRemSix() {
        this.template.querySelector('[data-name="myButtonName5"]').classList.remove('class1');
    }
    changeRobAddSix() {
        this.template.querySelector('[data-name="myButtonName5"]').classList.add('class1');
    }
    changeRobRemSeven() {
        this.template.querySelector('[data-name="myButtonName6"]').classList.remove('class1');
    }
    changeRobAddSeven() {
        this.template.querySelector('[data-name="myButtonName6"]').classList.add('class1');
    }
    changeRobRemEight() {
        this.template.querySelector('[data-name="myButtonName7"]').classList.remove('class1');
    }
    changeRobAddEight() {
        this.template.querySelector('[data-name="myButtonName7"]').classList.add('class1');
    }
    changeRobRemNine() {
        this.template.querySelector('[data-name="myButtonName8"]').classList.remove('class1');
    }
    changeRobAddNine() {
        this.template.querySelector('[data-name="myButtonName8"]').classList.add('class1');
    }
    onClickTooltip(event) {
        let targetId = '' + event.target.dataset.id;
        this.visible = true;
        this.template.querySelector('[data-id="' + targetId + '-tooltip"]').classList.remove('slds-hide');
    }
    onMouseOutTooltip(event) {
        let targetId = event.target.dataset.id;
        this.visible = true;
        this.template.querySelector('[data-id="' + targetId + '-tooltip"]').classList.add('slds-hide');
    }
    onclickOp1() {
        this.visible = true;
        this.template.querySelector('.divHelp').classList.remove('slds-hide');
    }
    hideOp1() {
        this.visible = true;
        this.template.querySelector('.divHelp').classList.add('slds-hide');
    }
    onclickOp2() {
        this.visible = true;
        this.template.querySelector('.divHelpOne').classList.remove('slds-hide');
    }
    hideOp2() {
        this.visible = true;
        this.template.querySelector('.divHelpOne').classList.add('slds-hide');
    }
    onclickOp3() {
        this.visible = true;
        this.template.querySelector('.divHelpTwo').classList.remove('slds-hide');
    }
    hideOp3() {
        this.visible = true;
        this.template.querySelector('.divHelpTwo').classList.add('slds-hide');
    }
    onclickOp4() {
        this.visible = true;
        this.template.querySelector('.divHelpThree').classList.remove('slds-hide');
    }
    hideOp4() {
        this.visible = true;
        this.template.querySelector('.divHelpThree').classList.add('slds-hide');
    }
    onclickOp5() {
        this.visible = true;
        this.template.querySelector('.divHelpFour').classList.remove('slds-hide');
    }
    hideOp5() {
        this.visible = true;
        this.template.querySelector('.divHelpFour').classList.add('slds-hide');
    }
    onclickOp6() {
        this.visible = true;
        this.template.querySelector('.divHelpFive').classList.remove('slds-hide');
    }
    hideOp6() {
        this.visible = true;
        this.template.querySelector('.divHelpFive').classList.add('slds-hide');
    }
    onclickOp7() {
        this.visible = true;
        this.template.querySelector('.divHelpSix').classList.remove('slds-hide');
    }
    hideOp7() {
        this.visible = true;
        this.template.querySelector('.divHelpSix').classList.add('slds-hide');
    }
    onclickOp8() {
        this.visible = true;
        this.template.querySelector('.divHelpSeven').classList.remove('slds-hide');
    }
    hideOp8() {
        this.visible = true;
        this.template.querySelector('.divHelpSeven').classList.add('slds-hide');
    }
    onclickOp9() {
        this.visible = true;
        this.template.querySelector('.divHelpEight').classList.remove('slds-hide');
    }
    hideOp9() {
        this.visible = true;
        this.template.querySelector('.divHelpEight').classList.add('slds-hide');
    }
    onclickOp10() {
        this.visible = true;
        this.template.querySelector('.divHelpNine').classList.remove('slds-hide');
    }
    hideOp10() {
        this.visible = true;
        this.template.querySelector('.divHelpNine').classList.add('slds-hide');
    }
    onclickOp11() {
        this.visible = true;
        this.template.querySelector('.divHelpTen').classList.remove('slds-hide');
    }
    hideOp11() {
        this.visible = true;
        this.template.querySelector('.divHelpTen').classList.add('slds-hide');
    }
    onclickOp12() {
        this.visible = true;
        this.template.querySelector('.divHelpEleven').classList.remove('slds-hide');
    }
    hideOp12() {
        this.visible = true;
        this.template.querySelector('.divHelpEleven').classList.add('slds-hide');
    }
    onclickOp13() {
        this.visible = true;
        this.template.querySelector('.divHelpTwelve').classList.remove('slds-hide');
    }
    hideOp13() {
        this.visible = true;
        this.template.querySelector('.divHelpTwelve').classList.add('slds-hide');
    }
    onclickOp14() {
        this.visible = true;
        this.template.querySelector('.divHelpThirteen').classList.remove('slds-hide');
    }
    hideOp14() {
        this.visible = true;
        this.template.querySelector('.divHelpThirteen').classList.add('slds-hide');
    }
    onclickOp15() {
        this.visible = true;
        this.template.querySelector('.divHelpFourteen').classList.remove('slds-hide');
    }
    hideOp15() {
        this.visible = true;
        this.template.querySelector('.divHelpFourteen').classList.add('slds-hide');
    }
    onclickOp16() {
        this.visible = true;
        this.template.querySelector('.divHelpFiveteen').classList.remove('slds-hide');
    }
    hideOp16() {
        this.visible = true;
        this.template.querySelector('.divHelpFiveteen').classList.add('slds-hide');
    }
    onclickOp17() {
        this.visible = true;
        this.template.querySelector('.divHelpSixTeen').classList.remove('slds-hide');
    }
    hideOp17() {
        this.visible = true;
        this.template.querySelector('.divHelpSixTeen').classList.add('slds-hide');
    }
    onclickOp18() {
        this.visible = true;
        this.template.querySelector('.divHelpSevenTeen').classList.remove('slds-hide');
    }
    hideOp18() {
        this.visible = true;
        this.template.querySelector('.divHelpSevenTeen').classList.add('slds-hide');
    }
    onclickOp19() {
        this.visible = true;
        this.template.querySelector('.divHelpEighTeen').classList.remove('slds-hide');
    }
    hideOp19() {
        this.visible = true;
        this.template.querySelector('.divHelpEighTeen').classList.add('slds-hide');
    }
    onclickOp20() {
        this.visible = true;
        this.template.querySelector('.divHelpNineTeen').classList.remove('slds-hide');
    }
    hideOp20() {
        this.visible = true;
        this.template.querySelector('.divHelpNineTeen').classList.add('slds-hide');
    }
    onclickOp21() {
        this.visible = true;
        this.template.querySelector('.divHelpTwenty').classList.remove('slds-hide');
    }
    hideOp21() {
        this.visible = true;
        this.template.querySelector('.divHelpTwenty').classList.add('slds-hide');
    }
    onclickOp22() {
        this.visible = true;
        this.template.querySelector('.divHelpTwentyOne').classList.remove('slds-hide');
    }
    hideOp22() {
        this.visible = true;
        this.template.querySelector('.divHelpTwentyOne').classList.add('slds-hide');
    }
    onclickOp23() {
        this.visible = true;
        this.template.querySelector('.divHelpTwentyTwo').classList.remove('slds-hide');
    }
    hideOp23() {
        this.visible = true;
        this.template.querySelector('.divHelpTwentyTwo').classList.add('slds-hide');
    }
    onclickOp24() {
        this.visible = true;
        this.template.querySelector('.divHelpTwentyThree').classList.remove('slds-hide');
    }
    hideOp24() {
        this.visible = true;
        this.template.querySelector('.divHelpTwentyThree').classList.add('slds-hide');
    }
    onclickOp25() {
        this.visible = true;
        this.template.querySelector('.divHelpTwentyFour').classList.remove('slds-hide');
    }
    hideOp25() {
        this.visible = true;
        this.template.querySelector('.divHelpTwentyFour').classList.add('slds-hide');
    }
    onclickOp26() {
        this.visible = true;
        this.template.querySelector('.divHelpTwentyFive').classList.remove('slds-hide');
    }
    hideOp26() {
        this.visible = true;
        this.template.querySelector('.divHelpTwentyFive').classList.add('slds-hide');
    }
    onclickOp27() {
        this.visible = true;
        this.template.querySelector('.divHelpTwentySix').classList.remove('slds-hide');
    }
    hideOp27() {
        this.visible = true;
        this.template.querySelector('.divHelpTwentySix').classList.add('slds-hide');
    }
    onclickOp28() {
        this.visible = true;
        this.template.querySelector('.divHelpTwentySeven').classList.remove('slds-hide');
    }
    hideOp28() {
        this.visible = true;
        this.template.querySelector('.divHelpTwentySeven').classList.add('slds-hide');
    }
    onclickOp29() {
        this.visible = true;
        this.template.querySelector('.divHelpTwentyEight').classList.remove('slds-hide');
    }
    hideOp29() {
        this.visible = true;
        this.template.querySelector('.divHelpTwentyEight').classList.add('slds-hide');
    }
}