import { LightningElement, track } from 'lwc';

export default class Mx_sb_vts_beneficiariosVSD extends LightningElement {//NOSONAR
    @track allRecords;

    submit() {
        let tables = Array.from( this.template.querySelectorAll("c-mx_sb_vts_table-beneficiarios") );
        this.allRecords = tables.map(table => table.retrieveRecords());
    }
}