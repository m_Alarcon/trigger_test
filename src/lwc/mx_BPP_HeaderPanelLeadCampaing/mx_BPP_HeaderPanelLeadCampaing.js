import { LightningElement, api, wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import idUser from '@salesforce/user/Id';
import LEAD_OBJECT from '@salesforce/schema/Lead';
import NAME_FIELD from '@salesforce/schema/Lead.Name';
import EMAIL_FIELD from '@salesforce/schema/Lead.Email';
import PHONE_FIELD from '@salesforce/schema/Lead.Phone';
import ACCOUNTCAMPID_FIELD from '@salesforce/schema/Lead.MX_WB_RCuenta__c';
import ACCOUNTCAMPNAME_FIELD from '@salesforce/schema/Lead.MX_WB_RCuenta__r.Name';
import AMOUNT_FIELD from '@salesforce/schema/Lead.MX_LeadAmount__c';
import convertLead from '@salesforce/apex/MX_BPP_PanelLeads_Ctrl.convertLead';
import getCampaignMember from '@salesforce/apex/MX_BPP_PanelLeads_Ctrl.getCampignMembers';
import menubt from '@salesforce/apex/MX_BPP_RelatedLeads_Crtl.buttonMenu';

export default class Mx_BPP_HeaderPanelLeadCampaing extends NavigationMixin(LightningElement) { //NOSONAR
    /** Id of record to display. */
    recordCampaignM;
    @api recordId;

    /* Expose schema objects/fields to the template. */
    leadObject = LEAD_OBJECT;
    isSpinner = false;

    /* Load Lead Info for custom rendering */
    @wire(getRecord, { recordId: '$recordId', fields: [NAME_FIELD, PHONE_FIELD, EMAIL_FIELD, ACCOUNTCAMPID_FIELD, ACCOUNTCAMPNAME_FIELD, AMOUNT_FIELD] })
    record;

    @wire(getCampaignMember, {leadId : '$recordId'})
    campaingMemb({ error, data }) {
        if (data) {
            this.recordCampaignM = data;
            this.error;
        } else if (error) {
            this.error = error;
            this.recordCampaignM;
        }
    }

    @wire(menubt, {
        idUser: idUser
    }) buttonMenu;

    get hasMenuBt() {
        return this.buttonMenu;
    }

    convertToLead(e) {
        this.isSpinner = true;
        convertLead({
            leadId: this.recordId,
        })
        .then((result) => {
            this.isSpinner = false;
            if(result.Success){
                this.toasteMessage('success','Correcto:', 'La Oportunidad '+ result.Success.Name + ' fue creada correctamente');
                this[NavigationMixin.Navigate]({
                    type: 'standard__recordPage',
                    attributes: {
                        recordId: result.Success.Id,
                        objectApiName: 'Opportunity',
                        actionName: 'view'
                    }
                });
            }else if(result.Error){
                this.toasteMessage('error','Error: ', result.Error);
            }
        })
        .catch((error) => {
            this.isSpinner = false;
            this.toasteMessage('error','Error: ',error);
        });
    }

    toasteMessage(tipo,titulo,mensaje){
        var evt = new ShowToastEvent({
            title: titulo,
            message: mensaje,
            variant: tipo,
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }

    /** Get the Info value. */
    get name() {
        return this.record.data ? getFieldValue(this.record.data, NAME_FIELD) : '';
    }

    get email() {
        return this.record.data ? getFieldValue(this.record.data, EMAIL_FIELD) : '';
    }

    get telefono() {
        return this.record.data ? getFieldValue(this.record.data, PHONE_FIELD) : '';
    }

    get accountcampid() {
        return this.record.data ? "/" + getFieldValue(this.record.data, ACCOUNTCAMPID_FIELD) : '';
    }

    get accountcampname() {
        return this.record.data ? getFieldValue(this.record.data, ACCOUNTCAMPNAME_FIELD) : '';
    }

    get amount() {
        return this.record.data ? getFieldValue(this.record.data, AMOUNT_FIELD) : '';
    }

    get producto() {
        var productoR;
        if(this.recordCampaignM && this.recordCampaignM.Campaign.MX_WB_Producto__r && this.recordCampaignM.Campaign.MX_WB_Producto__r.Name) {
            productoR = this.recordCampaignM.Campaign.MX_WB_Producto__r.Name
        } else { productoR = '' }
        return  productoR;
    }

    get familia() {
        var familiaR;
        if(this.recordCampaignM && this.recordCampaignM.Campaign.MX_WB_FamiliaProductos__r && this.recordCampaignM.Campaign.MX_WB_FamiliaProductos__r.Name) {
            familiaR = this.recordCampaignM.Campaign.MX_WB_FamiliaProductos__r.Name
        } else { familiaR = '' }
        return familiaR;
    }

    get fechaFin() {
        var fechaf;
        if(this.recordCampaignM && this.recordCampaignM.MX_LeadEndDate__c) {
            fechaf = this.recordCampaignM.MX_LeadEndDate__c
        } else { fechaf = '' }
        return fechaf;
    }
}