import { LightningElement, api, wire, track } from 'lwc';
import retrieveQuestionnaire from '@salesforce/apex/MX_SB_SAC_IdentificarCliente.retrieveQuestionnaire';
import { finishClientAuth } from 'c/mx_sb_sac_utils';

const firstQuestion = 'question0', secondQuestion = 'question1', thirdQuestion = 'question2', yes = 'si', wrong = 'no';

export default class Mx_sb_sac_dynamicQuestionnaire extends LightningElement {
    @api contractId;
    @api prodName;
    @api contractData;
    @api caseData;
    @api prodId;
    @api accId;
    @api type;
    @api comments;
    @track questions = [];
    @track sortQuestions = [];
    @track extraQuestion = [];
    @track values = [];
    @track showThirdQ = false;
    @track showNoAuth = false;
    @track showSave = false;
    @api tipification;
    answers = new Map();
    constructor() {
        super();
        this.template.addEventListener('backquestions', this.handleCustomEvent.bind(this));
    }

    @wire(retrieveQuestionnaire, { contractId: '$contractId', prod: '$prodName', caseData: '$caseData' })
    wiredData({error, data}) {
        if(data && this.caseData.MX_SB_SAC_Aplicar_Mala_Venta__c === 'No') {
            this.questions = data.slice().sort(() => {
                return 0.5 - Math.random();
            });

            let index = 0;
            const dataStruct = {};
            const localArray = [];
            this.questions.forEach(element => {
                dataStruct.key = 'question'+index;
                dataStruct.question = element;
                localArray.push({'key': dataStruct.key, 'question': element});
                index++;
            });
            this.sortQuestions = localArray;
            this.extraQuestion = this.sortQuestions[2];
            while(this.sortQuestions.length > 2) {
                this.sortQuestions.pop();
            }
        } else if(data && this.caseData.MX_SB_SAC_Aplicar_Mala_Venta__c === 'Si') {
            const dataStruct = {}
            const localArray = [];
            let index = 0;
            data.forEach(element => {
                dataStruct.key = 'question'+index;
                dataStruct.question = element;
                localArray.push({'key': dataStruct.key, 'question': element});
                index++;
            });
            this.sortQuestions = localArray;
        } else if(error) {
            this.questions = [];
            throw new Error('Error on retrieveQuestionnaire');
        }
    }

    get options() {
        return [
            { label: 'Si', value: 'si' },
            { label: 'No', value: 'no' }
        ]
    }

    handleOnChange(event) {
        const eventVal = event.target.value, eventName = event.target.name;
        this.values.push({'name':eventName, 'question':eventVal});
        for(let i = 0; i < this.values.length; i++) {
            this.answers.set(this.values[i].name, this.values[i].question);
        }
        if(this.answers.has(firstQuestion) && this.answers.has(secondQuestion)
        && this.caseData.MX_SB_SAC_Aplicar_Mala_Venta__c === 'No') {
            this.handleQuestionnaire(this.answers, eventVal);
        } else if(this.answers.has(firstQuestion) && this.answers.has(secondQuestion) &&
        this.answers.has(thirdQuestion) && this.caseData.MX_SB_SAC_Aplicar_Mala_Venta__c === 'Si') {
            this.handleMVQuestionnaire(this.answers, eventVal);
        }
    }

    handleThirdQ(event) {
        let eventVal = event.target.value;
        if(eventVal === 'si') {
            this.showSave = true;
        } else {
            this.handleShowNoAuth();
        }
    }

    handleSave() {
        finishClientAuth(this.accId, this.contractId, this.caseData, this.prodId, this.tipification, this.comments)
    }

    handleQuestionnaire(questions, eventVal) {
        if(questions.get(firstQuestion) === questions.get(secondQuestion) && eventVal === yes) {
            this.showSave = true;
            this.showThirdQ = false;
        } else if(questions.get(firstQuestion) === wrong && questions.get(secondQuestion) === wrong && eventVal === wrong) {
            this.handleShowNoAuth();
        } else if(questions.get(firstQuestion) !== questions.get(secondQuestion)) {
            this.showThirdQ = true;
            this.showSave = false;
        }
    }

    handleMVQuestionnaire(questions, eventVal) {
        if(questions.get(firstQuestion) === yes && questions.get(secondQuestion) === yes && questions.get(thirdQuestion) === yes) {
            this.showSave = true;
            this.showThirdQ = false;
        } else if(questions.get(firstQuestion) === wrong && questions.get(secondQuestion) === wrong
        && questions.get(thirdQuestion) === wrong) {
            this.handleShowNoAuth();
        } else if(questions.get(firstQuestion) !== questions.get(secondQuestion) && (questions.get(thirdQuestion) === wrong && eventVal === wrong
        || questions.get(thirdQuestion) === yes && eventVal === yes)) {
            this.handleShowNoAuth();
        } else if(questions.get(firstQuestion) === questions.get(secondQuestion) && questions.get(thirdQuestion) === wrong && eventVal === wrong) {
            this.handleShowNoAuth();
        } else if(questions.get(firstQuestion) === wrong && questions.get(secondQuestion) === wrong && questions.get(thirdQuestion) === yes) {
            this.handleShowNoAuth();
        }
    }

    handleGoBack() {
        const backEvent = new CustomEvent('goback', {
            detail: false,
            bubbles: true
        });
        this.dispatchEvent(backEvent);
    }

    handleCustomEvent(event) {
        this.showNoAuth = event.detail;
        this.showThirdQ = false;
        this.answers.clear();
        this.values = [];
    }

    handleShowNoAuth() {
        this.showSave = false;
        this.showNoAuth = true;
    }
}