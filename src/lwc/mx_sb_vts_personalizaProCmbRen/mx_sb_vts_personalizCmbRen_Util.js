import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const showToastMsgRen = (aTitle, aMessage, aVariant) => {
    const aEvt = new ShowToastEvent({
        title: aTitle,
        message: aMessage,
        variant: aVariant
    });
    dispatchEvent(aEvt);
};

const evaluateInactives = (getValStatus, strCodeCober, template) => {
    if (getValStatus === 'Inactivo') {
        template.querySelector('[data-name="' + strCodeCober + '"]').classList.add('class1');
    }
}

const findSelectedVal = (elementLists, porcCover) => {
    let elementItemRen;
    for (let indRex = 0; indRex < elementLists.length; indRex++) {
        let elementRen = elementLists[indRex];
        let splitArray = elementRen.value.split(',');
        if (splitArray[2] === porcCover) {
            elementItemRen = elementRen.value;
        }
    }
    return elementItemRen;
}

const evalueteEntRen = (totalEntry) => {
    let formatCurrencyR = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' });
    if (totalEntry !== '') {
        return formatCurrencyR.format(totalEntry);
    } else {
        return formatCurrencyR.format('0.00');
    }
}

const orderPayList = (setPays, arrPayments) => {
    let arrayPayRen = [];
    let points = Array.from(setPays.values());
    points.sort(function (a, b) { return b - a });
    let mapPrices = new Map();
    for (let index = 0; index < points.length; index++) {
        const element = points[index];
        for (let index2 = 0; index2 < arrPayments.length; index2++) {
            const elementPay = arrPayments[index2];
            if (elementPay.label === evalueteEntRen(element)) {
                if (mapPrices.has(elementPay) === false) {
                    mapPrices.set(element, elementPay);
                }
            }
        }
    }
    mapPrices.forEach(function (valor, clave) {
        arrayPayRen.push(valor);
    });
    return arrayPayRen;
}

const fillListPay = (payList) => {
    let arrayPayRen = [];
    let arrayPayOrder = []
    if (payList !== undefined) {
        const mySet = new Set();
        let paySplit = payList.split('|');
        for (let index = 0; index < paySplit.length; index++) {
            let element = paySplit[index];
            let elementPaysI = element.split(',');
            let elementPay = elementPaysI[0] + ',' + elementPaysI[1] + ',' + elementPaysI[2] + ',' + evalueteEntRen(elementPaysI[3]);
            const option = {
                label: evalueteEntRen(elementPaysI[3]),
                value: elementPay
            };
            mySet.add(parseFloat(elementPaysI[3]));
            arrayPayRen.push(option);
        }
        arrayPayOrder = orderPayList(mySet, arrayPayRen);
    }
    return arrayPayOrder;
}

const fillElementRen = (satotal, totalpay, lstPays, status, selectPay) => {
    return { totalPay: evalueteEntRen(totalpay), SA: evalueteEntRen(satotal), selectPay: selectPay, listPays: lstPays, status: status};
}

const fillBuildingVals = (buldingsArrRen, elementPays) => {
    let totalSARen = 0.00;
    let totalArray = 0.00;
    let statusCov = true;
    let paySelect;
    let listPays = [];
    for (let index = 0; index < buldingsArrRen.length; index++) {
        const element = buldingsArrRen[index];
        if (element.MX_SB_VTS_CoverageCode__c !== undefined && element.MX_SB_VTS_CoverageCode__c === elementPays) {
            statusCov = element.MX_SB_MLT_Estatus__c === 'Activo' ? true : false;
            totalSARen = parseFloat(element.MX_SB_MLT_SumaAsegurada__c);
            listPays = fillListPay(element.MX_SB_VTS_PayList__c);
            if (element.MX_SB_VTS_SelectedPay__c !== undefined) {
                paySelect = element.MX_SB_VTS_SelectedPay__c;
            }
        }
        totalArray = totalArray + parseFloat(element.MX_SB_VTS_CoversAmount__c);
    }
    return fillElementRen(totalSARen, totalArray, listPays, statusCov, paySelect);
}

export { showToastMsgRen, fillBuildingVals, findSelectedVal, evaluateInactives, fillElementRen, evalueteEntRen};