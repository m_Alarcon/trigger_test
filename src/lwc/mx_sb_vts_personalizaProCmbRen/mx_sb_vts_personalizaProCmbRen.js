import { LightningElement, api, track } from 'lwc';
import updCurrentStage from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.updCurrentOppCtrl';
import updateCuponCot from '@salesforce/apex/MX_SB_VTS_infoBasica_Ctrl.updateCuponCot';
import updateCriterial from '@salesforce/apex/MX_SB_VTS_DeleteCoverageHSD_ctrl.editPetsHSD';
import updateCoverage from '@salesforce/apex/MX_SB_VTS_DeleteCoverageHSD_ctrl.removeCoverages';
import mAddCobert from '@salesforce/apex/MX_SB_VTS_AddCoberturas_Ctrl.addCoverages';
import findDataQuo from '@salesforce/apex/MX_SB_VTS_DeleteCoverageHSD_ctrl.fillCotizData';
import fillAllDataCovers from '@salesforce/apex/MX_SB_VTS_DeleteCoverageHSD_ctrl.fillAllDataCovers';
import mGenCotizPrices from '@salesforce/apex/MX_SB_VTS_GetSetDatosPricesSrv_Ctrl.setDataPricesCtrl';
import mGenCotizPricLst from '@salesforce/apex/MX_SB_VTS_GetSetDatosPricesSrv_Ctrl.setDataPricCtrlLst';
import mReDataCotizCtrl from '@salesforce/apex/MX_SB_VTS_GetSetDatosPricesSrv_Ctrl.reDataCotizCtrl';
import dataCobert from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.fillCoberValsCtrl';
import syncQuoteByType from '@salesforce/apex/MX_SB_VTS_DeleteCoverageHSD_ctrl.syncQuoteByType';
import updateDynamicList from '@salesforce/apex/MX_SB_VTS_editCoverageHSD_ctrl.updateDynamicList';
import saveInsuredAmo from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.updateSumas'
import getPicklist from '@salesforce/apex/MX_SB_VTS_SumasAseguradas_Ctrl.getPick';
import savePicklist from '@salesforce/apex/MX_SB_VTS_SumasAseguradas_Ctrl.updatePick';
import getInsuredAmo from '@salesforce/apex/MX_SB_VTS_SumasAseguradas_Ctrl.getInsured';
import getIdSynCmRen from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.gtOppValsCrtl';
import findDataCmRen from '@salesforce/apex/MX_SB_VTS_CotizInitData_Ctrl.findDataClient';
import { getRecordNotifyChange } from 'lightning/uiRecordApi';
import { showToastMsgRen, fillBuildingVals, findSelectedVal, evaluateInactives, fillElementRen, evalueteEntRen } from './mx_sb_vts_personalizCmbRen_Util';
export default class Mx_sb_vts_personalizaProCmbRen extends LightningElement {
    @api cp;
    @api montos;
    @api wrapperParent = {};
    @api cotizProRen = '';
    @api registroid;
    @track idQuoCmbRen;
    @track rterreRisk;
    @track rhidroRisk;
    @track suggestAmount;
    @track attrValCmbRen;
    @track valSelected;
    @track valueAmount = '';
    @track itemsRem = [];
    @track valueRen = '';
    @track infoRen = [];
    @track arrayCut = '';
    @track inceValue = '';
    @track inceItems = [];
    @track roboValue = '';
    @track roboItems = [];
    @track lineaValue = '';
    @track lineaItems = [];
    @track glassValue = '';
    @track glassItems = [];
    @track valorCincuentaRen = 0.50;
    @track valorQuinceRen = 0.15;
    @track valorCienRen = 0.100;
    @track buttonClicRen = false;
    @track isLoading = false;
    @track lstcoberturas = ['btnComMejorandoCasa', 'btnComMedica', 'btnComMascotas', 'btnComIncendio', 'btnComTerremoto',
        'btnComDesaNat', 'btnComRobo', 'btnComEELineaB', 'btnComIncendioCons', 'btnComTerremotoCons', 'btnDesaNatCont2', 'btnTerrCont2',
        'btnComDesaNatAguaCons', 'btnComCivDanosTerCons', 'btnComRotCritalesCons', 'btnBalMejoraCasaAsis', 'btnBalMedicaAsis',
        'btnBalMascotaAsis', 'btnDesaNatCont', 'btnTerrCont', 'btnBalIncendioCont', 'btnBalTerremotoCont', 'btnBalDesNatAguaCont', 'btnBalRoboCont',
        'btnBalEELinBlancaCont', 'btnBasMejorandoCasaAsis', 'btnBasMedicaAsis', 'btnBasMascotasAsis', 'btnBasIncendioCons',
        'btnBasTerremotoCons', 'btnBasDesNatAguaCons', 'btnBasCivilDanosTercCons', 'btnBasRotCritalesCons', 'myBtnRen1', 'myBtnRen6',
        'myBtnRen2', 'myBtnRen4', 'btnRobPertCont', 'btnRobLineaCont'];
    @track totalAnual = '$0.00';
    @track totalSemiAnual = '$0.00';
    @track totalMounth = '$0.00';
    @track totalquartely = '$0.00';
    @track contentgFire = {};
    @track contentFire = {};
    @track contentGlass = {};
    @track contentBurglary = {};
    @track contentDamages = {};
    @track contentCivilityFam = {};
    @track contentCivility = {};
    @track contentInveFurni = {};
    @track buldingHidro = {};
    @track contentHidro = {};
    @track contentElect = {};
    @track contentElectHo = {};
    @track buildingEarqu = {};
    @track contentEarqu = {};
    @track buildingCivility = {};
    @track homeBurglary = {};
    @track sumas = [];
    @track minLimit = 0;
    @track plusVal = 0;
    @track lessVal = 0;
    @api flujo;
    @track optionalEarth = false;
    @track optionalHidro = false;
    @track totalRobo = '$0.00';
    @track totalElec = '$0.00';
    @track mData = new Object();
    @track rData = new Object();

    activeSectionsCmbRen = ['Asistencias', 'Contenidos', 'Construccion'];
    casaCmbRen = 'Cuentas con ayuda de especialistas por si en tu día a día necesitas: plomería, electricidad, cerrajería, albañilería, jardinería y más.';
    medicaCmbRen = 'Cuentas con asesoría médica telefónica, médico a tu domicilio y traslados en ambulancia (terrestre o aérea)';
    mascotasCmbRen = 'Tienes ayuda para tu perro o gato en servicios como: estética, hospedaje, vacunas, asesoría telefónica y más.'
    incendioCmbRen = 'Cubre daños ocasionados a las cosas que estén dentro de tu casa por: incendio o rayo';
    terremotoCmbRen = 'Cubre daños ocasionados a las cosas que estén dentro de tu casa por: terremoto o erupción volcánica';
    hidroCmbRen = 'Cubre daños ocasionados a las cosas que estén dentro de tu casa por: inundación por lluvia, granizo, huracán entre otros.';
    roboCmbRen = 'Protege objetos dentro de tu hogar como: Televisiones, LapTops, computadoras, celulares y más';
    roboArtCmbRen = 'Protege objetos dentro de tu hogar como: joyas, obras de arte, artículos deportivos y más'
    roboDineroCmbRen = 'Cubre el dinero en efectivo dentro del Hogar';
    linea_blancaCmbRen = 'Protege los daños de tu pantalla, tablet, equipo de audio, consola de videojuego, centro de lavado, refrigerador, cafetera, entre otros';
    cristalesCmbRen = 'Cubre la rotura accidental de vidrios o cristales interiores y exteriores como: ventanas, canceles, espejos y más.';
    tercerosCmbRen = 'Cubrimos los daños ocasionados por ti, tu familia o hasta tus mascotas a bienes o personas dentro y fuera de tu hogar';
    rentaHogar = 'Te cubrimos los daños que ocasiones en el lugar que rentas ';
    activeSectionsMessage = '';
    selectVal = 0;
    valSel = 0;
    connectedCallback() {
        this.optionalEarth = true;
        this.optionalHidro = false;
        this.fillPickList();
    }
    firstProcess() {
        this.isLoading = true;
        syncQuoteByType({ oppId: this.registroid, quoteNameId: 'Rentado' }).then(resultSync => {
            fillAllDataCovers({ oppId: this.registroid, quoteNameId: 'Rentado' }).then(result => {
                this.isLoading = false;
                this.processInitRen(result);
            }).catch(error => {
                this.isLoading = false;
                showToastMsgRen(error, 'Error al actualizar Sumas', 'error');
            });
        }).catch(errorSync => {
            showToastMsgRen('Error', 'No se pudo realizar selección de cotización', 'error');
        });
    }
    fillPickList() {
        let insureAmo = this.montos;
        if (insureAmo === '' || insureAmo == null) {
            this.getPick();
        } else {
            if (this.flujo === 'a') {
                this.minLimit = 50000;
                this.methodA();
            } else {
                this.minLimit = 150000;
                this.methodB();
            }
            var attrValCmbRen = this.sumas;
            attrValCmbRen.sort((a, b) => a - b);
            let formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
                minimumFractionDigits: 0,
            });
            for (let keyInCmbRen in attrValCmbRen) {
                const option = {
                    label: formatter.format(attrValCmbRen[keyInCmbRen]),
                    value: String(attrValCmbRen[keyInCmbRen])
                };
                this.itemsRem = [...this.itemsRem, option]
            }
            this.updatePick();
        }

    }
    processInitRen(dataInitR) {
        if (dataInitR.dataCot.dataOpp !== undefined && dataInitR.dataCot.dataOpp[0] !== undefined) {
            let objValsRen = [0];
            if (this.montos !== undefined) {
                objValsRen = this.montos;
            } else if (dataInitR.dataCot.PrimerPropietario__c !== undefined) {
                objValsRen = dataInitR.dataCot.PrimerPropietario__c;
            }
            let valChoose = parseInt(objValsRen[0]);
            let suggestOppR = dataInitR.dataCot.dataOpp[0].Monto_de_la_oportunidad__c;
            this.suggestAmount = suggestOppR !== undefined ? suggestOppR : valChoose;
            this.valueAmount = String(this.suggestAmount);
        }
        let criterialsObject = Object.entries(dataInitR.criterials);
        this.evaluateObjRen(criterialsObject);
    }
    evaluateObjRen(criterialsObject) {
        if (criterialsObject.length > 0 && criterialsObject.length !== undefined) {
            this.evaluateCriteRen(criterialsObject);
        } else {
            this.evaluteNewCotRen(false, false);
        }
    }
    evaluateCriteRen(criterialsObject) {
        let hasCover = false;
        let hasbase = false;
        for (let index = 0; index < criterialsObject.length; index++) {
            let criterial = criterialsObject[index];
            if (criterial[0] === '2008SA') {
                hasCover = true;
            }
            if (criterial[0] === '2008SABASE') {
                hasbase = true;
            }
        }
        this.evaluteNewCotRen(hasCover, hasbase);
    }
    reNewCotRen() {
        this.isLoading = true;
        mReDataCotizCtrl({ strOppoId: this.registroid, strProRent: 'Rentado' }).then(result => {
            this.isLoading = false;
            this.isLoading = true;
            mGenCotizPrices({ strOppoId: this.registroid, strSumAse: this.suggestAmount }).then(resultPrices => {
                this.isLoading = false;
                this.initCoverages();
            }).catch(error => {
                this.isLoading = false;
            });
        }).catch(error => {
            this.isLoading = true;
        });
    }
    evaluteNewCotRen(hasCoverSAR, hasCoverBaseR) {
        if (hasCoverSAR === true && hasCoverBaseR === true) {
            this.initCoverages();
        }
        else if (this.suggestAmount !== undefined) {
            if (hasCoverSAR === true && hasCoverBaseR === false) {
                this.isLoading = true;
                mGenCotizPrices({ strOppoId: this.registroid, strSumAse: this.suggestAmount }).then(result => {
                    this.isLoading = false;
                    this.initCoverages();
                }).catch(error => {
                    this.isLoading = false;
                });
            } else {
                this.reNewCotRen();
            }
        }
    }
    initCoverages() {
        syncQuoteByType({ oppId: this.registroid, quoteNameId: 'Rentado' }).then(resultSync => {
            this.fillWrapObjRen();
        }).catch(errorSync => {
            showToastMsgRen('Error', 'No se pudo realizar selección de cotización', 'error');
        });
    }
    evaluateSelected() {
        this.isLoading = true;
        dataCobert({ oppId: this.registroid }).then(data => {
            this.isLoading = false;
            if (data) {
                for (let key in data) {
                    this.evaluateSelectCove(key, data);
                }
            }
        }).catch(error => {
            this.isLoading = false;
            showToastMsgRen(error, 'Ocurrio un error al Obtener las coberturas preseleccionadas.', 'error');
        });
    }

    evaluateSelectCove(key, data) {
        let getValCoverage = data[key].MX_SB_VTS_CoverageCode__c;
        let getValStatus = data[key].MX_SB_MLT_Estatus__c;
        let getValDatosPar = data[key].MX_SB_MLT_Cobertura__c;
        switch (getValCoverage) {
            case 'HOME_BURGLARY':
                evaluateInactives(getValStatus, 'btnRobPertCont', this.template);
                break;
            case 'HYDROMETEOROLOGICAL_RISKS':
                evaluateInactives(getValStatus, 'btnDesaNatCont', this.template);
                evaluateInactives(getValStatus, 'btnDesaNatCont2', this.template);
                break;
            case 'EARTHQUAKE_VOLCANIC_ERUPTION':
                evaluateInactives(getValStatus, 'btnTerrCont', this.template);
                break;
            case 'ELECTRONIC_EQUIPMENT':
                evaluateInactives(getValStatus, 'btnRobLineaCont', this.template);
                break;
        }
        if (getValDatosPar === '2008ASMASCOTAS') {
            evaluateInactives(getValStatus, 'btnComMascotas', this.template);
        }
    }

    methodA() {
        let objValues = this.montos;
        let valueChoo = parseInt(objValues[0]);
        this.valueAmount = String(objValues[0]);
        switch (valueChoo) {
            case 10000000:
                this.sameMax(valueChoo);
                break;
            case 50000:
                this.sameMin(valueChoo);
                break;
            default:
                this.normalMet(valueChoo);
        }
    }
    methodB() {
        let objValues = this.montos;
        let valueChoo = parseInt(objValues[0]);
        this.valueAmount = String(objValues[0]);
        switch (valueChoo) {
            case 10000000:
                this.sameMax(valueChoo);
                break;
            case 150000:
                this.sameMin(valueChoo);
                break;
            default:
                this.normalMet(valueChoo);
        }
    }
    sameMax(valueChoo) {
        this.sumas.push(valueChoo);
        this.lessVal = valueChoo;
        let percentPro = valueChoo * 0.10;
        for (let i = 0; i < 2; i++) {
            this.lessVal = this.lessVal - percentPro;
            this.sumas.push(this.lessVal);
        }
    }
    sameMin(valueChoo) {
        this.sumas.push(valueChoo);
        this.plusVal = valueChoo;
        let plusPercent = valueChoo * 0.10;
        for (let i = 0; i < 2; i++) {
            this.plusVal = this.plusVal + plusPercent;
            this.sumas.push(this.plusVal);
        }
    }
    normalMet(valueChoo) {
        this.sumas.push(valueChoo);
        this.plusVal = valueChoo;
        this.lessVal = valueChoo;
        let percentVal = valueChoo * 0.10;
        for (let i = 0; i < 2; i++) {
            this.lessVal = this.lessVal - percentVal;
            this.sumas.push(this.lessVal);
        }
        for (let n = 0; n < 2; n++) {
            this.plusVal = this.plusVal + percentVal;
            this.sumas.push(this.plusVal);
        }
        this.limitsRem(this.sumas);
    }
    limitsRem(arrayVals) {
        for (this.valSel; this.valSel < arrayVals.length; this.valSel++) {
            if (arrayVals[this.valSel] < this.minLimit) {
                arrayVals.splice(this.valSel, 1);
                this.valSel--;
            }
        }
        for (this.selectVal = 0; this.selectVal < arrayVals.length; this.selectVal++) {
            if (arrayVals[this.selectVal] > 10000000) {
                arrayVals.splice(this.selectVal, 1);
                this.selectVal--;
            }
        }
        this.sumas = arrayVals;
    }
    get optionsInce() { return this.inceItems; }
    changeIncendio(event) {
        let tempSuggestAm = this.contentFire.selectPay;
        this.contentFire.selectPay = event.detail.value;
        this.updateSumaInce(tempSuggestAm);
    }
    get optionsRobo() { return this.roboItems; }
    changeRobo(event) {
        let tempSelect = this.homeBurglary.selectPay;
        this.homeBurglary.selectPay = event.detail.value;
        this.updatePicklist(event, 'homeBurglary', tempSelect);
    }
    get optionsLinea() { return this.lineaItems; }
    changeLinea(event) {
        let tempSelect = this.contentElect.selectPay;
        this.contentElect.selectPay = event.detail.value;
        this.updatePicklist(event, 'contentElect', tempSelect);
    }
    get optionsGlass() { return this.glassItems; }
    changeGlass(event) {
        let tempSelect = this.contentGlass.selectPay;
        this.contentGlass.selectPay = event.detail.value;
        this.updatePicklist(event, 'contentGlass', tempSelect);
    }
    revertSelect(picklistName, currentVal) {
        switch (picklistName) {
            case 'homeBurglary':
                this.homeBurglary.selectPay = currentVal;
                break;
            case 'contentElect':
                this.contentElect.selectPay = currentVal;
                break;
            case 'contentGlass':
                this.contentGlass.selectPay = currentVal;
                break;
            case 'contentCivilityFam':
                this.contentCivilityFam.selectPay = currentVal;
                break;
        }
    }
    get roleOptionsRen() { return this.itemsRem; }
    handleChangeRen(event) {
        let tempSuggestAm = this.valueAmount;
        this.valueAmount = event.detail.value;
        let checkVal = event.detail.value;
        this.updateSuma(checkVal, tempSuggestAm);
    }
    formatMoney(amountCmb, decimalCountCmb = 0, decimalCmb = ".", thousandsCmb = ",") {
        decimalCountCmb = Math.abs(decimalCountCmb);
        decimalCountCmb = isNaN(decimalCountCmb) ? 2 : decimalCountCmb;
        const negativeSign = amountCmb < 0 ? "-" : "";
        var valoramountCmb = parseInt(amountCmb);
        let i = valoramountCmb.toFixed(decimalCountCmb).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;
        const opPre = negativeSign + (j ? i.substr(0, j) + thousandsCmb : '');
        const opFinal = opPre + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousandsCmb);
        const opUlt = opFinal + (decimalCountCmb ? decimalCmb + Math.abs(amountCmb - i).toFixed(decimalCountCmb).slice(2) : "");
        return opUlt;
    }
    calculateValCmbRen(dataOption, dataCover, dataTrade) {
        if (dataOption !== undefined) {
            let arrValueRen = dataOption.split(',');
            let curreRen = arrValueRen[3];
            let number = Number(curreRen.replace(/[^0-9.-]+/g, ""));
            let valueSelRen = arrValueRen[0] + ',' + arrValueRen[1] + ',' + arrValueRen[2] + ',' + number;
            let lstCoversR = dataCover.split('|');
            let lstCritersR = arrValueRen[1];
            let arrayAddCoversR = [dataTrade];
            arrayAddCoversR.push(arrValueRen[1]);
            arrayAddCoversR.push(arrValueRen[0]);
            arrayAddCoversR.push(arrValueRen[2]);
            arrayAddCoversR.push(valueSelRen);
            updateDynamicList({ oppId: this.registroid, lstAfectCovers: lstCoversR, valuesCriterial: lstCritersR, lstPartDat: arrayAddCoversR }).then(result => {
                this.isLoading = false;
                this.updatePaymentsRen(result);
            }).catch(error => {
                this.isLoading = false;
                showToastMsgRen(error, 'Error al actualizar Sumas', 'error');
            });
        }
    }
    handledNextAction() {
        let upWrapStage = Object.assign({}, this.wrapperContext, { etapa: "emitida" });
        this.wrapperContext = upWrapStage;
        this.nextAction();
    }
    handledBackAction() {
        let upWrapStage = Object.assign({}, this.wrapperContext, { etapa: "newCot" });
        this.wrapperContext = upWrapStage;
        this.backAction();
    }
    backAction() {
        const jsonDetail = { cotizObject: this.wrapperContext, zipCode: this.cp, amount: this.montos };
        const backAction = new CustomEvent("backaction", { detail: jsonDetail, });
        this.dispatchEvent(backAction);
    }
    nextAction() {
        const jsonDetail = { cotizObject: this.wrapperContext, zipCode: this.cp, amount: this.montos };
        const nextAction = new CustomEvent("nextaction", { detail: jsonDetail, });
        this.dispatchEvent(nextAction);
    }
    handleToggleSection(event) {
        this.activeSectionMessage = 'Open section name:  ' + event.detail.openSections;
    }
    handleSectionToggle(event) {
        const openSections = event.detail.openSections;
        if (openSections.length === 0) {
            this.activeSectionsMessage = 'All sections are closed';
        } else {
            this.activeSectionsMessage = 'Open sections: ' + openSections.join(', ');
        }
    }

    onshodetailelem(event) {
        let eventDetail = event.currentTarget.dataset.idshow;
        this.template.querySelector('[data-id="' + eventDetail + '"]').classList.remove('slds-hide');
    }
    onhidetailelem(event) {
        let eventDetail = event.currentTarget.dataset.idshow;
        this.template.querySelector('[data-id="' + eventDetail + '"]').classList.add('slds-hide');
    }
    handleClick(event) {
        this.getDataPrices();
    }
    saveStage() {
        this.updStageName();
    }
    updStageName() {
        let stageNameRen = 'cmbRen';
        updCurrentStage({ srtOppId: this.registroid, etapaForm: stageNameRen }).then(result => {
            showToastMsgRen(result, 'Se guardó correctamente la etapa de Call me back Rentado', 'success');
        }).catch(error => {
            showToastMsgRen(error, 'Ocurrió un error al actualizar la etapa de Call me back Rentado', 'error');
        });
    }
    updateQuoteCup() {
        updateCuponCot({ oppId: this.registroid, cuponCode: '' }).then(result => {
            this.isLoading = false;
        }).catch(error => {
            this.isLoading = false;
            showToastMsgRen(error, 'Error al actualizar Quote', 'error');
        });
    }
    clkButtonCov(event) {
        let sNameButton = event.currentTarget.dataset.name;
        let sNameButtonType = event.currentTarget.dataset.type;
        if (this.lstcoberturas.includes(sNameButton)) {
            if (sNameButtonType === 'required') {
                showToastMsgRen('Agregar / Quitar Coberturas', '¡Esta cobertura es requerida, no puede eliminarse!', 'error');
            } else if (sNameButtonType === 'desactived') {
                showToastMsgRen('Agregar / Quitar Coberturas', '¡Cobertura no disponible, no puede eliminarse!', 'error');
            } else if (sNameButtonType === 'optional') {
                this.changeSelButton(sNameButton, event);
            }
        }
    }
    changeSelButton(sNameButton, eventData) {
        let objClassList = this.template.querySelector('[data-name="' + sNameButton + '"]').classList;
        let isExistClass = false;
        for (let i = 0; i < objClassList.length; i++) {
            if (objClassList[i] === 'class1') {
                isExistClass = true;
                break;
            }
        }
        if (isExistClass) {
            this.template.querySelector('[data-name="' + sNameButton + '"]').classList.remove('class1');
            this.addCobert(eventData);
        } else {
            this.template.querySelector('[data-name="' + sNameButton + '"]').classList.add('class1');
            this.removeCoveragesRen(eventData);
        }
    }
    removeCoveragesRen(eventData) {
        let typeRemCobRen = eventData.currentTarget.dataset.typecover;
        let valRemButtonRen = eventData.currentTarget.dataset.name;
        let valRemCoveRen = eventData.currentTarget.dataset.covercode;
        let lstRemCoveRen = valRemCoveRen.split('|');
        let valRemRamoRen = eventData.currentTarget.dataset.ramo;
        this.isActive = false;
        this.rData.ramo = valRemRamoRen;
        this.rData.categorie = 'INVENTORY_AND_FURNITURE';
        this.rData.isChecked = false;
        if (typeRemCobRen === 'criterial') {
            this.isLoading = true;
            let arrRemCoveRen = [valRemRamoRen];
            arrRemCoveRen.push('001');
            arrRemCoveRen.push('0');
            arrRemCoveRen.push(eventData.currentTarget.dataset.categorie);
            arrRemCoveRen.push(eventData.currentTarget.dataset.goodtype);
            if (valRemCoveRen === 'CODIGO_CUPON') {
                this.isActive = true;
            }
            arrRemCoveRen.push('YEARLY');
            updateCriterial({ oppId: this.registroid, lstPartData: lstRemCoveRen, valuesCriterial: arrRemCoveRen, coverActive: this.isActive }).then(result => {
                this.isLoading = false;
                this.obtDataQuo();
            }).catch(error => {
                this.isLoading = false;
                showToastMsgRen(error, 'Error al actualizar los datos de la Quote(Quitar DP)', 'error');
                this.template.querySelector('[data-name="' + valRemButtonRen + '"]').classList.remove('class1');
            });
        } else if (typeRemCobRen === 'coverage') {
            this.isLoading = true;
            updateCoverage({ oppId: this.registroid, coveragesCodes: lstRemCoveRen, rDataCob: this.rData }).then(result => {
                this.isLoading = false;
                this.obtDataQuo();
            }).catch(error => {
                this.isLoading = false;
                showToastMsgRen(error, 'Error al actualizar los datos de la Quote(Quitar COB)', 'error');
                this.template.querySelector('[data-name="' + valRemButtonRen + '"]').classList.remove('class1');
            });
        }
    }
    processResultCoverages(dataResult, sNameButton) {
        if (dataResult.isOk && dataResult.dataPayments !== undefined) {
            let dataPayments = dataResult.dataPayments;
            this.totalAnual = evalueteEntRen(dataPayments.YEARLY);
            this.totalSemiAnual = evalueteEntRen(dataPayments.SEMESTER);
            this.totalMounth = evalueteEntRen(dataPayments.MONTHLY);
            this.totalquartely = evalueteEntRen(dataPayments.QUARTELY);
            showToastMsgRen('', '¡Coberturas actualizadas!', 'success');
        } else {
            if (dataResult.dataPayments === undefined) {
                showToastMsgRen('Error al actualizar cotización', 'No se recuperaron frecuencias', 'error');
            }
            if (dataResult.isOk === false) {
                showToastMsgRen(dataResult.messageError, 'Error al actualizar cotización', 'error');
            }
            if (sNameButton !== '') {
                this.template.querySelector('[data-name="' + sNameButton + '"]').classList.remove('class1');
            }
        }
    }
    addCobert(eventData) {
        let typeCoregares = eventData.currentTarget.dataset.typecover;
        let valNameCoverage = eventData.currentTarget.dataset.covercode;
        let lstCoverages = valNameCoverage.split('|');
        let valRamo = eventData.currentTarget.dataset.ramo;
        let isActive = true;
        let sNameButton = eventData.currentTarget.dataset.name;
        this.isLoading = true;
        this.mData.ramo = valRamo;
        this.mData.categorie = 'INVENTORY_AND_FURNITURE';
        this.mData.isChecked = true;
        if (typeCoregares === 'criterial') {
            let arrCovers = [eventData.currentTarget.dataset.ramo];
            arrCovers.push('002');
            arrCovers.push('240');
            arrCovers.push(eventData.currentTarget.dataset.categorie);
            arrCovers.push(eventData.currentTarget.dataset.goodtype);
            arrCovers.push('YEARLY');
            updateCriterial({ oppId: this.registroid, lstPartData: lstCoverages, valuesCriterial: arrCovers, coverActive: isActive }).then(result => {
                this.isLoading = false;
                this.processResultCoverages(result, eventData);
            }).catch(error => {
                this.isLoading = false;
                showToastMsgRen(error, 'Error al actualizar Quote', 'error');
                this.template.querySelector('[data-name="' + sNameButton + '"]').classList.add('class1');
            });
        } else if (typeCoregares === 'coverage') {
            mAddCobert({ oppId: this.registroid, coveragesCodes: lstCoverages, mDataCob: this.mData }).then(result => {
                this.isLoading = false;
                showToastMsgRen('Éxito: ', 'Se ha agregado correctamente la cobertura', 'success');
                this.obtDataQuo();
            }).catch(error => {
                this.isLoading = false;
                this.template.querySelector('[data-name="' + sNameButton + '"]').classList.add('class1');
                showToastMsgRen('Error: ', 'Ocurrió un error al agregar la cobertura', 'error');
            });
        }
    }
    obtDataQuo() {
        this.isLoading = true;
        findDataQuo({ oppId: this.registroid }).then(result => {
            this.isLoading = false;
            if (result) {
                var mapValsRen = result.dataQuote;
                for (let key in mapValsRen) {
                    let getValMonthR = mapValsRen[key].MX_SB_VTS_TotalMonth__c;
                    var numMonthRen = '$' + getValMonthR.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                    let getValAnualR = mapValsRen[key].MX_SB_VTS_TotalAnual__c;
                    var numAnualRen = '$' + getValAnualR.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                    let getValQuartRen = mapValsRen[key].MX_SB_VTS_TotalQuarterly__c;
                    var numQuartRen = '$' + getValQuartRen.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                    let getValSemesR = mapValsRen[key].MX_SB_VTS_TotalSemiAnual__c;
                    var numSemiAnRen = '$' + getValSemesR.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                    this.assignValues(numMonthRen, numAnualRen, numQuartRen, numSemiAnRen);
                }
                showToastMsgRen('Éxito: ', 'Se obtuvo datos de la Cotización(Prima asegurada)', 'success');
            } else {
                showToastMsgRen('Importante: ', 'No se pudo obtener datos de la Cotización(Prima asegurada)', 'warning');
            }
        });
    }
    assignValues(numMonth, numAnual, numQuarter, numSemiAn) {
        this.totalMounth = (typeof numMonth !== 'undefined' && numMonth.length ? numMonth : '');
        this.totalAnual = (typeof numAnual !== 'undefined' && numAnual.length ? numAnual : '');
        this.totalSemiAnual = (typeof numSemiAn !== 'undefined' && numSemiAn.length ? numSemiAn : '');
        this.totalquartely = (typeof numQuarter !== 'undefined' && numQuarter.length ? numQuarter : '');
    }
    updateCritPorcen(eventdata) {
        this.isLoading = true;
        let varsPorce = eventdata.detail.value;
        let strCovers = eventdata.currentTarget.dataset.covercode;
        let lstCoversVals = strCovers.split('|');
        let varsPorceVals = varsPorce.split(',');
        let arrCovers = [eventdata.currentTarget.dataset.ramo];
        arrCovers.push(varsPorceVals[0]);
        arrCovers.push(varsPorceVals[2]);
        arrCovers.push(eventdata.currentTarget.dataset.categorie);
        arrCovers.push(eventdata.currentTarget.dataset.goodtype);
        arrCovers.push('YEARLY');
        updateCriterial({ oppId: this.registroid, lstPartData: lstCoversVals, valuesCriterial: arrCovers, coverActive: true }).then(result => {
            this.isLoading = false;
            this.processResultCoverages(result, '');
        }).catch(error => {
            this.isLoading = false;
            showToastMsgRen(error, 'Error al actualizar Quote', 'error');
        });
    }
    getDataPrices() {
        this.isLoading = true;
        fillAllDataCovers({ oppId: this.registroid, quoteNameId: 'Rentado' }).then(result => {
            this.isLoading = false;
            this.getOptiRen();
            this.updateFireAmounts(result);
            this.updatePaymentsRen(result);
        }).catch(error => {
            this.isLoading = false;
            showToastMsgRen(error, 'Error al actualizar Sumas', 'error');
        });
    }
    getOptiRen() {
        getIdSynCmRen({ idOpps: this.registroid }).then(data => {
            const dataValueRen = data;
            for (let key in dataValueRen) {
                let idChildRen = dataValueRen[key].SyncedQuoteId;
                this.idQuoCmbRen = (idChildRen === undefined ? '' : idChildRen);
            }
            this.getCoberRen();
        }).catch(error => {
            throw new Error(error);
        })
    }
    getCoberRen() {
        findDataCmRen({ quoteId: this.idQuoCmbRen }).then(result => {
            if (result) {
                var mapQuoteRen = new Map(Object.entries(result.queteDat));
                let getValTeRen = mapQuoteRen.get('QuoteToPostalCode');
                let getValHiRen = mapQuoteRen.get('ShippingPostalCode');
                this.rterreRisk = (getValTeRen === undefined ? '' : getValTeRen);
                this.rhidroRisk = (getValHiRen === undefined ? '' : getValHiRen);
                this.isLoading = false;
                if (this.rterreRisk === '2' || this.rterreRisk === '3') {
                    this.optionalEarth = false;
                } else {
                    this.optionalEarth = true;
                }
                if (this.rhidroRisk === '1PY' || this.rhidroRisk === '1PS' || this.rhidroRisk === '1GM') {
                    this.optionalHidro = false;
                } else if (this.rhidroRisk === '1IR') {
                    this.optionalHidro = true;
                } else {
                    this.optionalHidro = true;
                }
                showToastMsgRen('Éxito: ', 'Se obtuvo datos de la Cotización getCoberRen', 'success');
                this.evaluateSelected();
            } else {
                showToastMsgRen('Importante: ', 'No se pudo obtener datos de la Cotización getCoberRen', 'warning');
            }

        });
    }
    updateFireAmounts(datResult) {
        let mapCriterials = datResult.criterials;
        let entriCrit = new Map(Object.entries(mapCriterials));
        let fireTrade = datResult.coverages.INCE;
        let inceTrade = fireTrade.FIRE;
        let inventory = inceTrade.INVENTORY_AND_FURNITURE;
        let miscTrade = datResult.coverages.MISC;
        let rcpfTrade = datResult.coverages.RCPF;
        let rihiTrade = datResult.coverages.RIHI;
        let tecnTrade = datResult.coverages.TECN;
        let terrTrade = datResult.coverages.TERR;
        this.processContent(inventory, datResult);
        this.processMisc(miscTrade.MISCELLANEOUS, datResult);
        this.contentCivility = fillBuildingVals(rcpfTrade.CIVIL_LIABILITY_INSURANCE.CIVIL_LIABILITY.CIVIL_LIABILITY_LESSEE, 'CIVIL_LIABILITY_LESSEE');
        this.contentCivilityFam = fillBuildingVals(rcpfTrade.CIVIL_LIABILITY_INSURANCE.CIVIL_LIABILITY.FAMILY_CIVIL_LIABILITY, 'FAMILY_CIVIL_LIABILITY');
        let contCivilFam = entriCrit.get('2008PORCSARC');
        let contCivilFamTra = parseFloat(contCivilFam.MX_SB_VTS_TradeValue__c);
        let contCivilFamStr = contCivilFamTra.toFixed(1);
        this.contentCivilityFam.selectPay = this.contentCivilityFam.selectPay !== undefined ? this.contentCivilityFam.selectPay :
            findSelectedVal(this.contentCivilityFam.listPays, contCivilFamStr);
        this.contentHidro = fillBuildingVals(rihiTrade.HYDROMETEOROLOGICAL_RISKS.INVENTORY_AND_FURNITURE.CONTENT, 'HYDROMETEOROLOGICAL_RISKS');
        this.contentElect = fillBuildingVals(tecnTrade.TECHNICAL.ELECTRONIC_DEVICES.ELECTRONIC_DEVICES, 'ELECTRONIC_EQUIPMENT');
        let conttElect = entriCrit.get('2008PORCEQELEC');
        let conttElectFloat = parseFloat(conttElect.MX_SB_VTS_TradeValue__c);
        let conttElectStr = conttElectFloat.toFixed(1);
        this.contentElect.selectPay = this.contentElect.selectPay !== undefined ? this.contentElect.selectPay : findSelectedVal(this.contentElect.listPays, conttElectStr);
        this.contentElectHo = fillBuildingVals(tecnTrade.TECHNICAL.ELECTRONIC_DEVICES.HOME_APPLIANCE, 'APPLIANCE_EQUIPMENT');
        this.processbuldingEart(terrTrade.EARTHQUAKE.BUILDINGS);
        this.contentEarqu = fillBuildingVals(terrTrade.EARTHQUAKE.INVENTORY_AND_FURNITURE.CONTENT, 'EARTHQUAKE_VOLCANIC_ERUPTION');
        Number(this.contentDamages.totalPay.replace(/[^0-9\.-]+/g, ""))
        let contentDamTemp = this.contentDamages.totalPay === undefined ? 0 : Number(this.contentDamages.totalPay.replace(/[^0-9\.-]+/g, ""));
        let contentBurTemp = this.contentBurglary.totalPay === undefined ? 0 : Number(this.contentBurglary.totalPay.replace(/[^0-9\.-]+/g, ""));
        let homeBurglaTemp = this.homeBurglary.totalPay === undefined ? 0 : Number(this.homeBurglary.totalPay.replace(/[^0-9\.-]+/g, ""));
        this.totalRobo = evalueteEntRen((contentDamTemp + contentBurTemp + homeBurglaTemp));
        let equipElec = this.contentElect.totalPay === undefined ? 0 : Number(this.contentElect.totalPay.replace(/[^0-9\.-]+/g, ""));
        let lineaBlan = this.contentElectHo.totalPay === undefined ? 0 : Number(this.contentElectHo.totalPay.replace(/[^0-9\.-]+/g, ""));
        this.totalElec = evalueteEntRen(equipElec + lineaBlan);
    }
    processbuldingHidro(rihiTrade) {
        if (rihiTrade !== undefined) {
            if (rihiTrade.CONTENT !== undefined) {
                this.buldingHidro = fillBuildingVals(rihiTrade.CONTENT, 'HYDROMETEOROLOGICAL_RISKS');
            }
        }
    }
    fillWrapObjRen() {
        let arrayTempRen = [];
        this.contentFire = fillElementRen('0.00', '0.00', arrayTempRen, true, '');
        this.contentCivilityFam = fillElementRen('0.00', '0.00', arrayTempRen, true, '');
        this.contentInveFurni = fillElementRen('0.00', '0.00', arrayTempRen, true, '');
        this.buldingHidro = fillElementRen('0.00', '0.00', arrayTempRen, true, '');
        this.contentElectHo = fillElementRen('0.00', '0.00', arrayTempRen, true, '');
        this.buildingEarqu = fillElementRen('0.00', '0.00', arrayTempRen, true, '');
        this.contentEarqu = fillElementRen('0.00', '0.00', arrayTempRen, true, '');
        this.contentgFire = fillElementRen('0.00', '0.00', arrayTempRen, true, '');
        this.contentHidro = fillElementRen('0.00', '0.00', arrayTempRen, true, '');
        this.buildingCivility = fillElementRen('0.00', '0.00', arrayTempRen, true, '');
        this.contentCivility = fillElementRen('0.00', '0.00', arrayTempRen, true, '');
        this.contentGlass = fillElementRen('0.00', '0.00', arrayTempRen, true, '');
        this.contentBurglary = fillElementRen('0.00', '0.00', arrayTempRen, true, '');
        this.contentElect = fillElementRen('0.00', '0.00', arrayTempRen, true, '');
        this.contentDamages = fillElementRen('0.00', '0.00', arrayTempRen, true, '');
        this.homeBurglary = fillElementRen('0.00', '0.00', arrayTempRen, true, '');
        this.getDataPrices();
    }
    updatePaymentsRen(dataResult) {
        let dataQuoteRen = dataResult.dataCot.dataQuote;
        var mapQuoteRen = new Map(Object.entries(dataQuoteRen));
        const iterQuoteRen = mapQuoteRen.values();
        let quoteDataRen = iterQuoteRen.next().value;
        this.totalAnual = evalueteEntRen(quoteDataRen.MX_SB_VTS_TotalAnual__c);
        this.totalSemiAnual = evalueteEntRen(quoteDataRen.MX_SB_VTS_TotalSemiAnual__c);
        this.totalMounth = evalueteEntRen(quoteDataRen.MX_SB_VTS_TotalMonth__c);
        this.totalquartely = evalueteEntRen(quoteDataRen.MX_SB_VTS_TotalQuarterly__c);
    }
    processbuldingEart(eartTrade) {
        if (eartTrade !== undefined) {
            if (eartTrade.BUILDING !== undefined) {
                this.buildingEarqu = fillBuildingVals(eartTrade.BUILDING, 'EARTHQUAKE_VOLCANIC_ERUPTION');
            }
        }
    }
    processMisc(miscTrade, dataResult) {
        var mapCriterials = new Map(Object.entries(dataResult.criterials));
        let critalSign = miscTrade.CRYSTALS_SIGNS;
        let glasses = critalSign.GLASSES
        this.contentGlass = fillBuildingVals(glasses, 'GLASS_BREAKAGE');
        let contGlass = mapCriterials.get('2008PORCCRISTAL');
        let glassTrade = parseFloat(contGlass.MX_SB_VTS_TradeValue__c);
        let glassTradeStr = glassTrade.toFixed(2);
        this.contentGlass.selectPay = this.contentGlass.selectPay !== undefined ? this.contentGlass.selectPay : findSelectedVal(this.contentGlass.listPays, glassTradeStr);
        let elecDevices = miscTrade.ELECTRONIC_DEVICES
        let elecDevicesItem = elecDevices.HOME_AMENITIES
        this.homeBurglary = fillBuildingVals(elecDevicesItem, 'HOME_BURGLARY');
        let homeBurPay = mapCriterials.get('2008PORCMENCASA');
        let floatTradeRen = parseFloat(homeBurPay.MX_SB_VTS_TradeValue__c);
        let floatTradestrR = floatTradeRen.toFixed(1);
        this.homeBurglary.selectPay = this.homeBurglary.selectPay !== undefined ? this.homeBurglary.selectPay :
            findSelectedVal(this.homeBurglary.listPays, floatTradestrR);
        let personalItemRen = miscTrade.PERSONAL_ITEMS;
        let personalItemR = personalItemRen.ART_SCULPTURE;
        this.contentDamages = fillBuildingVals(personalItemR, 'THEFT_WORKS_ART');
        let valuableItemsR = miscTrade.VALUABLE_ITEMS;
        let moneyValuesRen = valuableItemsR.MONEY_VALUES;
        this.contentBurglary = fillBuildingVals(moneyValuesRen, 'BURGLARY_WITH_VIOLENCE');
    }
    processBuildingRen(buldingsTradeRen) {
        if (buldingsTradeRen !== undefined) {
            if (buldingsTradeRen.BUILDING !== undefined) {
                this.contentgFire = fillBuildingVals(buldingsTradeRen.BUILDING, 'FIRE_LIGHTNING');
            }
        }
    }
    processContent(inventory, dataResult) {
        if (inventory !== undefined) {
            if (inventory.CONTENT !== undefined) {
                this.contentFire = fillBuildingVals(inventory.CONTENT, 'FIRE_LIGHTNING');
                var mapCriterials = new Map(Object.entries(dataResult.criterials));
                let fireCont = mapCriterials.get('2008PORCSAIC');
                let floatTrade = parseFloat(fireCont.MX_SB_VTS_TradeValue__c);
                let floatTradestr = floatTrade.toFixed(1);
                this.contentFire.selectPay = this.contentFire.selectPay !== undefined ? this.contentFire.selectPay : findSelectedVal(this.contentFire.listPays, floatTradestr);
            }
        }
    }
    updateSuma(insuAmoRen, tempAmount) {
        this.isLoading = true;
        saveInsuredAmo({ oppId: this.registroid, insuredAmo: insuAmoRen }).then(result => {
            this.suggestAmount = insuAmoRen;
            this.isLoading = false;
            this.reNewCotRen();
        }).catch(error => {
            this.valueAmount = tempAmount;
            this.isLoading = false;
            showToastMsgRen(error, 'Error al intentar guardar la suma asegurada', 'error');
        });
    }
    updatePick() {
        let insuAmo = String(this.sumas);
        savePicklist({ srtOppId: this.registroid, insuredAmo: insuAmo }).then(result => {
            this.firstProcess();
            showToastMsgRen('Exito', 'Se ha guardado el picklist', 'success');
        }).catch(error => {
            showToastMsgRen(error, 'Error al intentar guardar las sumas aseguradas', 'error');
        });
    }
    getPick() {
        getPicklist({ srtOppId: this.registroid, }).then(result => {
            let sumasArray = Array.from(result.split(","));
            this.getInsuAmo();
            let formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
                minimumFractionDigits: 0,
            });
            for (let keySrch in sumasArray) {
                this.itemsRem = [...this.itemsRem, {
                    label: formatter.format(sumasArray[keySrch]),
                    value: String(sumasArray[keySrch])
                }];
            }
            showToastMsgRen('Exito', 'Se obtuvo lel listado de sumas', 'success');
        }).catch(error => {
            showToastMsgRen(error, 'Error al intentar recuperar las sumas aseguradas', 'error');
        });
    }
    getInsuAmo() {
        getInsuredAmo({ oppId: this.registroid, }).then(result => {
            let insuredAmo = result;
            showToastMsgRen('Exito', 'Se ha obtenido la suma asegurada', 'success');
            this.valueAmount = String(insuredAmo);
            this.firstProcess();
        }).catch(error => {
            showToastMsgRen(error, 'Error al intentar obtener la suma asegurada', 'error');
        });
    }
    updateSumaInce(tempAmount) {
        let arrValues = this.contentFire.selectPay.split(',');
        let currency = '';
        for (let index = 3; index < arrValues.length; index++) {
            currency = currency + arrValues[index];
        }
        let newSABase = (Number(currency.replace(/[^0-9.-]+/g, ""))).toString();
        let entriesCov = new Map([['criterial', arrValues[1]], ['id_data', arrValues[0]], ['value', arrValues[2]]]);
        let criteSa = new Map([['criterial', '2008SA'], ['id_data', '001'], ['value', newSABase]]);
        let criteSaBase = new Map([['criterial', '2008SABASE'], ['id_data', '001'], ['value', newSABase]]);
        let arrCov = [];
        let objCovers = Object.fromEntries(entriesCov);
        let objSa = Object.fromEntries(criteSa);
        let objSaBase = Object.fromEntries(criteSaBase);
        arrCov.push(objSa, objSaBase, objCovers);
        this.isLoading = true;
        mReDataCotizCtrl({ strOppoId: this.registroid, strProRent: 'Rentado' }).then(result => {
            this.isLoading = false;
            this.isLoading = true;
            mGenCotizPricLst({ strOppoId: this.registroid, strSumAse: newSABase, lstDatPart: arrCov }).then(resultPrices => {
                this.isLoading = false;
                getRecordNotifyChange([{ recordId: this.registroid }]);
                this.initCoverages();
            }).catch(error => {
                showToastMsgRen(error, 'Error al generar nueva cotización', 'error');
                this.isLoading = false;
            });
        }).catch(error => {
            showToastMsgRen(error, 'Error al generar nueva cotización', 'error');
            this.isLoading = false;
        });
    }
    updatePicklist(dataEvent, picklistName, tempSelect) {
        if (dataEvent !== undefined) {
            let dataOptionRen = dataEvent.detail.value;
            let dataCover = dataEvent.currentTarget.dataset.covercode;
            let dataTrade = dataEvent.currentTarget.dataset.ramo;
            let arrValues = dataOptionRen.split(',');
            let currencyRen = '';
            for (let index = 3; index < arrValues.length; index++) {
                currencyRen = currencyRen + arrValues[index];
            }
            let valueSelectRen = dataEvent.currentTarget.dataset.criterialcode;
            let lstCoversRen = dataCover.split('|');
            let lstCritersRen = valueSelectRen.split('|');
            let arrayAddCoverRen = [dataTrade];
            arrayAddCoverRen.push(arrValues[1]);
            arrayAddCoverRen.push(arrValues[0]);
            arrayAddCoverRen.push(arrValues[2]);
            arrayAddCoverRen.push(dataOptionRen);
            this.isLoading = true;
            updateDynamicList({ oppId: this.registroid, lstAfectCovers: lstCoversRen, valuesCriterial: lstCritersRen, lstPartDat: arrayAddCoverRen }).then(result => {
                this.isLoading = false;
                this.updatePaymentsRen(result);
                this.updateFireAmounts(result);
            }).catch(error => {
                this.revertSelect(picklistName, tempSelect);
                this.isLoading = false;
                showToastMsgRen(error, 'Error al actualizar Sumas', 'error');
            });
        }
    }

    damageChangeRen(event) {
        let tempSelect = this.contentCivilityFam.selectPay;
        this.contentCivilityFam.selectPay = event.detail.value;
        this.updatePicklist(event, 'contentCivilityFam', tempSelect);
    }
}