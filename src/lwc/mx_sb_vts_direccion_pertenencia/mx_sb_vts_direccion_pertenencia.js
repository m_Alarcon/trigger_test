import { LightningElement, api, track } from 'lwc';
import { getRecordNotifyChange } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getDataInputCP from '@salesforce/apex/MX_SB_VTS_GetSetDPForm_Ctrl.dataInputCP';
import mSetDataFormDCDP from '@salesforce/apex/MX_SB_VTS_GetSetDatosDCDPFrm_Ctrl.setDataFormDCDP';
import mSetDataFormComSrv from '@salesforce/apex/MX_SB_VTS_GetSetDatosDCDPFrm_Ctrl.setDataFormComSrv';
import updCurrentStage from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.updCurrentOppCtrl';
import getAddValues from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.getValAddressCtrl';
import findDataClient from '@salesforce/apex/MX_SB_VTS_CotizInitData_Ctrl.findDataClient';
import updCrtQuo from '@salesforce/apex/MX_SB_VTS_InitQuo_Ctrl.updSyncAddCtrl';
import updCurrAdd from '@salesforce/apex/MX_SB_VTS_GetSetDatosDCDPFrm_Ctrl.saveAddReCtrl';


export default class Mx_sb_vts_direccion_pertenencia extends LightningElement {

    @api cp = new Object();
    @api infoParam;
    @api idquotechild;
    @api wrapperParent = {};
    @api item = '';
    @api registroid = '';
    @api itemEstado = '';
    @api itemCiudad = '';
    @api itemAlcaldia = '';
    @api cotizObject = '';
    @api wrapperparent;
    @track getSumValue = {};
    @track sfDatosGenerales = [];
    @track sfDatosGeneralesParam = [];
    @track sfDatosColonias = [];
    @track sfDatosColoniasParams = [];
    @track oDataOptions = [];
    @track strVal = '';
    @track extVal = '';
    @track cpVal1 = '';
    @track strVal1 = '';
    @track extVal1 = '';
    @track sCPParamInit = '';
    @track cpEstadoVal1 = '';
    @track cpCiudadVal1 = '';
    @track cpAlcaldiaVal1 = '';
    @track cmbDataColoniaLbl = '';
    @track cmbDataColoniaLblDC = '';
    @track colVal1Lbl;
    @track colValDP;
    @track valDpCol;
    @track colValDpAse;
    @track valDpColAse;
    @track sumAseg;
    @track sumVal;
    @track cpVal;
    @track colVal;
    @track colValLbl;
    @track colVal1;
    @track intVal;
    @track intVal1;
    @track cpVal2;
    @track colVal2;
    @track strVal2;
    @track extVal2;
    @track intVal2;
    @track cpVal4 = '';
    @track cmbDataColoniaVal;
    @track cmbDataColoniaValDC;
    @track cmbSelDirSendLbl;
    @track cmbSelDirSendVal;
    @track oDataParamsDC;
    @track objCPDirProp;
    @track objCPDirCont;
    @track typeAddress;
    @track typeAddressAse;
    @track enviPoliz;
    @track enviPoliz1;
    @track chkenvpolcltesi = false;
    @track chkenvpolclteno = true;
    @track dsbAddressSend = true;
    @track checkboxVal = false;
    @track checkboxVal2 = true;
    @track bSelectDirSend = false;
    @track bSelDirSend = false;
    @track isLoading = false;
    @track isChecked = false;
    @track isCheckedAse = false;
    @track numExt;
    @track numInt;
    @track getStrVal;
    @track getItemVal;
    @track getItemValEstado;
    @track getItemValCiudad;
    @track getItemValAlcaldia;
    @track getEnviPoliz;
    @track valCol;
    @track numExt1;
    @track numInt1;
    @track getStrVal1;
    @track getItemVal1;
    @track getItemValEstado1;
    @track getItemValCiudad1;
    @track getItemValAlcaldia1;
    @track getEnviPoliz1;
    @track valColAse;
    @track fnumExt;
    @track fnumInt;
    objDirPropiedad = new Object();
    objDirContratante = new Object();
    objDirPertenencia = new Object();
    objDataForm = new Object();

    connectedCallback() {
        this.isLoading = true;
        setTimeout(() => {
            this.getDirVal();
        }, 1000);
        setTimeout(() => {
            this.getDirValCont();
        }, 3000);
    }

    getDirVal() {
        this.typeAddress = 'Domicilio asegurado';
        getAddValues({
            iddOpps: this.registroid, dirType: this.typeAddress
        }).then(data => {
            if (data) {
                this.obtDomAse(data);
                this.getPostalCode();
                if (this.infoParam) {
                    this.oDataParamsDC = JSON.parse(this.infoParam);
                } else {
                    this.fillDataDc();
                }
                this.showToastMessage('Éxito', 'Obtuvo información de las Direcciones.', 'success');
            } else {
                if (this.infoParam !== undefined) {
                    this.oDataParamsDC = JSON.parse(this.infoParam);
                    this.sCPParamInit = this.cp['cp'];
                }
            }
            this.isLoading = false;
        }).catch(error => {
            this.isLoading = false;
            this.showToastMessage('Error', 'Ocurrio un error al Obtener Dirección de la Propiedad.', 'error');
        })
    }

    obtDomAse(dataValues) {
        for (let key in dataValues) {
            this.numExt = dataValues[key].MX_RTL_AddressExtNumber__c;
            this.numInt = dataValues[key].MX_RTL_AddressIntNumber__c;
            this.getStrVal = dataValues[key].MX_RTL_AddressStreet__c;
            this.getItemVal = dataValues[key].MX_RTL_PostalCode__c;
            this.getItemValEstado = dataValues[key].MX_RTL_AddressState__c;
            this.getItemValCiudad = dataValues[key].MX_RTL_AddressMunicipality__c;
            this.getItemValAlcaldia = dataValues[key].MX_RTL_AddressCity__c;
            this.getEnviPoliz = dataValues[key].MX_RTL_SendPolice__c;
            this.sfDatosColoniasParams = [...this.sfDatosColoniasParams, {
                label: dataValues[key].MX_SB_VTS_Colonias__c,
                value: dataValues[key].MX_SB_VTS_Colonias__c
            }];
            this.valCol = this.sfDatosColoniasParams[0].value;
            this.assigmentVals();
            this.isChecked = true;
        }
    }

    assigmentVals() {
        this.fnumExt = (this.numExt === undefined ? '' : this.numExt);
        this.fnumInt = (this.numInt === undefined ? '' : this.numInt);
        this.strVal = (this.getStrVal === undefined ? '' : this.getStrVal);
        this.item = (this.getItemVal === undefined ? '' : this.getItemVal);
        this.itemEstado = (this.getItemValEstado === undefined ? '' : this.getItemValEstado);
        this.itemCiudad = (this.getItemValCiudad === undefined ? '' : this.getItemValCiudad);
        this.itemAlcaldia = (this.getItemValAlcaldia === undefined ? '' : this.getItemValAlcaldia);
        this.enviPoliz = (this.getEnviPoliz === undefined ? false : this.getEnviPoliz);
        this.valDpCol = (this.valCol === undefined ? '' : this.valCol);
        this.sCPParamInit = this.item;
        this.anotherVals();
    }

    anotherVals() {
        if (this.fnumExt !== '') {
            this.extVal = this.fnumExt;
        } else {
            this.intVal = this.fnumInt;
        }
        if (this.enviPoliz === true) {
            this.chkenvpolcltesi = false;
            this.chkenvpolclteno = true;
        }
    }

    getDirValCont() {
        this.typeAddressAse = 'Domicilio cliente';
        getAddValues({
            iddOpps: this.registroid, dirType: this.typeAddressAse
        }).then(data => {
            if (data) {
                this.obtDomCont(data);
                this.postalCode();
            } else {
                this.showToastMessage('Atención', 'No existe el registro o no se obtuvieron datos del contratante', 'info');
            }
        }).catch(error => {
            this.isLoading = false;
        })
    }

    obtDomCont(dataValAse) {
        for (let key in dataValAse) {
            this.numExt1 = dataValAse[key].MX_RTL_AddressExtNumber__c;
            this.numInt1 = dataValAse[key].MX_RTL_AddressIntNumber__c;
            this.getStrVal1 = dataValAse[key].MX_RTL_AddressStreet__c;
            this.getItemVal1 = dataValAse[key].MX_RTL_PostalCode__c;
            this.getItemValEstado1 = dataValAse[key].MX_RTL_AddressState__c;
            this.getItemValCiudad1 = dataValAse[key].MX_RTL_AddressMunicipality__c;
            this.getItemValAlcaldia1 = dataValAse[key].MX_RTL_AddressCity__c;
            this.getEnviPoliz1 = dataValAse[key].MX_RTL_SendPolice__c;
            this.sfDatosColonias = [...this.sfDatosColonias, {
                label: dataValAse[key].MX_SB_VTS_Colonias__c,
                value: dataValAse[key].MX_SB_VTS_Colonias__c
            }];
            this.valColAse = this.sfDatosColonias[0].value;
            this.assigAse();
            this.isCheckedAse = true;
        }
    }

    assigAse() {
        this.strVal1 = (this.getStrVal1 === undefined ? '' : this.getStrVal1);
        this.cpVal4 = (this.getItemVal1 === undefined ? '' : this.getItemVal1);
        this.cpEstadoVal1 = (this.getItemValEstado1 === undefined ? '' : this.getItemValEstado1);
        this.cpCiudadVal1 = (this.getItemValCiudad1 === undefined ? '' : this.getItemValCiudad1);
        this.cpAlcaldiaVal1 = (this.getItemValAlcaldia1 === undefined ? '' : this.getItemValAlcaldia1);
        this.enviPoliz1 = (this.getEnviPoliz1 === undefined ? false : this.getEnviPoliz1);
        this.extVal1 = (this.numExt1 === undefined ? '' : this.numExt1);
        this.intVal1 = (this.numInt1 === undefined ? '' : this.numInt1);
        this.valDpColAse = (this.valColAse === undefined ? '' : this.valColAse);
        this.anothVal();
    }

    anothVal() {
        if (this.enviPoliz1 === true) {
            this.chkenvpolcltesi = false;
            this.chkenvpolclteno = true;
        }
        if (this.cpVal4 === null || this.cpVal4 === '') {
            this.checkboxVal = false;
            this.checkboxVal2 = true;
        } else {
            this.checkboxVal = true;
            this.checkboxVal2 = false;
        }
    }

    fillDataDc() {
        findDataClient({
            quoteId: this.idquotechild
        }).then(result => {
            if (result) {
                var mapQuote = new Map(Object.entries(result.queteDat));
                let tempWrapp = new Object();
                let sexGen = mapQuote.get('MX_SB_VTS_Sexo_Conductor__c');
                tempWrapp.NombreCliente = mapQuote.get('MX_SB_VTS_Nombre_Contrante__c');
                tempWrapp.ApellidoPaterno = mapQuote.get('MX_SB_VTS_Apellido_Paterno_Contratante__c');
                tempWrapp.ApellidoMaterno = mapQuote.get('MX_SB_VTS_Apellido_Materno_Contratante__c');
                if (sexGen === 'H') {
                    tempWrapp.SexoMasculino = 'true';
                    tempWrapp.SexoFemenino = 'false';
                } else {
                    tempWrapp.SexoMasculino = 'false';
                    tempWrapp.SexoFemenino = 'true';
                }
                let fechaDate = mapQuote.get('MX_SB_VTS_FechaNac_Contratante__c').split('-');
                tempWrapp.FechaNac = fechaDate[2] + '/' + fechaDate[1] + '/' + fechaDate[0];
                tempWrapp.LugarNac = mapQuote.get('MX_SB_VTS_LugarNac_Contratante__c');
                tempWrapp.rfc = mapQuote.get('MX_SB_VTS_RFC__c');
                tempWrapp.Homoclave = mapQuote.get('MX_SB_VTS_Homoclave_Contratante__c');
                tempWrapp.TelefonoCel = mapQuote.get('Phone');
                tempWrapp.CorreoElect = mapQuote.get('MX_SB_VTS_Email_txt__c');
                tempWrapp.Curp = mapQuote.get('MX_SB_VTS_Curp_Contratante__c');
                tempWrapp.OppoId = this.registroid;
                tempWrapp.QuoteId = this.idquotechild;
                let jsonStr = JSON.stringify(tempWrapp);
                this.oDataParamsDC = JSON.parse(jsonStr);
                this.showToastMessage('Éxito', 'Obtuvo Información de la Cotización.', 'success');
            } else {
                this.showToastMessage('Error', 'Ocurrio un error al Obtener los datos de la Cotización.', 'error');
            }
        });
    }
    onSave() {
        this.updQuoDir();
        this.updAddress();
        this.updStage();
    }

    updStage() {
        let stageName = 'formalizada';
        updCurrentStage({
            srtOppId: this.registroid, etapaForm: stageName
        }).then(result => {
            this.showToastMessage('Éxito', 'Se acualizó la etapa correctamente a Formalizada.', 'success');
        }).catch(error => {
            this.showToastMessage('Error', 'Ocurrio un error al actualizar la etapa a Formalizada.', 'error');
        });
    }

    updAddress() {
        const dataAddress = this.getDataFieldsObject();
        updCurrAdd({
            dataAdd: dataAddress
        }).then(result => {
            this.showToastMessage('Éxito', 'Se acualizaron los datos de Direcciones', 'success');
        }).catch(error => {
            this.showToastMessage('Error', 'Ocurrio un error al actualizar los registros de Direcciones.', 'error');
        });
    }

    updQuoDir() {
        updCrtQuo({
            currId: this.registroid
        }).then(result => {
            this.showToastMessage('Éxito', 'Se actualizó correctamente la Quote en Direcciones.', 'success');
        }).catch(error => {
            this.showToastMessage('Error', 'Ocurrio un error al actualizar la Quote en Direcciones.', 'error');
        });
    }

    updStageFlujo() {
        let stageCobro = 'cobro';
        updCurrentStage({
            srtOppId: this.registroid, etapaForm: stageCobro
        }).then(result => {
            getRecordNotifyChange([{ recordId: this.registroid }]);
            this.showToastMessage('Éxito', 'Se actualizó correctamente la etapa a Cobro.', 'success');
        }).catch(error => {
            this.showToastMessage('Error', 'Ocurrio un error al actualizar la etapa a Cobro y resumen.', 'error');
        });
    }

    handleClick(event) {
        this.checkboxVal = event.target.checked;
        this.checkboxVal2 = false;
        this.getFormThird();
        this.checkValidity();
    }

    handleClickTwo(event) {
        this.checkboxVal2 = event.target.checked;
        this.checkboxVal = false;
        this.checkValidity();
        this.getFormThird();
    }

    checkValidity() {
        const checkVal1 = this.checkboxVal;
        const checkVal2 = this.checkboxVal2;
        if (checkVal1 === false && checkVal2 === false) {
            this.checkboxVal2 = true;
        }
    }

    handleOptEnvSi(event) {
        this.chkenvpolcltesi = event.target.checked;
        this.chkenvpolclteno = false;
        this.dsbAddressSend = false;
        this.obtDirEnvPoliz();
        this.checkValEnvPoliz();
    }

    handleOptEnvNo(event) {
        this.chkenvpolclteno = event.target.checked;
        this.chkenvpolcltesi = false;
        this.dsbAddressSend = true;
        this.checkValEnvPoliz();
    }

    checkValEnvPoliz() {
        const chkEnvPolizSi = this.chkenvpolcltesi;
        const chkEnvPolizNo = this.chkenvpolclteno;
        if (chkEnvPolizSi === false && chkEnvPolizNo === false) {
            this.chkenvpolclteno = true;
            this.dsbAddressSend = true;
        }
    }

    handleValDirContra() {
        let lblDirContra = null;
        let sSpace = ', ';
        let sNothing = '';
        if (this.checkboxVal) {
            let sNIntDC = (this.intVal1 === undefined ? '' : this.intVal1);
            if (this.cpVal1 === sNothing || this.cpEstadoVal1 === sNothing ||
                this.cpCiudadVal1 === sNothing || this.cpAlcaldiaVal1 === sNothing ||
                this.strVal1 === sNothing || this.extVal1 === sNothing ||
                this.cmbDataColoniaLblDC === sNothing) {
                this.bSelDirSend = true;
                this.chkenvpolcltesi = false;
                this.chkenvpolclteno = true;
            } else {
                lblDirContra = 'Dirección del Contratante: ' +
                    this.strVal1 + sSpace + this.extVal1 + sSpace +
                    sNIntDC + sSpace + this.cmbDataColoniaLblDC + sSpace +
                    this.cpVal1 + sSpace + this.cpAlcaldiaVal1 + sSpace +
                    this.cpCiudadVal1 + sSpace + this.cpEstadoVal1;
                this.oDataOptions.push(
                    { label: lblDirContra, value: 'dircontratante' }
                );
                this.bSelDirSend = true;
                this.chkenvpolcltesi = false;
                this.chkenvpolclteno = true;
            }
        }
        if (this.bSelDirSend) {
            this.dsbAddressSend = false;
        } else {
            this.dsbAddressSend = true;
        }
    }
    obtValDirDP() {
        let lblDirPropiedad = null;
        let sSpace = ', ';
        let sNothing = '';
        let sNoInt = (this.intVal === undefined ? '' : this.intVal);
        if (this.item === sNothing || this.itemEstado === sNothing ||
            this.itemCiudad === sNothing || this.itemAlcaldia === sNothing ||
            this.strVal === sNothing || this.extVal === sNothing ||
            this.cmbDataColoniaLbl === undefined) {
            if (this.checkboxVal) {
                this.showToastMessage('Validación de Campos: Dirección de la Propiedad.', 'Campos Requeridos. No se mostrara la opción: Dirección de la Propiedad.', 'info');
                this.bSelectDirSend = true;
            } else {
                this.showToastMessage('Validación de Campos.', 'No cuenta con información de: Dirección de la propiedad / Dirección del Contratante.', 'info');
            }
        } else {
            lblDirPropiedad = 'Dirección de la Propiedad: ' +
                this.strVal + sSpace + this.extVal + sSpace +
                sNoInt + sSpace + this.cmbDataColoniaLbl + sSpace +
                this.item + sSpace + this.itemAlcaldia + sSpace +
                this.itemCiudad + sSpace + this.itemEstado;
            this.oDataOptions.push(
                { label: lblDirPropiedad, value: 'dirpropiedad' }
            );
            this.bSelectDirSend = true;
        }
    }

    obtValDirDC() {

        let lblDirContratante = null;
        let sSpace = ', ';
        let sNothing = '';
        if (this.checkboxVal) {
            let sNoIntDC = (this.intVal1 === undefined ? '' : this.intVal1);
            if (this.cpVal1 === sNothing || this.cpEstadoVal1 === sNothing ||
                this.cpCiudadVal1 === sNothing || this.cpAlcaldiaVal1 === sNothing ||
                this.strVal1 === sNothing || this.extVal1 === sNothing ||
                this.cmbDataColoniaLblDC === sNothing) {
                this.showToastMessage('Validación de Campos: Dirección del Contratante.', 'Campos Requeridos. No se mostrara la opción: Dirección del Contratante.', 'info');
                this.bSelectDirSend = true;
            } else {
                lblDirContratante = 'Dirección del Contratante: ' +
                    this.strVal1 + sSpace + this.extVal1 + sSpace +
                    sNoIntDC + sSpace + this.cmbDataColoniaLblDC + sSpace +
                    this.cpVal1 + sSpace + this.cpAlcaldiaVal1 + sSpace +
                    this.cpCiudadVal1 + sSpace + this.cpEstadoVal1;
                this.oDataOptions.push(
                    { label: lblDirContratante, value: 'dircontratante' }
                );
                this.bSelectDirSend = true;
            }
        }
    }

    obtDirEnvPoliz() {

        this.oDataOptions = [];
        this.obtValDirDP();
        this.obtValDirDC();
        if (this.bSelectDirSend) {
            this.dsbAddressSend = false;
        } else {
            this.dsbAddressSend = true;
        }
    }

    obtDataColonia(event) {

        this.cmbDataColoniaVal = event.detail.value;
        this.cmbDataColoniaLbl = event.target.options.find(opt => opt.value === event.detail.value).label;
    }

    obtDataColoniaDC(event) {
        this.cmbDataColoniaValDC = event.detail.value;
        this.cmbDataColoniaLblDC = event.target.options.find(opt => opt.value === event.detail.value).label;
    }

    obtDataSelSend(event) {
        this.cmbSelDirSendVal = event.detail.value;
        this.cmbSelDirSendLbl = event.target.options.find(opt => opt.value === event.detail.value).label;
    }

    getPostalCode() {
        this.isLoading = true;
        getDataInputCP({
            sCodigoPostal: this.sCPParamInit
        }).then(data => {
            this.objCPDirProp = data;
            this.item = this.item;
            this.itemAlcaldia = this.itemAlcaldia;
            this.itemCiudad = this.itemCiudad;
            this.itemEstado = this.itemEstado;
            if (this.item !== '' || this.item !== undefined) {
                this.sfDatosGeneralesParam = data.SFDatosGenerales;
                this.sfDatosColoniasParams = data.SFDatosColonias;
                if (this.isChecked) {
                    this.validChecked();
                }
            }
        }).catch(error => {
            this.isLoading = false;
            this.showToastMessage('Error', 'Ocurrio un error al obtener datos del Código Postal.', 'error');
        })
    }

    validChecked() {
        let nArray = this.sfDatosColoniasParams;
        for (let key in nArray) {
            if (this.valDpCol === this.sfDatosColoniasParams[key].label) {
                this.colValDP = this.sfDatosColoniasParams[key].value;
                this.cmbDataColoniaVal = this.sfDatosColoniasParams[key].value;
                this.cmbDataColoniaLbl = this.sfDatosColoniasParams[key].label;
                break;
            }
        }
    }

    postalCode(event) {
        if (this.cpVal4.length === 5) {
            this.cpVal1 = this.cpVal4;
        } else {
            this.cpVal1 = event.target.value;
        }

        if (this.cpVal1.length === 5) {
            getDataInputCP({ sCodigoPostal: this.cpVal1 }).then(result => {
                this.objCPDirCont = result;
                this.sfDatosGenerales = result.SFDatosGenerales;
                this.cpEstadoVal1 = this.sfDatosGenerales[0].state;
                this.cpCiudadVal1 = this.sfDatosGenerales[0].city;
                this.cpAlcaldiaVal1 = this.sfDatosGenerales[0].country;
                this.sfDatosColonias = result.SFDatosColonias;
                if (this.isCheckedAse) {
                    let nArrayDp = this.sfDatosColonias;
                    for (let key in nArrayDp) {
                        if (this.valDpColAse === this.sfDatosColonias[key].label) {
                            this.colValDpAse = this.sfDatosColonias[key].value;
                            this.cmbDataColoniaValDC = this.sfDatosColonias[key].value;
                            this.cmbDataColoniaLblDC = this.sfDatosColonias[key].label;
                            break;
                        }
                    }
                }
            }).catch(error => {
                this.showToastMessage(error, 'Ocurrio un error al momento de recuperar los datos del CP en DP', 'error');
            })
        }

    }

    getInitPostalCode() {
        if (this.item.length === 5) {
            getDataInputCP({ sCodigoPostal: this.item }).then(result => {
            }).catch(error => {
                this.showToastMessage('Error', 'Ocurrio un error al momento de recuperar los datos del CP', 'error');
            })
        }
    }

    getFormVal(event) {
        if (event.target.name === 'str_estado_val') {
            this.itemEstado = event.target.value;
        } else if (event.target.name === 'str_ciudad_val') {
            this.itemCiudad = event.target.value;
        } else if (event.target.name === 'str_alcaldia_val') {
            this.itemAlcaldia = event.target.value;
        } else if (event.target.name === 'str_colonia_val') {
            this.colVal = event.target.value;
            this.colValLbl = event.target.options.find(opt => opt.value === event.detail.value).label;
        } else if (event.target.name === 'str_Val') {
            this.strVal = event.target.value;
            const valStr = this.strVal;
            this.strVal2 = valStr;
        } else if (event.target.name === 'ext_Val') {
            this.extVal = event.target.value;
            const valExt = this.extVal;
            this.extVal2 = valExt;
        } else if (event.target.name === 'int_Val') {
            this.intVal = event.target.value;
            const valInt = this.intVal;
            this.intVal2 = valInt;
        }
        if (event.target.name === 'str_estado_val1') {
            this.cpEstadoVal1 = event.target.value;
        } else if (event.target.name === 'str_ciudad_val1') {
            this.cpCiudadVal1 = event.target.value;
        } else if (event.target.name === 'cpAlcaldiaVal1') {
            this.cpAlcaldiaVal1 = event.target.value;
        } else if (event.target.name === 'col_Val1') {
            this.colVal1 = event.target.value;
            this.colVal1Lbl = event.target.options.find(opt => opt.value === event.detail.value).label;
        } else if (event.target.name === 'str_Val1') {
            this.strVal1 = event.target.value;
        } else if (event.target.name === 'ext_Val1') {
            this.extVal1 = event.target.value;
        } else if (event.target.name === 'int_Val1') {
            this.intVal1 = event.target.value;
        }
    }
    getFormThird() {
        const checkVal = this.checkboxVal;
        const checkVal2 = this.checkboxVal2;
        if (checkVal === true) {
            this.colVal2 = '';
            this.strVal2 = '';
            this.extVal2 = '';
            this.intVal2 = '';
        } else if (checkVal2 === true) {
            const valCol = this.colVal;
            this.colVal2 = valCol;
            const valStr = this.strVal;
            this.strVal2 = valStr;
            const valExt = this.extVal;
            this.extVal2 = valExt;
            const valInt = this.intVal;
            this.intVal2 = valInt;
        }
    }

    handledBackAction(event) {
        let upWrapStage = Object.assign({}, this.wrapperContext, {
            etapa: "tarificada"
        });
        this.wrapperContext = upWrapStage;
        this.backAction();
    }

    backAction() {
        const jsonDetail = { cotizObject: this.wrapperContext, zipCode: this.cp };
        const backAction = new CustomEvent("backaction", {
            detail: jsonDetail,
        });
        this.dispatchEvent(backAction);
    }

    fireEve() {
        const validFieldsDP = [...this.template.querySelectorAll('[data-dp="fieldsdp"]')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);
        if (validFieldsDP) {
            this.condVals();
        } else {
            this.showToastMessage('Error', 'Complete los campos requeridos antes de continuar', 'error');
        }
    }

    condVals() {
        if (this.checkboxVal) {
            const validFiledsDC = [...this.template.querySelectorAll('[data-dc="fieldsdc"]')]
                .reduce((validSoFar, inputCmp) => {
                    inputCmp.reportValidity();
                    return validSoFar && inputCmp.checkValidity();
                }, true);
            if (validFiledsDC) {
                const oDataForm = this.getDataFieldsObject();
                if (oDataForm.isSuccess) {
                    this.sendSaveDataSF(oDataForm);
                }
            } else {
                this.showToastMessage('Error', 'Complete los campos requeridos antes de continuar', 'error');
            }
        } else {
            const oDataForm = this.getDataFieldsObject();
            if (oDataForm.isSuccess) {
                this.sendSaveDataSF(oDataForm);
            }
        }
    }

    getDataFieldsObject() {
        try {
            this.objDirPropiedad.DirPropiedad_CP = (this.item === undefined ? '' : this.item);
            this.objDirPropiedad.DirPropiedad_Estado = (this.itemEstado === undefined ? '' : this.itemEstado);
            this.objDirPropiedad.DirPropiedad_Ciudad = (this.itemCiudad === undefined ? '' : this.itemCiudad);
            this.objDirPropiedad.DirPropiedad_Alcaldia = (this.itemAlcaldia === undefined ? '' : this.itemAlcaldia);
            this.objDirPropiedad.DirPropiedad_ColoniaVal = (this.cmbDataColoniaVal === undefined ? '' : this.cmbDataColoniaVal);
            this.objDirPropiedad.DirPropiedad_ColoniaLbl = (this.cmbDataColoniaLbl === undefined ? '' : this.cmbDataColoniaLbl);
            this.objDirPropiedad.DirPropiedad_Calle = (this.strVal === undefined ? '' : this.strVal);
            this.objDirPropiedad.DirPropiedad_NumExt = (this.extVal === undefined ? '' : this.extVal);
            this.objDirPropiedad.DirPropiedad_NumInt = (this.intVal === undefined ? '' : this.intVal);
            this.objDirPropiedad.DirPropiedad_StateId = this.objCPDirProp.SFDatosGenerales[0].stateId;
            this.objDirPropiedad.DirPropiedad_CityId = this.objCPDirProp.SFDatosGenerales[0].cityId;
            this.objDirPropiedad.DirPropiedad_CountryId = this.objCPDirProp.SFDatosGenerales[0].countryId;
            this.objDirPropiedad.DirPropiedad_NeighbId = this.objCPDirProp.SFDatosGenerales[0].neighbId;
            this.objDirContratante.DirContratante_CdP = (this.checkboxVal === false ? this.checkboxVal2 : this.checkboxVal);
            if (this.checkboxVal) {
                this.fillDataCont();
            } else {
                this.difDataCont();
            }
            this.objDirPertenencia.DirProp = this.objDirPropiedad;
            this.objDirPertenencia.DirCont = this.objDirContratante;
            this.objDirPertenencia.EnvFisPoliz = (this.chkenvpolcltesi === false ? false : true);
            this.objDirPertenencia.EnvTypeVal = this.cmbSelDirSendVal;
            this.objDirPertenencia.EnvTypeLbl = this.cmbSelDirSendLbl;
            this.objDataForm.objDataFormDP = this.objDirPertenencia;
            this.objDataForm.objDataFormDC = this.oDataParamsDC;
            this.objDataForm.isSuccess = true;
            this.objDataForm.strOppoId = this.registroid;
            return this.objDataForm;
        } catch (e) {
            this.objDataForm.isSuccess = false;
            this.showToastMessage('Contrucción de Datos', 'Ocurrio un error al momento de llenar los datos: ' + e, 'error');
        }
    }

    difDataCont() {
        this.objDirContratante.sameDir = this.checkboxVal;
        this.objDirContratante.DirContratante_CP = (this.item === undefined ? '' : this.item);
        this.objDirContratante.DirContratante_Estado = (this.itemEstado === undefined ? '' : this.itemEstado);
        this.objDirContratante.DirContratante_Ciudad = (this.itemCiudad === undefined ? '' : this.itemCiudad);
        this.objDirContratante.DirContratante_Alcaldia = (this.itemAlcaldia === undefined ? '' : this.itemAlcaldia);
        this.objDirContratante.DirContratante_ColoniaVal = (this.cmbDataColoniaVal === undefined ? '' : this.cmbDataColoniaVal);
        this.objDirContratante.DirContratante_ColoniaLbl = (this.cmbDataColoniaLbl === undefined ? '' : this.cmbDataColoniaLbl);
        this.objDirContratante.DirContratante_Calle = (this.strVal === undefined ? '' : this.strVal);
        this.objDirContratante.DirContratante_NumExt = (this.extVal === undefined ? '' : this.extVal);
        this.objDirContratante.DirContratante_NumInt = (this.intVal === undefined ? '' : this.intVal);
        this.objDirContratante.DirContratante_StateId = this.objCPDirProp.SFDatosGenerales[0].stateId;
        this.objDirContratante.DirContratante_CityId = this.objCPDirProp.SFDatosGenerales[0].cityId;
        this.objDirContratante.DirContratante_CountryId = this.objCPDirProp.SFDatosGenerales[0].countryId;
        this.objDirContratante.DirContratante_NeighbId = this.objCPDirProp.SFDatosGenerales[0].neighbId;
    }

    fillDataCont() {
        this.objDirContratante.sameDir = this.checkboxVal;
        this.objDirContratante.DirContratante_CP = (this.cpVal1 === undefined ? '' : this.cpVal1);
        this.objDirContratante.DirContratante_Estado = (this.cpEstadoVal1 === undefined ? '' : this.cpEstadoVal1);
        this.objDirContratante.DirContratante_Ciudad = (this.cpCiudadVal1 === undefined ? '' : this.cpCiudadVal1);
        this.objDirContratante.DirContratante_Alcaldia = (this.cpAlcaldiaVal1 === undefined ? '' : this.cpAlcaldiaVal1);
        this.objDirContratante.DirContratante_ColoniaVal = (this.cmbDataColoniaValDC === undefined ? '' : this.cmbDataColoniaValDC);
        this.objDirContratante.DirContratante_ColoniaLbl = (this.cmbDataColoniaLblDC === undefined ? '' : this.cmbDataColoniaLblDC);
        this.objDirContratante.DirContratante_Calle = (this.strVal1 === undefined ? '' : this.strVal1);
        this.objDirContratante.DirContratante_NumExt = (this.extVal1 === undefined ? '' : this.extVal1);
        this.objDirContratante.DirContratante_NumInt = (this.intVal1 === undefined ? '' : this.intVal1);
        this.objDirContratante.DirContratante_StateId = (this.objCPDirCont === undefined ? '' : this.objCPDirCont.SFDatosGenerales[0].stateId);
        this.objDirContratante.DirContratante_CityId = (this.objCPDirCont === undefined ? '' : this.objCPDirCont.SFDatosGenerales[0].cityId);
        this.objDirContratante.DirContratante_CountryId = (this.objCPDirCont === undefined ? '' : this.objCPDirCont.SFDatosGenerales[0].countryId);
        this.objDirContratante.DirContratante_NeighbId = (this.objCPDirCont === undefined ? '' : this.objCPDirCont.SFDatosGenerales[0].neighbId);
    }

    sendSaveDataSF(oDataForm) {
        this.isLoading = true;
        mSetDataFormDCDP({ mDataObjDCDP: oDataForm }).then(result => {
            let iStatusCodeSrv = parseInt(result.oResponse.code);
            let sDescription = result.oResponse.description;
            if (iStatusCodeSrv === 200 || iStatusCodeSrv === 204) {
                this.showToastMessage('Web Service[createCustomerData]', 'Consumo de Servicio Exitoso: ' + iStatusCodeSrv + ' - ' + sDescription, 'success');
                mSetDataFormComSrv({ mDataObjDCDP: oDataForm }).then(result => {
                    let iStatusCodeSrv = parseInt(result.oResponse.code);
                    let sDescription = result.oResponse.description;
                    if (iStatusCodeSrv === 200 || iStatusCodeSrv === 204) {
                        this.showToastMessage('Web Service[modifyHomePolicyTemp]', 'Consumo de Servicio Exitoso: ' + iStatusCodeSrv + ' - ' + sDescription, 'success');
                        this.showToastMessage('Éxito', 'Proceso completado', 'success');
                        this.updQuoDir();
                        this.updStageFlujo();
                        this.isLoading = false;
                    } else {
                        this.showToastMessage('Web Service[modifyHomePolicyTemp]', 'Ocurrio un error al consumir el servicio: ' + iStatusCodeSrv + ' - ' + sDescription, 'error');
                        this.isLoading = false;
                    }
                }).catch(error => {
                    this.showToastMessage('Web Service[modifyHomePolicyTemp]', 'Ocurrio un error al consumir el servicio.', 'error');
                    this.isLoading = false;
                })
            } else {
                this.showToastMessage('Web Service[createCustomerData]', 'Ocurrio un error al consumir el servicio: ' + iStatusCodeSrv + ' - ' + sDescription, 'error');
                this.isLoading = false;
            }
        }).catch(error => {
            this.showToastMessage('Web Service[createCustomerData]', 'Ocurrio un error al consumir el servicio.', 'error');
            this.isLoading = false;
        });
    }

    showToastMessage(sTitle, sMessage, sVariant) {
        const oEvt = new ShowToastEvent({
            title: sTitle,
            message: sMessage,
            variant: sVariant
        });
        this.dispatchEvent(oEvt);
    }
}