import { LightningElement, track, api } from 'lwc';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import updateCuponCot from '@salesforce/apex/MX_SB_VTS_infoBasica_Ctrl.updateCuponCot';
import initDataCotiz from '@salesforce/apex/MX_SB_VTS_CotizInitData_Ctrl.initDataCotiz';
import updCurrentStage from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.updCurrentOppCtrl';
import getOppValues from '@salesforce/apex/MX_SB_VTS_CotizInitData_Ctrl.getOppVal';
import updateFrecuencie from '@salesforce/apex/MX_SB_VTS_CotizInitData_Ctrl.updateFrecuencie';
import updateCriterial from '@salesforce/apex/MX_SB_VTS_DeleteCoverageHSD_ctrl.editPetsHSD';

export default class Mx_sb_vts_cotizPayments extends LightningElement {

    @api cp;
    @api montos;
    @api comentarios;
    @api recordId;
    @api quoteId;
    @api ejemplo;
    @api wrapperParent = {};
    @api idquote = '';
    @api registroid;
    @api clientName = '';
    @api cotizProRen = '';
    @api valrentado;
    @api valpropio;
    @api hasfoot = false;
    @api hastitle = false;
    @api showModalEmail = false;
    @api isquarterly = false;
    @api hiddentitle = false;
    @track error;
    @track responseCupon;
    @track idItemSel = '';
    @track payments = [];
    @track processedArray = [];
    @track wrapperContext = {};
    @track paymentSelect = true;
    @track isLoading = false;
    @track totalAnual = 0;
    @track valCupon = '';
    @track frecSelected = 'YEARLY';

    connectedCallback(event) {
        this.obtOppVal();
        initDataCotiz({ oppId: this.recordId }).then(result => {
            let dataInfo = result;
            if (dataInfo) {
                this.initDataCotizHandler();
            }
        }).catch(error => {
        });
        this.wrapperContext = this.wrapperParent;
    }

    obtOppVal() {
        getOppValues({
            reId: this.recordId
        }).then(data => {
            const dataValues = data;
            for (let key in dataValues) {
                this.comentarios = dataValues[key].MX_WB_comentariosCierre__c;
            }
        }).catch(error => {
            throw new Error(error);
        })
    }

    saveStage() {
        this.updStageName();
    }

    onCheckPayment(event) {
       this.evaluateCheckedPays(event);
    }

    evaluateCheckedPays(event) {
        let idItemSelected = event.target.value.toString();
        this.idItemSel = idItemSelected;
        let checkboxes = this.template.querySelectorAll('[data-id="checkboxPayments"]')
        let intControl;
        for (intControl = 0; intControl < checkboxes.length; intControl++) {
            let idItemSelectedCheck = checkboxes[intControl].value.toString();
            if (idItemSelected !== idItemSelectedCheck) {
                checkboxes[intControl].checked = false;
            } else {
                checkboxes[intControl].checked = true;
            }
        }
        this.payments.forEach(element => {
            let itemId = element.Id.toString();
            if (idItemSelected !== itemId) {
                element.isChecked = false;
            } else {
                element.isChecked = true;
            }
        });
        this.payments.forEach(element => {
            if (element.isChecked) {
                this.paymentSelect = false;
            }
        });
        this.savePaymentFrec(event, 'frecuency');
    }

    savePaymentFrec(eventData, typeCoverCode) {
        this.isLoading = true;
        let lstCoversVals = [];
        let arrCovers = [eventData.currentTarget.dataset.ramo];
        let activeCover = false;
        let tempValue = this.valCupon;
        this.payments.forEach(element => {
            if (element.isChecked) {
                switch (element.Id) {
                    case 1:
                        this.frecSelected = 'YEARLY';
                        break;
                    case 2:
                        this.frecSelected = 'SEMESTER';
                        break;
                    case 3:
                        this.frecSelected = 'MONTHLY';
                        break;
                    case 4:
                        this.frecSelected = 'QUARTELY';
                        break;
                }
            }
        });
        if (typeCoverCode === 'CODIGO_CUPON') {
            lstCoversVals.push(eventData.currentTarget.dataset.covercode);
            arrCovers.push('001');
            if(this.valCupon !== '') {
                arrCovers.push(this.valCupon);
            } else {
                arrCovers.push('NA');
            }
            activeCover = true;
        } else {
            arrCovers.push('');
            arrCovers.push('');
        }
        arrCovers.push(eventData.currentTarget.dataset.categorie);
        arrCovers.push(eventData.currentTarget.dataset.goodtype);
        arrCovers.push(this.frecSelected);
        updateCriterial({ oppId: this.registroid, lstPartData: lstCoversVals, valuesCriterial: arrCovers, coverActive: activeCover }).then(result => {
            this.isLoading = false;
            this.processResultCrit(result, typeCoverCode, tempValue);
        }).catch(error => {
            this.isLoading = false;
            this.sendToastEvent(error, 'Error al actualizar Quote', 'error');
        });
    }

    processResultCrit(dataResult, typeAction, eventData) {
        if(typeAction === 'CODIGO_CUPON') {
            this.processCupon(eventData, dataResult);
        } else {
            this.updateFrecuencie(dataResult, typeAction, eventData);
        }
    }

    updateFrecuencie(dataResult, typeAction, eventData) {
        if(dataResult.isOk) {
            let frecStr = 'Anual';
            let mensualidad = 0;
            let totalAnual = 0;
            this.payments.forEach(element => {
                if (element.isChecked) {
                    switch (element.Id) {
                        case 1:
                            frecStr = 'Anual';
                            break;
                        case 2:
                            frecStr = 'Semestral';
                            break;
                        case 3:
                            frecStr = 'Mensual';
                            break;
                        case 4:
                            frecStr = 'Trimestral';
                            break;
                    }
                    totalAnual = element.totalPayment
                    mensualidad = element.mensualidad;
                }
            });
            let lstPayments = [];
            lstPayments.push(frecStr);
            lstPayments.push(mensualidad);
            lstPayments.push(totalAnual);
            updateFrecuencie({oppId: this.recordId, lstPayments: lstPayments}).then(result => {
                this.sendToastEvent('Valor actualizado correctamente', 'Frecuencia actualizada', 'success');
            }).catch(error => {

            });
        }
    }

    handledCloseDialog(event) {
        this.showModalEmail = event.detail;
    }

    handleClick(event) {
        this.showModalEmail = true;
    }

    handledBackAction() {
        if (this.comentarios === 'Propio') {
            let upWrapStage = Object.assign({}, this.wrapperContext, {
                etapa: "cotizada"
            });
            let upPropio = Object.assign({}, this.wrapperCheck, {
                checkpropio: "Propio"
            });
            this.wrapperContext = upWrapStage;
            this.wrapperCheck = upPropio;
        } else if (this.comentarios === 'Rentado') {
            let upWrapStageRenta = Object.assign({}, this.wrapperContext, {
                etapa: "cotizadaRenta"
            });
            let upRentado = Object.assign({}, this.wrapperCheck, {
                checkrentado: "Rentado"
            });
            this.wrapperContext = upWrapStageRenta;
            this.wrapperCheck = upRentado;
        }
        this.backAction();
    }

    handledNextAction(event) {
        let dataInfoQuote = new Object();
        let finalCheckVal = this.idItemSel;
        if (finalCheckVal === 1) {
            dataInfoQuote.valCheck = 'Anual';
        } else if (finalCheckVal === 2) {
            dataInfoQuote.valCheck = 'Semestral';
        } else {
            dataInfoQuote.valCheck = 'Mensual';
        }
        if (this.paymentSelect === false) {
            let upWrapStage = Object.assign({}, this.wrapperContext, {
                etapa: "tarificada"
            });
            this.wrapperContext = upWrapStage;
            this.nextAction();
        } else {
            this.sendToastEvent('Error', 'Seleccione una forma de pago', 'error');
        }
    }

    backAction() {
        const jsonDetail = { cotizObject: this.wrapperContext, zipCode: this.cp, cotizProRen: this.wrapperCheck, amount: this.montos };
        const backAction = new CustomEvent("backaction", {
            detail: jsonDetail,
        });
        this.dispatchEvent(backAction);
    }

    nextAction() {
        const jsonDetail = { cotizObject: this.wrapperContext, zipCode: this.cp, amount: this.montos };
        const nextAction = new CustomEvent("nextaction", {
            detail: jsonDetail,
        });
        this.dispatchEvent(nextAction);
    }

    formatCurrency = (amount) => {
        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
        });
        return formatter.format(amount);
    }

    updateVal(event) {
        this.valCupon = event.target.value;
        if(this.valCupon === '') {
            this.isLoading = true;
            this.savePaymentFrec(event, 'CODIGO_CUPON');
        }
    }

    handleBlur(event) {
        let inputtarget = this.valCupon;
        if (inputtarget !== undefined && inputtarget !== '') {
            this.isLoading = true;
            this.savePaymentFrec(event, 'CODIGO_CUPON');
        }
        if(inputtarget === '') {
            this.isLoading = true;
            this.savePaymentFrec(event, 'CODIGO_CUPON');
        }
    }

    processCupon(eventData, dataResult) {
        let dataPayments = dataResult.dataPayments;
        let anualTemp = Number.parseFloat(dataPayments.YEARLY);
        if(this.totalAnual !== anualTemp) {
            let inputtarget = eventData;
            updateCuponCot({ oppId: this.recordId, cuponCode: inputtarget }).then(result => {
                this.isLoading = false;
                this.responseCupon = result;
            }).catch(error => {
                this.isLoading = false;
                this.sendToastEvent(error, 'Error al procesar cupón', 'error');
            });
            if(anualTemp >= this.totalAnual && eventData === '') {
                this.sendToastEvent('Se elimino el cupon correctamente', 'Cupón eliminado', 'success');
            }
            if(this.totalAnual >= anualTemp) {
                this.sendToastEvent('Cupon validado correctamente', 'Cupón valido', 'success');
            }
        } else {
            if(eventData === undefined || eventData === '') {
                this.sendToastEvent('Se elimino el cupon correctamente', 'Cupón eliminado', 'success');
            } else {
                this.valCupon = '';
                this.sendToastEvent('El cupón no es valido', 'Cupón no valido', 'Warning');
            }
        }
        this.initDataCotizHandler();
    }

    recotizHSD() {
        this.hiddentitle = true;
    }

    recotizVSD() {
        this.hiddentitle = true;
        this.initDataCotizHandler();
    }

    initDataCotizHandler() {
        initDataCotiz({ oppId: this.recordId }).then(result => {
            if (result.hasError === false) {
                if (result.oppRecord.SyncedQuoteId !== undefined) {
                    var mapQuote = new Map(Object.entries(result.lstQuotes));
                    const iteratorQuote = mapQuote.values();
                    let quoteData = iteratorQuote.next().value;
                    var mapQuoli = new Map(Object.entries(result.lstQuoteLi));
                    const iteratorQuoli = mapQuoli.values();
                    let quoliData = iteratorQuoli.next().value;
                    this.updatePayments(quoteData, quoliData);
                }
            }
        }).catch(error => {
        });
    }

    updatePayments(quoteData, quoliData) {
        if (quoteData.Id !== undefined) {
            this.valCupon = quoteData.MX_SB_VTS_Cupon__c !== '' ? quoteData.MX_SB_VTS_Cupon__c : '';
            this.payments = [];
            let temPayments = quoteData.MX_SB_VTS_GCLID__c.split('|');
            this.totalAnual = quoteData.MX_SB_VTS_TotalAnual__c;
            let element3 = this.fillElement(3, 12, quoteData.MX_SB_VTS_TotalMonth__c, 'Pago mensual', quoliData.MX_SB_VTS_Frecuencia_de_Pago__c, temPayments[3]);
            this.payments.push(element3);
            if (this.isquarterly) {
                let quarterlyPay = this.fillElement(4, 4, quoteData.MX_SB_VTS_TotalQuarterly__c, 'Pago trimestral', quoliData.MX_SB_VTS_Frecuencia_de_Pago__c, temPayments[2]);
                this.payments.push(quarterlyPay);
            }
            let element2 = this.fillElement(2, 2, quoteData.MX_SB_VTS_TotalSemiAnual__c, 'Pago semestral', quoliData.MX_SB_VTS_Frecuencia_de_Pago__c, temPayments[1]);
            this.payments.push(element2);
            let element1 = this.fillElement(1, 1, quoteData.MX_SB_VTS_TotalAnual__c, 'Pago anual', quoliData.MX_SB_VTS_Frecuencia_de_Pago__c, temPayments[0]);
            this.payments.push(element1);
        }
    }

    fillElement(idPay, totalPays, totalAmount, labelTotal, frecuencia, mensualidad) {
        let elementPay = new Object();
        let pagoStr = 'pago';
        elementPay.Id = idPay;
        elementPay.totalPagos = totalPays;
        let totalPayment = 600;
        if (totalAmount !== undefined) {
            totalPayment = totalAmount;
        }
        elementPay.totalPayment = totalPayment;
        elementPay.totalToPay = this.formatCurrency(totalPayment);
        elementPay.checked = this.validateChecked(labelTotal, frecuencia);
        if (elementPay.checked) {
            this.paymentSelect = false;
        }
        if (elementPay.totalPagos > 1) {
            pagoStr += 's';
        }
        elementPay.label = labelTotal + ' ' + elementPay.totalPagos + ' ' + pagoStr + ' de: ' + this.formatCurrency(mensualidad);
        elementPay.mensualidad = mensualidad;
        return elementPay;
    }

    validateChecked(labelTotal, frecuencia) {
        let isChecked = false;
        switch (labelTotal) {
            case 'Pago anual':
                isChecked = this.isEqualsPayments('Anual', frecuencia);
                this.validateFrec(isChecked, 'YEARLY');
                break;

            case 'Pago semestral':
                isChecked = this.isEqualsPayments('Semestral', frecuencia);
                this.validateFrec(isChecked, 'SEMESTER');
                break;

            case 'Pago trimestral':
                isChecked = this.isEqualsPayments('Trimestral', frecuencia);
                this.validateFrec(isChecked, 'QUARTELY');
                break;

            case 'Pago mensual':
                isChecked = this.isEqualsPayments('Mensual', frecuencia);
                this.validateFrec(isChecked, 'MONTHLY');
                break;
        }
        return isChecked;
    }

    validateFrec(ischecked, strPayment) {
        if(ischecked) {
            this.frecSelected = strPayment;
        }
    }

    isEqualsPayments(labelPay, frecuency) {
        let isEqualsVars = false;
        if (labelPay === frecuency) {
            isEqualsVars = true;
        }
        return isEqualsVars;
    }

    updStageName() {
        let stageNameRen = 'emitida';
        updCurrentStage({
            srtOppId: this.registroid, etapaForm: stageNameRen
        }).then(result => {
            this.sendToastEvent(result, 'Se ha guardado correctamente la etapa de Emitida', 'success');
        }).catch(error => {
            this.sendToastEvent(error, 'Ocurrió un error al guardar la etapa a Emitida', 'error');
        });
    }

    sendToastEvent(msjError, msjTitle, typeToast) {
        this.isLoading = false;
        this.dispatchEvent(
            new ShowToastEvent({
                title: msjTitle,
                message: msjError,
                variant: typeToast
            })
        );
    }
}