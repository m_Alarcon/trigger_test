import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import initCot from '@salesforce/apex/MX_SB_VTS_CotizInitData_Ctrl.initDataCotiz';
import getCurrentVal from '@salesforce/apex/MX_SB_VTS_CotizInitData_Ctrl.updCurrRec';
import getOppValues from '@salesforce/apex/MX_SB_VTS_CotizInitData_Ctrl.getOppVal';
import getStageName from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.gtOppValsCrtl';
export default class Mx_sb_vts_cotizadorHogar extends LightningElement {

    @api cp;
    @api flujo;
    @api recordId;
    @api cotizObject;
    @api objdatafrms;
    @api quoteId;
    @api amount;
    @api recordValue;
    @api comentariosCierre;
    @api valpropio;
    @api valrentado;
    @api montos;
    @api dcparam;
    @api payments = [];
    @api picklistValues = [];
    @api cotizProRen = '';
    @api propio = '';
    @api rentado = '';
    @api comentarios = '';
    @api idquote = '';
    @track pleadsource = '';
    @track stageName = '';
    @track nwStage = '';
    @track error;
    @track gtData;
    @track leadSource;
    @track newCot = false;
    @track cotizada = false;
    @track cotizadaRenta = false;
    @track cotizadaCmb = false;
    @track cotizadaCmbRent = false;
    @track formalizada = false;
    @track tarificada = false;
    @track emitida = false;
    @track isLoaded = false;
    @track cmb = false;
    @track cmbRen = false;
    @track outbound = false;
    @track outboundRen = false;

    connectedCallback() {
        this.getOppVals();
        this.initCot();
        this.getStage();
        this.hideStages();
    }

    getStage() {
        getStageName({
            idOpps: this.recordId
        }).then(data => {
            const dataValuesStage = data;
            this.gtData = dataValuesStage;
            for (let key in dataValuesStage) {
                let idChild = dataValuesStage[key].SyncedQuoteId;
                this.idquote = (idChild === undefined ? '' : idChild);
                this.stageName = dataValuesStage[key].Descripcion_y_comentarios__c;
            }
            if (this.stageName === 'newCot' || this.stageName === '' || this.stageName === undefined) {
                this.newCot = true;
            } else if (this.stageName === 'cmbRen') {
                this.newCot = false;
                this.cotizadaRenta = true;
                this.cmbRen = true;
            } else if (this.stageName === 'cmb') {
                this.newCot = false;
                this.cotizada = true;
                this.cmb = true;
            } else if (this.stageName === 'outboundRen') {
                this.newCot = false;
                this.cotizadaRenta = true;
                this.outboundRen = true;
            } else if (this.stageName === 'outbound') {
                this.newCot = false;
                this.cotizada = true;
                this.outbound = true;
            } else if (this.stageName === 'emitida') {
                this.newCot = false;
                this.emitida = true;
            } else if (this.stageName === 'tarificada') {
                this.newCot = false;
                this.tarificada = true;
            } else if (this.stageName === 'formalizada') {
                this.newCot = false;
                this.formalizada = true;
            }
        }).catch(error => {
            throw new Error(error);
        })

    }

    getOppVals() {
        getOppValues({
            reId: this.recordId
        }).then(data => {
            const dataValues = data;
            for (let key in dataValues) {
                this.leadSource = dataValues[key].LeadSource;
                this.pleadsource = dataValues[key].LeadSource;
            }
        }).catch(error => {
            throw new Error(error);
        })
    }

    initCot() {
        if (this.recordId) {
            initCot({
                oppId: this.recordId
            }).then(result => {
                const cotizObjecTemp = new Object();
                cotizObjecTemp.totalAmount = 11500;
                cotizObjecTemp.postalCode = '';
                cotizObjecTemp.lstQuotes = [];
                cotizObjecTemp.etapa = 'Creada';
                cotizObjecTemp.opportunity = {};
                cotizObjecTemp.wrapperClient = {};
                this.cotizObject = cotizObjecTemp;
            });
        } else {
            const cotizObjecTemp = new Object();
            cotizObjecTemp.totalAmount = 11500;
            cotizObjecTemp.postalCode = '';
            cotizObjecTemp.lstQuotes = [];
            cotizObjecTemp.etapa = 'Creada';
            cotizObjecTemp.opportunity = {};
            cotizObjecTemp.wrapperClient = {};
            this.cotizObject = cotizObjecTemp;
        }
    }

    handledEventBack(event) {
        this.moveStages(event);
    }

    handledEventNext(event) {
        this.moveStages(event);
    }

    moveStages(event) {
        this.isLoaded = false;
        this.cotizObject = event.detail.cotizObject;
        this.cotizProRen = event.detail.cotizProRen;
        this.cp = event.detail.zipCode;
        this.amount = event.detail.amount;
        let stage = this.cotizObject.etapa;
        this.dcparam = event.detail.dcForm;
        this.flujo = event.detail.flujo;
        this.objdatafrms = event.detail.objdatfrms;
        this.updCurrentVal(stage);
        this.hideStages();
        this.moveToStage(stage);
    }

    hideStages() {
        this.newCot = false;
        this.cotizada = false;
        this.cotizadaRenta = false;
        this.cotizadaCmb = false;
        this.cotizadaCmbRent = false;
        this.formalizada = false;
        this.tarificada = false;
        this.emitida = false;
    }

    updCurrentVal(etapa) {
        if (etapa === 'cotizadaRenta' || etapa === 'cotizada') {
            let propio = (this.cotizProRen.checkpropio === undefined ? '' : this.cotizProRen.checkpropio);
            let rentado = (this.cotizProRen.checkrentado === undefined ? '' : this.cotizProRen.checkrentado);
            if (propio.length > 0) {
                this.valpropio = propio;
                this.comentarios = 'Propio';
            } else if (rentado.length > 0) {
                this.valrentado = rentado;
                this.comentarios = 'Rentado';
            }
            getCurrentVal({
                recId: this.recordId, strValPropio: this.valpropio, strValRentado: this.valrentado
            }).then(result => {
                this.showToastMsgCotiz(result, 'Se actualizó el valor Propio/Rentado', 'success');
            }).catch(error => {
                this.showToastMsgCotiz(error, 'Error al actualizar el valor Propio/Rentado', 'error');
            });
        }
    }

    moveToStage(etapa) {
        if (etapa === 'newCot') {
            this.evalnCot();
        } else if (etapa === 'cotizada' && this.leadSource === 'Outbound') {
            this.outbound = true;
            this.cotizada = true;
        } else if (etapa === 'cotizadaRenta' && this.leadSource === 'Outbound') {
            this.outboundRen = true;
            this.cotizadaRenta = true;
        } else if (etapa === 'cotizada' && (this.leadSource === 'Call me back' || this.leadSource === 'Tracking Web' || this.leadSource === 'Facebook')) {
            this.cmb = true;
            this.cotizada = true;
        } else if (etapa === 'cotizadaRenta' && (this.leadSource === 'Call me back' || this.leadSource === 'Tracking Web' || this.leadSource === 'Facebook')) {
            this.cmbRen = true;
            this.cotizadaRenta = true;
        } else if (etapa === 'emitida') {
            this.emitida = true;
        } else if (etapa === 'tarificada') {
            this.tarificada = true;
        } else if (etapa === 'formalizada') {
            this.formalizada = true;
        } else {
            this.formalizada = true;
        }
        this.isLoaded = true;
    }

    evalnCot() {
        if (this.stageName === 'cmbRen' || this.stageName === 'newCot' || this.stageName === undefined) {
            this.newCot = true;
            this.cotizadaRenta = false;
        } else if (this.stageName === 'cmb' || this.stageName === 'newCot' || this.stageName === undefined) {
            this.newCot = true;
            this.cotizada = false;
        } else if (this.stageName === 'outbound' || this.stageName === 'newCot' || this.stageName === undefined) {
            this.cotizada = false;
            this.newCot = true;
        } else if (this.stageName === 'outboundRen' || this.stageName === 'newCot' || this.stageName === undefined) {
            this.cotizadaRenta = false;
            this.newCot = true;
        }
        else if (this.stageName === 'emitida' || this.stageName === 'tarificada' || this.stageName === 'formalizada') {
            this.newCot = true;
        } else {
            this.newCot = false;
        }
    }

    showToastMsgCotiz(qTitle, qMessage, qVariant) {
        const qEvt = new ShowToastEvent({
            title: qTitle,
            message: qMessage,
            variant: qVariant
        });
        this.dispatchEvent(qEvt);
    }
}