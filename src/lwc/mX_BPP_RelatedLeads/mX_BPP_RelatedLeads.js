/*eslint no-console: ["error", { allow: ["warn", "error"] }] */
import {
    LightningElement,
    wire,
    api
} from 'lwc';
import {
    NavigationMixin
} from 'lightning/navigation';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import idUser from '@salesforce/user/Id';
import menubt from '@salesforce/apex/MX_BPP_RelatedLeads_Crtl.buttonMenu';
import findLeads from '@salesforce/apex/MX_BPP_RelatedLeads_Crtl.relatedLeads';
import leadConv from '@salesforce/apex/MX_BPP_RelatedLeads_Crtl.convertLeads';
import {
    refreshApex
} from '@salesforce/apex';

export default class MX_BPP_RelatedLeads extends NavigationMixin(LightningElement) {
    @api recordId;
    @wire(findLeads, {
        idAccount: '$recordId'
    })
    leads;

    @wire(menubt, {
        idUser: idUser
    }) buttonMenu;

    get hasMenuBt() {
        return this.buttonMenu;
    }

    get leadSize() {
        return (this.leads.data.length > 0);
    }

    get tituloLeads() {
        return "Campañas (" + this.leads.data.length + ")";
    }


    handleSelect(event) {
        switch (event.detail.value) {
            case "Edit":
                this.editPage(event.currentTarget.dataset.id);
                break;
            case "Convert":
                this.convertLead(event.currentTarget.dataset.id);
                break;
        }

    }

    editPage(leadid) {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: leadid,
                objectApiName: 'Lead',
                actionName: 'edit'
            }
        });
    }

    convertLead(leadid) {
        leadConv({
                idLead: leadid
            })
            .then(result => {
                this.openOpp(result[leadid]);
            })
            .catch(error => {
                const toast = new ShowToastEvent({
                    title: 'Error',
                    message: error,
                    variant: 'error',
                    mode: 'dismissable'
                });
                this.dispatchEvent(toast);
            });
    }

    openOpp(oppId) {
        refreshApex(this.leads);
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: oppId,
                actionName: 'view'
            }
        });
    }

}