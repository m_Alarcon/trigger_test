import { LightningElement, wire, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getcampaigndata from '@salesforce/apex/MX_SB_VTS_Campanas_lwc_cls.getcampaigndata';

export default class MxsbvtsCampanas extends LightningElement {
    @api recordId;
    @track  getcampaigndata = {};
    @wire(getcampaigndata,{recordId:'$recordId'})
    wireData({error, data}) {
        if(data) {
            data.forEach(element => {
               this.getcampaigndata.Id = element.Id;
               this.getcampaigndata.campana = element.MX_SB_VTS_CampaFaceName__c;
               this.getcampaigndata.detalle = element.MX_SB_VTS_DetailCampaFace__c;
               this.getcampaigndata.codigo = element.MX_SB_VTS_CodCampaFace__c;
            });
        } else if(error) {
            // es-lint allow-console-log
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error',
                    message: error.message,
                    variant: 'error'
                })
            );
        }
    }
}