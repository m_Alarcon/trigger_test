import { LightningElement, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getDataInputCP from '@salesforce/apex/MX_SB_VTS_GetSetDPForm_Ctrl.dataInputCP';
export default class Mx_sb_vts_vsd_mod_dir_contratante extends LightningElement { //NOSONAR
    @track fldcalle = '';
    @track fldnoext = '';
    @track fldnoint = '';
    @track fldcp = '';
    @track fldcoloniaopts;
    @track fldalcmun = '';
    @track fldestado = '';
    @track fldcolval = '';
    @track fldcollbl = '';
    @track tmpalcmun = '';
    @track tmpestado = '';
    @track mObjDataDC;

    onPostalCode(event) {
        let strFldCP = '';
        if(event.target.name === 'str-cp-dc') {
            strFldCP = event.target.value;
            if(strFldCP.length === 5) {
                getDataInputCP({sCodigoPostal: strFldCP}).then(result => {
                    this.fldalcmun = result.SFDatosGenerales[0].country;
                    this.fldestado = result.SFDatosGenerales[0].state;
                    this.tmpalcmun = result.SFDatosGenerales[0].country;
                    this.tmpestado = result.SFDatosGenerales[0].state;
                    this.fldcoloniaopts = result.SFDatosColonias;
                    this.fldcp = result.SFDatosGenerales[0].zipcode;
                }).catch(error => {
                    this.showToastMessage('Error', 'Ocurrio un error al momento de recuperar los datos del CP', 'error');
                });
            } else {
                this.fldalcmun = '';
                this.fldestado = '';
                this.fldcoloniaopts = [];
            }
        }
    }

    onPostalCodeBlur(event) {
        let strFldCP = '';
        if(event.target.name === 'str-cp-dc') {
            strFldCP = event.target.value;
            if(strFldCP.length < 5) {
                this.showToastMessage('Validación de Campos', 'Por favor, completa el código postal', 'error');
            }
        }
    }

    onChangeData(event) {
        let objDataDC = new Object();
        if(event.target.name === 'str-calle-dc') {
            this.fldcalle = event.target.value;
        } else if (event.target.name === 'str-next-dc') {
            this.fldnoext = event.target.value;
        } else if (event.target.name === 'str-nint-dc') {
            this.fldnoint = event.target.value;
        } else if (event.target.name === 'str-cp-dc') {
            this.fldcp = event.target.value;
        } else if (event.target.name === 'str_colonia_dc') {
            this.fldcolval = event.target.value;
            this.fldcollbl = event.target.options.find(opt => opt.value === event.detail.value).label;
        }
        objDataDC.dc_calle = this.fldcalle;
        objDataDC.dc_next = this.fldnoext;
        objDataDC.dc_nint = this.fldnoint;
        objDataDC.dc_cp = this.fldcp;
        objDataDC.dc_colval = this.fldcolval;
        objDataDC.dc_collbl = this.fldcollbl;
        objDataDC.dc_alcnum = this.tmpalcmun;
        objDataDC.dc_estado = this.tmpestado;
        this.mObjDataDC = JSON.stringify(objDataDC);

        const objEvent = new CustomEvent('paramdc', { detail: { oparamdc:this.mObjDataDC }});
        this.dispatchEvent(objEvent);
    }

    showToastMessage(sTitle, sMessage, sVariant) {
        const oEvt = new ShowToastEvent({
            title: sTitle,
            message: sMessage,
            variant: sVariant,
            mode: 'sticky'
        });
        this.dispatchEvent(oEvt);
    }
}