import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecordNotifyChange } from 'lightning/uiRecordApi';
import srchZipSearch from '@salesforce/apex/MX_SB_VTS_Colonias_Ctrl.getZipCode';
import getListPlaces from '@salesforce/apex/MX_SB_VTS_Colonias_Ctrl.getListPlaces';
import getNeighborhood from '@salesforce/apex/MX_SB_VTS_Colonias_Ctrl.getNeighborhood';
import mGetSetAlianzasRP from '@salesforce/apex/MX_SB_VTS_GetSetAlianzas_RP_Ctrl.obtListInsInfoCatCtrl';
import mGetSetAlianzasPP from '@salesforce/apex/MX_SB_VTS_GetSetAlianzas_PP_Ctrl.obtListCarInsCatCtrl';
import getDatosPart from '@salesforce/apex/MX_SB_VTS_GetPartData_DP_Ctrl.dptListCarInsCtrl';
import getCoverages from '@salesforce/apex/MX_SB_VTS_GetPartData_DP_Ctrl.obtListCarInsCtrl';
import getInsuredAmount from '@salesforce/apex/MX_SB_VTS_GetSumInsured_Ctrl.getAmounts';
import crtAddressVal from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.createAddressCtrl';
import getAddValues from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.getValAddressCtrl';
import updCurrentStage from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.updCurrentOppCtrl';
import getOppValues from '@salesforce/apex/MX_SB_VTS_CotizInitData_Ctrl.getOppVal';
import updateCuponCot from '@salesforce/apex/MX_SB_VTS_infoBasica_Ctrl.updateCuponCot';
import resetQuotes from '@salesforce/apex/MX_SB_VTS_infoBasica_Ctrl.resetQuotes';
import crtQuoteVal from '@salesforce/apex/MX_SB_VTS_InitQuo_Ctrl.ctrDataQuoteCtrl';
import saveInsuredAmo from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.updateSumas';

export default class Mx_sb_vts_infoBasica extends LightningElement {

    @api recordId;
    @api OpportunityId;
    @api insuredAmount;
    @api montosAsegurados = [];
    @api wrapperParent = {};
    @api leadsource = '';
    @api registroid = '';
    @api propiedad = '';
    @api cp = '';
    @api openModal = false;
    @track value = 'Nuevo';
    @track button = true;
    @track hidden = false;
    @track check1 = false;
    @track check2 = false;
    @track check3 = false;
    @track check4 = false;
    @track check5 = false;
    @track check6 = false;
    @track selection;
    @track estado;
    @track ciudad;
    @track alcaldia;
    @track hidro;
    @track sismica;
    @track hidrocheck;
    @track sismicacheck;
    @track habitaSi;
    @track habitaNo;
    @track usoHabitaSi;
    @track usoHabitaNo;
    @track casa;
    @track depa;
    @track objAlianzasRP;
    @track objAlizanasPP;
    @track objBienesAseg;
    @track typeAddress;
    @track stateOpts = [];
    @track cityOpts = [];
    @track muniOpts = [];
    @track stateValue = '';
    @track cityValue = '';
    @track muniValue = '';
    @track latitude = '';
    @track longitude = '';
    @track construccion = '';
    @track stageCheckName = '';
    @track idAddPro = '';
    @track m2 = '';
    @track piso = '';
    @track pisodepa = '';
    @track pisovive = '';
    @api flujo ;
    @track isLoading = false;
    @track isProRen = '';
    @track failProxy = false;
    @track calNewNeigh = false;
    @track sumaSugerida = '';
    dataInfo = new Object();
    textError = 'Ocurrio un error al consumir el servicio: ';

    connectedCallback() {
        this.getOppVal();
    }

    getOppVal() {
        this.isLoading = true;
        getOppValues({
            reId: this.registroid
        }).then(data => {
            let dataValues = data;
            this.leadSource = dataValues[0].LeadSource;
            this.sumaSugerida = dataValues[0].Monto_de_la_oportunidad__c;
            if(this.sumaSugerida === undefined || this.sumaSugerida === '') {
                this.calNewNeigh = true;
            }
            if (this.leadsource === 'Tracking Web') {
                this.bienesAsegurados();
            } else {
                this.obtAlianzasSrv();
            }
        }).catch(error => {
            this.isLoading = false;
            throw new Error(error);
        })
    }

    getAddressVal() {
        this.typeAddress = 'Domicilio asegurado';
        this.isLoading = true;
        getAddValues({iddOpps: this.registroid, dirType: this.typeAddress}).then(data => {
            if (data) {
                this.iterMethod(data);
                this.buttonOnOf();
            }
            this.isLoading = false;
        }).catch(error => {
            this.isLoading = false;
            this.showToastMessage('Información', 'No existe el registro o no existen datos de la dirección', 'warning');
        })
    }

    iterMethod(dataValues) {
        for (let key in dataValues) {
            const idAddress = dataValues[key].Id;
            this.idAddPro = (idAddress === undefined ? '' : idAddress);
            const valCheckCD = dataValues[key].MX_RTL_AddressReference__c;
            const valCheckPR = dataValues[key].MX_RTL_Tipo_Propiedad__c;
            const valCheckUso = dataValues[key].MX_RTL_Uso_Habitacional__c;
            const codigoCp = dataValues[key].MX_RTL_PostalCode__c;
            this.cp = (codigoCp === undefined ? '' : codigoCp);
            this.stateOpts = [...this.stateOpts, {
                label: dataValues[key].MX_RTL_AddressState__c,
                value: dataValues[key].MX_RTL_AddressState__c
            }];
            const stateOps = this.stateOpts[0].value;
            this.stateValue = (stateOps === undefined ? '' : stateOps);
            this.muniOpts = [...this.muniOpts, {
                label: dataValues[key].MX_RTL_AddressMunicipality__c,
                value: dataValues[key].MX_RTL_AddressMunicipality__c
            }];
            const munici = this.muniOptions[0].value;
            this.muniValue = (munici === undefined ? '' : munici);
            this.cityOpts = [...this.cityOpts, {
                label: dataValues[key].MX_RTL_AddressCity__c,
                value: dataValues[key].MX_RTL_AddressCity__c
            }];
            const cityOps = this.cityOpts[0].value;
            this.cityValue = (cityOps === undefined ? '' : cityOps);
            this.valCheckC(valCheckCD);
            let metros2 = dataValues[key].MX_SB_VTS_Metros_Cuadrados__c;
            this.valCheckP(metros2, valCheckPR);
            this.valCheckUse(valCheckUso);
        }
    }

    valCheckC(valCheckCD) {
        if (valCheckCD === 'Casa') {
            this.construccion = 'house';
            this.check5 = true;
        } else if (valCheckCD === 'Departamento') {
            this.construccion = 'flat';
            this.check6 = true;
        } else {
            this.check6 = false;
            this.check5 = false;
            this.button = false;
        }
    }

    valCheckP(metros2, valCheckPR) {
        if (valCheckPR === 'Propio') {
            this.check1 = true;
            this.propiedad = 'Propio';
            this.m2 = metros2;
            this.button = false;
        } else if (valCheckPR === 'Rentado') {
            this.check2 = true;
            this.propiedad = 'Rentado';
            this.button = false;
        } else {
            this.check1 = false;
            this.check2 = false;
            this.button = false;
        }
    }

    valCheckUse(valCheckUso) {
        if (valCheckUso === 'Si') {
            this.check3 = true;
        } else if (valCheckUso === 'No') {
            this.check4 = true;
        } else {
            this.check3 = false;
            this.check4 = false;
            this.button = false;
        }
    }

    getZipCode() {
        this.isLoading = true;
        srchZipSearch({zipCode: this.cp}).then(data => {
            const test = data;
            if (test.oResponse[0].code === '200') {
                this.getStates(test);
                this.getMunicipio(test);
                this.getCity(test);
                this.clearQuotes();
            } else if (test.oResponse[0].code !== '200' || this.getStates === '' || this.getMunicipio === '' || this.getCity === '') {
                this.showToastMessage('Web Service[getListInsuraceCustomerCatalog]', this.textError + test.oResponse[0].code + ' - ' + test.oResponse[0].description, 'error');
            }
            this.isLoading = false;
        }).catch(error => {
            this.isLoading = false;
            this.showToastMessage('Web Service ', this.textError + error, 'error');
        })
    }

    clearQuotes() {
        resetQuotes({strOppoId: this.registroid}).then(data => {
        }).catch(error=> {
            this.showToastMessage('Web Service[ListPlaces]', 'Error al actualizar Código postal', 'warning');
        });
    }

    listPlaces() {
        this.isLoading = true;
        getListPlaces({zipCode: this.cp}).then(data => {
            let statusCode = Boolean(data['statusCode']);
            if (statusCode) {
                let getLat = data['latitude'];
                if (getLat === '' || getLat === null || typeof getLat === 'undefined') {
                    this.showToastMessage('Web Service[ListPlaces]', 'Unable to tunnel through proxy. Proxy returns "HTTP/1.1 503 Service Unavailable', 'warning');
                    this.flujo = 'b';
                    this.openModal = true;
                } else {
                    let latitude = String(getLat[0]);
                    let getLong = data['longitude'];
                    let longitude = String(getLong[0]);
                    this.latitude = latitude;
                    this.longitude = longitude;
                    this.initFlujoA();
                }
            } else {
                this.showToastMessage('Web Service[ListPlaces]', 'Ocurrio un error al consumir el servicio.', 'error');
                this.isLoading = false;
                this.flujo = 'b';
                this.openModal = true;
            }

        }).catch(error => {
            this.showToastMessage('Error al invocar ListPlaces', 'Ocurrio un error al consumir el servicio.', 'error');
        })
        this.crtAddress();
        this.crtQuotes();
    }

    initFlujoA() {
        if (this.latitude === '0' && this.longitude === '0') {
            this.flujo = 'b';
            this.openModal = true;
        } else {
            this.flujo = 'a';
            let neighborhood = { latitude: this.latitude, longitude: this.longitude, isNewDev: true, propertyType:  'flat'}
            this.neighborhood(neighborhood, 'deparentado');
        }
    }

    evaluateInitNeig(loppIter) {
        let neighborhood = {};
        switch (loppIter) {
            case 'deparentado':
                neighborhood = {latitude: this.latitude, longitude: this.longitude, isNewDev: false, propertyType: 'flat'};
                this.neighborhood(neighborhood, 'casapropio');
                break;
            case 'casapropio':
                neighborhood = {latitude: this.latitude, longitude: this.longitude, isNewDev: true, propertyType: 'house'};
                this.neighborhood(neighborhood, 'casarentado');
                break;
            case 'casarentado':
                neighborhood = {latitude: this.latitude, longitude: this.longitude, isNewDev: false, propertyType: 'house'};
                this.neighborhood(neighborhood, '');
                break;

            default:
                this.flujo = 'b';
                this.openModal = true;
                break;
        }
    }

    neighborhood(neighborhood, tipoFlujo) {
        getNeighborhood({dataNeighborhood: neighborhood}).then(data => {
            if (String(data) == 'proxy') {
                this.failProxy = true;
                this.showToastMessage('Web Service[Neighborhoodd]', 'Unable to tunnel through proxy. Proxy returns "HTTP/1.1 503 Service Unavailable', 'warning');
                this.flujo = 'b';
                this.openModal = true;
            } else if (String(data) == 'error') {
                this.openModal = false;
                this.failProxy = true;
                this.showToastMessage('Web Service[Neighborhoodd]', 'Las coordenadas proporcionadas no coinciden en la base de datos.', 'error');
                this.flujo = 'b';
                this.openModal = true;
            } else {
                let priceM2 = data[0];
                this.evaluateFlujoA(priceM2, tipoFlujo);
            }
        }).catch(error => {
            this.showToastMessage(error, 'Error al recuperar monto sugerido', 'error');
        });
    }

    evaluateFlujoA(dataNeigh, typeFlow) {
        let suggesAmmount = dataNeigh;
        if(suggesAmmount === undefined || suggesAmmount === '' ) {
            this.evaluateInitNeig(typeFlow);
        } else {
            if (this.propiedad === 'Propio') {
                let sumaPropio = suggesAmmount * parseInt(this.m2);
                this.insuredAmount = sumaPropio;
            } else if (this.propiedad === 'Rentado') {
                let sumaRentado = suggesAmmount * 60;
                sumaRentado = sumaRentado / 2;
                this.insuredAmount = sumaRentado;
            }
            let sumaAsegurada = this.insuredAmount;
            this.roundTotalAmounts(sumaAsegurada);
        }
    }

    roundTotalAmounts(sumaAsegurada) {
        this.isLoading = true;
        getInsuredAmount({insAmount: sumaAsegurada, propiedad: this.propiedad}).then(result => {
            this.montosAsegurados = result;
            this.suggestAmount = result[0];
            this.updateSuma();
            this.isLoading = false;
        }).catch(error => {
            this.isLoading = false;
            this.showToastMessage(error, 'Error al Obtener suma sugerida, intente nuevamente', 'error');
        });
    }

    updateSuma() {
        this.isLoading = true;
        let insuAmo = this.suggestAmount;
        saveInsuredAmo({oppId: this.registroid, insuredAmo: insuAmo}).then(result => {
            this.isLoading = false;
            this.showToastMessage('Exito', 'Se ha guardado la suma asegurada', 'success');
            this.nextAction();
        }).catch(error => {
            this.isLoading = false;
            this.showToastMessage(error, 'Error al intentar guardar la suma asegurada, intente nuevamente', 'error');
        });
    }

    getStates(data) {
        let states = data['Estado'];
        let statesReduce = states.reduce(
            (unique, item) => (unique.includes(item) ? unique : [...unique, item]),
            [],
        );
        for (let keySrch in statesReduce) {
            this.stateOpts = [...this.stateOpts, {
                label: states[keySrch],
                value: states[keySrch]
            }];
        }
        this.stateValue = this.stateOpts[0].value;
    }

    get stateOptions() {
        return this.stateOpts;
    }

    selectedState(event) {
        this.stateValue = event.detail.value;
    }

    getMunicipio(data) {
        let municipios = data['Municipio'];
        let muniReduce = municipios.reduce(
            (unique, item) => (unique.includes(item) ? unique : [...unique, item]),
            [],
        );
        for (let keySrch in muniReduce) {
            this.muniOpts = [...this.muniOpts, {
                label: muniReduce[keySrch],
                value: muniReduce[keySrch]
            }];
        }
        this.muniValue = this.muniOpts[0].value;
    }

    get muniOptions() {
        return this.muniOpts;
    }

    selectedMuni(event) {
        this.muniValue = event.detail.value;
    }

    getCity(data) {
        let cities = data['Ciudad'];
        let cityReduce = cities.reduce(
            (unique, item) => (unique.includes(item) ? unique : [...unique, item]),
            [],
        );
        for (let keySrch in cityReduce) {
            this.cityOpts = [...this.cityOpts, {
                label: cityReduce[keySrch],
                value: cityReduce[keySrch]
            }];
        }
        this.cityValue = this.cityOpts[0].value;
    }

    get cityOptions() {
        return this.cityOpts;
    }

    selectedCity(event) {
        this.cityValue = event.detail.value;
    }

    handleCP(event) {
        const codigo = event.target.value;
        const regExp = /^([0-9])*$/;
        if (codigo.length === 5 && regExp.test(codigo)) {
            const output = 'exito';
            this.error = null;
            if (output != '') {
                this.cp = event.target.value;
                this.calNewNeigh = true;
                this.getZipCode();
            } else {
                this.estado = 'No Found';
                this.ciudad = 'No found';
                this.alcaldia = 'No Found';
            }
        } else {
            this.stateOpts = [];
            this.stateValue = "";
            this.muniOpts = [];
            this.muniValue = "";
            this.cityOpts = [];
            this.cityValue = "";
            this.hidro = "";
            this.sismica = "";
            this.hidrocheck = false;
            this.sismicacheck = false;
        }
        this.buttonOnOf();
    }


    handleClick1(event) {
        this.check1 = event.target.checked;
        this.propiedad = 'Propio';
        this.check2 = false;
        this.m2 = '';
        this.calNewNeigh = true;
        this.buttonOnOf();
    }

    handleClick2(event) {
        this.check2 = event.target.checked;
        this.check1 = false;
        this.propiedad = 'Rentado';
        this.m2 = 60;
        this.calNewNeigh = true;
        this.buttonOnOf();
    }

    handleClick3(event) {
        this.check3 = event.target.checked;
        this.check4 = event.target.checked = false;
        this.buttonOnOf();
    }

    handleClick4(event) {
        this.check4 = event.target.checked;
        this.check3 = false;
        this.buttonOnOf();
        if (this.check4) {
            this.showToastMessage('Importante', 'Debido a la información que proporcionó el cliente tu flujo concluye aquí.', 'Error');
        }
    }

    handleClick5(event) {
        this.check5 = event.target.checked;
        this.check6 = false;
        this.piso = 2;
        this.pisodepa = '';
        this.pisovive = '';
        this.buttonOnOf();
        this.construccion = 'house';
    }

    handleClick6(event) {
        this.check6 = event.target.checked;
        this.check5 = false;
        this.piso = '';
        this.pisodepa = 5;
        this.pisovive = 2;
        this.buttonOnOf();
        this.construccion = 'flat';
    }

    handleConstructions(event) {
        this.m2 = event.target.value;
        this.buttonOnOf();
    }


    hidrometeoroligica() {
        const outputt = 'exito';
        this.error = null;
        if (outputt != '') {
            this.hidro = 'Alfa 1 peninsula de Yucatan';
            if (this.hidro === 'Alfa 1 peninsula de Yucatan' || this.hidro === 'Alfa 1 Pacifico Sur' || this.hidro === 'Alfa 1 Golfo de Mexico') {
                this.hidrocheck = true;
            } else {
                this.hidrocheck = false;
            }
            this.sismica = 'Zona Sismica 3';
            if (this.sismica === 'Zona Sismica 2' || this.sismica === 'Zona Sismica 3') {
                this.sismicacheck = true;
            } else {
                this.sismicacheck = false;
            }
        }
    }

    get options() {
        return [{label: 'Nuevo',value: 'Nuevo'},{label: 'Viejo',value: 'Viejo'}];
    }

    handleChange(event) {
        this.value = event.detail.value;
    }

    saveOut(event) {
        this.updStage();
        this.crtAddress();
        this.selection = event.target.label;
        if (this.cp === undefined) {
            this.cp = '';
        }
    }

    buttonOnOf() {
        this.updateQuoteCup();
        if (this.check1 || this.check2) {
            if ((this.m2 >= 40 && this.m2 <= 99999) || this.check2) {
                if (this.cp !== undefined && this.cp.length === 5) {
                    this.buttonOnOf2();
                } else {
                    this.button = true;
                }
            } else {
                this.button = true;
            }
        } else {
            this.button = true;
        }
    }

    buttonOnOf2() {
        if (this.check3) {
            if (this.check5 || this.check6) {
                this.button = false;
            } else {
                this.button = true;
            }
        } else {
            this.button = true;
        }
    }

    handledNextAction() {
        const propio = this.check1;
        const rentado = this.check2;
        if (propio === true) {
            let upWrapStage = Object.assign({}, this.wrapperContext, {
                etapa: "cotizada"
            });
            let upPropio = Object.assign({}, this.wrapperCheck, {
                checkpropio: "Propio"
            });
            this.wrapperContext = upWrapStage;
            this.wrapperCheck = upPropio;
        } else if (rentado === true) {
            let upWrapStageRen = Object.assign({}, this.wrapperContext, {
                etapa: "cotizadaRenta"
            });
            let upRentado = Object.assign({}, this.wrapperCheck, {
                checkrentado: "Rentado"
            });
            this.wrapperContext = upWrapStageRen;
            this.wrapperCheck = upRentado;
        }
        this.flujo = 'a';
        if(this.calNewNeigh) {
            this.listPlaces();
        } else {
            this.roundTotalAmounts(this.sumaSugerida);
        }

    }

    nextAction() {
        this.isLoading = true;
        let objdataforms = this.setTransObjData();
        let montos = this.montosAsegurados;
        let objCP = new Object();
        objCP.cp = this.cp;
        objCP.zipCode = this.zipCode;
        objCP.State = this.stateValue;
        objCP.Municipio = this.muniValue;
        objCP.City = this.cityValue;
        let jsonDetail = {
            cotizObject: this.wrapperContext,
            zipCode: objCP,
            alianzaRP: this.objAlianzasRP,
            alianzaPP: this.objAlizanasPP,
            cotizProRen: this.wrapperCheck,
            amount: montos,
            bienesAsegTW: this.objBienesAseg,
            flujo: this.flujo,
            objdatfrms: objdataforms
        };
        const nextAction = new CustomEvent("nextaction", {
            detail: jsonDetail,
        });
        this.dispatchEvent(nextAction);
        this.isLoading = false;
    }

    crtQuotes() {
        const valChePro = this.check1;
        const valCheRen = this.check2;
        if (valChePro === true) {
            this.isProRen = 'Propio';
        } else if (valCheRen === true) {
            this.isProRen = 'Rentado';
        }
        crtQuoteVal({
            idOp: this.registroid, nVal: this.isProRen
        }).then(result => { }).catch(error => {
            this.showToastMessage('Información', 'Ocurrió un error al crear el registro de Cotización', 'error');
        });
    }

    crtAddress() {
        const codigoCp = (this.cp === undefined ? '' : this.cp);
        const valCheckPro = this.check1;
        const valCheckRen = this.check2;
        const valCheckUso = this.check3;
        const valCheckUsoNo = this.check4;
        const valMts = (this.m2 === undefined ? '' : this.m2);
        const valCasa = this.check5;
        const valDepto = this.check6;
        const idAddPropi = this.idAddPro;
        this.idPropiedad(idAddPropi);
        this.dataInfo.codigoPostal = codigoCp;
        this.stateInfo();
        this.checkPropiedad(valCheckPro, valCheckRen, valMts);
        this.usoPropio(valCheckUso, valCheckUsoNo);
        this.propiReference(valCasa, valDepto);
        crtAddressVal({
            srtId: this.registroid, fdataAddress: this.dataInfo
        }).then(result => {
            getRecordNotifyChange([{ recordId: this.registroid }]);
        }).catch(error => {
            this.showToastMessage('Información', 'Ocurrió un error al crear o actualizar el registro de dirección', 'error');
        });
    }

    idPropiedad(idAddPropi) {
        if (idAddPropi === '') {
            this.dataInfo.idAddrPro = 'NoId';
            this.dataInfo.checkedId = 'false';
        } else {
            this.dataInfo.idAddrPro = idAddPropi;
            this.dataInfo.checkedId = 'true';
        }
    }

    stateInfo() {
        this.dataInfo.State = (this.stateValue === undefined ? '' : this.stateValue);
        this.dataInfo.Municipio = (this.muniValue === undefined ? '' : this.muniValue);
        this.dataInfo.City = (this.cityValue === undefined ? '' : this.cityValue);
    }

    checkPropiedad(valCheckPro, valCheckRen, valMts) {
        if (valCheckPro === true) {
            this.dataInfo.valPropio = 'Propio';
            this.dataInfo.mtsBuild = valMts;
        } else if (valCheckRen === true) {
            this.dataInfo.valPropio = 'Rentado';
        } else {
            this.dataInfo.valPropio = 'Na';
        }
    }

    usoPropio(valCheckUso, valCheckUsoNo) {
        if (valCheckUso === true) {
            this.dataInfo.valUso = 'Si';
        } else if (valCheckUsoNo === true) {
            this.dataInfo.valUso = 'No';
        } else {
            this.dataInfo.valUso = 'Na';
        }
    }

    propiReference(valCasa, valDepto) {
        if (valCasa === true) {
            this.dataInfo.Reference = 'Casa';
        } else if (valDepto === true) {
            this.dataInfo.Reference = 'Departamento';
        } else {
            this.dataInfo.Reference = 'Na';
        }
    }

    obtAlianzasSrv() {
        this.isLoading = true;
        mGetSetAlianzasRP({}).then(data => {
            this.getAddressVal();
            let objDatos = new Object();
            let iStatusCode;
            let sDescription;
            let sAllianceCode;
            let sProductCode;
            let planReview;
            let planCode;
            if (data) {
                iStatusCode = parseInt(data.oResponse.code);
                sDescription = data.oResponse.description;
                if (iStatusCode === 200) {
                    this.objAlianzasRP = data;
                    for (let i = 0; i < data.oData.length; i++) {
                        sAllianceCode = data.oData[i].allianceCode;
                        sProductCode = data.oData[i].productCode;
                        planReview = data.oData[i].planReview;
                        planCode = data.oData[i].planCode;
                        objDatos.codigoAlianza = sAllianceCode;
                        objDatos.codigoProducto = sProductCode;
                        objDatos.revisionPlan = planReview;
                        objDatos.codigoPlan = planCode;
                    }
                    this.isLoading = true;
                    mGetSetAlianzasPP({ sManagmenUnit: sAllianceCode, sProductCode: sProductCode }).then(result => {
                        this.isLoading = false;
                        iStatusCode = parseInt(result.oResponse.code);
                        sDescription = data.oResponse.description;
                        if (iStatusCode === 200) {
                            this.objAlizanasPP = result;
                            this.datosParticulares(sAllianceCode, sProductCode);
                            this.getCover(objDatos);
                        }
                    }).catch(error => {
                        this.isLoading = false;
                        this.showToastMessage('Web Service[listCarInsuranceCatalogs]', 'Ocurrio un error al consumir el servicio.', 'error');
                    });
                } else {
                    this.showToastMessage('Web Service[listInsuranceInformationCatalogs]', this.textError + iStatusCode + ' - ' + sDescription, 'error');
                }
            }
            this.isLoading = false;
        }).catch(error => {
            this.isLoading = false;
            this.getAddressVal();
            this.showToastMessage('Web Service[listInsuranceInformationCatalogs]', 'Ocurrio un error al consumir el servicio.', 'error');
        });
    }

    bienesAsegurados() {
        this.getAddressVal();
    }

    datosParticulares(pAllianceCode, pProductCode) {
        this.isLoading = true;
        getDatosPart({ sManagmenUnit: pAllianceCode, sProductCode: pProductCode }).then(result => {
            let dPStatusCode = parseInt(result.oResponse.code);
            let dpDescription = result.oResponse.description;
            if (dPStatusCode !== 200) {
                this.showToastMessage('Obtener Datos Particulares', this.textError + dPStatusCode + ' - ' + dpDescription, 'error');
            }
            this.isLoading = false;
        }).catch(error =>{
            this.isLoading = false;
        });
    }

    getCover(objsDatos) {
        this.isLoading = true;
        getCoverages({ datosFinales: objsDatos }).then(result => {
            let oBStatusCode = parseInt(result.oResponse.code);
            let oBDescription = result.oResponse.description;
            if (oBStatusCode !== 200) {
                this.showToastMessage('Obtener Coberturas', this.textError + oBStatusCode + ' - ' + oBDescription, 'error');
            }
            this.isLoading = false;
        }).catch(error => {
            this.isLoading = false;
        });
    }

    handledCloseDialog(event) {
        this.isLoading = false;
        if(event.detail.isNext) {
            this.flujo = 'b';
            this.montosAsegurados = event.detail.amount;
            this.suggestAmount = this.montosAsegurados[0];
            this.updateSuma();
        } else {
            this.isLoading = false;
            this.openModal = false;
        }
    }

    updStage() {
        let stageName = 'newCot';
        updCurrentStage({
            srtOppId: this.registroid, etapaForm: stageName
        }).then(result => {
            this.showToastMessage('Éxito', 'Etapa correctamente guardada a NewCot ', 'success');

        }).catch(error => {
            this.showToastMessage('Información', 'No se pudo guardar la etapa correctamente ', 'error');
        });
    }

    updateQuoteCup() {
        updateCuponCot({ oppId: this.registroid, cuponCode: '' }).then(result => {
            this.isLoading = false;
        }).catch(error => {
            this.isLoading = false;
            this.showToastMessage(error, 'Error al actualizar Quote', 'error');
        });
    }

    showToastMessage(sTitle, sMessage, sVariant) {
        const oEvt = new ShowToastEvent({
            title: sTitle,
            message: sMessage,
            variant: sVariant
        });
        this.dispatchEvent(oEvt);
    }

    setTransObjData() {
        let objDataForms = new Object();
        objDataForms.ib_data_propio = this.check1;
        objDataForms.ib_data_rentado = this.check2;
        objDataForms.ib_data_cp = this.cp;
        objDataForms.ib_data_propusosi = this.check3;
        objDataForms.ib_data_propusosn = this.check4;
        objDataForms.ib_data_conscasa = this.check5;
        objDataForms.ib_data_consdept = this.check6;
        objDataForms = JSON.stringify(objDataForms);
        return objDataForms;
    }
}