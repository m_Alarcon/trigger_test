import { LightningElement,api,track} from 'lwc';
export default class Mx_sb_vts_vsd_cotizadorPagos extends LightningElement {//NOSONAR
    @api quoteId = '';
    @track totalmonth = '$ ' + 0.00;
    @track totalsemianual = '$ ' + 0.00;
    @track totalanual = '$ ' + 0.00;

    @api
    calcTotal(strSumAsePoliz) {
        this.totalmonth = '$ ' + this.formatMoney((parseFloat(strSumAsePoliz) / 12));
        this.totalsemianual = '$ ' +  this.formatMoney(parseFloat(strSumAsePoliz) / 2);
        this.totalanual = '$ ' +  this.formatMoney(parseFloat(strSumAsePoliz));
    }

    @api
    updtTotal() {
        let strSumaAsePoliz = 3500;
        let rndTest = Math.floor(Math.random() * (500 - 50)) + 50;
        let updtSumAse = parseFloat(strSumaAsePoliz) - rndTest;
        this.calcTotal(updtSumAse);
    }

    formatMoney = (amount) => {
        var formatter = new Intl.NumberFormat('en-US', {style: 'currency',
            currency: 'USD',
        });
        return formatter.format(amount);
    }
}