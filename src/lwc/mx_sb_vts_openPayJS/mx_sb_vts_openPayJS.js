import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { loadScript } from 'lightning/platformResourceLoader';
import OpenPayJS from '@salesforce/resourceUrl/openPayJSBKP';
import findSandbox from '@salesforce/apex/MX_SB_VTS_PaymentModule_Ctrl.findGenerica';

export default class Mx_sb_vts_openPayJS extends LightningElement {
    resource = OpenPayJS;
    @api generica = false;
    @track successMessage = '';
    @track apitokentrack = '';
    renderedCallback() {
        findSandbox().then(result => {
            if(result) {
                this.generica = result.generica;
            }
            this.generateToken();
        }).catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: error.enhancedErrorType,
                    message: 'No se pudo recuperar información de token',
                    variant: 'warning'
                })
            );
        });
    }

    @api
    generateToken() {
        loadScript(this, this.resource + '/openpay.v1.js').then(() => {
            document.cookie = '_ga; SameSite=None; Secure';
            document.cookie = '_gid; SameSite=None; Secure'
            OpenPay.setId('mnqcgrppv7kl3jluue9y');
            OpenPay.setApiKey('sk_5831b35232884c1d89dd6d0203924503');
            loadScript(this, this.resource + '/openpay-data.v1.js').then(() => {
                OpenPay.setSandboxMode(!this.generica);
                const deviceSessionId = OpenPay.deviceData.setup("tokenme-form", "deviceSession_id");
                this.successMessage = 'deviceSessionId: ' + deviceSessionId;
                this.apitokentrack = deviceSessionId;
            }).catch(errorCat => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error!!',
                        message: error,
                        variant: 'error'
                    })
                )});
        }).catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error!!',
                    message: error,
                    variant: 'error'
                })
            );
        });
    }

    @api
    retriveTokenHandler() {
        let apiToken = this.apitokentrack;
        return apiToken;
    }
}