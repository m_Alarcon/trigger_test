import { LightningElement,wire,track } from 'lwc';
import MX_SB_PS_DemoninacionSegurosBBVA from '@salesforce/label/c.MX_SB_PS_DemoninacionSegurosBBVA';
import getMetadata from '@salesforce/apex/MX_SB_PS_ScriptComponet_Ctrl.obtenerDatos';
import getUserDetails from '@salesforce/apex/MX_SB_PS_ScriptComponet_Ctrl.getUserDetails';
import Id from '@salesforce/user/Id';

export default class MX_SB_PS_ScriptComponet extends LightningElement {
    recId = Id;
    Label_DemoninacionSegurosBBVA = MX_SB_PS_DemoninacionSegurosBBVA;
    @track user;
    @track error;
    @wire(getMetadata, {}) recordMdt;
    @wire(getUserDetails, { recId: '$recId' }) infoUser({ error, data }){
        if (data) {
            this.user = data;
        } else if (error) {
            this.error = error;
        }
    }
}// NOSONAR