import { LightningElement, api, track } from 'lwc';

export default class Mx_sb_vts_cotizadorVSD extends LightningElement {//NOSONAR
    @api recordId;
    @track isNewCot = false;
    @track isFrecuency = false;
    @track cotizada = false;
    @track formalizada = false;
    @track tarificada = false;
    @track emitida = false;

    @track isLoaded = false;

    connectedCallback() {
        this.isNewCot = true;
    }
    handledEventBack(event) {
        this.moveStages(event);
    }
    handledEventNext(event) {
        this.moveStages(event);
    }
    moveStages(event) {
        this.isLoaded = false;
        this.cotizObject = event.detail;
        let stage = this.cotizObject.etapa;
        this.hideStages();
        this.moveToStage(stage);
    }
    hideStages() {
        this.isNewCot = false;
        this.isFrecuency = false;
        this.cotizada = false;
        this.formalizada = false;
        this.tarificada = false;
    }
    moveToStage(etapa) {
        switch (etapa) {
            case 'newCot':
                this.isNewCot = true;
                break;
            case 'frecuencia':
                this.isFrecuency = true;
                break;
            case 'cotizada':
                this.cotizada = true;
                break;
            case 'tarificada':
                this.tarificada = true;
                break;
            case 'formalizada':
                this.formalizada = true;
                break;
            default:
                break;
        }
        this.isLoaded = true;
    }
}