import {api, LightningElement, track} from 'lwc';
import getContentDetails from '@salesforce/apex/MX_BPP_FichaProducto_Ctrl.getContentDetails';
import getAssetLibraries from '@salesforce/apex/MX_BPP_FichaProducto_Ctrl.getAssetLibraries';
import getContentDocuments from '@salesforce/apex/MX_BPP_FichaProducto_Ctrl.getContentDocuments';
import {NavigationMixin} from 'lightning/navigation';

const columns = [
    { label:'Ficha Producto', fieldName: 'Title', wrapText:true,
        cellAttributes: {
            iconName: {fieldName: 'icon'}, iconPosition: 'left'
        }
    },

    {label: 'Preview', type: 'button', typeAttributes: {
            label: 'Preview', name: 'Preview', variant: 'brand-outline',
            iconName: 'utility:preview' , iconPosition: 'right'
        }
    }
];

export default class Mx_BPP_FichaProducto extends NavigationMixin(LightningElement) {
    @api recordId;
    @api selectedIds;
    @api contentDocsIds = [];
    @track columnsList = columns;
    @track dataList;
    @track searchList;
    @track librariesList =[];
    @track librariesPicklist= [{label: 'Todos', value:'Todos'}];
    @track librariesObj;
    @api previewFromVisit;
    @track filesToSend = [];
    @track items = [];

    connectedCallback() {
        this.getContentVersionDetails();
    }

    getContentVersionDetails() {
        let supportedIconExtensions = ['attachment', 'csv', 'excel',
            'flash', 'folder', 'gdoc', 'gdocs', 'html', 'library_folder',
            'pdf', 'txt', 'unknown','word', 'xml', 'zip'];
        getContentDetails({
            recordId : this.recordId
        }).then(result => {
                let parsedData = JSON.parse(result);
                let stringifiedData = JSON.stringify(parsedData);
                let finalData = JSON.parse(stringifiedData);
                let baseUrl = this.getBaseUrl();
                finalData.forEach(file => {

                    file.downloadUrl = baseUrl+'sfc/servlet.shepherd/document/download/'+file.ContentDocumentId;
                    file.fileUrl = baseUrl+'sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId='+file.Id;
                    let fileType = file.ContentDocument.FileType.toLowerCase();
                    this.contentDocsIds.push(file.ContentDocumentId.toString());
                    if(supportedIconExtensions.includes(fileType)) {
                        file.icon = 'doctype:' + fileType;
                    }

                });
                this.dataList = finalData;
                this.librariesList = this.dataList;
            }).then(data => {
                this.getFamilyPicklistValues();
            }).catch(error=> {
                console.error('***ERROR*** \n', error);
            });
    }

    getFamilyPicklistValues() {

        if(this.contentDocsIds.length > 0 ) {
            getAssetLibraries({
                cntDocsIds : this.contentDocsIds
            }).then(result => {
                let retrievedData = JSON.parse(result);
                let values = retrievedData;
                this.librariesObj = values;
                this.librariesObj.forEach(value => {
                    this.librariesPicklist.push({label: value.Name, value: value.Id});
                });

                this.template.querySelector('[data-id="librariesValues"]').options = this.librariesPicklist;
            })
        }
    }

    handleRowAction(event) {
        const actionName = event.detail.action.name;
        const row = event.detail.row;
        switch (actionName) {
            case 'Preview':
                this.previewFile(row);
                break;
            default:
        }
    }

    previewFile(file) {

        if(this.previewFromVisit) {
            this[NavigationMixin.Navigate]({
                type: 'standard__namedPage',
                attributes: {
                    pageName: 'filePreview'
                },
                state: {
                    selectedRecordId: file.ContentDocumentId
                }
            });

        }else {
            window.open(file.downloadUrl);
        }
    }

    handleSearch(event) {
        let value = event.target.value;
        let name = event.target.name;
        this.searchList = this.librariesList;
        if(name === 'Title') {
            this.searchList = this.searchList.filter(file => {
               return file.Title.toLocaleLowerCase().includes(value.toLocaleLowerCase());
            });
            this.template.querySelector('[data-id="documentsTable"]').data = this.searchList;
        }
    }

    handleLibraries(event) {
        let value = event.target.value;
        this.librariesList = this.dataList;
        if(value=== 'Todos') {
            this.template.querySelector('[data-id="documentsTable"]').data = this.librariesList;
        } else {
            getContentDocuments({
                docIds: this.contentDocsIds
            }).then(resultData => {
                let result = JSON.parse(resultData);
                let strObj = JSON.stringify(result);
                let objDocs = JSON.parse(strObj);
                objDocs = objDocs.filter(document => {
                    return document.ParentId === value
                }).map(document => document.Id);

                this.librariesList = this.librariesList.filter(file => objDocs.includes(file.ContentDocumentId));
                this.template.querySelector('[data-id="documentsTable"]').data = this.librariesList;


            }).catch(error => {
                console.error('***ERROR***\n' + error);
            })
        }
    }

    attachFiles(event) {
        const selected = this.template.querySelector('lightning-datatable').getSelectedRows();
        this.selectedIds = [];
        this.items = [];
        if(selected.length > 0) {
            let cleanFiles = selected
            let tempAttachArr = [];
            cleanFiles.forEach(file => {
                {
                    file.IsAssetEnabled = true;
                    delete file.downloadUrl;
                    delete file.fileUrl;
                    delete file.ContentDocument
                    tempAttachArr.push({"name":`${file.Id}`, "label": `${file.Title}`});
                }
            })

            this.updateFilesToSend(cleanFiles);
            this.items = tempAttachArr;
        }
    }

    updateFilesToSend(files) {
        this.filesToSend = files;
        let attachFilesEvt = new CustomEvent('attach', {detail: JSON.stringify(this.filesToSend)});
        this.dispatchEvent(attachFilesEvt);
    }

    handleRemoveItem(event) {
        let result = [];
        if(this.filesToSend.length > 0 ) {
            const name = event.detail.item.name;
            const index = event.detail.index;
            this.items.splice(index, 1);
            result = this.filesToSend.filter(file => file.Id !== name);
            this.updateFilesToSend(result);
        }
    }

    getBaseUrl() {
        let baseUrl = 'https://'+location.host+'/';
        return baseUrl;
    }

}