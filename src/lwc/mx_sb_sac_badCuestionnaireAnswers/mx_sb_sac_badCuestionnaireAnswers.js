import { LightningElement, api, wire, track } from 'lwc';
import { updateRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';
import MX_SB_SAC_DETALLE_FIELD from '@salesforce/schema/Case.MX_SB_SAC_Detalle__c';
import MX_SB_SAC_FINALIZAFLUJO_FIELD from '@salesforce/schema/Case.MX_SB_SAC_FinalizaFlujo__c';
import MX_SB_SAC_PRODUCTOSPARAVIDA_FIELD from '@salesforce/schema/Case.MX_SB_SAC_ProductosParaVida__c';
import MX_SB_SAC_PRODUCTOSPARAVENTA_FIELD from '@salesforce/schema/Case.MX_SB_SAC_ProductosParaVenta__c';
import MX_SB_SAC_CONTRATO_FIELD from '@salesforce/schema/Case.MX_SB_SAC_Contrato__c';
import REASON_FIELD from '@salesforce/schema/Case.Reason';
import PRODUCTID_FIELD from '@salesforce/schema/Case.ProductId';
import DESCRIPTION_FIELD from '@salesforce/schema/Case.Description';
import ID_FIELD from '@salesforce/schema/Case.Id';
import MX_SB_SAC_SUBDETALLE_FIELD from '@salesforce/schema/Case.MX_SB_SAC_Subdetalle__c';
import MX_SB_SAC_SUBDETALLE_VIDA_FIELD from '@salesforce/schema/Case.MX_SB_SAC_Subdetalle_Vida__c';
import MX_SB_SAC_APLICAR_MALA_VENTA_FIELD from '@salesforce/schema/Case.MX_SB_SAC_Aplicar_Mala_Venta__c';
import initialData from '@salesforce/apex/MX_SB_SAC_IdentificarCliente.initialData';
import Id from '@salesforce/user/Id';
import { handleUndefinedValues } from 'c/mx_sb_sac_utils';
import MX_SB_1500_ISAUTHENTICATED_FIELD from '@salesforce/schema/Case.MX_SB_1500_isAuthenticated__c';

export default class Mx_sb_sac_badCuestionnaireAnswers extends LightningElement {
    @track clientName;
    @track userName;
    @track description = '';
    @api caseId;
    @api contractData;
    userId = Id;
    product = 'Consultas Sin Póliza';
    is1500 = false;

    handleUpdateDescription(event) {
        this.description = event.target.value;
    }

    handleFinish() {
        if(this.description.length === 0) {
            const commentserror = new ShowToastEvent({
                title: 'Error',
                message: 'Por favor llene los comentarios para cerrar el caso',
                variant: 'error',
                mode: 'dismissable'
            });
            this.dispatchEvent(commentserror);
        }
        else {
            const fields = {};
            fields[ID_FIELD.fieldApiName] = this.caseId;
            fields[REASON_FIELD.fieldApiName] = 'Consultas';
            fields[MX_SB_SAC_FINALIZAFLUJO_FIELD.fieldApiName] = true;
            if(this.is1500 === false) {
                fields[MX_SB_SAC_PRODUCTOSPARAVIDA_FIELD.fieldApiName] = this.product;
            } else {
                fields[MX_SB_1500_ISAUTHENTICATED_FIELD.fieldApiName] = true;
            }
            fields[MX_SB_SAC_PRODUCTOSPARAVENTA_FIELD.fieldApiName] = this.product;
            fields[MX_SB_SAC_CONTRATO_FIELD.fieldApiName] = this.contractData.Id;
            fields[PRODUCTID_FIELD.fieldApiName] = this.contractData.product;
            const recordInput = { fields };
            updateRecord(recordInput).then(() => {
            }).then(() => {
                fields[MX_SB_SAC_DETALLE_FIELD.fieldApiName] = 'No autentica correctamente';
                fields[MX_SB_SAC_SUBDETALLE_FIELD.fieldApiName] = '';
                if(this.is1500 === false) {
                    fields[MX_SB_SAC_SUBDETALLE_VIDA_FIELD.fieldApiName] = '';
                    fields[MX_SB_SAC_APLICAR_MALA_VENTA_FIELD.fieldApiName] = '';
                }
                fields[DESCRIPTION_FIELD.fieldApiName] = this.description;
                const upRecord = { fields };
                updateRecord(upRecord);
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Éxito',
                        message: 'Se ha actualizado el Caso',
                        variant: 'success'
                    })
                );
                return refreshApex(this.record);
            }).catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: error.enhancedErrorType,
                        message: 'Algo fallo, por favor contacte a su administrador de sistema ',
                        variant: 'error'
                    })
                );
            })
        }

    }

    @wire(initialData, { recId : '$caseId', userId: '$userId' })
    wiredData({ error, data}) {
        if(data) {
            const  conts = data;
            for(let key in conts){
                if(key !== null) {
                    if(key === 'case') {
                        this.clientName = handleUndefinedValues(conts[key].MX_SB_SAC_Nombre__c)
                                    + ' ' + handleUndefinedValues(conts[key].MX_SB_SAC_ApellidoPaternoCliente__c)
                                    + ' ' + handleUndefinedValues(conts[key].MX_SB_SAC_ApellidoMaternoCliente__c);
                        this.is1500 = conts[key].MX_SB_1500_is1500__c;
                    }
                    if(key === 'user') {
                        this.userName = conts[key].Name;
                        this.userType = handleUndefinedValues(conts[key].Department);
                    }
                }
            }
        } else if (error) {
            throw new Error('Failed to retrieve initialData');
        }
    }

    handleGoBack() {
        const backToQuestions = new CustomEvent('backquestions', {
            detail: false,
            bubbles: true
        });
        this.dispatchEvent(backToQuestions)
    }
}