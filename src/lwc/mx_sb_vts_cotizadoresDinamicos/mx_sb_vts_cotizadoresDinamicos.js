import { LightningElement, api, track } from 'lwc';
import getOppVal from '@salesforce/apex/MX_SB_VTS_CotizInitData_Ctrl.getOppVal';
export default class Mx_sb_vts_cotizadoresDinamicos extends LightningElement {
    @api recordId;
    @api isHSD = false;
    @api isVSD = false;
    @track oppData;

    connectedCallback() {
        getOppVal({reId: this.recordId}).then(result => {
            this.oppData = result[0];
            this.evaluteProduct();
        }).catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: error.enhancedErrorType,
                    message: 'No se pudo recuperar el producto del cotizador',
                    variant: 'warning'
                })
            );
        });
    }

    evaluteProduct() {
        let productName = this.oppData.Producto__c;
        switch (productName) {
            case 'Hogar seguro dinámico':
                this.isHSD = true;
                break;

            case 'Vida segura dinámico':
                this.isVSD = true;
                break;
            default:
                break;
        }
    }
}