import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import retrieveRecords from '@salesforce/apex/MX_SB_SAC_IdentificarCliente.retrieveRecords';
import searchContractOrAccount from '@salesforce/apex/MX_SB_SAC_IdentificarCliente.searchContractOrAccount';
import searchProductData from '@salesforce/apex/MX_SB_SAC_IdentificarCliente.searchProductData';
import retrieveTipification from '@salesforce/apex/MX_SB_SAC_IdentificarCliente.retrieveTipification';
import makeCallout from '@salesforce/apex/MX_SB_SAC_IdentificarCliente.makeCallout';
import { finishClientAuth, handleUndefinedValues, finishQuoting, getSalesProcess } from 'c/mx_sb_sac_utils';

const columns = [
    { label: 'Poliza', fieldName: 'Policy', wrapText: true },
    { label: 'Contratante', fieldName: 'AccountName', wrapText: true },
    { label: 'RFC Contratante', fieldName: 'AccountRFC', wrapText: true },
    { label: 'Asegurado', fieldName: 'Beneficiary', wrapText: true },
    { label: 'RFC Asegurado', fieldName: 'BeneficiaryRFC', wrapText: true },
    { label: 'Producto', fieldName: 'Product', wrapText: true },
    { label: 'Placas', fieldName: 'Plate', wrapText: true }
];

export default class Mx_sb_sac_searchData extends LightningElement { // NOSONAR
    @api clientName;
    @api userType;
    @api caseData;
    @api comments;
    @api caseId;
    @api isQuoting;
    @track searchKey = '';
    @track recordList = [];
    @track value = '';
    @track columns = columns;
    @track filter = '';
    @track showRecords = false;
    @track selectedRecord = [];
    @track show404 = false;
    @track showQuestions = false;
    @track contractId = '';
    @track prod = '';
    @track rawData = []
    @track prodId = '';
    @track accountId = '';
    @track showNext = false;
    @track endFlow = false;
    @track tipification = [];
    @track policyNumber = '';
    @track showSpinner = false;
    @track disableInput = true;
    @track productData = [];
    showSalesPath = false;
    product = '';
    saleProcess = '';
    dataSearch = true;
    extraData = new Map();
    disableNext = true;
    newCaseData = [];

    constructor() {
        super();
        this.template.addEventListener('goback', this.handleCustomEvent.bind(this));
    }

    @wire(retrieveRecords, { keyword: '$searchKey', option: '$value' })
    wiredData({ error, data }) {
        if(data) {
            this.show404 = false;
            this.recordList = this.prepareData(data);
            this.rawData = data;
        } else if(error) {
            this.recordList = [];
            this.show404 = true;
            this.showRecords = false;
            this.dispatchEvent(this.showToast);
            throw new Error('Failure on retrieveRecords');
        }
    }


    @wire(retrieveTipification, { recordId: '$caseId', prodId: '$prodId', type: '$userType' })
    wiredTipification({ error, data }) {
        if(data) {
            data.forEach(element => {
                this.tipification.Id = element.Id;
                this.tipification.MX_SB_SAC_ProductosParaVenta__c = element.MX_SB_SAC_ProductosParaVenta__c;
                this.tipification.MX_SB_SAC_SubRamo__c = element.MX_SB_SAC_SubRamo__c;
                this.tipification.MX_SB_SAC_TipoDeRegistro__c = element.MX_SB_SAC_TipoDeRegistro__c;
            });
        } else if(error) {
            throw new Error(error);
        }
    }

    prepareData(array) {
        let prepareRecords = [];
        array.forEach(element => {
            let preparedData = {};
            preparedData.Id = element.Id;
            preparedData.AccountName = element.MX_SB_SAC_NombreCuentaText__c;
            preparedData.AccountRFC = element.MX_SB_SAC_RFCContratanteText__c;
            preparedData.Policy = element.MX_SB_SAC_NumeroPoliza__c;
            preparedData.Beneficiary = element.MX_SB_SAC_NombreClienteAseguradoText__c;
            preparedData.BeneficiaryRFC = element.MX_SB_SAC_RFCAsegurado__c;
            preparedData.Product = element.MX_WB_Producto__r.Name;
            preparedData.Plate = element.MX_SB_SB_Placas__c;
            preparedData.PersonEmail = element.Account.PersonEmail;
            prepareRecords.push(preparedData);
            this.showRecords = true;
        });

        return prepareRecords;
    }

    handleKeywordSearch(event) {
        clearTimeout(this.delayTimeout);
        const keyword = event.target.value;
        this.showSpinner = true;
        if(this.value === 'poliza') {
            // eslint-disable-next-line @lwc/lwc/no-async-operation
            this.delayTimeout = setTimeout(() => {
                this.policyNumber = keyword;
                makeCallout({keyword: this.policyNumber})
                .then(result => {
                    this.showSpinner = false;
                    this.rawData = result;
                    this.recordList = this.prepareData(result);
                })
                .catch(error => {
                    console.error(error);
                    this.rawData = [];
                    this.recordList = [];
                    this.showSpinner = false;
                    this.showRecords = false;
                    this.dispatchEvent(this.showToast());
                });
            }, 10000)
        } else {
            // eslint-disable-next-line @lwc/lwc/no-async-operation
            this.delayTimeout = setTimeout(() => {
                this.searchKey = keyword;
                this.showSpinner = false;
                this.checkValue(this.searchKey);
            }, 500);
        }
    }

    get options() {
        return [
            {
                label: 'Buscar Poliza (Clipert): No. de Póliza',
                value: 'poliza'
            },
            {
                label: 'Datos Asegurado: No. de Póliza, Nombre(s) y Apellido Paterno, Apellido Materno o RFC, No de Serie ó No de Placa.',
                value: 'asegurado'
            },
            {
                label: 'Contratante: No. de Póliza, Nombre(s) y Apellido Paterno, Apellido Materno o RFC, No de Serie ó No de Placa.',
                value: 'contratante'
            }
        ]
    }

    handleFilterSelection(event) {
        this.value = event.currentTarget.value;
        this.disableInput = false;
        if(this.showNext) {
            this.showNext = false;
        }
    }

    handleSelectRecord(event) {
        const selectedRows = event.detail.selectedRows;
        const values = {}
        this.selectedRecord = selectedRows;
        this.selectedRecord.forEach(element => {
            this.contractId = element.Id;
            this.prod = element.Product;
            if(element.PersonEmail !== undefined || element.PersonEmail !== '') {
                this.extraData.set('email', element.PersonEmail);
            }
            this.extraData.set('accountId', element.AccountId);
            this.rawData.forEach(elemnt => {
                if(element.Id === elemnt.Id) {
                    values.productId = elemnt.MX_WB_Producto__c;
                    values.accountId = elemnt.AccountId;
                }
            })
        });
        clearTimeout(this.delayTimeout);
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            this.prodId = values.productId;
            this.accountId = values.accountId;
        }, 100);
        this.handleShowButtons();
    }

    handleShowQuestions() {
        this.showQuestions = true;
        this.newCaseData = Object.assign({}, this.caseData, {MX_SB_SAC_CorreoCuenta__c: this.extraData.get('email')});
        this.showNext = false;
    }

    handleShowButtons() {
        switch(this.userType) {
            case 'Linea BBVA':
                this.endFlow = true;
                break;
            case '#1500':
                if((this.caseData.Reason === 'Servicios' || this.caseData.Reason === 'Cancelación') && this.comments === false) {
                    this.showNext = true;
                } else {
                    this.endFlow = true;
                }
                break;
            case 'SAC':
                if(this.caseData.Reason !== 'Cotización') {
                    this.showNext = true;
                } else {
                    this.endFlow = true;
                }
                break;
            default:
                break;
        }
    }

    handleSave() {
        const commentsEvent = new CustomEvent('comments1500', {
            detail: true,
            bubbles: true
        });
        this.dispatchEvent(commentsEvent);
        this.newCaseData = Object.assign({}, this.caseData, {MX_SB_SAC_CorreoCuenta__c: this.extraData.get('email')});
        finishClientAuth(this.accountId, this.contractId, this.newCaseData, this.prodId, this.tipification, this.comments);
    }

    showToast() {
        return new ShowToastEvent({
            title: 'Error',
            message: 'Los datos proporcionados no son validos',
            variant: 'error'
        });
    }

    checkValue(value) {
        if(value === '') {
            this.recordList = [];
            this.showNext = false;
            this.showRecords = false;
            this.endFlow = false;
        }
    }

    handleCustomEvent(event) {
        this.showNext = event.detail;
        this.showQuestions = false;
    }

    get productOptions() {
        return [
            { label: 'Seleccione una opción', value: ''},
            { label: 'Auto Seguro Dinámico', value: 'Auto Seguro Dinámico' },
            { label: 'Seguro Estudia', value: 'Seguro Estudia' },
            { label: 'Respaldo Seguro Contra Cancer', value: 'Respaldo Seguro Contra Cancer' },
            { label: 'Retiro Efectivo', value: 'Retiro Efectivo' },
            { label: 'Seguro de Gastos Funerarios', value: 'Seguro de Gastos Funerarios' },
            { label: 'Cirugía', value: 'Cirugía' },
            { label: 'Contigo Bancomer', value: 'Contigo Bancomer' },
            { label: 'Ayuda Hospitalaria', value: 'Ayuda Hospitalaria' },
            { label: 'Transacción Segura', value: 'Transacción Segura' }
        ];
    }

    renderSalesProcess(value) {
        if(value.currentTarget.value !== '') {
            this.showSalesPath = true;
            this.product = value.currentTarget.value;
            this.saleProcess = getSalesProcess(this.product);
            this.disableNext = false;
            searchProductData({searchParam: this.product})
            .then(result => {
                if(result) {
                    this.productData = result;
                }
            }).catch(error => {
                this.disableNext = true;
                this.showSalesPath = false;
                throw new Error(error);
            });
        } else {
            this.disableNext = true;
            this.showSalesPath = false;
            this.product = '';
        }
    }

    changeClientName(value) {
        this.clientName = value.currentTarget.value;
    }

    handleSaveQuoting() {
        finishQuoting(this.caseData, this.productData, this.saleProcess, this.extraData);
    }

    goBackQuotingSeach() {
        this.endFlow = false;
        this.dataSearch = true;
        this.showSalesPath = false;
    }

    getAccountData() {
        if(this.columns.length === 7) {
            this.columns.push({label: 'Email', fieldName: 'PersonEmail', wrapText: true})
        }
        searchContractOrAccount({searchParam: this.clientName})
        .then(result => {
            if(result) {
                const newStruct = [];
                for(let key in result) {
                    if(key === 'contract') {
                        this.rawData = result[key];
                        result[key].forEach(element => {
                            let preparedData = {};
                            preparedData.Id = element.Id;
                            preparedData.AccountId = element.AccountId;
                            preparedData.AccountName = element.MX_SB_SAC_NombreCuentaText__c;
                            preparedData.AccountRFC = element.MX_SB_SAC_RFCContratanteText__c;
                            preparedData.Policy = element.MX_SB_SAC_NumeroPoliza__c;
                            preparedData.Beneficiary = element.MX_SB_SAC_NombreClienteAseguradoText__c;
                            preparedData.BeneficiaryRFC = element.MX_SB_SAC_RFCAsegurado__c;
                            preparedData.Product = element.MX_WB_Producto__r.Name;
                            preparedData.Plate = element.MX_SB_SB_Placas__c;
                            preparedData.PersonEmail = element.Account.PersonEmail;
                            newStruct.push(preparedData);
                            this.showRecords = true;
                        });
                    } else if(key === 'account') {
                        this.rawData = result[key];
                        result[key].forEach(element => {
                            this.rawData.push(result[key]);
                            let preparedData = {};
                            preparedData.Id = element.Id;
                            preparedData.AccountId = element.Id;
                            preparedData.AccountName = handleUndefinedValues(element.FirstName)
                                                    + ' ' + handleUndefinedValues(element.LastName)
                                                    + ' ' + handleUndefinedValues(element.Apellido_Materno__pc);
                            preparedData.AccountRFC = element.RFC__c;
                            preparedData.PersonEmail = element.PersonEmail;
                            newStruct.push(preparedData);
                            this.showRecords = true;
                        });
                    }
                }
                this.dataSearch = false;
                this.recordList = newStruct;
            }
        }).catch(error => {
            throw new Error(error);
        })
    }
}