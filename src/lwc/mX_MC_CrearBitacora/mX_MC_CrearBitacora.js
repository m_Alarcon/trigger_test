import {
    LightningElement,
    wire,
    api
} from 'lwc';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import {
    refreshApex
} from '@salesforce/apex';
import hasBitacora from '@salesforce/apex/MX_MC_CrearBitacora_Crtl.hasBitacora';
import importBitacora from '@salesforce/apex/MX_MC_CrearBitacora_Crtl.importBitacora';
import {
    getRecord,
    getFieldValue
} from 'lightning/uiRecordApi';

import CREATED_DATE from '@salesforce/schema/Case.CreatedDate';
import CONVERSATION_ID from '@salesforce/schema/Case.mycn__MC_ConversationId__c';
import ACCOUNT_ID from '@salesforce/schema/Case.AccountId';

const fields = [
    CREATED_DATE,
    CONVERSATION_ID,
    ACCOUNT_ID,
];
export default class MX_MC_CrearBitacora extends LightningElement {
    @api recordId;

    @wire(hasBitacora, {
        idConv: '$recordId'
    }) bitEmpty;

    @wire(getRecord, {
        recordId: '$recordId',
        fields
    })
    case;

    bitacoraData;
    get visit() {
        importBitacora({
                idConv: this.recordId
            })
            .then(result => {
                this.bitacoraData = result;
            })
            .catch(error => {
                throw new Error('Error en recuperado de bitácora');
            });
        return this.bitacoraData;
    }

    get visitUrl() {
        return '/' + this.visit.Id;
    }

    get caseId() {
        return this.recordId;
    }

    get createdDate() {
        return getFieldValue(this.case.data, CREATED_DATE);
    }
    get conversationId() {
        return getFieldValue(this.case.data, CONVERSATION_ID);
    }
    get accountId() {
        return getFieldValue(this.case.data, ACCOUNT_ID);
    }

    handleCreated(event) {
        refreshApex(this.bitacora);
        const toast = new ShowToastEvent({
            title: 'Registro Creado',
            message: 'Se registro la conversación correctamente',
            variant: 'success',
            mode: 'dismissable'
        });
        this.dispatchEvent(toast);
        refreshApex(this.bitEmpty);
        const inputFields = this.template.querySelectorAll(
            '.inpFields'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
            });
        }
    }
}