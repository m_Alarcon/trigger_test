import { LightningElement, api, track } from 'lwc';
import findDataClient from '@salesforce/apex/MX_SB_VTS_CotizInitData_Ctrl.findDataClient';
import updCurrentStage from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.updCurrentOppCtrl';
import updCurrentQuo from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.updQuoLineCtrl';
import getIdSync from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.gtOppValsCrtl';
import findAddVals from "@salesforce/apex/MX_SB_VTS_ObtStates_Ctrl.obtEstadosCtrl";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class Mx_sb_vts_datosContratante extends LightningElement { //NOSONAR

    @api cp;
    @api lugarNacAp;
    @api idquotechild;
    @api objDatosContFrm;
    @api registroid = '';
    @api nombreClient = '';
    @api apellidoPat = '';
    @api wrapperParent = {};
    @api wrapperContext = {};
    @api wrapperClient = {};
    @track stateLst = [];
    @track sexo;
    @track objDatosUpd;
    @track idQuoChild;
    @track apellidoMaterno = '';
    @track genero = '';
    @track fechaNac = '';
    @track lugarNac = '';
    @track rfcClient = '';
    @track homoclave = '';
    @track telefonoMov = '';
    @track emailClient = '';
    @track curpClient = '';
    @track recLst = '';
    @track recAbre = '';
    @track fem = false;
    @track mas = false;
    @track visible = false;
    @track isLoading = false;
    @track isOpened = false;
    @track button = true;
    @track tempEmail = 'dummy00011@bbva.com';

    connectedCallback() {
        this.wrapperContext = (this.wrapperParent === undefined ? '' : this.wrapperParent);
        this.getIdSyn();
        this.isLoading = true;
        setTimeout(() => {
            let quoteId = this.idQuoChild;
            if (quoteId) {
                this.obtDatosQuo();
            } else {
                this.initDataClient();
            }
        }, 2000);
        if (this.wrapperContext.wrapperClient && this.wrapperContext.wrapperClient.nombreClient) {
            this.nombreClient = this.wrapperContext.wrapperClient.nombreClient;
        }
    }
    getState() {
        findAddVals({})
            .then((result) => {
                if (result.length === 0) {
                    this.recordsLst = [];
                    this.message = "No Records Found";
                } else {
                    const finalVals = result;
                    for (let key in finalVals) {
                        let nameVal = finalVals[key].MX_SB_VTS_States__c;
                        let nameAbre = finalVals[key].MX_SB_VTS_Abreviacion__c;
                        if (this.lugarNac === nameVal) {
                            this.recLst = nameVal;
                            this.recAbre = nameAbre;
                        }
                    }
                    this.obtCurp();
                }
                this.error = null;
            })
            .catch((error) => {
                this.error = error;
                this.recLst = null;
            });
    }
    getIdSyn() {
        getIdSync({
            idOpps: this.registroid
        }).then(data => {
            const dataValueId = data;
            for (let key in dataValueId) {
                let idChild = dataValueId[key].SyncedQuoteId;
                this.idQuoChild = (idChild === undefined ? '' : idChild);
            }
        }).catch(error => {
            throw new Error(error);
        })
    }
    obtDatosQuo() {
        findDataClient({
            quoteId: this.idQuoChild
        }).then(result => {
            if (result) {
                var mapQuote = new Map(Object.entries(result.queteDat));
                var mapOpp = result.oppRecord;
                this.assignValues(mapQuote,mapOpp);
                this.checkField();
                this.isLoading = false;
                this.showToastMessage('Éxito: ', 'Se obtuvo información de los datos guardados del Contratante', 'success');
            } else {
                this.showToastMessage('Importante: ', 'No se pudo obtener los datos del Contratante', 'error');
            }
        });
    }
    assignValues(mapVals,mapOpp) {
        let getValCli = mapVals.get('MX_SB_VTS_Nombre_Contrante__c');
        let getValApePa = mapVals.get('MX_SB_VTS_Apellido_Paterno_Contratante__c');
        let getValApeMa = mapVals.get('MX_SB_VTS_Apellido_Materno_Contratante__c');
        let getValGenero = mapVals.get('MX_SB_VTS_Sexo_Conductor__c');
        let getValFecha = mapVals.get('MX_SB_VTS_FechaNac_Contratante__c');
        let getValLugar = mapVals.get('MX_SB_VTS_LugarNac_Contratante__c');
        let getValRfc = mapVals.get('MX_SB_VTS_RFC__c');
        let getvalHomo = mapVals.get('MX_SB_VTS_Homoclave_Contratante__c');
        let getValPhone = mapVals.get('Phone');
        let getValMail = mapVals.get('MX_SB_VTS_Email_txt__c');
        let getValCurp = mapVals.get('MX_SB_VTS_Curp_Contratante__c');
        this.nombreClient = (getValCli === undefined ? '' : getValCli);
        this.apellidoPat = (getValApePa === undefined ? '' : getValApePa);
        this.apellidoMaterno = (getValApeMa === undefined ? '' : getValApeMa);
        this.fechaNac = (getValFecha === undefined ? '' : getValFecha);
        this.lugarNacAp = (getValLugar === undefined ? '' : getValLugar);
        this.stateLst = [...this.stateLst, {
            label: getValLugar,
            value: getValLugar
        }];
        const stateOps = this.stateLst[0].value;
        this.lugarNac = (stateOps === undefined ? '' : stateOps);
        this.rfcClient = (getValRfc === undefined ? '' : getValRfc);
        this.homoclave = (getvalHomo === undefined ? '' : getvalHomo);
        this.telefonoMov = (getValPhone === undefined ? '' : getValPhone);
        this.emailClient = (getValMail === undefined ? '' : getValMail);
        let tempEmail = mapOpp.Account.PersonEmail;
        this.tempEmail = tempEmail;
        let emailVal = (tempEmail === undefined ?  '' : tempEmail);
        this.emailClient = (this.emailClient === '' ?  emailVal : this.emailClient);
        this.emailClient = this.emailClient.includes('dummy') ? '' : this.emailClient;
        this.curpClient = (getValCurp === undefined ? '' : getValCurp);
        this.optValues(getValGenero);
    }
    optValues(genderVal) {
        if (genderVal === 'H') {
            this.mas = true;
            this.sexo = 'H';
        } else if (genderVal === 'M') {
            this.fem = true;
            this.sexo = 'M';
        }
        if (this.lugarNacAp !== '' || this.lugarNacAp !== undefined) {
            this.isOpened = true;
        } else {
            this.isOpened = false;
        }
    }
    initDataClient() {
        let tempWrapp = new Object();
        tempWrapp.nombreClient = this.nombreClient;
        tempWrapp.apellidoPat = this.apellidoPat;
        tempWrapp.apellidoMaterno = this.apellidoMaterno;
        tempWrapp.genero = this.genero;
        tempWrapp.fechaNac = this.fechaNac;
        tempWrapp.lugarNac = this.lugarNac;
        tempWrapp.rfcClient = this.rfcClient;
        tempWrapp.homoclave = this.homoclave;
        tempWrapp.telefonoMov = this.telefonoMov;
        tempWrapp.emailClient = this.emailClient;
        tempWrapp.curpClient = this.curpClient;
        this.wrapperClient = tempWrapp;
    }
    handleFem(event) {
        this.fem = event.target.checked;
        this.mas = false;
        this.sexo = "M";
        this.obtCurp();
    }
    handleMas(event) {
        this.mas = event.target.checked;
        this.fem = false;
        this.sexo = "H";
        this.obtCurp();
    }
    handledBackAction() {
        this.initDataClient();
        let upWrapStage = Object.assign({}, this.wrapperContext, {
            etapa: "emitida",
            wrapperClient: this.wrapperClient,
        });
        this.wrapperContext = upWrapStage;
        this.backAction();
    }
    backAction() {
        const jsonDetail = { cotizObject: this.wrapperContext, zipCode: this.cp };
        const backAction = new CustomEvent("backaction", {
            detail: jsonDetail,
        });
        this.dispatchEvent(backAction);
    }
    handledNextAction() {
        this.getDataFieldsObject();
        this.updQuote();
        let upWrapStage = Object.assign({}, this.wrapperContext, {
            etapa: "formalizada",
            wrapperClient: this.wrapperClient,
        });
        this.wrapperContext = upWrapStage;
        this.nextAction();
    }
    nextAction() {
        const jsonDetail = { cotizObject: this.wrapperContext, zipCode: this.cp, dcForm: this.objDatosContFrm };
        const nextAction = new CustomEvent("nextaction", {
            detail: jsonDetail,
        });
        this.dispatchEvent(nextAction);
    }
    saveOut() {
        this.updStage();
        this.getDataFieldsObject();
        this.updQuote();
    }
    updStage() {
        let stageName = 'tarificada';
        updCurrentStage({
            srtOppId: this.registroid, etapaForm: stageName
        }).then(result => {
            this.showToastMessage('Éxito: ', 'Se ha guardado correctamente el avance', 'success');
        }).catch(error => {
            this.showToastMessage('Error: ', 'Ocurrió un error al guardar el avance', 'error');
        });
    }
    updQuote() {
        updCurrentQuo({
            srtId: this.registroid, mapDataQuote: this.objDatosUpd
        }).then(result => {
            this.showToastMessage('Éxito: ', 'Se han actualizado los datos del Contratante en la cotización', 'success');
        }).catch(error => {
            this.showToastMessage('Error: ', 'Ocurrió un error al actualizar los datos del Contratante', 'error');
        });
    }
    getDataFieldsObject() {
        let objDatosCont = new Object();
        objDatosCont.NombreCliente = (this.nombreClient === '' ? '' : this.nombreClient);
        objDatosCont.ApellidoPaterno = (this.apellidoPat === '' ? '' : this.apellidoPat);
        objDatosCont.ApellidoMaterno = (this.apellidoMaterno === '' ? '' : this.apellidoMaterno);
        objDatosCont.SexoMasculino = this.mas;
        objDatosCont.SexoFemenino = this.fem;
        if (this.mas === true) {
            objDatosCont.SexoHm = 'H';
        } else if (this.fem === true) {
            objDatosCont.SexoHm = 'M';
        } else {
            objDatosCont.SexoHm = 'No seleccion';
        }
        let dateVal = this.fechaNac;
        if (dateVal === '') {
            objDatosCont.FechaNac = 'NA';
        } else {
            let convertDat = this.fechaNac.split('-');
            let finalDate = convertDat[2] + '/' + convertDat[1] + '/' + convertDat[0];
            objDatosCont.FechaNac = finalDate;
        }
        objDatosCont.LugarNac = (this.lugarNac === '' ? '' : this.lugarNac);
        objDatosCont.rfc = (this.rfcClient === '' ? '' : this.rfcClient);
        objDatosCont.Homoclave = (this.homoclave === '' ? '' : this.homoclave);
        objDatosCont.TelefonoCel = (this.telefonoMov === '' ? '' : this.telefonoMov);
        objDatosCont.CorreoElect = (this.emailClient === '' ? this.tempEmail : this.emailClient);
        objDatosCont.Curp = (this.curpClient === '' ? '' : this.curpClient);
        objDatosCont.OppoId = this.registroid;
        objDatosCont.QuoteId = this.idquotechild;
        this.objDatosContFrm = JSON.stringify(objDatosCont);
        this.objDatosUpd = objDatosCont;
    }
    changeFechaNac(event) {
        this.fechaNac = event.target.value;
        const fechaDate = this.fechaNac;
        var today = new Date();
        var birthDate = new Date(fechaDate);
        var ages = today.getFullYear() - birthDate.getFullYear();
        if (ages > 99 || ages < 18) {
            this.showToastMessage('Error: ', 'Fecha inválida', 'error');
        }
        this.checkField();
        this.obtCurp();
        return ages;
    }
    nombreChange(event) {
        if (event.target.name === 'nombreCliente') {
            this.nombreClient = event.target.value;
        } else if (event.target.name === 'apellidoPa') {
            this.apellidoPat = event.target.value;
        } else if (event.target.name === 'apellidoMa') {
            this.apellidoMaterno = event.target.value;
        } else if (event.target.name === 'lugarNa') {
            this.lugarNac = event.target.value;
        } else if (event.target.name === 'getCurp') {
            this.curpClient = event.target.value;
        } else if (event.target.name === 'getSex') {
            this.sexo = event.target.value;
        } else if (event.target.name === 'homoclaves') {
            this.homoclave = event.target.value;
        } else if (event.target.name === 'emailclient') {
            this.emailClient = event.target.value;
            this.tempEmail = this.emailClient !== '' ? this.emailClient : this.tempEmail;
        } else if (event.target.name === 'cel') {
            this.telefonoMov = event.target.value;
        } else if (event.target.name === 'RFC') {
            this.rfcClient = event.target.value;
        }
        this.obtCurp();
        this.checkField();
    }
    obtCurp() {
        const nombreCli = this.nombreClient;
        const apePa = this.apellidoPat;
        const apeMa = this.apellidoMaterno;
        const fechaDate = this.fechaNac;
        const getFecha = (fechaDate === null ? '' : fechaDate);
        const getSexo = this.sexo;
        const anio = getFecha.slice(0, 4);
        const curp = [];
        curp[0] = apePa.charAt(0).toUpperCase();
        curp[1] = this.buscaVocal(apePa).toUpperCase();
        curp[2] = apeMa.charAt(0).toUpperCase();
        curp[3] = nombreCli.charAt(0).toUpperCase();
        curp[4] = anio.toString().slice(2);
        curp[5] = getFecha.slice(5, 7);
        curp[6] = getFecha.slice(8, 10);
        curp[7] = (getSexo === 'H' ? 'H' : 'M');
        curp[8] = this.recAbre;
        curp[9] = apePa.slice(1).replace(/[aeiou]/gi, "").charAt(0).toUpperCase();
        curp[10] = apeMa.slice(1).replace(/[aeiou]/gi, "").charAt(0).toUpperCase();
        curp[11] = nombreCli.slice(1).replace(/[aeiou]/gi, "").charAt(0).toUpperCase();
        curp[12] = getFecha.substring(0, 2) === '19' ? '0' : 'A';
        this.curpClient = curp.join("");
        curp[13] = this.ultdig();
        this.curpClient = curp.join("");
        return curp.join("");
    }
    buscaVocal(str) {
        var vocales = 'aeiou';
        var i, c;
        for (i = 1; i < str.length; i++) {
            c = str.charAt(i);
            if (vocales.indexOf(c) >= 0) {
                return c;
            }
        }
        return 'X';
    }
    tabla(i, x) {
        if (i >= '0' && i <= '9') return x - 48;
        else if (i >= 'A' && i <= 'N') return x - 55;
        else if (i >= 'O' && i <= 'Z') return x - 54;
        else return 0;
    }
    ultdig() {
        var i, c, dv = 0;
        const rerCurp = this.curpClient;
        for (i = 0; i < rerCurp.length; i++) {
            c = this.tabla(rerCurp.charAt(i), rerCurp.charCodeAt(i));
            dv += c * (18 - i);
        }
        dv %= 10;
        return dv == 0 ? 0 : 10 - dv;
    }
    onclickRFC() {
        this.visible = true;
        this.template.querySelector('.divHelp').classList.remove('slds-hide');
    }
    hideRFC() {
        this.visible = true;
        this.template.querySelector('.divHelp').classList.add('slds-hide');
    }
    onclickHomo() {
        this.visible = true;
        this.template.querySelector('.divHelp1').classList.remove('slds-hide');
    }
    hideHomo() {
        this.visible = true;
        this.template.querySelector('.divHelp1').classList.add('slds-hide');
    }
    onclickPhone() {
        this.visible = true;
        this.template.querySelector('.divHelp2').classList.remove('slds-hide');
    }
    hidePhone() {
        this.visible = true;
        this.template.querySelector('.divHelp2').classList.add('slds-hide');
    }
    onclickCurp() {
        this.visible = true;
        this.template.querySelector('.divHelp3').classList.remove('slds-hide');
    }
    hideCurp() {
        this.visible = true;
        this.template.querySelector('.divHelp3').classList.add('slds-hide');
    }

    showToastMessage(sTitle, sMessage, sVariant) {
        const oEvt = new ShowToastEvent({
            title: sTitle,
            message: sMessage,
            variant: sVariant
        });
        this.dispatchEvent(oEvt);
    }
    onEstadoSelection(event) {
        this.lugarNac = event.detail.selectedValue;
        if (this.lugarNac == null) {
            this.lugarNac = "";
        }
        this.getState();
    }
    checkField() {
        if (this.nombreClient === '' || this.apellidoPat === '' || this.apellidoMaterno === '') {
            this.button = true;
        } else if (this.sexo === '' || this.sexo === '' || this.fechaNac === '') {
            this.button = true;
        } else if (this.lugarNac === '' || this.rfcClient === '' || this.telefonoMov === '') {
            this.button = true;
        } else if (this.curpClient === '' || this.fechaNac === null) {
            this.button = true;
        } else {
            this.button = false;
        }
    }
}