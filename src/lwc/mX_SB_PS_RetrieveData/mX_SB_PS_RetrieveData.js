import {LightningElement,track,wire,api} from 'lwc';
import {ShowToastEvent } from 'lightning/platformShowToastEvent';
import getServiceInfo   from '@salesforce/apex/MX_SB_PS_RetrieveData.obtieneplanes';
import getQuoteLineItem from '@salesforce/apex/MX_SB_PS_RetrieveData.obtieneQLI';
import getLapsos   from '@salesforce/apex/MX_SB_PS_RetrieveData.getLapsos';
const columns = [
    { label: 'modulo1', fieldName: 'modulo1' },
    { label: 'modulo2', fieldName: 'modulo2'  },
    { label: 'modulo3', fieldName: 'modulo3'  },
    { label: 'modulo4', fieldName: 'modulo4' }
];
export default class MX_SB_PS_RetrieveData extends LightningElement {
    @api recordId;
    @track data = [];
    @track error ;
    @track columns = columns;
    @track showResumen=true;
    @track value = [];
    @track planTabs=[];
    @track lapsosOp=[
        {'label':'MENSUAL $','value':'M'},
        {'label':'TRIMESTRAL $','value':'T'},
        {'label':'SEMESTRAL $','value':'S'},
        {'label':'ANUAL $','value':'A'},
    ];
    @track payments=[
        {'first':0,'total':0,'subsecuents':11},
        {'first':0,'total':0,'subsecuents':3},
        {'first':0,'total':0,'subsecuents':1},
        {'first':0,'total':0,'subsecuents':0},
    ];
    @track polizaOptions=[];
    @track isLoaded=false;
    @track actPlanTab='';
    @track selectedValue='M';
    @track defaultSelected=[];
    @track coberturaSeguro={};
    @track quotedata={};
    @track vhssuma=[];
    @track dataCovers;
    @track servicetry={};
    @track servicevar=false;
    @track newvarfolio='';
    @track cambioplantab='Plan 30000';
    @track excnumber=0;
    areac ='0004';
    get options() {
        return this.lapsosOp;
    }
    @wire(getQuoteLineItem,{oppid:'$recordId'})
    wire2({data,error}){
        if(data){
            if (this.excnumber===0){
                data='';
            }
            this.coberturaSeguro.folioCot=data.split(',')[0];
            this.newvarfolio=data.split(',')[0];
        }
    }
    @wire(getServiceInfo,{areacode:'$areac'})
    wire1({data,error}) {
        if(data) {
            this.selectedValue='M';
            this.coberturaSeguro.formaPago='MENSUAL';
            this.defaultSelected.push(0);
            this.data = data;
            var cols=JSON.parse(data);
            this.planTabs=[];
            for(var ite=0;ite<cols.iCatalogItem.planParticularData[0].particularData.length;ite++) {
                this.getObjects(cols.iCatalogItem.planParticularData[0].particularData[ite]);
            }
            this.planTabs.sort(function(a, b){return a-b});
            for (let i2=0;i2<this.planTabs.length;i2++) {
                this.planTabs[i2]='Plan '+this.planTabs[i2];
            }
            this.isLoaded=true;
            var plantabdata =JSON.parse(JSON.stringify(this.planTabs));
            this.actPlanTab=plantabdata[plantabdata.length-1];
            this.getLapsosOp(true);
        } else if (error) {
            this.isLoaded=true;
            const event = new ShowToastEvent({
                title: 'Error',
                variant : 'Error',
                message: 'Ocurrio un error, favor de recargar la pagina.',
            });
            this.dispatchEvent(event);
            throw new Error (error);
        }
    }
    getObjects(aiterar) {
        var  lpp= aiterar.transformer.length;
        var intloop=0;
        var intloop2=0;
        switch (aiterar.aliasCriterion) {
            case 'VHSEDAD' :
                break;
            case 'VHSTEMP' :
                break;
            case 'VHSSUMA' :
                for (intloop ; intloop<lpp;intloop++) {
                    this.planTabs.push(aiterar.transformer[intloop].catalogItemBase[0].name);
                    this.vhssuma[intloop]={id:aiterar.transformer[intloop].catalogItemBase[0].id,name:aiterar.transformer[intloop].catalogItemBase[0].name};
                }
               break;
            case 'VHSPLAN' :
                var listint = [];
                for (intloop ; intloop<lpp;intloop++) {
                    listint.push(aiterar.transformer[intloop].catalogItemBase[0].id);
                }
                listint.sort();
                for(intloop =0 ; intloop<lpp;intloop++) {
                    for(intloop2=0 ; intloop2<lpp;intloop2++) {
                        if(aiterar.transformer[intloop2].catalogItemBase[0].id===listint[intloop]) {
                            this.polizaOptions.push(aiterar.transformer[intloop2].catalogItemBase[0]);
                        }
                    }
                }
                this.quotedata.planid=listint[0];
                break;
            default :
                break;
        }
    }
    getLapsosOp(changefp) {
        this.excnumber++;
        if(this.excnumber>2){
            this.executegetlapsos();
        }
    }
    executegetlapsos(){
        var maplapsos = new Map();
        maplapsos.set('MENSUAL',0);
        maplapsos.set('TRIMESTRAL',1);
        maplapsos.set('SEMESTRAL',2);
        maplapsos.set('ANUAL',3);
        this.isLoaded=false;
        var datosnuevos = JSON.parse(JSON.stringify(this.vhssuma));
        var datosnuevosquote = JSON.parse(JSON.stringify(this.quotedata));
        var strplan=this.cambioplantab;
        var cmpr=strplan.substring(5,strplan.length);
        for (var iv=0; iv<datosnuevos.length;iv++){
            if (datosnuevos[iv].name===cmpr){
                datosnuevosquote.vhssuma=datosnuevos[iv];
            }
        }
        var mapGetLapsos = {"planselected":datosnuevosquote.planid,
        "formapago":this.coberturaSeguro.formaPago,
        "intval":this.excnumber,
        "oppid":this.recordId,
        "nombreplan":"Individual"}
        getLapsos({"mapdata1":mapGetLapsos, "mapdata":datosnuevosquote.vhssuma}).then(result=>{
            this.servicevar=true;
            this.lapsosOp=[];
            var arrayobj=JSON.parse(result).data;
            var covers = arrayobj.insuredList[0].coverages;
            this.dataCovers = arrayobj;
            for(let iter1=0;iter1<covers.length;iter1++) {
                switch(covers[iter1].catalogItemBase.id){
                    case 'AGHO' :
                        this.coberturaSeguro.agf=parseInt(covers[iter1].premium.amount);
                    break;
                    default :
                    break;
                }
            }
            var strplan=this.actPlanTab;
            this.coberturaSeguro.sumaAseg=parseInt(strplan.substring(5,strplan.length));
            this.coberturaSeguro.dias5=this.coberturaSeguro.sumaAseg;
            this.coberturaSeguro.dias35=this.coberturaSeguro.sumaAseg*2;
            this.coberturaSeguro.dias35mas=this.coberturaSeguro.sumaAseg*3;
            this.coberturaSeguro.agf=this.coberturaSeguro.sumaAseg;
            for(let iter=0;iter<arrayobj.rateQuote.length;iter++) {
                this.coberturaSeguro.idcotiza=arrayobj.id;
                this.coberturaSeguro.folioCot=arrayobj.numbr;
                let number = arrayobj.rateQuote[iter].firstPayment.amount;
                switch (arrayobj.rateQuote[iter].paymentWay.name) {
                    case 'MENSUAL' :
                        this.lapsosOp[0]={'label':arrayobj.rateQuote[iter].paymentWay.name+' $ '+number.toLocaleString("en-US"),
                        'value':arrayobj.rateQuote[iter].paymentWay.id,checked:true,
                        'amount':arrayobj.rateQuote[iter].firstPayment.amount};
                        this.payments[0]={'lapso':arrayobj.rateQuote[iter].paymentWay.name,
                        'first':arrayobj.rateQuote[iter].firstPayment.amount,
                        'total':arrayobj.rateQuote[iter].totalPaymentWithTax.amount,
                        'subsecuents':arrayobj.rateQuote[iter].subsequentPaymentsNumber};
                        break;
                    case 'TRIMESTRAL' :
                        this.lapsosOp[1]={'label':arrayobj.rateQuote[iter].paymentWay.name+' $ '+number.toLocaleString("en-US"),
                        'value':arrayobj.rateQuote[iter].paymentWay.id,checked:false,
                        'amount':arrayobj.rateQuote[iter].firstPayment.amount};
                        this.payments[1]={'lapso':arrayobj.rateQuote[iter].paymentWay.name,
                        'first':arrayobj.rateQuote[iter].firstPayment.amount,
                        'total':arrayobj.rateQuote[iter].totalPaymentWithTax.amount,
                        'subsecuents':arrayobj.rateQuote[iter].subsequentPaymentsNumber};
                        break;
                    case 'SEMESTRAL' :
                        this.lapsosOp[2]={'label':arrayobj.rateQuote[iter].paymentWay.name+' $ '+number.toLocaleString("en-US"),
                        'value':arrayobj.rateQuote[iter].paymentWay.id,checked:false,
                        'amount':arrayobj.rateQuote[iter].firstPayment.amount};
                        this.payments[2]={'lapso':arrayobj.rateQuote[iter].paymentWay.name,
                        'first':arrayobj.rateQuote[iter].firstPayment.amount,
                        'total':arrayobj.rateQuote[iter].totalPaymentWithTax.amount,
                        'subsecuents':arrayobj.rateQuote[iter].subsequentPaymentsNumber};
                        break;
                    case 'ANUAL' :
                        this.lapsosOp[3]={'label':arrayobj.rateQuote[iter].paymentWay.name+' $ '+number.toLocaleString("en-US"),
                        'value':arrayobj.rateQuote[iter].paymentWay.id,checked:false,
                        'amount':arrayobj.rateQuote[iter].firstPayment.amount};
                        this.payments[3]={'lapso':arrayobj.rateQuote[iter].paymentWay.name,
                        'first':arrayobj.rateQuote[iter].firstPayment.amount,
                        'total':arrayobj.rateQuote[iter].totalPaymentWithTax.amount,
                        'subsecuents':arrayobj.rateQuote[iter].subsequentPaymentsNumber};
                        break;
                    default :
                        break;
                }
            }
            var lapsoactual= maplapsos.get(this.coberturaSeguro.formaPago);
            var parsepays=JSON.parse(JSON.stringify(this.payments));
            this.coberturaSeguro.primerPago=parsepays[lapsoactual].first;
            this.coberturaSeguro.totalAPagar=parsepays[lapsoactual].total;
            this.coberturaSeguro.pagosFaltantes=parsepays[lapsoactual].subsecuents;
            this.isLoaded=true;
            this.actPlanTab=this.cambioplantab;
            this.actualizadatos();
        }).catch(error=>{
            this.isLoaded=true;
            const event = new ShowToastEvent({
                title: 'Error de conexión',
                variant : 'Error',
                message: 'Ocurrio un error en la consulta de planes, favor de recargar la pagina.',
            });
            this.dispatchEvent(event);
        });
    }
    tabselect(evt) {
        this.cambioplantab=evt.target.value;
        this.actPlanTab=evt.target.value;
        this.coberturaSeguro.agf=0;
        this.getLapsosOp(true);
    }
    get keyname(){
        for(let iter=0;iter<this.lapsosOp.length;iter++) {
            this.lapsosOp[iter]=this.actPlanTab+this.lapsosOp[iter];
        }
    }
    renderedCallback() {
        if(this.defaultSelected[0]===0){
            this.defaultSelected[0]=1;
            this.defaultselection(true);
        }
    }
    handleradiochange(event){
        var lapsoId = new Map();
        lapsoId.set('M',0);
        lapsoId.set('T',1);
        lapsoId.set('S',2);
        lapsoId.set('A',3);
        this.selectedValue = event.target.value;
        var parsepay =JSON.parse(JSON.stringify(this.payments));
        this.coberturaSeguro.formaPago=parsepay[lapsoId.get(this.selectedValue)].lapso;
        this.getLapsosOp(false);
    }
    changetipopoliza(event){
        this.quotedata.planid=event.target.value;
        this.getLapsosOp(true);
    }
    defaultselection(esload){
        var constplan=this.planTabs[this.planTabs.length-1];
            this.template.querySelector('lightning-tabset').activeTabValue = constplan;
            var pckl2=this.template.querySelector('.tipopoliza');
            this.coberturaSeguro.plansel=pckl2.options[pckl2.selectedIndex].text;
            this.quotedata.planid=pckl2.options[pckl2.selectedIndex].value;
            this.polizaOptions.forEach((optionval, idx) =>{
                if(optionval.description==='Individual' && esload===true){
                    this.template.querySelector('.tipopoliza').value=optionval.id;
                    this.coberturaSeguro.plansel='Individual';
                }
            });
    }
    actualizadatos(){
        var datosnuevos = JSON.parse(JSON.stringify(this.vhssuma));
        var datosnuevosquote = JSON.parse(JSON.stringify(this.quotedata));
        var strplan=this.cambioplantab;
        var cmpr=strplan.substring(5,strplan.length);
        for (var iv=0; iv<datosnuevos.length;iv++){
            if (datosnuevos[iv].name===cmpr){
                datosnuevosquote.vhssuma=datosnuevos[iv];
            }
        }
        var pckl=this.template.querySelector('.tipopoliza');
        this.coberturaSeguro.plansel=pckl.options[pckl.selectedIndex].text;
        this.quotedata.planid=pckl.options[pckl.selectedIndex].value;
        var rdata = this.coberturaSeguro;
        var objetos ={};
        objetos['Quote']={'Quote':{ 'MX_SB_PS_PagosSubsecuentes__c':rdata.pagosFaltantes,
                                    'MX_SB_VTS_TotalMonth__c':rdata.primerPago,
                                    'MX_SB_VTS_TotalAnual__c':rdata.totalAPagar
                                }
        };
        objetos['QuoteLineItem']={'QuoteLineItem':{
            'UnitPrice':rdata.primerPago,
            'Quantity':rdata.totalAPagar,
            'MX_SB_PS_Dias36omas__c':rdata.dias35mas,
            'MX_SB_PS_Dias3a5__c':rdata.dias5,
            'MX_SB_PS_Dias6a35__c':rdata.dias35,
            'MX_SB_PS_SumaAsegurada__c':rdata.sumaAseg,
            'MX_SB_VTS_Total_Gastos_Funerarios__c':rdata.agf,
            'MX_SB_VTS_FormaPAgo__c':rdata.formaPago,
            'MX_SB_VTS_Plan__c':this.actPlanTab,
            'MX_SB_VTS_Tipo_Plan__c':rdata.plansel,
            'MX_WB_subMarca__c':''+datosnuevosquote.vhssuma.name,
            'MX_WB_origen__c':''+datosnuevosquote.vhssuma.id,
            'MX_WB_placas__c ':''+datosnuevosquote.planid
        }
        };
        objetos['foliocot']={'foliocot':{'foliocot':rdata.folioCot
        }
        };
        const vobj= JSON.stringify(objetos);
        const textChangeEvent = new CustomEvent('txtchange',{
            detail : {vobj},
        });
        this.dispatchEvent(textChangeEvent);
    }
}