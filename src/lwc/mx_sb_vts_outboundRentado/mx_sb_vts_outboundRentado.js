import {
    LightningElement,
    api,
    wire,
    track
} from 'lwc';

import getSumVal from '@salesforce/apex/MX_SB_VTS_GetSumInsured_Ctrl.amountVal';

export default class Mx_sb_vts_outboundRentado extends LightningElement {
    @api wrapperParent = {}
    activeSections = ['Asistencias', 'Contenidos'];
    buttonClicked;
    titleMedica = 'Enfermera: $ 350.00 por turno de 8hrs\nMédico a domicilio: Hasta $ 150.00\nAmbulancia (aérea): Sin Costo\nAmbulancia (terrestre): Sin Costo';
    titleMascota = 'Vacunas: Hasta $ 200.00 \nEstética: Hasta $ 300.00\nHospedaje: Hasta $ 730.00 \nConsulta Veterinario : $ 250.00\n';
    extraTitle = 'Sacrificio por Accidente : $ 500.00\nAsistencia Funeraria : $ 2,000.00';
    titleIncendio = '1% de deducible y 10% de Coaseguro en perdida parcial\nSe eliminan deducible y coaseguro con perdida total';
    titleRobo = '5,000';
    medica = 'Cuentas con asesoría médica telefónica, médico a tu domicilio y traslados en ambulancia (terrestre o aérea)';
    casa = 'Cuentas con ayuda de especialistas por si en tu día a día necesitas: plomería, electricidad, cerrajería, albañilería, jardinería y más';
    mascotas = 'Tienes ayuda para tu perro o gato en servicios como: estética, hospedaje, vacunas, asesoría telefónica y más';
    incendio = 'Cubre daños ocasionados a las cosas que estén dentro de tu casa por: incendio o rayo';
    terremoto = 'Cubre daños ocasionados a las cosas que estén dentro de tu casa por: terremoto o erupción volcánica ';
    desastres = 'Cubre daños ocasionados a las cosas que estén dentro de tu casa por: inundación por lluvia, granizo, huracán entre otros.';
    danios = 'Cubrimos los daños ocasionados por ti, tu familia o hasta tus mascotas a bienes o personas dentro y fuera de tu hogar';
    cristales = 'Cubre la rotura accidental de vidrios o cristales interiores y exteriores como: ventanas, canceles, espejos y más';
    roboLabel = '';
    robo = '- Protege objetos dentro de tu hogar como: Televisiones, LapTops, computadoras, celulares y más\n';
    roboVal = '- Protege objetos dentro de tu hogar como: joyas, obras de arte, artículos deportivos y más\n';
    roboDinero = '- Cubre el dinero en efectivo dentro del Hogar';
    lineaBlanca = 'Protege los daños de tu pantalla, tablet, equipo de audio, consola de videojuego, centro de lavado, refrigerador, cafetera, entre otros';
    daniosHogar = 'Te cubrimos los daños que ocasiones en el lugar que rentas';

    @api cp;
    @track values = [];
    @track value = '';
    @track valorCincuenta = 0.50;
    @track valorVeinticinco = 0.25;
    @track valorQuince = 0.15;
    @track valorCien = 0.100;
    @track optIncendio = 0;
    @track optTerremoto = 0;
    @track optDisaster = 0;
    @track optCivil = 0;
    @track optCristal = 0;
    @track optRoboMen = 0;
    @track optRoboObj = 0;
    @track optLinea = 0;
    @track optDanios = 0;

    renderedCallback() {
        this.template.querySelector('[data-id="medica"]').title = this.titleMedica;
        this.template.querySelector('[data-id="mascotas"]').title = this.titleMascota + this.extraTitle;
        this.template.querySelector('[data-id="incendio"]').title = this.titleIncendio;
        this.template.querySelector('[data-id="desastresAgua"]').title = this.titleIncendio;
        this.template.querySelector('[data-id="terremoto"]').title = this.titleIncendio;
        this.template.querySelector('[data-id="robo"]').title = this.titleRobo;
        this.roboLabel = this.robo + this.roboVal + this.roboDinero;
    }
    @wire(getSumVal)
    responseJson({
        error,
        data
    }) {
        if (data) {
            const testVal = data;
            let objValues = testVal['Rentado'];
            let formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
                minimumFractionDigits: 0,
            });
            for (let keySrch in objValues) {
                this.values = [...this.values, {
                    label: formatter.format(parseInt(objValues[keySrch].replace(/,/g, ''))),
                    value: parseInt(objValues[keySrch].replace(/,/g, ''))
                }];
            }
        } else if (error) {
            this.error = error;
        }
    }

    get options() {
        return this.values;
    }

    handleChange(event) {
        this.value = parseInt(event.detail.value);
        this.calculateSum();
    }
    handleClick() {}

    calculateSum(event) {
        const valueSelected = this.value;
        let parseMoney = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 0,
        });
        this.optIncendio = parseMoney.format(parseInt(valueSelected * this.valorCincuenta));
        this.optTerremoto = parseMoney.format(parseInt(valueSelected * this.valorCincuenta));
        this.optDisaster = parseMoney.format(parseInt(valueSelected * this.valorCincuenta));
        this.optCivil = parseMoney.format(parseInt(valueSelected * this.valorCien));
        this.optCristal = parseMoney.format(parseInt(valueSelected * this.valorCien));
        this.optRoboMen = parseMoney.format(parseInt(valueSelected * this.valorVeinticinco));
        this.optRoboObj = parseMoney.format(parseInt(valueSelected * this.valorQuince));
        this.optLinea = parseMoney.format(parseInt(valueSelected * this.valorQuince));
        this.optDanios = parseMoney.format(parseInt(valueSelected * this.valorCincuenta));
    }
    handledBackAction() {
        let upWrapStage = Object.assign({}, this.wrapperContext, {
            etapa: "newCot"
        });
        this.wrapperContext = upWrapStage;
        this.backAction();
    }
    backAction() {
        const jsonDetail = {cotizObject:this.wrapperContext, zipCode : this.cp};
        const backAction = new CustomEvent("backaction", {
            detail: jsonDetail,
        });
        this.dispatchEvent(backAction);
    }

    handledNextAction(event) {
        let upWrapStage = Object.assign({}, this.wrapperContext, {
            etapa: "emitida"
        });
        this.wrapperContext = upWrapStage;
        this.nextAction();
    }
    nextAction() {
        const jsonDetail = {cotizObject:this.wrapperContext, zipCode : this.cp};
        const nextAction = new CustomEvent("nextaction", {
            detail: jsonDetail,
        });
        this.dispatchEvent(nextAction);
    }

    tapButtonPets() {
        this.buttonClicked = !this.buttonClicked;
        if (this.buttonClicked) {
            this.selectAction();
        } else {
            this.noAction();
        }
    }
    noAction() {
        this.template.querySelector('[data-id="petButton"]').classList.remove('class1');
        this.template.querySelector('[data-id="mascotas"]').classList.remove('class1');
    }
    selectAction() {
        this.template.querySelector('[data-id="petButton"]').classList.add('class1');
        this.template.querySelector('[data-id="mascotas"]').classList.add('class1');
    }

    tapButtonCristal() {
        this.buttonClicked = !this.buttonClicked;
        if (this.buttonClicked) {
            this.selectActionCristal();
        } else {
            this.noActionCristal();
        }
    }
    noActionCristal() {
        this.template.querySelector('[data-id="cristalButton"]').classList.remove('class1');
        this.template.querySelector('[data-id="cristales"]').classList.remove('class1');
    }
    selectActionCristal() {
        this.template.querySelector('[data-id="cristalButton"]').classList.add('class1');
        this.template.querySelector('[data-id="cristales"]').classList.add('class1');
    }

    tapButtonRobo(event) {
        this.buttonClicked = !this.buttonClicked;
        if (this.buttonClicked) {
            this.selectActionRobo();
        } else {
            this.noActionRobo();
        }
    }
    noActionRobo() {
        this.template.querySelector('[data-id="roboButton"]').classList.remove('class1');
        this.template.querySelector('[data-id="robo"]').classList.remove('class1');
    }
    selectActionRobo() {
        this.template.querySelector('[data-id="roboButton"]').classList.add('class1');
        this.template.querySelector('[data-id="robo"]').classList.add('class1');
    }

    tapButtonLineaB() {
        this.buttonClicked = !this.buttonClicked;
        if (this.buttonClicked) {
            this.selectActionLinea();
        } else {
            this.noActionLinea();
        }
    }
    noActionLinea() {
        this.template.querySelector('[data-id="lineaButton"]').classList.remove('class1');
        this.template.querySelector('[data-id="lineaBlanca"]').classList.remove('class1');
    }
    selectActionLinea() {
        this.template.querySelector('[data-id="lineaButton"]').classList.add('class1');
        this.template.querySelector('[data-id="lineaBlanca"]').classList.add('class1');
    }

    tapButtonDanios() {
        this.buttonClicked = !this.buttonClicked;
        if (this.buttonClicked) {
            this.selectActionDanios();
        } else {
            this.noActionDanios();
        }
    }
    noActionDanios() {
        this.template.querySelector('[data-id="danioButton"]').classList.remove('class1');
        this.template.querySelector('[data-id="danioHogar"]').classList.remove('class1');
    }
    selectActionDanios() {
        this.template.querySelector('[data-id="danioButton"]').classList.add('class1');
        this.template.querySelector('[data-id="danioHogar"]').classList.add('class1');
    }

    onclickCasa() {
        this.visible = true;
        this.template.querySelector('[data-id="helpCasa"]').classList.remove('slds-hide');
    }
    hideCasa() {
        this.visible = true;
        this.template.querySelector('[data-id="helpCasa"]').classList.add('slds-hide');
    }
    onclickMedica() {
        this.visible = true;
        this.template.querySelector('[data-id="helpMedica"]').classList.remove('slds-hide');
    }
    hideMedica() {
        this.visible = true;
        this.template.querySelector('[data-id="helpMedica"]').classList.add('slds-hide');
    }
    onclickMascotas() {
        this.visible = true;
        this.template.querySelector('[data-id="helpMascotas"]').classList.remove('slds-hide');
    }
    hideMascotas() {
        this.visible = true;
        this.template.querySelector('[data-id="helpMascotas"]').classList.add('slds-hide');
    }
    onclickIncendio() {
        this.visible = true;
        this.template.querySelector('[data-id="helpIncendio"]').classList.remove('slds-hide');
    }
    hideIncendio() {
        this.visible = true;
        this.template.querySelector('[data-id="helpIncendio"]').classList.add('slds-hide');
    }
    onclickTerremoto() {
        this.visible = true;
        this.template.querySelector('[data-id="helpTerremoto"]').classList.remove('slds-hide');
    }
    hideTerremoto() {
        this.visible = true;
        this.template.querySelector('[data-id="helpTerremoto"]').classList.add('slds-hide');
    }
    onclickDesastres() {
        this.visible = true;
        this.template.querySelector('[data-id="helpDesastres"]').classList.remove('slds-hide');
    }
    hideDesastres() {
        this.visible = true;
        this.template.querySelector('[data-id="helpDesastres"]').classList.add('slds-hide');
    }
    onclickDanios() {
        this.visible = true;
        this.template.querySelector('[data-id="helpDanios"]').classList.remove('slds-hide');
    }
    hideDanios() {
        this.visible = true;
        this.template.querySelector('[data-id="helpDanios"]').classList.add('slds-hide');
    }
    onclickCristales() {
        this.visible = true;
        this.template.querySelector('[data-id="helpCristales"]').classList.remove('slds-hide');
    }
    hideCristales() {
        this.visible = true;
        this.template.querySelector('[data-id="helpCristales"]').classList.add('slds-hide');
    }
    onclickRobo() {
        this.visible = true;
        this.template.querySelector('[data-id="helpRobo"]').classList.remove('slds-hide');
    }
    hideRobo() {
        this.visible = true;
        this.template.querySelector('[data-id="helpRobo"]').classList.add('slds-hide');
    }
    onclickBlanca() {
        this.visible = true;
        this.template.querySelector('[data-id="helpBlanca"]').classList.remove('slds-hide');
    }
    hideBlanca() {
        this.visible = true;
        this.template.querySelector('[data-id="helpBlanca"]').classList.add('slds-hide');
    }
    onclickDaniosHogar() {
        this.visible = true;
        this.template.querySelector('[data-id="helpDaniosHogar"]').classList.remove('slds-hide');
    }
    hideDaniosHogar() {
        this.visible = true;
        this.template.querySelector('[data-id="helpDaniosHogar"]').classList.add('slds-hide');
    }
}