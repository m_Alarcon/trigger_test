import { LightningElement, track, wire, api} from 'lwc';
import initialData from '@salesforce/apex/MX_SB_SAC_IdentificarCliente.initialData';
import Id from '@salesforce/user/Id';
import MX_SB_1500_CaseComments from '@salesforce/label/c.MX_SB_1500_CaseComments';
import { handleUndefinedValues } from 'c/mx_sb_sac_utils';
import updateCaseExecutive from '@salesforce/apex/MX_SB_SAC_1500_ExecutiveAuth.updateCaseExecutive';

export default class Mx_sb_1500_executiveAuth extends LightningElement { // NOSONAR
    @track caseData = []
    @track managerFlag = false;
    @track coworkerFlag = false;
    @track startValidation = true;
    @track commentsFlag = false;
    @track agentData = [];
    @track showSearchData = false;
    @api recordId;
    @track clientName = '';
    @track shouldShowComments = false;
    userId = Id;
    customText = '';
    userType = '#1500';
    disable1 = true;
    disable2 = true;

    constructor() {
        super();
        this.template.addEventListener('comments1500', this.handleCustomEvent.bind(this));
    }

    @wire(initialData, { recId : '$recordId', userId: '$userId' })
    wireData({error, data}) {
        if(data) {
            for(let key in data) {
                if(key === 'case') {
                    this.caseData = data[key];
                    this.customText = data[key].MX_SB_1500_ExecutiveGivenName__c + ' ' + data[key].MX_SB_1500_ExecutiveSureName__c
                                    + ' ' + MX_SB_1500_CaseComments;
                    this.clientName = handleUndefinedValues(data[key].MX_SB_SAC_Nombre__c)
                                    + ' ' + handleUndefinedValues(data[key].MX_SB_SAC_ApellidoPaternoCliente__c)
                                    + ' ' + handleUndefinedValues(data[key].MX_SB_SAC_ApellidoMaternoCliente__c);
                }
                if(key === 'user') {
                    this.agentData = data[key];
                }
            }
        } else if(error) {
            throw new Error('failure on retrieveExecutiveData')
        }
    }

    get options() {
        return [
            {
                label: 'Si',
                value: '1'
            },
            {
                label: 'No',
                value: '0'
            }
        ]
    }

    handleOnChange(event) {
        const name = event.currentTarget.name;
        const value = parseInt(event.currentTarget.value);
        switch(name) {
            case 'valid-pu-options':
                this.handleOnChangeExecutive(value);
                break;
            case 'valid-manger-options':
                this.handleOnChangeManager(value);
                break;
            case 'valid-coworker-options':
                this.handleOnChangeCoworker(value);
                break;
            default:
                break;
        }
    }

    handleOnChangeExecutive(value) {
        if(value === 1) {
            this.handleBusinessFlow(true);
        } else if(value === 0) {
            this.managerFlag = true;
            this.disable1 = false;
        }
    }

    handleOnChangeManager(value) {
        if(value === 1) {
            this.handleBusinessFlow(true);
        } else if(value === 0) {
            this.coworkerFlag = true;
            this.disable2 = false;
        }
    }

    handleOnChangeCoworker(value) {
        if(value === 1) {
            this.handleBusinessFlow();
        } else if(value === 0) {
            this.handleBusinessFlow(false);
            this.shouldShowComments = true;
        }
    }

    handleCustomEvent(event) {
        if(this.shouldShowComments === true) {
            this.commentsFlag = event.detail;
            this.showSearchData = false;
        }
    }

    backToValidation() {
        this.managerFlag = false;
        this.showSearchData = false;
        this.startValidation = true;
        this.coworkerFlag = false;
        this.commentsFlag = false;
        this.disable1 = true;
        this.disable2 = true;
    }

    handleBusinessFlow(flag) {
        let switchStatement = 'default';
        if(this.caseData.Reason === 'Transferencia' || this.caseData.Reason === 'Consultas') {
            switchStatement = this.caseData.Reason;
        }
        switch(switchStatement) {
            case 'Transferencia':
                if(flag === true) {
                    this.finishProcess();
                } else {
                    this.startValidation = false;
                    this.commentsFlag = true;
                }
                break;
            case 'Consultas':
                if(flag === true) {
                    this.finishProcess();
                } else {
                    this.commentsFlag = true;
                    this.startValidation = false; }
                break;
            case 'default':
                this.continueProcess();
                break;
        }
    }

    backToSearch() {
        this.showSearchData = true;
        this.commentsFlag = false
    }

    finishProcess() {
        updateCaseExecutive({idCase: this.caseData.Id, isAuthenticated: true})
        .then(result => {
            eval("$A.get('e.force:refreshView').fire();");
        }).catch(error => {
            throw new Error(error);
        });
    }

    continueProcess() {
        if(this.caseData.MX_SB_SAC_EsCliente__c) {
            this.showSearchData = true;
            this.startValidation = false;
        } else {
            this.startValidation = false;
            this.commentsFlag = true;
        }
    }
}