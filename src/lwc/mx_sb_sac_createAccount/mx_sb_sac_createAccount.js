import { LightningElement, api, track, wire } from 'lwc';
import { getSalesProcess, finishQuoting } from 'c/mx_sb_sac_utils';
import searchAccountOrContract from '@salesforce/apex/MX_SB_SAC_IdentificarCliente.searchContractOrAccount';
import searchProductData from '@salesforce/apex/MX_SB_SAC_IdentificarCliente.searchProductData';
import getRecordTypeByDevName from '@salesforce/apex/MX_SB_SAC_CreateAccount_Ctlr.getRecordTypeByDevName';
import upsertAccount from '@salesforce/apex/MX_SB_SAC_CreateAccount_Ctlr.upsertAccount';

export default class Mx_sb_sac_createAccount extends LightningElement {
    @api options;
    @api caseData;
    @track salesPath = '';
    @track accountId = '';
    @track productData = [];
    recordTypeId = '';
    showSalesPath = false;
    BPYP_RTDN = 'BPyP_tre_Cliente';
    searchKey = '';

    @wire(getRecordTypeByDevName, { fields: 'Id', devName: 'PersonAccount', sOType: 'Account'})
    wiredData({error, data}) {
        if(data) {
            this.recordTypeId = data;
        } else if(error) {
            throw new Error(error);
        }
    }

    selectSalesPath(event) {
        const productName = event.currentTarget.value;
        if(productName !== '') {
            this.salesPath = getSalesProcess(productName);
            this.showSalesPath = true;
            this.getProduct(productName);
        } else {
            this.showSalesPath = false;
        }
    }


    getAccountByEmail(event) {
        this.accountId = '';
        clearTimeout(this.delayTimeout);
        const email = event.currentTarget.value;
        const regex = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/;
        this.delayTimeout = setTimeout(() => {
            this.searchKey = email;
            if(this.searchKey !== '' && this.searchKey.match(regex)) {
                searchAccountOrContract({searchParam: this.searchKey}).then(result => {
                    if(result) {
                        for(let keyVal in result) {
                            if(keyVal === 'contract') {
                                result[keyVal].forEach(element => {
                                    this.accountId = element.AccountId
                                });
                            } else if(keyVal === 'account') {
                                result[keyVal].forEach(element => {
                                    this.accountId = element.Id;
                                })
                            }
                        }
                    }
                });
            }
        }, 500);
    }

    getProduct(productName) {
        searchProductData({searchParam: productName})
        .then(result => {
            if(result) {
                this.productData = result;
            }
        }).catch(error => {
            throw new Error(error);
        });
    }

    handleSave() {
        const data = { 'sObjectType' : 'Account' };
        const rtbpyp = ['MX_BPP_PersonAcc_Client', 'MX_BPP_PersonAcc_NoClient', 'BPyP_tre_Cliente', 'BPyP_tre_noCliente'];
        if(this.accountId !== '') {
            data.Id = this.accountId;
        } else if(rtbpyp.indexOf(accData.devName) === -1) {
            data.RecordTypeId = this.recordTypeId;
        }
        data.FirstName = this.template.querySelector("[data-field='FirstName']").value;
        data.LastName = this.template.querySelector("[data-field='LastName']").value;
        data.Apellido_materno__pc = this.template.querySelector("[data-field='Apellido_Materno__pc']").value;
        data.PersonEmail = this.template.querySelector("[data-field='PersonEmail']").value;
        data.Phone = this.template.querySelector("[data-field='Phone']").value;

        upsertAccount({accData: data, rtName: 'PersonAccount'})
        .then(result => {
            if(result) {
                const acc = result;
                const extraData = new Map();
                extraData.set('accountId', acc.Id);
                extraData.set('email', acc.PersonEmail);
                finishQuoting(this.caseData, this.productData, this.salesPath, extraData)
            }
        }).catch(error => {
            throw new Error(error);
        });
    }
}