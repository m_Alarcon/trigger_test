import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getInsuredAmount from '@salesforce/apex/MX_SB_VTS_GetSumInsured_Ctrl.getAmounts'

export default class Mx_sb_vts_insuredAmount extends LightningElement {
    @api isModalOpen = false;
    @api insuredAmount;
    @track disableOpt = true;
    @track minVal = '$600,000';
    @track maxVal = '$20,000,000';
    @api typeBuild
    @track minValue = 0;
    @track maxValue = 0;
    @api montosAsegurados;
    @track isLoading = false;
    @track proxyFail = false;

    openModal() {
        this.isModalOpen = true;
    }
    handleAmount(event) {
        let propierty = this.typeBuild;
        if(propierty === 'Propio') {
            this.minValue = 600000;
            this.maxValue = 20000000;
            this.insuredAmount = event.target.value;
        } else if (propierty === 'Rentado') {
            this.minValue = 150000;
            this.maxValue = 10000000;
            this.insuredAmount = event.target.value;
        }
    }
    returnOpt() {
        let sendAmount = {isNext: false};
        const closedialog = new CustomEvent("closedialog", {
            detail: sendAmount
        });
        this.dispatchEvent(closedialog)
    }
    closeModal() {
        this.isModalOpen = false;
    }
    getAmounts() {
        getInsuredAmount({insAmount: this.insuredAmount, propiedad: this.typeBuild}).then((data) => {
            if(String(data) === 'proxy') {
                this.showToastMessage('Web Service[InsuredAmount]', 'Unable to tunnel through proxy. Proxy returns "HTTP/1.1 503 Service Unavailable', 'warning');
                this.proxyFail = true;
            }  else {
                const test = data;
                let statusCD = String(test[0]);
                if(statusCD === 'error') {
                    this.showToastMessage('Web Service[InsuredAmount]', 'Ocurrio un error al consumir el servicio.', 'error');
                    this.proxyFail = true;
                } else {
                    this.montosAsegurados = data;
                    this.isLoading = false;
                    this.sendData();
                }
            }
        }).catch(error => {
            this.showToastMessage(error, 'Error al Obtener suma sugerida, intente nuevamente', 'error');
        });
    }
    submitDetails() {
        let sAsegurada = this.insuredAmount;
        let maxVal = this.maxValue;
        let minVal = this.minValue;
        if(sAsegurada <= maxVal && sAsegurada >= minVal) {
            this.isLoading = true;
            this.getAmounts();
        } else {
            this.showToastMessage('Ocurrio un error', 'La suma asegurada es incorrecta', 'error');
        }
    }
    sendData() {
        let sendAmount = {isNext: true, amount : this.montosAsegurados};
        const closedialog = new CustomEvent("closedialog", {
            detail: sendAmount
        });
        this.isModalOpen = false;
        this.dispatchEvent(closedialog)
    }
    get condition() {
        const propiedad = this.typeBuild;
        let showMin = false
        if(propiedad === 'Propio') {
            showMin = true;
        } else {
            this.maxVal = '$10,000,000';
            this.minVal = '$150,000';
        }
        return showMin;
    }
    showToastMessage(sTitle, sMessage, sVariant) {
        const oEvt = new ShowToastEvent({
            title: sTitle,
            message: sMessage,
            variant: sVariant
        });
        this.dispatchEvent(oEvt);
    }
}