import { LightningElement, api, track } from 'lwc';
export default class Mx_sb_vts_dir_contratante_vsd extends LightningElement {//NOSONAR
    @track modvisdircont = true;
    @track modvisenvpoli = true;
    @api objdatadc;

    handleParams(event) {
        this.objdatadc = event.detail.oparamdc;
        this.template.querySelector('c-mx_sb_vts_vsd_mod_env_poliza').onResetCmp();
    }

    handledNextAction() {
        let upWrapStage = Object.assign({}, this.wrapperContext, {
            etapa : "tarificada"
          });
        this.wrapperContext = upWrapStage;
        this.nextAction();
    }

    nextAction() {
        const nextAction = new CustomEvent("nextaction", {
            detail: this.wrapperContext,
        });
        this.dispatchEvent(nextAction);
    }
}