import { LightningElement, api, track } from 'lwc';
export default class Mx_sb_vts_vsd_mod_env_poliza extends LightningElement {//NOSONAR
    @track fldchksi = false;
    @track fldchkno = true;
    @track flddirenvpoliz = [];
    @api lwcparamdc;

    onHandleSiCHK(event) {
        this.fldchksi = event.target.checked;
        this.fldchkno = false;
        let objDataDC = JSON.parse(this.lwcparamdc);
        this.flddirenvpoliz = [];
        this.onCreateDirPol(objDataDC);
    }

    onHandleNoCHK(event) {
        this.fldchkno = event.target.checked;
        this.fldchksi = false;
        this.flddirenvpoliz = [];
    }

    onCreateDirPol(objDataDC) {
        let lblDirPropiedad = null;
        let sSpace = ', ';
        let sNoInt = (objDataDC.dc_nint === undefined ? '' : objDataDC.dc_nint);
        lblDirPropiedad = 'Dirección Contratante: ' +
                          objDataDC.dc_calle + sSpace +
                          objDataDC.dc_next + sSpace +
                          sNoInt + sSpace +
                          objDataDC.dc_collbl + sSpace +
                          objDataDC.dc_alcnum + sSpace +
                          objDataDC.dc_estado + sSpace +
                          objDataDC.dc_cp;
        this.flddirenvpoliz.push(
            { label: lblDirPropiedad, value: 'dircontratante' }
        );
    }

    @api
    onResetCmp() {
        this.flddirenvpoliz = [];
        this.fldchksi = false;
        this.fldchkno = true;
    }
}