import { LightningElement, api, track} from 'lwc';
import sendtoemail from '@salesforce/apex/MX_SB_VTS_CotizInitData_Ctrl.findQuoteEmail';
import sendingemail from '@salesforce/apex/MX_SB_VTS_CotizInitData_Ctrl.sendingEmail';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
const CSS_CLASS = 'modal-hidden';

export default class Mx_sb_vts_cotizSendCotEmail extends LightningElement { 
    @api showModalEmail = false;
    @api showModalTitle = '';
    @api clientName = '';
    @api clientEmail= '';
    @track hasClientName = false;
    @track hasClientEmail= false;
    @track noReadyToSend = true;
    @api cotId = '';
    @api isLoaded = false;

    connectedCallback() {
        this.handLoadSendEmail();
    }

    handleDialogClose() {
        const closedialog = new CustomEvent("closedialog", {
            detail: false
        });
        this.dispatchEvent(closedialog);
    }

    handleSlotTaglineChange() {
        const taglineEl = this.template.querySelector('p');
        taglineEl.classList.remove(CSS_CLASS);
    }

    handleSlotFooterChange() {
        const footerEl = this.template.querySelector('footer');
        footerEl.classList.remove(CSS_CLASS);
    }

    sendingEmail(event) {
        this.handleDialogClose();
    }

    handValidateName(event) {
        let valName = event.target.value.toString();
        this.clientName = valName;
        this.validateName(valName);
    }

    validateName(valName) {
        if(valName.length > 1) {
            this.hasClientName = true;
        } else {
            this.hasClientName = false;
        }
        this.validateButton();
    }

    handValidateEmail(event) {
        let valEmail = event.target.value.toString();
        this.clientEmail = valEmail;
        this.validateEmail(valEmail);
    }

    validateEmail(valEmail) {
        let isValid = this.emailIsValid(valEmail);
        if(isValid) {
            this.hasClientEmail = true;
        } else {
            this.hasClientEmail = false;
        }
        this.validateButton();
    }

    validateButton() {
        let nameOk = this.hasClientName;
        let emailOk = this.hasClientEmail;
        if(nameOk && emailOk) {
            this.noReadyToSend = false;
        } else {
            this.noReadyToSend = true;
        }
    }

    handsendingEmail(event) {
        this.isLoaded = false;
        sendingemail().then(result => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Envio exitoso',
                    message: 'La cotización se envio correctamente',
                    variant: 'success'
                })
            );
            this.isLoaded = true;
            this.handleDialogClose();
            }).catch(error => {
                this.dispatchEvent(
                new ShowToastEvent({
                    title: error.enhancedErrorType,
                    message: 'Algo fallo, por favor contacte a su administrador de sistema ',
                    variant: 'error'
                })
            );
            this.isLoaded = true;
        });
    }

    emailIsValid (email) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
            return (true);
        } else {
            return (false);
        }
    }

    handLoadSendEmail() {
        sendtoemail().then(result => {
            if(result.dataQuote && result.dataQuote.length > 0) {
                if(this.clientName === '') {
                    this.clientName = result.dataQuote.Account.FirstName+ ' '+ result.dataQuote.Account.LastName;
                    if(result.dataQuote.Account.Apellido_materno__pc){
                        this.clientName += (' '+result.dataQuote.Account.Apellido_materno__pc);
                    }
                }
                if(this.clientEmail === '') {
                    this.clientEmail = result.dataQuote.Email;
                }
                if(this.clientName) {
                    this.hasClientName = true;
                }
                if(this.clientEmail) {
                    this.hasClientEmail = true;
                }
            }
            this.validateButton();
            this.isLoaded = true;
        }).catch(error => {
            this.hasClientName = false;
            this.hasClientEmail = false;
            this.dispatchEvent(
                new ShowToastEvent({
                    title: error.enhancedErrorType,
                    message: 'No se pudo recuperar información del cliente',
                    variant: 'warning'
                })
            );
        });
    }
}