import { LightningElement, api, track} from 'lwc';
import validPoliz from '@salesforce/apex/MX_SB_VTS_CotizInitData_Ctrl.validatePoliz';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';

export default class Mx_sb_vts_validaPoliza extends LightningElement {
    @api isValidPol = false;
    @track polizNum = '';
    @api hasErrorPoliz = false;
    @api hasErrorValid = false;
    @api areDetailsVisible = false;
    @api recordId;

    handledValidPoliz() {
        this.areDetailsVisible = true;
        validPoliz({polizNum: this.polizNum, oppId : this.recordId}).then(result => {
            if(result.isExist) {
                this.areDetailsVisible = false;
            if(result.isValidPoliz) {
                let messageExist = result.policyNumber + ' \n';
                messageExist += 'Número de póliza: ' +result.message + ' \n';
                messageExist += 'Fecha inicio: '+ result.startDateContract + ' \n';
                messageExist += 'Fecha fin: '+ result.endDateContract + ' \n';
                messageExist += result.isValidPolizMsg;
                const evtSucc = new ShowToastEvent({
                    title: 'Poliza validada',
                    message: messageExist,
                    variant: 'success'
                });
                this.isValidPol = true;
                this.dispatchEvent(evtSucc);
            } else {
                let messageError = result.message + ' \n';
                messageError += result.isValidPolizMsg;
                const evt = new ShowToastEvent({
                    title: 'Error al validar la póliza',
                    message: messageError,
                    variant: 'error'
                });
                this.dispatchEvent(evt);
            }
        } else {
            let messageError = result.message + ' \n';
            this.areDetailsVisible = false;
            const evt = new ShowToastEvent({
                title: 'Error al validar la póliza',
                message: messageError,
                variant: 'error'
            });
            this.dispatchEvent(evt);
        }
        });
    }

    handleFormInputChange(event){
        this.polizNum = event.target.value;
    }
}