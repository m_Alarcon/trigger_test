import { LightningElement,track} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class Mx_sb_vts_personalizaProCasoComp_vsd extends LightningElement {//NOSONAR
    @track isVisCotP = true;
    @track cmbsumaseg = [];
    @track cmbdesempl = [];
    @track lstcoberturas = ['btnApoGastFun','btnMuerteAcci', 'btnAsistFunFam', 'btnAsistMed','btnMomenEsp','btnAntEnfGraves','btnMemVita',
        'btnApoDiaHospi','btnPriDiagCan','btnInvaTotPerm','btnDesempleo'];
    @track monApoGasFun = '$ ' + 0.00;
    @track monMuerAcc = '$ ' + 0.00;
    @track monAntEnfGrav = '$ ' + 0.00;
    @track monPrimDiagCan = '$ ' + 0.00;
    @track monInvTotPerm = '$ ' + 0.00;
    @track cmbSumAseDis = false;
    @track optSumAseDis = true;
    @track btnConsulDis = true;
    @track inpSumAseVal = '';
    @track toolmsgdata = '';

    get obtDataSumAseg() {
        this.cmbsumaseg = [
            {label: '$ 555,555.00', value: 'sumase_1'},
            {label: '$ 350,000.00', value: 'sumase_2'},
            {label: '$ 400,000.00', value: 'sumase_3'},
            {label: '$ 500,000.00', value: 'sumase_4'},
            {label: '$ 1,000,000.00', value: 'sumase_5'},
            {label: '$ 2,000,000.00', value: 'sumase_6'},
            {label: '$ 3,000,000.00', value: 'sumase_7'}
        ];
        return this.cmbsumaseg;
    }

    get obtDataDesemp() {
        this.cmbdesempl = [
            {label: '$ 500.00', value: 'desemp_1'},
            {label: '$ 1,000.00', value: 'desemp_2'},
            {label: '$ 3,000.00', value: 'desemp_3'}
        ];
        return this.cmbdesempl;
    }

    chkButtonAnother(event) {
        let chkAnother = event.target.checked;

        if(chkAnother){
            this.cmbSumAseDis = true;
            this.optSumAseDis = false;
            this.btnConsulDis = false;
        } else {
            this.cmbSumAseDis = false;
            this.optSumAseDis = true;
            this.btnConsulDis = true;
        }
    }

    clkButtonConsul(event) {
        this.calculateSumAse(this.inpSumAseVal);
    }

    clkButtonCov(event) {
        let sNameButton = event.currentTarget.dataset.name;
        let sNameButtonType = event.currentTarget.dataset.type;
        if(this.lstcoberturas.includes(sNameButton)) {
            if (sNameButtonType === 'optional') {
                this.changeSelButton(sNameButton);
            }
        }
    }

    clkMejoraPlan() {
        this.showToastMessage('Mejora tu plan', 'La suma asegurada aumentara un 5% cada año', 'info');
    }

    clkIconButtonCov(event) {
        let sNameToolIco = event.currentTarget.dataset.tooltip;
        this.toolmsgdata = this.searchMsgTootip(sNameToolIco);
        this.template.querySelector('[data-id="' + sNameToolIco + '"]').classList.remove('slds-hide');
    }

    mouseOverIconCov(event) {
        let sNameToolIco = event.currentTarget.dataset.tooltip;
        this.template.querySelector('[data-id="' + sNameToolIco + '"]').classList.add('slds-hide');
    }

    searchMsgTootip(sNameToolIco) {
        let strMsgTooltip = '';
        if(sNameToolIco === 'tolApoGastFun') {
            strMsgTooltip = 'Tu seres queridos recibiran un monto adicional para los ultimos gastos.';
        } else if(sNameToolIco === 'tolMuerteAcciIco') {
            strMsgTooltip = 'Este monto recibirán tus seres queridos en caso de que el fallecimiento haya sido por un accidente.';
        } else if(sNameToolIco === 'tolAsistFunFam') {
            strMsgTooltip = 'Te apoyamos con el servicio funerario y trámites legales en caso de que llegues a faltar por cualquier causa.'+
            'También aplica si tu cónyuge, hijos o padres fallece en algun accidente.';
        } else if(sNameToolIco === 'tolAsistMed') {
            strMsgTooltip = 'Para que cuides tu salud, te ofrecemos asesoria médica telefónica, '+
            'medico a domicilio o ambulancia via terrestre en caso de necesitarlos.';
        } else if(sNameToolIco === 'tolMomenEsp') {
            strMsgTooltip = 'Para celebrar contigo, te entregamos una cantidad adicional cuando te cases.';
        } else if(sNameToolIco === 'tolAntEnfGraves') {
            strMsgTooltip = 'En caso de enfermedad grave, definida en las condiciones generales, '+
            'te anticipamos el 20% de la suma asegurada, siempre y cuando hayan pasado 3 meses desde la contratación, o 6 meses en caso de párkinson.';
        } else if(sNameToolIco === 'tolMemVita') {
            strMsgTooltip = 'Disfruta del programa de bienestar lider a nivel mundial que te recompensa por llevar un estilo de vida saludable.';
        } else if(sNameToolIco === 'tolApoDiaHospi') {
            strMsgTooltip = 'Te rembolsamos este monto por cada dia de hospitalización a causa de accidente o enfermedad. '+
            'Aplica a partir del tercer día de hospitalización.';
        } else if(sNameToolIco === 'tolPriDiagCan') {
            strMsgTooltip = 'Te entregamos esta cantidad en caso de tener cáncer por primera vez, siempre y cuando se '+
            'diagnostique 3 meses después de contratar tu seguro.';
        } else if(sNameToolIco === 'tolInvaTotPerm') {
            strMsgTooltip = 'Si sufres algun accidente o enfermedad que te imposibilite trabajar de por vida, recibiras este monto para ayudarte con tus gastos.';
        }
        return strMsgTooltip;
    }

    changeSelButton(sNameButton) {
        let objClassList = this.template.querySelector('[data-name="' + sNameButton + '"]').classList;
        let isExistClass = false;
        for(let i=0; i < objClassList.length; i++) {
            if(objClassList[i] === 'class1') {
                isExistClass = true;
                break;
            }
        }
        if(isExistClass){
            this.template.querySelector('[data-name="' + sNameButton + '"]').classList.remove('class1');
        } else {
            this.template.querySelector('[data-name="' + sNameButton + '"]').classList.add('class1');
        }
    }

    changeSetInput(event) {
        let strNameField = event.target.name;
        if (strNameField === 'inputSumAse') {
            this.inpSumAseVal = event.target.value;
        }
    }

    changeSelSumAse(event) {
        let cmbDataSumAseLbl = event.target.options.find(opt => opt.value === event.detail.value).label;
        let valSumAse = parseFloat(cmbDataSumAseLbl.replace('$','').replace(',','').trim());
        this.calculateSumAse(valSumAse);
    }

    calculateSumAse(strSumAseParam) {
        this.monApoGasFun = '$ ' + this.formatMoney((strSumAseParam * .05));
        this.monMuerAcc = '$ ' + this.formatMoney((strSumAseParam * 1.00));
        this.monAntEnfGrav = '$ ' + this.formatMoney((strSumAseParam * .20));
        this.monPrimDiagCan = '$ ' + this.formatMoney((strSumAseParam * .30));
        this.monInvTotPerm = '$ ' + this.formatMoney((strSumAseParam * .50));
        this.template.querySelector("c-mx_sb_vts_vsd_cotizador-Pagos").calcTotal(strSumAseParam);
    }

    formatMoney = (amount) => {
        var formatter = new Intl.NumberFormat('en-US', {style: 'currency',
            currency: 'USD',
        });
        return formatter.format(amount);
    }

    showToastMessage(sTitle, sMessage, sVariant) {
        const oEvt = new ShowToastEvent({
            title: sTitle,
            message: sMessage,
            variant: sVariant,
            mode: 'sticky'
        });
        this.dispatchEvent(oEvt);
    }
}