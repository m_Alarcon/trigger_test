import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class Mx_sb_vts_infoBasicaVsd extends LightningElement {//NOSONAR
    @api wrapperParent = {}
    @track birthVal = '';

    getDateVal(event) {
        this.birthVal = event.target.value;
        const getValBith = this.birthVal;
        var currDay = new Date();
        var birtDate = new Date(getValBith);
        var setAgesBirth = currDay.getFullYear() - birtDate.getFullYear();
        if (setAgesBirth > 65 || setAgesBirth < 18) {
            this.showToast('ATENCIÓN', 'Fecha inválida', 'warning');
        }
        return setAgesBirth;
    }

    onClickMsgCurp() {
        this.visible = true;
        this.template.querySelector('.divMsgCurp').classList.remove('slds-hide');
    }

    onHideMsgCurp() {
        this.visible = true;
        this.template.querySelector('.divMsgCurp').classList.add('slds-hide');
    }

    handledNextAction() {
        let upWrapStage = Object.assign({}, this.wrapperContext, {
            etapa : "frecuencia"
          });
        this.wrapperContext = upWrapStage;
        this.nextAction();
    }

    nextAction() {
        const nextAction = new CustomEvent("nextaction", {
            detail: this.wrapperContext,
        });
        this.dispatchEvent(nextAction);
    }

    showToast(titleMsg, messageBody, typeMsg) {
        const oEvt = new ShowToastEvent({
            title: titleMsg,
            message: messageBody,
            variant: typeMsg
        });
        this.dispatchEvent(oEvt);
    }
}