import { LightningElement, api, track } from 'lwc';

export default class Mx_sb_vts_datosBeneficiariosVSD extends LightningElement {//NOSONAR
    @track relationshipType;
    @track benDomicilio = false;
    @api record;
    @api field;
    @track street = '';
    @track numExt = '';
    @track numInt = '';
    @track zipCode = '';
    @track neighborhood = '';
    @track alcaldia = '';
    @track state = '';
    @track city = '';

    value;
    label;
    connectedCallback() {
        this.value = this.record[this.field];
        this.label = this.field;
    }

    @api
    inputValue() {
        return { value : this.value, field: this.field };
    }
    get relationshipTypes() {
        return [
            { label: 'Esposo(a)', value: 'spouse' },
            { label: 'Unión libre', value: 'free' },
            { label: 'Hijo(a)', value: 'son' },
            { label: 'Padre', value: 'father' },
            { label: 'Madre', value: 'mother' },
            { label: 'Otro (hermano, amigo, etc.)', value: 'other' },
        ];
    }
    selectRelationship(event) {
        this.relationshipType = event.detail.value;
    }

    dataContratante(event) {
        this.benDomicilio = event.target.checked;
        if (this.benDomicilio) {
            this.street = 'Atzacoalco';
            this.numExt = '1129';
            this.numInt = 'F-601';
            this.zipCode = '07469';
            this.neighborhood = 'Constitución de la Republica';
            this.alcaldia = 'Gustavo A. Madero';
            this.state = 'Ciudad de México';
            this.city = 'México';
        } else {
            this.street = '';
            this.numExt = '';
            this.numInt = '';
            this.zipCode = '';
            this.neighborhood = '';
            this.alcaldia = '';
            this.state = '';
            this.city = '';
        }
    }
}