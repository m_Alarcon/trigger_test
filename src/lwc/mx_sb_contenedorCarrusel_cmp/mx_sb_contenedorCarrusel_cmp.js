/**
 * @File Name          : mx_sb_contenedorCarrusel_cmp.js
 * @Description        : Componente carrusel
 * @Author             : Arsenio.perez.lopez.contractor@BBVA.com
 * @Group              : BBVA
 * @Last Modified By   : Arsenio.perez.lopez.contractor@BBVA.com
 * @Last Modified On   : 6/2/2020 17:45:01
 * @Modification Log   : Last Modification
 * Ver       Date            Author      		                         Modification
 * 1.0       6/2/2020        Arsenio.perez.lopez.contractor@BBVA.com     Initial Version
 * 1.1       26/6/2020       jesusalexandro.corzo.contractor@bbva.com    Se agrega variable para recuperar el Id de la Opportunidad
 * 1.2       1/7/2020        jesusalexandro.corzo.contractor@bbva.com    Se realizan ajustes al componente
 * 1.3       7/7/2020        jesusalexandro.corzo.contractor@bbva.com    Se modifica el texto del mensaje de error
 * 1.4       31/07/2020      jesusalexandro.corzo.contractor@bbva.com    Se realizan ajustes conforme a la etiqueta lightning/uiRecordApi de acuerdo
 *                                                                        a observaciones realizadas por Retail y se adapta a SOC
**/
import { LightningElement, api, wire, track} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getDataComponent from '@salesforce/apex/MX_SB_VTS_Carrusel_Ctrl.getDataComponent';
import getDataSubEtapaOppo from '@salesforce/apex/MX_SB_VTS_Carrusel_Ctrl.obtDataSubEtapaOppo';
import setDataComponent from '@salesforce/apex/MX_SB_VTS_Carrusel_Ctrl.setDataComponent';
import getconsultallamada from '@salesforce/apex/MX_SB_VTA_Carrusel_cls.getconsultallamada';
export default class Mx_sb_contenedorCarrusel_cmp extends LightningElement {
    @api recordId;
    @api objectApiName;
    @track cssClass= 'slds-section';
    @track subStage;
    @track stage;
    @track product;
    @track staticUrl;
    @track oppudpate;
    @track loaded = true;
    @track lstOptsSubEtapa = [];
    @track lstOptsSubEtapaValue;
    @track consultallamada = [];
    @track dsbFieldSubEtapa = true;
    @track vsbGrpButtonSaveCancel = false;
    @track dsbButtonSave = true;
    @track dsbButtonEdit = false;
    @track dsbButtonCancel = true;
    buttonClicked=false;
    ifsone=true;
    notiscontacto = true;
    @wire(getDataComponent, {sModule: '$objectApiName', sId:'$recordId'})
    wireDataComponent({ error, data}) {
        if(data) {
            if (data.Options[0] === 'isLead'){
                this.lstOptsSubEtapaValue = 'Contacto';
                getDataSubEtapaOppo().then(result => {
                    this.lstOptsSubEtapa = result.optSubEtapasOppo;
                }).catch(error => {
                    this.showToastMessage('Error', 'Ocurrio un error al momento de recueprar las opciones de Status', 'error');
                });
                this.stage = this.lstOptsSubEtapaValue;
                this.subStage = this.lstOptsSubEtapaValue;
                this.staticUrl = true;
                this.product = 'Seguro Estudia';
                this.notiscontacto = this.stage === 'Contacto' ? false : true;
                this.ifsone = false;
                getconsultallamada({subEtapa:this.lstOptsSubEtapaValue,
                    stage: this.stage,
                    staticUrl: true,
                    producto: this.product
                }).then(resultRef =>{
                    this.consultallamada = resultRef;
                    this.ifsone = true;
                }).catch(errorRef =>{
                    this.showToastMessage('Error', 'Ocurrio un error al momento de obtener los datos[GetConsultaLlamada][Lead]', 'error');
                });
            }
            if (data.Options[0] === 'isOpportunity') {
                getDataSubEtapaOppo().then(result => {
                    this.lstOptsSubEtapa = result.optSubEtapasOppo;
                }).catch(error => {
                    this.showToastMessage('Error', 'Ocurrio un error al momento de recueprar las opciones de Status', 'error');
                });
                this.stage = data.Records[0].StageName;
                this.subStage = data.Records[0].MX_SB_VTA_SubEtapa__c;
                this.staticUrl = true;
                this.product = data.Records[0].Producto__c;
                this.notiscontacto = this.stage === 'Contacto' ? false : true;
                this.lstOptsSubEtapaValue = this.subStage;
                this.ifsone = false;
                getconsultallamada({subEtapa:this.lstOptsSubEtapaValue,
                    stage: this.stage,
                    staticUrl: true,
                    producto: this.product
                }).then(resultRef =>{
                    this.consultallamada = resultRef;
                    this.ifsone = true;
                }).catch(errorRef =>{
                    this.showToastMessage('Error', 'Ocurrio un error al momento de obtener los datos[GetConsultaLlamada][Opportunity]', 'error');
                });
            }
        }
    }
    handleChangeSubEtapa(event) {
        this.lstOptsSubEtapaValue = event.detail.value;
    }
    handleButtonSave() {
        if (this.objectApiName === 'Lead') {
            if (this.lstOptsSubEtapaValue !== 'Contacto') {
                this.vsbGrpButtonSaveCancel = false;
                this.dsbButtonSave = true;
                this.dsbButtonEdit = false;
                this.dsbFieldSubEtapa = true;
                this.showToastMessage('Notificación: Cambio de Subetapa.', 'Para ver los siguientes pasos a seguir, por favor tipifica y avanza a Oportunidades.', 'error');
                this.lstOptsSubEtapaValue = 'Contacto';
            } else {
                this.vsbGrpButtonSaveCancel = false;
                this.dsbButtonSave = true;
                this.dsbButtonEdit = false;
                this.dsbFieldSubEtapa = true;
                this.showToastMessage('Notificación: Cambio de Subetapa.', 'Subetapa almacenada satisfactoriamente.', 'success');
            }
        }
        if (this.objectApiName === 'Opportunity'){
            this.dsbButtonSave = true;
            this.dsbButtonCancel = true;
            this.dsbFieldSubEtapa = true;
            const oParams = { sModule: this.objectApiName, sId: this.recordId, sSubEtapa: this.lstOptsSubEtapaValue, sStageName: this.stage, sProducto: this.product };
            setDataComponent({ objParams: oParams }).then(result => {
                this.ifsone = false;
                getconsultallamada({subEtapa:this.lstOptsSubEtapaValue,
                                     stage: this.stage,
                                     staticUrl: true,
                                     producto: this.product
                }).then(resultRef =>{
                    this.consultallamada = resultRef;
                    this.ifsone = true;
                    this.vsbGrpButtonSaveCancel = false;
                    this.dsbButtonSave = true;
                    this.dsbButtonEdit = false;
                    this.dsbFieldSubEtapa = true;
                    this.showToastMessage('Notificación: Cambio de Subetapa.', 'Subetapa almacenada satisfactoriamente.', 'success');
                }).catch(errorRef =>{
                    this.showToastMessage('Notificación: Cambio de Subetapa.', 'Ocurrio un error al momento de obtener las referencias visuales. [handleButtonSave]', 'error');
                });
            }).catch(error => {
                this.showToastMessage('Notificación: Cambio de Subetapa.', 'Ocurrio un error al momento de realizar el proceso: [handleButtonSave]', 'error');
            });
        }
    }
    handleButtonCancel() {
        this.vsbGrpButtonSaveCancel = false;
        this.dsbButtonSave = true;
        this.dsbButtonCancel = true;
        this.dsbButtonEdit = false;
        this.dsbFieldSubEtapa = true;
        if (this.objectApiName === 'Lead'){
            this.lstOptsSubEtapaValue = 'Contacto';
        }
    }
    handleButtonEdit() {
        this.vsbGrpButtonSaveCancel = true;
        this.dsbButtonSave = false;
        this.dsbButtonCancel = false;
        this.dsbButtonEdit = true;
        this.dsbFieldSubEtapa = false;
    }
    toglet() {
        this.buttonClicked = !this.buttonClicked; //set to true if false, false if true.
        this.cssClass = this.buttonClicked ? 'slds-section slds-is-open' : 'slds-section';
    }
    togletielse() {
        this.ifsone = false;
    }
    togletif() {
        this.ifsone = true;
    }
    showToastMessage(sTitle, sMessage, sVariant) {
        const oEvt = new ShowToastEvent({
            title: sTitle,
            message: sMessage,
            variant: sVariant
        });
        this.dispatchEvent(oEvt);
    }
}