import {
    api,
    LightningElement,
    track,
    wire
} from "lwc";
import {
    getObjectInfo
} from "lightning/uiObjectInfoApi";
import {
    refreshApex
} from '@salesforce/apex';
import {
    getRecord,
    updateRecord
} from "lightning/uiRecordApi";
import {
    ShowToastEvent
} from "lightning/platformShowToastEvent";

import ACC_OBJECT from "@salesforce/schema/Account";
import ID_FIELD from '@salesforce/schema/Account.Id';
import STATUS_FIELD from "@salesforce/schema/Account.BPyP_ls_Estado_del_candidato__c";
import MOTIVO_FIELD from "@salesforce/schema/Account.BPyP_ls_Motivo__c";

const ABIERTO = "Abierto";
const GESTION = "En Gestión";
const CERRADO = "Descartado"

export default class MX_BPP_ProspectosPath extends LightningElement {
    /** Record ID of the record.
     * @type {String} */
    @api recordId;

    /** Reflects if the component is loading data from server.
     * @type {Boolean} */
    @track loading = true;

    /** Error message, if any.
     * @type {String} */
    @track errorMessage;

    /** Open the modal.
     * @type {Boolean} */
    @track openmodel = false;

    /** Items to display in the path.
     * @type {Object[]} */
    items = [{
            "label": ABIERTO,
            "value": ABIERTO
        },
        {
            "label": GESTION,
            "value": GESTION
        },
        {
            "label": CERRADO,
            "value": CERRADO
        }
    ]; // label and value
    /** Selected item of the status field.
     * @type {String} */
    @track selectedItem; // value of selected item

    @wire(getObjectInfo, {
        objectApiName: ACC_OBJECT
    }) objectInfo;

    @wire(getRecord, {
        recordId: "$recordId",
        fields: [STATUS_FIELD, MOTIVO_FIELD],
    })
    accObj({
        data,
        error
    }) {
        const accCb = (data) => {
            this.selectedItem = data.fields[STATUS_FIELD.fieldApiName].displayValue
            this.loading = false;
        };
        this._handleWireCallback({
            data,
            error,
            cb: accCb
        });
    }

    /** Selected next item of the status field.
     * @type Integer */
    nextItemIndex = null;

    /** Old ClassList for pathItem
     * @type {Object} */
    oldClassName;

    /** BPyP_ls_Motivo__c value
     * @type String */
    motivoValue = null;

    /**
     * Auxiliary data structure to help the rendering of path items on
     * the user interface.
     * @type {Object}
     */
    get itemsToDisplay() {
        // First, check if the selected value is included within the
        // items.
        const selectedItemExists =
            this.items.some(item => item.value === this.selectedItem);

        // Build metadata used by component.
        const displayedItemInfos = [];
        var selectedItemFound = false;
        for (let i = 0; i < this.items.length; i++) {
            const item = this.items[i];
            const displayedItemInfo = {};

            let itemType;
            if (item.value === this.selectedItem) {
                itemType = 'selected';
                if (this.selectedItem === CERRADO) {
                    itemType = 'lost';
                }

                selectedItemFound = true;
            } else if (!selectedItemExists || selectedItemFound) {
                // If the selected item is not in the set of values, then no value
                // in the path should appear as selected.
                // Also, if the element is after the selected (current) item, it
                // must appear as incomplete.
                itemType = 'incomplete';
            } else {
                // The selected value exists in the set of values but the current
                // value is before that value, so it must appear as complete.
                itemType = 'complete';
            }

            displayedItemInfo.label = item.label;
            displayedItemInfo.value = item.value;
            displayedItemInfo.index = i;
            switch (itemType) {
                case 'complete':
                    displayedItemInfo.selected = false;
                    displayedItemInfo.assistiveText = 'Stage complete';
                    displayedItemInfo.classes = 'slds-path__item slds-is-complete';
                    break;
                case 'selected':
                    displayedItemInfo.selected = true;
                    displayedItemInfo.assistiveText = 'Current stage';
                    displayedItemInfo.classes = 'slds-path__item slds-is-active slds-is-current';
                    break;
                case 'lost':
                    displayedItemInfo.selected = true;
                    displayedItemInfo.assistiveText = 'Current stage';
                    displayedItemInfo.classes = 'slds-path__item  slds-is-lost slds-is-active slds-is-current';
                    break;
                case 'incomplete':
                    displayedItemInfo.selected = false;
                    displayedItemInfo.assistiveText = 'Stage incomplete';
                    displayedItemInfo.classes = 'slds-path__item slds-is-incomplete';
                    break;
                default:
                    displayedItemInfo.classes = 'slds-path__item slds-is-incomplete';
                    displayedItemInfo.assistiveText = 'Stage incomplete';
                    displayedItemInfo.selected = false;
                    break;
            }

            displayedItemInfos.push(displayedItemInfo);
        }

        return displayedItemInfos;
    }

    /**
     * Auxiliary data structure to help the rendering of path items on
     * the user interface.
     * @type Boolean
     */
    get buttonState() {
        var stateBtn = true;
        if (stateBtn && this.helperNextItem() === null) {
            stateBtn = false;
        }
        return stateBtn;
    }

    /**
     * Help to recover the original classes
     */
    recoverClassList() {
        if (this.nextItemIndex != null) {
            this.template.querySelector('[data-id="' + this.nextItemIndex + '"]').className = this.oldClassName;
        }
    }


    /**
     * Set the classes for the current item
     */
    currentClassList() {
        this.oldClassName = this.template.querySelector('[data-id="' + this.nextItemIndex + '"]').className;
        this.template.querySelector('[data-id="' + this.nextItemIndex + '"]').className = 'slds-path__item slds-is-incomplete slds-is-current';
    }

    /**
     * Help to set the next item for the status field selected
     */
    handleItemClick(event) {
        console.log("handleItemClick");
        event.stopPropagation();
        if (this.buttonState) {
            this.recoverClassList();
            if (this.selectedItem !== this.items[event.currentTarget.dataset.id].value) {
                this.nextItemIndex = event.currentTarget.dataset.id;
                this.currentClassList();
            } else {
                this.nextItemIndex = null;
            }
        }
    }

    /**
     * Help to set the next item for the status field when user don't click any item
     */
    helperNextItem() {
        let newItem = this.items.findIndex(x => x.value === this.selectedItem) + 1;
        if (newItem >= this.items.length) {
            newItem = null;
        }

        return newItem;
    }

    /**
     * Call the apex method to update the record
     */
    updateRecord() {
        if (this.nextItemIndex !== null) {
            const fields = {};
            fields[ID_FIELD.fieldApiName] = this.recordId;
            fields[STATUS_FIELD.fieldApiName] = this.items[this.nextItemIndex].value
            if (this.motivoValue !== null) {
                fields[MOTIVO_FIELD.fieldApiName] = this.motivoValue;
            }

            const recordInput = {
                fields
            };
            this.loading = true;
            this.nextItemIndex = null;
            updateRecord(recordInput)
                .then(() => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Prospecto Actualizado',
                            message: 'Se actualizo el prospecto',
                            variant: 'success'
                        })
                    );
                    return refreshApex(this.accObj);
                })
                .catch(error => {
                    this.loading = false;
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error actualizando el prospecto',
                            message: error.body.message,
                            variant: 'error'
                        })
                    );
                });
        }
    }

    /**
     * Help to handle the click in the button to update the record
     */
    handleButtonClick(event) {
        event.stopPropagation();
        if (this.nextItemIndex === null) {
            this.nextItemIndex = this.helperNextItem();
        }
        if (this.items[this.nextItemIndex].value === CERRADO) {
            this.openmodel = true;
        } else {
            this.updateRecord();
        }
    }

    // truly private methods, only called from within this file
    _handleWireCallback = ({
        data,
        error,
        cb
    }) => {
        if (error) console.error(error);
        else if (data) {
            cb(data);
        }
    };

    handleMotivoChange(event) {
        this.motivoValue = event.target.value;
    }

    closeModal() {
        this.openmodel = false;
    }

    saveMethod() {
        this.updateRecord();
        this.closeModal();

    }
}