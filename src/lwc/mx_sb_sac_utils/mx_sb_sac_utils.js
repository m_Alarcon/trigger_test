import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import updateCaseComments from '@salesforce/apex/MX_SB_SAC_1500_ExecutiveAuth.updateCaseComments';
import createEntitlement from '@salesforce/apex/MX_SB_SAC_Utils_Cls.createEntitlement';

const updateCase = (entitlementId, prodId, accId, contractId, tipification, caseData, comments) => {
    const dataCase = { 'sObjectType' : 'Case' };
    dataCase.Id = caseData.Id;
    dataCase.ProductId = prodId;
    dataCase.MX_SB_SAC_Contrato__c = contractId;
    dataCase.MX_SB_SAC_CorreoCuenta__c = caseData.MX_SB_SAC_CorreoCuenta__c;
    if(caseData.MX_SB_1500_is1500__c === true && comments === true) {
        dataCase.Status ='Identificación del cliente';
        dataCase.MX_SB_SAC_Etapa__c = 'Identificación del cliente';
    }else{
        dataCase.Status = 'Ejecución de tramite';
        dataCase.MX_SB_SAC_Etapa__c = 'Ejecución de tramite';
        dataCase.EntitlementId = entitlementId;
        dataCase.AccountId = accId;
        dataCase.MX_SB_SAC_FinalizaFlujo__c = true;
        dataCase.MX_SB_SAC_Catalogotipificacion__c = tipification.Id;
        dataCase.MX_SB_SAC_Ramo__c = tipification.MX_SB_SAC_SubRamo__c;
        if(caseData.MX_SB_1500_is1500__c === true)  {
            dataCase.MX_SB_1500_isAuthenticated__c = true;
            dataCase.MX_SB_SAC_ActivarReglaComentario__c = true;
        }
    }
    switch(tipification.MX_SB_SAC_SubRamo__c) {
        case 'Auto':
        case 'LBBVA Auto':
        case '1500 Auto':
            dataCase.MX_SB_SAC_ProductosParaVenta__c = tipification.MX_SB_SAC_ProductosParaVenta__c ? tipification.MX_SB_SAC_ProductosParaVenta__c : '';
            break;
        case 'Vida':
        case 'LBBVA Vida':
        case '1500 Vida':
            dataCase.MX_SB_SAC_ProductosParaVida__c = tipification.MX_SB_SAC_ProductosParaVenta__c ? tipification.MX_SB_SAC_ProductosParaVenta__c : '';
            break
        default:
            break;
    }
    updateCaseComments({ caseData : dataCase })
    .then(result =>{
        ShowToastEvent({
            title: 'Exito',
            message: 'Caso actualizado',
            variant: 'success'
        })
        eval("$A.get('e.force:refreshView').fire();");
    }).catch(error => {
        ShowToastEvent({
            title: 'Error',
            message: 'Fallo al actualizar el caso, motivo: ' + error.body,
            variant: 'error'
        })
    })
}
const finishClientAuth = (accId, contractId, caseData, prodId, tipification, comments)  =>  {
    const formattedDate =  new Date(caseData.CreatedDate).toDateString('dd/mm/yyyy');
    const dataEntitlement = { 'sObjectType' : 'Entitlement' };
    dataEntitlement.Type = 'Asistencia por Internet';
    dataEntitlement.AccountId = accId;
    dataEntitlement.Name = caseData.CaseNumber + ' ' + formattedDate;
    createEntitlement({ entitlementData : dataEntitlement })
    .then(result => {
        updateCase(result.Id, prodId, accId, contractId, tipification, caseData,  comments);
    }).catch(error => {
        throw new Error(error + ' Fail on entitlement creation');
    })
}
const handleUndefinedValues = (value) => {
    let handledValue = '';
    if(value) {
        handledValue = value;
    }
    return handledValue;
}

const goBackwards = () => {
    return true;
}
const showNotification = (notificationData) => {
    ShowToastEvent({
        title: notificationData.get('title'),
        message: notificationData.get('message'),
        variant: notificationData.get('variant')
    });
}
const finishQuoting = (caseData, prodData, saleProcess, extraData) => {
    const notificationData = new Map();
    const caseObject = { 'sObjectType' : 'Case' };
    caseObject.Id = caseData.Id;
    caseObject.ProductId = prodData[0].Id;
    caseObject.MX_SB_SAC_MetodoVta__c = saleProcess;
    caseObject.MX_SB_SAC_FinalizaFlujo__c = true;
    caseObject.Status = 'Identificación del cliente';
    caseObject.MX_SB_SAC_Etapa__c = 'Identificación del cliente';
    caseObject.MX_SB_SAC_Ramo__c = 'Auto';
    if(extraData.has('email')) {
        caseObject.MX_SB_SAC_CorreoCuenta__c = extraData.get('email');
    }
    if(extraData.has('accountId')) {
        caseObject.AccountId = extraData.get('accountId');
    }
    updateCaseComments({ caseData : caseObject })
    .then(result =>{
        notificationData.set('title', 'Exito');
        notificationData.set('message', 'Caso actualizado');
        notificationData.set('variant', 'success');
        showNotification(notificationData);
        eval("$A.get('e.force:refreshView').fire();");
    }).catch(error => {
        notificationData.set('title', 'Error');
        notificationData.set('message', 'Fallo al actualizar el caso, motivo: ' + error.body);
        notificationData.set('variant', 'error');
        showNotification(notificationData);
    })
}
const getSalesProcess = (productName) => {
    let process = ''
    if(productName !== 'Seguro Estudia') {
        process = 'Venta directa';
    } else if(productName === 'Seguro Estudia') {
        process = 'Venta asistida digital';
    }
    return process;
}

export { finishClientAuth, handleUndefinedValues, goBackwards, finishQuoting, getSalesProcess };