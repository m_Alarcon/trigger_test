import { LightningElement, track} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class Mx_sb_vts_datosasegurado extends LightningElement {

    @track valFem = false;
    @track valMas = false;
    @track valMex = false;
    @track valExt = false;
    @track valFechaNa = '';
    @track valNomCli = '';
    @track valApePa = '';
    @track valApeMa = '';
    @track valRfc = '';
    @track lugarNac = '';

    onEstadoSelection(event) {
        this.lugarNac = event.detail.selectedValue;
    }

    getValFem(event) {
        this.valFem = event.target.checked;
        this.valMas = false;
    }
    getValMas(event) {
        this.valMas = event.target.checked;
        this.valFem = false;
    }
    getValMex(event) {
        this.valMex = event.target.checked;
        this.valExt = false;
    }
    getValExt(event) {
        this.valExt = event.target.checked;
        this.valMex = false;
    }
    getValFechaNa(event) {
        this.valFechaNa = event.target.value;
        const getValFecha = this.valFechaNa;
        var currentDay = new Date();
        var birthdayDate = new Date(getValFecha);
        var setAges = currentDay.getFullYear() - birthdayDate.getFullYear();
        if (setAges > 65 || setAges < 18) {
            this.showToast('ATENCIÓN', 'Fecha inválida', 'warning');
        }
        return setAges;
    }
    changeValues(event) {
        if (event.target.name === 'nombreCliente') {
            this.valNomCli = event.target.value;
        } else if (event.target.name === 'apellidoPa') {
            this.valApePa = event.target.value;
        } else if (event.target.name === 'apellidoMa') {
            this.valApeMa = event.target.value;
        } else if (event.target.name === 'RFC') {
            this.valRfc = event.target.value;
        }
        this.generateRfc();
    }
    generateRfc() {
        const nombCliente = this.valNomCli;
        const apePaterno = this.valApePa;
        const apeMaterno = this.valApeMa;
        const fechaCumple = this.valFechaNa;
        const anio = fechaCumple.slice(0, 4);
        const generateRfc = [];
        generateRfc[0] = apePaterno.charAt(0).toUpperCase();
        generateRfc[1] = this.generateVocal(apePaterno).toUpperCase();
        generateRfc[2] = apeMaterno.charAt(0).toUpperCase();
        generateRfc[3] = nombCliente.charAt(0).toUpperCase();
        generateRfc[4] = anio.toString().slice(2);
        generateRfc[5] = fechaCumple.slice(5, 7);
        generateRfc[6] = fechaCumple.slice(8, 10);
        this.valRfc = generateRfc.join("");
        return generateRfc.join("");
    }
    generateVocal(str) {
        var listaVocales = 'aeiou';
        var i, c;
        for (i = 1; i < str.length; i++) {
            c = str.charAt(i);
            if (listaVocales.indexOf(c) >= 0) {
                return c;
            }
        }
        return 'X';
    }

    handledNextAction() {
        let upWrapStage = Object.assign({}, this.wrapperContext, {
            etapa : "formalizada"
          });
        this.wrapperContext = upWrapStage;
        this.nextAction();
    }

    nextAction() {
        const nextAction = new CustomEvent("nextaction", {
            detail: this.wrapperContext,
        });
        this.dispatchEvent(nextAction);
    }

    onClickPhone() {
        this.visible = true;
        this.template.querySelector('.divPhone').classList.remove('slds-hide');
    }
    onHidePhone() {
        this.visible = true;
        this.template.querySelector('.divPhone').classList.add('slds-hide');
    }
    onClickHomo() {
        this.visible = true;
        this.template.querySelector('.divHomo').classList.remove('slds-hide');
    }

    onHideHomo() {
        this.visible = true;
        this.template.querySelector('.divHomo').classList.add('slds-hide');
    }
    onClickRFC() {
        this.visible = true;
        this.template.querySelector('.divRfc').classList.remove('slds-hide');
    }

    onHideRFC() {
        this.visible = true;
        this.template.querySelector('.divRfc').classList.add('slds-hide');
    }
    onClickCurp() {
        this.visible = true;
        this.template.querySelector('.divCurp').classList.remove('slds-hide');
    }

    onHideCurp() {
        this.visible = true;
        this.template.querySelector('.divCurp').classList.add('slds-hide');
    }

    showToast(titleMsg, messageBody, typeMsg) {
        const oEvt = new ShowToastEvent({
            title: titleMsg,
            message: messageBody,
            variant: typeMsg
        });
        this.dispatchEvent(oEvt);
    }
}