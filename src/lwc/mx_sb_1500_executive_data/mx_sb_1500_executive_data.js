import { LightningElement, wire, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import getExecutiveData from '@salesforce/apex/MX_SB_1500_GetExecutiveData_Ctlr.getExecutiveData';

export default class Mx_sb_1500_executive_data extends LightningElement { // NOSONAR
    @api recordId;
    @track executiveData = [];
    showData = false;
    @wire(getExecutiveData, { recordId: '$recordId'})
    getExecutiveData({error, data}) {
        if(data) {
            this.showData = true;
            this.executiveData = data;
        } else if(error) {
            console.error(error);
            this.dispatchEvent(new ShowToastEvent({
                title: 'Error',
                message: error,
                variant: 'error'
            }));
        }
    }
}