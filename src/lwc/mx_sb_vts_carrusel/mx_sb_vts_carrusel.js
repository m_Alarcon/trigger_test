/**
 * @File Name          : mx_sb_vts_carrusel.js
 * @Description        : Carrusel
 * @Author             : Arsenio.perez.lopez.contractor@BBVA.com
 * @Group              : BBVA
 * @Last Modified By   : Arsenio.perez.lopez.contractor@BBVA.com
 * @Last Modified On   : 10/2/2020 17:27:42
 * @Modification Log   : Last Mod
 * Ver       Date            Author                             		 Modification
 * 1.0       10/2/2020       Arsenio.perez.lopez.contractor@BBVA.com     Initial Version
 * 1.1       26/6/2020       jesusalexandro.corzo.contractor@bbva.com    Ajustes al codigo para que permita recibir 2 parametros en vez de 4
 * 1.2       1/7/2020        jesusalexandro.corzo.contractor@bbva.com    Se realizan ajustes al componente
 * 1.3       1/7/2020        jesusalexandro.corzo.contractor@bbva.com    Se modifica el texto del mensaje de error
**/
import { LightningElement, api, wire, track} from 'lwc';
import getconsultallamada from '@salesforce/apex/MX_SB_VTA_Carrusel_cls.getconsultallamada';
export default class Mxsbvtscarrusel extends LightningElement {
    @api pasubstage;
    @api pastage;
    @api pastaticurl;
    @api paproduct;
    @track consultallamada = [];
    @wire(getconsultallamada,{subEtapa:'$pasubstage', stage:'$pastage', staticUrl:'$pastaticurl', producto:'$paproduct'})
    wireQuery({ error, data }){
        if(data) {
            this.consultallamada=data;
        } else if (error) {
            throw new Error('Ocurrio un error al momento de ejecutar el proceso');
        }
    }
}