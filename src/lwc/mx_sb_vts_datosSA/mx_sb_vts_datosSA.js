import { LightningElement, track } from 'lwc';
export default class Mx_sb_vts_datosSA extends LightningElement {//NOSONAR
    @track amount = '';
    @track currencyType = '';
    @track firstQues = false;
    @track secondQues = false;
    @track thirdQues = false;
    labelText = '* Cancer o tumores malignos, leucemia. \n';
    labelText2 = '* Aneurisma, trombosis, embolia, derrame cerebral, infarto, Isquemia o arritmia. \n';
    labelText3 = '* Efisema, broquitis, tuberculosis, enfermedad pulmonar obstructiva crónica (EPOC) o '+
        'enfermedades Infecciosas relacionadas al aparato respiratorio.';
    labelFinal = this.labelText + '\n' + this.labelText2 + '\n'+ this.labelText3;

    get rangeAmount() {
        return [
            { label: '$0 - $20,000', value: '$0 - $20,000' },
            { label: '$20,001 - $50,000', value: '$20,001 - $50,000' },
            { label: 'Mayor a $50,000', value: 'Mayor a $50,000' },
        ];
    }

    get currencies() {
        return [
            { label: 'Pesos(MXN)', value: 'Pesos(MXN)' },
            { label: 'Dólares(USD)', value: 'Dólares(USD)' },
        ];
    }

    takeAmount(event) {
        this.amount = event.detail.value;
    }

    takeCurrency(event) {
        this.currencyType = event.detail.value;
    }

    oneChecked(event) {
        this.firstQues = event.target.value;
        this.secondQues = false;
        this.thirdQues = false;
    }

    secChecked(event) {
        this.secondQues = event.target.value;
        this.firstQues = false;
        this.thirdQues = false;
    }
    thiChecked(event) {
        this.thirdQues = event.target.value;
        this.firstQues = false;
        this.secondQues = false;
    }
}