import { LightningElement, api, track } from 'lwc';
import initDataCotiz from '@salesforce/apex/MX_SB_VTS_CotizInitData_Ctrl.initDataCotiz';
export default class Mx_sb_vts_paymentsFrecuencies extends LightningElement {//NOSONAR
    @api recordId;
    @api stagecount = '0';
    @api totalstages = '0';
    @api hasquartly = false;
    @api hasTitle = false;
    @api hasFoot = false;
    @api oppData;
    @api cotizObject;
    @api ejemplo = '';
    @api comentarios = '';
    @api cotizProRen = '';
    @api cp;
    @api registroid;
    @track isLoadOk = false;

    connectedCallback() {
        if(this.recordId === undefined) {
            this.recordId = this.registroid;
        }
        initDataCotiz({oppId: this.recordId}).then(result => {
            this.isLoadOk = true;
            if(result.hasError === false) {
                this.oppData = result.oppRecord;
                this.evaluteProduct();
            }
        }).catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: error.enhancedErrorType,
                    message: 'No se pudo recuperar mensualidades',
                    variant: 'warning'
                })
            );
        });
    }

    evaluteProduct() {
        let productName = this.oppData.Producto__c;
        switch (productName) {
            case 'Hogar seguro dinámico':
                this.hasquartly = false;
                break;

            case 'Vida segura dinámico':
                this.hasquartly = false;
                this.hasFoot = true;
                this.hasTitle = true;
                break;
            default:
                break;
        }
    }
    handledNextAction() {
        let upWrapStage = Object.assign({}, this.wrapperContext, {
            etapa : "cotizada"
          });
        this.wrapperContext = upWrapStage;
        this.nextAction();
    }

    nextAction() {
        const nextAction = new CustomEvent("nextaction", {
            detail: this.wrapperContext,
        });
        this.dispatchEvent(nextAction);
    }
}