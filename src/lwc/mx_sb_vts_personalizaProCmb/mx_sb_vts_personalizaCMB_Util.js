import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const showToastMsgPro = (aTitle, aMessage, aVariant) => {
    const aEvt = new ShowToastEvent({
        title: aTitle,
        message: aMessage,
        variant: aVariant
    });
    dispatchEvent(aEvt);
};

const evaluateInactives = (getValStatus, strCodeCober, isCobAct, document) => {
    if (getValStatus === 'Inactivo') {
        document.querySelector('[data-name="' + strCodeCober + '"]').classList.add('class1');
    } else if (isCobAct === true && getValStatus === 'Activo') {
        if (strCodeCober === 'btnDesaNatCont') {
            document.querySelector('[data-name="' + strCodeCober + '"]').classList.remove('class1');
        } else if (strCodeCober === 'btnDesNatAguaCons') {
            document.querySelector('[data-name="' + strCodeCober + '"]').classList.remove('class1');
        } else if (strCodeCober === 'btnTerrCont') {
            document.querySelector('[data-name="' + strCodeCober + '"]').classList.remove('class1');
        } else if (strCodeCober === 'btnTerremotoCons') {
            document.querySelector('[data-name="' + strCodeCober + '"]').classList.remove('class1');
        }
    }
};

const evalueteEntry = (totalEntry) => {
    let formatCurrency = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' });
    if (totalEntry !== '') {
        return formatCurrency.format(totalEntry);
    } else {
        return formatCurrency.format('0.00');
    }
};

const orderPayList = (setPays, arrPayments) => {
    let arrayPay = [];
    let points = Array.from(setPays.values());
    points.sort(function (a, b) { return b - a });
    let mapPrices = new Map();
    for (let index = 0; index < points.length; index++) {
        const element = points[index];
        for (let index2 = 0; index2 < arrPayments.length; index2++) {
            const elementPay = arrPayments[index2];
            if (elementPay.label === evalueteEntry(element)) {
                if (mapPrices.has(elementPay) === false) {
                    mapPrices.set(element, elementPay);
                }
            }
        }
    }
    mapPrices.forEach(function (valor, clave) {
        arrayPay.push(valor);
    });
    return arrayPay;
};


const fillListPay = (payList) => {
    let arrayPay = [];
    let arrayPayOrder = []
    if (payList !== undefined) {
        const mySet = new Set();
        let paySplit = payList.split('|');
        for (let index = 0; index < paySplit.length; index++) {
            let element = paySplit[index];
            let elementPaysI = element.split(',');
            let elementPay = elementPaysI[0] + ',' + elementPaysI[1] + ',' + elementPaysI[2] + ',' + evalueteEntry(elementPaysI[3]);
            const option = {
                label: evalueteEntry(elementPaysI[3]),
                value: elementPay
            };
            mySet.add(parseFloat(elementPaysI[3]));
            arrayPay.push(option);
        }
        arrayPayOrder = orderPayList(mySet, arrayPay);
    }
    return arrayPayOrder;
};

const fillElement = (satotal, totalpay, lstPays, status, selectPay) => {
    return { SA: evalueteEntry(satotal), totalPay: evalueteEntry(totalpay), listPays: lstPays, status: status, selectPay: selectPay};
}

const fillBuildingVals = (buldingsArr, elementPays) => {
    let totalSA = 0.00;
    let totalArray = 0.00;
    let statusCov = true;
    let paySelect;
    let listPays = [];
    for (let index = 0; index < buldingsArr.length; index++) {
        const element = buldingsArr[index];
        if (element.MX_SB_VTS_CoverageCode__c !== undefined && element.MX_SB_VTS_CoverageCode__c === elementPays) {
            statusCov = element.MX_SB_MLT_Estatus__c === 'Activo' ? true : false;
            totalSA = parseFloat(element.MX_SB_MLT_SumaAsegurada__c);
            listPays = fillListPay(element.MX_SB_VTS_PayList__c);
            if (element.MX_SB_VTS_SelectedPay__c !== undefined) {
                paySelect = element.MX_SB_VTS_SelectedPay__c;
            }
        }
        totalArray = totalArray + parseFloat(element.MX_SB_VTS_CoversAmount__c);
    }
    return fillElement(totalSA, totalArray, listPays, statusCov, paySelect);
};

const findSelectedVal = (elementLists, porcCover) => {
    let elementItem;
    for (let index = 0; index < elementLists.length; index++) {
        let element = elementLists[index];
        let splitElement = element.value.split(',');
        if (splitElement[2] === porcCover) {
            elementItem = element.value;
        }
    }
    return elementItem;
};

export { showToastMsgPro, evaluateInactives, evalueteEntry, orderPayList, fillBuildingVals, findSelectedVal, fillElement};