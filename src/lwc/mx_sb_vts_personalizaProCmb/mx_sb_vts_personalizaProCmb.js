import { LightningElement, api, track } from 'lwc';
import getIdSynCmb from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.gtOppValsCrtl';
import findDataCmb from '@salesforce/apex/MX_SB_VTS_CotizInitData_Ctrl.findDataClient';
import { getRecordNotifyChange } from 'lightning/uiRecordApi';
import updCurrentStage from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.updCurrentOppCtrl';
import { NavigationMixin } from 'lightning/navigation';
import updateCuponCot from '@salesforce/apex/MX_SB_VTS_infoBasica_Ctrl.updateCuponCot';
import mAddCobert from '@salesforce/apex/MX_SB_VTS_AddCoberturas_Ctrl.addCoverages';
import findDataQuo from '@salesforce/apex/MX_SB_VTS_DeleteCoverageHSD_ctrl.fillCotizData';
import updatePartData from '@salesforce/apex/MX_SB_VTS_DeleteCoverageHSD_ctrl.editPetsHSD';
import updateCoverage from '@salesforce/apex/MX_SB_VTS_DeleteCoverageHSD_ctrl.removeCoverages';
import dataCobert from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.fillCoberValsCtrl';
import fillAllDataCovers from '@salesforce/apex/MX_SB_VTS_DeleteCoverageHSD_ctrl.fillAllDataCovers';
import mGenCotizPrices from '@salesforce/apex/MX_SB_VTS_GetSetDatosPricesSrv_Ctrl.setDataPricesCtrl';
import mReDataCotizCtrl from '@salesforce/apex/MX_SB_VTS_GetSetDatosPricesSrv_Ctrl.reDataCotizCtrl';
import syncQuoteByType from '@salesforce/apex/MX_SB_VTS_DeleteCoverageHSD_ctrl.syncQuoteByType';
import updateDynamicList from '@salesforce/apex/MX_SB_VTS_editCoverageHSD_ctrl.updateDynamicList';
import saveInsuredAmo from '@salesforce/apex/MX_SB_VTS_AddressInfo_Ctrl.updateSumas';
import savePicklist from '@salesforce/apex/MX_SB_VTS_SumasAseguradas_Ctrl.updatePick';
import getPicklist from '@salesforce/apex/MX_SB_VTS_SumasAseguradas_Ctrl.getPick';
import getInsuredAmo from '@salesforce/apex/MX_SB_VTS_SumasAseguradas_Ctrl.getInsured';
import mGenCotizPricLst from '@salesforce/apex/MX_SB_VTS_GetSetDatosPricesSrv_Ctrl.setDataPricCtrlLst';
import { showToastMsgPro, evaluateInactives, evalueteEntry, fillBuildingVals, findSelectedVal, fillElement } from './mx_sb_vts_personalizaCMB_Util';

export default class Mx_sb_vts_personalizaProCmb extends NavigationMixin(LightningElement) { //NOSONAR

    @api cp;
    @api montos;
    @api registroid;
    @api wrapperParent = {};
    @track chosenValue = '';
    @track newArray = '';
    @track valueInce = '';
    @track itemsInce = [];
    @track valueRobo = '';
    @track itemsRobo = [];
    @track valueLine = '';
    @track itemsLine = [];
    @track valueDama = '';
    @track itemsDama = [];
    @track valueGlass = '';
    @track itemsGlass = [];
    @track attrValueCmb;
    @track idQuoCmb = '';
    @track hidroRisk = '';
    @track terreRisk = '';
    @track valMonth = '$0.00';
    @track valQuarter = '$0.00';
    @track valAnual = '$0.00';
    @track valSemiAnual = '$0.00';
    @track items = [];
    @track value = '';
    @track info = [];
    @track valoresCoverages;
    @track buttonClic = false;
    @track isLoading = false;
    @track isActive = false;
    @track valorCincuenta = 0.50;
    @track valorVeinticinco = 0.25;
    @track valorQuince = 0.15;
    @track valorCien = 0.100;
    @track lstCobCmb = ['btnCasaAsis', 'btnMedicaAsis', 'btnMascotasAsis',
        'btnIncendioCont', 'btnTerrCont', 'btnDesaNatCont', 'btnTercerosCont', 'btnRobPertCont',
        'btnRoboArtCont', 'btnRobDinEfeCont', 'btnRobLineaCont', 'btnIncendioCons',
        'btnTerremotoCons', 'btnDesNatAguaCons', 'btnCivilDanosTercCons', 'btnRotCritalesCons'];
    @track suggestAmount;
    @track buldingFire = {};
    @track contentFire = {};
    @track contentGlass = {};
    @track contentBurglary = {};
    @track contentDamages = {};
    @track contentCivilityFam = {};
    @track buildingCivility = {};
    @track contentCivility = {};
    @track contentInveFurni = {};
    @track buldingHidro = {};
    @track contentHidro = {};
    @track contentElect = {};
    @track contentElectHo = {};
    @track buildingEarqu = {};
    @track contentEarqu = {};
    @track homeBurglary = {};
    @api flujo;
    @track sumasSecu = [];
    @track limiteMin = 0;
    @track decrePropio = 0;
    @track increPropio = 0;
    @track optionalEarth = false;
    @track optionalHidro = false;
    @track totalRobo = '$0.00';
    @track selectVal;
    @track valSel;
    @track mInfo = new Object();
    @track rInfo = new Object();
    @track isCobAct = false;

    activeSectionsCmb = ['Asistencias', 'Contenidos', 'Construccion'];
    casaCmb = 'Cuentas con ayuda de especialistas por si en tu día a día necesitas: plomería, electricidad, cerrajería, albañilería, jardinería y más.';
    medicaCmb = 'Cuentas con asesoría médica telefónica, médico a tu domicilio y traslados en ambulancia (terrestre o aérea)';
    mascotasCmb = 'Cuentas con asesoría médica telefónica, médico a tu domicilio y traslados en ambulancia (terrestre o aérea)'
    incendioCmb = 'Cubre daños ocasionados a las cosas que estén dentro de tu casa por: incendio o rayo';
    terremotoCmb = 'Cubre daños ocasionados a las cosas que estén dentro de tu casa por: teremoto o erupción volcánica';
    hidroCmb = 'Cubre daños ocasionados a las cosas que estén dentro de tu casa por: inundación por lluvia, granizo, huracán entre otros.';
    roboCmb = 'Protege objetos dentro de tu hogar como: Televisiones, LapTops, celulares, joyas, obras de arte, artículos deportivos, dinero en efectivo y más';
    roboArtCmb = 'Protege objetos dentro de tu hogar como: joyas, obras de arte, artículos deportivos y más'
    roboDineroCmb = 'Cubre el dinero en efectivo dentro del Hogar';
    linea_blancaCmb = 'Protege los daños de tu pantalla, tablet, equipo de audio, consola de videojuego, centro de lavado, refrigerador, cafetera, entre otros';
    cristalesCmb = 'Cubre la rotura accidental de vidrios o cristales interiores y exteriores como: ventanas, canceles, espejos y más';
    tercerosCmb = 'Cubrimos los daños ocasionados por ti, tu familia o hasta tus mascotas a bienes o personas dentro y fuera de tu hogar';
    activeSectionsMessage = '';

    connectedCallback() {
        this.fillPickList();
    }
    firstProcess() {
        this.isLoading = true;
        syncQuoteByType({ oppId: this.registroid, quoteNameId: 'Completa' }).then(resultSync => {
            fillAllDataCovers({ oppId: this.registroid, quoteNameId: 'Completa' }).then(result => {
                this.isLoading = false;
                this.processInit(result);
            }).catch(error => {
                this.isLoading = false;
                showToastMsgPro(error, 'Error al actualizar Sumas', 'error');
            });
        });
    }
    processInit(dataInit) {
        if (dataInit.dataCot.dataOpp !== undefined && dataInit.dataCot.dataOpp[0] !== undefined) {
            let objValues = [0];
            if (this.montos !== undefined) {
                objValues = this.montos;
            } else if (dataInit.dataCot.PrimerPropietario__c !== undefined) {
                objValues = dataInit.dataCot.PrimerPropietario__c;
            }
            let valueChoo = parseInt(objValues[0]);
            let suggestOpp = dataInit.dataCot.dataOpp[0].Monto_de_la_oportunidad__c;
            this.suggestAmount = suggestOpp !== undefined ? suggestOpp : valueChoo;
        }
        let criterialsObject = Object.entries(dataInit.criterials);
        this.evalVals(criterialsObject);
    }
    evalVals(criterialsObject) {
        if (criterialsObject.length > 0 && criterialsObject.length !== undefined) {
            this.evaluateCriterials(criterialsObject);
        } else {
            this.evaluteNewCot(false, false);
        }
    }
    evaluateCriterials(criterialsObject) {
        let hasCover = false;
        let hasbase = false;
        for (let index = 0; index < criterialsObject.length; index++) {
            let criterial = criterialsObject[index];
            if (criterial[0] === '2008SA') {
                hasCover = true;
            }
            if (criterial[0] === '2008SABASE') {
                hasbase = true;
            }
        }
        this.evaluteNewCot(hasCover, hasbase);
    }
    evaluteNewCot(hasCoverSA, hasCoverBase) {
        if (hasCoverSA === true && hasCoverBase === true) {
            this.initCoverages();
        }
        else if (this.suggestAmount !== undefined) {
            if (hasCoverSA === true && hasCoverBase === false) {
                this.isLoading = true;
                mGenCotizPrices({ strOppoId: this.registroid, strSumAse: this.suggestAmount }).then(result => {
                    this.isLoading = false;
                    this.initCoverages();
                });
            } else {
                this.reNewCot();
            }
        }
    }
    initCoverages() {
        syncQuoteByType({ oppId: this.registroid, quoteNameId: 'Completa' }).then(resultSync => {
            this.fillWrapObjects();
        });
    }
    evaluateSelected() {
        this.isLoading = true;
        dataCobert({ oppId: this.registroid }).then(data => {
            this.isLoading = false;
            if (data) {
                for (let key in data) {
                    this.evaluateInactive(key, data);
                }
            }
        }).catch(error => {
            this.isLoading = false;
            showToastMsgPro(error, 'Ocurrio un error al Obtener las coberturas preseleccionadas.', 'error');
        })
    }
    evaluateInactive(key, data) {
        let getValCoverage = data[key].MX_SB_VTS_CoverageCode__c;
        let getValStatus = data[key].MX_SB_MLT_Estatus__c;
        let getValDatosPar = data[key].MX_SB_MLT_Cobertura__c;
        switch (getValCoverage) {
            case 'HOME_BURGLARY':
                evaluateInactives(getValStatus, 'btnRobPertCont', this.isCobAct, this.template);
                break;
            case 'HYDROMETEOROLOGICAL_RISKS':
                evaluateInactives(getValStatus, 'btnDesaNatCont', this.isCobAct, this.template);
                evaluateInactives(getValStatus, 'btnDesNatAguaCons', this.isCobAct, this.template);
                break;
            case 'EARTHQUAKE_VOLCANIC_ERUPTION':
                evaluateInactives(getValStatus, 'btnTerrCont', this.isCobAct, this.template);
                evaluateInactives(getValStatus, 'btnTerremotoCons', this.isCobAct, this.template);
                break;
            case 'ELECTRONIC_EQUIPMENT':
                evaluateInactives(getValStatus, 'btnRobLineaCont', this.isCobAct, this.template);
                break;
        }
        if (getValDatosPar === '2008ASMASCOTAS') {
            evaluateInactives(getValStatus, 'btnMascotasAsis', this.isCobAct, this.template);
        }
    }
    fillPickList() {
        let insuAmo = this.montos;
        if (insuAmo === '' || insuAmo === null || insuAmo === undefined) {
            this.getPick();
        } else {
            if (this.flujo === 'a') {
                this.limiteMin = 100000;
                this.methodA();
            } else {
                this.limiteMin = 600000;
                this.methodB();
            }
            let attrValueCmb = this.sumasSecu;
            attrValueCmb.sort((a, b) => a - b);
            let formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
                minimumFractionDigits: 0,
            });
            for (let keyInCmb in attrValueCmb) {
                let arrayM = this.montos;
                let cutArray = arrayM.indexOf(this.chosenValue) + 1;
                let nuevoArray = [...arrayM];
                this.newArray = nuevoArray.slice(0, cutArray);
                const option = {
                    label: formatter.format(attrValueCmb[keyInCmb]),
                    value: String(attrValueCmb[keyInCmb])
                };
                this.items = [...this.items, option];
            }
            this.updatePick();
        }
    }
    getPick() {
        getPicklist({ srtOppId: this.registroid }).then(result => {
            let sumasTest = Array.from(result.split(","));
            this.getInsuAmo();
            let formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
                minimumFractionDigits: 0,
            });
            for (let keyInCmb in sumasTest) {
                const option = {
                    label: formatter.format(sumasTest[keyInCmb]),
                    value: String(sumasTest[keyInCmb])
                };
                this.items = [...this.items, option];
            }
            showToastMsgPro('Exito', 'Se ha guardado el picklist', 'success');
        }).catch(error => {
            showToastMsgPro(error, 'Error al intentar guardar las sumas aseguradas', 'error');
        });
    }
    getInsuAmo() {
        getInsuredAmo({ oppId: this.registroid }).then(result => {
            let insuredAmo = result;
            showToastMsgPro('Exito', 'Se ha obtenido la suma asegurada', 'success');
            this.chosenValue = String(insuredAmo);
            this.firstProcess();
        }).catch(error => {
            showToastMsgPro(error, 'Error al intentar obtener la suma asegurada', 'error');
        });
    }
    updatePick() {
        let insuAmo = String(this.sumasSecu);
        savePicklist({ srtOppId: this.registroid, insuredAmo: insuAmo }).then(result => {
            this.firstProcess();
            showToastMsgPro('Exito', 'Se ha guardado el picklist', 'success');
        }).catch(error => {
            showToastMsgPro(error, 'Error al intentar guardar las sumas aseguradas', 'error');
        });
    }
    methodA() {
        let objValues = this.montos;
        if (objValues !== undefined) {
            let valueChoo = parseInt(objValues[0]);
            this.chosenValue = String(objValues[0]);
            switch (valueChoo) {
                case 20000000:
                    this.sameMax(valueChoo);
                    break;
                case 100000:
                    this.sameMin(valueChoo);
                    break;
                default:
                    this.normalMet(valueChoo);
            }
        }
    }
    methodB() {
        let objValues = this.montos;
        let valueChoo = parseInt(objValues[0]);
        this.chosenValue = String(objValues[0]);
        switch (valueChoo) {
            case 20000000:
                this.sameMax(valueChoo);
                break;
            case 600000:
                this.sameMin(valueChoo);
                break;
            default:
                this.normalMet(valueChoo);
        }
    }
    sameMax(valueChoo) {
        this.sumasSecu.push(valueChoo);
        this.decrePropio = valueChoo;
        let percentPro = valueChoo * 0.10;
        for (let i = 0; i < 2; i++) {
            this.decrePropio = this.decrePropio - percentPro;
            this.sumasSecu.push(this.decrePropio);
        }
    }
    sameMin(valueChoo) {
        this.sumasSecu.push(valueChoo);
        this.increPropio = valueChoo;
        let percentPro = valueChoo * 0.10;
        for (let i = 0; i < 2; i++) {
            this.increPropio = this.increPropio + percentPro;
            this.sumasSecu.push(this.increPropio);
        }
    }
    normalMet(valueChoo) {
        this.sumasSecu.push(valueChoo);
        this.decrePropio = valueChoo;
        this.increPropio = valueChoo;
        let percentIter = valueChoo * 0.10;
        for (let i = 0; i < 2; i++) {
            this.decrePropio = this.decrePropio - percentIter;
            this.sumasSecu.push(this.decrePropio);
        }
        for (let n = 0; n < 2; n++) {
            this.increPropio = this.increPropio + percentIter;
            this.sumasSecu.push(this.increPropio);
        }
        this.limitRemove(this.sumasSecu);
    }

    limitRemove(arrayVals) {
        for (this.valSel = 0; this.valSel < arrayVals.length; this.valSel++) {
            if (arrayVals[this.valSel] < this.limiteMin) {
                arrayVals.splice(this.valSel, 1);
                this.valSel--;
            }
        }
        for (this.selectVal = 0; this.selectVal < arrayVals.length; this.selectVal++) {
            if (arrayVals[this.selectVal] > 20000000) {
                arrayVals.splice(this.selectVal, 1);
                this.selectVal--;
            }
        }
        this.sumasSecu = arrayVals;
    }
    get roleOptions() { return this.items; }
    get incendioOpts() { return this.itemsInce; }
    incendioChan(event) {
        let tempSuggestAm = this.contentFire.selectPay;
        this.contentFire.selectPay = event.detail.value;
        this.updateSumaInce(tempSuggestAm);
    }
    updateSumaInce(tempAmount) {
        let arrValuesFire = this.contentFire.selectPay.split(',');
        let currency = '';
        for (let index = 3; index < arrValuesFire.length; index++) {
            currency = currency + arrValuesFire[index];
        }
        let entriesCov = new Map([['criterial', arrValuesFire[1]], ['id_data', arrValuesFire[0]], ['value', arrValuesFire[2]]]);
        let criteSa = new Map([['criterial', '2008SA'], ['id_data', '001'], ['value', this.suggestAmount.toString()]]);
        let criteSaBase = new Map([['criterial', '2008SABASE'], ['id_data', '001'], ['value', this.suggestAmount.toString()]]);
        let arrCov = [];
        let objCovers = Object.fromEntries(entriesCov);
        let objSa = Object.fromEntries(criteSa);
        let objSaba = Object.fromEntries(criteSaBase);
        arrCov.push(objCovers, objSa, objSaba);
        this.isLoading = true;
        mReDataCotizCtrl({ strOppoId: this.registroid, strProRent: 'Propio' }).then(result => {
            mGenCotizPricLst({ strOppoId: this.registroid, strSumAse: this.suggestAmount, lstDatPart: arrCov }).then(resultPrices => {
                this.isLoading = false;
                this.initCoverages();
            }).catch(error => {
                showToastMsgPro(error, 'Error al generar nueva cotización', 'error');
                this.isLoading = false;
            });
        }).catch(error => {
            showToastMsgPro(error, 'Error al generar nueva cotización', 'error');
            this.isLoading = false;
        });
    }
    get roboOpts() { return this.itemsRobo; }
    roboChange(event) {
        let tempSelect = this.homeBurglary.selectPay;
        this.homeBurglary.selectPay = event.detail.value;
        this.updatePicklist(event, 'homeBurglary', tempSelect);
    }
    get lineaOpts() { return this.itemsLine; }
    lineaChange(event) {
        let tempSelect = this.contentElect.selectPay;
        this.contentElect.selectPay = event.detail.value;
        this.updatePicklist(event, 'contentElect', tempSelect);
    }
    get damageOpts() { return this.itemsDama; }
    damageChange(event) {
        let tempSelect = this.contentCivilityFam.selectPay;
        this.contentCivilityFam.selectPay = event.detail.value;
        this.updatePicklist(event, 'contentCivilityFam', tempSelect);
    }
    get glassOpts() { return this.itemsGlass; }
    glassChange(event) {
        let tempSelect = this.contentGlass.selectPay;
        this.contentGlass.selectPay = event.detail.value;
        this.updatePicklist(event, 'contentGlass', tempSelect);
    }
    revertSelect(picklistName, currentVal) {
        switch (picklistName) {
            case 'homeBurglary':
                this.homeBurglary.selectPay = currentVal;
                break;
            case 'contentElect':
                this.contentElect.selectPay = currentVal;
                break;
            case 'contentGlass':
                this.contentGlass.selectPay = currentVal;
                break;
            case 'contentCivilityFam':
                this.contentCivilityFam.selectPay = currentVal;
                break;
        }
    }
    handleChange(event) {
        let tempSuggestAm = this.chosenValue;
        this.chosenValue = event.detail.value;
        let checkVal = event.detail.value;
        this.updateSuma(checkVal, tempSuggestAm);
    }
    calculateValueCmb(dataOption, dataCover, dataTrade) {
        if (dataOption !== undefined) {
            let arrValues = dataOption.split(',');
            let currency = arrValues[3];
            let number = Number(currency.replace(/[^0-9.-]+/g, ""));
            let valueSelect = arrValues[0] + ',' + arrValues[1] + ',' + arrValues[2] + ',' + number;
            let lstCovers = dataCover.split('|');
            let lstCriters = arrValues[1];
            let arrayAddCovers = [dataTrade];
            arrayAddCovers.push(arrValues[1]);
            arrayAddCovers.push(arrValues[0]);
            arrayAddCovers.push(arrValues[2]);
            arrayAddCovers.push(valueSelect);
            updateDynamicList({ oppId: this.registroid, lstAfectCovers: lstCovers, valuesCriterial: lstCriters, lstPartDat: arrayAddCovers }).then(result => {
                this.isLoading = false;
                this.updatePayments(result);
            }).catch(error => {
                this.isLoading = false;
                showToastMsgPro(error, 'Error al actualizar Sumas', 'error');
            });
        }
    }
    onshodetailelem(event) {
        let eventDetail = event.currentTarget.dataset.idshow;
        this.template.querySelector('[data-id="' + eventDetail + '"]').classList.remove('slds-hide');
    };
    onhidetailelem(event) {
        let eventDetail = event.currentTarget.dataset.idshow;
        this.template.querySelector('[data-id="' + eventDetail + '"]').classList.add('slds-hide');
    };
    updatePicklist(dataEvent, picklistName, tempSelect) {
        if (dataEvent !== undefined) {
            let dataOption = dataEvent.detail.value;
            let dataCover = dataEvent.currentTarget.dataset.covercode;
            let dataTrade = dataEvent.currentTarget.dataset.ramo;
            let arrValues = dataOption.split(',');
            let currency = '';
            for (let index = 3; index < arrValues.length; index++) {
                currency = currency + arrValues[index];
            }
            let valueSelect = dataEvent.currentTarget.dataset.criterialcode;
            let lstCovers = dataCover.split('|');
            let lstCriters = valueSelect.split('|');
            let arrayAddCovers = [dataTrade];
            arrayAddCovers.push(arrValues[1]);
            arrayAddCovers.push(arrValues[0]);
            arrayAddCovers.push(arrValues[2]);
            arrayAddCovers.push(dataOption);
            this.isLoading = true;
            updateDynamicList({ oppId: this.registroid, lstAfectCovers: lstCovers, valuesCriterial: lstCriters, lstPartDat: arrayAddCovers }).then(result => {
                this.isLoading = false;
                this.updatePayments(result);
                this.updateFireAmounts(result);
            }).catch(error => {
                this.revertSelect(picklistName, tempSelect);
                this.isLoading = false;
                showToastMsgPro(error, 'Error al actualizar Sumas', 'error');
            });
        }
    }
    handleToggleSection(event) {
        this.activeSectionMessage = 'Open section name:  ' + event.detail.openSections;
    }
    handleSectionToggle(event) {
        const openSections = event.detail.openSections;
        if (openSections.length === 0) {
            this.activeSectionsMessage = 'All sections are closed';
        } else {
            this.activeSectionsMessage = 'Open sections: ' + openSections.join(', ');
        }
    }
    handledBackAction() {
        let upWrapStage = Object.assign({}, this.wrapperContext, {
            etapa: "newCot"
        });
        this.wrapperContext = upWrapStage;
        this.backAction();
    }
    backAction() {
        const jsonDetail = { cotizObject: this.wrapperContext, zipCode: this.cp, amount: this.montos, flujo: this.flujo };
        const backAction = new CustomEvent("backaction", { detail: jsonDetail, });
        this.dispatchEvent(backAction);
    }
    handledNextAction() {
        let upWrapStage = Object.assign({}, this.wrapperContext, { etapa: "emitida" });
        this.wrapperContext = upWrapStage;
        this.nextAction();
    }
    nextAction() {
        const jsonDetail = { cotizObject: this.wrapperContext, zipCode: this.cp, amount: this.montos, flujo: this.flujo };
        const nextAction = new CustomEvent("nextaction", { detail: jsonDetail, });
        this.dispatchEvent(nextAction);
    }
    onSaveProcmb() {
        this.updDataStage();
    }
    onChangeCob(event) {
        let valNameButton = event.currentTarget.dataset.name;
        let valNameButtonType = event.currentTarget.dataset.type;
        if (this.lstCobCmb.includes(valNameButton)) {
            if (valNameButtonType === 'required') {
                showToastMsgPro('Agregar / Quitar Coberturas', '¡Esta cobertura es requerida, no puede eliminarse!', 'error');
            } else if (valNameButtonType === 'desactived') {
                showToastMsgPro('Agregar / Quitar Coberturas', '¡Cobertura no disponible, no puede eliminarse!', 'error');
            } else if (valNameButtonType === 'optional') {
                this.changeSelectedBtn(valNameButton, event);
            }
        }
    }
    changeSelectedBtn(valNameButton, event) {
        let objClassList = this.template.querySelector('[data-name="' + valNameButton + '"]').classList;
        let isExistClass = false;
        for (let i = 0; i < objClassList.length; i++) {
            if (objClassList[i] === 'class1') {
                isExistClass = true;
                break;
            }
        }
        try {
            let tempObject = {
                typeCober: event.currentTarget.dataset.typecover, valNameButton: event.currentTarget.dataset.name, valNameCoverage: event.currentTarget.dataset.covercode,
                valRamo: event.currentTarget.dataset.ramo, categorie: event.currentTarget.dataset.categorie, goodtype: event.currentTarget.dataset.goodtype
            };
            if (isExistClass) {
                this.template.querySelector('[data-name="' + valNameButton + '"]').classList.remove('class1');
                this.addCobertCmb(tempObject, true, 'BUILDINGS');
            } else {
                this.template.querySelector('[data-name="' + valNameButton + '"]').classList.add('class1');
                this.removeCoveragesCmb(tempObject, true, 'BUILDINGS');
            }
        } catch (error) {
            showToastMsgPro(error, 'Ocurrió un error al recuperar valor', 'error');
        }
    }
    addCobertCmb(tempObject, isRecursive, goodtype) {
        let typeCober = tempObject.typeCober;
        let valNameButton = tempObject.valNameButton;
        let valNameCoverage = tempObject.valNameCoverage;
        let lstCoverages = valNameCoverage.split('|');
        let valRamo = tempObject.valRamo;
        this.isActive = true;
        this.mInfo.ramo = valRamo;
        this.mInfo.categorie = goodtype;
        this.mInfo.isChecked = true;
        if (typeCober === 'criterial') {
            this.isLoading = true;
            let arrayAddCovers = [valRamo];
            arrayAddCovers.push('002');
            arrayAddCovers.push('240');
            arrayAddCovers.push(tempObject.categorie);
            arrayAddCovers.push(tempObject.goodtype);
            arrayAddCovers.push('YEARLY');
            updatePartData({ oppId: this.registroid, lstPartData: lstCoverages, valuesCriterial: arrayAddCovers, coverActive: this.isActive }).then(result => {
                this.isLoading = false;
                this.obtDataQuo();
            }).catch(error => {
                this.isLoading = false;
                showToastMsgPro(error, 'Error al actualizar los datos de la Quote (Agregar DP)', 'error');
                this.template.querySelector('[data-name="' + valNameButton + '"]').classList.add('class1');
            });
        } else if (typeCober === 'coverage') {
            this.isLoading = true;
            mAddCobert({ oppId: this.registroid, coveragesCodes: lstCoverages, mDataCob: this.mInfo }).then(result => {
                if (isRecursive) {
                    this.addCobertCmb(tempObject, false, 'INVENTORY_AND_FURNITURE')
                } else {
                    this.isLoading = false;
                    showToastMsgPro('Éxito: ', 'Se actualizó los datos de la Quote (Agregar COB)', 'success');
                    this.obtDataQuo();
                    getRecordNotifyChange([{ recordId: this.registroid }]);
                    this.evalBtn(valNameButton);
                }
            }).catch(error => {
                console.log(error);
                this.isLoading = false;
                showToastMsgPro('Error: ', 'Error al actualizar los datos de la Quote (Agregar COB)', 'error');
                this.template.querySelector('[data-name="' + valNameButton + '"]').classList.add('class1');
            });
        }
    }
    evalBtn(valNameButton) {
        this.isCobAct = true;
        if (valNameButton === 'btnDesaNatCont' || valNameButton === 'btnDesNatAguaCons') {
            this.evaluateSelected();
        } else if (valNameButton === 'btnTerrCont' || valNameButton === 'btnTerremotoCons') {
            this.evaluateSelected();
        }
    }
    removeCoveragesCmb(tempObject, isRecursive, goodtype) {
        let typeRemCober = tempObject.typeCober;
        let valRemButton = tempObject.valNameButton;
        let valRemCoverage = tempObject.valNameCoverage.valueOf();
        let lstRemCoverages = valRemCoverage.split('|');
        let valRemRamo = tempObject.valRamo;
        this.isActive = false;
        this.rInfo.ramo = valRemRamo;
        this.rInfo.categorie = goodtype;
        this.rInfo.isChecked = false;
        if (typeRemCober === 'criterial') {
            this.isLoading = true;
            let arrRemCovers = [valRemRamo];
            arrRemCovers.push('001');
            arrRemCovers.push('0');
            arrRemCovers.push(tempObject.categorie);
            arrRemCovers.push(tempObject.goodtype);
            if (valRemCoverage === 'CODIGO_CUPON') {
                this.isActive = true;
            }
            arrRemCovers.push('YEARLY');
            updatePartData({ oppId: this.registroid, lstPartData: lstRemCoverages, valuesCriterial: arrRemCovers, coverActive: this.isActive }).then(result => {
                this.isLoading = false;
                this.obtDataQuo();
            }).catch(error => {
                this.isLoading = false;
                showToastMsgPro(error, 'Error al actualizar los datos de la Quote(Quitar DP)', 'error');
                this.template.querySelector('[data-name="' + valRemButton + '"]').classList.remove('class1');
            });
        } else if (typeRemCober === 'coverage') {
            this.isLoading = true;
            updateCoverage({ oppId: this.registroid, coveragesCodes: lstRemCoverages, rDataCob: this.rInfo }).then(result => {
                if (isRecursive) {
                    this.removeCoveragesCmb(tempObject, false, 'INVENTORY_AND_FURNITURE')
                } else {
                    this.isLoading = false;
                    this.obtDataQuo();
                    getRecordNotifyChange([{ recordId: this.registroid }]);
                    this.evalBtn(valRemButton);
                }
            }).catch(error => {
                this.isLoading = false;
                showToastMsgPro(error, 'Error al actualizar los datos de la Quote(Quitar COB)', 'error');
                this.template.querySelector('[data-name="' + valRemButton + '"]').classList.remove('class1');
            });
        }
    }
    obtDataQuo() {
        this.isLoading = true;
        findDataQuo({ oppId: this.registroid }).then(result => {
            this.isLoading = false;
            if (result) {
                var mapVals = result.dataQuote;
                for (let key in mapVals) {
                    let getValMonth = mapVals[key].MX_SB_VTS_TotalMonth__c;
                    var numMonth = '$' + getValMonth.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                    let getValAnual = mapVals[key].MX_SB_VTS_TotalAnual__c;
                    var numAnual = '$' + getValAnual.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                    let getValQuarter = mapVals[key].MX_SB_VTS_TotalQuarterly__c;
                    var numQuarter = '$' + getValQuarter.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                    let getValSemes = mapVals[key].MX_SB_VTS_TotalSemiAnual__c;
                    var numSemiAn = '$' + getValSemes.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                    this.assignVals(numMonth, numAnual, numQuarter, numSemiAn);
                }
                showToastMsgPro('Éxito: ', 'Se obtuvo datos de la Cotización(Prima asegurada)', 'success');
            } else {
                showToastMsgPro('Importante: ', 'No se pudo obtener datos de la Cotización(Prima asegurada)', 'warning');
            }
        });
    }
    assignVals(numMonth, numAnual, numQuarter, numSemiAn) {
        this.valMonth = (numMonth === undefined ? '' : numMonth);
        this.valAnual = (numAnual === undefined ? '' : numAnual);
        this.valSemiAnual = (numSemiAn === undefined ? '' : numSemiAn);
        this.valQuarter = (numQuarter === undefined ? '' : numQuarter);
    }
    updDataStage() {
        let stageNameRen = 'cmb';
        this.isLoading = true;
        updCurrentStage({ srtOppId: this.registroid, etapaForm: stageNameRen }).then(result => {
            this.isLoading = false;
            showToastMsgPro(result, 'Se guardó correctamente a la etapa de Cmb', 'success');
        }).catch(error => {
            this.isLoading = false;
            showToastMsgPro(error, 'Ocurrió un error al guardar la etapa de Cmb', 'error');
        });
    }
    updateQuoteCup() {
        this.isLoading = true;
        updateCuponCot({ oppId: this.registroid, cuponCode: '' }).then(result => {
            this.isLoading = false;
        }).catch(error => {
            this.isLoading = false;
            showToastMsgPro(error, 'Error al actualizar Quote', 'error');
        });
    }
    fillWrapObjects() {
        let arrayTemp = [];
        this.contentFire = fillElement('0.00', '0.00', arrayTemp, true, '');
        this.contentCivilityFam = fillElement('0.00', '0.00', arrayTemp, true, '');
        this.contentInveFurni = fillElement('0.00', '0.00', arrayTemp, true, '');
        this.buldingHidro = fillElement('0.00', '0.00', arrayTemp, true, '');
        this.contentElectHo = fillElement('0.00', '0.00', arrayTemp, true, '');
        this.buildingEarqu = fillElement('0.00', '0.00', arrayTemp, true, '');
        this.contentEarqu = fillElement('0.00', '0.00', arrayTemp, true, '');
        this.buldingFire = fillElement('0.00', '0.00', arrayTemp, true, '');
        this.contentHidro = fillElement('0.00', '0.00', arrayTemp, true, '');
        this.buildingCivility = fillElement('0.00', '0.00', arrayTemp, true, '');
        this.contentCivility = fillElement('0.00', '0.00', arrayTemp, true, '');
        this.contentGlass = fillElement('0.00', '0.00', arrayTemp, true, '');
        this.contentBurglary = fillElement('0.00', '0.00', arrayTemp, true, '');
        this.contentElect = fillElement('0.00', '0.00', arrayTemp, true, '');
        this.contentDamages = fillElement('0.00', '0.00', arrayTemp, true, '');
        this.homeBurglary = fillElement('0.00', '0.00', arrayTemp, true, '');
        this.getDataPrices();
    }
    updatePayments(dataResult) {
        let dataQuote = dataResult.dataCot.dataQuote;
        var mapQuote = new Map(Object.entries(dataQuote));
        const iteratorQuote = mapQuote.values();
        let quoteData = iteratorQuote.next().value;
        this.valAnual = evalueteEntry(quoteData.MX_SB_VTS_TotalAnual__c);
        this.valSemiAnual = evalueteEntry(quoteData.MX_SB_VTS_TotalSemiAnual__c);
        this.valMonth = evalueteEntry(quoteData.MX_SB_VTS_TotalMonth__c);
        this.valQuarter = evalueteEntry(quoteData.MX_SB_VTS_TotalQuarterly__c);
    }
    getDataPrices() {
        this.isLoading = true;
        fillAllDataCovers({ oppId: this.registroid, quoteNameId: 'Completa' }).then(
            result => {
                getRecordNotifyChange([{ recordId: this.registroid }]);
                this.isLoading = false;
                this.getOptionalCo();
                this.updateFireAmounts(result);
                this.updatePayments(result);
            }).catch(error => {
                this.isLoading = false;
                console.log(error);
                showToastMsgPro(error, 'Error al actualizar Sumas', 'error');
            });
    }
    getOptionalCo() {
        getIdSynCmb({ idOpps: this.registroid }).then(data => {
            const dataValueId = data;
            for (let key in dataValueId) {
                let idChild = dataValueId[key].SyncedQuoteId;
                this.idQuoCmb = (idChild === undefined ? '' : idChild);
            }
            this.getCoberOpt();
        }).catch(error => {
            throw new Error(error);
        })
    }
    getCoberOpt() {
        findDataCmb({ quoteId: this.idQuoCmb }).then(result => {
            if (result) {
                var mapQuote = new Map(Object.entries(result.queteDat));
                let getValTerre = mapQuote.get('QuoteToPostalCode');
                let getValHidro = mapQuote.get('ShippingPostalCode');
                this.terreRisk = (getValTerre === undefined ? '' : getValTerre);
                this.hidroRisk = (getValHidro === undefined ? '' : getValHidro);
                this.isLoading = false;
                if (this.terreRisk === '2' || this.terreRisk === '3') {
                    this.optionalEarth = false;
                } else {
                    this.optionalEarth = true;
                }
                if (this.hidroRisk === '1PY' || this.hidroRisk === '1PS' || this.hidroRisk === '1GM') {
                    this.optionalHidro = false;
                } else if (this.hidroRisk === '1IR') {
                    this.optionalHidro = true;
                } else {
                    this.optionalHidro = true;
                }
                showToastMsgPro('Éxito: ', 'Se obtuvo datos de la Cotización getCoberOpt', 'success');
                this.evaluateSelected();
            } else {
                showToastMsgPro('Importante: ', 'No se pudo obtener datos de la Cotización getCoberOpt', 'warning');
            }
        });
    }
    updateFireAmounts(datResult) {
        let fireTrade = datResult.coverages.INCE;
        let inceTrade = fireTrade.FIRE;
        let buldingsTrade = inceTrade.BUILDINGS;
        let rihiTrade = datResult.coverages.RIHI;
        let tecnTrade = datResult.coverages.TECN;
        let terrTrade = datResult.coverages.TERR;
        let inventory = inceTrade.INVENTORY_AND_FURNITURE;
        let miscTrade = datResult.coverages.MISC;
        let rcpfTrade = datResult.coverages.RCPF;
        var mapCriterials = new Map(Object.entries(datResult.criterials));
        this.processBuilding(buldingsTrade);
        this.processContent(inventory, mapCriterials);
        this.processMisc(miscTrade.MISCELLANEOUS, mapCriterials);
        this.contentCivilityFam = fillBuildingVals(rcpfTrade.CIVIL_LIABILITY_INSURANCE.CIVIL_LIABILITY.FAMILY_CIVIL_LIABILITY, 'FAMILY_CIVIL_LIABILITY');
        let contCivilFam = mapCriterials.get('2008PORCSARC');
        let contCivilFamTra = parseFloat(contCivilFam.MX_SB_VTS_TradeValue__c);
        let contCivilFamStr = contCivilFamTra.toFixed(1);
        this.contentCivilityFam.selectPay = this.contentCivilityFam.selectPay !== undefined ? this.contentCivilityFam.selectPay :
            findSelectedVal(this.contentCivilityFam.listPays, contCivilFamStr);
        this.processbuldingHidro(rihiTrade.HYDROMETEOROLOGICAL_RISKS.BUILDINGS);
        this.contentHidro = fillBuildingVals(rihiTrade.HYDROMETEOROLOGICAL_RISKS.INVENTORY_AND_FURNITURE.CONTENT, 'HYDROMETEOROLOGICAL_RISKS');
        this.contentElect = fillBuildingVals(tecnTrade.TECHNICAL.ELECTRONIC_DEVICES.ELECTRONIC_DEVICES, 'ELECTRONIC_EQUIPMENT');
        let conttElect = mapCriterials.get('2008PORCEQELEC');
        let electBln = parseFloat(conttElect.MX_SB_VTS_TradeValue__c);
        let electBlnStr = electBln.toFixed(1);
        this.contentElect.selectPay = this.contentElect.selectPay !== undefined ? this.contentElect.selectPay : findSelectedVal(this.contentElect.listPays, electBlnStr);
        this.contentElectHo = fillBuildingVals(tecnTrade.TECHNICAL.ELECTRONIC_DEVICES.HOME_APPLIANCE, 'APPLIANCE_EQUIPMENT');
        this.processbuldingEart(terrTrade.EARTHQUAKE.BUILDINGS);
        this.contentEarqu = fillBuildingVals(terrTrade.EARTHQUAKE.INVENTORY_AND_FURNITURE.CONTENT, 'EARTHQUAKE_VOLCANIC_ERUPTION');
        this.buildingCivility = fillBuildingVals(rihiTrade.HYDROMETEOROLOGICAL_RISKS.BUILDINGS.BUILDING, 'HYDROMETEOROLOGICAL_RISKS');
        Number(this.contentDamages.totalPay.replace(/[^0-9\.-]+/g, ""))
        let contentDamTemp = this.contentDamages.totalPay === undefined ? 0 : Number(this.contentDamages.totalPay.replace(/[^0-9\.-]+/g, ""));
        let contentBurTemp = this.contentBurglary.totalPay === undefined ? 0 : Number(this.contentBurglary.totalPay.replace(/[^0-9\.-]+/g, ""));
        let homeBurglaTemp = this.homeBurglary.totalPay === undefined ? 0 : Number(this.homeBurglary.totalPay.replace(/[^0-9\.-]+/g, ""));
        this.totalRobo = evalueteEntry((contentDamTemp + contentBurTemp + homeBurglaTemp));
    }
    processbuldingHidro(rihiTrade) {
        if (rihiTrade !== undefined && rihiTrade.BUILDING !== undefined) {
            this.buldingHidro = fillBuildingVals(rihiTrade.BUILDING, 'HYDROMETEOROLOGICAL_RISKS');
        }
    }
    processbuldingEart(eartTrade) {
        if (eartTrade !== undefined && eartTrade.BUILDING !== undefined) {
            this.buildingEarqu = fillBuildingVals(eartTrade.BUILDING, 'EARTHQUAKE_VOLCANIC_ERUPTION');
        }
    }
    processMisc(miscTrade, mapcrits) {
        let critalSign = miscTrade.CRYSTALS_SIGNS;
        let glasses = critalSign.GLASSES
        this.contentGlass = fillBuildingVals(glasses, 'GLASS_BREAKAGE');
        let contGlass = mapcrits.get('2008PORCCRISTAL');
        let glassTrade = parseFloat(contGlass.MX_SB_VTS_TradeValue__c);
        let glassTradeStr = glassTrade.toFixed(2);
        this.contentGlass.selectPay = this.contentGlass.selectPay !== undefined ? this.contentGlass.selectPay : findSelectedVal(this.contentGlass.listPays, glassTradeStr);
        let elecDevices = miscTrade.ELECTRONIC_DEVICES
        let elecDevicesItem = elecDevices.HOME_AMENITIES
        this.homeBurglary = fillBuildingVals(elecDevicesItem, 'HOME_BURGLARY');
        let homeBurPay = mapcrits.get('2008PORCMENCASA');
        let floatTrade = parseFloat(homeBurPay.MX_SB_VTS_TradeValue__c);
        let floatTradestr = floatTrade.toFixed(1);
        this.homeBurglary.selectPay = this.homeBurglary.selectPay !== undefined ? this.homeBurglary.selectPay : findSelectedVal(this.homeBurglary.listPays, floatTradestr);
        let personalItems = miscTrade.PERSONAL_ITEMS;
        let personalItem = personalItems.ART_SCULPTURE;
        this.contentDamages = fillBuildingVals(personalItem, 'THEFT_WORKS_ART');
        let valuableItems = miscTrade.VALUABLE_ITEMS;
        let moneyValues = valuableItems.MONEY_VALUES;
        this.contentBurglary = fillBuildingVals(moneyValues, 'BURGLARY_WITH_VIOLENCE');
    }
    processBuilding(buldingsTrade) {
        if (buldingsTrade !== undefined) {
            if (buldingsTrade.BUILDING !== undefined) {
                this.buldingFire = fillBuildingVals(buldingsTrade.BUILDING, 'FIRE_LIGHTNING');
            }
        }
    }
    processContent(inventory, mapCriterials) {
        if (inventory !== undefined) {
            if (inventory.CONTENT !== undefined) {
                this.contentFire = fillBuildingVals(inventory.CONTENT, 'FIRE_LIGHTNING');
                let contentFireSt = mapCriterials.get('2008PORCSAIC');
                let coFireStrade = parseFloat(contentFireSt.MX_SB_VTS_TradeValue__c);
                let coFiStradeStr = coFireStrade.toFixed(1);
                this.contentFire.selectPay = this.contentFire.selectPay !== undefined ? this.contentFire.selectPay : findSelectedVal(this.contentFire.listPays, coFiStradeStr);
            }
        }
    }
    updateSuma(insuAmo, tempAmount) {
        this.isLoading = true;
        saveInsuredAmo({ oppId: this.registroid, insuredAmo: insuAmo }).then(result => {
            this.suggestAmount = insuAmo;
            this.isLoading = false;
            this.reNewCot();
        }).catch(error => {
            this.chosenValue = tempAmount;
            this.isLoading = false;
            showToastMsgPro(error, 'Error al intentar guardar la suma asegurada', 'error');
        });
    }
    reNewCot() {
        this.isLoading = true;
        mReDataCotizCtrl({ strOppoId: this.registroid }).then(result => {
            this.isLoading = false;
            this.isLoading = true;
            mGenCotizPrices({ strOppoId: this.registroid, strSumAse: this.suggestAmount }).then(resultPrices => {
                this.isLoading = false;
                this.initCoverages()
            }).catch(error => {
                this.isLoading = false;
            });
        }).catch(error => {
            this.isLoading = true;
        });
    }
}