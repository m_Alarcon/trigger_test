import {LightningElement, track, api } from 'lwc';
import findRecordsVal from "@salesforce/apex/MX_SB_VTS_ObtStates_Ctrl.getDirValueCtrl";

export default class Mx_sb_vts_seleccionEstado extends LightningElement {

    @api lugarnaci = '';
    @api selectedValue;
    @api selectedRecordId;
    @api iconName;
    @api lookupLabel;
    @track recordsList;
    @track searchKey = "";
    @track message;

    connectedCallback() {
        this.selectedValue = this.lugarnaci;
        this.onSeletedRecordUpdate();
    }
    handleKeyChange(event) {
        const searchKey = event.target.value;
        this.searchKey = searchKey;
        this.getLookupResult();
    }
    getLookupResult() {
        findRecordsVal({ searchKey: this.searchKey })
            .then((result) => {
                if (result.length === 0) {
                    this.recordsList = [];
                    this.message = "No Records Found";
                } else {
                    this.recordsList = result;
                    this.message = "";
                }
                this.error = null;
            })
            .catch((error) => {
                this.error = error;
                this.recordsList = null;
            });
    }
    onRecordSelection(event) {
        this.selectedRecordId = event.target.dataset.key;
        this.selectedValue = event.target.dataset.name;
        this.searchKey = "";
        this.onSeletedRecordUpdate();
    }
    onSeletedRecordUpdate() {
        const passEventr = new CustomEvent('recordselection', {
            detail: { selectedRecordId: this.selectedRecordId, selectedValue: this.selectedValue }
        });
        this.dispatchEvent(passEventr);
    }
    removeRecordOnLookup(event) {
        this.searchKey = "";
        this.selectedValue = null;
        this.selectedRecordId = null;
        this.recordsList = null;
        this.onSeletedRecordUpdate();
    }
    onLeave() {
        setTimeout(() => {
            this.searchKey = "";
            this.recordsList = null;
        }, 300);
    }
}