import { LightningElement, track, wire, api } from 'lwc';
import { handleUndefinedValues } from 'c/mx_sb_sac_utils';
import initialData from '@salesforce/apex/MX_SB_SAC_IdentificarCliente.initialData';
import Id from '@salesforce/user/Id';
import MX_SB_SAC_CaseComments from '@salesforce/label/c.MX_SB_SAC_CaseComments';

export default class MX_SB_SAC_IdentificarCliente extends LightningElement {
    @track value = '';
    @track startValidation = false;
    @track showComments = false;
    @track showSearch = false;
    @api recordId;
    @track userType = '';
    userId = Id;
    @track clientName = '';
    @track reason = '';
    @track record= [];
    customText;
    isQuoting = false;
    showNormal = false;
    is1500 = false;
    agentName = '';

    @wire(initialData, { recId : '$recordId', userId: '$userId' })
    wiredData({ error, data}) {
        if(data) {
            const  conts = data;
            for(let key in conts){
                if(key === 'case') {
                    this.clientName = handleUndefinedValues(conts[key].MX_SB_SAC_Nombre__c)
                                + ' ' + handleUndefinedValues(conts[key].MX_SB_SAC_ApellidoPaternoCliente__c)
                                + ' ' + handleUndefinedValues(conts[key].MX_SB_SAC_ApellidoMaternoCliente__c);
                    this.agentName = '';
                    this.reason = conts[key].Reason;
                    this.evalOrigin(conts[key].Origin);
                    this.record = conts[key];
                    this.customText = 'Sr/Srta '+ this.clientName + '. ' + MX_SB_SAC_CaseComments;
                    if(conts[key].MX_SB_1500_is1500__c === true) {
                        this.evalOrigin('#1500');
                    }
                    this.evalReason();
                }
                if(key === 'user') {
                    this.userName = conts[key].Name;
                }
            }
        } else if (error) {
            throw new Error('Failed to retrieve initialData');
        }
    }
    get options() {
        return [
            { label: 'Si', value: 'option1' },
            { label: 'No', value: 'option2' }
        ];
    }

    handleIsClientValidation(event) {
        const eventVal = event.currentTarget.value;
        if(eventVal === 'option2') {
            this.showComments = true;
            this.startValidation = true;
        } else if(eventVal === 'option1') {
            this.showSearch = true;
            this.startValidation = true;
        }
    }

    handleReturnValidation() {
        this.startValidation = false;
        this.showComments = false;
        this.showSearch = false;
    }

    evalOrigin(value) {
        switch(value) {
            case 'Phone':
			case 'Vitality':
            case 'Ventas':
                this.userType = 'SAC';
                break;
            case 'Linea BBVA':
                this.userType = value;
                break;
            case '#1500':
                this.userType = '#1500';
                break;
            default:
                break;
        }
    }

    evalReason() {
        if(this.reason === 'Cotización') {
            this.isQuoting = true;
        } else if(this.userType === '#1500' && this.reason === 'Consultas'
        && this.record.MX_SB_SAC_Subdetalle__c !== 'N/A' && this.record.MX_SB_SAC_Detalle__c !== ''){
            this.is1500 = true;
        } else {
            this.showNormal = true;
        }
    }
}