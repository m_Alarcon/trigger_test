import { LightningElement, api } from 'lwc';

export default class Mx_sb_vts_cotizadoresPagos extends LightningElement {
    @api quoteId = '';
    @api totalentmonth = '';
    @api totalentqueartly = '';
    @api totalentsemianual = '';
    @api totalentanual = '';
    @api hidequeartly = false;
}