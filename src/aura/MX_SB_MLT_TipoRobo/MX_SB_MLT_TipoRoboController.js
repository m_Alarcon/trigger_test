({
    handleCancelTelefonicaRobo: function(component) {
        component.set('v.isEditMode', false);
    },
    doInitTelefonicaRobo : function(component,event,helper) {
        var action22 = component.get("c.getRecordType");
        action22.setParams({"devname":"MX_SB_MLT_RamoAuto"});
        action22.setCallback(this, function(response) {
            component.set("v.recordtypeAUTO",response.getReturnValue());
        });
        $A.enqueueAction(action22);
    },
    handleSuccessTelefonicaRobo : function(component, event) {
    var payload34 = event.getParams().response;
        var navService = component.find("navService");
        var pageReferenceRob = {
            type: 'standard__recordPage',
            attributes: {
                "recordId": payload34.id,
                "objectApiName": component.get("v.sObjectName"),
                "actionName": "view"
            }
        }
        event.preventDefault();
        navService.navigate(pageReferenceRob);
        var toastEvent = $A.get("e.force:showToast");
        if(component.get('v.continuar')===false){
            toastEvent.setParams({
            "title": "Actualización de registro",
                    "type":"success",
            "message": "Cambio exitoso"
            });
            toastEvent.fire();
        }
    },
    toggleEditModeTelefonicaRobo : function(component) {
        component.set("v.isEditMode", !component.get("v.isEditMode"));
    },
    handleEditFormTelefonicaRobo : function(component) {
        component.set('v.status','Detalles del Siniestro');
        component.set('v.continuar',false);
        component.find("SiniestroEditForm").submit();
    },
    handleEditFormTelefonicaRoboCon : function(component) {
        component.set('v.status','Crear Servicios');
        component.set('v.continuar',true);
	component.find("SiniestroEditForm").submit();
	}
})