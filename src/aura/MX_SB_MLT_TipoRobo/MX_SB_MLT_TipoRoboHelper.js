({
    getValue : function(component,event,helper){
        var action = component.get("c.getValue");
        var recId = component.get("v.recordId");
        console.log('recId ' + recId);
        action.setParams({"recId": component.get("v.recordId") });
        action.setCallback(this, function(response) {
            console.log(response.getReturnValue());
            var someVar = JSON.stringify(response.getReturnValue());
            console.log('PickValue ' + someVar);
        });
        $A.enqueueAction(action);
    }
})