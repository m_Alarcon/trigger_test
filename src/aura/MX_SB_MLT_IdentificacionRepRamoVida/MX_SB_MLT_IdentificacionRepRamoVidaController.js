({
    handleCancelIdenVida: function(component) {
        component.set('v.isEditMode', false);
    },
    doInitIdenVida : function(component) {
        var action = component.get("c.getRecordType");
	action.setParams({"devname":"MX_SB_MLT_RamoVida"});
        action.setCallback(this, function(response) {
            component.set("v.recordtypeVida",response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    handleSuccessIdenVida : function(component, event) {
        var payload = event.getParams().response;
        var navService = component.find("navService");
        var pageReference = {
            type: 'standard__recordPage',
            attributes: {
                "recordId": payload.id,
                "objectApiName": component.get("v.sObjectName"),
                "actionName": "view"
            }
        }
        event.preventDefault();
        navService.navigate(pageReference);
	},
	toggleEditIdenVida : function(component) {
		component.set("v.isEditMode", !component.get("v.isEditMode"));
    },
    handleCreateIdenVida : function(component) {
        component.find("leadCreateForm2").submit();
        $A.get("e.force:refreshView").fire();
        $A.get("e.force:editRecord").fire();
        component.set('v.isEditMode', false);
    }
})