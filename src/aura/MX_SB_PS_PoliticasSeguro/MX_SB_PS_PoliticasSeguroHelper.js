({
    doInit : function(component, event, helper) {
        component.set("v.productName",component.get("v.opp").Producto__c);
        if(component.get("v.opp").Producto__c === 'Respaldo Seguro Para Hospitalización'){
            component.set("v.flagProduct","True");
        }
    },
})