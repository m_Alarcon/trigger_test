({
    onSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "El registro se ha guardado exitosamente",
            "duration": "5000",
            "mode": "dismissible",
            "type": "success"
        });
        toastEvent.fire();
        var navigate = component.get("v.navigateFlow");
        navigate("NEXT");
    },
    onSubmit : function(component, event, helper) {
    },
    onLoad : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Loaded!",
            "message": "The record has been Loaded successfully ."
        });
        toastEvent.fire();
    },
    onError : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error!",
            "message": "Error, contacta a tu administrador de sistema",
            "duration": "5000",
            "mode": "dismissible",
            "type": "error"
        });
        toastEvent.fire();
    }
})