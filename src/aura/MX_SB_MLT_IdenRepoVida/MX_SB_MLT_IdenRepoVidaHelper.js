({
	init : function(component, event, helper) {
        let recordId = component.get("v.recordId");
		let action = component.get("c.searchSinVida");
        action.setParams({'siniId':recordId});
        action.setCallback(this,function(response) {
            if(response.getState() === 'SUCCESS') {
                let siniResponse = response.getReturnValue();
                component.set("v.Siniestro",siniResponse);
                let sinType = siniResponse.TipoSiniestro__c;
                helper.addListValue(component, event, helper, sinType);
                let insuranceCompany = siniResponse.MX_SB_MLT_InsuranceCompany__c;
                helper.changeInsurance(component, event, helper, insuranceCompany)
            }
        });
        $A.enqueueAction(action);
	},
    labelRecordType : function(component, event, helper) {
        let recordId = component.get("v.recordId");
        let getLabel = component.get("c.searchRecordType");
        getLabel.setParams({'recordId':recordId});
        getLabel.setCallback(this,function(response) {
            if(response.getState() === 'SUCCESS') {
                let labelResponse = response.getReturnValue();
                helper.tipoRamo(component, event, helper, labelResponse);
            }
        });
        $A.enqueueAction(getLabel);
    },
    tipoRamo : function(component, event, helper, ramo) {
        switch(ramo) {
            case 'MX_SB_MLT_RamoAuto' :
                component.set("v.ramo",'Auto');
            break;
            case 'MX_SB_MLT_RamoGF' :
                component.set("v.ramo",'Gastos Funerarios');
            break;
            case 'MX_SB_MLT_RamoGMM' :
                component.set("v.ramo",'Gastos Medicos Mayores');
            break;
            case 'MX_SB_MLT_RamoVida' :
                component.set("v.ramo",'Vida');
           	break;
            default:
        }
    },
    comentarios : function(component, event, helper) {
        let recordId = component.get("v.recordId");
        let getHistoryFields = component.get("c.historyFields");
        getHistoryFields.setParams({'recordId':recordId});
        getHistoryFields.setCallback(this,function(response) {
            if(response.getState() === 'SUCCESS') {
                let respObject = response.getReturnValue();
                if(respObject.hasOwnProperty('Histories')) {
                    component.set("v.tracking",respObject.hasOwnProperty('Histories'));
                }
                component.set("v.informacion",respObject);
            }
        });
        $A.enqueueAction(getHistoryFields);
    },
    addListValue : function(component, event, helper, valueType) {
        switch(valueType) {
            case 'Siniestros':
                let listAccordion = component.get("v.openSection");
                listAccordion.push('comentarios');
                component.set("v.openSection",listAccordion);
                break;
            case 'Asistencias':
                let listValues = component.get("v.openSection");
                listValues.push('fallecido');
                component.set("v.openSection",listValues);
                break;
            default:
        }
    },
    changeInsurance : function(component, event, helper, insuranceCompany) {
        switch(insuranceCompany) {
            case 'SEGUROS BANCOMER' :
                component.set("v.insuranceCompany",'SEGUROS BBVA');
                break;
            default :
                component.set("v.insuranceCompany",insuranceCompany);
        }
    },
})