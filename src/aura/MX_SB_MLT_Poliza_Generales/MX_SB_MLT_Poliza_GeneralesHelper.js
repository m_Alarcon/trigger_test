({  consultaInicial : function(component, event) {
		var action = component.get("c.consultaPoliza");
        var sinId = component.get('v.recordId');
        action.setParams({
            'siniestroId' : sinId
        });
        action.setCallback(this,function(response){
            if (response.getState() === "SUCCESS") {
                var StoreResponse = response.getReturnValue();
                component.set('v.mapPoliza',StoreResponse);
                component.set('v.polizaSelect',true);
                var numPol = component.get('v.mapPoliza.numpoliza');
                var idPol = component.get('v.mapPoliza.idContrato');
                component.set('v.poliza',numPol);
                component.set('v.polizaId',idPol);
            }
        });
         $A.enqueueAction(action);
	},
     certificados : function(component, event, helper) {
        var evtcert = $A.get("e.force:navigateToComponent");
        evtcert.setParams({
            componentDef : "c:MX_SB_MLT_TablaCertificados",
            componentAttributes: {
                poliza : component.get("v.poliza")
            }
        });
        evtcert.fire();
    },
    datosP : function(component, event, helper) {
        var evntPrs = $A.get("e.force:navigateToComponent");
        evntPrs.setParams({
            componentDef : "c:MX_SB_MLT_Poliza_DatosParticulares",
            componentAttributes: {
                poliza : component.get("v.poliza")
            }
        });
        evntPrs.fire();
    },
    recibos : function(component, event, helper) {
        var evntRcb = $A.get("e.force:navigateToComponent");
        evntRcb.setParams({
            componentDef : "c:MX_SB_MLT_Poliza_Recibos",
            componentAttributes: {
                poliza : component.get("v.polizaId")
            }
        });
        evntRcb.fire();
    },
})