({  doInit : function(component, event, helper) {
		helper.consultaInicial(component, event);
	},
    coberturas : function(component, event, helper) {
        helper.certificados(component,event,helper);
    },
    datosPart : function(component, event, helper) {
        helper.datosP(component,event,helper);
    },
    recibosPol : function(component, event, helper) {
        helper.recibos(component,event,helper);
    }
})