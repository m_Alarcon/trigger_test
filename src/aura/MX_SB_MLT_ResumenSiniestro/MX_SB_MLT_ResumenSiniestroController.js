({
	doinit : function(component, event, helper) {
        helper.initCobertura(component,event,helper);
        helper.initServicios(component,event,helper);
		var recId= component.get("v.recordId");
        component.set('v.sinObj.Id',recId);
        var action = component.get("c.searchSinVida");
        action.setParams({'siniId':component.get("v.sinObj.Id")});
        action.setCallback(this, function(response) {
            component.set("v.sinObj",response.getReturnValue()[0]);
            if(response.getReturnValue().MX_SB_MLT_ClaimNumber__c===undefined){
            	component.set("v.sinObj.MX_SB_MLT_ClaimNumber__c",'-');
            }
        });
        $A.enqueueAction(action);
	},
})