({
	initCobertura: function(component,event) {
        component.set('v.columns', [
		{label: 'Estatus', fieldName: 'MX_SB_MLT_Estatus__c', type: 'text',cellAttributes: { alignment: 'center' }},
		{label: 'Cobertura', fieldName: 'Name', type: 'text',cellAttributes: { alignment: 'center' }},
		{label: 'Asegurado', fieldName: 'NombreCliente__c', type: 'text',cellAttributes: { alignment: 'center' }},
		{label: 'Reservas Disponibles', fieldName: 'MX_SB_MLT_SumaAsegurada__c', type: 'currency',cellAttributes: { alignment: 'center' }},
		{label: 'Pagado', fieldName: 'TotalAPagar__c', type: 'currency',cellAttributes: { alignment: 'center' }}
        ]);
    },
    initServicios: function(component,event) {
    	component.set('v.columns2', [
    	{label: 'Estatus', fieldName: 'Status', type: 'text'},
        {label: 'Numero de Servicio', fieldName: 'MX_SB_MLT_ClaimId__c', type: 'text'},
		{label: 'Relacionado a', fieldName: 'MX_SB_MLT_Siniestro__c', type: 'text'},
		{label: 'Servicios', fieldName: 'MX_SB_MLT_TipoServicioAsignado__c', type: 'text'},
		{label: 'Proveedor', fieldName: 'MX_SB_MLT_NameAjustadorCC__c', type: 'text'}
 		]);
		},
})