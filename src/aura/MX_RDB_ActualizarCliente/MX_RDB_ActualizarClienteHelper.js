/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RDB_ActualizarClienteHelper
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2019-11-29
* @Group  		MX_RDB_ActualizarCliente
* @Description 	Helper from MX_RDB_ActualizarCliente Lightning Component
* @Changes
*
*/
({
    getClientID : function(component) {
        console.log('getclienteID');
        // Call apex class method
        var action = component.get("c.getClientID");
        action.setParams({ idAcc : component.get("v.recordId") });
        action.setCallback(this, function(data) {
            // Set response value in ClienteData attribute on component
            component.set("v.ClientId", data.getReturnValue());
        });
        $A.enqueueAction(action);
    },

    actualizarDatos : function(component) {
        console.log('actualizardatos');
        var resultMessage = $A.get("$Label.c.MX_RDB_ActClienteErrorMsg");
        var clientID = component.get("{!v.ClientId}");
        var updateClientInfo = component.get("c.updateClientInfo");
        updateClientInfo.setParams({
            'clientId': clientID
        });
        updateClientInfo.setCallback(this, function(response) {
            console.log(response.getState());
            console.log(response.getReturnValue());
            if (response.getState() === 'SUCCESS' && response.getReturnValue() === 'OK') {
                resultMessage = $A.get("$Label.c.MX_RDB_ActClienteMsg");
                this.showAlert(component, event, "Exito", "dismissible", "SUCCESS", resultMessage);
            }
            else {
                this.showAlert(component, event, "Error", "dismissible", "ERROR", resultMessage);
            }
            $A.get("e.force:closeQuickAction").fire();
        });
        $A.enqueueAction(updateClientInfo);
    },

    showAlert : function(component, event, title, mode, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "mode": mode,
            "duration": 3000,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    }
})