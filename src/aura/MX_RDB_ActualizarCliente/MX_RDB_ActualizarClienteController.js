/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RDB_ActualizarClienteController.js
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2019-11-29
* @Group  		MX_RDB_ActualizarCliente
* @Description 	Controller from MX_RDB_ActualizarCliente Lightning Component
* @Changes
*
*/
({
	doInit : function(component, event, helper) {
        helper.getClientID(component);
        helper.actualizarDatos(component);
	}
})