({
	Showhide : function(component, event, helper) {
         var vidcheck = event.getSource().get('v.name');
         var checkBox = event.getSource().get('v.checked');
         var minimo= "Minimo"+vidcheck;
         var maximo= "Maximo"+vidcheck;
         var mes = "v.mes"+vidcheck;
             component.find(minimo).set("v.disabled", !checkBox);
             component.find(maximo).set("v.disabled", !checkBox);
             component.set(mes,checkBox);
    }
})