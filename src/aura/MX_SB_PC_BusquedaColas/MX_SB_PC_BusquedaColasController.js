({
    onCheck: function(component, evt){
        var set = new Set(component.get("v.seleccionados"));
        if(component.get("v.seleccion")){
            set.add(component.get("v.idUsuario"));
        }else{
            set.delete(component.get("v.idUsuario"));
        }
        component.set("v.seleccionados", set);
    },
    guardaAsignacion: function(component, evt){
        var action = component.get("c.guardarNuevasAsignaciones");
        var user = component.get("v.idUsuario");
        var userIds = [];
        userIds.push(user);
        var origen = component.get("v.idCola");
        var destino = component.get("v.idColaNuevo");
        action.setParams({
            'userIds': userIds,
            'origen': origen,
            'destino': destino
        });
        action.setCallback(this, function(guardarAsignaciones) {
            var state = guardarAsignaciones.getState();
            if(state === "SUCCESS") {
                var eventoToast = $A.get("e.force:showToast");
                eventoToast.setParams({
                    title : 'Éxito',
                    message: 'Se guardaron los registros',
                    type: 'success'
                });
                eventoToast.fire();
            } else {
                var errors = respuesta.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        alert("Error de mensaje: " + errors[0].message);
                    }
                } else {
                    alert("Error desconocido");
                }            }
        });
        $A.enqueueAction(action);
    }
})