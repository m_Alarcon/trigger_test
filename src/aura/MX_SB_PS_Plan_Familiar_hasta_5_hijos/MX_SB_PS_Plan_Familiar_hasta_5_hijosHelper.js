({
    doInitCH : function(component,event,helper) {
        var oppId= component.get('v.recordId');
        var laction2= component.get('c.loadOppF');
        laction2.setParams({'oppId':oppId});
        laction2.setCallback(this, function(responseCH) {
            component.set('v.oppObj',responseCH.getReturnValue());
            var action7=component.get('c.getintN');
            action7.setParams({'quotId':responseCH.getReturnValue().SyncedQuoteId});
            action7.setCallback(this, function(responseCI) {
                component.set('v.allClicks',responseCI.getReturnValue());
                this.availableBenefs(component, event,helper,responseCI.getReturnValue());
                component.set('v.refreshC',false);
            });
            $A.enqueueAction(action7);
            if(responseCH.getReturnValue().Plan__c !=='Individual' ) {
                this.loadsetplan(component,event,helper,responseCH);
            }else {
                component.set('v.titulo','Individual');
            }
        });
        $A.enqueueAction(laction2);
    },
    loadsetplan : function(component,event,helper,responseCH) {
        component.set("v.visible",true);
                if(component.get('v.titulo')==='Individual') {
                    component.set('v.titulo',responseCH.getReturnValue().Plan__c);
                    this.loadMaxClicks(component,event);
                    this.availableBenefs(component,event,helper, component.get('v.allClicks'));
                    if(responseCH.getReturnValue().Plan__c.includes('Conyugal')) {
                        component.set('v.options2', component.get('v.listopt1'));
                        component.set('v.Datos.MX_MainClientRelationship','Cónyuge');
                    }
                    if(responseCH.getReturnValue().Plan__c.includes('Titular')) {
                        component.set('v.options2',component.get('v.listopt2'));
                        component.set('v.Datos.MX_MainClientRelationship','Hijo');
                    }
                    if(responseCH.getReturnValue().Plan__c.includes('Familiar')) {
                        var quota = responseCH.getReturnValue().SyncedQuoteId;
                        this.loadbenef(component,quota);
                    }
                }else{
                    if(responseCH.getReturnValue().Plan__c.includes('Titular')) {
                        component.set('v.options2',component.get('v.listopt2'));
                        component.set('v.Datos.MX_MainClientRelationship','Hijo');
                    }
                    if(responseCH.getReturnValue().Plan__c.includes('Familiar')) {
                        var quotat = responseCH.getReturnValue().SyncedQuoteId;
                        this.loadbenef(component,quotat);
                    }
                }
    },
    loadbenef : function(component,quota) {
        var action2= component.get("c.srchBenefs");
        action2.setParams({'quotId':quota});
        action2.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
                if(!JSON.stringify(response.getReturnValue()).includes('Cónyuge')) {
                    component.set('v.options2',component.get('v.listopt1'));
                    component.set('v.Datos.MX_MainClientRelationship','Cónyuge');
                }
                if(response.getReturnValue()===undefined) {
                    component.set('v.options2',component.get('v.listopt1'));
                    component.set('v.Datos.MX_MainClientRelationship','Cónyuge');
                }
                if(JSON.stringify(response.getReturnValue()).includes('Cónyuge')) {
                    component.set('v.options2',component.get('v.listopt2'));
                    component.set('v.Datos.MX_MainClientRelationship','Hijo');
                }
                if(JSON.stringify(response.getReturnValue()).includes('Hijo') &&
                   component.get('v.titulo').includes('Titular')) {
                    component.set('v.options2',component.get('v.listopt2'));
                    component.set('v.Datos.MX_MainClientRelationship','Hijo');
                }
                if(JSON.stringify(response.getReturnValue()).includes('Cónyuge') && component.get('v.titulo').includes('Familiar')) {
                    component.set('v.options2',component.get('v.listopt2'));
                    component.set('v.Datos.MX_MainClientRelationship','Hijo');
                }
                else {
                    component.set('v.options2',component.get('v.listopt3'));
                }
            }
            else {
                component.set('v.options2',component.get('v.listopt1'));
                component.set('v.Datos.MX_MainClientRelationship','Cónyuge');
            }
        });
        $A.enqueueAction(action2);
    },
    selListaH: function(component,event,helper) {
        component.set("v.visible2",false);
        this.quoteful(component,event,helper);
        var seccion = event.getParam("seccion");
        component.set('v.MaxClicks',event.getParam("MaxClicks"));
        if(component.get('v.allClicks')>=event.getParam("MaxClicks")) {
            component.set('v.availableBenfs',false);
        }
        if(component.get('v.allClicks')<event.getParam("MaxClicks")) {
            component.set('v.availableBenfs', true);
        }
        component.set("v.titulo",seccion);
        if(seccion!=="Individual" || component.get("v.titulo")==="Individual" ) {
            component.set("v.visible",true);
        } else {
            component.set("v.visible",false);
        }
        if(seccion.includes('Conyugal')) {
            component.set('v.options2', component.get('v.listopt1'));
            component.set('v.Datos.MX_MainClientRelationship','Cónyuge');
        }
        if(seccion.includes('Titular')) {
            component.set('v.options2',component.get('v.listopt2'));
            component.set('v.Datos.MX_MainClientRelationship','Hijo');
        }
        if(seccion.includes('Familiar')) {
            var quota = component.get('v.oppObj.SyncedQuoteId');
            this.loadbenef(component,quota);
        }
        component.set("v.visible2",true);
    },
    requiredFields: function(component) {
        let validFields = component.find("fieldToValidate");
        if(validFields.length!==undefined) {
            var allValid = validFields.reduce(function (validSoFar, inputCmp) {
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid;
            }, true);
            if (!allValid) {
                component.set('v.allFRequiredQ',false);
                this.toastRequiredFields();
            } else {
                component.set('v.allFRequiredQ',true);
            }
        }
    },
    toastRequiredFields : function () {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error:',
            message:'Porfavor llene los campos obligatorios.',
            duration:' 2000',
            key: 'info_alt',
            type: 'error'
        });
        toastEvent.fire();
    },
    eraseValues : function(component, event) {
        component.set('v.Datos.Id',null);
        component.set('v.Datos.FirstName','');
        component.set('v.Datos.LastName','');
        component.set('v.Datos.Apellido_materno','');
        component.set('v.Datos.PersonBirthdate','');
        component.set('v.Datos.MX_WB_txt_RFC','');
        component.set('v.Datos.MX_WB_txt_Clave_Texto','');
        component.set('v.Datos.CURP','');
        component.set('v.Datos.MX_MainClientRelationship','Cónyuge');
        component.set('v.visible',false);
        this.isVisible(component,event);
    },
    isVisible: function(component, event) {
        component.set('v.visible',true);
    },
    toastError : function (mensaje) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error:',
            message: 'Limite excedido,\n Porfavor seleccionar otro tipo de poliza',
            duration:' 2000',
            key: 'info_alt',
            type: 'error'
        });
        toastEvent.fire();
    },
    toastPlanCError: function() {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message: 'Favor de validar la informacion proporcionada',
            duration:' 2000',
            key: 'info_alt',
            type: 'error'
        });
        toastEvent.fire();
    },
    toastFAseg : function (mensaje) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Atención:',
            message: 'Favor de validar la informacion proporcionada',
            duration:' 2000',
            key: 'info_alt',
            type: 'error'
        });
        toastEvent.fire();
    },
    toastSuccessAd : function (component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message: 'El registro se ha actualizado correctamente',
            duration:' 2000',
            key: 'check',
            type: 'success'
        });
        toastEvent.fire();
    },
    validDateh : function(component,event,helper) {
        var dteT=new Date(component.get('v.Datos.PersonBirthdate'));
        var today = new Date();
        var time_diff=today-dteT;
        var EdadMaxHS=$A.get("$Label.c.MX_SB_PS_EdadHijoMax");
        var EdadMaxHI=parseInt(EdadMaxHS);
        var EdadMinCS=$A.get("$Label.c.MX_SB_PS_EdadConygMin");
        var EdadMinCI=parseInt(EdadMinCS);
        var val1 = time_diff -EdadMinCI;
        var valToEv=Math.floor(val1);
        var valToEv2= time_diff -EdadMaxHI;
        var constantS =$A.get("$Label.c.MX_SB_PS_Constante");
	var constantI=parseInt(constantS);
        var parentType= component.get('v.Datos.MX_MainClientRelationship');
        if(parentType.includes('Cónyuge') && valToEv>=0 && valToEv<=constantI) {
            component.set('v.disnot',false);
        }
        else if(parentType.includes('Cónyuge') && valToEv<0) {
            this.toastAlertaMa(component);
        }
            else if(parentType.includes('Cónyuge') && valToEv>constantI){
                this.toastAlertMaxAge(component);
            }
                else if(parentType.includes('Hijo') && valToEv2<=0) {
                    component.set('v.disnot',false);
                }
                    else if(parentType.includes('Hijo') && valToEv2>0) {
            this.toastAlertam(component);
        } else {
            component.set('v.disnot',false);
        }
    },
    toastAlertaMa : function (component) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Atención:',
            message: 'El cónyuge debe ser mayor de edad',
            duration:' 2000',
            key: 'info_alt',
            type: 'warning'
        });
        toastEvent.fire();
        component.set('v.disnot',true);
    },
    toastAlertam : function (component) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Atención:',
            message: 'El Hijo debe ser menor a 25 años',
            duration:' 2000',
            key: 'info_alt',
            type: 'warning'
        });
        toastEvent.fire();
        component.set('v.disnot',true);
    },
    toastAlertMaxAge : function (component) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Atención:',
            message: 'El Cónyuge debe ser menor a 70 años',
            duration:' 2000',
            key: 'info_alt',
            type: 'warning'
        });
        toastEvent.fire();
        component.set('v.disnot',true);
    },
    calcRfc: function (component,event) {
        let RFC;
        let ap_Paterno_F;
        let ap_Materno_F;
        let nombre_F;
        let ap_Paterno = component.get('v.Datos.LastName');
        let ap_Materno = component.get('v.Datos.Apellido_materno');
        let nombre = component.get('v.Datos.FirstName');
        let fNacimiento = component.get('v.Datos.PersonBirthdate');

        if(nombre && ap_Paterno && ap_Materno && fNacimiento) {
            fNacimiento = fNacimiento.substring(2,4)+fNacimiento.substring(5,7)+fNacimiento.substring(8,10);
            ap_Paterno_F = this.filtraAcentosRFC(component, ap_Paterno.toLowerCase());
            ap_Materno_F = this.filtraAcentosRFC(component, ap_Materno.toLowerCase());
            nombre_F = this.filtraAcentosRFC(component, nombre.toLowerCase());

            ap_Paterno_F = this.filtraNombresRFC(component, ap_Paterno_F);
            ap_Materno_F = this.filtraNombresRFC(component, ap_Materno_F);
            nombre_F = this.filtraNombresRFC(component, nombre_F);

            if (ap_Paterno_F.length > 0 && ap_Materno_F.length > 0) {
                if (ap_Paterno_F.length < 3) {
                    RFC = this.apellidoCortoRFC(component,ap_Paterno_F, ap_Materno_F, nombre_F);
                } else {
                    RFC = this.armaRFC(component,ap_Paterno_F, ap_Materno_F, nombre_F);
                }
            }
            if (ap_Paterno_F.length === 0 && ap_Materno_F.length > 0) {
                RFC = this.unApellidoRFC(component, ap_Materno_F, nombre_F);
            }
            if (ap_Paterno_F.length > 0 && ap_Materno_F.length === 0) {
                RFC = this.unApellidoRFC(component, ap_Paterno_F, nombre_F);
            }
            RFC = this.quitaProhibidasRFC(component,RFC);
            RFC = RFC + fNacimiento;
            component.set('v.Datos.MX_WB_txt_RFC',RFC);
        }
    },
    filtraAcentosRFC: function (component, nombreC_f) {
        nombreC_f = nombreC_f.replace('á', 'a');
        nombreC_f = nombreC_f.replace('é', 'e');
        nombreC_f = nombreC_f.replace('í', 'i');
        nombreC_f = nombreC_f.replace('ó', 'o');
        nombreC_f = nombreC_f.replace('ú', 'u');
        return nombreC_f;
    },
    filtraNombresRFC: function (component, nombreC_FN) {
        let i = 0;
        let preposiciones = [".", ",", "de ", "del ", "la ", "los ", "las ", "y ", "mc ", "mac ", "von ", "van "];
        for (i; i <= preposiciones.length; i++) {
            nombreC_FN = nombreC_FN.replace(preposiciones[i], "");
        }
        preposiciones = ["jose ", "maria ", "j ", "ma "];
        for (i=0; i <= preposiciones.length; i++) {
            nombreC_FN = nombreC_FN.replace(preposiciones[i], "");
        }
        switch (nombreC_FN.substr(0, 2)) {
            case 'ch':
                nombreC_FN = nombreC_FN.replace('ch', 'c')
                break;
            case 'll':

                nombreC_FN = nombreC_FN.replace('ll', 'l')
                break;
        }
        return nombreC_FN;
    },
    apellidoCortoRFC: function (component,a_paterno, a_materno, nombre_C) {
        return a_paterno.substr(0, 1) + a_materno.substr(0, 1) + nombre_C.substr(0, 2);
    },
    armaRFC: function (component,ap_paterno, ap_materno, nombre) {
        let vocales = 'aeiou';
        let primeraVocal = '';
        let i = 0;
        let x = 0;
        let y = 0;
        for (i = 1; i <= ap_paterno.length; i++) {
            if (y == 0) {
                for (x = 0; x <= vocales.length; x++) {
                    if (ap_paterno.substr(i, 1) === vocales.substr(x, 1)) {
                        y = 1;
                        primeraVocal = ap_paterno.substr(i, 1);
                    }
                }
            }
        }
        return ap_paterno.substr(0, 1) + primeraVocal + ap_materno.substr(0, 1) + nombre.substr(0, 1);
    },
    unApellidoRFC: function (component,apellido_UA, nombre_UA) {
        return apellido_UA.substr(0, 2) + nombre_UA.substr(0, 2);
    },
    quitaProhibidasRFC: function (component,rfc_QP) {
        let res;
        rfc_QP = rfc_QP.toUpperCase();
        let prohibidas = "BUEI*BUEY*CACA*CACO*CAGA*CAGO*CAKA*CAKO*COGE*COJA*";
        prohibidas = prohibidas + "KOGE*KOJO*KAKA*KULO*MAME*MAMO*MEAR*";
        prohibidas = prohibidas + "MEAS*MEON*MION*COJE*COJI*COJO*CULO*";
        prohibidas = prohibidas + "FETO*GUEY*JOTO*KACA*KACO*KAGA*KAGO*";
        prohibidas = prohibidas + "MOCO*MULA*PEDA*PEDO*PENE*PUTA*PUTO*";
        prohibidas = prohibidas + "QULO*RATA*RUIN*";
        res = prohibidas.match(rfc_QP);
        if (res != null) {
            rfc_QP = rfc_QP.substr(0, 3) + 'X';
            return rfc_QP;
        } else {
            return rfc_QP;
        }
    },
    addAsegH: function(component, event, helper,Ide) {
        var Datos= component.get('v.Datos');
        component.set('v.accObj.FirstName',Datos.FirstName);
        component.set('v.accObj.LastName',Datos.LastName);
        component.set('v.accObj.Apellido_materno__pc',Datos.Apellido_materno);
        component.set('v.accObj.PersonBirthdate',Datos.PersonBirthdate);
        component.set('v.accObj.RFC__c',Datos.MX_WB_txt_RFC);
        component.set('v.accObj.MX_MainClientRelationship__pc',Datos.MX_MainClientRelationship);
        component.set('v.accObj.MX_RTL_CURP__pc',Datos.CURP);
        component.set('v.accObj.MX_Gender__pc',Datos.MX_Gender);
        component.set('v.availableBenfs', true);
        var action5 = component.get('c.upsrtAcc');
        var pAcc= component.get('v.accObj');
        action5.setParams({'pAcc':pAcc});
        action5.setCallback(this, function(response) {
            component.set('v.benefObj.Id',Ide);
            component.set('v.benefObj.Name',Datos.FirstName);
            component.set('v.benefObj.MX_SB_VTS_APaterno_Beneficiario__c',Datos.LastName);
            component.set('v.benefObj.MX_SB_VTS_AMaterno_Beneficiario__c',Datos.Apellido_materno);
            component.set('v.benefObj.MX_SB_SAC_FechaNacimiento__c',Datos.PersonBirthdate);
            component.set('v.benefObj.MX_SB_SAC_RFC__c',Datos.MX_WB_txt_RFC);
            component.set('v.benefObj.MX_SB_PS_CURP__c',Datos.CURP);
            component.set('v.benefObj.MX_SB_SAC_Homoclave__c',Datos.MX_WB_txt_Clave_Texto);
            component.set('v.benefObj.MX_SB_VTS_Parentesco__c',Datos.MX_MainClientRelationship);
            component.set('v.benefObj.MX_SB_VTS_Quote__c',component.get('v.oppObj.SyncedQuoteId'));
            component.set('v.benefObj.MX_SB_MLT_NombreContacto__c',response.getReturnValue()[0].PersonContactId);
            if(Datos.MX_Gender==='Masculíno') {
                component.set('v.benefObj.MX_SB_SAC_Genero__c','Hombre');
            } else {
                component.set('v.benefObj.MX_SB_SAC_Genero__c',Datos.MX_Gender);
            }
            var benefObj= component.get('v.benefObj');
            var action6= component.get('c.insrtBenef');
            action6.setParams({'benefObj':benefObj});
            action6.setCallback(this, function(response) {
                component.set('v.refreshC',true);
                component.set('v.disnot',false);
                component.set('v.availableBenfs', true);
                var quota = component.get('v.oppObj.SyncedQuoteId');
                helper.loadbenef(component,quota);
                component.set('v.Datos.Id',null);
                component.set('v.wasEditEvent',false);
                component.set('v.labelB','Guardar asegurado');
            });
            $A.enqueueAction(action6);
            helper.eraseValues(component,event,helper);
            $A.get('e.force:refreshView').fire();
        });
        $A.enqueueAction(action5);
    },
    loadMaxClicks : function(component,event,helper) {
        switch (component.get('v.titulo')) {
            case 'Conyugal':
            case 'Titular y un hijo':
                this.availableBenefs(component,event,helper, '1');
                component.set('v.MaxClicks','1');
                break;
            case 'Familiar hasta 3 hijos':
                this.availableBenefs(component,event,helper,'4');
                component.set('v.MaxClicks','4');
                break;
            case 'Familiar hasta 5 hijos':
                component.set('v.MaxClicks','6');
                this.availableBenefs(component,event,helper,'6');
                break;
            case 'Titular hasta 3 hijos':
                this.availableBenefs(component, event, helper,'3');
                component.set('v.MaxClicks','3');
                break;
            case 'Titular hasta 5 hijos':
                this.availableBenefs(component, event, helper,'5');
                component.set('v.MaxClicks','5');
                break;
        }
    },
    availableBenefs : function(component,event,helper,allClicks) {
        var allClicks1="'"+allClicks+"'";
        var allClicks2= allClicks1.replaceAll("'","");
        if(allClicks>component.get('v.MaxClicks')) {
            component.set('v.availableBenfs',false);
            component.set('v.planBenefs',false);
            component.set('v.disableSig',true);
        }
        if(allClicks2===component.get('v.MaxClicks')) {
            component.set('v.availableBenfs',false);
            component.set('v.planBenefs',true);
            component.set('v.disableSig',false);
        }
        if(allClicks<component.get('v.MaxClicks')) {
            component.set('v.availableBenfs', true);
            component.set('v.planBenefs',true);
            component.set('v.disableSig',false);
        }
        if(component.get('v.titulo')==='Individual') {
            component.set('v.availableBenfs', false);
            component.set('v.planBenefs',false);
            component.set('v.disableSig',false);
        }
    },
    helperCond : function(component,event,helper) {
        this.helperCondicionF(component,event,helper);
        this.helperCondicionT(component,event,helper);
    },
    helperCondicionT : function(component,event,helper) {
        var allClicksN=component.get('v.allClicks');
        var allClicksS="'"+allClicksN+"'";
		var allClicksSI= allClicksS.replaceAll("'","");
        if(component.get('v.allClicks')< component.get('v.MaxClicks') && component.get('v.wasEditEvent')===false )  {
            component.set('v.Datos.Id',null);
            this.addAsegH(component,event,helper,null);
            this.toastSuccessAd(component,event,helper);
        }
        if(allClicksSI === component.get('v.MaxClicks')
           && component.get('v.wasEditEvent')===true) {
            var Ide=component.get('v.Datos.Id');
            this.addAsegH(component,event,helper,Ide);
            this.toastSuccessAd(component,event,helper);
        }
    },
    helperCondicionF : function(component,event,helper) {
        var allClicks=component.get('v.allClicks');
        var allClicksS="'"+allClicks+"'";
        var allClicksS2I= allClicksS.replaceAll("'","");
        if(component.get('v.allClicks')> component.get('v.MaxClicks')
           && component.get('v.wasEditEvent')===false) {
            this.toastError(component,event);
        }
        if(allClicksS2I === component.get('v.MaxClicks')
           && component.get('v.wasEditEvent')===false) {
            this.toastError(component,event);
        }
        if(component.get('v.allClicks')< component.get('v.MaxClicks') && component.get('v.wasEditEvent')===true )  {
            var Ide=component.get('v.Datos.Id');
            this.addAsegH(component,event,helper,Ide);
            this.toastSuccessAd(component,event,helper);
        }
    },
    eventFire : function(component, event, helper) {
        var cmpEvent =$A.get("e.c:MX_SB_PS_EventValidData");
        cmpEvent.setParams({"continuar" : true, "nextStageName" : $A.get("$Label.c.MX_SB_PS_Etapa3")});
        cmpEvent.fire();
    },
    siguienteHValidate1: function(component, event, helper) {
        if(component.get('v.titulo')==='Individual' ||
           component.get('v.titulo')==='Conyugal' ||
           component.get('v.titulo')==='Titular y un hijo' ) {
            this.eventFire(component, event, helper);
        }
        if(component.get('v.titulo')==='Titular hasta 3 hijos'
           && component.get('v.allClicks')<2 ) {
            this.toastMalaVenta(component, event, helper);
        }
        if(component.get('v.titulo')==='Titular hasta 3 hijos'
           && component.get('v.allClicks')>=2) {
            this.eventFire(component, event, helper);
        }
    },
    siguienteHValidate2: function(component, event, helper) {
        if(component.get('v.titulo')==='Familiar hasta 3 hijos'
           && component.get('v.allClicks')<2 ) {
            this.toastMalaVenta(component, event, helper);
        }
        if(component.get('v.titulo')==='Familiar hasta 3 hijos'
           && component.get('v.allClicks')>=2) {
            this.eventFire(component, event, helper);
        }
        if(component.get('v.titulo')==='Titular hasta 5 hijos'
           && component.get('v.allClicks')<4 ) {
            this.toastMalaVenta(component, event, helper);
        }
    },
    siguienteHValidate3: function(component, event, helper) {
        if(component.get('v.titulo')==='Titular hasta 5 hijos'
           && component.get('v.allClicks')>=4) {
            this.eventFire(component, event, helper);
        }
        if(component.get('v.titulo')==='Familiar hasta 5 hijos'
           && component.get('v.allClicks')<5 ) {
            this.toastMalaVenta(component, event, helper);
        }
        if(component.get('v.titulo')==='Familiar hasta 5 hijos'
           && component.get('v.allClicks')>=5) {
            this.eventFire(component, event, helper);
        }
    },
    siguienteH : function(component, event, helper) {
        if(component.get('v.allClicks')<=component.get('v.MaxClicks')) {
            this.siguienteHValidate1(component, event, helper);
            this.siguienteHValidate2(component, event, helper);
            this.siguienteHValidate3(component, event, helper);
        }
        if(component.get('v.allClicks')===0 && component.get('v.MaxClicks')===undefined) {
            this.eventFire(component, event, helper);
        }
    },
    toastMalaVenta : function (component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: 'Se ha detectado detectado una mala venta',
            message: 'Favor de cambiar al plan correspondiente a la información proporcionada',
            duration:' 2000',
            key: 'warning',
            type: 'warning'
        });
        toastEvent.fire();
    },
    casuisticas:function(component,event,helper,jStResponse) {
        if(jStResponse!=='[]' && component.get('v.titulo')!=='Individual') {
            this.conyugal(component,event,helper,jStResponse);
            this.otrasCasuisticas(component,event,helper,jStResponse);
        }
        if(jStResponse!=='[]' && component.get('v.titulo')==='Individual') {
            this.toastPlanCError(component,event);
        }
        if(component.get('v.titulo')==='Individual' && jStResponse==='[]' ) {
            this.siguienteH(component,event,helper);
        }
        if(jStResponse==='[]' && component.get('v.titulo')!=='Individual') {
            this.toastPlanCError(component,event);
        }
    },
    conyugal: function(component,event,helper,jStResponse) {
        if(jStResponse.includes('Cónyuge')
           && component.get('v.titulo')==='Conyugal') {
            this.siguienteH(component,event,helper);
        }
        if(jStResponse.includes('Hijo')
           && component.get('v.titulo')==='Conyugal') {
            this.toastPlanCError(component,event);
        }
        if(jStResponse.includes('Cónyuge')
           && component.get('v.titulo').includes('Titular')) {
            this.toastPlanCError(component,event);
        }
        if(jStResponse.includes('Hijo') && !jStResponse.includes('Cónyuge')
           && component.get('v.titulo').includes('Titular')) {
            this.siguienteH(component,event,helper);
        }
    },
    otrasCasuisticas: function(component,event,helper,jStResponse) {
        if(jStResponse.includes('Cónyuge')
           && component.get('v.titulo').includes('Familiar')) {
            this.siguienteH(component,event,helper);
        }
        if(!jStResponse.includes('Cónyuge')
           && component.get('v.titulo').includes('Titular')) {
            this.siguienteH(component,event,helper);
        }
        if(component.get('v.titulo')==='Individual'
           && (!jStResponse.includes('Cónyuge')
               || !jStResponse.includes('Hijo'))) {
            this.siguienteH(component,event,helper);
        }
        if(jStResponse.includes('Hijo')
           && !jStResponse.includes('Cónyuge')
           && component.get('v.titulo').includes('Familiar')) {
            this.toastPlanCError(component,event);
        }
    },
    quoteful: function (component,event,helper) {
        var quote = event.getParam("quoteLineI");
        var foliocot1 = event.getParam("foliocotizacion");
        var psubse1 = event.getParam("pagossubse");
        component.set('v.quoteLineIt', quote);
        component.set('v.foliocot', foliocot1);
        component.set('v.psubse1', psubse1);
    }
})