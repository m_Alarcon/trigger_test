({
    doInitC : function(component, event, helper) {
        helper.doInitCH(component,event, helper);
    },
    addAsg : function(component, event, helper) {
        helper.requiredFields(component,event,helper);
        if(component.get('v.allFRequiredQ')===true) {
            component.set('v.disnot',true);
            helper.loadMaxClicks(component,event,helper);
            helper.helperCond(component,event,helper);
            }
    },
    setParentType: function(component, event, helper) {
        var parentVal= component.get("v.Datos.MX_MainClientRelationship");
        component.set("v.Datos.MX_MainClientRelationship",parentVal);
        helper.validDateh(component,event,helper);
    },
    selLista : function(component, event, helper) {
        helper.selListaH(component,event,helper);
    },
    caps1 : function(component, event, helper) {
        var rfc = component.get('v.Datos.MX_WB_txt_RFC');
        component.set('v.Datos.MX_WB_txt_RFC',rfc.replace(/[^\w\s]/gi,'').toUpperCase());
    },
    caps2 : function(component, event, helper) {
        var homoClave = component.get('v.Datos.MX_WB_txt_Clave_Texto');
        component.set('v.Datos.MX_WB_txt_Clave_Texto',homoClave.replace(/[^\w\s]/gi,'').toUpperCase());
    },
    caps3 : function(component, event, helper) {
        var curp = component.get('v.Datos.CURP');
        component.set('v.Datos.CURP',curp.replace(/[^\w\s]/gi,'').toUpperCase());
    },
    nameU : function(component, event, helper) {
        var nameu= component.get('v.Datos.FirstName');
        component.set('v.Datos.FirstName',nameu.replace(/[^\ÑA-Za-zñ\s]/gi,'').toUpperCase());
        helper.calcRfc(component,event);
    },
    apatU : function(component, event, helper) {
        var apatu= component.get('v.Datos.LastName');
        component.set('v.Datos.LastName',apatu.replace(/[^\ÑA-Za-zñ\s]/gi,'').toUpperCase());
        helper.calcRfc(component,event);
    },
    amatU : function(component, event, helper) {
        var amatu= component.get('v.Datos.Apellido_materno');
        component.set('v.Datos.Apellido_materno',amatu.replace(/[^\ÑA-Za-zñ\s]/gi,'').toUpperCase());
        helper.calcRfc(component,event);
    },
    eraseValues : function(component, event, helper) {
        helper.eraseValues(component, event, helper);
        component.set('v.refreshC',true);
    },
    validDate : function(component, event, helper) {
        helper.validDateh(component, event, helper);
        helper.calcRfc(component,event);
    },
    eventDel : function(component, event, helper) {
        var evento = event.getParam("info");
        var plan = component.get('v.oppObj.Plan__c');
        component.set('v.disableSig',false);
        if(evento.includes('Cónyuge') && plan.includes('Familiar')) {
            component.set('v.options2',component.get('v.listopt2'));
            component.set('v.Datos.MX_MainClientRelationship','Hijo');
        }
        if(!evento.includes('Cónyuge') && plan.includes('Familiar')) {
            component.set('v.options2',component.get('v.listopt1'));
            component.set('v.Datos.MX_MainClientRelationship','Cónyuge');
        }
        if(evento.includes('Hijo') && plan.includes('Titular')) {
            component.set('v.options2',component.get('v.listopt2'));
            component.set('v.Datos.MX_MainClientRelationship','Hijo');
        }
    },
    editHandler : function(component, event, helper) {
	  component.set('v.availableBenfs',true);
      	  component.set('v.labelB','Actualizar');
	  var eventId = event.getParam("benefId");
	  var wasEdit= event.getParam('wasEditEvent');
        component.set('v.wasEditEvent',wasEdit);
        var action2= component.get("c.getbenefbyId");
        action2.setParams({'value':eventId});
        action2.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
				component.set('v.Datos.Id',response.getReturnValue()[0].Id);
                component.set('v.Datos.FirstName',response.getReturnValue()[0].Name);
                component.set('v.Datos.LastName',response.getReturnValue()[0].MX_SB_VTS_APaterno_Beneficiario__c);
                component.set('v.Datos.Apellido_materno',response.getReturnValue()[0].MX_SB_VTS_AMaterno_Beneficiario__c);
                component.set('v.Datos.PersonBirthdate',response.getReturnValue()[0].MX_SB_SAC_FechaNacimiento__c);
                component.set('v.Datos.MX_WB_txt_RFC',response.getReturnValue()[0].MX_SB_SAC_RFC__c);
                component.set('v.Datos.MX_WB_txt_Clave_Texto',response.getReturnValue()[0].MX_SB_SAC_Homoclave__c);
                component.set('v.Datos.MX_MainClientRelationship',response.getReturnValue()[0].MX_SB_VTS_Parentesco__c);
                component.set('v.Datos.CURP',response.getReturnValue()[0].MX_SB_PS_CURP__c);
                component.set('v.Datos.MX_Gender',response.getReturnValue()[0].MX_SB_SAC_Genero__c);
                if(response.getReturnValue()[0].MX_SB_VTS_Parentesco__c==='Cónyuge') {
					component.set('v.options2',component.get('v.listopt1'));
                    component.set('v.Datos.MX_MainClientRelationship','Cónyuge');
                }
                if(response.getReturnValue()[0].MX_SB_VTS_Parentesco__c==='Hijo') {
                    component.set('v.options2',component.get('v.listopt2'));
                }
            }
        });
        $A.enqueueAction(action2);
    },
    siguiente: function(component, event,helper) {
	helper.doInitCH(component,event, helper);
	var quota = component.get('v.oppObj.SyncedQuoteId');
        var action2= component.get("c.srchBenefs");
        action2.setParams({'quotId':quota});
        action2.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
                var jStResponse= JSON.stringify(response.getReturnValue());
                helper.casuisticas(component,event,helper,jStResponse);
            }
        });
        $A.enqueueAction(action2);
    },
    availableBenefs : function(component,event,helper) {
        helper.availableBenefs(component,event,helper);
    },
    changedPlan: function(component, event, helper) {
        var action7=component.get('c.getintN');
            action7.setParams({'quotId':component.get('v.oppObj.SyncedQuoteId')});
            action7.setCallback(this, function(responseAC) {
                component.set('v.allClicks',responseAC.getReturnValue());
                helper.availableBenefs(component, event,helper,responseAC.getReturnValue());
                component.set('v.refreshC',false);
            });
            $A.enqueueAction(action7);
    },
})