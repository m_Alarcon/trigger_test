({
	doInit : function(component, event, helper) {
        helper.LoadSiniestroData(component, event,helper);
        helper.datosIni(component,event,helper);
		helper.getRecordGrua(component, event,helper);
        helper.getRecordGasolina(component, event,helper);
        helper.getRecordCambioLlanta(component, event,helper);
        helper.getRecordPasoCorriente(component, event,helper);
	},
    comparaDate : function(component,event,helper) {
        helper.comparaDate(component,event,helper);
    },
      changeState : function(component,event,helper) {
   		component.set('v.isexpanded',!component.get('v.isexpanded'));
 },
    buscaAjustadorGas :function(component) {
        component.set('v.tipoBusqueda','Proveedor de Gasolina');
        component.set("v.busquedaVisible",true);
    },
    buscaAjustadorLlanta :function(component) {
        component.set('v.tipoBusqueda','Proveedor de Cambio de llanta');
        component.set("v.busquedaVisible",true);
    },
    buscaAjustadorCorriente :function(component) {
        component.set('v.tipoBusqueda','Proveedor de Paso de corriente');
        component.set("v.busquedaVisible",true);
    },
    buscaAjustadorGrua :function(component) {
        component.set('v.tipoBusqueda','Proveedor de Grúa');
        component.set("v.busquedaVisible",true);
    },
    cierreBusqueda : function(component,event) {
        var nombre = event.getParam("nombre");
        var idSales = event.getParam("idSalesforce");
        var idClaim = event.getParam("idClaim");
        var tipo = component.get('v.tipoBusqueda');
        switch (tipo) {
            case 'Proveedor de Grúa':
                component.set('v.ajustadorNombre',nombre);
                component.set('v.ajustadorSFId',idSales);
                component.set('v.ajustadorCCId',idClaim);
                break;
            case 'Proveedor de Cambio de llanta':
                component.set('v.ajustadorNombre2',nombre);
                component.set('v.ajustadorSFId2',idSales);
                component.set('v.ajustadorCCId2',idClaim);
                break;
            case 'Proveedor de Paso de corriente':
                component.set('v.ajustadorNombre3',nombre);
                component.set('v.ajustadorSFId3',idSales);
                component.set('v.ajustadorCCId3',idClaim);
                break;
           case 'Proveedor de Gasolina':
                component.set('v.ajustadorNombre4',nombre);
                component.set('v.ajustadorSFId4',idSales);
                component.set('v.ajustadorCCId4',idClaim);
                break;
        }
        component.set("v.busquedaVisible",false);
    },
    toggleGrua: function(component, event, helper) {
        var changeValue = component.get("v.gruaRadio");
        if(changeValue==='true') {
            component.set('v.Grua.MX_SB_MLT_Reembolso__c',true);
        } else {
            component.set('v.Grua.MX_SB_MLT_Reembolso__c',false);
        }
    },
    toggleLlanta: function(component, event, helper) {
        var changeValue = component.get("v.llantaRadio");
        if(changeValue==='true') {
            component.set('v.CambioLlanta.MX_SB_MLT_Reembolso__c',true);
        } else {
            component.set('v.CambioLlanta.MX_SB_MLT_Reembolso__c',false);
        }
    },
    toggleGasolina: function(component, event, helper) {
        var changeValue = component.get("v.gasolinaRadio");
        if(changeValue==='true') {
            component.set('v.Gasolina.MX_SB_MLT_Reembolso__c',true);
        } else {
            component.set('v.Gasolina.MX_SB_MLT_Reembolso__c',false);
        }
    },
    toggleCorriente: function(component, event, helper) {
        var changeValue = component.get("v.corrienteRadio");
        if(changeValue==='true') {
            component.set('v.PasoCorriente.MX_SB_MLT_Reembolso__c',true);
        } else {
            component.set('v.PasoCorriente.MX_SB_MLT_Reembolso__c',false);
        }
    },
    updateEnvio: function(component,event,helper) {
       component.set('v.enviar',true);
       helper.validacionFormulario(component,event,helper);
    },
    updateNoEnvio: function(component,event,helper) {
       component.set('v.enviar',false);
       helper.validacionFormulario(component,event,helper);
    },
    updateSin: function(component, event, helper) {
        helper.LoadSiniestroData(component, event,helper);
        var GruaData = component.get("v.Grua");
        var SiniestroData = component.get("v.SiniObj");
        var CambioLlantaData = component.get("v.CambioLlanta");
        var PasoCorrienteData = component.get("v.PasoCorriente");
        var GasolinaData = component.get("v.Gasolina");
        var GruaId = component.get("v.Grua.Id");
        var PasoCorrienteId = component.get("v.PasoCorriente.Id");
        var GasolinaId = component.get("v.Gasolina.Id");
		var CambioLlantaId = component.get("v.CambioLlanta.Id");
        var enviarFlag = component.get('v.enviar');
        if(GruaId!=null) {
        var ProveedorGrua= component.get("v.ajustadorSFId");
		var action = component.get("c.updateCaseGrua");
            action.setParams({'caseId':GruaId,'idProveedor':ProveedorGrua,'strSinData':SiniestroData,'objGrua':GruaData,'enviar':enviarFlag});
        	action.setCallback(this, function(response) {
            var state = response.getState();
                helper.resultado(component,event,helper,state);
        		} );
        $A.enqueueAction(action);
            }
        if(CambioLlantaId!=null) {
        var ProveedorCamLl= component.get("v.ajustadorSFId2");
        var action2 = component.get("c.updateCaseCambioLL");
            action2.setParams({'caseId':CambioLlantaId,'idProveedor':ProveedorCamLl,'strSinData':SiniestroData,'objCL':CambioLlantaData,'enviar':enviarFlag});
        	action2.setCallback(this, function(response) {
            var state2 = response.getState();
                 helper.resultado(component,event,helper,state2);
        		} );
        		$A.enqueueAction(action2);
        }
        if(PasoCorrienteId!=null) {
        var ProveedorPasoC= component.get("v.ajustadorSFId3");
  		var action3 = component.get("c.updateCasePasoC");
            action3.setParams({'caseId':PasoCorrienteId,'idProveedor':ProveedorPasoC,'strSinData':SiniestroData,'objPasoC':PasoCorrienteData,'enviar':enviarFlag});
        	action3.setCallback(this, function(response) {
            var state3 = response.getState();
            helper.resultado(component,event,helper,state3);
        		} );
        		$A.enqueueAction(action3);
        	}
        if(GasolinaId!=null) {
        var ProveedorGas= component.get("v.ajustadorSFId4");
        var action4 = component.get("c.updateCaseGasolina");
        action4.setParams({'caseId':GasolinaId,'idProveedor':ProveedorGas,'strSinData':SiniestroData,'objGas':GasolinaData,'enviar':enviarFlag});
        action4.setCallback(this, function(response) {
            var state4 = response.getState();
            helper.resultado(component,event,helper,state4);
        		} );
        $A.enqueueAction(action4);
        }
    }
})