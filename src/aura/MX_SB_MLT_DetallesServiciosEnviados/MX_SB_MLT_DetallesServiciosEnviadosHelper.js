({
	getRecordGrua : function(compRecGrua,event, helper) {
        var recordId = compRecGrua.get("v.recordId");
		var action = compRecGrua.get("c.getGruaWO");
        action.setParams({'sinid':recordId});
        action.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
                 if(response.getReturnValue().MX_SB_MLT_ClaimId__c!=null) {
                	compRecGrua.set('v.serviciosEnviadosGrua',true);
                    compRecGrua.set('v.Grua',null);
                 } else {
                     compRecGrua.set("v.Grua",response.getReturnValue());
                     if(response.getReturnValue().MX_SB_MLT_Reembolso__c===true) {
                        compRecGrua.set('v.gruaRadio','true');
                    } else {
                        compRecGrua.set('v.Grua.MX_SB_MLT_Reembolso__c',false);
                    }
                    if(response.getReturnValue().AccountId!=null) {
                        compRecGrua.set("v.ajustadorSFId",response.getReturnValue().AccountId);
            		   compRecGrua.set("v.ajustadorNombre",response.getReturnValue().Account.Name);
            		   compRecGrua.set("v.ajustadorCCId",response.getReturnValue().Account.MX_SB_MLT_ClaimCenterId__c);
                    }
                 }
            } else {
                compRecGrua.set('v.Grua',null);
            }
        });
        $A.enqueueAction(action);
	},
    getRecordGasolina : function(compRecGas,event) {
        var recordId = compRecGas.get("v.recordId");
        var action = compRecGas.get("c.getGasWO");
        action.setParams( { 'sinid':recordId } );
        action.setCallback(this, function(response) {
           if(response.getState()==='SUCCESS') {
               if(response.getReturnValue().MX_SB_MLT_ClaimId__c!=null) {
                	compRecGas.set('v.serviciosEnviadosGas',true);
                   compRecGas.set('v.Gasolina',null);
               } else {
                   compRecGas.set("v.Gasolina",response.getReturnValue());
                   if(response.getReturnValue().MX_SB_MLT_Reembolso__c===true) {
                       compRecGas.set('v.gasolinaRadio','true');
                   } else {
                       compRecGas.set('v.Gasolina.MX_SB_MLT_Reembolso__c',false);
                   }
                   if(response.getReturnValue().AccountId!=null) {
                       compRecGas.set("v.ajustadorSFId4",response.getReturnValue().AccountId);
            		   compRecGas.set("v.ajustadorNombre4",response.getReturnValue().Account.Name);
            		   compRecGas.set("v.ajustadorCCId4",response.getReturnValue().Account.MX_SB_MLT_ClaimCenterId__c);
                   }
               }
           } else {
                compRecGas.set('v.Gasolina',null);
            }
        });
        $A.enqueueAction(action);
    },
    getRecordCambioLlanta : function(compRecCmbLlanta,event) {
        var recordId = compRecCmbLlanta.get("v.recordId");
        var action = compRecCmbLlanta.get("c.getCambioLLWO");
        action.setParams( { 'sinid':recordId } );
        action.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
                 if(response.getReturnValue().MX_SB_MLT_ClaimId__c!=null) {
                	compRecCmbLlanta.set('v.serviciosEnviadosLlanta',true);
                     compRecCmbLlanta.set('v.CambioLlanta',null);
                 } else {
                     compRecCmbLlanta.set("v.CambioLlanta",response.getReturnValue());
                     if(response.getReturnValue().MX_SB_MLT_Reembolso__c===true) {
                          compRecCmbLlanta.set('v.llantaRadio','true');
                     } else {
                         compRecCmbLlanta.set('v.CambioLlanta.MX_SB_MLT_Reembolso__c',false);
                     }
                     if(response.getReturnValue().AccountId!=null) {
                         compRecCmbLlanta.set("v.ajustadorSFId2",response.getReturnValue().AccountId);
                     	 compRecCmbLlanta.set("v.ajustadorNombre2",response.getReturnValue().Account.Name);
                     	 compRecCmbLlanta.set("v.ajustadorCCId2",response.getReturnValue().Account.MX_SB_MLT_ClaimCenterId__c);
                     }
                 }
            } else {
                compRecCmbLlanta.set('v.CambioLlanta',null);
            }
        });
        $A.enqueueAction(action);
    },
    getRecordPasoCorriente : function(compPasCorr,event) {
        var recordId = compPasCorr.get("v.recordId");
        var action = compPasCorr.get("c.getPasoCWO");
        action.setParams( { 'sinid':recordId } );
        action.setCallback(this, function(response) {
             if(response.getState()==='SUCCESS') {
                 if(response.getReturnValue().MX_SB_MLT_ClaimId__c!=null) {
                	 compPasCorr.set('v.serviciosEnviadosCorr',true);
                     compPasCorr.set('v.PasoCorriente',null);
                 } else {
                     compPasCorr.set("v.PasoCorriente",response.getReturnValue());
                     if(response.getReturnValue().MX_SB_MLT_Reembolso__c===true) {
                          compPasCorr.set('v.corrienteRadio','true');
                     } else {
                         compPasCorr.set('v.PasoCorriente.MX_SB_MLT_Reembolso__c',false);
                     }
                     if(response.getReturnValue().AccountId!=null) {
                         compPasCorr.set("v.ajustadorSFId3",response.getReturnValue().AccountId);
                     	 compPasCorr.set("v.ajustadorNombre3",response.getReturnValue().Account.Name);
                     	 compPasCorr.set("v.ajustadorCCId3",response.getReturnValue().Account.MX_SB_MLT_ClaimCenterId__c);
                     }
                 }
            } else {
                compPasCorr.set('v.PasoCorriente',null);
            }
        });
        $A.enqueueAction(action);
    },
    LoadSiniestroData : function(component,event) {
        var action = component.get("c.getSiniestroD");
         action.setParams( { "siniId": component.get("v.recordId") } );
         action.setCallback(this, function(response) {
            var state = response.getState();
             var toastEvent = $A.get("e.force:showToast");
            if (state === "SUCCESS") {
                var responseStudentRecord = response.getReturnValue();
                component.set("v.SiniObj", responseStudentRecord);
                if("v.SiniObj.MX_SB_MLT_Fecha_Hora_Siniestro__c"!='') {
					component.set("v.LugarAtencionValue", 'Cita');
                }
                else {
                    component.set("v.LugarAtencionValue", 'Crucero');
                }
            } else if (state === "INCOMPLETE") {
                toastEvent.setParams( {
                    "title": "OFFLINE!",
                    "message": "You are in offline."
                } );
                toastEvent.fire();
            }else if (state === "ERROR") {
                var errors = response.getError();
                toastEvent.setParams( {
                    "title": "ERROR!",
                    "message": errors[0].message
                } );
                toastEvent.fire();
            } else {
                toastEvent.setParams( {
                    "title": "UNKOWN!",
                    "message": "Unknown error."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
        comparaDate : function(component,event,helper) {
        var Fecha2= new Date(component.find("Fecha").get("v.value"));
        var Hoy= new Date();
       	var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams( {
                title : 'Error Message',
                message:'Fecha de Siniestro no Valida',
                messageTemplate: 'Porfavor ingrese una fecha y horario valido',
                duration:' 3000',
                key: 'info_alt',
                type: 'error',
                mode: 'pester'
            } );
        var showtoast = false;
        if(Fecha2.getFullYear()<Hoy.getFullYear()) {
			showtoast=true;
        } else {
            if(Fecha2.getMonth()<Hoy.getMonth()) {
            	showtoast=true;
            } else {
                if(Fecha2.getDate()<Hoy.getDate()) {
                    showtoast=true;
                }
            }
        }
        if (showtoast) {
            toastEvent.fire();
            component.set("v.SiniObj.MX_SB_MLT_Fecha_Hora_Siniestro__c","");
        }
    },
    validacionFormulario : function(component,event,helper) {
        var enviar = true;
        var rsMethod =[];
        var mensaje = '';
        var llanta = component.get('v.CambioLlanta');
        var corriente = component.get('v.PasoCorriente');
        var grua = component.get('v.Grua');
        var gas = component.get('v.Gasolina');
        if(grua!=null){
            rsMethod = this.evaluaGrua(component,event,helper);
            if(rsMethod.write===true) {
                enviar=rsMethod.enviar;
                mensaje=rsMethod.mensaje;
            }
        }
        if(llanta!=null && enviar) {
            rsMethod = this.evaluaLlanta(component,event,helper);
            if(rsMethod.write===true) {
                enviar=rsMethod.enviar;
                mensaje=rsMethod.mensaje;
            }
        }
        if(corriente!=null && enviar) {
            rsMethod = this.evaluaCoriente(component,event,helper);
            if(rsMethod.write===true) {
                enviar=rsMethod.enviar;
                mensaje=rsMethod.mensaje;
            }
        }
        if(gas!=null && enviar) {
            rsMethod = this.evaluaGas(component,event,helper);
            if(rsMethod.write===true) {
                enviar=rsMethod.enviar;
                mensaje=rsMethod.mensaje;
            }
        }
        helper.actualizaSin(component,event,helper,enviar,mensaje);
    },
    datosIni : function(component,event,helper) {
        var hoy = new Date();
        var inicio = new Date(hoy.getTime()+90*60000);
        var fechaIni = $A.localizationService.formatDate(inicio, "yyyy-MM-dd");
        var horaIni=$A.localizationService.formatDate(inicio, "HH:mm:ssZ");
        var minimot = fechaIni+'T'+horaIni;
        var fin = new Date();
        fin.setDate(hoy.getDate()+30);
        var fechaFin =  $A.localizationService.formatDate(fin, "yyyy-MM-dd");
        var maximot = fechaFin+'T23:59:00';
        component.set('v.fechafin',maximot);
        component.set('v.fechaini',minimot);
    },
    resultado :function(component,event,helper,state) {
         var enviarFlag = component.get('v.enviar');
         var toastEvent = $A.get("e.force:showToast");
        if (state === "SUCCESS") {
                toastEvent.setParams( {
                    "title": "Enhorabuena!",
                    "message": "Se ha actualizado el caso correctamente."
                } );
                if(enviarFlag) {
                    window.location.reload();
                }
            } else {
                    toastEvent.setParams( {
                    "title": "Error!",
                    "message": "Problemas  al actualizar caso correctamente."
                } );
                }
        toastEvent.fire();
    },
    evaluaGas : function(component,event,helper) {
        var returnvalsGas = {'write':false,'enviar':false,'mensaje':''};
        var gas = component.get('v.Gasolina');
            if(gas.id!=='' &&  gas.MX_SB_MLT_ClaimId__c==null) {
                if(component.get('v.gasolinaRadio')==='true') {
                    var gasCheck = component.find('costoGas');
                	if(gasCheck.get('v.validity').valid===false) {
                        returnvalsGas.enviar=false;
                        returnvalsGas.mensaje='Se requiere el costo del reembolso de gasolina';
                        returnvalsGas.write=true;
                	}
                } else {
                    var proveedorGas = component.get('v.ajustadorSFId4');
                    if(proveedorGas==null) {
                        returnvalsGas.enviar=false;
                        returnvalsGas.mensaje='Se requiere la selección de un Proveedor para la asistencia de Gasolina';
                        returnvalsGas.write=true;
                    }
                }
            }
        return returnvalsGas;
    },
    evaluaCoriente : function(component,event,helper) {
        var returnvalsCorriente ={'write':false,'enviar':false,'mensaje':''};
        var corriente = component.get('v.PasoCorriente');
        if(corriente!==null){
            if(corriente.id!=='' &&  corriente.MX_SB_MLT_ClaimId__c==null && component.get('v.corrienteRadio')==='true') {
                var corrCheck = component.find('costoCorriente');
                if(corrCheck.get('v.validity').valid===false) {
                        returnvalsCorriente.enviar=false;
                        returnvalsCorriente.mensaje='Se requiere el costo del reembolso del paso de corriente';
                        returnvalsCorriente.write=true;
                }
            } else {
                var proveedorCorr = component.get('v.ajustadorSFId3');
                if(proveedorCorr==null) {
                    returnvalsCorriente.enviar=false;
                    returnvalsCorriente.mensaje='Se requiere la selección de un Proveedor para la asistencia de Paso de Corriente';
                    returnvalsCorriente.write=true;
                }
            }
        }
        return returnvalsCorriente;
    },
    evaluaLlanta : function(component,event,helper) {
        var returnvalsLlanta = {'write':false,'enviar':false,'mensaje':''};
        var llanta = component.get('v.CambioLlanta');
            if(llanta.id!=='' &&  llanta.MX_SB_MLT_ClaimId__c==null && component.get('v.llantaRadio')==='true') {
                var llantaCheck = component.find('costoLlanta');
                if(llantaCheck.get('v.validity').valid===false) {
                            returnvalsLlanta.enviar=false;
                            returnvalsLlanta.mensaje='Se requiere el costo del reembolso del cambio de llanta';
                            returnvalsLlanta.write=true;
               }
             } else {
                        var proveedorLlanta = component.get('v.ajustadorSFId2');
                        if(proveedorLlanta==null) {
                            returnvalsLlanta.enviar=false;
                            returnvalsLlanta.mensaje='Se requiere la selección de un Proveedor para la asistencia de Cambio de Llanta';
                            returnvalsLlanta.write=true;
                        }
             }
        return returnvalsLlanta;
    },
    evaluaGrua : function(component,event,helper) {
        var returnvalsGrua = {'write':false,'enviar':false,'mensaje':''};
        var grua = component.get('v.Grua');
        if(grua!=null) {
            if(grua.MX_SB_MLT_ClaimId__c==null) {
                if(component.get('v.gruaRadio')==='true') {
                     var gruaCheck = component.find('costoGrua');
                    if(gruaCheck.get('v.validity').valid===false) {
                        returnvalsGrua.enviar=false;
                        returnvalsGrua.mensaje='Se requiere el costo del reembolso de la Grua';
                        returnvalsGrua.write=true;
                    }
                } else {
                    var arrayfechagrua = this.evaluaFechaGrua(component,event,helper);
                    if(arrayfechagrua.write===true) {
                        returnvalsGrua=arrayfechagrua;
                    }
                }
            }
        }
        return returnvalsGrua;
    },
    evaluaFechaGrua : function(component,event,helper){
        var returnvalsFG ={'write':false,'enviar':false,'mensaje':''};
        var fechaGrua = this.methodFCita(component,event,helper);
        if(fechaGrua.write===true){
            returnvalsFG=fechaGrua;
        }
        var proveedorGrua = component.get('v.ajustadorSFId');
        if(proveedorGrua==null) {
            returnvalsFG.enviar=false;
            returnvalsFG.mensaje='Se requiere la selección de un Proveedor para la asistencia de Grua';
            returnvalsFG.write=true;
        }
        return returnvalsFG;
    },
    methodFCita : function (component,event,helper) {
        var returnFechaGrua ={'write':false,'enviar':false,'mensaje':''};
        var atencion=component.get('v.LugarAtencionValue');
        if(atencion==='Cita') {
            var fechaObj = component.find("fechaInput");
            if(fechaObj.get('v.value')==='') {
                returnFechaGrua.enviar=false;
                returnFechaGrua.mensaje='La fecha esta vacia';
                returnFechaGrua.write=true;
            }  else if(fechaObj.get('v.validity').valid===false) {
                returnFechaGrua.enviar=false;
                returnFechaGrua.mensaje='Se requiere la selección de fecha válida para la cita';
                returnFechaGrua.write=true;
            }
        }
        return returnFechaGrua;
    },
    actualizaSin : function (component,event,helper,envio,mensaje) {
        var toastEvent = $A.get("e.force:showToast");
         if(envio) {
             var a = component.get('c.updateSin');
             $A.enqueueAction(a);
        } else {
            toastEvent.setParams( {
                title : 'Error de Validación',
                message:mensaje ,
                type: 'error'
            } );
            toastEvent.fire();
        }
    },
})