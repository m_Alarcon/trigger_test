({
    doInit : function(component, event, helper) {
        var fields = component.get("v.fieldsDetails");
        var res = fields.split(",");
        var fv1 = component.get("v.fieldVisibility1");
        var v1 = fv1.split(",");
        var fv2 = component.get("v.fieldVisibility2");
        var v2 = fv2.split(",");
        var fv3 = component.get("v.fieldVisibility3");
        var v3 = fv3.split(",");
        var fv4 = component.get("v.fieldVisibility4");
        var v4 = fv4.split(",");
        var fv5 = component.get("v.fieldVisibility5");
        var v5 = fv5.split(",");
        var fv6 = component.get("v.fieldVisibility6");
        var v6 = fv6.split(",");
        var fv7 = component.get("v.fieldVisibility7");
        var v7 = fv7.split(",");
        var fv8 = component.get("v.fieldVisibility8");
        var v8 = fv8.split(",");
        var fv9 = component.get("v.fieldVisibility9");
        var v9 = fv9.split(",");
        var fv10 = component.get("v.fieldVisibility10");
        var v10 = fv10.split(",");
        var objDetails = "{'sobjectType' : '"+component.get("v.objDetail")+"'}";
        res.forEach(dynamicPickValues);
       function dynamicPickValues(item, index) {
            var strMap = index+2;
            if(index < res.length-1) {
            helper.fetchPicklistValues(component,objDetails,res[index], res[index+1], "v.lv"+strMap+"Map");
            }
        }
        var action = component.get("c.fetchOpp");
        action.setParams({ strId : component.get("v.recordId"),
        varObject : component.get("v.objDetail"),
        pckList : component.get("v.fieldCtrl")+','+component.get("v.startIndex") } );
        action.setCallback(this, function(res) {
            var state = res.getState();
            if (state === "SUCCESS") {
               component.set("v.CurrentStage",  res.getReturnValue());
               component.set("v.Visibility1",v1[component.get("v.CurrentStage")]);
               component.set("v.Visibility2",v2[component.get("v.CurrentStage")]);
               component.set("v.Visibility3",v3[component.get("v.CurrentStage")]);
               component.set("v.Visibility4",v4[component.get("v.CurrentStage")]);
               component.set("v.Visibility5",v5[component.get("v.CurrentStage")]);
               component.set("v.Visibility6",v6[component.get("v.CurrentStage")]);
               component.set("v.Visibility7",v7[component.get("v.CurrentStage")]);
               component.set("v.Visibility8",v8[component.get("v.CurrentStage")]);
               component.set("v.Visibility9",v9[component.get("v.CurrentStage")]);
               component.set("v.Visibility10",v10[component.get("v.CurrentStage")]);
            }
        });
        $A.enqueueAction(action);
        var action2 = component.get("c.configType");
        action2.setParams({recordId:component.get("v.recordId")});
        action2.setCallback(this, function(res) {
            var state = res.getState();
            if (state === "SUCCESS") {
                 var etapa =  component.get("v.CurrentStage");
                var StoreResponse = res.getReturnValue();
                component.set("v.lv6PsMap",StoreResponse);
                var depnedentFieldMap = component.get("v.lv2Map");
                var depnedentFieldMapPs6 = component.get("v.lv6PsMap");
                var ListOfDependentFields = depnedentFieldMap['Contacto'];
                var ListOfDependentFieldsPs6;
                if(etapa === 0) {
                  ListOfDependentFieldsPs6 = depnedentFieldMapPs6['No Contacto'];
                }
                if(etapa === 1) {
                  ListOfDependentFieldsPs6 = depnedentFieldMapPs6['Sin Tipo0'];
                }
                if(etapa === 2) {
                    ListOfDependentFieldsPs6 = depnedentFieldMapPs6['Acepta cotización'];
                  }
                if(etapa === 3) {
                    ListOfDependentFieldsPs6 = depnedentFieldMapPs6['No Venta'];
                }
                if(etapa === 4) {
                    ListOfDependentFieldsPs6 = depnedentFieldMapPs6['Venta'];
                }
                helper.fetchDepValues(component, ListOfDependentFields,"v.tlv2lst");
                helper.fetchDepValues(component, ListOfDependentFieldsPs6,"v.tlv6Pslst");
            }
        });
        $A.enqueueAction(action2);
    },
    onControllerFieldChange: function(component, event, helper) {
        var controllerValueKey = event.getSource().get("v.value");
        component.set("v.fieldValue1",controllerValueKey);
        if (controllerValueKey !== '--- None ---') {
            if(ListOfDependentFields6Ps.length > 0) {
                component.set("v.bDisabledDependentFld" , false);
                helper.fetchDepValues(component, ListOfDependentFields6Ps,"v.tlv6Pslst");
            }else {
                component.set("v.bDisabledDependentFld" , true);
                component.set("v.tlv2lst", ['--- None ---']);
            }
        } else {
            component.set("v.tlv3lst", ['--- None ---']);
            component.set("v.bDisabledDependentFld" , true);
        }
        component.set("v.bDisabledSubDependentFld" , true);
    },
    onSubControllerFieldChange : function(component, event, helper) {
        var controllerValueKey = event.getSource().get("v.value");
        var depnedentFieldMap = component.get("v.lv3Map");
        component.set("v.showButton",false);
        var depnedentFieldMapPs6 = component.get("v.lv6PsMap");
        component.set("v.fieldValue2",controllerValueKey);
        if (controllerValueKey !== '--- None ---') {
            var ListOfDependentFields = depnedentFieldMap[controllerValueKey];
            var ListOfDependentFields6Ps = depnedentFieldMapPs6[controllerValueKey];
            if(ListOfDependentFields6Ps.length > 0) {
                component.set("v.bDisabledSubDependentFld" , false);
                helper.fetchDepValues(component, ListOfDependentFields,"v.tlv3lst");
                helper.fetchDepValues(component, ListOfDependentFields6Ps,"v.tlv6Pslst");
            }else {
                component.set("v.bDisabledSubDependentFld" , true);
            }
        } else {
            component.set("v.bDisabledSubDependentFld" , true);
        }
    },
    onSubControllerFieldChange3 : function(component, event, helper) {
        var controllerValueKey = event.getSource().get("v.value");
        var depnedentFieldMap = component.get("v.lv4Map");
        var depnedentFieldMap5 = component.get("v.lv5Map");
        var depnedentFieldMap6 = component.get("v.lv6Map");
        component.set("v.fieldValue3",controllerValueKey);
        if (controllerValueKey !== '--- None ---') {
            var ListOfDependentFields = depnedentFieldMap[controllerValueKey];
            var ListOfDependentFields5 = depnedentFieldMap5['No acepta cotización'];
            var ListOfDependentFields6 = depnedentFieldMap6['No Venta'];
            if(ListOfDependentFields.length > 0) {
                component.set("v.bDisabledSubDependentFld" , false);
                helper.fetchDepValues(component, ListOfDependentFields,"v.tlv4lst");
                helper.fetchDepValues(component, ListOfDependentFields5,"v.tlv5lst");
                helper.fetchDepValues(component, ListOfDependentFields6,"v.tlv6lst");
            }else {
                component.set("v.bDisabledSubDependentFld" , true);
            }
        } else {
            component.set("v.bDisabledSubDependentFld" , true);
        }
    },
    onSubControllerFieldChange4 : function(component, event, helper) {
        var controllerValueKey = event.getSource().get("v.value");
        var depnedentFieldMap = component.get("v.lv5Map");
        var depnedentFieldMap6 = component.get("v.lv6Map");
        component.set("v.fieldValue4",controllerValueKey);
        if (controllerValueKey !== '--- None ---') {
            var ListOfDependentFields = depnedentFieldMap[controllerValueKey];
            var ListOfDependentFields6 = depnedentFieldMap6['No Venta'];
            if(ListOfDependentFields.length > 0) {
                component.set("v.bDisabledSubDependentFld" , false);
                helper.fetchDepValues(component, ListOfDependentFields,"v.tlv5lst");
                helper.fetchDepValues(component, ListOfDependentFields6,"v.tlv6lst");
            }else {
                component.set("v.bDisabledSubDependentFld" , true);
                component.set("v.tlv5lst", ['--- None ---']);
            }
        } else {
            component.set("v.tlv5lst", ['--- None ---']);
            component.set("v.bDisabledSubDependentFld" , true);
        }
    },
    onSubControllerFieldChange5 : function(component, event, helper) {
        var controllerValueKey = event.getSource().get("v.value");
        var depnedentFieldMap = component.get("v.lv6Map");
        component.set("v.fieldValue5",controllerValueKey);
        if (controllerValueKey !== '--- None ---') {
            var ListOfDependentFields = depnedentFieldMap[controllerValueKey];
            if(ListOfDependentFields.length > 0) {
                component.set("v.bDisabledSubDependentFld" , false);
                helper.fetchDepValues(component, ListOfDependentFields,"v.tlv6lst");
            }else {
                component.set("v.bDisabledSubDependentFld" , true);
                component.set("v.tlv6lst", ['--- None ---']);
            }
        } else {
            component.set("v.tlv6lst", ['--- None ---']);
            component.set("v.bDisabledSubDependentFld" , true);
        }
    },
    onSubControllerFieldChange6 : function(component, event, helper) {
        var controllerValueKey = event.getSource().get("v.value");
        component.set("v.fieldValue6",controllerValueKey);
        component.set("v.showButton",false);
    },
    saveOpp : function(component, event, helper) {
        var action = component.get("c.saveType");
        var saveValue =  component.get("v.fieldValue6");
        action.setParams({ strId : component.get("v.recordId"),
        strType1 : saveValue } );
        action.setCallback(this, function(res){
            var state = res.getState();
            if (state === "SUCCESS") {
              helper.toastSuccess(component, event, helper);
              helper.updTaskLevel(component, event, helper);
            }else{
                helper.toastError(component, event, helper);
            }
        });
        $A.enqueueAction(action);
    }
})