({
    fetchPicklistValues: function(component,objDetails,controllerField, dependentField,mapAttrName) {
        var action = component.get("c.getDependentMap");
        action.setParams({
            'objDetail' : objDetails,
            'contrfieldApiName': controllerField,
            'depfieldApiName': dependentField
        });
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var StoreResponse = response.getReturnValue();
                component.set(mapAttrName,StoreResponse);
                if(mapAttrName === 'v.lv2Map') {
                    var listOfkeys = [];
                    var ControllerField = [];
                    for (var singlekey in StoreResponse) {
                        listOfkeys.push(singlekey);
                    }
                    for (var i = 0; i < listOfkeys.length; i++) {
                        ControllerField.push(listOfkeys[i]);
                    }
                   component.set("v.tlv1lst", ControllerField);
                }
            }
        });
        $A.enqueueAction(action);
    },
    fetchDepValues: function(component, ListOfDependentFields,lstAttrName) {
        var dependentFields = [];
        for (var i = 0; i < ListOfDependentFields.length; i++) {
            dependentFields.push(ListOfDependentFields[i]);
        }
        component.set(lstAttrName, dependentFields);
    },
    toastSuccess : function(component, event, helper) {
        var toastEvent1 = $A.get("e.force:showToast");
        toastEvent1.setParams({
            message:'Tipificación Guardada correctamente',
            duration:' 2000',
            key: 'check',
            type: 'success'
        });
        toastEvent1.fire();
    },
    toastError : function(component, event, helper) {
        var toastEvent2 = $A.get("e.force:showToast");
        toastEvent2.setParams({
            message:'Tipificación Guardada correctamente',
            duration:' 2000',
            key: 'check',
            type: 'success'
        });
        toastEvent2.fire();
    },
    updTaskLevel : function(component, event, helper) {
        helper.getTaskData(component, event, helper);
        var action = component.get("c.getOppData");
        action.setParams({oppId : component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if(component.get("v.updTask")==='True') {
                this.setTaskData(component, event, helper, storeResponse);
                }
            }
        });
        $A.enqueueAction(action);
    },
    getTaskData : function(component, event, helper) {
        var action = component.get("c.getTaskData");
		action.setParams(
            {oppId : component.get("v.recordId")
            }
        );
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                    component.set("v.updTask",'True');
                    component.set("v.tipifTask",storeResponse[0]);
            }
        });
        $A.enqueueAction(action);
    },
    setTaskData: function(component, event, helper, storeResponse) {
        component.set('v.taskObj.Id',component.get('v.tipifTask.Id'));
        component.set('v.taskObj.WhatId',component.get('v.recordId'));
        component.set('v.taskObj.MX_SB_VTA_Resultado_del_contacto__c',(storeResponse.MX_SB_VTS_Tipificacion_LV1__c === 'Contacto') ? 'Si' : 'No');
        component.set('v.taskObj.MX_SB_VTA_Resultado_contactoEfectivo__c',(storeResponse.MX_SB_VTS_Tipificacion_LV2__c === 'Contacto Efectivo') ? 'Si' : 'No');
        component.set('v.taskObj.MX_SB_VTA_Cliente_est_interesado__c',(storeResponse.MX_SB_VTS_Tipificacion_LV3__c === 'Interesado') ? 'Si' : 'No');
        component.set('v.taskObj.MX_SB_VTA_Cliente_acepta_cotizaci_n__c',(storeResponse.MX_SB_VTS_Tipificacion_LV4__c === 'Acepta cotización') ? 'Si' : 'No');
        component.set('v.taskObj.MX_SB_VTA_Se_pudo_realizar_cobro__c',(storeResponse.MX_SB_VTS_Tipificacion_LV5__c === 'Venta') ? 'Si' : 'No');
        component.set('v.taskObj.MX_SB_VTS_TipificacionNivel6__c',storeResponse.MX_SB_VTS_Tipificacion_LV6__c);
        component.set('v.taskObj.Status','Completed');
        var updTaskD = component.get('c.upsrtTask');
        updTaskD.setParams({'taskObj':component.get('v.taskObj'),'type':'Llamada'});
        updTaskD.setCallback(this, function(response) {
        });
        $A.enqueueAction(updTaskD);
    },
})