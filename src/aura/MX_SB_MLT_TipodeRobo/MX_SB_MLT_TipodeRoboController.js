({
	ValorPick:function(component,event, helper) {
		let action = component.get("c.getvaluePickRobo");
		action.setParams({"siniId":component.get("v.recordId")});
		action.setCallback(this, function(response) {
			component.set("v.ListValue",response.getReturnValue());
			});
		$A.enqueueAction(action);
	},
	roboDoInitTelefonica : function(component,event,helper) {
		let action = component.get("c.getRecordType");
		action.setParams({"devname":"MX_SB_MLT_RamoAuto"});
		action.setCallback(this, function(response) {
			component.set("v.recordtypeAUTO",response.getReturnValue());
		});
		$A.enqueueAction(action);
    },
})