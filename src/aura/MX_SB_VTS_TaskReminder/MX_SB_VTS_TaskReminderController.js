({
	fetchAccounts : function(component, event, helper) {
        var action = component.get("c.fetchTasks");
        action.setParams({
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.taskList", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    viewAccount : function(component, event, helper) {
        var viewRecordEvent = $A.get("e.force:navigateToURL");
        viewRecordEvent.setParams({
             "url": "/" + event.target.id
       });
       viewRecordEvent.fire();
    },
    isRefreshed : function(component , event, helper){
        var action = component.get("c.fetchTasks");
		action.setParams({
		});

		action.setCallback(this, function(response) {
			let state = response.getState();
			if(state === "SUCCESS") {
                 component.set("v.taskList", response.getReturnValue());
                $A.get("e.force:refreshView").fire();
			}
		});
        $A.enqueueAction(action);
    }
})