({
    doOnchange: function(component, event, helper) {
        helper.fetchDetailsSini(component, event, helper);
        var recordId = component.get("v.recordId");
        var getList = component.get('v.Grua.defaultValue');
        var GruaSelected = getList.indexOf('Grua') > -1;
        component.set("v.GruaAct", GruaSelected);
        var CambioSelected = getList.indexOf('Cambio de llanta') > -1;
        component.set("v.CambioAct", CambioSelected);
        var GasSelected = getList.indexOf('Gasolina') > -1;
        component.set("v.GasAct", GasSelected);
        var PasoSelected = getList.indexOf('Paso de corriente') > -1;
        component.set("v.PasoCAct", PasoSelected);
        if (GruaSelected == true) {
            var action1 = component.get("c.getGruaWO");
            action1.setParams({
                'sinid': recordId
            });
            action1.setCallback(this, function(response) {
				helper.respuestaServicio(component, event, helper, response);
            });
            $A.enqueueAction(action1);
        }
        if (CambioSelected == true) {
            var action2 = component.get("c.getCambioLLWO");
            action2.setParams({
                'sinid': recordId
            });
            action2.setCallback(this, function(response) {
				helper.respuestaServicio(component, event, helper, response);
            });
            $A.enqueueAction(action2);
        }
        if (PasoSelected == true) {
            var action3 = component.get("c.getPasoCWO");
            action3.setParams({
                'sinid': recordId
            });
            action3.setCallback(this, function(response) {
				helper.respuestaServicio(component, event, helper, response);
            });
            $A.enqueueAction(action3);
        }
        if (GasSelected == true) {
            var action4 = component.get("c.getGasWO");
            action4.setParams({
                'sinid': recordId
            });
            action4.setCallback(this, function(response) {
                helper.respuestaServicio(component, event, helper, response);
            });
            $A.enqueueAction(action4);
        }
    },
    insertCaso: function(component, event, helper) {
        helper.insertCaso(component, event, helper);
    },
})