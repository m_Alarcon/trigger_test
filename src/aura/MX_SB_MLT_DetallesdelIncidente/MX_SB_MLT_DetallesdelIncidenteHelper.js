({
    insertCaso: function(component, event,helper) {
		var correo=null;
        var Condiciones=null;
        var correoValida='';
        var valido = true;
        if (component.get('v.GruaAct') === true) {
            correoValida = component.find("correoGrua");
             correo = component.get('v.Grua.email');
             Condiciones = component.get('v.Grua.condicionesVeh');
        }
        if (component.get("v.CambioAct") === true) {
            Condiciones = component.get('v.CambioLlanta.condicionesVeh');
            correoValida = component.find("correoLlanta");
             correo = component.get('v.CambioLlanta.email');
        }
        if (component.get("v.GasAct") === true) {
             correo = component.get('v.Gasolina.email');
             Condiciones = component.get('v.Gasolina.condicionesVeh');
            correoValida = component.find("correoGas");
        }
        if (component.get("v.PasoCAct") === true) {
            correoValida = component.find("correoCorr");
             correo = component.get('v.PasoCorriente.email');
             Condiciones = component.get('v.PasoCorriente.condicionesVeh');
        }
        if(correoValida.get('v.validity').valid===false){
                valido = false;
                var errorValida= $A.get("e.force:showToast");
                    errorValida.setParams({
                        "title": "Alerta",
                        "message": "Error en correo electrónico",
                        "type":"error"
                    });
                    errorValida.fire();
            }
        if(valido) {
        	var action2 = component.get("c.updateSinies");
	        var btnValue = event.getSource().get("v.value");
            action2.setParams({'mapa':{
            'siniId': component.get("v.recordId"),
            'strEmail': correo,
            'strCond': Condiciones,
            'button': btnValue
                              }});
        action2.setCallback(this, function(response) {});
        if(btnValue==='true'){
            window.location.reload();
        }
        $A.enqueueAction(action2);
		if (component.get('v.GruaAct') === true) {
            var Grua = component.get("v.Grua");
            var action7 = component.get("c.insertWOGrua");
            action7.setParams({
                'strGrua': JSON.stringify(Grua),
                'strId': component.get("v.recordId")
            });
            action7.setCallback(this, function(response) {
                    var toastEvent1 = $A.get("e.force:showToast");
                    toastEvent1.setParams({
                        "title": "Enhorabuena!",
                        "message": "El caso Grua se ha creado correctamente."
                    });
                    component.set("v.Cambia", !component.get("v.Cambia"));
                	toastEvent1.fire();
            });
            $A.enqueueAction(action7);
        }

        if (component.get("v.CambioAct") === true) {
            var CambioLlanta = component.get("v.CambioLlanta");
            var action4 = component.get("c.insertWOCambLl");
            action4.setParams({
                'strCambioLlanta': JSON.stringify(CambioLlanta),
                'strId': component.get("v.recordId")
            });
            action4.setCallback(this, function(response) {
                    var toastEvent2 = $A.get("e.force:showToast");
                    toastEvent2.setParams({
                        "title": "Enhorabuena!",
                        "message": "Se ha creado el caso de cambio de llanta correctamente."
                    });
                    component.set("v.Cambia", !component.get("v.Cambia"));
                    toastEvent2.fire();
            });

            $A.enqueueAction(action4);

        }
        if (component.get("v.GasAct") === true) {
            var Gasolina = component.get("v.Gasolina");
            var action8 = component.get("c.insertWOGas");
            action8.setParams({
                'strGasolina': JSON.stringify(Gasolina),
                'strId': component.get("v.recordId")
            });
            action8.setCallback(this, function(response) {
                    var toastEvent3 = $A.get("e.force:showToast");
                    toastEvent3.setParams({
                        "title": "Enhorabuena!",
                        "message": "Se ha creado el caso de gasolina correctamente."
                    });
                    component.set("v.Cambia", !component.get("v.Cambia"));
                    toastEvent3.fire();
            });
            $A.enqueueAction(action8);
        }
        helper.evaluaPaso(component,event,helper);
        component.set("v.Grua.email", correo);
        component.set("v.CambioLlanta.email", correo);
        component.set("v.Gasolina.email", correo);
        component.set('v.Grua.condicionesVeh', Condiciones);
        component.set('v.CambioLlanta.condicionesVeh', Condiciones);
        component.set('v.PasoCorriente.condicionesVeh', Condiciones);
        component.set('v.Gasolina.condicionesVeh', Condiciones);
		}
    },
    evaluaPaso: function(component,event,helper) {
        if (component.get("v.PasoCAct") === true) {
            var PasoCorriente = component.get("v.PasoCorriente");
            var action9 = component.get("c.insertWOPasoC");
            action9.setParams({
                'strPasoCorriente': JSON.stringify(PasoCorriente),
                'strId': component.get("v.recordId")
            });
            action9.setCallback(this, function(response) {
                    var toastEvent4 = $A.get("e.force:showToast");
                    toastEvent4.setParams({
                        "title": "Enhorabuena!",
                        "message": "Se ha creado el caso de paso de corriente correctamente."
                    });
                    component.set("v.Cambia", !component.get("v.Cambia"));
                    toastEvent4.fire();
            });
            $A.enqueueAction(action9);
        }
    },
    fetchDetailsSini: function(component, event) {
        var action = component.get("c.getSiniestroD");
        action.setParams({
            "siniId": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
               var toastEvent6 = $A.get("e.force:showToast");
               var EmailSin = response.getReturnValue().MX_SB_MLT_CorreoElectronico__c;
               var CondicionesVehiculo = response.getReturnValue().MX_SB_MLT_Condiciones_del_Vehiculo__c;
               var disabled=false;
               if(response.getReturnValue().MX_SB_SAC_Contrato__c==='' || response.getReturnValue().MX_SB_SAC_Contrato__c==null ){
                    disabled=true;
                  	var toastEvt6 = $A.get("e.force:showToast");
                    toastEvt6.setParams({
                        "title": "Error!",
                        "message": "No has seleccionado ninguna poliza",
                        "type": "Error"
                    });
                    toastEvt6.fire();
                }
                toastEvent6.fire();
            }
            component.set('v.Grua.email', EmailSin);
            component.set('v.CambioLlanta.email', EmailSin);
            component.set('v.PasoCorriente.email', EmailSin);
            component.set('v.Gasolina.email', EmailSin);
            component.set('v.dsbutton',disabled);
            component.set('v.Grua.condicionesVeh', CondicionesVehiculo);
            component.set('v.CambioLlanta.condicionesVeh', CondicionesVehiculo);
            component.set('v.PasoCorriente.condicionesVeh', CondicionesVehiculo);
            component.set('v.Gasolina.condicionesVeh', CondicionesVehiculo);
        });
        $A.enqueueAction(action);
    },
    iteradorDochange: function(comp, event, helper) {
        var idProveedorGrua = comp.get("v.responseOnchange").AccountId;
        var state = comp.get("v.responseOnchange").state;
        if (state === "SUCCESS" && idProveedorGrua !== null) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Ya se ha creado esta asistencia."
            });
            toastEvent.fire();
        }
    },
    respuestaServicio : function(component, event, helper, response) {
        if(response.getState() === "SUCCESS"){
            component.set("v.responseOnchange",{
                "AccountId": response.getReturnValue().AccountId,
                "state": response.getState()}
                         );
            helper.iteradorDochange(component, event, helper);
        } else  {
            let AccountId = {"AccountId" : ""};
            component.set ("v.responseOnchange",AccountId);
        }
    }
})