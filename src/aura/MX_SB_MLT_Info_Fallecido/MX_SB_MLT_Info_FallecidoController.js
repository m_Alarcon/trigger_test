({
    initFallecido : function(component, event, helper) {
		helper.initialDate(component, event, helper);
    },
    showReembolso : function(component, event, helper) {
		helper.selectChange(component, event, helper);
	},
    validFields : function(component, event, helper) {
        helper.validRequired(component, event, helper);
    },
    upperCase : function(component, event, helper) {
        let fieldSelected = event.getSource().getLocalId();
        helper.upperCase(component, event, helper, fieldSelected);
    },
    actions : function(component, event, helper) {
        helper.whatAction(component, event, helper);
    },
    createService : function(component, event, helper) {
        helper.validFields(component, event, helper);
    }
})