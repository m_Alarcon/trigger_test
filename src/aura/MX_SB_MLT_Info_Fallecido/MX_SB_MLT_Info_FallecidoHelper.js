({
	selectChange : function(component, event, helper) {
        var combo = component.find("combo");
        var button = component.find("button");
        $A.util.toggleClass(combo, "slds-hide");
        $A.util.toggleClass(button, "slds-hide");
        component.set("v.Siniestro.MX_SB_MLT_Reembolso__c",'true');
	},
    selectRadio : function(component, event, helper, reembolso) {
        if(reembolso) {
            component.set("v.disabled",'true');
            component.set("v.radio",'true');
            helper.selectChange(component, event, helper);
        }
    },
    initialDate : function(component, event, helper) {
        let mindate = new Date();
        let maxdate = mindate;
        let time = new Date(mindate.getTime());
        let timeLimit=$A.localizationService.formatDate(time, "HH:mm:ssZ");
        maxdate = $A.localizationService.formatDate(maxdate, "YYYY-MM-DD");
        mindate.setDate(mindate.getDate() - 2);
        mindate = $A.localizationService.formatDate(mindate, "YYYY-MM-DD");
        component.set("v.minDate", mindate);
        let dTimeMax = maxdate + 'T' + timeLimit;
        component.set("v.maxDate", dTimeMax);
        let recordId = component.get("v.recordId");
        let action = component.get("c.searchSinVida");
        action.setParams({"siniId": recordId});
        action.setCallback(this, function(response){
        	let state = response.getState();
        	if(state === "SUCCESS"){
                let infoResponse = response.getReturnValue();
                component.set("v.Siniestro",infoResponse);
                let reembolso = infoResponse.MX_SB_MLT_Reembolso__c;
                helper.selectRadio(component, event, helper, reembolso);
            }
        });
        $A.enqueueAction(action);
    },
    validRequired : function(component, event, helper) {
       let valueDate = component.find("fechaDeseso").get("v.validity");
       let vNameDef = component.find("nameDef").get("v.validity");
       let vaPatDef = component.find("aPatDef").get("v.validity");
       let vaMatDef = component.find("aMatDef").get("v.validity");
       let vparenDef = component.find("parenDef").get("v.validity");
       let vCausa = component.find("tipo").get("v.validity");
        if (valueDate.valid && vNameDef.valid && vaPatDef.valid && vaMatDef.valid && vparenDef.valid && vCausa.valid) {
            helper.saveFallecido(component, event, helper);
        } else {
            component.find("fechaDeseso").reportValidity();
            component.find("nameDef").reportValidity();
            component.find("aPatDef").reportValidity();
            component.find("aMatDef").reportValidity();
            component.find("parenDef").reportValidity();
            component.find("tipo").reportValidity();
        }
    },
    saveFallecido : function(component, event, helper) {
        let recordId = component.get("v.recordId");
        component.set("v.Siniestro.Id",recordId);
        let action = component.get("c.insertFallecido");
        action.setParams({"fallecido": component.get("v.Siniestro")});
        action.setCallback(this, function(response){
        	let state = response.getState();
        	if(state === "SUCCESS"){
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Éxito!",
                    "type": "success",
                    "message": "Se ha guardado la información del fallecido"
                })
                toastEvent.fire();
                location.reload(true);
        	}
    	});
        $A.enqueueAction(action);
    },
    upperCase : function(component, event, helper, auraId) {
        switch (auraId) {
            case 'nameDef':
                let nameDef = component.get("v.Siniestro.MX_SB_MLT_NameDefunct__c");
                component.set("v.Siniestro.MX_SB_MLT_NameDefunct__c",nameDef.replace(/[^\w\s]/gi,'').toUpperCase());
                break;
            case 'aPatDef':
                let lastNameP = component.get("v.Siniestro.MX_SB_MLT_NamePDefunct__c");
                component.set("v.Siniestro.MX_SB_MLT_NamePDefunct__c",lastNameP.replace(/[^\w\s]/gi,'').toUpperCase());
                break;
            case 'aMatDef':
                let lastNameM = component.get("v.Siniestro.MX_SB_MLT_NameMDefunct__c");
                component.set("v.Siniestro.MX_SB_MLT_NameMDefunct__c",lastNameM.replace(/[^\w\s]/gi,'').toUpperCase());
                break;
            default:
    	}
    },
    whatAction : function(component, event, helper) {
        let recordId = component.get("v.recordId");
        var btnValue = event.getSource().get("v.value");
        var action = component.get("c.sinAction");
		action.setParams({'sinAction':btnValue, 'sinId':recordId});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === 'SUCCESS'){
                location.reload();
            }
        });
        $A.enqueueAction(action);
    },
    createAssistant : function(component, event, helper) {
        let messageToast = $A.get("e.force:showToast");
        let recordId = component.get("v.recordId");
        let executeWO = component.get("c.sendService");
        executeWO.setParams({'serviceWOId' : recordId});
        executeWO.setCallback(this, function(response){
            let state = response.getState();
            if (state === 'SUCCESS'){
                messageToast.setParams({
                    "title": "Servicio Enviado!",
                    "type": "success",
                    "message": "Se ha enviado el servicio"
                })
                messageToast.fire();
				component.set("v.disabled",'true');
            }
        });
        $A.enqueueAction(executeWO);
    },
    validFields : function(component, event, helper) {
       let dateDef = component.find("fechaDeseso").get("v.validity");
       let nameDef = component.find("nameDef").get("v.validity");
       let patDef = component.find("aPatDef").get("v.validity");
       let matDef = component.find("aMatDef").get("v.validity");
       let relatedDef = component.find("parenDef").get("v.validity");
       let causeDef = component.find("tipo").get("v.validity");
       let reembolso = component.find("reembolso").get("v.validity");
        if (dateDef.valid && nameDef.valid && patDef.valid && matDef.valid && relatedDef.valid && causeDef.valid && reembolso.valid) {
            helper.createAssistant(component, event, helper);
        } else {
            component.find("fechaDeseso").reportValidity();
            component.find("nameDef").reportValidity();
            component.find("aPatDef").reportValidity();
            component.find("aMatDef").reportValidity();
            component.find("parenDef").reportValidity();
            component.find("tipo").reportValidity();
            component.find("reembolso").reportValidity();
        }
    },
})