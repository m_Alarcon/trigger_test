({
    handleCancelCrearServiciosRobo: function(component) {
        component.set('v.isEditMode', false);
    },
    doInitCrearServiciosRobo : function(component) {
	    var action = component.get("c.getRecordType");
        action.setCallback(this, function(response) {
            component.set("v.recordtypeAUTO",response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    handleSuccessCrearServiciosRobo : function(component, event) {
	var payload = event.getParams().response;
        var navService = component.find("navService");
        var pageReference = {
            type: 'standard__recordPage',
            attributes: {
                "recordId": payload.id,
                "objectApiName": component.get("v.sObjectName"),
                "actionName": "view"
            }
        }
        event.preventDefault();
        navService.navigate(pageReference);
    },
    toggleEditModeCrearServiciosRobo : function(component) {
        component.set("v.isEditMode", !component.get("v.isEditMode"));
    },
    handleEditFormCrearServiciosRobo : function(component) {
        component.find("SiniestroEditForm").submit();
        $A.get("e.force:refreshView").fire();
        $A.get("e.force:editRecord").fire();
        component.set('v.isEditMode', false);
    }
})