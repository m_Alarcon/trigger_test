({
  // function call on component Load
    doInit: function(component, event, helper) {
        helper.getListTemas(component, event);
        component.set("v.vbotoncan",true);
    },
    // function for save the Records
    Save: function(component, event, helper) {
        if (helper.validateRequired(component, event)) {
            helper.saveTemas(component, event, helper);
            helper.getListTemas(component, event);
        }
    },
    // function for create new object Row in Temas List
    addNewRow: function(component, event, helper) {
        helper.createObjectData(component, event);
    },
    // function for delete the row
    removeDeletedRow: function(component, event, helper) {
        var index = event.getParam("indexVar");
        var AllRowsList = component.get("v.ListTemas");
        AllRowsList.splice(index, 1);
        component.set("v.ListTemas", AllRowsList);
    },
    allowDrop: function(component, event, helper) {
        event.preventDefault();
    },
    drag: function (component, event, helper) {
      event.dataTransfer.setData("text", event.target.id);
    },
    drop: function (component, event, helper) {
        var data = event.dataTransfer.getData("text");
    // Find the record ID
    var tar = event.target.closest('[id]');
    var ListTemas = component.get("v.ListTemas");
    var inicial = ListTemas[0].MX_WF_Hora_inicio_Tema__c;
    var index1, index2, temp;
    // Find the index of each item to move
    ListTemas.forEach(
                     (v,i) => { if(v.Id===data) {
                                    index1 = i;
                                }
                                if(v.Id===tar.id) {
                                    index2 = i;
                                }
                              });
    if(index1<index2) {
        // Lower index to higher index; we move the lower index first, then remove it.
        ListTemas.splice(index2+1, 0, ListTemas[index1]);
        ListTemas.splice(index1, 1);
    } else {
        // Higher index to lower index; we remove the higher index, then add it to the lower index.
        temp = ListTemas.splice(index1, 1)[0];
        ListTemas.splice(index2, 0, temp);
    }
    // Trigger aura:valueChange, component will rerender
    //component.set("v.ListTemas", ListTemas);
     var horaIni= $A.localizationService.formatDateTimeUTC(inicial,'YYYY-MM-DDThh:mm:ssZ');
     var date = new Date(horaIni);
     var minutos = date.getUTCMinutes();
     var hora = date.getUTCHours();
     var allTemasRows = ListTemas;
        for (var indexVar = 1; indexVar < allTemasRows.length; indexVar++) {
            var newminutos = minutos + allTemasRows[indexVar-1].MX_WF_Tiempo_Minutos__c;
                minutos = newminutos;
            if (minutos >= 60) {
                var newhora = hora + 1;
                hora = newhora;
                minutos = minutos-60;
            }
         var varhr = hora<10?('0'+ hora):hora;
         var varmin= minutos<10?('0'+minutos):minutos;
         var varIni = varhr+':' +varmin +':00.999Z';
               allTemasRows[indexVar].MX_WF_Hora_inicio_Tema__c = varIni;
        }
        allTemasRows[0].MX_WF_Hora_inicio_Tema__c= inicial;
        component.set('v.ListTemas', allTemasRows);
    event.preventDefault();
},
    // function for modify Records
    modify: function(component, event, helper) {
        component.set("v.vboton",false);
        component.set("v.vInputBox",false);
        component.set("v.vbotoncan",false);
    },
    send: function(component, event, helper) {
        helper.sendh(component, event, helper);
    },
    // function for cancel Records
    cancel: function(component, event, helper) {
        component.set("v.vboton",true);
        component.set("v.vInputBox",true);
        component.set("v.vbotoncan",true);
        helper.getListTemas(component, event);
        $A.get("e.force:refreshView");
    }
})