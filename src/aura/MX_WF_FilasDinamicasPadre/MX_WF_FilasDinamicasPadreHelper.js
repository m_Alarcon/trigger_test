({
    getListTemas:function(component) {
        var action = component.get("c.getTemas");
        action.setParams({ precordId : component.get("v.recordId")}  );
        action.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
                var allTemasRows = response.getReturnValue();
                if (allTemasRows.length !== 0) {
                for (var indexVar = 0; indexVar < allTemasRows.length; indexVar++) {
                     var horaIni= new Date(allTemasRows[indexVar].MX_WF_Hora_inicio_Tema__c);
                     var varhr = horaIni.getUTCHours()<10?('0'+horaIni.getUTCHours()):horaIni.getUTCHours();
                     var varmin= horaIni.getUTCMinutes()<10?('0'+horaIni.getUTCMinutes()):horaIni.getUTCMinutes();
                     var varIni = varhr+':' +varmin +':00.999Z';

                     allTemasRows[indexVar].MX_WF_Hora_inicio_Tema__c = varIni;
                     component.set("v.vInputBox",true);
                     component.set("v.vboton",true);
                      }
              }
                    component.set('v.ListTemas', allTemasRows);
            }
            else {
                console.log('error ' + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },
    saveTemas:function(component, event, helper) {
        var action = component.get("c.saveTema");
        action.setParams({ precordId : component.get("v.recordId"),
                           listTema : component.get("v.ListTemas")}  );
        action.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
              if(response.getReturnValue() === true) {
                    helper.showToast({
                        "title": "Temas registrados",
                        "type": "success",
                        "message": " Los temas fueron guardados con éxito",
                        "mode": "sticky"});
                   $A.get("e.force:refreshView");
                  helper.reloadDataTable();
                    component.set("v.vboton",true);
                    component.set("v.vInputBox",true);
                    component.set("v.vbotoncan",true);
                } else { //Si la actualización falla
                    helper.showToast({
                        "title": "Error!!",
                        "type": "error",
                        "message": "Error al guardar temas"});
                }
            }
        });
        $A.enqueueAction(action);
    },
    createObjectData: function(component, event) {
        if(!$A.util.isEmpty(component.get("v.ListTemas"))) {
           this.obtenerhoras(component, event);
        } else {
        var action = component.get("c.getHora");
        action.setParams({ precordId : component.get("v.recordId")});
        action.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
                var horainivar = response.getReturnValue();
                     var horaIni= new Date(horainivar);
                     var varhr = horaIni.getUTCHours()<10?('0'+horaIni.getUTCHours()):horaIni.getUTCHours();
                     var varmin= horaIni.getUTCMinutes()<10?('0'+horaIni.getUTCMinutes()):horaIni.getUTCMinutes();
                     var varIni = varhr+':' +varmin +':00.999Z';
                component.set('v.inicio',varIni);
                // get the ListTemas from component and add(push) New Object to List
                var RowItemList = component.get("v.ListTemas");
                RowItemList.push({
                    'sobjectType': 'MX_WF_Tema__c',
                    'Name': '',
                    'MX_WF_Tiempo_Minutos__c': '',
                    'MX_WF_Hora_inicio_Tema__c': component.get("v.inicio")
                });
                component.set("v.ListTemas", RowItemList);
            } else {
                console.log('error ' + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
        }
    },
    // helper function for check if Name is not null/blank on save
    validateRequired: function(component, event) {
        var isValid = true;
        var allTemasRows = component.get("v.ListTemas");
        for (var indexVar = 0; indexVar < allTemasRows.length; indexVar++) {
            if (allTemasRows[indexVar].Name === '') {
                isValid = false;
                    alert('El tema es obligatorio' + (indexVar + 1));
            }
        }
        return isValid;
    },
    /*
     * Muestra alertas
     * */
    showToast : function(params) {
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent) {
            toastEvent.setParams(params);
            toastEvent.fire();
        } else {
            alert(params.message);
        }
    },    /*
     * recarga valores en datatable
     * */
    reloadDataTable : function() {
    var refreshEvent = $A.get("e.force:refreshView");
        if(refreshEvent) {
            refreshEvent.fire();
        }
    },
    sendh: function(component, event, helper) {
        var action = component.get("c.sendTemplatedEmail");
        action.setParams({ whatId : component.get("v.recordId")});
        action.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: "Envío Exitoso",
                    message: "La agenda fue enviada a todos los invitados.",
                    type: "success"});
                toastEvent.fire();
            } else {

                var toastError = $A.get("e.force:showToast");
                toastError.setParams({
                    title: "Error",
                    message: "Ocurrió un error al enviar agenda.",
                    type: "error"
                });
                toastError.fire();
              }
        });
        $A.enqueueAction(action);
    },
    obtenerhoras: function(component, event) {
        var allTemasRows = component.get("v.ListTemas");
        var finicial = allTemasRows[allTemasRows.length-1].MX_WF_Hora_inicio_Tema__c;
        var horaIni= $A.localizationService.formatDateTimeUTC(finicial,'YYYY-MM-DDThh:mm:ssZ');
        var date = new Date(horaIni);
        var minutos = date.getUTCMinutes();
        var hora = date.getUTCHours();
        var varminu = allTemasRows[allTemasRows.length-1].MX_WF_Tiempo_Minutos__c;
        var newminutes = Number(minutos) + Number(varminu);
            if (newminutes >= 60) {
                var newhora = hora + 1;
                hora = newhora;
                newminutes = newminutes-60;
            }
         var varhr = hora<10?('0'+ hora):hora;
         var varmin= newminutes<10?('0'+newminutes):newminutes;
         var varIni = varhr+':' +varmin +':00.999Z';
             allTemasRows.push({
                    'sobjectType': 'MX_WF_Tema__c',
                    'Name': '',
                    'MX_WF_Tiempo_Minutos__c': '',
                    'MX_WF_Hora_inicio_Tema__c': varIni
                });
                component.set("v.ListTemas", allTemasRows);
    }
})