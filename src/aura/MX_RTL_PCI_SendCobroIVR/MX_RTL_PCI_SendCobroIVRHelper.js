({
    changeOppHelp : function(component, event, helper) {
        var folioQuote = event.getSource().get('v.value');
        component.set('v.folioQuote',folioQuote);
    },
    getOppStage : function(component, event, helper) {
        var action = component.get("v.etapa");
        if(action) {
            component.set('v.etapa',action.fields.StageName.value);
        }
    },
    retriveQuotes : function(component, event, helper) {
      var action = component.get("c.retrieveOpps");
	    action.setParams({oppsId: component.get("v.recordId")});
	    action.setCallback(this, function(response) {
		  let state = response.getState();
		  if(state === "SUCCESS") {
			  component.set("v.quoteList", response.getReturnValue());
		  }
		});
        $A.enqueueAction(action);
    },
    configParam : function(component, event, helper) {
        var active = component.get('c.activeLabel');
        active.setCallback(this, function(response) {
            var statusLabel = response.getState();
            if (statusLabel === 'SUCCESS') {
                component.set('v.cotIvr',response.getReturnValue());
            }
        });
        $A.enqueueAction(active);
    },
    permissionIvr : function(component, event, helper) {
        var userPermission = component.get('c.userInfPermission');
        userPermission.setCallback(this, function(response) {
            var statusIvr = response.getState();
            if (statusIvr === 'SUCCESS') {
                component.set('v.permissionIvr',response.getReturnValue());
            }
        });
        $A.enqueueAction(userPermission);
    },
	loadDataQuote : function(component, event, helper) {
		var idquote = component.get('v.recordId');
        var folioCotizacion = component.get('v.folioQuote');
        var action = component.get('c.getQuoteIvr');
        action.setParams({'idquoteopp':idquote,'quotefol':folioCotizacion});
        action.setCallback(this,function(response){
            var status = response.getState();
            if(status === 'SUCCESS') {
                helper.validateRes(component, event, helper, response);
            } else {
                var toastErr = $A.get("e.force:showToast");
                toastErr.setParams({
                    title : 'Error',
                    message: 'No se encontro ninguna cotización',
                    duration:'3000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastErr.fire();
            }
        });
        $A.enqueueAction(action);
	},
    validateRes : function(component, event, helper, response) {
        var resService = response.getReturnValue();
        var idcotiza = resService.MX_ASD_PCI_IdCotiza__c;
        var tokenAf = resService.MX_ASD_PCI_FolioAntifraude__c;
        var resCod = resService.MX_ASD_PCI_ResponseCode__c;
        if(idcotiza !== undefined && tokenAf !== undefined) {
            if(resCod === null || resCod === undefined) {
                helper.sendAttr(component, event, helper, resService);
            } else {
                var infCobro = $A.get("e.force:showToast");
                infCobro.setParams({
                    title : 'Alerta',
                    message: 'Esta cotización ya cuenta con un estado de cobro',
                    duration:'3000',
                    key: 'info_alt',
                    type: 'warning',
                    mode: 'sticky'
                });
                infCobro.fire();
            }
        } else {
            var attrError = $A.get("e.force:showToast");
            attrError.setParams({
                title : 'Error',
                message: 'No se encontraron datos para envio a cobro, Intente de nuevo!',
                duration:'3000',
                key: 'info_alt',
                type: 'error',
                mode: 'pester'
            });
            attrError.fire();
        }
    },
    sendAttr : function(component, event, helper, resService) {
        let quoteId = resService.Id;
        let idCotiza = resService.MX_ASD_PCI_IdCotiza__c;
        let tokenAf = resService.MX_ASD_PCI_FolioAntifraude__c;
        let idproveedor = resService.MX_ASD_PCI_DescripcionCode__c;
        component.set("v.quoteId",quoteId);
        component.set("v.idCotiza",idCotiza);
        var pureCLoudEvent = $A.get("e.c:MX_SB_RTL_ListenerPaymentPC");
        pureCLoudEvent.setParams({"folioCot" : idCotiza});
        pureCLoudEvent.setParams({"securityToken" : tokenAf});
        pureCLoudEvent.setParams({"proveedor" : idproveedor});
        pureCLoudEvent.setParams({"oppId" : quoteId});
        pureCLoudEvent.fire();
        if(pureCLoudEvent) {
            var successfire = $A.get("e.force:showToast");
            successfire.setParams({
                title : 'Exito',
                message: 'Los datos se han enviado',
                duration:'3000',
                key: 'info_alt',
                type: 'success',
                mode: 'pester'
            });
            successfire.fire();
        }
    },
    responseIvr : function(component, event, helper) {
        var quotefol = component.get('v.folioQuote');
        var idCotiza = component.get('v.idCotiza');
        var actionResp = component.get('c.getResIvr');
        actionResp.setParams({'quotefol':quotefol,'idCotiza':idCotiza});
        actionResp.setCallback(this,function(response){
            var status = response.getState();
            if(status === 'SUCCESS') {
                var resp = response.getReturnValue();
                if(resp.MX_ASD_PCI_ResponseCode__c === '0') {
                    var successCode = $A.get("e.force:showToast");
                    successCode.setParams({
                        title : 'Exito',
                        message: resp.MX_ASD_PCI_MsjAsesor__c,
                        duration:'3000',
                        key: 'info_alt',
                        type: 'success',
                        mode: 'pester'
                    });
                    successCode.fire();
                } else {
                    var errCode = $A.get("e.force:showToast");
                    errCode.setParams({
                        title : 'Error',
                        message: resp.MX_ASD_PCI_MsjAsesor__c,
                        duration:'3000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    errCode.fire();
                }
            }
        });
        $A.enqueueAction(actionResp);
    }
})