({
    initQuote : function(component, event, helper) {
        helper.retriveQuotes(component, event, helper);
    },
    udpStage : function(component, event, helper) {
        helper.getOppStage(component, event, helper);
        helper.configParam(component, event, helper);
        helper.permissionIvr(component, event, helper);
    },
    updtQuotes : function(component, event, helper) {
        helper.retriveQuotes(component, event, helper);
    },
    retrieveFol : function(component, event, helper) {
        let quoteFol = event.getParam("FolioCotizacion");
        if ($A.util.isEmpty(quoteFol) === false) {
            component.set('v.folioQuote',quoteFol);
        }
    },
	envioIvrCobro : function(component, event, helper) {
		helper.loadDataQuote(component, event, helper);
	},
    changeQuote : function(component, event, helper) {
        helper.changeOppHelp(component,event,helper);
    },
    responseIvr : function(component, event, helper) {
        helper.responseIvr(component, event, helper);
    }
})