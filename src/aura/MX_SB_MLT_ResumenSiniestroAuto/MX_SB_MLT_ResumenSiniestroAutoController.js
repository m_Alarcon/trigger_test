({
    doInit: function(component, event, helper) {
        var action = component.get("c.getDatosSiniestro");
        action.setParams({"idsini": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.defval", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
})