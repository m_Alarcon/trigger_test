({
    init: function (cmp, event, helper) {
        cmp.set('v.columns', [
            {label: 'Contacto', fieldName: 'Name', type: 'text'},
            {label: 'Correo electrónico', fieldName: 'Email', type: 'email'},
            {label: 'Dirección General', fieldName: 'MX_RTE_Direccion_General__c', type: 'phone'}]);
        helper.getInvitados(cmp);

        cmp.set('v.columnsrol', [{label: 'Rol', fieldName: 'MX_RTE_Rol__c', type: 'text'}]);
        helper.getRol(cmp);

        cmp.set('v.columnsequ', [{label: 'Nombre de Iniciativa', fieldName: 'Name', type: 'text'}]);
        helper.getEquipo(cmp);
    },
    changeq : function (cmp, event, helper) {
        var check = event.getParam('checked');
        switch (cmp.get("v.selTabId")) {
            case 'tab1' :
                if(check) {
                      cmp.set('v.bqactualI',true);
                  } else {
                        cmp.set('v.bqactualI',false);
                  }
                helper.getInvitados(cmp);
                break;
            case 'tab2':
                  if(check) {
                      cmp.set('v.bqactualR',true);
                  } else {
                      cmp.set('v.bqactualR',false);
                  }
                helper.getRol(cmp);
                break;
            case 'tab3':
                  if(check) {
                      cmp.set('v.bqactualE',true);
                  } else {
                        cmp.set('v.bqactualE',false);
                  }
                helper.getEquipo(cmp);
                break;
        }
    },
    selected: function (cmp, event) {
        var selectedRows = event.getParam('selectedRows');
        var allSelectedRows = cmp.get('v.selection');
        var sinfiltro = cmp.get('v.sinfiltro');
        var i = allSelectedRows.length;
        while (true) {
            if (i === 0) {
            break;
                }
            i -= 1;
            if(sinfiltro) {
               allSelectedRows.splice(i, 1);
            }
        }
        selectedRows.forEach(function(row) {
            allSelectedRows.push(row.Id);
            });
            cmp.set('v.selection', allSelectedRows);
    },
    invSelected: function (cmp, event) {
        var selectedRows = event.getParam('selectedRows');
            switch (cmp.get("v.selTabId")) {
                case 'tab3' :
                    cmp.set('v.equipos', "");
                    var resul = cmp.get('v.equipos');
                    if(selectedRows!==null && selectedRows!=='') {
                       for (var i=0;i<selectedRows.length;i++) {
                            resul.push(selectedRows[i].Id);
                            }
                    }
                        cmp.set('v.equipos', resul);
                    break;
                case 'tab2' :
                    cmp.set('v.roles', "");
                    var rroles = cmp.get('v.roles');
                    if(!$A.util.isEmpty(selectedRows)) {
                        for (var j=0;j<selectedRows.length;j++) {
                            rroles.push(selectedRows[j].MX_RTE_Rol__c);
                        }
                    }
                        cmp.set('v.roles', rroles);
                    break;
              }
 },
    handleClick : function (cmp, event, helper) {
        var lblbutton = event.getSource().get('v.label');
        if (lblbutton === "Agregar Invitados") {
            cmp.set('v.final', "true");
            event.getSource().set('v.label',"Confirmar");
            cmp.set('v.titulo','Contactos seleccionados:');
            cmp.set('v.labelcanc','Regresar');
            cmp.set('v.columnsfin', [
                {label: 'Contacto', fieldName: 'Name', type: 'text'},
                {label: 'Correo electrónico', fieldName: 'Email', type: 'email'},
                {label: 'Rol', fieldName: 'MX_RTE_Rol__c', type: 'text'}]);
        var selectedE = cmp.get('v.equipos');
        var selectedI = cmp.get('v.selection');
            var mInvEqui = selectedE.concat(selectedI);
            cmp.set('v.contactoEq', mInvEqui);
            helper.getInvitadosSel(cmp);
        } else {
                helper.insInvitados(cmp);
        }
    },
    handleClickCanc : function (cmp, event, helper) {
        var bcancel = cmp.get('v.labelcanc');
        if (bcancel === "Cancelar") {
            $A.get("e.force:closeQuickAction").fire();
        } else {
            cmp.set('v.final', "false");
            cmp.set('v.labelcanc', "Cancelar");
            cmp.set('v.titulo','Seleccionar asistentes:');
            cmp.set('v.label',"Agregar Invitados");
        }
    },
    changeinput: function (cmp, event) {
        var keysearch = event.getSource().get('v.value');
        cmp.set('v.keyword',keysearch);
        var data = cmp.get("v.data");
         var allData = cmp.get("v.UnfilteredData");
         var searchKey = cmp.get("v.keyword");
        cmp.set('v.sinfiltro', false);
         if(!$A.util.isEmpty(data)) {
           var filtereddata = allData.filter(word => (!searchKey) || word.Name.toLowerCase().indexOf(searchKey.toLowerCase()) > -1);
         }
         cmp.set("v.data", filtereddata);
         if(searchKey==='') {
           cmp.set('v.sinfiltro', true);
           cmp.set("v.data",cmp.get("v.UnfilteredData"));
           cmp.find('contactos').set('v.selectedRows',cmp.get('v.selection'));
         }
    },
})