({
    getInvitados: function (cmp) {
        var action = cmp.get("c.getInvitados");
        cmp.set('v.showSpinner',"true");
        action.setParams({ rtContact :"MX_RTE_Usuarios",
                           bqactual : cmp.get("v.bqactualI")});
        action.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
                var data = response.getReturnValue();
                cmp.set('v.data', data);
                cmp.set('v.UnfilteredData', data);
                cmp.set('v.showSpinner',"false");
            } else {
                console.log('error ' + JSON.stringify(response.getError()));
                cmp.set('v.showSpinner',"false");
            }
        });
        $A.enqueueAction(action);
    },
    getRol: function (cmp) {
        var action = cmp.get("c.getInvitadosRol");
        cmp.set('v.showSpinner',"true");
        action.setParams({ rtContact :"MX_RTE_Usuarios",
                           bqactual : cmp.get("v.bqactualR")});
        action.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
                cmp.set('v.datarol', response.getReturnValue());
                cmp.set('v.showSpinner',"false");
            } else {
                console.log('error ' + JSON.stringify(response.getError()));
                cmp.set('v.showSpinner',"false");
            }
        });
        $A.enqueueAction(action);
    },
    getEquipo: function (cmp) {
        var action = cmp.get("c.getInvitadosEquipo");
        cmp.set('v.showSpinner',"true");
        var now = new Date();
        action.setParams({ fecha :now,
                           bqactual : cmp.get("v.bqactualE")
                         });
        action.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
                cmp.set('v.dataequ', response.getReturnValue());
                cmp.set('v.showSpinner',"false");
            } else {
                console.log('error ' + JSON.stringify(response.getError()));
                cmp.set('v.showSpinner',"false");
            }
        });
        $A.enqueueAction(action);
    },
    getInvitadosSel: function (cmp, event, helper) {
        var banderaR = cmp.get("v.bqactualR");
        var banderaE = cmp.get("v.bqactualE");
        var action = cmp.get("c.getInvitadosSel");
        cmp.set('v.showSpinner',"true");
        action.setParams({listrol : cmp.get("v.roles"),
                          listinvi : cmp.get("v.contactoEq"),
                          rtContact :"MX_RTE_Usuarios",
                          bqactualR : banderaR,
                          bqactualE : banderaE
                         });
        action.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
                var datarows = response.getReturnValue();
                cmp.set('v.lfinal', "");
                cmp.set('v.datafin', datarows);
                var resul = cmp.get('v.lfinal');
                if(!$A.util.isEmpty(datarows)) {
                    for(var i=0;i<datarows.length;i++) {
                       resul.push(datarows[i].Id);
                    }
                }
               cmp.set('v.lfinal', resul);
                cmp.set('v.showSpinner',"false");
            } else {
                console.log('error ' + JSON.stringify(response.getError()));
                cmp.set('v.showSpinner',"false");
            }
        });
        $A.enqueueAction(action);
    },
    insInvitados: function (cmp) {
            var action = cmp.get("c.insertInv");
            action.setParams({ listInvitados : cmp.get("v.lfinal"),
                               idEvent : cmp.get("v.recordId")});
            action.setCallback(this, function(response) {
                if(response.getState()==='SUCCESS') {
                        var successEvt = $A.get("e.force:showToast");
                        successEvt.setParams({
                            title: "Registro Exitoso",
                            message: "Los contactos fueron agregados a la sesión.",
                            type: "success"
                        });
                        successEvt.fire();
                        var dismissActionPanel = $A.get("e.force:closeQuickAction");
                        dismissActionPanel.fire();
                        var refreshEvent = $A.get("e.force:refreshView");
                        refreshEvent.fire();
                } else {
                    console.log('error ' + JSON.stringify(response.getError()));
                       var errorEvt = $A.get("e.force:showToast");
                       errorEvt.setParams({
                            title: "Error",
                            message: "Ocurrió un error al crear los registros.",
                            type: "error"
                        });
                        errorEvt.fire();
                  }
            });
            $A.enqueueAction(action);
    },
})