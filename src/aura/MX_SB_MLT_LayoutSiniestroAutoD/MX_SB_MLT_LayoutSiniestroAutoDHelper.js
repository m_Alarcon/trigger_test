({
    requiredFields: function(component) {
        var apaterno = component.find("LastNameIdenAtn");
        apaterno.reportValidity();
        apaterno=apaterno.get("v.validity");
        var nombre = component.find("NameIdenAtn");
        nombre.reportValidity();
        nombre = nombre.get("v.validity");
        var amaterno = component.find("LastName2IdenAtn");
        amaterno.reportValidity();
        amaterno = amaterno.get("v.validity");
        if(!nombre.valid || !apaterno.valid || !amaterno.valid) {
            	this.toastTelefono();
		}
        else{
            component.set("v.validate",'false');
        }
    },
    toastSuccessAuto: function(){
        var toastEvtAuto = $A.get("e.force:showToast");
        toastEvtAuto.setParams({
            title: 'Enhorabuena',
            message: 'Se ha guardado el registro exitosamente',
            key: 'info_alt',
            type: 'Success'
        });
        toastEvtAuto.fire();
    },
    toastTelefono : function () {
        var toastEvento = $A.get("e.force:showToast");
        toastEvento.setParams({
                title : 'Error:',
                message: 'Porfavor llene los campos obligatorios.',
                key: 'info_alt',
                duration: ' 2000',
                type: 'error'
            });
            toastEvento.fire();
    },
    valMobile: function(component,event) {
		var MobileA = component.find('MobilePhoneIdenAtn');
		var MobileValue = MobileA.get('v.value');
        component.set("v.sinObj.MX_SB_MLT_Mobilephone__c",MobileValue.replace(/[^0-9\.]+/g,''));
        var phoneValidityA = component.find("MobilePhoneIdenAtn").get("v.validity");
        var phoneValid = phoneValidityA.valid;
        if(phoneValid===false) {
            component.set('v.errorMessage2',"Su entrada es demasiado corta.");
        }
        if(isNaN(MobileValue)) {
			component.set('v.errorMessage2',"Porfavor ingrese un numero valido");
        }
		else{
            component.set('v.errorMessage2',null);
    	}
	},
    valMobile2: function(component,event) {
		var MobileA2 = component.find('MobilePhoneIdenAtn2');
		var MobileValue2 = MobileA2.get('v.value');
        component.set("v.sinObj.MX_SB_MLT_Mobilephone2__c",MobileValue2.replace(/[^0-9\.]+/g,''));
        var phoneValidityA2 = component.find("MobilePhoneIdenAtn2").get("v.validity");
        var phoneValidA2 = phoneValidityA2.valid;
        if(phoneValidA2===false) {
            component.set('v.errorMessage2',"Su entrada es demasiado corta.");
        }
        if(isNaN(MobileValue2)) {
			component.set('v.errorMessage2',"Porfavor ingrese un numero valido");
        }
		else{
            component.set('v.errorMessage2',null);
    	}
	},
     valExt1:function(component,event){
        var Exte1= component.get('v.sinObj.MX_SB_MLT_ExtPhone__c');
        component.set('v.sinObj.MX_SB_MLT_ExtPhone__c',Exte1.replace(/[^0-9\.]+/g,''));
    },
    changeTabNamePar : function(component, event) {
       var workspaceAPITCA = component.find("workspace");
       workspaceAPITCA.getFocusedTabInfo().then(function(response) {
                       var focusedTabId = response.tabId;
                       workspaceAPITCA.setTabLabel({
                       tabId: focusedTabId,
                       label: "Crear Siniestro/Asistencia",
                       title: "Crear Siniestro/Asistencia"
                       });
			})
       .catch(function(error) {
    });
    },
    valExt2:function(component,event){
        var Exte2= component.get('v.sinObj.MX_SB_MLT_ExtPhone2__c');
        component.set('v.sinObj.MX_SB_MLT_ExtPhone2__c',Exte2.replace(/[^0-9\.]+/g,''));
	},
})