({
	showInfo: function(component, event, helper) {
        var selectedMenuItemValU = event.getParam("value");
		var selectedMenuItemValue= selectedMenuItemValU.toLowerCase();
        component.set("v.toggleAddrem",!component.get('v.toggleAddrem'));
        component.set('v.sinObj.MX_SB_MLT_PhoneTypeAdditional__c',selectedMenuItemValue)
        if(selectedMenuItemValue==='oficina'){
           component.set("v.BooleanExt1",true)
        }
    },
    showInfo2: function(component, event, helper) {
    var selectedMenuItemValuE = event.getParam("value");
	var selectedMenuItemValue= selectedMenuItemValuE.toLowerCase();
        component.set('v.sinObj.MX_SB_MLT_PhoneTypeAdditional2__c',selectedMenuItemValue);
        component.set("v.toggleAddrem2",!component.get('v.toggleAddrem2'));
		if(selectedMenuItemValue==='oficina'){
           component.set("v.BooleanExt2",true)
        }
    },
    closeToggle: function(component, event, helper) {
     component.set("v.toggleAddrem",!component.get('v.toggleAddrem'));
     component.set("v.sinObj.MX_SB_MLT_ExtPhone__c",'');
     component.set("v.sinObj.MX_SB_MLT_Mobilephone__c",'');
     component.set("v.BooleanExt1",false)
    },
    doInitLayAuto : function(component, event, helper) {
        helper.changeTabNamePar(component, event);
        var action = component.get("c.getRecordType");
        component.set("v.sinObj.MX_SB_MLT_Telefono__c",component.get("v.Telefono"))
        action.setParams({"devname":"MX_SB_MLT_RamoAuto"});
        action.setCallback(this, function(response) {
                component.set("v.recordtypeAuto",response.getReturnValue());
            });
            $A.enqueueAction(action);
        },
	closeToggle2: function(component, event, helper) {
     component.set("v.toggleAddrem2",!component.get('v.toggleAddrem2'));
     component.set("v.sinObj.MX_SB_MLT_Mobilephone2__c",'');
     component.set("v.sinObj.MX_SB_MLT_ExtPhone2__c",'');
     component.set("v.BooleanExt2",false)
    },
    setPhoneType: function(component, event){
        var tipophone= component.find("phoneType");
        var tipophoneVal= tipophone.get("v.value");
        component.set("v.sinObj.MX_SB_MLT_PhoneType__c",tipophoneVal);
    },
    setPhoneAdType2:function(component,event) {
        var tipophoneAdVal2= component.get("v.sinObj.MX_SB_MLT_PhoneTypeAdditional2__c");
        if(tipophoneAdVal2==="oficina"){
            component.set("v.BooleanExt2",true);
        }
        else{
            component.set("v.BooleanExt2",false);
        }
    },
     setPhoneAdType:function(component) {
        var tipophoneAdVal= component.get("v.sinObj.MX_SB_MLT_PhoneTypeAdditional__c");
        if(tipophoneAdVal==="oficina"){
            component.set("v.BooleanExt1",true);
        }
        else{
            component.set("v.BooleanExt1",false);
        }
    },
    NuevoOnsuccess: function(component, event, helper) {
        var recId=component.get("v.recordId");
        var recordtpAuto= component.get("v.recordtypeAuto");
        component.set("v.sinObj.RecordTypeId",recordtpAuto);
        if(recId!==''){
            component.set("v.sinObj.Id", recId);
        }
        helper.requiredFields(component,event,helper);
        component.set("v.sinObj.MX_SB_MLT_Telefono__c",component.get("v.Telefono"));
        if(component.get("v.validate")==='true') {
            component.set("v.validate","true");
        }
        if(component.get("v.validate")==='false') {
            var sinies= component.get("v.sinObj");
            var action21 = component.get('c.upSertsini');
            component.set("v.disabled",true);
            action21.setParams({'sinIn': sinies});
            action21.setCallback(this, function(response) {
                if (response.getState() === 'SUCCESS') {
                    helper.toastSuccessAuto(component,event,helper);
                    component.set("v.disabled",true);
                }
                else{
                    component.set("v.disabled",false);
                }
            });
            $A.enqueueAction(action21);
        }
        if(component.get("v.validate")===undefined) {
            var siniestr= component.get("v.sinObj");
            var action3 = component.get('c.upSertsini');
            component.set("v.disabled",true);
            action3.setParams({'sinIn': siniestr});
            action3.setCallback(this, function(response) {
                if (response.getState() === 'SUCCESS') {
                    helper.toastSuccessAuto(component,event,helper);
                    component.set("v.disabled",true);
                }
                else{
                    component.set("v.disabled",false);
                }
            });
            $A.enqueueAction(action3);
        }
    },
    setFalse2: function(component,event,helper){
    	component.set("v.validate",'false');
    	helper.valMobile2(component,event,helper);
    },
    setFalse: function(component,event,helper){
    	component.set("v.validate",'false');
    	helper.valMobile(component,event,helper);
    },
    toastInfo: function(component,event,helper){
        var mensaje1=event.getParams().message;
        if(mensaje1.includes("Porfavor")) {
			component.set("v.validate",'true');
        }
    },
    setPhone: function(component,event,helper) {
        component.set("v.validate",'false');
        var phoneAuD = component.find('PhoneIdenAtn');
		var phoneValueAuD = phoneAuD.get('v.value');
        component.set("v.sinObj.MX_SB_MLT_Telefono__c",phoneValueAuD.replace(/[^0-9\.]+/g,''));

        var phoneValidAuD = component.find("PhoneIdenAtn").get("v.validity").valid;
        if(isNaN(phoneValueAuD)){
			component.set('v.errorMessage1',"Porfavor ingrese un numero valido");
        }
        if(phoneValidAuD===false){
			component.set('v.errorMessage1',"Su entrada es demasiado corta.");
        }
		else{
            component.set('v.errorMessage1',null);
    	}
    },
    	setFalse4: function(component,event,helper) {
        component.set("v.validate",'false');
		helper.valExt2(component,event,helper);
    },
    caps1:function(component,event,helper){
        var NombCon= component.get("v.sinObj.MX_SB_MLT_NombreConductor__c");
        component.set("v.sinObj.MX_SB_MLT_NombreConductor__c",NombCon.replace(/[^\w\s]/gi,'').toUpperCase());
    },
        setFalse3: function(component,event,helper) {
        component.set("v.validate",'false');
		helper.valExt1(component,event,helper);
    },
    caps4:function(component,event,helper){
        var mail= component.get("v.sinObj.MX_SB_MLT_CorreoElectronico__c");
        component.set("v.sinObj.MX_SB_MLT_CorreoElectronico__c",mail.toLowerCase());
    },
    caps3:function(component,event){
        var AMCon= component.get("v.sinObj.MX_SB_MLT_AMaternoConductor__c");
        component.set("v.sinObj.MX_SB_MLT_AMaternoConductor__c",AMCon.replace(/[^\w\s]/gi,'').toUpperCase());
    },
    caps2:function(component,event,helper){
        var APCon= component.get("v.sinObj.MX_SB_MLT_APaternoConductor__c");
        component.set("v.sinObj.MX_SB_MLT_APaternoConductor__c",APCon.replace(/[^\w\s]/gi,'').toUpperCase());
    },
})