({
    doInit: function(component, event, helper) {
	var ObjectN = component.get("v.Valor");
        console.log('OBJ:' + ObjectN);
        var action = component.get("c.getDatosSiniestro");
        var idobj = component.get("v.recordId");
        console.log('Id:' + idobj);
        action.setParams({"idsini": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
   			component.set("v.Valor", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
})