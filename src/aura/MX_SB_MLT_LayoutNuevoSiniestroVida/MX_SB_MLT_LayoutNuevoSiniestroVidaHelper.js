({
    requiredFields: function(component, event, helper) {
        var nombre = component.find("NameIdenAtn");
        nombre.reportValidity();
        nombre = nombre.get("v.validity");
        var apaterno = component.find("LastNameIdenAtn");
        apaterno.reportValidity();
        apaterno=apaterno.get("v.validity");
        var amaterno = component.find("LastName2IdenAtn");
        amaterno.reportValidity();
        amaterno = amaterno.get("v.validity");
        if(!nombre.valid || !apaterno.valid || !amaterno.valid ) {
            helper.toastPolicy('Error:', 'Porfavor llene los campos obligatorios.', 'error');
		}
        else{
            component.set("v.validate",'false');
        }
    },
    valMobile: function(component,event) {
        var Mobile = component.find('MobilePhoneIdenAtn');
        var MobileValueVida = Mobile.get('v.value');
        component.set("v.sinObj.MX_SB_MLT_Mobilephone__c",MobileValueVida.replace(/[^0-9\.]+/g,''));
        var phoneValidity = component.find("MobilePhoneIdenAtn").get("v.validity");
        var phoneValid = phoneValidity.valid;
        if(phoneValid===false) {
            component.set('v.errorMessage2',"Su entrada es demasiado corta.");
        }
        if(isNaN(MobileValueVida)) {
            component.set('v.errorMessage2',"Porfavor ingrese un numero valido");
        }
        else{
            component.set('v.errorMessage2',null);
        }
    },
    valMobile2: function(component,event) {
		var Mobile2 = component.find('MobilePhoneIdenAtn2');
		var MobileValueVida2 = Mobile2.get('v.value');
        component.set("v.sinObj.MX_SB_MLT_Mobilephone2__c",MobileValueVida2.replace(/[^0-9\.]+/g,''));
        var phoneValidity2 = component.find("MobilePhoneIdenAtn2").get("v.validity");
        var phoneValid2 = phoneValidity2.valid;
        if(phoneValid2===false) {
            component.set('v.errorMessage2',"Su entrada es demasiado corta.");
        }
        if(isNaN(MobileValueVida2)) {
			component.set('v.errorMessage2',"Porfavor ingrese un numero valido");
        }
		else{
            component.set('v.errorMessage2',null);
    	}
	},
    upperCase : function(component, event, helper, auraId) {
        switch (auraId) {
            case 'nombreAsegurado':
                let nameIns = component.get("v.sinObj.MX_SB_MLT_NombreAsegurado__c");
                component.set("v.sinObj.MX_SB_MLT_NombreAsegurado__c",nameIns.replace(/[^\w\s]/gi,'').toUpperCase());
                break;
            case 'aPaternoAsegurado':
                let lastNameP = component.get("v.sinObj.MX_SB_MLT_InsuredLastName2__c");
                component.set("v.sinObj.MX_SB_MLT_InsuredLastName2__c",lastNameP.replace(/[^\w\s]/gi,'').toUpperCase());
                break;
            case 'aMaternoAsegurado':
                let lastNameM = component.get("v.sinObj.MX_SB_MLT_InsuredLastName__c");
                component.set("v.sinObj.MX_SB_MLT_InsuredLastName__c",lastNameM.replace(/[^\w\s]/gi,'').toUpperCase());
                break;
            case 'policyIns':
                let policy = component.get("v.sinObj.Poliza__c");
                component.set("v.sinObj.Poliza__c",policy.replace(/[^\w\s]/gi,'').toUpperCase());
           	break;
            default:
        }
    },
    changeTabNamePar : function(component, event) {
       var workspaceAPILNSV = component.find("workspace");
       workspaceAPILNSV.getFocusedTabInfo().then(function(response) {
                       var focusedTabId = response.tabId;
                       workspaceAPILNSV.setTabLabel({
                       title: "Crear Siniestro/Asistencia",
                       tabId: focusedTabId,
                       label: "Crear Siniestro/Asistencia"
                       });
			})
       .catch(function(error) {
    });
    },
    searchPolicy : function(component, event, helper) {
        let valuePolicy = component.get("v.sinObj.Poliza__c");
        if (valuePolicy === undefined) {
            valuePolicy = '';
        }
        let searchContract = component.get("c.checkPolicy");
        searchContract.setParams({'policy': valuePolicy});
        searchContract.setCallback(this, function(response) {
            if(response.getState() === 'SUCCESS' && response.getReturnValue()[0] !==undefined) {
	    	let idContract = response.getReturnValue()[0];
                component.set("v.sinObj.MX_SB_SAC_Contrato__c", idContract);
                this.toastPolicy('Exito en busqueda:', 'Póliza encontrada','success');
            } else {
                component.set("v.sinObj.MX_SB_SAC_Contrato__c", '');
                this.toastPolicy('Error en busqueda:','Póliza no valida','error');
            }
        });
        $A.enqueueAction(searchContract);
    },
    valExt1:function(component,event){
        var Exte1= component.get('v.sinObj.MX_SB_MLT_ExtPhone__c');
        component.set('v.sinObj.MX_SB_MLT_ExtPhone__c',Exte1.replace(/[^0-9\.]+/g,''));
    },
    valExt2:function(component,event){
        var Exte2= component.get('v.sinObj.MX_SB_MLT_ExtPhone2__c');
        component.set('v.sinObj.MX_SB_MLT_ExtPhone2__c',Exte2.replace(/[^0-9\.]+/g,''));
	},
    toastPolicy : function (title, message, type) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            duration:' 2000',
            key: 'info_alt',
            type: type
        });
        toastEvent.fire();
    },
    limitDateSelected : function(component, event, helper) {
        let dateToday = new Date();
        let begin = new Date(dateToday.getTime());
        let dateInit = $A.localizationService.formatDate(begin, "yyyy-MM-dd");
        let hourInit=$A.localizationService.formatDate(begin, "HH:mm:ssZ");
        let minimot = dateInit+'T'+hourInit;
        component.set("v.minDate",minimot);
        helper.limitMaxDateSelected(component, event, helper);
    },
    limitMaxDateSelected : function(component, event, helper) {
        let todayDate = new Date();
        todayDate.setDate(todayDate.getDate() + 5);
        let endTime = new Date(todayDate.getTime());
        let endDate = $A.localizationService.formatDate(endTime, "yyyy-MM-dd");
        let maxDate = endDate+'T'+"23:59:00";
        component.set("v.maxDate",maxDate);
    },
    getTaskValues : function(component, event, helper) {
        component.set("v.taskObject.MX_SB_MLT_Siniestro__c",component.get("v.recordId"));
        component.set("v.taskObject.WhatId",component.get("v.recordId"));
        component.set("v.taskObject.ReminderDateTime",component.get("v.sinObj.MX_SB_MLT_CitaVidaAsegurado__c"));
        component.set("v.taskObject.MX_SB_MLT_SubRamo__c", component.get("v.sinObj.MX_SB_MLT_SubRamo__c"));
        component.set("v.taskObject.IsReminderSet", true);
        component.set("v.taskObject.Subject", 'Cita Agendada');
        component.set("v.taskObject.Telefono__c",component.get("v.sinObj.MX_SB_MLT_Telefono__c"));
    },
    createTask : function(component, event, helper,seleccion) {
        if(seleccion === 'Agendar Cita') {
            helper.getTaskValues(component, event, helper);
            let taskObject = component.get("v.taskObject");
            let createTask = component.get("c.whatIdSin");
                createTask.setParams({"taskPC":taskObject,'taskType':'Cita'});
                createTask.setCallback(this,function(response) {
                });
            $A.enqueueAction(createTask);
        }
    },
    validPolicyValues : function(component, event, helper) {
		let valValue = component.find("policyIns").get("v.validity").valid;
        if(!valValue) {
            component.find("policyIns").reportValidity();
        } else {
            helper.searchPolicy(component, event, helper);
        }
    },
    validityValues : function(component, event, helper) {
        let createSiniestro = component.get('c.NuevoOnsuccessVida');
        let questionAtencion = component.find("atencion").get("v.value");
        if(questionAtencion === "Agendar Cita") {
            helper.validInsuranceValues(component, event, helper);
        } else {
            $A.enqueueAction(createSiniestro);
        }
    },
    validInsuranceValues : function(component, event, helper) {
        let actionCreate = component.get('c.NuevoOnsuccessVida');
        let question = component.find("reportante").get("v.value");
        if(question === 'false') {
            let insuranceName = component.find("nombreAsegurado").get("v.validity").valid;
            let insurancePName = component.find("aPaternoAsegurado").get("v.validity").valid;
            let insuranceMName = component.find("aMaternoAsegurado").get("v.validity").valid;
            let dataTask = component.find("dateTask").get("v.validity").valid;
            if(!insuranceName||!insurancePName||!insuranceMName||!dataTask) {
                component.find("nombreAsegurado").reportValidity();
                component.find("aPaternoAsegurado").reportValidity();
                component.find("aMaternoAsegurado").reportValidity();
                component.find("dateTask").reportValidity();
            } else {
                $A.enqueueAction(actionCreate);
            }
        } else {
            helper.dateError(component, event, helper);
        }
    },
    dateError : function(component, event, helper) {
        let action = component.get('c.NuevoOnsuccessVida');
        let correctDate = component.find("dateTask").get("v.validity").valid;
        if(!correctDate) {
            component.find("dateTask").reportValidity();
        } else {
            $A.enqueueAction(action);
        }
    },
    showSpinner: function(component, event, helper, reporte) {
        if (reporte === 'Siniestros') {
           let boton = component.find("boton");
           let spinner = component.find("spinner");
           $A.util.toggleClass(boton,"slds-hide");
           $A.util.toggleClass(spinner,"slds-hide");
        } else {
            let boton2 = component.find("boton2");
            let spinner2 = component.find("spinner2");
            $A.util.toggleClass(boton2,"slds-hide");
            $A.util.toggleClass(spinner2,"slds-hide");
        }
	},
    updteWhatId : function(component,event) {
        var recordId = component.get("v.recordId");
        var action= component.get("c.whatIdSin");
        component.set("v.relatedCall.WhatId",recordId);
        action.setParams({'taskPC':component.get('v.relatedCall'),'taskType':'PureCloud'});
        action.setCallback(this, function(response) {
        });
        $A.enqueueAction(action);
    },
})