({
    closeToggleVida: function(component, event, helper) {
        component.set("v.toggleAddrem",!component.get('v.toggleAddrem'));
        component.set("v.sinObj.MX_SB_MLT_Mobilephone__c",'');
        component.set("v.sinObj.MX_SB_MLT_ExtPhone__c",'');
        component.set("v.BooleanExt1",false)
       },
    showPhone: function(component, event, helper) {
       var selectedMenuItemValU = event.getParam("value");
		var selectedMenuItemValue= selectedMenuItemValU.toLowerCase();
        component.set("v.toggleAddrem",!component.get('v.toggleAddrem'));
        component.set('v.sinObj.MX_SB_MLT_PhoneTypeAdditional__c',selectedMenuItemValue)
        if(selectedMenuItemValue==='oficina'){
           component.set("v.BooleanExt1",true)
        }
    },
    showPhone2: function(component, event, helper) {
        var selectedMenuItemValuE = event.getParam("value");
	var selectedMenuItemValue= selectedMenuItemValuE.toLowerCase();
            component.set("v.toggleAddrem2",!component.get('v.toggleAddrem2'));
        component.set('v.sinObj.MX_SB_MLT_PhoneTypeAdditional2__c',selectedMenuItemValue);
            if(selectedMenuItemValue==='oficina'){
               component.set("v.BooleanExt2",true)
            }
        },
    doInitLayVida : function(component, event, helper) {
    helper.limitDateSelected(component, event, helper);
	helper.changeTabNamePar(component, event);
    var telphone=component.get("v.Telefono");
	var TaskId= component.get("v.TaskId");
        component.set("v.relatedCall.Id",TaskId);
    var action1 = component.get("c.getRecordType");
        if(telphone!==undefined) {
         component.set("v.sinObj.MX_SB_MLT_Telefono__c",telphone);
        }
    var subramo= component.get('v.selected2');
    component.set("v.sinObj.MX_SB_MLT_SubRamo__c",subramo);
        if(subramo==='Ahorro' || subramo==='Salud' || subramo==='Fallecimiento') {
            action1.setParams({"devname":"MX_SB_MLT_RamoVida"});
        }
  	    action1.setCallback(this, function(response) {
            component.set("v.recordtypeVida",response.getReturnValue());
        });
        $A.enqueueAction(action1);
    },
    setPhoneAdType2:function(component,event, helper) {
        var tipophoneAdValue2= component.get("v.sinObj.MX_SB_MLT_PhoneTypeAdditional2__c");
        component.set("v.sinObj.MX_SB_MLT_PhoneTypeAdditional2__c",tipophoneAdValue2);
        if(tipophoneAdValue2==="oficina"){
            component.set("v.BooleanExt2",true);
        }
        else{
            component.set("v.BooleanExt2",false);
        }
    },
    closeToggleVida2: function(component, event, helper) {
        component.set("v.toggleAddrem2",!component.get('v.toggleAddrem2'));
        component.set("v.sinObj.MX_SB_MLT_Mobilephone2__c",'');
        component.set("v.sinObj.MX_SB_MLT_ExtPhone2__c",'');
        component.set("v.BooleanExt2",false)
       },
    setPhoneAdType:function(component,event) {
    	var tipophoneAdVal= component.get("v.sinObj.MX_SB_MLT_PhoneTypeAdditional__c");
        component.set("v.sinObj.MX_SB_MLT_PhoneTypeAdditional__c",tipophoneAdVal);
        if(tipophoneAdVal==="oficina"){
            component.set("v.BooleanExt1",true);
        }
        else{
            component.set("v.BooleanExt1",false);
        }
    },
    setFalse3: function(component,event,helper) {
        component.set("v.validate",'false');
		helper.valExt1(component,event,helper);
    },
    setPhoneType: function(component, event, helper){
        var tipophone= component.find("phoneType");
        var tipophoneValueV= tipophone.get("v.value");
        component.set("v.sinObj.MX_SB_MLT_PhoneType__c",tipophoneValueV);
    },
    caps4:function(component,event,helper){
        var mail= component.get("v.sinObj.MX_SB_MLT_CorreoElectronico__c");
        component.set("v.sinObj.MX_SB_MLT_CorreoElectronico__c",mail.toLowerCase());
    },
    NuevoOnsuccessVida: function(component, event, helper) {
        var recordtpVida= component.get("v.recordtypeVida");
        component.set("v.sinObj.RecordTypeId",recordtpVida);
         var reId=component.get("v.recordId");
        if(reId!==''){
            component.set("v.sinObj.Id", reId);
        }
        helper.requiredFields(component,event,helper);
        if(component.get("v.validate")==='false') {
            var sinies= component.get("v.sinObj");
            var action5 = component.get('c.upSertsini');
            action5.setParams({'sinIn': sinies});
            action5.setCallback(this, function(response) {
                if (response.getState() === 'SUCCESS') {
                    var recordId= response.getReturnValue().Id;
                    component.set("v.recordId",recordId);
                    let agenda = component.get("v.sinObj.MX_SB_MLT_AtencionVida__c");
                    helper.createTask(component, event, helper, agenda);
                    helper.updteWhatId(component,event,helper);
                    var navService = component.find("navService");
                    var pageReference5 = {
                        type: 'standard__recordPage',
                        attributes: {
                            "recordId": recordId,
                            "objectApiName": "Siniestro__c",
                            "actionName": "view"
                        }
                    }
                    navService.navigate(pageReference5);
                }
            });
            $A.enqueueAction(action5);
        }
        if(component.get("v.validate")==='true') {
            component.set("v.validate","true");
        }
        if(component.get("v.validate")===undefined) {
            var siniestr= component.get("v.sinObj");
            var action3 = component.get('c.upSertsini');
            action3.setParams({'sinIn': siniestr});
            action3.setCallback(this, function(response) {
                if (response.getState() === 'SUCCESS') {
                    var recordId= response.getReturnValue().Id;
                    component.set("v.recordId",recordId);
                    let agenda = component.get("v.sinObj.MX_SB_MLT_AtencionVida__c");
                    helper.createTask(component, event, helper, agenda);
                    helper.updteWhatId(component,event,helper);
                    var navService = component.find("navService");
                    var pageReference6 = {
                        attributes: {
                            "objectApiName": "Siniestro__c",
                            "recordId": recordId,
                            "actionName": "view"
                        },
                        type: 'standard__recordPage'
                    }
                    navService.navigate(pageReference6);
                }
            });
            $A.enqueueAction(action3);
        }
    },
    setFalse: function(component,event,helper){
    	component.set("v.validate",'false');
    	helper.valMobile(component,event,helper);
    },
    setFalse2: function(component,event,helper){
    	component.set("v.validate",'false');
    	helper.valMobile2(component,event,helper);
    },
    toastInfo: function(component,event,helper){
        var mensajeV=event.getParams().message;
        if(mensajeV.includes("Porfavor")) {
			component.set("v.validate",'true');
        }
    },
    setFalse4: function(component,event,helper) {
        component.set("v.validate",'false');
		helper.valExt2(component,event,helper);
    },
    caps1:function(component,event,helper){
        var NombCond= component.get("v.sinObj.MX_SB_MLT_NombreConductor__c");
        component.set("v.sinObj.MX_SB_MLT_NombreConductor__c",NombCond.replace(/[^\w\s]/gi,'').toUpperCase());
    },
   upperCase : function(component, event, helper) {
        let fieldSelected = event.getSource().getLocalId();
        helper.upperCase(component, event, helper, fieldSelected);
    },
    caps2:function(component,event,helper){
        var APCond= component.get("v.sinObj.MX_SB_MLT_APaternoConductor__c");
        component.set("v.sinObj.MX_SB_MLT_APaternoConductor__c",APCond.replace(/[^\w\s]/gi,'').toUpperCase());
    },
    validityPolicy : function(component, event, helper) {
       helper.validPolicyValues(component, event, helper);
    },
    caps3:function(component,event,helper) {
        var AMCond= component.get("v.sinObj.MX_SB_MLT_AMaternoConductor__c");
        component.set("v.sinObj.MX_SB_MLT_AMaternoConductor__c",AMCond.replace(/[^\w\s]/gi,'').toUpperCase());
    },
    validityDate : function(component, event, helper) {
        helper.limitDateSelected(component, event, helper);
    },
    validityInsurance : function(component, event, helper) {
        helper.validityValues(component, event, helper);
    },
})