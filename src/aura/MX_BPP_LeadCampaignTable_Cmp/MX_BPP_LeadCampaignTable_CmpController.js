({
    init: function (cmp, event, helper) {
        helper.initData(cmp, event, helper);
    },
    onchangeInfo: function (cmp, event, helper) {
        var amountFilter = cmp.find("amountFilter").get('v.value');
        var amountFilterTwo = cmp.find("amountFilterTwo").get('v.value');
        if(amountFilter.length > 25){
            cmp.find("amountFilter").set('v.value',amountFilter.substring(0,25));
        }
        if(amountFilterTwo.length > 25){
            cmp.find("amountFilterTwo").set('v.value',amountFilterTwo.substring(0,25));
        }
        helper.setLeadsTable(cmp, event, helper);
    },

    onchangeProduct: function (cmp, event, helper) {
        helper.getProduct(cmp, event, helper);
        helper.setLeadsTable(cmp, event, helper);
    },

    handleRowAction: function (cmp, event, helper) {
        helper.leadConversion(cmp, event, helper);
    },

    handleSorting: function (cmp, event, helper) {
    	helper.sortColumn(cmp, event, helper);
	},

    onchangeDiv: function (cmp, event, helper) {
        cmp.find('DOInfo').set('v.value','');
        cmp.find('staffInfo').set('v.value','');
    	helper.getInfoSucursal(cmp, event);
	},

    onchangeOff: function (cmp, event, helper) {
        cmp.find('staffInfo').set('v.value','');
    	helper.filterByUser(cmp, event, helper);
	}
})