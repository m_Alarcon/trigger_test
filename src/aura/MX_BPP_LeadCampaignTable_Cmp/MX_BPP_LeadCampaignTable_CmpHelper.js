({
	initData: function (cmp, event, helper) {
        this.getInfoUserL(cmp);
        this.getFamily(cmp);
        this.getStatus(cmp);
        cmp.set('v.renderForm', 'true');
    },
    getInfoUserL: function(cmp, event, helper) {
        var getuserInfo = cmp.get('c.userInfo');
        getuserInfo.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var userInfoValues = response.getReturnValue();
                if(userInfoValues && userInfoValues[0] && userInfoValues[0] === 'BPyP Estandar') {
                    cmp.set('v.userInfoList', userInfoValues);
                    this.getBanker(cmp);
                }else {
                    cmp.set('v.staffRole', 'true');
                    cmp.set('v.userInfoList', userInfoValues);
                    this.setInfoSelected(cmp);
                }
            }
        });
        $A.enqueueAction(getuserInfo);
    },
    setInfoSelected: function(cmp, event, helper) {
        var infoUsuario = cmp.get('v.userInfoList');
        if(infoUsuario[0] === 'BPyP STAFF' || infoUsuario[0] === 'Soporte BPyP' || infoUsuario[0] === 'Administrador del sistema') {
            this.getInfoDivision(cmp,event);
        } else if(infoUsuario[0] === 'BPyP Director Divisional') {
            cmp.find('DDInfo').set('v.value', infoUsuario[1]);
            cmp.find('DDInfo').set("v.disabled", true);
            cmp.set('v.DDData', [infoUsuario[1]]);
            this.getInfoSucursal(cmp, event);
        } else if(infoUsuario[0] === 'BPyP Director Oficina' || infoUsuario[0] === 'BPyP Banquero Asociado') {
            cmp.set('v.DDData', [infoUsuario[1]]);
        	cmp.set('v.DOData', [infoUsuario[2]]);
            cmp.find('DDInfo').set('v.value', infoUsuario[1]);
            cmp.find('DOInfo').set('v.value', infoUsuario[2]);
            cmp.find('DDInfo').set("v.disabled", true);
            cmp.find('DOInfo').set("v.disabled", true);
            this.filterByUser(cmp, event, helper);
        }
    },
    getBanker: function (cmp, event, helper) {
        var toastEventB = $A.get("e.force:showToast");
        cmp.set('v.IsSpinner', true);
        var actionB = cmp.get('c.userData');
        actionB.setCallback(this, function (response) {
            if (actionB.getState() === 'SUCCESS') {
                var result = response.getReturnValue();
                if (result) {
                    if (result.length === 1) {
                        cmp.set('v.bankersData', result);
                        cmp.set('v.bankerRole', 'true');
                        cmp.find('bankerInfo').set('v.value', result[0].Id);
                        this.setLeadsTable(cmp);
                    } else if (result.length > 1) {
                        cmp.set('v.staffsData', result);
                        cmp.set('v.staffRole', 'true');
                        this.filterByUser(cmp, event, helper);
                    }
                    cmp.set('v.IsSpinner', false);
                }
            } else if (actionB.getState() === 'ERROR') {
                var errors = response.getError()[0].fieldErrors.CloseDate[0].message;
                cmp.set('v.IsSpinner', false);

                toastEventB.setParams({
                    title: "Error",
                    message: errors,
                    type: "error"
                });
                toastEventB.fire();
            }
        });
        $A.enqueueAction(actionB);
    },
    getFamily: function (cmp, event, helper) {
        var toastEventF = $A.get("e.force:showToast");
        cmp.set('v.IsSpinner', true);
        var actionF = cmp.get('c.familyData');
        actionF.setCallback(this, function (response) {
            if (actionF.getState() === 'SUCCESS') {
                var result = response.getReturnValue();
                if (result) {
                    cmp.set('v.familiesData', result);
                    cmp.set('v.IsSpinner', false);
                }
            } else if (actionF.getState() === 'ERROR') {
                var errors = response.getError()[0].fieldErrors.CloseDate[0].message;
                cmp.set('v.IsSpinner', false);

                toastEventF.setParams({
                    title: "Error",
                    message: errors,
                    type: "error"
                });
                toastEventF.fire();
            }
        });
        $A.enqueueAction(actionF);
    },

    getProduct: function (cmp, event, helper) {
        var toastEventP = $A.get("e.force:showToast");
        cmp.set('v.IsSpinner', true);
        var productString = 'Opportunity.MX_RTL_Producto__c';
        var actionP = cmp.get('c.getPicklistValues');
        var selected = cmp.find('familyInfo').get('v.value');
        actionP.setParams({
            picklistObject: productString
        });

        actionP.setCallback(this, function (response) {
            if (actionP.getState() === 'SUCCESS') {
                var result = response.getReturnValue();
                if (result) {
                    var res = result[selected];
                    cmp.set('v.productsData', res);
                    cmp.set('v.IsSpinner', false);
                }
            } else if (actionP.getState() === 'ERROR') {
                var errors = response.getError()[0].fieldErrors.CloseDate[0].message;
                cmp.set('v.IsSpinner', false);

                toastEventP.setParams({
                    title: "Error",
                    message: errors,
                    type: "error"
                });
                toastEventP.fire();
            }
        });
        $A.enqueueAction(actionP);
    },

    getStatus: function (cmp, event, helper) {
        var toastEventS = $A.get("e.force:showToast");
        cmp.set('v.IsSpinner', true);
        var actionS = cmp.get('c.statusData');
        actionS.setCallback(this, function (response) {
            if (actionS.getState() === 'SUCCESS') {
                var result = response.getReturnValue();
                if (result) {
                    cmp.set('v.statussData', result);
                    cmp.set('v.IsSpinner', false);
                }
            } else if (actionS.getState() === 'ERROR') {
                var errors = response.getError()[0].fieldErrors.CloseDate[0].message;
                cmp.set('v.IsSpinner', false);

                toastEventS.setParams({
                    title: "Error",
                    message: errors,
                    type: "error"
                });
                toastEventS.fire();
            }
        });
        $A.enqueueAction(actionS);
    },

    controlFamilyP: function(cmp, family, all) {
        if (family === 'Captación' || family === 'Colocación' || family === 'Servicios' || family === 'Seguros') {
            cmp.set('v.productText', all);
            cmp.find('productInfo').set('v.disabled', false);
        } else {
            cmp.find('productInfo').set('v.disabled', true);
            cmp.set('v.productText', 'Selecciona una familia');
            cmp.find('productInfo').set('v.value', '');
        }
    },

    fetchListUser: function (cmp, staff) {
        var listUsersV = []
        if(cmp.get('v.listUsers').length > 0 && staff.length === 0) {
            listUsersV = cmp.get('v.listUsers');
        }else {
            var banquero = cmp.find('staffInfo').get('v.value') ? cmp.get('v.staffsData').find(element => element.Id === cmp.find('staffInfo').get('v.value')) : '' ;
            listUsersV.push(banquero);
        }
        return listUsersV;
    },

    setLeadsTable: function (cmp, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        var bankerRole = cmp.get('v.bankerRole');
        cmp.set('v.IsSpinner', true);
        var arregloTableBank = this.getArrayTable();
        var informacionUser = cmp.get('v.userInfoList');
        if (bankerRole || informacionUser[0] === 'BPyP Banquero Asociado') {
            var actions = [{
                label: 'Generar oportunidad',
                name: 'conversion'
            }]
            arregloTableBank.push({
                    type: 'action',
                    typeAttributes: {
                        rowActions: actions
                    }
                });
            cmp.set('v.leadColumns', arregloTableBank);
        } else {
            cmp.set('v.leadColumns', arregloTableBank);
        }
        var owner;
        var staffRole = cmp.get('v.staffRole');
        var action = cmp.get('c.getLeadData');
        var account = cmp.find('accountFilter').get('v.value');
        var lead = cmp.find('leadFilter').get('v.value');
        var family = cmp.find('familyInfo').get('v.value');
        var origin = cmp.find('originInfo').get('v.value');
        var product = cmp.find('productInfo').get('v.value');
        var amount = cmp.find('amountFilter').get('v.value');
        var amountTwo = cmp.find('amountFilterTwo').get('v.value');
        var endDate = cmp.find('endDateFilter').get('v.value');
        var endDateTwo = cmp.find('endDateFilterTwo').get('v.value');
        var status = cmp.find('statusInfo').get('v.value');
        var all = cmp.get('v.all');
        var listUsersV = [];

        this.controlFamilyP(cmp, family, all);

        if (staffRole) {
            var staff = cmp.find('staffInfo').get('v.value');
            owner = staff;
            listUsersV = this.fetchListUser (cmp, staff);
        } else if (bankerRole) {
            var banker = cmp.find('bankerInfo').get('v.value') ? cmp.get('v.bankersData').find(element => element.Id === cmp.find('bankerInfo').get('v.value')) : '' ;
            owner = banker.Id;
            listUsersV.push(banker);
        }

        var parametos = [account, lead, origin, family, product, amount, amountTwo, endDate, endDateTwo, status, owner];
        action.setParams({
            params: parametos,
            listUsers: listUsersV
        });

        action.setCallback(this, function (response) {
            if (action.getState() === 'SUCCESS') {
                var result = response.getReturnValue();
                if (result) {
                    cmp.set('v.leadDataOrigin', result);
                    this.assignDataToTable(cmp, result);
                    cmp.set('v.IsSpinner', false);
                }
            } else if (action.getState() === 'ERROR') {
                var errors = response.getError()[0].fieldErrors.CloseDate[0].message;
                cmp.set('v.IsSpinner', false);

                toastEvent.setParams({
                    title: "Error",
                    message: errors,
                    type: "error"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },

    assignDataToTable: function (cmp, listData) {
        var dataProcessing = [];
        for(var i in listData) {
            dataProcessing.push({
                Account_URL : "/" + listData[i].Lead.MX_WB_RCuenta__c,
                NameAccount : listData[i].Lead.MX_WB_RCuenta__r.Name,
                Lead_URL : "/" + listData[i].LeadId,
                NameLead : listData[i].Lead.Name,
                DescriptionLead : listData[i].Lead.Description,
                SourceLead : listData[i].Lead.LeadSource,
                FamilyCampaign : listData[i].Campaign.MX_WB_FamiliaProductos__r.Name,
                ProductCampaign : listData[i].Campaign.MX_WB_Producto__r.Name,
                AmountLead : listData[i].Lead.MX_LeadAmount__c,
                EndDateLead : listData[i].MX_LeadEndDate__c,
                StatusLead : listData[i].Status,
                PropietarioLead : listData[i].Lead.Owner.Name
            });
        }

        cmp.set('v.leadData', dataProcessing);
    },

    leadConversion: function (cmp, event, helper) {
        var row = event.getParam('row');
        var toastEvent = $A.get("e.force:showToast");
        cmp.set('v.IsSpinner', true);

        var action = cmp.get('c.leadConv');

        var leadId = (row.Lead_URL).slice(1);
        var rows = cmp.get('v.leadData');
        var rowIndex = rows.indexOf(row);
        action.setParams({
            leadId: leadId,
        });

        action.setCallback(this, function (response) {
            if (action.getState() === 'SUCCESS') {
                var result = response.getReturnValue();
                if (result) {
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": result.Id,
                        "slideDevName": "detail"
                    });
                    navEvt.fire();
                    rows.splice(rowIndex, 1);
                    cmp.set('v.leadData', rows);
                    cmp.set('v.IsSpinner', false);
                }
            } else if (action.getState() === 'ERROR') {
                var errors = response.getError()[0].fieldErrors.CloseDate[0].message;
                cmp.set('v.IsSpinner', false);

                toastEvent.setParams({
                    title: "Error",
                    message: errors,
                    type: "error"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },

    sortColumn: function (cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        this.sortData(cmp, fieldName, sortDirection);
    },

    sortData: function (component, fieldName, sortDirection) {
        var data = component.get("v.leadData");
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.leadData", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer ? function(x) {return primer(x[field])} : function(x) {return x[field]};
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            a = key(a)
            b = key(b)
            return reverse * ((a > b) - (b > a));
        }
    },
    getArrayTable: function() {
        var tableArray = [{
                    label: 'Cuenta',
                    fieldName: 'Account_URL',
                    type: 'url',
                    sortable: true,
                    typeAttributes: {
                        label: {
                            fieldName: 'NameAccount'
                        }
                    }
                },
                {
                    label: 'Campaña',
                    fieldName: 'Lead_URL',
                    type: 'url',
                    sortable: true,
                    typeAttributes: {
                        label: {
                            fieldName: 'NameLead'
                        },
                        tooltip: {
                            fieldName: 'DescriptionLead'
                        }
                    }
                },
                {
                    label: 'Origen',
                    fieldName: 'SourceLead',
                    sortable: true,
                    type: 'text'
                },
                {
                    label: 'Familia',
                    fieldName: 'FamilyCampaign',
                    sortable: true,
                    type: 'text'
                },
                {
                    label: 'Producto',
                    fieldName: 'ProductCampaign',
                    sortable: true,
                    type: 'text'
                },
                {
                    label: 'Monto',
                    fieldName: 'AmountLead',
                    sortable: true,
                    type: 'number'
                },
                {
                    label: 'Fecha fin',
                    fieldName: 'EndDateLead',
                    sortable: true,
                    type: 'text'
                },
                {
                    label: 'Estatus',
                    sortable: true,
                    fieldName: 'StatusLead',
                    type: 'text'
                },
                {
                    label: 'Propietario',
                    sortable: true,
                    fieldName: 'PropietarioLead',
                    type: 'text'
                }
            ];
        return tableArray;
    },

    fetchParameters: function(cmp, informacionUser, division, oficina, banquero) {
        var parametros = [];
      	if(informacionUser[0] === 'BPyP STAFF' || informacionUser[0] === 'Soporte BPyP' || informacionUser[0] === 'Administrador del sistema') {
            parametros = [division, oficina, banquero];
        } else if(informacionUser[0] === 'BPyP Director Divisional') {
            parametros = [informacionUser[1], oficina, banquero];
        } else if(informacionUser[0] === 'BPyP Director Oficina' || informacionUser[0] === 'BPyP Banquero Asociado') {
            parametros = [informacionUser[1],informacionUser[2], banquero];
        } else if(informacionUser[0] === 'BPyP Estandar') {
            const foundBan = cmp.get('bankersData').find(element => element.Id === cmp.find('v.bankerInfo').get('v.value'));
            parametros = [informacionUser[1],informacionUser[2], foundBan];
        }
        return parametros;
    },

    filterByUser: function (cmp, event, helper) {
        var fetchCampaignMember = cmp.get("c.fetchCampaignMember");
        var informacionUser = cmp.get('v.userInfoList');
        var banquero = cmp.find('staffInfo').get('v.value') ? cmp.get('v.staffsData').find(element => element.Id === cmp.find('staffInfo').get('v.value')) : '' ;
        var oficina = cmp.find('DOInfo').get('v.value') ? cmp.find('DOInfo').get('v.value') : '';
        var division = cmp.find('DDInfo').get('v.value') ? cmp.find('DDInfo').get('v.value') : '';
        var parametros = this.fetchParameters(cmp, informacionUser, division, oficina, banquero);

        fetchCampaignMember.setParams({
            params: parametros
        });
         fetchCampaignMember.setCallback(this, function (responseA) {
            if (fetchCampaignMember.getState() === 'SUCCESS') {
                var resultA = responseA.getReturnValue();
                if (resultA) {
                    cmp.set('v.listUsers', resultA);
                    if(informacionUser[0] !== 'BPyP Estandar') {
                    	cmp.set('v.staffsData', resultA);
                    }
                    this.setLeadsTable(cmp, event, helper);
                }
            } else if (fetchCampaignMember.getState() === 'ERROR') {
                var errorsA = responseA.getError()[0].fieldErrors.CloseDate[0].message;

                toastEvent.setParams({
                    title: "Error",
                    message: errorsA,
                    type: "error"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(fetchCampaignMember);
    },

    getInfoSucursal: function(cmp, event){
        var infoSucursal = cmp.get('c.fetchListSucursales');
        infoSucursal.setParams({
            division: cmp.find('DDInfo').get('v.value')
        });
        infoSucursal.setCallback(this, function (responseS) {
            if (infoSucursal.getState() === 'SUCCESS') {
                var resultS = responseS.getReturnValue();
                if (resultS) {
                    cmp.set('v.DOData',resultS);
                    this.filterByUser(cmp, event);
                }
            } else if (infoSucursal.getState() === 'ERROR') {
                var errorsS = responseS.getError()[0].fieldErrors.CloseDate[0].message;
                toastEvent.setParams({
                    title: "Error",
                    message: errorsS,
                    type: "error"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(infoSucursal);
    },

    getInfoDivision: function(cmp, event){
        var infoDivision = cmp.get('c.fetchListDivisiones');
        infoDivision.setCallback(this, function (responseD) {
            if (infoDivision.getState() === 'SUCCESS') {
                var resultD = responseD.getReturnValue();
                if (resultD) {
                    cmp.set('v.DDData',resultD);
                    this.filterByUser(cmp, event);
                }
            } else if (infoDivision.getState() === 'ERROR') {
                var errorsD = responseD.getError()[0].fieldErrors.CloseDate[0].message;
                toastEvent.setParams({
                    title: "Error",
                    message: errorsD,
                    type: "error"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(infoDivision);
    }
})