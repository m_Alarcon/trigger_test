({
    registraLlamada: function (component) {
        component.set("v.showModal", true);

        var flow = component.find("flowData");
        flow.startFlow("BPyP_Registro_de_llamada");
    },

    handleStatusChange: function (component, event) {
        if (event.getParam("status") === "FINISHED") {
            component.set("v.showModal", false);
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "¡Correcto!",
                message: "La llamada se guardo con éxito.",
                type: "success",
            });
            toastEvent.fire();
            $A.get("e.force:refreshView").fire();
        }
    },

    closeModal: function (component, event) {
        component.set("v.showModal", false);
    },

    handleRecordUpdated: function (component, event) {
        var eventParams = event.getParams();
        if (eventParams.changeType === "LOADED") {
            if (component.get("v.currentUser.Profile.Name") === "BPyP Banquero Apoderado") {
                component.set("v.apoderado", true);
            }
        }
    },
});