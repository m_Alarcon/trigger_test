({
    handleSuccessLayDanos : function(component, event, helper) {
        var payload = event.getParams().response;
        var navService = component.find("navService");
        var pageReference = {
            type: 'standard__recordPage',
            attributes: {
                "recordId": payload.id,
                "objectApiName": "Siniestro__c",
                "actionName": "view"
            }
        }
        event.preventDefault();
        navService.navigate(pageReference);
    },
    doInitLayDanos : function(component, event, helper) {
        var action = component.get("c.getRecordType");
	action.setParams({"devname":"MX_SB_MLT_RamoDanos"});
        action.setCallback(this, function(response) {
            component.set("v.recordtypeDanios",response.getReturnValue());
        });
        $A.enqueueAction(action);
    }
})