({
    InitTelefonicaRobo : function(component,event,helper) {
        var action = component.get("c.getRecordType");
        action.setParams({"devname":"MX_SB_MLT_RamoAuto"});
        action.setCallback(this, function(response) {
            component.set("v.recordtypeAUTO",response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    handleSuccessRobo : function(component, event) {
    var payload = event.getParams().response;
        var navService = component.find("navService");
        var pageReference = {
            type: 'standard__recordPage',
            attributes: {
                "recordId": payload.id,
                "objectApiName": component.get("v.sObjectName"),
                "actionName": "view"
            }
        }
        event.preventDefault();
        navService.navigate(pageReference);
        if(component.get('v.continuar') === false){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
            "title": "Actualización de registro",
                    "type":"success",
            "message": "Cambio exitoso"
            });
            toastEvent.fire();
        }else{
             var toast = $A.get("e.force:showToast");
            toast.setParams({
            "title": "Actualización de registro",
                    "type":"success",
            "message": "Cambio exitoso"
            });
            toast.fire();
        }
    },
    handleEditFormRobo : function(component) {
        component.set('v.status','Detalles del Siniestro');
        component.set('v.continuar',false);
        component.find("SiniestroEditForm").submit();
	},
    handleEditFormRoboCon : function(component) {
        component.set('v.status','Crear Servicios');
        component.set('v.continuar',true);
	    component.find("SiniestroEditForm").submit();
	}

})