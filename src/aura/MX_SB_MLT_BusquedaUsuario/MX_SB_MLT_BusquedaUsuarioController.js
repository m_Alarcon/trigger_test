({
    init : function(component, event, helper) {
         var actions = [
            { label: 'Seleccionar', name: 'ac_select' }
        ];

        var tipo = component.get("v.objetoTipo");
        switch (tipo) {
            case 'User':
                component.set('v.columns', [
            {label: 'Nombre', fieldName: 'Name', type: 'Url'},
            {label: 'Trasporte', fieldName: 'MX_SB_MLT_TipoTrasporte__c', type: 'text'},
            {label: 'ID Claim Center', fieldName: 'MX_SB_MLT_ClaimCenterId__c', type: 'text'},
              { type: 'action', typeAttributes: { rowActions: actions } }
        ]);
                break;
            case 'Account':
                component.set('v.columns', [
            {label: 'Nombre', fieldName: 'Name', type: 'text'},
            {label: 'Categoria', fieldName: 'MX_SB_MLT_TipoProveedor_MA__c', type: 'text'},
            {label: 'Contacto', fieldName: 'nombreIntermediario__c', type: 'text'},
            {label: 'Número', fieldName: 'Phone', type: 'text'},
            {label: 'ID Claim Center', fieldName: 'Id', type: 'text'},
              { type: 'action', typeAttributes: { rowActions: actions } }
        ]);
                helper.listaInicial(component,event);
                break;
        }
    },
	buscarBtn : function(component, event, helper) {
        helper.buscaUsu(component,event);
    },
    cierraMod : function(component){
          var cmpEvent = component.getEvent("asignacionAjustadorEvent");
         cmpEvent.fire();
    },
    buscaCmp :function(component,helper){
        helper.buscaUsu(component);
    },
    seleccion :function(component,event) {
        helper.selecUsu(component,event);
    },
    handleRowAction :function(cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'detalle':
                break;
            case 'ac_select':
                helper.enviarDato(cmp, row);
                break;
        }
    },
})