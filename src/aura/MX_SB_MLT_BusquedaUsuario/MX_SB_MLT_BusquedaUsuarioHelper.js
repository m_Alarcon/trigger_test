({
	buscaUsu : function(component) {
		var tipo = component.get("v.objetoTipo");
        var action = component.get("c.buscaUsu");
        switch (tipo) {
            case 'User':
                action = component.get("c.buscaUsu");
                break;
            case 'Account':
                action = component.get("c.buscaCuenta");
                break;
        }
        var textoBusqueda = component.get("v.textoBusqueda");
        action.setParams({
            "nombre": textoBusqueda
        });
        action.setCallback(this, function(res) {
                var storedResponse = res.getReturnValue();
                var numTot = storedResponse.length;
                component.set('v.data',storedResponse);
                component.set('v.numRes',numTot);
        });
        $A.enqueueAction(action);
	},
    enviarDato :function(component,row) {
        var cmpEvent = component.getEvent("asignacionAjustadorEvent");
        cmpEvent.setParams({"idSalesforce" : row.Id,"nombre" : row.Name,"idClaim" :row.MX_SB_MLT_ClaimCenterId__c});
        cmpEvent.fire();
    },
    listaInicial : function(component,event) {
        var tipo = component.get('v.tipo');
        var action = component.get('c.busquedaIni');
        action.setParams({"tipo" : tipo});
        action.setCallback(this, function(res) {
             var storedResponse = res.getReturnValue();
                var numTot = storedResponse.length;
                component.set('v.data',storedResponse);
                component.set('v.numRes',numTot);
        });
        $A.enqueueAction(action);
    }
})