({
    doInitDetallesTelefonica : function(component, event, helper) {
        var action = component.get("c.getRecordType");
        action.setParams({"devname":"MX_SB_MLT_RamoAuto"});
        action.setCallback(this, function(response) {
            component.set("v.recordtypeAUTO",response.getReturnValue());
        });
        $A.enqueueAction(action);
        helper.dateValidation(component, event, helper);
        helper.selectedDate(component, event, helper);
    },
    handleSuccessDetallesTelefonica : function(component, event) {
        var payload = event.getParams().response;
        var navService = component.find("navService");
        var pageReference = {
            type: 'standard__recordPage',
            attributes: {
                "recordId": payload.id,
                "objectApiName": component.get("v.sObjectName"),
                "actionName": "view"
            }
        }
        event.preventDefault();
        navService.navigate(pageReference);
        let etapa = component.get("v.etapa");
        let toastEvent = $A.get("e.force:showToast");
        if(etapa === 'Detalles del Siniestro') {
            toastEvent.setParams({
                title : 'Exito',
                message:'Su registro a sido guardado correctamente.',
                messageTemplate: 'Registro Guardado',
                type: 'success'
            });
            toastEvent.fire();
        } else {
            toastEvent.setParams({
                title : 'Continue con la toma de reporte',
                message:'Su registro a sido guardado correctamente.',
                messageTemplate: 'Registro Guardado',
                type: 'success'
            });
            toastEvent.fire();
        }
    },
     validacionForm : function(component, event, helper) {
            helper.validityFields(component, event, helper);
        	helper.comboBox(component, event, helper);
    },
    setDate : function(component, event, helper) {
        helper.assignDate(component, event, helper);
    },
    cambiarEtapa : function(component, event, helper) {
        component.set("v.etapa",'Crear Servicios');
        let validation = component.get('c.validacionForm');
        $A.enqueueAction(validation);
        var servicio = component.get('c.enviarWSAjustador');
    	var tipoSin= component.get("v.SinRecord.TipoSiniestro__c");
        var recordIde = component.get("v.recordId");
        if(tipoSin==='Colisión'){
            servicio.setParams({"idSini":recordIde});
        	servicio.setCallback(this, function(response) {});
        $A.enqueueAction(servicio);
        }
    },
})