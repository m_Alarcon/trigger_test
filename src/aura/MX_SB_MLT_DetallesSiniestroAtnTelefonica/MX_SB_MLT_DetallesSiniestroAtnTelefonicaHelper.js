({
    comboBox : function(component, event, helper) {
        var message = $A.get("e.force:showToast");
        if(component.get("v.SinRecord").TipoSiniestro__c==='Cristales'){
        var values = component.find("comboBox").get("v.value");
        let conteo = values.split(";");
            if (conteo.length > 3) {
                message.setParams({
                    title : 'Error',
                    message:'Se ha rebasado el limite de cristales seleccionados',
                    messageTemplate: 'El maximo de selección es de tres cristales ',
                    duration:' 4000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                event.preventDefault();
                message.fire();
            }
        }
    },
    dateValidation : function(component, event, helper) {
        var dateToday = new Date();
        var begin = new Date(dateToday.getTime());
        var dateInit = $A.localizationService.formatDate(begin, "yyyy-MM-dd");
        var hourInit=$A.localizationService.formatDate(begin, "HH:mm:ssZ");
        var minimot = dateInit+'T'+hourInit;
        component.set('v.fechafin',minimot);
    },
    assignDate : function(component, event, helper) {
        let valueSet = component.find("fechaInput").get("v.value");
        component.set("v.fechaInput",valueSet);
    },
    selectedDate : function(component, event, helper) {
        let recordId = component.get("v.recordId");
        var getSinValues = component.get("c.getDatosSiniestro");
        getSinValues.setParams({"idsini":recordId});
        getSinValues.setCallback(this, function(response){
            component.set("v.fechaInput",response.getReturnValue().MX_SB_MLT_Fecha_Hora_Siniestro__c);
        });
        $A.enqueueAction(getSinValues);
    },
    validityFields : function(component, event, helper) {
        let valueEvaluated = component.find("fechaInput").get("v.validity");
        let message = $A.get("e.force:showToast");
        if(!valueEvaluated.valid) {
            message.setParams({
                title : 'Error',
                message:'La selección de fecha es erronea o vacía.',
                messageTemplate: 'Seleccione una fecha ',
                duration:' 4000',
                key: 'info_alt',
                type: 'error',
                mode: 'pester'
            });
            event.preventDefault();
            message.fire();
        }
    },
})