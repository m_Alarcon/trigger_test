({
	showInfoGMM: function(component, event, helper) {
	var selectedMenuItemValU = event.getParam("value");
	var selectedMenuItemValue= selectedMenuItemValU.toLowerCase();
        component.set("v.toggleAddrem",!component.get('v.toggleAddrem'));
        component.set('v.sinObj.MX_SB_MLT_PhoneTypeAdditional__c',selectedMenuItemValue)
        if(selectedMenuItemValue==='oficina'){
           component.set("v.BooleanExt1",true)
        }
    },
    showInfoGMM2: function(component, event, helper) {
    var selectedMenuItemValuE = event.getParam("value");
	var selectedMenuItemValue= selectedMenuItemValuE.toLowerCase();
        component.set('v.sinObj.MX_SB_MLT_PhoneTypeAdditional2__c',selectedMenuItemValue);
        component.set("v.toggleAddrem2",!component.get('v.toggleAddrem2'));
		if(selectedMenuItemValue==='oficina'){
           component.set("v.BooleanExt2",true)
        }
    },
	closeToggleGMM2: function(component, event, helper) {
     component.set("v.toggleAddrem2",!component.get('v.toggleAddrem2'));
     component.set("v.sinObj.MX_SB_MLT_Mobilephone2__c",'');
     component.set("v.sinObj.MX_SB_MLT_ExtPhone2__c",'');
     component.set("v.BooleanExt2",false)
    },
    closeToggleGMM: function(component, event, helper) {
     component.set("v.toggleAddrem",!component.get('v.toggleAddrem'));
     component.set("v.sinObj.MX_SB_MLT_Mobilephone__c",'');
     component.set("v.sinObj.MX_SB_MLT_ExtPhone__c",'');
     component.set("v.BooleanExt1",false)
    },
    doInitLayGMM : function(component, event, helper) {
	helper.changeTabNamePar(component, event);
    var action = component.get("c.getRecordType");
    component.set("v.sinObj.MX_SB_MLT_Telefono__c",component.get("v.Telefono"))
	action.setParams({"devname":"MX_SB_MLT_RamoGMM"});
    action.setCallback(this, function(response) {
            component.set("v.recordtypeGMM",response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
     setPhoneAdType:function(component,event, helper) {
        var tipophoneAdVal= component.get("v.sinObj.MX_SB_MLT_PhoneTypeAdditional__c");
        component.set("v.sinObj.MX_SB_MLT_PhoneTypeAdditional__c",tipophoneAdVal);
        if(tipophoneAdVal==="Oficina"){
            component.set("v.BooleanExt1",true);
        }
        else{
            component.set("v.BooleanExt1",false);
        }
    },
    setPhoneAdType2:function(component,event, helper) {
        var tipophoneAdVal2= component.get("v.sinObj.MX_SB_MLT_PhoneTypeAdditional2__c");
        component.set("v.sinObj.MX_SB_MLT_PhoneTypeAdditional2__c",tipophoneAdVal2);
        if(tipophoneAdVal2==="Oficina"){
            component.set("v.BooleanExt2",true);
        }
        else{
            component.set("v.BooleanExt2",false);
        }
    },
    setPhoneType: function(component, event, helper){
        var tipophone= component.find("phoneType");
        var tipophoneVal= tipophone.get("v.value");
        component.set("v.sinObj.MX_SB_MLT_PhoneType__c",tipophoneVal);
    },
    NuevoOnsuccess: function(component, event, helper) {
        var recordtpGMM= component.get("v.recordtypeGMM");
        component.set("v.sinObj.RecordTypeId",recordtpGMM);
        var recIdG=component.get("v.recordId");
        if(recIdG!==''){
            component.set("v.sinObj.Id", recIdG);
        }
        component.set("v.sinObj.MX_SB_MLT_Telefono__c",component.get("v.Telefono"));
        helper.requiredFields(component,event,helper);
        if(component.get("v.validate")==='true') {
            component.set("v.validate","true");
        }
        if(component.get("v.validate")==='false') {
            var sinies= component.get("v.sinObj");
            var action2G = component.get('c.upSertsini');
            component.set("v.disabled",true);
            action2G.setParams({'sinIn': sinies});
            action2G.setCallback(this, function(response) {
                if (response.getState() === 'SUCCESS') {
                    helper.toastSuccessGMM(component,event,helper);
                    component.set("v.disabled",true);
                }
                else{
                    component.set("v.disabled",false);
                }
            });
            $A.enqueueAction(action2G);
        }
        if(component.get("v.validate")===undefined) {
            var siniestr= component.get("v.sinObj");
            var action3G = component.get('c.upSertsini');
            component.set("v.disabled",true);
            action3G.setParams({'sinIn': siniestr});
            action3G.setCallback(this, function(response) {
                if (response.getState() === 'SUCCESS') {
                    helper.toastSuccessGMM(component,event,helper);
                    component.set("v.disabled",true);
                }
                else{
                    component.set("v.disabled",false);
                }
            });
            $A.enqueueAction(action3G);
        }
    },
    setFalse: function(component,event,helper){
    	component.set("v.validate",'false');
    	helper.valMobile(component,event,helper);
    },
    toastInfo: function(component,event,helper){
        var mensajeG=event.getParams().message;
        if(mensajeG.includes("Porfavor")) {
			component.set("v.validate",'true');
        }
    },
    setFalse2: function(component,event,helper){
    	component.set("v.validate",'false');
    	helper.valMobile2(component,event,helper);
    },
    setPhone: function(component,event,helper) {
        component.set("v.validate",'false');
        var phoneG = component.find('PhoneIdenAtn');
		var phoneValueG = phoneG.get('v.value');
        component.set("v.sinObj.MX_SB_MLT_Telefono__c",phoneValueG.replace(/[^0-9\.]+/g,''));

        var phoneValidityG = component.find("PhoneIdenAtn").get("v.validity");
        var phoneValidG = phoneValidityG.valid;
        if(isNaN(phoneValueG)){
			component.set('v.errorMessage1',"Porfavor ingrese un numero valido");
        }
        if(phoneValidG===false){
			component.set('v.errorMessage1',"Su entrada es demasiado corta.");
        }
		else{
            component.set('v.errorMessage1',null);
    	}
    },
    caps1:function(component,event,helper){
        var NombCondG= component.get("v.sinObj.MX_SB_MLT_NombreConductor__c");
        component.set("v.sinObj.MX_SB_MLT_NombreConductor__c",NombCondG.replace(/[^\w\s]/gi,'').toUpperCase());
    },
    caps4:function(component,event,helper){
        var mail= component.get("v.sinObj.MX_SB_MLT_CorreoElectronico__c");
        component.set("v.sinObj.MX_SB_MLT_CorreoElectronico__c",mail.toLowerCase());
    },
    caps2:function(component,event,helper){
        var APCondG= component.get("v.sinObj.MX_SB_MLT_APaternoConductor__c");
        component.set("v.sinObj.MX_SB_MLT_APaternoConductor__c",APCondG.replace(/[^\w\s]/gi,'').toUpperCase());
    },
    caps3:function(component,event,helper){
        var AMCondG= component.get("v.sinObj.MX_SB_MLT_AMaternoConductor__c");
        component.set("v.sinObj.MX_SB_MLT_AMaternoConductor__c",AMCondG.replace(/[^\w\s]/gi,'').toUpperCase());
    },
    setFalse4: function(component,event,helper) {
        component.set("v.validate",'false');
		helper.valExt2(component,event,helper);
    },
    setFalse3: function(component,event,helper) {
        component.set("v.validate",'false');
		helper.valExt1(component,event,helper);
    },
})