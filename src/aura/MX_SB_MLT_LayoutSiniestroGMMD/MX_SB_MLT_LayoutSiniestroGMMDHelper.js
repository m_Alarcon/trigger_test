({
    valExt1:function(component,event){
   var Exte1= component.get('v.sinObj.MX_SB_MLT_ExtPhone__c');
   component.set('v.sinObj.MX_SB_MLT_ExtPhone__c',Exte1.replace(/[^0-9\.]+/g,''));
},
requiredFields: function(component) {
   var nombre = component.find("NameIdenAtn");
   nombre.reportValidity();
   nombre = nombre.get("v.validity");
   var apaterno = component.find("LastNameIdenAtn");
   apaterno.reportValidity();
   apaterno=apaterno.get("v.validity");
   var amaterno = component.find("LastName2IdenAtn");
   amaterno.reportValidity();
   amaterno = amaterno.get("v.validity");
   if(!nombre.valid || !apaterno.valid || !amaterno.valid) {
           this.toastTelefono();
   }
   else{
       component.set("v.validate",'false');
   }
},
   valExt2:function(component,event){
   var Exte2= component.get('v.sinObj.MX_SB_MLT_ExtPhone2__c');
   component.set('v.sinObj.MX_SB_MLT_ExtPhone2__c',Exte2.replace(/[^0-9\.]+/g,''));
},
toastSuccessGMM: function(){
   var toastEvtGMM = $A.get("e.force:showToast");
   toastEvtGMM.setParams({
       title: 'Enhorabuena',
       message: 'Se ha guardado el registro exitosamente',
       key: 'info_alt',
       type: 'Success'
   });
   toastEvtGMM.fire();
},
toastTelefono : function () {
   var toastEvent = $A.get("e.force:showToast");
       toastEvent.setParams({
           title : 'Error:',
           message:'Porfavor llene los campos obligatorios.',
           duration:' 2000',
           key: 'info_alt',
           type: 'error'
       });
       toastEvent.fire();
},
valMobile: function(component,event) {
   var Mobile = component.find('MobilePhoneIdenAtn');
   var MobileValue = Mobile.get('v.value');
   component.set("v.sinObj.MX_SB_MLT_Mobilephone__c",MobileValue.replace(/[^0-9\.]+/g,''));
   var phoneValidity = component.find("MobilePhoneIdenAtn").get("v.validity");
   var phoneValid = phoneValidity.valid;
   if(phoneValid===false) {
       component.set('v.errorMessage2',"Su entrada es demasiado corta.");
   }
   if(isNaN(MobileValue)) {
       component.set('v.errorMessage2',"Porfavor ingrese un numero valido");
   }
   else{
       component.set('v.errorMessage2',null);
   }
},
valMobile2: function(component,event) {
   var Mobile2 = component.find('MobilePhoneIdenAtn2');
   var MobileValue2 = Mobile2.get('v.value');
   component.set("v.sinObj.MX_SB_MLT_Mobilephone2__c",MobileValue2.replace(/[^0-9\.]+/g,''));
   var phoneValidity2 = component.find("MobilePhoneIdenAtn2").get("v.validity");
   var phoneValid2 = phoneValidity2.valid;
   if(phoneValid2===false) {
       component.set('v.errorMessage2',"Su entrada es demasiado corta.");
   }
   if(isNaN(MobileValue2)) {
       component.set('v.errorMessage2',"Porfavor ingrese un numero valido");
   }
   else{
       component.set('v.errorMessage2',null);
   }
},
changeTabNamePar : function(component, event) {
  var workspaceAPIDataPart = component.find("workspace");
  workspaceAPIDataPart.getFocusedTabInfo().then(function(response) {
                  var focusedTabId = response.tabId;
                  workspaceAPIDataPart.setTabLabel({
                  tabId: focusedTabId,
                  label: "Crear Siniestro/Asistencia",
                  title: "Crear Siniestro/Asistencia"
                  });
       })
  .catch(function(error) {
});
},
})