({
    doInit2ProveedoresColision : function(c,e,h) {
        h.doInit_HelperProveedoresColision(c,e,h)
    },
     handleCancelProveedoresColision: function(component) {
        component.set('v.isEditMode', false);
    },
    doInitProveedoresColision : function(component) {
	var action = component.get("c.getRecordType");
        action.setParams({"devname":"MX_SB_MLT_RamoAuto"});
        action.setCallback(this, function(response) {
            component.set("v.recordtypeAUTO",response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    handleSuccessProveedoresColision : function(component, event) {
	var payload = event.getParams().response;
        var navService = component.find("navService");
        var pageReference = {
            type: 'standard__recordPage',
            attributes: {
                "recordId": payload.id,
                "objectApiName": component.get("v.sObjectName"),
                "actionName": "view"
            }
        }
        event.preventDefault();
        navService.navigate(pageReference);
    },
    toggleEditModeProveedoresColision : function(component) {
        component.set("v.isEditMode", !component.get("v.isEditMode"));
    },
    handleEditFormProveedoresColision : function(component) {
        component.find("SiniestroEditForm").submit();
        $A.get("e.force:editRecord").fire();
        component.set('v.isEditMode', false);
    }
})