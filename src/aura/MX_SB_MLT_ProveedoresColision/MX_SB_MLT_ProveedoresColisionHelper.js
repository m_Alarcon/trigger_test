({
    doInit_HelperProveedoresColision : function(c,e,h) {
        var id = c.get("v.recordId");
        var action = c.get("c.getProveedorInfo");
        action.setParams({
            "sinid": id
        });
                action.setCallback(this, function(respo) {
                var storedResponse4 = respo.getReturnValue();
                    var newObjList4 = [];
                    for (var i = 0; i < storedResponse4.length; i++) {
                        var obj4 = storedResponse4[i];
                        var newObj4 = {};
                        for (var key4 in obj4) {
                            if (obj4.hasOwnProperty(key4)) {
                                var keyList4 = key4.split('__');
                                var newKey4 = '';
                                if (keyList4.length > 2) {
                                    newKey4 = keyList4[1].concat('__' + keyList4[2]);
                                } else {
                                    newKey4 = key4;
                                }
                                newObj4[newKey4] = obj4[key4];
                            }
                        }
                        c.set("v.SiniestroObject", newObj4);
                        newObjList4.push(newObj4);
                    }
                    c.set("v.SiniestroObjectList", newObjList4);
        });
        $A.enqueueAction(action);
    },
})