({
	toastsubRamo : function () {
        var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Alerta:',
                message:'Porfavor seleccione una sublinea de negocio.',
                duration:' 2000',
                key: 'info_alt',
                type: 'info'
            });
            toastEvent.fire();
    },
})