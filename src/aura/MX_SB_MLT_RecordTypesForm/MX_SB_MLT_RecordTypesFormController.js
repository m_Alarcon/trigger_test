({
    handleSelect: function (cmp, event,helper) {
        var mapping2 = { '421' : 'Ahorro', '422' : 'Salud', '423' : 'Fallecimiento','431' :'Hogar'};
        cmp.set('v.selected2', mapping2[event.getParam('name')]);
        var mapping1 = { '41' : 'Auto', '42' : 'Vida', '43' : 'Daños',
                        '44' : 'GMM'};
        cmp.set('v.selected', mapping1[event.getParam('name')]);
        var selected = mapping1[event.getParam('name').toString()];
        if(selected==='Vida'){
            helper.toastsubRamo(cmp,event);
        }
    },
    init: function (cmp,evt) {
        var items = [{
            "name": "41",
            "label": "Auto",
                }, {
                "name": "42",
                "label": "Vida",
                "expanded": false,
                "items" :[ {
                    "label": "Ahorro",
                    "expanded": true,
                    "name": "421",
                    "items" :[]
                }, {
                    "label": "Salud",
                    "expanded": true,
                    "name": "422",
                    "items" :[]
                }, {
                    "label": "Fallecimiento",
                    "expanded": true,
                    "name": "423",
                    "items" :[]
                }]
            }, {
            "label": "Daños",
            "name": "43","expanded": false,
                "items" :[{
                    "label": "Hogar",
                    "name": "431",
                    "expanded": true,
                    "items" :[]
                }]
            }, {
            "label": "GMM",
            "name": "44",
                },
                    ];
        cmp.set('v.items', items);
    },
});