({
    inicializardatos : function(component,evnt,helper) {
        var obtieneSiniestro = component.get("c.siniestroActual");
        obtieneSiniestro.setParams({"sinId":component.get("v.recordId")});
        obtieneSiniestro.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.siniestroDatos",response.getReturnValue());
                component.set("v.rtype",response.getReturnValue().RecordType.DeveloperName);
            }
          });
        $A.enqueueAction(obtieneSiniestro);
    },
    consultaDatos : function(component, event, helper) {
        var showToastPol = helper.lockservicePers(component,event,helper);
        var showToastPer = helper.lockservicePol(component,event,helper);
        if (showToastPol){
            var varpoliza =component.get("v.polizaSrch");
            var vercertif =component.get("v.certificado");
            if(varpoliza==='' || vercertif==='')    {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error Message',
                    message:'Poliza y certificado son obligatorios para la busqueda',
                    messageTemplate: 'Debe completar formulario',
                    duration:' 3000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'});
                toastEvent.fire();
            } else {
                helper.consultaPol(component,event,helper);
            }
        } else if (showToastPer){
            var nombreAse =component.get("v.nombreAsegurado");
            var apellidoAse =component.get("v.aPaterno");
            if (nombreAse==='' || apellidoAse==='') {
                var toastEvent2 = $A.get("e.force:showToast");
                toastEvent2.setParams({
                    title : 'Error Message',
                    message:'Nombre y apellido del asegurado son obligatorios para la busqueda',
                    messageTemplate: 'Debe compoletar formulario',
                    duration:' 3000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'});
                toastEvent2.fire();
            }  else {
                helper.consultaPol(component,event,helper);
            }
        }
    },
    coberturas : function(component, event, helper) {
        helper.certificadosCobert(component,event,helper);
    },
    datosPart : function(component, event, helper) {
        helper.datosP(component,event,helper);
    },
    recibosPol : function(component, event, helper) {
        helper.recibos(component,event,helper);
    },
    actualizaPoliza : function(component, event, helper) {
        helper.polizacertificado(component,event,helper);
    },
    comparaDate : function(component,event,helper) {
        helper.comparaDate(component,event,helper);
    },
    polFields : function(component, event, helper){
        var str =event.getSource().get('v.value');
        var strname='v.'+event.getSource().get("v.name");
        component.set(strname,str.replace(/[^\w\s]/gi, ''));
        helper.lockservicePol(component, event, helper);
    },
    persFields : function(component, event, helper){
        var strp =event.getSource().get('v.value');
        var strnamep='v.'+event.getSource().get("v.name");
        component.set(strnamep,strp.replace(/[^\w\s]/gi, ''));
        helper.lockservicePers(component, event, helper);
    },
    muestrapoliza : function(component,event,helper) {
        helper.muestrapoliza(component,event,helper);
        component.set("v.showdivPoliza","mostrar");
    },
    comparaDateNacimiento : function(component,event,helper) {
        helper.comparaDateNacimiento(component,event,helper);
    },
    toggleSection : function(component, event, helper) {
        var sectionAuraId = event.target.getAttribute("data-auraId");
        var sectionDiv=component.find(sectionAuraId).getElement();
        var sectionState =sectionDiv.getAttribute('class').search('slds-is-open');
        if(sectionState === -1){
            sectionDiv.setAttribute('class' , 'slds-section slds-is-open');
            if(sectionAuraId ==="fruitsSection3"){
                var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Atencion',
                        message:'Póliza Cancelada',
                        messageTemplate: 'Póliza Cancelada',
                        duration:' 2000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'});
                    toastEvent.fire();
            }
        }else{
            sectionDiv.setAttribute('class' , 'slds-section slds-is-close');
        }
    },
    volveretapa : function(component) {
          component.set("v.Step","S1");
    },
    avanzar: function(component,event,helper){
        var validaahorro = ['Supervivencia','Metasegura','Metaeducación','Seguroestudia','ILP','MLB','Pensiones'];
        if(validaahorro.indexOf(component.get("v.polizaSeleccionada").MX_WB_Producto__r.Name)>=0){
            helper.validaSupervivencia(component,event,helper);
            helper.validaILP(component,event,helper);
        } else {
            helper.avanzar(component,event,helper);
        }
    },
    cerrartr: function(component,event,helper){
        helper.cerrartr(component,event,helper);
    },
        actualizaafectacion: function (component,event,helper) {
        component.set("v.afectaCoberturas",event.getParam("afecta"));
    },
    })