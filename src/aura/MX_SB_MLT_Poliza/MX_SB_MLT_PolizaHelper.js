({  certificadosCobert : function(compCobert, event, helper) {
    var evntCobert = $A.get("e.force:navigateToComponent");
    evntCobert.setParams({
        componentDef : "c:MX_SB_MLT_TablaCertificados",
        componentAttributes: {
            poliza : compCobert.get("v.polizaSrch"),
            varxt : compCobert.get("v.polizaSrch")
        }
    });
    evntCobert.fire();
},
datosP : function(compdatosP, event, helper) {
    var evtdatosP = $A.get("e.force:navigateToComponent");
    evtdatosP.setParams({
        componentDef : "c:MX_SB_MLT_Poliza_DatosParticulares",
        componentAttributes: {
            poliza : compdatosP.get("v.polizaSrch")
        }
    });
    evtdatosP.fire();
},
recibos : function(comprecibos, event, helper) {
    var evtrecibos = $A.get("e.force:navigateToComponent");
    evtrecibos.setParams({
        componentDef : "c:MX_SB_MLT_Poliza_Recibos",
        componentAttributes: {
            poliza : comprecibos.get("v.polizaSrch")
        }
    });
    evtrecibos.fire();
},
consultaPol : function(compCPol, event, helper) {
    var tipoplan ="metasegura";
    compCPol.set("v.fullScreen", true);
    compCPol.set("v.columns",[
        {label: 'COBERTURA', fieldName: 'prod', type: 'text'},
        {label: 'DESCRIPCION', fieldName: 'descript', type: 'text'},
        {label: 'MONTO', fieldName: 'monto', type: 'number', editable:true },
        {label: 'TIPO DE PLAN', fieldName: 'tipoplan', type: 'text', editable: tipoplan=='metasegura' },
        {label: 'RENTAS', fieldName: 'rentas', type: 'text', editable: tipoplan=='metasegura'},
        {label: 'MONTO DE RENTA', fieldName: 'montorenta', type: 'text', editable: tipoplan=='metasegura'},
        {label: 'ESTIMACION', fieldName: 'estimacion', type: 'text', editable: tipoplan=='metasegura'}
    ]);
    compCPol.set("v.datatabs",[
        {prod:'Metasegura Continuidad',descript:'Meta Segura',monto:'50000',tipoplan:'VIDA',fin:'24',montorenta:'600',estimacion:''},
        {prod:'Metaeducación',descript:'Metaeducación',monto:'50000',tipoplan:'VIDA',fin:'24',montorenta:'600',estimacion:''},
        {prod:'Metaeducación',descript:'Metaeducación',monto:'0',tipoplan:'VIDA',fin:'24',montorenta:'600',estimacion:''},
        {prod:'Gastos funerarios(V104)',descript:'Gastos funerarios',monto:'50000',tipoplan:'VIDA',fin:'24',montorenta:'600',estimacion:''},
        {prod:'Gastos funerarios(V105)',descript:'Gastos funerarios',monto:'150000',tipoplan:'VIDA',fin:'24',montorenta:'600',estimacion:''}
    ]);
    var parentescos =[];
    parentescos.push({label:'padre',value:'padre'});
    compCPol.set("v.columnsBen",[
        {label: 'Nombre', fieldName: 'name', type: 'text'},
        {label: 'Parentesco', fieldName: 'parent', type: 'picklist', editable:true,
        typeAttributes: {
            selectOptions : parentescos,
        }},
        {label: '%', fieldName: 'percent', type: 'number',editable:true},
        {label: 'Fecha de Naciemiento', fieldName: 'datenac', type: 'text'},
        {label: 'Edad', fieldName: 'yearsold	', type: 'text'},
        {label: 'Representante legal', fieldName: 'legalparent', type: 'text'},
        {label: 'RFC', fieldName: 'rfc', type: 'text'},
        {label: 'Genero', fieldName: 'gender', type: 'text'}
    ]);
    compCPol.set("v.datatabsBen",[
        {name:'Juan Perez',parent:'Hijo',percent:'50',datenac:'10/06/1976',yearsold:'46',legalparent:'-',rfc:'PEMA100676',gender:'Hombre'}
    ]);
    compCPol.set("v.columnsEnd",[
        {label: 'Entidad', fieldName: 'ent', type: 'text'},
        {label: 'Campo', fieldName: 'field', type: 'text'},
        {label: 'Valor Anterior', fieldName: 'oldval', type: 'text'},
        {label: 'Valor Nuevo', fieldName: 'newval', type: 'text'},
        {label: 'Fecha', fieldName: 'datem', type: 'text'}
    ]);
    compCPol.set("v.datatabsEnd",[
        {ent:'BBVA',field:'-',oldval:'400000',newval:'600000',datem:'10/05/2019'}
    ]);
    compCPol.set("v.columnsAnexo",[
        {label: 'Anexos', fieldName: 'anexo', type: 'text'},
        {label: 'Excluciones', fieldName: 'exclucion', type: 'text'}
    ]);
    compCPol.set("v.datatabsAnexo",[
        {anexo:'',exclucion:''}
    ]);
    var toastEvent = $A.get("e.force:showToast");
    var action = compCPol.get("c.srchCtt");
    var jsnStr = {	"polizaSrch" : compCPol.get("v.polizaSrch"),
                    "certificado" : compCPol.get("v.certificado"),
                    "producto" :"",
                    "fecocurrido" : compCPol.get("v.fecocurrido"),
                    "nombreAsegurado" : compCPol.get("v.nombreAsegurado"),
                    "rfcSrch" : compCPol.get("v.rfcSrch"),
                    "aPaterno" : compCPol.get("v.aPaterno"),
                    "aMaterno" : compCPol.get("v.aMaterno"),
                    "nombreCta" : compCPol.get("v.nombreCta"),
                    "aPaternoCta" : compCPol.get("v.aPaternoCta"),
                    "aMaternoCta" : compCPol.get("v.aMaternoCta"),
                      "telefono" : compCPol.get("v.telefono"),
                      "serie" : compCPol.get("v.serie"),
                      "nEmpleado" : compCPol.get("v.numEmpleado"),
                      "nCredito" : compCPol.get("v.numCredito"),
                      "placas":compCPol.get("v.placas")};
    var strjsn = JSON.stringify(jsnStr);
    var validform =false;
    JSON.parse(strjsn,function (idx,value){
        if(idx!==null && idx.trim()!==''){
            if(value!==null && value.trim()!==''){validform=true;}
        }
    });
    if(validform){
        var servicioPersona = false;
        servicioPersona = this.lockservicePol(compCPol,event,helper);
        action.setParams({"dataPol":strjsn,"servicioPersona":servicioPersona,"idcuenta":null});
           action.setCallback(this, function(response) {
                let state = response.getState();
                    if (state === "SUCCESS") {
                        compCPol.set("v.mapaPolizasWS",response.getReturnValue().contratos);
                        compCPol.set("v.mapaPersonasWS",response.getReturnValue().cuentas);
                        this.agregaTablasFront(compCPol,event,helper);
                    } else {
                        compCPol.set("v.showdivPoliza","");
                            compCPol.set("v.showdivPersona","");
                            toastEvent.setParams({
                                "title": "Error!",
                                "type": "error",
                                "message": "Ocurrió un error al realizar la consulta"
                            });
                            toastEvent.fire();
                    }
            });
        $A.enqueueAction(action);
    } else {
        toastEvent.setParams({
            "title": "Error!",
            "type": "error",
            "message": "Debe llenar al menos un campo para busqueda"
        });
        toastEvent.fire();
    }
    $A.get('e.force:refreshView').fire();
},
polizacertificado : function(component,event,helper){
    var msg = event.getSource().get('v.value');
    var polizaChosed = component.get("c.getContractByPolCert");
    polizaChosed.setParams({"poliza":msg.numpoliza,"cert":msg.numcertificado});
    polizaChosed.setCallback(this,function(response){
        let state = response.getState();
        if(state === "SUCCESS"){
            component.set("v.polizaSeleccionada",response.getReturnValue());
        }
    });
    var updsin = component.get("c.updateSinCont");
    updsin.setParams({"data":msg,"idsin":component.get("v.recordId")});
    updsin.setCallback(this,function(response2){
        let state = response2.getState();
        if(state === "SUCCESS"){
            component.set("v.Step","S2");
        }
    });
    $A.enqueueAction(updsin);
    $A.enqueueAction(polizaChosed);
    $A.get('e.force:refreshView').fire();
},
comparaDate : function(component,event,helper){
    var Fecha2= new Date(component.find("Fecha").get("v.value"));
    var Hoy= new Date();
       var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error Message',
            message:'Fecha de Siniestro no Valida',
            messageTemplate: 'Porfavor ingrese una fecha y horario valido',
            duration:' 3000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
    var showtoast = false;
    if(Fecha2.getFullYear()>Hoy.getFullYear()){
        showtoast=true;
    }else{
        if(Fecha2.getMonth()>Hoy.getMonth()){
            showtoast=true;
        }else {
            if((Fecha2.getDate()+1)>Hoy.getDate()){
                showtoast=true;
            }
        }
    }
    if (showtoast){
        component.set("v.fecocurrido","");
        toastEvent.fire();
    }
    component.set("v.disableSrchPers",true);
    this.lockservicePers(component, event, helper);
},
comparaDateNacimiento : function(component,event,helper){
    var Fecha= new Date(component.find("fechaNac").get("v.value"));
    var Hoy= new Date();
       var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error Message',
            message:'Fecha de nacimiento no valida',
            messageTemplate: 'Porfavor ingrese una fecha valida',
            duration:' 3000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
    var showtoastf = false;
    if(Fecha.getFullYear()>Hoy.getFullYear()){
        showtoastf=true;
    }else{
        if(Fecha.getMonth()>Hoy.getMonth()){
            showtoastf=true;
        }else {
            if((Fecha.getDate()+1)>Hoy.getDate()){
                showtoastf=true;
            }
        }
    }
    if (showtoastf){
        component.set("v.fecNacimiento","");
        toastEvent.fire();
    }
    component.set("v.disableSrchPol",true);
    this.lockservicePol(component, event, helper);
},
lockservicePol: function(component,event,helper) {
    var jsonreturn = {
        "nombreAsegurado" : component.get("v.nombreAsegurado"),
        "rfcSrch" : component.get("v.rfcSrch"),
        "aPaterno" : component.get("v.aPaterno"),
        "aMaterno" : component.get("v.aMaterno"),
        "nombreCta" : component.get("v.nombreCta"),
        "aPaternoCta" : component.get("v.aPaternoCta"),
        "aMaternoCta" : component.get("v.aMaternoCta"),
        "telefono" : component.get("v.telefono"),
        "fecNacimiento": component.get("v.fecNacimiento")
    };
    var strjsn = JSON.stringify(jsonreturn);
    var validlock =false;
    JSON.parse(strjsn,function (idx,value){
        if(idx!==null && idx.trim()!==''){
            if(value!==null && value.trim()!==''){validlock=true;}
        }
    });
    if(jsonreturn.length===0){validlock=false;}
    if (validlock===false) {
        component.set("v.disableSrchPol",false);
    } else {
        component.set("v.disableSrchPol",true);
    }
    return validlock;
},
lockservicePers: function(compperson,event,helper) {
    var jsonreturnPers = {
        "polizaSrch" : compperson.get("v.polizaSrch"),
        "certificado" : compperson.get("v.certificado"),
        "fecocurrido" : compperson.get("v.fecocurrido"),
        "serie" : compperson.get("v.serie"),
        "placas":compperson.get("v.placas"),
        "nEmpleado" : compperson.get("v.numEmpleado"),
        "nCredito" : compperson.get("v.numCredito")
    };
    var strjsnpers = JSON.stringify(jsonreturnPers);
    var validlockpers =false;
    JSON.parse(strjsnpers,function (idx,value){
        if(idx!==null && idx.trim()!==''){
            if(value!==null && value.trim()!==''){validlockpers=true;}
        }
    });
    if(jsonreturnPers.length===0){validlockpers=false;}
    if (validlockpers===false) {
        compperson.set("v.disableSrchPers",false);
    } else {
        compperson.set("v.disableSrchPers",true);
    }
    return validlockpers;
},
muestrapoliza : function(compmPoliza,event,helper){
    var action4 = compmPoliza.get("c.srchCtt");
    action4.setParams({"dataPol":null,"servicioPersona":false,"idcuenta":event.getSource().get("v.value").idCuenta});
    action4.setCallback(this, function(response) {
        if(response.getState()==="SUCCESS") {
            var arrayTable =[];
            var arrayresp = response.getReturnValue().contratos;
            for (var i=0; i<arrayresp.length ; i++) {
                 var jsonitem3 = {"prod": "2002",
                                    "plan": "Extenso",
                                    "numpoliza": arrayresp[i].MX_SB_SAC_NumeroPoliza__c ,
                                  	"numcertificado":"1",
                                  	"cliente":"",
                                    "idepoliza": "123CDB76" ,
                                    "estatus": "Act" ,
                                    "inicio": arrayresp[i].StartDate ,
                                    "fin": arrayresp[i].EndDate ,
                                  	"fechaemision":"2020-01-01",
                                    "formpago": "TDD",
                                    "moneda": arrayresp[i].MX_SB_SAC_TipoMoneda__c ,
                                    "tipo": "1" ,
                                    "nren": "2"};
                arrayTable.push(jsonitem3);
            }
            compmPoliza.set("v.tablaPolizas",arrayTable);
            compmPoliza.set("v.showdivPoliza","mostrar");
        }
    });
    compmPoliza.set("v.showdivPersona","");
    $A.enqueueAction(action4);
},
arrayRespContratos : function(comp,event,helper){
    var arrayresp = comp.get("v.mapaPolizasWS");
    var arrayTablePersPol =[];
    for (var iter=0; iter<arrayresp.length ; iter++) {
        var jsonitem = {
        "nombre": arrayresp[iter].MX_SB_SAC_NombreClienteAseguradoText__c,
        "secnombre": "",
        "apaterno": arrayresp[iter].MX_WB_apellidoPaternoAsegurado__c,
        "amaterno": arrayresp[iter].MX_WB_apellidoMaternoAsegurado__c,
        "rfc": arrayresp[iter].MX_SB_SAC_RFCAsegurado__c,
        "phone": arrayresp[iter].MX_WB_celularAsegurado__c,
        "mail": arrayresp[iter].MX_SB_SAC_EmailAsegurado__c,
        "idpersona": "123456790",
        "idCuenta":arrayresp[iter].AccountId};
        arrayTablePersPol.push(jsonitem);
    }
    return arrayTablePersPol;
},
arrayRespCuentas : function(comp,event,helper){
    var arrayrespPers = comp.get("v.mapaPersonasWS");
    var arrayTabPersCuentas=[];
    for (var iter2=0; iter2<arrayrespPers.length ; iter2++) {
        var jsonitem2 = {
        "nombre": arrayrespPers[iter2].FirstName,
        "secnombre": arrayrespPers[iter2].middlename ,
        "apaterno": arrayrespPers[iter2].LastName ,
        "amaterno": arrayrespPers[iter2].ApellidoMaterno__c,
        "rfc": arrayrespPers[iter2].RFC__c ,
        "phone": arrayrespPers[iter2].PersonHomePhone,
        "mail": arrayrespPers[iter2].PersonEmail,
        "idpersona": "123457689",
        "idCuenta": arrayrespPers[iter2].Id};
        arrayTabPersCuentas.push(jsonitem2);
    }
    return arrayTabPersCuentas;
},
arrayRespContratosPol : function(comp,event,helper){
    var arrayRespContPol = comp.get("v.mapaPolizasWS");
    var arrayTableContPol = [];
    for (var iter3=0; iter3<arrayRespContPol.length ; iter3++) {
        var jsonitem3 = {"prod": "2002",
        "plan": "Extenso",
        "numpoliza": arrayRespContPol[iter3].MX_SB_SAC_NumeroPoliza__c,
        "numcertificado":"1",
		"cliente":"",
        "idepoliza": "1623SFDV3",
        "estatus": "Act",
        "inicio": arrayRespContPol[iter3].StartDate,
        "fin": arrayRespContPol[iter3].EndDate,
        "fechaemision":"2020-01-01",
        "formpago": "TDD",
        "moneda": arrayRespContPol[iter3].MX_SB_SAC_TipoMoneda__c,
        "tipo": "",
        "nren": ""};
        arrayTableContPol.push(jsonitem3);
    }
    return arrayTableContPol;
},
agregaTablasFront : function(comp,event,helper){
    var toastEventmethod = $A.get("e.force:showToast");
    var validapoliza = comp.get("v.mapaPolizasWS");
    var validapersona = comp.get("v.mapaPersonasWS");
    var servpersona = false;
    servpersona = this.lockservicePol(comp,event,helper);
    if(validapoliza.length>0 || validapersona.length>0 ){
        toastEventmethod.setParams({
            "title": "Error!",
            "type": "error",
            "message": "Error de conexion con clippert, realizando busqueda interna"
        });
        toastEventmethod.fire();
        var arrayTable=[];
        if(servpersona) {
            arrayTable=this.arrayRespContratos(comp,event,helper);
            arrayTable.concat(this.arrayRespCuentas(comp,event,helper));
            comp.set("v.tablaPersonas",arrayTable);
            comp.set("v.showdivPoliza","");
            comp.set("v.showdivPersona","mostrar");
        } else {
            arrayTable=this.arrayRespContratosPol(comp,event,helper);
            comp.set("v.tablaPolizas",arrayTable);
            comp.set("v.showdivPoliza","mostrar");
            comp.set("v.showdivPersona","");
        }
    } else {
        comp.set("v.showdivPoliza","");
        comp.set("v.showdivPersona","");
        toastEventmethod.setParams({
            "title": "Error!",
            "type": "error",
            "message": "No se encontraron registros"
        });
        toastEventmethod.fire();
    }
},
avanzar : function(cmp,evt,hlp) {
	var acttr = cmp.get("c.upds");
    var etapa = 'Creación del Siniestro o Asistencia';
    acttr.setParams({"data":{"MX_SB_MLT_JourneySin__c":etapa,"Id":cmp.get("v.recordId")}});
    if(cmp.get("v.afectaCoberturas")){
        acttr.setCallback(this, function(response) {
            if(response.getState()==="SUCCESS") {
                    $A.get('e.force:refreshView').fire();
            }
        });
    } else {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Atencion',
            message:'Revisar Afectación coberturas',
            messageTemplate: 'Revisar Afectación coberturas',
            duration:' 2000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'});
        toastEvent.fire();
    }
    $A.enqueueAction(acttr);
},
cerrartr : function(cmp,evt,hlp) {
	var acttr = cmp.get("c.upds");
    acttr.setParams({"data":{"MX_SB_MLT_JourneySin__c":"Cerrado","Estatus__c":"Inactivo","Id":cmp.get("v.recordId")}});
    acttr.setCallback(this, function(response) {
        if(response.getState()==="SUCCESS") {
                $A.get('e.force:refreshView').fire();
        }
    });
    $A.enqueueAction(acttr);
},
validaSupervivencia : function (cmp,evt,hlp){
    var srchSinProd = cmp.get("c.getSiniestroByProducto");
    srchSinProd.setParams({"polizadata":cmp.get("v.polizaSeleccionada"),"producto":cmp.get("v.polizaSeleccionada").MX_WB_Producto__r.Name});
    srchSinProd.setCallback(this, function (response){
        if(response.getState()==="SUCCESS") {
            if(response.getReturnValue().length>1) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Atencion',
                    message:'La póliza ya cuenta con un siniestro creado',
                    messageTemplate: 'La póliza ya cuenta con un siniestro creado',
                    duration:' 2000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'});
                toastEvent.fire();
            } else {
                this.avanzar(cmp,evt,hlp);
            }
        }
    });
    $A.enqueueAction(srchSinProd);
},
validaILP : function (cmp,evt,hlp){
    var srchSinProd = cmp.get("c.getSiniestroByProducto");
    srchSinProd.setParams({"polizadata":cmp.get("v.polizaSeleccionada"),"producto":"ILP"});
    srchSinProd.setCallback(this, function (response2){
        if(response2.getState()==="SUCCESS") {
            if(response2.getReturnValue().length>0) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Atencion',
                    message:'La póliza ya cuenta con un siniestro creado',
                    messageTemplate: 'La póliza ya cuenta con un siniestro creado',
                    duration:' 2000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'});
                toastEvent.fire();
            } else {
                this.avanzar(cmp,evt,hlp);
            }
        }
    });
    $A.enqueueAction(srchSinProd);
},
})