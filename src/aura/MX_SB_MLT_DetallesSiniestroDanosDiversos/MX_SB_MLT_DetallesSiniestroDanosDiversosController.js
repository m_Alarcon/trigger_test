({
    handleCancelDanos: function(component) {
        component.set('v.isEditMode', false);
    },
    doInitDanos : function(component) {
        var action = component.get("c.getRecordType");
	action.setParams({"devname":"MX_SB_MLT_RamoDanos"});
        action.setCallback(this, function(response) {
            component.set("v.recordtypeDanios",response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    doInitDanos2 : function (component){
        var action = component.get("c.getType");
        action.setCallback(this, function(response){
            alert(response.getReturnValue())
            component.set("v.SinType",response.getReturnValue())
        });
        $A.enqueueAction(action);
    },
    handleSuccessDanos : function(component, event) {
	var payload = event.getParams().response;
        var navService = component.find("navService");
        var pageReference = {
            type: 'standard__recordPage',
            attributes: {
                "recordId": payload.id,
                "objectApiName": component.get("v.sObjectName"),
                "actionName": "view"
            }
        }
        event.preventDefault();
        navService.navigate(pageReference);
    },
    toggleEditModeDanos : function(component) {
        component.set("v.isEditMode", !component.get("v.isEditMode"));
    },
    handleEditDanos : function(component) {
        component.find("SiniestroEditForm").submit();
        $A.get("e.force:refreshView").fire();
        $A.get("e.force:editRecord").fire();
        component.set('v.isEditMode', false);
    }
})