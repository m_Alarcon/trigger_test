({
    doInit: function(component, event, helper) {
      helper.callQueryOpp(component,event,helper);
      helper.initTask(component,event,helper);
      component.set("v.Etapa1",$A.get("$Label.c.MX_SB_PS_Etapa1"));
	  component.set("v.Etapa2",$A.get("$Label.c.MX_SB_PS_Etapa2"));
	  component.set("v.Etapa3",$A.get("$Label.c.MX_SB_PS_Etapa3"));
	  component.set("v.Etapa4",$A.get("$Label.c.MX_SB_PS_Etapa4"));
	  component.set("v.Etapa5",$A.get("$Label.c.MX_SB_PS_Etapa5"));
    },
    handleSubmit: function(component, event, helper) {
      $A.get('e.force:refreshView').fire();
     },
     siguiente : function(component, event, helper) {
        if(event.getParam("continuar")===true && event.getParam('nextStageName')==='Planes y Formas de pago') {
	      helper.validasiguiente(component,event,helper,$A.get("$Label.c.MX_SB_PS_Etapa1"));
	      $A.get('e.force:refreshView').fire();
		  component.set('v.reloaded',true);
	}
      if(event.getParam("continuar")===true && event.getParam('nextStageName')===$A.get("$Label.c.MX_SB_PS_Etapa3")) {
          helper.upsrtquoteLI(component,event,helper);
          helper.toastSuccess(component, event, helper);
	      helper.validasiguiente(component,event,helper,$A.get("$Label.c.MX_SB_PS_Etapa3"));
          helper.generateTaskInfo(component,event,helper,$A.get("$Label.c.MX_SB_PS_Etapa3"));
	      $A.get('e.force:refreshView').fire();
              component.set('v.reloaded',true);
	}
	   if(event.getParam("continuar")===true && event.getParam('nextStageName')===$A.get("$Label.c.MX_SB_PS_Etapa4")) {
          helper.toastSuccess(component, event, helper);
	  	  helper.validasiguiente(component,event,helper,$A.get("$Label.c.MX_SB_PS_Etapa4"));
          helper.generateTaskInfo(component,event,helper,$A.get("$Label.c.MX_SB_PS_Etapa3"));
	      $A.get('e.force:refreshView').fire();
         }
	   if(event.getParam("continuar")===true && event.getParam('nextStageName')===$A.get("$Label.c.MX_SB_PS_Etapa5")) {
          helper.toastSuccess(component, event, helper);
	      helper.validasiguiente(component,event,helper,$A.get("$Label.c.MX_SB_PS_Etapa5"));
          helper.generateTaskInfo(component,event,helper,$A.get("$Label.c.MX_SB_PS_Etapa5"));
	      $A.get('e.force:refreshView').fire();
              component.set('v.reloaded2',true);
        }
	  if(event.getParam("continuar")===false || event.getParam("continuar")===undefined ) {
      		helper.toastError(component, event, helper);
      }
    },
    siguientemp : function(component, event, helper) {
      helper.validasiguiente(component,event,helper,'Resumen de Contratación');
      $A.get('e.force:refreshView').fire();
    },
    handleClick : function(component, event, helper) {
      helper.validasiguiente(component,event,helper,'Datos cliente y Asegurados');
        $A.get('e.force:refreshView').fire();
    },
    lwcEventHandler : function(component, event, helper) {
        var eventData = event.getParam('vobj');
        component.set("v.textFromEvent",eventData);
        helper.upsrtquoteLI(component,event,helper);
    },
})