({
    callQueryOpp : function(C,E,H) {
        var action = C.get("c.fetchOpp");
        action.setParams({ strId : C.get("v.recordId") });
        action.setCallback(this, function(res){
            var state = res.getState();
            if (state === "SUCCESS") {
                C.set("v.CurrentStage", res.getReturnValue().StageName);
                if(res.getReturnValue().StageName===$A.get("$Label.c.MX_SB_PS_Etapa1")) {
                    C.set('v.numEtapa','1');
                } else if (res.getReturnValue().StageName===$A.get("$Label.c.MX_SB_PS_Etapa2")) {
                    C.set('v.numEtapa','2');
                } else if (res.getReturnValue().StageName===$A.get("$Label.c.MX_SB_PS_Etapa3")) {
                    C.set('v.numEtapa','3');
                } else if (res.getReturnValue().StageName===$A.get("$Label.c.MX_SB_PS_Etapa4")) {
                    C.set('v.numEtapa','4');
                } else if (res.getReturnValue().StageName===$A.get("$Label.c.MX_SB_PS_Etapa5")) {
                    C.set('v.numEtapa','5');
                } else {
                    C.set('v.numEtapa','');
                }
                C.set("v.CurrentRT", res.getReturnValue().RecordType.Name);
                C.set("v.CurrentProdID",res.getReturnValue().Producto__c);
                C.set("v.opport",res.getReturnValue());
                let action2 = C.get('c.loadAccountJ');
                action2.setParams({'recId':res.getReturnValue().AccountId});
                action2.setCallback(this, function(responseB) {
                    C.set("v.accObj",responseB.getReturnValue()[0]);
                });
                $A.enqueueAction(action2);
            }
        });
        $A.enqueueAction(action);
    },
    initTask: function(component,event,helper) {
        var getTaskI= component.get("c.getTaskData");
        getTaskI.setParams({'oppId': component.get('v.recordId')});
        getTaskI.setCallback(this, function(response) {
            component.set('v.taskObj', response.getReturnValue()[0]);
            component.set('v.taskObj.WhatId',component.get('v.recordId'));
        });
        $A.enqueueAction(getTaskI);
    },
    validasiguiente : function(cmp,evt,helper,stgname) {
        var updopp = cmp.get("c.upsrtOpp");

        updopp.setParams({"opp":{"Id":cmp.get("v.recordId"),
                                 "StageName":stgname}
                         });
        updopp.setCallback(this, function(response) {
            if(response.getState()==="SUCCESS") {
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(updopp);
    },
    toastSuccess : function(component, event, helper) {
        document.getElementById('divtofocus').scrollIntoView();
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message:'Etapa cambió correctamente.',
            duration:' 2000',
            key: 'check',
            type: 'success'
        });
        toastEvent.fire();
    },
    toastError : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message:'Ooops... algo ocurrio',
            duration:' 2000',
            key: 'error',
            type: 'error'
        });
        toastEvent.fire();
    },
    generateTaskInfo : function(component, event, helper,stgName) {
        if(component.get('v.taskObj.ActivityDate')==='' && component.get('v.taskObj.ActivityDate')===null){
            helper.getCampaingMember(component, event, helper);
        }
        component.set('v.taskObj.MX_SB_VTS_AgendadoPor__c',component.get('v.taskObj.LastModifiedBy.Name'));
        component.set('v.taskObj.WhatId',component.get('v.taskObj.WhatId'));
        component.set('v.taskObj.MX_WB_telefonoUltimoContactoCTI__c', component.get('v.accObj.Phone'));
        component.set('v.taskObj.ActivityDate', component.get('v.taskObj.CreatedDate'));
        component.set('v.taskObj.Origen__c', 'Outbound');
        component.set('v.taskObj.MX_SB_VTA_Resultado_del_contacto__c','Si');
        component.set('v.taskObj.MX_SB_VTA_Resultado_contactoEfectivo__c','Si');
        var updTaskD = component.get('c.upsrtTask');
        updTaskD.setParams({'taskObj':component.get('v.taskObj'),'typo':'Llamada'});
        updTaskD.setCallback(this, function(response) {
        });
        $A.enqueueAction(updTaskD);
    },
    upsrtquoteLI : function(component,event,helper){
        var objdata = JSON.parse(component.get("v.textFromEvent"));
        objdata.QuoteLineItem.QuoteLineItem['Name']=component.get("v.opport").Name;
        objdata.Quote.Quote['Name']=component.get("v.opport").Name;
        var upsertqli = component.get("c.upsrtQuoteLI");
        upsertqli.setParams({'quote':objdata.Quote.Quote,'quoteLI':objdata.QuoteLineItem.QuoteLineItem,'prod':component.get("v.recordId")});
        upsertqli.setCallback(this, function(res) {
        });
        $A.enqueueAction(upsertqli);
    },
    getCampaingMember : function(component, event, helper) {
        var action = component.get("c.getDataCampaign");
        action.setParams(
            {
                idCamp : component.get("v.opport").CampaignId
            }
        );
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set('v.taskObj.ActivityDate', storeResponse[0].EndDate);
            }
        });
        $A.enqueueAction(action);
    },
})