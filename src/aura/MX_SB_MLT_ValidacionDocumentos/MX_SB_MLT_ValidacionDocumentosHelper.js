({
    doInitHelperkits:function(component,event) {
        var recordId= component.get("v.recordId");
        var action = component.get("c.searchSinVida");
        action.setParams({'siniId':recordId});
        action.setCallback(this, function(response) {
            component.set('v.contractId',response.getReturnValue()[0].MX_SB_SAC_Contrato__c);
            component.set('v.producto',response.getReturnValue()[0].MX_SB_SAC_Contrato__r.MX_WB_Producto__r.Name);
            component.set('v.fechaNacimiento',response.getReturnValue()[0].MX_SB_MLT_FechaNacimiento__c);
            component.set('v.subramo',response.getReturnValue()[0].MX_SB_MLT_SubRamo__c);
            component.set('v.subtipo',response.getReturnValue()[0].MX_SB_MLT_Subtipo__c);
            component.set('v.StarDate',response.getReturnValue()[0].MX_SB_SAC_Contrato__r.StartDate);
            component.set('v.EndDate',response.getReturnValue()[0].MX_SB_SAC_Contrato__r.EndDate);
            component.set('v.SinDate',response.getReturnValue()[0].MX_SB_MLT_DateDifunct__c);
            var subramo =component.get('v.subramo');
            var producto = component.get('v.producto');
            var subtipo= component.get("v.subtipo");
            var todayVal = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
            var btwnSin= (Date.parse(todayVal)-Date.parse(component.get('v.SinDate')))/(24*60*60*1000);
            var edad = ((Date.parse(todayVal) - Date.parse(component.get('v.fechaNacimiento')))/(24*60*60*1000)/365)
            component.set('v.edad',edad);
            var years=  btwnSin/365;
            const percentr=50;
            var starDateVal= Date.parse(component.get("v.StarDate"));
            var endDateVal=  Date.parse(component.get('v.EndDate'));
            var sinDate= Date.parse(component.get("v.SinDate"));
            var period= (sinDate-starDateVal)*100/(endDateVal-starDateVal);
            const sumR= 2500000;
            var sumAseg=component.get('v.SumaAseg');
            var fechaIni= component.get("v.StarDate");
            var fechaSini= component.get("v.SinDate");
            var tiempo = ((Date.parse(fechaSini)- Date.parse(fechaIni))/(24*60*60*1000))/365;
            var plan = component.get("v.plan");
            this.FamiliaSeguraF(component,event,producto,subramo,subtipo,years);
            this.GastosFBancomer(component,event,producto,subramo,subtipo,years);
            this.MetaSeguraF(component,event,subramo, producto,period,percentr);
            this.MetaSeguraA(component,event,subramo,producto);
            this.PymanF(component,subramo,producto,subtipo,sumAseg,sumR,edad);
            this.VidaSeguraEmF(component,event,producto,subramo,tiempo);
            this.GastosFunBBVAF(component,event,producto,subramo,tiempo);
            this.VidaSegEmprlF(component,event,producto,subramo,tiempo);
            this.VidaSegPrefF(component, event, producto, subramo, tiempo);
            this.VidaSegF(component, event, producto, subramo,plan);
            this.RespVSCncr(component,event, producto, subramo,years);
            this.initBenefactor(component,event);
            component.set('v.columns', [
                {label: 'Documentos', fieldName: 'MX_SB_MLT_DocumentosRequeridos__c', type: 'text'}
            ]);
            var action2 = component.get("c.searchkit");
            action2.setParams({'mapa':{
                'product':producto ,
                'subramo':subramo,
                'subtipo':subtipo,
                'idSin':component.get("v.recordId"),
                'regla1':component.get("v.regla1"),
                'regla2':component.get("v.regla2")
            }});
            action2.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.info", response.getReturnValue());
                    this.getKitName(component,event);
                }
            });
            $A.enqueueAction(action2);
        });
        $A.enqueueAction(action);
    },
    MetaSeguraA: function(component,event,subramo,producto){
        if(subramo==='Ahorro' && producto ==='Metasegura') {
            component.set("v.regla1","-");
        }
    },
    MetaSeguraF: function(component,event,subramo, producto,period,percentr) {
        if(subramo==='Fallecimiento' && producto ==='Metasegura') {
            if(period>=percentr) {
                component.set("v.regla1","más/= 50% periodo contratado");
            }
            else {
                component.set("v.regla1","menos 50% periodo contratado");
            }
        }
    },
    GastosFBancomer: function(component,event,producto,subramo,subtipo,years) {
        if(subramo==='Fallecimiento' && producto ==='Gastos Funerarios Bancomer') {
            if(years>=1) {
                component.set("v.regla1","Más/= de 1 año fallecimiento");
            }
            else {
                component.set("v.regla1","Menos de 1 año fallecimiento");
            }
        }
    },
    FamiliaSeguraF: function(component,event,producto,subramo,subtipo,years) {
        if(subramo==='Fallecimiento' && producto ==='Familia Segura') {
            if(subtipo==='Natural') {
                if(years>=2) {
                    component.set("v.regla1","más/= de 2 años fallecimiento");
                }
                else {
                    component.set("v.regla1","menos de 2 años de fallecimiento");
                }
            }
            else {
                component.set("v.regla1","-");
            }
        }
    },
    PymanF: function(component,subramo,producto,subtipo,sumAseg,sumR,edad) {
        if(subramo==='Fallecimiento' && producto==='Pyman') {
            if(subtipo==='Natural') {
                if(sumAseg>sumR) {
                    component.set("v.regla1",'más de 2.5 mdp suma asegurada');
                }
                else {
                    component.set("v.regla1","menos de 2.5 mdp suma asegurada");
                    if(edad>55) {
                        component.set("v.regla2","mayor a 55 años");
                    }
                    else {
                        component.set("v.regla2","18 a 55 años");
                    }
                }
            }
            else {
                component.set('v.relgla1','-');
            }
        }
    },
    VidaSeguraEmF: function(component,event,producto,subramo,tiempo){
        if(producto==='Vida Segura Empresa' && subramo==='Fallecimiento') {
            if(tiempo>=2) {
                component.set("v.regla1",'Más / =  de 2 años fecha contratación');
            }
            else {
                component.set("v.regla1",'Menos de 2 años fecha contratación');
            }
        }
    },
    GastosFunBBVAF: function(component,event,producto,subramo,tiempo) {
        if(producto ==='Gastos Funerarios BBVA' && subramo==='Fallecimiento') {
            if(tiempo>=1) {
                component.set("v.regla1",'Más/= de 1 año contratación');
            }
            else {
                component.set("v.regla1",'Menos de 1 año contratación');
            }
        }
    },
    VidaSegEmprlF: function(component,event,producto,subramo,tiempo) {
        if(producto==='Vida Segura Empresarial' && subramo==='Fallecimiento') {
            if(tiempo>=2) {
                component.set("v.regla1",'Más / =  de 2 años fecha contratación');
            }
            else {
                component.set("v.regla1",'Menos de 2 años fecha contratación');
            }
        }
    },
    VidaSegPrefF: function(component, event, producto, subramo, tiempo) {
        if(producto==='Vida Segura Preferente' && subramo==='Fallecimiento') {
            if(tiempo>=2) {
                component.set("v.regla1",'Más / =  de 2 años fecha contratación');
            }
            else {
                component.set("v.regla1","Menos de 2 años fecha contratación");
            }
        }
    },
    VidaSegF: function(component, event, producto, subramo,plan){
        if(producto==='Vida Segura' && subramo==='Fallecimiento') {
            if(plan==='Plan I o II') {
                component.set("v.regla1",'Plan I o II');
            }
            else {
                component.set("v.regla1",'-');
            }
        }
    },
    RespVSCncr: function(component,event, producto, subramo,years) {
        if(producto==='Respaldo contra cáncer' && subramo==='Fallecimiento') {
            if(years>=2) {
                component.set("v.regla1",'más/= de 2 años fallecimiento');
            }
            else {
                component.set("v.regla1",'menos de 2 años de fallecimiento');
            }
        }
    },
    initBenefactor: function(component,event) {
        component.set('v.columns2', [
            {label: 'Correo Electronico', fieldName: 'MX_SB_SAC_Email__c', type: 'text'},
            {label: 'Rol', fieldName: 'recordtype.Name', type: 'text'},{label: 'Nombre', fieldName: 'Name', type: 'text'},
            {label: 'Apellido', fieldName: 'MX_SB_VTS_APaterno_Beneficiario__c', type: 'text'}
        ]);
        var action = component.get("c.benefactor");
        var contrato= component.get("v.contractId");
        action.setParams({'contractId':contrato});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.info2", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    getKitName: function(component,event) {
        var action5= component.get("c.kit");
		action5.setParams({'mapa':{
                'product':component.get('v.producto') ,
                'subramo':component.get('v.subramo'),
                'subtipo':component.get("v.subtipo"),
                'idSin':component.get("v.sinObj.Id"),
                'regla1':component.get("v.regla1"),
                'regla2':component.get("v.regla2")
            }});
        	action5.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.sinObj.MX_SB_MLT_KitVida__c", response.getReturnValue());
                    var action9= component.get('c.updateSin');
                    action9.setParams({'sin':component.get("v.sinObj")});
                    action9.setCallback(this, function(response) {
                    });
                    $A.enqueueAction(action9);
                }
            });
        $A.enqueueAction(action5);
    },
})