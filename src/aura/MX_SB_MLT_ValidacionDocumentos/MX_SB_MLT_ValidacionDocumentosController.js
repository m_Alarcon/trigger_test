({
    doInitDocumentosRequeridos : function(component, event, helper) {
        helper.doInitHelperkits(component, event, helper);
	component.set("v.sinObj.Id",component.get("v.recordId"));
    },
    doAction : function(component, event, helper) {
        var mailito = component.get("v.canalEnvio");
        var comments = component.get("v.comentarios");
        component.set("v.sinObj.MX_SB_MLT_ComentariosSiniestro__c",comments);
        var btnValue = event.getSource().get("v.value");
        if(btnValue==='Crear' && mailito.match(/Email/)) {
            var action4 = component.get("c.updateSin");
            action4.setParams({'sin':component.get("v.sinObj")});
           	action4.setCallback(this, function(response){
            });
            	$A.enqueueAction(action4);
            var action5=component.get("c.updtBenef");
            action5.setParams({'data':component.get('v.selectedRowsDetails'),'recId':component.get("v.sinObj.Id")});
            action5.setCallback(this, function(response){
            });
            	$A.enqueueAction(action5);
        }
        var action = component.get("c.siniestroAction");
        action.setParams({'siniestroAct':btnValue, 'sinId':component.get("v.sinObj.Id")});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === 'SUCCESS') {
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    updateSelectedText : function(component,event,helper) {
        var selectedRows = event.getParam('selectedRows');
        component.set("v.selectedRowsCount" ,selectedRows.length );
        let obj = [];
        for (var i = 0; i < selectedRows.length; i++){
            obj.push({MX_SB_SAC_Email__c:selectedRows[i].MX_SB_SAC_Email__c},{Id:selectedRows[i].Id});
        }
        component.set("v.selectedRowsDetails" ,JSON.stringify(obj));
    }
})