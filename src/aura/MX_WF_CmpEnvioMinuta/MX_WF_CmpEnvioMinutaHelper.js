({
    sendEmail: function (cmp) {
        var action = cmp.get("c.sendTemplatedEmail");
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        action.setParams({ whatId : cmp.get("v.recordId")});
        action.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: "Envío Exitoso",
                    message: "La minuta fue enviada a todos los asistentes.",
                    type: "success"});
                toastEvent.fire();
                 dismissActionPanel.fire();
            }
         else {
                console.log('error ' + JSON.stringify(response.getError()));
                var toastError = $A.get("e.force:showToast");
                toastError.setParams({
                    title: "Error",
                    message: "Ocurrió un error al enviar la minuta.",
                    type: "error"
                });
                toastError.fire();
              }
        });
        $A.enqueueAction(action);
    }
})