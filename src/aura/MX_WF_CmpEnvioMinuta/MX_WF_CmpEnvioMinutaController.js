({
    handleClick: function (cmp, event, helper) {
        helper.sendEmail(cmp);
        },
    handleClickCanc : function (cmp, event, helper) {
       var dismissActionPanel = $A.get("e.force:closeQuickAction");
       dismissActionPanel.fire();
    }
})