({
    doInit:function(component, event, helper){
        helper.changeStatus(component, event, helper);
        var action = component.get("c.getValue");
        action.setParams({"recId": component.get("v.recordId") });
        action.setCallback(this, function(response) {
            component.set("v.Phone", response.getReturnValue());
        });
        $A.enqueueAction(action);
    }
})