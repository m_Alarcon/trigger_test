({
	changeStatus : function(component, event, helper) {
        const recordId = component.get("v.recordId");
        let openCase = component.get("c.changeStatus");
        openCase.setParams({"recordId": recordId });
        openCase.setCallback(this, function(response) {
        });
        $A.enqueueAction(openCase);
	}
})