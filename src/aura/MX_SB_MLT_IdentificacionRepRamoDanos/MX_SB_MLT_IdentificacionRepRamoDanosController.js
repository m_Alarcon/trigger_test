({
    handleCancelIdenDanos: function(component) {
        component.set('v.isEditMode', false);
    },
    doInitIdenDanos : function(component) {
        var action = component.get("c.getRecordType");
	action.setParams({"devname":"MX_SB_MLT_RamoDanos"});
        action.setCallback(this, function(response) {
            component.set("v.recordtypeDanios",response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    handleSuccessIdenDanos : function(component, event) {
	var payload = event.getParams().response;
        var navService = component.find("navService");
        var pageReference = {
            type: 'standard__recordPage',
            attributes: {
                "recordId": payload.id,
                "objectApiName": component.get("v.sObjectName"),
                "actionName": "view"
            }
        }
        event.preventDefault();
        navService.navigate(pageReference);
    },
    toggleEditModeIdenDanos : function(component) {
        component.set("v.isEditMode", !component.get("v.isEditMode"));
    },
    handleEditIdenDanos : function(component) {
        component.find("SiniestroEditForm").submit();
        $A.get("e.force:refreshView").fire();
        $A.get("e.force:editRecord").fire();
        component.set('v.isEditMode', false);
    }
})