({
    doInit : function(component, event, helper) {
		var action = component.get('c.loadOppE');
        action.setParams({'oppId': component.get('v.recordId')});
        action.setCallback(this, function(response) {
            component.set('v.oppObj', response.getReturnValue());
            var action2= component.get('c.loadContactAcc');
            action2.setParams({'accId':response.getReturnValue().AccountId});
            action2.setCallback(this, function(response) {
                component.set('v.cntcObj',response.getReturnValue()[0]);
            });
            $A.enqueueAction(action2);
        });
        $A.enqueueAction(action);
    },
	viewAccount : function(component, event, helper) {
        if(component.find('envname').get("v.value")===''){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                message: 'Debe ingresar el nombre de destinatario.',
                duration:' 2000',
                key: 'check',
                type: 'error'
            });
            toastEvent.fire();
        }else if (component.find('envemail').get("v.value")===''){
            var toastEvent2 = $A.get("e.force:showToast");
            toastEvent2.setParams({
                message: 'Debe ingresar el correo de destinatario.',
                duration:' 2000',
                key: 'check',
                type: 'error'
            });
            toastEvent2.fire();
        }else{
            helper.helperMethod(component, event, helper);
            var toastEvent3 = $A.get("e.force:showToast");
            toastEvent3.setParams({
                message: 'Se ha enviado el correo exitosamente.',
                duration:' 2000',
                key: 'check',
                type: 'success'
            });
            toastEvent3.fire();
        }
	},
})