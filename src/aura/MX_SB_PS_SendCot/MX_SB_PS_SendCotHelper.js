({
    helperMethod : function(component, event, helper) {
        var sncQte = component.get('v.oppObj.SyncedQuoteId');
            var action2 = component.get('c.getQuoteLneItem');
        action2.setParams({'quoteId':sncQte});
            action2.setCallback(this, function(responseQL) {
                var actionA=component.get('c.sendCotService');
                var sNombre=component.find('envname').get("v.value");
            var correin=component.find('envemail').get("v.value");
            var formatter = new Intl.NumberFormat('en-US', {
  				style: 'currency',
  				currency: 'USD',
			});
                actionA.setParams({'mapdata':{
                'sNombre': sNombre.toUpperCase(),
                'sMONTO': formatter.format(responseQL.getReturnValue()[0].Quantity),
                'sNUMERO': '' ,
                'sPERIODICIDAD': responseQL.getReturnValue()[0].MX_SB_VTS_FormaPAgo__c,
                'sCODIGOPROMO': '',
                'sPLAN': responseQL.getReturnValue()[0].MX_SB_VTS_Plan__c.toUpperCase(),
                'sCANTIDAD1': formatter.format(responseQL.getReturnValue()[0].MX_SB_PS_Dias3a5__c),
                'sCANTIDAD2': formatter.format(responseQL.getReturnValue()[0].MX_SB_PS_Dias6a35__c),
                'sCANTIDAD3': formatter.format(responseQL.getReturnValue()[0].MX_SB_PS_Dias36omas__c),
                'sCANTIDAD4': formatter.format(responseQL.getReturnValue()[0].MX_SB_VTS_Total_Gastos_Funerarios__c),
                    'listhref':'',
                'mailreciver': correin,
                'subject': 'Cotizaciòn de Respaldo Seguro para Hospitalizaciòn',
                    'productId':'RS_HOSPITALARIO'
                }});
                actionA.setCallback(this, function(response) {
                });
                $A.enqueueAction(actionA);
            });
            $A.enqueueAction(action2);
    },
})