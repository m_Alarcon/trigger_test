({ consultaCertificados : function(component, event, helper) {
        var action = component.get("c.getTableCertificados");
        action.setParams({"ideuno":"test","test":"otravar"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.infoCertificado", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    certificadosTable : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:MX_SB_MLT_DetalleCoberturas",
            componentAttributes: {
                objetoId : "a128A000001CWQTQA4"
            }
        });
        evt.fire();
    },
    changeTabName : function(component, event) {
        var workspaceAPICertificado = component.find("workspace");
        workspaceAPICertificado.getFocusedTabInfo().then(function(response) {
			workspaceAPICertificado.isSubtab({
				tabId: response.tabId
            	}).then(function(response) {
                	if (response) {
						var focusedTabId = response.tabId;
            			workspaceAPICertificado.setTabLabel({
                		tabId: focusedTabId,
                		label: "Certificados",
            			});
            			workspaceAPICertificado.setTabIcon({
                		tabId: focusedTabId,
                		icon: "action:record",
                        iconAlt:"Certificados"
            			});
                	}
            });
        })
    }
})