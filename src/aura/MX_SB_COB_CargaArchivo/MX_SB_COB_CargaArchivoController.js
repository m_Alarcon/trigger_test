({
	 doInit : function(component, event, helper) {
    var myAction = component.get("c.getCargaArchivo");
         var param = component.get("v.recordId");
         myAction.setParams({
             "idIn" :  param
         });
    myAction.setCallback(this, function(response) {
      if(response.getState() === "SUCCESS") {
        component.set("v.archivos", response.getReturnValue());
          var existe = component.get("v.archivos");
          if(existe.Name!=null){
              component.set('v.flag',true);
          }
      }else{
       alert('error'+response.getError());
      }
    });
    $A.enqueueAction(myAction);
  },
})