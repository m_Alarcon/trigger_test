({
    handleCancelSeleccionarServiciosAtnCol: function(component) {
        component.set('v.isEditMode', false);
    },
    doInitSeleccionarServiciosAtnCol : function(component) {
	var action = component.get("c.getRecordType");
        action.setParams({"devname":"MX_SB_MLT_RamoAuto"});
        action.setCallback(this, function(response) {
            component.set("v.recordtypeAUTO",response.getReturnValue());
        });
        $A.enqueueAction(action);
        var action2 = component.get("c.getDatosSiniestro");
        action2.setParams( { "idsini":component.get("v.recordId") } );
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.vgrua",response.getReturnValue().MX_SB_MLT_Proveedor__c);
                component.set("v.vparamedico",response.getReturnValue().MX_SB_MLT_Proveedor_Paramedico__c);
                component.set("v.vambulancia",response.getReturnValue().MX_SB_MLT_ProveedorAmbulancia__c);
            }
        });
        $A.enqueueAction(action2);
    },
    toggleEditModeSeleccionarServiciosAtnCol : function(component) {
       component.set("v.isEditMode", !component.get("v.isEditMode"));
    },
    handleEditFormSeleccionarServiciosAtnCol : function(component) {
        component.set('v.etapa','Servicios Enviados');
        component.find("SiniestroEditForm").submit();
        var action = component.get("c.createServices");
        action.setParams({"idSiniestro":component.get("v.recordId"),
			"idProveedor":component.get("v.vgrua"),
			"idParamedico":component.get("v.vparamedico"),
			"idAmbulancia":component.get("v.vambulancia")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            var mensaje ='';
            var tipo ='';
            var toastEvent = $A.get("e.force:showToast");
            if (state === "SUCCESS") {
          		tipo='success';
                mensaje='cambio exitoso';
            } else {
                tipo='error';
                mensaje='Error en envío';
            }
              toastEvent.setParams( {
                title: "Actualización de registro",
                type:tipo,
            	message: mensaje
            } );
           toastEvent.fire();
        });
        $A.enqueueAction(action);
    },
})