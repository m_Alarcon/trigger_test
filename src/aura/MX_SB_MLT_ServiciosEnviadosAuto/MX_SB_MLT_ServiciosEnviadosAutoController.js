({
    doInitServiciosEnviadosAuto : function(c,e,h) {
        h.doInit_HelperServiciosEnviadosAuto(c,e,h)
    },
    ndleClick: function (component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.sin.Id")
        });
        navEvt.fire();
    }
})