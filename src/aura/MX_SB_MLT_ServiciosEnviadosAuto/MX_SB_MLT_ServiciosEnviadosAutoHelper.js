({
    doInit_HelperServiciosEnviadosAuto : function(c,e,h) {
        var id = c.get("v.recordId");
        var action = c.get("c.getServiciosEnviados");
        action.setParams({
            "sinid": id
        });
        action.setCallback(this, function(res) {
                var storedResponse = res.getReturnValue();
                    var newObjList = [];
                    for (var i = 0; i < storedResponse.length; i++) {
                        var obj = storedResponse[i];
                        var newObj = {};
                        for (var key in obj) {
                            if (obj.hasOwnProperty(key)) {
                                var keyList = key.split('__');
                                var newKey = '';
                                if (keyList.length > 2) {
                                    newKey = keyList[1].concat('__' + keyList[2]);
                                } else {
                                    newKey = key;
                                }
                                newObj[newKey] = obj[key];
                            }
                        }
                        c.set("v.SiniestroObject", newObj);
                        newObjList.push(newObj);
                    }
                    c.set("v.SiniestroObjectList", newObjList);
        });
        $A.enqueueAction(action);
    },
})