({
    doInit: function (component, event, helper) {
        let oppId = component.get('v.recordId');
        if (oppId !== undefined) {
            helper.findDataoPP(component, event, helper);
        }
    },
    generarOTP: function (component, event, helper) {
        helper.generateToken(component, event, helper);
    },
    closeModel: function (component, event, helper) {
        component.set("v.otpOk", false);
    },
    failOTP: function (component, event, helper) {
        component.set("v.otpOk", false);
    },
    processPC: function (component, event, helper) {
        component.set("v.otpOk", false);
        component.set("v.isDisabled", true);
        component.set("v.paymentOk", false);
    },
    processResponsePC: function (component, event, helper) {
        let status = event.getParam("responseStatus");
        let statusDetail = event.getParam("responseDescript");
        let resDetailMe = event.getParam("responseMessage");
        helper.processResponceHelp(component, event, helper, status, statusDetail, resDetailMe);
    },
    checkServiceStatus: function (component, event, helper) {
        helper.callWebService(component, event, helper);
    },
    createdPolicy: function (component, event, helper) {
        helper.createPolicy(component, event, helper);
    },
    sendPolyceEmail: function (component, event, helper) {
        helper.sendPolicyByEmail(component, event, helper);
    },
    updState: function (component, event, helper) {
        helper.updStage(component, event, helper);
    }
})