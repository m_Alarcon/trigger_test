({
    findDataoPP: function (component, event, helper) {
        let oppId = component.get('v.recordId');
        var action = component.get('c.findDataQuote');
        action.setParams({
            oppId: oppId
        });
        action.setCallback(this, function (response) {
            let state = response.getState();
            let dataRes = response.getReturnValue();
            if (state === 'SUCCESS') {
                if (dataRes.isOk) {
                    component.set('v.genericRecord', dataRes.quoteData);
                    helper.processResponse(component, event, helper, dataRes);
                    if (dataRes.quoteData.MX_SB_VTS_Numero_de_Poliza__c !== undefined && dataRes.quoteData.MX_SB_VTS_Numero_de_Poliza__c !== '') {
                        component.set('v.dataOk', false);
                        component.set('v.paymentOk', true);
                        component.set('v.sendPolicy', true);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },

    processResponse: function (component, event, helper, data) {
        let lstObjects = [];
        let totalPayments = '0';
        var itemsProcessed = 0;
        data.dataPayment.forEach(element => {
            itemsProcessed++;
            let valueData = '';
            switch (element.idValue) {
                case 'frecuencia':
                    totalPayments = helper.evaluteFrec(element);
                    valueData = element.dataValue;
                    break;
                case 'pagos':
                    if (totalPayments === '1') {
                        valueData = totalPayments + ' pago de ' + helper.formatCurrency(element.dataValue);
                    } else {
                        valueData = totalPayments + ' pago(s) de ' + helper.formatCurrency(element.dataValue);
                    }
                    break;
                case 'sumaAseg':
                case 'totalPagar':
                    valueData = helper.formatCurrency(element.dataValue);
                    break;
                case 'cobertura':
                    component.set('v.coberturasProduct', element.dataValue);
                    component.set('v.coberturasTitle', element.textVal);
                    break;
                default:
                    valueData = element.dataValue;
                    if (element.idValue === 'correoEmail' && valueData.includes('dummy')) {
                        valueData = 'Sin correo';
                    }
                    if (valueData === '') {
                        valueData = 'Sin datos';
                    }
                    break;
            }
            if (element.idValue !== 'cobertura') {
                let itemDisplay = {
                    "title": element.textVal,
                    "valueItem": valueData
                }
                lstObjects.push(itemDisplay);
            }
            if (itemsProcessed === data.dataPayment.length) {
                component.set('v.dataSectOne', lstObjects);
                component.set('v.dataOk', true);
            }
        });
        data.dataPayment.forEach(element => {
            if (element.idValue === 'cobertura') {
                component.set('v.coberturasProduct', element.dataValue);
                component.set('v.coberturasTitle', element.textVal);
            }
        });
        component.set('v.policy', data.quoteData.MX_SB_VTS_Numero_de_Poliza__c);
        component.set('v.startDate', data.quoteData.MX_SB_VTS_FechaInicio__c);
    },

    evaluteFrec: function (elementData) {
        let lstVals = '';
        switch (elementData.dataValue) {
            case 'Mensual':
                lstVals = '12';
                break;
            case 'Semestral':
                lstVals = '2';
                break;
            case 'Anual':
                lstVals = '1';
                break;
            case 'Trimestral':
                lstVals = '4';
                break;
            default:
                break;
        }
        return lstVals;
    },

    formatCurrency: function (amount) {
        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
        });
        return formatter.format(amount);
    },
    generateToken: function (component, event, helper) {
        helper.generateOTP(component, event, helper);
    },
    generateOTP: function (component, event, helper) {
        component.set('v.spinner', true);
        let oppId = component.get('v.recordId');
        var findQuote = component.get('c.updateQuoteIVR');
        findQuote.setParams({
            oppId: oppId
        });
        findQuote.setCallback(this, function (response) {
            component.set('v.spinner', false);
            let state = response.getState();
            if (state === 'SUCCESS') {
                let action = component.find('openpaychild').retriveTokenHandler();
                let quoteData = component.get('v.genericRecord');
                let pureCLoudEvent = $A.get("e.c:MX_SB_RTL_ListenerPaymentPC");
                pureCLoudEvent.setParams({ "folioCot": quoteData.MX_SB_VTS_ASO_FolioCot__c });
                pureCLoudEvent.setParams({ "oppId": quoteData.Id });
                pureCLoudEvent.setParams({ "securityToken": action });
                pureCLoudEvent.fire();
                component.set('v.otpOk', true);
            }
        });
        $A.enqueueAction(findQuote);
    },
    callWebService: function (component, event, helper) {
        component.set('v.spinner', true);
        let recordId = component.get("v.recordId");
        let updateStatus = component.get("c.updateStatuscOT");
        updateStatus.setParams({
            oppId: recordId
        });
        component.find('openpaychild').generateToken();
        updateStatus.setCallback(this, function (response) {
            component.set('v.spinner', false);
            if (response.getState() === 'SUCCESS') {
                helper.processReStatus(component, event, helper, response);
            } else {
                helper.showToast(component, event, helper, '', 'warning', 'Ha ocurrido un error');
            }
        });
        $A.enqueueAction(updateStatus);
    },
    processReStatus: function (component, event, helper, response) {
        let values = response.getReturnValue();
        if (values.statusQuote === true) {
            helper.generateOTPService(component, event, helper);
        } else {
            if (values.updateQuote !== undefined) {
                if (values.updateQuote.quoteUpdated === true) {
                    helper.generateOTPService(component, event, helper);
                } else {
                    helper.showToast(component, event, helper, values.updateQuote.callExMsj, 'Error', 'Ha ocurrido un error al formalizar la cotización');
                }
            } else {
                helper.showToast(component, event, helper, 'No se ha podido formalizar la Cotización, Intente nuevamente',
                    'warning',
                    'Ha ocurrido un error al formalizar la cotización');
            }
        }
    },
    generateOTPService: function (component, event, helper) {
        var tokenChild = component.find('openpaychild').retriveTokenHandler();
        component.set('v.spinner', true);
        let recordId = component.get("v.recordId");
        let service = component.get("c.getCodeStatus");
        service.setParams({
            recordId: recordId, tokenSF : tokenChild
        });
        service.setCallback(this, function (response) {
            component.set('v.spinner', false);
            if (response.getState() === 'SUCCESS') {
                let values = response.getReturnValue();
                helper.messageError(component, event, helper, values.Code);
            }
        });
        $A.enqueueAction(service);
    },
    messageError: function (component, event, helper, code) {
        switch (code) {
            case '405' :
                let error405 = $A.get("$Label.c.MX_SB_VTS_createOTP405");
                helper.showToast(component, event, helper, error405);
                break;
            case '400':
                let error400 = $A.get("$Label.c.MX_SB_VTS_createOTP400");
                helper.showToast(component, event, helper, error400, 'warning', 'Ha ocurrido un error');
                break;
            case '500':
                helper.showToast(component, event, helper, 'Error interno en el servidor, Intente nuevamente', 'warning', 'Ha ocurrido un error, Intente nuevamente');
                helper.generateToken(component, event, helper);
                break;
            case "200":
            case "201":
            case "202":
            case "204":
                component.set('v.otpOk', true);
                helper.generateToken(component, event, helper);
                break;
        }
    },
    showToast: function (component, event, helper, message, toastType, toastTitle) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": toastTitle,
            "message": message,
            "type": toastType
        });
        toastEvent.fire();
    },
    processResponceHelp: function (component, event, helper, status, statusDetail, resDetailMe) {
        if(status === '' || status === '""') {
            helper.evaluateNOK(component, event, helper, resDetailMe);
        } else {
            helper.evaluateIvrCode(component, event, helper, status, statusDetail, resDetailMe);
        }
    },
    evaluateNOK: function(component, event, helper, resDetailMe) {
        helper.showToast(component, event, helper, resDetailMe, 'warning', 'El cliente requiere apoyo');
    },
    evaluateIvrCode: function(component, event, helper, status, statusDetail, resDetailMe) {
        if(status === '400') {
            if(resDetailMe !== '' && resDetailMe !== '""') {
                helper.findStatus(component, event, helper, resDetailMe);
            }
        } else {
            helper.processIvrCode(component, event, helper, status);
        }
    },
    findStatus: function(component, event, helper, resDetailMe) {
        if(resDetailMe !== undefined && resDetailMe.includes('#')) {
            let indexInit = resDetailMe.indexOf('#');
            let indexFinish = resDetailMe.indexOf('-');
            let statusFourth = resDetailMe.substring(indexInit+1, indexFinish);
            helper.processIvrCode(component, event, helper, statusFourth);
        } else {
            helper.showToast(component, event, helper, resDetailMe, 'warning', 'El cliente requiere apoyo');
        }
    },
    processIvrCode: function (component, event, helper, status) {
        var action = component.get('c.findDataCode');
        action.setParams({
            ivrCode: status
        });
        action.setCallback(this, function (response) {
            let state = response.getState();
            let dataRes = response.getReturnValue();
            if (state === 'SUCCESS') {
                if (status !== '0') {
                    helper.showToast(component, event, helper, dataRes.msjDetail, 'error', dataRes.title);
                } else {
                    helper.showToast(component, event, helper, 'El Pagó se ha realizó con Éxito!', 'success', 'Cobro Exitoso');
                    helper.createPolicy(component, event, helper);
                }
            }
        });
        $A.enqueueAction(action);
    },
    createPolicy: function (component, event, helper) {
        component.set('v.spinner', true);
        var action = component.get('c.createdPolyce');
        let recordId = component.get("v.recordId");
        action.setParams({
            oppId: recordId
        });
        action.setCallback(this, function (response) {
            component.set('v.spinner', false);
            let state = response.getState();
            if (state === 'SUCCESS') {
                let dataRes = response.getReturnValue();
                if (dataRes.policyCreated === true) {
                    helper.evaluteCretedPolicy(component, event, helper, dataRes);
                } else {
                    helper.showToast(component, event, helper, 'La poliza ya se encuentra emitida', 'error', 'Poliza emitida');
                }
            }
        });
        $A.enqueueAction(action);
    },
    evaluteCretedPolicy: function (component, event, helper, data) {
        component.set("v.paymentOk", true);
        if (data.polyceStatus !== undefined) {
            if (data.polyceStatus.quoteUpdated === true) {
                helper.evaluateSend(component, event, helper, data);
            } else {
                helper.showToast(component, event, helper, data.polyceStatus.callExMsj, 'warning', 'Ocurrio un error al emitir la poliza');
            }
        } else {
            helper.showToast(component, event, helper, 'No se logro emitir la poliza, intente más tarde', 'warning', 'Poliza no emitida');
        }
    },
    evaluateSend: function (component, event, helper, data) {
        component.set("v.paymentOk", true);
        helper.showToast(component, event, helper, 'La poliza se emitio correctamente', 'success', 'Poliza emitida');
        component.set('v.policy', data.polyceStatus.upsrtQuote.MX_SB_VTS_Numero_de_Poliza__c);
        component.set('v.startDate', data.polyceStatus.upsrtQuote.MX_SB_VTS_FechaInicio__c);
        component.set('v.sendPolicy', true);
        let sendPolicyC = component.get('v.sendPol');
        if (sendPolicyC) {
            helper.sendDocumentsByEmail(component, event, helper);
        }
    },
    sendPolicyByEmail: function (component, event, helper) {
        let oppId = component.get('v.recordId');
        var action = component.get('c.findDataQuote');
        action.setParams({
            oppId: oppId
        });
        component.set('v.spinner', true);
        action.setCallback(this, function (response) {
            let state = response.getState();
            let dataRes = response.getReturnValue();
            if (state === 'SUCCESS') {
                component.set('v.spinner', false);
                if (dataRes.isOk) {
                    if (dataRes.quoteData.MX_SB_VTS_Numero_de_Poliza__c !== undefined && dataRes.quoteData.MX_SB_VTS_Numero_de_Poliza__c !== '') {
                        helper.sendDocumentsByEmail(component, event, helper, 'policy');
                    } else {
                        component.set('v.sendPol', true);
                        helper.createPolicy(component, event, helper);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    sendDocumentsByEmail: function (component, event, helper, doctype) {
        let oppId = component.get('v.recordId');
        var actionSent = component.get('c.findCustomerDocument');
        actionSent.setParams({
            oppId: oppId,
            docType: doctype
        });
        component.set('v.spinner', true);
        actionSent.setCallback(this, function (response) {
            let state = response.getState();
            let dataRes = response.getReturnValue();
            component.set('v.spinner', false);
            if (state === 'SUCCESS') {
                if (dataRes.isOk) {
                    helper.processGetDocs(component, event, helper, doctype, dataRes);
                } else {
                    helper.showToast(component, event, helper, dataRes.msjError, 'Error', 'Envío no exitoso');
                }
            } else {
                helper.showToast(component, event, helper, 'Ocurrio un error al intentar enviar la póliza', 'Error', 'Servicio no disponible');
            }
        });
        $A.enqueueAction(actionSent);
    },
    processGetDocs: function (component, event, helper, doctype, response) {
        switch (doctype) {
            case 'policy':
                component.set('v.policyhref', response.hrefDoc);
                helper.sendDocumentsByEmail(component, event, helper, 'kit');
                break;
            case 'kit':
                component.set('v.kithref', response.hrefDoc);
                helper.sendDocumentsByEmail(component, event, helper, 'conditions');
                break;
            case 'conditions':
                component.set('v.conditionshref', response.hrefDoc);
                helper.sendLstDocs(component, event, helper);
                break;
            default:
                break;
        }
    },
    sendLstDocs: function (component, event, helper) {
        let policyHref = component.get('v.policyhref');
        let kithref = component.get('v.kithref');
        let conditionshref = component.get('v.conditionshref');
        let oppId = component.get('v.recordId');
        var actionSent = component.get('c.sendCustomerDocument');
        let lstDocs = [policyHref, kithref, conditionshref];
        actionSent.setParams({
            oppId: oppId,
            lstHrefsDocs: lstDocs
        });
        component.set('v.spinner', true);
        actionSent.setCallback(this, function (response) {
            let state = response.getState();
            let dataRes = response.getReturnValue();
            component.set('v.spinner', false);
            if (state === 'SUCCESS') {
                if (dataRes.isOk) {
                    helper.showToast(component, event, helper, 'La poliza se envío correctamente', 'success', 'Envio correcto');
                } else {
                    helper.showToast(component, event, helper, dataRes.msjError, 'Error', 'Envío no exitoso');
                }
            } else {
                helper.showToast(component, event, helper, 'Ocurrio un error al intentar enviar la póliza', 'Error', 'Servicio no disponible');
            }
        });
        $A.enqueueAction(actionSent);
    },
    updStage: function (component, event, helper) {
        var actionUpd = component.get('c.updCurrentOppCtrl');
        let recOppId = component.get('v.recordId');
        actionUpd.setParams({
            srtOppId: recOppId,
            etapaForm: 'formalizada'
        });
        actionUpd.setCallback(this, function (response) {
            let state = response.getState();
            if (state === 'SUCCESS') {
                $A.get("e.force:refreshView").fire();
                helper.showToast(component, event, helper, 'Se regreso a la etapa correctamente', 'success', 'Envio correcto');
            } else {
                helper.showToast(component, event, helper, 'Ocurrio un error al intentar regresar a la etapa', 'Error', 'Error');
            }
        });
        $A.enqueueAction(actionUpd);
    }
})