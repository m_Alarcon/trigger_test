({
    doinit :  function(component, event, helper) {
        helper.doInit(component,event,helper);
    },
    onSingle: function(component, event, helper) {
        var objetos =JSON.parse(component.get('v.textFromEvent'));
        component.set('v.oppObj.Plan__c',objetos.QuoteLineItem.QuoteLineItem.MX_SB_VTS_Tipo_Plan__c);
        component.set('v.foliocot',objetos.foliocot.foliocot.foliocot);
        component.set('v.psecuente',objetos.Quote.Quote.MX_SB_PS_PagosSubsecuentes__c);
        component.set('v.quoteLinIt',objetos.QuoteLineItem.QuoteLineItem);
        helper.onSingle(component,event,helper);
        helper.onclickear(component,event,helper);
    },
})