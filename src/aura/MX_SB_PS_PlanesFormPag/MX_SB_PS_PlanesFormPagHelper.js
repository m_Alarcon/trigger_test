({
    doInit : function(component,event,helper) {
        var oppId=component.get('v.recordId');
        var action2= component.get('c.loadOppF');
        action2.setParams({'oppId':oppId});
        action2.setCallback(this, function(response2) {
            component.set('v.oppObj',response2.getReturnValue());
            component.set('v.quoteObj.OpportunityId',response2.getReturnValue().Id);
            component.set('v.quoteObj.Name',response2.getReturnValue().Name);
            if(response2.getReturnValue().Plan__c===undefined) {
                component.set('v.oppObj.Plan__c','Individual');
            }
            var oppObj = component.get('v.oppObj');
            var action = component.get('c.upsrtOpp');
            action.setParams({'oppObj':oppObj});
            action.setCallback(this, function(response) {
            });
            $A.enqueueAction(action);
            var quoteObj= component.get('v.quoteObj');
            var action3= component.get('c.getUpsrtQuote');
            action3.setParams({'quoteObj':quoteObj});
            action3.setCallback(this, function(response3) {
                component.set('v.oppObj.SyncedQuoteId',response3.getReturnValue()[0].Id);
                var oppObj = component.get('v.oppObj');
                var action = component.get('c.upsrtOpp');
                action.setParams({'oppObj':oppObj});
                action.setCallback(this, function(response) {
                });
                $A.enqueueAction(action);
            });
            $A.enqueueAction(action3);
        });
        $A.enqueueAction(action2);
    },
    onSingle: function(component, event, helper) {
        if(component.get('v.oppObj.Plan__c')!== '') {
            component.set('v.itsEmpty',false);
        }
        switch (component.get('v.oppObj.Plan__c')) {
            case 'Conyugal':
                component.set('v.MaxClicks','1');
                break;
            case 'Familiar hasta 3 hijos':
                component.set('v.MaxClicks','4');
                break;
            case 'Familiar hasta 5 hijos':
                component.set('v.MaxClicks','6');
                break;
            case 'Titular y un hijo':
                component.set('v.MaxClicks','1');
                break;
            case 'Titular hasta 3 hijos':
                component.set('v.MaxClicks','3');
                break;
            case 'Titular hasta 5 hijos':
                component.set('v.MaxClicks','5');
                break;
        }
        var cmpEvent =$A.get("e.c:MX_SB_PS_EventHandlerOpp");
        cmpEvent.setParams({"seccion" : component.get('v.oppObj.Plan__c'),
                            "MaxClicks" : component.get('v.MaxClicks'),
                            "quoteLineI":component.get('v.quoteLinIt'),
                            "foliocotizacion":component.get('v.foliocot'),
                            "pagossubse":component.get('v.psecuente')});
        cmpEvent.fire();
    },
    onclickear: function(component,event,helper) {
        var oppObj = component.get('v.oppObj');
        var action = component.get('c.upsrtOpp');
        action.setParams({'oppObj':oppObj});
        action.setCallback(this, function(response) {
        });
        $A.enqueueAction(action);
    },
})