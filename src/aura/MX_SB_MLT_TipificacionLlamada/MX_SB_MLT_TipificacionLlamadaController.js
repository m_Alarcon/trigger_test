({
    showInfo: function(component, event, helper) {
        var btnValue = event.getSource().get("v.value");
        var selectedMenuItemValue = event.getParam("value");
        component.set("v.Task.Status",btnValue);
        component.set('v.Task.Resultado_llamada__c',selectedMenuItemValue);
        var recordId= component.get("v.recordId");
        var action2= component.get("c.updTask");
        action2.setParams({
            'recId':recordId,
            'tipo': btnValue,
            'subtipo':selectedMenuItemValue
        });
        action2.setCallback(this, function(response) {
            component.set("v.disnot",true);
            });
        $A.enqueueAction(action2);
    },
     doInit:function(component, event, helper) {
        var action = component.get("c.getValue");
        action.setParams({"recId": component.get("v.recordId") });
        action.setCallback(this, function(response) {
            component.set("v.Telefono", response.getReturnValue());
        });
        $A.enqueueAction(action);
        var recId = component.get("v.recordId");
        var action3 = component.get("c.getInfo");
         action3.setParams({'recId': recId});
         action3.setCallback(this, function(response) {
             if(response.getReturnValue().Status!=='No iniciada') {
                 component.set("v.disnot",true);
             }
         });
         $A.enqueueAction(action3);
    },
    navigateToMyComponent : function(component, event, helper) {
        var btnValue = event.getSource().get("v.value");
        component.set("v.Task.Status",btnValue);
        var recordId= component.get("v.recordId");
        var action4= component.get("c.updTask");
        action4.setParams({
            'recId':recordId,
            'tipo': btnValue,
            'subtipo':''
        });
        action4.setCallback(this, function(response) {});
        $A.enqueueAction(action4);
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
			componentDef : "c:MX_SB_MLT_RecordTypesFormD",
            isredirect: true,
            componentAttributes: {
                Telefono : component.get("v.Telefono")
            }
        });
        evt.fire();
    }
})