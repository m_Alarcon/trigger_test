({
	rechazar : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.autoajuste");
        action.setParams({'recordId':recordId,'rechacept':'rechazar'});
        action.setCallback(this,function(response) {
          var state = response.getState();
            if(state ==='SUCCESS') {
		component.set("v.isOpen",false);
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
	},
    aceptar : function(component, event, helper) {
        let recordId = component.get('v.recordId');
        let action = component.get("c.autoajuste");
        action.setParams({'recordId':recordId,'rechacept':'aceptar'});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.isOpen",false);
                    $A.get('e.force:refreshView').fire();
                }
    		});
    	$A.enqueueAction(action);
    },

})