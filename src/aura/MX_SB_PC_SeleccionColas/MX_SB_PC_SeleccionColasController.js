({
    handleValueChange: function(component, evt, helper) {
        var objAsignaciones = component.get("v.userAsignaciones");
        var idCola = component.get("v.idCola");
        var idUsuario = component.get("v.idUsuario");
        for(var i = 0; i<objAsignaciones.length;i++) {
            var mismaCola = idCola === objAsignaciones[i].GroupId;
            var mismoUsuario =  idUsuario === objAsignaciones[i].UserOrGroupId;
            if(mismaCola && mismoUsuario) {
                component.set("v.seleccion","asignada");
            }
        }
    },
    guardarUsuarios: function (component, event) {
        var action = component.get("c.guardarAsignaciones");
        var usuarioId = component.get("v.idUsuario");
        var idDeCola = component.get("v.idCola");
        var estaAsignada = component.get("v.seleccion") === "asignada";
        action.setParams({
            'idUsuario': usuarioId,
            'idCola': idDeCola,
            'seAsigna': estaAsignada
        });
        action.setCallback(this, function(guardarNuevasAsignaciones) {
            var state = guardarNuevasAsignaciones.getState();
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Éxito',
                message: 'El registro se guardó',
                type: 'success'
            });
            if(state === "SUCCESS") {
                toastEvent.fire();
            }  else if(state === "ERROR") {
                var errors = respuesta.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        alert("Error de mensaje: " + errors[0].message);
                    }
                } else {
                    alert("Error desconocido");
                }
            } else if(state === "INCOMPLETE") {
                alert('respuesta incorrecta');
            }
        });
        $A.enqueueAction(action);
    }
})