({
    initComponent : function(component, event, helper) {
        helper.loadComponent(component, event, helper);
        helper.updateInteraction(component, event, helper);
    },
    sendingToPC : function(component, event, helper) {
        let folioCot = event.getParam("folioCot");
        let token = event.getParam("securityToken");
        let oppId = event.getParam("oppId");
        component.set('v.securityToken',token);
        component.set('v.folioCot',folioCot);
        component.set('v.oppId',oppId);
        helper.transferValues(component, event, helper);
    }
})