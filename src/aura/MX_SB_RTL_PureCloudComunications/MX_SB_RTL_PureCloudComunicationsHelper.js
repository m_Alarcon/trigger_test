({
    transferValues: function(component, event, helper) {
        var folioCot = event.getParam("folioCot");
        var otpCode = event.getParam("oppId");
        var token = event.getParam("securityToken");
        var proveedor = event.getParam("proveedor");
        var attributes = {
            "idcotiza":folioCot,
            "folioantifraude":token,
            "DescriptionCode":"",
            "ResponseCode":"",
            "MensajeAsesor":"",
            "objectId": otpCode,
            "idproveedor" : proveedor,
            "campania":""
        };
        helper.updateInteraction(component, event, helper, attributes);
    },
    updateInteraction : function(component, event, helper, attributes) {
        helper.initWindowEvent(component, helper);
    	var clientOrg = component.get('v.clientOrigin');
        var sourc = component.get('v.postMessageSource');
        var interaction = component.get('v.interactionId');
        var id = component.get('v.interactionId');
        if(sourc) {
            sourc.postMessage({
                type: 'PureCloud.Interaction.updateState',
                data: { id: interaction },
            }, clientOrg);
            sourc.postMessage({
                type: 'PureCloud.User.updateStatus',
                data: { id: interaction },
            }, clientOrg);

            sourc.postMessage({
                type: 'PureCloud.Interaction.addCustomAttributes',
                data: {
                    attributes: attributes,
                    id: id
                },
            }, clientOrg);
            helper.channelSubs(component, event, helper);
        }
    },
    outputToConsole: function(component, message) {
        if(message) {
            var consol = component.get('v.consoleMessages');
            component.set('v.consoleMessages', consol + message + " \r\n###");
        }
    },

    onReceiveNotification : function (component, message, helper) {
        var idOpportunidadCotiza = component.get('v.oppId');
        if(message.data.payload.MX_SB_RTL_IdActividad__c === idOpportunidadCotiza) {
            var errorCode = message.data.payload.MX_SB_RTL_ResponseCode__c;
            var errorDesc = message.data.payload.MX_SB_RTL_DescriptionCode__c;
            const empApi = component.find('empApi');
    	    var erroMsj= message.data.payload.MX_SB_RTL_MensajeAsesor__c;
            var pureCLoudEvent = $A.get("e.c:MX_SB_RTL_ResponsePaymentPC");
            pureCLoudEvent.setParams({"oppId" : idOpportunidadCotiza});
            pureCLoudEvent.setParams({"responseStatus" : errorCode});
            pureCLoudEvent.setParams({"responseDescript" : errorDesc});
	        pureCLoudEvent.setParams({"responseMessage" : erroMsj});
            pureCLoudEvent.fire();
            empApi.unsubscribe(component.get('v.subscription'), $A.getCallback(unsubscribed => {
                component.set('v.subscription', null);
            }));
        }
    },
    loadComponent: function(component, event, helper) {
        var action = component.get("c.callCenterUrl");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                var returnedUrl = response.getReturnValue();
                if(returnedUrl !== '') {
                    var clientOrigin = returnedUrl.match(/^(http(s?):\/\/[^\/]+)/gi)[0];
                    component.set('v.clientOrigin', clientOrigin);
                    helper.initWindowEvent(component, helper);
                }
            }
        });
        $A.enqueueAction(action);
    },
    channelSubs: function(component, event, helper) {
        const empApi = component.find('empApi');
	    const channel = component.get('v.channel');
	    const replayId = -1;
        const callback = function (message) {
            const descripMsj = message.data.payload.MX_SB_RTL_DescriptionCode__c;
            const msjase = message.data.payload.MX_SB_RTL_MensajeAsesor__c;
            const codeErr = message.data.payload.MX_SB_RTL_ResponseCode__c;
            if(descripMsj !== null || msjase !== null || codeErr !== null) {
                helper.onReceiveNotification(component, message);
                empApi.unsubscribe(component.get('v.subscription'), $A.getCallback(unsubscribed => {
                    component.set('v.subscription', null);
                }));
            }
        };
        empApi.subscribe(channel, replayId, $A.getCallback(callback)).then($A.getCallback(function (newSubscription) {
              component.set('v.subscription', newSubscription);
        }));
    },
    initWindowEvent: function(component, helper) {
        let clientOrigin = component.get('v.clientOrigin');
        window.addEventListener("message", $A.getCallback(function(event) {
            let message = '';
            if(event.origin !== clientOrigin) {
                return;
            }
            if(event.data && event.data.type) {
                if(event.source && event.data.type === 'Handshake') {
                    message = JSON.stringify(event.data);
                    helper.outputToConsole(component, message);
                    component.set('v.postMessageSource', event.source);
                }
                if(event.source && event.data.type === 'Interaction') {
                    message = JSON.stringify(event.data);
                    helper.outputToConsole(component, message);
                    if(event.data.data.id) {
                        component.set("v.interactionId", event.data.data.id );
                    }
                }
            }
        }), false);
    }
})