({
    helperInit : function(component) {
        var activeContracts = component.get('c.fetchActiveContractsByAccount');
        let recordId = component.get('v.recordId');
        activeContracts.setParams({
            oppId : recordId
        });
        activeContracts.setCallback(this, function(response){
            let result = response.getReturnValue();
            component.set('v.activeContracts', result);
            this.helperGetLength(component);
        });
     	$A.enqueueAction(activeContracts);
    },
    helperGetLength: function(component) {
        let array = component.get('v.activeContracts');
        switch(array.length) {
            case null:
            case undefined:
                component.set('v.listLength', 0);
                break;
            default:
                component.set('v.listLength', array.length);
                break;
        }
    }
})