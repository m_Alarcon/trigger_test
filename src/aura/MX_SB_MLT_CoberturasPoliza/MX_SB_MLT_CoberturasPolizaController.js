({
	toggleSection : function(component, event, helper) {
    var sectionAuraId =event.target.getAttribute("data-auraId");
    var sectionDiv = component.find(sectionAuraId).getElement();
    var sectionState =sectionDiv.getAttribute('class').search('slds-is-open');
    if(sectionState === -1) {
        sectionDiv.setAttribute('class' , 'slds-section slds-is-open');
    } else{
        sectionDiv.setAttribute('class' , 'slds-section slds-is-close');
    }
},
	actualiza: function(component,event,helper) {
    var afectados = event.getParam('selectedRows');
    var afecta = false;
    var validaahorro = ['Meta Ahorro','Meta Segura','Metaeducación','Seguro Estudia','ILP','MLB','Pensiones'];
    let counterahorro=0;
    if(validaahorro.indexOf(component.get("v.producto").MX_WB_Producto__r.Name)>=0){
        afectados.forEach(function(item, index, array) {
            if(validaahorro.indexOf(item.descript)>=0){
                counterahorro  = counterahorro+1;
            }
        });
    }else {
        afecta =helper.validafallecimiento(component,event,helper,afectados);
        component.set("v.afecta",afecta);
    }
    if (counterahorro>1) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Atencion',
            message:'Borrar selección  y elegir solo una cobertura de supervivencia/ahorro',
            messageTemplate: 'Borrar selección  y elegir solo una cobertura de supervivencia/ahorro',
            duration:' 2000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'});
        toastEvent.fire();
        afecta =false;
        component.set("v.afecta",afecta);
    } else {
        helper.validafecha(component,event,helper);
    }
    afecta= helper.validamonto(component,event,helper,afectados);
    component.set("v.afecta",afecta);
    var cmpEvent = component.getEvent("afectacion");
        cmpEvent.setParams({
            				"afecta" : component.get("v.afecta")
        				   });
        cmpEvent.fire();
},
})