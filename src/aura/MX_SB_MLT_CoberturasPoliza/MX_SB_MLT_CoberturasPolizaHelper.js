({validafecha : function(cmp,evt,helper){
	var validaahorro = ['Meta Ahorro','Meta Segura','Metaeducación','Seguro Estudia','ILP','MLB','Pensiones'];
	var fecharetorno=false;
	if(validaahorro.indexOf(cmp.get("v.producto").MX_WB_Producto__r.Name)>=0){
		fecharetorno = this.validafechaSupervivencia(cmp,evt,helper);
	} else {
		this.validafechareporte(cmp,evt,helper);
	}
	cmp.set("v.afecta",fecharetorno);
},
validafallecimiento : function (cmp,evt,hlp,afectados) {
	var continuafall=true;
	var diagnostico =false;
	var fallesimiento = false;
	afectados.forEach(function(item, index, array) {
		if(item.descript.includes('fallecimiento')){
			fallesimiento  =true;
		}
		if(item.descript.includes('diagnostico') && item.descript.includes('cancer')){
			diagnostico  =true;
			continuafall =this.validafechafallecimiento(cmp,evt,hlp);
		}
	});
	if(diagnostico && fallesimiento){
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
            title : 'Atencion',
            message:'No se puede elegir primer diagnostico de cancer y fallesimiento en el mismo siniestro',
            messageTemplate: 'No se puede elegir primer diagnostico de cancer y fallesimiento en el mismo siniestro',
            duration:' 2000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'});
		toastEvent.fire();
		continuafall= false;
	}
	return continuafall;
},
validafechafallecimiento : function (cmp,evt,hlp){
	var continua =true;
	var date = new Date(cmp.get("v.producto").MX_SB_MLT_DateDifunct__c);
	var deceso= date.getTime()/3600000;
	var date2= new Date(cmp.get("v.siniestro").CreatedDate);
	var diagnosticocancer = date2.getTime()/3600000;
	var time4evaluation = diagnosticocancer - deceso;
	if(time4evaluation>0){
		continua =false;
		cmp.set("v.afecta",continua);
	}else {
		cmp.set("v.afecta",continua);
	}
	return continua;
},
validafechaSupervivencia : function (cmp,evt,hlp){

	var continua =true;
	var validaahorro = ['Meta Ahorro','Meta Segura','Metaeducación','Seguro Estudia','ILP','MLB','Pensiones'];
    if(validaahorro.indexOf(cmp.get("v.producto").MX_WB_Producto__r.Name)>=0){
		var date = new Date(cmp.get("v.producto").MX_SB_SAC_FechaFinContrato__c);
		var vencimiento= date.getTime()/3600000;
		var date2= new Date(cmp.get("v.siniestro").CreatedDate);
		var siniestro = date2.getTime()/3600000;
		var time4evaluation = siniestro - vencimiento;
		if(time4evaluation>0){
			cmp.set("v.afecta",continua);
		} else {
			continua=false;
			cmp.set("v.afecta",continua);
			var toastEvent = $A.get("e.force:showToast");
			toastEvent.setParams({
				title : 'Atencion',
				message:'Fecha de siniestro no puedes ser antes de Vigencia de póliza',
				messageTemplate: 'Fecha de siniestro no puedes ser antes de Vigencia de póliza',
				duration:' 2000',
				key: 'info_alt',
				type: 'error',
				mode: 'pester'});
			toastEvent.fire();
		}
	}
	return continua;
},
validamonto : function(cmp,evt,hlp,afectados) {

	var continua=false;
	afectados.forEach(function(item, index, array) {
		if(item.monto>0) {
			continua=true
		}
	});
	if(continua===false){
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
            title : 'Atencion',
            message:'La suma total ed las coberturas no puedes igual a 0',
            messageTemplate: 'La suma total ed las coberturas no puedes igual a 0',
            duration:' 2000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'});
		toastEvent.fire();
	}
	return continua;
},
validafechareporte : function (cmp,evt,hlp){
	var continua =true;
	var date = new Date(cmp.get("v.producto").MX_SB_SAC_FechaFinContrato__c);
	var fvig= date.getTime()/3600000;
	var date2= new Date(cmp.get("v.siniestro").CreatedDate);
	var fechareporte = date2.getTime()/3600000;
	var time4evaluation =  fvig -fechareporte;
	if(time4evaluation>0){
		continua =false;
		cmp.set("v.afecta",continua);
	}else {
		cmp.set("v.afecta",continua);
		var toastEventreporte = $A.get("e.force:showToast");
		toastEventreporte.setParams({
            title : 'Atencion',
            message:'Fecha de siniestro debe ser anterior a fin de vigencia',
            messageTemplate: 'Fecha de siniestro debe ser anterior a fin de vigencia',
            duration:' 2000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'});
		toastEventreporte.fire();
	}
	return continua;
},
})