({
	loadInfo: function (component, e, helper) {
        var ctx = component.find("myChartL").getElement();
        var action = component.get('c.fetchDataClass');
        var paramsLeadInfo = [component.find("pklTyOppBpyP").get("v.value"),component.find("pklOrigBpyP").get("v.value")];
        var paramsUserInfo = [component.get('v.seloff'),component.get('v.seldiv'), component.get('v.selbkm')];
        var paramsDateInfo = [component.get('v.sDate'), component.get('v.eDate')];
        var tipo;

        if(component.get('v.seldiv') === '' || component.get('v.seldiv').length < 2) {
            tipo = 'division';
        } else if(component.get('v.seloff') === '') {
            tipo = 'oficina';
        } else if(component.get('v.selbkm') === '') {
            tipo = 'banquero';
        } else {
            tipo = 'banquero';
        }

        action.setParams({
            paramsLead: paramsLeadInfo,
            paramsUser: paramsUserInfo,
            paramsDate: paramsDateInfo,
            expression: tipo
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
				helper.fecthNumRecord(component, e, res);
                var height = 100 + (res.lsLabels.toString().split(',').length * 30);

                component.set('v.chartHeight', height);
                if (window.myChartL != null) {
                    window.myChartL.clear();
                    window.myChartL.destroy();
                }
                window.myChartL = new Chart(ctx, { type: 'horizontalBar', data: {}, options: window.barOptions_stacked });
                this.actionHelper(component, res, tipo);
            }
        });
        $A.enqueueAction(action);
    },

    fecthNumRecord: function(component, e, dataLeads) {
        var numeroRecord = 0;
        if(dataLeads && dataLeads.lsData){
            for (const key in dataLeads.lsData) {
                numeroRecord += dataLeads.lsData[key];
            }
        }
        component.set('v.numLeads', numeroRecord);
    },


    combosSet: function (component, e) {
        var wrapperObj = component.get('v.wrapperObject');
        var divSelected = component.get('v.seldiv');
        var offSelected = component.get('v.seloff');
        component.set('v.ListOfOff', wrapperObj.mapSucursales[divSelected]);
        component.set('v.ListOfBkMn', wrapperObj.maplistUserName[divSelected+offSelected]);
    },


    initCombos: function (component, e, helper) {
         component.set("v.accessCombos" , {
            "BPyP Estandar" : {"div":false, "off":false, "bnk":false },
            "BPyP Director Oficina" : {"div":false, "off":false, "bnk":true },
            "BPyP Director Divisional" : {"div":false, "off":true, "bnk":true },
            "BPyP STAFF" : {"div":true, "off":true, "bnk":true },
            "Soporte BPyP" : {"div":true, "off":true, "bnk":true },
            "Administrador del sistema" : {"div":true, "off":true, "bnk":true }
        } );

        var fetchusdata = component.get('c.fetchusdata');
        fetchusdata.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var chartdrawn = false;
                var perfilC = String(response.getReturnValue()).split(',')[0];
                var divisionC = String(response.getReturnValue()).split(',')[1];
                var oficinaC = String(response.getReturnValue()).split(',')[2];
                var usrnameC = String(response.getReturnValue()).split(',')[3];
                var admC = $A.get("$Label.c.MX_PERFIL_SystemAdministrator");
                var SoportePerfil ="Soporte BPyP";
                var dwpPerfil ="BPyP STAFF";
                var dirdiv = "BPyP Director Divisional";
                var diroff = "BPyP Director Oficina";
                var bpypestandar = "BPyP Estandar";

                component.set("v.userPInfo", perfilC);
                component.set("v.accessBnk", !component.get("v.accessCombos")[perfilC].bnk);
                component.set("v.accessOff", !component.get("v.accessCombos")[perfilC].off);
                component.set("v.accessDiv", !component.get("v.accessCombos")[perfilC].div);
                if (perfilC === (dwpPerfil) || perfilC === (admC) || perfilC === (SoportePerfil)) {
                    component.set('v.seldiv', divisionC);
                    component.set('v.seloff', oficinaC);
                    this.combosSet(component, e);
                    this.loadInfo(component, e, helper);
                    chartdrawn = true;
                } else if (perfilC === dirdiv) {
                    component.set('v.seldiv', divisionC);
                    this.combosSet(component, e);
                    this.loadInfo(component, e, helper);
                    chartdrawn = true;
                } else if (perfilC === diroff) {
                    component.set('v.isOff', true);
                    component.set('v.seldiv', divisionC);
                    component.set('v.seloff', oficinaC);
                    this.combosSet(component, e);
                    this.loadInfo(component, e, helper);
                    chartdrawn = true;
                } else if (perfilC === bpypestandar) {
                    component.set('v.seldiv', divisionC);
                    component.set('v.seloff', oficinaC);
                    component.set('v.selbkm', usrnameC);
                    this.combosSet(component, e);
                    this.loadInfo(component, e, helper);
                    chartdrawn = true;
                }
                if (!chartdrawn){
                    this.loadInfo(component, e, helper);
                    this.combosSet(component, e);
                }
            }
        });
        $A.enqueueAction(fetchusdata);
    },

    fetchLeads: function(component, e, helper, offsetVal) {
        var paramsLead = [component.find("pklTyOppBpyP").get("v.value"),component.find("pklOrigBpyP").get("v.value")];
        var paramsUser = [component.get('v.seloff'), component.get('v.seldiv'), component.get('v.selbkm')];
        var paramsDate = [component.get('v.sDate'), component.get('v.eDate')];

        var gettotalRecords = component.get('c.infoLeadByClause');
        gettotalRecords.setParams({
            paramsLead: paramsLead,
            paramsUser: paramsUser,
            paramsDate: paramsDate
        });
        gettotalRecords.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                component.set('v.numLeads', response.getReturnValue().length);

                this.configPage(component, e);
            }
        });

        $A.enqueueAction(gettotalRecords);

        var infoLeadByClause = component.get('c.infoLeadByClause');
        infoLeadByClause.setParams({
            paramsLead: paramsLead,
            paramsUser: paramsUser,
            paramsDate: paramsDate,
            limite: offsetVal
        });

        infoLeadByClause.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if (component.get('v.selbkm') !== "") {
                    component.set('v.isOpp', true);
                }
                else {
                    component.set('v.isOpp', false);
                }
                component.set('v.ListOfLead', response.getReturnValue());
            }
        });
        $A.enqueueAction(infoLeadByClause);
    },

    configPage: function(component, e) {

        component.set('v.NumOfPag', 0);
        component.set('v.ActPag', 1);
        var fetchPgs = component.get('c.fetchPgs');
        var totalrecord = component.get('v.numLeads') ? component.get('v.numLeads') : 0;
        fetchPgs.setParams({
            numRecords : totalrecord
        });
        fetchPgs.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                component.set('v.NumOfPag', response.getReturnValue());
                component.set('v.ActPag', 1);
                window.sizeofAcc = 10;
            }
        });
        $A.enqueueAction(fetchPgs);
    },

   actionHelper: function (component, res, types) {
        var labelsAH = [];
        for (var i = res.lsLabels.toString().split(',').length - 1; i >= 0; i--) {
            myChartL.config.data.labels.push(res.lsLabels.toString().split(',')[i]);
            labelsAH.push(res.lsLabels.toString().split(',')[i]);
        }
        window.amm = [];
        for (var j = res.lsTyOpp.toString().split(',').length - 1; j >= 0; j--) {
            var datatemp = [];
            var dataamm = [];
            for (var k = 0; k < labelsAH.length; k++) {
                datatemp.push(res.lsData[(res.lsTyOpp.toString().split(',')[j] + labelsAH[k])] == null ? 0 : res.lsData[(res.lsTyOpp.toString().split(',')[j] + labelsAH[k])]);
            }
            window.amm.push(dataamm);

            var newDataset = { label: res.lsTyOpp.toString().split(',')[j], backgroundColor: res.lsColor.toString().split('),')[j] + ')', data: datatemp, }

            myChartL.data.datasets.push(newDataset);
        }
        myChartL.update();
        component.set('v.DateOfUpd', new Date().getDate() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getFullYear());
        jQuery('#wrapperOpp').html(window.dataToTable(myChartL.data, types.toUpperCase() ));
    },

    pagefunction: function (component, pgMov, condition) {
        var paramsLeadPag = [component.find("pklTyOppBpyP").get("v.value"),component.find("pklOrigBpyP").get("v.value")];
        var paramsUserPag = [component.get('v.seloff'), component.get('v.seldiv'), component.get('v.selbkm')];
        var paramsDatePag = [component.get('v.sDate'), component.get('v.eDate')];
        var infoLeadByClause = component.get('c.infoLeadByClause');
        infoLeadByClause.setParams({
            paramsLead: paramsLeadPag,
            paramsUser: paramsUserPag,
            paramsDate: paramsDatePag,
            limite: pgMov.toString()
        });
        infoLeadByClause.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if (component.get('v.selbkm') !== "") {
                    component.set('v.isOpp', true);
                }
                if(condition) {
                    pgMov = pgMov / window.sizeofAcc;
                    pgMov++;
                    component.set('v.ActPag', pgMov);
                } else {
                    component.set('v.ActPag', 1);
                }
                component.set('v.ListOfLead', response.getReturnValue());

            }
        });
        $A.enqueueAction(infoLeadByClause);
    }
})