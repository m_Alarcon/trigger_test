({
    doInit : function(component) {
        component.set('v.ListOfLead', null);
        component.set('v.ActPag', 1);
        component.set('v.ListOfTyOpp', [{'Status':'Abierto'}, {'Status':'Convertido'}]);
        component.set('v.ListOfOrigin', [{'LeadSource':'Carga central'}, {'LeadSource':'Preaprobados'}]);

        var getuserInfo = component.get('c.userInfo');
        getuserInfo.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnWrapper = response.getReturnValue();
                component.set('v.wrapperObject', returnWrapper);
                component.set('v.ListOfDiv', returnWrapper.setDivisiones);
            }
        });
        $A.enqueueAction(getuserInfo);

    },

    generateChart: function (component, e, helper) {
        window.dataToTable = function (dataset, title) {
            var htmlG = '<table>';
            htmlG += '<thead><tr><th >' + title + '</th>';
            var columnCount = 0;
            jQuery.each(dataset.datasets, function (idx, item) {
                htmlG += '<th style="background-color:' + item.fillColor + ';">' + item.label + '</th>';
                columnCount += 1;
            });
            htmlG += '</tr></thead>';
            jQuery.each(dataset.labels, function (idx, item) {
                htmlG += '<tr><td>' + item + '</td>';
                for (var i = 0; i < columnCount; i++) {
                    htmlG += '<td style="background-color:' + dataset.datasets[i].fillColor
                        + ';">' + (dataset.datasets[i].data[idx] === '0' ? '-' : dataset.datasets[i].data[idx]) + '</td>';
                }
                htmlG += '</tr>';
            });
            htmlG += '</tr><tbody></table>';
            return htmlG;
        };
        window.barOptions_stacked = {
            tooltips: {
                enabled: true,
                callbacks: {
                    afterLabel: function (tooltipItem, data) {
                        var tsG = [];
                        tsG = ([window.amm[tooltipItem.datasetIndex][tooltipItem.index]].toString()).split("ralm ");
                        return tsG;
                    },
                }
            },
            hover: {
                animationDuration: 10
            },
            legend: {
                position: 'bottom',
                padding: 10,
                onClick: function (event, legendItem) { },
            },
            animation: {
                onComplete: function () {
                    var chartInstanceG = this.chart;
                    var ctxG = chartInstanceG.ctx;
                    ctxG.textAlign = "left";
                    ctxG.fillStyle = "#fff";
                    Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstanceG.controller.getDatasetMeta(i);
                        Chart.helpers.each(meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            if (data !== 0) {
                                ctxG.fillText(data, bar._model.x - 20, bar._model.y - 15);
                            }
                        }), this)
                    }), this);
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                    stacked: true,
                    gridLines: { display: false },
                }],
                yAxes: [{
                    stacked: true,
                    gridLines: {
                        display: false
                    },
                }]
            },
        };
		component.set('v.IsSpinner',true);
        window.setTimeout(
            $A.getCallback(function() {
                helper.initCombos(component, e, helper);
                component.set('v.IsSpinner',false);
            }), 5000
        );
    },

    upInfo: function (component, e, helper) {
        if(component.get('v.seldiv') === '' || component.get('v.seldiv').length < 2){
            component.set('v.seloff','');
            component.set('v.selbkm','');
        }else if(component.get('v.seloff') === '' || component.get('v.seloff').length < 2){
            component.set('v.selbkm','');
        }
        helper.loadInfo(component, e, helper);
        helper.combosSet(component, e);
        component.set('v.isOpp', false);
        component.set('v.ListOfLead', null);
        if(component.get("v.selbkm").length > 2){
            component.set('v.isOpp', true);
            helper.fetchLeads(component, e, helper, 0);
        }
    },

    frst: function (component, e, helper) {
        helper.pagefunction(component, 0);
    },

    prev: function (component, e, helper) {
        var actPgprev = component.get('v.ActPag');
        var prv = actPgprev === 1 ? 1 : actPgprev - 1;
        prv--;
        prv = prv * window.sizeofAcc;
        helper.pagefunction(component, prv, true);
    },

    next: function (component, e, helper) {
        var actPg = component.get('v.ActPag');
        var totPg = component.get('v.NumOfPag');
        var nxt = actPg === totPg ? totPg : actPg + 1;
        nxt--;
        nxt = nxt * window.sizeofAcc;
        helper.pagefunction(component, nxt, true);
    },

    lst: function (component, e, helper) {
        var totPg = component.get('v.NumOfPag');
        totPg--;
        totPg = totPg * window.sizeofAcc;
        helper.pagefunction(component, totPg, true);
    }
})