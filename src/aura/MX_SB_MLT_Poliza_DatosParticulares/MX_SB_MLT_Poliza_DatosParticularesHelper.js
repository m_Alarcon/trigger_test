({  changeTabNamePar : function(component, event) {
       var workspaceAPIDataPart = component.find("workspace");
       workspaceAPIDataPart.getFocusedTabInfo().then(function(response) {
           workspaceAPIDataPart.isSubtab({
               tabId: response.tabId
               }).then(function(response) {
                   if (response) {
                       var focusedTabId = response.tabId;
                       workspaceAPIDataPart.setTabLabel({
                       tabId: focusedTabId,
                       label: "Datos Particulares",
                       });
                       workspaceAPIDataPart.setTabIcon({
                       tabId: focusedTabId,
                       icon: "action:description",
                       iconAlt:"Datos Particulares"
                       });
                   }
           });
       })
          .catch(function(error) {
       });
   },
   getDatosContrato: function(component,event){
       var action = component.get("c.getDatosContrato");
       action.setParams({"poliza":component.get("v.poliza")});
       action.setCallback(this, function(response) {
           component.set("v.contratoObject",response.getReturnValue());
       });
       $A.enqueueAction(action);
   },
   consultaContrato : function(component, event) {
       var action = component.get("c.obTableEquipamento");
       action.setCallback(this, function(response) {
           var state = response.getState();
           if (state === "SUCCESS") {
               component.set("v.infoContrato", response.getReturnValue());
           }
       });
       $A.enqueueAction(action);
   },
   datosParticularesPoliza : function(component, event) {
       var action = component.get("c.obDatosParticulares");
       action.setCallback(this, function(response) {
           var state = response.getState();
           if (state === "SUCCESS") {
               component.set("v.DatosContrato", response.getReturnValue());
           }
       });
       $A.enqueueAction(action);
   },
   datosParticularesPoliza2 : function(component, event) {
       var action = component.get("c.obDatosParticular");
       action.setCallback(this, function(response) {
           var state = response.getState();
           if (state === "SUCCESS") {
               component.set("v.DatosContrato2", response.getReturnValue());
           }
       });
       $A.enqueueAction(action);
   },
   showToast : function(component, event, helper) {
       var toastEvent = $A.get("e.force:showToast");
       let testVar = component.get("v.poliza");
       if (testVar === null) {
       toastEvent.setParams({
           title : 'Error',
           message:'No se han encontrado datos particulares',
           key: 'info_alt',
           type: 'error',
           mode: 'pester'
       });
       toastEvent.fire();
       }
       else {
             toastEvent.setParams({
               "title": "Exito",
               "message": "Se han encontrado datos particulares.",
                type: 'success'
           });
           toastEvent.fire();
           }
   }
})