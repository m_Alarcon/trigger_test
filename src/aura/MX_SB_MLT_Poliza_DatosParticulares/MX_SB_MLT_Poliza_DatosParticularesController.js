({
    doInit : function(component, event, helper) {
        helper.showToast(component, event);
		helper.changeTabNamePar(component, event);
		helper.getDatosContrato(component, event);
        helper.datosParticularesPoliza(component, event);
        helper.datosParticularesPoliza2(component, event);
        helper.consultaContrato(component, event);
	},
    toggleSection : function(component, event, helper) {
    var sectionAuraId = event.target.getAttribute("data-auraId");
    var sectionDiv = component.find(sectionAuraId).getElement();
    var sectionState = sectionDiv.getAttribute('class').search('slds-is-open');
    if(sectionState === -1){
        sectionDiv.setAttribute('class' , 'slds-section slds-is-open');
    }else{
        sectionDiv.setAttribute('class' , 'slds-section slds-is-close');
    }
},
})