({
    getInvitados:function(component) {
        var action = component.get("c.getInvitados");
        action.setParams({ precordId : component.get("v.recordId")});
        action.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
                component.set('v.ListInvitados', response.getReturnValue());
            } else {
                console.log('error ' + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },
    saveInvitados:function(component, event, helper) {
        var action = component.get("c.saveInvitado");
        action.setParams({ listInvitado : component.get("v.ListInvitados")});
        action.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
              if(response.getReturnValue() === true) {
                    helper.showToast({
                        "title": "Asistencia registrada",
                        "type": "success",
                        "message": " Los registros fueron actualizados correctamente.",
                        "mode": "sticky"});
                   helper.reloadDataTable();
                } else { //Si la actualización falla
                    helper.showToast({
                        "title": "Error!!",
                        "type": "error",
                        "message": "Error al actualizar registros."});
                }
            }
        });
        $A.enqueueAction(action);
    },
    /*
     * Muestra alertas
     * */
    showToast : function(params) {
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent) {
            toastEvent.setParams(params);
            toastEvent.fire();
        } else {
            alert(params.message);
        }
    },
    /*
     * recarga valores en datatable.
     * */
    reloadDataTable : function() {
    var refreshEvent = $A.get("e.force:refreshView");
        if(refreshEvent) {
            refreshEvent.fire();
        }
    }
})