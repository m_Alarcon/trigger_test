({

    doInit: function(component, event, helper) {
        helper.getInvitados(component, event);
    },
    Save: function(component, event, helper) {
            helper.saveInvitados(component, event, helper);
            component.set('v.vboton', true);
    },
    handleChange: function(component, event, helper) {
        component.set('v.vboton', false);
    },
    cancel: function(component, event, helper) {
        helper.getInvitados(component, event);
        $A.get('e.force:refreshView').fire();
        component.set('v.vboton', true);
    }
})