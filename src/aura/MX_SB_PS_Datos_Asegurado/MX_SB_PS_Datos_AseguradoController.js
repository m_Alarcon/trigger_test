({
    doInit : function(component, event, helper) {
        var jsonobj={
            "Calle":"",
            "NumExterior":"",
            "NumInterior":"",
            "PostalCode":"",
            "Colonia":"",
            "Municipio":"",
            "Estado":""}
        component.set("v.dirForm",jsonobj);
        helper.doInitH(component, event, helper);
        helper.getEstados(component,event,helper);
        component.set("v.rvalue","igual");
        helper.searchdirdata(component,event,helper);
    },
    changeView : function(component, event, helper) {
        var btnValue = event.getSource().get("v.value");
        if(btnValue==='Editar') {
            component.set('v.isEditModeOff',!component.get('v.isEditModeOff'));
            component.set('v.isEditModeOn',!component.get('v.isEditModeOn'));
        }
        if(btnValue==='Guardar') {
            helper.requiredFields(component,event,helper);
            if(component.get('v.allFRequiredQ')===true) {
                component.set('v.loaded', false);
                helper.saveContactData(component,event,helper);
            }
        }
        if(btnValue==='Cancelar') {
            component.set('v.isEditModeOff',!component.get('v.isEditModeOff'));
            component.set('v.isEditModeOn',!component.get('v.isEditModeOn'));
            helper.customtoast(component,event,helper,'Se han revertido los cambios','ERROR');
            helper.doInitH(component, event, helper);
        }
    },
    validacp : function(component,event,helper){
        helper.validacp(component,event,helper,'page');
    },
    validacpmod : function(component,event,helper){
        helper.validacp(component,event,helper,'modal');
    },
    cap1: function(component,event,helper) {
        var Fname = component.get('v.CntctObj.FirstName');
        component.set('v.CntctObj.FirstName',Fname.replace(/[^\ÑA-Za-zñ\s]/gi,'').toUpperCase());
        helper.calcRfc(component,event);
    },
    cap2: function(component,event,helper) {
        var LName = component.get('v.CntctObj.LastName');
        component.set('v.CntctObj.LastName',LName.replace(/[^\ÑA-Za-zñ\s]/gi,'').toUpperCase());
        helper.calcRfc(component,event);
    },
    cap3: function(component,event,helper) {
        helper.calcRfc(component,event);
    },
    cap4: function(component,event,helper) {
        var homoclv = component.get('v.accObj.MX_SB_SAC_Homoclave__c');
        component.set('v.accObj.MX_SB_SAC_Homoclave__c',homoclv.replace(/[^\w\s]/gi,'').toUpperCase());
    },
    cap5: function(component,event,helper) {
        var curp = component.get('v.CntctObj.MX_RTL_CURP__c');
        component.set('v.CntctObj.MX_RTL_CURP__c',curp.replace(/[^\w\s]/gi,'').toUpperCase());
    },
    cap6: function(component,event,helper) {
        var LNMat = component.get('v.CntctObj.Apellido_materno__c');
        component.set('v.CntctObj.Apellido_materno__c',LNMat.replace(/[^\ÑA-Za-zñ\s]/gi,'').toUpperCase());
        helper.calcRfc(component,event);
    },
    onChange : function(component, event, helper) {
        var e = document.getElementById("colonia");
        component.set('v.MX_WB_txt_Colonia',e.options[e.selectedIndex].innerHTML);
        component.set('v.accObj.Colonia__c',e.options[e.selectedIndex].innerHTML);
        component.set('v.coloniaDefVal',e.options[e.selectedIndex].value);
    },
    changecolmodal : function(component, event, helper) {
        var ev = document.getElementById("coloniamodal");
        var newdir2=component.get("v.dirForm");
        newdir2['Colonia']=ev.options[ev.selectedIndex].innerHTML;
        newdir2['Coloniaid']=ev.options[ev.selectedIndex].value;
        component.set('v.dirForm',newdir2);
    },
    radiochange: function(component,event,helper){
        switch (event.getSource().get("v.value")) {
            case 'fisico' :
                let msg = 'Al activár esta opción, los datos se guardarán para realizar el envío de la póliza y Kit de bienvenida por correo físico al domicilio del titular';
                helper.customtoast(component,event,helper,msg,'info');
                helper.searchdirdata(component,event,helper);
                break;
            case 'correo' :
                helper.showmodalof(component,event,helper);
                break;
            default :
                break;
        }
    },
    radiochangemod: function(component,event,helper){
        switch (event.getSource().get("v.value")) {
            case 'diferente' :
                helper.showmodal(component,event,helper);
                component.set("v.newdir",true);
                break;
            case 'igual' :
                component.set("v.newdir",false);
                helper.fillsameDir(component,event,helper);
                break;
            default :
                break;
        }
    },
    showmodalof : function(component, event, helper) {
        helper.searchdirdata(component,event,helper);
        helper.showmodalof(component,event,helper);
    },
    savecontact : function(component,event,helper){
        helper.requiredFieldsModal(component,event, helper);
    },
    siguiente : function(component, event, helper) {
        component.set('v.disabledUL',true);
        component.set('v.loaded', !component.get('v.loaded'));
        helper.createcustomerdata(component,event,helper);
        helper.createContract(component, event, helper);
    },
    consultacurp: function(component,event,helper){
        helper.getCurp(component,event,helper);
    },
    setEstado: function (component, event,helper) {
        helper.getCurp(component,event,helper);
    },
    soloFormalizar: function(component, event,helper) {
        var formalizar= component.get('c.selectmethod');
        var quoteId=component.get('v.quotesync.Id');
        formalizar.setParams({'mtname':'getQuoli','data':quoteId});
        formalizar.setCallback(this, function(respons) {
            if(respons.getState()==='SUCCESS') {
                var quoteLi = JSON.parse(respons.getReturnValue());
                component.set('v.quotelineI',quoteLi.quolis[0]);
                var idCotiza=component.get('v.quotelineI.MX_WB_Folio_Cotizacion__c');
                var goToFormalizar= component.get('c.selectmethod');
                goToFormalizar.setParams({'mtname':'formaliza','data':idCotiza});
                goToFormalizar.setCallback(this, function(resp) {
                    component.set('v.response',JSON.parse(resp.getReturnValue()));
                    if (component.get('v.response.cadena')==='"{}"') {
                        var cmpEvent =$A.get("e.c:MX_SB_PS_EventValidData");
                        cmpEvent.setParams({"continuar" : true, "nextStageName" : $A.get("$Label.c.MX_SB_PS_Etapa4")});
                        cmpEvent.fire();
                    } else if(component.get('v.response.cadena')==='ERROR') {
                        component.set('v.estadoCot',true);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title:'Error al ejecutar el servicio',
                            duration:' 2000',
                            message:'No fue posible formalizar la cotización',
                            key: 'info_alt',
                            type:'Error'
                        });
                        toastEvent.fire();
                    }
                });
                $A.enqueueAction(goToFormalizar);
            }
        });
        $A.enqueueAction(formalizar);
    },
})