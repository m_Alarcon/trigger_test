({
    doInitH: function(component,event,helper) {
        var action2= component.get('c.selectmethod');
        var recordId=component.get('v.recordId');
        action2.setParams({'mtname':'loadOppF','data':recordId});
        action2.setCallback(this, function(responseA) {
            if(responseA.getState()==='SUCCESS') {
                var oportrs =JSON.parse(responseA.getReturnValue());
                component.set("v.OppObj",oportrs.opport);
                var getBen= component.get('c.selectmethod');
                getBen.setParams({'mtname':'srchBenefs','data': oportrs.opport.SyncedQuoteId});
                getBen.setCallback(this, function(responseBen){
                    if (responseBen.getState()==='SUCCESS') {
                        var benes= JSON.parse(responseBen.getReturnValue());
                        component.set('v.benefObj', benes.benes);
                    }
                });
                $A.enqueueAction(getBen);
                var action = component.get('c.selectmethod');
                action.setParams({'mtname':'loadAccountF','data':oportrs.opport.AccountId});
                action.setCallback(this, function(responseB) {
                    var accounts = JSON.parse(responseB.getReturnValue());
                    component.set("v.accObj",accounts.accounts[0]);
                    component.set('v.RFC',accounts.accounts[0].RFC__c);
                    component.set('v.accObj.MX_SB_SAC_Homoclave__c',accounts.accounts[0].MX_SB_SAC_Homoclave__c);
                    component.set('v.MX_WB_txt_Colonia',accounts.accounts[0].Colonia__c );
                });
                $A.enqueueAction(action);
                var action3 = component.get('c.selectmethod');
                action3.setParams({'mtname':'loadContactAcc','data':oportrs.opport.AccountId});
                action3.setCallback(this, function(responseC) {
                    var contacts = JSON.parse(responseC.getReturnValue());
                    component.set('v.CntctObj',contacts.contacts[0]);
                    helper.obtieneCp(component,event,helper,'page');
                    this.replaceFields(component,event,helper,contacts);
                });
                $A.enqueueAction(action3);
                var action4 = component.get('c.selectmethod');
                action4.setParams({'mtname':'getContract','data':recordId});
                action4.setCallback(this, function(responseE) {
                    if(JSON.parse(responseE.getReturnValue())!==undefined) {
                        var getContract = JSON.parse(responseE.getReturnValue());
                        if(getContract.contracts[0]!==undefined) {
                            component.set("v.cntrctObj",getContract.contracts[0]);
                        }else {
                            component.set("v.cntrctObj.Id",undefined);
                        }
                    }
                });
                $A.enqueueAction(action4);
            }
        });
        $A.enqueueAction(action2);
    },
    replaceFields: function(component,event,helper,contacts) {
        if(contacts.contacts[0].MX_WB_ph_Telefono1__c===undefined) {
            component.set('v.CntctObj.MX_WB_ph_Telefono1__c',component.get('v.accObj.PersonHomePhone'));
            if(component.get('v.accObj.PersonHomePhone')===undefined) {
                component.set('v.CntctObj.MX_WB_ph_Telefono1__c',component.get('v.accObj.PersonOtherPhone'));
            }
        }
        if (contacts.contacts[0].MX_WB_ph_Telefono2__c===undefined) {
            component.set('v.CntctObj.MX_WB_ph_Telefono2__c',component.get('v.accObj.Phone'));
        }
        if (contacts.contacts[0].Email===undefined) {
            component.set('v.CntctObj.Email',component.get('v.accObj.Correo_Electronico__c'));
        }
        if(contacts.contacts[0].Apellido_materno__c===undefined) {
            component.set('v.CntctObj.Apellido_materno__c',component.get('v.accObj.ApellidoMaterno__c'));
        }
        if(contacts.contacts[0].MX_RTE_Codigo_Lugar__c!==undefined) {
            component.set('v.coloniaDefVal',contacts.contacts[0].MX_RTE_Codigo_Lugar__c);
            component.set('v.coloniaDef',component.get('v.MX_WB_txt_Colonia'));
        }
        if(contacts.contacts[0].MailingStreet!==undefined){
            var calle=contacts.contacts[0].MailingStreet;
            var array = [];
            if(calle.includes('@@')) {
            	array = calle.split('@@');
            } else {
 	    	array = calle.split(',');
            }
            component.set('v.Calle',array[0]);
            component.set('v.NumExterior',array[1]);
            component.set('v.NumInterior',array[2]);
        }
    },
    saveContactData : function (component, event,helper) {
        var calle= component.get('v.Calle');
        var ext = component.get('v.NumExterior');
        var int = component.get('v.NumInterior');
        var cei = calle+','+ext+','+int;
        var cntc=component.get('v.CntctObj');
        component.set('v.CntctObj.MailingStreet',cei);
        component.set('v.CntctObj.MX_RTE_Codigo_Lugar__c',component.get('v.coloniaDefVal'));
        var action= component.get('c.supsert');
        action.setParams({'tipo':'contacto','objct':JSON.stringify(cntc)});
        action.setCallback(this, function(responseD) {
            component.set("v.modaldir",false);
        });
        $A.enqueueAction(action);
        component.set('v.accObj.FirstName',component.get('v.CntctObj.FirstName'));
        var accObj = component.get('v.accObj');
        var varac = JSON.parse(JSON.stringify(accObj));
        varac['RFC__c']=component.get('v.RFC');
        var contactoz =  component.get("v.CntctObj");
        varac['MX_RTL_CURP__pc']=contactoz['MX_RTL_CURP__c'];
        var action2 = component.get('c.supsert');
        action2.setParams({'tipo':'cuenta','objct':JSON.stringify(varac)});
        action2.setCallback(this, function(responseAcc) {
        });
        $A.enqueueAction(action2);
        var actQuoteData = component.get('c.actQuoteData');
        actQuoteData.setParams({'idoportunidad':component.get("v.recordId")});
        actQuoteData.setCallback(this,function(rsact){
            if(rsact.getState()==='SUCCESS') {
                component.set('v.loaded', !component.get('v.loaded'));
                if(rsact.getReturnValue()){
                    component.set('v.isEditModeOff',false);
                    component.set('v.isEditModeOn',true);
                    this.toastSuccess(component,event);
                } else {
                    this.customtoast(component,event,helper,'Error al actualizar informacion, por favor intente nuevamente','ERROR');
                    component.set('v.isEditModeOff',true);
                    component.set('v.isEditModeOn',false);
                }
            } else {
                component.set('v.loaded', !component.get('v.loaded'));
                component.set('v.isEditModeOff',true);
                component.set('v.isEditModeOn',false);
                this.customtoast(component,event,helper,'Error al actualizar informacion, por favor intente nuevamente.','ERROR');
            }
        });
        $A.enqueueAction(actQuoteData);
    },
    saveContractData : function (component,event) {
        var calle= component.get('v.Calle');
        var ext = component.get('v.NumExterior');
        var int = component.get('v.NumInterior');
        var cei = calle+','+ext+','+int;
        component.set('v.cntrctObj.BillingStreet',cei);
        var cntrct = component.get('v.cntrctObj');
        var action2= component.get('c.supsert');
        action2.setParams({'tipo':'contrato','objct':JSON.stringify(cntrct)});
        action2.setCallback(this, function(responseF) {
        });
        $A.enqueueAction(action2);
    },
    validacp : function(cmp,evt,help,from) {
        var contact =  evt.getSource().get("v.value");
        if(contact.length===5){
            this.validakey(cmp,evt,help,from);
        }
    },
    obtieneCp : function(cmp, evt, help, from) {
        var cpContact = cmp.get('v.CntctObj.MailingPostalCode');
        if(cpContact!== undefined) {
            this.validakey(cmp,evt,help,from);
        }
    },
    toastSuccess: function(component,event,helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message:'Se han guardado los datos exitosamente.',
            duration:' 2000',
            key: 'info_alt',
            type: 'success'
        });
        toastEvent.fire();
    },
    validakey : function(cmp,evt,help,from){
        var srvcol= cmp.get("c.getcolonias");
        var newdir = cmp.get("v.dirForm");
        var contacto =  cmp.get("v.CntctObj");
        var zipc;
        if(from==='modal') {
            zipc=newdir['PostalCode'];
        } else if(from==='page'){
            zipc =contacto['MailingPostalCode'];
        }
        srvcol.setParams({"zipcode":zipc});
        srvcol.setCallback(this,function(res){
            if(res.getState()==='SUCCESS') {
                switch (from) {
                    case 'page':
                        var mapa1 = JSON.parse(res.getReturnValue());
                        cmp.set("v.mapcol",mapa1);
                        contacto['MailingCity']=mapa1.iCatalogItem.suburb[0].county.name;
                        contacto['MailingState']=mapa1.iCatalogItem.suburb[0].state.name;
                        cmp.set("v.CntctObj",contacto);
                        break;
                    case 'modal':
                        var mapatemp = JSON.parse(res.getReturnValue());
                        cmp.set("v.mapcolmodal",mapatemp);
                        newdir['Municipio']=mapatemp.iCatalogItem.suburb[0].county.name;
                        newdir['Estado']=mapatemp.iCatalogItem.suburb[0].state.name;
                        cmp.set("v.dirForm",newdir);
                        break;
                }
            } else {
                this.customtoast(cmp,evt,help,'Error de conexion','ERROR AL CONSULTAR EL CP');
            }
        });
        $A.enqueueAction(srvcol);
    },
    showmodal : function(component, event, helper) {
        component.set("v.modaldir",true);
        component.set("v.rvalue","igual");
        this.searchdirdata(component,event,helper);
    },
    showmodalof : function(component, event, helper) {
        component.set("v.modaldir",false);
        component.set("v.rvalue","igual");
    },
    saveNewDirData: function(component, event, helper) {
        var savedata = component.get("c.saveDirData");
        var ndir = component.get("v.dirForm");
        var newdirquote = {BillingStreet:ndir['Calle']+','+ndir['NumExterior']+','+ndir['NumInterior'],
                           BillingCity:ndir['Municipio'],
                           BillingState:ndir['Estado'],
                           BillingPostalCode:ndir['PostalCode'],
                           MX_SB_VTS_Colonia__c:ndir['Colonia']+','+ndir['Coloniaid'],
                           OpportunityId:component.get("v.recordId")};
        savedata.setParams({newdir:newdirquote});
        savedata.setCallback(this,function (respons){
            if( respons.getState()==='SUCCESS'){
                this.customtoast(component, event, helper,'Haz guardado la dirección de envío de la póliza.','succes');
            }
        });
        $A.enqueueAction(savedata);
    },
    customtoast : function (compoonent, event,helper,msg,tipo) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message:msg,
            duration:' 2000',
            key: 'info_alt',
            type: tipo
        });
        toastEvent.fire();
    },
    createContract : function (component,event,helper) {
        var action = component.get("c.contractParam");
        var now_date = new Date();
        var Ide =component.get('v.cntrctObj.Id');
        var tomorrow=now_date.setDate(now_date.getDate() + 1);
        var tomorrow_Date= new Date(tomorrow).toISOString().split('T')[0];
        var tomorrow_DateVa = $A.localizationService.formatDate(tomorrow_Date, "dd/MM/yyyy");
        var tomorrow_DateVar = tomorrow_DateVa.toLocaleString('en-US', { timeZone: 'America/mexico_city' });
        action.setParams(
            {
                mapa:{
                    StartDate : tomorrow_DateVar,
                    BillingCity : component.get('v.accObj.BillingCity'),
                    BillingCountry : component.get('v.accObj.BillingCountry'),
                    BillingPostalCode : component.get('v.accObj.BillingPostalCode'),
                    BillingState : component.get('v.accObj.BillingState'),
                    BillingStreet : component.get('v.accObj.BillingStreet'),
                    AccountId : component.get('v.OppObj.AccountId'),
                    NumeroPoliza : 'OTN-12030430544',
                    oportunidad : component.get('v.OppObj.Id'),
                    Status : 'Draft',
                    origen : component.get('v.OppObj.LeadSource')
                },
                productName : 'Respaldo Seguro Para Hospitalización',
                ide : Ide
            }
        );
        action.setCallback(this, function(response) {
        });
        $A.enqueueAction(action);
    },
    searchdirdata: function(component,event,helper) {
        var getdirquote= component.get('c.selectmethod');
        var recordId=component.get('v.recordId');
        getdirquote.setParams({'mtname':'getDirData','data':recordId});
        getdirquote.setCallback(this,function(respons) {
            if(respons.getState()==='SUCCESS') {
                var getDirData = JSON.parse(respons.getReturnValue());
                this.dirdataFunction(component,event,helper,getDirData);
            }
        });
        $A.enqueueAction(getdirquote);
    },
    dirdataFunction: function(component,event,helper,getDirData) {
        component.set("v.quotesync",getDirData.quotes[0]);
        if(getDirData.quotes[0].Status==='Creada') {
            component.set('v.estadoCot',false);
        }else if(getDirData.quotes[0].Status==='Cotizada') {
            component.set('v.estadoCot',false);
        } else if(getDirData.quotes[0].Status==='Tarificada') {
            component.set('v.estadoCot',true);
        } else if(getDirData.quotes[0].Status==='Formalizada') {
            component.set('v.estadoCot',true);
        }
        if(getDirData.quotes[0].BillingStreet!==undefined){
            var adrs = getDirData.quotes[0].BillingStreet;
            var adrsdata = adrs.split(',');
            var dfdt = component.get("v.dirForm");
            dfdt['Calle']=adrsdata[0];
            dfdt['NumExterior']=adrsdata[1];
            dfdt['NumInterior']=adrsdata[2];
            dfdt['PostalCode']=getDirData.quotes[0].BillingPostalCode;
            dfdt['Colonia']=getDirData.quotes[0].MX_SB_VTS_Colonia__c.split(',')[0];
            dfdt['Municipio']=getDirData.quotes[0].BillingCity;
            dfdt['Estado']=getDirData.quotes[0].BillingState;
            component.set("v.dirForm",dfdt);
            component.set("v.newdir",true);
            component.set("v.rbvalue","fisico");
            component.set("v.rvalue","diferente");
        } else {
            var reverseDir = component.get("v.dirForm");
            reverseDir['Calle']='';
            reverseDir['NumExterior']='';
            reverseDir['NumInterior']='';
            reverseDir['PostalCode']='';
            reverseDir['Colonia']='';
            reverseDir['Municipio']='';
            reverseDir['Estado']='';
            component.set("v.dirForm",reverseDir);
            component.set("v.rvalue","igual");
        }
    },
    createcustomerdata: function(component,event,helper) {
    	if(component.get('v.rbvalue')==='correo') {
           component.set('v.emailQ','EMAIL');
        } else if(component.get('v.rbvalue')==='fisico') {
           component.set('v.emailQ','');
        }
        var createcd = component.get("c.createCustomData");
        var cntcobj = JSON.parse(JSON.stringify(component.get('v.CntctObj')));
        var cntcvar ={
            'Email':cntcobj.Email,
            'MailingPostalCode':cntcobj.MailingPostalCode,
            'MX_WB_ph_Telefono1__c':cntcobj.MX_WB_ph_Telefono1__c,
            'FirstName':cntcobj.FirstName,
            'LastName':cntcobj.LastName,
            'Birthdate':cntcobj.Birthdate,
            'Apellido_materno__c':cntcobj.Apellido_materno__c,
            'MX_RTL_CURP__c':cntcobj.MX_RTL_CURP__c
        };
        createcd.setParams({
            'oprt':component.get('v.recordId'),
            'cntc':cntcvar,
            'mapdata':{
                'rfc':component.get('v.RFC'),
                'calle':component.get('v.Calle'),
                'NumExterior':component.get('v.NumExterior'),
                'NumInterior':component.get('v.NumInterior'),
                'MX_WB_txt_Colonia':component.get('v.MX_WB_txt_Colonia'),
                'coloniaDefVal':component.get('v.coloniaDefVal'),
                'emailQ': component.get('v.emailQ')
            }
        });
        createcd.setCallback(this,function (rs){
            if (rs.getState()==='SUCCESS') {
                this.creaBeneficiarios(component,rs.getReturnValue(),'regular');
            }else{
                component.set('v.loaded', true);
                component.set('v.disabledUL',false);
                let errorMessage= rs.getState()+'. '+$A.get("$Label.c.MX_SB_PS_ErrorCreateCustomerData");
                var toastEvent1 = $A.get("e.force:showToast");
                toastEvent1.setParams({
                    message:errorMessage,
                    duration:' 2000',
                    key: 'info_alt',
                    type: rs.getState()
                });
                toastEvent1.fire();
            }
        });
        $A.enqueueAction(createcd);
    },
    getEstados: function(component,event,helper){
        var action = component.get('c.gEstados');
        var items;
        var response;
        var collectionData = [];
        action.setCallback(this,function (rs){
            var state = rs.getState();
            if (state === 'SUCCESS') {
                response = rs.getReturnValue();
                items = JSON.parse(response);
                var estado = items.data;
                for (var i in estado) {
                    var itemData = {
                        "label":  estado[i].name,
                        "value":  estado[i].id
                    };
                    collectionData.push(itemData);
                }
                component.set('v.picklistValues', collectionData);
            }
        });
        $A.enqueueAction(action);
    },
    getCurp: function(component,event,helper){
        var cntcobj = JSON.parse(JSON.stringify(component.get('v.CntctObj')));
        var cntcvar ={
            'Email':cntcobj.Email,
            'MailingPostalCode':cntcobj.MailingPostalCode,
            'MX_WB_ph_Telefono1__c':cntcobj.MX_WB_ph_Telefono1__c,
            'FirstName':cntcobj.FirstName,
            'LastName':cntcobj.LastName,
            'Birthdate':cntcobj.Birthdate,
            'Apellido_materno__c':cntcobj.Apellido_materno__c,
            'MX_RTL_CURP__c':cntcobj.MX_RTL_CURP__c
        };
        var contactinfo =component.get("v.CntctObj");
        var getc = component.get("c.getCurp");
        getc.setParams({'cntc':cntcvar,
                        "estadonac":event.getParam("value"),
                        "genero":cntcobj.Account.Genero__c});
        getc.setCallback(this,function(resp){
            if (resp.getReturnValue()==null){
                this.customtoast(component,event,helper,'Error al consultar curp \n Intente nuevamente. ','ERROR');
            } else {
                var rsdatamap =JSON.parse(resp.getReturnValue());
                switch (rsdatamap.code) {
                    case '204':
                        this.customtoast(component,event,helper,'No se encontraron datos\nValidar información. ','info');
                        break;
                    case '200':
                        var rsdata = JSON.parse(rsdatamap.body);
                        contactinfo['MX_RTL_CURP__c']=rsdata.listPersons[0].identityDocument[0].number;
                        component.set("v.CntctObj",contactinfo);
                        break;
                    default :
                        this.customtoast(component,event,helper,'Error al consultar curp \n Intente nuevamente. ','ERROR');
                        break;
                }
            }
        });
        $A.enqueueAction(getc);
    },
    requiredFields: function(component,event, helper) {
        let validFields = component.find("validData");
        if(validFields.length!==undefined) {
            var allValid = validFields.reduce(function (validSoFar, inputCmp) {
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid;
            }, true);
            if (!allValid) {
                component.set('v.allFRequiredQ',false);
                this.customtoast(component,event,helper,'Porfavor llene los campos obligatorios','ERROR');
            } else {
                component.set('v.allFRequiredQ',true);
            }
        }
    },
    requiredFieldsModal: function(component,event, helper) {
        let validFields = component.find("validData2");
        if(validFields.length!==undefined) {
            var allValid = validFields.reduce(function (validSoFar, inputCmp) {
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid;
            }, true);
            if (!allValid) {
                component.set('v.allFRequiredQModal',false);
                this.customtoast(component,event,helper,'Porfavor llene los campos obligatorios','ERROR');
            } else {
                component.set('v.allFRequiredQModal',true);
                helper.saveNewDirData(component,event,helper);
                component.set("v.newdir",true);
                component.set("v.modaldir",false);
            }
        }
    },
    creaBeneficiarios : function (component,listmap,tipo) {
        if(tipo==='regular') {
            var getc = component.get("c.createBens");
            component.set('v.loaded', !component.get('v.loaded'));
            getc.setParams({'mapas':listmap,'opotunidad':component.get("v.recordId"),'tipo':'regular'});
            getc.setCallback(this,function(resp){
                if (resp.getReturnValue()==='FRM'){
                    var cmpEvent =$A.get("e.c:MX_SB_PS_EventValidData");
                    cmpEvent.setParams({"continuar" : true, "nextStageName" : $A.get("$Label.c.MX_SB_PS_Etapa4")});
                    cmpEvent.fire();
                } else if(resp.getReturnValue()==='CRBEN') {
                    component.set('v.estadoCot',true);
                    var toastEvent3 = $A.get("e.force:showToast");
                    toastEvent3.setParams({
                        title:'Error al ejecutar el servicio',
                        message:'No fue posible formalizar la cotización',
                        duration:' 2000',
                        key: 'info_alt',
                        type:'Error'
                    });
                    toastEvent3.fire();
                }
                    else if(resp.getReturnValue()==='VAL') {
                        component.set('v.estadoCot',false);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title:'Error al ejecutar el servicio',
                            message:'Se ha generado un error al crear los beneficiarios/Asegurados, favor de intentar nuevamente',
                            duration:' 2000',
                            key: 'info_alt',
                            type:'Error'
                        });
                        toastEvent.fire();
                    }
            });
            $A.enqueueAction(getc);
        }
    },
    calcRfc: function (component,event) {
        let RFC;
        let ap_f;
        let am_f;
        let nam_f;
        let ap= component.get('v.CntctObj.LastName');
        let am =component.get('v.CntctObj.Apellido_materno__c');
        let nam= component.get('v.CntctObj.FirstName');
        let fechNa=component.get('v.CntctObj.Birthdate');

        if(nam && ap && am && fechNa) {
            fechNa = fechNa.substring(2,4)+fechNa.substring(5,7)+fechNa.substring(8,10);
            ap_f = this.filtraAcenRFC(component, ap.toLowerCase());
            am_f = this.filtraAcenRFC(component, am.toLowerCase());
            nam_f = this.filtraAcenRFC(component, nam.toLowerCase());

            ap_f = this.filtraNomRFC(component, ap_f);
            am_f = this.filtraNomRFC(component, am_f);
            nam_f = this.filtraNomRFC(component, nam_f);

            if (ap_f.length > 0 && am_f.length > 0) {
                if (ap_f.length < 3) {
                    RFC = this.apellidoCorRFC(component,ap_f, am_f, nam_f);
                } else {
                    RFC = this.organizaRFC(component,ap_f, am_f, nam_f);
                }
            }

            if (ap_f.length === 0 && am_f.length > 0) {
                RFC = this.unApeRFC(component, am_f, nam_f);
            }
            if (ap_f.length > 0 && am_f.length === 0) {
                RFC = this.unApeRFC(component, ap_f, nam_f);
            }

            RFC = this.quitaPalabrasRFC(component,RFC);
            RFC = RFC + fechNa;
            component.set('v.RFC',RFC);
        }
    },
    filtraAcenRFC: function (component, datoPersonal) {
        datoPersonal = datoPersonal.replace('á', 'a');
        datoPersonal = datoPersonal.replace('é', 'e');
        datoPersonal = datoPersonal.replace('í', 'i');
        datoPersonal = datoPersonal.replace('ó', 'o');
        datoPersonal = datoPersonal.replace('ú', 'u');
        return datoPersonal;
    },
    filtraNomRFC: function (component, datoPersonal) {
        let i = 0;
        let strArPalabras = [".", ",", "de ", "del ", "la ", "los ", "las ", "y ", "mc ", "mac ", "von ", "van "];
        for (i; i <= strArPalabras.length; i++) {
            datoPersonal = datoPersonal.replace(strArPalabras[i], "");
        }
        strArPalabras = ["jose ", "maria ", "j ", "ma "];
        for (i=0; i <= strArPalabras.length; i++) {
            datoPersonal = datoPersonal.replace(strArPalabras[i], "");
        }
        switch (datoPersonal.substr(0, 2)) {
            case 'ch':
                datoPersonal = datoPersonal.replace('ch', 'c')
                break;
            case 'll':
                datoPersonal = datoPersonal.replace('ll', 'l')
                break;
        }
        return datoPersonal
    },
    apellidoCorRFC: function (component,ap_paterno, ap_materno, nombre) {
        return ap_paterno.substr(0, 1) + ap_materno.substr(0, 1) + nombre.substr(0, 2);
    },
    organizaRFC: function (component,ap_paterno, ap_materno, nombre) {
        let strVocales = 'aeiou';
        let strPrimeraVocal = '';
        let i = 0;
        let x = 0;
        let y = 0;
        for (i = 1; i <= ap_paterno.length; i++) {
            if (y == 0) {
                for (x = 0; x <= strVocales.length; x++) {
                    if (ap_paterno.substr(i, 1) === strVocales.substr(x, 1)) {
                        y = 1;
                        strPrimeraVocal = ap_paterno.substr(i, 1);
                    }
                }
            }
        }
        return ap_paterno.substr(0, 1) + strPrimeraVocal + ap_materno.substr(0, 1) + nombre.substr(0, 1);
    },
    unApeRFC: function (component,apellido, nombre) {
        return apellido.substr(0, 2) + nombre.substr(0, 2);
    },
    quitaPalabrasRFC: function (component,rfc) {
        let res;
        rfc = rfc.toUpperCase();
        let strPalabras = "BUEI*BUEY*CACA*CACO*CAGA*CAGO*CAKA*CAKO*COGE*COJA*";
        strPalabras = strPalabras + "KOGE*KOJO*KAKA*KULO*MAME*MAMO*MEAR*";
        strPalabras = strPalabras + "MEAS*MEON*MION*COJE*COJI*COJO*CULO*";
        strPalabras = strPalabras + "FETO*GUEY*JOTO*KACA*KACO*KAGA*KAGO*";
        strPalabras = strPalabras + "MOCO*MULA*PEDA*PEDO*PENE*PUTA*PUTO*";
        strPalabras = strPalabras + "QULO*RATA*RUIN*";
        res = strPalabras.match(rfc);
        if (res != null) {
            rfc = rfc.substr(0, 3) + 'X';
            return rfc;
        } else {
            return rfc;
        }
    },
    fillsameDir: function(component, event, helper) {
        var fsdir = component.get("v.dirForm");
        fsdir['Calle']=component.get('v.Calle');
        fsdir['NumExterior']=component.get('v.NumExterior');
        fsdir['NumInterior']=component.get('v.NumInterior');
        fsdir['PostalCode']=component.get('v.CntctObj.MailingPostalCode');
        fsdir['Colonia']=component.get('v.MX_WB_txt_Colonia');
        fsdir['Municipio']=component.get('v.CntctObj.MailingCity');
        fsdir['Estado']=component.get('v.CntctObj.MailingState');
        component.set("v.dirForm",fsdir);
    },
})