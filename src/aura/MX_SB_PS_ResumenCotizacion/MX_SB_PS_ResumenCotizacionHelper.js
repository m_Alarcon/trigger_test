({
	quoteLineItem: function(component, event, helper){
		var action4= component.get('c.loadQuoteLineItem');
        action4.setParams({'value': component.get('v.OppObj.SyncedQuoteId')});
        action4.setCallback(this, function(responseD) {
        	component.set('v.quoteLineItem', responseD.getReturnValue()[0]);
			if(responseD.getReturnValue().length>0) {
				this.splitfolio(component);
			}
        });
	    $A.enqueueAction(action4);
	},
	splitfolio : function(cmp){
		var dataid2 =JSON.parse(JSON.stringify(cmp.get('v.quoteLineItem'))).MX_WB_Folio_Cotizacion__c;
		cmp.set('v.foliocot',dataid2);
	},
	quoteManual: function (component, event, helper) {
		var trem = component.get('v.psubse');
		component.set('v.quoteLineItem.Quote.MX_SB_PS_PagosSubsecuentes__c', trem);
	}
})