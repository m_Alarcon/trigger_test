({
	initQuoteLI : function(component, event, helper) {
		var visible = component.get('v.visible');
		if(visible)
			helper.quoteLineItem(component, event, helper);
		else
			helper.quoteManual(component,event,helper);
	},
})