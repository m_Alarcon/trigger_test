({
    helperCreateNewTask: function (component, event, helper) {
        var newTask = component.get("v.newTask");
        var action = component.get("c.saveTask");
        let isSegmented = component.get("v.isSegmented");
        var leadId = component.get("v.recordObject");
        action.setParams({
            "myTask": newTask,
            "segmented": isSegmented,
            "leadId": leadId.Id,
            "getSource3": leadId.RecordType.Name
        });
        action.setCallback(this, function (response) {
            var toastEvent = $A.get("e.force:showToast");
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            var state = response.getState();
            var urlEvent = $A.get("e.force:navigateToURL");
            var getSource = component.get('v.recordObject');
            var getOriginSac = getSource.LeadSource;
            var getRecordType = getSource.RecordType.Name;
            var getProdLead = getSource.Producto_Interes__c;
            var getProdOpp = getSource.Producto__c;
            var currentRecordId = getSource.Id;
            var recordPrefijo = currentRecordId.substring(0, 3);
            var workspaceAPI = component.find("workspace");
            var proSacLabel = $A.get("$Label.c.MX_SB_VTS_SeguroEstudia_LBL");
            var oriSacLabel = $A.get("$Label.c.MX_SB_VTS_VENTA");

            component.set("v.HideSpinner", false);
            if (component.isValid() && state === "SUCCESS" && getOriginSac !== 'SAC' && getProdLead !== 'Seguro Estudia' && recordPrefijo === '00Q') {
                urlEvent.setParams({
                    "url": "/lightning/page/home"
                });
                urlEvent.fire();
                helper.toastEventCustom(component, event, helper, "Success", $A.get("{!$Label.c.MX_SB_VTS_reagendaSuccess}"), "Exito!");
                dismissActionPanel.fire();
            } else if ( recordPrefijo === '006' && state === "SUCCESS" && component.isValid() && getProdOpp !== proSacLabel && getOriginSac !== oriSacLabel) {
                dismissActionPanel.fire();
                urlEvent.setParams({
                    "url": "/lightning/page/home"
                });
                urlEvent.fire();
                helper.toastEventCustom(component, event, helper, "Success", $A.get("{!$Label.c.MX_SB_VTS_reagendaSuccess}"), "Funcionó!");
            } else if (component.isValid() && state === "SUCCESS" && (getOriginSac === 'SAC' || getOriginSac === 'Call me back')) {
                if (getRecordType === 'Venta Asistida Digital' || getRecordType === 'ASD' || getRecordType === 'Ventas Telemarketing') {
                    workspaceAPI.isConsoleNavigation().then(function (response) {
                        if (response === true) {
                            var workspace = component.find("workspace");
                            workspace.getFocusedTabInfo().then(function (response) {
                                var focusedTabId = response.tabId;
                                workspace.closeTab({
                                    tabId: focusedTabId
                                });
                            })
                                .catch(function (error) {
                                    console.log("Error al querer cerrar la tab", error);
                                });
                        }
                    })
                    helper.toastEventCustom(component, event, helper, "Success", $A.get("{!$Label.c.MX_SB_VTS_reagendaSuccess}"), "Exito!");
                    dismissActionPanel.fire();
                    toastEvent.fire();
                }
            } else if (state === "ERROR") {
                toastEvent.setParams({
                    "title": "Error",
                    "message": response.getError()[0],
                    "duration": "1000",
                    "type": "error"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    helperGetUsr: function (component) {
        var leadId = component.get("v.recordId");
        let getU = component.get("c.getUsers");
        getU.setParams({
            "leadId": leadId
        });
        getU.setCallback(this, function (response) {
            let result = response.getReturnValue();
            component.set("v.manager", result);
            component.set("v.taskSubject", "Reagenda");
            component.set("v.isSegmented", result[1].MX_SB_VTS_TieneSegmento__c);
            component.set("v.isDisabled", true);
        });
        $A.enqueueAction(getU);
    },
    helperHandleChange: function (component) {
        let task = component.get("v.newTask");
        task.Subject = component.get("v.taskSubject");
        task.Description = component.get("v.taskDescription");
        task.FechaHoraReagenda__c = component.get("v.taskActivityDate");
        task.ReminderDateTime = component.get("v.taskActivityDate");
        task.OwnerId = component.get("v.manager[0].ManagerId");
        task.MX_WB_telefonoUltimoContactoCTI__c = component.get("v.mobilePhone");
        let proStarHrLV = component.get("v.manager[1].MX_SB_VTS_BusinessHoursL_V__c");
        let proStarConv = proStarHrLV / (60 * 60 * 1000) % 60;
        let proEndHrLV = component.get("v.manager[1].MX_SB_VTS_FinishBusinessHoursL_V__c");
        let proEndConv = proEndHrLV / (60 * 60 * 1000) % 60;
        let proStarHrSa = component.get("v.manager[1].MX_SB_VTS_BusinessHoursL_S__c");
        let proStarConvSa = proStarHrSa / (60 * 60 * 1000) % 60;
        let proEndHrSa = component.get("v.manager[1].MX_SB_VTS_FinalBusinessHoursS__c");
        let proEndConSa = proEndHrSa / (60 * 60 * 1000) % 60;
        var getDate = new Date(task.FechaHoraReagenda__c);
        var saturday = getDate.getDay(6);
        var time = new Date(task.FechaHoraReagenda__c);
        let currentDate = component.get("v.manager[2]");
        let sMsg = "Lo sentimos, el horario de atención  es de  lunes a viernes, de 9:00 a 19:59 hrs \n y sábados de 9:00 a 14:59 hrs.\n";
        sMsg += " Por favor, selecciona un horario válido.";
        let toastEvent = $A.get("e.force:showToast");

        toastEvent.setParams({
            "title": "Atención!",
            "message": sMsg,
            "duration": "3000",
            "type": "warning"
        });
        if (task.FechaHoraReagenda__c < currentDate) {
            component.set("v.isDisabled", true);
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Atención!",
                "message": "La fecha y hora no pueden ser menor al día actual.",
                "duration": "2000",
                "type": "warning"
            });
            toastEvent.fire();
        } else if (6 != saturday && (time.getHours() < proStarConv || time.getHours() >= proEndConv) ||
            (6 === saturday && (time.getHours() < proStarConvSa || time.getHours() >= proEndConSa))) {
            component.set("v.isDisabled", true);
            toastEvent.fire();
        } else {
            component.set("v.isDisabled", false);
        }
        component.set("v.newTask", task);
    },
    helperAfterUpdate: function (component) {
        var recordVal = component.get('v.recordObject');
        var recordId = recordVal.Id;
        var recordPrefix = recordId.substring(0, 3);
        var getLeadBool = recordVal.MX_SB_VTS_IsReagenda__c;
        var getOppBool = recordVal.MX_SB_VTS_IsOppReagenda__c;
        var mobStr = '';
        if (recordPrefix === '00Q') {
            mobStr = recordVal.MobilePhone;
            component.set('v.isLead', true);
        } else if (recordPrefix === '006') {
            mobStr = recordVal.TelefonoCliente__c;
            component.set('v.isLead', false);
        }
        if (getLeadBool === true || getOppBool === true) {
            $A.get("e.force:closeQuickAction").fire();
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Atención!",
                "message": "Por favor, termina la reagenda del flujo",
                "duration": "7000",
                "type": "warning"
            });
            toastEvent.fire();
        }
        component.set('v.mobilePhone', mobStr);
    },
    updateRecordNA: function (component, event, helper) {
        var getSource1 = component.get('v.recordObject');
        var getSac1 = getSource1.LeadSource;
        var urlEvent = $A.get("e.force:navigateToURL");
        var updateRecord = component.get('c.getLvl7Value');
        var boolRecord = component.get('v.isLead');
        updateRecord.setParams({
            "recordId": getSource1.Id,
            "isLead": boolRecord
        });
        updateRecord.setCallback(this, function (response) {
            var result = response.getState();
            if (component.isValid() && result === "SUCCESS") {
                component.set("v.HideSpinner", true);
                if (getSac1 !== 'SAC') {
                    urlEvent.setParams({
                        "url": "/lightning/page/home"
                    });
                    helper.toastEventCustom(component, event, helper, "Success", $A.get("{!$Label.c.MX_SB_VTS_reagendaSuccess}"), "Exito!");
                    urlEvent.fire();
                    var navigate = component.get('v.navigateFlow');
                    navigate('NEXT');
                }
            }
        });
        $A.enqueueAction(updateRecord);
    },
    closeRecord: function (component, event, helper) {
        var workspaceAPI = component.find("workspace");
        var urlEvent = $A.get("e.force:navigateToURL");
        workspaceAPI.isConsoleNavigation().then(function (response) {
            if (response) {
                var workspace = component.find("workspace");
                workspace.getFocusedTabInfo().then(function (response) {
                    var focusedTabId = response.tabId;
                    helper.toastEventCustom(component, event, helper, "Success", $A.get("{!$Label.c.MX_SB_VTS_reagendaFlow}"), "Exito!");
                    workspace.closeTab({
                        tabId: focusedTabId
                    });
                })
                    .catch(function (errortab) {
                        console.log('cierra tab error', errortab);
                        helper.toastEventCustom(component, event, helper, "Error", errortab, "Ups!");
                    });
            } else {
                urlEvent.setParams({
                    "url": "/lightning/page/home"
                });
                helper.toastEventCustom(component, event, helper, "Success", $A.get("{!$Label.c.MX_SB_VTS_reagendaFlow}"), "Exito!");
                urlEvent.fire();
            }
        }).catch(function (error) {
            console.log('error 1 ', error);
            helper.toastEventCustom(component, event, helper, "Error", error, "Ups!");
        });
    },
    toastEventCustom: function (component, event, helper, type, message, title) {
        console.log('toastEventCustom', type, message, title);
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "duration": "2000",
            "type": type
        });
        toastEvent.fire();
    },
    helperupdateIsReagenda: function (component) {
        var leadId = component.get("v.recordId");
        let updateIsReagenda = component.get("c.updateIsReagenda");
        updateIsReagenda.setParams({
            "leadId": leadId
        });
        updateIsReagenda.setCallback(this, function (response) {
            if (response) {
                window.onerror = function (message, url, lineNumber) {
                    return true;
                }
                $A.get("e.force:refreshView").fire();
                let dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
                var navigate = component.get('v.navigateFlow');
                navigate('BACK');
            }

        });
        $A.enqueueAction(updateIsReagenda);
    },
    callQueryOpp: function (C) {
        var strId = C.get("v.recordId");
        var action = C.get("c.fetchOpp");
        action.setParams({
            'strId': strId
        });
        action.setCallback(this, function (res) {
            var state = res.getState();
            if (state === "SUCCESS") {
                C.set("v.CurrentRT", res.getReturnValue().RecordType.Name);
            }
        });
        $A.enqueueAction(action);
    }
})