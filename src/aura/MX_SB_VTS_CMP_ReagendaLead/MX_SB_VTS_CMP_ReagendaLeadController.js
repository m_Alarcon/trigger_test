({
    createNewTask: function (component, event, helper) {
        component.set("v.HideSpinner", true);
        helper.helperCreateNewTask(component, event, helper);
    },
    getUsr: function (component, event, helper) {
		helper.callQueryOpp(component);
        let isVTA = component.get("v.agendar");
        console.log('isVTA', isVTA);
        if (isVTA !== 'VTA') {
            helper.helperGetUsr(component);
        } else {
            helper.closeRecord(component, event, helper);
        }
    },
    cancelBtn: function (component, event, helper) {
        helper.helperupdateIsReagenda(component);
    },
    handleChange: function (component, event, helper) {
        helper.helperHandleChange(component);
    },
    afterUpdate: function (component, event, helper) {
        var agendar = component.get("v.agendar");
        if (agendar === 'Agendar') {
            helper.helperAfterUpdate(component);
        } else {
            helper.updateRecordNA(component, event, helper);
        }
    }
})