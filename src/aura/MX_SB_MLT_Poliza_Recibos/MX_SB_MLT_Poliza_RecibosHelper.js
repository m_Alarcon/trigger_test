({
	consultaInicial : function(component, event) {
		var action = component.get("c.consultaClipert");
        var numPol = component.get('v.poliza');
        action.setParams({
            'numPoliza' : numPol
        });
        action.setCallback(this,function(response){
            if (response.getState() === "SUCCESS") {
                var StoreResponse = response.getReturnValue();
                component.set('v.mapRecibos',StoreResponse);
                 var map = component.get('v.mapRecibos');
                 component.set('v.contactName',map[0].nombre);
                 component.set('v.rfc',map[0].rfc);
                  var a = component.get('c.evaluaSemaforo');
                  $A.enqueueAction(a);
            }
        });
         $A.enqueueAction(action);
        var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        "title": "Error de comunicacion con Clipert!",
        "message": "Se realizo consulta local del CRM"
    });
    toastEvent.fire();
	},
    evaluaSemaforo : function(component, event) {
    	component.set("semaforo",true);
    },
    changeTabNameRecibos : function(component, event) {
        var workspaceAPIRecCob = component.find("workspace");
        workspaceAPIRecCob.getFocusedTabInfo().then(function(response) {
			workspaceAPIRecCob.isSubtab({
				tabId: response.tabId
            	}).then(function(response) {
                	if (response) {
						var focusedTabId = response.tabId;
            			workspaceAPIRecCob.setTabLabel({
                		tabId: focusedTabId,
                		label: "Información de Recibos",
            			});
            			workspaceAPIRecCob.setTabIcon({
                		tabId: focusedTabId,
                		icon: "custom:custom17",
                        iconAlt:"Información de Recibos"
            			});
                	}
            });
        })
	       .catch(function(error) {
            console.log(error);
        });
    },
    consultaCliente : function(component, event) {
        var action2 = component.get("c.consultaCliente");
        var numPol = component.get('v.numPoliza');
        action2.setParams({
            'numPoliza' : numPol
        });
        action2.setCallback(this,function(response){
            if (response.getState() === "SUCCESS") {
                var StoreResponse = response.getReturnValue();
                component.set('v.clienteMap',StoreResponse);
            }
        });
         $A.enqueueAction(action2);
    },
})