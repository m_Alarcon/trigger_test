({
    handleCancelVida: function(component) {
        component.set('v.isEditMode',false);
    },
    doInit : function(component) {
        var action = component.get("c.getRecordType");
	action.setParams({"devname":"MX_SB_MLT_RamoVida"});
        action.setCallback(this, function(response) {
            component.set("v.recordtypeVida",response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    handleSuccessVida : function(component, event) {
	var payload = event.getParams().response;
        var navService = component.find("navService");
        var pageReference = {
            type: 'standard__recordPage',
            attributes: {
                "recordId": payload.id,
                "objectApiName": component.get("v.sObjectName"),
                "actionName": "view"
            }
        }
        event.preventDefault();
        navService.navigate(pageReference);
	},
	toggleEditModeVida : function(component) {
		component.set("v.isEditMode", !component.get("v.isEditMode"));
	},
    handleEditVida : function(component) {
        component.find("DetalleSiniestroVida").submit();
        $A.get("e.force:refreshView").fire();
        $A.get("e.force:editRecord").fire();
        component.set('v.isEditMode',false);
    }
})