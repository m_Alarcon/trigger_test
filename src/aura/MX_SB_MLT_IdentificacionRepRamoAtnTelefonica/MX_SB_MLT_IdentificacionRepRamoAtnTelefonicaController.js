({
    handleCancelIdenTel: function(component) {
        component.set('v.isEditMode', false);
    },
    doInitIdenTel : function(component, event, helper) {
        helper.modalAuto(component, event, helper);
        var recordId = component.get('v.recordId');
        var action2 = component.get("c.getDatosSiniestro");
        action2.setParams({"idsini":recordId});
        action2.setCallback(this, function(response){
            component.set("v.siniestroObj",response.getReturnValue());
        	component.set("v.phonep",response.getReturnValue().MX_SB_MLT_Telefono__c);
            if(response.getReturnValue().MX_SB_MLT_Mobilephone__c!==undefined) {
                component.set("v.mobilePhone",response.getReturnValue().MX_SB_MLT_Mobilephone__c)
            }
            else {
                component.set("v.mobilePhone",'');
            }
        });
        $A.enqueueAction(action2);
    },
    handleSuccessIdenTel : function(component, event,helper) {
        var continuar = component.get('v.continuar');
        if(continuar===false){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
            		"title": "Actualización de registro",
                    "type":"success",
            		"message": "Cambio exitoso"
            });
            toastEvent.fire();
        }
    },
    toggleEditModeIdenTel : function(component) {
        component.set("v.isEditMode", !component.get("v.isEditMode"));
    },
    handleEditSiniestroIdenTel : function(component,event, helper) {
        component.find("SiniestroEditForm").submit();
        $A.get("e.force:editRecord").fire();
        component.set('v.isEditMode', false);
    },
    CrearParticipanteSiniestroIdenTel : function(component, helper) {
    var recordId = component.get('v.recordId');
    var action = component.get('c.createPartSin');
        action.setParams({
            'sinId': recordId
        });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    validacionForm : function(component, event, helper) {
        component.set('v.siniestroObj.MX_SB_MLT_JourneySin__c','Identificación del Reportante');
        component.set('v.continuar',false);
        helper.telefono(component,event,helper);
    },
     validacionFormCon : function(component, event, helper) {
        component.set('v.siniestroObj.MX_SB_MLT_JourneySin__c','Validación de Poliza');
        component.set('v.continuar',true);
        helper.telefono(component,event,helper);
	},
    obTabId: function(component,event,helper){
		var workspaceAPI = component.find("workspace");
		workspaceAPI.getAllTabInfo().then(function(response) {
			for(var i=0;i<=response.length;i++){
                  if(response[i].title.match(/Crear Siniestro/)) {
                     var idTab = response[i].tabId;
                     workspaceAPI.closeTab({tabId: idTab });
                 }
			}
		});
    },
    numeroCambia: function(component,event,helper) {
        helper.validaNumero(component,event,helper);
    },
})