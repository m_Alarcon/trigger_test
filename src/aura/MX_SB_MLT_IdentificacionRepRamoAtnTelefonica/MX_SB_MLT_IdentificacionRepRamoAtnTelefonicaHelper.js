({
    toastTelefono : function () {
        var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error:',
                message:'Error en validación de campos',
                duration:' 5000',
                key: 'info_alt',
                type: 'error'
            });
            toastEvent.fire();
    },
    toastTelefonoTrue : function () {
        var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "Actualización de registro",
                type:"success",
            	message: "Cambio exitoso"
            });
            toastEvent.fire();
    },
	telefono : function(component, event,helper) {
	   	var telefonoTipo = component.find("tipoTel");
        telefonoTipo.reportValidity();
        telefonoTipo = telefonoTipo.get("v.validity");
        var telefono = component.find("MobilePhoneIdenAtn");
        telefono.reportValidity();
        telefono = telefono.get("v.validity");
        var nombre = component.find("NameIdenAtn");
        nombre.reportValidity();
        nombre = nombre.get("v.validity");
        var apaterno = component.find("LastNameIdenAtn");
        apaterno.reportValidity();
        apaterno=apaterno.get("v.validity");
        var aseguradora = component.find("asegtxt");
        aseguradora.reportValidity();
        aseguradora=aseguradora.get("v.validity");
        var amaterno = component.find("LastName2IdenAtn");
        amaterno.reportValidity();
        amaterno = amaterno.get("v.validity");
        if(telefonoTipo.valid && telefono.valid && nombre.valid && apaterno.valid && aseguradora.valid && amaterno.valid) {
            var siniestro = component.get('v.siniestroObj');
            siniestro.MX_SB_MLT_NombreConductor__c=siniestro.MX_SB_MLT_NombreConductor__c.toUpperCase();
            siniestro.MX_SB_MLT_APaternoConductor__c=siniestro.MX_SB_MLT_APaternoConductor__c.toUpperCase();
            if(siniestro.MX_SB_MLT_AMaternoConductor__c!==undefined) {
                siniestro.MX_SB_MLT_AMaternoConductor__c=siniestro.MX_SB_MLT_AMaternoConductor__c.toUpperCase();
            }
            var action = component.get('c.upSin');
        action.setParams( {
            'sinIn': siniestro
        } );
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                this.toastTelefonoTrue();
                var cont = component.get('v.continuar');
                if(cont) {
                    window.location.reload();
                }
            } else {
                this.toastTelefono();
            }
        });
        $A.enqueueAction(action);
        } else {
            this.toastTelefono();
        }
	},
    modalAuto : function(component, event, helper) {
        let recordId = component.get('v.recordId');
        let action = component.get('c.getDatosSiniestro');
        action.setParams({'idsini': recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue().TipoSiniestro__c==='Colisión' && response.getReturnValue().MX_SB_MLT_LugarAtencionAuto__c==='Crucero' ) {
                let ques1 = response.getReturnValue().MX_SB_MLT_PropiedadAjena__c;
                component.set("v.question1", ques1);
				let ques2 = response.getReturnValue().MX_SB_MLT_Q_ThirdPresent__c;
                component.set("v.question2", ques2);
            	let ques3 = response.getReturnValue().MX_SB_MLT_Q_AuthorityPresent__c;
                component.set("v.question3", ques3);
                let siniestro = response.getReturnValue().TipoSiniestro__c;
                component.set("v.siniestro", siniestro);

            }
        });
        $A.enqueueAction(action);
     },
    validaNumero: function(component,event,helper) {
        var tel = component.find('MobilePhoneIdenAtn');
        var phoneValue = tel.get('v.value');
       component.set("v.siniestroObj.MX_SB_MLT_Mobilephone__c",phoneValue.replace(/[^0-9\.]+/g,''));
    },
})