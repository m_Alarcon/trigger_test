({
	getCaseList : function(component, event, helper) {
      	component.set('v.columns', [
            {label: 'Fecha del Reporte', fieldName: 'CreatedDate', type: 'date'},
            {label: 'Numero del Reporte', fieldName: 'linkName', type: 'url',
            typeAttributes: {label: { fieldName: 'WorkOrderNumber' }, target: '_subTab'}},
            {label: 'Asistencia Solicitada', fieldName: 'MX_SB_MLT_TipoSiniestro__c', type: 'text',},
            {label: 'Estatus', fieldName: 'MX_SB_MLT_EstatusServicio__c', type: 'text'}
            ]);
		let recordId = component.get("v.recordId");
        let action = component.get("c.getServicio");
            action.setParams({ "sinId": recordId } );
            action.setCallback(this, function(response) {
                let state = response.getState();
                if (state === "SUCCESS") {
                    let records = response.getReturnValue();
                    records.forEach(function(record) {
                        record.linkName = '/'+record.Id;
                    });
                    component.set("v.info", response.getReturnValue());
                    component.set("v.size", response.getReturnValue().length);
                }
            });
            $A.enqueueAction(action);
	},
    countCases : function (component, event, helper) {
        let recordId = component.get("v.recordId");
        let action = component.get("c.countServicio");
            action.setParams({"sinId": recordId});
            action.setCallback(this, function(response){
                let state = response.getState();
                if (state === "SUCCESS") {
                    let grua = response.getReturnValue().Grua;
                    component.set("v.grua", grua);
                    let corriente = response.getReturnValue()['Paso de corriente'];
                    component.set("v.corriente", corriente);
                    let gasolina = response.getReturnValue().Gasolina;
                    component.set("v.gasolina", gasolina);
                    let cambio = response.getReturnValue()['Cambio de llanta'];
                    component.set("v.cambio", cambio);
                }
            });
            $A.enqueueAction(action);
    }
})