({
   initHelper : function(component, event, helper) {
		helper.utilSetMarkers(component, event, helper);
   },
   searchAddressGoogleAPI : function(component, event, helper) {
			let action = component.get("c.getLocationGeocodeString");
			action.setParams({"sAddress": component.find("sAddress").get("v.value") });
			action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.bMap",true);
				const data = response.getReturnValue();
				let latLong = [];
				latLong = data.split(',');
				component.find("sLocation").set("v.value",data)
				component.set('v.mapMarkers', [
					{
						location: {
							Latitude : latLong[0],
							Longitude : latLong[1]
						},
						icon:'utility:Tower',
						title: 'Siniestro',
						description: 'Siniestro'
					}
				]);
				component.set('v.markersTitle', 'Ubicación del siniestro');
				component.set('v.zoomLevel', 17);
			} else {
				component.set("v.bMap",true);
			}
		});
		$A.enqueueAction(action);
   },
   saveAddressToCase : function(component, event, helper) {
		component.find("recordHandler").saveRecord($A.getCallback(function(saveResult) {
			if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
				component.set("v.bMap",true);
			}
		}));
	},
	handleRecordUpdated: function(component, event, helper) {
		var eventParams = event.getParams();
		if(eventParams.changeType === "CHANGED") {
			var resultsToast = $A.get("e.force:showToast");
			resultsToast.setParams({
				"title": "Saved",
				"message": "The record was updated."
			});
			resultsToast.fire();
		}
	}
})