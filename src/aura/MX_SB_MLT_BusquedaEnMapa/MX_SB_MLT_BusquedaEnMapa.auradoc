<aura:documentation>
    <!--
    /*
    * @author:      Isaías Velazquez Cortes
    * @date:        02/05/2019
    * @description  Componente encargado de mostrar el mapa en un modal para modificar la dirección.
    * @Changes
    *       2019-05-02          Creación                               Isaías Velazquez Cortes
    */
    -->
    <aura:description>
        <p><b>NAME</b> MX_SB_MLT_BusquedaEnMapa</p>
        <p><b>DESCRIPTION</b>El componente muestra mapa de google maps en un modal y permite cambiar la dirección mediante un campo de texto.</p>
        <p><b>ATTRIBUTES</b>
            <ul>
                <li><p>attribute url : campo que almacena la URL del api de google </p></li>
                <li><p>attribute isActive: indica si está activo</p></li>
                <li><p>attribute title :  campo que almacena la ubicación</p></li>
                <li><p>attribute mapMarkers:  Campo que almacena el objeto para trabajar con Maps </p></li>
                <li><p>attribute location: Almacena la longitud y latitud. </p></li>
                <li><p>attribute markersTitle : Muestra el título del marker en el mapa.</p></li>
                <li><p>attribute zoomLevel : variable que se usa para definir el tamaño del zoom de el mapa.</p></li>
                <li><p>attribute bMap : Variable para mostrar el modal.</p></li>
                <li><p>attribute record :  Almacena el registro de la tarea de datos.</p></li>
                <li><p>attribute simpleRecord : Almacena los campo del registro de la tarea de datos </p></li>
                <li><p>attribute recordError : Variable par mostrar mensaje de error </p></li>
                <li><p>attribute recordId : Id del Caso</p></li>
            </ul>
        </p>    
        <p><b>CONTROLLER METHODS</b>
            <ul>
                <li><p><b>searchAddress</b>Realiza la llamada a la función searchAddressGoogleAPI del helper.</p></li>
                <li><p><b>saveAddress</b>Realiza la llamada a la función saveAddressToCase del helper.</p></li>
                <li><p><b>handleCancel</b>Oculta el modal.</p></li>
                <li><p><b>handleRecordUpdated</b>Realiza la llamada a la función saveAddressToCase del helper.</p></li>
            </ul>
        </p>
        <p><b>HELPER METHODS</b>
            <ul>
                <li><p><b>initHelper</b>Realiza llamada a utilSetMarkers.</p></li>
                <li><p><b>searchAddressGoogleAPI</b>Realiza la llama al método getLocationGeocodeString.</p></li>
                <li><p><b>saveAddressToCase</b>Aplica la actualización al registro de tarea de datos.</p></li>
                <li><p><b>handleRecordUpdated</b>Realiza un recarga del componente.</p></li>
            </ul>
        </p>
    </aura:description>
</aura:documentation>