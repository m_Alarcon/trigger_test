({
	searchAddress: function(component, event, helper) {
		component.set('v.bMap', false);
		helper.searchAddressGoogleAPI(component, event, helper);
	},
	saveAddress: function(component, event, helper) {
		helper.saveAddressToCase(component, event, helper);
		component.set('v.isActive', false);
		$A.get('e.force:closeQuickAction').fire();
		$A.get('e.force:refreshView').fire();
	},
	handleCancel: function(component) {
		component.set('v.isActive', false);
	},
	handleRecordUpdated: function(component, event, helper) {
		helper.saveAddressToCase(component, event, helper);
	}
});