({
     doInit:function(component, event, helper){
        var action = component.get("c.getValue");
        action.setParams({"recId": component.get("v.recordId") });
        action.setCallback(this, function(response) {
            component.set("v.Telefono", response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
	navigateToMyComponent : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:MX_SB_MLT_RecordTypesFormD",
            isredirect: true,
            componentAttributes: {
                Telefono : component.get("v.Telefono")
            }
        });
        evt.fire();
    }

})