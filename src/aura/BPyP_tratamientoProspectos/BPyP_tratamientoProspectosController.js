({
    init : function(cmp, event, helper) {
        cmp.set("v.url", window.location.origin);
        cmp.set('v.columns', [
            { label: 'Nombre', fieldName: 'LinkName', type: 'url', sortable: true,cellAttributes: { alignment: 'left' },
             hideDefaultActions:true, typeAttributes: { label: { fieldName: 'Name' }, target: '_blank' } },
            { label: 'Comentarios ', fieldName: 'BPyP_at_Comentarios__c', type: 'text', cellAttributes: { alignment: 'left' },hideDefaultActions:true},
            { label: 'Estado', fieldName: 'BPyP_ls_Estado_del_candidato__c', type: 'text', cellAttributes: { alignment: 'left'},hideDefaultActions:true },
            { label: 'Correo', fieldName: 'PersonEmail', type: 'text',sortable:true, cellAttributes: { alignment: 'left'},hideDefaultActions:true},
            { label: 'Oficina gestora', fieldName: 'Oficina_Gestora__c', type: 'text',sortable:true, cellAttributes: { alignment: 'left'},hideDefaultActions:true},
            { label: 'Valor', fieldName: 'BPyP_dv_Monto_de_inversi_n__c', type: 'currency',sortable:true, cellAttributes: { alignment: 'left'},hideDefaultActions:true},
            { label: 'Campaña', fieldName: 'Tipo_de_Registro_Manual__c', type: 'text', sortable:true,cellAttributes: { alignment: 'left'},hideDefaultActions:true},
        ]);

        cmp.set('v.userColumns', [
            { label: 'Nombre', fieldName: 'Name', type: 'text', sortable: true, cellAttributes: { alignment: 'left' } },
        ]);

        let createdMap = new Object;
        createdMap.Name = [{}];
        createdMap.BPyP_dv_Monto_de_inversi_n__c = [{}];
        createdMap.Oficina_Gestora__c = [{}];
        createdMap.Tipo_de_Registro_Manual__c = [{}];

        let filterObj = new Object;
        filterObj.Name = '';
        filterObj.BPyP_dv_Monto_de_inversi_n__c = '';
        filterObj.Oficina_Gestora__c = '';
        filterObj.Tipo_de_Registro_Manual__c = '';

        cmp.set("v.searchResults", createdMap);
        cmp.set("v.filterObj", createdMap);

        helper.getData(cmp);
    },

    handleSearch: function (cmp, event, helper) {
        let searchKey = event.getSource().get("v.name");
        let searchVal = event.getParam('value');
        let sizeNombre = cmp.get("v.valueNombre").length;
        let sizeValorMin = cmp.get("v.valueMin").length;
        let sizeValorMax = cmp.get("v.valueMax").length;
        let sizeOficina = cmp.get("v.valueOficina").length;
        let sizeCampania = cmp.get("v.valueCampania").length;
        let data = cmp.get("v.initialData");
        let valueMin=cmp.get("v.valueMin");
        let valueMax=cmp.get("v.valueMax");


        data = data.length > 0 ? data : initialData;

        if(searchVal.length > 2  ||( sizeValorMin > 0 ||valueMin === '')) {
            helper.switchSearch(cmp, searchKey, data, searchVal,sizeValorMax);

        }

        else if(searchVal.length < 3 && sizeNombre < 3  && sizeOficina < 3 && sizeCampania < 3 || (sizeValorMin === 0 && sizeValorMax === 0) ){
            cmp.set("v.data", cmp.get("v.initialData"));

        }
        if(valueMin === '' && valueMax !== '') {
            helper.switchSearch(cmp, searchKey, data, searchVal);
            cmp.set("v.data", cmp.get("v.initialData"));
            cmp.set('v.vMin','0');
        }


    },

    updateSelected: function (cmp, event, helper) {
        let selectedRows = event.getParam('selectedRows');
        let accsIds = [];

        for(let acc in selectedRows) {
            accsIds.push(selectedRows[acc].Id);
        }
        cmp.set("v.selectedData", accsIds);
    },

    handleClick: function (cmp, event, helper) {
        let button = event.getSource().get("v.variant");
        let selectedAccs = cmp.get("v.selectedData");

        if(selectedAccs.length > 0) {
            cmp.set("v.showModal", true);
            cmp.set("v.actionButton", button);
            cmp.set("v.modalTitle", button === "brand" ? "Selecciona al nuevo propietario" : "¿Desea descartar los prospectos seleccionados?");
            cmp.set("v.modalAction", button === "brand" ? "Asignar" : "Descartar");
        } else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Seleccione cuentas",
                "message": "Para continuar, seleciona por lo menos una cuenta.",
                "type": "error"
            });
            toastEvent.fire();
        }
    },

    closeModal: function (cmp, event, helper) {
        cmp.set("v.showModal", false);
    },

    handleUsrSearch: function (cmp, event, helper) {
        let searchVal = event.getParam('value');
        helper.searchUsr(cmp, searchVal);
    },

    selectedUser: function (cmp, event, helper) {
        var selectedRows = event.getParam('selectedRows');
        cmp.set("v.selectedUser", selectedRows[0].Id);
    },

    handleAction: function (cmp, event, helper) {
        let action = cmp.get("v.actionButton");
        let newOwner = cmp.get("v.selectedUser");
        let selectedAccs = cmp.get("v.selectedData");

        cmp.set("v.toggleSpinner", true);

        if(action === "brand") {
            helper.saveNewOwner(cmp, newOwner, selectedAccs);
        } else {
            helper.discardAcc(cmp, selectedAccs);
        }
    },

     handleSort: function(cmp, event, helper) {
        helper.handleSort(cmp, event);
    }


})