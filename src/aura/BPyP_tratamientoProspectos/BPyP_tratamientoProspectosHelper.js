({
    getData : function(cmp) {
        let actionData = cmp.get('c.getAccounts');
        actionData.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var records = response.getReturnValue();
                records.forEach(function (record) {
                record.LinkName = cmp.get('v.url') + '/' + record.Id;
            })
                cmp.set('v.data', response.getReturnValue());
                cmp.set('v.initialData', response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(actionData);
    },

    switchSearch: function(cmp, searchKey, data, searchVal,vMaxS) {

        switch(searchKey) {
            case "Name":
                this.searchLocal(cmp, "Name", data, searchVal);
                break;

            case "BPyP_dv_Monto_de_inversi_n__c":
                this.handleValues(cmp, event);
                break;
            case "Oficina_Gestora__c":
                this.searchLocal(cmp, "Oficina_Gestora__c", data, searchVal);
                break;

            case "Tipo_de_Registro_Manual__c":
                this.searchLocal(cmp, "Tipo_de_Registro_Manual__c", data, searchVal);
                break;
        }
    },

    searchLocal: function(cmp, field, data, searchVal, doneSearch) {
        let aFilter;
        let anAcc;
        let filterRes = cmp.get("v.filterObj");
        let copyfilterRes = Object.assign({}, filterRes);
        let mapResults = cmp.get("v.searchResults");
        let copyMapResults = Object.assign({}, mapResults);
        copyfilterRes[field] = searchVal;
        data = data.filter(function(acc) {
            for (let key in copyfilterRes) {
                aFilter = copyfilterRes[key].toString().toLowerCase();
                anAcc = acc[key] !== undefined ? acc[key].toString().toLowerCase() : ' ';
                if (key === field && (anAcc === aFilter || anAcc.startsWith(aFilter) || anAcc.includes(" "+aFilter))) {
                    return true;
                }
            }
            return false;
        });
        copyMapResults[field] = data;
        cmp.set("v.filterObj", copyfilterRes);
        cmp.set("v.searchResults", copyMapResults);
        cmp.set("v.data", data)
    },

    filterValues: function(cmp,data,valMin, valMax) {

        let field="BPyP_dv_Monto_de_inversi_n__c";
        let filterRes = cmp.get("v.filterObj");
        let copyfilterRes = Object.assign({}, filterRes);
        copyfilterRes[field] = ""+valMin+valMax;
        data = data.filter(function(acc) {

            let valor= acc[field];
            return valor >= valMin && valor <= valMax;
        });

        cmp.set("v.data", data);
    },

    searchUsr: function(cmp, searchVal) {
        if(searchVal.length > 2) {
            let actionUser = cmp.get('c.searchUser');
            searchVal = '%'+searchVal+'%';
            actionUser.setParams({ userName : searchVal });
            actionUser.setCallback(this, $A.getCallback(function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    cmp.set("v.searchedUser", response.getReturnValue());
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    console.error(errors);
                }
            }));
            $A.enqueueAction(actionUser);
        }
    },

    saveNewOwner: function(cmp, newOwner, accList) {
        cmp.set("v.showModal", false);

        let actionOwner = cmp.get('c.saveNewOwner');
        actionOwner.setParams({ newOwnerId : newOwner, accIds : accList});
        actionOwner.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.toggleSpinner", false);
                this.fireToast("Asignación correcta", "Las cuentas se han asignado exitosamente.", "success");
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(actionOwner);
    },

    discardAcc: function(cmp, accList) {
        cmp.set("v.showModal", false);

        let actionDiscard = cmp.get('c.discardAccounts');
        actionDiscard.setParams({accIds : accList});
        actionDiscard.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.toggleSpinner", false);

                this.fireToast("Cuentas descartadas", "Las cuentas se han descartado exitosamente.","info");

            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(actionDiscard);
    },

    fireToast: function(title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": title,
                "message": message,
                "type": type
            });
            toastEvent.fire();

            $A.get('e.force:refreshView').fire();
    },

     sortBy: function(field, reverse, primer) {
        var key = primer
            ? function(x) {
                  return primer(x[field]);
              }
            : function(x) {
                  return x[field];
              };

        return function(a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    },

    handleSort: function(cmp, event) {
        var sortedBy = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        var cloneData = cmp.get('v.data').slice(0);
        cloneData.sort((this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1)));

        cmp.set('v.data', cloneData);
        cmp.set('v.sortDirection', sortDirection);
        cmp.set('v.sortedBy', sortedBy);
    },


    handleValues: function(cmp,event,helper) {

        let valMax=cmp.get("v.valueMax");
        let valMin = cmp.get("v.valueMin");
        let vMaxActive= cmp.get("v.actVmax");
        let vMax=cmp.get("v.valueMax");
        let vMin = cmp.get("v.valueMin");
        let vMaxSize=cmp.get("v.valueMax").length;
        let data = cmp.get("v.initialData");

        vMax=Number(vMax);
        vMin=Number(vMin);
        if((valMin !== '') && (valMax === '')  && (vMaxSize === 0 || vMaxSize === undefined) ) {

            cmp.set("{!v.actVmax}",false);
            cmp.set("{!v.warnMsg}","Complete este campo");
        }


        if(vMin > vMax && vMaxActive === false &&  vMaxSize !== undefined) {
            cmp.set("{!v.warnMsg}","Valor 'desde' debe ser menor");

         }


         if (valMin === '' && valMax=== '' &&  vMaxActive === false) {
            vMaxActive= true;
            cmp.set("{!v.actVmax}",vMaxActive);
            cmp.set("{!v.warnMsg}","");
            cmp.set("v.data",data);

        }

        if(valMin === '' && valMax !== '') {
            vMaxActive=true;
            cmp.set("{!v.actVmax}",vMaxActive);
            cmp.set("{!v.valueMax}","");
        }

        else if((vMin < vMax) && (vMin > 0 && vMax > 0)) {
            cmp.set("{!v.warnMsg}","");
           this.filterValues(cmp,data,valMin, valMax);
            vMaxActive= false;
            cmp.set("{!v.actVmax}",vMaxActive);

        }


    }
})