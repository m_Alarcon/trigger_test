({
	sendMailCTRL : function(component, helper) {
        var action = component.get('c.sendMail');
        action.setParams({
            "currentId": component.get("v.currentRI")
        });
        action.setCallback(this, function (response) {
        });
        $A.enqueueAction(action);
        component.set('v.isActive', false);
	}
})