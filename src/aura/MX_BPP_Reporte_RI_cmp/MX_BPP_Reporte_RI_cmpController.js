({
    init: function (component, event, helper) {
    },
    sendMailCTRL : function(component, event, helper) {
        helper.sendMailCTRL(component, helper);
    },
    handleCancel: function (component) {
        component.set('v.isActive', false);
    },
    showReport: function (component) {
        var recordId = component.get("v.currentRI");
        var action = component.get("c.returnURL");
        var url2;
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                url2 = response.getReturnValue();
                var url = 'https://'+ url2 + '--c.visualforce.com/apex/MX_BPP_ReporteRI?id=' + recordId;
                window.open(url);
            }
        });
        $A.enqueueAction(action);
    },
})