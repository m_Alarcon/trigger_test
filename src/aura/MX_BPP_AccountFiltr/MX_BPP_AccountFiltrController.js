({
	doInit : function(component, event, helper) {
        helper.helperInitAcc(component, event);
	},

    generateChart : function(component, event, helper){
        helper.helperGenerateChartAcc(component, event, helper);
    },

    updTyInf : function(component, e, helper){
        helper.combosSetA(component, e);
        helper.draw(component, e);
        component.set('v.hasListValue', false);
        component.set('v.ListOfOpp', []);
        if(component.get("v.selbkm") !== 'TODAS') {
            helper.fetchAccs(component, e, 0);
        }
    },

    updDiv: function (component, e, helper) {
        component.set("v.seloff", "TODAS");
        component.set("v.selbkm", "TODAS");
        component.set('v.hasListValue', false);
        component.set('v.ListOfOpp', []);
        helper.combosSetA(component, e);
        helper.draw(component, e);
    },

    updOffice: function (component, e, helper) {
        component.set("v.selbkm", "TODAS");
        component.set('v.hasListValue', false);
        component.set('v.ListOfOpp', []);
        helper.combosSetA(component, e);
        helper.draw(component, e);
    },

    updBkM: function (component, e, helper) {
        component.set('v.hasListValue', false);
        component.set('v.ListOfOpp', []);
        helper.combosSetA(component, e);
        helper.draw(component, e);
        if(component.get("v.selbkm") !== 'TODAS') {
            helper.fetchAccs(component, e, 0);
        }
    },

    frst: function (component, e, helper) {
        helper.pagefunctionCont(component, 0);
    },

    next: function (component, e, helper) {
        var actPg = component.get('v.ActPag');
        var totPgA = component.get('v.NumOfPag');
        var nxt = actPg === totPgA ? totPgA : actPg + 1;
        nxt--;
        nxt = nxt * window.sizeofAcc;
        helper.pagefunctionCont(component, nxt, true);
    },

    prev: function (component, e, helper) {
        var actPgprev = component.get('v.ActPag');
        var prvA = actPgprev === 1 ? 1 : actPgprev - 1;
        prvA--;
        prvA = prvA * window.sizeofAcc;
        helper.pagefunctionCont(component, prvA, true);
    },

    lst: function (component, e, helper) {
        var totPgA = component.get('v.NumOfPag');
        totPgA--;
        totPgA = totPgA * window.sizeofAcc;
        helper.pagefunctionCont(component, totPgA, true);
    }
})