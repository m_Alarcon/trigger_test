({
    helperInitAcc : function(component, event) {
        component.set("v.baseurl", window.location.origin + "/");

        var actionfetchInfoByUserA = component.get('c.fetchInfoByUserAcc');
        actionfetchInfoByUserA.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnWrapperA = response.getReturnValue();
                component.set('v.wrapperObjectA', returnWrapperA);
                component.set('v.ListOfDiv', returnWrapperA.setDivisiones);
            }
        });
        $A.enqueueAction(actionfetchInfoByUserA);

        component.set("v.accessCombosA" , {
            "Administrador del sistema" : {"div":true, "off":true, "bnk":true },
            "Soporte BPyP" : {"div":true, "off":true, "bnk":true },
            "BPyP STAFF" : {"div":true, "off":true, "bnk":true },
            "BPyP Director Divisional" : {"div":false, "off":true, "bnk":true },
            "BPyP Director Oficina" : {"div":false, "off":false, "bnk":true },
            "BPyP Estandar" : {"div":false, "off":false, "bnk":false }
        } );

        if (component.get('v.bandera')) {
            component.set('v.bandera', true);
        }
	},
    helperGenerateChartAcc: function (component, e, helper) {
        window.optPie = {
            tooltips: { enabled: true, },
            hover: { animationDuration: 10 },
            legend: { position: 'bottom', padding: 10, onClick: function (event, legendItem) { }, },
            animation: {
                onComplete: function () {
                    var chartInstanceA = this.chart;
                    var ctx = chartInstanceA.ctx;
                    ctx.textAlign = "left";
                    ctx.fillStyle = "#fff";
                    Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstanceA.controller.getDatasetMeta(i);
                        Chart.helpers.each(meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            if (data !== 0) { ctx.fillText(data, bar._model.x - 20, bar._model.y - 10); }
                        }), this)
                    }), this);
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{ stacked: true, gridLines: { display: false }, }],
                yAxes: [{ stacked: true, gridLines: {display: false }, }]
            },
        };

        window.dataToTableAcc = function (dataset, title) {
            var htmlA = '<table>';
            htmlA += '<thead><tr><th >' + title + '</th>';
            var columnCount = 0;
            jQuery.each(dataset.datasets,
                function (idx, item) {
                    htmlA += '<th style="background-color:' + item.fillColor + ';">' + item.label + '</th>';
                    columnCount += 1;
                }); htmlA += '</tr></thead>';
            jQuery.each(dataset.labels, function (idx, item) {
                htmlA += '<tr><td>' + item + '</td>';
                for (var i = 0; i < columnCount; i++) {
                    htmlA += '<td style="background-color:' + dataset.datasets[i].fillColor + ';">' +
                        (dataset.datasets[i].data[idx] === '0' ? '-' : dataset.datasets[i].data[idx]) + '</td>';
                }
                htmlA += '</tr>';
            });
            htmlA += '</tr><tbody></table>';
            return htmlA;
        };

        component.set('v.IsSpinner',true);
        window.setTimeout(
            $A.getCallback(function() {
                helper.initdrawAcc(component, e);
                component.set('v.IsSpinner',false);
            }), 5000
        );
    },

    initdrawAcc: function (component, e) {
        var actionfetchUserInfoA = component.get('c.fetchUserInfoAcc');
        actionfetchUserInfoA.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                this.verifyPerfilA(component, e, response);
            }
        });
        $A.enqueueAction(actionfetchUserInfoA);
    },

    verifyPerfilA: function (component, e, response) {
        var bpypestandar = "BPyP Estandar";
        var diroff = "BPyP Director Oficina";
        var dirdiv = "BPyP Director Divisional";
        var staff = "BPyP STAFF";
        var SoportePerfil ="Soporte BPyP";
        var adm = $A.get("$Label.c.MX_PERFIL_SystemAdministrator");

        var perfilA = String(response.getReturnValue()).split(',')[0];
        var divisionA = String(response.getReturnValue()).split(',')[1];
        var oficinaA = String(response.getReturnValue()).split(',')[2];
        var usrname = String(response.getReturnValue()).split(',')[3];
        var title = String(response.getReturnValue()).split(',')[4];

        component.set("v.userPInfo", perfilA);
        component.set("v.accessDiv", !component.get("v.accessCombosA")[perfilA].div);
        component.set("v.accessOff", !component.get("v.accessCombosA")[perfilA].off);
        component.set("v.accessBnk", !component.get("v.accessCombosA")[perfilA].bnk);

        if (perfilA === (staff) || perfilA === (adm) || perfilA === (SoportePerfil)) {
            component.set("v.seldiv","TODAS");
            this.draw(component, e);
            this.combosSetA(component, e);
        } else if (perfilA === dirdiv) {
            component.set('v.isDiv', true);
            component.set('v.seldiv', divisionA);
            component.set("v.seloff","TODAS");
			this.draw(component, e);
            this.combosSetA(component, e);
        } else if (perfilA === diroff) {
            component.set('v.userInfoDO', {"name" : usrname,
                             "division" : divisionA,
                              "oficina" : oficinaA});
            component.set('v.isOff', true);
            component.set('v.seldiv', divisionA);
            component.set('v.seloff', oficinaA);
            component.set("v.selbkm","TODAS");
            this.draw(component, e);
            this.combosSetA(component, e);
        } else if (perfilA === bpypestandar) {
            component.set('v.isBkm', true);
            component.set("v.seltitle", title);

            component.set('v.seldiv', divisionA);
            component.set('v.ListOfDiv', [divisionA]);

            component.set('v.seloff', oficinaA);
            component.set('v.ListOfOff', [oficinaA]);

            component.set('v.selbkm', usrname);
            component.set('v.ListOfBkMn', [{'Name':usrname}]);
            component.set('v.vistaBnk', false);

            this.draw(component, e);
        }
    },

    combosSetA: function (component, e) {
        var wrapperObjA = component.get('v.wrapperObjectA');
        var divSelectedA = component.get('v.seldiv');
        var offSelectedA = component.get('v.seloff');
        component.set('v.ListOfOff', wrapperObjA.mapSucursales[divSelectedA]);
        var listOfBkMn = wrapperObjA.maplistUserName[divSelectedA+offSelectedA];

        if(component.get('v.userInfoDO') && component.get('v.userInfoDO')['oficina'] === offSelectedA && !listOfBkMn.includes(component.get('v.userInfoDO')['name'])) {
            console.log('Agregado');
            listOfBkMn.push(component.get('v.userInfoDO')['name']);
        }
        component.set('v.ListOfBkMn', listOfBkMn);
    },

    gttypoQuery: function(component) {
      let parametroA = '';
        let tipoQuery = this.tipoQueryF(component);
        if(tipoQuery === 'Oficina'){
            parametroA = component.get("v.seldiv");
        } else if(tipoQuery === 'Banquero') {
            parametroA = component.get("v.seloff");
        } else if(tipoQuery === 'BanqueroOnly') {
            parametroA = component.get("v.selbkm");
        }
        return parametroA;
    },

    tipoQueryF: function (component) {
        var  tipoA = "";
        if(component.get("v.seldiv") === 'TODAS') {
            tipoA = "Division";
        } else if(component.get("v.seloff") === 'TODAS') {
            tipoA = "Oficina";
        } else if(component.get("v.selbkm") === 'TODAS') {
            tipoA = "Banquero";
        } else {
            tipoA = "BanqueroOnly";
        }
        return tipoA;
    },

    draw: function (component, e) {
        var labelTypeQuery = {'Division' : 'DIVISIóN', 'Oficina' : 'OFICINA', 'Banquero' : 'BANQUERO', 'BanqueroOnly' : 'BANQUERO'};
        var tipoQ = this.tipoQueryF(component);
        var parametro = this.gttypoQuery(component);
        var tipoAcc = component.find("pklTyInfBpyP").get("v.value");
		var listparams = [tipoAcc, tipoQ, parametro];

        var actionfetchDataAcc = component.get('c.fetchDataAcc');
        actionfetchDataAcc.setParams({
            params: listparams
        });
        actionfetchDataAcc.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var ctx = component.find("piechart").getElement();
                var chartdata = { labels: [], datasets: [{ data: [], backgroundColor: [], }]
                }
                if (window.pieChart != null) {
                    window.pieChart.clear();
                    window.pieChart.destroy();
                }
                window.pieChart = new Chart(ctx, { type: 'pie', data: chartdata, options: window.optPie });
                this.helpersetResponseValue(component, response, ctx, labelTypeQuery[tipoQ]);
            }
        });
        $A.enqueueAction(actionfetchDataAcc);
    },

    helpersetResponseValue: function (component, response, ctx, type) {

        var resp = response.getReturnValue();
        var height = 100 + (resp.lsLabels.toString().split(',').length * 30);
        component.set('v.chartHeight', height);
        if (window.pieChart != null) {
            window.pieChart.clear();
            window.pieChart.destroy();
        }

        window.pieChart = new Chart(ctx, {
            type: 'horizontalBar', data: {}, options: window.optPie
        });

        var labels = [];
        for (var i = resp.lsLabels.toString().split(',').length - 1; i >= 0; i--) {
            pieChart.config.data.labels.push(resp.lsLabels.toString().split(',')[i]);
            labels.push(resp.lsLabels.toString().split(',')[i]);
        }
        for (var j = resp.lsTyAcc.toString().split(',').length - 1; j >= 0; j--) {
            var datatemp = [];
            var acc = resp.lsTyAcc.toString().split(',')[j].replace(' BPyP', '');
            var accor = resp.lsTyAcc.toString().split(',')[j];
            for (var k = 0; k < labels.length; k++) {
                datatemp.push(resp.lsData[(labels[k] + accor)] == null ? 0 : resp.lsData[(labels[k] + accor)]);
            }
            var newDataset = {
                label: acc,
                backgroundColor: resp.lscolor.toString().split('),')[j] + ')',
                data: datatemp,
            }
            pieChart.data.datasets.push(newDataset);
        }
        pieChart.update();
        jQuery('#wrapperAcco').html(window.dataToTableAcc(pieChart.data, type));
        component.set('v.DateOfUpd', new Date().getDate() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getFullYear());
    },

    fetchAccs: function (component, e, offsetVal) {
        var tipoQA = this.tipoQueryF(component);
        var parametroA = this.gttypoQuery(component);
        var tipoAccA = component.find("pklTyInfBpyP").get("v.value");
		var listparamsA = [tipoAccA, tipoQA, parametroA, ''];

        var actionfetchAcc = component.get('c.fetchAcc');
        actionfetchAcc.setParams({
            params: listparamsA
        });
        actionfetchAcc.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                component.set("v.numAccs", response.getReturnValue().length);
                this.configPageAcc(component, e);
            }
        });
        $A.enqueueAction(actionfetchAcc);

        var fetchAccList = component.get('c.fetchAcc');
        var listparamsAOffset = [tipoAccA, tipoQA, parametroA, offsetVal];
        fetchAccList.setParams({
            params: listparamsAOffset
        });
        fetchAccList.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                component.set('v.hasListValue', true);
            }else {
                component.set('v.hasListValue', false);
            }
            component.set('v.ListOfAccount', response.getReturnValue());
        });
        $A.enqueueAction(fetchAccList);
    },

    configPageAcc: function(component, e) {
        component.set('v.NumOfPag', 0);
        component.set('v.ActPag', 1);
        var fetchPgsAcc = component.get('c.fetchPgs');
        var totalrecordAcc = component.get('v.numAccs') ? component.get('v.numAccs') : 0;
        fetchPgsAcc.setParams({
            numRecords : totalrecordAcc
        });
        fetchPgsAcc.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				component.set('v.hasListValue', true);
                component.set('v.NumOfPag', response.getReturnValue());
                component.set('v.ActPag', 1);
                window.sizeofAcc = 10;
            }else {
                component.set('v.hasListValue', false);
            }
        });
        $A.enqueueAction(fetchPgsAcc);
    },

    pagefunctionCont: function (component, pgMovAcc, conditionAcc) {
		var parametroA = this.gttypoQuery(component);
        var tipoQA = this.tipoQueryF(component);
        var tipoAccA = component.find("pklTyInfBpyP").get("v.value");
		var listparamsA = [tipoAccA, tipoQA, parametroA, pgMovAcc];

        var fetchAccsList = component.get('c.fetchAcc');
        fetchAccsList.setParams({
            params: listparamsA
        });
        fetchAccsList.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
            	component.set('v.hasListValue', true);
                if(conditionAcc) {
                    pgMovAcc = pgMovAcc / window.sizeofAcc;
                    pgMovAcc++;
                    component.set('v.ActPag', pgMovAcc);
                } else {
                    component.set('v.ActPag', 1);
                }
            }else {
                component.set('v.hasListValue', false);
            }
            component.set('v.ListOfAccount', response.getReturnValue());
        });
        $A.enqueueAction(fetchAccsList);
    }
})