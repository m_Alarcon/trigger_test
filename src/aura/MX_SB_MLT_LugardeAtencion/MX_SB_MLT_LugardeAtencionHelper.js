({
    generaCita : function(component,event,helper){
        var toastEvent = $A.get("e.force:showToast");
        component.get('v.LugarAtencionValue');
        var action= component.get("c.creaServicio");
        if(component.find("idfecha").get("v.value") === null || component.find("idfecha").get("v.value") === '') {
            toastEvent.setParams({
                "title": "Error!",
                "type": "error",
                "message": "Debe seleccionar una fecha para agendar la cita"
            });
            toastEvent.fire();
        } else {
            var dataservice = {
				"fecha": component.find("idfecha").get("v.value"),
				"idSin": component.get("v.recordId")
            };
            action.setParams({"data":JSON.stringify(dataservice)});
            action.setCallback(this , function(response) {
                if (response.getState() === "SUCCESS") {
                    toastEvent.setParams({
                        mode: 'sticky',
                        message: 'Cita',
                        messageTemplate: 'La cita se agendó con fecha {0}',
                        messageTemplateData: [$A.localizationService.formatDate(component.find("idfecha").get("v.value")	, "dd MMMM yyyy, hh:mm:ss a") ]
                    });
                    toastEvent.fire();
                } else {
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Ocurrio un error al generar la cita"
                    });
                    toastEvent.fire();
                }
            });
        }
		$A.enqueueAction(action);
    },
     generaServicioCristales : function(component,event,helper){
        var toastEvent = $A.get("e.force:showToast");
        var recordId = component.get("v.recordId");
        var proveedorId = component.get("v.ajustadorSFId");
        if (proveedorId === undefined) {
            proveedorId = '';
        }
        var atencion = component.get("v.SinRecord.MX_SB_MLT_Lugar_de_Atencion_Cristalera__c");
        var action= component.get("c.generaServicioProveedor");
        if(atencion === 'Cristalera') {
            if(proveedorId.length === 0 ) {
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Debe seleccionar un proveedor"
                });
                toastEvent.fire();
            } else {
                action.setParams({"recordId":recordId,"proveedorId":proveedorId,});
                action.setCallback(this , function(response) {
                    this.methodCristalera(response);
                });
                $A.enqueueAction(action);
            }
        } else {
            if(proveedorId.length === 0) {
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Debe seleccionar un Ajustador"
                });
                toastEvent.fire();
            } else {
                action.setParams({"recordId":recordId,"proveedorId":proveedorId});
                action.setCallback(this , function(response) {
                    this.methodCristalera(response);
                });
                $A.enqueueAction(action);
            }
        }
    },
    checkSelectedContract : function(component, event, helper) {
        let recordId = component.get("v.recordId");
        let messageError = $A.get("e.force:showToast");
        var action = component.get("c.getDatosSiniestro");
        action.setParams({"idsini":recordId});
            action.setCallback(this,function(response){
                if (response.getState() === "SUCCESS") {
                    let contract = response.getReturnValue().MX_SB_SAC_Contrato__c;
                    if (contract != null) {
                        helper.validaCris(component,event,helper);
                    } else {
                        messageError.setParams({
                            "title": "No puede generar el servicio",
                            "type": "error",
                            "message": "Debe seleccionar una poliza para continuar"
                        });
                        messageError.fire();
                    }
                }
            });
        $A.enqueueAction(action);
    },
      methodCristalera: function(resultadomethod) {
        var toastEvent = $A.get("e.force:showToast");
        if (resultadomethod.getState() === "SUCCESS") {
            let resultado = resultadomethod.getReturnValue();
            if(resultado) {
                toastEvent.setParams({
                    title : 'Exito al Crear el Servicio',
                    message: 'Se ha generado el servicio exitosamente',
                    type: 'success'
                });
                toastEvent.fire();
                window.location.reload();
            } else {
                toastEvent.setParams({
                    title: "Error!",
                    type: "error",
                    message: "Ya se ha generado el Servicio"
                });
                toastEvent.fire();
            }
        }
    },
    validaCris: function(component,event,helper) {
        var solCris = component.find("solCris");
        solCris.reportValidity();
        solCris= solCris.get('v.value');
        let correo = component.find("correoCas");
        correo.reportValidity();
        let value = component.find("correoCas").get('v.value');
        const verification = '([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})';
        if(!$A.util.isEmpty(value)) {
            if (value.match(verification) && solCris!=='') {
                helper.generaServicioCristales(component,event,helper);
            }
        }
    },
})