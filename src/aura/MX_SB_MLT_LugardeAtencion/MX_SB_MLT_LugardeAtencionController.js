({
  doInitLugarAtn : function(component) {
	    var action = component.get("c.getRecordType");
        action.setParams({"devname":"MX_SB_MLT_RamoAuto"});
        action.setCallback(this, function(response) {
            component.set("v.recordtypeAUTO",response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    handleSuccessLugarAtn : function(component, event) {
	var payload = event.getParams().response;
        var navService = component.find("navService");
        var pageReference = {
            type: 'standard__recordPage',
            attributes: {
                "recordId": payload.id,
                "objectApiName": component.get("v.sObjectName"),
                "actionName": "view"
            }
        }
        event.preventDefault();
        navService.navigate(pageReference);
    },
    checkList : function(component) {
	   var recordId = component.get('v.recordId');
	   var action = component.get('c.getDatosSiniestro');
        action.setParams({
            'idsini': recordId
        });
        action.setCallback(this, function(response) {
            component.set("v.LugarAtencionValue",response.getReturnValue().MX_SB_MLT_LugarAtencionAuto__c);
        });
        $A.enqueueAction(action);
    },
    generarCita : function(component,event,helper){
		helper.generaCita(component,event,helper);
    },
    generaServicioCristal : function(component,event,helper){
        helper.checkSelectedContract(component, event, helper)
    },
     buscaAjustador :function(component) {
        component.set("v.busquedaVisible",true);
    },
    cierreBusqueda : function(component,event) {
        var nombre = event.getParam("nombre");
        var idSales = event.getParam("idSalesforce");
        var idClaim = event.getParam("idClaim");
        component.set('v.ajustadorNombre',nombre);
        component.set('v.ajustadorSFId',idSales);
        component.set('v.ajustadorCCId',idClaim);
        component.set("v.busquedaVisible",false);
    },
})