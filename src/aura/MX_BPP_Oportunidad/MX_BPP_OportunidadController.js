({
	doInit : function(component, event, helper) {
        helper.helperInit(component, event);
	},

    generateChart : function(component, event, helper){
        helper.helperGenerateChart(component, event, helper);
    },

    updOpAc : function(component, e, helper){
        helper.combosSet(component, e);
        helper.draw(component, e);
        component.set('v.hasListValue', false);
        component.set('v.ListOfOpp', []);
        if(component.get("v.selbkm") !== 'TODAS') {
            helper.fetchopps(component, e, 0);
        }
    },

    updDiv: function (component, e, helper) {
        component.set("v.seloff", "TODAS");
        component.set("v.selbkm", "TODAS");
        component.set('v.hasListValue', false);
        component.set('v.ListOfOpp', []);
        helper.combosSet(component, e);
        helper.draw(component, e);
    },

    updOffice: function (component, e, helper) {
        component.set("v.selbkm", "TODAS");
        component.set('v.hasListValue', false);
        component.set('v.ListOfOpp', []);
        helper.combosSet(component, e);
        helper.draw(component, e);
    },

    updBkM: function (component, e, helper) {
        component.set('v.hasListValue', false);
        component.set('v.ListOfOpp', []);
        helper.combosSet(component, e);
        helper.draw(component, e);
        if(component.get("v.selbkm") !== 'TODAS') {
            helper.fetchopps(component, e, 0);
        }
    },

    frst: function (component, e, helper) {
        helper.pagefunctionCont(component, 0);
    },

    next: function (component, e, helper) {
        var actPg = component.get('v.ActPag');
        var totPgO = component.get('v.NumOfPag');
        var nxt = actPg === totPgO ? totPgO : actPg + 1;
        nxt--;
        nxt = nxt * window.sizeofAcc;
        helper.pagefunctionCont(component, nxt, true);
    },

    prev: function (component, e, helper) {
        var actPgprev = component.get('v.ActPag');
        var prvO = actPgprev === 1 ? 1 : actPgprev - 1;
        prvO--;
        prvO = prvO * window.sizeofAcc;
        helper.pagefunctionCont(component, prvO, true);
    },

    lst: function (component, e, helper) {
        var totPgO = component.get('v.NumOfPag');
        totPgO--;
        totPgO = totPgO * window.sizeofAcc;
        helper.pagefunctionCont(component, totPgO, true);
    }
})