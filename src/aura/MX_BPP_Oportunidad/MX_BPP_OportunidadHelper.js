({
	helperInit : function(component, event) {
        component.set("v.baseurl", window.location.origin + "/");
        var actionfetchInfoByUserO = component.get('c.fetchInfoByUser');
        actionfetchInfoByUserO.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnWrapperO = response.getReturnValue();
                component.set('v.wrapperObject', returnWrapperO);
                component.set('v.ListOfDiv', returnWrapperO.setDivisiones);
            }
        });
        $A.enqueueAction(actionfetchInfoByUserO);

        if (component.get('v.bandera')) {
            component.set('v.bandera', true);
        }

        component.set("v.accessCombos" , {
            "Administrador del sistema" : {"div":true, "off":true, "bnk":true },
            "Soporte BPyP" : {"div":true, "off":true, "bnk":true },
            "BPyP STAFF" : {"div":true, "off":true, "bnk":true },
            "BPyP Director Divisional" : {"div":false, "off":true, "bnk":true },
            "BPyP Director Oficina" : {"div":false, "off":false, "bnk":true },
            "BPyP Estandar" : {"div":false, "off":false, "bnk":false }
        } );
	},

    helperGenerateChart: function (component, e, helper) {
		window.dataToTable = function (dataset, title) {
            var htmlO = '<table>';
            htmlO += '<thead><tr><th >' + title + '</th>';
            var columnCount = 0;
            jQuery.each(dataset.datasets, function (idx, item) {
                htmlO += '<th style="background-color:' + item.fillColor + ';">' + item.label + '</th>';
                columnCount += 1;
            });
            htmlO += '</tr></thead>';
            jQuery.each(dataset.labels, function (idx, item) {
                htmlO += '<tr><td>' + item + '</td>';
                for (var i = 0; i < columnCount; i++) {
                    htmlO += '<td style="background-color:' + dataset.datasets[i].fillColor + ';">'
                    + (dataset.datasets[i].data[idx] === '0' ? '-' : dataset.datasets[i].data[idx])
                    + '</td>';
                }
                htmlO += '</tr>';
            });
            htmlO += '</tr><tbody></table>';
            return htmlO;
        };
        window.barOptions_stacked = {
            tooltips: { enabled: true,
                callbacks: {
                    afterLabel: function (tooltipItem, data) { var ts = [];
                        ts = ([window.amm[tooltipItem.datasetIndex][tooltipItem.index]].toString()).split("ralm ");
                        return ts;
                    },
                }
            },
            hover: {animationDuration: 10},
            legend: {position: 'bottom', padding: 10, onClick: function (event, legendItem) { }, },
            animation: {
                onComplete: function () {
                    var chartInstance = this.chart;
                    var ctx = chartInstance.ctx;
                    ctx.textAlign = "left";
                    ctx.fillStyle = "#fff";
                    Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        Chart.helpers.each(meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            if (data !== 0) { ctx.fillText(data, bar._model.x - 20, bar._model.y - 15); }
                        }), this)
                    }), this);
                }
            }, responsive: true, maintainAspectRatio: false,
            scales: {
                xAxes: [{stacked: true, gridLines: { display: false }, }],
                yAxes: [{stacked: true, gridLines: {display: false}, }]
            },
        };

        component.set('v.IsSpinner',true);
        window.setTimeout(
            $A.getCallback(function() {
                helper.initdraw(component, e);
                component.set('v.IsSpinner',false);
            }), 5000
        );
    },

    initdraw: function (component, e) {
        var actionfetchUserInfoO = component.get('c.fetchUserInfo');
        actionfetchUserInfoO.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                this.verifyPerfil(component, e, response);
            }
        });
        $A.enqueueAction(actionfetchUserInfoO);
    },

    verifyPerfil: function (component, e, response) {
        var perfilO = String(response.getReturnValue()).split(',')[0];
        var divisionO = String(response.getReturnValue()).split(',')[1];
        var oficinaO = String(response.getReturnValue()).split(',')[2];
        var usrname = String(response.getReturnValue()).split(',')[3];
        var title = String(response.getReturnValue()).split(',')[4];

        var bpypestandar = "BPyP Estandar";
        var diroff = "BPyP Director Oficina";
        var dirdiv = "BPyP Director Divisional";
        var staff = "BPyP STAFF";
        var SoportePerfil ="Soporte BPyP";
        var adm = $A.get("$Label.c.MX_PERFIL_SystemAdministrator");

        component.set("v.userPInfo", perfilO);
        component.set("v.accessDiv", !component.get("v.accessCombos")[perfilO].div);
        component.set("v.accessOff", !component.get("v.accessCombos")[perfilO].off);
        component.set("v.accessBnk", !component.get("v.accessCombos")[perfilO].bnk);
        component.set("v.accessTf", !component.get("v.accessCombos")[perfilO].tf);

        if (perfilO === (staff) || perfilO === (adm) || perfilO === (SoportePerfil)) {
            component.set("v.seldiv","TODAS");
            this.draw(component, e);
            this.combosSet(component, e);
        } else if (perfilO === dirdiv) {
            component.set('v.isDiv', true);
            component.set('v.seldiv', divisionO);
            component.set("v.seloff","TODAS");
			this.draw(component, e);
            this.combosSet(component, e);
        } else if (perfilO === diroff) {
            component.set('v.isOff', true);
            component.set('v.seldiv', divisionO);
            component.set('v.seloff', oficinaO);
            component.set('v.userInfoDO', {"name" : usrname,
                             "division" : divisionO,
                              "oficina" : oficinaO});

            this.configDOffOpp(component, e);
            this.combosSet(component, e);
        } else if (perfilO === bpypestandar) {
            component.set('v.isBkm', true);
            component.set("v.seltitle", title);

            component.set('v.seldiv', divisionO);
            component.set('v.ListOfDiv', [divisionO]);

            component.set('v.seloff', oficinaO);
            component.set('v.ListOfOff', [oficinaO]);

            component.set('v.selbkm', usrname);
            component.set('v.ListOfBkMn', [{'Name':usrname}]);
            component.set('v.vistaBnk', false);

            this.draw(component, e);
        }
    },

    configDOffOpp: function(component, e) {
        var tpConsulta = ''
        if (component.get('v.bandera')) {
            component.set("v.accessBnk", true);
            tpConsulta = 'BanqueroOnly';
        } else {
            tpConsulta = 'Banquero';
            component.set('v.selbkm', 'TODAS');
        }
        this.draw(component, e, tpConsulta);
    },

    combosSet: function (component, e) {
        if(component.get('v.vistaBnk')) {
            var wrapperObjO = component.get('v.wrapperObject');
            var divSelectedO = component.get('v.seldiv');
            var offSelectedO = component.get('v.seloff');
            component.set('v.ListOfOff', wrapperObjO.mapSucursales[divSelectedO]);
            var listOfBkMnO = wrapperObjO.maplistUserName[divSelectedO+offSelectedO];

            if(component.get('v.userInfoDO') && component.get('v.userInfoDO')['oficina'] === offSelectedO && !listOfBkMnO.includes(component.get('v.userInfoDO')['name'])) {
                listOfBkMnO.push(component.get('v.userInfoDO')['name']);
            }
            component.set('v.ListOfBkMn', listOfBkMnO);
        }
    },

    gttypoQuery: function(component) {
      let parametro = '';
        let tipoQuery = this.tipoQueryF(component);
        if(tipoQuery === 'Oficina'){
            parametro = component.get("v.seldiv");
        } else if(tipoQuery === 'Banquero') {
            parametro = component.get("v.seloff");
        } else if(tipoQuery === 'BanqueroOnly') {
            parametro = component.get("v.selbkm");
        }
        return parametro;
    },

    tipoQueryF: function (component) {
        var  tipo = "";
        if(component.get("v.seldiv") === 'TODAS') {
            tipo = "Division";
        } else if(component.get("v.seloff") === 'TODAS') {
            tipo = "Oficina";
        } else if(component.get("v.selbkm") === 'TODAS') {
            tipo = "Banquero";
        } else {
            tipo = "BanqueroOnly";
        }
        return tipo;
    },

    draw: function (component, e) {
        var labelTypeQuery = {'Division' : 'DIVISIóN', 'Oficina' : 'OFICINA', 'Banquero' : 'BANQUERO', 'BanqueroOnly' : 'BANQUERO'};
        var ctxOpp = component.find("myChartOpp").getElement();
        var tipoQ = this.tipoQueryF(component);
        var parametro = this.gttypoQuery(component);
        var tipoOpp = component.find("pklTyOppBpyP").get("v.value");
        var tipoAcc = component.find("pklTyAccBpyP").get("v.value");
		var listparams = [tipoAcc, tipoOpp, tipoQ, parametro];

        var actionfetchDataOpp = component.get('c.fetchDataOpp');
        actionfetchDataOpp.setParams({
            params: listparams,
            sDate: component.find("expsdate").get("v.value"),
            eDate: component.find("expedate").get("v.value")
        });
        actionfetchDataOpp.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                var height = 100 + (res.lsLabels.toString().split(',').length * 30);
                component.set('v.chartHeight', height);
                if (window.myChartOpp != null) {
                    window.myChartOpp.clear();
                    window.myChartOpp.destroy();
                }
                window.myChartOpp = new Chart(ctxOpp, { type: 'horizontalBar', data: {}, options: window.barOptions_stacked });
                this.actionHelper(component, res, labelTypeQuery[tipoQ]);
            }
        });
        $A.enqueueAction(actionfetchDataOpp);
    },

    fetchopps: function (component, e, offsetVal) {
        var fetchOpp = component.get('c.fetchOpp');
        var listparams = [component.get("v.selbkm"),
                          component.find("pklTyAccBpyP").get("v.value"),
                          component.find("pklTyOppBpyP").get("v.value"), ''];
        fetchOpp.setParams({
            params: listparams,
            sDate: component.find("expsdate").get("v.value"),
            eDate: component.find("expedate").get("v.value")
        });
        fetchOpp.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                component.set("v.numOpps", response.getReturnValue().length);
                this.configPageOpp(component, e);
            }
        });
        $A.enqueueAction(fetchOpp);

        var fetchOppList = component.get('c.fetchOpp');
        var listparamsOffset = [component.get("v.selbkm"),
                          component.find("pklTyAccBpyP").get("v.value"),
                          component.find("pklTyOppBpyP").get("v.value"),
                          offsetVal];
        fetchOppList.setParams({
            params: listparamsOffset,
            sDate: component.find("expsdate").get("v.value"),
            eDate: component.find("expedate").get("v.value")
        });
        fetchOppList.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                component.set('v.hasListValue', true);
            }else {
                component.set('v.hasListValue', false);
            }
            component.set('v.ListOfOpp', response.getReturnValue());
        });
        $A.enqueueAction(fetchOppList);
    },

    configPageOpp: function(component, e) {
        component.set('v.NumOfPag', 0);
        component.set('v.ActPag', 1);
        var fetchPgsOpp = component.get('c.fetchPgs');
        var totalrecordOpp = component.get('v.numOpps') ? component.get('v.numOpps') : 0;
        fetchPgsOpp.setParams({
            numRecords : totalrecordOpp
        });
        fetchPgsOpp.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				component.set('v.hasListValue', true);
                component.set('v.NumOfPag', response.getReturnValue());
                component.set('v.ActPag', 1);
                window.sizeofAcc = 10;
            }else {
                component.set('v.hasListValue', false);
            }
        });
        $A.enqueueAction(fetchPgsOpp);
    },

    actionHelper: function (component, res, types) {
        var labels = [];
        for (var i = res.lsLabels.toString().split(',').length - 1; i >= 0; i--) {
            myChartOpp.config.data.labels.push(res.lsLabels.toString().split(',')[i]);
            labels.push(res.lsLabels.toString().split(',')[i]);
        }
        window.amm = [];
        for (var j = res.lsTyOpp.toString().split(',').length - 1; j >= 0; j--) {
            var datatempO = [];
            var dataammO = [];
            for (var k = 0; k < labels.length; k++) {
                datatempO.push(res.lsData[(res.lsTyOpp.toString().split(',')[j] + labels[k])] == null ? 0 : res.lsData[(res.lsTyOpp.toString().split(',')[j] + labels[k])]);
                dataammO.push(res.lsTool[(res.lsTyOpp.toString().split(',')[j] + labels[k])] == null ? '' : res.lsTool[(res.lsTyOpp.toString().split(',')[j] + labels[k])]);
            }
            window.amm.push(dataammO);

            var newDatasetO = { label: res.lsTyOpp.toString().split(',')[j], backgroundColor: res.lsColor.toString().split('),')[j] + ')', data: datatempO, }
            myChartOpp.data.datasets.push(newDatasetO);
        }
        myChartOpp.update();
        component.set('v.DateOfUpd', new Date().getDate() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getFullYear());
        jQuery('#wrapperOpps').html(window.dataToTable(myChartOpp.data, types));
    },

    pagefunctionCont: function (component, pgMovOpp, conditionOpp) {
        var fetchOppList = component.get('c.fetchOpp');
		var listparams = [component.get("v.selbkm"),
                          component.find("pklTyAccBpyP").get("v.value"),
                          component.find("pklTyOppBpyP").get("v.value"),
                          pgMovOpp.toString()];

        fetchOppList.setParams({
            params: listparams,
            sDate: component.find("expsdate").get("v.value"),
            eDate: component.find("expedate").get("v.value")
        });
        fetchOppList.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
            	component.set('v.hasListValue', true);
                if(conditionOpp) {
                    pgMovOpp = pgMovOpp / window.sizeofAcc;
                    pgMovOpp++;
                    component.set('v.ActPag', pgMovOpp);
                } else {
                    component.set('v.ActPag', 1);
                }
            }else {
                component.set('v.hasListValue', false);
            }
            component.set('v.ListOfOpp', response.getReturnValue());
        });
        $A.enqueueAction(fetchOppList);
    },
})