({
    AddNewRow : function(component, event, helper) {
        component.getEvent("AddRowEvt").fire();
    },

    removeRow : function(component, event, helper) {
       component.getEvent("DeleteRowEvt").setParams({"indexVar" : component.get("v.rowIndex") }).fire();
    },
    allowDrop: function(component, event, helper) {
        event.preventDefault();
    },

    drag: function (component, event, helper) {
        event.dataTransfer.setData("text", event.target.id);
    },
})