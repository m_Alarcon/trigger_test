({
    doInitLayGMM : function(component, event, helper) {
        var action = component.get("c.getRecordType");
	action.setParams({"devname":"MX_SB_MLT_RamoGMM"});
        action.setCallback(this, function(response) {
            component.set("v.recordtypeGMM",response.getReturnValue());
        });
        $A.enqueueAction(action);
	},
    handleSuccessLayGMM : function(component, event, helper) {
        var payload = event.getParams().response;
        var navService = component.find("navService");
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Creado!",
            "message": "Su registro a sido creado.",
            "type": "success"
        });
        var pageReference = {
            type: 'standard__recordPage',
            attributes: {
                    "recordId": payload.id,
                    "objectApiName": "Siniestro__c",
                    "actionName": "view"
                }
     }
    toastEvent.fire();
    navService.navigate(pageReference);
    }
})