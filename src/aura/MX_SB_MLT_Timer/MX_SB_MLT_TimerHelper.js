({
    timeElapsed : function(component, event, helper,sinCreateDt) {
                var action = component.get("c.fetchSinLastMDate");
        action.setParams({
            "recId" : component.get("v.recordId")
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                if(response.getReturnValue().length<1) {
                    component.set('v.endDate',null);
                }
                else {
                	component.set('v.endDate',response.getReturnValue()[0].CreatedDate);
                }
            }
        });
            $A.enqueueAction(action);
        var interval = window.setInterval(
            $A.getCallback(function() {
                if(component.get('v.endDate')==null) {
                    var now_date = new Date();
                    var timeDiff = now_date.getTime()-sinCreateDt.getTime();
                    var seconds=Math.floor(timeDiff/1000);
                    var minutes=Math.floor(seconds/60);
                    var hours=Math.floor(minutes/60);
                    var days=Math.floor(hours/24);
                    hours %=24;
                    minutes %=60;
                    seconds %=60;
                    component.set("v.day",days);
                    component.set("v.hour",hours);
                    component.set("v.minute",minutes);
                    component.set("v.second",seconds);
                } else {
                    var now_date2 =new Date(component.get('v.endDate'));
                    var timeDiff2 = now_date2.getTime()-sinCreateDt.getTime();
                    var seconds2=Math.floor(timeDiff2/1000);
                    var minutes2=Math.floor(seconds2/60);
                    var hours2=Math.floor(minutes2/60);
                    var days2=Math.floor(hours2/24);
                    hours2 %=24;
                    minutes2 %=60;
                    seconds2 %=60;
                    component.set("v.day",days2);
                    component.set("v.hour",hours2);
                    component.set("v.minute",minutes2);
                    component.set("v.second",seconds2);
                }
            }), 1000);
            component.set("v.interval",interval);
    },
})