({
    doInitTimer : function(component, event, helper) {
        let action = component.get("c.fetchSinCrtDate");
        action.setParams({
            "recId" : component.get("v.recordId")
        });
        action.setCallback(this,function(response){
            let state = response.getState();
            if(state === 'SUCCESS'){
                var result = response.getReturnValue()[0].CreatedDate;
                var sinCreateDt = new Date(result);
                var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
				var monthName = months[sinCreateDt.getMonth()];
                var dateNumber = sinCreateDt.getDate();
                var yearNumber =  sinCreateDt.getFullYear();
                var sinCreateDtVar = monthName+' '+dateNumber+' '+yearNumber;
                component.set("v.sinCreateDtVar",sinCreateDtVar);
				helper.timeElapsed(component, event, helper, sinCreateDt);
            }
        });
        $A.enqueueAction(action);
    }
})