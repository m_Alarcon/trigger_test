({
    getCampanas: function (cmp) {

        var action = cmp.get("c.getListCampanas");
        action.setParams({ pbrief : cmp.get("v.recordId")});
        action.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
                cmp.set('v.data', response.getReturnValue());
            }
            else {
                console.log('error ' + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },
    getArtes: function (cmp) {
        cmp.set('v.loaded', true);
        var action = cmp.get("c.getArtesDup");
        action.setParams({  campanias : cmp.get("v.reg_seleccionados"),
                            pbrief : cmp.get("v.recordId")});
        action.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
                   var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: "Registro Exitoso",
                        message: "Los canales fueron creados exitosamente.",
                        type: "success"
                    });
                    toastEvent.fire();
                 var dismissActionPanel = $A.get("e.force:closeQuickAction");
                 dismissActionPanel.fire();
                 var refreshEvent = $A.get("e.force:refreshView");
                 refreshEvent.fire();
            }
            else {
                console.log('error ' + JSON.stringify(response.getError()));
                    var toastErrorE = $A.get("e.force:showToast");
                    toastErrorE.setParams({
                        title: "Error",
                        message: "Ocurrió un error al crear los canales.",
                        type: "error"
                    });
                    toastErrorE.fire();
                    var closequicka = $A.get("e.force:closeQuickAction");
                    closequicka.fire();
            }
        });
        $A.enqueueAction(action);
    }
})