({
    init: function (cmp, event, helper) {
        cmp.set('v.columns', [
            {label: 'Campaña', fieldName: 'Name', type: 'text'},
            {label: 'FIC', fieldName: 'CRM_WF_FIC__c', type: 'text'},
            {label: 'Fecha inicio de vigencia', fieldName: 'StartDate', type: 'date'},
            {label: 'Fecha fin de vigencia', fieldName: 'EndDate', type: 'date'},
            {label: 'Fecha de publicación', fieldName: 'MX_WF_Fecha_de_publicacion__c', type: 'date'}
        ]);
        helper.getCampanas(cmp);
    },
    regSelected: function (cmp, event) {

       var rowsselected = event.getParam('selectedRows');
       cmp.set('v.reg_seleccionados', '');
        var resultado = cmp.get('v.reg_seleccionados');
        if(rowsselected!==null && rowsselected!=='') {
            resultado[0] = rowsselected[0].Id;
            var i;
            for (i=1;i<rowsselected.length;i++) {
                resultado.push(rowsselected[i].Id);
            }
            cmp.set('v.reg_seleccionados', resultado);
        }
    },
    handleClick: function (cmp, event, helper) {
        helper.getArtes(cmp);
    },
    handleClickCanc : function (cmp, event, helper) {
       var dismissActionPanel = $A.get("e.force:closeQuickAction");
       dismissActionPanel.fire();
    }
})