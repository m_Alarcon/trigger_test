({
	init : function(cmp, event, helper) {
        cmp.set('v.columns', [
            { label: 'Nombre', fieldName: 'urlAcc', type: 'url',
                typeAttributes: {label: {fieldName: 'accNombre'},target: '_self'}, cellAttributes: { alignment: 'left' }, sortable: true },
            { label: 'Folio', fieldName: 'urlCase', type: 'url', typeAttributes: {label: {fieldName: 'caseFolio'},target: '_self'}, cellAttributes: { alignment: 'left'} },
            { label: 'Fecha de alta', fieldName: 'caseFechaCrea', type: 'date-local', cellAttributes: { alignment: 'left'}, sortable: true },
            { label: 'Producto', fieldName: 'aclaraProd', type: 'text', cellAttributes: { alignment: 'left'}, sortable: true },
            { label: 'Terminación', fieldName: 'aclaraTerm', type: 'text', cellAttributes: { alignment: 'left'} },
            { label: 'Razón', fieldName: 'caseDesc', type: 'text', cellAttributes: { alignment: 'left'} },
        ]);

        helper.getData(cmp);
	},

    handleSearch : function (cmp, event, helper) {
        cmp.set("v.data", cmp.get("v.initialData"));
        let nombreCuenta = cmp.get("v.selectedValue");
        let fechaInicio = cmp.get("v.valueMinDate");
        let fechaFin = cmp.get("v.valueMaxDate");

        helper.searchLocal(cmp, event, helper, nombreCuenta, fechaInicio, fechaFin);
    },

    handleSort : function (cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        // assign the latest attribute with the sorted column fieldName and sorted direction
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        helper.sortData(cmp, fieldName, sortDirection);
    },

    updateSelected: function (cmp, event, helper) {
        let selectedRows = event.getParam('selectedRows');
        let casesIds = [];

        for(let caseU in selectedRows) {
            casesIds.push({caseId: selectedRows[caseU].caseId, accNombre: selectedRows[caseU].accNombre});
        }
        cmp.set("v.selectedData", casesIds);
    },

    handleClick: function (cmp, event, helper) {
        let selectedCases = cmp.get("v.selectedData");
        let diff = false;
        let casesIds = [];

        if(selectedCases.length > 0) {
            selectedCases.forEach(function(item) {
                casesIds.push(item['caseId']);
                if(selectedCases[0].caseId !== item['caseId']) {
                    diff = true;
                }
            });

            if(diff) {
                helper.fireToast('Error', 'Seleccione aclaraciones de un solo usuario', 'error');
            } else {
                cmp.set("v.toggleSpinner", true);
                helper.createVisit(cmp, casesIds);
            }
        } else {
            helper.fireToast('Error', 'Seleccione por lo menos una aclaración', 'error');
        }
    },

    handleAction: function (cmp, event, helper) {
        let action = cmp.get("v.actionButton");
        let newOwner = cmp.get("v.selectedUser");
        let selectedAccs = cmp.get("v.selectedData");

        cmp.set("v.toggleSpinner", true);

        if(action === "brand") {
            helper.saveNewOwner(cmp, newOwner, selectedAccs);
        } else {
            helper.discardAcc(cmp, selectedAccs);
        }
    }
})