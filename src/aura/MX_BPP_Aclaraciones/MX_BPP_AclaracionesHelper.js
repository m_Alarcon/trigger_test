({
	getData : function(cmp) {
        let actionData = cmp.get('c.retrieveCases');
        actionData.setCallback(this, $A.getCallback(function (response) {
            let state = response.getState();
            let picklist = new Set();
            if (state === "SUCCESS") {
                let res = response.getReturnValue();
                res.forEach(function(item) {
                    item['urlAcc'] = '/lightning/r/Account/' + item['accId'] + '/view';
                    item['urlCase'] = '/lightning/r/Case/' + item['caseId'] + '/view';
                    picklist.add(item['accNombre']);
                });
                cmp.set('v.data', res);
                cmp.set('v.initialData', response.getReturnValue());

                let unique = [{value: ''}]
                picklist.forEach(function(item) {
                    unique.push({value: item});
                });

                cmp.set("v.valueNombre", unique);
            }
        }));
        $A.enqueueAction(actionData);
    },

    sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get("v.data");
        var reverse = sortDirection !== 'asc';
        //sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse))
        cmp.set("v.data", data);
    },

    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
            function(x) {return x[field]};
        reverse = reverse ? 1 : -1;
        return function (a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        }
    },

    searchLocal: function(cmp, event, helper, nombreCuenta, fechaInicio, fechaFin) {
        let data = cmp.get("v.data");
        let searchRes = [];

        if(nombreCuenta !== null) {
            searchRes = this.searchName(cmp, event, helper, nombreCuenta, data);
        }
        if(fechaInicio !== null) {
            searchRes = this.searchDate(cmp, event, helper, fechaInicio, searchRes.length > 0 ? searchRes : data, 'inicio');
        }
        if(fechaFin !== null) {
            searchRes = this.searchDate(cmp, event, helper, fechaFin, searchRes.length > 0 ? searchRes : data, 'fin');
        }

        cmp.set("v.data", searchRes);
        if(nombreCuenta === '') {
            cmp.set("v.data", cmp.get("v.initialData"));
        }
    },

    searchName: function(cmp, event, helper, nombreCuenta, data) {
        let returnList = [];
        data.forEach(function(item) {
            if(item['accNombre'] === nombreCuenta) {
                returnList.push(item);
            }
        });

        return returnList;
    },

    searchDate: function(cmp, event, helper, fecha, data, type) {
        let returnList = [];

        if(type === 'inicio') {
            data.forEach(function(item) {
                if(item['caseFechaCrea'] >= fecha) {
                    returnList.push(item);
                }
            });
        } else if(type === 'fin') {
            data.forEach(function(item) {
                if(item['caseFechaCrea'] <= fecha) {
                    returnList.push(item);
                }
            });
        }

        return returnList;
    },

    createVisit: function(cmp, caseList) {
        cmp.set("v.showModal", false);

        let actionOwner = cmp.get('c.createNewVisit');
        actionOwner.setParams({ caseIds : caseList});
        actionOwner.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.toggleSpinner", false);
                this.fireToast("Visita creada", "La visita se ha creado exitosamente.", "success");
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": response.getReturnValue(),
                    "slideDevName": "detail"
                });
                navEvt.fire();
            }
        }));
        $A.enqueueAction(actionOwner);
    },

    fireToast: function(title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": title,
                "message": message,
                "type": type
            });
            toastEvent.fire();
    }
})