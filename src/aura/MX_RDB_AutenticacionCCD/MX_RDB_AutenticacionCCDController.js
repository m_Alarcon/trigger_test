/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RDB_AutenticacionCCDController.js
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2019-10-28
* @Group  		MX_RDB_AutenticacionCCD
* @Description 	JS Controller from MX_RDB_AutenticacionCCD Lightning Component
* @Changes
*
*/
({
    doInit: function(component, event, helper) {
        //Component's attributes initialization
        helper.getClienteData(component);
        helper.getGTicket(component);
        helper.getRNumber(component);
    },

	validarAutenticacion : function(component, event, helper) {
        //Fires the method that validates client authentication
        helper.validarAuthWS(component);
	}
})