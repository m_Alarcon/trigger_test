/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_RDB_AutenticacionCCDHelper.js
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2019-10-28
* @Group  		MX_RDB_AutenticacionCCD
* @Description 	JS Helper from MX_RDB_AutenticacionCCD Lightning Component
* @Changes
*
*/
({
    getClienteData : function(component) {
        // Call apex class method
        var action = component.get("c.getClienteData");
        action.setParams({ oppID : component.get("v.recordId") });
        action.setCallback(this, function(data) {
            // Set response value in ClienteData attribute on component
            component.set("v.ClienteData", data.getReturnValue());
        });
        $A.enqueueAction(action);
    },

    getGTicket : function(component) {
        // Call apex class method
        var action = component.get("c.rGTicket");
        action.setCallback(this, function(data) {
            // Set response value in GTicket attribute on component
            component.set("v.GTicket", data.getReturnValue());
        });
        $A.enqueueAction(action);
    },

    getRNumber : function(component) {
        // Call apex class method
        var action = component.get("c.rRNumber");
        action.setCallback(this, function(data) {
            // Set response value in RNumber attribute on component
            component.set("v.RNumber", data.getReturnValue());
        });
        $A.enqueueAction(action);
    },

    validarAuthWS : function(component) {
        var authMessage = $A.get("$Label.c.MX_RDB_AuthErrorMsg");
        var authStatus = false;
        var outputRM = component.find('outputRM');
        // Get values to be send to the methods for WS payloads
        var recClienteId = component.get("v.ClienteData.MX_SB_BCT_Id_Cliente_Banquero__c");
        var dataGT = component.get("v.GTicket");
        var dataSR = component.get("v.RNumber");
        // Get WS payloads (jsonGT grantingTicket | jsonSR getShortReferenceNumber)
        var jsonGT = this.gratingTicketData(dataGT);
        var jsonSR = this.shortReferenceData(dataSR, recClienteId);
        var requestGT = new XMLHttpRequest();
        var requestSR = new XMLHttpRequest();
        var urlGT = dataGT.MX_RDB_URL__c;
        var urlSR = dataSR.MX_RDB_URL__c;

        // Start spinner
        //component.set("v.SpinnerLoading", true);

        requestGT.open("POST", urlGT, false);
        requestSR.open("POST", urlSR, false);
        requestGT.setRequestHeader("Content-Type", "application/json");
        requestSR.setRequestHeader("Content-Type", "application/json");
        requestGT.onreadystatechange = function() {
            if (requestGT.readyState === 4 && requestGT.status === 200) {
                // TSEC header from the response set on the next request's header
                var tsec = requestGT.getResponseHeader("tsec");
                requestSR.setRequestHeader("tsec", tsec);
                // Set the message to be displayed for the user
                component.set("v.ResultMessage", authMessage);
                $A.util.addClass(outputRM, 'textResMessFail');
                component.set("v.SpinnerLoading", false);
                requestSR.onreadystatechange = function() {
                    if (requestSR.readyState === 4 && requestSR.status === 200) {
                        authMessage = $A.get("$Label.c.MX_RDB_AuthMsg");
                        authStatus = true;
                        $A.util.addClass(outputRM, 'textResMessOK');
                    } else {
                        $A.util.addClass(outputRM, 'textResMessFail');
                    }
                    // Set the message to be displayed for the user
                    component.set("v.ResultMessage", authMessage);
                    // Set the Authentication status, used to disable the Auth button
                    component.set("v.AuthStatus", authStatus);
                    component.set("v.SpinnerLoading", false);
                }
                requestSR.send(jsonSR);
            }
        }
        requestGT.send(jsonGT);
    },

    gratingTicketData : function(dataGT) {
        // Prepare JSON to be used as webservice payload
        var data = JSON.stringify({
            "backendUserRequest": {
                "userId": dataGT.MX_RDB_UserId__c,
                "accessCode": dataGT.MX_RDB_AccessCode__c,
                "dialogId": dataGT.MX_RDB_DialogId__c
            },
            "authentication": {
                "consumerID": dataGT.MX_RDB_ConsumerID__c,
                "authenticationType": dataGT.MX_RDB_AuthenticationType__c,
                "authenticationData": [
                    {
                        "idAuthenticationData": dataGT.MX_RDB_IdAuthenticationData__c,
                        "authenticationData": [
                            dataGT.MX_RDB_AuthenticationData__c
                        ]
                    }
                ],
                "userID": dataGT.MX_RDB_UserId__c
            }
        });

		return data;
	},

    shortReferenceData : function (dataSR, recClienteId) {
        // Prepare JSON to be used as webservice payload
        var data = JSON.stringify({
            "service": dataSR.MX_RDB_service__c,
            "segment": dataSR.MX_RDB_segment__c,
            "customerType": dataSR.MX_RDB_customerType__c,
            "customerId": recClienteId,
            "reference" : "",
            "referenceNumber": ""
        });
        return data;
    }
})