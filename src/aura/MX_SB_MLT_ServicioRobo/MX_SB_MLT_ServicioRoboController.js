({
    roboDoInit: function(component, event, helper) {
	    var action = component.get("c.getRoboWO");
		var sinId = component.get("v.recordId");
        action.setParams({'sinid':sinId});
        action.setCallback(this, function(response) {
            let getValue = response.getReturnValue();
                component.set("v.ajustadorCCId",getValue.ajustadorCCId);
                component.set("v.ajustadorNombre",getValue.ajustadorNombre);
            	component.set("v.ajustadorSFId",getValue.ajustadorSFId);
        });
        $A.enqueueAction(action);
	},
    doInitServiciosRobo : function(component) {
	    var action = component.get("c.getTipoRobo");
        var idSin = component.get('v.recordId');
        action.setParams({"devname":"MX_SB_MLT_RamoAuto","idSiniestro":idSin});
        action.setCallback(this, function(response) {
            component.set("v.initObj",response.getReturnValue());
            var obj = component.get('v.initObj');
            component.set("v.recordtypeAUTO",obj.recordTypeId);
            if(obj.tipo === 'Robo Parcial'){
                component.set('v.tipoRobo','Ajustador');
            } else {
                component.set('v.tipoRobo','Asesor Robo');
            }
        });
        $A.enqueueAction(action);
    },
    SuccessServiciosRobo : function(component, event) {
        var navService = component.find("navService");
        var pageReference = {
            type: 'standard__recordPage',
            attributes: {
                "recordId": payloadId.id,
                "objectApiName": component.get("v.sObjectName"),
                "actionName": "view"
            }
        }
        event.preventDefault();
        navService.navigate(pageReference);
    },
    toggleEditServiciosRobo : function(component) {
        component.set("v.isEditMode", !component.get("v.isEditMode"));
    },
    buscaAjustadores :function(component) {
        component.set("v.busquedaVisible",true);
   },
    cierredeBusqueda : function(component,event) {
        var nombre = event.getParam("nombre");
        var idSales = event.getParam("idSalesforce");
        var idClaim = event.getParam("idClaim");
        component.set('v.ajustadorNombre',nombre);
        component.set('v.ajustadorSFId',idSales);
        component.set('v.ajustadorCCId',idClaim);
        component.set("v.busquedaVisible",false);
    },
    generaServicio : function(component,event,helper) {
        var continua = component.get("v.continua");
        if (continua){
        	helper.crearServicio(component,event,helper);
        }else {
        	var toastEventRobo = $A.get("e.force:showToast");
            toastEventRobo.setParams({
                "title": "Alerta",
                "type":"error",
                "message": "No se encontro poliza ligada al siniestro en curso."
            });
            toastEventRobo.fire();
        }
    },
})