({
	crearServicio : function(component,event, helper) {
        var idSales = '';
        idSales = component.get("v.ajustadorSFId");
        let idClaim = '';
        idClaim = component.get("v.ajustadorCCId");
        let nameAjus = '';
        nameAjus = component.get("v.ajustadorNombre");
        var toastEvent = $A.get("e.force:showToast");
        if(idClaim === undefined){
            toastEvent.setParams({
                "title": "Alerta",
                "type":"error",
                "message": "El Ajustador/Asesor no tiene ID, seleccione otro"
            });
            toastEvent.fire();
        } else {
            helper.ajustadorMessage(component, idClaim, nameAjus, idSales, toastEvent);
        }
	},
    validaPoliza: function(cmpRobo,event,helper){
    	var apxValida = cmpRobo.get("c.sinRobo");
        apxValida.setParams({"idbusqueda": cmpRobo.get("v.recordId")});
		apxValida.setCallback(this, function(response) {
			var stateRobo = response.getState();
            if (stateRobo === "SUCCESS") {
                if(response.getReturnValue().MX_SB_SAC_Contrato__c ===undefined){
                    cmpRobo.set("v.continua",false);
                }
            }
        });
        $A.enqueueAction(apxValida);
    },
    ajustadorMessage : function(component, idClaim, nameAjus, idSales, toastEvent) {
        if(idClaim == null && nameAjus == null){
            toastEvent.setParams({
            "title": "Alerta",
            "type":"error",
            "message": "la selección del Ajustador/Asesor es obligatoria"
            });
            toastEvent.fire();
            } else {
                var action = component.get("c.enviarWSRobo");
                action.setParams({"idSiniestro":component.get("v.recordId"),"idAjustador":idSales});
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var respuesta = response.getReturnValue();
                        if(respuesta === 'Reporte registrado exitosamente'){
                            toastEvent.setParams({
                                "title": "Alerta",
                                "type":"success",
                                "message": respuesta});
                            toastEvent.fire();
                            window.location.reload();
                        } else {
                            toastEvent.setParams({
                                "title": "Alerta",
                                "mode":"sticky",
                                "type":"error",
                                "message": respuesta});
                            toastEvent.fire();
                        }
                    } else {
                        toastEvent.setParams({
                        "title": "Alerta",
                        "type":"error",
                        "message": "la selección del Ajustador/Asesor es obligatoria"
                        });
                            toastEvent.fire();
                    }
                });
                $A.enqueueAction(action);
            }
    },
})