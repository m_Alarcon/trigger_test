({
    getLeadWrapper : function(component) {

        var dateInit = component.find("dateInit");
        var dateEnd = component.find("dateEnd");
        var sendDate0 = dateInit.get("v.value");
        var sendDate1 = dateEnd.get("v.value");
		var cPadre = component.get("v.campoPadre");
        var cHijo = component.get("v.campoHijo");
        var actionGLC = component.get("c.getLeadToComp");
        var paramId = component.get("v.idRI");

        var leadParams = {
            idRI: paramId,
            fechaInicio: sendDate0,
            fechaFin: sendDate1,
			campoPadre : cPadre,
            campoHijo : cHijo
        };
        actionGLC.setParams({
            stringParams: JSON.stringify(leadParams)
        });
	    actionGLC.setCallback(this, function(response) {
	        var rState = response.getState();
	        if (component.isValid() && rState === "SUCCESS") {
                component.set("v.listaCampaignLeads", response.getReturnValue());
                component.set("v.DATEINIT", false);
	        }
	    });

	    $A.enqueueAction(actionGLC);
	},
	setInitDates: function(component){
        component.set("v.DATEINIT", true);
        var todayD = new Date();
        var dd = todayD.getDate();
        var mm = todayD.getMonth()+1;
        var yyyy = todayD.getFullYear();
        if(dd<10) {
            dd = '0' + dd;
        }
        if(mm<10) {
            mm = '0' + mm;
        }
        todayD = yyyy + '-' + mm + '-' + dd;
        var dateInit = component.find("dateInit");
        var dateEnd = component.find("dateEnd");
	    var actionPDM = component.get("c.getPrimerDiaMes");
        actionPDM.setParams({
            dateToday : todayD
        });
        actionPDM.setCallback(this, function(response) {
            var rState = response.getState();
            var responseDate = response.getReturnValue();
            if(rState === 'SUCCESS'){
                var dateAsign = responseDate.replace(" 00:00:00", "");
                dateInit.set("v.value", dateAsign);
            }
            var action0 = component.get("c.getUltimoDiaDosMesesSig");
            action0.setParams({
                dateToday : todayD
            });
            action0.setCallback(this, function(response) {
            	var state = response.getState();
            	var responseDate = response.getReturnValue();
                if(state === 'SUCCESS'){
                    var dateAsign = responseDate.replace(" 00:00:00", "");
                    dateEnd.set("v.value", dateAsign);
                }
                this.getLeadWrapper(component);
            });
            $A.enqueueAction(action0);
        });
        $A.enqueueAction(actionPDM);
    },
    generaOpp :  function(component){

		var spinner = component.find("mySpinner");
	    $A.util.removeClass(spinner, 'slds-show');
		$A.util.addClass(spinner, 'slds-hide');
        component.set("v.spinnerExec", true);
		var campMemList = component.get("v.listaCampaignLeads");
        var listOppW = [];
        for(var cm in campMemList) {
            if(campMemList[cm].seleccion){
            	listOppW.push(campMemList[cm]);
            }
        }
        var convertLeadToOpp = component.get("c.convertLeadToOpp");
	    convertLeadToOpp.setParams({
	        listOppWInt : listOppW
	    });
        convertLeadToOpp.setCallback(this, function(response) {
	        var state = response.getState();
	        if (component.isValid() && state === "SUCCESS") {
	        	this.generaCompromisos(response.getReturnValue(), component);
	        	$A.util.removeClass(spinner, 'slds-hide');
                $A.util.addClass(spinner, 'slds-show');
            }
	    });

	    $A.enqueueAction(convertLeadToOpp);
	},

    generaCompromisos :  function(listOppW, component){
	    var generaCompromisosOppBPYP = component.get("c.generaCompromisosOppBPYP");
	    generaCompromisosOppBPYP.setParams({
	        listOpp : JSON.stringify(listOppW)
	    });
	    generaCompromisosOppBPYP.setCallback(this, function(response) {
	        var state = response.getState();
	        if (component.isValid() && state === "SUCCESS") {
                component.set("v.spinnerExec", false);
	        	this.getLeadWrapper(component);
	        }
	    });
	    $A.enqueueAction(generaCompromisosOppBPYP);
	}
})