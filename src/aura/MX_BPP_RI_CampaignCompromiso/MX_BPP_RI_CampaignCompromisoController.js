({
	doInit: function(component, event, helper) {
            helper.setInitDates(component);
    },
    getLeadRange: function(component, event, helper){
        if(!component.get("v.DATEINIT")){
        	helper.getLeadWrapper(component);
        }
    },
    generaCompromisosView : function(component, event, helper) {
        helper.generaOpp(component);
	},
    ObtenerCamposPadreEHijo : function(component, event, helper) {
        var campoP = event.getParam("campoPadre");
        var campoH = event.getParam("campoHijo");
        component.set("v.campoPadre", campoP);
        component.set("v.campoHijo", campoH);
   		helper.getLeadWrapper(component, event);
    }
})