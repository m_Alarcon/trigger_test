({
    	handleSelect: function (cmp, event,helper) {
        var mapping1 = { '1' : 'Auto', '2' : 'Vida', '3' : 'Daños',
            '4' : 'GMM'};
        cmp.set('v.selected', mapping1[event.getParam('name')]);
        var mapping2 = {'24' : 'Ahorro', '25' : 'Salud', '26' : 'Fallecimiento'};
        cmp.set('v.selected2', mapping2[event.getParam('name')]);
            var selected = mapping1[event.getParam('name').toString()];
            if(selected==='Vida') {
                helper.toastsubRamo(cmp,event);
            }
    },

    init: function (cmp) {
        var items = [{
            "label": "Auto",
            "name": "1",
                }, {
                "label": "Vida",
                "name": "2",
                "expanded": false,
                "items" :[{
                    "label": "Ahorro",
                    "name": "24",
                    "expanded": true,
                    "items" :[]
                }, {
                    "label": "Salud",
                    "name": "25",
                    "expanded": true,
                    "items" :[]
                }, {
                    "label": "Fallecimiento",
                    "name": "26",
                    "expanded": true,
                    "items" :[]
                }]
            }, {
            "label": "Daños",
            "name": "3",
                },{
            "label": "GMM",
            "name": "4",
                },
                    ];
        cmp.set('v.items', items);
    },
})