({
    getInvitados: function (cmp) {
        var action = cmp.get("c.getInvitadosRecurrentes");
        action.setParams({ rtContact :"MX_RTE_Usuarios"});
        action.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
                cmp.set('v.data', response.getReturnValue());
            }
            else {
                console.log(JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },
    insInvitados: function (cmp) {
        var action = cmp.get("c.insertInv");
        action.setParams({ listInvitados : cmp.get("v.Invitados"), idEvent : cmp.get("v.recordId")});
        action.setCallback(this, function(response) {
            if(response.getState()==='SUCCESS') {
                    var successEvt = $A.get("e.force:showToast");
                    successEvt.setParams({title: "Registro Exitoso",
                        message: "Los registros fueron creados exitosamente.",
                        type: "success" });
                    successEvt.fire();
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get("e.force:refreshView").fire();
            }
         else {
                console.log('error ' + JSON.stringify(response.getError()));
                   var errorEvt = $A.get("e.force:showToast");
                   errorEvt.setParams({title: "Error",
                        message: "Ocurrió un error al crear los registros.",
                        type: "error"});
                    errorEvt.fire();
              }
        });
        $A.enqueueAction(action);
    }
})