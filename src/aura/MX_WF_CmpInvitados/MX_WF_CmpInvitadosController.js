({
    init: function (cmp, event, helper) {
        cmp.set('v.columns', [
            {label: 'Invitado', fieldName: 'Name', type: 'text'},
            {label: 'Correo electrónico', fieldName: 'Email', type: 'email'},
            {label: 'Dirección General', fieldName: 'MX_RTE_Direccion_General__c', type: 'phone'}]);
        helper.getInvitados(cmp);
    },
    invSelected: function (cmp, event) {
        var selectedRows = event.getParam('selectedRows');
        cmp.set('v.Invitados', "");
        var resul = cmp.get('v.Invitados');
        if(selectedRows!==null && selectedRows!=='') {
            for (var i=0;i<selectedRows.length;i++) {
                resul.push(selectedRows[i].Id);
            }
            cmp.set('v.Invitados', resul);
        }
 },
    handleClick : function (cmp, event, helper) {
            helper.insInvitados(cmp);
    },
    handleClickCanc : function (cmp, event, helper) {
       var dismissActionPanel = $A.get("e.force:closeQuickAction");
       dismissActionPanel.fire();
    },
})