({
    doInitH : function(component, event, helper) {
        let recordId=component.get('v.recordId');
        let action2= component.get('c.loadOppF');
        action2.setParams({'oppId':recordId});
        action2.setCallback(this, function(responseA) {
            if(responseA.getState()==='SUCCESS') {
                component.set("v.OppObj",responseA.getReturnValue());
                let action = component.get('c.loadAccountF');
                action.setParams({'recId':responseA.getReturnValue().AccountId});
                action.setCallback(this, function(responseB) {
                    component.set("v.accObj",responseB.getReturnValue()[0]);
		    helper.doInit(component, event, helper);
                    component.set('v.Nominado','Si');
                    component.set("v.lockData",'**** **** **** ');
                    component.set('v.tdcN','2564');
                });
                $A.enqueueAction(action);
            }
        });
        $A.enqueueAction(action2);
    },
})