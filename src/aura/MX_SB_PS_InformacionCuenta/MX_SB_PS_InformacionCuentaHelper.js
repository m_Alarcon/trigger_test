({
	doInit : function(component, event, helper) {
		var action = component.get("c.loadPersLiEvent");
		action.setParams(
	            {idContact : component.get('v.accObj.PersonContactId')}
        	);
	        action.setCallback(this, function(response) {
        		var state = response.getState();
		            if (state === "SUCCESS" &&  response.getReturnValue()[0]!==undefined) {
                		var storeResponse = response.getReturnValue();
		                component.set('v.CicloVida', storeResponse[0].Name);
		            }
	        });
        	$A.enqueueAction(action);
	}
})