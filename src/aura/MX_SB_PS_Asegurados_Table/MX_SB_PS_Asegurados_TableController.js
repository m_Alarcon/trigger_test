({
    initBenefactor: function(component,event,helper) {
        helper.initAseg(component,event,helper);
    },
        handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        switch (action.name) {
            case 'edit':
                helper.editRecord(component, event);
                break;
            case 'delete':
                helper.deleteRecord(component, event);
                break;
        }
    },
})