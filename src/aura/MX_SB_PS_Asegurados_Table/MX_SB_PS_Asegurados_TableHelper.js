({
    initAseg : function(component,event) {
        var oppId= component.get("v.recordId");
        var action= component.get("c.loadOppF");
        action.setParams({'oppId':oppId});
        action.setCallback(this, function(response) {
            component.set("v.oppObj",response.getReturnValue());
            if(response.getReturnValue().SyncedQuoteId===undefined) {
                component.set('v.refreshC',true);
            } else {
                component.set('v.refreshC',false);
                var action2= component.get("c.srchBenefs");
                action2.setParams({'quotId':response.getReturnValue().SyncedQuoteId});
                action2.setCallback(this, function(response) {
                    var info = response.getReturnValue();
                    component.set('v.refreshC',false);
                    for (var i = 0; i < info.length; i++) {
                        info[i].NameC= info[i].Name + ' ' + info[i].MX_SB_VTS_APaterno_Beneficiario__c +' '+info[i].MX_SB_VTS_AMaterno_Beneficiario__c;
                    }
                    component.set("v.info",info);
                });
                $A.enqueueAction(action2);
            }
        });
        $A.enqueueAction(action);
        var actions = [
            { iconName :'utility:edit',label: 'Editar', name: 'edit'},
            { iconName:'utility:delete',label: 'Borrar', name: 'delete'}
        ]
        component.set('v.columns', [
            {label: 'Nombre', fieldName: 'NameC', type: 'text'},
            {label: 'Edad', fieldName: 'MX_SB_PS_Edad__c', type: 'text'},
            {label: 'Sexo', fieldName: 'MX_SB_SAC_Genero__c', type: 'text'},
            {label: 'Parentesco', fieldName: 'MX_SB_VTS_Parentesco__c', type: 'text'},
            {type: 'action', typeAttributes: { rowActions: actions } }
        ]);
    },
    deleteRecord : function(component, event) {
        var row = event.getParam('row');
        var action = component.get("c.dltBenef");
        action.setParams({
            "benef": row
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" ) {
                var rows = component.get('v.info');
                var rowIndex = rows.indexOf(row);
                rows.splice(rowIndex, 1);
                component.set('v.info', rows);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: "Se ha actualizado el asegurado.",
                    type: 'success'
                });
                toastEvent.fire();
                var cmpEvent =$A.get("e.c:MX_SB_PS_EventHandlerDelBenef");
                cmpEvent.setParams({"wasDelete" : true, "parentesco" : row.MX_SB_VTS_Parentesco__c, "info": JSON.stringify((component.get('v.info')))});
                cmpEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    editRecord : function(component, event) {
        var row = event.getParam('row');
        var recordId = row.Id;
        var cmpEvent =$A.get("e.c:MX_SB_PS_EventHandlerEditBenef");
        cmpEvent.setParams({"benefId" :recordId });
        cmpEvent.setParams({'wasEditEvent':true});
        cmpEvent.fire();
    },
})