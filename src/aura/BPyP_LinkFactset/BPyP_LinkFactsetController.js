({
	init : function(component, event, helper) {
		var action = component.get('c.checkAccess');

		action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				component.set('v.access', response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
	},

	buttonPress : function(component, event, helper) {
		var urlLabel = $A.get("$Label.c.BPyP_LinkFactset");
		var win = window.open(urlLabel, '_blank');
 		win.focus();
	}
})