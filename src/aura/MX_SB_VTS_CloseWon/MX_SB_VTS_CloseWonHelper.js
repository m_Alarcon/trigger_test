({
    showToast: function (component, event, helper, type, message, title) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "duration": "2000",
            "type": type
        });
        toastEvent.fire();
    },
    helperDoInit: function (component, event, helper) {
        let workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation().then(function (response) {
            if (response) {
                workspaceAPI.getFocusedTabInfo().then(function (response) {
                    let focusedTabId = response.tabId;
                    helper.showToast(component, event, helper, "Success", "Felicidades", "Ha terminado la asistencia");
                    workspaceAPI.closeTab({
                        tabId: focusedTabId
                    });
                })
                    .catch(function (errortab) {
                        helper.showToast(component, event, helper, "Error", errortab, "Ocurrió algo mal");
                    });
            }
        })
    },
    helperAutoClose: function (component, event, helper) {
        var leadId = component.get("v.recordId");
        let updateRecord = component.get("c.updCurrRec");
        updateRecord.setParams({
            "leadId": leadId
        });
        updateRecord.setCallback(this, function (response) {
            if (response) {
               helper.helperDoInit(component, event, helper);
            }
        });
        $A.enqueueAction(updateRecord);
    }
})