({
    doInit: function (component, event, helper) {
        let decition = component.get("v.StageName");
        if (decition === "Closed Won" || decition === 'Inicio') {
            helper.helperDoInit(component, event, helper);
        } else if(decition === 'Ninguno') {
            helper.helperAutoClose(component, event, helper);
        }
    }
})