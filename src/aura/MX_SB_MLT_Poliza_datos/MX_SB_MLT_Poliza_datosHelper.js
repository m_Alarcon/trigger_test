({
	initReportante : function(component, event, helper) {
        let reporId = component.get("v.recordId");
		let reporAct = component.get("c.searchSinVida");
        reporAct.setParams({'siniId':reporId});
        reporAct.setCallback(this,function(response) {
            if(response.getState() === 'SUCCESS') {
                let repResponse = response.getReturnValue();
                component.set("v.Siniestro",repResponse);
            }
        });
        $A.enqueueAction(reporAct);
	},
    labelReporRT : function(component, event, helper) {
        let rptId = component.get("v.recordId");
        let getLbl = component.get("c.searchRecordType");
        getLbl.setParams({'recordId':rptId});
        getLbl.setCallback(this,function(response) {
            if(response.getState() === 'SUCCESS') {
                let lblResponse = response.getReturnValue();
                helper.typeRamo(component, event, helper, lblResponse);
            }
        });
        $A.enqueueAction(getLbl);
    },
    typeRamo : function(component, event, helper, rm) {
        switch(rm) {
            case 'MX_SB_MLT_RamoAuto' :
                component.set("v.ramo",'Auto');
            break;
            case 'MX_SB_MLT_RamoGF' :
                component.set("v.ramo",'Gastos Funerarios');
            break;
            case 'MX_SB_MLT_RamoGMM' :
                component.set("v.ramo",'Gastos Medicos Mayores');
            break;
            case 'MX_SB_MLT_RamoVida' :
                component.set("v.ramo",'Vida');
           	break;
            default:
        }
    },
    coment : function(component, event, helper) {
        let recordId = component.get("v.recordId");
        let getHstrFields = component.get("c.historyFields");
        getHstrFields.setParams({'recordId':recordId});
        getHstrFields.setCallback(this,function(response) {
            if(response.getState() === 'SUCCESS') {
                let rspObject = response.getReturnValue();
                if(rspObject.hasOwnProperty('Histories')) {
                    component.set("v.tracking",rspObject.hasOwnProperty('Histories'));
                }
                component.set("v.info",rspObject);
            }
        });
        $A.enqueueAction(getHstrFields);
    },
})