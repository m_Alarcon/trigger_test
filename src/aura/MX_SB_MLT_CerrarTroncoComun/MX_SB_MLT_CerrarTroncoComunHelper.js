({
	modalAuto : function(component, event, helper) {
        let recordId = component.get('v.recordId');
        let action = component.get('c.getDatosSiniestro');
        action.setParams({'idsini': recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue().TipoSiniestro__c==='Colisión'
                && response.getReturnValue().MX_SB_MLT_LugarAtencionAuto__c==='Crucero'
                && response.getReturnValue().MX_SB_MLT_JourneySin__c==='Validación de Poliza'
                && response.getReturnValue().MX_SB_MLT_AutoAjusteAccepted__c===undefined) {
                let ques1 = response.getReturnValue().MX_SB_MLT_PropiedadAjena__c;
                component.set("v.question1", ques1);
				let ques2 = response.getReturnValue().MX_SB_MLT_Q_ThirdPresent__c;
                component.set("v.question2", ques2);
            	let ques3 = response.getReturnValue().MX_SB_MLT_Q_AuthorityPresent__c;
                component.set("v.question3", ques3);
                let siniestro = response.getReturnValue().TipoSiniestro__c;
                component.set("v.siniestro", siniestro);
            }
        });
        $A.enqueueAction(action);
     },
})