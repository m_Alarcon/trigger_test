({
	doInit : function(component, event, helper) {
		helper.modalAuto(component, event, helper);
	},
    obTabId: function(component,event,helper){
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getAllTabInfo().then(function(response) {
            for(var i=0;i<=response.length;i++){
                if(response[i].title.match(/Crear Siniestro/) || response[i].title.match(/Llamada PureCloud/) ) {
                    var idTab = response[i].tabId;
                    workspaceAPI.closeTab({tabId: idTab });
                }
            }
        });
    },
})