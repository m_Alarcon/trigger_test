({
    doInitLayoutAsis : function(component, event, helper) {
        var action = component.get("c.getRecordType");
        action.setParams({"devname":"MX_SB_MLT_RamoAsistencias"});
        action.setCallback(this, function(response) {
            component.set("v.recordtypeAsistencias",response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    handleSuccessLayAsis : function(component, event, helper) {
        var payload = event.getParams().response;
        var navService = component.find("navService");
        var pageReference = {
            type: 'standard__recordPage',
            attributes: {
                "recordId": payload.id,
                "objectApiName": "Siniestro__c",
                "actionName": "view"
            }
        }
        event.preventDefault();
        navService.navigate(pageReference);
    }
})