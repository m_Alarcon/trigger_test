({
	init : function(cmp, event, helper) {
        var navService = cmp.find("navService");
        var pageReference = {
            "type": "standard__navItemPage",
            "attributes": {
                "apiName": "MX_BPP_Aclaraciones"
            }
        };
        cmp.set("v.pageReference", pageReference);
        var defaultUrl = "#";
        navService.generateUrl(pageReference)
            .then($A.getCallback(function(url) {
                cmp.set("v.url", url ? url : defaultUrl);
            }), $A.getCallback(function(error) {
                cmp.set("v.url", defaultUrl);
            }));
        cmp.set('v.columns', [
            { label: 'Nombre', fieldName: 'urlAcc', type: 'url', typeAttributes: {label: {fieldName: 'accNombre'},target: '_self'}, cellAttributes: { alignment: 'left'}},
            { label: 'Folios', fieldName: 'count', type: 'number', cellAttributes: { alignment: 'center'}},
            { label: 'Fecha de alta', fieldName: 'caseFechaCrea', type: 'date-local', cellAttributes: { alignment: 'left'}}
        ]);

        helper.getData(cmp);
    },
    handleClick: function(cmp, event, helper) {
        var navService = cmp.find("navService");
        var pageReference = cmp.get("v.pageReference");
        event.preventDefault();
        navService.navigate(pageReference);
    }
})