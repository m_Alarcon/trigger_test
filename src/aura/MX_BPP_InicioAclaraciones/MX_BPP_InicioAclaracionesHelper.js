({
	getData : function(cmp) {
        var accountsId = [];
        var resMod = [];
        var index;
        let actionData = cmp.get('c.retrieveCases');
        actionData.setCallback(this, $A.getCallback(function (response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let res = response.getReturnValue();
                res.forEach(function(item) {
                    item['urlAcc'] = '/lightning/r/Account/' + item['accId'] + '/view';
                    item['urlCase'] = '/lightning/r/Case/' + item['caseId'] + '/view';
                    if(!accountsId.includes(item['accId']) && accountsId.length <= 10) {
                        accountsId.push(item['accId'])
                        resMod.push({'urlAcc': item['urlAcc'],
                                       'accId': item['accId'],
                                       'accNombre': item['accNombre'],
                                       'caseFechaCrea': item['caseFechaCrea'],
                                       'count': 1
                                      })
                    } else if(accountsId.includes(item['accId']) && accountsId.length <= 10) {
                        index = accountsId.indexOf(item['accId']);
                        if(item['caseFechaCrea'] > resMod[index].caseFechaCrea){
                            resMod[index].caseFechaCrea = item['caseFechaCrea'];
                        }
                        resMod[index].count = resMod[index].count + 1;
                    }
                });
                this.setData(cmp, resMod);
            }
        }));
        $A.enqueueAction(actionData);
    },

    setData : function(cmp, resMod) {
        let singleAcc = [];
        let recordId = cmp.get("v.recordId");

        if(recordId !== null) {
            resMod.forEach(function(item) {
                if(item['accId'] === recordId) {
                    singleAcc.push(item);
                }
            });
            cmp.set("v.data", singleAcc);
        } else {
            cmp.set('v.data', resMod);
        }
    }
})