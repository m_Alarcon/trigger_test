({
     doInit:function(component, event, helper) {
        var action = component.get("c.getValue");
        action.setParams({"recId": component.get("v.recordId") });
        action.setCallback(this, function(response) {
            component.set("v.Telefono", response.getReturnValue());
        });
        $A.enqueueAction(action);
        var recId = component.get("v.recordId");
        var action3 = component.get("c.getInfo");
         component.set("v.TaskId",recId);
         action3.setParams({'recId': recId});
         action3.setCallback(this, function(response){
             if(response.getReturnValue().Status!=='No iniciada') {
                 component.set("v.disnot",true);
             }
         });
         $A.enqueueAction(action3);
    },
    navigateToMyComponent : function(component, event, helper) {
        var bttnValue = event.getSource().get("v.value");
        component.set("v.Task.Status",bttnValue);
        var recordId= component.get("v.recordId");
        var navToAction= component.get("c.updTask");
        navToAction.setParams({
            'recId':recordId,
            'tipo': bttnValue,
            'subtipo':''
        });
        navToAction.setCallback(this, function(response) {});
        $A.enqueueAction(navToAction);
        var evtNav = $A.get("e.force:navigateToComponent");
        evtNav.setParams({
			componentDef : "c:MX_SB_MLT_RecordTypesForm",
            isredirect: true,
            componentAttributes: {
                Telefono : component.get("v.Telefono"),
                TaskId : component.get("v.TaskId")
            }
        });
        evtNav.fire();
    },
    showInfo: function(component, event, helper) {
        var bttnValue = event.getSource().get("v.value");
        var selectedMenuItemValue = event.getParam("value");
        component.set("v.Task.Status",bttnValue);
        component.set('v.Task.Resultado_llamada__c',selectedMenuItemValue);
        var recordId= component.get("v.recordId");
        var action2= component.get("c.updTask");
        action2.setParams({
            'recId':recordId,
            'tipo': bttnValue,
            'subtipo':selectedMenuItemValue
        });
        action2.setCallback(this, function(response) {
            component.set("v.disnot",true);
            });
        $A.enqueueAction(action2);
    },
})