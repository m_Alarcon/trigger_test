({
    doInit : function(component, event, helper) {
        var whtId = component.get('v.recordId');
        var action= component.get('c.getTaskData');
        action.setParams({'oppId':whtId});
        action.setCallback(this, function(response) {
            component.set('v.taskObj',response.getReturnValue()[0]);
        });
        $A.enqueueAction(action);
        var action5 = component.get('c.loadOppK');
        action5.setParams({'oppId':component.get('v.recordId')});
        action5.setCallback(this, function(response) {
            component.set('v.oppObj',response.getReturnValue());
            var action6 = component.get('c.findGenericObject');
            action6.setParams({'str':response.getReturnValue().SyncedQuoteId,'tipo':'quot'});
            action6.setCallback(this, function(response) {
                if(response.getReturnValue()[0].MX_ASD_PCI_ResponseCode__c==='0') {
                    component.set("v.waitingPayment",false);
                }
            });
            $A.enqueueAction(action6);
            var action3 = component.get('c.getQuoteLneItem');
            action3.setParams({'quoteId':response.getReturnValue().SyncedQuoteId});
            action3.setCallback(this, function(response){
                component.set('v.QuoteLI',response.getReturnValue()[0]);
            });
            $A.enqueueAction(action3);
        });
        $A.enqueueAction(action5);
        var actionOpenPay= component.get('c.reqOpenPayCS');
        actionOpenPay.setCallback(this, function(response){
            component.set('v.openPayAttributes',response.getReturnValue());
        });
        $A.enqueueAction(actionOpenPay);
    },
    siguiente : function(component, event, helper) {
        helper.emitePoliza(component, event, helper);
    },
    generaCodigo :function(component,event,helper){
        component.set('v.showSpinner',true);
        var sessionID =  helper.generaSessionID(component, event, helper);
        var action2 = component.get('c.loadOppK');
        action2.setParams({'oppId':component.get('v.recordId')});
        action2.setCallback(this, function(response) {
            component.set('v.oppObj',response.getReturnValue());
            var action3 = component.get('c.getQuoteLneItem');
            action3.setParams({'quoteId':response.getReturnValue().SyncedQuoteId});
            action3.setCallback(this, function(response){
                component.set('v.QuoteLI',response.getReturnValue()[0]);
                var foliocotsp= response.getReturnValue()[0].MX_WB_Folio_Cotizacion__c;
				component.set('v.quoteIde',foliocotsp);
                var action2=component.get('c.enviarOtp');
                var phoneToCall=component.find('phonophono').get("v.value");
                action2.setParams({'mapdata':{
                    'QUOTEID': foliocotsp,
                    'PHONE': phoneToCall,
                    'SESSIONID': sessionID
                }});
                action2.setCallback(this, function(response) {
                    component.set('v.showSpinner',false);
                    helper.validaEnvOTP(component, event, helper, response.getReturnValue());
                });
                $A.enqueueAction(action2);
            });
            $A.enqueueAction(action3);
        });
        $A.enqueueAction(action2);
    },
    clickbotonmodal : function(component,event,helper){
        component.set("v.shwmodal",false);
    },
    processResponsePC: function(component, event, helper) {
        let status = event.getParam("responseStatus");
        let statusDetail = event.getParam("responseDescript")===null ? '': event.getParam("responseDescript");
        let mensajeAsesor = event.getParam("responseMessage")===undefined ? '': event.getParam("responseMessage");
        helper.processIvrMsj(component, event, helper,status, statusDetail, mensajeAsesor);
    },
    cap1: function(component,event,helper) {
		var phoni = component.get('v.taskObj.MX_WB_telefonoUltimoContactoCTI__c');
        component.set('v.taskObj.MX_WB_telefonoUltimoContactoCTI__c',phoni.replace(/[^0-9]+/g,'').toUpperCase());
    }
})