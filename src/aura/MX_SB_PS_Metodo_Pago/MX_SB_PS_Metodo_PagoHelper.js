({
    validasiguiente : function(cmp,evt,helper) {
        var updopp = cmp.get("c.actualizaoportunidad");
        updopp.setParams({"opp":{"Id":cmp.get("v.recordId"),
                                 "StageName": $A.get("$Label.c.MX_SB_PS_Etapa5")}
                         });
        updopp.setCallback(this, function(response) {
            if(response.getState()==="SUCCESS") {
                this.isRefreshed(cmp,evt,helper);
            }
        });
        $A.enqueueAction(updopp);
    },
    emitePoliza: function(component, event, helper) {
        var action4 = component.get('c.emisionPoliza');
        action4.setParams({'oppObj': component.get('v.oppObj')});
        action4.setCallback(this, function(response) {
            if (response.getState()==='SUCCESS'){
                this.validasiguiente(component,event,helper);
                component.set('v.QuoteLI.MX_WB_noPoliza__c',response.getReturnValue());
                var action = component.get('c.actualizaQuoli');
                action.setParams({'quoli': component.get('v.QuoteLI')});
                action.setCallback(this, function(response) {
                });
                $A.enqueueAction(action);
            } else {
                this.customtoast(component,event,helper,'Error al emitir cotizacion.','ERROR');
            }
        });
        $A.enqueueAction(action4);
    },
    isRefreshed: function(component, event, helper) {
        var cmpEvent =$A.get("e.c:MX_SB_PS_EventValidData");
        cmpEvent.setParams({"continuar" : true, "nextStageName" : $A.get("$Label.c.MX_SB_PS_Etapa5")});
        cmpEvent.fire();
    },
    centertext : function(component,ident){
        document.getElementById(ident).style.setProperty('display', 'flex');
        document.getElementById(ident).style.setProperty('justify-content',  'center');
        document.getElementById(ident).style.setProperty('align-content', 'center');
        document.getElementById(ident).style.setProperty('flex-direction', 'column');
        document.getElementById(ident).style.setProperty('text-align', 'center');
    },
    startTimer : function( cmp, evt, h ) {
        var i = 0;
        var interval = window.setInterval(
            $A.getCallback(function() {
                var value = i+ 1;
                cmp.set("v.timeLeft", value);
                if(cmp.get("v.oDisable")["StopTimer"]===true){
                    window.clearInterval(interval);
                    var edita=cmp.get("v.oDisable");
                    edita["StopTimer"]=false;
                    cmp.set("v.oDisable",edita);
                }
                if(i===20){
                    var typeresponse1 = cmp.get("v.oDisable");
                    typeresponse1["GeneraCodigo"]=false;
                    cmp.set("v.oDisable",typeresponse1);
                }
                if(i===120){
                    window.clearInterval(interval);
                    var typeresponse = cmp.get("v.oDisable");
                    cmp.set("v.ClassHeaderMP","slds-medium-size--6-of-6 mdlMetodoPagoH mdlHeaderTypeCveInc");
                    cmp.set("v.shwmodal",true);
                    cmp.set("v.msgModal",{"Body":"'Time out', El código de autenticación no ha sido ingresado dentro del tiempo permitido. Intente de nuevo",
                                          "Header":"Ingreso fallido del código de autenticación"});
                    typeresponse['TypeResp']='ERROR';
                    cmp.set("v.oDisable",typeresponse);
                }
                i=value;
            }), 1000
        );
        cmp.set("v.timeLeft", interval);
    },
    handleDestroy : function( cmp, evt, h ) {
        window.clearInterval(cmp.get("v.timeLeft"));
    },
    validaEnvOTP : function(component, event, helper, responseVal) {
        var typeresponse = component.get("v.oDisable");
        if(responseVal==='204') {
            component.set("v.msgModal",{"Body":"Código de seguridad generado con éxito",
                                        "Header":"Confirma con el cliente la recepción del SMS y continúa con el cobro de la póliza."});
            typeresponse['TypeResp']='SUCCESS';
            this.changestyle(component,"slds-medium-size--6-of-6 mdlMetodoPagoH mdlHeaderTypeOTPSuccess",typeresponse);
            helper.generateOTP(component,event,helper);
        } else if(responseVal==='500') {
            component.set("v.msgModal",{"Body":"Código de seguridad no generado",
                                        "Header":"Ha ocurrido un error interno en el servidor. Intentalo de nuevo."});
            typeresponse['TypeResp']='ERROR';
            this.changestyle(component,"slds-medium-size--6-of-6 mdlMetodoPagoH mdlHeaderTypeOTPSuccess",typeresponse);
        }
            else if(responseVal==='0') {
                var SESSIONID=this.generaSessionID(component, event, helper);
                var action2=component.get('c.enviarOtp');
                var phoneToCall=component.find('phonophono').get("v.value");
                action2.setParams({'mapdata':{
                    'QUOTEID': component.get('v.quoteIde'),
                    'PHONE': phoneToCall,
                    'SESSIONID': SESSIONID
                }});
                action2.setCallback(this, function(response) {
                    this.validaEnvOTP(component, event, helper, response.getReturnValue());
                });
                $A.enqueueAction(action2);
            } else {
            component.set("v.msgModal",{"Body":"Código de seguridad no generado",
                                        "Header":"Ha ocurrido un error al generar el codigo de autenticación. Inténtalo de nuevo"});
            typeresponse['TypeResp']='ERROR';
            this.changestyle(component,"slds-medium-size--6-of-6 mdlMetodoPagoH mdlHeaderTypeCobNr",typeresponse);
        }
    },
    validaCobro : function(component,resp, title, msjAsesor){
        var typeresponse = component.get("v.oDisable");
        var objdis = component.get("v.oDisable");
        var intentos = objdis['CobroIncorrecto'];
        if(intentos>=2){
            this.IntentosMsg(component);
        }else{
            if(resp==='TO' || resp==='INC'){
                component.set("v.msgModal",{"Body":msjAsesor, "Header":title});
                typeresponse['TypeResp']='ERROR';
                this.changestyle(component,"slds-medium-size--6-of-6 mdlMetodoPagoH mdlHeaderTypeCveInc",typeresponse);
            } else if(resp==='NR' || resp==='DC'){
                component.set("v.msgModal", {"Body":msjAsesor,"Header":title});
                typeresponse['TypeResp']='ERROR';
                this.changestyle(component,"slds-medium-size--6-of-6 mdlMetodoPagoH mdlHeaderTypeCobNr",typeresponse);
            } else if(resp==='CE'){
                component.set( "v.msgModal",{"Body":msjAsesor, "Header":title} );
                typeresponse['TypeResp']='SUCCESS';
                component.set("v.waitingPayment",false);
                this.changestyle(component,"slds-medium-size--6-of-6 mdlMetodoPagoH mdlHeaderTypeCobEx",typeresponse);
            }
        }
    },
    changestyle: function(component,cls,typeresp) {
        component.set("v.ClassHeaderMP",cls);
        component.set("v.shwmodal",true);
        component.set("v.oDisable",typeresp);
    },
    IntentosMsg: function(component) {
        var typeresponse = component.get("v.oDisable");
        component.set("v.msgModal",{"Body":"Número de intentos agotados",
                                    "Header":"Cobro no realizado"});
        typeresponse['TypeResp']='ERROR';
        this.changestyle(component,"slds-medium-size--6-of-6 mdlMetodoPagoH mdlHeaderTypeCobNr",typeresponse);
    },
    generateOTP : function(component, event, helper) {
        var action = component.find('openpaychild').retriveTokenHandler();
        let oppId = component.get('v.recordId');
        var findQuote = component.get('c.updateQuoteIVR');
        findQuote.setParams({
            oppId : oppId
        });
        findQuote.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                var pureCLoudEvent = $A.get("e.c:MX_SB_RTL_ListenerPaymentPC");
                pureCLoudEvent.setParams({"folioCot" : component.get('v.quoteIde')});
                pureCLoudEvent.setParams({"oppId" : component.get('v.oppObj.SyncedQuoteId')});
                pureCLoudEvent.setParams({"securityToken" : action });
                pureCLoudEvent.fire();
            }
        });
        $A.enqueueAction(findQuote);
    },
    showToast: function(component, event, helper, message, toastType, toastTitle) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": toastTitle,
            "message": message,
            "type" : toastType
        });
        toastEvent.fire();
    },
    processIvrCode: function(component, event, helper, status) {
        if(status !== '0') {
            helper.showToast(component, event, helper,'Error' , 'Opps algo extraño paso', 'Oopps...');
        } else {
            helper.showToast(component, event, helper, 'El Pagó se ha realizó con Éxito!', 'success', 'Cobro Exitoso');
        }
    },
    customtoast : function (compoonent, event,helper,msg,tipo) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message:msg,
            duration:' 2000',
            key: 'info_alt',
            type: tipo
        });
        toastEvent.fire();
    },
    processIvrMsj: function(component, event, helper,status, statusDetail, mensajeAsesor) {
        if(mensajeAsesor.includes('paymentFailed')) {
            var msj=mensajeAsesor ;
            var array = [];
            array = msj.split('#');
            var deconcat = array[1];
            var array2=[];
            array2 = deconcat.split('-');
            this.searchIvrCode(component, event, helper,array2[0]);
        } else if(mensajeAsesor.includes('Cobro Satisfactorio') || status === 0 || status ==='0' ) {
            this.validaCobro(component,'CE',
                'Cobro Exitoso','El cobro fue exitoso. Envía la póliza al cliente, tipifica la llamada como cobrada y finaliza la venta.');
        }
        else {
            this.findMensajeA(component, event, helper, mensajeAsesor);
        }
    },
    findMensajeA: function(component, event, helper, mensajeAsesor) {
        var getMensajeA= component.get('c.findGenericObject');
        getMensajeA.setParams({'str':mensajeAsesor,'tipo':'outToOP'});
        getMensajeA.setCallback(this, function(response){
            if(response.getReturnValue()[0]!==undefined) {
                this.validaCobro(component,response.getReturnValue()[0].MX_SB_VTS_Description__c,
                response.getReturnValue()[0].MX_SB_VTS_HEADER__c,response.getReturnValue()[0].MX_SB_VTS_HREF__c);
            }
        });
        $A.enqueueAction(getMensajeA);
    },
    searchIvrCode: function(component, event, helper, ivrCodeToS) {
        var getIvrCodes = component.get('c.findGenericObject');
        getIvrCodes.setParams({'str': ivrCodeToS,'tipo':'ivr'});
        getIvrCodes.setCallback(this, function(response) {
            if(response.getReturnValue()[0]!==undefined) {
                this.validaCobro(component,'NR',response.getReturnValue()[0].MX_RTL_TitleMsg__c,response.getReturnValue()[0].MX_RTL_MsgSF__c);
            } else {
                this.validaCobro(component,'NR','Cobro no realizado','No pudo completarse el cobro del método elegido. ¡Intenta de nuevo!');
            }
        });
        $A.enqueueAction(getIvrCodes);
    },
    generaSessionID: function(component, event, helper) {
        var cSett = component.get('v.openPayAttributes');
        OpenPay.setId(cSett.MX_SB_VTS_SRC__c);
        OpenPay.setApiKey(cSett.MX_SB_VTS_Status__c);
        OpenPay.setSandboxMode(cSett.MX_SB_VTS_Stage__c);
        return OpenPay.deviceData.setup("tokenme-form", "deviceSession_id");
    }
})