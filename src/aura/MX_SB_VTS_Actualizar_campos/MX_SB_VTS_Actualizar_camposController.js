({
    doInit : function(component) {
        $A.get('e.force:refreshView').fire();
    },
    refresh : function(component) {
        action.setCallback(component,
            function(response) {
                $A.get('e.force:refreshView').fire();
            }
        );
        $A.enqueueAction(action);
    }
})