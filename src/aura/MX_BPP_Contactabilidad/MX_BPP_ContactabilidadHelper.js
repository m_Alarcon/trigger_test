({
	helperInit : function(component, event) {
        component.set("v.accessCombos" , {
            "Administrador del sistema" : {"tf":true, "div":true, "off":true, "bnk":true },
            "Soporte BPyP" : {"tf":true, "div":true, "off":true, "bnk":true },
            "BPyP STAFF" : {"tf":true, "div":true, "off":true, "bnk":true },
            "BPyP Director Divisional" : {"tf":true, "div":false, "off":true, "bnk":true },
            "BPyP Director Oficina" : {"tf":true, "div":false, "off":false, "bnk":true },
            "BPyP Estandar" : {"tf":false, "div":false, "off":false, "bnk":false }
        } );

        component.set('v.columns', [
            { label: 'Nombre de la Cuenta', fieldName: 'LinkName', type: 'url', sortable: true, typeAttributes: { label: { fieldName: 'Name' }, target: '_blank' } },
            { label: 'Número de la Cuenta', fieldName: 'No_de_cliente__c', type: 'text', sortable: true },
            { label: 'Teléfono', fieldName: 'Phone', type: 'phone', sortable: true },
            { label: 'Última visita', fieldName: 'BPyP_Fecha_de_ultima_visita__c', type: 'Date', sortable: true },
            { label: 'Nombre del propietario', fieldName: 'OwnerName', type: 'text', sortable: true},
        ]);

        component.set("v.url", window.location.origin);
        component.set("v.seltitle", "PRIVADO");

        var actionfetchInfoByUser = component.get('c.fetchInfoByUser');
        actionfetchInfoByUser.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnWrapper = response.getReturnValue();
                component.set('v.wrapperObject', returnWrapper);
                component.set('v.ListOfDiv', returnWrapper.setDivisiones);
            }
        });
        $A.enqueueAction(actionfetchInfoByUser);

        if (component.get('v.bandera')) {
            component.set('v.bandera', true);
            this.cargoBkm(component);
        }
	},

    cargoBkm: function (component) {
        var Ri = component.get("v.RISelect");
        var actionRI = component.get('c.gtRISelect');
        actionRI.setParams({ idRI: Ri });
        actionRI.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.seltitle', response.getReturnValue());
            }
        });
        $A.enqueueAction(actionRI);
    },

    helperGenerateChart: function (component, e, helper) {
        window.dataToTable = function (dataset, title) {
            var html = '<html> <head> <style> th, td {border-bottom: 2px solid #ddd; text-align: center}</style> </head> <body><table>';
            html += '<thead><tr><th>' + title + '</th>';
            var columnCount1 = 0;
            jQuery.each(dataset.datasets, function (idx, item) {
                html += '<th style="text-align:center; background-color:' +
                    String(item.backgroundColor).substring(0, String(item.backgroundColor).length - 2) + '0.5);">' +
                    String(item.label).substring(0, String(item.label).indexOf(' ', 4)) + '<br/>' +
                    (String(item.label).substring(String(item.label).indexOf(' ', 4) + 1
                        , String(item.label).length)) + '</th>';
                columnCount1 += 1;
            });
            html += '</tr></thead>';
            jQuery.each(dataset.labels, function (idx, item) {
                html += '<tr><td>' + item + '</td>';
                for (var i = 0; i < columnCount1; i++) {
                    html += '<td style="text-align:center; background-color:' +
                        String(dataset.datasets[i].backgroundColor).substring(0, dataset.datasets[i].backgroundColor.length - 2) + '0.5);">' +
                        (dataset.datasets[i].data[idx] === '0' ? '-' : dataset.datasets[i].data[idx]) + '</td>';
                }
                html += '</tr>';
            });
            html += '</tr><tbody></table>';
            return html;
        };
        window.window.dataToTablePerc = function (dataset, title) {
            var html = '<html> <head> <style> th, td {border-bottom: 2px solid #ddd; text-align: center}</style> </head> <body><table>';
            html += '<thead><tr><th rowspan="2">' + title + '</th>';
            var columnCount = 0;
            jQuery.each(dataset.datasets, function (idx, item) {
                html += '<th colspan="2" style="text-align:center; background-color:' +
                    String(item.backgroundColor).substring(0, String(item.backgroundColor).length - 2) + '0.5);">' + item.label + '' + '</th>';
                columnCount += 1;
            });
            html += '</tr>';
            html += '<tr>';
            columnCount = 0;
            jQuery.each(dataset.datasets, function (idx, item) {
                html += '<th style="text-align:center; background-color:' +
                    String(item.backgroundColor).substring(0, String(item.backgroundColor).length - 2) +  '0.5);">' + 'Cuentas' + '</th>';
                html += '<th style="text-align:center; background-color:' +
                    String(item.backgroundColor).substring(0, String(item.backgroundColor).length - 2) + '0.5);">' + 'Porcentaje' + '</th>';
                columnCount += 1;
            });
            html += '</tr></thead>';
            jQuery.each(dataset.labels, function (idx, item) {
                html += '<tr><td>' + item + '</td>';
                for (var i = 0; i < columnCount; i++) {
                    html += '<td style="text-align:center; background-color:' +
                        String(dataset.datasets[i].backgroundColor).substring(0, dataset.datasets[i].backgroundColor.length - 2) + '0.5);">' +
                        (window.val[i][idx] === '0' ? '-' : window.val[i][idx]) + '</td>';
                    html += '<td style="text-align:center; background-color:' +
                        String(dataset.datasets[i].backgroundColor).substring(0, dataset.datasets[i].backgroundColor.length - 2) + '0.5);">' + '' +
                        (dataset.datasets[i].data[idx] === '0' ? '-' : dataset.datasets[i].data[idx]) + '%' + '</td>';
                } html += '</tr>';
            });
            html += '</tr><tbody></table>';
            return html;
        };
        window.barOptions_stacked_perc = {
            tooltips: {
                enabled: true, position: 'nearest', callbacks: {
                    label: function (tooltipItem, data) {
                        return data.datasets[tooltipItem.datasetIndex].label
                        + ': '
                        + window.val[tooltipItem.datasetIndex][tooltipItem.index]
                        + ' (' + tooltipItem.xLabel.toString()
                        + '%)';
                    },
                }
            },
            legend: { position: 'bottom', padding: 10, onClick: function (event, legendItem) { }, },
            hover: { animationDuration: 10 }, responsive: true, maintainAspectRatio: false, scales: {
                xAxes: [{
                    scaleLabel: { display: false },
                    gridLines: { display: false },
                    stacked: true, ticks: {
                        callback: function (value, index, values) {
                            return value + '%';
                        }
                    }
                }],
                yAxes: [{
                    scaleLabel: { display: false },
                    gridLines: { display: false },
                    stacked: true
                }]
            },
            animation: {
                onComplete: function () { helper.helperOnComplete(this, "tipo1"); }
            },
        };
        window.barOptions_stacked = {
            tooltips: { enabled: true, position: 'nearest', },
            legend: { position: 'bottom', padding: 10, onClick: function (event, legendItem) { }, },
            hover: { animationDuration: 10 },
            responsive: true, maintainAspectRatio: false, scales: {
                xAxes: [{
                    scaleLabel: { display: false },
                    gridLines: { display: false },
                    stacked: true,
                }],
                yAxes: [{
                    scaleLabel: { display: false },
                    gridLines: { display: false },
                    stacked: true
                }]
            },
            animation: {
                onComplete: function () { helper.helperOnComplete(this, "tipo2"); }
            },
        };
        window.redraw = true;

        component.set('v.IsSpinner',true);
        window.setTimeout(
            $A.getCallback(function() {
                helper.initdraw(component, e);
                component.set('v.IsSpinner',false);
            }), 5000
        );
    },

    combosSet: function (component, e) {
        if(component.get('v.vistaBnk')) {
            var wrapperObj = component.get('v.wrapperObject');
            var divSelected = component.get('v.seldiv');
            var offSelected = component.get('v.seloff');
            component.set('v.ListOfOff', wrapperObj.mapSucursales[divSelected]);

            if(offSelected !== "TODAS"){
                this.helperGetUsersByOffice(component);
            }
        }
    },

    initdraw: function (component, e) {
        var actionfetchusdata = component.get('c.fetchUserInfo');
        actionfetchusdata.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                this.verifyPerfil(component, e, response);
            }
        });
        $A.enqueueAction(actionfetchusdata);
    },
    verifyPerfil: function (component, e, response) {
        var perfil = String(response.getReturnValue()).split(',')[0];
        var division = String(response.getReturnValue()).split(',')[1];
        var oficina = String(response.getReturnValue()).split(',')[2];
        var usrname = String(response.getReturnValue()).split(',')[3];
        var title = String(response.getReturnValue()).split(',')[4];
        var adm = $A.get("$Label.c.MX_PERFIL_SystemAdministrator");
        var SoportePerfil ="Soporte BPyP";
        var staff = "BPyP STAFF";
        var dirdiv = "BPyP Director Divisional";
        var diroff = "BPyP Director Oficina";
        var bpypestandar = "BPyP Estandar";

        component.set("v.userPInfo", perfil);
        component.set("v.accessDiv", !component.get("v.accessCombos")[perfil].div);
        component.set("v.accessOff", !component.get("v.accessCombos")[perfil].off);
        component.set("v.accessBnk", !component.get("v.accessCombos")[perfil].bnk);
        component.set("v.accessTf", !component.get("v.accessCombos")[perfil].tf);

        if (perfil === (staff) || perfil === (adm) || perfil === (SoportePerfil)) {
            component.set('v.isDiv', 'TODAS');
            this.draw(component, e, 'Division');
            this.combosSet(component, e);
        } else if (perfil === dirdiv) {
            component.set('v.isDiv', true);
            component.set('v.seldiv', division);
            component.set('v.seloff', 'TODAS');
			this.draw(component, e, 'Oficina');
            this.combosSet(component, e);
        } else if (perfil === diroff) {
            component.set('v.isOff', true);
            component.set('v.seldiv', division);
            component.set('v.seloff', oficina);
            this.configDOff(component, e);
            this.combosSet(component, e);
        } else if (perfil === bpypestandar) {
            component.set('v.isBkm', true);
            component.set("v.seltitle", title);

            component.set('v.seldiv', division);
            component.set('v.ListOfDiv', [division]);

            component.set('v.seloff', oficina);
            component.set('v.ListOfOff', [oficina]);

            component.set('v.selbkm', usrname);
            component.set('v.ListOfBkMn', [{'Name':usrname}]);
            component.set('v.vistaBnk', false);

            this.draw(component, e, 'BanqueroOnly');
        }
    },

    configDOff: function(component, e) {
        if (component.get('v.bandera')) {
            component.set("v.accessBnk", true);
            component.set("v.accessTf", true);
            this.draw(component, e, 'BanqueroOnly');
        } else {
            component.set('v.selbkm', 'TODAS');
            this.draw(component, e, 'Banquero');
        }
    },

    gttypoQuery: function(component) {
      let parametro = '';
        let tipoQuery = this.tipoQueryF(component);
        if(tipoQuery === 'Oficina'){
            parametro = component.get("v.seldiv");
        } else if(tipoQuery === 'Banquero') {
            parametro = component.get("v.seloff");
        } else if(tipoQuery === 'BanqueroOnly') {
            parametro = component.get("v.selbkm");
        }
        return parametro;
    },

    draw: function (component, e, tipoConsulta) {
        var labelTypeQuery = {'Division' : 'DIVISIóN', 'Oficina' : 'OFICINA', 'Banquero' : 'BANQUERO', 'BanqueroOnly' : 'BANQUERO'};
        var parametro = this.gttypoQuery(component);

        this.helperProfileValidation(component);

        var ctx3 = component.find("myChartContacta").getElement();
        var action3 = component.get('c.fetchData');

        var listparams = [parametro];
        action3.setParams({
            tipoConsulta : tipoConsulta,
            titulo : component.get("v.seltitle"),
            params : listparams,
            startDte : component.find("expsdate").get("v.value"),
            endDte: component.find("expedate").get("v.value")
        });

        action3.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                var height = 100 + (res.lsLabels.toString().split(',').length * 30);

                component.set('v.chartHeight', height);
                if (window.myChartContacta != null) {
                    window.myChartContacta.clear();
                    window.myChartContacta.destroy();
                }
                window.myChartContacta = new Chart(ctx3, { type: 'horizontalBar', data: {}, options: window.barOptions_stacked_perc });
                this.helperLabels(res);
                myChartContacta.update();
                jQuery('#wrapperContacta').html(window.window.dataToTablePerc(window.myChartContacta.data, labelTypeQuery[tipoConsulta]));
            }
        });
        $A.enqueueAction(action3);
        this.helperProfileValidation(component);
    },

    helperProfileValidation: function (component) {
        if (window.redraw === false) {
            return;
        }
    },

    helperLabels: function (res) {
        var labels = [];
        for (var i = res.lsLabels.toString().split(',').length - 1; i >= 0; i--) {
            myChartContacta.config.data.labels.push(res.lsLabels.toString().split(',')[i]);
            labels.push(res.lsLabels.toString().split(',')[i]);
        }
        if (window.val != null) { window.val = null; }
        window.val = [];
        for (var j = res.lsTyOpp.toString().split(',').length - 1; j >= 0; j--) {
            var datatempConta = [];
            var datavalConta = [];
            for (var k = 0; k < labels.length; k++) {
                datatempConta.push(res.lsData[(res.lsTyOpp.toString().split(',')[j] + labels[k])] == null ? 0 : res.lsData[(res.lsTyOpp.toString().split(',')[j] + labels[k])]);
                datavalConta.push(res.numVis[(res.lsTyOpp.toString().split(',')[j] + labels[k])] == null ? 0 : res.numVis[(res.lsTyOpp.toString().split(',')[j] + labels[k])]);
            }
            window.val.push(datavalConta);

            var newDataset = { label: res.lsTyOpp.toString().split(',')[j], backgroundColor: res.lsColor.toString().split('),')[j] + ')', data: datatempConta, }
            myChartContacta.data.datasets.push(newDataset);
        }
    },

    helperOnComplete: function (fun, tipo) {
        var chartInstance = fun.chart;
        var ctx = chartInstance.ctx;
        ctx.textAlign = "left";
        ctx.fillStyle = "#fff";
        Chart.helpers.each(fun.data.datasets.forEach(function (dataset, i) {
            var meta = chartInstance.controller.getDatasetMeta(i);
            Chart.helpers.each(meta.data.forEach(function (bar, index) {
                var data = dataset.data[index];
                if (data !== 0 && tipo === "tipo1") { ctx.fillText(data + '%', bar._model.x - 40, bar._model.y - 10); }
                else if (data !== 0 && tipo === "tipo2") { ctx.fillText(data, bar._model.x - 20, bar._model.y - 10); }
            }), this)
        }), fun);
    },

    doFetchContact: function (component, e, offsetVal) {
        var action = component.get('c.contactabilityAccount');
        var contactType = component.get("v.contactStatus");
        var tittle = component.get("v.seltitle");
        var tipoQ = this.tipoQueryF(component);
        var parametro = this.gttypoQuery(component);
        var listparams = [parametro, contactType, tittle, '', tipoQ];
        action.setParams({
            params: listparams,
            contactType: contactType,
            startDte: component.find("expsdate").get("v.value"),
            endDte: component.find("expedate").get("v.value"),
            tittle: tittle,
            tipoQuery: tipoQ
        });
        action.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                component.set('v.hasListValue', true);
                component.set('v.numLeads', response.getReturnValue().length);
            }else {
                component.set('v.hasListValue', false);
            }
            component.set('v.ContactData', response.getReturnValue());
            this.pagListLoop(component, response);

        });
        $A.enqueueAction(action);
    },

    pagListLoop: function(component, response) {
        var PaginationList = [];
        var records = response.getReturnValue();
        records.forEach(function (record) {
            record.LinkName = component.get('v.url') + '/' + record.Id;
            record.OwnerName = record.Owner.Name;
            PaginationList.push(record);
        });
        if (component.get("v.ContactData").length <= 10) {
            component.set("v.tableHeight", 'height:auto;');
        } else {
            component.set("v.tableHeight", 'height:300px;');
        }
        component.set('v.PaginationList', PaginationList);
    },

    configPage: function(component, e) {
        component.set('v.NumOfPag', 0);
        component.set('v.ActPag', 1);
        var fetchPgs = component.get('c.fetchPgs');
        var totalrecord = component.get('v.numLeads') ? component.get('v.numLeads') : 0;
        fetchPgs.setParams({
            numRecords : totalrecord
        });
        fetchPgs.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				component.set('v.hasListValue', true);
                component.set('v.NumOfPag', response.getReturnValue());
                component.set('v.ActPag', 1);
                window.sizeofAcc = 10;
            }else {
                component.set('v.hasListValue', false);
            }
        });
        $A.enqueueAction(fetchPgs);
    },

    pagefunctionCont: function (component, pgMov, condition) {
        var contactabilityAccount = component.get('c.contactabilityAccount');
        var contactType = component.get("v.contactStatus");
        var tittle = component.get("v.seltitle");
        var tipoQ = this.tipoQueryF(component);
        var parametro = this.gttypoQuery(component);
        var listparams = [parametro, contactType, tittle, pgMov.toString(), tipoQ];
        contactabilityAccount.setParams({
            params: listparams,
            startDte: component.find("expsdate").get("v.value"),
            endDte: component.find("expedate").get("v.value")
        });
        contactabilityAccount.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if (component.find("contactStatus").get("v.value") !== "Ninguno") {
                    component.set('v.hasListValue', true);
                }
                if(condition) {
                    pgMov = pgMov / window.sizeofAcc;
                    pgMov++;
                    component.set('v.ActPag', pgMov);
                } else {
                    component.set('v.ActPag', 1);
                }
                component.set('v.ContactData', response.getReturnValue());

            }
        });
        $A.enqueueAction(contactabilityAccount);
    },

    tipoQueryF: function (component) {
        var  tipo = "";
        if(component.get("v.seldiv") === 'TODAS') {
            tipo = "Division";
        } else if(component.get("v.seloff") === 'TODAS') {
            tipo = "Oficina";
        } else if(component.get("v.selbkm") === 'TODAS') {
            tipo = "Banquero";
        } else {
            tipo = "BanqueroOnly";
        }
        return tipo;
    },

    helperGetUsersByOffice: function (component) {
        var actionUserByOfficeServ = component.get("c.fetchUserByOfficeServ");
        actionUserByOfficeServ.setParams({
            oficina: component.get("v.seloff"),
            titulo : component.get("v.seltitle")
        });
        actionUserByOfficeServ.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				component.set('v.ListOfBkMn', response.getReturnValue());
            } else {
                component.set('v.ListOfBkMn', null);
            }
        });
        $A.enqueueAction(actionUserByOfficeServ);
    },

    helperHandlerCombo: function(component, event) {
        this.combosSet(component, event);
        component.set('v.hasListValue', false);
        component.set('v.ContactData', null);
        if(component.find("contactStatus").get("v.value") !== 'Ninguno'){
            this.doFetchContact(component, event, 0);
        }
    },

    sortBy: function (field, reverse, primer) {
        var key = primer
            ? function (x) { return primer(x[field]) }
            : function (x) { return x[field] };
        return function (a1, b1) {
            var A = key(a1);
            var B = key(b1);
            return reverse * ((A > B) - (B > A));
        };
    },

    sortData: function (component, fieldName, sortDirection) {
        var dataC = component.get("v.PaginationList");
        var reverse = sortDirection !== 'asc';
        dataC = Object.assign([],
            dataC.sort(this.sortBy(fieldName, reverse ? -1 : 1))
        );
        component.set("v.PaginationList", dataC);
    }

})