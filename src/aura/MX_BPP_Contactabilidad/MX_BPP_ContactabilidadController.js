({
	doInit : function(component, event, helper) {
        helper.helperInit(component, event);
	},

    generateChart : function(component, event, helper){
        helper.helperGenerateChart(component, event, helper);
    },
    updDiv : function(component, event, helper){
        let perfilDiv = component.get("v.userPInfo");
        if(component.get("v.accessCombos")[perfilDiv].div) {
            component.set("v.seloff", "TODAS");
            component.set('v.ListOfOff', []);
            component.set('v.ListOfBkMn', []);
            component.set("v.selbkm", "TODAS");
            if(event.getSource().get("v.value") === "TODAS") {
                helper.draw(component, event, 'Division');
            } else {
            	helper.draw(component, event, 'Oficina');
            }
            helper.helperHandlerCombo(component, event);
        }
    },
  	updOffice : function(component, event, helper){
        let perfilDiv = component.get("v.userPInfo");
        if(component.get("v.accessCombos")[perfilDiv].off) {
            component.set("v.selbkm", "TODAS");
            component.set('v.ListOfBkMn', []);
            if(event.getSource().get("v.value") === "TODAS") {
                helper.draw(component, event, 'Oficina');
            } else {
            	helper.draw(component, event, 'Banquero');
            }
            helper.helperHandlerCombo(component, event);
        }
    },
    updOpAc : function(component, event, helper){
        helper.draw(component, event, helper.tipoQueryF(component));
        helper.combosSet(component, event);
        if(component.find("contactStatus").get("v.value") !== 'Ninguno'){
            component.set('v.hasListValue', false);
        	component.set('v.ContactData', null);
            helper.doFetchContact(component, event, 0);
        }
    },
    updBkM : function(component, event, helper){
        let perfilDiv = component.get("v.userPInfo");
        if(component.get("v.accessCombos")[perfilDiv].bnk) {
            if(event.getSource().get("v.value") === "TODAS") {
                helper.draw(component, event, 'Banquero');
            } else {
            	helper.draw(component, event, 'BanqueroOnly');
            }
            helper.helperHandlerCombo(component, event);
        }
    },
    updateAcc : function(component, event, helper){
        component.set('v.ContactData', null);
        component.set('v.hasListValue', false);
        if(component.find("contactStatus").get("v.value") !== 'Ninguno'){
            helper.doFetchContact(component, event, 0);
        }
    },

    frst: function (component, e, helper) {
        helper.pagefunctionCont(component, 0);
    },

    prev: function (component, e, helper) {
        var actPgprev = component.get('v.ActPag');
        var prv = actPgprev === 1 ? 1 : actPgprev - 1;
        prv--;
        prv = prv * window.sizeofAcc;
        helper.pagefunctionCont(component, prv, true);
    },

    next: function (component, e, helper) {
        var actPg = component.get('v.ActPag');
        var totPg = component.get('v.NumOfPag');
        var nxt = actPg === totPg ? totPg : actPg + 1;
        nxt--;
        nxt = nxt * window.sizeofAcc;
        helper.pagefunctionCont(component, nxt, true);
    },

    lst: function (component, e, helper) {
        var totPg = component.get('v.NumOfPag');
        totPg--;
        totPg = totPg * window.sizeofAcc;
        helper.pagefunctionCont(component, totPg, true);
    },

    updateColumnSorting: function (component, event, helper) {
        setTimeout(function () {
            var fieldName = event.getParam('fieldName');
            var sortDirection = event.getParam('sortDirection');
            component.set("v.sortedBy", fieldName);
            component.set("v.sortedDirection", sortDirection);
            helper.sortData(component, fieldName, sortDirection);
        }, 0);
    }

})