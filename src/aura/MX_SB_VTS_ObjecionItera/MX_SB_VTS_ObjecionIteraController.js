({
    changeclass : function(component, event, helper) {
        var spinner = event.currentTarget.name;
        var test = component.find(spinner);
        component.set('v.arrowObjecIcon', (component.get('v.arrowObjecIcon') === 'utility:up' ? 'utility:down' : 'utility:up'));
        $A.util.toggleClass(test,'slds-is-open');
    },
    saveObjections: function(component, event, helper) {
        component.set('v.errorMsjTitle', 'Éxito');
        component.set('v.errorMsj', $A.get("{!$Label.c.MX_SB_VTS_ObjecionCorrecta}"));
        helper.showToast(component, event, helper, 'success');
        helper.saveObjectCall(component, event, helper);
    },
    changeValLeft : function(component, event, helper) {
        component.set('v.isLoading', true);
        let lstText = component.get('v.objecion');
        lstText.usoObjecion = true;
        lstText.noUsoCatObject = false;
        var a = component.get('c.saveObjections');
        $A.enqueueAction(a);
        component.set('v.isLoading', false);
    },
    changeValRight : function(component, event, helper) {
        component.set('v.isLoading', true);
        let lstText = component.get('v.objecion');
        lstText.noUsoCatObject = true;
        lstText.usoObjecion = false;
        var a = component.get('c.saveObjections');
        $A.enqueueAction(a);
        component.set('v.isLoading', false);
    },
    afterUpDate: function(component, event, helper) {
        let varSObject = component.get('v.genericRecord');
        if($A.util.isEmpty(varSObject.Id)) {
            helper.manageErrors(component, event, helper, $A.get("{!$Label.c.MX_SB_VTS_errorGetRecords}"), $A.get("{!$Label.c.MX_SB_VTS_errorGetObjetions}"));
        } else {
            try {
                helper.getObjetions(component, event, helper);
            } catch (error) {
                helper.manageErrors(component, event, helper, $A.get("{!$Label.c.MX_SB_VTS_errorPermisions}"), $A.get("{!$Label.c.MX_WB_lg_TlmktError}"));
            }
        }
    },
})