({
    getContractData : function(component, event, helper) {
        var action = component.get("c.getContractData");
        action.setParams({'idOpp' : component.get('v.recordId')});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue()[0]!==undefined) {
                    var storeResponse = response.getReturnValue();
                    component.set('v.varVigencia', storeResponse[0].StartDate);
                    component.set('v.varPoliza', storeResponse[0].MX_SB_SAC_NumeroPoliza__c);
                } else {
                    var now_date = new Date();
                    var tomorrow=now_date.setDate(now_date.getDate() + 1);
                    var tomorrow_Date= new Date(tomorrow).toISOString().split('T')[0];
                    var tomorrow_DateVa = $A.localizationService.formatDate(tomorrow_Date, "dd/MM/yyyy");
                    var tomorrow_DateVar = tomorrow_DateVa.toLocaleString('en-US', { timeZone: 'America/mexico_city' });
                    component.set('v.varVigencia',tomorrow_DateVar);
                    component.set('v.varPoliza', component.get('v.varPoliza'));
                }
            }
        });
        $A.enqueueAction(action);
    },
    getClonedata : function(component, event, helper) {
        var action = component.get("c.getCloneOpp");
        action.setParams({'oppid' : component.get('v.recordId'),'product' : component.get('v.listValue')});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if(storeResponse!=null) {
                    var toastEvent1 = $A.get("e.force:showToast");
                        toastEvent1.setParams({
                            "title": "Success!",
                            "type" : "success",
                            "message": "The record has been updated successfully."
                        });
                    toastEvent1.fire();
                    var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                            "url": "/"+storeResponse
                        });
                    urlEvent.fire();
                } else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type" : "error",
                        "message": "El registro no fue creado correctamente."
                    });
                    toastEvent.fire();
                }
            }
            else {
                var toastEvent2 = $A.get("e.force:showToast");
                toastEvent2.setParams({
                    "title": "Error!",
                    "type" : "error",
                    "message": "El registro no fue creado correctamente."
                });
                toastEvent2.fire();
            }
        });
        $A.enqueueAction(action);
    },
})