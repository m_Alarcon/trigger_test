({
    handleClick: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/lightning/page/home"
        });
        urlEvent.fire();
    },
    initResumen : function(component, event, helper) {
        var action2= component.get('c.loadOppF');
        var recordId=component.get('v.recordId');
        helper.getContractData(component, event, helper);
        action2.setParams({'oppId':recordId});
        action2.setCallback(this, function(responseA) {
            if(responseA.getState()==='SUCCESS') {
                component.set("v.OppObj",responseA.getReturnValue());
                var action = component.get('c.loadAccountF');
                action.setParams({'recId':responseA.getReturnValue().AccountId});
                action.setCallback(this, function(responseB) {
                    component.set("v.accObj",responseB.getReturnValue()[0]);
                });
                $A.enqueueAction(action);
                var action3 = component.get('c.loadContactAcc');
                action3.setParams({'accId':responseA.getReturnValue().AccountId});
                action3.setCallback(this, function(responseC) {
                    component.set('v.CntctObj',responseC.getReturnValue()[0]);
                });
                $A.enqueueAction(action3);
                var action4= component.get('c.loadQuoteLineItem');
                action4.setParams({'value': responseA.getReturnValue().SyncedQuoteId});
                action4.setCallback(this, function(responseD) {
                   component.set('v.quoteLineItem', responseD.getReturnValue()[0]);
                });
                $A.enqueueAction(action4);
            }
        });
		$A.enqueueAction(action2);
    },
    showmodal : function(component, event, helper) {
        component.set('v.modalNew',true);
    },
    showmodalof : function(component, event, helper) {
        component.set('v.modalNew',false);
    },
    getClone : function (component, event, helper) {
        helper.getClonedata(component, event, helper);
    },
})