({
    inicializarColas: function(component, event) {
        var action = component.get("c.grupoColas");
        action.setCallback(this, function(respuesta) {
            var state = respuesta.getState();
            if (state === "SUCCESS") {
                var respuestaDeUsuario = respuesta.getReturnValue();
                component.set("v.listaGrupos", respuestaDeUsuario);
                var listaGrupos = component.get("v.listaGrupos");
                var items = [];
                for (var i = 0; i < listaGrupos.length; i++) {
                    var item1 = {"label":listaGrupos[i].Name,
                                 "value":listaGrupos[i].Id};
                    items.push(item1);
                }
                var item10 = {"label":"Sin asignacion",
                             "value":"noAsignada"};
                items.push(item10);
                component.set("v.optionsQueue", items);
            } else if (state === "INCOMPLETE") {
                alert('respuesta incorrecta');
            } else if (state === "ERROR") {
                var errors = respuesta.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        alert("Error de mensaje: " + errors[0].message);
                    }
                } else {
                    alert("Error desconocido");
                }
            }
        });
        $A.enqueueAction(action);
    },
    enviaUsuarios: function (component, event,listaUsr) {
        var listaUsuarios = listaUsr;
        var listaId = [];
        for (var i = 0; i < listaUsuarios.length; i++) {
            listaId.push(""+listaUsuarios[i].Id);
        }
        this.buscaAsignaciones(component,listaId);
    },
    busquedaColas: function(component, event) {
        var action = component.get("c.buscarUsuarios");
        action.setParams({
            'nombreABuscar': component.get("v.palabraClave")});
        action.setCallback(this, function(respuesta) {
            var state = respuesta.getState();
            if (state === "SUCCESS") {
                var respuestaDeUsuario = respuesta.getReturnValue();
                if (respuestaDeUsuario.length === 0) {
                    component.set("v.mensaje", true);
                } else {
                    component.set("v.mensaje", false);
                }
                component.set("v.numeroTotalDeRegistros", respuestaDeUsuario.length);
                component.set("v.Usuarios", respuestaDeUsuario);
                this.enviaUsuarios(component, event,respuestaDeUsuario);
            } else if (state === "INCOMPLETE") {
                alert('respuesta incorrecta');
            } else if (state === "ERROR") {
                var errors = respuesta.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error de mensaje: " +
                              errors[0].message);
                    }
                } else {
                    alert("Error desconocido");
                }
            }
            component.set('v.loaded', true);
        });
        $A.enqueueAction(action);
    },
    buscaAsignaciones: function(component, listaId) {
        var action = component.get("c.buscarColas");
        action.setParams({
            'listaDeId': listaId
        });
        this.respuestaColas(component, action);
    },
    buscarMiembroCola: function(component, colaBuscar) {
        var action = component.get("c.buscarMiembroCola");
        action.setParams({
            'colaBuscar': component.get("v.grupoId")
        });
            this.respuestaColas(component, action);
    },
    respuestaColas: function(component, action) {
        action.setCallback(this, function(respuesta) {
            var returnAsignaciones = [];
            var state = respuesta.getState();
            if (state === "SUCCESS") {
                returnAsignaciones = respuesta.getReturnValue();
                component.set("v.listaAsignaciones", returnAsignaciones);
            } else if (state === "INCOMPLETE") {
                alert('respuesta incorrecta');
            } else if (state === "ERROR") {
                var errors = respuesta.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error de mensaje: " + errors[0].message);
                    }
                } else {
                    alert("Error desconocido");
                }
            }
        });
        $A.enqueueAction(action);
    },
    getUsers: function (component, event,listaUsr) {
        var action = component.get("c.buscarUsuariosRole");
        action.setParams({
            'colaID': component.get("v.grupoId")
        });
        action.setCallback(this, function(respuesta) {
            var usuarios = [];
            var state = respuesta.getState();
            if (state === "SUCCESS") {
                usuarios = respuesta.getReturnValue();
                component.set("v.Usuarios", usuarios);
                component.set("v.numeroTotalDeRegistros", usuarios.length);
                this.listaSeleccion(component, event,usuarios);
            } else if (state === "INCOMPLETE") {
                alert('respuesta incorrecta');
            } else if (state === "ERROR") {
                var errors = respuesta.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error de mensaje: " + errors[0].message);
                    }
                } else {
                    alert("Error desconocido");
                }
            }
            component.set('v.loaded', true);
        });
        $A.enqueueAction(action);
    },
    listaSeleccion: function (component, event,listaUsr) {
        var listaUsuarios = listaUsr;
        var listaId = [];
        for (var i = 0; i < listaUsuarios.length; i++) {
            listaId.push(""+listaUsuarios[i].Id);
        }
        component.set("v.idsSeleccionados", listaId);
        component.set('v.loaded', true);
    }
})