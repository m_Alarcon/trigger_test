({
    doInit : function(component, event, helper) {
        helper.inicializarColas (component, event);
        var items = [];
        var item1 = {"label":"Asignada",
                     "value":"asignada"};
        var item2 = {"label":"Sin asignación",
                     "value":"noAsignada"};
        items.push(item1);
        items.push(item2);
        component.set("v.options", items);
    },
    boton: function(component, event, helper) {
        component.set("v.porCola", false);
        component.set('v.loaded', false);
        var CampoBusqueda = component.find('CampoBusqueda');
        var faltaValor = CampoBusqueda.get('v.validity').valueMissing;
        if(faltaValor) {
            CampoBusqueda.mostrarMensajeDeAyuda();
            CampoBusqueda.focus();
            component.set('v.loaded', true);
        } else {
            helper.busquedaColas(component, event);
        }
    },
    busquedaPorCola: function (component, event, helper) {
        component.set("v.idsSeleccionados", []);
        component.set("v.porCola", true);
        component.set('v.loaded', false);
        helper.getUsers(component, event);
    },
    asignarTodos : function (component, event, helper) {
        var accion = component.get("c.guardarNuevasAsignaciones");
        var idUsuarios = component.get("v.idsSeleccionados");
        var origin = component.get("v.grupoId");
        var destiny = component.get("v.colaSeleccionada");
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams( {
            title : 'Éxito',
            message: 'Se guardaron los registros',
            type: 'success'
        });
        accion.setParams({
            'userIds': idUsuarios,
            'origen': origin,
            'destino': destiny
        });
        accion.setCallback(this, function(guardarAsignaciones) {
            var state = guardarAsignaciones.getState();
            if(state === "SUCCESS") {
                toastEvent.fire();
            } else if(state === "INCOMPLETE") {
                alert('respuesta incorrecta');
            } else if(state === "ERROR") {
                var errors = respuesta.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        alert("Error de mensaje: " + errors[0].message);
                    }
                } else {
                    alert("Error desconocido");
                }
            }
        });
        $A.enqueueAction(accion);
    }
})