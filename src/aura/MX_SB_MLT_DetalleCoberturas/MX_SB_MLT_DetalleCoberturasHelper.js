({  consultaCoberturas : function(component, event, helper) {
        var actioncob = component.get("c.getTableCoberturas");
        var actioCobDed = component.get("c.getTableDeducibles");
        actioncob.setParams({"ideuno":"test","test":"otravar"});
        actioncob.setCallback(this, function(response) {
            var statecob = response.getState();
            if (statecob === "SUCCESS") {
                component.set("v.infoCobertura", response.getReturnValue());
            }
        });
        actioCobDed.setParams({"ideuno":"test"});
        actioCobDed.setCallback(this, function(response) {
            var stateded = response.getState();
            if (stateded === "SUCCESS") {
                component.set("v.infoDeducible",response.getReturnValue());
            }
        });
        $A.enqueueAction(actioncob);
        $A.enqueueAction(actioCobDed);
    },
    tabCobertura : function(component, event) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
			workspaceAPI.isSubtab({
				tabId: response.tabId
            	}).then(function(response) {
                	if (response) {
						var focusedTabId = response.tabId;
            			workspaceAPI.setTabLabel({
                		tabId: focusedTabId,
                		label: "Coberturas",
            			});
            			workspaceAPI.setTabIcon({
                		tabId: focusedTabId,
                		icon: "action:quote",
                        iconAlt:"Coberturas"
            			});
                	}
            });
        }).catch(function(error) {
        });
    },
})