({
    init : function(component, event, helper) {
        helper.initDetails(component, event, helper);
    },
    noEmail : function(component, event, helper) {
		helper.noEmail(component, event, helper);
	},
    asegurado : function(component, event, helper) {
        helper.esAsegurado(component, event, helper);
    },
    hidden : function(component, event, helper) {
      	helper.chooseKit(component, event, helper);
    },
    cancel : function(component, event, helper) {
        helper.cancelar(component, event, helper);
    },
    upper : function(component, event, helper) {
        let fieldId = event.getSource().getLocalId();
        helper.uppCase(component, event, helper, fieldId);
    },
    keyCheck : function(component, event, helper) {
        helper.keyCheck(component, event, helper);
    },
    closeModel : function(component, event, helper) {
        helper.closeModel(component, event, helper);
    },
    callRowAction : function(component, event, helper) {
        helper.callRowAction(component, event, helper);
    },
    handleClick : function(component, event, helper) {
    	helper.handleClick(component, event, helper);
	}
})