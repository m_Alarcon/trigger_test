({
    initDetails : function(component, event, helper) {
        let sinId = component.get("v.recordId");
        let minDate = new Date();
        let maxDate = minDate;
        maxDate = $A.localizationService.formatDate(maxDate, "YYYY-MM-DD");
        component.set("v.maxDate", maxDate);
        let act = component.get("c.searchSinVida");
        act.setParams({"siniId": sinId});
        act.setCallback(this, function(response){
            let state = response.getState();
            if(state === "SUCCESS"){
                let infResp = response.getReturnValue();
                component.set("v.sinDetalle",infResp[0]);
                component.set("v.nombre",infResp[0].MX_SB_MLT_NombreConductor__c);
                component.set("v.apPaterno",infResp[0].MX_SB_MLT_APaternoConductor__c);
                component.set("v.apMaterno",infResp[0].MX_SB_MLT_AMaternoConductor__c);
				component.set("v.email",infResp[0].MX_SB_MLT_CorreoElectronico__c);
                component.set("v.nameInsur",infResp[0].MX_SB_SAC_Contrato__r.MX_SB_SAC_NombreClienteAseguradoText__c + " " +
                             infResp[0].MX_SB_SAC_Contrato__r.MX_WB_apellidoPaternoAsegurado__c + " " +
                             infResp[0].MX_SB_SAC_Contrato__r.MX_WB_apellidoMaternoAsegurado__c);
                component.set("v.fechaNac",infResp[0].MX_SB_MLT_FechaNacConductor__c);
                if(infResp[0].MX_SB_MLT_SubRamo__c === "Fallecimiento") {
                    component.find("radioAs").set("v.disabled",true);
                } else {
					component.find("radioAs").set("v.disabled",false);
                }
            }
        });
        $A.enqueueAction(act);
    },
    noEmail : function(component, event, helper) {
        let isTrue = component.find("email").get("v.disabled");
        let checkInsur = component.get("v.radioReport");
        let sin = component.get("v.sinDetalle");
        if(!isTrue){
            component.set("v.email",'notienecorreo@gmail.com');
            component.find("email").set("v.disabled",true);
        } else {
            if(checkInsur === 'true'){
                component.set("v.email",sin.MX_SB_SAC_Contrato__r.MX_SB_SAC_EmailAsegurado__c);
                component.find("email").set("v.disabled",false);
            }
            if(checkInsur === 'false'){
                component.set("v.email",sin.MX_SB_MLT_CorreoElectronico__c);
                component.find("email").set("v.disabled",false);
            }
        }
	},
    esAsegurado : function(component, event, helper) {
        let asegurado = component.get("v.radioReport");
        let infoReport = component.get("v.sinDetalle");
        if(asegurado === 'true'){
            component.set("v.nombre",infoReport.MX_SB_SAC_Contrato__r.MX_SB_SAC_NombreClienteAseguradoText__c);
            component.set("v.apPaterno",infoReport.MX_SB_SAC_Contrato__r.MX_WB_apellidoPaternoAsegurado__c);
            component.set("v.apMaterno",infoReport.MX_SB_SAC_Contrato__r.MX_WB_apellidoMaternoAsegurado__c);
            component.set("v.email",infoReport.MX_SB_SAC_Contrato__r.MX_SB_SAC_EmailAsegurado__c);
            component.set("v.fechaNac",infoReport.MX_SB_MLT_FechaNacimiento__c);
        }
        if(asegurado === 'false'){
            component.set("v.nombre",infoReport.MX_SB_MLT_NombreConductor__c);
            component.set("v.apPaterno",infoReport.MX_SB_MLT_APaternoConductor__c);
            component.set("v.apMaterno",infoReport.MX_SB_MLT_AMaternoConductor__c);
            component.set("v.email",infoReport.MX_SB_MLT_CorreoElectronico__c);
            component.set("v.fechaNac",infoReport.MX_SB_MLT_FechaNacConductor__c);
        }
    },
    chooseKit : function(component, event, helper) {
        let vName = component.find("nameRep").get("v.validity");
        let vLastName = component.find("lastNameRep").get("v.validity");
        let vEmail = component.find("email").get("v.validity");
        let vCurp = component.find("curp").get("v.validity");
        let vFechaNac = component.find("fechaNac").get("v.validity");
        let vFechaSin = component.find("fechaSin").get("v.validity");
        let vCodes = helper.validCodes(component, event, helper);
        if (vName.valid && vLastName.valid && vEmail.valid && vCurp.valid && vFechaSin.valid && vFechaNac.valid && vCodes) {
            if(component.get("v.sinDetalle.MX_SB_MLT_Curp__c").length !== 18){
                let toastCurp = $A.get("e.force:showToast");
                toastCurp.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Favor de ingresar curp valido"
                })
                toastCurp.fire();
            } else {
                helper.address(component, event, helper);
            }
        } else {
            component.find("nameRep").reportValidity();
            component.find("lastNameRep").reportValidity();
            component.find("email").reportValidity();
            component.find("curp").reportValidity();
            component.find("fechaNac").reportValidity();
            component.find("fechaSin").reportValidity();
            let toastCodes = $A.get("e.force:showToast");
                toastCodes.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Se deben llenar todos los campos obligatorios."
                })
                toastCodes.fire();
        }
    },
    cancelar : function(component, event, helper) {
        let cancelId = component.get("v.recordId");
        var btnVal = event.getSource().get("v.value");
        var act = component.get("c.siniestroAction");
        act.setParams({'siniestroAct': btnVal, 'sinId':cancelId});
        act.setCallback(this, function(response){
            var state = response.getState();
            if (state === 'SUCCESS'){
                location.reload();
            }
        });
        $A.enqueueAction(act);
    },
    saveDetalles : function(component, event, helper) {
        let detailsId = component.get("v.recordId");
        let nameRep = component.get("v.name");
        let apPRep = component.get("v.apPaterno");
        let apMRep = component.get("v.apMaterno");
        let emailRep = component.get("v.email");
        let birthDRep = component.get("v.fechaNac");
        let fechaSin = component.get("v.sinDetalle.MX_SB_MLT_DateDifunct__c");
        fechaSin = $A.localizationService.formatDate(fechaSin, "YYYY-MM-DD");
        let tipoSin = "Natural";
        component.set("v.sinDetalle.Id",detailsId);
        component.set("v.sinDetalle.MX_SB_MLT_NombreConductor__c",nameRep);
        component.set("v.sinDetalle.MX_SB_MLT_APaternoConductor__c",apPRep);
        component.set("v.sinDetalle.MX_SB_MLT_AMaternoConductor__c",apMRep);
        component.set("v.sinDetalle.MX_SB_MLT_CorreoElectronico__c",emailRep);
        component.set("v.sinDetalle.MX_SB_MLT_FechaNacConductor__c",birthDRep);
        component.set("v.sinDetalle.MX_SB_MLT_Subtipo__c",tipoSin);
        component.set("v.sinDetalle.MX_SB_MLT_FechaNacimiento__c","1992-09-20");
        component.set("v.sinDetalle.Fechasiniestro__c",fechaSin);
        let detailAct = component.get("c.updateSiniestro");
        detailAct.setParams({"sin": component.get("v.sinDetalle")});
        detailAct.setCallback(this, function(response){
        	let state = response.getState();
        	if(state === "SUCCESS"){
                let toastDetail = $A.get("e.force:showToast");
                toastDetail.setParams({
                    "title": "Éxito!",
                    "type": "success",
                    "message": "Se han guardado los detalles del siniestro"
                })
                toastDetail.fire();
                component.set("v.detalles",false);
        	}
        });
        $A.enqueueAction(detailAct);
    },
    uppCase : function(component, event, helper, fieldId) {
        switch (fieldId) {
            case 'nameRep':
                let nameRep = component.get("v.nombre");
                component.set("v.nombre",nameRep.replace(/[^\w\s]/gi,'').toUpperCase());
                break;
            case 'lastNameRep':
                let lastNameRep = component.get("v.apPaterno");
                component.set("v.apPaterno",lastNameRep.replace(/[^\w\s]/gi,'').toUpperCase());
                break;
            case 'lastName2Rep':
                let lastName2Rep = component.get("v.apMaterno");
                component.set("v.apMaterno",lastName2Rep.replace(/[^\w\s]/gi,'').toUpperCase());
                break;
            case 'curp':
                let curp = component.get("v.sinDetalle.MX_SB_MLT_Curp__c");
                component.set("v.sinDetalle.MX_SB_MLT_Curp__c",curp.replace(/[^\w\s]/gi,'').toUpperCase());
                break;
            default:
    	}
    },
    address : function(component, event, helper) {
		let validAdd = component.get("c.searchSinVida");
        validAdd.setParams({"siniId": component.get("v.sinDetalle.Id")});
        validAdd.setCallback(this, function(response){
        	let state = response.getState();
        	if(state === "SUCCESS"){
                let isValid = response.getReturnValue();
                if(isValid[0].MX_SB_MLT_DireccionDestino__c == null || isValid[0].MX_SB_MLT_Address__c == null){
                    let toastAdd = $A.get("e.force:showToast");
                    toastAdd.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Favor de llenar la direccion del asegurado/siniestro"
                    })
                    toastAdd.fire();
                } else {
                    helper.saveDetalles(component, event, helper);
                }
            }
        });
		$A.enqueueAction(validAdd);
    },
    keyCheck : function(component, event, helper){
        if (event.which === 13){
            let tipo = event.type;
            if(tipo === 'keypress'){
                let check = component.get("v.sinDetalle.MX_SB_MLT_Ocupacion__c");
                helper.validOcup(component, event, helper, check);
            }
            if(tipo === 'keyup'){
            	let icd = component.get("v.sinDetalle.MX_SB_MLT_ICD__c");
            	helper.validICD(component, event, helper, icd);
            }
        }
    },
    closeModel : function(component, event, helper) {
        component.set("v.isModalOpen", false);
    },
    callRowAction : function(component, event, helper){
        var row = event.getParam('row');
        var type = component.get("v.modalTitle");
        if(type === 'Ocupacion'){
            component.set("v.sinDetalle.MX_SB_MLT_Ocupacion__c",row.MX_SB_MLT_Descripcion__c);
            component.set("v.sinDetalle.MX_SB_MLT_CodOcupacion__c",row.MX_SB_MLT_CodeClaim__c);
            component.set("v.isModalOpen", false);
            let toastOcup = $A.get("e.force:showToast");
                        toastOcup.setParams({
                            "title": "Exito!",
                            "type": "success",
                            "message": "Se ha seleccionado "+row.MX_SB_MLT_Descripcion__c
                        })
                        toastOcup.fire();
         } else {
            component.set("v.sinDetalle.MX_SB_MLT_ICD__c",row.MX_SB_MLT_Descripcion__c);
            component.set("v.sinDetalle.MX_SB_MLT_PublicId_ICD__c",row.MX_SB_MLT_CodeClaim__c);
            component.set("v.sinDetalle.MX_SB_MLT_CodICD__c",row.MX_SB_MLT_Code__c);
            component.set("v.isModalOpen", false);
            let toastICD = $A.get("e.force:showToast");
                        toastICD.setParams({
                            "title": "Exito!",
                            "type": "success",
                            "message": "Se ha seleccionado "+row.MX_SB_MLT_Descripcion__c
                        })
                        toastICD.fire();
            }
    },
    handleClick : function (component, event, helper) {
		let tipo = event.getSource().get("v.name");
        if(tipo === 'borrarOcp'){
            let checkOcp = component.get("v.sinDetalle.MX_SB_MLT_CodOcupacion__c");
            if(checkOcp == null || checkOcp === ""){
            	helper.toastType(component, event, helper, '5');
            } else {
            	component.set("v.sinDetalle.MX_SB_MLT_Ocupacion__c","");
            	component.set("v.sinDetalle.MX_SB_MLT_CodOcupacion__c","");
            	helper.toastType(component, event, helper, '3');
            }
         } else {
            let checkICD = component.get("v.sinDetalle.MX_SB_MLT_CodICD__c");
            if(checkICD == null || checkICD === ""){
            	helper.toastType(component, event, helper, '5');
            } else {
            	component.set("v.sinDetalle.MX_SB_MLT_ICD__c","");
            	component.set("v.sinDetalle.MX_SB_MLT_CodICD__c","");
                component.set("v.sinDetalle.MX_SB_MLT_PublicId_ICD__c","");
                component.set("v.codeIcd","");
            	helper.toastType(component, event, helper, '3');
            }
         }
 	},
    validOcup : function (component, event, helper, check) {
    	if(check == null || check === ""){
            component.set("v.sinDetalle.MX_SB_MLT_Ocupacion__c","");
			helper.toastType(component, event, helper, '1');
            } else if(check.length >= 3){
                helper.ocupacion(component, event, helper, check);
            } else {
            	helper.toastType(component, event, helper, '2');
            }
    },
    validICD : function (component, event, helper, icd) {
    	if(icd == null || icd === ""){
            	component.set("v.sinDetalle.MX_SB_MLT_ICD__c","");
				helper.toastType(component, event, helper, '1');
            } else if(icd.length >= 3){
                helper.icd(component, event, helper, icd);
            } else {
            	helper.toastType(component, event, helper, '2');
            }
    },
    ocupacion : function (component, event, helper, check) {
    	var action = component.get("c.getCatalogData");
        action.setParams({"descripcion": check, "tipo":"Ocupación"});
        action.setCallback(this, function(response) {
        	var state = response.getState();
            if (state === "SUCCESS") {
            	var ocup = response.getReturnValue();
            	if(ocup.length === 0){
            		helper.toastType(component, event, helper, '4');
            	} else {
            		component.set("v.modalTitle","Ocupacion");
            		component.set("v.listOcp", ocup);
            		component.set("v.isModalOpen", true);
            	}
         	}
         })
         $A.enqueueAction(action);
    },
    icd : function (component, event, helper, icd) {
    	var action = component.get("c.getCatalogData");
        action.setParams({"descripcion": icd, "tipo":"ICD"});
        action.setCallback(this, function(response) {
        	var state = response.getState();
            if (state === "SUCCESS") {
            	var icdResp = response.getReturnValue();
            	if(icdResp.length === 0){
            		helper.toastType(component, event, helper, '4');
            	} else {
            		component.set("v.modalTitle","ICD");
            		component.set("v.listOcp",icdResp);
            		component.set("v.isModalOpen",true);
            	}
         	}
         })
         $A.enqueueAction(action);
    },
    toastType : function (component, event, helper, type) {
            switch(type){
            	case '1':
            		let toast1 = $A.get("e.force:showToast");
                    toast1.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Favor de ingresar un criterio de búsqueda"
                    })
                    toast1.fire();
            	break;
            	case '2':
            		let toast2 = $A.get("e.force:showToast");
                    toast2.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Favor de ingresar al menos una palabra"
                    })
                    toast2.fire();
            	break;
            	case '3':
            		let toast3 = $A.get("e.force:showToast");
                    toast3.setParams({
                        "title": "Exito!",
                        "type": "success",
                        "message": "Se ha borrado la selección"
                    })
                    toast3.fire();
            	break;
            	case '4':
            		let toast4 = $A.get("e.force:showToast");
                    toast4.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "No existen coincidencias."
                    })
                    toast4.fire();
            	break;
            	case '5':
            		let toast5 = $A.get("e.force:showToast");
                    toast5.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Favor de realizar una busqueda."
                    })
                    toast5.fire();
            	break;
            }
    },
    validCodes : function(component, event, helper) {
    let vICD = component.get("v.sinDetalle.MX_SB_MLT_PublicId_ICD__c");
    let vOcup = component.get("v.sinDetalle.MX_SB_MLT_CodOcupacion__c");
        if(vICD == null || vOcup == null || vICD === "" || vOcup === "") {
            return false;
        } else {
         return true;
         }
     }
})