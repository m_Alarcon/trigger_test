({
	showInfoVida: function(component, event, helper) {
       var selectedMenuItemValU = event.getParam("value");
		var selectedMenuItemValue= selectedMenuItemValU.toLowerCase();
        component.set("v.toggleAddrem",!component.get('v.toggleAddrem'));
        component.set('v.sinObj.MX_SB_MLT_PhoneTypeAdditional__c',selectedMenuItemValue)
        if(selectedMenuItemValue==='oficina'){
           component.set("v.BooleanExt1",true)
        }
    },
    showInfoVida2: function(component, event, helper) {
	var selectedMenuItemValuE = event.getParam("value");
	var selectedMenuItemValue= selectedMenuItemValuE.toLowerCase();
        component.set("v.toggleAddrem2",!component.get('v.toggleAddrem2'));
        component.set('v.sinObj.MX_SB_MLT_PhoneTypeAdditional2__c',selectedMenuItemValue);
		if(selectedMenuItemValue==='oficina'){
           component.set("v.BooleanExt2",true)
        }
    },
	closeToggleVida2: function(component, event, helper) {
     component.set("v.toggleAddrem2",!component.get('v.toggleAddrem2'));
     component.set("v.sinObj.MX_SB_MLT_Mobilephone2__c",'');
     component.set("v.sinObj.MX_SB_MLT_ExtPhone2__c",'');
     component.set("v.BooleanExt2",false)
    },
    closeToggleVida: function(component, event, helper) {
     component.set("v.toggleAddrem",!component.get('v.toggleAddrem'));
     component.set("v.sinObj.MX_SB_MLT_Mobilephone__c",'');
     component.set("v.sinObj.MX_SB_MLT_ExtPhone__c",'');
     component.set("v.BooleanExt1",false)
    },
    doInitLayVida : function(component, event, helper) {
	helper.changeTabNamePar(component, event);
        var action = component.get("c.getRecordType");
        var tphone = component.get("v.Telefono");
        if(tphone!==undefined){
         component.set("v.sinObj.MX_SB_MLT_Telefono__c",tphone);
        }
    var subramo= component.get('v.selected2');
    component.set("v.sinObj.MX_SB_MLT_SubRamo__c",subramo);
        if(subramo==='Ahorro' || subramo==='Salud' || subramo==='Fallecimiento') {
            action.setParams({"devname":"MX_SB_MLT_RamoVida"});
        }
    action.setCallback(this, function(response) {
            component.set("v.recordtypeVida",response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    setPhoneAdType2:function(component,event, helper) {
        var tipophoneAdVal2= component.get("v.sinObj.MX_SB_MLT_PhoneTypeAdditional2__c");
        if(tipophoneAdVal2==="oficina"){
            component.set("v.BooleanExt2",true);
        }
        else{
            component.set("v.BooleanExt2",false);
        }
    },
    setPhoneAdType:function(component,event, helper) {
    	var tipophoneAdVal= component.get("v.sinObj.MX_SB_MLT_PhoneTypeAdditional__c").toLowerCase();
        if(tipophoneAdVal==="oficina"){
            component.set("v.BooleanExt1",true);
        }
        else{
            component.set("v.BooleanExt1",false);
        }
    },
    setPhoneType: function(component, event, helper){
        var tipophone= component.find("phoneType");
        var tipophoneVal= tipophone.get("v.value");
        component.set("v.sinObj.MX_SB_MLT_PhoneType__c",tipophoneVal);
    },
    NuevoOnsuccessVida: function(component, event, helper) {
        var recordtpVida= component.get("v.recordtypeVida");
        component.set("v.sinObj.RecordTypeId",recordtpVida);
         var recIdV=component.get("v.recordId");
        if(recIdV!==''){
            component.set("v.sinObj.Id", recIdV);
        }
        component.set("v.sinObj.MX_SB_MLT_Telefono__c",component.get("v.Telefono"));
        helper.requiredFields(component,event,helper);
        if(component.get("v.validate")==='true') {
            component.set("v.validate","true");
        }
        if(component.get("v.validate")==='false') {
            var sinies= component.get("v.sinObj");
            var action2V = component.get('c.upSertsini');
            component.set("v.disabled",true);
            action2V.setParams({'sinIn': sinies});
            action2V.setCallback(this, function(response) {
                if (response.getState() === 'SUCCESS') {
                    helper.toastSuccessVida(component,event,helper);
                    component.set("v.disabled",true);
                }
                else{
                    component.set("v.disabled",false);
                }
            });
            $A.enqueueAction(action2V);
        }
        if(component.get("v.validate")===undefined) {
            var siniestr= component.get("v.sinObj");
            var action3V = component.get('c.upSertsini');
            component.set("v.disabled",true);
            action3V.setParams({'sinIn': siniestr});
            action3V.setCallback(this, function(response) {
                if (response.getState() === 'SUCCESS') {
                    helper.toastSuccessVida(component,event,helper);
                component.set("v.disabled",true);
                }
                else{
                    component.set("v.disabled",false);
                }
            });
            $A.enqueueAction(action3V);
        }
    },
    setFalse: function(component,event,helper){
    	component.set("v.validate",'false');
    	helper.valMobile(component,event,helper);
    },
    toastInfo: function(component,event,helper){
        var mensajeV=event.getParams().message;
        if(mensajeV.includes("Porfavor")) {
			component.set("v.validate",'true');
        }
    },
    setFalse2: function(component,event,helper){
    	component.set("v.validate",'false');
    	helper.valMobile2(component,event,helper);
    },
    setPhone: function(component,event,helper) {
        component.set("v.validate",'false');
        var phoneV = component.find('PhoneIdenAtn');
		var phoneValueV = phoneV.get('v.value');
        component.set("v.sinObj.MX_SB_MLT_Telefono__c",phoneValueV.replace(/[^0-9\.]+/g,''));
        var phoneValidity = component.find("PhoneIdenAtn").get("v.validity");
        var phoneValid = phoneValidity.valid;
        if(isNaN(phoneValueV)){
			component.set('v.errorMessage1',"Porfavor ingrese un numero valido");
        }
        if(phoneValid===false){
			component.set('v.errorMessage1',"Su entrada es demasiado corta.");
        }
		else{
            component.set('v.errorMessage1',null);
    	}
    },
    setFalse3: function(component,event,helper) {
        component.set("v.validate",'false');
		helper.valExt1(component,event,helper);
    },
    caps1:function(component,event,helper){
        var NombCondV= component.get("v.sinObj.MX_SB_MLT_NombreConductor__c");
        component.set("v.sinObj.MX_SB_MLT_NombreConductor__c",NombCondV.replace(/[^\w\s]/gi,'').toUpperCase());
    },
    setFalse4: function(component,event,helper) {
        component.set("v.validate",'false');
		helper.valExt2(component,event,helper);
    },
    caps3:function(component,event,helper){
        var AMCondV= component.get("v.sinObj.MX_SB_MLT_AMaternoConductor__c");
        component.set("v.sinObj.MX_SB_MLT_AMaternoConductor__c",AMCondV.replace(/[^\w\s]/gi,'').toUpperCase());
    },
    caps4:function(component,event,helper){
        var mail= component.get("v.sinObj.MX_SB_MLT_CorreoElectronico__c");
        component.set("v.sinObj.MX_SB_MLT_CorreoElectronico__c",mail.toLowerCase());
    },
    caps2:function(component,event,helper){
        var APCondV= component.get("v.sinObj.MX_SB_MLT_APaternoConductor__c");
        component.set("v.sinObj.MX_SB_MLT_APaternoConductor__c",APCondV.replace(/[^\w\s]/gi,'').toUpperCase());
    },
})