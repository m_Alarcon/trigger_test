({
	valExt1:function(component,event){
        var Exte1= component.get('v.sinObj.MX_SB_MLT_ExtPhone__c');
        component.set('v.sinObj.MX_SB_MLT_ExtPhone__c',Exte1.replace(/[^0-9\.]+/g,''));
    },
    valExt2:function(component,event){
        var Exte2= component.get('v.sinObj.MX_SB_MLT_ExtPhone2__c');
        component.set('v.sinObj.MX_SB_MLT_ExtPhone2__c',Exte2.replace(/[^0-9\.]+/g,''));
	},
    requiredFields: function(component) {
        var nombreV = component.find("NameIdenAtn");
        nombreV.reportValidity();
        nombreV = nombreV.get("v.validity");
        var apaternoV = component.find("LastNameIdenAtn");
        apaternoV.reportValidity();
        apaternoV=apaternoV.get("v.validity");
        var amaternoV = component.find("LastName2IdenAtn");
        amaternoV.reportValidity();
        amaternoV = amaternoV.get("v.validity");
        if(!nombreV.valid || !apaternoV.valid || !amaternoV.valid) {
            	this.toastTelefono();
		}
        else{
            component.set("v.validate",'false');
        }
    },
    toastSuccessVida: function(){
        var toastEvtVida = $A.get("e.force:showToast");
        toastEvtVida.setParams({
            title: 'Enhorabuena',
            message: 'Se ha guardado el registro exitosamente',
            key: 'info_alt',
            type: 'Success'
        });
        toastEvtVida.fire();
    },
    toastTelefono : function () {
        var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error:',
                message:'Porfavor llene los campos obligatorios.',
                duration:' 2000',
                key: 'info_alt',
                type: 'error'
            });
            toastEvent.fire();
    },
    valMobile2: function(component,event) {
		var MobileV2 = component.find('MobilePhoneIdenAtn2');
		var MobileValueV2 = MobileV2.get('v.value');
        component.set("v.sinObj.MX_SB_MLT_Mobilephone2__c",MobileValueV2.replace(/[^0-9\.]+/g,''));
        var phoneValidityV2 = component.find("MobilePhoneIdenAtn2").get("v.validity");
        var phoneValidV2 = phoneValidityV2.valid;
        if(phoneValidV2===false) {
            component.set('v.errorMessage2',"Su entrada es demasiado corta.");
        }
        if(isNaN(MobileValueV2)) {
			component.set('v.errorMessage2',"Porfavor ingrese un numero valido");
        }
		else{
            component.set('v.errorMessage2',null);
    	}
	},
        valMobile: function(component,event) {
		var MobileV = component.find('MobilePhoneIdenAtn');
		var MobileValueV = MobileV.get('v.value');
        component.set("v.sinObj.MX_SB_MLT_Mobilephone__c",MobileValueV.replace(/[^0-9\.]+/g,''));
        var phoneValidityV = component.find("MobilePhoneIdenAtn").get("v.validity");
        var phoneValid = phoneValidityV.valid;
        if(phoneValid===false) {
            component.set('v.errorMessage2',"Su entrada es demasiado corta.");
        }
        if(isNaN(MobileValueV)) {
			component.set('v.errorMessage2',"Porfavor ingrese un numero valido");
        }
		else{
            component.set('v.errorMessage2',null);
    	}
	},
    changeTabNamePar : function(component, event) {
       var workspaceAPITCV = component.find("workspace");
       workspaceAPITCV.getFocusedTabInfo().then(function(response) {
                       var focusedTabId = response.tabId;
                       workspaceAPITCV.setTabLabel({
                       tabId: focusedTabId,
                       title: "Crear Siniestro/Asistencia",
                       label: "Crear Siniestro/Asistencia"
                       });
			})
       .catch(function(error) {
    });
    },
})