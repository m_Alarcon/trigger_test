({
    showInfo: function(component, event, helper) {
        var selectedMenuItemValU = event.getParam("value");
		var selectedMenuItemValue= selectedMenuItemValU.toLowerCase();
        component.set("v.toggleAddrem",!component.get('v.toggleAddrem'));
        component.set('v.sinObj.MX_SB_MLT_PhoneTypeAdditional__c',selectedMenuItemValue)
        if(selectedMenuItemValue==='oficina'){
           component.set("v.BooleanExt1",true)
        }
    },
    closeToggle: function(cmp, event, helper) {
        cmp.set("v.toggleAddrem",!cmp.get('v.toggleAddrem'));
        cmp.set("v.sinObj.MX_SB_MLT_Mobilephone__c",'');
        cmp.set("v.sinObj.MX_SB_MLT_ExtPhone__c",'');
     cmp.set("v.BooleanExt1",false)
    },
    showInfo2: function(component, event, helper) {
    var selectedMenuItemValuE = event.getParam("value");
	var selectedMenuItemValue= selectedMenuItemValuE.toLowerCase();
        component.set('v.sinObj.MX_SB_MLT_PhoneTypeAdditional2__c',selectedMenuItemValue);
        component.set("v.toggleAddrem2",!component.get('v.toggleAddrem2'));
		if(selectedMenuItemValue==='oficina'){
           component.set("v.BooleanExt2",true)
        }
    },
	closeToggle2: function(component, event, helper) {
    component.set("v.sinObj.MX_SB_MLT_Mobilephone2__c",'');
     component.set("v.toggleAddrem2",!component.get('v.toggleAddrem2'));
     component.set("v.sinObj.MX_SB_MLT_ExtPhone2__c",'');
     component.set("v.BooleanExt2",false)
    },
    doInitLayDanos : function(component, event, helper) {
	helper.changeTabNamePar(component, event);
    var action11 = component.get("c.getRecordType");
    component.set("v.sinObj.MX_SB_MLT_Telefono__c",component.get("v.Telefono"))
	action11.setParams({"devname":"MX_SB_MLT_RamoDanos"});
    action11.setCallback(this, function(response) {
            component.set("v.recordtypeDanos",response.getReturnValue());
        });
        $A.enqueueAction(action11);
    },
    setPhoneType: function(component, helper){
        var tipophoneD= component.find("phoneType");
        var tipophoneValD= tipophoneD.get("v.value");
        component.set("v.sinObj.MX_SB_MLT_PhoneType__c",tipophoneValD);
    },
     setPhoneAdType:function(component,event, helper) {
        var tipophoneAdValD= component.get("v.sinObj.MX_SB_MLT_PhoneTypeAdditional__c");
        component.set("v.sinObj.MX_SB_MLT_PhoneTypeAdditional__c",tipophoneAdValD);
        if(tipophoneAdValD==="oficina"){
            component.set("v.BooleanExt1",true);
        }
        else{
            component.set("v.BooleanExt1",false);
        }
    },
    setPhoneAdType2:function(component,event, helper) {
        var tipophoneAdVal2= component.get("v.sinObj.MX_SB_MLT_PhoneTypeAdditional2__c");
        component.set("v.sinObj.MX_SB_MLT_PhoneTypeAdditional2__c",tipophoneAdVal2);
        if(tipophoneAdVal2==="oficina"){
            component.set("v.BooleanExt2",true);
        }
        else{
            component.set("v.BooleanExt2",false);
        }
    },
    NuevoOnsuccessDanos: function(component, event, helper) {
        var recordtpDanos= component.get("v.recordtypeDanos");
        component.set("v.sinObj.RecordTypeId",recordtpDanos);
         var recId=component.get("v.recordId");
        if(recId!==''){
            component.set("v.sinObj.Id", recId);
        }
        component.set("v.sinObj.MX_SB_MLT_Telefono__c",component.get("v.Telefono"));
        helper.requiredFields(component,event,helper);
        if(component.get("v.validate")==='true') {
            component.set("v.validate","true");
        }
        if(component.get("v.validate")==='false') {
            var sinies= component.get("v.sinObj");
            component.set("v.disabled",true);
            var action25 = component.get('c.upSertsini');
            action25.setParams({'sinIn': sinies});
            action25.setCallback(this, function(response) {
                if (response.getState() === 'SUCCESS') {
                    helper.toastSuccessDanos(component,event,helper);
                    component.set("v.disabled",true);
                }
                else{
                    component.set("v.disabled",false);
                }
            });
            $A.enqueueAction(action25);
        }
        if(component.get("v.validate")===undefined) {
            var siniestr= component.get("v.sinObj");
            var action31 = component.get('c.upSertsini');
            action31.setParams({'sinIn': siniestr});
            component.set("v.disabled",true);
            action31.setCallback(this, function(response) {
                if (response.getState() === 'SUCCESS') {
                    helper.toastSuccessDanos(component,event,helper);
               		component.set("v.disabled",true);
                }
                else{
                    component.set("v.disabled",false);
                }
            });
            $A.enqueueAction(action31);
        }
    },
    setFalse: function(component,event,helper){
        helper.valMobile(component,event,helper);
        component.set("v.validate",'false');
    },
	setFalse2: function(component,event,helper) {
        helper.valMobile2(component,event,helper);
    	component.set("v.validate",'false');
    },
    toastInfo: function(component,event,helper){
        var mensaje3=event.getParams().message;
        if(mensaje3.includes("Porfavor")) {
			component.set("v.validate",'true');
        }
    },
    setPhone: function(component,event,helper) {
        component.set("v.validate",'false');
        var phoneD = component.find('PhoneIdenAtn');
		var phoneValueD = phoneD.get('v.value');
        component.set("v.sinObj.MX_SB_MLT_Telefono__c",phoneValueD.replace(/[^0-9\.]+/g,''));

        var phoneValidityD = component.find("PhoneIdenAtn").get("v.validity");
        var phoneValidD = phoneValidityD.valid;
        if(isNaN(phoneValueD)){
			component.set('v.errorMessage1',"Porfavor ingrese un numero valido");
        }
        if(phoneValidD===false){
			component.set('v.errorMessage1',"Su entrada es demasiado corta.");
        }
		else{
            component.set('v.errorMessage1',null);
    	}
    },
    caps4:function(component,event,helper){
        var mail= component.get("v.sinObj.MX_SB_MLT_CorreoElectronico__c");
        component.set("v.sinObj.MX_SB_MLT_CorreoElectronico__c",mail.toLowerCase());
    },
    caps1:function(component,event,helper) {
        var NombCondD= component.get("v.sinObj.MX_SB_MLT_NombreConductor__c");
        component.set("v.sinObj.MX_SB_MLT_NombreConductor__c",NombCondD.replace(/[^\w\s]/gi,'').toUpperCase());
    },
      	setFalse4: function(component,event,helper) {
        component.set("v.validate",'false');
		helper.valExt2(component,event,helper);
    },
    setFalse3: function(component,event,helper) {
        component.set("v.validate",'false');
		helper.valExt1(component,event,helper);
    },
    caps2:function(component,event,helper) {
        var APCondD= component.get("v.sinObj.MX_SB_MLT_APaternoConductor__c");
        component.set("v.sinObj.MX_SB_MLT_APaternoConductor__c",APCondD.replace(/[^\w\s]/gi,'').toUpperCase());
    },
    caps3:function(component,event,helper) {
        var AMCondD= component.get("v.sinObj.MX_SB_MLT_AMaternoConductor__c");
        component.set("v.sinObj.MX_SB_MLT_AMaternoConductor__c",AMCondD.replace(/[^\w\s]/gi,'').toUpperCase());
    },
})