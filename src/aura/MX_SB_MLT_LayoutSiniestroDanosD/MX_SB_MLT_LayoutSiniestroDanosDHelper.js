({
    requiredFields: function(component) {
        var nombreD = component.find("NameIdenAtn");
        nombreD.reportValidity();
        nombreD = nombreD.get("v.validity");
        var apaternoD = component.find("LastNameIdenAtn");
        apaternoD.reportValidity();
        apaternoD=apaternoD.get("v.validity");
        var amaternoD = component.find("LastName2IdenAtn");
        amaternoD.reportValidity();
        amaternoD = amaternoD.get("v.validity");
        if(!nombreD.valid || !apaternoD.valid || !amaternoD.valid) {
            	this.toastTelefono();
		}
        else{
            component.set("v.validate",'false');
        }
    },
    toastTelefono : function () {
        var toastEventD = $A.get("e.force:showToast");
            toastEventD.setParams({
                title : 'Error:',
                message:'Porfavor llene los campos obligatorios.',
                duration:' 2000',
                key: 'info_alt',
                type: 'error'
            });
            toastEventD.fire();
    },
    toastSuccessDanos: function(){
        var toastEvtDanos = $A.get("e.force:showToast");
        toastEvtDanos.setParams({
            title: 'Enhorabuena',
            message: 'Se ha guardado el registro exitosamente',
            key: 'info_alt',
            type: 'Success'
        });
        toastEvtDanos.fire();
    },
    valMobile: function(component,event) {
		var MobileD = component.find('MobilePhoneIdenAtn');
		var MobileValueD = MobileD.get('v.value');
        component.set("v.sinObj.MX_SB_MLT_Mobilephone__c",MobileValueD.replace(/[^0-9\.]+/g,''));
        var phoneValidityD = component.find("MobilePhoneIdenAtn").get("v.validity");
        var phoneValid = phoneValidityD.valid;
        if(phoneValid===false) {
            component.set('v.errorMessage2',"Su entrada es demasiado corta.");
        }
        if(isNaN(MobileValueD)) {
			component.set('v.errorMessage2',"Porfavor ingrese un numero valido");
        }
		else{
            component.set('v.errorMessage2',null);
    	}
	},
	    valExt2:function(component,event){
        var Exte2= component.get('v.sinObj.MX_SB_MLT_ExtPhone2__c');
        component.set('v.sinObj.MX_SB_MLT_ExtPhone2__c',Exte2.replace(/[^0-9\.]+/g,''));
	},
    valMobile2: function(component,event) {
		var Mobile2 = component.find('MobilePhoneIdenAtn2');
		var MobileValueD2 = Mobile2.get('v.value');
        component.set("v.sinObj.MX_SB_MLT_Mobilephone2__c",MobileValueD2.replace(/[^0-9\.]+/g,''));
        var phoneValidity2 = component.find("MobilePhoneIdenAtn2").get("v.validity");
        var phoneValid2 = phoneValidity2.valid;
        if(phoneValid2===false) {
            component.set('v.errorMessage2',"Su entrada es demasiado corta.");
        }
        if(isNaN(MobileValueD2)) {
			component.set('v.errorMessage2',"Porfavor ingrese un numero valido");
        }
		else{
            component.set('v.errorMessage2',null);
    	}
	},
    changeTabNamePar : function(component) {
       var workspaceAPID = component.find("workspace");
       workspaceAPID.getFocusedTabInfo().then(function(response) {
                       var focusedTabId = response.tabId;
                       workspaceAPID.setTabLabel({
                       tabId: focusedTabId,
                       label: "Crear Siniestro/Asistencia",
                       title: "Crear Siniestro/Asistencia"
                       });
			})
       .catch(function(error) {
    });
    },
         valExt1:function(component,event){
        var Exte1= component.get('v.sinObj.MX_SB_MLT_ExtPhone__c');
        component.set('v.sinObj.MX_SB_MLT_ExtPhone__c',Exte1.replace(/[^0-9\.]+/g,''));
    },
})