({
    doInitIdenAsistencias : function(component,event, helper) {
		helper.initAsistencias(component, event, helper);
    },
    submitSuccess : function(component, event, helper) {
		let response  = helper.validationFields(component, event, helper);
        let optionalRes = helper.validation(component, event, helper);
        if (response && optionalRes) {
            helper.sumbitChanges(component, event, helper);
        }
    },
    submitContinue : function(component, event, helper) {
        let response  = helper.validationFields(component, event, helper);
        let optionalRes = helper.validation(component, event, helper);
        component.set("v.siniestro.MX_SB_MLT_JourneySin__c",'Validación de Poliza');
        if (response && optionalRes) {
            helper.sumbitChanges(component, event, helper);
            window.location.reload();
        }
    },
    upperCase : function(component, event, helper) {
        let fieldSelected = event.getSource().getLocalId();
        helper.upperCase(component, event, helper, fieldSelected);
    },
     validacionForm : function(component, event, helper) {
        component.set('v.status','Identificación del Reportante');
        component.set('v.continuar',false);
        helper.telefono(component,event,helper);
    },
    validacionFormCon : function(component, event, helper) {
        component.set('v.status','Validación de Poliza');
        component.set('v.continuar',true);
        helper.telefono(component,event,helper);
    }
})