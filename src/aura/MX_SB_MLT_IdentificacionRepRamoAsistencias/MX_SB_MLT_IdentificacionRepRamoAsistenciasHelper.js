({
    initAsistencias : function (component, event, helper) {
        let recordId = component.get("v.recordId");
        let action = component.get("c.getDatosSiniestro");
        action.setParams({"idsini": recordId});
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.siniestro",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    sumbitChanges : function(component, event, helper) {
        let siniestro = component.get("v.siniestro");
        let submit = component.get("c.upSin");
        submit.setParams({"sinIn" : siniestro});
        submit.setCallback(this, function(response){
            let state = response.getState();
            if (state === "SUCCESS") {
                this.toastMessage();
            }
        });
        $A.enqueueAction(submit);
    },
    validation : function(component, event, helper) {
        let optional = false;
    	let phoneVal = component.find("phoneAd").get("v.validity");
        let maternoVal = component.find("ApellidoM").get("v.validity");
        if (phoneVal.valid && maternoVal.valid) {
            optional = true;
        } else {
			this.errorMessage();
        }
        return optional;
    },
    upperCase : function(component, event, helper, auraId) {
        switch (auraId) {
            case 'Nombre':
                let nameRepo = component.get("v.siniestro.MX_SB_MLT_NombreConductor__c");
                component.set("v.siniestro.MX_SB_MLT_NombreConductor__c",nameRepo.replace(/[^\w\s]/gi,'').toUpperCase());
                break;
            case 'ApellidoP':
                let lastNameP = component.get("v.siniestro.MX_SB_MLT_APaternoConductor__c");
                component.set("v.siniestro.MX_SB_MLT_APaternoConductor__c",lastNameP.replace(/[^\w\s]/gi,'').toUpperCase());
                break;
            case 'ApellidoM':
                let lastNameM = component.get("v.siniestro.MX_SB_MLT_AMaternoConductor__c");
                component.set("v.siniestro.MX_SB_MLT_AMaternoConductor__c",lastNameM.replace(/[^\w\s]/gi,'').toUpperCase());
                break;
            case 'phoneAd':
                let phoneAdditional = component.find("phoneAd").get("v.value");
                component.set("v.siniestro.MX_SB_MLT_Mobilephone__c",phoneAdditional.replace(/[^0-9\.]+/g,''));
           	break;
            default:
        }
    },
    validationFields : function(component, event, helper) {
        let completed = false;
        let nameVal = component.find("Nombre").get("v.validity");
        let paternoVal = component.find("ApellidoP").get("v.validity");
        let insuranceVal = component.find("Insurance").get("v.validity");
        let phoneTypeVal = component.find("Phone").get("v.validity");
        if (nameVal.valid && paternoVal.valid && insuranceVal.valid && phoneTypeVal.valid) {
            completed = true;
        } else {
			this.errorMessage();
        }
        return completed;
    },
    toastMessage : function(component, event, helper) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams( {
            "title": "Actualización de registro",
            "type":"success",
            "message": "Cambio exitoso"
        });
        toastEvent.fire();
    },
    errorMessage : function(component, event, helper) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams( {
            "title": "Error",
            "type":"error",
            "message": "No se puede guardar por caracteres especiales"
        });
        toastEvent.fire();
    },
	telefono : function(component, event,helper) {
    	var telefono = component.find("TelefonoIdentAtn").get("v.value");
        var nombre = component.find("nombretxt").get("v.value");
        var apellido = component.find("apellidotxt").get("v.value");
        var toastEvent = $A.get("e.force:showToast");
        var valida = true;
        var mensaje = '';
        if(nombre==null) {
            valida=false;
            mensaje ='El nombre es un campo obligatorio';
        } else if(nombre.length<1) {
                valida=false;
                mensaje ='El nombre no puede estar vacío';
            }
             if(apellido==null) {
                 valida=false;
                 mensaje ='El apellido es un campo obligatorio';
            } else if(apellido.length<1) {
                valida=false;
                mensaje ='El apellido no puede estar vacío';
            }
        if(telefono.length > 10 || telefono.length < 10) {
           mensaje ='El número debe ser de 10 digitos';
            valida=false;
        }
        toastEvent.setParams( {
                title : 'Error:',
                message:mensaje,
                duration:' 5000',
                key: 'info_alt',
                type: 'error'
            });
        if(valida) {
            component.find("SiniestroEditForm").submit();
        } else {
            toastEvent.fire();
        }
    },
})