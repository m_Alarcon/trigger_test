/*
*Autor:         Gabriel Garcia
*Proyecto:      Retail Migración
*Descripción:   Trigger para ejecución de eventos, con paquete gClendar.
*_______________________________________________________________________________________
*Versión    Fecha       Autor                           Descripción
*1.0        17/10/2019  Gabriel García                  Creación
*/
trigger MX_Event_tgr on Event (after insert, after update, after delete) {
    MX_Event_Handler eventHandler = new MX_Event_Handler();
}