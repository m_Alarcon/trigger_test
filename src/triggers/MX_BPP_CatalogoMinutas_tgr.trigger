/**
* ------------------------------------------------------------------------------------------------
* @Name     	MX_BPP_CatalogoMinutas_tgr
* @Author   	Edmundo Zacarias Gómez | edmundo.zacarias.gomez@bbva.com
* @Date     	Created: 2020-03-02
* @Description 	MX_BPP_CatalogoMinutas Trigger
* @Changes
*
*/
trigger MX_BPP_CatalogoMinutas_tgr on MX_BPP_CatalogoMinutas__c (before insert, before update) {
	new MX_BPP_CatalogoMinutas_TriggerHandler().run();
}