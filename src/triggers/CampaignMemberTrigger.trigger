/*
*Autor:         Gabriel Garcia
*Proyecto:      Retail BPyP
*Descripción:   Trigger para CampaignMember.
*_______________________________________________________________________________________
*Versión    Fecha       Autor                           Descripción
*1.0        24/06/2020  Jair Ignacio Gonzalez G.        Creación
*/
trigger CampaignMemberTrigger on CampaignMember (before insert, before update, after insert, after update) {
    new MX_RTL_CampaignMembers().run();
}