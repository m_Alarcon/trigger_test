/**
 * @File Name          : MX_SB_SAC_Contract.trigger
 * @Description        : 
 * @Author             : Jaime Terrats
 * @Group              : 
 * @Last Modified By   : Jaime Terrats
 * @Last Modified On   : 12/21/2019, 12:17:55 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/21/2019   Jaime Terrats     Initial Version
**/
trigger MX_SB_SAC_Contract on Contract (before insert, before update) {
    new MX_SB_SAC_ContractTriggerHandler().run();
}