/**
* @author bbva.com developers
* @date 2019
*
* @group global_hub_kit_visit
*
* @description trigger for dwp_kitv__Visit__c
**/
trigger VisitTrigger on dwp_kitv__Visit__c (after insert) {
    GBL_PersonAccountVisit_Cls.relateContactVisit(Trigger.new);
}