/**
*Desarrollado por Softtek
*
*Autor :        Cristian Espinosa
*Proyecto :     MAX BBVA Bancomer
*Descripción :  Trigger general para el objeto oportunidad. Las clases que contienen la
*               lógica deben ser llamadas en el Handler de este trigger segun el tiempo
*               de ejecución requerido.
*______________________________________________________________________________________
*Version    Fecha           Autor                   Descripción
*1.0        11/Ago/2017     Cristian Espinosa       Creación.
*1.1        21/11/2017		Arsenio Perez			Adaptacion Kevin O
*2.0        09-Mar-2016     Javier Tibamoza         Creación de la Clase
*2.1        27-Abr-2017     Manuel Mendez           Modificacion de la Clase
*2.2		13-Feb-2019	    Javier Ortiz Flores		Se agrega method para crear contratos en after update para proceso Outbound.
*2.3		13-Feb-2019	    Javier Ortiz Flores 	Se delimita funcionalidad Outbound por tipo de registro para los tiempos de ejecución.
*3.0        11/03/2020      Gabriel Garcia          Se realiza la unión de hanlder para oportunidad, ejecutado por un sólo trigger.
**/
trigger MX_Opportunity_tgr on Opportunity ( before insert, before update, after insert, after update) {
    new MX_SB_VTS_OpportunityTriggerHandler().run();
}