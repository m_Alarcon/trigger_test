initMap();
var map;
var marker2;
var id;
var markers= {};
var infowindow=new google.maps.InfoWindow;
function initMap() {
    var parcial='';
    var arrInput=document.getElementsByTagName("input");
    for(var i=0;i<arrInput.length;i++){
        var strInput = arrInput[i].id;
        var arrIds = strInput.split(':');
        if(arrIds.length>2){
            if(arrIds.includes('dirtxt')){
                 parcial = strInput.split('dirtxt');
            }
        }
    }
    var directionsService=new google.maps.DirectionsService();
    var directionsDisplay=new google.maps.DirectionsRenderer();
    var input = document.getElementById("address");
    input.addEventListener("keydown", function(event) {	
        if (event.keyCode === 13 || event.keyCode === 9 ) {
            event.preventDefault();
            document.getElementById("submit").click();
        }
    });

    var Origen=document.getElementById(parcial[0]+'Origintxt').value;
    if(Origen=="Phone") {
        var coordtexto=document.getElementById(parcial[0]+'loctxt').value;
        var array=coordtexto.split(',');
        var latIni=parseFloat(array[0]);
        var lonIni=parseFloat(array[1]);
        var mapOptions= {
	        zoom: 17, 
		    center: new google.maps.LatLng(latIni, lonIni), 
		    mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map=new google.maps.Map(document.getElementById('map2'), mapOptions);
        directionsDisplay.setMap(map);
        directionsDisplay.setPanel(document.getElementById('right-panel'));
        var control=document.getElementById('floating-panel');
        control.style.display='block';
        var onChangeHandler=function() {
            calculateAndDisplayRoute(directionsService, directionsDisplay);
        };
        document.getElementById('address').addEventListener('change', function(){
      		directionsDisplay.set('directions', null);
            var summaryPanelNull = document.getElementById('directions-panel');
     		summaryPanelNull.innerHTML = '';  
        });
        document.getElementById('end').addEventListener('change', onChangeHandler);
        var geocoder=new google.maps.Geocoder();
        document.getElementById('submit').addEventListener('click', function() {
            directionsDisplay.set('directions', null);
            geocodeAddress(geocoder, map, infowindow);
        });
        marker2=new google.maps.Marker( {
		map: map, 
		draggable: true, 
		animation: true, 
		label: 'A', 
		position: new google.maps.LatLng(latIni, lonIni),          
        });
        google.maps.event.addListener(marker2, 'dragend', function(evt) {
            var a=document.getElementById(parcial[0]+'loctxt').value=evt.latLng.lat().toFixed(7) +' , '+evt.latLng.lng().toFixed(7);
            geocoder.geocode({'address': a}, function(results, status) {
                if (status==='OK') {
                    document.getElementById(parcial[0]+'dirtxt').value=results[0].formatted_address;
                    document.getElementById('address').value=results[0].formatted_address;
                    document.getElementById('start').value=results[0].formatted_address;
                    document.getElementById(parcial[0]+'PostalCodetxt').value=results[0].address_components[6].long_name;
                    document.getElementById(parcial[0]+'StNumbertxt').value=results[0].address_components[0].long_name;
                    document.getElementById(parcial[0]+'StreetNametxt').value=results[0].address_components[1].long_name;
                    document.getElementById(parcial[0]+'Colonytxt').value=results[1].address_components[2].long_name;
                    document.getElementById(parcial[0]+'Statetxt').value=results[0].address_components[4].long_name;
                }
            });
        });
        var address=document.getElementById(parcial[0]+'dirtxt').value;
        geocoder.geocode({'address': address}, function(results, status) {
            marker2.setMap(null);
            if (status==='OK') {
                map.setCenter(results[0].geometry.location);
                var a=document.getElementById(parcial[0]+'loctxt').value=results[0].geometry.location.lat().toFixed(7)+',' +results[0].geometry.location.lng().toFixed(7);
                var b=document.getElementById(parcial[0]+'dirtxt').value=results[0].formatted_address;
               	var h=document.getElementById('address').value=results[0].formatted_address;
                var g=document.getElementById(parcial[0]+'PostalCodetxt').value=results[0].address_components[6].long_name;
                var c=document.getElementById(parcial[0]+'StNumbertxt').value=results[0].address_components[0].long_name;
                var d=document.getElementById(parcial[0]+'StreetNametxt').value=results[0].address_components[1].long_name;
                var e=document.getElementById(parcial[0]+'Colonytxt').value=results[0].address_components[2].long_name;
                var f=document.getElementById(parcial[0]+'Statetxt').value=results[0].address_components[5].long_name;
                map.setCenter(results[0].geometry.location);
                marker2=new google.maps.Marker({
			map: map, 
			label: 'A',
			draggable: true,
			position: results[0].geometry.location
                });
                google.maps.event.addListener(marker2, 'dragend', function(evt) {
                   var a =document.getElementById(parcial[0]+'loctxt').value=evt.latLng.lat().toFixed(7) +' , '+evt.latLng.lng().toFixed(7);
                   geocoder.geocode({'address': a}, function(results, status) {
                        if (status==='OK') {
                            document.getElementById(parcial[0]+'dirtxt').value=results[0].formatted_address;
                            document.getElementById('address').value=results[0].formatted_address;
                            document.getElementById(parcial[0]+'PostalCodetxt').value=results[0].address_components[6].long_name;
                            document.getElementById(parcial[0]+'StNumbertxt').value=results[0].address_components[0].long_name;
                            document.getElementById(parcial[0]+'StreetNametxt').value=results[0].address_components[1].long_name;
                            document.getElementById(parcial[0]+'Colonytxt').value=results[1].address_components[2].long_name;
                            document.getElementById(parcial[0]+'Statetxt').value=results[0].address_components[4].long_name;
                        }
                    });
                });
            }
        });
        google.maps.event.addListener(marker2, 'click', function(evt) {
            var a=document.getElementById(parcial[0]+'loctxt').value=evt.latLng.lat().toFixed(7) +' , '+evt.latLng.lng().toFixed(7);
            geocoder.geocode({'address': a}, function(results, status) {
                if (status==='OK') {
                    document.getElementById(parcial[0]+'dirtxt').value=results[0].formatted_address;
                    document.getElementById('address').value=results[0].formatted_address;
                    document.getElementById(parcial[0]+'PostalCodetxt').value=results[0].address_components[6].long_name;
                    document.getElementById(parcial[0]+'StNumbertxt').value=results[0].address_components[0].long_name;
                    document.getElementById(parcial[0]+'StreetNametxt').value=results[0].address_components[1].long_name;
                    document.getElementById(parcial[0]+'Colonytxt').value=results[0].address_components[2].long_name;
                    document.getElementById(parcial[0]+'Statetxt').value=results[0].address_components[4].long_name;
                    document.getElementById(parcial[0]+'Countrytxt').value=results[0].address_components[5].long_name;
                }
            });
        });
    }
    else {
        var coordtexto=document.getElementById(parcial[0]+'loctxt').value;
        var array=coordtexto.split(',');
        var latIni=parseFloat(array[0]);
        var lonIni=parseFloat(array[1]);
        var mapOptions= {
            zoom: 17,
	    accuracy: 10,
	    center: new google.maps.LatLng(latIni, lonIni),
	    mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map=new google.maps.Map(document.getElementById('map2'), mapOptions);
        marker2=new google.maps.Marker({
            map: map,
	    draggable: false,
	    animation: true,
	    position: new google.maps.LatLng(latIni, lonIni),
        });
    }
}

function geocodeAddress(geocoder, resultsMap) {
    var parcial='';
    var arrInput=document.getElementsByTagName("input");
    for(var i=0;i<arrInput.length;i++){
        var strInput = arrInput[i].id;
        var arrIds = strInput.split(':');
        if(arrIds.length>2){
            if(arrIds.includes('dirtxt')){
                 parcial = strInput.split('dirtxt');
            }
        }
    }
    var Origen=document.getElementById(parcial[0]+'Origintxt').value;
    if(Origen=="Phone") {
        var address=document.getElementById('address').value;
        geocoder.geocode({'address': address}, function(results, status) {
          
            marker2.setMap(null);
            if (status==='OK') {
                resultsMap.setCenter(results[0].geometry.location);
                var a=document.getElementById(parcial[0]+'loctxt').value=results[0].geometry.location.lat().toFixed(7)+',' +results[0].geometry.location.lng().toFixed(7);
                var h=document.getElementById('address').value=results[0].formatted_address;
                var b=document.getElementById(parcial[0]+'dirtxt').value=results[0].formatted_address;
                var x=document.getElementById('start').value=results[0].formatted_address;
                var g=document.getElementById(parcial[0]+'PostalCodetxt').value=results[0].address_components[6].long_name;
                var c=document.getElementById(parcial[0]+'StNumbertxt').value=results[0].address_components[0].long_name;
                var d=document.getElementById(parcial[0]+'StreetNametxt').value=results[0].address_components[1].long_name;
                var e=document.getElementById(parcial[0]+'Colonytxt').value=results[0].address_components[2].long_name;
                var f=document.getElementById(parcial[0]+'Statetxt').value=results[0].address_components[5].long_name;
                resultsMap.setCenter(results[0].geometry.location);
                marker2=new google.maps.Marker( {
                    map: resultsMap,
		    label: 'A',
		    draggable: true,
		    position: results[0].geometry.location

                });
                google.maps.event.addListener(marker2, 'click', function(evt) {
                    var a=document.getElementById(parcial[0]+'loctxt').value=evt.latLng.lat().toFixed(7) +' , '+evt.latLng.lng().toFixed(7);
                    geocoder.geocode({'address': a}, function(results, status) {
                        if (status==='OK') {
                            document.getElementById(parcial[0]+'dirtxt').value=results[0].formatted_address;
                            document.getElementById('address').value=results[0].formatted_address;
                            document.getElementById('start').value=results[0].formatted_address;
                            document.getElementById(parcial[0]+'PostalCodetxt').value=results[0].address_components[6].long_name;
                            document.getElementById(parcial[0]+'StNumbertxt').value=results[0].address_components[0].long_name;
                            document.getElementById(parcial[0]+'StreetNametxt').value=results[0].address_components[1].long_name;
                            document.getElementById(parcial[0]+'Colonytxt').value=results[0].address_components[2].long_name;
                            document.getElementById(parcial[0]+'Statetxt').value=results[0].address_components[4].long_name;
                        }
                    });
                });
                google.maps.event.addListener(marker2, 'dragend', function(evt) {
                    var a=document.getElementById(parcial[0]+'loctxt').value=evt.latLng.lat().toFixed(7) +' , '+evt.latLng.lng().toFixed(7);
                    geocoder.geocode({'address': a}, function(results, status) {
                        if (status==='OK') {
                            document.getElementById(parcial[0]+'dirtxt').value=results[0].formatted_address;
                            document.getElementById('address').value=results[0].formatted_address;
                            document.getElementById('start').value=results[0].formatted_address;
                            document.getElementById(parcial[0]+'PostalCodetxt').value=results[0].address_components[6].long_name;
                            document.getElementById(parcial[0]+'StNumbertxt').value=results[0].address_components[0].long_name;
                            document.getElementById(parcial[0]+'StreetNametxt').value=results[0].address_components[1].long_name;
                            document.getElementById(parcial[0]+'Colonytxt').value=results[0].address_components[2].long_name;
                            document.getElementById(parcial[0]+'Statetxt').value=results[0].address_components[4].long_name;
                        }
                    });
                });
                var infowindow=new google.maps.InfoWindow({
                    content: '<p>' + marker.getPosition(6).lat()+',' + marker.getPosition(6).lng() + '</p>'+ '<p>'+ results[0].formatted_address+'</p>'
                });
                infowindow.open(map, marker);
                id=marker.__gm_id 
				markers[id]=marker;
                google.maps.event.addListener(marker, 'rightclick', function (point) {
                    id=this.__gm_id;
                    delMarker(id)
                });
                var delMarker=function (id) {
                    marker=markers[id];
                    marker.setMap(null);
                }
            }
            else {
                resultsMap.setCenter(results[0].geometry.location)
            }
        });
    }
}

function toggleBounce() {
    if (marker.getAnimation() !==null) {
        marker.setAnimation(null);
    }
    else {
        marker.setAnimation(google.maps.Animation.DROP);
    }
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
   directionsService.route({
    origin: document.getElementById('start').value,
    destination: document.getElementById('end').value,
    travelMode: 'DRIVING'
  }, function(response, status) {
    if (status === 'OK') {
      directionsDisplay.setDirections(response);
      var route = response.routes[0];
      var summaryPanel = document.getElementById('directions-panel');
      summaryPanel.innerHTML = '';
      for (var i = 0; i < route.legs.length; i++) {
        var routeSegment = i + 1;
        summaryPanel.innerHTML += '<b>Distancia: ' + 
            '</b>';
        summaryPanel.innerHTML += route.legs[i].distance.text + '<br>';
      }
        
    } else {
      window.alert('Directions request failed due to ' + status);
      var summaryPanelNull = document.getElementById('directions-panel');
      summaryPanelNull.innerHTML = '';  
	  directionsDisplay.set('directions', null);
    }
  });
}